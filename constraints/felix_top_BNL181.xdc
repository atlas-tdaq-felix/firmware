#Bank 202 GTY_REFCLK0
set_property  PACKAGE_PIN L12      [get_ports  {sys_clk_n[0]}]
set_property  PACKAGE_PIN L13      [get_ports  {sys_clk_p[0]}]
#Bank 204 GTY_REFCLK0
set_property  PACKAGE_PIN G12      [get_ports  {sys_clk_n[1]}]
set_property  PACKAGE_PIN G13      [get_ports  {sys_clk_p[1]}]
 
#Bank 202 / 203 / 204 / 205
set_property  PACKAGE_PIN V1    [get_ports  {pcie_rxn[0]}]    
set_property  PACKAGE_PIN U3    [get_ports  {pcie_rxn[1]}]    
set_property  PACKAGE_PIN T1    [get_ports  {pcie_rxn[2]}]    
set_property  PACKAGE_PIN R3    [get_ports  {pcie_rxn[3]}]    
set_property  PACKAGE_PIN P1    [get_ports  {pcie_rxn[4]}]    
set_property  PACKAGE_PIN N3    [get_ports  {pcie_rxn[5]}]    
set_property  PACKAGE_PIN M1    [get_ports  {pcie_rxn[6]}]    
set_property  PACKAGE_PIN L3    [get_ports  {pcie_rxn[7]}]    
set_property  PACKAGE_PIN K1    [get_ports  {pcie_rxn[8]}]    
set_property  PACKAGE_PIN J3    [get_ports  {pcie_rxn[9]}]    
set_property  PACKAGE_PIN H1    [get_ports  {pcie_rxn[10]}]    
set_property  PACKAGE_PIN H5    [get_ports  {pcie_rxn[11]}]    
set_property  PACKAGE_PIN G3    [get_ports  {pcie_rxn[12]}]    
set_property  PACKAGE_PIN F1    [get_ports  {pcie_rxn[13]}]    
set_property  PACKAGE_PIN F5    [get_ports  {pcie_rxn[14]}]    
set_property  PACKAGE_PIN E3    [get_ports  {pcie_rxn[15]}]    

#PCIE_PERST_B                            501     PMC_MIO38_501   
set_property PACKAGE_PIN K21       [get_ports sys_reset_n]
set_property IOSTANDARD LVCMOS18   [get_ports sys_reset_n]

set_property    PACKAGE_PIN L17         [get_ports {leds[0]}] 
set_property    IOSTANDARD LVCMOS18     [get_ports {leds[0]}]
set_property    PACKAGE_PIN M17         [get_ports {leds[1]}]      
set_property    IOSTANDARD LVCMOS18     [get_ports {leds[1]}]
set_property    PACKAGE_PIN M20         [get_ports {leds[2]}]      
set_property    IOSTANDARD LVCMOS18     [get_ports {leds[2]}]
set_property    PACKAGE_PIN M19         [get_ports {leds[3]}]      
set_property    IOSTANDARD LVCMOS18     [get_ports {leds[3]}]

set_property PACKAGE_PIN BA12 [get_ports {SmaOut[0]}]
set_property PACKAGE_PIN AY13 [get_ports {SmaOut[1]}]
set_property PACKAGE_PIN BB13 [get_ports {SmaOut[2]}]
set_property PACKAGE_PIN BB14 [get_ports {SmaOut[3]}]

set_property IOSTANDARD LVCMOS15 [get_ports {SmaOut[0]}]
set_property IOSTANDARD LVCMOS15 [get_ports {SmaOut[1]}]
set_property IOSTANDARD LVCMOS15 [get_ports {SmaOut[2]}]
set_property IOSTANDARD LVCMOS15 [get_ports {SmaOut[3]}]

set_property PACKAGE_PIN M37 [get_ports {SI5345_A[3]}]
set_property PACKAGE_PIN L37 [get_ports {SI5345_A[2]}]
set_property PACKAGE_PIN J33 [get_ports {SI5345_A[1]}]
set_property PACKAGE_PIN J34 [get_ports {SI5345_A[0]}]
set_property PACKAGE_PIN M36 [get_ports {SI5345_INSEL[3]}]
set_property PACKAGE_PIN K37 [get_ports {SI5345_INSEL[2]}]
set_property PACKAGE_PIN L35 [get_ports {SI5345_INSEL[1]}]
set_property PACKAGE_PIN K36 [get_ports {SI5345_INSEL[0]}]
set_property PACKAGE_PIN L33 [get_ports {SI5345_OE[1]}]
set_property PACKAGE_PIN L34 [get_ports {SI5345_OE[0]}]
set_property PACKAGE_PIN N34 [get_ports {SI5345_RSTN[1]}]
set_property PACKAGE_PIN M35 [get_ports {SI5345_RSTN[0]}]
set_property PACKAGE_PIN G36 [get_ports {SI5345_SEL[1]}]
set_property PACKAGE_PIN G37 [get_ports {SI5345_SEL[0]}]
set_property PACKAGE_PIN J36 [get_ports {SI5345_nLOL[1]}]
set_property PACKAGE_PIN H36 [get_ports {SI5345_nLOL[0]}]   
        


set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_A[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_A[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_A[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_A[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_INSEL[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_INSEL[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_INSEL[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_INSEL[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_OE[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_OE[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_RSTN[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_RSTN[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_SEL[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_SEL[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_nLOL[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_nLOL[0]}]   

#FMC+ mezzanine
set_property PACKAGE_PIN AP15 [get_ports {SI5345_RSTN[3]}]
set_property PACKAGE_PIN AR15 [get_ports {SI5345_RSTN[2]}]
#Dummy pins on FMC+ mezzanine
set_property PACKAGE_PIN AV13 [get_ports {SI5345_SEL[3]}]
set_property PACKAGE_PIN AT14 [get_ports {SI5345_SEL[2]}]
set_property PACKAGE_PIN AR12 [get_ports {SI5345_nLOL[3]}]
set_property PACKAGE_PIN AP11 [get_ports {SI5345_nLOL[2]}]
set_property PACKAGE_PIN AP12 [get_ports {SI5345_A[7]}]
set_property PACKAGE_PIN AM21 [get_ports {SI5345_A[6]}]
set_property PACKAGE_PIN AM20 [get_ports {SI5345_A[5]}]
set_property PACKAGE_PIN AL16 [get_ports {SI5345_A[4]}]
set_property PACKAGE_PIN AM17 [get_ports {SI5345_INSEL[7]}]
set_property PACKAGE_PIN AT17 [get_ports {SI5345_INSEL[6]}]
set_property PACKAGE_PIN AU16 [get_ports {SI5345_INSEL[5]}]
set_property PACKAGE_PIN AN14 [get_ports {SI5345_INSEL[4]}]
set_property PACKAGE_PIN AP13 [get_ports {SI5345_OE[3]}]
set_property PACKAGE_PIN AR14 [get_ports {SI5345_OE[2]}]


set_property IOSTANDARD LVCMOS15  [get_ports {SI5345_A[7]}]
set_property IOSTANDARD LVCMOS15  [get_ports {SI5345_A[6]}]
set_property IOSTANDARD LVCMOS15  [get_ports {SI5345_A[5]}]
set_property IOSTANDARD LVCMOS15  [get_ports {SI5345_A[4]}]
set_property IOSTANDARD LVCMOS15  [get_ports {SI5345_INSEL[7]}]
set_property IOSTANDARD LVCMOS15  [get_ports {SI5345_INSEL[6]}]
set_property IOSTANDARD LVCMOS15  [get_ports {SI5345_INSEL[5]}]
set_property IOSTANDARD LVCMOS15  [get_ports {SI5345_INSEL[4]}]
set_property IOSTANDARD LVCMOS15  [get_ports {SI5345_OE[3]}]
set_property IOSTANDARD LVCMOS15  [get_ports {SI5345_OE[2]}]
set_property IOSTANDARD LVCMOS15  [get_ports {SI5345_RSTN[3]}]
set_property IOSTANDARD LVCMOS15  [get_ports {SI5345_RSTN[2]}]
set_property IOSTANDARD LVCMOS15  [get_ports {SI5345_SEL[3]}]
set_property IOSTANDARD LVCMOS15  [get_ports {SI5345_SEL[2]}]
set_property IOSTANDARD LVCMOS15  [get_ports {SI5345_nLOL[3]}]
set_property IOSTANDARD LVCMOS15  [get_ports {SI5345_nLOL[2]}]


#SI5345 B IN2
set_property PACKAGE_PIN BF18 [get_ports {clk40_ttc_ref_out_p[1]}] 
set_property IOSTANDARD LVDS15 [get_ports {clk40_ttc_ref_out_p[1]}]
#SI5345 A IN0
set_property PACKAGE_PIN BF11 [get_ports {clk40_ttc_ref_out_p[0]}]
set_property IOSTANDARD LVDS15 [get_ports {clk40_ttc_ref_out_p[0]}] 

set_property PACKAGE_PIN BA24 [get_ports {clk40_ttc_ref_out_p[2]}]
set_property IOSTANDARD LVDS15 [get_ports {clk40_ttc_ref_out_p[2]}]

set_property PACKAGE_PIN AR21 [get_ports {clk40_ttc_ref_out_p[3]}]
set_property IOSTANDARD LVDS15 [get_ports {clk40_ttc_ref_out_p[3]}]


#FMCP FF1..4 INTL, H19, H20, H22, H23
set_property PACKAGE_PIN AW20 [get_ports {OPTO_LOS[0]}]
set_property PACKAGE_PIN AY19 [get_ports {OPTO_LOS[1]}]
set_property PACKAGE_PIN AR18 [get_ports {OPTO_LOS[2]}]
set_property PACKAGE_PIN AT19 [get_ports {OPTO_LOS[3]}]
set_property IOSTANDARD LVCMOS15  [get_ports {OPTO_LOS[0]}]
set_property IOSTANDARD LVCMOS15  [get_ports {OPTO_LOS[1]}]
set_property IOSTANDARD LVCMOS15  [get_ports {OPTO_LOS[2]}]
set_property IOSTANDARD LVCMOS15  [get_ports {OPTO_LOS[3]}]
#FMCP FF1..4 RESETL, H13, H14, H16, H17
set_property PACKAGE_PIN AU24 [get_ports {opto_inhibit[0]}]
set_property PACKAGE_PIN AU23 [get_ports {opto_inhibit[1]}]
set_property PACKAGE_PIN AP24 [get_ports {opto_inhibit[2]}]
set_property PACKAGE_PIN AR24 [get_ports {opto_inhibit[3]}]
set_property IOSTANDARD LVCMOS15 [get_ports {opto_inhibit[0]}]
set_property IOSTANDARD LVCMOS15 [get_ports {opto_inhibit[1]}]
set_property IOSTANDARD LVCMOS15 [get_ports {opto_inhibit[2]}]
set_property IOSTANDARD LVCMOS15 [get_ports {opto_inhibit[3]}]

set_property    PACKAGE_PIN H18         [get_ports SDA]      
set_property    IOSTANDARD LVCMOS18     [get_ports SDA]
set_property    PACKAGE_PIN H19         [get_ports SCL]      
set_property    IOSTANDARD LVCMOS18     [get_ports SCL]
set_property    PACKAGE_PIN L18         [get_ports {i2cmux_rst[0]}]      
set_property    IOSTANDARD LVCMOS18     [get_ports {i2cmux_rst[0]}]
#I2Cmux RXTB on FMC+
set_property    PACKAGE_PIN AU13        [get_ports {i2cmux_rst[1]}]      
set_property    IOSTANDARD LVCMOS15     [get_ports {i2cmux_rst[1]}]

set_property PACKAGE_PIN BD23 [get_ports app_clk_in_p]
set_property IOSTANDARD LVDS15 [get_ports app_clk_in_p]
create_clock -name app_clk_in_200 -period 5.000 [get_ports app_clk_in_p]

set_property PACKAGE_PIN J20 [get_ports {TACH[0]}]
set_property IOSTANDARD LVCMOS18  [get_ports {TACH[0]}]



##GTY Bank 206: Firefly
#set_property PACKAGE_PIN C9  [get_ports {TX_P[8]}]
#set_property PACKAGE_PIN B11 [get_ports {TX_P[9]}]
#set_property PACKAGE_PIN A9  [get_ports {TX_P[10]}]
#set_property PACKAGE_PIN A13 [get_ports {TX_P[11]}]
#set_property PACKAGE_PIN C13 [get_ports {GTREFCLK_P_IN[2]}]
#GTY Bank 103, FMCP DP0-3
set_property PACKAGE_PIN AB41 [get_ports {TX_P[0]}]
set_property PACKAGE_PIN Y41 [get_ports {TX_P[1]}]
set_property PACKAGE_PIN V41 [get_ports {TX_P[2]}]
set_property PACKAGE_PIN U43 [get_ports {TX_P[3]}]
set_property PACKAGE_PIN W39 [get_ports {GTREFCLK_P_IN[0]}]
#FMCP_GBTCLK0_M2C_P
#GTY Bank 104
set_property PACKAGE_PIN T41 [get_ports {TX_P[4]}]
set_property PACKAGE_PIN R43 [get_ports {TX_P[5]}]
set_property PACKAGE_PIN P41 [get_ports {TX_P[6]}]
set_property PACKAGE_PIN M41 [get_ports {TX_P[7]}]
set_property PACKAGE_PIN R39 [get_ports {GTREFCLK_P_IN[1]}]
#SA_REF_CLK_6P
#GTY Bank 105
set_property PACKAGE_PIN K41 [get_ports {TX_P[8]}]
set_property PACKAGE_PIN J43 [get_ports {TX_P[9]}]
set_property PACKAGE_PIN H41 [get_ports {TX_P[10]}]
set_property PACKAGE_PIN G43 [get_ports {TX_P[11]}]
set_property PACKAGE_PIN L39 [get_ports {GTREFCLK_P_IN[2]}]
#GTY Bank 106
set_property PACKAGE_PIN F41 [get_ports {TX_P[12]}]
set_property PACKAGE_PIN D41 [get_ports {TX_P[13]}]
set_property PACKAGE_PIN B41 [get_ports {TX_P[14]}]
set_property PACKAGE_PIN A43 [get_ports {TX_P[15]}]
set_property PACKAGE_PIN G39 [get_ports {GTREFCLK_P_IN[3]}]
#FMCP_GBTCLK2_M2C_P

##GTY Bank 200: Firefly
set_property PACKAGE_PIN AF7 [get_ports {TX_P[16]}]
set_property PACKAGE_PIN AE9 [get_ports {TX_P[17]}]
set_property PACKAGE_PIN AD7 [get_ports {TX_P[18]}]
set_property PACKAGE_PIN AC9 [get_ports {TX_P[19]}]
set_property PACKAGE_PIN AF11 [get_ports {GTREFCLK_P_IN[4]}]
#
##GTY Bank 201: Firefly
set_property PACKAGE_PIN AB7 [get_ports {TX_P[20]}]
set_property PACKAGE_PIN AA9 [get_ports {TX_P[21]}]
set_property PACKAGE_PIN Y7  [get_ports {TX_P[22]}]
set_property PACKAGE_PIN W9  [get_ports {TX_P[23]}]
set_property PACKAGE_PIN AB11 [get_ports {GTREFCLK_P_IN[5]}]

#Use clocks from FMCP Si5345
#set_property PACKAGE_PIN U39 [get_ports {GTREFCLK_P_IN[0]}]
#set_property PACKAGE_PIN N39 [get_ports {GTREFCLK_P_IN[1]}]
#set_property PACKAGE_PIN J39 [get_ports {GTREFCLK_P_IN[2]}]
#set_property PACKAGE_PIN E39 [get_ports {GTREFCLK_P_IN[3]}]
#Use clocks from BNL181 Si5345

#set_property    PACKAGE_PIN AF43        [get_ports DDR4_DIMM1_CLK_N]
#set_property    PACKAGE_PIN AE42        [get_ports DDR4_DIMM1_CLK_P]
#set_property    IOSTANDARD DIFF_SSTL12  [get_ports DDR4_DIMM1_CLK_P] 
#set_property    IOSTANDARD DIFF_SSTL12  [get_ports DDR4_DIMM1_CLK_N] 

#create_clock -name DDR4_DIMM1_CLK -period 5.000 [get_ports DDR4_DIMM1_CLK_P]

create_generated_clock -name clk_out25_clk_wiz_regmap0 [get_pins -hierarchical -filter {NAME =~ "g_endpoints[0].pcie0/clk0/clk0/inst/clock_primitive_inst/MMCME5_inst/CLKOUT0"}]
create_generated_clock -name pcie_userclk0 [get_pins -hierarchical -filter {NAME =~ "g_endpoints[0].pcie0/ep0/g_NoSim.g_versal.pcie_ep_versal0/versal_cips_block_i/versal_cips_0/U0/DPLL_PCIE0_inst/CLKOUT0"}]
create_generated_clock -name clk_out25_clk_wiz_regmap1 [get_pins -hierarchical -filter {NAME =~ "g_endpoints[1].pcie0/clk0/clk0/inst/clock_primitive_inst/MMCME5_inst/CLKOUT0"}]
create_generated_clock -name pcie_userclk1 [get_pins -hierarchical -filter {NAME =~ "g_endpoints[1].pcie0/ep0/g_NoSim.g_versal.pcie_ep_versal0/versal_cips_block_i/versal_cips_0/U0/DPLL_PCIE0_inst/CLKOUT0"}]

#set_false_path -from [get_clocks DDR4_DIMM1_CLK_P] -to [get_clocks pcie_userclk0]


#Some timing constraints, can later move to separate timing constraints file
set_max_delay -datapath_only -from [get_clocks clk_out25*] -to [get_clocks *] 40.000
set_max_delay -datapath_only -from [get_clocks *] -to [get_clocks clk_out25*] 40.000


# These constraints are currently missing in the IP and should be integrated into the IP. 
# These are needed for write_device_image to work properly.
#set_property HD.TANDEM 1 [get_cells design_1_i/versal_cips_0/inst/IBUFDS_GTE5_inst]
set_property -quiet HD.TANDEM 1 [get_cells -hierarchical -filter {PRIMITIVE_TYPE == I/O.INPUT_BUFFER.IBUFDS_GTE5}]




# set clock uncertainty for PL paths
set_clock_uncertainty -hold 0.050 -from [get_clocks -of_objects [get_pins g_endpoints[0].pcie0/ep0/g_NoSim.g_versal.pcie_ep_versal0/versal_cips_block_i/versal_cips_0/inst/DPLL_PCIE0_inst/CLKOUT0]] \
                                     -to [get_clocks -of_objects [get_pins g_endpoints[0].pcie0/ep0/g_NoSim.g_versal.pcie_ep_versal0/versal_cips_block_i/versal_cips_0/inst/DPLL_PCIE0_inst/CLKOUT0]]

# set clock uncertainty for PL paths
set_clock_uncertainty -hold 0.050 -from [get_clocks -of_objects [get_pins g_endpoints[1].pcie0/ep0/g_NoSim.g_versal.pcie_ep_versal0/versal_cips_block_i/versal_cips_0/inst/DPLL_PCIE0_inst/CLKOUT0]] \
                                     -to [get_clocks -of_objects [get_pins g_endpoints[1].pcie0/ep0/g_NoSim.g_versal.pcie_ep_versal0/versal_cips_block_i/versal_cips_0/inst/DPLL_PCIE0_inst/CLKOUT0]]

create_generated_clock -name clk40_clk_wiz_40_0 [get_pins clk0/clk0/inst/clock_primitive_inst/MMCME5_inst/CLKOUT0]
create_generated_clock -name clk160_clk_wiz_40_0 [get_pins clk0/clk0/inst/clock_primitive_inst/MMCME5_inst/CLKOUT2]

###############################################################################
# User Configuration
# Link Width   - x8
# Link Speed   - gen3
# Family       - virtex7
# Part         - xc7vx690t
# Package      - ffg1761
# Speed grade  - -2
# PCIe Block   - X0Y1
###############################################################################
#
#########################################################################################################################
# User Constraints
#########################################################################################################################

set_property BITSTREAM.CONFIG.BPI_SYNC_MODE Type1 [current_design]
set_property BITSTREAM.CONFIG.EXTMASTERCCLK_EN div-1 [current_design]
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]

###############################################################################
# User Time Names / User Time Groups / Time Specs
###############################################################################

###############################################################################
# User Physical Constraints
###############################################################################

#! file TEST.XDC
#! net constraints for TEST design
### Main Felix clocks
set_property IOSTANDARD LVDS [get_ports app_clk_in_p]
#100 MHz Refclk
#Enable S1.4 to enable oscillator)
set_property PACKAGE_PIN AV38 [get_ports app_clk_in_n]
#100 MHz FMC Clk (fanout to both L24 and E35, Enable S1.6 to enable oscillator)
#set_property PACKAGE_PIN L24 [get_ports app_clk_in_n]
#set_property PACKAGE_PIN E35 [get_ports app_clk_in_n]
set_property IOSTANDARD LVDS [get_ports app_clk_in_n]
#create_clock -period 5.000 -name clk200 [get_ports clk_200_in_p]

## FPGA configuration clock
set_property IOSTANDARD LVCMOS18 [get_ports emcclk]
set_property PACKAGE_PIN AP37 [get_ports emcclk]

### XADC GPIO
set_property IOSTANDARD LVCMOS18 [get_ports emcclk_out]
set_property PACKAGE_PIN J42 [get_ports emcclk_out]

# TTC Clock Jitter Cleaned
# -- FMC_LA[17]_CC_N
set_property IOSTANDARD LVDS [get_ports CLK_TTC_P]
set_property PACKAGE_PIN J26 [get_ports CLK_TTC_N]
set_property IOSTANDARD LVDS [get_ports CLK_TTC_N]

# TTC Data
# -- FMC_LA[20]_N
set_property IOSTANDARD LVDS [get_ports DATA_TTC_P]
set_property PACKAGE_PIN G29 [get_ports DATA_TTC_N]
set_property IOSTANDARD LVDS [get_ports DATA_TTC_N]
#  ADN2814 LOL, LOS
# -- FMC_LA[09]_P
set_property IOSTANDARD LVCMOS18 [get_ports LOL_ADN]
set_property PACKAGE_PIN C38 [get_ports LOL_ADN]
# -- FMC_LA[09]_N
set_property IOSTANDARD LVCMOS18 [get_ports LOS_ADN]
set_property PACKAGE_PIN C39 [get_ports LOS_ADN]
# BUSY Out LEMO connector on TTCfx v3 board
set_property PACKAGE_PIN G32 [get_ports BUSY_OUT]
set_property IOSTANDARD LVCMOS18 [get_ports BUSY_OUT]

## TTCfx clocks
# -- FMC_LA[1]_CC_N
set_property IOSTANDARD LVDS [get_ports clk_ttcfx_ref_out_p]
set_property PACKAGE_PIN D36 [get_ports clk_ttcfx_ref_out_n]
set_property IOSTANDARD LVDS [get_ports clk_ttcfx_ref_out_n]
# -- FMC_LA[18]_CC_P
# -- FMC_LA[18]_CC_N
set_property IOSTANDARD LVDS  [get_ports clk_ttcfx_ref2_out_p]
set_property PACKAGE_PIN K25  [get_ports clk_ttcfx_ref2_out_n]
set_property IOSTANDARD LVDS  [get_ports clk_ttcfx_ref2_out_n]

# -- FMC_LA[0]_CC_P
# -- FMC_LA[0]_CC_N
#IOSTANDARD LVCMOS18 will be ignored when the pins are actually used as GTREFCLK
#AB: clashing with felix_gbt_cxp_copper_HTG710 !!
#set_property PACKAGE_PIN C35 [get_ports Q2_CLK0_GTREFCLK_PAD_P_IN]
#set_property IOSTANDARD LVCMOS18 [get_ports Q2_CLK0_GTREFCLK_PAD_P_IN]
#set_property PACKAGE_PIN C36 [get_ports Q2_CLK0_GTREFCLK_PAD_N_IN]
#set_property IOSTANDARD LVCMOS18 [get_ports Q2_CLK0_GTREFCLK_PAD_N_IN]
# -- FMC_HA[17]_CC_P
# -- FMC_HA[17]_CC_N
#AB: clashing with felix_gbt_cxp_copper_HTG710 !!
#set_property PACKAGE_PIN L31 [get_ports Q8_CLK0_GTREFCLK_PAD_P_IN]
#set_property IOSTANDARD LVCMOS18 [get_ports Q8_CLK0_GTREFCLK_PAD_P_IN]
#set_property PACKAGE_PIN K32 [get_ports Q8_CLK0_GTREFCLK_PAD_N_IN]
#set_property IOSTANDARD LVCMOS18 [get_ports Q8_CLK0_GTREFCLK_PAD_N_IN]
# -- FMC_HB[4]_P
# -- FMC_HB[4]_N
#set_property PACKAGE_PIN AD36 [get_ports ttcfx_clk_ref1_p]
#set_property IOSTANDARD LVDS  [get_ports ttcfx_clk_ref1_p]
#set_property PACKAGE_PIN AD37 [get_ports ttcfx_clk_ref1_n]
#set_property IOSTANDARD LVDS  [get_ports ttcfx_clk_ref1_n]


### System Reset, User Reset, User Link Up, User Clk Heartbeat
set_property PACKAGE_PIN F42 [get_ports {leds[0]}]
set_property PACKAGE_PIN E42 [get_ports {leds[1]}]
set_property PACKAGE_PIN D42 [get_ports {leds[2]}]
set_property PACKAGE_PIN D41 [get_ports {leds[3]}]
set_property PACKAGE_PIN B42 [get_ports {leds[4]}]
set_property PACKAGE_PIN B41 [get_ports {leds[5]}]
set_property PACKAGE_PIN A41 [get_ports {leds[6]}]
set_property PACKAGE_PIN A40 [get_ports {leds[7]}]
#
set_property IOSTANDARD LVCMOS18 [get_ports {leds[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[7]}]

#set_property PACKAGE_PIN A15 [get_ports FPGA_USER_PB]
#set_property IOSTANDARD LVCMOS15 [get_ports FPGA_USER_PB]
#
#set_property PACKAGE_PIN J42 [get_ports FPGA_USER_IO0]
#set_property IOSTANDARD LVCMOS18 [get_ports FPGA_USER_IO0]
#set_property PACKAGE_PIN K42 [get_ports FPGA_USER_IO1]
#set_property IOSTANDARD LVCMOS18 [get_ports FPGA_USER_IO1]
#set_property PACKAGE_PIN J41 [get_ports FPGA_USER_IO2]
#set_property IOSTANDARD LVCMOS18 [get_ports FPGA_USER_IO2]
#set_property PACKAGE_PIN L41 [get_ports FPGA_USER_IO3]
#set_property IOSTANDARD LVCMOS18 [get_ports FPGA_USER_IO3]
#set_property PACKAGE_PIN G42 [get_ports FPGA_USER_IO4]
#set_property IOSTANDARD LVCMOS18 [get_ports FPGA_USER_IO4]
#set_property PACKAGE_PIN L42 [get_ports FPGA_USER_IO5]
#set_property IOSTANDARD LVCMOS18 [get_ports FPGA_USER_IO5]
#set_property PACKAGE_PIN G41 [get_ports FPGA_USER_IO6]
#set_property IOSTANDARD LVCMOS18 [get_ports FPGA_USER_IO6]
#set_property PACKAGE_PIN M41 [get_ports FPGA_USER_IO7]
#set_property IOSTANDARD LVCMOS18 [get_ports FPGA_USER_IO7]

### Housekeeping signals
## TTCfx SPI interface
# -- CDCE_REF_SEL  -> FMC_LA[14]_P
# -- CDCE_PD       -> FMC_LA[6]_P
# -- CDCE_SYNC     -> FMC_LA[14]_N
# -- CDCE_SPI_LE   -> FMC_LA[9]_P
# -- CDCE_SPI_CLK  -> FMC_LA[9]_N
# -- CDCE_SPI_MOSI -> FMC_LA[5]_N
# -- CDCE_SPI_MISO -> FMC_LA[5]_P
# -- CDCE_PLL_LOCK -> FMC_LA[6]_N

#set_property PACKAGE_PIN B34 [get_ports CDCE_REF_SEL]
#set_property PACKAGE_PIN H38 [get_ports CDCE_PD]
#set_property PACKAGE_PIN A34 [get_ports CDCE_SYNC]
#set_property PACKAGE_PIN C38 [get_ports CDCE_SPI_LE]
#set_property PACKAGE_PIN C39 [get_ports CDCE_SPI_CLK]
#set_property PACKAGE_PIN F37 [get_ports CDCE_SPI_MOSI]
#set_property PACKAGE_PIN F36 [get_ports CDCE_SPI_MISO]
#set_property PACKAGE_PIN G38 [get_ports CDCE_PLL_LOCK]
#
#set_property IOSTANDARD LVCMOS18 [get_ports CDCE_REF_SEL]
#set_property IOSTANDARD LVCMOS18 [get_ports CDCE_PD]
#set_property IOSTANDARD LVCMOS18 [get_ports CDCE_SYNC]
#set_property IOSTANDARD LVCMOS18 [get_ports CDCE_SPI_LE]
#set_property IOSTANDARD LVCMOS18 [get_ports CDCE_SPI_CLK]
#set_property IOSTANDARD LVCMOS18 [get_ports CDCE_SPI_MOSI]
#set_property IOSTANDARD LVCMOS18 [get_ports CDCE_SPI_MISO]
#set_property IOSTANDARD LVCMOS18 [get_ports CDCE_PLL_LOCK]

## TTCfx Si5345 interface
# -- INSEL 1 -> FMC_LA[27]_N
# -- INSEL 0 -> FMC_LA[27]_P
# -- A 1     -> FMC_LA[13]_N
# -- A 0     -> FMC_LA[13]_P
# -- OE      -> FMC_LA[23]_P
# -- RSTN    -> FMC_LA[23]_N
# -- SEL     -> FMC_LA[26]_P
# -- nLOL    -> FMC_LA[6]_N

set_property PACKAGE_PIN J28 [get_ports {SI5345_INSEL[1]}]
set_property PACKAGE_PIN K28 [get_ports {SI5345_INSEL[0]}]
set_property PACKAGE_PIN A36 [get_ports {SI5345_A[1]}]
set_property PACKAGE_PIN A35 [get_ports {SI5345_A[0]}]
set_property PACKAGE_PIN H28 [get_ports SI5345_OE]
set_property PACKAGE_PIN H29 [get_ports SI5345_RSTN]
set_property PACKAGE_PIN G26 [get_ports SI5345_SEL]
set_property PACKAGE_PIN G38 [get_ports SI5345_nLOL]

set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_INSEL[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_INSEL[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_A[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_A[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports SI5345_OE]
set_property IOSTANDARD LVCMOS18 [get_ports SI5345_RSTN]
set_property IOSTANDARD LVCMOS18 [get_ports SI5345_SEL]
set_property IOSTANDARD LVCMOS18 [get_ports SI5345_nLOL]

#40 MHz clock output towards Si5345 ClkIn1
set_property PACKAGE_PIN K24 [get_ports clk_ttcfx_ref_out_p]
set_property PACKAGE_PIN K25 [get_ports clk_ttcfx_ref_out_n]

set_property IOSTANDARD LVDS [get_ports clk_ttcfx_ref_out_p]
set_property IOSTANDARD LVDS [get_ports clk_ttcfx_ref_out_n]



# DEBUG PORT: TTC, GBT, Clock monitor signals

#set_property PACKAGE_PIN AW40 [get_ports TTC_L1accept]
#set_property IOSTANDARD LVCMOS18 [get_ports TTC_L1accept]
## JTA : set slew rate to FAST to allow us to see 160MHz-speed signals
## JTA : also set drive current to 16mA to allow us to drive a coax cable better
#set_property SLEW FAST [get_ports TTC_L1accept]
#set_property DRIVE 16 [get_ports TTC_L1accept]

# SMA X3 on the HTG board CLK_SMA_MRCC_P
set_property PACKAGE_PIN AV40 [get_ports SmaOut_x3]
set_property IOSTANDARD LVCMOS18 [get_ports SmaOut_x3]
# JTA : set slew rate to FAST to allow us to see 160MHz-speed signals
# JTA : also set drive current to 16mA to allow us to drive a coax cable better
set_property SLEW FAST [get_ports SmaOut_x3]
set_property DRIVE 16 [get_ports SmaOut_x3]
# SMA X4 on the HTG board CLK_SMA_MRCC_N
set_property PACKAGE_PIN AW40 [get_ports SmaOut_x4]
set_property IOSTANDARD LVCMOS18 [get_ports SmaOut_x4]
# JTA : set slew rate to FAST to allow us to see 160MHz-speed signals
# JTA : also set drive current to 16mA to allow us to drive a coax cable better
set_property SLEW FAST [get_ports SmaOut_x4]
set_property DRIVE 16 [get_ports SmaOut_x4]

### I2C master for overall configuration
## see schematic page 7
##         -- PCA9548
##           -- CH0 : OSC RAM
##           -- CH1 : OSC REFCLK
##           -- CH2 : OSC CXP1
##           -- CH3 : OSC CXP2
##           -- CH4 : FMC
##           -- CH5 : CXP1
##           -- CH6 : CXP2
##           -- CH7 : DDR3
set_property PACKAGE_PIN BA37 [get_ports SDA]
set_property PACKAGE_PIN AY42 [get_ports SCL]
set_property PACKAGE_PIN BB37 [get_ports i2cmux_rst]

set_property IOSTANDARD LVCMOS18 [get_ports SDA]
set_property IOSTANDARD LVCMOS18 [get_ports SCL]
set_property IOSTANDARD LVCMOS18 [get_ports i2cmux_rst]


#########################################################################################################################
# End User Constraints
#########################################################################################################################
#
#
#
#########################################################################################################################
# PCIE Core Constraints
#########################################################################################################################

#
# SYS reset (input) signal.  The sys_reset_n signal should be
# obtained from the PCI Express interface if possible.  For
# slot based form factors, a system reset signal is usually
# present on the connector.  For cable based form factors, a
# system reset signal may not be available.  In this case, the
# system reset signal must be generated locally by some form of
# supervisory circuit.  You may change the IOSTANDARD and LOC
# to suit your requirements and VCCO voltage banking rules.
# Some 7 series devices do not have 3.3 V I/Os available.
# Therefore the appropriate level shift is required to operate
# with these devices that contain only 1.8 V banks.
#

set_property PACKAGE_PIN AN39 [get_ports sys_reset_n]
set_property IOSTANDARD LVCMOS18 [get_ports sys_reset_n]
set_property PULLUP true [get_ports sys_reset_n]

#
#
# SYS clock 100 MHz (input) signal. The sys_clk_p and sys_clk_n
# signals are the PCI Express reference clock. Virtex-7 GT
# Transceiver architecture requires the use of a dedicated clock
# resources (FPGA input pins) associated with each GT Transceiver.
# To use these pins an IBUFDS primitive (refclk_ibuf) is
# instantiated in user's design.
# Please refer to the Virtex-7 GT Transceiver User Guide
# (UG) for guidelines regarding clock resource selection.
#
set_property LOC IBUFDS_GTE2_X1Y10 [get_cells pcie0/ep0/g_NoSim.g_virtex7.refclk_buff]

###############################################################################
# Timing Constraints
###############################################################################
create_clock -period 6.25 -name ts_clk_adn_160 [get_pins u2/ibuf_ttc_clk/O]

create_clock -period 10.000 -name sys_clk [get_pins pcie0/ep0/g_NoSim.g_virtex7.refclk_buff/O]

create_clock -period 25.000 -name ts_clk_ttc_40 [get_nets clk_ttc_40]
#create_clock -period 6.250 -name ts_clk_adn_160 [get_nets clk_adn_160]
#create_clock -period 6.25   -name ts_ttcfx_clk_ref0 [get_nets ttcfx_clk_ref0]

create_generated_clock -name clk_125mhz_x0y1 [get_pins pcie0/ep0/g_NoSim.g_virtex7.pipe_clock0/mmcm0/CLKOUT0]
create_generated_clock -name clk_250mhz_x0y1 [get_pins pcie0/ep0/g_NoSim.g_virtex7.pipe_clock0/mmcm0/CLKOUT1]
create_generated_clock -name userclk1 [get_pins pcie0/ep0/g_NoSim.g_virtex7.pipe_clock0/mmcm0/CLKOUT2]
create_generated_clock -name userclk2 [get_pins pcie0/ep0/g_NoSim.g_virtex7.pipe_clock0/mmcm0/CLKOUT3]

create_generated_clock -name clk_125mhz_mux_x0y1 -source [get_pins pcie0/ep0/g_NoSim.g_virtex7.pipe_clock0/g0.pclk_i1/I0] -divide_by 1 [get_pins pcie0/ep0/g_NoSim.g_virtex7.g_NoSim.pipe_clock0/g0.pclk_i1/O]
create_generated_clock -name clk_250mhz_mux_x0y1 -source [get_pins pcie0/ep0/g_NoSim.g_virtex7.pipe_clock0/g0.pclk_i1/I1] -divide_by 1 -add -master_clock clk_250mhz_x0y1 [get_pins pcie0/ep0/g_NoSim.g_virtex7.pipe_clock0/g0.pclk_i1/O]
set_clock_groups -name pcieclkmux -physically_exclusive -group clk_125mhz_mux_x0y1 -group clk_250mhz_mux_x0y1

set_property BITSTREAM.CONFIG.OVERTEMPPOWERDOWN ENABLE [current_design]

set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets clk0/g1.clk0/inst/clk_100_in_clk_wiz_100_0]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets clk0/clk0/inst/clk320_clk_wiz_40_0]

###############################################################################
# These signals are only used for the VC709 board, we use some dummy ports for the HTG710
###############################################################################
set_property package_pin F41 [get_ports si5324_resetn]
set_property iostandard lvcmos18 [get_ports si5324_resetn]
set_property PACKAGE_PIN L39 [get_ports clk_adn_160_out_p]
set_property PACKAGE_PIN L40 [get_ports clk_adn_160_out_n]
set_property IOSTANDARD LVDS [get_ports clk_adn_160_out_p]

set_property IOSTANDARD LVDS [get_ports clk_adn_160_out_n]
###############################################################################
# End
###############################################################################
#allow usage of GTGREFCLK

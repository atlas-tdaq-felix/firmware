###############################################################################
# User Configuration
# Link Width   - x8
# Link Speed   - gen3
# Family       - virtex7
# Part         - xc7vx690t
# Package      - ffg1761
# Speed grade  - -2
# PCIe Block   - X0Y1
###############################################################################
#
###############################################################################
# User Constraints
###############################################################################

set_property PACKAGE_PIN AA40 [get_ports opto_los[0]]
set_property IOSTANDARD LVCMOS18 [get_ports opto_los[0]]

set_property PACKAGE_PIN Y39 [get_ports opto_los[1]]
set_property IOSTANDARD LVCMOS18 [get_ports opto_los[1]]

set_property PACKAGE_PIN AD38 [get_ports opto_los[2]]
set_property IOSTANDARD LVCMOS18 [get_ports opto_los[2]]

set_property PACKAGE_PIN AD40 [get_ports opto_los[3]]
set_property IOSTANDARD LVCMOS18 [get_ports opto_los[3]]

set_property PACKAGE_PIN Y42 [get_ports opto_inhibit[0]]
set_property IOSTANDARD LVCMOS18 [get_ports opto_inhibit[0]]

set_property PACKAGE_PIN AB41 [get_ports opto_inhibit[1]]
set_property IOSTANDARD LVCMOS18 [get_ports opto_inhibit[1]]

set_property PACKAGE_PIN AC38 [get_ports opto_inhibit[2]]
set_property IOSTANDARD LVCMOS18 [get_ports opto_inhibit[2]]

set_property PACKAGE_PIN AC40 [get_ports opto_inhibit[3]]
set_property IOSTANDARD LVCMOS18 [get_ports opto_inhibit[3]]

#NET RX_CLK_TP LOC = AM31 | IOSTANDARD = "LVCMOS18";

#pull out CXP ports from the VC709 design
#set_property package_pin "" [get_ports [list  CXP2_RST_L]]
#set_property package_pin "" [get_ports [list  CXP1_RST_L]]

###############################################################################
# User Time Names / User Time Groups / Time Specs
###############################################################################

####################### GT reference clock constraints #######################

#Direct routing from ADN chip towards Si5324 jitter cleaner
set_property package_pin AT36 [get_ports si5324_resetn]
set_property iostandard lvcmos18 [get_ports si5324_resetn]
set_property PACKAGE_PIN AW32 [get_ports clk_adn_160_out_p]
set_property PACKAGE_PIN AW33 [get_ports clk_adn_160_out_n]
set_property IOSTANDARD LVDS [get_ports clk_adn_160_out_p]
set_property IOSTANDARD LVDS [get_ports clk_adn_160_out_n]
# VC709 clock from Si5324
# FELIX: this is the quad clocking around Q2 (plus above and below)
set_property PACKAGE_PIN AH7 [get_ports GTREFCLK_Si5324_N_IN]
set_property PACKAGE_PIN AH8 [get_ports GTREFCLK_Si5324_P_IN]
# VC709: clock from SMA (J25/J26)
# FELIX: this is the quad clocking around Q2 (plus above and below)
set_property PACKAGE_PIN AK7 [get_ports GTREFCLK_N_IN[0]]
set_property PACKAGE_PIN AK8 [get_ports GTREFCLK_P_IN[0]]

#TTCfx V3 from REF1 (FMC_HPC_LA00_P/N) [AB: can this work??]
#set_property PACKAGE_PIN K40 [get_ports Q2_CLK0_GTREFCLK_PAD_N_IN]
#set_property PACKAGE_PIN K39 [get_ports Q2_CLK0_GTREFCLK_PAD_P_IN]


###############################################################################
# GBT SFP Physical Constraints
###############################################################################
# SFP 1-4
set_property PACKAGE_PIN AN5 [get_ports {RX_N[1]}]
set_property PACKAGE_PIN AM7 [get_ports {RX_N[0]}]
set_property PACKAGE_PIN AL5 [get_ports {RX_N[2]}]
set_property PACKAGE_PIN AJ5 [get_ports {RX_N[3]}]



###############################################################################
# Others
###############################################################################

# force Vivado to ignore usage of GTGREFCLK
#set_property SEVERITY {Warning} [get_drc_checks REQP-44]
#set_property SEVERITY {Warning} [get_drc_checks REQP-46]
# force vivado to ignore unplaced pins
#set_property SEVERITY {Warning} [get_drc_checks IOSTDTYPE-1]

###############################################################################
# End
###############################################################################

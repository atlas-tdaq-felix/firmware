#file: felix_gbt_minipod_BNL712_2x4ch_MROD.xdc
##GBT Transceiver pins
#bank 128
#set_property PACKAGE_PIN AC43 [get_ports {RX_P[0]}]
#set_property PACKAGE_PIN AF41 [get_ports {RX_P[3]}]
#set_property PACKAGE_PIN AE43 [get_ports {RX_P[2]}]
#set_property PACKAGE_PIN AG43 [get_ports {RX_P[1]}]
#bank 127 RX
set_property PACKAGE_PIN AH41 [get_ports {RX_P[0]}]
set_property PACKAGE_PIN AJ43 [get_ports {RX_P[1]}]
set_property PACKAGE_PIN AN43 [get_ports {RX_P[2]}]
set_property PACKAGE_PIN AL43 [get_ports {RX_P[3]}]

#bank 127 TX
set_property PACKAGE_PIN AJ39 [get_ports {TX_P[0]}]
set_property PACKAGE_PIN AK41 [get_ports {TX_P[1]}]
set_property PACKAGE_PIN AP41 [get_ports {TX_P[2]}]
set_property PACKAGE_PIN AM41 [get_ports {TX_P[3]}]

#bank 126
#set_property PACKAGE_PIN AU43 [get_ports {RX_P[8]}]
#set_property PACKAGE_PIN AR43 [get_ports {RX_P[9]}]
#set_property PACKAGE_PIN BA43 [get_ports {RX_P[10]}]
#set_property PACKAGE_PIN AW43 [get_ports {RX_P[11]}]
#bank 133
#set_property PACKAGE_PIN C43  [get_ports {RX_P[12]}]
#set_property PACKAGE_PIN E43  [get_ports {RX_P[13]}]
#set_property PACKAGE_PIN G43  [get_ports {RX_P[14]}]
#set_property PACKAGE_PIN J43  [get_ports {RX_P[15]}]
#bank 132 RX
set_property PACKAGE_PIN L43  [get_ports {RX_P[4]}]
set_property PACKAGE_PIN N43  [get_ports {RX_P[5]}]
set_property PACKAGE_PIN R43  [get_ports {RX_P[6]}]
set_property PACKAGE_PIN U43  [get_ports {RX_P[7]}]

#bank 132 TX
set_property PACKAGE_PIN K41  [get_ports {TX_P[4]}]
set_property PACKAGE_PIN M41  [get_ports {TX_P[5]}]
set_property PACKAGE_PIN P41  [get_ports {TX_P[6]}]
set_property PACKAGE_PIN T41  [get_ports {TX_P[7]}]

#bank 131
#set_property PACKAGE_PIN W43  [get_ports {RX_P[20]}]
#set_property PACKAGE_PIN V41  [get_ports {RX_P[21]}]
#set_property PACKAGE_PIN AA43 [get_ports {RX_P[23]}]
#set_property PACKAGE_PIN AB41 [get_ports {RX_P[22]}]

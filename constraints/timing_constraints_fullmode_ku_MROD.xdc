#file: timing_constraints_fullmode_ku_MROD.xdc
#create 40 MHz TTC clock
#create_clock -period 24.95 -name clk_ttc_40 [get_pins */ttc_dec/from_cdr_to_AandB/mmcm0/inst/clkout1_buf/O]

create_clock -name clk_ttc_40 -period 24.95 [get_pins u31/u2/ttc_dec/from_cdr_to_AandB/clock_iter/O]
#create_clock -period 6.250 -name clk_adn_160 [get_nets u31/u2/ibuf_ttc_clk/O]
##create_clock -period 6.250 -name clk_adn_160 [get_nets clk_adn_160]
##create_clock -period 6.25 -name clk_adn_160 [get_ports CLK_TTC_P]
#set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets clk_adn_160_BUFG]
#set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks clk_ttc_40] 6.25
#6.25 
##set_false_path -from [get_clocks clk_adn_160] -to [get_clocks clk_ttc_40]
#Clock domain crossings in TTC decoder

#6.250
#set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks clk40_clk_wiz_40_0*] 24.950

##set_max_delay -datapath_only -from [get_clocks clk_out1_ttc_phase_clock_wizard] -to [get_clocks clk_adn_160] 6.250
###set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks {GT_TX_WORD_CLK[0]}] 6.250

#set_false_path -from [get_clocks clk_adn_160] -to [get_clocks clk160_clk_wiz_40_0]

##set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
##set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks clk40_clk_wiz_40_0*] 24.950

#We need this line when we move to the second SRL
#create_clock -period 4.158 -name GTHREFCLK_1 -waveform {0.000 2.079} [get_ports {Q2_CLK0_GTREFCLK_PAD_P_IN}]
#create_clock -period 4.158 -name GTHREFCLK_2 -waveform {0.000 2.079} [get_ports {Q8_CLK0_GTREFCLK_PAD_P_IN}]
create_clock -period 10.000 -name sys_clk0_p -waveform {0.000 5.000} [get_ports {sys_clk0_p}]
create_clock -period 10.000 -name sys_clk1_p -waveform {0.000 5.000} [get_ports {sys_clk1_p}]

##create_clock -name g1.u2/GT_TX_WORD_CLK[0] -period 4.167 [get_pins g1.u2/clk_generate[0].GTTXOUTCLK_BUFG/O]

create_clock -name emcclk -period 20.000 [get_ports emcclk]






#set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks {g1.u2/GT_TX_WORD_CLK[*]}] 24.95

#Clock domain crossings within Central Router
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk80_clk_wiz_40_0*]  24.95
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk160_clk_wiz_40_0*] 24.95
set_max_delay -datapath_only -from [get_clocks clk80_clk_wiz_40_0*] -to [get_clocks clk160_clk_wiz_40_0*] 12.470
set_max_delay -datapath_only -from [get_clocks clk80_clk_wiz_40_0*] -to [get_clocks clk40_clk_wiz_40_0*]  12.470
set_max_delay -datapath_only -from [get_clocks clk80_clk_wiz_40_0*] -to [get_clocks clk160_clk_wiz_40_0*] 12.470
set_max_delay -datapath_only -from [get_clocks clk160_clk_wiz_40_0*] -to [get_clocks clk80_clk_wiz_40_0*] 12.470
#set_max_delay -datapath_only -from [get_clocks clk160_clk_wiz_40_0*] -to [get_pins -hierarchical -filter {NAME=~ "*GBTdmUp/GBTch_inFIFO/dout_reg[*]/D"}] 6.25
set_max_delay -datapath_only -from [get_clocks clk160_clk_wiz_40_0*] -to [get_clocks clk40_clk_wiz_40_0*] 24.95
set_max_delay -datapath_only -from [get_clocks clk160_clk_wiz_40_0*] -to [get_clocks clk40_clk_wiz_40_0*] 24.95
set_max_delay -datapath_only -from [get_clocks clk250_clk_wiz_250*] -to [get_clocks clk40_clk_wiz_40_0*] 24.95
set_max_delay -datapath_only -from [get_clocks clk250_clk_wiz_250*] -to [get_clocks clk240_clk_wiz_40_0*] 4.0
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk250_clk_wiz_250*] 24.95
set_max_delay -datapath_only -from [get_clocks txoutclk_out*] -to [get_clocks clk250_clk_wiz_250*] 4.170
set_max_delay -datapath_only -from [get_clocks clk250_clk_wiz_250*] -to [get_clocks txoutclk_out*] 4.170

#Clock domain crossings in GBT
#Clock domain crossings between Wupper and Central Router
set_max_delay -datapath_only -from [get_clocks clk250_clk_wiz_250*] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks *rd_clk] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
#set_max_delay -datapath_only -from [get_clocks *wr_clk] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks clk250_clk_wiz_250*] -to [get_clocks clk80_clk_wiz_40_0*] 12.470
set_max_delay -datapath_only -from [get_clocks *rd_clk] -to [get_clocks clk80_clk_wiz_40_0*] 12.470
#set_max_delay -datapath_only -from [get_clocks *wr_clk] -to [get_clocks clk80_clk_wiz_40_0*] 12.470
set_max_delay -datapath_only -from [get_clocks clk250_clk_wiz_250*] -to [get_clocks clk160_clk_wiz_40_0*] 6.25
set_max_delay -datapath_only -from [get_clocks *rd_clk] -to [get_clocks clk160_clk_wiz_40_0*] 6.25
#set_max_delay -datapath_only -from [get_clocks *wr_clk] -to [get_clocks clk160_clk_wiz_40_0*] 6.25
set_max_delay -datapath_only -from [get_clocks clk250_clk_wiz_250*] -to [get_clocks clk240_clk_wiz_40_*] 4.17
set_max_delay -datapath_only -from [get_clocks *rd_clk] -to [get_clocks clk240_clk_wiz_40_*] 4.17
#set_max_delay -datapath_only -from [get_clocks *wr_clk] -to [get_clocks clk240_clk_wiz_40_*] 4.17
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks  clk250_clk_wiz_250*] 24.95
set_max_delay -datapath_only -from [get_clocks clk80_clk_wiz_40_0*] -to [get_clocks  clk250_clk_wiz_250*] 12.470
set_max_delay -datapath_only -from [get_clocks clk160_clk_wiz_40_0*] -to [get_clocks clk250_clk_wiz_250*] 6.25
set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_*] -to [get_clocks  clk250_clk_wiz_250*] 4.17

set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks  *rd_clk] 24.95
set_max_delay -datapath_only -from [get_clocks clk80_clk_wiz_40_0*] -to [get_clocks  *rd_clk] 12.470
set_max_delay -datapath_only -from [get_clocks clk160_clk_wiz_40_0*] -to [get_clocks *rd_clk] 6.25
set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_*] -to [get_clocks  *rd_clk] 4.17

#set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks  *wr_clk] 24.95
#set_max_delay -datapath_only -from [get_clocks clk80_clk_wiz_40_0*] -to [get_clocks  *wr_clk] 12.470
#set_max_delay -datapath_only -from [get_clocks clk160_clk_wiz_40_0*] -to [get_clocks *wr_clk] 6.25
#set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_*] -to [get_clocks  *wr_clk] 4.17

set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40*] -to [get_clocks {rxoutclk*}] 24.95
set_max_delay -datapath_only -from [get_clocks {rxoutclk*}] -to [get_clocks clk40_clk_wiz_40*] 24.95
set_max_delay -datapath_only -from [get_clocks {rxoutclk*}] -to [get_clocks clk250_clk_wiz_250*] 4.17
set_max_delay -datapath_only -from [get_clocks {rxoutclk*}] -to [get_clocks clk250_clk_wiz_250*] 4.17
set_max_delay -datapath_only -from [get_clocks {rxoutclk*}] -to [get_clocks clk40_clk_wiz_40_0*] 24.0
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks {rxoutclk*}] 24.0

set_max_delay -datapath_only -from [get_clocks {rxoutclk*}] -to [get_clocks {clk250_clk_wiz_250*}] 5.198
set_max_delay -datapath_only -from [get_clocks {clk250_clk_wiz_250*}] -to [get_clocks {rxoutclk*}] 5.198
set_max_delay -datapath_only -from [get_clocks {clk240_clk_wiz_40_0*}] -to [get_clocks {clk250_clk_wiz_250*}] 5.198
set_max_delay -datapath_only -from [get_clocks {clk250_clk_wiz_250*}] -to [get_clocks {clk240_clk_wiz_40_0*}] 5.198

#These paths are excluded with BUFG muxes, and should never interact
set_false_path -from [get_clocks clk240_clk_wiz_40*] -to [get_clocks {rxoutclk*}]
set_false_path -from [get_clocks {rxoutclk*}] -to [get_clocks clk240_clk_wiz_40*]

set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets u32/clk0/g_200M.clk0/inst/clk40]
#set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets u31/u2/ttc_dec/from_cdr_to_AandB/mmcm0/inst/clk_out1] 
#set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets u31/u2/ttc_dec/from_cdr_to_AandB/mmcm0/inst/clk_out1_ttc_phase_clock_wizard]
#set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets u31/u2/ttc_dec/from_cdr_to_AandB/mmcm0/inst/clk_out1_ttc_phase_clock_wizard]

#Register map to 400kHz I2C clock
set_max_delay -datapath_only -from [get_clocks clk250_clk_wiz_250*] -to [get_clocks clk400] 100
set_max_delay -datapath_only -from [get_clocks *rd_clk] -to [get_clocks clk400] 100
#set_max_delay -datapath_only -from [get_clocks *wr_clk] -to [get_clocks clk400] 100


#Register map to central router clocks 
set_max_delay -datapath_only -from [get_clocks clk250_clk_wiz_250*] -to [get_clocks clk80_clk_wiz_40_0*] 12.470

#Register map monitor
set_max_delay -datapath_only -from [get_clocks clk_out25_clk_wiz_regmap*] -to [get_clocks *] 40.000
set_max_delay -datapath_only -from [get_clocks *] -to [get_clocks clk_out25_clk_wiz_regmap*] 40.000

#Clock domain crossings between Central Router and GBT
set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_0*] -to [get_clocks clk40_clk_wiz_40_0*] 24
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks {txoutclk_out*}] 24.95
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk240_clk_wiz_40_0*] 24.95
set_max_delay -datapath_only -from [get_clocks {txoutclk_out*}  ] -to [get_clocks clk40_clk_wiz_40_0*] 24.95
#set_max_delay -datapath_only -from [get_clocks {g1.u2/GT_TX_WORD_CLK[*]}] -to [get_clocks clk40_clk_wiz_40_0*] 24.95
#set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks clk40_clk_wiz_40_0*] 24.95
#set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks {g1.u2/GT_TX_WORD_CLK[*]}] 24.95
#set_max_delay -from [get_pins u32/clk0/clk0/inst/mmcme3_adv_inst/CLKOUT0] -to [get_pins -hierarchical -filter {NAME =~ "*timedomaincrossing_C/TX_FRAMECLK_I*/D"}] 4.17
set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_*] -to [get_clocks clk40_clk_wiz_40_*] 24.95
set_max_delay -datapath_only -from [get_clocks clk80_clk_wiz_40_*] -to [get_clocks clk40_clk_wiz_40_*] 24.95
#set_max_delay -datapath_only -from [get_clocks clk320_clk_wiz_40_0*] -to [get_clocks clk240_clk_wiz_40_0*] 4.17

set_max_delay -from [get_pins -hierarchical -filter { NAME =~ "*/rst0/fifoFLUSH_reg*/C" }]  24.95


#Clock domain crossings in PCIe core

#Reset paths can be false
#set_false_path -reset_path -from [get_ports sys_reset_n]
##set_false_path -reset_path -from [get_pins -hierarchical -filter { NAME =~ "*reg_user_reset_reg/C" } ]
#set_false_path -reset_path -from [get_ports sys_reset_n]
##set_false_path -reset_path -from [get_pins u13/u5/dma0/u1/reset_global_soft_40_s_reg/C]
##set_false_path -reset_path -from [get_pins clk0/reset_out_reg/C]
#set_false_path -reset_path -from [get_pins -hierarchical -filter { NAME =~  "*/rst_soft*" }]
#set_false_path -from [get_pins -hierarchical -filter { NAME =~ "*register_map_control_s_reg[MMCM_MAIN][LCLK_FORCE]*C"}]

set_multicycle_path -setup -start 3 -from [ get_pins u23/u5/dma0/u0/fromHostFifo_din_reg[*]/C ] -to [ get_pins u23/fromHostFifo_din_pipe_reg[*]/D ]
set_multicycle_path -hold  -end   2 -from [ get_pins u23/u5/dma0/u0/fromHostFifo_din_reg[*]/C ] -to [ get_pins u23/fromHostFifo_din_pipe_reg[*]/D ]

set_multicycle_path -setup -start 3 -from [ get_pins u23/u5/dma0/u0/fromHostFifo_we_reg/C ] -to [ get_pins u23/fromHostFifo_wr_en_pipe_reg/D ]
set_multicycle_path -hold  -end   2 -from [ get_pins u23/u5/dma0/u0/fromHostFifo_we_reg/C ] -to [ get_pins u23/fromHostFifo_wr_en_pipe_reg/D ]

set_multicycle_path -setup -start 3 -from [ get_pins u23/u1/dataMUXn/data_out_rdy_s_reg/C ] -to [ get_pins u23/toHostFifo_wr_en_pipe_reg/D]
set_multicycle_path -hold  -end   2 -from [ get_pins u23/u1/dataMUXn/data_out_rdy_s_reg/C ] -to [ get_pins u23/toHostFifo_wr_en_pipe_reg/D]

set_multicycle_path -setup -start 3 -from [ get_pins u23/u1/dataMUXn/data_out_reg[*]/C ] -to [ get_pins u23/toHostFifo_din_pipe_reg[*]/D]
set_multicycle_path -hold  -end   2 -from [ get_pins u23/u1/dataMUXn/data_out_reg[*]/C ] -to [ get_pins u23/toHostFifo_din_pipe_reg[*]/D]

set_multicycle_path -setup -start 3 -from [ get_pins u13/u5/dma0/u0/fromHostFifo_din_reg[*]/C ] -to [ get_pins u13/fromHostFifo_din_pipe_reg[*]/D ]
set_multicycle_path -hold  -end   2 -from [ get_pins u13/u5/dma0/u0/fromHostFifo_din_reg[*]/C ] -to [ get_pins u13/fromHostFifo_din_pipe_reg[*]/D ]

set_multicycle_path -setup -start 3 -from [ get_pins u13/u5/dma0/u0/fromHostFifo_we_reg/C ] -to [ get_pins u13/fromHostFifo_wr_en_pipe_reg/D ]
set_multicycle_path -hold  -end   2 -from [ get_pins u13/u5/dma0/u0/fromHostFifo_we_reg/C ] -to [ get_pins u13/fromHostFifo_wr_en_pipe_reg/D ]

set_multicycle_path -setup -start 3 -from [ get_pins u13/u1/dataMUXn/data_out_rdy_s_reg/C ] -to [ get_pins u13/toHostFifo_wr_en_pipe_reg/D]
set_multicycle_path -hold  -end   2 -from [ get_pins u13/u1/dataMUXn/data_out_rdy_s_reg/C ] -to [ get_pins u13/toHostFifo_wr_en_pipe_reg/D]

set_multicycle_path -setup -start 3 -from [ get_pins u13/u1/dataMUXn/data_out_reg[*]/C ] -to [ get_pins u13/toHostFifo_din_pipe_reg[*]/D]
set_multicycle_path -hold  -end   2 -from [ get_pins u13/u1/dataMUXn/data_out_reg[*]/C ] -to [ get_pins u13/toHostFifo_din_pipe_reg[*]/D]

#Multicycle paths in the TxGearbox
#set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler21bit/feedbackRegister_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" } ] 2
#set_multicycle_path  -hold  -end   -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler21bit/feedbackRegister_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" } ] 1

#set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler16bit/feedbackRegister_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" } ] 2
#set_multicycle_path  -hold  -end   -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler16bit/feedbackRegister_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" } ] 1


########### Input / Output delays (some ports can be false path)
set_false_path -from [get_ports {flash_a_msb[*]}]
set_false_path -from [get_ports LMK_LD]
set_false_path -from [get_ports SCL]
set_false_path -from [get_ports SDA]
set_false_path -from [get_ports SI5345_nLOL]
set_false_path -from [get_ports {PORT_GOOD[*]}]
set_false_path -from [get_ports LOL_ADN]
set_false_path -from [get_ports LOS_ADN]
#set_false_path -reset_path -from [get_ports sys_reset_n]
set_false_path -to [get_ports {SI5345_A[*]}]
set_false_path -to [get_ports {SI5345_INSEL[*]}]
set_false_path -to [get_ports {flash_a[*]}]
set_false_path -to [get_ports {flash_a_msb[*]}]
set_false_path -to [get_ports I2C_nRESET]
set_false_path -to [get_ports SCL]
set_false_path -to [get_ports SDA]
set_false_path -to [get_ports SI5345_OE]
set_false_path -to [get_ports SI5345_SEL]
set_false_path -to [get_ports flash_SEL]
set_false_path -to [get_ports PCIE_PERSTn2]
set_false_path -to [get_ports PEX_PERSTn]
set_false_path -to [get_ports PEX_SCL]
set_false_path -to [get_ports PEX_SDA]
set_false_path -to [get_ports BUSY_OUT]

set_input_delay -clock [get_clocks emcclk] 0.0 [get_ports {flash_d[*]}]
set_output_delay -clock [get_clocks emcclk] 0.0 [get_ports {flash_d[*]}]
#set_input_delay -clock [get_clocks clk_adn_160] 0.0 [get_ports DATA_TTC_N]
#set_input_delay -clock [get_clocks clk_adn_160] 0.0 [get_ports DATA_TTC_P]
set_output_delay -clock [get_clocks emcclk] 0.0 [get_ports flash_ce]
set_output_delay -clock [get_clocks emcclk] 0.0 [get_ports flash_re]
set_output_delay -clock [get_clocks emcclk] 0.0 [get_ports flash_we]
set_output_delay -clock [get_clocks clk10_clk_wiz_200_0] 0.0 [get_ports LMK_DATA]
set_output_delay -clock [get_clocks clk10_clk_wiz_200_0] 0.0 [get_ports LMK_GOE]
set_output_delay -clock [get_clocks clk10_clk_wiz_200_0] 0.0 [get_ports LMK_LE]
set_output_delay -clock [get_clocks clk10_clk_wiz_200_0] 0.0 [get_ports LMK_SYNCn]




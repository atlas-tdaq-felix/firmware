set GTHREFCLK_PERIOD 4.158
set LMK_PERIOD 4.158
#create 40 MHz TTC clock. RL: changed to 25 to stop errors in the implementation. this clock is not used in FELIG anyway
create_clock -name clk_ttc_40 -period 25 [get_pins */ttc_dec/from_cdr_to_AandB/clock_iter/O]

create_clock -period 6.25 -name clk_adn_160 [get_ports CLK_TTC_P]

create_clock -period 10.000 -name sys_clk0_p -waveform {0.000 5.000} [get_ports {sys_clk_p[0]}]
create_clock -quiet -period 10.000 -name sys_clk1_p -waveform {0.000 5.000} [get_ports {sys_clk_p[1]}]

create_clock -name emcclk -period 20.000 [get_ports emcclk]

set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets emcclk_IBUF_inst/O]

#GTREFCLKS up to 6, use -quiet for the ones that don't exist due to limited number of channels.
create_clock -quiet -name GTHREFCLK_0 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[0]}]
create_clock -quiet -name GTHREFCLK_1 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[1]}]
create_clock -quiet -name GTHREFCLK_2 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[2]}]
create_clock -quiet -name GTHREFCLK_3 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[3]}]
create_clock -quiet -name GTHREFCLK_4 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[4]}]
create_clock -quiet -name GTHREFCLK_5 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[5]}]
create_clock -quiet -name GTHREFCLK_6 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[6]}]
create_clock -quiet -name GTHREFCLK_7 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[7]}]
create_clock -quiet -name GTHREFCLK_8 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[8]}]
create_clock -quiet -name GTHREFCLK_9 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[9]}]
create_clock -quiet -name GTHREFCLK_10 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[10]}]
create_clock -quiet -name GTHREFCLK_11 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[11]}]

create_clock -quiet -name LMK0_REFCLK -period $LMK_PERIOD [get_ports {LMK_P[0]}]
create_clock -quiet -name LMK1_REFCLK -period $LMK_PERIOD [get_ports {LMK_P[1]}]
create_clock -quiet -name LMK2_REFCLK -period $LMK_PERIOD [get_ports {LMK_P[2]}]
create_clock -quiet -name LMK3_REFCLK -period $LMK_PERIOD [get_ports {LMK_P[3]}]
create_clock -quiet -name LMK4_REFCLK -period $LMK_PERIOD [get_ports {LMK_P[4]}]
create_clock -quiet -name LMK5_REFCLK -period $LMK_PERIOD [get_ports {LMK_P[5]}]
create_clock -quiet -name LMK6_REFCLK -period $LMK_PERIOD [get_ports {LMK_P[6]}]
create_clock -quiet -name LMK7_REFCLK -period $LMK_PERIOD [get_ports {LMK_P[7]}]

create_clock -quiet -name i2c_clock_pex -period 2500 [get_pins hk0/g_711_712.pex_init0/data_clk_reg/Q]

#set_max_delay from timing_constraints.xdc

#Register map to 400kHz I2C clock
set_max_delay -quiet -datapath_only -from [get_clocks -regexp ^((?!clk400).)*$] -to [get_clocks clk400] 100.000
set_max_delay -quiet -datapath_only -from [get_clocks -regexp ^((?!i2c_clock_pex).)*$] -to [get_clocks i2c_clock_pex] 100.000

#Exceptions from and to the register map clock
set_max_delay -datapath_only -from [get_clocks -regexp ^((?!clk_out25_clk_wiz_regmap).)*$] -to [get_clocks clk_out25_clk_wiz_regmap*] 40
set_max_delay -datapath_only -from [get_clocks clk_out25_clk_wiz_regmap*] -to [get_clocks -regexp ^((?!clk_out25_clk_wiz_regmap).)*$] 40

#switchable output clock can switch at any time
set_max_delay -quiet -datapath_only -from [get_clocks clk_adn_160*] -to [get_clocks clk40_clk_wiz_40_0*] 6.238
set_max_delay -quiet -datapath_only -from [get_clocks clk_adn_160*] -to [get_clocks clk_ttc_40*] 6.238
set_max_delay -quiet -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk_ttc_40*] 24.95
set_max_delay -quiet -datapath_only -from [get_clocks clk_ttc_40*] -to [get_clocks clk40_clk_wiz_40_0*] 24.95

#from timing_constraints

#Multicycle paths in the RxGearbox
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" }] 4
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" }] 3
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" }] 4
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" }] 3
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" }] 4
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" }] 3
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" }] 4
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" }] 3

#TX side
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[0].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 2
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[0].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 1
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[1].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 2
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[1].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 1
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[2].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 2
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[2].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 1
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[0].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 3
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[0].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 2
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[1].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 3
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[1].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 2

set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" }] 2
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" }] 1
 
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" }] 2
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" }] 1
 
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 1
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 0
 
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler16bit/RX_EXTRA_DATA_WIDEBUS_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 1
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler16bit/RX_EXTRA_DATA_WIDEBUS_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 0
 
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixDescrambler/RX_HEADER_O_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 1
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixDescrambler/RX_HEADER_O_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 0
create_clock -quiet -period 6.25 -name clk_adn_160 [get_ports {CLK_TTC_P[0]}]

#create 40 MHz TTC clock
create_clock -quiet -name clk_ttc_40 -period 24.95 [get_pins */ttc_dec/from_cdr_to_AandB/clock_iter/O]

create_clock -quiet -period 10.000 -name sys_clk0_p -waveform {0.000 5.000} [get_ports {sys_clk_p[0]}]
create_clock -quiet -period 10.000 -name sys_clk1_p -waveform {0.000 5.000} [get_ports {sys_clk_p[1]}]

set_property -quiet CLOCK_DEDICATED_ROUTE FALSE [get_nets emcclk_IBUF[0]_inst/O]

#GTREFCLKS up to 6, use -quiet for theones that don't exist due to limited number of channels.
create_clock -quiet -name GTHREFCLK_0 -period 4.158 [get_ports {GTREFCLK_P_IN[0]}]
create_clock -quiet -name GTHREFCLK_1 -period 4.158 [get_ports {GTREFCLK_P_IN[1]}]
create_clock -quiet -name GTHREFCLK_2 -period 4.158 [get_ports {GTREFCLK_P_IN[2]}]
create_clock -quiet -name GTHREFCLK_3 -period 4.158 [get_ports {GTREFCLK_P_IN[3]}]
create_clock -quiet -name GTHREFCLK_4 -period 4.158 [get_ports {GTREFCLK_P_IN[4]}]
create_clock -quiet -name GTHREFCLK_5 -period 4.158 [get_ports {GTREFCLK_P_IN[5]}]
create_clock -quiet -period 3.125 -name LMK0_REFCLK [get_ports {LMK_P[0]}]
create_clock -quiet -period 3.125 -name LMK1_REFCLK [get_ports {LMK_P[1]}]
create_clock -quiet -period 3.125 -name LMK2_REFCLK [get_ports {LMK_P[2]}]
create_clock -quiet -period 3.125 -name LMK3_REFCLK [get_ports {LMK_P[3]}]
create_clock -quiet -period 3.125 -name LMK4_REFCLK [get_ports {LMK_P[4]}]
create_clock -quiet -period 3.125 -name LMK5_REFCLK [get_ports {LMK_P[5]}]
create_clock -quiet -period 3.125 -name LMK6_REFCLK [get_ports {LMK_P[6]}]
create_clock -quiet -period 3.125 -name LMK7_REFCLK [get_ports {LMK_P[7]}]

create_clock -quiet -name i2c_clock_pex -period 2500 [get_pins hk0/g_711_712.pex_init0/data_clk_reg/Q]

#create_generated_clock -name gbt_lti_clk40_0  -source [get_pins g_clk40_bufgmux[0 ].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[0 ].buf0/O]
#create_generated_clock -name gbt_lti_clk40_1  -source [get_pins g_clk40_bufgmux[1 ].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[1 ].buf0/O]
#create_generated_clock -name gbt_lti_clk40_2  -source [get_pins g_clk40_bufgmux[2 ].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[2 ].buf0/O]
#create_generated_clock -name gbt_lti_clk40_3  -source [get_pins g_clk40_bufgmux[3 ].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[3 ].buf0/O]
#create_generated_clock -name gbt_lti_clk40_4  -source [get_pins g_clk40_bufgmux[4 ].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[4 ].buf0/O]
#create_generated_clock -name gbt_lti_clk40_5  -source [get_pins g_clk40_bufgmux[5 ].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[5 ].buf0/O]
#create_generated_clock -name gbt_lti_clk40_6  -source [get_pins g_clk40_bufgmux[6 ].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[6 ].buf0/O]
#create_generated_clock -name gbt_lti_clk40_7  -source [get_pins g_clk40_bufgmux[7 ].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[7 ].buf0/O]
#create_generated_clock -name gbt_lti_clk40_8  -source [get_pins g_clk40_bufgmux[8 ].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[8 ].buf0/O]
#create_generated_clock -name gbt_lti_clk40_9  -source [get_pins g_clk40_bufgmux[9 ].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[9 ].buf0/O]
#create_generated_clock -name gbt_lti_clk40_10 -source [get_pins g_clk40_bufgmux[10].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[10].buf0/O]
#create_generated_clock -name gbt_lti_clk40_11 -source [get_pins g_clk40_bufgmux[11].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[11].buf0/O]
#create_generated_clock -name gbt_lti_clk40_12 -source [get_pins g_clk40_bufgmux[12].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[12].buf0/O]
#create_generated_clock -name gbt_lti_clk40_13 -source [get_pins g_clk40_bufgmux[13].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[13].buf0/O]
#create_generated_clock -name gbt_lti_clk40_14 -source [get_pins g_clk40_bufgmux[14].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[14].buf0/O]
#create_generated_clock -name gbt_lti_clk40_15 -source [get_pins g_clk40_bufgmux[15].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[15].buf0/O]
#create_generated_clock -name gbt_lti_clk40_16 -source [get_pins g_clk40_bufgmux[16].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[16].buf0/O]
#create_generated_clock -name gbt_lti_clk40_17 -source [get_pins g_clk40_bufgmux[17].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[17].buf0/O]
#create_generated_clock -name gbt_lti_clk40_18 -source [get_pins g_clk40_bufgmux[18].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[18].buf0/O]
#create_generated_clock -name gbt_lti_clk40_19 -source [get_pins g_clk40_bufgmux[19].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[19].buf0/O]
#create_generated_clock -name gbt_lti_clk40_20 -source [get_pins g_clk40_bufgmux[20].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[20].buf0/O]
#create_generated_clock -name gbt_lti_clk40_21 -source [get_pins g_clk40_bufgmux[21].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[21].buf0/O]
#create_generated_clock -name gbt_lti_clk40_22 -source [get_pins g_clk40_bufgmux[22].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[22].buf0/O]
#create_generated_clock -name gbt_lti_clk40_23 -source [get_pins g_clk40_bufgmux[23].bufg_clk40/O] -divide_by 6 [get_pins g_clk40_bufgmux[23].buf0/O]


#set_max_delay -datapath_only -from [get_clocks clk40_lti_gbt*] -to [get_clocks clk40_clk_wiz_40_0*] 24.95
#set_max_delay -datapath_only -from [get_clocks rxoutclk_out*] -to [get_clocks clk40_clk_wiz_40_0*] 24.95
#set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk40_lti_gbt*]   24.95
#set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks rxoutclk_out*] 24.95
#set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks gtwiz_userclk_tx_srcclk_out*] 24.95
#set_multicycle_path -setup -start -from [get_clocks rxoutclk_out*] -to [get_clocks gbt_clk40_*] 2
#set_multicycle_path -hold -end -from [get_clocks rxoutclk_out*] -to [get_clocks gbt_clk40_*] 1


#Register map to 400kHz I2C clock
set_max_delay -quiet -datapath_only -from [get_clocks *] -to [get_clocks clk400] 100.000
set_max_delay -quiet -datapath_only -from [get_clocks *] -to [get_clocks i2c_clock_pex] 100.000

#Register map control / monitor
set_max_delay -datapath_only -from [get_clocks clk_out25*] -to [get_clocks *] 40.000
set_max_delay -datapath_only -from [get_clocks *] -to [get_clocks clk_out25*] 40.000

#switchable output clock can switch at any time
set_max_delay -quiet -datapath_only -from [get_clocks clk_adn_160*] -to [get_clocks clk40_clk_wiz_40_0*] 6.238
set_max_delay -quiet -datapath_only -from [get_clocks clk_adn_160*] -to [get_clocks clk_ttc_40*] 6.238
set_max_delay -quiet -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk_ttc_40*] 24.95
set_max_delay -quiet -datapath_only -from [get_clocks clk_ttc_40*] -to [get_clocks clk40_clk_wiz_40_0*] 24.95

set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" }] 4
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" }] 3
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" }] 4
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" }] 3
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" }] 4
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" }] 3
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" }] 4
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" }] 3

 
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 1
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 0
 
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler16bit/RX_EXTRA_DATA_WIDEBUS_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 1
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler16bit/RX_EXTRA_DATA_WIDEBUS_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 0
 
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixDescrambler/RX_HEADER_O_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 1
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixDescrambler/RX_HEADER_O_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 0

set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*din_3x_async_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*dout_3x_reg[*]/D" }] 2
set_multicycle_path -quiet -hold  -end   -from [get_pins -hierarchical -filter { NAME =~  "*din_3x_async_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*dout_3x_reg[*]/D" }] 1

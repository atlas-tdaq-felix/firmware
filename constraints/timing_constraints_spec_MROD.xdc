# file: timing_constraints_spec_MROD.xdc  Here are the special constraints for MROD.
# other timing constraints (BNL711/712) are converted by a script !
# pcie0=u13/u5 pcie1=u23/u5 cr0=u13/u1 cr1=u23/u1 clk0=u31/u2

#create_clock -period 10.000 -name sys_clk0 [get_pins u13/u5/u1/g_ultrascale.refclk_buff/O]
#create_clock -period 10.000 -name sys_clk1 [get_pins u23/u5/u1/g_ultrascale.refclk_buff/O]
#create_clock -period 20.000 -name sys_clkdiv2_0 [get_pins u13/u5/u1/g_ultrascale.refclk_buff/ODIV2]
#create_clock -period 20.000 -name sys_clkdiv2_1 [get_pins u23/u5/u1/g_ultrascale.refclk_buff/ODIV2]
#create_clock -period 2500 -name clk400 [get_pins u33/u2/bufg_i2c/O]
#
#create_generated_clock -name clk_250mhz_x0y1 [get_pins u23/u5/u1/g_ultrascale.g_devid_7039.u1/U0/gt_top_i/phy_clk_i/bufg_gt_pclk/O]
#set_false_path -from [get_pins u23/u5/dma0/u1/reset_global_soft_40_s_reg/C]

#-------------------------------------------------------------

# Below: keep in this file
#set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks clk40_clk_wiz_40_0*] 24.95

# MROD timing
set_max_delay -datapath_only -from [get_clocks clk50_clk_wiz_40_0*] -to [get_clocks clk80_clk_wiz_40_0*] 20.0
set_max_delay -datapath_only -from [get_clocks clk80_clk_wiz_40_0*] -to [get_clocks clk50_clk_wiz_40_0*] 20.0
set_max_delay -datapath_only -from [get_clocks clk50_clk_wiz_40_0*] -to [get_clocks clk40_clk_wiz_40_0*] 24.95
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk50_clk_wiz_40_0*] 24.95

set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk250_clk_wiz_250*] 24.95
set_max_delay -datapath_only -from [get_clocks clk50_clk_wiz_40_0*] -to [get_clocks clk250_clk_wiz_250*] 20.00
set_max_delay -datapath_only -from [get_clocks clk80_clk_wiz_40_0*] -to [get_clocks clk250_clk_wiz_250*] 12.47

set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks ep0_n_16*] 24.95

#Clock domain crossings in GBT
#Clock domain crossings between Wupper and Central Router
#set_max_delay -datapath_only -from [get_clocks *rd_clk*] -to [get_clocks clk40_clk_wiz_40_0*] 24.95
#set_max_delay -datapath_only -from [get_clocks *rd_clk*] -to [get_clocks clk80_clk_wiz_40_0*] 12.47
#set_max_delay -datapath_only -from [get_clocks *rd_clk*] -to [get_clocks clk160_clk_wiz_40_0*] 6.25
#set_max_delay -datapath_only -from [get_clocks *rd_clk*] -to [get_clocks clk240_clk_wiz_40_0*] 4.17
#set_max_delay -datapath_only -from [get_clocks *rd_clk*] -to [get_clocks clk250_clk_wiz_250*]  3.99
#
#set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*]  -to [get_clocks *rd_clk*] 24.95
#set_max_delay -datapath_only -from [get_clocks clk80_clk_wiz_40_0*]  -to [get_clocks *rd_clk*] 12.47
#set_max_delay -datapath_only -from [get_clocks clk160_clk_wiz_40_0*] -to [get_clocks *rd_clk*] 6.25
#set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_0*] -to [get_clocks *rd_clk*] 4.17
#set_max_delay -datapath_only -from [get_clocks clk250_clk_wiz_250*]  -to [get_clocks *rd_clk*] 3.99
#
#Register map to 400kHz I2C clock
#set_max_delay -datapath_only -from [get_clocks *rd_clk*] -to [get_clocks clk400] 100

# GTH clocks and constraints
# rewrite GTHREFCLK_1 and GTHREFCLK_2 with correct MROD transceiver frequency
create_clock -period 5.0 -name GTHREFCLK_1 -waveform {0.000 2.5} [get_ports {Q2_CLK0_GTREFCLK_PAD_P_IN}]
create_clock -period 5.0 -name GTHREFCLK_3 -waveform {0.000 2.5} [get_ports {Q5_CLK0_GTREFCLK_PAD_P_IN}]
create_clock -period 5.0 -name GTHREFCLK_5 -waveform {0.000 2.5} [get_ports {Q6_CLK0_GTREFCLK_PAD_P_IN}]
create_clock -period 5.0 -name GTHREFCLK_2 -waveform {0.000 2.5} [get_ports {Q8_CLK0_GTREFCLK_PAD_P_IN}]
create_clock -period 5.0 -name GTHREFCLK_4 -waveform {0.000 2.5} [get_ports {Q4_CLK0_GTREFCLK_PAD_P_IN}]

#---------------------------- TXCVR constraints ----------------------
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets u11/u1/gtrefclk_Q2]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets u11/u1/gtrefclk_Q5]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets u11/u1/gtrefclk_Q6]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets u21/u1/gtrefclk_Q8]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets u21/u1/gtrefclk_Q4]

# MROD specific GTH constraints
# ep0, rx 0-23
create_clock -period 20.0 -name RXOUTCLKA_0  -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[0].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_1  -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[1].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_2  -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[2].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_3  -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[3].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_4  -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[4].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_5  -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[5].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_6  -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[6].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_7  -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[7].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_8  -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[8].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_9  -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[9].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_10 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[10].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_11 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[11].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_12 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[12].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_13 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[13].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_14 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[14].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_15 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[15].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_16 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[16].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_17 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[17].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_18 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[18].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_19 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[19].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_20 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[20].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_21 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[21].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_22 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[22].g0_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKA_23 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[23].g0_ch.BUFG_GT_RXinst/O}]
# ep0, tx 0-23
create_clock -period 20.0 -name TXOUTCLKA_0  -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[0].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_1  -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[1].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_2  -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[2].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_3  -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[3].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_4  -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[4].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_5  -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[5].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_6  -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[6].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_7  -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[7].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_8  -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[8].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_9  -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[9].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_10 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[10].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_11 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[11].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_12 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[12].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_13 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[13].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_14 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[14].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_15 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[15].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_16 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[16].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_17 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[17].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_18 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[18].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_19 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[19].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_20 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[20].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_21 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[21].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_22 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[22].g0_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKA_23 -waveform {0.000 10.0} [get_pins {u11/u1/g0_all[23].g0_ch.BUFG_GT_TXinst/O}]
# ep1, rx 0-23
create_clock -period 20.0 -name RXOUTCLKB_0  -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[0].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_1  -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[1].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_2  -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[2].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_3  -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[3].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_4  -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[4].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_5  -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[5].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_6  -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[6].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_7  -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[7].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_8  -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[8].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_9  -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[9].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_10 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[10].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_11 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[11].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_12 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[12].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_13 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[13].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_14 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[14].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_15 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[15].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_16 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[16].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_17 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[17].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_18 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[18].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_19 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[19].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_20 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[20].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_21 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[21].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_22 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[22].g1_ch.BUFG_GT_RXinst/O}]
create_clock -period 20.0 -name RXOUTCLKB_23 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[23].g1_ch.BUFG_GT_RXinst/O}]
# ep1, tx 0-23
create_clock -period 20.0 -name TXOUTCLKB_0  -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[0].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_1  -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[1].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_2  -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[2].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_3  -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[3].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_4  -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[4].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_5  -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[5].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_6  -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[6].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_7  -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[7].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_8  -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[8].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_9  -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[9].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_10 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[10].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_11 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[11].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_12 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[12].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_13 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[13].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_14 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[14].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_15 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[15].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_16 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[16].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_17 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[17].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_18 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[18].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_19 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[19].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_20 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[20].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_21 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[21].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_22 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[22].g1_ch.BUFG_GT_TXinst/O}]
create_clock -period 20.0 -name TXOUTCLKB_23 -waveform {0.000 10.0} [get_pins {u21/u1/g1_all[23].g1_ch.BUFG_GT_TXinst/O}]
#
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40*] -to [get_clocks RXOUTCLK*] 24.95
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40*] -to [get_clocks TXOUTCLK*] 24.95
set_max_delay -datapath_only -from [get_clocks clk50_clk_wiz_40*] -to [get_clocks RXOUTCLK*] 20.0
set_max_delay -datapath_only -from [get_clocks clk50_clk_wiz_40*] -to [get_clocks TXOUTCLK*] 20.0
set_max_delay -datapath_only -from [get_clocks clk80_clk_wiz_40*] -to [get_clocks RXOUTCLK*] 20.0
set_max_delay -datapath_only -from [get_clocks clk80_clk_wiz_40*] -to [get_clocks TXOUTCLK*] 20.0

set_max_delay -datapath_only -from [get_clocks clk50_clk_wiz_40_0*] -to [get_clocks RXOUTCLK*] 20.0
set_max_delay -datapath_only -from [get_clocks clk50_clk_wiz_40_0*] -to [get_clocks TXOUTCLK*] 20.0
set_max_delay -datapath_only -from [get_clocks clk80_clk_wiz_40_0*] -to [get_clocks RXOUTCLK*] 20.0
set_max_delay -datapath_only -from [get_clocks clk80_clk_wiz_40_0*] -to [get_clocks TXOUTCLK*] 20.0

set_max_delay -datapath_only -from [get_clocks RXOUTCLK*] -to [get_clocks clk40_clk_wiz_40*] 24.95
set_max_delay -datapath_only -from [get_clocks TXOUTCLK*] -to [get_clocks clk40_clk_wiz_40*] 24.95

set_max_delay -datapath_only -from [get_clocks RXOUTCLK*] -to [get_clocks clk50_clk_wiz_40_0*] 20.0
set_max_delay -datapath_only -from [get_clocks TXOUTCLK*] -to [get_clocks clk50_clk_wiz_40_0*] 20.0
set_max_delay -datapath_only -from [get_clocks RXOUTCLK*] -to [get_clocks clk80_clk_wiz_40_0*] 20.0
set_max_delay -datapath_only -from [get_clocks TXOUTCLK*] -to [get_clocks clk80_clk_wiz_40_0*] 20.0

set_max_delay -datapath_only -from [get_clocks TXOUTCLK*] -to [get_clocks RXOUTCLK*] 20.0
set_max_delay -datapath_only -from [get_clocks RXOUTCLK*] -to [get_clocks TXOUTCLK*] 20.0


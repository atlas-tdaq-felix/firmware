#GBT Transceiver pins
#bank 128
set_property PACKAGE_PIN AC43 [get_ports {RX_P_FM[0]}]
set_property PACKAGE_PIN AF41 [get_ports {RX_P_FM[3]}]
set_property PACKAGE_PIN AE43 [get_ports {RX_P_FM[2]}]
set_property PACKAGE_PIN AG43 [get_ports {RX_P_FM[1]}]
#bank 127
set_property PACKAGE_PIN AH41 [get_ports {RX_P_FM[4]}]
set_property PACKAGE_PIN AJ43 [get_ports {RX_P_FM[5]}]
set_property PACKAGE_PIN AN43 [get_ports {RX_P_FM[6]}]
set_property PACKAGE_PIN AL43 [get_ports {RX_P_FM[7]}]
#bank 126
set_property PACKAGE_PIN AU43 [get_ports {RX_P_FM[8]}]
set_property PACKAGE_PIN AR43 [get_ports {RX_P_FM[9]}]
set_property PACKAGE_PIN BA43 [get_ports {RX_P_FM[10]}]
set_property PACKAGE_PIN AW43 [get_ports {RX_P_FM[11]}]
#bank 133
set_property PACKAGE_PIN C43 [get_ports {RX_P_FM[12]}]
set_property PACKAGE_PIN E43 [get_ports {RX_P_FM[13]}]
set_property PACKAGE_PIN G43 [get_ports {RX_P_FM[14]}]
set_property PACKAGE_PIN J43 [get_ports {RX_P_FM[15]}]
#bank 132                
set_property PACKAGE_PIN L43 [get_ports {RX_P_FM[16]}]
set_property PACKAGE_PIN N43 [get_ports {RX_P_FM[17]}]
set_property PACKAGE_PIN R43 [get_ports {RX_P_FM[18]}]
set_property PACKAGE_PIN U43 [get_ports {RX_P_FM[19]}]
#bank 131                
set_property PACKAGE_PIN W43  [get_ports {RX_P_FM[20]}]
set_property PACKAGE_PIN V41 [get_ports {RX_P_FM[21]}]
set_property PACKAGE_PIN AA43 [get_ports {RX_P_FM[22]}]
set_property PACKAGE_PIN AB41 [get_ports {RX_P_FM[23]}]



#MGT24,25,26,27 bank 231, use clk from bank 231 R6 (or 231 P8, or 229 Y8, or 233 L6)
#MGT28,29,30,31 bank 232, use clk from bank 231 R6 (or 231 P8, or 233 L6)
#MGT32,33,34,35 bank 233, use clk from bank 231 R6 (or 233 P8, or 233 L6)
#MGT36,37,38,39 bank 228, use clk from bank 228 AF8
#MGT40,41,42,43 bank 229, use clk from bank 231 R6 (or 231 P8, or 229 Y8)
#MGT44,45,46,47 bank 230, use clk from bank 231 R6 (or 231 P8, or 229 Y8)






# VC709 clock from Si5324
# FELIX: this is the quad clocking around Q2 (plus above and below)
set_property PACKAGE_PIN AD8 [get_ports GTREFCLK0_P_IN]
set_property PACKAGE_PIN AD7 [get_ports GTREFCLK0_N_IN]

set_property PACKAGE_PIN AU32 [get_ports SDA]
set_property PACKAGE_PIN AT35 [get_ports SCL]
set_property PACKAGE_PIN AY42 [get_ports i2cmux_rst]

set_property IOSTANDARD LVCMOS18 [get_ports SDA]
set_property IOSTANDARD LVCMOS18 [get_ports SCL]
set_property IOSTANDARD LVCMOS18 [get_ports i2cmux_rst]

#156.25MHz Si570 oscillator
set_property IOSTANDARD LVDS [get_ports app_clk_in_p]
set_property IOSTANDARD LVDS [get_ports app_clk_in_n]
set_property PACKAGE_PIN AL34 [get_ports app_clk_in_n]

set_property PACKAGE_PIN AT36 [get_ports si5324_resetn]
set_property IOSTANDARD LVCMOS18 [get_ports si5324_resetn]

set_property PACKAGE_PIN AW32 [get_ports clk_si5324_240_out_p]
set_property PACKAGE_PIN AW33 [get_ports clk_si5324_240_out_n]
set_property IOSTANDARD LVDS [get_ports clk_si5324_240_out_p]
set_property IOSTANDARD LVDS [get_ports clk_si5324_240_out_n]

###############################################################################
# GBT SFP Physical Constraints
###############################################################################
# SFP 
set_property PACKAGE_PIN AL5 [get_ports {gtrxn_in[0]}]
## FPGA configuration clock
set_property IOSTANDARD LVCMOS18 [get_ports emcclk]
set_property PACKAGE_PIN AP37 [get_ports emcclk]

#XADC GPIO
set_property PACKAGE_PIN BA21 [get_ports emcclk_out]
set_property IOSTANDARD LVCMOS18 [get_ports emcclk_out]

set_property PACKAGE_PIN AM39 [get_ports {leds[0]}]
set_property PACKAGE_PIN AN39 [get_ports {leds[1]}]
set_property PACKAGE_PIN AR37 [get_ports {leds[2]}]
set_property PACKAGE_PIN AT37 [get_ports {leds[3]}]
set_property PACKAGE_PIN AR35 [get_ports {leds[4]}]
set_property PACKAGE_PIN AP41 [get_ports {leds[5]}]
set_property PACKAGE_PIN AP42 [get_ports {leds[6]}]
set_property PACKAGE_PIN AU39 [get_ports {leds[7]}]
#
set_property IOSTANDARD LVCMOS18 [get_ports {leds[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[7]}]

set_property PACKAGE_PIN AP33 [get_ports {SFP_TX_ENABLE}]
set_property IOSTANDARD LVCMOS18 [get_ports {SFP_TX_ENABLE}]


set_property PACKAGE_PIN BA24 [get_ports {opto_inhibit[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {opto_inhibit[0]}]

set_property PACKAGE_PIN BB21 [get_ports {opto_inhibit[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {opto_inhibit[1]}]

set_property PACKAGE_PIN BB24 [get_ports {opto_inhibit[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {opto_inhibit[2]}]

set_property PACKAGE_PIN BB23 [get_ports {opto_inhibit[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {opto_inhibit[3]}]

set_property PACKAGE_PIN AV40 [get_ports RESET_BUTTON]
set_property IOSTANDARD LVCMOS18 [get_ports RESET_BUTTON]

# SMA USER_SMA_CLOCK_N J31
set_property PACKAGE_PIN AK32 [get_ports SmaOut_x3]
set_property IOSTANDARD LVCMOS18 [get_ports SmaOut_x3]
# JTA : set slew rate to FAST to allow us to see 160MHz-speed signals
# JTA : also set drive current to 16mA to allow us to drive a coax cable better
set_property SLEW FAST [get_ports SmaOut_x3]
set_property DRIVE 16 [get_ports SmaOut_x3]

# SMA USER_SMA_CLOCK_N J32
set_property PACKAGE_PIN AJ32 [get_ports SmaOut_x4]
set_property IOSTANDARD LVCMOS18 [get_ports SmaOut_x4]
# JTA : set slew rate to FAST to allow us to see 160MHz-speed signals
# JTA : also set drive current to 16mA to allow us to drive a coax cable better
set_property SLEW FAST [get_ports SmaOut_x4]
set_property DRIVE 16 [get_ports SmaOut_x4]
#Timing exception
                                                   
create_clock -name RXOUTCLK -period 4.17 [get_pins u0/g_gt_channel[0].g_gtx.gtxe2_i/RXOUTCLK]
create_clock -name TXOUTCLK -period 4.17 [get_pins u0/g_gt_channel[0].g_gtx.gtxe2_i/TXOUTCLK]

create_generated_clock -name clk40 [get_pins clk1/clk0/inst/mmcm_adv_inst/CLKOUT0]
set_max_delay -datapath_only -from [get_clocks clk40] -to [get_clocks TXOUTCLK] 24.97
set_max_delay -datapath_only -from [get_clocks clk40] -to [get_clocks RXOUTCLK] 24.97
set_max_delay -datapath_only -from [get_clocks RXOUTCLK] -to [get_clocks clk40] 24.97
set_max_delay -datapath_only -from [get_clocks TXOUTCLK] -to [get_clocks clk40] 24.97

set_property SEVERITY WARNING [get_drc_checks REQP-52] 

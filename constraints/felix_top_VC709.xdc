###############################################################################
# User Configuration
# Link Width   - x8
# Link Speed   - gen3
# Family       - virtex7
# Part         - xc7vx690t
# Package      - ffg1761
# Speed grade  - -2
# PCIe Block   - X0Y1
###############################################################################
#
#########################################################################################################################
# User Constraints
#########################################################################################################################
set_property CONFIG_VOLTAGE 1.8 [current_design]
set_property CFGBVS GND [current_design]
set_property BITSTREAM.CONFIG.BPI_SYNC_MODE Type1 [current_design]
set_property BITSTREAM.CONFIG.EXTMASTERCCLK_EN div-1 [current_design]
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]

###############################################################################
# User Time Names / User Time Groups / Time Specs
###############################################################################

###############################################################################
# User Physical Constraints
###############################################################################
set_property PACKAGE_PIN Y39 [get_ports {OPTO_LOS[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {OPTO_LOS[0]}]

set_property PACKAGE_PIN AA40 [get_ports {OPTO_LOS[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {OPTO_LOS[1]}]

set_property PACKAGE_PIN AD38 [get_ports {OPTO_LOS[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {OPTO_LOS[2]}]

set_property PACKAGE_PIN AD40 [get_ports {OPTO_LOS[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {OPTO_LOS[3]}]
#! file TEST.XDC
#! net constraints for TEST design
#156.25MHz Si570 oscillator
set_property IOSTANDARD LVDS [get_ports app_clk_in_p]
set_property IOSTANDARD LVDS [get_ports app_clk_in_n]
set_property PACKAGE_PIN AL34 [get_ports app_clk_in_n]
#200MHz SiTIME oscillator
#set_property IOSTANDARD DIFF_SSTL15_DCI [get_ports app_clk_in_p]
#set_property IOSTANDARD DIFF_SSTL15_DCI [get_ports app_clk_in_n]
#set_property PACKAGE_PIN G18 [get_ports app_clk_in_n]

## FPGA configuration clock
set_property IOSTANDARD LVCMOS18 [get_ports emcclk]
set_property PACKAGE_PIN AP37 [get_ports emcclk]

#XADC GPIO
#set_property PACKAGE_PIN AR38 [get_ports emcclk_out]
#set_property IOSTANDARD LVCMOS18 [get_ports emcclk_out] 

# TTC Clock Jitter Cleaned
# -- FMC_LA[17]_CC_N
set_property IOSTANDARD LVDS [get_ports CLK_TTC_P]
set_property PACKAGE_PIN K32 [get_ports CLK_TTC_N]
set_property IOSTANDARD LVDS [get_ports CLK_TTC_N]

# TTC Data
# -- FMC_LA[20]_N
set_property IOSTANDARD LVDS [get_ports DATA_TTC_P]
set_property PACKAGE_PIN Y30 [get_ports DATA_TTC_N]
set_property IOSTANDARD LVDS [get_ports DATA_TTC_N]
#  ADN2814 LOL, LOS
# -- FMC_LA[09]_P
set_property IOSTANDARD LVCMOS18 [get_ports LOL_ADN]
set_property PACKAGE_PIN R42 [get_ports LOL_ADN]
# -- FMC_LA[09]_N
set_property IOSTANDARD LVCMOS18 [get_ports LOS_ADN]
set_property PACKAGE_PIN P42 [get_ports LOS_ADN]
# BUSY Out LEMO connector on TTCfx v3 board
set_property PACKAGE_PIN M37 [get_ports BUSY_OUT]
set_property IOSTANDARD LVCMOS18 [get_ports BUSY_OUT]

## TTCfx clocks
# -- FMC_LA[1]_CC_P
# -- FMC_LA[1]_CC_N
set_property IOSTANDARD LVDS  [get_ports clk40_ttc_ref_out_p]
set_property PACKAGE_PIN J41  [get_ports clk40_ttc_ref_out_n]
set_property IOSTANDARD LVDS  [get_ports clk40_ttc_ref_out_n]
# -- FMC_LA[18]_CC_P
# -- FMC_LA[18]_CC_N
#set_property IOSTANDARD LVDS  [get_ports clk_ttcfx_ref2_out_p]
#set_property PACKAGE_PIN L32  [get_ports clk_ttcfx_ref2_out_n]
#set_property IOSTANDARD LVDS  [get_ports clk_ttcfx_ref2_out_n]
# -- FMC_LA[00]_CC_P
# -- FMC_LA[00]_CC_N
set_property IOSTANDARD LVDS  [get_ports clk_ttcfx_ref1_in_p]
set_property PACKAGE_PIN K40  [get_ports clk_ttcfx_ref1_in_n]
set_property IOSTANDARD LVDS  [get_ports clk_ttcfx_ref1_in_n]
# -- FMC_HA[17]_CC_P
# -- FMC_HA[17]_CC_N
set_property IOSTANDARD LVDS  [get_ports clk_ttcfx_ref2_in_p]
set_property PACKAGE_PIN C36  [get_ports clk_ttcfx_ref2_in_n]
set_property IOSTANDARD LVDS  [get_ports clk_ttcfx_ref2_in_n]

#set_property PACKAGE_PIN K39  [get_ports ttcfx_clk_ref0_p]
#set_property IOSTANDARD LVDS [get_ports ttcfx_clk_ref0_p]
#set_property PACKAGE_PIN K40  [get_ports ttcfx_clk_ref0_n]
#set_property IOSTANDARD LVDS [get_ports ttcfx_clk_ref0_n]
# -- FMC_HB[4]_P
# -- FMC_HB[4]_N
#set_property PACKAGE_PIN H24  [get_ports ttcfx_clk_ref1_p]
#set_property IOSTANDARD LVDS  [get_ports ttcfx_clk_ref1_p]
#set_property PACKAGE_PIN G24  [get_ports ttcfx_clk_ref1_n]
#set_property IOSTANDARD LVDS  [get_ports ttcfx_clk_ref1_n]
# -- FMC_HA[17]_CC_P
# -- FMC_HA[17]_CC_N
#set_property PACKAGE_PIN C35  [get_ports ttcfx_clk_ref2_p]
#set_property IOSTANDARD LVDS  [get_ports ttcfx_clk_ref2_p]
#set_property PACKAGE_PIN C36  [get_ports ttcfx_clk_ref2_n]
#set_property IOSTANDARD LVDS  [get_ports ttcfx_clk_ref2_n]

#System Reset, User Reset, User Link Up, User Clk Heartbeat
set_property PACKAGE_PIN AM39 [get_ports {leds[0]}]
set_property PACKAGE_PIN AN39 [get_ports {leds[1]}]
set_property PACKAGE_PIN AR37 [get_ports {leds[2]}]
set_property PACKAGE_PIN AT37 [get_ports {leds[3]}]
set_property PACKAGE_PIN AR35 [get_ports {leds[4]}]
set_property PACKAGE_PIN AP41 [get_ports {leds[5]}]
set_property PACKAGE_PIN AP42 [get_ports {leds[6]}]
set_property PACKAGE_PIN AU39 [get_ports {leds[7]}]
#
set_property IOSTANDARD LVCMOS18 [get_ports {leds[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[7]}]

### Housekeeping signals
## TTCfx SPI interface
# -- CDCE_REF_SEL  -> FMC_LA[14]_P
# -- CDCE_PD       -> FMC_LA[6]_P
# -- CDCE_SYNC     -> FMC_LA[14]_N
# -- CDCE_SPI_LE   -> FMC_LA[9]_P
# -- CDCE_SPI_CLK  -> FMC_LA[9]_N
# -- CDCE_SPI_MOSI -> FMC_LA[5]_N
# -- CDCE_SPI_MISO -> FMC_LA[5]_P
# -- CDCE_PLL_LOCK -> FMC_LA[6]_N

#set_property PACKAGE_PIN N39 [get_ports CDCE_REF_SEL]
#set_property PACKAGE_PIN K42 [get_ports CDCE_PD]
#set_property PACKAGE_PIN N40 [get_ports CDCE_SYNC]
#set_property PACKAGE_PIN R42 [get_ports CDCE_SPI_LE]
#set_property PACKAGE_PIN P42 [get_ports CDCE_SPI_CLK]
#set_property PACKAGE_PIN L41 [get_ports CDCE_SPI_MOSI]
#set_property PACKAGE_PIN M41 [get_ports CDCE_SPI_MISO]
#set_property PACKAGE_PIN J42 [get_ports CDCE_PLL_LOCK]

#set_property IOSTANDARD LVCMOS18 [get_ports CDCE_REF_SEL]
#set_property IOSTANDARD LVCMOS18 [get_ports CDCE_PD]
#set_property IOSTANDARD LVCMOS18 [get_ports CDCE_SYNC]
#set_property IOSTANDARD LVCMOS18 [get_ports CDCE_SPI_LE]
#set_property IOSTANDARD LVCMOS18 [get_ports CDCE_SPI_CLK]
#set_property IOSTANDARD LVCMOS18 [get_ports CDCE_SPI_MOSI]
#set_property IOSTANDARD LVCMOS18 [get_ports CDCE_SPI_MISO]
#set_property IOSTANDARD LVCMOS18 [get_ports CDCE_PLL_LOCK]

## TTCfx Si5345 interface
# -- INSEL 1 -> FMC_LA[27]_N
# -- INSEL 0 -> FMC_LA[27]_P
# -- A 1     -> FMC_LA[13]_N
# -- A 0     -> FMC_LA[13]_P
# -- OE      -> FMC_LA[23]_P
# -- RSTN    -> FMC_LA[23]_N
# -- SEL     -> FMC_LA[26]_P
# -- nLOL    -> FMC_LA[6]_N
# -- FDEC_B  -> FMC_LA05_N
# -- FINC_B  -> FMC_LA05_P
# -- INTR_B  -> FMC_LA06_P

set_property PACKAGE_PIN H31 [get_ports {SI5345_INSEL[1]}]
set_property PACKAGE_PIN J31 [get_ports {SI5345_INSEL[0]}]
set_property PACKAGE_PIN G39 [get_ports {SI5345_A[1]}]
set_property PACKAGE_PIN H39 [get_ports {SI5345_A[0]}]
set_property PACKAGE_PIN P30 [get_ports SI5345_OE]
set_property PACKAGE_PIN N31 [get_ports SI5345_RSTN]
set_property PACKAGE_PIN J30 [get_ports SI5345_SEL]
set_property PACKAGE_PIN J42 [get_ports SI5345_nLOL]
set_property PACKAGE_PIN L41 [get_ports {SI5345_FDEC_B[0]}]
set_property PACKAGE_PIN M41 [get_ports {SI5345_FINC_B[0]}]
set_property PACKAGE_PIN K42 [get_ports {SI5345_INTR_B[0]}]

set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_INSEL[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_INSEL[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_A[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_A[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports SI5345_OE]
set_property IOSTANDARD LVCMOS18 [get_ports SI5345_RSTN]
set_property IOSTANDARD LVCMOS18 [get_ports SI5345_SEL]
set_property IOSTANDARD LVCMOS18 [get_ports SI5345_nLOL]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_FDEC_B[0]}] 
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_FINC_B[0]}] 
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_INTR_B[0]}] 

### I2C master for overall configuration
## see UG887 page 44
##           -- PCA9548
##              -- CH0 USR_CLK
##              -- CH1 FMC
##              -- CH4 SFP -> goes to PCA9546 
##              -- CH7 Si5324
##           -- PCA9546 (0x75)
##              -- CH0 SFP1
##              -- CH1 SFP2
##              -- CH2 SFP3
##              -- CH3 SFP4

set_property PACKAGE_PIN AU32 [get_ports SDA]
set_property PACKAGE_PIN AT35 [get_ports SCL]
set_property PACKAGE_PIN AY42 [get_ports i2cmux_rst]

set_property IOSTANDARD LVCMOS18 [get_ports SDA]
set_property IOSTANDARD LVCMOS18 [get_ports SCL]
set_property IOSTANDARD LVCMOS18 [get_ports i2cmux_rst]

set_property IOSTANDARD LVCMOS18 [get_ports TACH]
set_property PACKAGE_PIN BB37 [get_ports TACH]

# DEBUG PORT: TTC, GBT, Clock monitor signals

#set_property PACKAGE_PIN AJ32 [get_ports TTC_L1accept]
#set_property IOSTANDARD LVCMOS18 [get_ports TTC_L1accept]
## JTA : set slew rate to FAST to allow us to see 160MHz-speed signals
## JTA : also set drive current to 16mA to allow us to drive a coax cable better
#set_property SLEW FAST [get_ports TTC_L1accept]
#set_property DRIVE 16 [get_ports TTC_L1accept]

# SMA USER_SMA_CLOCK_N J31
set_property PACKAGE_PIN AK32 [get_ports {SmaOut[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SmaOut[0]}]
# JTA : set slew rate to FAST to allow us to see 160MHz-speed signals
# JTA : also set drive current to 16mA to allow us to drive a coax cable better
set_property SLEW FAST [get_ports {SmaOut[0]}]
set_property DRIVE 16 [get_ports {SmaOut[0]}]

# SMA USER_SMA_CLOCK_N J32
set_property PACKAGE_PIN AJ32 [get_ports {SmaOut[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SmaOut[1]}]
# JTA : set slew rate to FAST to allow us to see 160MHz-speed signals
# JTA : also set drive current to 16mA to allow us to drive a coax cable better
set_property SLEW FAST [get_ports {SmaOut[1]}]
set_property DRIVE 16 [get_ports {SmaOut[1]}]


#########################################################################################################################
# End User Constraints
#########################################################################################################################
#
#
#
#########################################################################################################################
# PCIE Core Constraints
#########################################################################################################################

#
# SYS reset (input) signal.  The sys_reset_n signal should be
# obtained from the PCI Express interface if possible.  For
# slot based form factors, a system reset signal is usually
# present on the connector.  For cable based form factors, a
# system reset signal may not be available.  In this case, the
# system reset signal must be generated locally by some form of
# supervisory circuit.  You may change the IOSTANDARD and LOC
# to suit your requirements and VCCO voltage banking rules.
# Some 7 series devices do not have 3.3 V I/Os available.
# Therefore the appropriate level shift is required to operate
# with these devices that contain only 1.8 V banks.
#

set_property PACKAGE_PIN AV35 [get_ports sys_reset_n]
set_property IOSTANDARD LVCMOS18 [get_ports sys_reset_n]
set_property PULLUP true [get_ports sys_reset_n]

#
#
# SYS clock 100 MHz (input) signal. The sys_clk_p and sys_clk_n
# signals are the PCI Express reference clock. Virtex-7 GT
# Transceiver architecture requires the use of a dedicated clock
# resources (FPGA input pins) associated with each GT Transceiver.
# To use these pins an IBUFDS primitive (refclk_ibuf) is
# instantiated in user's design.
# Please refer to the Virtex-7 GT Transceiver User Guide
# (UG) for guidelines regarding clock resource selection.
#
set_property LOC IBUFDS_GTE2_X1Y11 [get_cells g_endpoints[0].pcie0/ep0/g_NoSim.g_virtex7.refclk_buff]

#BPI Flash pins
set_property PACKAGE_PIN AJ28 [get_ports {flash_a[0]}]
set_property PACKAGE_PIN AH28 [get_ports {flash_a[1]}]
set_property PACKAGE_PIN AG31 [get_ports {flash_a[2]}]
set_property PACKAGE_PIN AF30 [get_ports {flash_a[3]}]
set_property PACKAGE_PIN AK29 [get_ports {flash_a[4]}]
set_property PACKAGE_PIN AK28 [get_ports {flash_a[5]}]
set_property PACKAGE_PIN AG29 [get_ports {flash_a[6]}]
set_property PACKAGE_PIN AK30 [get_ports {flash_a[7]}]
set_property PACKAGE_PIN AJ30 [get_ports {flash_a[8]}]
set_property PACKAGE_PIN AH30 [get_ports {flash_a[9]}]
set_property PACKAGE_PIN AH29 [get_ports {flash_a[10]}]
set_property PACKAGE_PIN AL30 [get_ports {flash_a[11]}]
set_property PACKAGE_PIN AL29 [get_ports {flash_a[12]}]
set_property PACKAGE_PIN AN33 [get_ports {flash_a[13]}]
set_property PACKAGE_PIN AM33 [get_ports {flash_a[14]}]
set_property PACKAGE_PIN AM32 [get_ports {flash_a[15]}]
set_property PACKAGE_PIN AV41 [get_ports {flash_a[16]}]
set_property PACKAGE_PIN AU41 [get_ports {flash_a[17]}]
set_property PACKAGE_PIN BA42 [get_ports {flash_a[18]}]
set_property PACKAGE_PIN AU42 [get_ports {flash_a[19]}]
set_property PACKAGE_PIN AT41 [get_ports {flash_a[20]}]
set_property PACKAGE_PIN BA40 [get_ports {flash_a[21]}]
set_property PACKAGE_PIN BA39 [get_ports {flash_a[22]}]
set_property PACKAGE_PIN BB39 [get_ports {flash_a[23]}]
set_property PACKAGE_PIN AW42 [get_ports {flash_a[24]}]
#set_property PACKAGE_PIN AW41 [get_ports {flash_a[25]}]

set_property PACKAGE_PIN AW41 [get_ports {flash_a_msb[0]}]
#set_property PACKAGE_PIN BC27 [get_ports {flash_a_msb[1]}]


set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[7]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[8]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[9]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[10]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[11]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[12]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[13]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[14]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[15]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[16]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[17]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[18]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[19]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[20]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[21]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[22]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[23]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[24]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[25]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[26]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a_msb[0]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {flash_a_msb[1]}]


set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[7]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[8]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[9]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[10]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[11]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[12]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[13]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[14]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[15]}]

set_property PACKAGE_PIN AM36 [get_ports {flash_d[0]}]
set_property PACKAGE_PIN AN36 [get_ports {flash_d[1]}]
set_property PACKAGE_PIN AJ36 [get_ports {flash_d[2]}]
set_property PACKAGE_PIN AJ37 [get_ports {flash_d[3]}]
set_property PACKAGE_PIN AK37 [get_ports {flash_d[4]}]
set_property PACKAGE_PIN AL37 [get_ports {flash_d[5]}]
set_property PACKAGE_PIN AN35 [get_ports {flash_d[6]}]
set_property PACKAGE_PIN AP35 [get_ports {flash_d[7]}]
set_property PACKAGE_PIN AM37 [get_ports {flash_d[8]}]
set_property PACKAGE_PIN AG33 [get_ports {flash_d[9]}]
set_property PACKAGE_PIN AH33 [get_ports {flash_d[10]}]
set_property PACKAGE_PIN AK35 [get_ports {flash_d[11]}]
set_property PACKAGE_PIN AL35 [get_ports {flash_d[12]}]
set_property PACKAGE_PIN AJ31 [get_ports {flash_d[13]}]
set_property PACKAGE_PIN AH34 [get_ports {flash_d[14]}]
set_property PACKAGE_PIN AJ35 [get_ports {flash_d[15]}]

#set_property IOSTANDARD LVCMOS25 [get_ports clk]
#set_property PACKAGE_PIN AK26 [get_ports clk]



set_property PACKAGE_PIN BB41 [get_ports flash_we]
set_property IOSTANDARD LVCMOS18 [get_ports flash_we]

set_property PACKAGE_PIN AY37 [get_ports flash_adv]
set_property IOSTANDARD LVCMOS18 [get_ports flash_adv]

set_property IOSTANDARD LVCMOS18 [get_ports flash_ce]
set_property PACKAGE_PIN AL36 [get_ports flash_ce]

#set_property IOSTANDARD LVCMOS18 [get_ports flash_cclk]
#set_property PACKAGE_PIN N10 [get_ports flash_cclk]

set_property IOSTANDARD LVCMOS18 [get_ports flash_re]
set_property PACKAGE_PIN BA41 [get_ports flash_re]

#AM34 FLASH_WAIT LVCMOS18 F7 WAIT



set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets emcclk_IBUF]

set_property IOSTANDARD LVCMOS18 [get_ports TACH]
set_property PACKAGE_PIN BB37 [get_ports TACH]

###############################################################################
# Unused ports, assign to random unused pins
###############################################################################
set_property PACKAGE_PIN AT39 [get_ports CLK40_FPGA2LMK_N]
set_property PACKAGE_PIN AT40 [get_ports CLK40_FPGA2LMK_P]
set_property IOSTANDARD LVCMOS18 [get_ports CLK40_FPGA2LMK_N]
set_property IOSTANDARD LVCMOS18 [get_ports CLK40_FPGA2LMK_P]


set_property PACKAGE_PIN AR42 [get_ports {flash_a_msb[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a_msb[1]}]

set_property PACKAGE_PIN AT42 [get_ports I2C_nRESET_PCIe]
set_property IOSTANDARD LVCMOS18 [get_ports I2C_nRESET_PCIe]

set_property PACKAGE_PIN AY40 [get_ports I2C_SMB]
set_property IOSTANDARD LVCMOS18 [get_ports I2C_SMB]

set_property PACKAGE_PIN AW38 [get_ports I2C_SMBUS_CFG_nEN]
set_property IOSTANDARD LVCMOS18 [get_ports I2C_SMBUS_CFG_nEN]

set_property PACKAGE_PIN AY38 [get_ports LMK_CLK]
set_property IOSTANDARD LVCMOS18 [get_ports LMK_CLK]

set_property PACKAGE_PIN BB38 [get_ports LMK_DATA]
set_property IOSTANDARD LVCMOS18 [get_ports LMK_DATA]

set_property PACKAGE_PIN AH35 [get_ports LMK_GOE]
set_property IOSTANDARD LVCMOS18 [get_ports LMK_GOE]

set_property PACKAGE_PIN AH31 [get_ports LMK_LD]
set_property IOSTANDARD LVCMOS18 [get_ports LMK_LD]

set_property PACKAGE_PIN AJ33 [get_ports LMK_LE]
set_property IOSTANDARD LVCMOS18 [get_ports LMK_LE]

set_property PACKAGE_PIN AK33 [get_ports LMK_SYNCn]
set_property IOSTANDARD LVCMOS18 [get_ports LMK_SYNCn]

set_property PACKAGE_PIN AL31 [get_ports MGMT_PORT_EN]
set_property IOSTANDARD LVCMOS18 [get_ports MGMT_PORT_EN]

set_property PACKAGE_PIN AF29 [get_ports {NT_PORTSEL[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {NT_PORTSEL[0]}]

set_property PACKAGE_PIN AG32 [get_ports {NT_PORTSEL[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {NT_PORTSEL[1]}]

set_property PACKAGE_PIN AY34 [get_ports {NT_PORTSEL[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {NT_PORTSEL[2]}]


set_property PACKAGE_PIN BA35 [get_ports PCIE_PERSTn1]
set_property IOSTANDARD LVCMOS18 [get_ports PCIE_PERSTn1]

set_property PACKAGE_PIN AV36 [get_ports PCIE_PERSTn2]
set_property IOSTANDARD LVCMOS18 [get_ports PCIE_PERSTn2]

set_property PACKAGE_PIN AW36 [get_ports Perstn1_open]
set_property IOSTANDARD LVCMOS18 [get_ports Perstn1_open]

set_property PACKAGE_PIN BA34 [get_ports Perstn2_open]
set_property IOSTANDARD LVCMOS18 [get_ports Perstn2_open]

set_property PACKAGE_PIN BB34 [get_ports PEX_PERSTn]
set_property IOSTANDARD LVCMOS18 [get_ports PEX_PERSTn]

set_property PACKAGE_PIN BA36 [get_ports PEX_SCL]
set_property IOSTANDARD LVCMOS18 [get_ports PEX_SCL]

set_property PACKAGE_PIN BB36 [get_ports PEX_SDA]
set_property IOSTANDARD LVCMOS18 [get_ports PEX_SDA]

set_property PACKAGE_PIN BB32 [get_ports {PORT_GOOD[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[0]}]

set_property PACKAGE_PIN BB33 [get_ports {PORT_GOOD[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[1]}]

set_property PACKAGE_PIN AW35 [get_ports {PORT_GOOD[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[2]}]

set_property PACKAGE_PIN AY35 [get_ports {PORT_GOOD[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[3]}]

set_property PACKAGE_PIN AT34 [get_ports {PORT_GOOD[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[4]}]

set_property PACKAGE_PIN AV34 [get_ports {PORT_GOOD[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[5]}]

set_property PACKAGE_PIN AY32 [get_ports {PORT_GOOD[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[6]}]

set_property PACKAGE_PIN AW31 [get_ports {PORT_GOOD[7]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[7]}]

#Unused in VC709 designs, but could be used in the future: FMC1 GBTCLK1
set_property -quiet PACKAGE_PIN E10 [get_ports {GTREFCLK_P_IN[1]}]
set_property -quiet PACKAGE_PIN E9  [get_ports {GTREFCLK_N_IN[1]}]

set_property PACKAGE_PIN AP32 [get_ports flash_cclk]
set_property IOSTANDARD LVCMOS18 [get_ports flash_cclk]

set_property PACKAGE_PIN AR32 [get_ports flash_SEL]
set_property IOSTANDARD LVCMOS18 [get_ports flash_SEL]

set_property PACKAGE_PIN AN31 [get_ports SHPC_INT]
set_property IOSTANDARD LVCMOS18 [get_ports SHPC_INT]

set_property PACKAGE_PIN AP31 [get_ports {SmaOut[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SmaOut[2]}]

set_property PACKAGE_PIN M33 [get_ports {SmaOut[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SmaOut[3]}]

set_property PACKAGE_PIN AP33 [get_ports {STN0_PORTCFG[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {STN0_PORTCFG[0]}]

set_property PACKAGE_PIN AR33 [get_ports {STN0_PORTCFG[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {STN0_PORTCFG[1]}]

set_property PACKAGE_PIN AT31 [get_ports {STN1_PORTCFG[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {STN1_PORTCFG[0]}]

set_property PACKAGE_PIN K35 [get_ports {STN1_PORTCFG[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {STN1_PORTCFG[1]}]

set_property PACKAGE_PIN J35 [get_ports {TESTMODE[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {TESTMODE[0]}]

set_property PACKAGE_PIN J32 [get_ports {TESTMODE[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {TESTMODE[1]}]

set_property PACKAGE_PIN J33 [get_ports {TESTMODE[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {TESTMODE[2]}]

set_property PACKAGE_PIN K33 [get_ports uC_reset_N]
set_property IOSTANDARD LVCMOS18 [get_ports uC_reset_N]

set_property PACKAGE_PIN K34 [get_ports {UPSTREAM_PORTSEL[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {UPSTREAM_PORTSEL[0]}]

set_property PACKAGE_PIN L34 [get_ports {UPSTREAM_PORTSEL[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {UPSTREAM_PORTSEL[1]}]

set_property PACKAGE_PIN L35 [get_ports {UPSTREAM_PORTSEL[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {UPSTREAM_PORTSEL[2]}]

###############################################################################
# Timing Constraints
###############################################################################
#create_clock -period 10.000 -name sys_clk [get_ports {sys_clk_p[0]}]

set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets clk0/g_156M.clk0/inst/clk_in1_clk_wiz_156_0]


set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets clk0/g_156M.clk0/inst/clk_in1_clk_wiz_156_0]
set_property -quiet CLOCK_DEDICATED_ROUTE FALSE [get_nets g1.u2/QPLL_GEN.GTH_inst[0].GTH_TOP_INST/qpll_inst/U0/gtwizard_QPLL_4p8g_V7_i/gt3_gtwizard_QPLL_4p8g_V7_i/gt3_rxoutclk_out]
#set_property -quiet CLOCK_DEDICATED_ROUTE FALSE [get_nets clk0/clk0/inst/clk40_clk_wiz_40_0]
#set_property -quiet CLOCK_DEDICATED_ROUTE ANY_CMT_COLUMN [get_nets clk0/clk0/inst/clk240_clk_wiz_40_0]

create_generated_clock -name clk_125mhz_x0y1 [get_pins g_endpoints[0].pcie0/ep0/g_NoSim.g_virtex7.pipe_clock0/mmcm0/CLKOUT0]
create_generated_clock -name clk_250mhz_x0y1 [get_pins g_endpoints[0].pcie0/ep0/g_NoSim.g_virtex7.pipe_clock0/mmcm0/CLKOUT1]
create_generated_clock -name userclk1 [get_pins g_endpoints[0].pcie0/ep0/g_NoSim.g_virtex7.pipe_clock0/mmcm0/CLKOUT2]
create_generated_clock -name userclk2 [get_pins g_endpoints[0].pcie0/ep0/g_NoSim.g_virtex7.pipe_clock0/mmcm0/CLKOUT3]

create_generated_clock -name clk_125mhz_mux_x0y1 -source [get_pins g_endpoints[0].pcie0/ep0/g_NoSim.g_virtex7.pipe_clock0/g0.pclk_i1/I0] -divide_by 1 [get_pins g_endpoints[0].pcie0/ep0/g_NoSim.g_virtex7.pipe_clock0/g0.pclk_i1/O]
create_generated_clock -name clk_250mhz_mux_x0y1 -source [get_pins g_endpoints[0].pcie0/ep0/g_NoSim.g_virtex7.pipe_clock0/g0.pclk_i1/I1] -divide_by 1 -add -master_clock clk_250mhz_x0y1 [get_pins g_endpoints[0].pcie0/ep0/g_NoSim.g_virtex7.pipe_clock0/g0.pclk_i1/O]
set_clock_groups -name pcieclkmux -physically_exclusive -group clk_125mhz_mux_x0y1 -group clk_250mhz_mux_x0y1
set_max_delay 4.000 -from [get_pins g_endpoints[0].pcie0/ep0/g_NoSim.g_virtex7.pipe_clock0/pclk_sel_reg/C] -to [get_pins g_endpoints[0].pcie0/ep0/g_NoSim.g_virtex7.pipe_clock0/g0.pclk_i1/S0]
set_max_delay 4.000 -from [get_pins g_endpoints[0].pcie0/ep0/g_NoSim.g_virtex7.pipe_clock0/pclk_sel_reg/C] -to [get_pins g_endpoints[0].pcie0/ep0/g_NoSim.g_virtex7.pipe_clock0/g0.pclk_i1/S1]

#U40 is 80 MHz oscillator 
create_clock -name emcclk -period 12.500 [get_ports emcclk]


set_property BITSTREAM.CONFIG.OVERTEMPPOWERDOWN ENABLE [current_design]

set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets clk0/g_156M.clk0/inst/clk_in1_clk_wiz_156_0]

###############################################################################
# End
###############################################################################

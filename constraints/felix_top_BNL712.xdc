###############################################################################
# User Configuration
# Link Width   - x16
# Link Speed   - gen3
# Family       - KintexUltrascale
# Part         -
# Package      -
# Speed grade  - -2
# PCIe Block   -
###############################################################################
#
###############################################################################
# User Constraints
###############################################################################

###############################################################################
# User Time Names / User Time Groups / Time Specs
###############################################################################

###############################################################################
# User Physical Constraints
###############################################################################

#! file TEST.XDC
#! net constraints for TEST design

set_property IOSTANDARD LVCMOS25 [get_ports {emcclk[0]}]
set_property PACKAGE_PIN AK26 [get_ports {emcclk[0]}]

#unused pin on bank 66
#set_property IOSTANDARD LVCMOS18 [get_ports emcclk_out]
#set_property PACKAGE_PIN BB22 [get_ports emcclk_out]


#set_property BITSTREAM.CONFIG.BPI_SYNC_MODE Type1 [current_design]
set_property BITSTREAM.CONFIG.BPI_SYNC_MODE disable [current_design]
#set_property BITSTREAM.CONFIG.EXTMASTERCCLK_EN div-1 [current_design]
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]

set_property CONFIG_MODE BPI16 [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 6 [current_design]
#set_property BITSTREAM.CONFIG.CONFIGRATE 9 [current_design]
set_property BITSTREAM.CONFIG.EXTMASTERCCLK_EN DISABLE [current_design]
#set_property BITSTREAM.CONFIG.EXTMASTERCCLK_EN DIV-6 [current_design]
#set_property BITSTREAM.CONFIG.EXTMASTERCCLK_EN DIV-8 [current_design]


##System Reset, User Reset, User Link Up, User Clk Heartbeat
set_property PACKAGE_PIN H11 [get_ports {leds[0]}]
set_property PACKAGE_PIN H10 [get_ports {leds[1]}]
set_property PACKAGE_PIN T10 [get_ports {leds[2]}]
set_property PACKAGE_PIN U15 [get_ports {leds[3]}]
set_property PACKAGE_PIN U14 [get_ports {leds[4]}]
set_property PACKAGE_PIN V12 [get_ports {leds[5]}]
set_property PACKAGE_PIN R12 [get_ports {leds[6]}]
#unused
#set_property PACKAGE_PIN A24 [get_ports {leds[7]}]
##
set_property IOSTANDARD LVCMOS33 [get_ports {leds[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {leds[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {leds[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {leds[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {leds[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {leds[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {leds[6]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {leds[7]}]

#Jumper J14 Position 8 selects bifurcation:
#* OFF ('1') (production default): Bifurcation
#* ON  ('0')                     : no bifurcation   
set_property IOSTANDARD LVCMOS18 [get_ports {select_bifurcation[0]}]
set_property PACKAGE_PIN P25 [get_ports {select_bifurcation[0]}]



set_property -quiet PACKAGE_PIN P10 [get_ports {OPTO_LOS[0]}]
set_property -quiet PACKAGE_PIN P14 [get_ports {OPTO_LOS[1]}]
set_property -quiet PACKAGE_PIN P13 [get_ports {OPTO_LOS[2]}]
set_property -quiet PACKAGE_PIN T13 [get_ports {OPTO_LOS[3]}]
set_property -quiet IOSTANDARD LVCMOS33 [get_ports {OPTO_LOS[0]}]
set_property -quiet IOSTANDARD LVCMOS33 [get_ports {OPTO_LOS[1]}]
set_property -quiet IOSTANDARD LVCMOS33 [get_ports {OPTO_LOS[2]}]
set_property -quiet IOSTANDARD LVCMOS33 [get_ports {OPTO_LOS[3]}]

################################################################################
# End User Constraints
################################################################################
#
#
#
#################################################################################
# PCIE Core Constraints
#################################################################################

#
# SYS reset (input) signal.  The sys_reset_n signal should be
# obtained from the PCI Express interface if possible.  For
# slot based form factors, a system reset signal is usually
# present on the connector.  For cable based form factors, a
# system reset signal may not be available.  In this case, the
# system reset signal must be generated locally by some form of
# supervisory circuit.  You may change the IOSTANDARD and LOC
# to suit your requirements and VCCO voltage banking rules.
# Some 7 series devices do not have 3.3 V I/Os available.
# Therefore the appropriate level shift is required to operate
# with these devices that contain only 1.8 V banks.
#

set_property PACKAGE_PIN J10 [get_ports sys_reset_n]
set_property IOSTANDARD LVCMOS33 [get_ports sys_reset_n]
set_property PULLUP true [get_ports sys_reset_n]

#set_property PACKAGE_PIN AK8 [get_ports sys_clk0_p]



###############################################################################
# Timing Constraints, specific to BNL711 design. Other timing constraints are in timing_constraints.xdc
###############################################################################
create_clock -period 10.000 -name sys_clk0 [get_pins {g_endpoints[0].pcie0/ep0/g_NoSim.g_ultrascale.refclk_buff/O}]
create_clock -period 10.000 -name sys_clk1 [get_pins {g_endpoints[1].pcie0/ep0/g_NoSim.g_ultrascale.refclk_buff/O}]
create_clock -period 20.000 -name sys_clkdiv2_0 [get_pins {g_endpoints[0].pcie0/ep0/g_NoSim.g_ultrascale.refclk_buff/ODIV2}]
create_clock -period 20.000 -name sys_clkdiv2_1 [get_pins {g_endpoints[1].pcie0/ep0/g_NoSim.g_ultrascale.refclk_buff/ODIV2}]
create_clock -period 2500.000 -name clk400 [get_pins hk0/*.pex_init0/data_clk_reg/Q]
create_clock -period 20.000 -name emcclk [get_ports emcclk]
#create_generated_clock -name clk_250mhz_x0y1 [get_pins g_endpoints[1].pcie0/ep0/g_NoSim.g_ultrascale.g_devid_7039.u1/U0/gt_top_i/phy_clk_i/bufg_gt_pclk/O]

###############################################################################
# Timing
###############################################################################
set_false_path -from [get_ports sys_reset_n]
#set_false_path -reset_path -from [get_pins g_endpoints[0].pcie0/ep0/g_NoSim.g_ultrascale.u1/inst/gt_top_i/pipe_wrapper_i/pipe_reset_i/cpllreset_reg/C]
#set_false_path -reset_path -from [get_pins g_endpoints[1].pcie0/ep0/g_NoSim.g_ultrascale.u1/inst/gt_top_i/pipe_wrapper_i/pipe_reset_i/cpllreset_reg/C]
#set_false_path -from [get_pins pcie0/dma0/u1/flush_fifo_reg/C]
#set_false_path -from [get_pins pcie1/dma0/u1/reset_global_soft_40_s_reg/C]

#taken /Projects/felix_top_ultrascale/felix_top_ultrascale.srcs/sources_1/ip/gtwizard_ultrascale_single_channel_cpll/synth/gtwizard_ultrascale_single_channel_cpll.xdc, which we disabled
set_false_path -to [get_cells -hierarchical -filter {NAME =~ *bit_synchronizer*inst/i_in_meta_reg}]
set_false_path -to [get_cells -hierarchical -filter {NAME =~ *reset_synchronizer*inst/rst_in_*_reg}]
###############################################################################
# End
###############################################################################

## MiniPOD enable input
set_property PACKAGE_PIN G14 [get_ports {opto_inhibit[0]}]
set_property PACKAGE_PIN H14 [get_ports {opto_inhibit[2]}]
set_property PACKAGE_PIN K10 [get_ports {opto_inhibit[1]}]
set_property PACKAGE_PIN K11 [get_ports {opto_inhibit[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {opto_inhibit[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {opto_inhibit[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {opto_inhibit[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {opto_inhibit[3]}]


#set_property LOC PCIE_3_1_X0Y3 [get_cells g_endpoints[1].pcie0/ep0/g_NoSim.g_ultrascale.g_devid_7039.u1/U0/pcie3_uscale_top_inst/pcie3_uscale_wrapper_inst/PCIE_3_1_inst]
#set_property LOC PCIE_3_1_X0Y1 [get_cells g_endpoints[1].pcie0/ep0/g_NoSim.g_ultrascale.g_devid_7038.u1/U0/pcie3_uscale_top_inst/pcie3_uscale_wrapper_inst/PCIE_3_1_inst]


#constraints for Bank 129/130
set_property PACKAGE_PIN V8 [get_ports {sys_clk_p[1]}]

#set_property LOC GTHE3_CHANNEL_X1Y20 [get_cells -hierarchical -filter {NAME =~ *gen_channel_container[29].*gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST}]
#set_property LOC GTHE3_CHANNEL_X1Y21 [get_cells -hierarchical -filter {NAME =~ *gen_channel_container[29].*gen_gthe3_channel_inst[1].GTHE3_CHANNEL_PRIM_INST}]
#set_property LOC GTHE3_CHANNEL_X1Y22 [get_cells -hierarchical -filter {NAME =~ *gen_channel_container[29].*gen_gthe3_channel_inst[2].GTHE3_CHANNEL_PRIM_INST}]
#set_property LOC GTHE3_CHANNEL_X1Y23 [get_cells -hierarchical -filter {NAME =~ *gen_channel_container[29].*gen_gthe3_channel_inst[3].GTHE3_CHANNEL_PRIM_INST}]
#set_property LOC GTHE3_CHANNEL_X1Y24 [get_cells -hierarchical -filter {NAME =~ *gen_channel_container[30].*gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST}]
#set_property LOC GTHE3_CHANNEL_X1Y25 [get_cells -hierarchical -filter {NAME =~ *gen_channel_container[30].*gen_gthe3_channel_inst[1].GTHE3_CHANNEL_PRIM_INST}]
#set_property LOC GTHE3_CHANNEL_X1Y26 [get_cells -hierarchical -filter {NAME =~ *gen_channel_container[30].*gen_gthe3_channel_inst[2].GTHE3_CHANNEL_PRIM_INST}]
#set_property LOC GTHE3_CHANNEL_X1Y27 [get_cells -hierarchical -filter {NAME =~ *gen_channel_container[30].*gen_gthe3_channel_inst[3].GTHE3_CHANNEL_PRIM_INST}]

## PCIe system clock inputs
set_property PACKAGE_PIN AK8 [get_ports {sys_clk_p[0]}]
## These loc constraints also set the PCIe transceiver pins
#set_property LOC GTHE3_CHANNEL_X1Y8 [get_cells -hierarchical -filter {NAME =~ *gen_channel_container[26].*gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST}]
#set_property LOC GTHE3_CHANNEL_X1Y9 [get_cells -hierarchical -filter {NAME =~ *gen_channel_container[26].*gen_gthe3_channel_inst[1].GTHE3_CHANNEL_PRIM_INST}]
#set_property LOC GTHE3_CHANNEL_X1Y10 [get_cells -hierarchical -filter {NAME =~ *gen_channel_container[26].*gen_gthe3_channel_inst[2].GTHE3_CHANNEL_PRIM_INST}]
#set_property LOC GTHE3_CHANNEL_X1Y11 [get_cells -hierarchical -filter {NAME =~ *gen_channel_container[26].*gen_gthe3_channel_inst[3].GTHE3_CHANNEL_PRIM_INST}]
#set_property LOC GTHE3_CHANNEL_X1Y12 [get_cells -hierarchical -filter {NAME =~ *gen_channel_container[27].*gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST}]
#set_property LOC GTHE3_CHANNEL_X1Y13 [get_cells -hierarchical -filter {NAME =~ *gen_channel_container[27].*gen_gthe3_channel_inst[1].GTHE3_CHANNEL_PRIM_INST}]
#set_property LOC GTHE3_CHANNEL_X1Y14 [get_cells -hierarchical -filter {NAME =~ *gen_channel_container[27].*gen_gthe3_channel_inst[2].GTHE3_CHANNEL_PRIM_INST}]
#set_property LOC GTHE3_CHANNEL_X1Y15 [get_cells -hierarchical -filter {NAME =~ *gen_channel_container[27].*gen_gthe3_channel_inst[3].GTHE3_CHANNEL_PRIM_INST}]

## I2C interface for the jitter cleaner
set_property PACKAGE_PIN V13 [get_ports SCL]
set_property PACKAGE_PIN V14 [get_ports SDA]
set_property IOSTANDARD LVCMOS33 [get_ports SCL]
set_property IOSTANDARD LVCMOS33 [get_ports SDA]

# I2C switch reset
#set_property PACKAGE_PIN W11 [get_ports I2C_nRESET]
#set_property IOSTANDARD LVCMOS33 [get_ports I2C_nRESET]

set_property PACKAGE_PIN W10 [get_ports {I2C_nRESET_PCIe[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {I2C_nRESET_PCIe[0]}]

## 200 MHz crystal clock input
#set_property IOSTANDARD LVDS [get_ports SYSCLK_P]
#set_property IOSTANDARD LVDS [get_ports SYSCLK_N]
#set_property DIFF_TERM_ADV TERM_100 [get_ports SYSCLK_P]
#set_property DIFF_TERM_ADV TERM_100 [get_ports SYSCLK_N]
#set_property PACKAGE_PIN R31 [get_ports SYSCLK_P]

set_property IOSTANDARD LVDS [get_ports {TB_trigger_P[0]}]
set_property IOSTANDARD LVDS [get_ports {TB_trigger_N[0]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {TB_trigger_P[0]}]
set_property DIFF_TERM_ADV TERM_100 [get_ports {TB_trigger_N[0]}]
set_property PACKAGE_PIN G17 [get_ports {TB_trigger_P[0]}]
set_property PACKAGE_PIN F17 [get_ports {TB_trigger_N[0]}]
set_property PULLDOWN true [get_ports {TB_trigger_P[0]}]
set_property PULLUP true [get_ports {TB_trigger_N[0]}]

set_property IOSTANDARD LVDS [get_ports app_clk_in_p]
set_property IOSTANDARD LVDS [get_ports app_clk_in_n]
set_property DIFF_TERM_ADV TERM_100 [get_ports app_clk_in_p]
set_property DIFF_TERM_ADV TERM_100 [get_ports app_clk_in_n]
set_property PACKAGE_PIN AT18 [get_ports app_clk_in_p]

## ADN TTC inputs (Data and Clock)
set_property PACKAGE_PIN H26 [get_ports {DATA_TTC_P[0]}]
set_property PACKAGE_PIN G25 [get_ports {CLK_TTC_P[0]}]
set_property IOSTANDARD LVDS [get_ports {DATA_TTC_P[0]}]
set_property IOSTANDARD LVDS [get_ports {DATA_TTC_N[0]}]
set_property IOSTANDARD LVDS [get_ports {CLK_TTC_P[0]}]
set_property IOSTANDARD LVDS [get_ports {CLK_TTC_N[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LOL_ADN[0]}]
set_property PACKAGE_PIN L12 [get_ports {LOL_ADN[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LOS_ADN[0]}]
set_property PACKAGE_PIN M11 [get_ports {LOS_ADN[0]}]

## BUSY Out LEMO connector
set_property PACKAGE_PIN J11 [get_ports {BUSY_OUT[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {BUSY_OUT[0]}]

## These input buffers have to be declared but are unconnected in the design
set_property PACKAGE_PIN AN27 [get_ports {Perstn_open[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Perstn_open[0]}]
set_property PACKAGE_PIN AV28 [get_ports {Perstn_open[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Perstn_open[1]}]

## Ports to configure the PEX switch
set_property PACKAGE_PIN AL12 [get_ports {I2C_SMB[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {I2C_SMB[0]}]
set_property PACKAGE_PIN AN22 [get_ports {I2C_SMBUS_CFG_nEN[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {I2C_SMBUS_CFG_nEN[0]}]
set_property PACKAGE_PIN AJ10 [get_ports {MGMT_PORT_EN[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {MGMT_PORT_EN[0]}]
set_property PACKAGE_PIN AK10 [get_ports {SHPC_INT[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SHPC_INT[0]}]
set_property PACKAGE_PIN AH12 [get_ports {PEX_PERSTn[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PEX_PERSTn[0]}]
set_property PACKAGE_PIN AH11 [get_ports {PCIE_PERSTn_out[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PCIE_PERSTn_out[0]}]
set_property PACKAGE_PIN AG11 [get_ports {PCIE_PERSTn_out[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PCIE_PERSTn_out[1]}]
set_property PACKAGE_PIN AT12 [get_ports {PEX_SDA[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PEX_SDA[0]}]
set_property PACKAGE_PIN AU12 [get_ports {PEX_SCL[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PEX_SCL[0]}]

#Test points
#set_property PACKAGE_PIN AL14 [get_ports TP1_P]
#set_property IOSTANDARD LVDS [get_ports TP1_P]
#set_property IOSTANDARD LVDS [get_ports TP1_N]

#set_property PACKAGE_PIN K22     [get_ports TP2_P]
#set_property IOSTANDARD LVDS [get_ports TP2_P]
#set_property IOSTANDARD LVDS [get_ports TP2_N]

#The ports below are unused in the design, but can be added to chipscope for debugging purposes.
set_property PACKAGE_PIN AU10 [get_ports {PORT_GOOD[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[0]}]
set_property PACKAGE_PIN AL14 [get_ports {PORT_GOOD[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[1]}]
set_property PACKAGE_PIN AK13 [get_ports {PORT_GOOD[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[2]}]
set_property PACKAGE_PIN AM14 [get_ports {PORT_GOOD[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[3]}]
set_property PACKAGE_PIN AL13 [get_ports {PORT_GOOD[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[4]}]
set_property PACKAGE_PIN AP14 [get_ports {PORT_GOOD[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[5]}]
set_property PACKAGE_PIN AN14 [get_ports {PORT_GOOD[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[6]}]
set_property PACKAGE_PIN AL15 [get_ports {PORT_GOOD[7]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[7]}]

## LMK03200
set_property PACKAGE_PIN AU22 [get_ports {CLK40_FPGA2LMK_P[0]}]
set_property PACKAGE_PIN AV22 [get_ports {CLK40_FPGA2LMK_N[0]}]
set_property IOSTANDARD LVDS [get_ports {CLK40_FPGA2LMK_P[0]}]
set_property IOSTANDARD LVDS [get_ports {CLK40_FPGA2LMK_N[0]}]
set_property PACKAGE_PIN K13 [get_ports {LMK_DATA[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LMK_DATA[0]}]
set_property PACKAGE_PIN J13 [get_ports {LMK_CLK[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LMK_CLK[0]}]
set_property PACKAGE_PIN K12 [get_ports {LMK_LE[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LMK_LE[0]}]
set_property PACKAGE_PIN L14 [get_ports {LMK_GOE[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LMK_GOE[0]}]
set_property PACKAGE_PIN J14 [get_ports {LMK_SYNCn[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LMK_SYNCn[0]}]
set_property PACKAGE_PIN L13 [get_ports {LMK_LD[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LMK_LD[0]}]
## Si5345 constraints
set_property PACKAGE_PIN N16 [get_ports {SI5345_INSEL[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_INSEL[0]}]
set_property PACKAGE_PIN M16 [get_ports {SI5345_INSEL[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_INSEL[1]}]
set_property PACKAGE_PIN P16 [get_ports {SI5345_nLOL[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_nLOL[0]}]
set_property PACKAGE_PIN R18 [get_ports {SI5345_SEL[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_SEL[0]}]
set_property PACKAGE_PIN R17 [get_ports {SI5345_OE[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_OE[0]}]
set_property PACKAGE_PIN R15 [get_ports {SI5345_A[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_A[0]}]
set_property PACKAGE_PIN P15 [get_ports {SI5345_A[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_A[1]}]
set_property -quiet PACKAGE_PIN D35 [get_ports {SI5345_RSTN[0]}]
set_property -quiet IOSTANDARD LVCMOS18 [get_ports {SI5345_RSTN[0]}] 
set_property PACKAGE_PIN N18 [get_ports {SI5345_FDEC_B[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_FDEC_B[0]}]
set_property PACKAGE_PIN R16 [get_ports {SI5345_FINC_B[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_FINC_B[0]}]
set_property PACKAGE_PIN P18 [get_ports {SI5345_INTR_B[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_INTR_B[0]}]




# Si5345 input from the main MMCM
set_property IOSTANDARD LVDS [get_ports {clk40_ttc_ref_out_p[0]}]
set_property PACKAGE_PIN AT20 [get_ports {clk40_ttc_ref_out_n[0]}]
set_property IOSTANDARD LVDS [get_ports {clk40_ttc_ref_out_n[0]}]
# Si5345 output to the FPGA fabric
## those are abused pins called DDR4 clocks
#set_property IOSTANDARD LVDS   [get_ports clk_ttcfx_ref1_in_p]
#set_property PACKAGE_PIN AT22  [get_ports clk_ttcfx_ref1_in_n]
#set_property IOSTANDARD LVDS   [get_ports clk_ttcfx_ref1_in_n]
#set_property IOSTANDARD LVDS   [get_ports clk_ttcfx_ref2_in_p]
#set_property PACKAGE_PIN H22   [get_ports clk_ttcfx_ref2_in_n]
#set_property IOSTANDARD LVDS   [get_ports clk_ttcfx_ref2_in_n]
#unused, assign to random pin
#set_property PACKAGE_PIN H21 [get_ports clk_adn_160_out_p]
#set_property IOSTANDARD LVDS [get_ports clk_adn_160_out_p]
#set_property IOSTANDARD LVDS [get_ports clk_adn_160_out_n]

#set_property PACKAGE_PIN R21 [get_ports GTREFCLK_Si5324_P_IN]
#set_property IOSTANDARD LVCMOS18 [get_ports GTREFCLK_Si5324_P_IN]
#set_property PACKAGE_PIN R20 [get_ports GTREFCLK_Si5324_N_IN]
#set_property IOSTANDARD LVCMOS18 [get_ports GTREFCLK_Si5324_N_IN]

#set_property PACKAGE_PIN R22 [get_ports si5324_resetn]
#set_property IOSTANDARD LVCMOS18 [get_ports si5324_resetn]
set_property PACKAGE_PIN D35 [get_ports {SI5345_RSTN[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_RSTN[0]}]

set_property PACKAGE_PIN P20 [get_ports {i2cmux_rst[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {i2cmux_rst[0]}]

# Test Points connected to the Debug Port
# TP1_P - J3
set_property PACKAGE_PIN AU17 [get_ports {SmaOut[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SmaOut[0]}]
# TP1_N - J4
set_property PACKAGE_PIN AU16 [get_ports {SmaOut[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SmaOut[1]}]
# TP2_P - J5
set_property PACKAGE_PIN P34 [get_ports {SmaOut[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SmaOut[2]}]
# TP2_N - J9
set_property PACKAGE_PIN P35 [get_ports {SmaOut[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SmaOut[3]}]

set_property PACKAGE_PIN B34 [get_ports {uC_reset_N[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {uC_reset_N[0]}]


## some more constraints
set_property PACKAGE_PIN AP21 [get_ports {NT_PORTSEL[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {NT_PORTSEL[0]}]
set_property PACKAGE_PIN AN23 [get_ports {NT_PORTSEL[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {NT_PORTSEL[1]}]
set_property PACKAGE_PIN AP23 [get_ports {NT_PORTSEL[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {NT_PORTSEL[2]}]

set_property PACKAGE_PIN AK22 [get_ports {TESTMODE[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {TESTMODE[0]}]
set_property PACKAGE_PIN AK21 [get_ports {TESTMODE[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {TESTMODE[1]}]
set_property PACKAGE_PIN AJ23 [get_ports {TESTMODE[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {TESTMODE[2]}]

set_property PACKAGE_PIN AK23 [get_ports {UPSTREAM_PORTSEL[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {UPSTREAM_PORTSEL[0]}]
set_property PACKAGE_PIN AM21 [get_ports {UPSTREAM_PORTSEL[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {UPSTREAM_PORTSEL[1]}]
set_property PACKAGE_PIN AM20 [get_ports {UPSTREAM_PORTSEL[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {UPSTREAM_PORTSEL[2]}]

set_property PACKAGE_PIN AL23 [get_ports {STN0_PORTCFG[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {STN0_PORTCFG[0]}]
set_property PACKAGE_PIN AL22 [get_ports {STN0_PORTCFG[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {STN0_PORTCFG[1]}]

set_property PACKAGE_PIN AJ20 [get_ports {STN1_PORTCFG[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {STN1_PORTCFG[0]}]
set_property PACKAGE_PIN AK20 [get_ports {STN1_PORTCFG[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {STN1_PORTCFG[1]}]

## KCU settings
#set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets mmcm0_i_1_n_2]
set_property BITSTREAM.CONFIG.OVERTEMPSHUTDOWN ENABLE [current_design]
#set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets -hierarchical -filter { NAME =~ "mmcm0*" } ]
#PBLOCKS, put channels 0..7 in the lower SRL + PCIe0, put channels 8..15 in the upper SRL + PCIe1

#BPI Flash pins
set_property PACKAGE_PIN AR27 [get_ports {flash_a[0]}]
set_property PACKAGE_PIN AT27 [get_ports {flash_a[1]}]
set_property PACKAGE_PIN AP25 [get_ports {flash_a[2]}]
set_property PACKAGE_PIN AR25 [get_ports {flash_a[3]}]
set_property PACKAGE_PIN AU26 [get_ports {flash_a[4]}]
set_property PACKAGE_PIN AU27 [get_ports {flash_a[5]}]
set_property PACKAGE_PIN AT25 [get_ports {flash_a[6]}]
set_property PACKAGE_PIN AU25 [get_ports {flash_a[7]}]
set_property PACKAGE_PIN AW25 [get_ports {flash_a[8]}]
set_property PACKAGE_PIN AW26 [get_ports {flash_a[9]}]
set_property PACKAGE_PIN AV26 [get_ports {flash_a[10]}]
set_property PACKAGE_PIN AV27 [get_ports {flash_a[11]}]
set_property PACKAGE_PIN AY26 [get_ports {flash_a[12]}]
set_property PACKAGE_PIN BA27 [get_ports {flash_a[13]}]
set_property PACKAGE_PIN AW28 [get_ports {flash_a[14]}]
set_property PACKAGE_PIN AY28 [get_ports {flash_a[15]}]
set_property PACKAGE_PIN AY25 [get_ports {flash_a[16]}]
set_property PACKAGE_PIN BA25 [get_ports {flash_a[17]}]
set_property PACKAGE_PIN AY27 [get_ports {flash_a[18]}]
set_property PACKAGE_PIN BA28 [get_ports {flash_a[19]}]
set_property PACKAGE_PIN BD25 [get_ports {flash_a[20]}]
set_property PACKAGE_PIN BD26 [get_ports {flash_a[21]}]
set_property PACKAGE_PIN BB26 [get_ports {flash_a[22]}]
set_property PACKAGE_PIN BB27 [get_ports {flash_a[23]}]
set_property PACKAGE_PIN BB24 [get_ports {flash_a[24]}]
#set_property PACKAGE_PIN BC26 [get_ports {flash_a[25]}]
#set_property PACKAGE_PIN BC27 [get_ports {flash_a[26]}]
set_property PACKAGE_PIN BC26 [get_ports {flash_a_msb[0]}]
set_property PACKAGE_PIN BC27 [get_ports {flash_a_msb[1]}]


set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[6]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[8]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[9]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[10]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[11]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[12]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[13]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[14]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[15]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[16]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[17]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[18]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[19]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[20]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[21]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[22]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[23]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[24]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[25]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {flash_a[26]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a_msb[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_a_msb[1]}]


set_property IOSTANDARD LVCMOS25 [get_ports {flash_d[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_d[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_d[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_d[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_d[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_d[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_d[6]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_d[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_d[8]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_d[9]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_d[10]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_d[11]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_d[12]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_d[13]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_d[14]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_d[15]}]

set_property PACKAGE_PIN AK25 [get_ports {flash_d[0]}]
set_property PACKAGE_PIN BB25 [get_ports {flash_d[1]}]
set_property PACKAGE_PIN BC28 [get_ports {flash_d[2]}]
set_property PACKAGE_PIN BD28 [get_ports {flash_d[3]}]
set_property PACKAGE_PIN AL27 [get_ports {flash_d[4]}]
set_property PACKAGE_PIN AM27 [get_ports {flash_d[5]}]
set_property PACKAGE_PIN AL24 [get_ports {flash_d[6]}]
set_property PACKAGE_PIN AM24 [get_ports {flash_d[7]}]
set_property PACKAGE_PIN AM26 [get_ports {flash_d[8]}]
set_property PACKAGE_PIN AN26 [get_ports {flash_d[9]}]
set_property PACKAGE_PIN AL25 [get_ports {flash_d[10]}]
set_property PACKAGE_PIN AM25 [get_ports {flash_d[11]}]
set_property PACKAGE_PIN AP26 [get_ports {flash_d[12]}]
set_property PACKAGE_PIN AR26 [get_ports {flash_d[13]}]
set_property PACKAGE_PIN AN24 [get_ports {flash_d[14]}]
set_property PACKAGE_PIN AP24 [get_ports {flash_d[15]}]

#set_property IOSTANDARD LVCMOS25 [get_ports clk]
#set_property PACKAGE_PIN AK26 [get_ports clk]

set_property IOSTANDARD LVCMOS25 [get_ports {flash_re[0]}]
set_property PACKAGE_PIN BC24 [get_ports {flash_re[0]}]


set_property PACKAGE_PIN BD24 [get_ports {flash_we[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_we[0]}]

set_property PACKAGE_PIN AT24 [get_ports {flash_adv[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {flash_adv[0]}]

set_property IOSTANDARD LVCMOS25 [get_ports {flash_ce[0]}]
set_property PACKAGE_PIN BA24 [get_ports {flash_ce[0]}]

set_property IOSTANDARD LVCMOS18 [get_ports {flash_SEL[0]}]
set_property PACKAGE_PIN AY18 [get_ports {flash_SEL[0]}]

#set_property IOSTANDARD LVCMOS18 [get_ports PEX_SEL1]
#set_property PACKAGE_PIN AP20 [get_ports PEX_SEL1]

#set_property IOSTANDARD LVCMOS18 [get_ports PEX_SEL0]
#set_property PACKAGE_PIN AR20 [get_ports PEX_SEL0]


set_property IOSTANDARD LVCMOS25 [get_ports {flash_cclk[0]}]
set_property PACKAGE_PIN AJ25 [get_ports {flash_cclk[0]}]

set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets emcclk_IBUF[0]_inst/O]

set_property IOSTANDARD LVCMOS18 [get_ports {TACH[0]}]
set_property PACKAGE_PIN AU36 [get_ports {TACH[0]}]


#bank 126, 127, 128 use clk from bank 127 Refclk1
set_property PACKAGE_PIN AH38 [get_ports {GTREFCLK_N_IN[0]}]
set_property PACKAGE_PIN AH37 [get_ports {GTREFCLK_P_IN[0]}]
#bank 131, 132, 133 use clk from bank 132 Refclk1
set_property PACKAGE_PIN T37 [get_ports {GTREFCLK_P_IN[1]}]
set_property PACKAGE_PIN T38 [get_ports {GTREFCLK_N_IN[1]}]

#bank 231, 232, 233,use clk from bank 232 Refclk1, Q4_CLK0_GTREFCLK_PAD_N_IN
set_property -quiet PACKAGE_PIN M8 [get_ports {GTREFCLK_P_IN[2]}]
set_property -quiet PACKAGE_PIN M7 [get_ports {GTREFCLK_N_IN[2]}]
#bank 228 use clk from bank 228 Refclk0 Q5_CLK0_GTREFCLK_PAD_N_IN
set_property -quiet PACKAGE_PIN AF8 [get_ports {GTREFCLK_P_IN[3]}]
set_property -quiet PACKAGE_PIN AF7 [get_ports {GTREFCLK_N_IN[3]}]

#bank 224, 225 use clk from bank 225 Refclk0 Q6_CLK0_GTREFCLK_PAD_P_IN
set_property -quiet PACKAGE_PIN AP8 [get_ports {GTREFCLK_P_IN[4]}]
set_property -quiet PACKAGE_PIN AP7 [get_ports {GTREFCLK_N_IN[4]}]

#GTREFCLK0_LTITTC_P/N is the same pin as GTREFCLK_P_IN[6]
set_property -quiet PACKAGE_PIN N6 [get_ports {GTREFCLK0_LTITTC_P[0]}]
set_property -quiet PACKAGE_PIN N5 [get_ports {GTREFCLK0_LTITTC_N[0]}]
create_clock -name GTREFCLK0_LTITTC -period 4.158 [get_ports {GTREFCLK0_LTITTC_P[0]}]

set_property -quiet PACKAGE_PIN AL40 [get_ports {LMK_N[0]}]
set_property -quiet PACKAGE_PIN AL39 [get_ports {LMK_P[0]}]
set_property -quiet PACKAGE_PIN AF38 [get_ports {LMK_N[1]}]
set_property -quiet PACKAGE_PIN AF37 [get_ports {LMK_P[1]}]
set_property -quiet PACKAGE_PIN Y38 [get_ports {LMK_N[2]}]
set_property -quiet PACKAGE_PIN Y37 [get_ports {LMK_P[2]}]
set_property -quiet PACKAGE_PIN R40 [get_ports {LMK_N[3]}]
set_property -quiet PACKAGE_PIN R39 [get_ports {LMK_P[3]}]
set_property -quiet PACKAGE_PIN AD7 [get_ports {LMK_N[4]}]
set_property -quiet PACKAGE_PIN AD8 [get_ports {LMK_P[4]}]
set_property -quiet PACKAGE_PIN AN5 [get_ports {LMK_N[5]}]
set_property -quiet PACKAGE_PIN AN6 [get_ports {LMK_P[5]}]
#GTREFCLK1_LTITTC_P/N is the same pin as LMK_P/N[6], either of them is used in the design
set_property -quiet PACKAGE_PIN P7 [get_ports {GTREFCLK1_LTITTC_N[0]}]
set_property -quiet PACKAGE_PIN P8 [get_ports {GTREFCLK1_LTITTC_P[0]}]
create_clock -name GTREFCLK1_LTITTC -period 4.158 [get_ports {GTREFCLK1_LTITTC_P[0]}]

create_generated_clock -name clk40_ltittc -source [get_pins -hierarchical -filter {NAME =~ "*.GTH_LTITTCLINK_TOP_INST/BUFGCE_DIV_inst/I"}] -divide_by 6 [get_pins -hierarchical -filter {NAME =~ "*.GTH_LTITTCLINK_TOP_INST/BUFGCE_DIV_inst/I"}]

#single FF sync from rxusrclk_lti (240MHz to clk40_ltittc (BUFG_DIV6)
set_max_delay -from [get_pins TTCLTI.ltittc0/ltittc_decoder/data_out_i_reg/C] -to [get_pins TTCLTI.ltittc0/ltittc_decoder/data_out_reg/D] 4.15

#set_property -quiet PACKAGE_PIN P7 [get_ports {LMK_N[6]}]
#set_property -quiet PACKAGE_PIN P8 [get_ports {LMK_P[6]}]
#set_property -quiet PACKAGE_PIN L5 [get_ports {LMK_N[7]}]
#set_property -quiet PACKAGE_PIN L6 [get_ports {LMK_P[7]}]

###############################################################################
# End
###############################################################################

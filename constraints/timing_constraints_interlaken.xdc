#create 40 MHz TTC clock
create_clock -name clk_ttc_40 -period 24.95 [get_pins */ttc_dec/from_cdr_to_AandB/clock_iter/O]

create_clock -period 10.000 -name sys_clk0_p -waveform {0.000 5.000} [get_ports {sys_clk_p[0]}]
create_clock -quiet -period 10.000 -name sys_clk1_p -waveform {0.000 5.000} [get_ports {sys_clk_p[1]}]

set_property -quiet CLOCK_DEDICATED_ROUTE FALSE [get_nets emcclk_IBUF[0]_inst/O]

#We need this line when we move to the second SRL
create_clock -name GTHREFCLK_0 -period 6.4 [get_ports {GTREFCLK_P_IN[0]}]
create_clock -quiet -name GTHREFCLK_1 -period 6.4 [get_ports {GTREFCLK_P_IN[1]}]
create_clock -quiet -name GTHREFCLK_2 -period 6.4 [get_ports {GTREFCLK_P_IN[2]}]
create_clock -quiet -name GTHREFCLK_3 -period 6.4 [get_ports {GTREFCLK_P_IN[3]}]
create_clock -quiet -name GTHREFCLK_4 -period 6.4 [get_ports {GTREFCLK_P_IN[4]}]

create_clock -quiet -name i2c_clock_pex -period 2500 [get_pins hk0/g_711_712.pex_init0/data_clk_reg/Q]

#Register map to 400kHz I2C clock
set_max_delay -datapath_only -from [get_clocks *] -to [get_clocks clk400] 100.000
set_max_delay -datapath_only -from [get_clocks *] -to [get_clocks i2c_clock_pex] 100.000

#Register map control / monitor
set_max_delay -datapath_only -from [get_clocks clk_out25*] -to [get_clocks *] 40.000
set_max_delay -datapath_only -from [get_clocks *] -to [get_clocks clk_out25*] 40.000

#switchable output clock can switch at any time
set_false_path -quiet -from [get_clocks clk_adn_160*] -to [get_clocks clk160_clk_wiz_40_0*]
set_max_delay -quiet -datapath_only -from [get_clocks clk_adn_160*] -to [get_clocks clk40_clk_wiz_40_0*] 6.238
set_max_delay -quiet -datapath_only -from [get_clocks clk_adn_160*] -to [get_clocks clk_ttc_40*] 6.238
set_max_delay -quiet -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk_ttc_40*] 24.95
set_max_delay -quiet -datapath_only -from [get_clocks clk_ttc_40*] -to [get_clocks clk40_clk_wiz_40_0*] 24.95

set_false_path -from [get_pins {g_endpoints[*].sync_fromhost_aresetn/syncstages_ff_reg[1]/C}] -to [get_clocks clkout1_primitive_8*]
set_false_path -from [get_clocks clkout1_primitive_10*] -to [get_clocks clkout1_primitive_8*]

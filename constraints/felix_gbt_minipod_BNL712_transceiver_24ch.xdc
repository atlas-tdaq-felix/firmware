##GBT Transceiver pins
#bank 128
set_property PACKAGE_PIN AC43 [get_ports {RX_P[0]}]
set_property PACKAGE_PIN AF41 [get_ports {RX_P[3]}]
set_property PACKAGE_PIN AE43 [get_ports {RX_P[2]}]
set_property PACKAGE_PIN AG43 [get_ports {RX_P[1]}]
#bank 127
set_property PACKAGE_PIN AH41 [get_ports {RX_P[4]}]
set_property PACKAGE_PIN AJ43 [get_ports {RX_P[5]}]
set_property PACKAGE_PIN AN43 [get_ports {RX_P[6]}]
set_property PACKAGE_PIN AL43 [get_ports {RX_P[7]}]
#bank 126
set_property PACKAGE_PIN AU43 [get_ports {RX_P[8]}]
set_property PACKAGE_PIN AR43 [get_ports {RX_P[9]}]
set_property PACKAGE_PIN BA43 [get_ports {RX_P[10]}]
set_property PACKAGE_PIN AW43 [get_ports {RX_P[11]}]
#bank 133
set_property PACKAGE_PIN C43  [get_ports {RX_P[12]}]
set_property PACKAGE_PIN E43  [get_ports {RX_P[13]}]
set_property PACKAGE_PIN G43  [get_ports {RX_P[14]}]
set_property PACKAGE_PIN J43  [get_ports {RX_P[15]}]
#bank 132
set_property PACKAGE_PIN L43  [get_ports {RX_P[16]}]
set_property PACKAGE_PIN N43  [get_ports {RX_P[17]}]
set_property PACKAGE_PIN R43  [get_ports {RX_P[18]}]
set_property PACKAGE_PIN U43  [get_ports {RX_P[19]}]
#bank 131
set_property PACKAGE_PIN W43  [get_ports {RX_P[20]}]
set_property PACKAGE_PIN V41  [get_ports {RX_P[21]}]
set_property PACKAGE_PIN AA43 [get_ports {RX_P[23]}]
set_property PACKAGE_PIN AB41 [get_ports {RX_P[22]}]
#MGT0,1,2,4 bank 128, use clk from bank 127 AH37
#MGT3,5,6,7 bank 127, use clk from bank 127 AH37
#MGT8,9,10,11 bank 126, use clk from bank 127 AH37
#MGT12,13,14,15 bank 133, use clk from bank 132 T37
#MGT16,17,18,19 bank 132, use clk from bank 132 T37
#MGT20,21,22,23 bank 131, use clk from bank 132 T37

#bank 231
set_property -quiet PACKAGE_PIN J2 [get_ports {RX_P_LTITTC[0]}]
#bank 231
set_property -quiet PACKAGE_PIN J2 [get_ports {RX_P[24]}]
set_property -quiet PACKAGE_PIN H4 [get_ports {RX_P[25]}]
set_property -quiet PACKAGE_PIN G2 [get_ports {RX_P[26]}]
set_property -quiet PACKAGE_PIN F4 [get_ports {RX_P[27]}]
#bank 232 
set_property -quiet PACKAGE_PIN C2 [get_ports {RX_P[28]}]
set_property -quiet PACKAGE_PIN E2 [get_ports {RX_P[29]}]
set_property -quiet PACKAGE_PIN B4 [get_ports {RX_P[30]}]
set_property -quiet PACKAGE_PIN D4 [get_ports {RX_P[31]}]
#bank 233 
set_property -quiet PACKAGE_PIN C10 [get_ports {RX_P[32]}]
set_property -quiet PACKAGE_PIN A6  [get_ports {RX_P[33]}]
set_property -quiet PACKAGE_PIN B12 [get_ports {RX_P[34]}]
set_property -quiet PACKAGE_PIN E10 [get_ports {RX_P[35]}]
#bank 228 
set_property -quiet PACKAGE_PIN AG2  [get_ports {RX_P[36]}]
set_property -quiet PACKAGE_PIN AF4  [get_ports {RX_P[37]}]
set_property -quiet PACKAGE_PIN AC2  [get_ports {RX_P[38]}]
set_property -quiet PACKAGE_PIN AE2  [get_ports {RX_P[39]}]
#bank 229 
set_property -quiet PACKAGE_PIN W2  [get_ports {RX_P[40]}]
set_property -quiet PACKAGE_PIN AB4  [get_ports {RX_P[41]}]
set_property -quiet PACKAGE_PIN V4  [get_ports {RX_P[42]}]
set_property -quiet PACKAGE_PIN AA2  [get_ports {RX_P[43]}]
#bank 230 
set_property -quiet PACKAGE_PIN N2  [get_ports {RX_P[44]}]
set_property -quiet PACKAGE_PIN U2  [get_ports {RX_P[45]}]
set_property -quiet PACKAGE_PIN L2 [get_ports {RX_P[46]}]
set_property -quiet PACKAGE_PIN R2 [get_ports {RX_P[47]}]

#MGT24,25,26,27 bank 231, use clk from bank 231 R6 (or 231 P8, or 229 Y8, or 233 L6)
#MGT28,29,30,31 bank 232, use clk from bank 231 R6 (or 231 P8, or 233 L6)
#MGT32,33,34,35 bank 233, use clk from bank 231 R6 (or 233 P8, or 233 L6)
#MGT36,37,38,39 bank 228, use clk from bank 228 AF8
#MGT40,41,42,43 bank 229, use clk from bank 231 R6 (or 231 P8, or 229 Y8)
#MGT44,45,46,47 bank 230, use clk from bank 231 R6 (or 231 P8, or 229 Y8)

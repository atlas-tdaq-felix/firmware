

##############################################
##########      Configuration       ##########
##############################################
set_property CONFIG_VOLTAGE 1.8 [current_design]
set_property CONFIG_MODE SPIx4 [current_design]
# Bitstream configuration settings
set_property BITSTREAM.CONFIG.USR_ACCESS TIMESTAMP [current_design]
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
# Must set to "NO" if loading from backup flash partition
set_property BITSTREAM.CONFIG.SPI_32BIT_ADDR YES [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 85.0 [current_design]
set_property BITSTREAM.CONFIG.SPI_FALL_EDGE YES [current_design]



###############################################
###########           LEDs           ##########
###############################################
# Active Low Led 0
set_property IOSTANDARD LVCMOS18 [get_ports {leds[0]}]
set_property PACKAGE_PIN BH24 [get_ports {leds[0]}]
# Active Low Led 1
set_property IOSTANDARD LVCMOS18 [get_ports {leds[1]}]
set_property PACKAGE_PIN BG24 [get_ports {leds[1]}]
# Active Low Led 2
set_property IOSTANDARD LVCMOS18 [get_ports {leds[2]}]
set_property PACKAGE_PIN BG25 [get_ports {leds[2]}]
# Active Low Led 3
set_property IOSTANDARD LVCMOS18 [get_ports {leds[3]}]
set_property PACKAGE_PIN BF25 [get_ports {leds[3]}]

set_property IOSTANDARD LVCMOS18 [get_ports {leds[4]}]
set_property PACKAGE_PIN BF26 [get_ports {leds[4]}]
# Active Low Led 1
set_property IOSTANDARD LVCMOS18 [get_ports {leds[5]}]
set_property PACKAGE_PIN BF27 [get_ports {leds[5]}]
# Active Low Led 2
set_property IOSTANDARD LVCMOS18 [get_ports {leds[6]}]
set_property PACKAGE_PIN BG27 [get_ports {leds[6]}]
# Active Low Led 3
set_property IOSTANDARD LVCMOS18 [get_ports {leds[7]}]
set_property PACKAGE_PIN BG28 [get_ports {leds[7]}]

##############################################
##########           PCIe           ##########
##############################################
#set_property PACKAGE_PIN AY23 [get_ports progclk_b1_p]
#set_property PACKAGE_PIN BA23 [get_ports progclk_b1_n]
#set_property IOSTANDARD DIFF_SSTL18_I [get_ports progclk_b1_p]
# PCIE Active Low Reset

set_property PACKAGE_PIN BF41 [get_ports sys_reset_n]
set_property IOSTANDARD LVCMOS12 [get_ports sys_reset_n]
set_property PULLUP true [get_ports sys_reset_n]
set_false_path -from [get_ports sys_reset_n]
# PCIE Reference Clock 0
 #MGTREFCLK0N_227
set_property PACKAGE_PIN AL14 [get_ports {sys_clk_n[0]}]
set_property PACKAGE_PIN AL15 [get_ports {sys_clk_p[0]}]

set_property PACKAGE_PIN AR14 [get_ports {sys_clk_n[1]}]
set_property PACKAGE_PIN AR15 [get_ports {sys_clk_p[1]}]

#already in timing_constraints_fullmode_ku.xdc create_clock -period 10.000 -name sys_clk0 [get_ports {sys_clk_p[0]}]
#already in timing_constraints_fullmode_ku.xdc create_clock -period 10.000 -name sys_clk1 [get_ports {sys_clk_p[1]}]

set_property PACKAGE_PIN AL11 [get_ports {pcie_txp[0]}]
set_property PACKAGE_PIN AL10 [get_ports {pcie_txn[0]}]
set_property PACKAGE_PIN AL2  [get_ports {pcie_rxp[0]}]
set_property PACKAGE_PIN AL1  [get_ports {pcie_rxn[0]}]

set_property PACKAGE_PIN AM9  [get_ports {pcie_txp[1]}]
set_property PACKAGE_PIN AM8  [get_ports {pcie_txn[1]}]
set_property PACKAGE_PIN AM4  [get_ports {pcie_rxp[1]}]
set_property PACKAGE_PIN AM3  [get_ports {pcie_rxn[1]}]

set_property PACKAGE_PIN AN11 [get_ports {pcie_txp[2]}]
set_property PACKAGE_PIN AN10 [get_ports {pcie_txn[2]}]
set_property PACKAGE_PIN AN6  [get_ports {pcie_rxp[2]}]
set_property PACKAGE_PIN AN5  [get_ports {pcie_rxn[2]}]

set_property PACKAGE_PIN AP9  [get_ports {pcie_txp[3]}]
set_property PACKAGE_PIN AP8  [get_ports {pcie_txn[3]}]
set_property PACKAGE_PIN AN2  [get_ports {pcie_rxp[3]}]
set_property PACKAGE_PIN AN1  [get_ports {pcie_rxn[3]}]

set_property PACKAGE_PIN AR11 [get_ports {pcie_txp[4]}]
set_property PACKAGE_PIN AR10 [get_ports {pcie_txn[4]}]
set_property PACKAGE_PIN AP4  [get_ports {pcie_rxp[4]}]
set_property PACKAGE_PIN AP3  [get_ports {pcie_rxn[4]}]

set_property PACKAGE_PIN AR7  [get_ports {pcie_txp[5]}]
set_property PACKAGE_PIN AR6  [get_ports {pcie_txn[5]}]
set_property PACKAGE_PIN AR2  [get_ports {pcie_rxp[5]}]
set_property PACKAGE_PIN AR1  [get_ports {pcie_rxn[5]}]

set_property PACKAGE_PIN AT9  [get_ports {pcie_txp[6]}]
set_property PACKAGE_PIN AT8  [get_ports {pcie_txn[6]}]
set_property PACKAGE_PIN AT4  [get_ports {pcie_rxp[6]}]
set_property PACKAGE_PIN AT3  [get_ports {pcie_rxn[6]}]

set_property PACKAGE_PIN AU11 [get_ports {pcie_txp[7]}]
set_property PACKAGE_PIN AU10 [get_ports {pcie_txn[7]}]
set_property PACKAGE_PIN AU2  [get_ports {pcie_rxp[7]}]
set_property PACKAGE_PIN AU1  [get_ports {pcie_rxn[7]}]

set_property PACKAGE_PIN AU7  [get_ports {pcie_txp[8]}]
set_property PACKAGE_PIN AU6  [get_ports {pcie_txn[8]}]
set_property PACKAGE_PIN AV4  [get_ports {pcie_rxp[8]}]
set_property PACKAGE_PIN AV3  [get_ports {pcie_rxn[8]}]

set_property PACKAGE_PIN AV9  [get_ports {pcie_txp[9]}]
set_property PACKAGE_PIN AV8  [get_ports {pcie_txn[9]}]
set_property PACKAGE_PIN AW6  [get_ports {pcie_rxp[9]}]
set_property PACKAGE_PIN AW5  [get_ports {pcie_rxn[9]}]

set_property PACKAGE_PIN AW11 [get_ports {pcie_txp[10]}]
set_property PACKAGE_PIN AW10 [get_ports {pcie_txn[10]}]
set_property PACKAGE_PIN AW2  [get_ports {pcie_rxp[10]}]
set_property PACKAGE_PIN AW1  [get_ports {pcie_rxn[10]}]

set_property PACKAGE_PIN AY9  [get_ports {pcie_txp[11]}]
set_property PACKAGE_PIN AY8  [get_ports {pcie_txn[11]}]
set_property PACKAGE_PIN AY4  [get_ports {pcie_rxp[11]}]
set_property PACKAGE_PIN AY3  [get_ports {pcie_rxn[11]}]

set_property PACKAGE_PIN BA11 [get_ports {pcie_txp[12]}]
set_property PACKAGE_PIN BA10 [get_ports {pcie_txn[12]}]
set_property PACKAGE_PIN BA6  [get_ports {pcie_rxp[12]}]
set_property PACKAGE_PIN BA5  [get_ports {pcie_rxn[12]}]

set_property PACKAGE_PIN BB9  [get_ports {pcie_txp[13]}]
set_property PACKAGE_PIN BB8  [get_ports {pcie_txn[13]}]
set_property PACKAGE_PIN BA2  [get_ports {pcie_rxp[13]}]
set_property PACKAGE_PIN BA1  [get_ports {pcie_rxn[13]}]

set_property PACKAGE_PIN BC11 [get_ports {pcie_txp[14]}]
set_property PACKAGE_PIN BC10 [get_ports {pcie_txn[14]}]
set_property PACKAGE_PIN BB4  [get_ports {pcie_rxp[14]}]
set_property PACKAGE_PIN BB3  [get_ports {pcie_rxn[14]}]

set_property PACKAGE_PIN BC7  [get_ports {pcie_txp[15]}]
set_property PACKAGE_PIN BC6  [get_ports {pcie_txn[15]}]
set_property PACKAGE_PIN BC2  [get_ports {pcie_rxp[15]}]
set_property PACKAGE_PIN BC1  [get_ports {pcie_rxn[15]}]

#DDR4_CLK_100MHz
set_property PACKAGE_PIN BH51 [get_ports app_clk_in_p] 
set_property PACKAGE_PIN BJ51 [get_ports app_clk_in_n] 
set_property IOSTANDARD DIFF_HSTL_I_12 [get_ports app_clk_in_p]
set_property IOSTANDARD DIFF_HSTL_I_12 [get_ports app_clk_in_n]

#ENET_CLKOUT
set_property PACKAGE_PIN BJ4 [get_ports {emcclk[0]}]
set_property IOSTANDARD LVCMOS12 [get_ports {emcclk[0]}]
#Si5328 pins
##Ckin1_P, QSFP1_RECCLK_P
#set_property PACKAGE_PIN BH26 [get_ports clk40_ttc_ref_out_p]
#set_property IOSTANDARD LVDS  [get_ports clk40_ttc_ref_out_p]

set_property PACKAGE_PIN H24 [get_ports {clk40_ttc_ref_out_p[0]}]
set_property IOSTANDARD LVDS [get_ports {clk40_ttc_ref_out_p[0]}]
#Ckin2_P, QSFP2_RECCLK_P
#set_property PACKAGE_PIN BJ26 [get_ports clk_adn_160_out_p]
#set_property IOSTANDARD LVDS  [get_ports clk_adn_160_out_p]
##Ckin2_N, QSFP2_RECCLK_N
#set_property PACKAGE_PIN BK25 [get_ports clk_adn_160_out_n]
#set_property IOSTANDARD LVDS  [get_ports clk_adn_160_out_n]
##Ckout1_P, SI5328_CLOCK1_C_P
##Ckout1_N, SI5328_CLOCK1_C_N
#set_property PACKAGE_PIN R41 [get_ports {GTREFCLK_N_IN[0]}]
##Ckout2_P, SI5328_CLOCK2_C_P
##Ckout2_N, SI5328_CLOCK2_C_N
#set_property PACKAGE_PIN W41 [get_ports {GTREFCLK_N_IN[1]}]
##Si570 QSFP4, bank 131 MGTREFCLK0
#set_property PACKAGE_PIN AB43 [get_ports {GTREFCLK_N_IN[2]}]
#Bank 124 refclk0, FMC+ GBTCLK0
set_property PACKAGE_PIN AV43 [get_ports {GTREFCLK_N_IN[0]}]
#Bank 125 refclk0, FMC+ GBTCLK1
set_property PACKAGE_PIN AR41 [get_ports {GTREFCLK_N_IN[1]}]
#Bank 126 refclk0, FMC+ GBTCLK2
set_property PACKAGE_PIN AN41 [get_ports {GTREFCLK_N_IN[2]}]
#Bank 127 refclk0, FMC+ GBTCLK3
set_property PACKAGE_PIN AL41 [get_ports {GTREFCLK_N_IN[3]}]
#Bank 128 refclk0, FMC+ GBTCLK4
set_property PACKAGE_PIN AJ41 [get_ports {GTREFCLK_N_IN[4]}]
#Bank 129 refclk0, FMC+ GBTCLK5
set_property PACKAGE_PIN AG41 [get_ports {GTREFCLK_N_IN[5]}]


#Si5328_SDA, I2C0 mux TCA9548, Port 1 -> I2C0_SDA
#Si5328_SCL, I2C0 mux TCA9548, Port 1 -> I2C0_SCL

#Si5328_RST_B
set_property PACKAGE_PIN BN14 [get_ports si5324_resetn]
set_property IOSTANDARD LVCMOS12 [get_ports si5324_resetn]
#SI5328_INT_ALM

#PL_I2C0_SDA_LS
set_property PACKAGE_PIN BL28 [get_ports SDA]
set_property IOSTANDARD LVCMOS18 [get_ports SDA]
#PL_I2C0_SCL_LS
set_property PACKAGE_PIN BM27 [get_ports SCL]
set_property IOSTANDARD LVCMOS18 [get_ports SCL]

##qsfp1 bank 135 (refclk 0 from bank 134)
#set_property PACKAGE_PIN G53  [get_ports {RX_P[0]}]
#set_property PACKAGE_PIN F51  [get_ports {RX_P[1]}]
#set_property PACKAGE_PIN E53  [get_ports {RX_P[2]}]
#set_property PACKAGE_PIN D51  [get_ports {RX_P[3]}]
##qsfp2 bank 134  (refclk 0 from bank 134)
#set_property PACKAGE_PIN L53  [get_ports {RX_P[4]}]
#set_property PACKAGE_PIN K51  [get_ports {RX_P[5]}]
#set_property PACKAGE_PIN J53  [get_ports {RX_P[6]}]
#set_property PACKAGE_PIN H51  [get_ports {RX_P[7]}]
##qsfp3 Bank 132 (refclk1 from bank 132)
#set_property PACKAGE_PIN U53  [get_ports {RX_P[8]}]
#set_property PACKAGE_PIN U49  [get_ports {RX_P[9]}]
#set_property PACKAGE_PIN T51  [get_ports {RX_P[10]}]
#set_property PACKAGE_PIN R53  [get_ports {RX_P[11]}]
##qsfp4 Bank 131 (only Si570 or SMA refclk possible)
#set_property PACKAGE_PIN AA53 [get_ports {RX_P[12]}]
#set_property PACKAGE_PIN Y51  [get_ports {RX_P[13]}]
#set_property PACKAGE_PIN W53  [get_ports {RX_P[14]}]
#set_property PACKAGE_PIN V51  [get_ports {RX_P[15]}]
#FMC+ DP0..3 bank 124 (refclk 0 from bank 124)
set_property PACKAGE_PIN BC53  [get_ports {RX_P[0]}]
set_property PACKAGE_PIN BB51  [get_ports {RX_P[1]}]
set_property PACKAGE_PIN BA53  [get_ports {RX_P[2]}]
set_property PACKAGE_PIN BA49  [get_ports {RX_P[3]}]
#FMC+ DP4..7 bank 125 (refclk 0 from bank 125)
set_property PACKAGE_PIN AY51  [get_ports {RX_P[4]}]
set_property PACKAGE_PIN AW53  [get_ports {RX_P[5]}]
set_property PACKAGE_PIN AW49  [get_ports {RX_P[6]}]
set_property PACKAGE_PIN AV51  [get_ports {RX_P[7]}]
#FMC+ DP8..11 bank 126 (refclk0 from bank 126)
set_property PACKAGE_PIN AU53  [get_ports {RX_P[8]}]
set_property PACKAGE_PIN AT51  [get_ports {RX_P[9]}]
set_property PACKAGE_PIN AR53  [get_ports {RX_P[10]}]
set_property PACKAGE_PIN AP51  [get_ports {RX_P[11]}]
#FMC+ DP12..15 Bank 127 (refclk0 from bank 127)
set_property PACKAGE_PIN AN53 [get_ports {RX_P[12]}]
set_property PACKAGE_PIN AN49 [get_ports {RX_P[13]}]
set_property PACKAGE_PIN AM51 [get_ports {RX_P[14]}]
set_property PACKAGE_PIN AL53 [get_ports {RX_P[15]}]
#FMCP Bank 128, DP16-19 (refclk0 from bank 128)
set_property PACKAGE_PIN AL49 [get_ports {RX_P[16]}]
set_property PACKAGE_PIN AK51 [get_ports {RX_P[17]}]
set_property PACKAGE_PIN AJ53 [get_ports {RX_P[18]}]
set_property PACKAGE_PIN AH51 [get_ports {RX_P[19]}]
#FMCP Bank 129, DP20-23 (refclk0 from bank 129)
set_property PACKAGE_PIN AG53 [get_ports {RX_P[20]}]
set_property PACKAGE_PIN AF51 [get_ports {RX_P[21]}]
set_property PACKAGE_PIN AE53 [get_ports {RX_P[22]}]
set_property PACKAGE_PIN AE49 [get_ports {RX_P[23]}]


set_property PACKAGE_PIN BN25 [get_ports {opto_inhibit[0]}]
set_property PACKAGE_PIN BN6  [get_ports {opto_inhibit[1]}]
set_property PACKAGE_PIN BL6  [get_ports {opto_inhibit[2]}]
set_property PACKAGE_PIN BK24 [get_ports {opto_inhibit[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {opto_inhibit[0]}]
set_property IOSTANDARD LVCMOS12 [get_ports {opto_inhibit[1]}]
set_property IOSTANDARD LVCMOS12 [get_ports {opto_inhibit[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {opto_inhibit[3]}]
set_property PACKAGE_PIN BM25 [get_ports {OPTO_LOS[0]}]
set_property PACKAGE_PIN BN7  [get_ports {OPTO_LOS[1]}]
set_property PACKAGE_PIN BM7  [get_ports {OPTO_LOS[2]}]
set_property PACKAGE_PIN BL22 [get_ports {OPTO_LOS[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {OPTO_LOS[0]}]
set_property IOSTANDARD LVCMOS12 [get_ports {OPTO_LOS[1]}]
set_property IOSTANDARD LVCMOS12 [get_ports {OPTO_LOS[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {OPTO_LOS[3]}]

#J13, SMA_CLK_OUTPUT_N
set_property PACKAGE_PIN BL25 [get_ports BUSY_OUT]
set_property IOSTANDARD LVCMOS18 [get_ports BUSY_OUT]

#FMCP_HSPC_LA18_CC_P
set_property PACKAGE_PIN E19 [get_ports CLK_TTC_P]
set_property IOSTANDARD LVDS [get_ports CLK_TTC_P]
#FMCP_HSPC_LA22__P
set_property PACKAGE_PIN B16 [get_ports DATA_TTC_P]
set_property IOSTANDARD LVDS [get_ports DATA_TTC_P]
#FMCP_HSPC_LA17_CC_P
set_property PACKAGE_PIN F18 [get_ports clk_ttcfx_ref1_in_p]
set_property IOSTANDARD LVDS [get_ports clk_ttcfx_ref1_in_p]
#FMCP_HSPC_CLK1_M2C_P
set_property PACKAGE_PIN G18 [get_ports clk_ttcfx_ref2_in_p]
set_property IOSTANDARD LVDS [get_ports clk_ttcfx_ref2_in_p]
#FMC bank, NC
set_property PACKAGE_PIN F20 [get_ports CLK40_FPGA2LMK_P]
set_property IOSTANDARD LVCMOS18 [get_ports CLK40_FPGA2LMK_P]
set_property PACKAGE_PIN F19 [get_ports CLK40_FPGA2LMK_N]
set_property IOSTANDARD LVCMOS18 [get_ports CLK40_FPGA2LMK_N]

#NC pins
set_property PACKAGE_PIN BJ49 [get_ports TACH]
set_property IOSTANDARD LVCMOS12 [get_ports TACH]
set_property PACKAGE_PIN BJ48 [get_ports {STN0_PORTCFG[0]}]
set_property IOSTANDARD LVCMOS12 [get_ports {STN0_PORTCFG[0]}]
set_property PACKAGE_PIN BK51 [get_ports {STN0_PORTCFG[1]}]
set_property IOSTANDARD LVCMOS12 [get_ports {STN0_PORTCFG[1]}]
set_property PACKAGE_PIN BK50 [get_ports {STN1_PORTCFG[0]}]
set_property IOSTANDARD LVCMOS12 [get_ports {STN1_PORTCFG[0]}]
set_property PACKAGE_PIN BK49  [get_ports {STN1_PORTCFG[1]}]
set_property IOSTANDARD LVCMOS12 [get_ports {STN1_PORTCFG[1]}]

set_property PACKAGE_PIN BF21 [get_ports {flash_a[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[0]}]
set_property PACKAGE_PIN BJ21 [get_ports {flash_a[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[1]}]
set_property PACKAGE_PIN BL23 [get_ports {flash_a[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[2]}]
set_property PACKAGE_PIN BJ24 [get_ports {flash_a[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[3]}]
set_property PACKAGE_PIN BM23 [get_ports {flash_a[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[4]}]

set_property PACKAGE_PIN BH12 [get_ports {flash_a[5]}]
set_property IOSTANDARD LVCMOS12 [get_ports {flash_a[5]}]
set_property PACKAGE_PIN BL11 [get_ports {flash_a[6]}]
set_property IOSTANDARD LVCMOS12 [get_ports {flash_a[6]}]
set_property PACKAGE_PIN BF6 [get_ports {flash_a[7]}]
set_property IOSTANDARD LVCMOS12 [get_ports {flash_a[7]}]


set_property PACKAGE_PIN C13 [get_ports {flash_a[8]}]
set_property IOSTANDARD LVCMOS12 [get_ports {flash_a[8]}]
set_property PACKAGE_PIN A13 [get_ports {flash_a[9]}]
set_property IOSTANDARD LVCMOS12 [get_ports {flash_a[9]}]
set_property PACKAGE_PIN C14 [get_ports {flash_a[10]}]
set_property IOSTANDARD LVCMOS12 [get_ports {flash_a[10]}]
set_property PACKAGE_PIN H9 [get_ports {flash_a[11]}]
set_property IOSTANDARD LVCMOS12 [get_ports {flash_a[11]}]
set_property PACKAGE_PIN G15 [get_ports {flash_a[12]}]
set_property IOSTANDARD LVCMOS12 [get_ports {flash_a[12]}]

set_property PACKAGE_PIN C19 [get_ports {flash_a[13]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[13]}]
set_property PACKAGE_PIN A18 [get_ports {flash_a[14]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[14]}]
set_property PACKAGE_PIN A19 [get_ports {flash_a[15]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[15]}]
set_property PACKAGE_PIN A20 [get_ports {flash_a[16]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[16]}]
set_property PACKAGE_PIN A21 [get_ports {flash_a[17]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[17]}]
set_property PACKAGE_PIN B17 [get_ports {flash_a[18]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[18]}]
set_property PACKAGE_PIN B18 [get_ports {flash_a[19]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[19]}]
set_property PACKAGE_PIN B20 [get_ports {flash_a[20]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[20]}]
set_property PACKAGE_PIN B21 [get_ports {flash_a[21]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[21]}]
set_property PACKAGE_PIN C17 [get_ports {flash_a[22]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[22]}]
set_property PACKAGE_PIN C18 [get_ports {flash_a[23]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[23]}]
set_property PACKAGE_PIN D20 [get_ports {flash_a[24]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a[24]}]
set_property PACKAGE_PIN C20 [get_ports {flash_a_msb[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a_msb[0]}]
set_property PACKAGE_PIN D19 [get_ports {flash_a_msb[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_a_msb[1]}]

set_property PACKAGE_PIN D16 [get_ports {flash_adv}]
set_property PACKAGE_PIN D17 [get_ports {flash_cclk}]
set_property PACKAGE_PIN D21 [get_ports {flash_ce}]
set_property PACKAGE_PIN E21 [get_ports {flash_d[0]}]
set_property PACKAGE_PIN E16 [get_ports {flash_d[1]}]
set_property PACKAGE_PIN F16 [get_ports {flash_d[2]}]
set_property PACKAGE_PIN J16 [get_ports {flash_d[3]}]
set_property PACKAGE_PIN F21 [get_ports {flash_d[4]}]
set_property PACKAGE_PIN G21 [get_ports {flash_d[5]}]
set_property PACKAGE_PIN H18 [get_ports {flash_d[6]}]
set_property PACKAGE_PIN H19 [get_ports {flash_d[7]}]
set_property PACKAGE_PIN G20 [get_ports {flash_d[8]}]
set_property PACKAGE_PIN H20 [get_ports {flash_d[9]}]
set_property PACKAGE_PIN G16 [get_ports {flash_d[10]}]
set_property PACKAGE_PIN H17 [get_ports {flash_d[11]}]
set_property PACKAGE_PIN J19 [get_ports {flash_d[12]}]
set_property PACKAGE_PIN J20 [get_ports {flash_d[13]}]
set_property PACKAGE_PIN J21 [get_ports {flash_d[14]}]
set_property PACKAGE_PIN K21 [get_ports {flash_d[15]}]
set_property PACKAGE_PIN K18 [get_ports {flash_re}]
set_property PACKAGE_PIN K19 [get_ports {flash_SEL}]
set_property PACKAGE_PIN L20 [get_ports {flash_we}]

set_property IOSTANDARD LVCMOS18 [get_ports {flash_adv}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_cclk}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_ce}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[7]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[8]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[9]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[10]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[11]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[12]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[13]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[14]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_d[15]}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_re}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_SEL}]
set_property IOSTANDARD LVCMOS18 [get_ports {flash_we}]

set_property PACKAGE_PIN L21 [get_ports {GTREFCLK_Si5324_N_IN}]
set_property PACKAGE_PIN L18 [get_ports {GTREFCLK_Si5324_P_IN}]
set_property PACKAGE_PIN L19 [get_ports {LMK_CLK}]
set_property PACKAGE_PIN K16 [get_ports {LMK_DATA}]
set_property PACKAGE_PIN K17 [get_ports {LMK_GOE}]
set_property IOSTANDARD LVCMOS18 [get_ports {GTREFCLK_Si5324_N_IN}]
set_property IOSTANDARD LVCMOS18 [get_ports {GTREFCLK_Si5324_P_IN}]
set_property IOSTANDARD LVCMOS18 [get_ports {LMK_CLK}]
set_property IOSTANDARD LVCMOS18 [get_ports {LMK_DATA}]
set_property IOSTANDARD LVCMOS18 [get_ports {LMK_GOE}]

set_property PACKAGE_PIN C22 [get_ports {I2C_nRESET_PCIe}]
set_property PACKAGE_PIN A24 [get_ports {I2C_SMB}]
set_property PACKAGE_PIN A25 [get_ports {I2C_SMBUS_CFG_nEN}]
set_property PACKAGE_PIN A26 [get_ports {i2cmux_rst[0]}]
set_property PACKAGE_PIN G21 [get_ports {i2cmux_rst[1]}]
set_property PACKAGE_PIN B27 [get_ports {LMK_LD}]
set_property PACKAGE_PIN A23 [get_ports {LMK_LE}]
set_property PACKAGE_PIN B23 [get_ports {LMK_SYNCn}]
set_property PACKAGE_PIN B25 [get_ports {LOL_ADN}]
set_property PACKAGE_PIN B26 [get_ports {LOS_ADN}]
set_property PACKAGE_PIN C24 [get_ports {MGMT_PORT_EN}]
set_property PACKAGE_PIN C25 [get_ports {NT_PORTSEL[0]}]
set_property PACKAGE_PIN B22 [get_ports {NT_PORTSEL[1]}]
set_property PACKAGE_PIN C23 [get_ports {NT_PORTSEL[2]}]
set_property PACKAGE_PIN C27 [get_ports {PCIE_PERSTn1}]
set_property PACKAGE_PIN D27 [get_ports {PCIE_PERSTn2}]
set_property PACKAGE_PIN E27 [get_ports {Perstn1_open}]
set_property PACKAGE_PIN D26 [get_ports {Perstn2_open}]
set_property PACKAGE_PIN D24 [get_ports {PEX_PERSTn}]
set_property PACKAGE_PIN D25 [get_ports {PEX_SCL}]
set_property PACKAGE_PIN D22 [get_ports {PEX_SDA}]
set_property PACKAGE_PIN E22 [get_ports {PORT_GOOD[0]}]
set_property PACKAGE_PIN F25 [get_ports {PORT_GOOD[1]}]
set_property PACKAGE_PIN F26 [get_ports {PORT_GOOD[2]}]
set_property PACKAGE_PIN E23 [get_ports {PORT_GOOD[3]}]
set_property PACKAGE_PIN E24 [get_ports {PORT_GOOD[4]}]
set_property PACKAGE_PIN H25 [get_ports {PORT_GOOD[5]}]
set_property PACKAGE_PIN G25 [get_ports {PORT_GOOD[6]}]
set_property PACKAGE_PIN G26 [get_ports {PORT_GOOD[7]}]
#set_property PACKAGE_PIN G22 [get_ports {Q5_CLK0_GTREFCLK_PAD_N_IN}]
#set_property PACKAGE_PIN G23 [get_ports {Q5_CLK0_GTREFCLK_PAD_P_IN}]
set_property PACKAGE_PIN G27 [get_ports {TESTMODE[0]}]
set_property PACKAGE_PIN H27 [get_ports {TESTMODE[1]}]
set_property PACKAGE_PIN H22 [get_ports {TESTMODE[2]}]
set_property PACKAGE_PIN J22 [get_ports {uC_reset_N}]
set_property PACKAGE_PIN H23 [get_ports {UPSTREAM_PORTSEL[0]}]
set_property PACKAGE_PIN H24 [get_ports {UPSTREAM_PORTSEL[1]}]
set_property PACKAGE_PIN J25 [get_ports {UPSTREAM_PORTSEL[2]}]
set_property PACKAGE_PIN J26 [get_ports {SHPC_INT}]
set_property PACKAGE_PIN J27 [get_ports {SI5345_A[0]}]
set_property PACKAGE_PIN K27 [get_ports {SI5345_A[1]}]
set_property PACKAGE_PIN K22 [get_ports {SI5345_INSEL[0]}]
set_property PACKAGE_PIN L23 [get_ports {SI5345_INSEL[1]}]
set_property PACKAGE_PIN K23 [get_ports {SI5345_nLOL}]
set_property PACKAGE_PIN K24 [get_ports {SI5345_OE}]
set_property PACKAGE_PIN K26 [get_ports {SI5345_RSTN}]
set_property PACKAGE_PIN L26 [get_ports {SI5345_SEL}]
set_property PACKAGE_PIN L24 [get_ports {SmaOut_x4}]
set_property PACKAGE_PIN L25 [get_ports {SmaOut_x5}]
set_property PACKAGE_PIN C42 [get_ports {SmaOut_x6}]

set_property IOSTANDARD LVCMOS18 [get_ports {I2C_nRESET_PCIe}]
set_property IOSTANDARD LVCMOS18 [get_ports {I2C_SMB}]
set_property IOSTANDARD LVCMOS18 [get_ports {I2C_SMBUS_CFG_nEN}]
set_property IOSTANDARD LVCMOS18 [get_ports {i2cmux_rst[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {i2cmux_rst[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {LMK_LD}]
set_property IOSTANDARD LVCMOS18 [get_ports {LMK_LE}]
set_property IOSTANDARD LVCMOS18 [get_ports {LMK_SYNCn}]
set_property IOSTANDARD LVCMOS18 [get_ports {LOL_ADN}]
set_property IOSTANDARD LVCMOS18 [get_ports {LOS_ADN}]
set_property IOSTANDARD LVCMOS18 [get_ports {MGMT_PORT_EN}]
set_property IOSTANDARD LVCMOS18 [get_ports {NT_PORTSEL[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {NT_PORTSEL[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {NT_PORTSEL[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PCIE_PERSTn1}]
set_property IOSTANDARD LVCMOS18 [get_ports {PCIE_PERSTn2}]
set_property IOSTANDARD LVCMOS18 [get_ports {Perstn1_open}]
set_property IOSTANDARD LVCMOS18 [get_ports {Perstn2_open}]
set_property IOSTANDARD LVCMOS18 [get_ports {PEX_PERSTn}]
set_property IOSTANDARD LVCMOS18 [get_ports {PEX_SCL}]
set_property IOSTANDARD LVCMOS18 [get_ports {PEX_SDA}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PORT_GOOD[7]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {Q5_CLK0_GTREFCLK_PAD_N_IN}]
#set_property IOSTANDARD LVCMOS18 [get_ports {Q5_CLK0_GTREFCLK_PAD_P_IN}]
set_property IOSTANDARD LVCMOS18 [get_ports {TESTMODE[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {TESTMODE[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {TESTMODE[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {uC_reset_N}]
set_property IOSTANDARD LVCMOS18 [get_ports {UPSTREAM_PORTSEL[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {UPSTREAM_PORTSEL[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {UPSTREAM_PORTSEL[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SHPC_INT}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_A[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_A[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_INSEL[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_INSEL[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_nLOL}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_OE}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_RSTN}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_SEL}]
set_property IOSTANDARD LVCMOS18 [get_ports {SmaOut_x4}]
set_property IOSTANDARD LVCMOS18 [get_ports {SmaOut_x5}]
set_property IOSTANDARD LVCMOS12 [get_ports {SmaOut_x6}]

#2 Si5345 chips on FELIX mezzanine
set_property PACKAGE_PIN G21 [get_ports {SI5345_A[0]}]
set_property PACKAGE_PIN J20 [get_ports {SI5345_A[1]}]
set_property PACKAGE_PIN J19 [get_ports {SI5345_A[2]}]
set_property PACKAGE_PIN H20 [get_ports {SI5345_A[3]}]
set_property PACKAGE_PIN G20 [get_ports {SI5345_INSEL[0]}]
set_property PACKAGE_PIN E22 [get_ports {SI5345_INSEL[1]}]
set_property PACKAGE_PIN D22 [get_ports {SI5345_INSEL[2]}]
set_property PACKAGE_PIN B23 [get_ports {SI5345_INSEL[3]}]
set_property PACKAGE_PIN A23 [get_ports {SI5345_OE[0]}]
set_property PACKAGE_PIN C23 [get_ports {SI5345_OE[1]}]
set_property PACKAGE_PIN C18 [get_ports {SI5345_RSTN[0]}]
set_property PACKAGE_PIN C17 [get_ports {SI5345_RSTN[1]}]
set_property PACKAGE_PIN B22 [get_ports {SI5345_SEL[0]}]
set_property PACKAGE_PIN E21 [get_ports {SI5345_SEL[1]}]
set_property PACKAGE_PIN A19 [get_ports {SI5345_nLOL[0]}]
set_property PACKAGE_PIN A18 [get_ports {SI5345_nLOL[1]}]
set_property PACKAGE_PIN H23 [get_ports {clk40_ttc_ref_out_n[0]}]
set_property PACKAGE_PIN E23 [get_ports {clk40_ttc_ref_out_n[1]}]
set_property PACKAGE_PIN H24 [get_ports {clk40_ttc_ref_out_p[0]}]
set_property PACKAGE_PIN E24 [get_ports {clk40_ttc_ref_out_p[1]}]

set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_A[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_A[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_A[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_A[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_INSEL[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_INSEL[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_INSEL[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_INSEL[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_OE[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_OE[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_RSTN[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_RSTN[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_SEL[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_SEL[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_nLOL[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {SI5345_nLOL[1]}]
set_property IOSTANDARD LVDS [get_ports {clk40_ttc_ref_out_n[0]}]
set_property IOSTANDARD LVDS [get_ports {clk40_ttc_ref_out_n[1]}]
set_property IOSTANDARD LVDS [get_ports {clk40_ttc_ref_out_p[0]}]
set_property IOSTANDARD LVDS [get_ports {clk40_ttc_ref_out_p[1]}]


#J12 SMA_CLK_OUTPUT_P
set_property PACKAGE_PIN BK26 [get_ports SmaOut_x3]
set_property IOSTANDARD LVCMOS18 [get_ports SmaOut_x3]



#set_clock_groups -name async18 -asynchronous -group [get_clocks {sys_clk*}] -group [get_clocks -of_objects [get_pins -hierarchical -filter {NAME =~ *gen_channel_container[*].*gen_gtye4_channel_inst[*].GTYE4_CHANNEL_PRIM_INST/TXOUTCLK}]]
#set_clock_groups -name async19 -asynchronous -group [get_clocks -of_objects [get_pins -hierarchical -filter {NAME =~ *gen_channel_container[*].*gen_gtye4_channel_inst[*].GTYE4_CHANNEL_PRIM_INST/TXOUTCLK}]] -group [get_clocks {sys_clk*}]
#set_clock_groups -name async5 -asynchronous -group [get_clocks {sys_clk*}] -group [get_clocks -of_objects [get_pins */*/*/inst/gt_top_i/diablo_gt.diablo_gt_phy_wrapper/phy_clk_i/bufg_gt_userclk/O]]
#set_clock_groups -name async6 -asynchronous -group [get_clocks -of_objects [get_pins */*/*/inst/gt_top_i/diablo_gt.diablo_gt_phy_wrapper/phy_clk_i/bufg_gt_userclk/O]] -group [get_clocks {sys_clk*}]
#set_clock_groups -name async1 -asynchronous -group [get_clocks {sys_clk*}] -group [get_clocks -of_objects [get_pins */*/*/inst/gt_top_i/diablo_gt.diablo_gt_phy_wrapper/phy_clk_i/bufg_gt_pclk/O]]
#set_clock_groups -name async2 -asynchronous -group [get_clocks -of_objects [get_pins */*/*/inst/gt_top_i/diablo_gt.diablo_gt_phy_wrapper/phy_clk_i/bufg_gt_pclk/O]] -group [get_clocks {sys_clk*}]
#set_clock_groups -name async24 -asynchronous -group [get_clocks -of_objects [get_pins */*/*/inst/gt_top_i/diablo_gt.diablo_gt_phy_wrapper/phy_clk_i/bufg_gt_intclk/O]] -group [get_clocks {sys_clk}]





##GBT Transceiver pins
#Bank 128
set_property PACKAGE_PIN AC43 [get_ports {RX_P[0]}]
set_property PACKAGE_PIN AF41 [get_ports {RX_P[3]}]
set_property PACKAGE_PIN AE43 [get_ports {RX_P[2]}]
set_property PACKAGE_PIN AG43 [get_ports {RX_P[1]}]
#bank 127
#set_property PACKAGE_PIN AH41 [get_ports {RX_P[4]}]
#set_property PACKAGE_PIN AJ43 [get_ports {RX_P[5]}]
#set_property PACKAGE_PIN AN43 [get_ports {RX_P[6]}]
#set_property PACKAGE_PIN AL43 [get_ports {RX_P[7]}]
#bank 133
set_property -quiet PACKAGE_PIN C43  [get_ports {RX_P[4]}]
set_property -quiet PACKAGE_PIN E43  [get_ports {RX_P[5]}]
set_property -quiet PACKAGE_PIN G43  [get_ports {RX_P[6]}]
set_property -quiet PACKAGE_PIN J43  [get_ports {RX_P[7]}]
#bank 231
set_property -quiet PACKAGE_PIN J2 [get_ports {RX_P_LTITTC[0]}]


#MGT0,1,2,4 bank 128, use clk from bank 127 AH37
#MGT3,5,6,7 bank 127, use clk from bank 127 AH37
#MGT8k,9,10,11 bank 126, use clk from bank 127 AH37
#MGT12,13,14,15 bank 133, use clk from bank 132 T37


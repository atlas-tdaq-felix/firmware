#!/bin/bash

MODELSIMDIR=$1

if [ -d /eda/fpga/mentor/questasim_2019.1/bin/ ]; then
    #Nikhef questasim installation path and license server.
    export PATH=/eda/fpga/mentor/questasim_2019.1/bin/:$PATH
    export LM_LICENSE_FILE="$LM_LICENSE_FILE"
elif [ -d /afs/cern.ch/work/f/fschreud/public/questasim_2019.1/ ]; then
    #Cern questasim installation path and license server
    export PATH=/afs/cern.ch/work/f/fschreud/public/questasim_2019.1/linux_x86_64/:$PATH
    export MGLS_LICENSE_FILE=1717@lxlicen01,1717@lxlicen02,1717@lxlicen03,1717@lnxmics1,1717@lxlicen08
elif [ -d $MODELSIMDIR ]; then
    export PATH=$MODELSIMDIR:$PATH
else
    echo "Could not find questasim installation path, exiting."
    exit 1
fi

cd ../UVVM/script/
while vsim -c -do ./compile_all.do -do quit|grep EEXIST; 
do
    echo compiling uvvm
done
echo done compiling uvvm
cd -

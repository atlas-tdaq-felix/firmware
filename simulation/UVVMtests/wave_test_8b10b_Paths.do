
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Ohad Shaked
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /egroup_tb/i_test_harness/DUT/clk160
add wave -noupdate /egroup_tb/i_test_harness/DUT/clk240
add wave -noupdate /egroup_tb/i_test_harness/DUT/rst_clk40
add wave -noupdate -color Orange -itemcolor Orange /egroup_tb/i_test_harness/DUT/DATA16bitIN
add wave -noupdate -expand /egroup_tb/i_test_harness/DUT/BWORD_array
add wave -noupdate -expand /egroup_tb/i_test_harness/DUT/BWORD_RDY_array
add wave -noupdate -color Magenta -itemcolor Magenta -expand -subitemconfig {/egroup_tb/i_test_harness/DUT/dec8b10b_code_err(0) {-color Magenta -itemcolor Magenta} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err(1) {-color Magenta -itemcolor Magenta} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err(2) {-color Magenta -itemcolor Magenta} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err(3) {-color Magenta -itemcolor Magenta} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err(4) {-color Magenta -itemcolor Magenta} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err(5) {-color Magenta -itemcolor Magenta} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err(6) {-color Magenta -itemcolor Magenta} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err(7) {-color Magenta -itemcolor Magenta} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err(8) {-color Magenta -itemcolor Magenta} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err(9) {-color Magenta -itemcolor Magenta} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err(10) {-color Magenta -itemcolor Magenta} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err(11) {-color Magenta -itemcolor Magenta} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err(12) {-color Magenta -itemcolor Magenta} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err(13) {-color Magenta -itemcolor Magenta} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err(14) {-color Magenta -itemcolor Magenta}} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err
add wave -noupdate -color Magenta -itemcolor Magenta -expand -subitemconfig {/egroup_tb/i_test_harness/DUT/dec8b10b_code_err_Array(0) {-color Magenta -height 16 -itemcolor Magenta} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err_Array(1) {-color Magenta -height 16 -itemcolor Magenta} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err_Array(2) {-color Magenta -height 16 -itemcolor Magenta} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err_Array(3) {-color Magenta -height 16 -itemcolor Magenta} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err_Array(4) {-color Magenta -height 16 -itemcolor Magenta} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err_Array(5) {-color Magenta -height 16 -itemcolor Magenta} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err_Array(6) {-color Magenta -height 16 -itemcolor Magenta} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err_Array(7) {-color Magenta -height 16 -itemcolor Magenta}} /egroup_tb/i_test_harness/DUT/dec8b10b_code_err_Array
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {7084684 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 222
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {3974557 ps} {4346709 ps}

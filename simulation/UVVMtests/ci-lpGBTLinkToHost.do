
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

source ./lpGBTLinkToHost_import_questa.tcl
source external_editor.tcl

project removefile tb/HGTD_fastcmd_tb.vhd
project removefile tb/FULLModeToHost_tb.vhd
project removefile tb/GBTLinkToHost_tb.vhd
project removefile tb/GBTCrCoding_tb.vhd
project removefile ../../sources/encoding/EncodingEpathLPGBT.vhd
project removefile ../../sources/encoding/encoding.vhd
#project removefile ../../sources/decoding/DecodingPixelLinkLPGBT.vhd

vsim -voptargs=+acc -t ps work.lpGBTLinkToHost_tb  work.glbl


add wave -group top sim:/lpgbtlinktohost_tb/*
add wave -group framegen sim:/lpgbtlinktohost_tb/framegen0/*
for {set link 0} {$link < 6} {incr link} {
    for {set egroup 0} {$egroup < 7} {incr egroup} {
        add wave -group link${link} -group egroup${egroup} -group gb0 sim:/lpgbtlinktohost_tb/decoding0/g_lpgbt8b10b/g_lpGBT_Links(${link})/g_Egroups(${egroup})/eGroup0/g_32b_gearbox0/i_DecodingGearBox_32b/*
        add wave -group link${link} -group egroup${egroup} -group gb1 sim:/lpgbtlinktohost_tb/decoding0/g_lpgbt8b10b/g_lpGBT_Links(${link})/g_Egroups(${egroup})/eGroup0/g_8b_gearbox1/i_DecodingGearBox_8b1/*
        add wave -group link${link} -group egroup${egroup} -group gb2 sim:/lpgbtlinktohost_tb/decoding0/g_lpgbt8b10b/g_lpGBT_Links(${link})/g_Egroups(${egroup})/eGroup0/g_16b_gearbox/i_DecodingGearBox_16b/*
        add wave -group link${link} -group egroup${egroup} -group gb3 sim:/lpgbtlinktohost_tb/decoding0/g_lpgbt8b10b/g_lpGBT_Links(${link})/g_Egroups(${egroup})/eGroup0/g_8b_gearbox2/i_DecodingGearBox_8b2/*
        add wave -group link${link} -group egroup${egroup} -group dec8b10b_0 sim:/lpgbtlinktohost_tb/decoding0/g_lpgbt8b10b/g_lpGBT_Links(${link})/g_Egroups(${egroup})/eGroup0/gen_dec(0)/decoder8b10b0/*
        add wave -group link${link} -group egroup${egroup} -group dec8b10b_1 sim:/lpgbtlinktohost_tb/decoding0/g_lpgbt8b10b/g_lpGBT_Links(${link})/g_Egroups(${egroup})/eGroup0/gen_dec(1)/decoder8b10b0/*
        add wave -group link${link} -group egroup${egroup} -group dec8b10b_2 sim:/lpgbtlinktohost_tb/decoding0/g_lpgbt8b10b/g_lpGBT_Links(${link})/g_Egroups(${egroup})/eGroup0/gen_dec(2)/decoder8b10b0/*
        add wave -group link${link} -group egroup${egroup} -group dec8b10b_3 sim:/lpgbtlinktohost_tb/decoding0/g_lpgbt8b10b/g_lpGBT_Links(${link})/g_Egroups(${egroup})/eGroup0/gen_dec(3)/decoder8b10b0/*
        add wave -group link${link} -group egroup${egroup} -group ByteToAxiStream0 sim:/lpgbtlinktohost_tb/decoding0/g_lpgbt8b10b/g_lpGBT_Links(${link})/g_Egroups(${egroup})/eGroup0/g_byteToAxiStream32b0/i_ByteToAxiStream0/*
        add wave -group link${link} -group egroup${egroup} -group ByteToAxiStream0 sim:/lpgbtlinktohost_tb/decoding0/g_lpgbt8b10b/g_lpGBT_Links(${link})/g_Egroups(${egroup})/eGroup0/g_byteToAxiStream32b0/i_ByteToAxiStream0/g_multibyte/*
        add wave -group link${link} -group egroup${egroup} -group ByteToAxiStream1 sim:/lpgbtlinktohost_tb/decoding0/g_lpgbt8b10b/g_lpGBT_Links(${link})/g_Egroups(${egroup})/eGroup0/g_byteToAxiStream8b1/i_ByteToAxiStream1/*
        add wave -group link${link} -group egroup${egroup} -group ByteToAxiStream1 sim:/lpgbtlinktohost_tb/decoding0/g_lpgbt8b10b/g_lpGBT_Links(${link})/g_Egroups(${egroup})/eGroup0/g_byteToAxiStream8b1/i_ByteToAxiStream1/g_multibyte/*
        add wave -group link${link} -group egroup${egroup} -group ByteToAxiStream2 sim:/lpgbtlinktohost_tb/decoding0/g_lpgbt8b10b/g_lpGBT_Links(${link})/g_Egroups(${egroup})/eGroup0/g_byteToAxiStream16b/i_ByteToAxiStream2/*
        add wave -group link${link} -group egroup${egroup} -group ByteToAxiStream2 sim:/lpgbtlinktohost_tb/decoding0/g_lpgbt8b10b/g_lpGBT_Links(${link})/g_Egroups(${egroup})/eGroup0/g_byteToAxiStream16b/i_ByteToAxiStream2/g_multibyte/*
        add wave -group link${link} -group egroup${egroup} -group ByteToAxiStream3 sim:/lpgbtlinktohost_tb/decoding0/g_lpgbt8b10b/g_lpGBT_Links(${link})/g_Egroups(${egroup})/eGroup0/g_byteToAxiStream8b2/i_ByteToAxiStream3/*
        add wave -group link${link} -group egroup${egroup} -group ByteToAxiStream3 sim:/lpgbtlinktohost_tb/decoding0/g_lpgbt8b10b/g_lpGBT_Links(${link})/g_Egroups(${egroup})/eGroup0/g_byteToAxiStream8b2/i_ByteToAxiStream3/g_multibyte/*
    }
    add wave -group link${link} sim:/lpgbtlinktohost_tb/cr0/thFMdataManagers(${link})/thFMdmN/g_32b/chStreamController/*
    add wave -group link${link} sim:/lpgbtlinktohost_tb/cr0/thFMdataManagers(${link})/thFMdmN/g_32b/chStreamController/chunk_proc/*
}
add wave -group sink sim:/lpgbtlinktohost_tb/sink0/gDescriptors(0)/block_data

run -all
quit

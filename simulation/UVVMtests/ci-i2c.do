
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               jacopo pinzino
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

source ./i2c_import_questa.tcl
vsim -voptargs=+acc work.i2c_tb(func)  work.glbl
add wave -position insertpoint  \
sim:/i2c_tb/register_map_control.I2C_RD \
sim:/i2c_tb/register_map_control.I2C_WR \
sim:/i2c_tb/register_map_hk_monitor.I2C_RD \
sim:/i2c_tb/register_map_hk_monitor.I2C_WR \
sim:/i2c_tb/ack_in \
sim:/i2c_tb/appreg_clk \
sim:/i2c_tb/C_CLK_PERIOD_25 \
sim:/i2c_tb/C_I2C_BFM_CONFIG \
sim:/i2c_tb/C_SCOPE \
sim:/i2c_tb/clk \
sim:/i2c_tb/clk25 \
sim:/i2c_tb/clock_ena \
sim:/i2c_tb/cmd_ack \
sim:/i2c_tb/Din \
sim:/i2c_tb/Dout \
sim:/i2c_tb/ena \
sim:/i2c_tb/i2c_if \
sim:/i2c_tb/nReset \
sim:/i2c_tb/rden \
sim:/i2c_tb/read \
sim:/i2c_tb/rnw \
sim:/i2c_tb/RST \
sim:/i2c_tb/rst_soft \
sim:/i2c_tb/start \
sim:/i2c_tb/stop \
sim:/i2c_tb/wren \
sim:/i2c_tb/write \
sim:/i2c_tb/WRITE_2BYTES

run -all
quit

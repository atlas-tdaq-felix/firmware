
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

source ./GBTCrCoding_import_questa.tcl
vsim -voptargs="+acc" -t 1ps work.GBTCRCoding_tb work.glbl
add wave -group top -position insertpoint sim:/gbtcrcoding_tb/*
for {set i 0} {$i < 12} {incr i} {
    add wave -group chStreamController${i} -format analog -min 0 -max 2027 -height 100 -position insertpoint sim:/gbtcrcoding_tb/crt0/thFMdataManagers(${i})/thFMdmN/wr_data_count
    add wave -position insertpoint -group chStreamController${i} sim:/gbtcrcoding_tb/crt0/thFMdataManagers(${i})/thFMdmN/g_32b/chStreamController/chunk_proc/*
    add wave -position insertpoint -group chStreamController${i} sim:/gbtcrcoding_tb/crt0/thFMdataManagers(${i})/thFMdmN/g_32b/chStreamController/*
    
    for {set g 0} {$g < 5} {incr g} {
        for {set p 0} {$p < 8} {incr p} {
            add wave -position insertpoint -group decodingGBT${i} -group Egroup${g} -group Epath${p} sim:/gbtcrcoding_tb/decoding0/g_gbtmode/g_GBT_Links(${i})/g_noltdb/g_Egroups(${g})/eGroup0/g_Epaths(${p})/do_generate/Epath0/*
            add wave -position insertpoint -group decodingGBT${i} -group Egroup${g} -group dec8b10b${p} sim:/gbtcrcoding_tb/decoding0/g_gbtmode/g_GBT_Links(${i})/g_noltdb/g_Egroups(${g})/eGroup0/g_Epaths(${p})/do_generate/Epath0/g_include8b10b/decoder8b10b0/*
            add wave -position insertpoint -group decodingGBT${i} -group Egroup${g} -group gearbox${p} sim:/gbtcrcoding_tb/decoding0/g_gbtmode/g_GBT_Links(${i})/g_noltdb/g_Egroups(${g})/eGroup0/g_Epaths(${p})/do_generate/Epath0/gearbox0/*
            add wave -position insertpoint -group decodingGBT${i} -group Egroup${g} -group ToAxis${p} sim:/gbtcrcoding_tb/decoding0/g_gbtmode/g_GBT_Links(${i})/g_noltdb/g_Egroups(${g})/eGroup0/g_Epaths(${p})/do_generate/Epath0/toAxis0/*
        }
    }
    
}
add wave -group sink0 -position insertpoint sim:/gbtcrcoding_tb/sink0/*
add wave -group sink0 -position insertpoint sim:/gbtcrcoding_tb/sink0/gDescriptors(0)/checker/*
add wave -group sink0 -position insertpoint sim:/gbtcrcoding_tb/sink0/gDescriptors(0)/block_data

run -all
quit

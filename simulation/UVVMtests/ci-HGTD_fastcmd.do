
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

source ./HGTD_fastcmd_import_questa.tcl
source external_editor.tcl

project removefile ../../sources/decoding/DecodingPixelLinkLPGBT.vhd
project removefile ../../sources/RD53/RD53Interface.vhd
project removefile tb/GBTLinkToHost_tb.vhd
project removefile tb/GBTCrCoding_tb.vhd
project removefile tb/FULLModeToHost_tb.vhd
project removefile tb/lpGBTLinkToHost_tb.vhd


vsim -voptargs=+acc -t ps work.HGTD_fastcmd_tb  work.glbl
# external_editor

add wave -position insertpoint sim:/hgtd_fastcmd_tb/*
add wave -position insertpoint -group fastcmd0 sim:/hgtd_fastcmd_tb/fastcmd0/*


run -all
quit

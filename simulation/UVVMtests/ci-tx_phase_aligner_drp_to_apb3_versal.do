
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               jacopo pinzino
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

source ./tx_phase_aligner_drp_to_apb3_versal_import_questa.tcl
#vsim -t fs work.tx_phase_aligner_drp_to_apb3_versal_tb  work.glbl
vsim -t fs work.tx_phase_aligner_drp_to_apb3_versal_tb work.glbl -L xpm -L unisims_ver -L secureip -L util_reduced_logic_v2_0_4 -L xlconcat_v2_1_4 -L xlslice_v1_0_2


# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Elena Zhivun
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

source ./strips_import_questa.tcl

set strips_tb {tb_bypass_frame_aggregator \
  tb_bypass_scheduler_continuous_write \
  tb_lcb_regmap \
  tb_r3l1_regmap \
  tb_lcb_frame_generator \
  tb_lcb_command_decoder \
  tb_strips_configuration_decoder \
  tb_trickle_trigger \
  tb_playback_controller \
  tb_lcb_scheduler_encoder \
  tb_l0a_frame_generator \
  tb_lcb_axi_encoder \
  tb_bypass_frame_vvc \
  tb_r3l1_scheduler_encoder \
  tb_r3l1_frame_generator \
  tb_r3l1_frame_synchronizer \
  tb_r3l1_axi_encoder}

foreach testbench_name $strips_tb {
  puts "Running ITk Strips testbench: $testbench_name..."
  vsim work.$testbench_name
  if {[runStatus] == "nodesign"} {
    puts "$testbench_name failed to load"
    exit -code 3
  }
  run -all
  set test_result [coverage attribute -name TESTSTATUS -concise]
  if {$test_result > 1 || [runStatus] == "break" } {
    echo $testbench_name tests failed!
    exit -code 3
  } else {
    echo $testbench_name tests passed 
  }
  exit -sim
}

puts "All ITk Strips tests passed"
quit

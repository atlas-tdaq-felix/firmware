
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               jacopo pinzino
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

source ./GBT_FULL_import_questa.tcl
vsim -voptargs=+acc work.egroup_tb(arch)  work.glbl
#add wave -group CD_COUNTER -position insertpoint sim:/egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(0)/FDn/Module_enable/SCDataMANAGER_inst/g_trunc/CD_COUNTER_inst/*
#add wave -group SCDataManager -position insertpoint sim:/egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(0)/FDn/Module_enable/SCDataMANAGER_inst/*
#add wave -group FIFO_DRIVER -position insertpoint sim:/egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(0)/FDn/*
#add wave -group tb -position insertpoint \
# sim:/egroup_tb/block_data0 \
# sim:/egroup_tb/block_data1 \
# sim:/egroup_tb/chunk_error0 \
# sim:/egroup_tb/TrailerError \
# sim:/egroup_tb/Trunc \
# sim:/egroup_tb/Eout \
# sim:/egroup_tb/Eout_rdy \
# sim:/egroup_tb/block_error0 \
# sim:/egroup_tb/backpressure
#add wave -group BLOCK_WORD_COUNTER -position insertpoint sim:/egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(0)/FDn/Module_enable/BLOCK_WORD_COUNTER_inst/*

run -all
quit

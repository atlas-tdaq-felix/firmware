source ../../scripts/helper/clear_filesets.tcl

set SIM_ARCH "VU37P"

set XIL_PROJECTS "FLX128_FELIX"
set PROJECT_NAME Loopback25G

source ../../scripts/filesets/crToHost_fileset.tcl
source ../../scripts/filesets/crFromHost_fileset.tcl
source ../../scripts/filesets/UVVM_fileset.tcl
source ../../scripts/filesets/core1990_interlaken_fileset.tcl
source external_editor.tcl

source ../../scripts/helper/questa_import_generic.tcl


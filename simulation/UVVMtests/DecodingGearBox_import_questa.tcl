
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

source ../../scripts/helper/clear_filesets.tcl

set XIL_PROJECTS "FLX712_FULLMODE FLX712_GBT"
set PROJECT_NAME DecodingGearBox

source ../../scripts/filesets/crToHost_fileset.tcl
#source ../../scripts/filesets/crFromHost_fileset.tcl
source ../../scripts/filesets/decoding_fileset.tcl
#source ../../scripts/filesets/encoding_fileset.tcl
source ../../scripts/filesets/64b66b_fileset.tcl
source ../../scripts/filesets/UVVM_fileset.tcl
source ../../scripts/filesets/gbt_emulator_fileset.tcl
source ../../scripts/filesets/gbt_fanout_fileset.tcl
source ../../scripts/filesets/fullmode_fanout_fileset.tcl


source ../../scripts/helper/questa_import_generic.tcl


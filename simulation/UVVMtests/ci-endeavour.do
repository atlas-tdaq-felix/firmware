
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               jacopo pinzino
#               Frans Schreuder
#               Elena Zhivun
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

source ./GBT_FULL_import_questa.tcl


set amac_tb {amac_demo_tb \
  tb_amac_decoder \
  tb_amac_deglitcher \
  tb_amac_encoder}


foreach testbench_name $amac_tb {
  puts "Running Endeavour testbench: $testbench_name..."
  vsim work.$testbench_name work.glbl
  if {[runStatus] == "nodesign"} {
    puts "$testbench_name failed to load"
    exit -code 3
  }
  run -all
  set test_result [coverage attribute -name TESTSTATUS -concise]
  if {$test_result > 1 || [runStatus] == "break" } {
    echo $testbench_name tests failed!
    exit -code 3
  } else {
    echo $testbench_name tests passed 
  }
  exit -sim
}

puts "All Endeavour tests passed"
quit

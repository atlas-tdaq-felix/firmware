
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

source ./HDLCDataPath_import_questa.tcl
vsim -voptargs=+acc -t ps work.HDLCDataPath_tb  work.glbl
# add wave -position end -divider "testbench"
# add wave -position end  sim:/hdlcdatapath_tb/*
# add wave -position end -divider "encoder"
# add wave -position end  sim:/hdlcdatapath_tb/enc/g_includeEpath/g_includeHDLC/EncoderHDLC0/*
# add wave -position end -divider "decoder"
# add wave -position end  sim:/hdlcdatapath_tb/dec/g_includeHDLC/decoderHDLC0/*
# config wave -signalnamewidth 1
run -all
quit

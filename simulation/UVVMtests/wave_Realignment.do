
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Ohad Shaked
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -color Coral /egroup_tb/i_test_harness/Data16bitIN
add wave -noupdate /egroup_tb/clk40
add wave -noupdate /egroup_tb/clk240
add wave -noupdate /egroup_tb/i_test_harness/reset
add wave -noupdate /egroup_tb/i_test_harness/DUT/BWORD_array
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/Module_enable/dec_8b10b_generated/dec_8b10/disp_err
add wave -noupdate /egroup_tb/i_test_harness/DUT/BWORD_RDY_array
add wave -noupdate -color Magenta /egroup_tb/i_test_harness/DUT/Dec8b10b_code_err
add wave -noupdate -color Magenta /egroup_tb/i_test_harness/DUT/Dec8b10b_code_err_Array
add wave -noupdate -divider {New Divider}
add wave -noupdate /egroup_tb/i_test_harness/DUT/Dec8b10b_code_err_evnt
add wave -noupdate /egroup_tb/i_test_harness/DUT/Dec8b10b_code_err_s
add wave -noupdate /egroup_tb/i_test_harness/DUT/Dec8b10b_Epath1_Din
add wave -noupdate /egroup_tb/i_test_harness/DUT/Dec8b10b_Epath1_RDY
add wave -noupdate /egroup_tb/i_test_harness/DUT/Dec8b10b_ReAlignment_en
add wave -noupdate /egroup_tb/i_test_harness/DUT/Dec8b10b_ReAlignment_en_s
add wave -noupdate /egroup_tb/i_test_harness/DUT/Dec8b10b_ReAlignment_rst_pulse
add wave -noupdate /egroup_tb/i_test_harness/DUT/Realignment_cnt
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {3676433 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 252
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {52015 ns} {58315 ns}

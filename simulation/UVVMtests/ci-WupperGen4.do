
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

source ./WupperGen4_import_questa.tcl
vsim -t 1ps -voptargs="+acc" work.Wupper_tb  work.glbl

add wave -position insertpoint -group sim_model0 sim:/wupper_tb/pcie0/ep0/g_simulation/sim_model0/*
add wave -position insertpoint -group sim_model0 sim:/wupper_tb/pcie0/ep0/g_simulation/sim_model0/ToHostMem
add wave -position insertpoint -group sim_model0 sim:/wupper_tb/pcie0/ep0/g_simulation/sim_model0/FromHostMem
add wave -position insertpoint -group dma_read_write sim:/wupper_tb/pcie0/dma0/u0/*
add wave -position insertpoint -group dma_read_write -group add_header sim:/wupper_tb/pcie0/dma0/u0/add_header/*
add wave -position insertpoint -group dma_read_write -group strip_header sim:/wupper_tb/pcie0/dma0/u0/strip_hdr/*
add wave -position insertpoint -group fifos sim:/wupper_tb/pcie0/fifos0/*
add wave -position insertpoint -group fifos -group tohost0 sim:/wupper_tb/pcie0/fifos0/g_tohost(0)/*
add wave -position insertpoint -group fifos -group tohost1 sim:/wupper_tb/pcie0/fifos0/g_tohost(1)/*
add wave -position insertpoint -group fifos -group tohost2 sim:/wupper_tb/pcie0/fifos0/g_tohost(2)/*
add wave -position insertpoint -group fifos -group tohost3 sim:/wupper_tb/pcie0/fifos0/g_tohost(3)/*

run -all
quit

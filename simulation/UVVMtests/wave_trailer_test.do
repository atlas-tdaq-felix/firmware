
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Ohad Shaked
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /egroup_tb/i_test_harness/DUT/Realignment_en
add wave -noupdate /egroup_tb/i_test_harness/DUT/Elink_Realign_status_clr
add wave -noupdate /egroup_tb/i_test_harness/DUT/Dec8b10b_ReAlignment_rst_pulse
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/Module_enable/dec_8b10b_generated/dec_8b10/disp_err
add wave -noupdate -color Magenta /egroup_tb/i_test_harness/DUT/Dec8b10b_code_err
add wave -noupdate -color Magenta /egroup_tb/i_test_harness/DUT/Dec8b10b_code_err_Array
add wave -noupdate -color Cyan -itemcolor Cyan /egroup_tb/i_test_harness/DUT/DATA_OUT_array(1)
add wave -noupdate -color Cyan -itemcolor Cyan /egroup_tb/i_test_harness/DUT/DATA_RDY_array(1)
add wave -noupdate -color Magenta -itemcolor Magenta /egroup_tb/i_test_harness/DUT/BWORD_RDY_array(1)
add wave -noupdate -color Magenta -itemcolor Magenta /egroup_tb/i_test_harness/DUT/BWORD_array(1)
add wave -noupdate -divider Trailer_start
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/GENERATE_TRUNCATION_MECHANISM
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/clk40
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/clk160
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/rst
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/encoding
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/maxCLEN
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/raw_DIN
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/raw_DIN_RDY
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/prog_full_in
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/timeCntIn
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/TimeoutEnaIn
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/instTimeoutEnaIn
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/wordOUT
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/wordOUT_RDY
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/busyOut
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/dec8b10b_code_err
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/DIN
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/DIN_RDY
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/raw_DIN_s0
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/raw_DIN_s1
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/input_mux_sel0
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/input_mux_sel1
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/DIN_r
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/DIN_r1
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/DIN_CODE_r
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/DIN_s
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/DIN_s0
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/DIN_s1
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/DIN_RDY_s
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/DIN_RDY_s0
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/DIN_RDY_s1
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/DIN_RDY_r
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/receiving_state
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/data_shift_trig
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/trailer_shift_trig
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/trailer_shift_trig_s
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/EOC_error
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/SOC_error
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/rst_clen_counter
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/data16bit_rdy
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/truncating_state
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/truncation_trailer_sent
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/trailer_sent
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/send_trailer_trig
add wave -noupdate -color Yellow -itemcolor Yellow /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/data_shift_trig_s
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/DIN_prev_is_zeroByte
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/DIN_is_zeroByte
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/direct_data_mode
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/direct_data_boundary_detected
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/trailer_trunc_bit
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/trailer_cerr_bit
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/first_subchunk
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/first_subchunk_on
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/trailer_mod_bits
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/trailer_type_bits
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/EOB_MARK
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/preEOB_MARK
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/truncateDataFlag
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/flushed
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/flush_trig
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/data_rdy
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/trailer_shift_trigs
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/trailer_shift_trig0
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/header_shift_trigs
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/trailer_shift_trig1
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/trailer
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/trailer0
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/trailer1
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/header
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/data
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/wordOUT_s
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/pathENA
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/pathENAtrig
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/blockCountRdy
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/xoff_s
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/not_a_comma
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/trailer_reserved_bit
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/prog_full_s
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/timeCnt_lastClk
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/instTimeoutEnaIn_s
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/TimeoutEnaIn_s
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/timeout_state
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/timeout_ena
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/timeout_soc_ena
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/timeout_eoc_ena
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/timeout_event
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/timeout_trailer_case
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/clk160_cnt0
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/clk160_cnt1
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/data16bit_rdy_code
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/HDLC_mode
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/dec8b10b_disp_err
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/dec8b10b_code_err_sig
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/dec8b10b_code_err_d1
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/dec8b10b_code_err_d2
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/dec8b10b_code_err_d3
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/trailer_type_bits_msk
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/trailer_dec_err_trig_s
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/trailer_dec_err_trig
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/dec8b10b_code_err_s
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/dec8b10b_code_err_s1
add wave -noupdate -divider Trailer_END
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/Module_enable/SCDataMANAGER_inst/dataCNTena
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/Module_enable/SCDataMANAGER_inst/trailerOUT
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/Module_enable/SCDataMANAGER_inst/trailerOUTrdy
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/Module_enable/SCDataMANAGER_inst/sc_counter_rst
add wave -noupdate -color {Sky Blue} /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/Module_enable/SCDataMANAGER_inst/schunk_length
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/Module_enable/SCDataMANAGER_inst/trailer_s
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/Module_enable/SCDataMANAGER_inst/reset_truncation_counter
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/encoding
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/receiving_state
add wave -noupdate -color Orange -itemcolor Orange /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/dec8b10b_code_err
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/dec8b10b_code_err_sig
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/dec8b10b_code_err_s
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/dec8b10b_code_err_d1
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/dec8b10b_code_err_d2
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/dec8b10b_code_err_d3
add wave -noupdate /egroup_tb/i_test_harness/DUT/PATH_FIFO_DRIVERs(1)/FDn/dec8b10b_disp_err
add wave -noupdate /egroup_tb/i_test_harness/DUT/probe_vio
add wave -noupdate /egroup_tb/i_test_harness/DUT/ILA_probe5
add wave -noupdate /egroup_tb/i_test_harness/DUT/ILA_probe6
add wave -noupdate /egroup_tb/i_test_harness/DUT/probe_vio
add wave -noupdate /egroup_tb/i_test_harness/DUT/Realignment_cnt
add wave -noupdate /egroup_tb/i_test_harness/DUT/BWORD_array
add wave -noupdate /egroup_tb/i_test_harness/DUT/BWORD_RDY_array
add wave -noupdate -color {Medium Spring Green} /egroup_tb/i_test_harness/DUT/DATA_OUT_array(1)
add wave -noupdate -color {Medium Spring Green} /egroup_tb/i_test_harness/DUT/DATA_OUT_array
add wave -noupdate /egroup_tb/i_test_harness/DUT/DATA_RDY_array
add wave -noupdate /egroup_tb/i_test_harness/DUT/Dec8b10b_code_err
add wave -noupdate /egroup_tb/i_test_harness/DUT/Dec8b10b_code_err_Array
add wave -noupdate /egroup_tb/i_test_harness/DUT/Dec8b10b_code_err_evnt
add wave -noupdate /egroup_tb/i_test_harness/DUT/Dec8b10b_code_err_s
add wave -noupdate /egroup_tb/i_test_harness/DUT/Dec8b10b_ReAlignment_rst_pulse
add wave -noupdate /egroup_tb/i_test_harness/DUT/Realignment_cnt
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {907836050 ps} 0} {{Cursor 2} {448556268 ps} 0}
quietly wave cursor active 2
configure wave -namecolwidth 234
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {448508631 ps} {449025783 ps}

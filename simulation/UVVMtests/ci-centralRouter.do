
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

source ./GBT_FULL_import_questa.tcl
vsim -voptargs=+acc work.centralRouter_tb

add wave -group CD_COUNTER0 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(0)/FDn/Module_enable/SCDataMANAGER_inst/g_trunc/CD_COUNTER_inst/*
add wave -group SCDataManager0 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(0)/FDn/Module_enable/SCDataMANAGER_inst/*
add wave -group FIFO_DRIVER0 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(0)/FDn/*
add wave -group BLOCK_WORD_COUNTER0 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(0)/FDn/Module_enable/BLOCK_WORD_COUNTER_inst/*

add wave -group CD_COUNTER1 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(1)/FDn/Module_enable/SCDataMANAGER_inst/g_trunc/CD_COUNTER_inst/*
add wave -group SCDataManager1 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(1)/FDn/Module_enable/SCDataMANAGER_inst/*
add wave -group FIFO_DRIVER1 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(1)/FDn/*
add wave -group BLOCK_WORD_COUNTER1 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(1)/FDn/Module_enable/BLOCK_WORD_COUNTER_inst/*

add wave -group CD_COUNTER2 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(2)/FDn/Module_enable/SCDataMANAGER_inst/g_trunc/CD_COUNTER_inst/*
add wave -group SCDataManager2 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(2)/FDn/Module_enable/SCDataMANAGER_inst/*
add wave -group FIFO_DRIVER2 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(2)/FDn/*
add wave -group BLOCK_WORD_COUNTER2 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(2)/FDn/Module_enable/BLOCK_WORD_COUNTER_inst/*

add wave -group CD_COUNTER3 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(3)/FDn/Module_enable/SCDataMANAGER_inst/g_trunc/CD_COUNTER_inst/*
add wave -group SCDataManager3 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(3)/FDn/Module_enable/SCDataMANAGER_inst/*
add wave -group FIFO_DRIVER3 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(3)/FDn/*
add wave -group BLOCK_WORD_COUNTER3 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(3)/FDn/Module_enable/BLOCK_WORD_COUNTER_inst/*

add wave -group CD_COUNTER4 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(4)/FDn/Module_enable/SCDataMANAGER_inst/g_trunc/CD_COUNTER_inst/*
add wave -group SCDataManager4 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(4)/FDn/Module_enable/SCDataMANAGER_inst/*
add wave -group FIFO_DRIVER4 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(4)/FDn/*
add wave -group BLOCK_WORD_COUNTER4 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(4)/FDn/Module_enable/BLOCK_WORD_COUNTER_inst/*

add wave -group CD_COUNTER5 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(5)/FDn/Module_enable/SCDataMANAGER_inst/g_trunc/CD_COUNTER_inst/*
add wave -group SCDataManager5 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(5)/FDn/Module_enable/SCDataMANAGER_inst/*
add wave -group FIFO_DRIVER5 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(5)/FDn/*
add wave -group BLOCK_WORD_COUNTER5 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(5)/FDn/Module_enable/BLOCK_WORD_COUNTER_inst/*

add wave -group CD_COUNTER6 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(6)/FDn/Module_enable/SCDataMANAGER_inst/g_trunc/CD_COUNTER_inst/*
add wave -group SCDataManager6 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(6)/FDn/Module_enable/SCDataMANAGER_inst/*
add wave -group FIFO_DRIVER6 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(6)/FDn/*
add wave -group BLOCK_WORD_COUNTER6 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(6)/FDn/Module_enable/BLOCK_WORD_COUNTER_inst/*

add wave -group CD_COUNTER7 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(7)/FDn/Module_enable/SCDataMANAGER_inst/g_trunc/CD_COUNTER_inst/*
add wave -group SCDataManager7 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(7)/FDn/Module_enable/SCDataMANAGER_inst/*
add wave -group FIFO_DRIVER7 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(7)/FDn/*
add wave -group BLOCK_WORD_COUNTER7 -position insertpoint sim:/centralrouter_tb/cr0/GBTdataManagers(0)/GBTdmN/GBTdmDown/generate_all/EgroupsNormalMode/Egroups(0)/EgN/PATH_FIFO_DRIVERs(7)/FDn/Module_enable/BLOCK_WORD_COUNTER_inst/*



add wave -group tb -position insertpoint \
 sim:/centralRouter_tb/block_data \
 sim:/centralRouter_tb/chunk_error0 \
 sim:/centralRouter_tb/TrailerError \
 sim:/centralRouter_tb/Trunc \
 sim:/centralRouter_tb/block_error0 \
 sim:/centralRouter_tb/backpressure_applied \
 sim:/centralrouter_tb/toHostFifo_wr_en \
 sim:/centralrouter_tb/toHostFifo_wr_clk \
 sim:/centralrouter_tb/toHostFifo_rst \
 sim:/centralrouter_tb/toHostFifo_prog_full \
 sim:/centralrouter_tb/toHostFifo_din

run -all
quit

#!/bin/bash
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Elena Zhivun
#               Frans Schreuder
#               Ohad Shaked
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#Installation path and license server at Nikhef. For other institutes, please try another location.
echo "free -h"
free -h
if [ -d /eda/fpga/mentor/questasim_2019.1/bin/ ]; then
    #Nikhef questasim installation path and license server.
    export PATH=/eda/fpga/mentor/questasim_2019.1/bin/:$PATH
    export LM_LICENSE_FILE=$LM_LICENSE_FILE
elif [ -d /afs/cern.ch/work/f/fschreud/public/questasim_2019.1/ ]; then
    #Cern questasim installation path and license server
    export PATH=/afs/cern.ch/work/f/fschreud/public/questasim_2019.1/linux_x86_64/:$PATH
    export MGLS_LICENSE_FILE=1717@lxlicen01,1717@lxlicen02,1717@lxlicen03,1717@lnxmics1,1717@lxlicen08
else
    echo "Could not find questasim installation path, exiting."
    exit 1
fi
    



if [ ! -d ../UVVM/ ]; then
  echo UVVM does not exist, copy from /project/et
  if [ -d /project/et/vhdllibs/v2019.1/UVVM ]; then
    cp -r /project/et/vhdllibs/v2019.1/UVVM ../UVVM
  elif [ -d /afs/cern.ch/work/f/fschreud/public/UVVM/ ]; then
    cp -r /afs/cern.ch/work/f/fschreud/public/UVVM ../UVVM
  else
    echo "Could not find UVVM library, exiting"
    exit 1
  fi
fi

if [ ! -d ../xilinx_simlib/ ]; then
  echo xilinx_simlib does not exist, copy from /project/et
  if [ -d /project/et/vhdllibs/v2019.1/xilinx_lib ]; then
    cp -r /project/et/vhdllibs/v2019.1/xilinx_lib ../xilinx_simlib
  elif [ -d /afs/cern.ch/work/f/fschreud/public/xilinx_simlib/ ]; then
    cp -r /afs/cern.ch/work/f/fschreud/public/xilinx_simlib/ ../xilinx_simlib
  else
    echo "Could not find xilinx libraries, exiting"
    exit 1
  fi
fi

#Disable egroup test, testbench needs to be replaced with new AXI stream testbench
TESTS="encodingepath"

for TEST in $TESTS 
do
  vsim -do ci-${TEST}.do
  mv transcript transcript-${TEST}
  if grep -q "Simulation SUCCESS: No mismatch between counted and expected serious alerts" transcript-${TEST}
then
      echo ${TEST} simulation successful
else
      echo ${TEST} sumulation ERROR
    exit 1
fi
done


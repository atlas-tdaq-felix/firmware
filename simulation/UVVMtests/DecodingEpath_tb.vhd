--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/15/2020 10:56:03 AM
-- Design Name: 
-- Module Name: DecodingEpath_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.centralRouter_package.all;
use work.pcie_package.all;
use work.axi_stream_package.all;

entity DecodingEpath_tb is
--  Port ( );
end DecodingEpath_tb;

architecture Behavioral of DecodingEpath_tb is

signal register_map_control    : register_map_control_type;
signal clk40, clk80, clk160, reset, GBTlinkValid: std_logic;
signal GBTData: std_logic_vector(119 downto 0);

constant clk40_period: time := 25 ns;
constant clk80_period: time := clk40_period/2;
constant clk160_period: time := clk40_period/4;

constant EGroup: integer := 0;
constant ElinkWidth: integer := 2;
signal EW: std_logic_vector(2 downto 0);

signal ElinkData: std_logic_vector(ElinkWidth-1 downto 0); 

signal m_axis : axis_32_type;  
signal m_axis_tready : std_logic;
signal m_axis_aresetn : std_logic;

signal DecoderAligned: std_logic;
signal AlignmentPulse: std_logic;

signal getDataTrig : std_logic;                
signal HDLCGenIn : std_logic_vector (9 downto 0);
signal HDLCGenValid : std_logic;               
signal HDLCGenOut : std_logic_vector(1 downto 0);

begin

clk40_proc: process
begin
    clk40 <= '0';
    wait for clk40_period/2;
    clk40 <= '1';
    wait for clk40_period/2;
end process;

clk80_proc: process
begin
    clk80 <= '0';
    wait for clk80_period/2;
    clk80 <= '1';
    wait for clk80_period/2;
end process;

clk160_proc: process
begin
    clk160 <= '0';
    wait for clk160_period/2;
    clk160 <= '1';
    wait for clk160_period/2;
end process;


reset_proc: process
begin
    reset <= '1';
    wait for clk40_period * 5;
    reset <= '0';
    wait;
end process;

m_axis_aresetn <= not reset;

--Enable emulator.
register_map_control.GBT_EMU_ENA.TOHOST <= "1";
register_map_control.GBT_EMU_CONFIG_WE_ARRAY <= (others => '0'); 
register_map_control.GBT_EMU_CONFIG.WRADDR <= (others => '0');
register_map_control.GBT_EMU_CONFIG.WRDATA <= (others => '0');

emu0: entity work.GBTdataEmulator
Port map(
    clk40                   => clk40,--: in  std_logic;
    wrclk                   => clk40,--: in  std_logic;
    rst_hw                  => reset,--: in  std_logic;
    rst_soft                => reset,--: in  std_logic;
    xoff                    => '0',--: in  std_logic;
    register_map_control    => register_map_control,--: in  register_map_control_type;
    
    GBTdata                 => GBTData,--: out std_logic_vector(119 downto 0);
    GBTlinkValid            => GBTLinkValid --: out std_logic    
    );
    
    ElinkData <= GBTData(((Egroup+2)*16)+ElinkWidth-1 downto ((Egroup+2)*16));
    with ElinkWidth select EW <= 
        "000" when 2, 
        "001" when 4, 
        "010" when 8, 
        "011" when 16, 
        "100" when 32, 
        "000" when others; 
        
hdlc_proc: process(clk160, reset)
    constant HDLCLength: integer := 14;
    variable hdlccnt: integer range 0 to HDLCLength+6;
    
begin
    if reset = '1' then
        hdlccnt := HDLCLength+5;
    elsif rising_edge(clk160) then
        
        
        HDLCGenIn(7 downto 0) <= x"FF"; --ignored.
        if hdlccnt = 0 then
            HDLCGenIn(9 downto 8) <= "10"; --SOC
        end if;
        if hdlccnt > 0 and hdlccnt < HDLCLength+1 then
            HDLCGenIn(9 downto 8) <= "00"; --Data
            HDLCGenIn(7 downto 0) <= std_logic_vector(to_unsigned(hdlccnt,8));
        end if;
        if hdlccnt = HDLCLength+1 then
            HDLCGenIn(9 downto 8) <= "01"; --EOC
        end if; 
        if hdlccnt > HDLCLength+1 then
            HDLCGenIn(9 downto 8) <= "11"; --Idle
        end if; 
        
        if getDataTrig = '1' then
            HDLCGenValid <= '1';            
            if hdlccnt < HDLCLength+6 then
                hdlccnt := hdlccnt + 1;
            else
                hdlccnt := 0;
            end if;
        end if;       
    end if;
end process;
        
hdlc_gen0: entity work.EPROC_OUT2_HDLC
    generic map(
        HDLC_IDLE_STATE => x"7F"--determine the HDLC line idle state. for EC: 0x7F, for IC: 0xFF
    )
    port map(
        bitCLK      => clk40,--: in  std_logic;
        bitCLKx2    => clk80,--: in  std_logic;
        bitCLKx4    => clk160,--: in  std_logic;
        rst         => reset,--: in  std_logic; 
        getDataTrig => getDataTrig,--: out std_logic;   
        edataIN     => HDLCGenIn,--: in  std_logic_vector (9 downto 0);
        edataINrdy  => HDLCGenValid,--: in  std_logic;
        EdataOUT    => HDLCGenOut--: out std_logic_vector(1 downto 0)
    );

ePath0: entity work.DecodingEpathGBT
 generic map(
  MAX_INPUT      => ElinkWidth,
  INCLUDE_8b     => '1', --: std_logic := '1';
  INCLUDE_4b     => '1', --: std_logic := '1';
  INCLUDE_2b     => '1', --: std_logic := '1';
  INCLUDE_8b10b  => '1', --: std_logic := '1';
  INCLUDE_HDLC   => '1', --: std_logic := '1';
  INCLUDE_DIRECT => '1', --: std_logic := '1';
  BLOCKSIZE      => 1024 --: integer := 1024
 )
 port map(
  clk40 => clk40,
  reset => reset,
  EpathEnable => '1',
  EpathEncoding => "10",
  ElinkWidth => EW,
  MsbFirst  => '1',
  ElinkData => HDLCGenOut,
  ElinkAligned => GBTLinkValid, 
  DecoderAligned => DecoderAligned,
  AlignmentPulse => AlignmentPulse,
  m_axis => m_axis,
  m_axis_tready => m_axis_tready,
  m_axis_aclk => clk40,
  m_axis_aresetn => m_axis_aresetn
  
  );
  
  m_axis_tready <= '1';

pulsegen0: entity work.AlignmentPulseGen
 generic map(
   MAX_VAL => 20480 --2048 bytes at 8b10b / 80 Mb/s elink.
   )
 port map(
  clk40 => clk40,
  AlignmentPulse => AlignmentPulse
  
  );

end Behavioral;

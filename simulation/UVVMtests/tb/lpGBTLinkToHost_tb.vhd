--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;

    use std.env.all;
    use work.axi_stream_package.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;
    use work.interlaken_package.slv_67_array;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

library bitvis_vip_axistream;
    use bitvis_vip_axistream.axistream_bfm_pkg.all;
    use bitvis_vip_axistream.vvc_methods_pkg.all;
    context bitvis_vip_axistream.vvc_context;
library bitvis_vip_scoreboard; -- @suppress "Library 'bitvis_vip_scoreboard' is not available"
library uvvm_vvc_framework;
    use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

entity lpGBTLinkToHost_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end lpGBTLinkToHost_tb;

architecture tb of lpGBTLinkToHost_tb is
    -- Interrupt related signals
    signal clock_ena     : boolean   := false;
    signal ElinkWidth : std_logic_vector(2 downto 0);

    constant C_CLK_PERIOD_40     : time := 25 ns;
    constant C_CLK_PERIOD_80     : time := 12.5 ns;
    constant C_CLK_PERIOD_160     : time := 6.25 ns;
    constant C_CLK_PERIOD_240    : time := 4.167 ns;
    constant C_CLK_PERIOD_250    : time := 4.000 ns;
    constant GBT_NUM : integer := 6;
    constant LINK_NUM : integer := GBT_NUM;
    constant NUMBER_OF_DESCRIPTORS : integer := 2;
    constant LINK_CONFIG: IntArray(0 to GBT_NUM-1) := (others => 0);

    constant USE_URAM : boolean := false;

    signal clk40, clk80, clk160, clk240, clk250: std_logic;
    signal reset: std_logic;

    signal DataIn32b : std_logic_vector(31 downto 0);
    signal m_axis: axis_32_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST_MODE(FIRMWARE_MODE_LPGBT)-1);
    signal m_axis_tready: axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST_MODE(FIRMWARE_MODE_LPGBT)-1);
    signal m_axis_prog_empty: axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST_MODE(FIRMWARE_MODE_LPGBT)-1); -- @suppress "signal m_axis_prog_empty is never read"
    constant MessageLength: integer:= 32;
    signal StartFrameGen: std_logic;
    signal NumberOfMessages: integer;
    constant TTC_ToHost_Data_in : TTC_data_type := TTC_zero;
    signal lpGBT_UPLINK_USER_DATA : array_224b(0 to GBT_NUM-1);
    signal lpGBT_UPLINK_EC_DATA : array_2b(0 to GBT_NUM-1);
    signal lpGBT_UPLINK_IC_DATA : array_2b(0 to GBT_NUM-1);
    signal LinkAligned : std_logic_vector(GBT_NUM-1 downto 0);
    constant ElinkBusyIn : array_57b(0 to GBT_NUM-1) := (others => (others => '0'));
    constant DmaBusyIn : std_logic := '0';
    constant FifoBusyIn : std_logic := '0';
    constant BusySumIn : std_logic := '0';
    signal m_axis_aux : axis_32_array_type(0 to 1);
    signal m_axis_aux_prog_empty : axis_tready_array_type(0 to 1);
    signal m_axis_aux_tready : axis_tready_array_type(0 to 1);
    signal register_map_control : register_map_control_type;
    signal register_map_decoding_monitor : register_map_decoding_monitor_type;
    signal register_map_xoff_monitor : register_map_xoff_monitor_type; -- @suppress "signal register_map_xoff_monitor is never read"
    signal register_map_crtohost_monitor : register_map_crtohost_monitor_type; -- @suppress "signal register_map_crtohost_monitor is never read"
    signal s_axis64 : axis_64_array_type(0 to LINK_NUM-1); -- @suppress "signal s_axis64 is never written"
    signal s_axis64_tready : axis_tready_array_type(0 to LINK_NUM-1); -- @suppress "signal s_axis64_tready is never read"
    signal s_axis64_prog_empty : axis_tready_array_type(0 to LINK_NUM-1); -- @suppress "signal s_axis64_prog_empty is never written"
    signal toHostFifo_din : slv_array(0 to NUMBER_OF_DESCRIPTORS-2);
    signal toHostFifo_wr_en : std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
    signal toHostFifo_prog_full : std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
    signal toHostFifo_wr_clk : std_logic;
    signal toHostFifo_rst : std_logic;
    signal checker_done : std_logic;
    signal ElinkAligned : std_logic_vector(3 downto 0);
    signal aclk : std_logic;
    signal clk40_stable : std_logic;
    signal daq_reset : std_logic;
    signal daq_fifo_flush : std_logic;

--signal Interlaken_RX_Data       : slv_67_array(0 to GBT_NUM - 1);
--signal Interlaken_RX_Datavalid  : std_logic_vector(GBT_NUM - 1 downto 0);
--signal toHost_axis64_aclk       : std_logic;
--signal toHost_axis64            : axis_64_array_type(0 to GBT_NUM - 1);
--signal toHost_axis64_tready     : axis_tready_array_type(0 to GBT_NUM- 1);
--signal toHost_axis64_prog_empty : axis_tready_array_type(0 to GBT_NUM - 1);

begin
    -- Instantiate the concurrent procedure that initializes UVVM
    i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

    -----------------------------------------------------------------------------
    -- Clock Generator
    -----------------------------------------------------------------------------
    clock_generator(clk40, clock_ena, C_CLK_PERIOD_40, "40 MHz clock");
    clock_generator(clk80, clock_ena, C_CLK_PERIOD_80, "80 MHz clock");
    clock_generator(clk160, clock_ena, C_CLK_PERIOD_160, "160 MHz clock");
    clock_generator(clk240, clock_ena, C_CLK_PERIOD_240, "240 MHz clock");
    clock_generator(clk250, clock_ena, C_CLK_PERIOD_250, "250 MHz clock");


    framegen0: entity work.DecEgroup_8b10b_framegen
        generic map(
            UseFELIXDataFormat => true
        )
        port map(
            clk40 => clk40,
            clk80 => clk80,
            clk160 => clk160,
            reset => daq_reset,
            strips_mode_en => '0', --MRMW
            direct_mode_en => '0', --MRMW
            IDLES => 20,
            MessageLength => MessageLength,
            ElinkWidth => ElinkWidth,
            DataOut32b => DataIn32b,
            StartFrameGen => StartFrameGen,
            NumberOfMessages => NumberOfMessages,
            HGTD_ALTIROC_ENCODING => '0'
        );

    --All E-groups get the same 32-bit data from framegen.
    g_lpGBT_data: for link in 0 to LINK_NUM-1 generate
        g_egroup: for egr in 0 to 6 generate
            lpGBT_UPLINK_USER_DATA(link)(egr*32+31 downto egr*32) <= DataIn32b;
        end generate;
        lpGBT_UPLINK_EC_DATA(link) <= "00";
        lpGBT_UPLINK_IC_DATA(link) <= "00";
        LinkAligned(link) <= '1';
    end generate;

    g_regmap: for link in 0 to LINK_NUM-1 generate
        g_egroup: for egr in 0 to 6 generate
            register_map_control.DECODING_EGROUP_CTRL(link)(egr).ENABLE_TRUNCATION <= "1";
            register_map_control.DECODING_EGROUP_CTRL(link)(egr).EPATH_ENA <= x"0F";
            register_map_control.DECODING_EGROUP_CTRL(link)(egr).EPATH_WIDTH <= ElinkWidth;
            register_map_control.DECODING_EGROUP_CTRL(link)(egr).PATH_ENCODING <= x"00001111"; --8b10b
            register_map_control.DECODING_EGROUP_CTRL(link)(egr).REVERSE_ELINKS <= "00000000";
        end generate;
        register_map_control.CRTOHOST_INSTANT_TIMEOUT_ENA(link) <= "00" & x"000" & x"0000000"; --No instant timeout, that causes truncation
    end generate;
    register_map_control.DECODING_DISEGROUP <= "0000000";
    register_map_control.DECODING_REVERSE_10B <= "1";
    register_map_control.TIMEOUT_CTRL.ENABLE <= "1";
    register_map_control.TIMEOUT_CTRL.TIMEOUT <= x"00000FFF";
    register_map_control.SUPER_CHUNK_FACTOR_LINK <= (others => x"01");
    register_map_control.DECODING_HGTD_ALTIROC <= "0";
    register_map_control.ELINK_REALIGNMENT.ENABLE <= "1";

    decoding0: entity work.decoding
        generic map(
            CARD_TYPE => 712,
            GBT_NUM => GBT_NUM,
            FIRMWARE_MODE => FIRMWARE_MODE_LPGBT,
            STREAMS_TOHOST => STREAMS_TOHOST_MODE(FIRMWARE_MODE_LPGBT),
            BLOCKSIZE => 1024,
            LOCK_PERIOD => 20480,
            IncludeDecodingEpath2_HDLC => "0000000",
            IncludeDecodingEpath2_8b10b => "0000000",
            IncludeDecodingEpath4_8b10b => "0000000",
            IncludeDecodingEpath8_8b10b =>  "1111111",
            IncludeDecodingEpath16_8b10b => "1111111",
            IncludeDecodingEpath32_8b10b => "1111111",
            IncludeDirectDecoding => "1111111",
            RD53Version => "A",
            PCIE_ENDPOINT => 0,
            VERSAL => false,
            AddFULLMODEForDUNE => false
        )
        port map(
            RXUSRCLK => (others => clk240),
            FULL_UPLINK_USER_DATA => (others => (others => '0')),
            GBT_UPLINK_USER_DATA => (others => (others => '0')),
            lpGBT_UPLINK_USER_DATA => lpGBT_UPLINK_USER_DATA,
            lpGBT_UPLINK_EC_DATA => lpGBT_UPLINK_EC_DATA,
            lpGBT_UPLINK_IC_DATA => lpGBT_UPLINK_IC_DATA,
            LinkAligned => LinkAligned,
            clk160 => clk160,
            clk240 => clk240,
            clk250 => clk250,
            clk40 => clk40,
            clk365                        => '0',
            aclk_out => aclk,
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            m_axis => m_axis,
            m_axis_tready => m_axis_tready,
            m_axis_prog_empty => m_axis_prog_empty,
            m_axis_noSC => open,
            m_axis_noSC_tready => (others => (others => '0')),
            m_axis_noSC_prog_empty => open,
            TTC_ToHost_Data_in => TTC_ToHost_Data_in,
            TTCin => TTC_zero,
            FE_BUSY_out => open,
            ElinkBusyIn => ElinkBusyIn,
            DmaBusyIn => DmaBusyIn,
            FifoBusyIn => FifoBusyIn,
            BusySumIn => BusySumIn,
            m_axis_aux => m_axis_aux,
            m_axis_aux_prog_empty => m_axis_aux_prog_empty,
            m_axis_aux_tready => m_axis_aux_tready,
            register_map_control => register_map_control,
            register_map_decoding_monitor => register_map_decoding_monitor,
            Interlaken_RX_Data_In         => (others => (others => '0')),
            Interlaken_RX_Datavalid       => (others => '0'),
            Interlaken_RX_Gearboxslip     => open,
            Interlaken_Decoder_Aligned_out => open,
            m_axis64                      => open,
            m_axis64_tready               => (others => '0'),
            m_axis64_prog_empty           => open,
            toHost_axis64_aclk_out        => open
        );

    resetmgr0: entity work.CRresetManager
        port map(
            clk40         => clk40,
            rst           => reset,
            clk40_stable  => clk40_stable,
            cr_rst        => daq_reset,
            cr_fifo_flush => daq_fifo_flush
        );

    g_URAM: if USE_URAM generate
        g_axis32: if LINK_CONFIG(0) = 0 generate
            toHostFifo_wr_clk <= aclk;
        else generate
            toHostFifo_wr_clk <= clk250;
        end generate;
    else generate --BRAM FIFO's in Virtex7 and Kintex Ultrascale
        toHostFifo_wr_clk <= clk160;
    end generate;

    cr0: entity work.CRToHost
        generic map(
            NUMBER_OF_DESCRIPTORS => NUMBER_OF_DESCRIPTORS,
            NUMBER_OF_INTERRUPTS => 8,
            LINK_NUM => GBT_NUM,
            LINK_CONFIG => LINK_CONFIG,
            toHostTimeoutBitn => 16,
            STREAMS_TOHOST => STREAMS_TOHOST_MODE(FIRMWARE_MODE_LPGBT),
            BLOCKSIZE => 1024,
            DATA_WIDTH => 256,
            FIRMWARE_MODE => FIRMWARE_MODE_LPGBT,
            USE_URAM => USE_URAM
        )
        port map(
            clk40 => clk40,
            aclk_tohost => aclk,
            aclk64_tohost => '0',
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            register_map_control => register_map_control,
            register_map_xoff_monitor => register_map_xoff_monitor,
            register_map_crtohost_monitor => register_map_crtohost_monitor,
            interrupt_call => open,
            xoff_out => open,
            s_axis => m_axis,
            s_axis_tready => m_axis_tready,
            s_axis_prog_empty => m_axis_prog_empty,
            s_axis_aux => m_axis_aux,
            s_axis_aux_tready => m_axis_aux_tready,
            s_axis_aux_prog_empty => m_axis_aux_prog_empty,
            s_axis64 => s_axis64,
            s_axis64_tready => s_axis64_tready,
            s_axis64_prog_empty => s_axis64_prog_empty,
            dma_enable_in => (others => '1'),
            toHostFifo_din => toHostFifo_din,
            toHostFifo_wr_en => toHostFifo_wr_en,
            toHostFifo_prog_full => toHostFifo_prog_full,
            toHostFifo_wr_clk => toHostFifo_wr_clk,
            toHostFifo_rst => toHostFifo_rst
        );

    sink0: entity work.FELIXDataSink
        generic map(
            NUMBER_OF_DESCRIPTORS => NUMBER_OF_DESCRIPTORS,
            BLOCKSIZE => 1024,
            GBT_NUM => GBT_NUM,
            FIRMWARE_MODE => FIRMWARE_MODE_LPGBT,
            DATA_WIDTH => 256,
            SIM_NUMBER_OF_BLOCKS => 1000,
            APPLY_BACKPRESSURE_AFTER_BLOCKS => 500
        )
        port map(
            checker_done => checker_done,
            toHostFifo_din => toHostFifo_din,
            toHostFifo_wr_en => toHostFifo_wr_en,
            toHostFifo_prog_full => toHostFifo_prog_full,
            toHostFifo_wr_clk => toHostFifo_wr_clk,
            reset => toHostFifo_rst,
            start_check => open,
            Trunc => open,
            TrailerError => open,
            CRCError => open,
            ChunkLengthError => open,
            register_map_control => register_map_control
        );

    ElinkAligned <= register_map_decoding_monitor.DECODING_LINK_ALIGNED(0)(3 downto 0);
    ------------------------------------------------
    -- PROCESS: p_main
    ------------------------------------------------
    p_main: process
        constant C_SCOPE     : string  := C_TB_SCOPE_DEFAULT;
    begin
        clk40_stable <= '0';
        NumberOfMessages <= -1;
        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);
        enable_log_msg(ALL_MESSAGES);
        disable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        enable_log_msg(ID_SEQUENCER);
        -- Wait for UVVM to finish initialization
        await_uvvm_initialization(VOID);
        clock_ena <= true;
        clk40_stable <= '1';
        reset <= '1';
        wait for 25 ns;
        reset <= '0';
        wait for 25 ns;
        for i in 0 to 3 loop
            shared_axistream_vvc_config(i).bfm_config.max_wait_cycles := 10000;
            shared_axistream_vvc_config(i).bfm_config.max_wait_cycles_severity := ERROR;
            shared_axistream_vvc_config(i).bfm_config.clock_period := C_CLK_PERIOD_160;
            shared_axistream_vvc_config(i).bfm_config.setup_time := C_CLK_PERIOD_160/4;
            shared_axistream_vvc_config(i).bfm_config.hold_time  := C_CLK_PERIOD_160/4;
            shared_axistream_vvc_config(i).bfm_config.clock_period_margin := 0 ns;
            shared_axistream_vvc_config(i).bfm_config.bfm_sync := SYNC_WITH_SETUP_AND_HOLD;
            shared_axistream_vvc_config(i).bfm_config.match_strictness := MATCH_EXACT;
            shared_axistream_vvc_config(i).bfm_config.byte_endianness := FIRST_BYTE_LEFT;
            shared_axistream_vvc_config(i).bfm_config.check_packet_length := false;
            shared_axistream_vvc_config(i).bfm_config.protocol_error_severity := ERROR;
        end loop;

        log(ID_LOG_HDR, "Simulation of TB for ByteToAxiStream", C_SCOPE);
        ------------------------------------------------------------
        for w in 0 to 2 loop
            case w is
                when 0 =>
                    ElinkWidth <= "010"; --8b.
                when 1 =>
                    ElinkWidth <= "011"; --16b.
                when 2 =>
                    ElinkWidth <= "100"; --32b.
            end case;
            reset <= '1';
            wait for 25 ns;
            reset <= '0';
            wait for 25 ns;

            if ElinkAligned /= "0000" then
                await_value(ElinkAligned, "0000",0 ps, 1 ms, "Waiting for decoder to misalign");
            end if;
            case w is
                when 0 =>
                    await_value(ElinkAligned, "1111",0 ps, 1 ms, "Waiting for decoder to align");
                when 1 =>
                    await_value(ElinkAligned, "0101",0 ps, 1 ms, "Waiting for decoder to align");
                when 2 =>
                    await_value(ElinkAligned, "0001",0 ps, 1 ms, "Waiting for decoder to align");
            end case;

            StartFrameGen <= '1';
            await_value(checker_done, '1', 0 ps, 100 ms, "Waiting for FELIXDataSink to be done");
            StartFrameGen <= '0';
            wait for 25 ns;

        end loop;
        --wait for 500 us;

        --==================================================================================================
        -- Ending the simulation
        --------------------------------------------------------------------------------------
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        -- Finish the simulation
        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely

    end process p_main;

end  architecture;

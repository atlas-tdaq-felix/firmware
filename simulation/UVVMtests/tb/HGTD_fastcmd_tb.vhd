--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!               Filiberto Bonini
--!               Nico Giangiacomi
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- file: GBTCrCoding_tb.vhd.
-- Testbench to excercise CrFromHost, CRToHost, encoding, decoding in GBT mode
-- Using data derived from fuptest.
----------------------------------------------------------------------------------
library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

    use work.pcie_package.all;
    use work.lpgbtfpga_package.all;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.axi_stream_package.all;



-- Test bench entity
entity HGTD_fastcmd_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity;

-- Test bench architecture
architecture arch of HGTD_fastcmd_tb is

    constant C_SCOPE : string := "HGTD_fastcmd_tb";
    constant C_CLK240_PERIOD : time    := 4.158 ns;
    constant C_CLK40_PERIOD  : time    := C_CLK240_PERIOD*6;
    constant C_CLK160_PERIOD : time    := C_CLK40_PERIOD/4;
    constant C_CLK25_PERIOD  : time    := 40 ns;
    --constant C_CLK250_PERIOD : time    := 4 ns;

    signal register_map_40_control             : register_map_control_type;

    signal clk40, clk160, clk25 : std_logic:='0';
    signal clk160p, clk160n: std_logic;
    signal clock_ena     : boolean   := false;

    signal reset : std_logic;
    signal aresetn : std_logic;

    --signal checker_done: std_logic; -- @suppress "signal checker_done is never written"

    signal TTCin: TTC_data_type;

    signal TTC_ToHost_Data : TTC_data_type; -- @suppress "signal TTC_ToHost_Data is never read"
    signal register_map_ttc_monitor : register_map_ttc_monitor_type; -- @suppress "signal register_map_ttc_monitor is never read"

    signal FastCmd: std_logic_vector(7 downto 0);

    constant CMD_IDLEV2      : std_logic_vector(7 downto 0) := "10101100"; -- IDLE frame for ALTIROC v2
    constant CMD_IDLEV3      : std_logic_vector(7 downto 0) := "11110000"; -- IDLE frame for ALTIROC v3 -- @suppress "Unused declaration"
    constant CMD_IDLE: std_logic_vector(7 downto 0) := CMD_IDLEV2;
    constant CMD_TRIGGER   : std_logic_vector(7 downto 0) := "10110010"; -- L0 or L1 trigger
    constant CMD_BCR       : std_logic_vector(7 downto 0) := "10011001"; -- Bunch Counter Reset
    constant CMD_TRIGBCR   : std_logic_vector(7 downto 0) := "01101001"; -- Trigger and BCR
    constant CMD_CAL       : std_logic_vector(7 downto 0) := "11010100"; -- Calibration Pulse
    constant CMD_GBRST     : std_logic_vector(7 downto 0) := "11001010"; -- Global Reset
    constant CMD_SYNCLUMI  : std_logic_vector(7 downto 0) := "01100110"; -- Synchronize luminosity stream
    constant CMD_SETTRIGID : std_logic_vector(7 downto 0) := "01010011"; -- Set Trigger ID
    constant CMD_TRIGID    : std_logic_vector(7 downto 0) := "01000001"; -- Trigger ID
    constant TRIG_IDLES : integer := 10; -- Number of IDLES between CAL and TRIGGER.
    type HGTD_FASTCMD_type is (IDLE, TRIGGER, BCR, TRIGBCR, CAL, GBRST, SYNCLUMI, SETTRIGID, TRIGID, UNKNOWN);
    signal HGTD_FASTCMD: HGTD_FASTCMD_type; -- @suppress "signal HGTD_FASTCMD is never read"

    signal DECODED_TRIGID_4B: std_logic_vector(3 downto 0); -- @suppress "signal DECODED_TRIGID_4B is never read"

    signal SEND_SYNCLUMI, SEND_GBRST: std_logic;
    signal L1A_BCR_ECR : std_logic_vector(2 downto 0); -- @suppress "signal L1A_BCR_ECR is never read"
    type slv8_array is array(0 to 2047) of std_logic_vector(7 downto 0);
    signal expected_values: slv8_array;
begin



    reset_proc: process
    begin
        reset <= '1';
        wait for C_CLK40_PERIOD*15;
        reset <= '0';
        wait;
    end process;

    aresetn     <= not reset;

    register_map_40_control.TTC_EMU_CONTROL.L1A <= "0";
    register_map_40_control.TTC_EMU_CONTROL.BROADCAST <= "000000";
    register_map_40_control.TTC_EMU_CONTROL.BCR <= "0";
    register_map_40_control.TTC_EMU_CONTROL.BUSY_IN_ENABLE <= "0";
    register_map_40_control.TTC_EMU_CONTROL.ECR <= "1";

    register_map_40_control.TTC_EMU.SEL <= "1";
    register_map_40_control.TTC_EMU.ENA <= "1";

    register_map_40_control.TTC_EMU_BCR_PERIOD <= x"00001000";
    register_map_40_control.TTC_EMU_L1A_PERIOD <= x"00000100";
    register_map_40_control.TTC_EMU_ECR_PERIOD <= x"00010000";
    register_map_40_control.TTC_EMU_LONG_CHANNEL_DATA <= x"00000000";

    register_map_40_control.TTC_EMU_RESET(64) <= reset;
    register_map_40_control.TTC_DEC_CTRL.TOHOST_RST <= (others => reset);

    register_map_40_control.TTC_DEC_CTRL.B_CHAN_DELAY <= x"0";
    register_map_40_control.TTC_DEC_CTRL.TT_BCH_EN <= "1";

    register_map_40_control.HGTD_ALTIROC_FASTCMD.ALTIROC3_IDLE <= "0";
    register_map_40_control.HGTD_ALTIROC_FASTCMD.USE_CAL <= "1";
    register_map_40_control.HGTD_ALTIROC_FASTCMD.SYNCLUMI <= (others => SEND_SYNCLUMI);
    register_map_40_control.HGTD_ALTIROC_FASTCMD.GBRST <= (others => SEND_GBRST);
    register_map_40_control.HGTD_ALTIROC_FASTCMD.TRIG_DELAY <= std_logic_vector(to_unsigned(TRIG_IDLES,11)); --5 BC between CAL and IDLE
    register_map_40_control.DECODING_HGTD_ALTIROC <= "1";

    clk160p <= clk160;
    clk160n <= not clk160;

    ttc0: entity work.ttc_wrapper
        generic map(
            CARD_TYPE => 712,
            ISTESTBEAM => false,
            ITK_TRIGTAG_MODE => -1,
            FIRMWARE_MODE => 0,
            GBT_NUM => 1
        )
        port map(
            CLK_TTC_P => (others => clk160p),
            CLK_TTC_N => (others => clk160n),
            DATA_TTC_P => "0",
            DATA_TTC_N => "1",
            TB_trigger => "0",
            LOL_ADN => "1",
            LOS_ADN => "1",
            register_map_control => register_map_40_control,
            register_map_ttc_monitor => register_map_ttc_monitor,
            register_map_control_appreg_clk => register_map_40_control,
            appreg_clk => clk25,
            TTC_out => TTCin,
            clk_adn_160 => open,
            clk_ttc_40 => open,
            clk40 => clk40,
            BUSY => open,
            cdrlocked_out => open,
            TTC_ToHost_Data_out => TTC_ToHost_Data,
            TTC_BUSY_mon_array => (others => (others => '0')),
            BUSY_IN => '0',
            toHostXoff => (others => '0'),
            LTI_FE_TXUSRCLK_in => (others => '0'),
            LTI_FE_Data_Transceiver_out => open,
            LTI_FE_CharIsK_out => open
        );

    fastcmd0: entity work.HGTD_Altiroc_fastcmd
        port map(
            clk40   => clk40,
            TTCin   => TTCin,
            FastCmd => FastCmd,
            HGTD_ALTIROC_FASTCMD_CONTROL => register_map_40_control.HGTD_ALTIROC_FASTCMD
        );

    HGTD_decode_proc: process(FastCmd)
    begin

        DECODED_TRIGID_4B <= "XXXX";

        case FastCmd is

            when CMD_IDLE       => HGTD_FASTCMD <= IDLE;
            when CMD_TRIGGER    => HGTD_FASTCMD <= TRIGGER;
            when CMD_BCR        => HGTD_FASTCMD <= BCR;
            when CMD_TRIGBCR    => HGTD_FASTCMD <= TRIGBCR;
            when CMD_CAL        => HGTD_FASTCMD <= CAL;
            when CMD_GBRST      => HGTD_FASTCMD <= GBRST;
            when CMD_SYNCLUMI   => HGTD_FASTCMD <= SYNCLUMI;
            when CMD_SETTRIGID  => HGTD_FASTCMD <= SETTRIGID;
            when others =>
                HGTD_FASTCMD <= UNKNOWN;
                if FastCmd(7 downto 6) = CMD_TRIGID(7 downto 6) and
                   FastCmd(1 downto 0) = CMD_TRIGID(1 downto 0) then
                    HGTD_FASTCMD <= TRIGID;
                    DECODED_TRIGID_4B <= FastCmd(5 downto 2);
                end if;
        end case;


    end process;

    clock_generator(clk25, clock_ena, C_CLK25_PERIOD, "25 MHz clock");
    clock_generator(clk40, clock_ena, C_CLK40_PERIOD, "40 MHz clock");
    clock_generator(clk160, clock_ena, C_CLK160_PERIOD, "160 MHz clock");

    L1A_BCR_ECR <= TTCin.L1A & TTCin.BCR & TTCin.ECR;

    expected_value_proc: process(clk40)
        variable SEND_SYNCLUMI_p1, SEND_GBRST_p1: std_logic;
    begin
        if rising_edge(clk40) then
            if aresetn = '0' then
                expected_values <= (others => CMD_IDLE);
                SEND_SYNCLUMI_p1 := '0';
                SEND_GBRST_p1 := '0';
            else
                for i in 0 to expected_values'high-2 loop
                    expected_values(i) <= expected_values(i+1);
                end loop;
                if TTCin.L1A = '1' then
                    expected_values(1) <= CMD_CAL;
                    expected_values(2+TRIG_IDLES) <= CMD_TRIGGER;
                elsif TTCin.BCR = '1' then
                    expected_values(1) <= CMD_BCR;
                elsif TTCin.ECR = '1' then
                    expected_values(1) <= CMD_SETTRIGID;
                    expected_values(2) <= CMD_TRIGID;
                    expected_values(3) <= CMD_TRIGID;
                    expected_values(4) <= CMD_TRIGID;
                elsif SEND_SYNCLUMI = '1' and SEND_SYNCLUMI_p1 = '0' then
                    expected_values(2) <= CMD_SYNCLUMI;
                elsif SEND_GBRST = '1' and SEND_GBRST_p1 = '0' then
                    expected_values(2) <= CMD_GBRST;
                end if;
                SEND_SYNCLUMI_p1 := SEND_SYNCLUMI;
                SEND_GBRST_p1 := SEND_GBRST;
            end if;
        end if;
    end process;

    sequencer : process
    begin
        SEND_SYNCLUMI <= '0';
        SEND_GBRST <= '0';
        clock_ena <= false;
        -- Print the configuration to the log
        --wait for 1 ns;
        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);

        --enable_log_msg(ALL_MESSAGES);
        disable_log_msg(ALL_MESSAGES, NON_QUIET, C_SCOPE);
        enable_log_msg(ID_SEQUENCER, NON_QUIET, C_SCOPE);
        disable_log_msg(ID_POS_ACK, NON_QUIET, C_SCOPE);


        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "----- initializing test bench       ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        -- Wait for UVVM to finish initialization
        --await_uvvm_initialization(VOID);

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "-----    end of initialisation        ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        wait for (10*C_CLK40_PERIOD);
        clock_ena <= true;
        wait for (1*C_CLK40_PERIOD);
        await_value(aresetn, '1', 0 ns, 10*C_CLK40_PERIOD,  ERROR, C_SCOPE);

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "-- test bench sequence starts here      ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);


        log(ID_LOG_HDR, "Starting simulation of TB for EGROUP using VVCs", C_SCOPE);
        ------------------------------------------------------------

        --await_value(checker_done, '1', 0 ns, 1000 ms, TB_ERROR, "wait for all tests to finish", C_SCOPE);
        enable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        enable_log_msg(ID_SEQUENCER);
        enable_log_msg(ID_UVVM_SEND_CMD);


        for i in 0 to 100000 loop
            check_value(FastCmd, expected_values(0), ERROR, "FastCmd have the expected value", C_SCOPE);
            wait for C_CLK40_PERIOD;
            if i = 12345 then
                SEND_SYNCLUMI <= '1';
            end if;
            if i = 20 then
                SEND_GBRST <= '1';
            end if;
        end loop;


        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely
    --
    end process; -- sequencer

end architecture;


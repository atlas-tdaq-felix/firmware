--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Kazuki Todome
--!               Nico Giangiacomi
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--  ATLAS
--  FELIX UVVM test automation project
--  F. Schreuder
--  Nikhef
--  January 2020
----------------------------------------------------------------------------------
-- file: DecodingGearBox_tb.vhd.
----------------------------------------------------------------------------------
library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use work.axi_stream_package.all;
    use work.FELIX_package.all;

    use std.env.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

--library uvvm_vvc_framework;
--use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

--use work.vvc_methods_pkg.all;
--use work.td_vvc_framework_common_methods_pkg.all;
--use work.egr_bfm_pkg.all;


-- Test bench entity
entity EncodingEpath_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end EncodingEpath_tb;

architecture tb of EncodingEpath_tb is

    signal clk40        : std_logic:= '0';
    signal clk40_en     : boolean   := false;
    constant clk40_period : time := 25 ns;
    signal s_axis_aclk_i : std_logic:= '0';
    signal s_axis_aclk_en     : boolean   := false;
    constant axiclk_period : time := 10 ns;
    signal reset        : std_logic:= '0';

    signal   ElinkData_i : std_logic_vector(7 downto 0);
    signal   ElinkWidth_i: std_logic_vector(1 downto 0) :="00";
    signal   DecodingEpathWidth_i : std_logic_vector(2 downto 0);
    signal   EpathEncoding_i: std_logic_vector(3 downto 0) := x"1";
    constant    XOff_i    : std_logic := '0';
    signal s_axis_i :  axis_8_type;
    signal s_axis_tready_i : std_logic; -- @suppress "signal s_axis_tready_i is never read"
    signal   TTCOption_i: std_logic_vector(3 downto 0) :=(others => '0');
    signal   TTCOption_control: std_logic_vector(3 downto 0) :=(others => '0');
    signal   TTCin_i : TTC_data_type := TTC_zero;

    signal   TTCin_temp : std_logic_vector(16 downto 0) :=(others => '0');

    --file OUTPUT, OUTPUT2 : text;

    signal reading, finish_read: std_logic := '0';
    signal ttc_wrote: std_logic := '0';
    --signal max : integer := 255;
    signal max : integer := 33;
    signal combined_data : std_logic_vector(7 downto 0);
    signal word_length : integer:=0;

    signal DataOut_tmp : std_logic_vector(7 downto 0); -- @suppress "signal DataOut_tmp is never written"
    signal data_two_words : std_logic_vector(15 downto 0);
    signal data_10b : std_logic_vector(9 downto 0); -- @suppress "signal data_10b_rev is never read"
    signal data_10b_rev : std_logic_vector(9 downto 0); -- @suppress "signal data_10b_rev is never read"
    signal AlignmentPulseAlign : std_logic;
    signal AlignmentPulseDeAlign : std_logic;
    signal DecoderAligned : std_logic;
    signal m_axis: axis_32_type;

    signal data_HDLC : std_logic_vector(7 downto 0);
    signal data_HDLC_valid : std_logic;
    signal clk40_stable : std_logic;
    signal daq_reset : std_logic;
    signal daq_fifo_flush : std_logic;



--signal debug_flag: std_logic;

begin
    gen: for i in 0 to 9 generate
        data_10b_rev(i) <= data_10b(9-i);
    end generate;
    -----------------------------------------------------------------------------
    -- Instantiate DUT
    -----------------------------------------------------------------------------

    EncodingEpathGBT_uut: entity work.EncodingEpathGBT
        generic Map(
            MAX_OUTPUT => 8,
            INCLUDE_8b => '1',
            INCLUDE_4b => '1',
            INCLUDE_2b => '1',
            INCLUDE_8b10b => '1',
            INCLUDE_HDLC => '1',
            INCLUDE_DIRECT => '1',
            INCLUDE_TTC => '1',
            --BLOCKSIZE => 1024,
            GENERATE_FEI4B => false,
            GENERATE_LCB_ENC => false,
            HDLC_IDLE_STATE => x"7F",
            USE_BUILT_IN_FIFO => '1',
            SUPPORT_HDLC_DELAY => false,
            DISTR_RAM => false,
            INCLUDE_XOFF => false
        )
        port Map(
            clk40 => clk40,
            daq_reset => reset,
            daq_fifo_flush => daq_fifo_flush,
            EpathEnable => '1',
            EpathEncoding => EpathEncoding_i,
            ElinkWidth => ElinkWidth_i,
            MsbFirst => '1',
            ReverseOutputBits => '0',
            ElinkData => ElinkData_i,
            toHostXoff => XOff_i,
            epath_almost_full => open,
            s_axis => s_axis_i,
            s_axis_tready => s_axis_tready_i,
            s_axis_aclk => s_axis_aclk_i,
            EnableHDLCDelay => '0',
            TTCOption => TTCOption_i,
            TTCin => TTCin_i,
            FEI4Config => (others => '0')
        );

    DecodingEpathWidth_i <= '0'&ElinkWidth_i;

    DecodingEpathGBT_uut: entity work.DecodingEpathGBT
        generic map(
            MAX_INPUT => 8, --: integer := 8;
            INCLUDE_16b => '0',
            INCLUDE_8b => '1', --: std_logic := '1';
            INCLUDE_4b => '1', --: std_logic := '1';
            INCLUDE_2b => '1', --: std_logic := '1';
            INCLUDE_8b10b => '1', --: std_logic := '1';
            INCLUDE_HDLC => '1', --: std_logic := '1';
            INCLUDE_DIRECT => '1', --: std_logic := '1';
            BLOCKSIZE => 1024, --: integer := 1024
            USE_BUILT_IN_FIFO => '1',
            GENERATE_FEI4B => false,
            VERSAL => false)
        port map(
            clk40             => clk40,--: in std_logic; --BC clock for DataIn
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            EpathEnable       => '1',--: in std_logic; --From register map
            EpathEncoding     => EpathEncoding_i,--: in std_logic_vector(3 downto 0); --0: direct, 1: 8b10b, 2: HDLC
            ElinkWidth        => DecodingEpathWidth_i,--: in std_logic_vector(2 downto 0); --runtime configuration: 0:2, 1:4, 2:8, 3:16, 4:32
            MsbFirst          => '1',--: in std_logic; --Default 1, make 0 to reverse the bit order
            ReverseInputBits  => '0',--: in std_logic; --Default 0, reverse the bits of the input Elink
            EnableTruncation  => '0',
            ElinkData         => ElinkData_i,--: in std_logic_vector(MAX_INPUT-1 downto 0);
            ElinkAligned      => '1',--: in std_logic;
            DecoderAligned    => DecoderAligned,--: out std_logic;
            AlignmentPulseAlign    => AlignmentPulseAlign,--: in std_logic; --2 pulses to realign if FLAG found
            AlignmentPulseDeAlign    => AlignmentPulseDeAlign,--: in std_logic; --2 pulses to realign if FLAG found
            AutoRealign => '1',
            RealignmentEvent => open,
            FE_BUSY_out => open,
            m_axis            => m_axis,--: out axis_32_type;  --FIFO read port (axi stream)
            m_axis_tready     => '1',--: in std_logic; --FIFO read tready (axi stream)
            m_axis_aclk       => s_axis_aclk_i,--: in std_logic; --FIFO read clock (axi stream)
            m_axis_prog_empty => open --: out std_logic
        );

    pulse0: entity work.AlignmentPulseGen
        generic map(
            MAX_VAL_DEALIGN => 20480,
            MAX_VAL_ALIGN => 20
        )
        port map(
            clk40 => clk40,
            AlignmentPulseAlign => AlignmentPulseAlign,
            AlignmentPulseDeAlign => AlignmentPulseDeAlign
        );


    -- 8b10b encoder
    --dec_8b10b_INST: entity work.dec_8b10b
    --PORT MAP(
    --    RBYTECLK=> clk40,
    --    AI     => data_10b_rev(9),
    --    BI     => data_10b_rev(8),
    --    CI     => data_10b_rev(7),
    --    DI     => data_10b_rev(6),
    --    EI     => data_10b_rev(5),
    --    II     => data_10b_rev(4),
    --    FI     => data_10b_rev(3),
    --    GI     => data_10b_rev(2),
    --    HI     => data_10b_rev(1),
    --    JI     => data_10b_rev(0),
    --
    --    KO     => kflag,
    --    HO     => DataOut_tmp(7),
    --    GO     => DataOut_tmp(6),
    --    FO     => DataOut_tmp(5),
    --    EO     => DataOut_tmp(4),
    --    DO     => DataOut_tmp(3),
    --    CO     => DataOut_tmp(2),
    --    BO     => DataOut_tmp(1),
    --    AO     => DataOut_tmp(0)
    --
    --);


    dec_HDLC_inst: entity work.DecoderHDLC
        generic map(
            g_WORD_SIZE => 8,
            g_DELIMITER => x"7E",
            g_IDLE => x"7F"
        )
        PORT MAP (
            clk40 => clk40,
            ena => '1',
            reset => reset,
            DataIn => ElinkData_i(1 downto 0),
            DataOut => data_HDLC,
            DataOutValid => data_HDLC_valid,
            GearboxValidOut => open,
            EOP => open,
            TruncateHDLC => open,
            EnableTruncation => '1'
        );



    resetmgr0: entity work.CRresetManager
        port map(
            clk40         => clk40,
            rst           => reset,
            clk40_stable  => clk40_stable,
            cr_rst        => daq_reset,
            cr_fifo_flush => daq_fifo_flush
        );
    -----------------------------------------------------------------------------
    -- Clock Generator
    -----------------------------------------------------------------------------
    clock_generator(clk40, clk40_en, clk40_period, "40 MHz CLK");
    clock_generator(s_axis_aclk_i, s_axis_aclk_en, axiclk_period, "AXI CLK");


    ------------------------------------------------
    -- PROCESS: p_output_check
    ------------------------------------------------
    p_output_check: process
        constant C_SCOPE     : string  := C_TB_SCOPE_DEFAULT;
        variable position_i: integer:=0;
        variable TP : std_logic:='0';
        variable Brc_t2 : std_logic_vector(1 downto 0):= (others=>'0'); -- @suppress "variable Brc_t2 is never read"
        variable Brc_d4 : std_logic_vector(3 downto 0):= (others=>'0');
        variable ECR : std_logic:='0';
        variable BCR : std_logic:='0';
        variable OCR : std_logic:='0';
        variable OCRcount : integer:=0;
        variable BCRd : std_logic:='0';
        variable Bch : std_logic:='0';
        variable L1A : std_logic:='0';
        variable ttc_out: std_logic_vector(7 downto 0):= (others=>'0');
        variable TTCin_i_d : TTC_data_type := TTC_zero;
        variable TTCin_i_dd  : TTC_data_type := TTC_zero;
        variable CheckData32b: std_logic_vector(31 downto 0);

        procedure check_data(
            constant expected : in std_logic_vector(7 downto 0)) is
        begin
            for words_i in 1 to word_length loop
                wait until (rising_edge(clk40));
            --log("debug " & to_string(combined_data));
            end loop;
            check_value(combined_data, expected, ERROR, "Check for output data " & to_string(expected), C_SCOPE);
        end;

        procedure slice_data is
        begin
            wait until (falling_edge(clk40));
            if (ElinkWidth_i = "10") then
                case position_i is
                    when 1 =>
                        data_10b <= data_two_words(15 downto 6);
                    when 2 =>
                        data_10b <= data_two_words(13 downto 4);
                    when 3 =>
                        data_10b <= data_two_words(11 downto 2);
                    when 4 =>
                        data_10b <= data_two_words(9 downto 0);
                    when 5 =>
                        data_10b <= (others => '0');
                    when others =>
                        error("Unexpected Position",C_TB_SCOPE_DEFAULT);
                end case;
            elsif (ElinkWidth_i = "01") then
                case position_i is
                    when 1 =>
                        data_10b <= data_two_words(15 downto 6);
                    when 3 =>
                        data_10b <= data_two_words(13 downto 4);
                    when 2|4|5 =>
                        data_10b <= (others => '0');
                    when others =>
                        error("Unexpected Position",C_TB_SCOPE_DEFAULT);
                end case;
            elsif (ElinkWidth_i = "00") then
                case position_i is
                    when 1 =>
                        data_10b <= data_two_words(15 downto 6);
                    when 2|3|4|5 =>
                        data_10b <= (others => '0');
                    when others =>
                        error("Unexpected Position",C_TB_SCOPE_DEFAULT);
                end case;
            end if;
            wait for 10 ns;
        end;

        --procedure change_pos is
        --begin
        --  if (position_i = 5) then
        --    position_i := 1;
        --  else
        --    position_i := position_i + 1;
        --  end if;
        --  wait for 5 ns;
        --end;

        --procedure check_timing is
        --begin
        --  while position_i = 1 loop
        --
        --    slice_data;
        --
        --    if (data_10b_rev = COMMAp) then
        --      next_sign := 2;
        --      position_i := 2;
        --      log("pos data " &to_string(data_two_words)& " "& to_string(data_10b_rev));
        --    elsif (data_10b_rev = COMMAn) then
        --      next_sign := 1;
        --      position_i := 2;
        --      log("neg data " &to_string(data_two_words)& " "& to_string(data_10b_rev));
        --    end if;
        --
        --    wait for 10 ns;
        --
        --  end loop;
        --end;

        --procedure wait_header is
        --begin
        --  header_flag := '0';
        --  while header_flag = '0' loop
        --
        --    slice_data;
        --    log("In wait data " &to_string(data_two_words)& " "& to_string(data_10b_rev));
        --
        --    if (data_10b_rev = std_logic_vector(to_unsigned(0, 10))) then
        --      log("skip one cycle");
        --    elsif (next_sign = 1) then
        --      if(data_10b_rev = SOCp) then
        --        log("header found");
        --        header_flag := '1';
        --      else
        --        check_value(data_10b_rev, COMMAp, ERROR, "Comma pos", C_SCOPE);
        --      end if;
        --      next_sign := 2;
        --    elsif (next_sign = 2) then
        --      if(data_10b_rev = SOCn) then
        --        log("header found");
        --        header_flag := '1';
        --      else
        --        check_value(data_10b_rev, COMMAn, ERROR, "Comma neg", C_SCOPE);
        --      end if;
        --      next_sign := 1;
        --    else
        --      error("Unexpected sign",C_TB_SCOPE_DEFAULT);
        --    end if;
        --
        --    change_pos;
        --  end loop;
        --
        --end;

        --procedure check_data_8b10b(
        --  constant expected : in std_logic_vector(7 downto 0)) is
        --begin
        --
        --  slice_data;
        --
        --  if (data_10b_rev = std_logic_vector(to_unsigned(0, 10))) then
        --    --log("skip one cycle");
        --    change_pos;
        --    check_data_8b10b(expected);
        --  else
        --    log("In check data " &to_string(data_10b_rev)& " (10b) should be "& to_string(expected) & " (8b)");
        --    wait until (rising_edge(clk40));
        --    wait for 5 ns;
        --    --log("Decoded as " & to_string(DataOut_tmp));
        --    check_value(DataOut_tmp, expected, ERROR, "Decoded", C_SCOPE);
        --    change_pos;
        --    next_sign := 3-next_sign; -- sign changed every cycle except skipping timing
        --  end if;
        --end;

        procedure check_data_m_axis(
            constant expected : in std_logic_vector(31 downto 0)) is
        begin
            await_value(m_axis.tvalid, '1', 0 ns, 100 ms, ERROR, "Wait for m_axis data to arrive", C_SCOPE);
            check_value(m_axis.tdata, expected, ERROR, "m_axis.tdata does not have the expected value "& to_string(expected), C_SCOPE);
            await_value(m_axis.tvalid, '0', 0 ns, 100 ns, ERROR, "Wait for m_axis.tready to fall", C_SCOPE);

        end;

        procedure check_data_HDLC(
            constant expected : in std_logic_vector(7 downto 0)) is
        begin

            slice_data;

            --if (position_i = 5) then
            if (data_HDLC_valid = '0') then
                --log("skip one cycle");
                --change_pos;
                check_data_HDLC(expected);
            else
                log("In check data " &to_string(data_HDLC)& " should be "& to_string(expected) & " (8b)");
                wait until (rising_edge(clk40));
                wait for 5 ns;
                --log("Decoded as " & to_string(DataOut_tmp));
                check_value(DataOut_tmp, expected, ERROR, "Decoded", C_SCOPE);
            --change_pos;
            end if;
        end;

        procedure slice_ttc is
        begin
            TTCin_i_dd:=TTCin_i_d;
            TTCin_i_d:=TTCin_i;
            BCRd:=BCR;

            if (TTCOption_i = x"3") then
                TP:=TTCin_i_dd.ExtendedTestPulse;
                Brc_t2:= TTCin_i_dd.Brcst(5 downto 4);
                Brc_d4:= TTCin_i_dd.Brcst(3 downto 0);
                ECR:=TTCin_i_dd.ECR;
                BCR:=TTCin_i_dd.BCR;
                Bch:=TTCin_i_dd.Bchan;
                L1A:=TTCin_i_dd.L1A;
            else
                TP:=TTCin_i.ExtendedTestPulse;
                Brc_t2:= TTCin_i.Brcst(5 downto 4);
                Brc_d4:= TTCin_i.Brcst(3 downto 0);
                ECR:=TTCin_i_dd.ECR;
                BCR:=TTCin_i_dd.BCR;
                Bch:=TTCin_i_dd.Bchan;
                L1A:=TTCin_i_dd.L1A;
            end if;
            if (BCR) then
                OCR:='1';
                if (TTCin_i_dd.Brcst(5)) then--Brc_t2(1)
                    OCRcount:=1;
                --log("debug OCRcount 1");
                else
                    OCRcount:=0;
                end if;
            elsif (OCR) then
                if (OCRcount=1) then
                    OCRcount:=0;
                --log("debug OCRcount 0");
                else
                    OCR:='0';
                end if;
            end if;


            --log("test ttc mode "&to_string(TTCOption_i));
            case TTCOption_control is
                when x"0"=>
                    ttc_out:="000000"&Bch&L1A;
                when x"1"=>
                    ttc_out:="0000"&Bch&ECR&BCR&L1A;
                when x"2"=>
                    ttc_out:=Bch&Brc_d4&ECR&BCR&L1A;
                when x"3"=>
                    ttc_out:=L1A&Brc_d4(1)&TP&ECR&OCR&L1A&Brc_d4(3)&ECR;
                when x"4"=>
                    ttc_out:="0000"&BCR&BCR&BCR&BCR;
                when x"5"=>
                    ttc_out:="000000"&BCR&BCRd;
                when others =>
                    error("Unexpected TTCOption",C_TB_SCOPE_DEFAULT);
            end case;
            wait until (rising_edge(clk40));
        end;

    begin --p_output_check process
        position_i := 1;
        await_value(reading, '1', 0 ns, 100 ms, ERROR, "Wait for start to write", C_SCOPE);
        if (EpathEncoding_i = x"0") then
            await_value(combined_data, x"C9", 0 ns, 2 us, ERROR, "Wait for header", C_SCOPE);
            wait until (rising_edge(clk40));
            for i in 1 to max loop
                check_data(std_logic_vector(to_unsigned(i, 8)));
            end loop;
        elsif (EpathEncoding_i = x"1" or EpathEncoding_i = x"2") then
            --check_timing;
            --wait_header;
            --check_data_8b10b(x"C9");
            --for i in 1 to max loop
            --  check_data_8b10b(std_logic_vector(to_unsigned(i, 8)));
            --end loop;
            CheckData32b(31 downto 0) := x"030201C9";
            check_data_m_axis(CheckData32b);
            for i in 1 to (max+1)/4-1 loop
                CheckData32b(7 downto 0)   := std_logic_vector(to_unsigned(i*4, 8));
                CheckData32b(15 downto 8)  := std_logic_vector(to_unsigned(i*4+1, 8));
                CheckData32b(23 downto 16) := std_logic_vector(to_unsigned(i*4+2, 8));
                CheckData32b(31 downto 24) := std_logic_vector(to_unsigned(i*4+3, 8));
                check_data_m_axis(CheckData32b);
            end loop;
        -- HDLC can also be checked by the Decoding Epath / axis32
        --elsif (EpathEncoding_i = x"2") then --HDLC
        --  await_value(data_HDLC, x"C9", 0 ns, 2 us, ERROR, "Wait for header", C_SCOPE);
        --  for i in 1 to max loop
        --      check_data_HDLC(std_logic_vector(to_unsigned(i, 8)));
        --  end loop;
        elsif (EpathEncoding_i = x"3") then
            await_value(ttc_wrote, '1', 0 ns, 2 us, ERROR, "Wait write flag of TTC", C_SCOPE);
            wait until (rising_edge(clk40));
            slice_ttc;
            check_value(ElinkData_i, x"A9", ERROR, "Check for first TTC", C_SCOPE);

            for i in 0 to 4 loop -- for reset
                slice_ttc;
                check_value(ElinkData_i(2 downto 0), x"0", ERROR, "Check for output data for reset ttc", C_SCOPE);
            end loop;

            for i in 0 to max loop -- main check
                slice_ttc;
                if (TTCOption_control = x"3") then
                    check_value(ElinkData_i(1 downto 1), ttc_out(1 downto 1), ERROR, "Check for output data for input TTC", C_SCOPE);
                else
                    check_value(ElinkData_i(2 downto 0), "0"&ttc_out(1 downto 0), ERROR, "Check for output data for input TTC", C_SCOPE);
                end if;
            end loop;

            for i in 0 to 4 loop -- for reset
                slice_ttc;
                check_value(ElinkData_i(1 downto 0), x"0", ERROR, "Check for output data for reset ttc", C_SCOPE);
            end loop;

        end if;
        position_i := 1;
        finish_read<='1';
        await_value(reading, '0', 0 ns, 1 ms, ERROR, "closing check output process", C_SCOPE);
        finish_read<='0';
    end process p_output_check;


    ------------------------------------------------
    -- PROCESS: p_combine
    ------------------------------------------------

    p_combine: process(clk40,reset)
    --constant C_SCOPE     : string  := C_TB_SCOPE_DEFAULT;
    begin
        if (reset = '1') then
            combined_data <= (others => '0');
            word_length <= 0;
            data_two_words <= (others => '0');
        end if;

        if (reading = '1' and rising_edge(clk40)) then
            case ElinkWidth_i is
                when "00"=>
                    combined_data <= combined_data(5 downto 0)&ElinkData_i(1 downto 0);
                    data_two_words <= data_two_words(13 downto 0)&ElinkData_i(1 downto 0);
                    word_length <= 4;
                when "01"=>
                    combined_data <= combined_data(3 downto 0)&ElinkData_i(3 downto 0);
                    data_two_words <= data_two_words(11 downto 0)&ElinkData_i(3 downto 0);
                    word_length <= 2;
                when "10"=>
                    combined_data <= ElinkData_i(7 downto 0);
                    data_two_words <= data_two_words(7 downto 0)&ElinkData_i(7 downto 0);
                    word_length <= 1;
                when others =>
                    error("Unexpected ElinkWidth_i",C_TB_SCOPE_DEFAULT);
            end case;
            if EpathEncoding_i /= x"3" and EpathEncoding_i /= x"2" then
                log("Check Elink output(" & to_string(ElinkData_i,HEX) & " " & to_string(ElinkData_i) & ") Combin output(" & to_string(combined_data,HEX) & " " & to_string(combined_data) & ")");
            end if;

        end if;
    end process p_combine;

    ------------------------------------------------
    -- PROCESS: p_main
    ------------------------------------------------
    p_main: process
        constant C_SCOPE     : string  := C_TB_SCOPE_DEFAULT;

        procedure write_axis(
            constant tdata   : in std_logic_vector(7 downto 0);
            constant tvalid : in std_logic;
            constant tlast  : in std_logic) is
        begin
            --wait for axiclk_period;
            wait until (rising_edge(s_axis_aclk_i));
            s_axis_i.tdata <= tdata;
            s_axis_i.tvalid <= tvalid;
            s_axis_i.tlast <= tlast;
        --log("debug write "&to_string(tdata));
        end;

        procedure make_inputs is
        begin
            if (EpathEncoding_i=x"3") then
                case TTCOption_control is
                    when x"0"=>
                        max <= 3;
                    when x"1"=>
                        max <= 15;
                    when x"2"=>
                        max <= 255;
                    when x"3"=>
                        max <= 2047;
                    when x"4"|x"5"=>
                        max <= 15;
                    when others =>
                        max <= 4;
                end case;
                log("Make input for TTC "&to_string(TTCOption_control));
                wait until (rising_edge(clk40));
                TTCin_i <= TTC_zero;
                TTCin_i.L1A <= '1';
                TTCin_i.L0A <= '1';
                TTCOption_i <= x"0";
                ttc_wrote<= '1';
                wait until (rising_edge(clk40));

                --reset
                for i in 0 to 4 loop
                    TTCin_i <= TTC_zero;
                    TTCOption_i <= x"0";
                    wait until (rising_edge(clk40));
                end loop;

                --test
                for i in 0 to max loop
                    if (TTCOption_control= x"3") then
                        TTCin_temp <= std_logic_vector(to_unsigned(i, 17));
                        TTCin_i <= TTC_zero;
                        TTCin_i.ExtendedTestPulse <= TTCin_temp(9);
                        TTCin_i.Brcst <= TTCin_temp(8 downto 3);
                        TTCin_i.ECR <= TTCin_temp(2);
                        TTCin_i.BCR <= TTCin_temp(0);
                        TTCin_i.Bchan <= '0';
                        TTCin_i.L1A   <= TTCin_temp(1);
                        TTCin_i.L0A   <= TTCin_temp(1);
                    else
                        TTCin_i <= TTC_zero;
                    end if;
                    TTCOption_i <= TTCOption_control;
                    wait until (rising_edge(clk40));
                end loop;

                --reset
                for i in 0 to 4 loop
                    TTCin_i <= TTC_zero;
                    TTCOption_i <= x"0";
                    wait until (rising_edge(clk40));
                end loop;
                ttc_wrote<= '0';
            else
                max<=7;
                write_axis(x"C9",'1','0');
                for i in 1 to max loop
                    log("Make input(" & to_string(i) & ")");
                    --set data
                    if (i=max) then
                        write_axis(std_logic_vector(to_unsigned(i, 8)),'1','1');
                    else
                        write_axis(std_logic_vector(to_unsigned(i, 8)),'1','0');
                    end if;
                end loop;
                write_axis((others => '0'),'0','0');
            end if;
        end;

        procedure reset_flags is
        begin
            write_axis((others => '0'),'0','0');
            wait_num_rising_edge(clk40,100);
            gen_pulse(reset, '1', 100 * clk40_period, "Pulsed reset-signal - active for 100 * 25nsec");
            wait_num_rising_edge(s_axis_aclk_i,1250);
        --log(ID_LOG_HDR, "Check defaults on output ports", C_SCOPE);
        --check_value(ElinkData_i, X"00", ERROR, "Default data check", C_SCOPE);
        end;

        procedure setElinkWidth(
            constant Width: in std_logic_vector(1 downto 0))is
        begin
            log(ID_SEQUENCER, "Set ElinkWidth " & to_string(ElinkWidth_i), C_SCOPE);
            reset <= '1';
            wait_num_rising_edge(clk40,1);
            ElinkWidth_i <= Width;
            wait_num_rising_edge(clk40,1);
            reset <= '0';
            wait_num_rising_edge(clk40,100);
            if EpathEncoding_i = "0001" and DecoderAligned = '0' then --for 8b10b we want to wait for the decoder to align to it.
                await_value(DecoderAligned, '1', 100 ns, 1 ms, ERROR, "Decoder did not align", C_SCOPE);
            end if;
            --gen_pulse(reset, '1', 1 * clk40_period, "Pulsed reset-signal - active for 25nsec");
            wait_num_rising_edge(clk40,100);
        end;

        procedure test_system is
        begin
            log(ID_LOG_HDR, "Test with ElinkWidth " & to_string(ElinkWidth_i), C_SCOPE);
            reading <= '1';
            wait for 10 ns;
            make_inputs;
            await_value(finish_read, '1', 0 ns, 1 ms, ERROR, "Wait for finish to ckeck all input", C_SCOPE);
            reading <= '0';
            --log(ID_SEQUENCER, "Test done with ElinkWidth " & to_string(ElinkWidth_i), C_SCOPE);
            wait for 100 ns;
        end;

    begin
        clk40_stable <= '0';
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);
        enable_log_msg(ALL_MESSAGES);
        log(ID_LOG_HDR, "Simulation of TB for Encoding Epath", C_SCOPE);

        --Direct mode
        --enable clock
        clk40_en<= true;
        s_axis_aclk_en<= true;
        clk40_stable <= '1';
        EpathEncoding_i<=x"0";
        wait for 1 ns;


        --start simulation
        log(ID_LOG_HDR, "Starting simulation", C_SCOPE);
        log(ID_LOG_HDR, "c " & to_string(EpathEncoding_i), C_SCOPE);
        reset_flags;

        setElinkWidth("10");
        test_system;

        setElinkWidth("01");
        test_system;

        setElinkWidth("00");
        test_system;


        clk40_en<= false;
        s_axis_aclk_en<= false;
        wait for 1000 ns;
        --8b10b mode
        EpathEncoding_i<=x"1";
        clk40_en<= true;
        s_axis_aclk_en<= true;
        wait for 1 ns;

        log(ID_LOG_HDR, "Starting 8b10b mode", C_SCOPE);
        log(ID_LOG_HDR, "c " & to_string(EpathEncoding_i), C_SCOPE);
        reset_flags;

        setElinkWidth("10");
        test_system;

        setElinkWidth("01");
        test_system;

        setElinkWidth("00");
        test_system;

        clk40_en<= false;
        s_axis_aclk_en<= false;
        wait for 1000 ns;
        --HDLC mode
        EpathEncoding_i<=x"2";
        clk40_en<= true;
        s_axis_aclk_en<= true;
        wait for 1 ns;
        log(ID_LOG_HDR, "Starting HDLC mode", C_SCOPE);
        log(ID_LOG_HDR, "c " & to_string(EpathEncoding_i), C_SCOPE);
        reset_flags;

        setElinkWidth("00");
        test_system;

        clk40_en<= false;
        s_axis_aclk_en<= false;
        wait for 1000 ns;
        --TTC mode

        EpathEncoding_i<=x"3";
        clk40_en<= true;
        s_axis_aclk_en<= true;
        wait for 1 ns;

        log(ID_LOG_HDR, "c " & to_string(EpathEncoding_i), C_SCOPE);
        reset_flags;
        setElinkWidth("00");

        TTCOption_control<=x"0";
        test_system;

        TTCOption_control<=x"1";
        test_system;

        TTCOption_control<=x"2";
        test_system;

        TTCOption_control<=x"3";
        test_system;

        TTCOption_control<=x"4";
        test_system;

        TTCOption_control<=x"5";
        test_system;

        --==================================================================================================
        -- Ending the simulation
        --------------------------------------------------------------------------------------
        wait for 1 ms;             -- to allow some time for completion
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        -- Finish the simulation
        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely
    end process p_main;


end tb;

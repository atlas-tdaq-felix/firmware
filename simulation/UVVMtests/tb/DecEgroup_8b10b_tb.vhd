--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;

    use std.env.all;
    use work.axi_stream_package.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

library bitvis_vip_axistream;
    use bitvis_vip_axistream.axistream_bfm_pkg.all;
    use bitvis_vip_axistream.vvc_methods_pkg.all;
    context bitvis_vip_axistream.vvc_context;
library bitvis_vip_scoreboard;
library uvvm_vvc_framework;
    use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

entity DecEgroup_8b10b_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end DecEgroup_8b10b_tb;

architecture tb of DecEgroup_8b10b_tb is
    -- Interrupt related signals
    signal clock_ena     : boolean   := false;
    signal ElinkWidth : std_logic_vector(2 downto 0);

    constant C_CLK_PERIOD_40     : time := 25 ns;
    constant C_CLK_PERIOD_80     : time := 12.5 ns;
    constant C_CLK_PERIOD_160     : time := 6.25 ns;

    signal clk40, clk80, clk160: std_logic;
    signal reset: std_logic;

    signal DataIn32b : std_logic_vector(31 downto 0);
    signal ElinkAligned: std_logic_vector(3 downto 0);
    signal m_axis: axis_32_array_type(0 to 3);
    signal m_axis_tready: axis_tready_array_type(0 to 3);
    signal m_axis_prog_empty: axis_tready_array_type(0 to 3); -- @suppress "signal m_axis_prog_empty is never read"
    signal MessageLength: integer;
    signal AlignmentPulseAlign, AlignmentPulseDeAlign: std_logic;
    signal StartFrameGen: std_logic := '0';
    signal NumberOfMessages: integer;
    signal HGTD_ALTIROC_ENCODING: std_logic := '1';

    signal axistream_vvc_if1 : t_axistream_if(tdata(31 downto 0), tkeep(3 downto 0), tuser(3 downto 0), tstrb(3 downto 0), tid(0 downto 0), tdest(0 downto 0)) := init_axistream_if_signals(false, 32, 4, 1, 1);
    signal axistream_vvc_if2 : t_axistream_if(tdata(31 downto 0), tkeep(3 downto 0), tuser(3 downto 0), tstrb(3 downto 0), tid(0 downto 0), tdest(0 downto 0)) := init_axistream_if_signals(false, 32, 4, 1, 1);
    signal axistream_vvc_if3 : t_axistream_if(tdata(31 downto 0), tkeep(3 downto 0), tuser(3 downto 0), tstrb(3 downto 0), tid(0 downto 0), tdest(0 downto 0)) := init_axistream_if_signals(false, 32, 4, 1, 1);
    signal axistream_vvc_if4 : t_axistream_if(tdata(31 downto 0), tkeep(3 downto 0), tuser(3 downto 0), tstrb(3 downto 0), tid(0 downto 0), tdest(0 downto 0)) := init_axistream_if_signals(false, 32, 4, 1, 1);

    constant GC_AXISTREAM_BFM_CONFIG : t_axistream_bfm_config := C_AXISTREAM_BFM_CONFIG_DEFAULT;
    signal IDLES: integer := 0;
    signal clk40_stable : std_logic;
    signal daq_reset : std_logic;
    signal daq_fifo_flush : std_logic;

begin
    -- Instantiate the concurrent procedure that initializes UVVM
    i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

    -----------------------------------------------------------------------------
    -- Clock Generator
    -----------------------------------------------------------------------------
    clock_generator(clk40, clock_ena, C_CLK_PERIOD_40, "40 MHz clock");
    clock_generator(clk80, clock_ena, C_CLK_PERIOD_80, "80 MHz clock");
    clock_generator(clk160, clock_ena, C_CLK_PERIOD_160, "160 MHz clock");


    axi_stream_vvc_1: entity bitvis_vip_axistream.axistream_vvc
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => false,
            GC_DATA_WIDTH => 32,
            GC_USER_WIDTH => 4,
            GC_ID_WIDTH => 1,
            GC_DEST_WIDTH => 1,
            GC_INSTANCE_IDX => 0,
            GC_AXISTREAM_BFM_CONFIG => GC_AXISTREAM_BFM_CONFIG
        )
        port map (
            clk => clk160,
            axistream_vvc_if => axistream_vvc_if1
        );

    axistream_vvc_if1.tkeep <= m_axis(0).tkeep;
    axistream_vvc_if1.tuser <= m_axis(0).tuser;
    axistream_vvc_if1.tdata <= m_axis(0).tdata;
    axistream_vvc_if1.tvalid <= m_axis(0).tvalid;
    axistream_vvc_if1.tlast <= m_axis(0).tlast;
    m_axis_tready(0) <= axistream_vvc_if1.tready;

    axi_stream_vvc_2: entity bitvis_vip_axistream.axistream_vvc
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => false,
            GC_DATA_WIDTH => 32,
            GC_USER_WIDTH => 4,
            GC_ID_WIDTH => 1,
            GC_DEST_WIDTH => 1,
            GC_INSTANCE_IDX => 1,
            GC_AXISTREAM_BFM_CONFIG => GC_AXISTREAM_BFM_CONFIG
        )
        port map (
            clk => clk160,
            axistream_vvc_if => axistream_vvc_if2
        );

    axistream_vvc_if2.tkeep <= m_axis(1).tkeep;
    axistream_vvc_if2.tuser <= m_axis(1).tuser;
    axistream_vvc_if2.tdata <= m_axis(1).tdata;
    axistream_vvc_if2.tvalid <= m_axis(1).tvalid;
    axistream_vvc_if2.tlast <= m_axis(1).tlast;
    m_axis_tready(1) <= axistream_vvc_if2.tready;

    axi_stream_vvc_3: entity bitvis_vip_axistream.axistream_vvc
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => false,
            GC_DATA_WIDTH => 32,
            GC_USER_WIDTH => 4,
            GC_ID_WIDTH => 1,
            GC_DEST_WIDTH => 1,
            GC_INSTANCE_IDX => 2,
            GC_AXISTREAM_BFM_CONFIG => GC_AXISTREAM_BFM_CONFIG
        )
        port map (
            clk => clk160,
            axistream_vvc_if => axistream_vvc_if3
        );

    axistream_vvc_if3.tkeep <= m_axis(2).tkeep;
    axistream_vvc_if3.tuser <= m_axis(2).tuser;
    axistream_vvc_if3.tdata <= m_axis(2).tdata;
    axistream_vvc_if3.tvalid <= m_axis(2).tvalid;
    axistream_vvc_if3.tlast <= m_axis(2).tlast;
    m_axis_tready(2) <= axistream_vvc_if3.tready;

    axi_stream_vvc_4: entity bitvis_vip_axistream.axistream_vvc
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => false,
            GC_DATA_WIDTH => 32,
            GC_USER_WIDTH => 4,
            GC_ID_WIDTH => 1,
            GC_DEST_WIDTH => 1,
            GC_INSTANCE_IDX => 3,
            GC_AXISTREAM_BFM_CONFIG => GC_AXISTREAM_BFM_CONFIG
        )
        port map (
            clk => clk160,
            axistream_vvc_if => axistream_vvc_if4
        );

    axistream_vvc_if4.tkeep <= m_axis(3).tkeep;
    axistream_vvc_if4.tuser <= m_axis(3).tuser;
    axistream_vvc_if4.tdata <= m_axis(3).tdata;
    axistream_vvc_if4.tvalid <= m_axis(3).tvalid;
    axistream_vvc_if4.tlast <= m_axis(3).tlast;
    m_axis_tready(3) <= axistream_vvc_if4.tready;

    framegen0: entity work.DecEgroup_8b10b_framegen
        generic map(
            UseFELIXDataFormat => false-- Just use a counter value
        )port map(
            clk40 => clk40,
            clk80 => clk80,
            clk160 => clk160,
            reset => daq_reset,
            strips_mode_en => '0', --MRMW
            direct_mode_en => '0', --MRMW
            IDLES => IDLES,
            MessageLength => MessageLength,
            ElinkWidth => ElinkWidth,
            DataOut32b => DataIn32b,
            StartFrameGen => StartFrameGen,
            NumberOfMessages => NumberOfMessages,
            HGTD_ALTIROC_ENCODING => HGTD_ALTIROC_ENCODING -- FIXME fixed to HGTD encoding for now (special HGTD-only build)
        );


    ------------------------------------------------
    -- PROCESS: p_main
    ------------------------------------------------
    p_main: process
        constant C_SCOPE     : string  := C_TB_SCOPE_DEFAULT;
        variable testdata : t_byte_array(0 to 99);
    begin
        clk40_stable <= '0';
        for i in 0 to 99 loop
            testdata(i) := std_logic_vector(to_unsigned(i,8));
        end loop;
        NumberOfMessages <= 10;
        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);
        enable_log_msg(ALL_MESSAGES);
        disable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        -- Wait for UVVM to finish initialization
        await_uvvm_initialization(VOID);
        clock_ena <= true;
        clk40_stable <= '1';
        reset <= '1';
        wait for 25 ns;
        reset <= '0';
        wait for 250 ns;
        for i in 0 to 3 loop
            shared_axistream_vvc_config(i).bfm_config.max_wait_cycles := 10000;
            shared_axistream_vvc_config(i).bfm_config.max_wait_cycles_severity := ERROR;
            shared_axistream_vvc_config(i).bfm_config.clock_period := C_CLK_PERIOD_160;
            shared_axistream_vvc_config(i).bfm_config.setup_time := C_CLK_PERIOD_160/4;
            shared_axistream_vvc_config(i).bfm_config.hold_time  := C_CLK_PERIOD_160/4;
            shared_axistream_vvc_config(i).bfm_config.clock_period_margin := 0 ns;
            shared_axistream_vvc_config(i).bfm_config.bfm_sync := SYNC_WITH_SETUP_AND_HOLD;
            shared_axistream_vvc_config(i).bfm_config.match_strictness := MATCH_EXACT;
            shared_axistream_vvc_config(i).bfm_config.byte_endianness := FIRST_BYTE_LEFT;
            shared_axistream_vvc_config(i).bfm_config.check_packet_length := false;
            shared_axistream_vvc_config(i).bfm_config.protocol_error_severity := ERROR;
        end loop;

        log(ID_LOG_HDR, "Simulation of TB for ByteToAxiStream", C_SCOPE);
        ------------------------------------------------------------
        for NSW_HGTD in 0 to 1 loop --Execute testbench both in NSW format and in HGTD ALTIROC format
            if NSW_HGTD = 0 then
                HGTD_ALTIROC_ENCODING <= '0';
            else
                HGTD_ALTIROC_ENCODING <= '1';
            end if;
            for w in 0 to 2 loop
                case w is
                    when 0 =>
                        ElinkWidth <= "010"; --8b.
                        IDLES <= 0;
                    when 1 =>
                        ElinkWidth <= "011"; --16b.
                        IDLES <= 0;
                    when 2 =>
                        ElinkWidth <= "100"; --32b.
                        IDLES <= 1; --At least one IDLE is needed in between EOP/SOP to avoid congestion in ByteToAxiStream
                end case;
                reset <= '1';
                wait for 25 ns;
                reset <= '0';
                wait for 250 ns;
                if ElinkAligned /= "0000" then
                    await_value(ElinkAligned, "0000",0 ps, 1 ms, "Waiting for decoder to misalign");
                end if;
                case w is
                    when 0 =>
                        await_value(ElinkAligned, "1111",0 ps, 1 ms, "Waiting for decoder to align");
                    when 1 =>
                        await_value(ElinkAligned, "0101",0 ps, 1 ms, "Waiting for decoder to align");
                    when 2 =>
                        await_value(ElinkAligned, "0001",0 ps, 1 ms, "Waiting for decoder to align");
                end case;



                for ml in 4 to 99 loop
                    MessageLength <= ml + 1;
                    StartFrameGen <= '1';
                    for i in 0 to NumberOfMessages-1 loop
                        axistream_expect_bytes(AXISTREAM_VVCT, 0, testdata(0 to ml),  "expecting axi stream data on 1st interface"); --@suppress
                        if ElinkWidth = "011" or ElinkWidth = "010" then
                            axistream_expect_bytes(AXISTREAM_VVCT, 2, testdata(0 to ml),  "expecting axi stream data on 3rd interface"); --@suppress
                        end if;
                        if ElinkWidth = "010" then
                            axistream_expect_bytes(AXISTREAM_VVCT, 1, testdata(0 to ml),  "expecting axi stream data on 2nd interface"); --@suppress
                            axistream_expect_bytes(AXISTREAM_VVCT, 3, testdata(0 to ml),  "expecting axi stream data on 4th interface"); --@suppress
                        end if;
                    end loop;
                    await_completion(AXISTREAM_VVCT,0, 1 ms, "Wait for receive to finish on 1st interface"); --@suppress
                    if ElinkWidth = "011" or ElinkWidth = "010" then
                        await_completion(AXISTREAM_VVCT,2, 1 ms, "Wait for receive to finish on 3rd interface"); --@suppress
                    end if;
                    if ElinkWidth = "010" then
                        await_completion(AXISTREAM_VVCT,1, 1 ms, "Wait for receive to finish on 2nd interface"); --@suppress
                        await_completion(AXISTREAM_VVCT,3, 1 ms, "Wait for receive to finish on 4th interface"); --@suppress
                    end if;
                    StartFrameGen <= '0';
                    wait for 25 ns;
                end loop;
            end loop;
        end loop;
        --wait for 500 us;

        --==================================================================================================
        -- Ending the simulation
        --------------------------------------------------------------------------------------
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        -- Finish the simulation
        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely

    end process p_main;

    resetmgr0: entity work.CRresetManager
        port map(
            clk40         => clk40,
            rst           => reset,
            clk40_stable  => clk40_stable,
            cr_rst        => daq_reset,
            cr_fifo_flush => daq_fifo_flush
        );

    uut: entity work.DecEgroup_8b10b
        generic map(
            BLOCKSIZE => 1024,
            Support32bWidth => '1',
            Support16bWidth => '1',
            Support8bWidth => '1',
            IncludeElinks => "1111",
            VERSAL => false
        )
        port map(
            clk40 => clk40,
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            tick_1us_i => '0',
            dm_start_mask_i => x"0000",
            dm_capture_length_i => x"00",
            DataIn => DataIn32b,
            EgroupCounterClear => '0',
            EnableIn => "1111",
            LinkAligned => '1',
            ElinkWidth => ElinkWidth,
            AlignmentPulseAlign => AlignmentPulseAlign,
            AlignmentPulseDeAlign => AlignmentPulseDeAlign,
            AutoRealign => '1',
            RealignmentEvent => open,
            PathEncoding => "0001000100010001", --All 8b10b encoded
            ElinkAligned => ElinkAligned,
            DecodingErrors => open,
            MsbFirst => '1',
            ReverseInputBits => "0000",
            HGTD_ALTIROC_DECODING => HGTD_ALTIROC_ENCODING, -- FIXME fixed to HGTD encoding for now (special HGTD-only build)
            FE_BUSY_out => open,
            m_axis => m_axis,
            m_axis_tready => m_axis_tready,
            m_axis_aclk => clk160,
            m_axis_prog_empty => m_axis_prog_empty
        );

    pulsegen0: entity work.AlignmentPulseGen
        generic map(
            MAX_VAL_DEALIGN => 2048, --2048 bytes at 8b10b / 80 Mb/s elink.
            MAX_VAL_ALIGN => 10 --2 bytes at 8b10b / 80 Mb/s elink.
        )
        port map(
            clk40 => clk40,
            AlignmentPulseAlign => AlignmentPulseAlign,
            AlignmentPulseDeAlign => AlignmentPulseDeAlign
        );
end  architecture;

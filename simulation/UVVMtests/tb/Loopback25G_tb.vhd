----------------------------------------------------------------------------------
--  ATLAS
--  FELIX UVVM test automation project
--  F. Schreuder
--  Nikhef
--  January 2020
----------------------------------------------------------------------------------
-- file: DecodingGearBox_tb.vhd.
----------------------------------------------------------------------------------
library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use work.axi_stream_package.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;
    use std.env.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

--library uvvm_vvc_framework;
--use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

--use work.vvc_methods_pkg.all;
--use work.td_vvc_framework_common_methods_pkg.all;
--use work.egr_bfm_pkg.all;


-- Test bench entity
entity Loopback25G_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end Loopback25G_tb;

architecture tb of Loopback25G_tb is

    signal clk40        : std_logic:= '0';
    signal clk40_en     : boolean   := false;
    signal clk160 : std_logic;
    signal clk250 : std_logic;

    constant clk40_period : time := 25 ns;
    constant clk160_period : time := 6.25 ns;
    constant clk250_period : time := 4 ns;
    --signal s_axis_aclk_en     : boolean   := false;
    --constant axiclk_period : time := 10 ns;
    constant GBT_NUM : integer := 4;
    constant FIRMWARE_MODE : integer := FIRMWARE_MODE_GBT;
    constant BLOCKSIZE : integer := 1024;
    constant STREAMS_TOHOST: integer := STREAMS_TOHOST_MODE(FIRMWARE_MODE);
    constant GROUP_CONFIG_FROMHOST : IntArray(0 to 7) := STREAMS_FROMHOST_MODE(FIRMWARE_MODE);
    constant STREAMS_FROMHOST: integer := sum(GROUP_CONFIG_FROMHOST);
    constant aclk64_period: time := 2.739 ns;
    signal aclk64: std_logic;
    signal aresetn: std_logic;
    constant NUMBER_OF_DESCRIPTORS : integer := 2;
    constant NUMBER_OF_INTERRUPTS : integer := 8;
    constant LINK_NUM : integer := GBT_NUM;
    constant DATA_WIDTH : integer := 512;
    signal interrupt_call : std_logic_vector(NUMBER_OF_INTERRUPTS-1 downto 4);-- @suppress "signal interrupt_call is never read"
    constant LINK_CONFIG : IntArray := (1,1,1,1);
    signal register_map_control : register_map_control_type;
    signal register_map_crtohost_monitor : register_map_crtohost_monitor_type; -- @suppress "signal register_map_crtohost_monitor is never read"
    signal register_map_xoff_monitor : register_map_xoff_monitor_type; -- @suppress "signal register_map_xoff_monitor is never read"
    constant s_axis_aux_prog_empty : axis_tready_array_type(0 to 1) := (others => '1');
    signal s_axis_aux_tready : axis_tready_array_type(0 to 1); -- @suppress "signal s_axis_aux_tready is never read"
    constant s_axis_aux : axis_32_array_type(0 to 1) := (others => (
                                                                     tdata => (others => '0'),
                                                                     tvalid => '0',
                                                                     tlast => '0',
                                                                     tuser => (others => '0'),
                                                                     tkeep => (others => '0'),
                                                                     tid => (others => '0')
                                                                   ));
    constant s_axis_prog_empty : axis_tready_2d_array_type(0 to LINK_NUM-1, 0 to STREAMS_TOHOST-1) := (others => (others => '1'));
    signal s_axis_tready : axis_tready_2d_array_type(0 to LINK_NUM-1, 0 to STREAMS_TOHOST-1); -- @suppress "signal s_axis_tready is never read"
    constant s_axis : axis_32_2d_array_type(0 to LINK_NUM-1, 0 to STREAMS_TOHOST-1):= (others => (others => (
                                                                                                              tdata => (others => '0'),
                                                                                                              tvalid => '0',
                                                                                                              tlast => '0',
                                                                                                              tuser => (others => '0'),
                                                                                                              tkeep => (others => '0'),
                                                                                                              tid => (others => '0')
                                                                                                            )));
    constant USE_URAM : boolean := false;
    signal s_axis64_prog_empty : axis_tready_array_type(0 to LINK_NUM-1);
    signal s_axis64_tready : axis_tready_array_type(0 to LINK_NUM-1);
    signal s_axis64 : axis_64_array_type(0 to LINK_NUM-1);
    --signal thFIFObusyOut : std_logic; -- @suppress "signal thFIFObusyOut is never read"
    signal toHostFifo_din : slv_array(0 to NUMBER_OF_DESCRIPTORS-2);
    signal toHostFifo_prog_full : std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
    signal toHostFifo_rst : std_logic; -- @suppress "signal toHostFifo_rst is never read"
    signal toHostFifo_wr_clk : std_logic;
    --constant toHostFifo_wr_data_count : slv12_array(0 to NUMBER_OF_DESCRIPTORS-2):= (others => (others => '0'));
    signal toHostFifo_wr_en : std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
    constant toHostTimeoutBitn : integer := 16;
    signal fromHostFifo_dout : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal fromHostFifo_rd_en : std_logic;
    signal fromHostFifo_empty : std_logic;
    signal fromHostFifo_rst : std_logic; -- @suppress "signal fromHostFifo_rst is never read"
    -- signal fhAxis_aclk : std_logic;
    signal fhAxis : axis_8_2d_array_type(0 to LINK_NUM-1, 0 to STREAMS_FROMHOST-1); -- @suppress "signal fhAxis is never read"
    constant fhAxis_tready : axis_tready_2d_array_type(0 to LINK_NUM-1, 0 to STREAMS_FROMHOST-1) := (others => (others => '1'));
    signal fhAxis64 : axis_64_array_type(0 to LINK_NUM-1);
    signal fhAxis64_tready : axis_tready_array_type(0 to LINK_NUM-1);
    signal fifo_monitoring : bitfield_crfromhost_fifo_status_r_type; -- @suppress "signal fifo_monitoring is never read"
    constant SIM_NUMBER_OF_BLOCKS : integer := 500;
    constant APPLY_BACKPRESSURE_AFTER_BLOCKS : integer := 1000;
    signal checker_done : std_logic; -- @suppress "signal checker_done is never read"
    signal reset : std_logic;
    signal start_check : std_logic; -- @suppress "signal start_check is never read"
    signal Trunc : std_logic; -- @suppress "signal Trunc is never read"
    signal TrailerError : std_logic; -- @suppress "signal TrailerError is never read"
    signal CRCError : std_logic; -- @suppress "signal CRCError is never read"
    signal ChunkLengthError : std_logic; -- @suppress "signal ChunkLengthError is never read"
    constant ChunkLength : integer := 32;
    signal sourceDone : std_logic; -- @suppress "signal sourceDone is never read"
    signal sourceStart : std_logic;
    constant StreamID : integer := 0;
    signal clk40_stable : std_logic;
    signal daq_reset : std_logic;
    signal daq_fifo_flush : std_logic;



begin

    register_map_control.SUPER_CHUNK_FACTOR_LINK <= (others => x"01");
    register_map_control.TIMEOUT_CTRL.TIMEOUT <= x"0000FFFF";
    register_map_control.TIMEOUT_CTRL.ENABLE <= "1";
    -----------------------------------------------------------------------------
    -- Clock Generator
    -----------------------------------------------------------------------------
    clock_generator(clk40, clk40_en, clk40_period, "40 MHz CLK");
    clock_generator(clk160, clk40_en, clk160_period, "160 MHz CLK");
    clock_generator(clk250, clk40_en, clk250_period, "250 MHz CLK");
    clock_generator(aclk64, clk40_en, aclk64_period, "365 MHz CLK");

    -- fhAxis_aclk <= clk160;

    sink0: entity work.FELIXDataSink
        generic map(
            NUMBER_OF_DESCRIPTORS => NUMBER_OF_DESCRIPTORS,
            BLOCKSIZE => BLOCKSIZE,
            GBT_NUM => GBT_NUM,
            FIRMWARE_MODE => FIRMWARE_MODE,
            DATA_WIDTH => DATA_WIDTH,
            SIM_NUMBER_OF_BLOCKS => SIM_NUMBER_OF_BLOCKS,
            APPLY_BACKPRESSURE_AFTER_BLOCKS => APPLY_BACKPRESSURE_AFTER_BLOCKS
        )
        port map(
            checker_done => checker_done,
            toHostFifo_din => toHostFifo_din,
            toHostFifo_wr_en => toHostFifo_wr_en,
            toHostFifo_prog_full => toHostFifo_prog_full,
            toHostFifo_wr_clk => toHostFifo_wr_clk,
            reset => reset,
            start_check => start_check,
            Trunc => Trunc,
            TrailerError => TrailerError,
            CRCError => CRCError,
            ChunkLengthError => ChunkLengthError,
            register_map_control => register_map_control
        );

    resetmgr0: entity work.CRresetManager
        port map(
            clk40         => clk40,
            rst           => reset,
            clk40_stable  => clk40_stable,
            cr_rst        => daq_reset,
            cr_fifo_flush => daq_fifo_flush
        );

    g_URAM: if USE_URAM generate
        g_axis32: if LINK_CONFIG(0) = 0 generate
            toHostFifo_wr_clk <= clk250;
        else generate
            toHostFifo_wr_clk <= clk250;
        end generate;
    else generate --BRAM FIFO's in Virtex7 and Kintex Ultrascale
        g_toHostFifo_wr_clk_160: if FIRMWARE_MODE = FIRMWARE_MODE_GBT or FIRMWARE_MODE = FIRMWARE_MODE_LTDB or FIRMWARE_MODE = FIRMWARE_MODE_LPGBT generate
            toHostFifo_wr_clk <= clk160;
        else generate
            toHostFifo_wr_clk <= clk250;
        end generate;
    end generate;

    crth0: entity work.CRToHost
        generic map(
            NUMBER_OF_DESCRIPTORS => NUMBER_OF_DESCRIPTORS,
            NUMBER_OF_INTERRUPTS => NUMBER_OF_INTERRUPTS,
            LINK_NUM => LINK_NUM,
            LINK_CONFIG => LINK_CONFIG,
            toHostTimeoutBitn => toHostTimeoutBitn,
            STREAMS_TOHOST => STREAMS_TOHOST,
            BLOCKSIZE => BLOCKSIZE,
            DATA_WIDTH => DATA_WIDTH,
            FIRMWARE_MODE => FIRMWARE_MODE,
            USE_URAM => USE_URAM
        )
        port map(
            clk40 => clk40,
            aclk_tohost => clk250,
            aclk64_tohost => aclk64,
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            register_map_control => register_map_control,
            register_map_xoff_monitor => register_map_xoff_monitor,
            register_map_crtohost_monitor => register_map_crtohost_monitor,
            interrupt_call => interrupt_call,
            xoff_out => open,
            s_axis => s_axis,
            s_axis_tready => s_axis_tready,
            s_axis_prog_empty => s_axis_prog_empty,
            s_axis_aux => s_axis_aux,
            s_axis_aux_tready => s_axis_aux_tready,
            s_axis_aux_prog_empty => s_axis_aux_prog_empty,
            s_axis64 => s_axis64,
            s_axis64_tready => s_axis64_tready,
            s_axis64_prog_empty => s_axis64_prog_empty,
            dma_enable_in => (others => '1'),
            toHostFifo_din => toHostFifo_din,
            toHostFifo_wr_en => toHostFifo_wr_en,
            toHostFifo_prog_full => toHostFifo_prog_full,
            toHostFifo_wr_clk => toHostFifo_wr_clk,
            toHostFifo_rst => toHostFifo_rst
        );

    crfh0: entity work.CRFromHostAxis generic map(
            LINK_NUM => LINK_NUM,
            LINK_CONFIG => LINK_CONFIG,
            STREAMS_PER_LINK_FROMHOST => STREAMS_FROMHOST,
            DATA_WIDTH => DATA_WIDTH
        )
        port map(
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            fromHostFifo_clk => clk250,
            fromHostFifo_dout => fromHostFifo_dout,
            fromHostFifo_rd_en => fromHostFifo_rd_en,
            fromHostFifo_empty => fromHostFifo_empty,
            fromHostFifo_rst => fromHostFifo_rst,
            -- fhAxis_aclk => fhAxis_aclk,
            fhAxis => fhAxis,
            fhAxis_tready => fhAxis_tready,
            -- fhAxis64_aclk => clk250,
            fhAxis64 => fhAxis64,
            fhAxis64_tready => fhAxis64_tready,
            fifo_monitoring => fifo_monitoring,
            register_map_control => register_map_control
        );

    source0: entity work.FELIXDataSource     generic map(
            DATA_WIDTH => DATA_WIDTH,
            ThrottleData => FALSE--,
        --NEW_DATA_FORMAT => true
        )
        port map(
            clk => clk250,
            NumberOfChunks => SIM_NUMBER_OF_BLOCKS*2*(1024/ChunkLength),
            ChunkLength => ChunkLength,
            GBTId => 0,
            StreamID => StreamID,
            ElinkWidth => 32,
            fromHostFifo_dout => fromHostFifo_dout,
            fromHostFifo_empty => fromHostFifo_empty,
            fromHostFifo_rd_en => fromHostFifo_rd_en,
            start => sourceStart,
            done => sourceDone
        );

    g_fifos: for i in 0 to LINK_NUM-1 generate
        fifo0:entity work.Axis64Fifo     generic map(
                DEPTH => 2048/8
            )
            port map(
                s_axis_aresetn => aresetn,
                s_axis_aclk => clk250,
                s_axis => fhAxis64(i),
                s_axis_tready => fhAxis64_tready(i),
                m_axis_aclk => aclk64,
                m_axis => s_axis64(i),
                m_axis_tready => s_axis64_tready(i),
                m_axis_prog_empty => s_axis64_prog_empty(i)
            );
    end generate g_fifos;


    reset_proc: process
    begin
        aresetn <= '0';
        reset <= '1';
        wait for clk40_period * 10;
        aresetn <= '1';
        reset <= '0';
        wait;
    end process;



    ------------------------------------------------
    -- PROCESS: p_main
    ------------------------------------------------
    p_main: process
        constant C_SCOPE     : string  := C_TB_SCOPE_DEFAULT;





    begin
        clk40_stable <= '0';
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);
        enable_log_msg(ALL_MESSAGES);
        log(ID_LOG_HDR, "Simulation of TB for Encoding Epath", C_SCOPE);

        --enable clock
        clk40_en<= true;
        clk40_stable <= '1';

        --start simulation
        log(ID_LOG_HDR, "Starting simulation", C_SCOPE);
        sourceStart <= '1';

        log(ID_LOG_HDR, "Stopping simulation", C_SCOPE);


        --==================================================================================================
        -- Ending the simulation
        --------------------------------------------------------------------------------------
        await_value(checker_done, '1', 0 ns, 20 ms, ERROR, "waiting for checker_done");
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        -- Finish the simulation
        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely
    end process p_main;


end tb;

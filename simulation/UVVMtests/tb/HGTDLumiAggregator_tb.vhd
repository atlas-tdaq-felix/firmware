-- This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
-- Copyright (C) 2001-2022 CERN for the benefit of the ATLAS collaboration.
-- Authors:
--               Marius Wensing
--
--   Licensed under the Apache License, Version 2.0 (the "License");
--   you may not use this file except in compliance with the License.
--   You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--   Unless required by applicable law or agreed to in writing, software
--   distributed under the License is distributed on an "AS IS" BASIS,
--   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--   See the License for the specific language governing permissions and
--   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

library uvvm_util;
    context uvvm_util.uvvm_util_context;
library uvvm_vvc_framework;
    use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;
library bitvis_vip_axistream;
    use bitvis_vip_axistream.axistream_bfm_pkg.all;
    use bitvis_vip_axistream.vvc_methods_pkg.all;
    context bitvis_vip_axistream.vvc_context;

    use work.axi_stream_package.all;

entity HGTDLumiAggregator_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end HGTDLumiAggregator_tb;

architecture arch of HGTDLumiAggregator_tb is
    constant C_SCOPE : string := "HGTDLumiAggregator_tb";
    constant C_CLK_PERIOD : time := 25 ns;
    constant C_AXICLK_PERIOD : time := 5 ns;
    signal clk_ena : boolean;
    signal clk : std_logic;
    signal aclk : std_logic;
    signal rst : std_logic;

    constant LHC_TURNS : integer := 50;
    constant SIMULATED_BIT_ERROR_RATE : real := 0.0;
    signal source_data : std_logic_vector(15 downto 0);

    signal chA_data : std_logic_vector(15 downto 0);
    signal chA_m_axis : axis_32_type;
    signal chA_m_axis_tready : std_logic;
    signal chA_m_axis_prog_empty : std_logic; -- @suppress "signal chA_m_axis_prog_empty is never read"
    signal chA_synced : std_logic; -- @suppress "signal chA_synced is never read"
    signal chB_data : std_logic_vector(15 downto 0);
    signal chB_m_axis : axis_32_type;
    signal chB_m_axis_tready : std_logic;
    signal chB_m_axis_prog_empty : std_logic; -- @suppress "signal chB_m_axis_prog_empty is never read"
    signal chB_synced : std_logic; -- @suppress "signal chB_synced is never read"
    signal conf_lhcturns : std_logic_vector(9 downto 0);

    constant GC_AXISTREAM_BFM_CONFIG : t_axistream_bfm_config := C_AXISTREAM_BFM_CONFIG_DEFAULT;
    signal axistream_vvc_if_chA : t_axistream_if(tdata(31 downto 0), tkeep(3 downto 0), tuser(3 downto 0), tstrb(3 downto 0), tid(0 downto 0), tdest(0 downto 0)) := init_axistream_if_signals(false, 32, 4, 1, 1);
    signal axistream_vvc_if_chB : t_axistream_if(tdata(31 downto 0), tkeep(3 downto 0), tuser(3 downto 0), tstrb(3 downto 0), tid(0 downto 0), tdest(0 downto 0)) := init_axistream_if_signals(false, 32, 4, 1, 1);
begin

    -- Instantiate the concurrent procedure that initializes UVVM
    i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

    -- generate central clock
    clock_generator(clk, clk_ena, C_CLK_PERIOD, "LHC clock");
    clock_generator(aclk, clk_ena, C_AXICLK_PERIOD, "AXI-Stream clock");

    -- source for lumi data
    source: entity work.HGTDLumiDataSource
        generic map (
            SIMULATED_BIT_ERROR_RATE => SIMULATED_BIT_ERROR_RATE
        )
        port map (
            clk => clk,
            data => source_data
        );

    -- link source to both channels
    chA_data <= source_data;
    chB_data <= source_data;

    -- fixed setting for LHC turns
    conf_lhcturns <= std_logic_vector(to_unsigned(LHC_TURNS, conf_lhcturns'length));

    -- aggregator to be simulated
    dut: entity work.HGTDLumiAggregator
        port map (
            clk => clk,
            rst => rst,
            align_seq => x"4778",
            chA_enable => '1',
            chA_data => chA_data,
            chA_m_axis_aclk => aclk,
            chA_m_axis => chA_m_axis,
            chA_m_axis_tready => chA_m_axis_tready,
            chA_m_axis_prog_empty => chA_m_axis_prog_empty,
            chA_synced => chA_synced,
            chB_enable => '1',
            chB_data => chB_data,
            chB_m_axis_aclk => aclk,
            chB_m_axis => chB_m_axis,
            chB_m_axis_tready => chB_m_axis_tready,
            chB_m_axis_prog_empty => chB_m_axis_prog_empty,
            chB_synced => chB_synced,
            conf_lhcturns => conf_lhcturns
        );

    -- AXI stream VVC
    axistream_vvc_chA: entity bitvis_vip_axistream.axistream_vvc
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => false,
            GC_DATA_WIDTH => 32,
            GC_USER_WIDTH => 4,
            GC_ID_WIDTH => 1,
            GC_DEST_WIDTH => 1,
            GC_INSTANCE_IDX => 0,
            GC_AXISTREAM_BFM_CONFIG => GC_AXISTREAM_BFM_CONFIG
        )
        port map (
            clk => aclk,
            axistream_vvc_if => axistream_vvc_if_chA
        );

    axistream_vvc_if_chA.tkeep <= chA_m_axis.tkeep;
    axistream_vvc_if_chA.tuser <= chA_m_axis.tuser;
    axistream_vvc_if_chA.tdata <= chA_m_axis.tdata;
    axistream_vvc_if_chA.tvalid <= chA_m_axis.tvalid;
    axistream_vvc_if_chA.tlast <= chA_m_axis.tlast;
    chA_m_axis_tready <= axistream_vvc_if_chA.tready;

    axistream_vvc_chB: entity bitvis_vip_axistream.axistream_vvc
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => false,
            GC_DATA_WIDTH => 32,
            GC_USER_WIDTH => 4,
            GC_ID_WIDTH => 1,
            GC_DEST_WIDTH => 1,
            GC_INSTANCE_IDX => 1,
            GC_AXISTREAM_BFM_CONFIG => GC_AXISTREAM_BFM_CONFIG
        )
        port map (
            clk => aclk,
            axistream_vvc_if => axistream_vvc_if_chB
        );

    axistream_vvc_if_chB.tkeep <= chB_m_axis.tkeep;
    axistream_vvc_if_chB.tuser <= chB_m_axis.tuser;
    axistream_vvc_if_chB.tdata <= chB_m_axis.tdata;
    axistream_vvc_if_chB.tvalid <= chB_m_axis.tvalid;
    axistream_vvc_if_chB.tlast <= chB_m_axis.tlast;
    chB_m_axis_tready <= axistream_vvc_if_chB.tready;

    stim: process
        variable data_expected : t_byte_array(0 to 14259);

        function calc_expected_packet(turns : integer) return t_byte_array is
            variable word : std_logic_vector(31 downto 0);
            variable ret : t_byte_array(0 to 14259);
            variable sumW1 : integer;
            variable sumW2 : integer;
        begin
            -- data section
            for bcid in 0 to 3564 loop
                if bcid = 0 then
                    -- header
                    word := "1000000000000000000000" & std_logic_vector(to_unsigned(turns, 10));
                elsif bcid = 3564 then
                    -- trailer
                    word := (others => '0');
                else
                    -- data
                    sumW1 := turns * (bcid mod 128);
                    sumW2 := turns * (bcid mod 32);
                    word := std_logic_vector(to_unsigned(sumW1, 17)) & std_logic_vector(to_unsigned(sumW2, 15));
                end if;

                ret(4*bcid + 0) := word(7 downto 0);
                ret(4*bcid + 1) := word(15 downto 8);
                ret(4*bcid + 2) := word(23 downto 16);
                ret(4*bcid + 3) := word(31 downto 24);
            end loop;

            return ret;
        end function;
    begin
        -- defaults
        clk_ena <= false;
        rst <= '1';

        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);
        enable_log_msg(ALL_MESSAGES);
        disable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);

        -- Wait for UVVM to finish initialization
        await_uvvm_initialization(VOID);

        -- VVC configuration
        disable_log_msg(AXISTREAM_VVCT, 0, ID_PACKET_DATA); --@suppress
        disable_log_msg(AXISTREAM_VVCT, 1, ID_PACKET_DATA); --@suppress
        for i in 0 to 3 loop
            shared_axistream_vvc_config(i).bfm_config.max_wait_cycles := 10000000;
            shared_axistream_vvc_config(i).bfm_config.max_wait_cycles_severity := ERROR;
            shared_axistream_vvc_config(i).bfm_config.clock_period := C_AXICLK_PERIOD;
            shared_axistream_vvc_config(i).bfm_config.setup_time := C_AXICLK_PERIOD/4;
            shared_axistream_vvc_config(i).bfm_config.hold_time  := C_AXICLK_PERIOD/4;
            shared_axistream_vvc_config(i).bfm_config.clock_period_margin := 0 ns;
            shared_axistream_vvc_config(i).bfm_config.bfm_sync := SYNC_WITH_SETUP_AND_HOLD;
            shared_axistream_vvc_config(i).bfm_config.match_strictness := MATCH_EXACT;
            shared_axistream_vvc_config(i).bfm_config.byte_endianness := FIRST_BYTE_LEFT;
            shared_axistream_vvc_config(i).bfm_config.check_packet_length := false;
            shared_axistream_vvc_config(i).bfm_config.protocol_error_severity := ERROR;
        end loop;

        -- enable clock and reset DUT
        clk_ena <= true;
        rst <= '1';
        wait for 100 ns;
        rst <= '0';
        wait for 400 ns;

        -- receive one packet on each channel
        data_expected := calc_expected_packet(LHC_TURNS);
        axistream_expect(AXISTREAM_VVCT, 0, data_expected, "expect packet for chA");  --@suppress
        axistream_expect(AXISTREAM_VVCT, 1, data_expected, "expect packet for chB");  --@suppress
        await_completion(AXISTREAM_VVCT,0, 10 ms, "Wait for receive to finish on chA");  --@suppress
        await_completion(AXISTREAM_VVCT,1, 10 ms, "Wait for receive to finish on chB");  --@suppress

        --==================================================================================================
        -- Ending the simulation
        --------------------------------------------------------------------------------------
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely


    end process;


end architecture;

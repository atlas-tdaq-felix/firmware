----------------------------------------------------------------------------------
--  ATLAS
--  FELIX UVVM test automation project
--  M. Trovato
--  ANL
--  January 2020
----------------------------------------------------------------------------------
-- file: DecodingGearBox_tb.vhd.
----------------------------------------------------------------------------------
library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.numeric_std_unsigned.all;

    use std.env.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

--library uvvm_vvc_framework;
--use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

--use work.vvc_methods_pkg.all;
--use work.td_vvc_framework_common_methods_pkg.all;
--use work.egr_bfm_pkg.all;

    use work.pcie_package.all;
    use work.FELIX_package.all;
    use work.PCK_CRC16_D32.all;

-- Test bench entity
entity ltittc_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end ltittc_tb;

architecture tb of ltittc_tb is
    signal register_map_control        : register_map_control_type;

    signal rst_hw                      : std_logic                      := '0';

    signal clk40                       : std_logic                      := '0';
    signal clk240                      : std_logic                      := '0';

    constant clk240_period : time := 4.15 ns;


    --type array_184b is array (natural range <>) of std_logic_vector(183 downto 0);

    signal decoder_aligned             : std_logic                      := '0';

    signal axi_miso_ttc_lti : axi_miso_type; -- @suppress "Signal axi_miso_ttc_lti is never read"
    signal LTI2cips_gpio : STD_LOGIC_VECTOR ( 3 downto 0 ); -- @suppress "Signal LTI2cips_gpio is never read"
    constant axi_mosi_ttc_lti : axi_mosi_type:=
    (
      AXI_LITE_0_araddr  => x"0000",--:  STD_LOGIC_VECTOR ( 15 downto 0 );
      AXI_LITE_0_arvalid => '0',--:  STD_LOGIC;
      AXI_LITE_0_awaddr  => x"0000",--:  STD_LOGIC_VECTOR ( 15 downto 0 );
      AXI_LITE_0_awvalid => '0',--:  STD_LOGIC;
      AXI_LITE_0_bready  => '0',--:  STD_LOGIC;
      AXI_LITE_0_rready  => '0',--: std_logic;
      AXI_LITE_0_wdata   => x"00000000",--: std_logic_vector(31 downto 0);
      AXI_LITE_0_wvalid  => '0',--: std_logic;
      s_axi_lite_resetn_0 => '1'--: std_logic;
    );
    constant cips2LTI_gpio : STD_LOGIC_VECTOR ( 3 downto 0 ) := "0000";
    signal ltidata_c_index: integer range 0 to 1013 := 0;
    signal ltidata_c: array_36b(0 to 1013) := (
                                                  x"19c1a50bc",x"086d10000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"14ceb50bc",x"086d20000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1081b50bc",x"086d30000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1f44a50bc",x"086d40000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"181fb50bc",x"080000000",x"0000002aa",x"0aaaaaaaa",x"0aaaaaaaa",x"0aaaaaaaa",
                                                  x"1087a50bc",x"006d60000",x"00000003a",x"00002795a",x"0060adfdb",x"000000000",
                                                  x"1a5be50bc",x"006d70000",x"00000007a",x"00002795a",x"0060adfdb",x"000000000",
                                                  x"1a9de50bc",x"086d80000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1d23850bc",x"086d90000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"12e6950bc",x"086da0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"16a9950bc",x"086db0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"196c850bc",x"086dc0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1e37950bc",x"086dd0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"11f2850bc",x"086de0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"15bd850bc",x"086df0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1a78950bc",x"086e00000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1bfb550bc",x"086e10000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"143e450bc",x"086e20000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1071450bc",x"086e30000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1fb4550bc",x"086e40000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"18ef450bc",x"086e50000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"172a550bc",x"086e60000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1365550bc",x"086e70000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1ca0450bc",x"086e80000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1dd3750bc",x"086e90000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1216650bc",x"086ea0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1659650bc",x"086eb0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"199c750bc",x"086ec0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1ec7650bc",x"086ed0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1102750bc",x"086ee0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"154d750bc",x"086ef0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1a88650bc",x"086f00000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"17ab150bc",x"086f10000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"186e050bc",x"086f20000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1c21050bc",x"086f30000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"13e4150bc",x"086f40000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"14bf050bc",x"086f50000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1b7a150bc",x"086f60000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1f35150bc",x"086f70000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"10f0050bc",x"086f80000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1183350bc",x"086f90000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1e46250bc",x"086fa0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1a09250bc",x"086fb0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"15cc350bc",x"086fc0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1297250bc",x"086fd0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1d52350bc",x"006fe0000",x"00000003b",x"00002795a",x"0060adfdb",x"000000000",
                                                  x"19df650bc",x"006ff0000",x"00000007b",x"00002795a",x"0060adfdb",x"000000000",
                                                  x"1919650bc",x"087000000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1d9d350bc",x"087010000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1258250bc",x"087020000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1617250bc",x"087030000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"19d2350bc",x"087040000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1e89250bc",x"087050000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"114c350bc",x"087060000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1503350bc",x"087070000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1ac6250bc",x"087080000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1bb5150bc",x"087090000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1470050bc",x"0870a0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"103f050bc",x"0870b0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1ffa150bc",x"0870c0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"18a1050bc",x"0870d0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1764150bc",x"0870e0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"132b150bc",x"0870f0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1cee050bc",x"087100000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"11cd750bc",x"087110000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1e08650bc",x"087120000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1a47650bc",x"087130000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1582750bc",x"087140000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"12d9650bc",x"087150000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1d1c750bc",x"087160000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1953750bc",x"087170000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1696650bc",x"087180000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"17e5550bc",x"087190000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1820450bc",x"0871a0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1c6f450bc",x"0871b0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"13aa550bc",x"0871c0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"14f1450bc",x"0871d0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1b34550bc",x"0871e0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1f7b550bc",x"0871f0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"10be450bc",x"087200000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"113d850bc",x"087210000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1ef8950bc",x"087220000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1ab7950bc",x"087230000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1572850bc",x"087240000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1229950bc",x"087250000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1dec850bc",x"007260000",x"00000003c",x"00002795a",x"0060adfdb",x"000000000",
                                                  x"1a45f50bc",x"007270000",x"00000007c",x"00002795a",x"0060adfdb",x"000000000",
                                                  x"1a83f50bc",x"087280000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1715a50bc",x"087290000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"18d0b50bc",x"0872a0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1c9fb50bc",x"0872b0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"135aa50bc",x"0872c0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1401b50bc",x"0872d0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1bc4a50bc",x"0872e0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1f8ba50bc",x"0872f0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"104eb50bc",x"087300000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1d6dc50bc",x"087310000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"12a8d50bc",x"087320000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"16e7d50bc",x"087330000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1922c50bc",x"087340000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1e79d50bc",x"087350000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"11bcc50bc",x"087360000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"15f3c50bc",x"087370000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1a36d50bc",x"087380000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1b45e50bc",x"087390000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1480f50bc",x"0873a0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"10cff50bc",x"0873b0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1f0ae50bc",x"0873c0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1851f50bc",x"0873d0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1794e50bc",x"0873e0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"13dbe50bc",x"0873f0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1c1ef50bc",x"087400000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"10dc650bc",x"087410000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1f19750bc",x"087420000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1b56750bc",x"087430000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1493650bc",x"087440000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"13c8750bc",x"087450000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1c0d650bc",x"087460000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1842650bc",x"087470000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1787750bc",x"087480000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"16f4450bc",x"087490000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1931550bc",x"0874a0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1d7e550bc",x"0874b0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"12bb450bc",x"0874c0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"15e0550bc",x"0874d0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1a25450bc",x"0074e0000",x"00000003d",x"00002795a",x"0060adfdb",x"000000000",
                                                  x"1480250bc",x"0074f0000",x"00000007d",x"00002795a",x"0060adfdb",x"000000000",
                                                  x"1446250bc",x"087500000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1c8c250bc",x"087510000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1349350bc",x"087520000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1706350bc",x"087530000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"18c3250bc",x"087540000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1f98350bc",x"087550000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"105d250bc",x"087560000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1412250bc",x"087570000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1bd7350bc",x"087580000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1aa4050bc",x"087590000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1561150bc",x"0875a0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"112e150bc",x"0875b0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1eeb050bc",x"0875c0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"19b0150bc",x"0875d0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1675050bc",x"0875e0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"123a050bc",x"0875f0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1dff150bc",x"087600000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1c7cd50bc",x"087610000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"13b9c50bc",x"087620000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"17f6c50bc",x"087630000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1833d50bc",x"087640000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1f68c50bc",x"087650000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"10add50bc",x"087660000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"14e2d50bc",x"087670000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1b27c50bc",x"087680000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1a54f50bc",x"087690000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1591e50bc",x"0876a0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"11dee50bc",x"0876b0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1e1bf50bc",x"0876c0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1940e50bc",x"0876d0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1685f50bc",x"0876e0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"12caf50bc",x"0876f0000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1d0fe50bc",x"087700000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"102c950bc",x"087710000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1fe9850bc",x"087720000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1ba6850bc",x"087730000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1463950bc",x"087740000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1338850bc",x"087750000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1cfd950bc",x"007760000",x"00000003e",x"00002795a",x"0060adfdb",x"000000000",
                                                  x"1d4cf50bc",x"007770000",x"00000007e",x"00002795a",x"0060adfdb",x"000000000",
                                                  x"1d8af50bc",x"087780000",x"000000000",x"000000000",x"000000000",x"000000000",
                                                  x"1604b50bc",x"087790000",x"000000000",x"000000000",x"000000000",x"000000000");

    constant GBT_NUM : integer := 4;
    signal register_map_ltittc_monitor : register_map_ltittc_monitor_type; -- @suppress "Signal register_map_ltittc_monitor is never read"
    signal TTC_out : TTC_data_type; -- @suppress "Signal TTC_out is never read"
    signal clk40_ttc_out : std_logic; -- @suppress "Signal clk40_ttc_out is never read"
    signal cdrlocked_out : std_logic; -- @suppress "Signal cdrlocked_out is never read"
    signal TTC_ToHost_Data_out : TTC_data_type; -- @suppress "Signal TTC_ToHost_Data_out is never read"
    signal BUSY_IN : std_logic;
    signal GTREFCLK0_P_IN : std_logic;
    signal GTREFCLK0_N_IN : std_logic;
    signal GTREFCLK1_P_IN : std_logic_vector(NUM_LTITTC_GTREFCLK1(712,'1')-1 downto 0);
    signal GTREFCLK1_N_IN : std_logic_vector(NUM_LTITTC_GTREFCLK1(712,'1')-1 downto 0);
    signal FLX182_LTI_GTREFCLK1_in : std_logic;
    constant RX_P_LTITTC : std_logic := '0';
    constant RX_N_LTITTC : std_logic := '1';
    signal TX_P_LTITTC : std_logic; -- @suppress "Signal TX_P_LTITTC is never read"
    signal TX_N_LTITTC : std_logic; -- @suppress "Signal TX_N_LTITTC is never read"
    signal RXUSRCLK_LTI : std_logic; -- @suppress "Signal RXUSRCLK_LTI is never read"
    signal TXUSRCLK_LTI : std_logic; -- @suppress "Signal TXUSRCLK_LTI is never read"
    signal axi_clk_in : std_logic;
    signal LTI_TXUSRCLK_in : std_logic_vector(GBT_NUM - 1 downto 0);
    signal LTI_TX_Data_Transceiver_out : array_32b(0 to GBT_NUM - 1); -- @suppress "Signal LTI_TX_Data_Transceiver_out is never read"
    signal LTI_TX_TX_CharIsK_out : array_4b(0 to GBT_NUM - 1); -- @suppress "Signal LTI_TX_TX_CharIsK_out is never read"
    signal toHostXoff : std_logic_vector(GBT_NUM-1 downto 0);
    signal LTITTC_TTYPE_MONITOR_VALUE : std_logic_vector(31 downto 0);
    signal LTITTC_SL0ID_MONITOR_VALUE : std_logic_vector(31 downto 0);
    signal LTITTC_SORB_MONITOR_VALUE : std_logic_vector(31 downto 0);
    signal LTITTC_GRST_MONITOR_VALUE : std_logic_vector(31 downto 0);
    signal LTITTC_SYNC_MONITOR_VALUE : std_logic_vector(31 downto 0);
    signal LTITTC_BCR_ERR_MONITOR_VALUE : std_logic_vector(31 downto 0);
    signal LTITTC_L0ID_ERR_MONITOR_VALUE : std_logic_vector(31 downto 0);
    signal LTITTC_CRC_ERR_MONITOR_VALUE : std_logic_vector(31 downto 0);
begin

    --! The constants in ltidata_c contain a CRC value that was taken from a snapshot, before the correct algorithm
    --! was known. We are using the function provided in FLX-2502 here to calculate the correct CRC from the LTI
    --! to also verify that our LTI-decoder CRC is correct
    initCRC: process
        variable index: integer := 0;
        variable crc: std_logic_vector(15 downto 0) := x"FFFF";
    begin
        wait for 1 ns;
        for i in 1 to 1013 loop
            if index < 5 then
                crc := nextCRC16_D32(ltidata_c(i)(31 downto 0), crc);
                index := index + 1;
            else
                ltidata_c(i)(31 downto 16) <= crc;
                crc := x"FFFF";
                index := 0;
            end if;
        end loop;
        ltidata_c(0)(31 downto 16) <= crc;
        wait;
    end process;


    BUSY_IN <= '0';
    toHostXoff <= (others => '0');

    -----------------------------------------------------------------------------
    -- Clock Generator
    -----------------------------------------------------------------------------
    clk40            <= not clk40 after 12.5 ns;

    clk_proc: process
    begin
        clk240 <= '1';
        wait for clk240_period / 2;
        clk240 <= '0';
        wait for clk240_period / 2;
    end process;






    rst_hw_proc: process
    begin
        rst_hw <= '1';
        wait for 25 ns;
        rst_hw <= '0';
        wait;
    end process;


    encdata_proc: process(rst_hw, RXUSRCLK_LTI)
    begin
        if rst_hw = '1' then
            ltidata_c_index <= 0;
        elsif rising_edge(RXUSRCLK_LTI) then
            if ltidata_c_index < 1013 then
                ltidata_c_index <= ltidata_c_index + 1;
            else
                ltidata_c_index <= 0;
            end if;

        end if;
    end process;


    GTREFCLK0_P_IN <= clk240;
    GTREFCLK0_N_IN <= not clk240;
    GTREFCLK1_P_IN <= (others => clk240);
    GTREFCLK1_N_IN <= (others => not clk240);
    FLX182_LTI_GTREFCLK1_in <= clk240;
    axi_clk_in <= clk40;
    LTI_TXUSRCLK_in <= (others => clk240);
    decoder_aligned <= TTC_out.LTI_decoder_aligned;

    uut: entity work.ltittc_wrapper generic map(
            CARD_TYPE => 712,
            ITK_TRIGTAG_MODE => 2,
            GBT_NUM => GBT_NUM,
            FIRMWARE_MODE => FIRMWARE_MODE_FULL,
            SIMULATION => true
        )
        port map(
            reset_in => rst_hw,
            register_map_control => register_map_control,
            register_map_ltittc_monitor => register_map_ltittc_monitor,
            TTC_out => TTC_out,
            clk40_ttc_out => clk40_ttc_out,
            clk40_in => clk40,
            cdrlocked_out => cdrlocked_out,
            TTC_ToHost_Data_out => TTC_ToHost_Data_out,
            BUSY_IN => BUSY_IN,
            GTREFCLK0_P_IN => GTREFCLK0_P_IN,
            GTREFCLK0_N_IN => GTREFCLK0_N_IN,
            GTREFCLK1_P_IN => GTREFCLK1_P_IN,
            GTREFCLK1_N_IN => GTREFCLK1_N_IN,
            FLX182_LTI_GTREFCLK1_in => FLX182_LTI_GTREFCLK1_in,
            LMK_locked_in => '1',
            RX_P_LTITTC => RX_P_LTITTC,
            RX_N_LTITTC => RX_N_LTITTC,
            TX_P_LTITTC => TX_P_LTITTC,
            TX_N_LTITTC => TX_N_LTITTC,
            RXUSRCLK_LTI => RXUSRCLK_LTI,
            TXUSRCLK_LTI => TXUSRCLK_LTI,
            axi_clk_in => axi_clk_in,
            axi_miso_ttc_lti => axi_miso_ttc_lti,
            axi_mosi_ttc_lti => axi_mosi_ttc_lti,
            LTI2cips_gpio => LTI2cips_gpio,
            cips2LTI_gpio => cips2LTI_gpio,
            LTI_TXUSRCLK_in => LTI_TXUSRCLK_in,
            LTI_TX_Data_Transceiver_out => LTI_TX_Data_Transceiver_out,
            LTI_TX_TX_CharIsK_out => LTI_TX_TX_CharIsK_out,
            toHostXoff => toHostXoff,
            sim_rxdata_in => ltidata_c(ltidata_c_index)
        );

    ltittc_mon_counters: entity work.ltittc_mon_counters
        port map(
            clk40                           => clk40,
            register_map_control            => register_map_control,
            l0id                            => TTC_out.l0id,
            sl0id                           => TTC_out.sl0id,
            l0a                             => TTC_out.l0a,
            typemsg                         => not TTC_out.L0A,
            ttype                           => TTC_out.TriggerType,
            bcid                            => TTC_out.bcid,
            sorb                            => TTC_out.sorb,
            sync                            => TTC_out.sync,
            grst                            => TTC_out.grst,
            crc_valid                       => TTC_out.LTI_crc_valid,
            LTITTC_TTYPE_MONITOR_VALUE => LTITTC_TTYPE_MONITOR_VALUE,
            LTITTC_SL0ID_MONITOR_VALUE => LTITTC_SL0ID_MONITOR_VALUE,
            LTITTC_SORB_MONITOR_VALUE => LTITTC_SORB_MONITOR_VALUE,
            LTITTC_GRST_MONITOR_VALUE => LTITTC_GRST_MONITOR_VALUE,
            LTITTC_SYNC_MONITOR_VALUE => LTITTC_SYNC_MONITOR_VALUE,
            LTITTC_BCR_ERR_MONITOR_VALUE => LTITTC_BCR_ERR_MONITOR_VALUE,
            LTITTC_L0ID_ERR_MONITOR_VALUE => LTITTC_L0ID_ERR_MONITOR_VALUE,
            LTITTC_CRC_ERR_MONITOR_VALUE => LTITTC_CRC_ERR_MONITOR_VALUE
        );

    check_proc : process
    begin
        ---------------
        ---REGISTERS---
        ---------------
        register_map_control.TTC_EMU_RESET  <= "1";
        register_map_control.LTITTC_CTRL.LTITTC_CHANNEL_DISABLE      <= "0";
        register_map_control.LTITTC_CTRL.LTITTC_GENERAL_CTRL         <= "00"; --do not the logic for 000
        register_map_control.LTITTC_CTRL.LTITTC_SOFT_RESET           <= "0";
        register_map_control.LTITTC_CTRL.LTITTC_CPLL_RESET           <= "0";
        register_map_control.LTITTC_CTRL.LTITTC_SOFT_TX_RESET        <= "0";
        register_map_control.LTITTC_CTRL.LTITTC_SOFT_RX_RESET        <= "0";
        register_map_control.LTITTC_CTRL.LTITTC_GTH_LOOPBACK_CONTROL <= "000";
        register_map_control.LTITTC_BCR_ERR_MONITOR.CLEAR  <= "0";
        register_map_control.LTITTC_L0ID_ERR_MONITOR.CLEAR <= "0";
        register_map_control.LTITTC_GRST_MONITOR.CLEAR     <= "0";
        register_map_control.LTITTC_CRC_ERR_MONITOR.CLEAR  <= "0";
        register_map_control.LTITTC_SL0ID_MONITOR.CLEAR    <= "0";
        register_map_control.LTITTC_SORB_MONITOR.CLEAR     <= "0";
        register_map_control.LTITTC_SYNC_MONITOR.CLEAR     <= "0";
        register_map_control.LTITTC_TTYPE_MONITOR.CLEAR    <= "0";

        register_map_control.TTC_DEC_CTRL.TOHOST_RST                 <= "0";
        register_map_control.TTC_ECR_MONITOR.CLEAR                   <= "0";
        register_map_control.TTC_TTYPE_MONITOR.CLEAR                 <= "0";
        register_map_control.TTC_BCR_PERIODICITY_MONITOR.CLEAR       <= "0";

        register_map_control.TTC_DEC_CTRL.XL1ID_RST <= "1";

        register_map_control.TTC_EMU_CONTROL.PT <= "0";
        register_map_control.TTC_EMU_CONTROL.PARTITION <= "00";
        register_map_control.TTC_EMU_CONTROL.SYNC <= "0";
        register_map_control.TTC_EMU_CONTROL.GRST <= "0";
        register_map_control.TTC_EMU_CONTROL.SYNC_USER_DATA <= x"0000";
        register_map_control.TTC_EMU_CONTROL.SYNC_GLOBAL_DATA <= x"0000";
        register_map_control.TTC_EMU_TP_DELAY <= x"0000_0000";
        register_map_control.TTC_EMU_L1A_PERIOD <= x"0000_0100";
        register_map_control.TTC_EMU_ECR_PERIOD <= x"0001_0000";
        register_map_control.TTC_EMU_BCR_PERIOD <= std_logic_vector(to_unsigned(3564, 32));

        register_map_control.TTC_EMU_LONG_CHANNEL_DATA <= x"DEAD_BEEF"; --AS: used for individually addressed command
        register_map_control.TTC_EMU_CONTROL.BROADCAST <= (others => '0');


        register_map_control.TTC_EMU_CONTROL.L1A <= "0";
        register_map_control.TTC_EMU_CONTROL.ECR <= "0";
        register_map_control.TTC_EMU_CONTROL.BCR <= "0";
        register_map_control.TTC_EMU_CONTROL.BUSY_IN_ENABLE <= "0";
        register_map_control.TTC_L1A_DELAY <= (others => '0');
        register_map_control.TTC_ASYNCUSERDATA.WR_EN <= "0";
        register_map_control.TTC_ASYNCUSERDATA.DATA <= (others => '0');

        register_map_control.TTC_EMU.SEL   <= "0";
        register_map_control.TTC_EMU.ENA                             <= "1";
        wait for 200 ns; --before that decoder_aligned is unstable...eyeballed
        register_map_control.TTC_EMU_RESET <= "0";

        register_map_control.TTC_DEC_CTRL.XL1ID_RST  <= "0";
        check_value(decoder_aligned, '1', ERROR, "CheckDecoder Alignment", C_SCOPE);
        check_value(TTC_out.LTI_CRC_valid, '1', ERROR, "check CRC valid", C_SCOPE);
        wait for 1000 us;
        check_stable(decoder_aligned, 1000 us, ERROR, "Check that decoder alignment is stable", C_SCOPE);
        check_stable(TTC_out.LTI_CRC_valid, 1000 us, ERROR, "Check that crc valid is stable", C_SCOPE);

        register_map_control.TTC_EMU.SEL                             <= "1";
        register_map_control.TTC_EMU.ENA                             <= "1";
        register_map_control.TTC_EMU_RESET <= "1";
        wait for 100 ns;
        register_map_control.TTC_EMU_RESET <= "0";
        wait for 1 us;
        register_map_control.LTITTC_BCR_ERR_MONITOR.CLEAR  <= "1";
        register_map_control.LTITTC_L0ID_ERR_MONITOR.CLEAR <= "1";
        register_map_control.LTITTC_GRST_MONITOR.CLEAR     <= "1";
        register_map_control.LTITTC_CRC_ERR_MONITOR.CLEAR  <= "1";
        register_map_control.LTITTC_SL0ID_MONITOR.CLEAR    <= "1";
        register_map_control.LTITTC_SORB_MONITOR.CLEAR     <= "1";
        register_map_control.LTITTC_SYNC_MONITOR.CLEAR     <= "1";
        register_map_control.LTITTC_TTYPE_MONITOR.CLEAR    <= "1";
        wait for 100 ns;
        register_map_control.LTITTC_BCR_ERR_MONITOR.CLEAR  <= "0";
        register_map_control.LTITTC_L0ID_ERR_MONITOR.CLEAR <= "0";
        register_map_control.LTITTC_GRST_MONITOR.CLEAR     <= "0";
        register_map_control.LTITTC_CRC_ERR_MONITOR.CLEAR  <= "0";
        register_map_control.LTITTC_SL0ID_MONITOR.CLEAR    <= "0";
        register_map_control.LTITTC_SORB_MONITOR.CLEAR     <= "0";
        register_map_control.LTITTC_SYNC_MONITOR.CLEAR     <= "0";
        register_map_control.LTITTC_TTYPE_MONITOR.CLEAR    <= "0";
        wait for 1000 us;
        check_value(LTITTC_BCR_ERR_MONITOR_VALUE, x"0000_0000", ERROR, "Check that BRC_ERR_MONITOR is 0", C_SCOPE);
        check_value(LTITTC_L0ID_ERR_MONITOR_VALUE, x"0000_0000", ERROR, "Check that L0ID_ERR_MONITOR is 0", C_SCOPE);
        check_value(LTITTC_CRC_ERR_MONITOR_VALUE, x"0000_0000", ERROR, "Check that CRC_ERR_MONITOR is 0", C_SCOPE);
        check_stable(LTITTC_BCR_ERR_MONITOR_VALUE, 1000 us, ERROR, "Check that LTITTC_BCR_ERR_MONITOR has not increased", C_SCOPE);
        check_stable(LTITTC_L0ID_ERR_MONITOR_VALUE, 1000 us, ERROR, "Check that LTITTC_L0ID_ERR_MONITOR has not increased", C_SCOPE);
        check_stable(LTITTC_CRC_ERR_MONITOR_VALUE, 1000 us, ERROR, "Check that LTITTC_CRC_ERR_MONITOR has not increased", C_SCOPE);
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);
        -- Finish the simulation
        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely
    end process;



end tb;

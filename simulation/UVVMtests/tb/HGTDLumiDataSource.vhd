--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use IEEE.MATH_REAL.ALL;
    use work.code6b8b_package.ALL;

entity HGTDLumiDataSource is
    generic (
        SIMULATED_BIT_ERROR_RATE : real := 0.0
    );
    port (
        clk : in std_logic;
        data : out std_logic_vector(15 downto 0)
    );
end HGTDLumiDataSource;

architecture sim of HGTDLumiDataSource is
    signal bcid : integer range 0 to 3563 := 0;
begin

    process (clk)
        variable r : real;
        variable seed1, seed2 : integer := 999; -- @suppress "The type of a variable has to be constrained in size"
        variable W1 : std_logic_vector(6 downto 0);
        variable W2 : std_logic_vector(4 downto 0);
        variable error_vect : std_logic_vector(15 downto 0);
        variable data_vect : std_logic_vector(15 downto 0);
    begin
        if rising_edge(clk) then
            -- calculate an error vector based on the provided bit error rate
            if SIMULATED_BIT_ERROR_RATE = 0.0 then
                error_vect := (others => '0');
            else
                for I in 0 to 15 loop
                    uniform(seed1, seed2, r);
                    if r <= SIMULATED_BIT_ERROR_RATE then
                        error_vect(I) := '1';
                    else
                        error_vect(I) := '0';
                    end if;
                end loop;
            end if;

            -- generate lumi data
            if bcid = 0 then
                data_vect := x"4778";      -- send ALTIROC_LUMI_IDLE_WORD at BCID 0
            else
                W1 := std_logic_vector(to_unsigned(bcid mod 128, 7));
                W2 := std_logic_vector(to_unsigned(bcid mod 32, 5));
                data_vect := encode_6b8b(W2 & W1(6), '0') & encode_6b8b(W1(5 downto 0), '0');
            end if;

            -- count BCID
            if bcid < 3563 then
                bcid <= bcid + 1;
            else
                bcid <= 0;
            end if;

            -- output data
            for I in 0 to 15 loop
                data(I) <= data_vect(I) xor error_vect(I);
            end loop;
        end if;
    end process;

end architecture;

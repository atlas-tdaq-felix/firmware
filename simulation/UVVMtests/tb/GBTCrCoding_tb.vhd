--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!               Filiberto Bonini
--!               Nico Giangiacomi
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- file: GBTCrCoding_tb.vhd.
-- Testbench to excercise CrFromHost, CRToHost, encoding, decoding in GBT mode
-- Using data derived from fuptest.
----------------------------------------------------------------------------------
library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

    use work.pcie_package.all;
    use work.lpgbtfpga_package.all;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.axi_stream_package.all;
    use work.interlaken_package.slv_67_array;



-- Test bench entity
entity GBTCrCoding_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity;

-- Test bench architecture
architecture arch of GBTCrCoding_tb is

    constant C_SCOPE : string := "GBTLinkToHost_tb";
    constant C_CLK240_PERIOD : time    := 4.1 ns;
    constant C_CLK40_PERIOD  : time    := C_CLK240_PERIOD*6;
    constant C_CLK160_PERIOD : time    := C_CLK40_PERIOD/4;
    constant C_CLK250_PERIOD : time    := 4 ns;

    constant GBT_NUM : integer := 2;
    constant ENDPOINTS : integer := 1;
    constant FIRMWARE_MODE : integer := FIRMWARE_MODE_GBT;
    constant NUMBER_OF_DESCRIPTORS : integer := 2;
    constant STREAMS_TOHOST: integer := STREAMS_TOHOST_MODE(FIRMWARE_MODE);
    constant GROUP_CONFIG_FROMHOST : IntArray(0 to 7) := STREAMS_FROMHOST_MODE(FIRMWARE_MODE);
    constant STREAMS_FROMHOST: integer := sum(GROUP_CONFIG_FROMHOST);
    constant BLOCKSIZE : integer :=1024;
    constant LOCK_PERIOD : integer := 640;
    constant DATA_WIDTH : integer := 256;
    constant SIM_NUMBER_OF_BLOCKS : integer := 300;

    constant USE_URAM : boolean := false;


    constant PCIE_ENDPOINT: integer := 0;
    signal register_map_xoff_monitor           : register_map_xoff_monitor_type; -- @suppress "signal register_map_xoff_monitor is never read"
    signal register_map_gbtemu_monitor         : register_map_gbtemu_monitor_type; -- @suppress "Unused declaration"
    signal register_map_decoding_monitor       : register_map_decoding_monitor_type; -- @suppress "signal register_map_decoding_monitor is never read"
    signal register_map_40_control             : register_map_control_type;
    signal toHostFifo_din                      : slv_array(0 to NUMBER_OF_DESCRIPTORS-2);
    signal toHostFifo_wr_en                    : std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
    signal toHostFifo_prog_full                : std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
    signal toHostFifo_wr_clk                   : std_logic;
    signal decoding_aclk                       : std_logic;
    signal decoding_axis                       : axis_32_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
    signal decoding_axis_tready                : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
    signal decoding_axis_prog_empty            : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);


    signal LinkAligned                         : std_logic_vector(GBT_NUM-1 downto 0);
    signal RXUSRCLK                            : std_logic_vector(GBT_NUM-1 downto 0);

    signal clk40, clk240, clk250, clk160  : std_logic:='0';
    signal clock_ena     : boolean   := false;

    signal reset : std_logic;

    signal Trunc      : std_logic:='0'; -- @suppress "signal Trunc is never read"
    signal TrailerError:std_logic:='0'; -- @suppress "signal TrailerError is never read"
    signal CRCError : std_logic:='0'; -- @suppress "signal CRCError is never read"
    signal ChunkLengthError: std_logic:='0'; -- @suppress "signal ChunkLengthError is never read"


    signal GTH_FM_RX_33b_out                   : array_33b(0 to (GBT_NUM-1));

    signal checker_done: std_logic;
    constant IncludeDecodingEpath2_HDLC : std_logic_vector(6 downto 0) := (others => '1');
    constant IncludeDecodingEpath2_8b10b : std_logic_vector(6 downto 0) := (others => '1');
    constant IncludeDecodingEpath4_8b10b : std_logic_vector(6 downto 0) := (others => '1');
    constant IncludeDecodingEpath8_8b10b : std_logic_vector(6 downto 0) := (others => '1');
    constant IncludeDecodingEpath16_8b10b : std_logic_vector(6 downto 0) := (others => '1');
    constant IncludeDecodingEpath32_8b10b : std_logic_vector(6 downto 0) := (others => '1');

    signal TTCin: TTC_data_type;
    signal register_map_crtohost_monitor : register_map_crtohost_monitor_type; -- @suppress "signal register_map_crtohost_monitor is never read"
    signal fromHostFifo_dout : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal fromHostFifo_rd_en : std_logic;
    signal fromHostFifo_empty : std_logic;
    -- signal fhAxis_aclk : std_logic;
    signal fhAxis : axis_8_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_FROMHOST-1);
    signal fhAxis_tready : axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_FROMHOST-1);
    signal fifo_monitoring : bitfield_crfromhost_fifo_status_r_type; -- @suppress "signal fifo_monitoring is never read"
    signal register_map_encoding_monitor : register_map_encoding_monitor_type; -- @suppress "signal register_map_encoding_monitor is never read"
    signal lpGBT_UPWNLINK_USER_DATA : array_32b(0 to GBT_NUM-1); -- @suppress "signal lpGBT_UPWNLINK_USER_DATA is never read"
    signal lpGBT_DOWNLINK_IC_DATA : array_2b(0 to GBT_NUM-1); -- @suppress "signal lpGBT_DOWNLINK_IC_DATA is never read"
    signal lpGBT_DOWNLINK_EC_DATA : array_2b(0 to GBT_NUM-1); -- @suppress "signal lpGBT_DOWNLINK_EC_DATA is never read"
    signal GBT_UPLINK_USER_DATA : array_120b(0 to GBT_NUM-1);
    signal DECODING_LINK_ALIGNED : std_logic_vector(GBT_NUM*40-1 downto 0);
    signal start_check : std_logic; -- @suppress "signal start_check is never read"

    constant NumberOfChunks : integer := 2;
    constant ChunkLength    : integer := 130;
    signal GBTId          : integer := 0;
    signal StreamID       : integer := 0;
    constant ElinkWidth     : integer := 4;
    signal sourceStart    : std_logic := '0';
    signal sourceDone     : std_logic;
    constant TTC_ToHost_Data_in : TTC_data_type := TTC_zero;
    constant ElinkBusyIn : array_57b(0 to GBT_NUM-1) := (others => (others => '0'));
    constant DmaBusyIn : std_logic := '0';
    constant FifoBusyIn : std_logic := '0';
    constant BusySumIn : std_logic := '0';
    signal decoding_axis_aux : axis_32_array_type(0 to 1);
    signal decoding_axis_aux_prog_empty : axis_tready_array_type(0 to 1);
    signal decoding_axis_aux_tready : axis_tready_array_type(0 to 1);
    constant LINK_CONFIG : IntArray(0 to GBT_NUM-1):=(others => 0);
    --signal fhAxis64_aclk : std_logic;
    signal fhAxis64 : axis_64_array_type(0 to GBT_NUM-1); -- @suppress "signal fhAxis64 is never read"
    signal fhAxis64_tready : axis_tready_array_type(0 to GBT_NUM-1); -- @suppress "signal fhAxis64_tready is never written"

    signal LTI_TXUSRCLK: std_logic_vector(GBT_NUM-1 downto 0);
    --signal LTI_TX_Data_Transceiver : array_32b(0 to GBT_NUM - 1);
    --signal LTI_TX_TX_CharIsK : array_4b(0 to GBT_NUM-1);
    constant Interlaken_RX_Data : slv_67_array(0 to GBT_NUM-1) := (others => (others => '0'));
    constant Interlaken_RX_Datavalid : std_logic_vector(GBT_NUM-1 downto 0) := (others => '0');
    signal clk40_stable : std_logic;
    signal daq_reset : std_logic;
    signal daq_fifo_flush : std_logic;


begin
    LTI_TXUSRCLK <= (others => clk240);
    TTCin <= TTC_zero;

    LinkAligned <= (others => '1');
    GTH_FM_RX_33b_out <= (others => (others => '0'));
    register_map_40_control.DECODING_REVERSE_10B <= "1";
    register_map_40_control.ENCODING_REVERSE_10B <= "1";
    register_map_40_control.TIMEOUT_CTRL.TIMEOUT <= x"0000FFFF";
    register_map_40_control.TIMEOUT_CTRL.ENABLE <= "1";
    toHostFifo_prog_full <= (others => '0');
    register_map_40_control.DECODING_HGTD_ALTIROC <= "0";

    GEN_SCF_LINKS: for i in 0 to 11 generate
        register_map_40_control.SUPER_CHUNK_FACTOR_LINK(i) <= x"01";
    end generate;


    g_EgroupConfig: process
    begin
        for i in 0 to GBT_NUM-1 loop
            register_map_40_control.BROADCAST_ENABLE(i) <= (others => '0');
            register_map_40_control.CRTOHOST_INSTANT_TIMEOUT_ENA(i) <= "000000000000001000000000000000000000000000";
            --register_map_40_control.CRTOHOST_INSTANT_TIMEOUT_ENA(i) <= "000000000000000000000000000000000000000000";
            for j in 0 to 4 loop
                register_map_40_control.ENCODING_EGROUP_CTRL(i)(j).ENABLE_DELAY <= "0";
                if ElinkWidth = 2 then
                    register_map_40_control.DECODING_EGROUP_CTRL(i)(j).REVERSE_ELINKS <= x"00"; -- enables bit reversing for the elink in the given epath -- @suppress "Dead code"
                    register_map_40_control.DECODING_EGROUP_CTRL(i)(j).PATH_ENCODING  <= x"11111111"; -- Encoding for every EPATH, 8 EPATHS per EGROUP 0: direct mode, 1: 8b10b mode, 2: HDLC mode
                    register_map_40_control.DECODING_EGROUP_CTRL(i)(j).EPATH_WIDTH    <= "000"; --2b elinks
                    register_map_40_control.DECODING_EGROUP_CTRL(i)(j).EPATH_ENA      <= "11111111"; --2b elinks
                    register_map_40_control.ENCODING_EGROUP_CTRL(i)(j).REVERSE_ELINKS <= x"00"; -- enables bit reversing for the elink in the given epath
                    register_map_40_control.ENCODING_EGROUP_CTRL(i)(j).PATH_ENCODING  <= x"11111111"; -- Encoding for every EPATH, 8 EPATHS per EGROUP 0: direct mode, 1: 8b10b mode, 2: HDLC mode
                    register_map_40_control.ENCODING_EGROUP_CTRL(i)(j).EPATH_WIDTH    <= "000"; --2b elinks
                    register_map_40_control.ENCODING_EGROUP_CTRL(i)(j).EPATH_ENA      <= "11111111"; --2b elinks
                elsif ElinkWidth = 4 then
                    register_map_40_control.DECODING_EGROUP_CTRL(i)(j).REVERSE_ELINKS <= x"00"; -- enables bit reversing for the elink in the given epath
                    register_map_40_control.DECODING_EGROUP_CTRL(i)(j).PATH_ENCODING  <= x"01010101"; -- Encoding for every EPATH, 8 EPATHS per EGROUP 0: direct mode, 1: 8b10b mode, 2: HDLC mode
                    register_map_40_control.DECODING_EGROUP_CTRL(i)(j).EPATH_WIDTH    <= "001"; --4b elinks
                    register_map_40_control.DECODING_EGROUP_CTRL(i)(j).EPATH_ENA      <= "01010101"; --4b elinks
                    register_map_40_control.ENCODING_EGROUP_CTRL(i)(j).REVERSE_ELINKS <= x"00"; -- enables bit reversing for the elink in the given epath
                    register_map_40_control.ENCODING_EGROUP_CTRL(i)(j).PATH_ENCODING  <= x"01010101"; -- Encoding for every EPATH, 8 EPATHS per EGROUP 0: direct mode, 1: 8b10b mode, 2: HDLC mode
                    register_map_40_control.ENCODING_EGROUP_CTRL(i)(j).EPATH_WIDTH    <= "001"; --4b elinks
                    register_map_40_control.ENCODING_EGROUP_CTRL(i)(j).EPATH_ENA      <= "01010101"; --4b elinks
                elsif ElinkWidth = 8 then -- @suppress "Dead code"
                    register_map_40_control.DECODING_EGROUP_CTRL(i)(j).REVERSE_ELINKS <= x"00"; -- enables bit reversing for the elink in the given epath
                    register_map_40_control.DECODING_EGROUP_CTRL(i)(j).PATH_ENCODING  <= x"00010001"; -- Encoding for every EPATH, 8 EPATHS per EGROUP 0: direct mode, 1: 8b10b mode, 2: HDLC mode
                    register_map_40_control.DECODING_EGROUP_CTRL(i)(j).EPATH_WIDTH    <= "010"; --8b elinks
                    register_map_40_control.DECODING_EGROUP_CTRL(i)(j).EPATH_ENA      <= "00010001"; --8b elinks
                    register_map_40_control.ENCODING_EGROUP_CTRL(i)(j).REVERSE_ELINKS <= x"00"; -- enables bit reversing for the elink in the given epath
                    register_map_40_control.ENCODING_EGROUP_CTRL(i)(j).PATH_ENCODING  <= x"00010001"; -- Encoding for every EPATH, 8 EPATHS per EGROUP 0: direct mode, 1: 8b10b mode, 2: HDLC mode
                    register_map_40_control.ENCODING_EGROUP_CTRL(i)(j).EPATH_WIDTH    <= "010"; --8b elinks
                    register_map_40_control.ENCODING_EGROUP_CTRL(i)(j).EPATH_ENA      <= "00010001"; --8b elinks
                else -- @suppress "Dead code"
                    tb_error("ElinkWidth must be 2, 4 or 8", C_SCOPE);
                end if;

            end loop;
        end loop;
        wait;
    end process;




    RXUSRCLK <= (others => clk240);

    reset_proc: process
    begin
        reset <= '1';
        wait for C_CLK40_PERIOD*15;
        reset <= '0';
        wait;
    end process;

    register_map_40_control.FE_EMU_ENA.EMU_TOHOST <= "1";
    register_map_40_control.FE_EMU_ENA.EMU_TOFRONTEND <= "1";
    register_map_40_control.FE_EMU_CONFIG.WE <= (others => '0');
    register_map_40_control.FE_EMU_CONFIG.WRADDR   <= (others => '0');
    register_map_40_control.FE_EMU_CONFIG.WRDATA   <= (others => '0');


    decoding0: entity work.decoding
        generic map(
            CARD_TYPE => 712,
            GBT_NUM => GBT_NUM/ENDPOINTS,
            FIRMWARE_MODE => FIRMWARE_MODE,
            STREAMS_TOHOST => STREAMS_TOHOST,
            BLOCKSIZE => BLOCKSIZE,
            LOCK_PERIOD => LOCK_PERIOD,
            IncludeDecodingEpath2_HDLC => IncludeDecodingEpath2_HDLC,
            IncludeDecodingEpath2_8b10b => IncludeDecodingEpath2_8b10b,
            IncludeDecodingEpath4_8b10b => IncludeDecodingEpath4_8b10b,
            IncludeDecodingEpath8_8b10b => IncludeDecodingEpath8_8b10b,
            IncludeDecodingEpath16_8b10b => IncludeDecodingEpath16_8b10b,
            IncludeDecodingEpath32_8b10b => IncludeDecodingEpath32_8b10b,
            IncludeDirectDecoding => "0000000",
            RD53Version => "A",
            PCIE_ENDPOINT => 0,
            VERSAL => false,
            AddFULLMODEForDUNE => false
        )
        Port map(
            RXUSRCLK => RXUSRCLK(((GBT_NUM/ENDPOINTS)*(PCIE_ENDPOINT+1))-1 downto (GBT_NUM/ENDPOINTS)*PCIE_ENDPOINT), --: in  std_logic_vector(GBT_NUM-1 downto 0);
            FULL_UPLINK_USER_DATA => GTH_FM_RX_33b_out((GBT_NUM/ENDPOINTS)*PCIE_ENDPOINT to((GBT_NUM/ENDPOINTS)*(PCIE_ENDPOINT+1))-1), --: in  txrx33b_type(0 to (GBT_NUM-1)); --Full mode data input
            GBT_UPLINK_USER_DATA => GBT_UPLINK_USER_DATA, -- : in  array_120b(GBT_NUM-1 downto 0);   --GBT data input
            lpGBT_UPLINK_USER_DATA => (others => (others => '0')), -- TODO: connect lpGBT data : in  txrx230b_type(GBT_NUM-1 downto 0); --lpGBT data input
            lpGBT_UPLINK_EC_DATA => (others => (others => '0')), -- TODO: connect lpGBT data : in  array_2b(GBT_NUM-1 downto 0);   --lpGBT EC data input
            lpGBT_UPLINK_IC_DATA => (others => (others => '0')), -- TODO: connect lpGBT data : in  array_2b(GBT_NUM-1 downto 0);   --lpGBT IC data input
            LinkAligned => LinkAligned, --: in  std_logic_vector(GBT_NUM-1 downto 0);
            clk160 => clk160, --: in  std_logic;
            clk240 => clk240,
            clk250 => clk250, --: in  std_logic;
            clk40 => clk40, --: in  std_logic;
            clk365 => '0',
            aclk_out => decoding_aclk, --: out std_logic;
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            m_axis => decoding_axis,
            m_axis_tready => decoding_axis_tready,
            m_axis_prog_empty => decoding_axis_prog_empty,
            m_axis_noSC => open,
            m_axis_noSC_tready => (others => (others => '1')),
            m_axis_noSC_prog_empty => open,
            TTC_ToHost_Data_in => TTC_ToHost_Data_in,
            TTCin => TTCin,
            FE_BUSY_out => open,
            ElinkBusyIn => ElinkBusyIn,
            DmaBusyIn => DmaBusyIn,
            FifoBusyIn => FifoBusyIn,
            BusySumIn => BusySumIn,
            m_axis_aux => decoding_axis_aux,
            m_axis_aux_prog_empty => decoding_axis_aux_prog_empty,
            m_axis_aux_tready => decoding_axis_aux_tready,
            register_map_control => register_map_40_control,
            register_map_decoding_monitor => register_map_decoding_monitor,
            Interlaken_RX_Data_In         => Interlaken_RX_Data(PCIE_ENDPOINT*(GBT_NUM/ENDPOINTS) to ((PCIE_ENDPOINT+1)*(GBT_NUM/ENDPOINTS))-1),
            Interlaken_RX_Datavalid       => Interlaken_RX_Datavalid(((PCIE_ENDPOINT+1)*(GBT_NUM/ENDPOINTS))-1 downto PCIE_ENDPOINT*(GBT_NUM/ENDPOINTS)),
            Interlaken_RX_Gearboxslip     => open,
            Interlaken_Decoder_Aligned_out => open,
            m_axis64                      => open,
            m_axis64_tready               => (others => '0'),
            m_axis64_prog_empty           => open,
            toHost_axis64_aclk_out        => open
        );
    --CR_TOHOST_GBT_MON     => register_map_cr_monitor.CR_TOHOST_GBT_MON      );
    resetmgr0: entity work.CRresetManager
        port map(
            clk40         => clk40,
            rst           => reset,
            clk40_stable  => clk40_stable,
            cr_rst        => daq_reset,
            cr_fifo_flush => daq_fifo_flush
        );

    g_URAM: if USE_URAM generate
        g_axis32: if LINK_CONFIG(0) = 0 generate
            toHostFifo_wr_clk <= decoding_aclk;
        else generate
            toHostFifo_wr_clk <= clk250;
        end generate;
    else generate --BRAM FIFO's in Virtex7 and Kintex Ultrascale
        g_toHostFifo_wr_clk_160: if FIRMWARE_MODE = FIRMWARE_MODE_GBT or FIRMWARE_MODE = FIRMWARE_MODE_LTDB or FIRMWARE_MODE = FIRMWARE_MODE_LPGBT generate
            toHostFifo_wr_clk <= clk160;
        else generate
            toHostFifo_wr_clk <= clk250;
        end generate;
    end generate;


    crt0: entity work.CRToHost
        generic map(
            NUMBER_OF_DESCRIPTORS => 2,
            NUMBER_OF_INTERRUPTS => 8,
            LINK_NUM => GBT_NUM/ENDPOINTS,
            LINK_CONFIG => (0 to GBT_NUM-1 => 0),
            toHostTimeoutBitn => 16,
            STREAMS_TOHOST => STREAMS_TOHOST,
            BLOCKSIZE => BLOCKSIZE,
            DATA_WIDTH => DATA_WIDTH,
            FIRMWARE_MODE => FIRMWARE_MODE,
            USE_URAM => USE_URAM)
        port map(
            clk40 => clk40,
            aclk_tohost => decoding_aclk,
            aclk64_tohost => '0',
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            register_map_control => register_map_40_control,
            register_map_xoff_monitor => register_map_xoff_monitor,
            register_map_crtohost_monitor => register_map_crtohost_monitor,
            interrupt_call => open,
            xoff_out => open,
            s_axis => decoding_axis,
            s_axis_tready => decoding_axis_tready,
            s_axis_prog_empty => decoding_axis_prog_empty,
            s_axis_aux => decoding_axis_aux,
            s_axis_aux_tready => decoding_axis_aux_tready,
            s_axis_aux_prog_empty => decoding_axis_aux_prog_empty,
            s_axis64 => (others => axis_64_zero_c),
            s_axis64_tready => open,
            s_axis64_prog_empty => (others => '1'),
            dma_enable_in => (others => '1'),
            toHostFifo_din => toHostFifo_din,
            toHostFifo_wr_en => toHostFifo_wr_en,
            toHostFifo_prog_full => toHostFifo_prog_full,
            toHostFifo_wr_clk => toHostFifo_wr_clk,
            toHostFifo_rst => open);

    -- fhAxis_aclk <= clk250;

    crfh0: entity work.CRFromHostAxis
        generic map(
            LINK_NUM => GBT_NUM,
            LINK_CONFIG => LINK_CONFIG,
            STREAMS_PER_LINK_FROMHOST => STREAMS_FROMHOST,
            DATA_WIDTH => DATA_WIDTH
        )
        port map(
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            fromHostFifo_clk => clk250,
            fromHostFifo_dout => fromHostFifo_dout,
            fromHostFifo_rd_en => fromHostFifo_rd_en,
            fromHostFifo_empty => fromHostFifo_empty,
            fromHostFifo_rst => open,
            -- fhAxis_aclk => fhAxis_aclk,
            fhAxis => fhAxis,
            fhAxis_tready => fhAxis_tready,
            -- fhAxis64_aclk => '0',
            fhAxis64 => fhAxis64,
            fhAxis64_tready => fhAxis64_tready,
            fifo_monitoring => fifo_monitoring,
            register_map_control => register_map_40_control
        );

    encoding0: entity work.encoding
        generic map(
            GBT_NUM => GBT_NUM,
            FIRMWARE_MODE => FIRMWARE_MODE,
            --BLOCKSIZE => BLOCKSIZE,
            STREAMS_FROMHOST => STREAMS_FROMHOST,
            IncludeEncodingEpath2_HDLC => "11111",
            IncludeEncodingEpath2_8b10b => "11111",
            IncludeEncodingEpath4_8b10b => "11111",
            IncludeEncodingEpath8_8b10b => "11111",
            INCLUDE_DIRECT => "11111",
            INCLUDE_TTC => "11111",
            INCLUDE_RD53 => "00000",
            DEBUGGING_RD53 => false,
            RD53Version => "A",
            DISTR_RAM => false,
            SUPPORT_HDLC_DELAY => false,
            USE_ULTRARAM_LCB => false,
            INCLUDE_XOFF => false
        )
        port map(
            clk40 => clk40,
            aclk => clk250, --fhAxis_aclk,
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            s_axis => fhAxis,
            s_axis_tready => fhAxis_tready,
            register_map_control => register_map_40_control,
            register_map_encoding_monitor => register_map_encoding_monitor,
            GBT_DOWNLINK_USER_DATA => GBT_UPLINK_USER_DATA,
            lpGBT_DOWNLINK_USER_DATA => lpGBT_UPWNLINK_USER_DATA,
            lpGBT_DOWNLINK_IC_DATA => lpGBT_DOWNLINK_IC_DATA,
            lpGBT_DOWNLINK_EC_DATA => lpGBT_DOWNLINK_EC_DATA,
            TTCin => TTCin,
            toHostXoff => (others => '0')
        );

    clock_generator(clk40, clock_ena, C_CLK40_PERIOD, "40 MHz clock");
    clock_generator(clk160, clock_ena, C_CLK160_PERIOD, "160 MHz clock");
    clock_generator(clk240, clock_ena, C_CLK240_PERIOD, "240 MHz clock");
    clock_generator(clk250, clock_ena, C_CLK250_PERIOD, "250 MHz clock");


    sequencer : process
    begin
        clk40_stable <= '0';
        -- Print the configuration to the log
        --wait for 1 ns;
        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);

        --enable_log_msg(ALL_MESSAGES);
        disable_log_msg(ALL_MESSAGES, NON_QUIET, C_SCOPE);
        enable_log_msg(ID_SEQUENCER, NON_QUIET, C_SCOPE);
        disable_log_msg(ID_POS_ACK, NON_QUIET, C_SCOPE);


        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "----- initializing test bench       ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        -- Wait for UVVM to finish initialization
        --await_uvvm_initialization(VOID);

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "-----    end of initialisation        ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        wait for (10*C_CLK40_PERIOD);
        clock_ena <= true;
        clk40_stable <= '1';

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "-- test bench sequence starts here      ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);


        log(ID_LOG_HDR, "Starting simulation of TB for EGROUP using VVCs", C_SCOPE);
        ------------------------------------------------------------

        await_value(checker_done, '1', 0 ns, 1000 ms, TB_ERROR, "wait for all tests to finish", C_SCOPE);
        enable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        enable_log_msg(ID_SEQUENCER);
        enable_log_msg(ID_UVVM_SEND_CMD);

        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely
    --
    end process; -- sequencer

    g_aligned: for i in 0 to GBT_NUM-1 generate
        DECODING_LINK_ALIGNED(i*40+39 downto i*40) <= register_map_decoding_monitor.DECODING_LINK_ALIGNED(i)(39 downto 0);
    end generate;


    fupload_proc: process
        variable expected_alignment_value: std_logic_vector(GBT_NUM*40-1 downto 0);
    begin
        for i in 0 to GBT_NUM-1 loop
            if ElinkWidth = 2 then
                expected_alignment_value(i*40+39 downto i*40) := x"FFFFFFFFFF"; -- @suppress "Dead code"
            elsif ElinkWidth = 4 then
                expected_alignment_value(i*40+39 downto i*40) := x"5555555555";
            elsif ElinkWidth = 8 then -- @suppress "Dead code"
                expected_alignment_value(i*40+39 downto i*40) := x"1111111111";
            else -- @suppress "Dead code"
                log(ID_LOG_HDR, "Error: Wrong ElinkWidth supplied", C_SCOPE);
            end if;
        end loop;
        sourceStart <= '0';
        await_value(DECODING_LINK_ALIGNED, expected_alignment_value , 0 ns, 100 us, TB_ERROR, "wait for all E-links to align", C_SCOPE);
        log(ID_LOG_HDR, "Starting fupload", C_SCOPE);
        loop
            for epath in 0 to (8/(ElinkWidth/2))-1 loop
                for egroup in 0 to 4 loop
                    for link in 0 to GBT_NUM-1 loop
                        GBTId <= link;
                        StreamID <= epath*(ElinkWidth/2)+egroup*8;
                        sourceStart <= '1';
                        wait until rising_edge(clk250);
                        sourceStart <= '0';
                        wait until sourceDone = '1';
                        wait until rising_edge(clk250);
                    end loop;
                end loop;
            end loop;
        end loop;
    --log(ID_LOG_HDR, "Fupload complete", C_SCOPE);
    --wait;
    end process;





    source0: entity work.FELIXDataSource
        generic map(
            DATA_WIDTH => DATA_WIDTH,
            ThrottleData => false
        )
        port map(
            clk                => clk250,
            NumberOfChunks     => NumberOfChunks,
            ChunkLength        => ChunkLength,
            GBTId              => GBTId,
            StreamID           => StreamID,
            ElinkWidth         => ElinkWidth,
            fromHostFifo_dout  => fromHostFifo_dout,
            fromHostFifo_empty => fromHostFifo_empty,
            fromHostFifo_rd_en => fromHostFifo_rd_en,
            start              => sourceStart,
            done               => sourceDone
        );



    sink0: entity work.FELIXDataSink
        generic map(
            NUMBER_OF_DESCRIPTORS           => NUMBER_OF_DESCRIPTORS,
            BLOCKSIZE                       => BLOCKSIZE,
            GBT_NUM                         => GBT_NUM,
            FIRMWARE_MODE                   => FIRMWARE_MODE,
            DATA_WIDTH                      => DATA_WIDTH,
            SIM_NUMBER_OF_BLOCKS            => SIM_NUMBER_OF_BLOCKS,
            APPLY_BACKPRESSURE_AFTER_BLOCKS => SIM_NUMBER_OF_BLOCKS
        )
        port map(
            checker_done         => checker_done,
            toHostFifo_din       => toHostFifo_din,
            toHostFifo_wr_en     => toHostFifo_wr_en,
            toHostFifo_prog_full => toHostFifo_prog_full,
            toHostFifo_wr_clk    => toHostFifo_wr_clk,
            reset                => reset,
            start_check          => start_check,
            Trunc                => Trunc,
            TrailerError         => TrailerError,
            CRCError             => CRCError,
            ChunkLengthError     => ChunkLengthError,
            register_map_control => register_map_40_control
        );

end architecture;


--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use IEEE.MATH_REAL.ALL;

library uvvm_util;
    context uvvm_util.uvvm_util_context;
library bitvis_vip_axistream;
    use bitvis_vip_axistream.axistream_bfm_pkg.all;
    use bitvis_vip_axistream.vvc_methods_pkg.all;
    context bitvis_vip_axistream.vvc_context;
library bitvis_vip_scoreboard; -- @suppress "Library 'bitvis_vip_scoreboard' is not available"
library uvvm_vvc_framework;
    use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;


    use work.axi_stream_package.all;
    use work.FELIX_package.all;

entity BusyVirtualElink_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end BusyVirtualElink_tb;

architecture tb of BusyVirtualElink_tb is
    -- configuration
    constant C_SCOPE : string                       := "CRFromHostAxis_tb";
    constant C_CLK160_PERIOD : time                 := 6.25 ns;
    constant C_CLK40_PERIOD : time                 := 25 ns;

    -- clocks
    signal clk40 : std_logic;
    signal clk_ena : boolean := false;

    -- checker value
    signal checker_done : boolean := false;

    signal reset : std_logic;
    signal m_axis_aclk : std_logic;
    signal m_axis : axis_32_type;
    signal m_axis_prog_empty : std_logic; -- @suppress "signal m_axis_prog_empty is never read"
    signal m_axis_tready : std_logic;

    -- some test data
    constant TESTDATA0 : t_slv_array := (x"ABC10B11",x"00000001");
    constant TESTDATA1 : t_slv_array := (x"DEFF8105",x"00000002");
    constant TESTDATA2 : t_slv_array := (x"123F8303",x"00000003");
    constant TESTDATA3 : t_slv_array := (x"456F8509",x"00000004");
    constant TESTDATA4 : t_slv_array := (x"789F8400",x"00000005");
    --constant USER_ARRAY  : t_user_array := ("00000000", "00000000");

    signal axistream_vvc_if : t_axistream_if(tdata(31 downto 0), tkeep(3 downto 0), tuser(3 downto 0), tstrb(3 downto 0), tid(0 downto 0), tdest(0 downto 0)) := init_axistream_if_signals(false, 32, 4, 1, 1);

    constant GBT_NUM : integer := 24;
    constant BLOCKSIZE : integer := 1024;
    signal Orbit : std_logic_vector(31 downto 0);
    signal BCID : std_logic_vector(11 downto 0);
    signal ElinkBusyIn : array_57b(0 to GBT_NUM-1);
    signal SoftBusyIn : std_logic;
    signal DmaBusyIn : std_logic;
    signal FifoBusyIn : std_logic;
    signal BusySumIn : std_logic;
    signal Enable : std_logic;
    constant GC_AXISTREAM_BFM_CONFIG : t_axistream_bfm_config := C_AXISTREAM_BFM_CONFIG_DEFAULT;
    signal clk40_stable : std_logic;
    signal daq_reset : std_logic;
    signal daq_fifo_flush : std_logic;

begin

    -- Instantiate the concurrent procedure that initializes UVVM
    i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

    resetmgr0: entity work.CRresetManager
        port map(
            clk40         => clk40,
            rst           => reset,
            clk40_stable  => clk40_stable,
            cr_rst        => daq_reset,
            cr_fifo_flush => daq_fifo_flush
        );


    --DUT:
    busy0: entity work.BusyVirtualElink     generic map(
            GBT_NUM => GBT_NUM,
            BLOCKSIZE => BLOCKSIZE
        )
        port map(
            clk => clk40,
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            Orbit => Orbit,
            BCID => BCID,
            ElinkBusyIn => ElinkBusyIn,
            SoftBusyIn => SoftBusyIn,
            DmaBusyIn => DmaBusyIn,
            FifoBusyIn => FifoBusyIn,
            BusySumIn => BusySumIn,
            Enable => Enable,
            m_axis => m_axis,
            m_axis_prog_empty => m_axis_prog_empty,
            m_axis_tready => m_axis_tready,
            m_axis_aclk => m_axis_aclk
        );

    -- clock generator
    clock_generator(m_axis_aclk, clk_ena, C_CLK160_PERIOD, "160 MHz clock");
    clock_generator(clk40, clk_ena, C_CLK40_PERIOD, "40 MHz clock");



    axi_stream_vvc_I: entity bitvis_vip_axistream.axistream_vvc
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => false,
            GC_DATA_WIDTH => 32,
            GC_USER_WIDTH => 4,
            GC_ID_WIDTH => 1,
            GC_DEST_WIDTH => 1,
            GC_INSTANCE_IDX => 0,
            GC_AXISTREAM_BFM_CONFIG => GC_AXISTREAM_BFM_CONFIG
        )
        port map (
            clk => m_axis_aclk,
            axistream_vvc_if => axistream_vvc_if
        );

    axistream_vvc_if.tkeep <= m_axis.tkeep;
    axistream_vvc_if.tuser <= m_axis.tuser;
    axistream_vvc_if.tdata <= m_axis.tdata;
    axistream_vvc_if.tvalid <= m_axis.tvalid;
    axistream_vvc_if.tlast <= m_axis.tlast;
    --axistream_vvc_if.tstrb <= "0000";
    --axistream_vvc_if.tid <= "0";
    --axistream_vvc_if.tdest <= "0";
    m_axis_tready <= axistream_vvc_if.tready;

    --We are using index 2,5 later in the sequencer
    process(clk40)
    begin
        if rising_edge(clk40) then
            BusySumIn <= SoftBusyIn or FifoBusyIn or DmaBusyIn or ElinkBusyIn(2)(5);
        end if;
    end process;

    sequencer : process
    begin
        clk40_stable <= '0';
        shared_axistream_vvc_config(0).bfm_config.max_wait_cycles := 10000;
        shared_axistream_vvc_config(0).bfm_config.max_wait_cycles_severity := ERROR;
        shared_axistream_vvc_config(0).bfm_config.clock_period := C_CLK160_PERIOD;
        shared_axistream_vvc_config(0).bfm_config.clock_period_margin := 0 ns;
        shared_axistream_vvc_config(0).bfm_config.clock_margin_severity := WARNING;
        shared_axistream_vvc_config(0).bfm_config.setup_time := C_CLK160_PERIOD/4;
        shared_axistream_vvc_config(0).bfm_config.hold_time := C_CLK160_PERIOD/4;
        shared_axistream_vvc_config(0).bfm_config.bfm_sync := SYNC_WITH_SETUP_AND_HOLD;
        shared_axistream_vvc_config(0).bfm_config.match_strictness := MATCH_EXACT;
        shared_axistream_vvc_config(0).bfm_config.byte_endianness := FIRST_BYTE_LEFT;
        shared_axistream_vvc_config(0).bfm_config.check_packet_length := true;
        shared_axistream_vvc_config(0).bfm_config.protocol_error_severity := ERROR;

        clk_ena <= true;
        clk40_stable <= '1';
        reset <= '1';
        wait for C_CLK40_PERIOD * 10;
        reset <= '0';

        Enable <= '0';
        Orbit <= x"0000_0000";
        BCID  <= x"000";
        DmaBusyIn <= '0';
        FifoBusyIn <= '0';
        SoftBusyIn <= '0';
        ElinkBusyIn <= (others => (others => '0'));

        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);

        enable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_BITVIS_DEBUG);
        --disable_log_msg(ALL_MESSAGES, NON_QUIET, C_SCOPE);
        --enable_log_msg(ID_SEQUENCER, NON_QUIET, C_SCOPE);
        --disable_log_msg(ID_POS_ACK, NON_QUIET, C_SCOPE);

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "----- initializing test bench       ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        -- Wait for UVVM to finish initialization
        await_uvvm_initialization(VOID);

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "-----    end of initialisation        ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        wait until daq_reset = '0';
        wait for C_CLK40_PERIOD * 5;
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "-- test bench sequence starts here      ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        Enable <= '1';
        wait for C_CLK40_PERIOD;
        Orbit <= x"0000_0001";
        BCID  <= x"ABC";
        ElinkBusyIn <= (others => (others => '0'));
        ElinkBusyIn(2)(5) <= '1';
        wait for C_CLK40_PERIOD;
        Orbit <= x"0000_0002";
        BCID  <= x"DEF";

        ElinkBusyIn <= (others => (others => '0'));
        DmaBusyIn <= '1';
        wait for C_CLK40_PERIOD;
        Orbit <= x"0000_0003";
        BCID  <= x"123";

        DmaBusyIn <= '0';
        FifoBusyIn <= '1';
        wait for C_CLK40_PERIOD;
        Orbit <= x"0000_0004";
        BCID  <= x"456";
        FifoBusyIn <= '0';
        SoftBusyIn <= '1';
        wait for C_CLK40_PERIOD;
        Orbit <= x"0000_0005";
        BCID  <= x"789";
        SoftBusyIn <= '0';



        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);

        log(ID_LOG_HDR, "Starting simulation of TB for CRFromHostAxis using VVCs", C_SCOPE);


        await_value(checker_done, true, 0 ns, 1000 us, TB_ERROR, "wait for all tests to finish", C_SCOPE);
        enable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        enable_log_msg(ID_SEQUENCER);
        enable_log_msg(ID_UVVM_SEND_CMD);

        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely
    end process;

    checker : process
    begin
        checker_done <= false;

        -- Wait for UVVM to finish initialization
        await_uvvm_initialization(VOID);

        wait until daq_reset = '0';
        wait for 0.675 us;
        axistream_expect(AXISTREAM_VVCT, 0, TESTDATA0,  "expecting axi stream data"); --@suppress
        axistream_expect(AXISTREAM_VVCT, 0, TESTDATA1, "expecting axi stream data");--@suppress
        axistream_expect(AXISTREAM_VVCT, 0, TESTDATA2, "expecting axi stream data");--@suppress
        axistream_expect(AXISTREAM_VVCT, 0, TESTDATA3, "expecting axi stream data");--@suppress
        axistream_expect(AXISTREAM_VVCT, 0, TESTDATA4, "expecting axi stream data");--@suppress

        await_completion(AXISTREAM_VVCT, 0, 1200 * C_CLK160_PERIOD);--@suppress
        --wait for C_CLK40_PERIOD * 1200;
        checker_done <= true;
        wait;
    end process;


end architecture;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2023 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

library XPM;
    use XPM.VCOMPONENTS.ALL;

library uvvm_util;
    context uvvm_util.uvvm_util_context;
library bitvis_vip_axistream;
    use bitvis_vip_axistream.axistream_bfm_pkg.all;
    use bitvis_vip_axistream.vvc_methods_pkg.all;
    context bitvis_vip_axistream.vvc_context;
library uvvm_vvc_framework;
    use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

    use work.axi_stream_package.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;

entity CRFromHost_tb is
    generic(
        use_vunit: boolean := false;
        DATA_WIDTH : integer := 256
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end CRFromHost_tb;

architecture tb of CRFromHost_tb is
    -- configuration
    constant C_SCOPE : string                         := "CRFromHost_tb";
    constant C_CLK_PERIOD : time                      := 4 ns;
    constant LINK_NUM : integer                       := 2;
    constant LINK_CONFIG : IntArray(0 to LINK_NUM-1)  := (0, 1);                    -- implement one GBT and one 25G link
    constant STREAMS_PER_LINK_FROMHOST : integer      := 8;

    constant C_AXISTREAM_BFM_CONFIG : t_axistream_bfm_config := (
                                                                  max_wait_cycles                => 1000,
                                                                  max_wait_cycles_severity       => ERROR,
                                                                  clock_period                   => -1 ns,
                                                                  clock_period_margin            => 0 ns,
                                                                  clock_margin_severity          => TB_ERROR,
                                                                  setup_time                     => -1 ns,
                                                                  hold_time                      => -1 ns,
                                                                  bfm_sync                       => SYNC_ON_CLOCK_ONLY,
                                                                  match_strictness               => MATCH_EXACT,
                                                                  byte_endianness                => LOWER_BYTE_LEFT,
                                                                  valid_low_at_word_num          => 0,
                                                                  valid_low_multiple_random_prob => 0.5,
                                                                  valid_low_duration             => 0,
                                                                  valid_low_max_random_duration  => 5,
                                                                  check_packet_length            => false,
                                                                  protocol_error_severity        => ERROR,
                                                                  ready_low_at_word_num          => 0,
                                                                  ready_low_multiple_random_prob => 0.5,
                                                                  ready_low_duration             => 0,
                                                                  ready_low_max_random_duration  => 5,
                                                                  ready_default_value            => '1',
                                                                  id_for_bfm                     => ID_BFM
                                                                );


    signal reset : std_logic;
    signal daq_reset : std_logic;
    signal daq_fifo_flush : std_logic;
    signal clk : std_logic;
    signal clk_stable : std_logic;
    signal fromHostFifo_dout : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal fromHostFifo_din : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal fromHostFifo_rd_en : std_logic;
    signal fromHostFifo_wr_en : std_logic;
    signal fromHostFifo_empty : std_logic;
    signal fromHostFifo_full : std_logic;
    signal fromHostFifo_rst : std_logic;
    signal fhAxis : axis_8_2d_array_type(0 to LINK_NUM-1, 0 to STREAMS_PER_LINK_FROMHOST-1);
    signal fhAxis_tready : axis_tready_2d_array_type(0 to LINK_NUM-1, 0 to STREAMS_PER_LINK_FROMHOST-1);
    signal fhAxis64 : axis_64_array_type(0 to LINK_NUM-1);
    signal fhAxis64_tready : axis_tready_array_type(0 to LINK_NUM-1);

    signal register_map_control : register_map_control_type;
    signal fifo_monitoring : bitfield_crfromhost_fifo_status_r_type; -- @suppress "signal fifo_monitoring is never read"

    signal clk_ena : boolean := false;

    constant AXIS8_INSTANCE : integer := 0;
    constant AXIS64_INSTANCE : integer := 1;
    signal axistream8_vvc_if : t_axistream_if(tdata(7 downto 0), tkeep(0 downto 0), tuser(0 downto 0), tstrb(0 downto 0), tid(0 downto 0), tdest(0 downto 0)) := init_axistream_if_signals(true, 8, 1, 1, 1, C_AXISTREAM_BFM_CONFIG);
    signal axistream64_vvc_if : t_axistream_if(tdata(63 downto 0), tkeep(7 downto 0), tuser(3 downto 0), tstrb(7 downto 0), tid(7 downto 0), tdest(0 downto 0)) := init_axistream_if_signals(true, 64, 4, 8, 1, C_AXISTREAM_BFM_CONFIG);
    signal active_link : integer range 0 to LINK_NUM-1 := 0;
    signal active_stream : integer range 0 to STREAMS_PER_LINK_FROMHOST-1 := 0;
begin

    -- Instantiate the concurrent procedure that initializes UVVM
    i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

    -- clock generators
    clock_generator(clk, clk_ena, C_CLK_PERIOD, "clock");

    resetmgr0: entity work.CRresetManager
        port map(
            clk40         => clk,
            rst           => reset,
            clk40_stable  => clk_stable,
            cr_rst        => daq_reset,
            cr_fifo_flush => daq_fifo_flush
        );

    -- device under test
    dut: entity work.CRFromHostAxis
        generic map (
            LINK_NUM => LINK_NUM,
            LINK_CONFIG => LINK_CONFIG,
            STREAMS_PER_LINK_FROMHOST => STREAMS_PER_LINK_FROMHOST,
            DATA_WIDTH => DATA_WIDTH
        )
        port map (
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            fromHostFifo_clk => clk,
            fromHostFifo_dout => fromHostFifo_dout,
            fromHostFifo_rd_en => fromHostFifo_rd_en,
            fromHostFifo_empty => fromHostFifo_empty,
            fromHostFifo_rst => fromHostFifo_rst,
            fhAxis => fhAxis,
            fhAxis_tready => fhAxis_tready,
            fhAxis64 => fhAxis64,
            fhAxis64_tready => fhAxis64_tready,
            fifo_monitoring => fifo_monitoring,
            register_map_control => register_map_control
        );

    -- input FIFO
    fifo: xpm_fifo_sync
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT, SIM_ASSERT_CHK"
            DOUT_RESET_VALUE => "0",
            ECC_MODE => "no_ecc",
            FIFO_MEMORY_TYPE => "block",
            FIFO_READ_LATENCY => 1,
            FIFO_WRITE_DEPTH => 64,
            FULL_RESET_VALUE => 0,
            PROG_EMPTY_THRESH => 10,
            PROG_FULL_THRESH => 10,
            RD_DATA_COUNT_WIDTH => 1,
            READ_DATA_WIDTH => DATA_WIDTH,
            READ_MODE => "fwft",
            USE_ADV_FEATURES => "0000",
            WAKEUP_TIME => 0,
            WRITE_DATA_WIDTH => DATA_WIDTH,
            WR_DATA_COUNT_WIDTH => 1
        )
        port map (
            sleep => '0',
            rst => fromHostFifo_rst,
            wr_clk => clk,
            wr_en => fromHostFifo_wr_en,
            din => fromHostFifo_din,
            full => fromHostFifo_full,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => open,
            almost_full => open,
            wr_ack => open,
            rd_en => fromHostFifo_rd_en,
            dout => fromHostFifo_dout,
            empty => fromHostFifo_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => open,
            almost_empty => open,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );

    axistream8_vvc_I: entity bitvis_vip_axistream.axistream_vvc
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_AXISTREAM_BFM_CONFIG, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY" -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => false,
            GC_DATA_WIDTH => 8,
            GC_USER_WIDTH => 1,
            GC_ID_WIDTH => 1,
            GC_DEST_WIDTH => 1,
            GC_INSTANCE_IDX => AXIS8_INSTANCE,
            GC_AXISTREAM_BFM_CONFIG => C_AXISTREAM_BFM_CONFIG
        )
        port map (
            clk => clk,
            axistream_vvc_if => axistream8_vvc_if
        );

    axistream64_vvc_I: entity bitvis_vip_axistream.axistream_vvc
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_AXISTREAM_BFM_CONFIG, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY" -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => false,
            GC_DATA_WIDTH => 64,
            GC_USER_WIDTH => 4,
            GC_ID_WIDTH => 8,
            GC_DEST_WIDTH => 1,
            GC_INSTANCE_IDX => AXIS64_INSTANCE,
            GC_AXISTREAM_BFM_CONFIG => C_AXISTREAM_BFM_CONFIG
        )
        port map (
            clk => clk,
            axistream_vvc_if => axistream64_vvc_if
        );

    -- multiplex streams
    axismux: process (active_link, active_stream, fhAxis, fhAxis64, axistream8_vvc_if, axistream64_vvc_if) begin
        for l in 0 to LINK_NUM-1 loop
            for s in 0 to STREAMS_PER_LINK_FROMHOST-1 loop
                fhAxis_tready(l, s) <= '1';
            end loop;
            fhAxis64_tready(l) <= '1';
        end loop;

        axistream8_vvc_if.tkeep <= "1";
        axistream8_vvc_if.tdata <= fhAxis(active_link, active_stream).tdata;
        axistream8_vvc_if.tvalid <= fhAxis(active_link, active_stream).tvalid;
        axistream8_vvc_if.tlast <= fhAxis(active_link, active_stream).tlast;
        fhAxis_tready(active_link, active_stream) <= axistream8_vvc_if.tready;

        axistream64_vvc_if.tkeep <= fhAxis64(active_link).tkeep;
        axistream64_vvc_if.tdata <= fhAxis64(active_link).tdata;
        axistream64_vvc_if.tvalid <= fhAxis64(active_link).tvalid;
        axistream64_vvc_if.tlast <= fhAxis64(active_link).tlast;
        axistream64_vvc_if.tid <= fhAxis64(active_link).tid;
        axistream64_vvc_if.tuser <= fhAxis64(active_link).tuser;
        fhAxis64_tready(active_link) <= axistream64_vvc_if.tready;
    end process;

    -- stimulus
    stim: process
        procedure test_delay(constant delay : positive) is
            variable num_blocks : positive;
            variable last_block : boolean; -- @suppress "variable last_block is never read"
            variable block_length : natural;
        begin
            -- use first link and first stream
            active_link <= 0;
            active_stream <= 0;
            wait for (10*C_CLK_PERIOD);

            -- log message
            log(ID_SEQUENCER, "preparing delay packet with delay=" & integer'image(delay) & ", link=0, stream=0", C_SCOPE);

            -- prepare expected data (delay x 0xFF-packets)
            for i in 0 to delay-1 loop
                axistream_expect(AXISTREAM_VVCT, AXIS8_INSTANCE, (x"FF"), "expecting axi8 stream data"); --@suppress
            end loop;

            -- calculate number of blocks
            num_blocks := (delay + (DATA_WIDTH-32)/8 - 1) / ((DATA_WIDTH-32)/8);
            log(ID_SEQUENCER, "sending out " & integer'image(num_blocks) & " blocks", C_SCOPE);
            for i in 0 to num_blocks-1 loop
                wait until rising_edge(clk);
                log(ID_SEQUENCER, "sending out block #" & integer'image(i), C_SCOPE);

                -- calculate block length
                if i = num_blocks-1 then            -- this is the last block
                    block_length := delay mod ((DATA_WIDTH-32)/8);
                    if block_length = 0 then
                        block_length := (DATA_WIDTH-32)/8;
                    end if;
                    last_block := true;
                else                                -- more blocks will follow
                    block_length := (DATA_WIDTH-32)/8;
                    last_block := false;
                end if;

                -- prepare block header and payload
                fromHostFifo_din(DATA_WIDTH-32+31 downto DATA_WIDTH-32+24) <= (others => '0');        -- reserved
                fromHostFifo_din(DATA_WIDTH-32+23 downto DATA_WIDTH-32+16) <= "00000000";
                fromHostFifo_din(DATA_WIDTH-32+15 downto DATA_WIDTH-32+8) <= "00000000";
                fromHostFifo_din(DATA_WIDTH-32+7 downto DATA_WIDTH-32) <= "00000000";
                for j in 0 to ((DATA_WIDTH-32)/8)-1 loop
                    if j < block_length then
                        fromHostFifo_din(DATA_WIDTH-32-8*j-1 downto DATA_WIDTH-32-8*j-8) <= x"FF";
                    else
                        fromHostFifo_din(DATA_WIDTH-32-8*j-1 downto DATA_WIDTH-32-8*j-8) <= (others => '0');
                    end if;
                end loop;

                -- wait for data to be acceptable
                while fromHostFifo_full = '1' loop
                    fromHostFifo_wr_en <= '0';
                    wait until rising_edge(clk);
                end loop;

                -- transmit data
                fromHostFifo_wr_en <= '1';
            end loop;

            -- let last transmission finish
            wait until rising_edge(clk);
            fromHostFifo_wr_en <= '0';

            -- wait for AXI to finish
            await_completion(AXISTREAM_VVCT, AXIS8_INSTANCE, 1200 * C_CLK_PERIOD);--@suppress
        end procedure;

        procedure transmit_and_expect(constant link : natural; constant stream : natural; constant pktlength : positive) is
            variable pktdata : t_slv_array(0 to pktlength-1)(7 downto 0);
            variable num_blocks : positive;
            variable last_block : boolean;
            variable block_length : natural;
        begin
            -- select link/stream
            if link > LINK_NUM-1 then
                active_link <= 0;
            else
                active_link <= link;
            end if;
            if stream > STREAMS_PER_LINK_FROMHOST-1 then
                active_stream <= 0;
            else
                active_stream <= stream;
            end if;
            wait for (10*C_CLK_PERIOD);

            -- prepare packet payload
            log(ID_SEQUENCER, "preparing packet with pktlength=" & integer'image(pktlength) & ", link=" & integer'image(link) & ", stream=" & integer'image(stream), C_SCOPE);
            for i in 0 to pktlength-1 loop
                pktdata(i) := std_logic_vector(to_unsigned(i mod 256, 8));
            end loop;

            -- prepare the expect
            if LINK_CONFIG(link) = 1 then
                axistream_expect(AXISTREAM_VVCT, AXIS64_INSTANCE, pktdata, "expecting axi64 stream data"); --@suppress
            else
                axistream_expect(AXISTREAM_VVCT, AXIS8_INSTANCE, pktdata, "expecting axi8 stream data");--@suppress
            end if;

            -- send out into FIFO in blocks
            num_blocks := (pktlength + (DATA_WIDTH-32)/8 - 1) / ((DATA_WIDTH-32)/8);
            log(ID_SEQUENCER, "sending out " & integer'image(num_blocks) & " blocks", C_SCOPE);
            for i in 0 to num_blocks-1 loop
                wait until rising_edge(clk);
                log(ID_SEQUENCER, "sending out block #" & integer'image(i), C_SCOPE);

                -- calculate block length
                if i = num_blocks-1 then            -- this is the last block
                    block_length := pktlength mod ((DATA_WIDTH-32)/8);
                    if block_length = 0 then
                        block_length := (DATA_WIDTH-32)/8;
                    end if;
                    last_block := true;
                else                                -- more blocks will follow
                    block_length := (DATA_WIDTH-32)/8;
                    last_block := false;
                end if;

                -- prepare block header
                fromHostFifo_din(DATA_WIDTH-32+31 downto DATA_WIDTH-32+24) <= (others => '0');        -- reserved
                fromHostFifo_din(DATA_WIDTH-32+23 downto DATA_WIDTH-32+16) <= std_logic_vector(to_signed(link, 8));
                fromHostFifo_din(DATA_WIDTH-32+15 downto DATA_WIDTH-32+8) <= std_logic_vector(to_signed(stream, 8));
                if last_block then
                    -- this will be the end-of-message
                    fromHostFifo_din(DATA_WIDTH-32+7 downto DATA_WIDTH-32) <= std_logic_vector(to_unsigned(block_length, 8));
                else
                    -- no end-of-message
                    fromHostFifo_din(DATA_WIDTH-32+7 downto DATA_WIDTH-32) <= "11111111";
                end if;

                -- prepare packet payload
                for j in 0 to ((DATA_WIDTH-32)/8)-1 loop
                    if j < block_length then
                        fromHostFifo_din(DATA_WIDTH-32-8*j-1 downto DATA_WIDTH-32-8*j-8) <= pktdata((DATA_WIDTH-32)/8*i+j);
                    else
                        fromHostFifo_din(DATA_WIDTH-32-8*j-1 downto DATA_WIDTH-32-8*j-8) <= (others => '0');
                    end if;
                end loop;

                -- wait for data to be acceptable
                while fromHostFifo_full = '1' loop
                    fromHostFifo_wr_en <= '0';
                    wait until rising_edge(clk);
                end loop;

                -- transmit data
                fromHostFifo_wr_en <= '1';
            end loop;

            -- let last transmission finish
            wait until rising_edge(clk);
            fromHostFifo_wr_en <= '0';

            -- wait for AXI to finish
            if LINK_CONFIG(link) = 1 then
                await_completion(AXISTREAM_VVCT, AXIS64_INSTANCE, 1200 * C_CLK_PERIOD); --@suppress
            else
                await_completion(AXISTREAM_VVCT, AXIS8_INSTANCE, 1200 * C_CLK_PERIOD); --@suppress
            end if;
        end procedure;
    begin
        -- initialize
        uvvm_completed <= '0';
        clk_ena <= false;
        clk_stable <= '0';
        fromHostFifo_din <= (others => '0');
        fromHostFifo_wr_en <= '0';
        reset <= '1';
        register_map_control.BROADCAST_ENABLE <= (others => (others => '0'));

        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);

        --enable_log_msg(ALL_MESSAGES);
        disable_log_msg(ALL_MESSAGES, NON_QUIET, C_SCOPE);
        enable_log_msg(ID_SEQUENCER, NON_QUIET, C_SCOPE);
        disable_log_msg(ID_POS_ACK, NON_QUIET, C_SCOPE);

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "----- initializing test bench       ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        -- Wait for UVVM to finish initialization
        await_uvvm_initialization(VOID);

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "-----    end of initialisation        ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        --shared_axistream_vvc_config(0).bfm_config.ready_default_value <= '1';
        --shared_axistream_vvc_config(1).bfm_config.ready_default_value <= '1';

        -- disable some log messages
        disable_log_msg(AXISTREAM_VVCT, AXIS8_INSTANCE, ID_PACKET_DATA); --@suppress
        disable_log_msg(AXISTREAM_VVCT, AXIS8_INSTANCE, ID_CMD_EXECUTOR);--@suppress
        disable_log_msg(AXISTREAM_VVCT, AXIS8_INSTANCE, ID_CMD_INTERPRETER);--@suppress
        disable_log_msg(AXISTREAM_VVCT, AXIS64_INSTANCE, ID_PACKET_DATA);--@suppress
        disable_log_msg(AXISTREAM_VVCT, AXIS64_INSTANCE, ID_CMD_EXECUTOR);--@suppress
        disable_log_msg(AXISTREAM_VVCT, AXIS64_INSTANCE, ID_CMD_INTERPRETER);--@suppress

        wait for (40*C_CLK_PERIOD);
        clk_ena <= true;
        clk_stable <= '1';
        wait for (20*C_CLK_PERIOD);
        reset <= '0';
        wait for (100*C_CLK_PERIOD);

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "-- test bench sequence starts here      ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        -- test all possible link/stream combinations
        for link in 0 to LINK_NUM-1 loop
            if LINK_CONFIG(link) = 1 then
                transmit_and_expect(link, 0, 16);
            else
                for stream in 0 to STREAMS_PER_LINK_FROMHOST-1 loop
                    transmit_and_expect(link, stream, 16);
                    wait for (10*C_CLK_PERIOD);
                end loop;
            end if;
        end loop;

        -- test different lengths on all links
        for link in 0 to LINK_NUM-1 loop
            for length in 1 to 4*DATA_WIDTH/8 loop
                transmit_and_expect(link, 0, length);
                wait for (10*C_CLK_PERIOD);
            end loop;
        end loop;

        -- test delay packets
        --for delay in 1 to 64 loop
        --    test_delay(delay);
        --end loop;

        -- test delay and regular packets intermixed
        --transmit_and_expect(0, 0, 16);
        --test_delay(16);
        --transmit_and_expect(0, 0, 16);
        --test_delay(16);
        --transmit_and_expect(0, 0, 16);
        --test_delay(16);

        -- test broadcasting on link 0
        register_map_control.BROADCAST_ENABLE(0) <= "000000000000000000000000000000000011111111";
        transmit_and_expect(0, 255, 20);
        register_map_control.BROADCAST_ENABLE(0) <= "000000000000000000000000000000000011111101";
        transmit_and_expect(0, 255, 20);
        register_map_control.BROADCAST_ENABLE(0) <= "000000000000000000000000000000000011110001";
        transmit_and_expect(0, 255, 20);
        register_map_control.BROADCAST_ENABLE(0) <= "000000000000000000000000000000000011111111";
        transmit_and_expect(0, 255, 20);

        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);

        -- cleanup
        enable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        enable_log_msg(ID_SEQUENCER);
        enable_log_msg(ID_UVVM_SEND_CMD);

        -- final report
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        -- finished simulation
        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely

    end process;

end architecture;

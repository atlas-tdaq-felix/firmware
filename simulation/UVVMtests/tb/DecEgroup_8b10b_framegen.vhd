--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               Ton Fleuren
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee, xpm;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use xpm.vcomponents.all;

entity DecEgroup_8b10b_framegen is
    generic (
        UseFELIXDataFormat : boolean := false --If false, use counter value 0, 1, 2, .... For true, use the data format used in the GBT data emulator etc.
    );
    port (
        clk40 : in std_logic;
        clk80 : in std_logic;
        clk160 : in std_logic;
        reset : in std_logic;
        strips_mode_en : in std_logic;
        direct_mode_en : in std_logic;
        --direct_mode_sync : out std_logic;
        IDLES : in integer;
        MessageLength : in integer;
        ElinkWidth : in std_logic_vector(2 downto 0);
        DataOut32b : out std_logic_vector(31 downto 0);
        StartFrameGen: in std_logic;
        NumberOfMessages: in integer; -- -1 for infinite
        HGTD_ALTIROC_ENCODING: in std_logic
    );
end entity DecEgroup_8b10b_framegen;

architecture rtl of DecEgroup_8b10b_framegen is
    signal wr_clk: std_logic;
    signal wr_en32: std_logic;
    signal wr_en16: std_logic;
    signal wr_en8: std_logic;
    signal DataIn : std_logic_vector(7 downto 0);
    signal DataInValid: std_logic;
    signal EOP_in : std_logic;
    signal EncoderReadyOut : std_logic;
    signal DataToFifo : std_logic_vector(7 downto 0);
    signal fifo_din : std_logic_vector(7 downto 0);
    signal DataToGearbox : std_logic_vector(9 downto 0);
    signal empty32: std_logic;
    signal empty16: std_logic;
    signal empty8: std_logic;
    signal full32: std_logic;
    signal full16: std_logic;
    signal full8: std_logic;
    signal rd_en32: std_logic;
    signal rd_en16: std_logic;
    signal rd_en8: std_logic;
    signal GearBoxReadyOut : std_logic;
    signal rd_rst_busy32, wr_rst_busy32: std_logic;
    signal rd_rst_busy16, wr_rst_busy16: std_logic;
    signal rd_rst_busy8, wr_rst_busy8: std_logic;
    signal reset_s: std_logic;
    signal DataOut32b_s: std_logic_vector(31 downto 0);
    signal DataOut16b_s: std_logic_vector(15 downto 0);
    signal DataOut8b_s: std_logic_vector(7 downto 0);
    signal cnt: integer range 0 to 100000;
    signal MessagesSent : integer range 0 to 1000000000:= 0;

    type slv8_array is array (natural range <>) of std_logic_vector(7 downto 0);
    --type slv8_array is array(0 to 255) of std_logic_vector(7 downto 0);
    signal FELIX_Data_Frame : slv8_array (0 to 255);
    signal L1ID, L1ID_next: integer range 0 to 1000000000:= 0;
    constant IDLESTREAM : slv8_array(0 to 4) := (x"c1", x"4f", x"ac", x"14", x"fa");
    signal dm_idlecnt : natural := 0;
    signal dm_startframegen : std_logic;

begin

    wr_clk <= clk40 when ElinkWidth = "010" else --8 bit
              clk80 when ElinkWidth = "011" else --16 bit
              clk160 when ElinkWidth = "100" else -- 32 bit
              clk40; --Illegal

    reset_s <= reset  or wr_rst_busy32 or wr_rst_busy16 or wr_rst_busy8;

    FELIX_Data_Frame_gen: process(MessageLength, ElinkWidth, L1ID)
    begin
        for i in 0 to MessageLength loop
            case i is
                when 0 => FELIX_Data_Frame(i) <= x"AA";
                when 1 => FELIX_Data_Frame(i) <= std_logic_vector(to_unsigned(abs(MessageLength-8)/256, 8));
                when 2 => FELIX_Data_Frame(i) <= std_logic_vector(to_unsigned(abs(MessageLength-8)mod 256, 8));
                when 3 => FELIX_Data_Frame(i) <= std_logic_vector(to_unsigned(L1ID/256, 8));
                when 4 => FELIX_Data_Frame(i) <= std_logic_vector(to_unsigned(L1ID, 8));
                when 5 => FELIX_Data_Frame(i) <= x"BB";
                when 6 => FELIX_Data_Frame(i) <= x"AA";
                when 7 =>
                    case ElinkWidth is
                        when "010" => FELIX_Data_Frame(i) <= x"08";
                        when "011" => FELIX_Data_Frame(i) <= x"10";
                        when "100" => FELIX_Data_Frame(i) <= x"20";
                        when others => FELIX_Data_Frame(i) <= x"00";
                    end case;
                when others => FELIX_Data_Frame(i) <= std_logic_vector(to_unsigned(i-8, 8));

            end case;

        end loop;
    end process;

    framegen_proc: process(wr_clk, reset_s)
    begin
        if reset_s = '1' then
            cnt<= 0;
            MessagesSent <= 0;
            L1ID <= 0;
            dm_idlecnt <= 0;
            dm_startframegen <= '0';

        elsif rising_edge(wr_clk) then
            if (direct_mode_en = '0') then -- simple split for direct-mode
                if StartFrameGen = '1' and ((MessagesSent < NumberOfMessages) or NumberOfMessages = -1) then
                    if (EncoderReadyOut = '1') then
                        L1ID <= L1ID_next;
                        if cnt < (MessageLength-1)+IDLES then
                            cnt <= cnt + 1;
                        else
                            cnt <= 0;
                            MessagesSent <= MessagesSent + 1;
                        end if;
                    end if;
                else
                    if StartFrameGen = '0' then
                        cnt <= 0;
                        MessagesSent <= 0;
                    end if;
                end if;
                dm_idlecnt <= 0;
            --direct_mode_sync <= '0';

            else --(direct_mode_en = '1')
                if (dm_startframegen = '1') and ((MessagesSent < NumberOfMessages) or NumberOfMessages = -1) then
                    if cnt < (MessageLength-1) then
                        cnt <= cnt + 1;
                    else
                        cnt <= 0;
                        MessagesSent <= MessagesSent + 1;
                    end if;
                else
                    if dm_startframegen = '0' then
                        cnt <= 0;
                        MessagesSent <= 0;
                    end if;
                end if;

                -- Direct mode counter for sending 5 byte idle pattern
                --direct_mode_sync <= '0'; -- default
                if (dm_idlecnt = 9) then -- trying syncing to 10 bytes too keep predictability
                    dm_idlecnt <= 0;
                    --direct_mode_sync <= '1';
                    dm_startframegen <= StartFrameGen; -- sync to idle pattern boundary
                else
                    dm_idlecnt <= dm_idlecnt + 1;
                end if;

            end if; --(direct_mode_en)


        end if;
    end process;

    framegen_comb_proc: process(all)
    begin
        L1ID_next <= L1ID;
        DataIn <= x"00";
        DataInValid <= '0';
        EOP_in <= '0';

        if (direct_mode_en = '0') then -- simple split for direct-mode
            if StartFrameGen = '1' and ((MessagesSent < NumberOfMessages) or NumberOfMessages = -1) then
                if cnt < IDLES then
                    DataIn <= x"BC";
                else
                    if cnt = (MessageLength-1)+IDLES then
                        EOP_in <= '1';
                        L1ID_next <= L1ID + 1;
                    end if;
                    if UseFELIXDataFormat then
                        DataIn <= FELIX_Data_Frame(cnt-IDLES);
                    elsif (strips_mode_en = '1') and (cnt = IDLES) then
                        DataIn <= std_logic_vector(to_unsigned(MessagesSent,4)) & x"0"; -- ABC/HCC packet types
                    else
                        DataIn <= std_logic_vector(to_unsigned(cnt-IDLES,8));
                    end if;
                    DataInValid <= '1';
                end if;
            end if;

        else --(direct_mode_en = '1')

            if dm_startframegen = '1' and ((MessagesSent < NumberOfMessages) or NumberOfMessages = -1) then
                DataIn <= std_logic_vector(to_unsigned(cnt,8));
                DataInValid <= '1';
            else
                DataIn <= IDLESTREAM(dm_idlecnt mod 5);
                DataInValid <= '1';
            end if;


        end if; --(direct_mode_en)

    end process;

    encoder0: entity work.Encoder8b10b
        generic map(
            GENERATE_FEI4B  => false,
            GENERATE_LCB_ENC => false,
            INSERT_IDLES => false,
            INCLUDE_XOFF => false
        )
        port map(
            reset => reset_s,
            clk40 => wr_clk, --Run at 1x, 2x or 4x rate and push into FIFO
            EnableIn => '1',
            DataIn => DataIn,
            DataInValid => DataInValid,
            EOP_in => EOP_in,
            toHostXoff => '0',
            readyIn => GearBoxReadyOut,
            HGTD_ALTIROC_ENCODING => HGTD_ALTIROC_ENCODING,
            readyOut => EncoderReadyOut,
            DataOut => DataToGearbox
        );

    gearbox0: entity work.EncodingGearBox
        generic map(
            MAX_OUTPUT => 8,
            MAX_INPUT => 10,
            -- 8, 4, 2
            SUPPORT_OUTPUT => "100",
            -- 10, 8
            SUPPORT_INPUT => "10"
        )
        port map(
            Reset                => reset_s,
            clk40                => wr_clk,

            ELinkData            => DataToFifo,
            ElinkWidth           => "10", --8-bit operation
            MsbFirst             => '1',
            ReverseOutputBits    => '0',

            DataIn               => DataToGearbox,
            InputWidth           => '1',
            ReadyOut             => GearBoxReadyOut
        );

    fifo_din <= DataToFifo when (direct_mode_en = '0') else DataIn;

    fifo32 : xpm_fifo_async
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT"
            CDC_SYNC_STAGES => 2,
            DOUT_RESET_VALUE => "0",
            ECC_MODE => "no_ecc",
            FIFO_MEMORY_TYPE => "auto",
            FIFO_READ_LATENCY => 1,
            FIFO_WRITE_DEPTH => 2048,
            FULL_RESET_VALUE => 0,
            PROG_EMPTY_THRESH => 8,
            PROG_FULL_THRESH => 2040,
            RD_DATA_COUNT_WIDTH => 1,
            READ_DATA_WIDTH => 32,
            READ_MODE => "fwft",
            RELATED_CLOCKS => 1,
            SIM_ASSERT_CHK => 0,
            USE_ADV_FEATURES => "0000",
            WAKEUP_TIME => 0,
            WRITE_DATA_WIDTH => 8,
            WR_DATA_COUNT_WIDTH => 1
        )
        port map (
            sleep => '0',
            rst => reset,
            wr_clk => wr_clk,
            wr_en => wr_en32,
            din => fifo_din,
            full => full32,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => wr_rst_busy32,
            almost_full => open,
            wr_ack => open,
            rd_clk => clk40,
            rd_en => rd_en32,
            dout => DataOut32b_s,
            empty => empty32,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => rd_rst_busy32,
            almost_empty => open,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );

    fifo16 : xpm_fifo_async
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT"
            CDC_SYNC_STAGES => 2,
            DOUT_RESET_VALUE => "0",
            ECC_MODE => "no_ecc",
            FIFO_MEMORY_TYPE => "auto",
            FIFO_READ_LATENCY => 1,
            FIFO_WRITE_DEPTH => 2048,
            FULL_RESET_VALUE => 0,
            PROG_EMPTY_THRESH => 8,
            PROG_FULL_THRESH => 2040,
            RD_DATA_COUNT_WIDTH => 1,
            READ_DATA_WIDTH => 16,
            READ_MODE => "fwft",
            RELATED_CLOCKS => 1,
            SIM_ASSERT_CHK => 0,
            USE_ADV_FEATURES => "0000",
            WAKEUP_TIME => 0,
            WRITE_DATA_WIDTH => 8,
            WR_DATA_COUNT_WIDTH => 1
        )
        port map (
            sleep => '0',
            rst => reset,
            wr_clk => wr_clk,
            wr_en => wr_en16,
            din => fifo_din,
            full => full16,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => wr_rst_busy16,
            almost_full => open,
            wr_ack => open,
            rd_clk => clk40,
            rd_en => rd_en16,
            dout => DataOut16b_s,
            empty => empty16,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => rd_rst_busy16,
            almost_empty => open,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );

    fifo8 : xpm_fifo_async
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT"
            CDC_SYNC_STAGES => 2,
            DOUT_RESET_VALUE => "0",
            ECC_MODE => "no_ecc",
            FIFO_MEMORY_TYPE => "auto",
            FIFO_READ_LATENCY => 1,
            FIFO_WRITE_DEPTH => 2048,
            FULL_RESET_VALUE => 0,
            PROG_EMPTY_THRESH => 8,
            PROG_FULL_THRESH => 2040,
            RD_DATA_COUNT_WIDTH => 1,
            READ_DATA_WIDTH => 8,
            READ_MODE => "fwft",
            RELATED_CLOCKS => 1,
            SIM_ASSERT_CHK => 0,
            USE_ADV_FEATURES => "0000",
            WAKEUP_TIME => 0,
            WRITE_DATA_WIDTH => 8,
            WR_DATA_COUNT_WIDTH => 1
        )
        port map (
            sleep => '0',
            rst => reset,
            wr_clk => wr_clk,
            wr_en => wr_en8,
            din => fifo_din,
            full => full8,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => wr_rst_busy8,
            almost_full => open,
            wr_ack => open,
            rd_clk => clk40,
            rd_en => rd_en8,
            dout => DataOut8b_s,
            empty => empty8,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => rd_rst_busy8,
            almost_empty => open,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );

    DataOut32b <= DataOut32b_s(7 downto 0) &
                  DataOut32b_s(15 downto 8) &
                  DataOut32b_s(23 downto 16) &
                  DataOut32b_s(31 downto 24) when ElinkWidth = "100" else
                  DataOut16b_s(7 downto 0) &
                  DataOut16b_s(15 downto 8) &
                  DataOut16b_s(7 downto 0) &
                  DataOut16b_s(15 downto 8) when ElinkWidth = "011" else
                  DataOut8b_s(7 downto 0) &
                  DataOut8b_s(7 downto 0) &
                  DataOut8b_s(7 downto 0) &
                  DataOut8b_s(7 downto 0);



    wr_en32 <= (not full32) and (not wr_rst_busy32) when rising_edge(clk40); --needs to be aligned with clk40 for direct mode framing
    wr_en16 <= (not full16) and (not wr_rst_busy16) when rising_edge(clk40); --  i.e. byte alignment of the output word
    wr_en8 <= (not full8) and (not wr_rst_busy8);

    rd_en32 <= (not empty32) and (not rd_rst_busy32);
    rd_en16 <= (not empty16) and (not rd_rst_busy16);
    rd_en8 <= (not empty8) and (not rd_rst_busy8);

end architecture rtl;

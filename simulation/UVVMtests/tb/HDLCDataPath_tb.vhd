library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

library uvvm_util;
    context uvvm_util.uvvm_util_context;
library bitvis_vip_axistream;
    use bitvis_vip_axistream.axistream_bfm_pkg.all;
    use bitvis_vip_axistream.vvc_methods_pkg.all;
    context bitvis_vip_axistream.vvc_context;
library uvvm_vvc_framework;
    use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

    use work.axi_stream_package.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;

entity HDLCDataPath_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end HDLCDataPath_tb;

architecture tb of HDLCDataPath_tb is
    -- configuration
    constant C_SCOPE : string                         := "HDLCDataPath_tb";
    constant C_CLK40_PERIOD : time                    := 25 ns;

    signal clk_ena : boolean := false;
    signal clk40 : std_logic;
    signal rst40 : std_logic;
    signal elink_data : std_logic_vector(1 downto 0);

    signal enc_enable_delay : std_logic;
    signal enc_enable : std_logic;
    signal enc_axis : axis_8_type;
    signal enc_axis_tready : std_logic;
    signal dec_enable : std_logic;
    signal dec_axis : axis_32_type;
    signal dec_axis_tready : std_logic;
    signal dec_axis_prog_empty : std_logic; -- @suppress "signal dec_axis_prog_empty is never read"

    constant ENC_AXIS_INSTANCE : integer := 0;
    constant DEC_AXIS_INSTANCE : integer := 1;
    signal enc_axis_vvc_if : t_axistream_if(tdata(7 downto 0), tkeep(0 downto 0), tuser(0 downto 0), tstrb(0 downto 0), tid(0 downto 0), tdest(0 downto 0)) := init_axistream_if_signals(true, 8, 1, 1, 1);
    signal dec_axis_vvc_if : t_axistream_if(tdata(31 downto 0), tkeep(3 downto 0), tuser(3 downto 0), tstrb(3 downto 0), tid(7 downto 0), tdest(0 downto 0)) := init_axistream_if_signals(true, 32, 4, 8, 1);
    signal clk40_stable : std_logic;
    signal daq_reset : std_logic;
    signal daq_fifo_flush : std_logic;
begin

    -- Instantiate the concurrent procedure that initializes UVVM
    i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

    -- clock generator
    clock_generator(clk40, clk_ena, C_CLK40_PERIOD, "40 MHz clock");

    enc: entity work.EncodingEpathGBT
        generic map(
            MAX_OUTPUT => 2,
            INCLUDE_8b => '0',
            INCLUDE_4b => '0',
            INCLUDE_2b => '1',
            INCLUDE_8b10b => '1',
            INCLUDE_HDLC => '1',
            INCLUDE_DIRECT => '1',
            INCLUDE_TTC => '0',
            --BLOCKSIZE => BLOCKSIZE,
            GENERATE_FEI4B => false,
            GENERATE_LCB_ENC => false,
            HDLC_IDLE_STATE => x"FF",
            USE_BUILT_IN_FIFO => '1',
            SUPPORT_HDLC_DELAY => true,
            DISTR_RAM => true,
            INCLUDE_XOFF => false
        )
        port map(
            clk40 => clk40,
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            EpathEnable => enc_enable,
            EpathEncoding => "0010", --Only support HDLC
            ElinkWidth => "00",
            MsbFirst => '0',
            ReverseOutputBits => '0',
            ElinkData => elink_data,
            toHostXoff => '0',
            epath_almost_full => open,
            s_axis => enc_axis,
            s_axis_tready => enc_axis_tready,
            s_axis_aclk => clk40,
            EnableHDLCDelay => enc_enable_delay,
            TTCOption => (others => '0'),
            TTCin => TTC_zero,
            FEI4Config => (others => '0')
        );

    dec: entity work.DecodingEpathGBT
        generic map(
            MAX_INPUT => 2,
            INCLUDE_16b => '0',
            INCLUDE_8b => '0',
            INCLUDE_4b => '0',
            INCLUDE_2b => '1',
            INCLUDE_8b10b => '1',
            INCLUDE_HDLC => '1',
            INCLUDE_DIRECT => '0',
            BLOCKSIZE => 1024,
            USE_BUILT_IN_FIFO => '1',
            GENERATE_FEI4B => false,
            VERSAL => false
        )
        port map(
            clk40 => clk40,
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            EpathEnable => dec_enable,
            EpathEncoding => "0010", --Only support HDLC
            ElinkWidth => "000",
            MsbFirst   => '0',
            ReverseInputBits => '0',
            EnableTruncation => '0',
            ElinkData  => elink_data,
            ElinkAligned => '1',
            DecoderAligned => open,
            AlignmentPulseAlign => '0',
            AlignmentPulseDeAlign => '0',
            AutoRealign => '1',
            RealignmentEvent => open,
            FE_BUSY_out => open,
            m_axis => dec_axis,
            m_axis_tready => dec_axis_tready,
            m_axis_aclk => clk40,
            m_axis_prog_empty => dec_axis_prog_empty
        );

    enc_axis_vvc: entity bitvis_vip_axistream.axistream_vvc
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_AXISTREAM_BFM_CONFIG, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => true,
            GC_DATA_WIDTH => 8,
            GC_USER_WIDTH => 1,
            GC_ID_WIDTH => 1,
            GC_DEST_WIDTH => 1,
            GC_INSTANCE_IDX => ENC_AXIS_INSTANCE
        )
        port map (
            clk => clk40,
            axistream_vvc_if => enc_axis_vvc_if
        );

    enc_axis.tdata <= enc_axis_vvc_if.tdata;
    enc_axis.tvalid <= enc_axis_vvc_if.tvalid;
    enc_axis.tlast <= enc_axis_vvc_if.tlast;
    enc_axis_vvc_if.tready <= enc_axis_tready;

    dec_axis_vvc: entity bitvis_vip_axistream.axistream_vvc
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_AXISTREAM_BFM_CONFIG, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => false,
            GC_DATA_WIDTH => 32,
            GC_USER_WIDTH => 4,
            GC_ID_WIDTH => 8,
            GC_DEST_WIDTH => 1,
            GC_INSTANCE_IDX => DEC_AXIS_INSTANCE
        )
        port map (
            clk => clk40,
            axistream_vvc_if => dec_axis_vvc_if
        );

    dec_axis_vvc_if.tdata <= dec_axis.tdata;
    dec_axis_vvc_if.tkeep <= dec_axis.tkeep;
    dec_axis_vvc_if.tvalid <= dec_axis.tvalid;
    dec_axis_vvc_if.tuser <= dec_axis.tuser;
    dec_axis_vvc_if.tlast <= dec_axis.tlast;
    dec_axis_tready <= dec_axis_vvc_if.tready;

    resetmgr0: entity work.CRresetManager
        port map(
            clk40         => clk40,
            rst           => rst40,
            clk40_stable  => clk40_stable,
            cr_rst        => daq_reset,
            cr_fifo_flush => daq_fifo_flush
        );

    stim: process
        procedure test_data(constant pktdata : t_byte_array; constant wait_to_complete : boolean := true) is
        begin
            -- transmit and expect
            axistream_transmit(AXISTREAM_VVCT, ENC_AXIS_INSTANCE, pktdata, "transmitting data");--@suppress
            axistream_expect(AXISTREAM_VVCT, DEC_AXIS_INSTANCE, pktdata, "receiving data");--@suppress

            -- wait for AXIS to complete
            if wait_to_complete then
                await_completion(AXISTREAM_VVCT, ENC_AXIS_INSTANCE, 10000 * C_CLK40_PERIOD);--@suppress
                await_completion(AXISTREAM_VVCT, DEC_AXIS_INSTANCE, 10000 * C_CLK40_PERIOD);--@suppress
            end if;
        end procedure;

        procedure test_16byte_fixed(constant val : std_logic_vector(7 downto 0)) is
            variable pktdata : t_byte_array(0 to 15);
        begin
            -- prepare packet
            pktdata := (others => val);

            -- send the packet
            test_data(pktdata, true);
        end procedure;

        procedure test_length(constant len : integer) is
            variable pktdata : t_byte_array(0 to len-1);
        begin
            -- prepare packet
            for i in 0 to len-1 loop
                pktdata(i) := std_logic_vector(to_unsigned(i mod 256, 8));
            end loop;

            -- send the packet
            test_data(pktdata, true);
        end procedure;

        procedure test_random(constant num_pkts : integer; constant len : integer) is
            variable pktdata : t_byte_array(0 to len-1);
        begin
            for n in 1 to num_pkts loop
                -- prepare packet
                for i in 0 to len-1 loop
                    pktdata(i) := random(8);
                end loop;

                -- send the packet
                test_data(pktdata, true);
            end loop;
        end procedure;

        procedure test_random_back2back(constant num_pkts : integer; constant len : integer) is
            variable pktdata : t_byte_array(0 to len-1);
        begin
            for n in 1 to num_pkts loop
                -- prepare packet
                for i in 0 to len-1 loop
                    pktdata(i) := random(8);
                end loop;

                -- send the packet
                test_data(pktdata, n = num_pkts);
            end loop;
        end procedure;
    begin
        -- initialize
        clk_ena <= false;
        clk40_stable <= '0';
        rst40 <= '1';
        dec_enable <= '0';
        enc_enable <= '0';
        enc_enable_delay <= '0';

        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);

        --enable_log_msg(ALL_MESSAGES);
        disable_log_msg(ALL_MESSAGES, NON_QUIET, C_SCOPE);
        enable_log_msg(ID_SEQUENCER, NON_QUIET, C_SCOPE);
        disable_log_msg(ID_POS_ACK, NON_QUIET, C_SCOPE);

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "----- initializing test bench       ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        -- Wait for UVVM to finish initialization
        await_uvvm_initialization(VOID);

        -- disable some logging
        disable_log_msg(AXISTREAM_VVCT, ENC_AXIS_INSTANCE, ID_PACKET_DATA); --@suppress
        disable_log_msg(AXISTREAM_VVCT, ENC_AXIS_INSTANCE, ID_CMD_INTERPRETER);--@suppress
        disable_log_msg(AXISTREAM_VVCT, ENC_AXIS_INSTANCE, ID_CMD_INTERPRETER_WAIT);--@suppress
        disable_log_msg(AXISTREAM_VVCT, ENC_AXIS_INSTANCE, ID_PACKET_INITIATE);--@suppress
        disable_log_msg(AXISTREAM_VVCT, ENC_AXIS_INSTANCE, ID_PACKET_COMPLETE);--@suppress
        disable_log_msg(AXISTREAM_VVCT, ENC_AXIS_INSTANCE, ID_CMD_EXECUTOR);--@suppress
        disable_log_msg(AXISTREAM_VVCT, DEC_AXIS_INSTANCE, ID_PACKET_DATA); --@suppress
        disable_log_msg(AXISTREAM_VVCT, DEC_AXIS_INSTANCE, ID_CMD_INTERPRETER);--@suppress
        disable_log_msg(AXISTREAM_VVCT, DEC_AXIS_INSTANCE, ID_CMD_INTERPRETER_WAIT);--@suppress
        disable_log_msg(AXISTREAM_VVCT, DEC_AXIS_INSTANCE, ID_PACKET_INITIATE);--@suppress
        disable_log_msg(AXISTREAM_VVCT, DEC_AXIS_INSTANCE, ID_PACKET_COMPLETE);--@suppress
        disable_log_msg(AXISTREAM_VVCT, DEC_AXIS_INSTANCE, ID_CMD_EXECUTOR);--@suppress

        -- init random number generator
        randomize(1234, 5678);

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "-----    end of initialisation        ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        wait for (40*C_CLK40_PERIOD);
        clk_ena <= true;
        clk40_stable <= '1';
        wait for (20*C_CLK40_PERIOD);
        wait until rising_edge(clk40);
        rst40 <= '0';

        wait for (80*C_CLK40_PERIOD);
        wait until rising_edge(clk40);
        dec_enable <= '1';
        enc_enable <= '1';

        wait for (20*C_CLK40_PERIOD);

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "-- test bench sequence starts here      ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        -- test various packet lengths
        for i in 1 to 128 loop
            test_length(i);
        end loop;

        -- test various 16 byte packets with different values
        for i in 0 to 255 loop
            test_16byte_fixed(std_logic_vector(to_unsigned(i, 8)));
        end loop;

        -- test random packets
        test_random(10000, 16);

        -- test random packets back2back
        test_random_back2back(100, 16);

        -- test henk's testdata
        enc_enable_delay <= '0';
        test_data(( x"00", x"00", x"0a", x"08", x"00", x"00", x"fe", x"00", x"00", x"03", x"82", x"e3" ), false);
        test_data(( x"00", x"00", x"0a", x"08", x"00", x"22", x"fe", x"00", x"00", x"03", x"9b", x"95" ), false);
        test_data(( x"00", x"00", x"0a", x"0a", x"00", x"44", x"01", x"00", x"02", x"02", x"00", x"04", x"3d", x"1a" ), false);
        test_data(( x"00", x"00", x"0a", x"0c", x"00", x"66", x"fd", x"02", x"04", x"21", x"00", x"04", x"00", x"00", x"27", x"ff" ), false);
        test_data(( x"00", x"00", x"0a", x"0c", x"00", x"88", x"fe", x"02", x"04", x"01", x"00", x"04", x"00", x"00", x"3e", x"1b" ), false);
        test_data(( x"00", x"00", x"0a", x"0c", x"00", x"aa", x"02", x"02", x"04", x"20", x"00", x"00", x"08", x"00", x"70", x"c9" ), false);
        test_data(( x"00", x"00", x"0a", x"0c", x"00", x"cc", x"03", x"02", x"04", x"10", x"ff", x"df", x"f7", x"ff", x"f9", x"d9" ), true);

        -- wait for everything to finish up
        await_completion(AXISTREAM_VVCT, ENC_AXIS_INSTANCE, 1200 * C_CLK40_PERIOD);--@suppress
        await_completion(AXISTREAM_VVCT, DEC_AXIS_INSTANCE, 1200 * C_CLK40_PERIOD);--@suppress

        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);

        -- cleanup
        enable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        enable_log_msg(ID_SEQUENCER);
        enable_log_msg(ID_UVVM_SEND_CMD);

        -- final report
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        -- finished simulation
        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely

    end process;

end architecture;

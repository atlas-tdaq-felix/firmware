
library IEEE, UNISIM;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;
    use ieee.numeric_std.all;
    use work.FELIX_package.all;

Library xpm;
    use xpm.vcomponents.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;


entity LTI_FE_Transmitter_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end LTI_FE_Transmitter_tb;

architecture Behavioral of LTI_FE_Transmitter_tb is
    constant GBT_NUM : integer := 4;
    constant clk240_period : time := 4.157 ns;
    constant clk40_period : time := clk240_period*6;


    signal daq_fifo_flush  : std_logic;
    signal clk40    : std_logic := '0';
    signal clk240   : std_logic;        --240.474
    signal clk40_en : boolean   := false;

    signal LTI_TX_Data_Transceiver                             : array_32b(0 to GBT_NUM - 1);
    signal LTI_TX_TX_CharIsK                                   : array_4b(0 to GBT_NUM - 1);
    signal TTCin, TTC_decoded_p1                  : TTC_data_type := TTC_zero;
    signal TTC_decoded : TTC_data_array_type(0 to GBT_NUM-1) := (others => TTC_zero);
    signal TTCverify, TTCverify_p1, TTCverify_p2, TTCverify_p3 : TTC_data_type;
    signal LTI_CRC_Err : std_logic_vector(GBT_NUM-1 downto 0);

    signal TTCin_matches_ttc_decoded : std_logic; -- @suppress "Signal TTCin_matches_ttc_decoded is never read"
    signal ttc_crc_valid                        : std_logic;
    constant XoffIn : std_logic_vector(GBT_NUM-1 downto 0) := (others => '0');
    signal L0A: std_logic;
    signal LTI_Receiver_aligned : std_logic_vector(0 to GBT_NUM-1);
    constant ones: std_logic_vector(0 to GBT_NUM-1) := (others => '1');
begin

    clock_generator(clk40, clk40_en, clk40_period, "40 MHz CLK");
    clock_generator(clk240, clk40_en, clk240_period, "240 MHz CLK");

    reset_proc : process
    begin
        daq_fifo_flush <= '1';
        wait for clk40_period * 10;

        daq_fifo_flush <= '0';
        wait;
    end process;

    l0a_proc: process
    begin
        L0A <= '0';
        await_value(LTI_Receiver_aligned, ones, 0 ns, 200 us, TB_ERROR, "Wait lti receiver to align", C_SCOPE);
        wait for clk40_period * 20;
        wait for 1 ps;
        for i in 0 to 10 loop
            L0A <= '1';
            wait for clk40_period;
            L0A <= '0';
            wait for clk40_period*39;
        end loop;
        wait;
    end process;

    TTC_LTI_TX_0 : entity work.LTI_FE_Transmitter
        generic map(
            GBT_NUM => GBT_NUM
        )
        port map(
            clk40                   => clk40,
            daq_fifo_flush          => daq_fifo_flush,
            TTCin                   => TTCin,
            XoffIn => XoffIn,
            LTI_TX_Data_Transceiver => LTI_TX_Data_Transceiver,
            LTI_TX_TX_CharIsK       => LTI_TX_TX_CharIsK,
            LTI_TXUSRCLK_in         => (others => clk240)
        );

    TTC_LTI_RX_0: entity work.Frontend_TTC_LTI_Receiver
        generic map(
            GBT_NUM => GBT_NUM
        )
        port map(
            TTCout                  => TTC_decoded,
            clk40_in                => (others => clk40),
            LTI_CRC_Err             => LTI_CRC_Err,
            LTI_Receiver_aligned    => LTI_Receiver_aligned,
            LTI_RX_Data_Transceiver => LTI_TX_Data_Transceiver,
            LTI_RX_RX_CharIsK       => LTI_TX_TX_CharIsK,
            LTI_RXUSRCLK_in         => (others => clk240),
            reset_RX_40M_FrameClk_BUFG => open
        );

    verify : process(clk40)
    begin
        if rising_edge(clk40) then
            TTCverify_p1   <= TTCin;
            TTCverify_p2   <= TTCverify_p1;
            TTCverify_p3   <= TTCverify_p2;
            TTCverify      <= TTCverify_p3;
            TTC_decoded_p1 <= TTC_decoded(0);
        end if;
    end process;

    TTCin_matches_ttc_decoded <= '1' when TTC_decoded_p1 = TTCverify else '0';
    ttc_crc_valid             <= not LTI_CRC_Err(0);
    TTCin.L0A              <= L0A;
    TTCin.L1A              <= L0A;

    UVVM_main_proc: process
    begin
        clk40_en <= true;

        TTCin.TriggerType      <= x"DEAD";
        TTCin.LBID             <= x"BEEF";
        TTCin.OrbitID          <= x"76543210";
        TTCin.L0ID             <= "00"&x"012345678";
        TTCin.SyncGlobalData   <= x"5555";
        TTCin.TS               <= '0';
        TTCin.ErrorFlags       <= x"9";
        TTCin.SL0ID            <= '0';
        TTCin.SOrb             <= '0';
        TTCin.Sync             <= '0';
        TTCin.GRst             <= '0';
        --TTCin.MT               <= '0';
        TTCin.PT               <= '0';
        TTCin.Partition        <= "00";
        TTCin.BCID             <= x"ABC";
        TTCin.SyncUserData     <= x"DA1A";
        await_value(daq_fifo_flush, '0', 0 ns, 200 us, TB_ERROR, "Wait for aresetn to go low", C_SCOPE);
        await_value(TTC_decoded_p1.LTI_decoder_aligned, '1', 0 ns, 200 us, TB_ERROR, "TTCin must align", C_SCOPE);
        wait for 2 us;
        check_value(TTC_decoded_p1.PT,        TTCverify.PT,        ERROR, "TTCin PT must match decoded data", C_SCOPE);
        check_value(TTC_decoded_p1.Partition, TTCverify.Partition, ERROR, "TTCin Partition must match decoded data", C_SCOPE);
        check_value(TTC_decoded_p1.BCID,      TTCverify.BCID,      ERROR, "TTCin BCID must match decoded data", C_SCOPE);
        check_value(TTC_decoded_p1.SyncUserData,      TTCverify.SyncUserData,      ERROR, "TTCin SyncUserData must match decoded data", C_SCOPE);
        check_value(TTC_decoded_p1.SyncGlobalData,      TTCverify.SyncGlobalData,      ERROR, "TTCin SyncGlobalData must match decoded data", C_SCOPE);
        check_value(TTC_decoded_p1.TS,      TTCverify.TS,      ERROR, "TTCin TS must match decoded data", C_SCOPE);
        check_value(TTC_decoded_p1.ErrorFlags,      TTCverify.ErrorFlags,      ERROR, "TTCin TS must match decoded data", C_SCOPE);
        check_value(TTC_decoded_p1.SL0ID,      TTCverify.SL0ID,      ERROR, "TTCin SL0ID must match decoded data", C_SCOPE);
        check_value(TTC_decoded_p1.SOrb,      TTCverify.SOrb,      ERROR, "TTCin SOrb must match decoded data", C_SCOPE);
        check_value(TTC_decoded_p1.Sync,      TTCverify.Sync,      ERROR, "TTCin Sync must match decoded data", C_SCOPE);
        check_value(TTC_decoded_p1.GRst,      TTCverify.GRst,      ERROR, "TTCin GRst must match decoded data", C_SCOPE);
        check_value(TTC_decoded_p1.L0A,      TTCverify.L0A,      ERROR, "TTCin L0A must match decoded data", C_SCOPE);
        check_value(TTC_decoded_p1.L0ID,      TTCverify.L0ID,      ERROR, "TTCin L0ID must match decoded data", C_SCOPE);
        check_value(TTC_decoded_p1.OrbitID,      TTCverify.OrbitID,      ERROR, "TTCin OrbitID must match decoded data", C_SCOPE);
        check_value(TTC_decoded_p1.TriggerType,      TTCverify.TriggerType,      ERROR, "TTCin TriggerType must match decoded data", C_SCOPE);
        check_value(TTC_decoded_p1.LBID,      TTCverify.LBID,      ERROR, "TTCin LBID must match decoded data", C_SCOPE);
        check_value(TTC_decoded_p1.AsyncUserData,      TTCverify.AsyncUserData,      ERROR, "TTCin AsyncUserData must match decoded data", C_SCOPE);
        check_value(TTC_decoded_p1.XOFF,      TTCverify.XOFF,      ERROR, "TTCin XOFF must match decoded data", C_SCOPE);
        check_value(TTC_decoded_p1.L1A,      TTCverify.L1A,      ERROR, "TTCin L1A must match decoded data", C_SCOPE);

        for i in 0 to 20 loop
            wait for clk40_period*10;

            TTCin.LBID    <= random(16);
            TTCin.OrbitID <= random(32);
            TTCin.SyncGlobalData <= random(16);
            TTCin.SyncUserData   <= random(16);
        end loop;


        wait for 1 us;
        --check_value(TTCin_matches_ttc_decoded, '1', ERROR, "Check whether decoded TTC frame matches input", C_SCOPE);

        check_value(ttc_crc_valid, '1', ERROR, "Check whether calculated CRC matches received CRC", C_SCOPE);

        --==================================================================================================
        -- Ending the simulation
        --------------------------------------------------------------------------------------
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        -- Finish the simulation
        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely
    end process;
end architecture;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Filiberto Bonini
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--  ATLAS
--  FELIX UVVM test automation project
--  A.SKAF
--  University of Goettingen
--  July 2019
----------------------------------------------------------------------------------
-- file: EGroup_tb.vhd.
----------------------------------------------------------------------------------
library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

    use work.pcie_package.all;
    use work.lpgbtfpga_package.all;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.axi_stream_package.all;
    use work.interlaken_package.slv_67_array;



-- Test bench entity
entity GBTLinkToHost_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity;

-- Test bench architecture
architecture arch of GBTLinkToHost_tb is

    constant C_SCOPE : string := "GBTLinkToHost_tb";
    constant C_CLK240_PERIOD : time    := 4.158 ns;
    constant C_CLK40_PERIOD  : time    := C_CLK240_PERIOD*6;
    constant C_CLK160_PERIOD : time    := C_CLK40_PERIOD/4;
    constant C_CLK250_PERIOD : time    := 4 ns;

    constant GBT_NUM : integer := 8;
    constant ENDPOINTS : integer := 1;
    constant FIRMWARE_MODE : integer := FIRMWARE_MODE_GBT;
    constant NUMBER_OF_DESCRIPTORS : integer := 2;
    constant STREAMS_TOHOST: integer := STREAMS_TOHOST_MODE(FIRMWARE_MODE);
    constant BLOCKSIZE : integer :=1024;
    constant LOCK_PERIOD : integer := 640;
    constant DATA_WIDTH : integer := 256;
    constant SIM_NUMBER_OF_BLOCKS : integer := 500; -- 5000; --500;

    constant USE_URAM : boolean := false;


    constant pcie_endpoint: integer := 0;
    signal GBT_UPLINK_USER_DATA                : array_120b(0 to (GBT_NUM-1));
    signal register_map_xoff_monitor           : register_map_xoff_monitor_type; -- @suppress "signal register_map_xoff_monitor is never read"
    signal register_map_gbtemu_monitor         : register_map_gbtemu_monitor_type; -- @suppress "Unused declaration"
    signal register_map_decoding_monitor       : register_map_decoding_monitor_type; -- @suppress "signal register_map_decoding_monitor is never read"
    signal register_map_40_control             : register_map_control_type;
    signal emuToHost_GBTdata: std_logic_vector(119 downto 0);
    signal emuToHost_GBTlinkValid : std_logic;
    signal GBT_UPLINK_USER_DATA_FOSEL : array_120b (0 to GBT_NUM/ENDPOINTS-1);

    signal LinkAligned_FOSEL : std_logic_vector(GBT_NUM/ENDPOINTS-1 downto 0);
    signal toHostFifo_din                      : slv_array(0 to NUMBER_OF_DESCRIPTORS-2);
    signal toHostFifo_wr_en                    : std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
    signal toHostFifo_prog_full                : std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
    signal toHostFifo_wr_clk                   : std_logic;
    signal toHostFifo_rst                      : std_logic; -- @suppress "signal toHostFifo_rst is never read"
    signal decoding_aclk                       : std_logic;
    signal decoding_axis                       : axis_32_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
    signal decoding_axis_tready                : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
    signal decoding_axis_prog_empty            : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
    signal fanout_sel_axis                     : axis_32_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
    signal fanout_sel_axis_tready              : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
    signal LinkAligned                         : std_logic_vector(GBT_NUM-1 downto 0);
    signal RXUSRCLK                            : std_logic_vector(GBT_NUM-1 downto 0);

    signal clk40, clk240, clk250, clk160  : std_logic:='0';
    signal clock_ena     : boolean   := false;

    signal reset : std_logic;
    signal rst_hw, rst_soft_40: std_logic;

    signal start_check: std_logic:='0'; -- @suppress "signal start_check is never read"

    signal Trunc      : std_logic:='0'; -- @suppress "signal Trunc is never read"
    signal TrailerError:std_logic:='0'; -- @suppress "signal TrailerError is never read"
    signal CRCError : std_logic:='0'; -- @suppress "signal CRCError is never read"

    signal ChunkLengthError: std_logic:='0';  -- @suppress "signal ChunkLengthError is never read"
    signal GTH_FM_RX_33b_out                   : array_33b(0 to (GBT_NUM-1));

    signal checker_done: std_logic;
    constant IncludeDecodingEpath2_HDLC : std_logic_vector(6 downto 0) := (others => '1');
    constant IncludeDecodingEpath2_8b10b : std_logic_vector(6 downto 0) := (others => '1'); -- @suppress "signal block_error0 is never read"
    constant IncludeDecodingEpath4_8b10b : std_logic_vector(6 downto 0) := (others => '1');
    constant IncludeDecodingEpath8_8b10b : std_logic_vector(6 downto 0) := (others => '1');
    constant IncludeDecodingEpath16_8b10b : std_logic_vector(6 downto 0) := (others => '1');
    constant IncludeDecodingEpath32_8b10b : std_logic_vector(6 downto 0) := (others => '1');
    signal register_map_crtohost_monitor : register_map_crtohost_monitor_type; -- @suppress "signal register_map_crtohost_monitor is never read"

    constant TTC_ToHost_Data_in : TTC_data_type := TTC_zero;
    constant ElinkBusyIn : array_57b(0 to GBT_NUM-1) := (others => (others => '0'));
    constant DmaBusyIn : std_logic := '0';
    constant FifoBusyIn : std_logic := '0';
    constant BusySumIn : std_logic := '0';
    signal decoding_axis_aux : axis_32_array_type(0 to 1);
    signal decoding_axis_aux_prog_empty : axis_tready_array_type(0 to 1);
    signal decoding_axis_aux_tready : axis_tready_array_type(0 to 1);

    constant Interlaken_RX_Data : slv_67_array(0 to GBT_NUM-1):= (others => (others => '0'));
    constant Interlaken_RX_Datavalid : std_logic_vector(GBT_NUM-1 downto 0) := (others => '0');
    signal clk40_stable : std_logic;
    signal daq_reset : std_logic;
    signal daq_fifo_flush : std_logic;
--signal toHost_axis64_aclk : std_logic;
--signal toHost_axis64 : axis_64_array_type(0 to GBT_NUM/ENDPOINTS - 1);
--signal toHost_axis64_tready : axis_tready_array_type(0 to GBT_NUM/ENDPOINTS - 1);
--signal toHost_axis64_prog_empty : axis_tready_array_type(0 to GBT_NUM/ENDPOINTS - 1);

begin

    LinkAligned <= (others => '0');
    GTH_FM_RX_33b_out <= (others => (others => '0'));
    GBT_UPLINK_USER_DATA <= (others => (others => '0'));

    register_map_40_control.DECODING_REVERSE_10B <= "1";
    register_map_40_control.ENCODING_REVERSE_10B <= "1";
    register_map_40_control.TIMEOUT_CTRL.TIMEOUT <= x"0000FFFF";
    register_map_40_control.TIMEOUT_CTRL.ENABLE <= "1";
    register_map_40_control.DECODING_HGTD_ALTIROC <= "0";
    register_map_40_control.DISCARD_DATA_FOR_DESCR.DMA_DISABLED <= x"00";
    register_map_40_control.DISCARD_DATA_FOR_DESCR.FIFO_FULL <= x"00";
    register_map_40_control.ELINK_REALIGNMENT.ENABLE <= "1";


    GEN_SCF_LINKS: for i in 0 to 11 generate
        register_map_40_control.SUPER_CHUNK_FACTOR_LINK(i) <= x"01";
        register_map_40_control.CRTOHOST_INSTANT_TIMEOUT_ENA(i) <= "000000000000000000000000000000000000000001";
    --register_map_40_control.CRTOHOST_INSTANT_TIMEOUT_ENA(i) <= "000000000000000000000000000000000000000000"; --No instant timeout

    end generate;

    g_EgroupConfig: process
    begin
        for i in 0 to 11 loop
            register_map_40_control.DECODING_EGROUP_CTRL(i)(0).REVERSE_ELINKS <= x"00"; -- enables bit reversing for the elink in the given epath
            register_map_40_control.DECODING_EGROUP_CTRL(i)(0).PATH_ENCODING  <= x"11111111"; -- Encoding for every EPATH, 8 EPATHS per EGROUP 0: direct mode, 1: 8b10b mode, 2: HDLC mode
            register_map_40_control.DECODING_EGROUP_CTRL(i)(0).EPATH_WIDTH    <= "000"; --2b elinks
            register_map_40_control.DECODING_EGROUP_CTRL(i)(0).EPATH_ENA      <= "11111111"; --2b elinks

            register_map_40_control.DECODING_EGROUP_CTRL(i)(1).REVERSE_ELINKS <= x"00"; -- enables bit reversing for the elink in the given epath
            register_map_40_control.DECODING_EGROUP_CTRL(i)(1).PATH_ENCODING  <= x"11111111"; -- Encoding for every EPATH, 8 EPATHS per EGROUP 0: direct mode, 1: 8b10b mode, 2: HDLC mode
            register_map_40_control.DECODING_EGROUP_CTRL(i)(1).EPATH_WIDTH    <= "001"; --4b elinks
            register_map_40_control.DECODING_EGROUP_CTRL(i)(1).EPATH_ENA      <= "01010101"; --4b elinks

            register_map_40_control.DECODING_EGROUP_CTRL(i)(2).REVERSE_ELINKS <= x"00"; -- enables bit reversing for the elink in the given epath
            register_map_40_control.DECODING_EGROUP_CTRL(i)(2).PATH_ENCODING  <= x"11111111"; -- Encoding for every EPATH, 8 EPATHS per EGROUP 0: direct mode, 1: 8b10b mode, 2: HDLC mode
            register_map_40_control.DECODING_EGROUP_CTRL(i)(2).EPATH_WIDTH    <= "010"; --8b elinks
            register_map_40_control.DECODING_EGROUP_CTRL(i)(2).EPATH_ENA      <= "00010001"; --8b elinks

            register_map_40_control.DECODING_EGROUP_CTRL(i)(3).REVERSE_ELINKS <= x"00"; -- enables bit reversing for the elink in the given epath
            register_map_40_control.DECODING_EGROUP_CTRL(i)(3).PATH_ENCODING  <= x"11111111"; -- Encoding for every EPATH, 8 EPATHS per EGROUP 0: direct mode, 1: 8b10b mode, 2: HDLC mode
            register_map_40_control.DECODING_EGROUP_CTRL(i)(3).EPATH_WIDTH    <= "011"; --16b elinks
            register_map_40_control.DECODING_EGROUP_CTRL(i)(3).EPATH_ENA      <= "00000001"; --16b elinks

            register_map_40_control.DECODING_EGROUP_CTRL(i)(4).REVERSE_ELINKS <= x"00"; -- enables bit reversing for the elink in the given epath
            register_map_40_control.DECODING_EGROUP_CTRL(i)(4).PATH_ENCODING  <= x"11111111"; -- Encoding for every EPATH, 8 EPATHS per EGROUP 0: direct mode, 1: 8b10b mode, 2: HDLC mode
            register_map_40_control.DECODING_EGROUP_CTRL(i)(4).EPATH_WIDTH    <= "000"; --2b elinks
            register_map_40_control.DECODING_EGROUP_CTRL(i)(4).EPATH_ENA      <= "11111111"; --2b elinks
        end loop;
        wait;
    end process;




    RXUSRCLK <= (others => clk240);

    reset_proc: process
    begin
        reset <= '1';
        wait for C_CLK40_PERIOD*15;
        reset <= '0';
        wait;
    end process;

    rst_hw      <= reset;
    rst_soft_40 <= reset;

    register_map_40_control.FE_EMU_ENA.EMU_TOHOST <= "1";
    register_map_40_control.FE_EMU_ENA.EMU_TOFRONTEND <= "1";
    register_map_40_control.FE_EMU_CONFIG.WE <= (others => '0');
    register_map_40_control.FE_EMU_CONFIG.WRADDR   <= (others => '0');
    register_map_40_control.FE_EMU_CONFIG.WRDATA   <= (others => '0');

    gbtEmuToHost0: entity work.GBTdataEmulator
        generic map(
            EMU_DIRECTION => "ToHost",
            FIRMWARE_MODE => FIRMWARE_MODE,
            MEM_DEPTH => 16384,
            LPGBT_TOHOST_WIDTH => 32,
            LPGBT_TOFE_WIDTH => 8
        )
        port map(
            clk40 => clk40,
            wrclk => clk40,
            rst_hw => rst_hw,
            rst_soft => rst_soft_40,
            xoff => '0',
            register_map_control => register_map_40_control,
            GBTdata => emuToHost_GBTdata,
            lpGBTdataToFE => open,
            lpGBTdataToHost => open,
            lpGBTECdata => open,
            lpGBTICdata => open,
            GBTlinkValid => emuToHost_GBTlinkValid);

    gbtoSelToHost0: entity work.GBT_fanout_selector
        generic map(
            GBT_NUM => GBT_NUM/ENDPOINTS
        )
        port map(
            GBTDataIn => GBT_UPLINK_USER_DATA((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1), --: in     array_120b(0 to GBT_NUM-1);
            lpGBTDataToFEIn => (others => (others => '0')),
            lpGBTDataToHostIn => (others => (others => '0')),
            lpGBTECDataIn => (others => (others => '0')),
            lpGBTICDataIn => (others => (others => '0')),
            GBTLinkValidIn => LinkAligned(((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1 downto (GBT_NUM/ENDPOINTS)*pcie_endpoint), --: in     std_logic_vector(GBT_NUM-1 downto 0);
            EMU_GBTlinkValidIn => emuToHost_GBTlinkValid, --: in     std_logic;
            EMU_GBTDataIn => emuToHost_GBTdata, --: in     std_logic_vector(119 downto 0);
            EMU_lpGBTDataToFEIn => (others => '0'),
            EMU_lpGBTDataToHostIn => (others => '0'),
            EMU_lpGBTECDataIn => (others => '0'),
            EMU_lpGBTICDataIn => (others => '0'),
            GBTDataOut => GBT_UPLINK_USER_DATA_FOSEL, --: out    array_120b(0 to (GBT_NUM-1));
            lpGBTDataToFEOut => open,
            lpGBTDataToHostOut => open,
            lpGBTECDataOut => open,
            lpGBTICDataOut => open,
            GBTLinkValidOut => LinkAligned_FOSEL, --: out    std_logic_vector(0 to (GBT_NUM-1));
            clk40 => clk40, --: in     std_logic;
            sel => (others => '1'));

    decoding0: entity work.decoding
        generic map(
            CARD_TYPE => 712,
            GBT_NUM => GBT_NUM/ENDPOINTS,
            FIRMWARE_MODE => FIRMWARE_MODE,
            STREAMS_TOHOST => STREAMS_TOHOST,
            BLOCKSIZE => BLOCKSIZE,
            LOCK_PERIOD => LOCK_PERIOD,
            IncludeDecodingEpath2_HDLC => IncludeDecodingEpath2_HDLC,
            IncludeDecodingEpath2_8b10b => IncludeDecodingEpath2_8b10b,
            IncludeDecodingEpath4_8b10b => IncludeDecodingEpath4_8b10b,
            IncludeDecodingEpath8_8b10b => IncludeDecodingEpath8_8b10b,
            IncludeDecodingEpath16_8b10b => IncludeDecodingEpath16_8b10b,
            IncludeDecodingEpath32_8b10b => IncludeDecodingEpath32_8b10b,
            IncludeDirectDecoding => "0000000",
            RD53Version => "A",
            PCIE_ENDPOINT => 0,
            VERSAL => false,
            AddFULLMODEForDUNE => false
        )
        Port map(
            RXUSRCLK => RXUSRCLK(((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1 downto (GBT_NUM/ENDPOINTS)*pcie_endpoint), --: in  std_logic_vector(GBT_NUM-1 downto 0);
            FULL_UPLINK_USER_DATA => GTH_FM_RX_33b_out((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1), --: in  txrx33b_type(0 to (GBT_NUM-1)); --Full mode data input
            GBT_UPLINK_USER_DATA => GBT_UPLINK_USER_DATA_FOSEL, -- : in  array_120b(GBT_NUM-1 downto 0);   --GBT data input
            lpGBT_UPLINK_USER_DATA => (others => (others => '0')), -- TODO: connect lpGBT data : in  txrx230b_type(GBT_NUM-1 downto 0); --lpGBT data input
            lpGBT_UPLINK_EC_DATA => (others => (others => '0')), -- TODO: connect lpGBT data : in  txrx2b_type(GBT_NUM-1 downto 0);   --lpGBT EC data input
            lpGBT_UPLINK_IC_DATA => (others => (others => '0')), -- TODO: connect lpGBT data : in  txrx2b_type(GBT_NUM-1 downto 0);   --lpGBT IC data input
            LinkAligned => LinkAligned_FOSEL, --: in  std_logic_vector(GBT_NUM-1 downto 0);
            clk160 => clk160, --: in  std_logic;
            clk240 => clk240,
            clk250 => clk250, --: in  std_logic;
            clk40 => clk40, --: in  std_logic;
            clk365 => '0',
            aclk_out => decoding_aclk, --: out std_logic;
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            m_axis => decoding_axis,
            m_axis_tready => decoding_axis_tready,
            m_axis_prog_empty => decoding_axis_prog_empty,
            m_axis_noSC => open,
            m_axis_noSC_tready => (others => (others => '1')),
            m_axis_noSC_prog_empty => open,
            TTC_ToHost_Data_in => TTC_ToHost_Data_in,
            TTCin => TTC_zero,
            FE_BUSY_out => open,
            ElinkBusyIn => ElinkBusyIn,
            DmaBusyIn => DmaBusyIn,
            FifoBusyIn => FifoBusyIn,
            BusySumIn => BusySumIn,
            m_axis_aux => decoding_axis_aux,
            m_axis_aux_prog_empty => decoding_axis_aux_prog_empty,
            m_axis_aux_tready => decoding_axis_aux_tready,
            register_map_control => register_map_40_control,
            register_map_decoding_monitor => register_map_decoding_monitor,
            Interlaken_RX_Data_In         => Interlaken_RX_Data(pcie_endpoint*(GBT_NUM/ENDPOINTS) to ((pcie_endpoint+1)*(GBT_NUM/ENDPOINTS))-1),
            Interlaken_RX_Datavalid       => Interlaken_RX_Datavalid(((pcie_endpoint+1)*(GBT_NUM/ENDPOINTS))-1 downto pcie_endpoint*(GBT_NUM/ENDPOINTS)),
            Interlaken_RX_Gearboxslip     => open,
            Interlaken_Decoder_Aligned_out => open,
            m_axis64                      => open,
            m_axis64_tready               => (others => '0'),
            m_axis64_prog_empty           => open,
            toHost_axis64_aclk_out        => open
        );

    resetmgr0: entity work.CRresetManager
        port map(
            clk40         => clk40,
            rst           => reset,
            clk40_stable  => clk40_stable,
            cr_rst        => daq_reset,
            cr_fifo_flush => daq_fifo_flush
        );


    g_URAM: if USE_URAM generate
        toHostFifo_wr_clk <= decoding_aclk;
    else generate --BRAM FIFO's in Virtex7 and Kintex Ultrascale
        g_toHostFifo_wr_clk_160: if FIRMWARE_MODE = FIRMWARE_MODE_GBT or FIRMWARE_MODE = FIRMWARE_MODE_LTDB or FIRMWARE_MODE = FIRMWARE_MODE_LPGBT generate
            toHostFifo_wr_clk <= clk160;
        else generate
            toHostFifo_wr_clk <= clk250;
        end generate;
    end generate;

    crt0: entity work.CRToHost
        generic map(
            NUMBER_OF_DESCRIPTORS => 2,
            NUMBER_OF_INTERRUPTS => 8,
            LINK_NUM => GBT_NUM/ENDPOINTS,
            LINK_CONFIG => (0 to GBT_NUM-1 => 0),
            toHostTimeoutBitn => 16,
            STREAMS_TOHOST => STREAMS_TOHOST,
            BLOCKSIZE => BLOCKSIZE,
            DATA_WIDTH => DATA_WIDTH,
            FIRMWARE_MODE => FIRMWARE_MODE,
            USE_URAM => USE_URAM)
        port map(
            clk40 => clk40,
            aclk_tohost => decoding_aclk,
            aclk64_tohost => '0',
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            register_map_control => register_map_40_control,
            register_map_xoff_monitor => register_map_xoff_monitor,
            register_map_crtohost_monitor => register_map_crtohost_monitor,
            interrupt_call => open,
            xoff_out => open,
            s_axis => fanout_sel_axis,
            s_axis_tready => fanout_sel_axis_tready,
            s_axis_prog_empty => decoding_axis_prog_empty,
            s_axis_aux => decoding_axis_aux,
            s_axis_aux_tready => decoding_axis_aux_tready,
            s_axis_aux_prog_empty => decoding_axis_aux_prog_empty,
            s_axis64 => (others => axis_64_zero_c),
            s_axis64_tready => open,
            s_axis64_prog_empty => (others => '1'),
            dma_enable_in => (others => '1'),
            toHostFifo_din => toHostFifo_din,
            toHostFifo_wr_en => toHostFifo_wr_en,
            toHostFifo_prog_full => toHostFifo_prog_full,
            toHostFifo_wr_clk => toHostFifo_wr_clk,
            toHostFifo_rst => toHostFifo_rst);

    g_DisableFullModeEmulator: if FIRMWARE_MODE /= FIRMWARE_MODE_FULL generate
        fanout_sel_axis <= decoding_axis;
        decoding_axis_tready <= fanout_sel_axis_tready;
    end generate;


    clock_generator(clk40, clock_ena, C_CLK40_PERIOD, "40 MHz clock");
    clock_generator(clk160, clock_ena, C_CLK160_PERIOD, "160 MHz clock");
    clock_generator(clk240, clock_ena, C_CLK240_PERIOD, "240 MHz clock");
    clock_generator(clk250, clock_ena, C_CLK250_PERIOD, "250 MHz clock");


    sequencer : process
    begin
        clock_ena <= false;
        clk40_stable <= '0';
        -- Print the configuration to the log
        --wait for 1 ns;
        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);

        --enable_log_msg(ALL_MESSAGES);
        disable_log_msg(ALL_MESSAGES, NON_QUIET, C_SCOPE);
        enable_log_msg(ID_SEQUENCER, NON_QUIET, C_SCOPE);
        disable_log_msg(ID_POS_ACK, NON_QUIET, C_SCOPE);


        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "----- initializing test bench       ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        -- Wait for UVVM to finish initialization
        --await_uvvm_initialization(VOID);

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "-----    end of initialisation        ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        wait for (10*C_CLK40_PERIOD);
        clock_ena <= true;
        clk40_stable <= '1';

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "-- test bench sequence starts here      ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);


        log(ID_LOG_HDR, "Starting simulation of TB for EGROUP using VVCs", C_SCOPE);
        ------------------------------------------------------------

        await_value(checker_done, '1', 0 ns, 1000 ms, TB_ERROR, "wait for all tests to finish", C_SCOPE);
        enable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        enable_log_msg(ID_SEQUENCER);
        enable_log_msg(ID_UVVM_SEND_CMD);

        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely
    --
    end process; -- sequencer

    sink0: entity work.FELIXDataSink
        generic map(
            NUMBER_OF_DESCRIPTORS           => NUMBER_OF_DESCRIPTORS,
            BLOCKSIZE                       => BLOCKSIZE,
            GBT_NUM                         => GBT_NUM,
            FIRMWARE_MODE                   => FIRMWARE_MODE,
            DATA_WIDTH                      => DATA_WIDTH,
            SIM_NUMBER_OF_BLOCKS            => SIM_NUMBER_OF_BLOCKS,
            APPLY_BACKPRESSURE_AFTER_BLOCKS => SIM_NUMBER_OF_BLOCKS/10
        )
        port map(
            checker_done         => checker_done,
            toHostFifo_din       => toHostFifo_din,
            toHostFifo_wr_en     => toHostFifo_wr_en,
            toHostFifo_prog_full => toHostFifo_prog_full,
            toHostFifo_wr_clk    => toHostFifo_wr_clk,
            reset                => reset,
            start_check          => start_check,
            Trunc                => Trunc,
            TrailerError         => TrailerError,
            CRCError             => CRCError,
            ChunkLengthError     => ChunkLengthError,
            register_map_control => register_map_40_control
        );

end architecture;


--ricardo luz, argonne
--based on simulation/FELIG/felig_sim_top_bnl712.vhd

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.numeric_std_unsigned.all;
    use IEEE.std_logic_textio.all;

    use std.env.all;
    use std.textio.all;

    use work.FELIX_package.all;
    use work.pcie_package.all;
    use work.axi_stream_package.all;
    use work.type_lib.ALL;

library xpm;
    use xpm.vcomponents.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

entity FELIGemulator_tb is
    generic(
        use_vunit       : boolean := false;
        FIRMWARE_MODE   : integer := 11 --6 GBT, 11 LPGBT
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end FELIGemulator_tb;

architecture tb of FELIGemulator_tb is
    function FM_TO_STREAMS (FIRMWARE_MODE:integer)
        return integer is
    begin
        if FIRMWARE_MODE = 11 then
            return 30;
        elsif FIRMWARE_MODE = 6 then
            return 42;
        else
            return 0;
        end if;
    end function;

    constant GBT_NUM            : integer := 1;
    constant NUMELINKmax        : integer := 112; --name number of epahts per egroup (theoretically 2b epath * 112=224, but never happens)
    constant NUMEGROUPmax       : integer := 7; --name number of egroups
    constant STREAMS_TOHOST     : integer := FM_TO_STREAMS(FIRMWARE_MODE);

    constant clk40_period       : time      := 25 ns;
    signal clk_en               : boolean   := false;
    signal clk40                : std_logic;
    signal clk40_tmp            : std_logic := '0';
    signal clk240               : std_logic;
    signal clk240_tmp           : std_logic := '0';
    signal clk160               : std_logic;
    signal clk160_tmp           : std_logic := '0';
    signal clk320               : std_logic;
    signal clk320_tmp           : std_logic := '0';
    signal clk_prcss            : std_logic := '0';
    signal clkTX                : std_logic_vector(GBT_NUM-1 downto 0);
    signal clkRX                : std_logic_vector(GBT_NUM-1 downto 0);
    signal cnt_div            : std_logic_vector(1 downto 0) := (others => '0');
    signal cnt_div_max        : std_logic_vector(1 downto 0) := (others => '0');

    signal reset                : std_logic;
    signal reset_d              : std_logic;

    signal lane_control         : array_of_lane_control_type(GBT_NUM-1 downto 0);
    signal lane_monitor         : array_of_lane_monitor_type(GBT_NUM-1 downto 0);
    signal select_random        : std_logic_vector(0 downto 0) := "0";

    signal link_tx_data         : array_228b(0 to GBT_NUM-1);
    --signal link_rx_data         : txrx120b_type(0 to GBT_NUM-1);
    signal link_tx_valid        : std_logic_vector(0 to GBT_NUM-1);
    signal link_tx_flag         : std_logic_vector(GBT_NUM-1 downto 0);
    --signal link_rx_flag         : std_logic_vector(GBT_NUM-1 downto 0);

    signal l1id                 : std_logic_vector(15 downto 0) := (others => '0');
    signal l1id_buf             : std_logic_vector(15 downto 0) := (others => '0');
    signal l1a, l1a_d           : std_logic := '0';
    signal l1a_int_trigger      : std_logic := '0';
    signal l1a_int_trigger_d    : std_logic := '0';

    signal m_axis               : axis_32_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1);
    signal m_axis_aclk          : std_logic;
    type array_2d_8b is array (natural range <>, natural range <>) of std_logic_vector(7 downto 0);
    type array_2d_3b is array (natural range <>, natural range <>) of std_logic_vector(2 downto 0);
    type array_2d_1b is array (natural range <>, natural range <>) of std_logic;
    signal epath_enabled        : array_2d_1b(GBT_NUM-1 downto 0, STREAMS_TOHOST-3 downto 0);
    signal epath_aligned        : array_2d_1b(GBT_NUM-1 downto 0, STREAMS_TOHOST-3 downto 0);
    signal epath_width          : array_2d_3b(GBT_NUM-1 downto 0, STREAMS_TOHOST-3 downto 0);
    signal data_good            : array_2d_1b(GBT_NUM-1 downto 0, STREAMS_TOHOST-3 downto 0);

    signal epath_enabled_slv    : std_logic_vector((STREAMS_TOHOST-2)*GBT_NUM-1 downto 0);
    signal epath_aligned_slv    : std_logic_vector((STREAMS_TOHOST-2)*GBT_NUM-1 downto 0);
    signal epath_aligned_d_slv  : std_logic_vector((STREAMS_TOHOST-2)*GBT_NUM-1 downto 0) := (others => '0');
    signal epath_aligned_dd_slv : std_logic_vector((STREAMS_TOHOST-2)*GBT_NUM-1 downto 0) := (others => '0');
    signal data_good_slv        : std_logic_vector((STREAMS_TOHOST-2)*GBT_NUM-1 downto 0) := (others => '0');
    signal chunk_size           : array_2d_8b(GBT_NUM-1 downto 0, STREAMS_TOHOST-3 downto 0);

    signal count_final          : std_logic_vector(4 downto 0) := (others => '0');
    signal enabled_aligned      : array_2b(15 downto 0) := (others => (others => '0'));
    signal timeout              : boolean := false;
begin

    --clocks

    clk160_tmp <= not clk160_tmp after clk40_period/8; --160 MHZ
    clk160     <= clk160_tmp     when  clk_en else '0';
    clk240_tmp <= not clk240_tmp after clk40_period/12; --240 MHZ
    clk240     <= clk240_tmp     when  clk_en else '0';
    clk320_tmp <= not clk320_tmp after clk40_period/16; --320 MHZ
    clk320     <= clk320_tmp     when  clk_en else '0';

    cnt_div_max <= "11" when FIRMWARE_MODE = 11 else "10" when FIRMWARE_MODE = 6 else "00";
    clk_prcss   <= clk320 when FIRMWARE_MODE = 11 else clk240 when FIRMWARE_MODE = 6 else '0';
    clk40       <= clk40_tmp;
    process(clk_prcss) --clk40 needs to be in sync with clk240 for GBT and clk320 for LPGBT
    begin
        if rising_edge(clk_prcss) then
            if cnt_div = cnt_div_max then
                cnt_div <= "00";
                clk40_tmp <= not clk40_tmp;
            else
                cnt_div <= cnt_div + "01";
            end if;
        end if;
    end process;


    --Link Configuration
    g_lane_control : for link in 0 to GBT_NUM-1 generate
        signal elink_output_width   : array_of_slv_2_0(NUMELINKmax-1 downto 0) := (others => "111");
        signal elink_enable         : std_logic_vector(NUMELINKmax-1 downto 0) := (others => '1');
        signal data_format          : std_logic_vector(1 downto 0);
    begin
        lane_control(link).global.framegen_reset               <= '0';
        lane_control(link).global.elink_sync                   <= '0';
        lane_control(link).global.framegen_data_select         <= '0';
        lane_control(link).global.emu_data_select              <= '1';
        lane_control(link).global.l1a_source                   <= '0';
        lane_control(link).global.loopback_fifo_delay          <= "00010";
        lane_control(link).global.loopback_fifo_reset          <= '0';
        lane_control(link).global.a_ch_bit_sel                 <= "0000001";
        lane_control(link).global.b_ch_bit_sel                 <= "0000010";
        lane_control(link).global.MSB                          <= '1';
        lane_control(link).global.FEC                          <= '0';
        lane_control(link).global.DATARATE                     <= '1';
        lane_control(link).global.aligned                      <= '1';
        lane_control(link).global.l1a_max_count                <= x"0000190";

        lane_control(link).fmemu_random.FMEMU_RANDOM_CONTROL.SELECT_RANDOM <= select_random;
        lane_control(link).fmemu_random.FMEMU_RANDOM_CONTROL.SEED          <= "1000000000"; --0x200
        lane_control(link).fmemu_random.FMEMU_RANDOM_CONTROL.POLYNOMIAL    <= "1001000000"; --0x240
        lane_control(link).fmemu_random.FMEMU_RANDOM_RAM_ADDR              <= "0000000000";
        lane_control(link).fmemu_random.FMEMU_RANDOM_RAM.WE                <= "0";
        lane_control(link).fmemu_random.FMEMU_RANDOM_RAM.CHANNEL_SELECT    <= (others=>'0');
        lane_control(link).fmemu_random.FMEMU_RANDOM_RAM.DATA              <= (others=>'0');

        lane_control(link).emulator(0).output_width  <= "010" when FIRMWARE_MODE = 11 else "000" when FIRMWARE_MODE = 6 else "000";-- 8b 2b
        lane_control(link).emulator(1).output_width  <= "010" when FIRMWARE_MODE = 11 else "000" when FIRMWARE_MODE = 6 else "000";-- 8b 2b
        lane_control(link).emulator(2).output_width  <= "011" when FIRMWARE_MODE = 11 else "001" when FIRMWARE_MODE = 6 else "000";--16b 4b
        lane_control(link).emulator(3).output_width  <= "011" when FIRMWARE_MODE = 11 else "001" when FIRMWARE_MODE = 6 else "000";--16b 4b
        lane_control(link).emulator(4).output_width  <= "100" when FIRMWARE_MODE = 11 else "010" when FIRMWARE_MODE = 6 else "000";--32b 8b
        lane_control(link).emulator(5).output_width  <= "100";--32b
        lane_control(link).emulator(6).output_width  <= "100";--32b

        data_format <= "01";

        g_emu_control : for egroup in lane_control(link).emulator'range generate
            lane_control(link).emulator(egroup).pattern_select   <= "00";
            lane_control(link).emulator(egroup).data_format      <= data_format;
            lane_control(link).emulator(egroup).sw_busy          <= '0';
            lane_control(link).emulator(egroup).reset            <= '0';--emu_reset;
            lane_control(link).emulator(egroup).chunk_length     <= X"003C"; --60+8
            lane_control(link).emulator(egroup).userdata         <= X"ABCD";

            less_that_five : if egroup<5 generate
                elink_enable(egroup*16+7 downto egroup*16)    <= X"FF" when (lane_control(link).emulator(egroup).output_width <= "000") else
                                                                 X"55" when (lane_control(link).emulator(egroup).output_width <= "001") else
                                                                 X"11" when (lane_control(link).emulator(egroup).output_width <= "010") else
                                                                 X"01" when (lane_control(link).emulator(egroup).output_width <= "011") else
                                                                 X"01" when (lane_control(link).emulator(egroup).output_width <= "100");
                elink_enable(egroup*16+15 downto egroup*16+8) <= X"FF" when (lane_control(link).emulator(egroup).output_width <= "000" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1') else
                                                                 X"55" when (lane_control(link).emulator(egroup).output_width <= "001" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1') else
                                                                 X"11" when (lane_control(link).emulator(egroup).output_width <= "010" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1') else
                                                                 X"01" when (lane_control(link).emulator(egroup).output_width <= "011" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1') else
                                                                 X"00";
            end generate less_that_five;
            five : if egroup=5 generate
                elink_enable(egroup*16+7 downto egroup*16)    <= X"FF" when (lane_control(link).emulator(egroup).output_width <= "000" and FIRMWARE_MODE = 11) else
                                                                 X"55" when (lane_control(link).emulator(egroup).output_width <= "001" and FIRMWARE_MODE = 11) else
                                                                 X"11" when (lane_control(link).emulator(egroup).output_width <= "010" and FIRMWARE_MODE = 11) else
                                                                 X"01" when (lane_control(link).emulator(egroup).output_width <= "011" and FIRMWARE_MODE = 11) else
                                                                 X"01" when (lane_control(link).emulator(egroup).output_width <= "100" and FIRMWARE_MODE = 11) else
                                                                 X"00";
                elink_enable(egroup*16+15 downto egroup*16+8) <= X"FF" when (lane_control(link).emulator(egroup).output_width <= "000" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1') else
                                                                 X"55" when (lane_control(link).emulator(egroup).output_width <= "001" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1') else
                                                                 X"11" when (lane_control(link).emulator(egroup).output_width <= "010" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1') else
                                                                 X"01" when (lane_control(link).emulator(egroup).output_width <= "011" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1') else
                                                                 X"00";
            end generate five;
            six : if egroup=6 generate
                elink_enable(egroup*16+7 downto egroup*16)    <= X"FF" when (lane_control(link).emulator(egroup).output_width <= "000" and FIRMWARE_MODE = 11 and lane_control(link).global.FEC = '0') else
                                                                 X"55" when (lane_control(link).emulator(egroup).output_width <= "001" and FIRMWARE_MODE = 11 and lane_control(link).global.FEC = '0') else
                                                                 X"11" when (lane_control(link).emulator(egroup).output_width <= "010" and FIRMWARE_MODE = 11 and lane_control(link).global.FEC = '0') else
                                                                 X"01" when (lane_control(link).emulator(egroup).output_width <= "011" and FIRMWARE_MODE = 11 and lane_control(link).global.FEC = '0') else
                                                                 X"01" when (lane_control(link).emulator(egroup).output_width <= "100" and FIRMWARE_MODE = 11 and lane_control(link).global.FEC = '0') else
                                                                 X"00";
                elink_enable(egroup*16+15 downto egroup*16+8) <= X"FF" when (lane_control(link).emulator(egroup).output_width <= "000" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1' and lane_control(link).global.FEC = '0') else
                                                                 X"55" when (lane_control(link).emulator(egroup).output_width <= "001" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1' and lane_control(link).global.FEC = '0') else
                                                                 X"11" when (lane_control(link).emulator(egroup).output_width <= "010" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1' and lane_control(link).global.FEC = '0') else
                                                                 X"01" when (lane_control(link).emulator(egroup).output_width <= "011" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1' and lane_control(link).global.FEC = '0') else
                                                                 X"00";
            end generate six;
            width : for k in 0 to 7 generate
                elink_output_width(egroup*16+k)   <= lane_control(link).emulator(egroup).output_width;
                elink_output_width(egroup*16+k+8) <= lane_control(link).emulator(egroup).output_width when (FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1') else "111";
            end generate width;

        end generate g_emu_control;

        g_elink_control : for epath in lane_control(link).elink'range generate
            lane_control(link).elink(epath).input_width    <= '0' when data_format = "00" else '1';
            lane_control(link).elink(epath).endian_mode    <= '0';
            lane_control(link).elink(epath).enable         <= elink_enable(epath);
            lane_control(link).elink(epath).output_width   <= elink_output_width(epath);
        end generate g_elink_control;

    end generate g_lane_control;

    --FELIG emulator wrapper

    g_lw_signals : for i in 0 to GBT_NUM-1 generate
        signal count_max    : std_logic_vector(2 downto 0);
        signal count        : std_logic_vector(2 downto 0) := "000";
    begin
        clkTX(i)    <= clk320   when FIRMWARE_MODE = 11 else clk240 when FIRMWARE_MODE = 6 else '0';
        count_max   <= "111"    when FIRMWARE_MODE = 11 else "101" when FIRMWARE_MODE = 6 else "000";
        process(clkTX(i))
        begin
            if rising_edge(clkTX(i)) then
                if count = count_max then
                    count <= "000";
                    link_tx_flag(i) <= '1';
                else
                    count <= count + "001";
                    link_tx_flag(i) <= '0';
                end if;
            end if;
        end process;
    end generate g_lw_signals;

    clkRX <= clkTX;
    --link_rx_flag <= link_tx_flag;

    FELIG_emu : entity work.EmulatorWrapper
        generic map(
            GBT_NUM                     => GBT_NUM,
            NUMELINKmax                 => NUMELINKmax,
            NUMEGROUPmax                => NUMEGROUPmax,
            FIRMWARE_MODE               => FIRMWARE_MODE)
        port map (
            clk40                       => clk40,
            gt_txusrclk_in              => clkTX,
            gt_rxusrclk_in              => clkRX,
            link_tx_data_228b_array_out => link_tx_data,
            data_ready_tx_out           => link_tx_valid,
            link_rx_data_120b_array_in  => (others => (others => '0')),
            link_tx_flag_in             => link_tx_flag,
            link_rx_flag_in             => (others => '0'),--link_rx_flag,
            l1a_int_trigger_out         => l1a_int_trigger,
            lane_control                => lane_control,
            lane_monitor                => lane_monitor
        );

    l1id <= lane_monitor(0).global.l1a_id(15 downto 0);
    process(clk40)
    begin
        if rising_edge(clk40) then
            l1id_buf <= l1id;
            if l1id_buf /= l1id then
                l1a <= '1';
            else
                l1a <= '0';
            end if;
        end if;
    end process;

    --decoding lpgbt
    g_dec_LPGBT: if FIRMWARE_MODE = 11 generate
        signal lpGBT_UPLINK_USER_DATA   : array_224b(0 to GBT_NUM-1);
        signal AlignmentPulseAlign      : std_logic;
        signal AlignmentPulseDeAlign    : std_logic;
    begin
        m_axis_aclk <= clk240;
        pulsegen0: entity work.AlignmentPulseGen
            generic map(
                MAX_VAL_DEALIGN => 2048, --2048 bytes at 8b10b / 80 Mb/s elink.
                MAX_VAL_ALIGN => 20 --2 bytes at 8b10b / 80 Mb/s elink.
            )
            port map(
                clk40 => clk40,
                AlignmentPulseAlign => AlignmentPulseAlign,
                AlignmentPulseDeAlign => AlignmentPulseDeAlign
            );
        g_Links : for link in 0 to GBT_NUM-1 generate
        begin
            process(clk40)
            begin
                if rising_edge(clk40) then
                    lpGBT_UPLINK_USER_DATA(link) <= link_tx_data(link)(223 downto 0);
                end if;
            end process;
            g_Egroups: for egroup in 0 to 6 generate
                signal ElinkWidth   : std_logic_vector(2 downto 0);
                signal PathEnable   : std_logic_vector(3 downto 0);
                signal ElinkAligned : std_logic_vector(3 downto 0);
                signal m_axis_s     : axis_32_array_type(0 to 3);
            begin
                g_axisindex: for i in 0 to 3 generate
                    m_axis(link,egroup*4+i) <= m_axis_s(i);
                end generate;
                ElinkWidth <= lane_control(link).emulator(egroup).output_width;
                PathEnable <= "1111" when ElinkWidth = "010" else
                              "0101" when ElinkWidth = "011" else
                              "0001" when ElinkWidth = "100" else
                              "0000";

                g_epaths : for epath in 0 to 3 generate
                    epath_width(link,egroup*4+epath)   <= lane_control(link).emulator(egroup).output_width;
                    epath_enabled(link,egroup*4+epath) <= PathEnable(epath);
                    epath_aligned(link,egroup*4+epath) <= ElinkAligned(epath);
                end generate;
                --! Instantiate one Egroup.
                eGroup0: entity work.DecEgroup_8b10b
                    generic map(
                        BLOCKSIZE           => 1024,
                        Support32bWidth     => '1',
                        Support16bWidth     => '1',
                        Support8bWidth      => '1',
                        IncludeElinks       => "1111",
                        VERSAL              => false
                    )
                    port map(
                        clk40                   => clk40,
                        daq_reset               => reset,
                        daq_fifo_flush          => '0',
                        tick_1us_i              => '0',
                        dm_start_mask_i         => x"0000",
                        dm_capture_length_i     => x"00",
                        DataIn                  => lpGBT_UPLINK_USER_DATA(link)(egroup*32+31 downto egroup*32),
                        EgroupCounterClear      => '0',
                        EnableIn                => PathEnable,
                        LinkAligned             => lane_control(link).global.aligned,
                        ElinkWidth              => ElinkWidth,
                        AlignmentPulseAlign     => AlignmentPulseAlign,
                        AlignmentPulseDeAlign   => AlignmentPulseDeAlign,
                        AutoRealign             => '1',
                        RealignmentEvent        => open,
                        PathEncoding            => x"1111", --"0001" per epath. All 8b10b encoded
                        ElinkAligned            => ElinkAligned,
                        DecodingErrors          => open,
                        MsbFirst                => '1',
                        ReverseInputBits        => (others => '0'),
                        HGTD_ALTIROC_DECODING   => '0',
                        FE_BUSY_out             => open,
                        m_axis                  => m_axis_s,
                        m_axis_tready           => (others => '1'),
                        m_axis_aclk             => m_axis_aclk,
                        m_axis_prog_empty       => open
                    );
            end generate g_Egroups;
        end generate g_Links;
    end generate g_dec_LPGBT;

    g_dec_GBT: if FIRMWARE_MODE = 6 generate
        type txrx80b_type                 is array (natural range <>) of std_logic_vector(79 downto 0);
        signal GBT_UPLINK_USER_DATA     : txrx80b_type(0 to GBT_NUM-1);
        signal AlignmentPulseAlign      : std_logic;
        signal AlignmentPulseDeAlign    : std_logic;
    begin
        m_axis_aclk <= clk160;
        pulsegen0: entity work.AlignmentPulseGen
            generic map(
                MAX_VAL_DEALIGN => 2048, --2048 bytes at 8b10b / 80 Mb/s elink.
                MAX_VAL_ALIGN => 20 --2 bytes at 8b10b / 80 Mb/s elink.
            )
            port map(
                clk40 => clk40,
                AlignmentPulseAlign => AlignmentPulseAlign,
                AlignmentPulseDeAlign => AlignmentPulseDeAlign
            );
        g_Links : for link in 0 to GBT_NUM-1 generate
        begin
            process(clk40)
            begin
                if rising_edge(clk40) then
                    GBT_UPLINK_USER_DATA(link) <= link_tx_data(link)(111 downto 32);
                end if;
            end process;
            g_Egroups: for egroup in 0 to 4 generate
                signal ReverseElinks            : std_logic_vector(7 downto 0);
                signal ElinkWidth               : std_logic_vector(2 downto 0);
                signal PathEnable               : std_logic_vector(7 downto 0);
                signal EnableEgroupTruncation   : std_logic;
                signal DecoderAligned           : std_logic_vector(7 downto 0);
                signal m_axis_s                 : axis_32_array_type(0 to 7);
            begin
                g_axisindex: for i in 0 to 7 generate
                    m_axis(link,egroup*8+i) <= m_axis_s(i);
                end generate;

                ReverseElinks           <= (others => '0');
                ElinkWidth              <= lane_control(link).emulator(egroup).output_width;
                PathEnable              <=  "11111111" when ElinkWidth = "000" else
                                           "01010101" when ElinkWidth = "001" else
                                           "00010001" when ElinkWidth = "010" else
                                           "00000001" when ElinkWidth = "011" else
                                           "00000000";
                EnableEgroupTruncation  <= '0'; --to_sl(register_map_control.DECODING_EGROUP_CTRL(link mod 12)(egroup).ENABLE_TRUNCATION);

                g_epaths : for epath in 0 to 7 generate
                    epath_width(link,egroup*8+epath)   <= lane_control(link).emulator(egroup).output_width;
                    epath_enabled(link,egroup*8+epath) <= PathEnable(epath);
                    epath_aligned(link,egroup*8+epath) <= DecoderAligned(epath);
                end generate;

                eGroup0: entity work.DecodingEgroupGBT
                    generic map(
                        INCLUDE_16b         => '1',
                        INCLUDE_8b          => '1',
                        INCLUDE_4b          => '1',
                        INCLUDE_2b          => '1',
                        INCLUDE_8b10b       => '1',
                        INCLUDE_HDLC        => '0',
                        INCLUDE_DIRECT      => '0',
                        BLOCKSIZE           => 1024,
                        USE_BUILT_IN_FIFO   => x"AA",
                        GENERATE_FEI4B      => false,
                        VERSAL              => false
                    )
                    port map(
                        clk40                   => clk40,
                        daq_reset               => reset,
                        daq_fifo_flush          => '0',
                        EpathEnable             => PathEnable,
                        EpathEncoding           => x"11111111", --"0001" per epath. All 8b10b encoded
                        ElinkWidth              => ElinkWidth,
                        MsbFirst                => '1',
                        ReverseInputBits        => ReverseElinks,
                        EnableTruncation        => EnableEgroupTruncation,
                        DecoderAligned          => DecoderAligned,
                        EGroupData              => GBT_UPLINK_USER_DATA(link)(egroup*16+15 downto egroup*16),
                        GBTAligned              => lane_control(link).global.aligned,
                        FE_BUSY_out             => open,
                        m_axis                  => m_axis_s,
                        m_axis_tready           => (others => '1'),
                        m_axis_aclk             => m_axis_aclk,
                        m_axis_prog_empty       => open,
                        AlignmentPulseAlign     => AlignmentPulseAlign,
                        AlignmentPulseDeAlign   => AlignmentPulseDeAlign,
                        AutoRealign             => '1',
                        RealignmentEvent        => open
                    );

            end generate g_Egroups;
        end generate g_Links;
    end generate g_dec_GBT;

    --checking data
    g_checking_data : for link in 0 to GBT_NUM-1 generate
        g_Streams: for stream in 0 to STREAMS_TOHOST-3 generate
            signal data         : std_logic_vector(31 downto 0);
            signal data10b      : std_logic_vector(9 downto 0);
            signal valid        : std_logic;
            signal last         : std_logic;
            signal keep         : std_logic_vector(3 downto 0);
            signal count8b      : std_logic_vector(1 downto 0) := "00";
            signal count_chunks : std_logic_vector(6 downto 0);
            signal checked      : std_logic := '0';
            signal check_words  : std_logic := '0';
            signal check_length : std_logic := '0';
            signal reset_v_ch   : std_logic := '0';
            signal width        : std_logic_vector(7 downto 0);
            signal exp_checks   : std_logic_vector(6 downto 0) := (others => '0');
            signal cnt_checks   : std_logic_vector(6 downto 0) := (others => '0');
            signal good         : std_logic := '0';
        begin
            data    <= m_axis(link,stream).tdata    when epath_aligned(link,stream) = '1' and epath_enabled(link,stream) = '1' else (others => '0');
            valid   <= m_axis(link,stream).tvalid   when epath_aligned(link,stream) = '1' and epath_enabled(link,stream) = '1' else '0';
            last    <= m_axis(link,stream).tlast    when epath_aligned(link,stream) = '1' and epath_enabled(link,stream) = '1' else '0';
            keep    <= m_axis(link,stream).tkeep    when epath_aligned(link,stream) = '1' and epath_enabled(link,stream) = '1' else (others => '0');
            width   <= "00000010" when epath_width(link,stream) = "000" else
                       "00000100" when epath_width(link,stream) = "001" else
                       "00001000" when epath_width(link,stream) = "010" else
                       "00010000" when epath_width(link,stream) = "011" else
                       "00100000" when epath_width(link,stream) = "100" else
                       "00000000";
            --data_good(link,stream) <= '0' when stream = 24  else good;
            data_good(link,stream) <= good;

            process(m_axis_aclk)
            begin
                if rising_edge(m_axis_aclk) then
                    if count8b = "00" and valid = '0' then
                        count8b <= count8b;
                    elsif count8b = "11" then
                        count8b <= "00";
                    else
                        count8b <= count8b + "01";
                    end if;
                end if;
            end process;
            data10b <= "11" & data( 7 downto  0) when count8b = "00" and valid = '1' and keep(0) = '1' and keep(3 downto 1) = "000" and last = '1' else
                       "01" & data( 7 downto  0) when count8b = "00" and valid = '1' and keep(0) = '1' else
                       "11" & data(15 downto  8) when count8b = "01" and keep(1) = '1' and keep(3 downto 2) = "00" and last = '1' else
                       "01" & data(15 downto  8) when count8b = "01" and keep(1) = '1' else
                       "11" & data(23 downto 16) when count8b = "01" and keep(2) = '1' and keep(3) = '0' and last = '1' else
                       "01" & data(23 downto 16) when count8b = "10" and keep(2) = '1'else
                       last & "1" & data(31 downto 24) when count8b = "11" and keep(3) = '1'else
                       (others=> '0');
            process(m_axis_aclk)
                variable v_chunks : integer := 0;
                variable size     : integer := 0;
                variable next_w   : integer := 0;
                variable next_b   : integer := 0;
            begin
                if rising_edge(m_axis_aclk) then
                    if data10b(9 downto 8) = "01" then
                        v_chunks := v_chunks + 1;
                        reset_v_ch <= '0';
                    --check_length <= '0';
                    elsif data10b(9 downto 8) = "11" then
                        v_chunks := v_chunks + 1;
                        reset_v_ch <= '1';
                        if v_chunks = size + 8 then --size + header
                            check_length <= '1';
                        --else
                        --check_length <= '0';
                        end if;
                    elsif data10b(9 downto 8) = "00" then
                        --check_length <= '0';
                        if reset_v_ch = '1' then
                            if v_chunks = 0 then
                                cnt_checks <= (others => '0');
                                reset_v_ch <= '0';
                            else
                                v_chunks := 0;
                            end if;
                        else
                            reset_v_ch <= '0';
                        end if;
                    end if;

                    count_chunks    <= std_logic_vector(to_unsigned(v_chunks, count_chunks'length));
                    exp_checks      <= std_logic_vector(to_unsigned(5 + size/2, exp_checks'length));

                    if v_chunks = 1 and data10b(7 downto 0) = x"aa" and data10b(8) = '1' then --checks first byte
                        check_words <= '1';
                    elsif v_chunks = 3 and data10b(8) = '1' then
                        size := to_integer(unsigned(data10b(7 downto 0)));
                        chunk_size(link,stream) <= data10b(7 downto 0);
                    elsif v_chunks = 5 and data10b(7 downto 0) = l1id(7 downto 0) and data10b(8) = '1' then
                        check_words <= '1';
                        next_w      := 10;
                        next_b      := size - 2;
                    elsif v_chunks = 6 and data10b(7 downto 0) = x"bb" and data10b(8) = '1'then
                        check_words <= '1';
                    elsif v_chunks = 7 and data10b(7 downto 0) = x"aa" and data10b(8) = '1'then
                        check_words <= '1';
                    elsif v_chunks = 8 and data10b(7 downto 0) = width and data10b(8) = '1' then
                        check_words <= '1';
                    elsif v_chunks = next_w and data10b(7 downto 0) = std_logic_vector(to_unsigned(next_b,8)) and data10b(8) = '1' then
                        check_words <= '1';
                        next_w      := next_w + 2;
                        if next_b > 1 then
                            next_b      := next_b - 2;
                        end if;
                    else
                        check_words <= '0';
                    end if;

                    if check_words = '1' then
                        cnt_checks <= cnt_checks + '1';
                    end if;

                    if cnt_checks = exp_checks and check_length = '1' then
                        check_length <= '0';
                        good <= '1';
                    end if;

                    if l1a = '1' then --new trigger
                        good <= '0';
                    end if;
                end if;
            end process;
        end generate g_Streams;
    end generate g_checking_data;

    process(m_axis_aclk)
        variable count_f    : integer := 0;
    begin
        if rising_edge(m_axis_aclk) then
            reset_d <= reset;
            l1a_d <= l1a;
            l1a_int_trigger_d <= l1a_int_trigger;
            if l1a_int_trigger = '1' and l1a_int_trigger_d = '0' then
                epath_aligned_d_slv <= epath_aligned_slv;
                epath_aligned_dd_slv <= epath_aligned_d_slv; --delaying again because l1a_int_trigger comes before l1a so comparison wouldnt work
            end if;
            if l1a_d = '0' and l1a = '1'then
                count_f := count_f + 1;
                if data_good_slv = epath_aligned_dd_slv and count_f <= 16 then
                    count_final <= count_final + '1';
                    if epath_aligned_dd_slv = epath_enabled_slv then
                        enabled_aligned(count_f-1) <= "11";
                    else
                        enabled_aligned(count_f-1) <= "01";
                    end if;
                end if;
            elsif l1a_d = '1' and l1a = '0' then
                log(ID_MONITOR,"enabled_aligned value is "&to_string(enabled_aligned(count_f-1),HEX)&". 0: data bad, 1: data good but not all decoders aligned, 3: data goot and aligned", C_SCOPE);
            end if;
        end if;
    end process;

    g_to_slv_link : for link in 0 to GBT_NUM-1 generate
        g_to_slv_stream: for stream in 0 to STREAMS_TOHOST-3 generate
            epath_enabled_slv(link*(STREAMS_TOHOST-2)+stream) <= epath_enabled(link,stream);
            epath_aligned_slv(link*(STREAMS_TOHOST-2)+stream) <= epath_aligned(link,stream);
            data_good_slv(link*(STREAMS_TOHOST-2)+stream)     <= data_good(link,stream);
        end generate g_to_slv_stream;
    end generate g_to_slv_link;

    --sim process
    mainproc : process
        variable vcnt : std_logic_vector(4 downto 0);
    begin
        assert false
            report "START SIMULATION"
            severity NOTE;
        clk_en  <= true;
        reset <= '0';
        select_random <= "0";
        wait for 2 ns;
        reset <= '1';
        wait for 50 ns;
        reset <= '0';
        log(ID_MONITOR,"chunk size fixed to "&to_string(lane_control(0).emulator(0).chunk_length,DEC), C_SCOPE);
        while true loop
            wait until l1a = '1' or timeout;
            if l1id /= "0000000000000000" then
                log(ID_MONITOR,"TRIGGER NUMBER "&to_string(l1id,DEC), C_SCOPE);
                for link in 0 to GBT_NUM-1 loop
                    for stream in 0 to STREAMS_TOHOST-3 loop
                        if epath_enabled(link,stream) = '1' then
                            log(ID_MONITOR,"LINK "&to_string(link)&
                                           " EPATH "&to_string(stream)&
                                           " ENABLED "&to_string(epath_enabled(link,stream))&
                                           " ALIGNED "&to_string(epath_aligned(link,stream))&
                                           " SIZE "&to_string(chunk_size(link,stream),DEC)&
                                           " DATA GOOD "&to_string(data_good(link,stream))
                                           , C_SCOPE);
                        end if;
                    end loop;
                end loop;
            end if;
            wait for 1 ns;
            if timeout or l1id = "0000000000010000" then
                exit;
            elsif l1id = "0000000000000111" then
                select_random <= "1";
            end if;
        end loop;
        wait for 1 us;

        vcnt  := count_final;
        check_value(vcnt,l1id(4 downto 0), ERROR, "Verifying the number of triggers with good data",C_SCOPE);
        check_value(timeout,false, ERROR, "Timeout check",C_SCOPE);
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        -- Finish the simulation
        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely
        finish;
        wait for 1000 us;
    end process;

    --timeout
    timeoutproc : process
    begin
        wait for 1000 us;
        timeout <= true;
    end process;
end tb;


--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.numeric_std_unsigned.all;

    use std.env.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;
    --library uvvm_vvc_framework;
    --use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

--use work.vvc_methods_pkg.all;
--use work.td_vvc_framework_common_methods_pkg.all;
--use work.egr_bfm_pkg.all;

    use work.pcie_package.all;
    use work.FELIX_package.all;

-- Test bench entity
entity ttc_emulator_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end ttc_emulator_tb;

architecture tb of ttc_emulator_tb is
    signal register_map_control        : register_map_control_type;

    signal reset                      : std_logic                      := '0';

    signal clk40                       : std_logic                      := '0';
    signal clk160                      : std_logic                      := '0';
    signal clk240                      : std_logic                      := '0';

    constant CARD_TYPE : integer := 712;
    constant GBT_NUM : integer := 4;
    constant FIRMWARE_MODE : integer := FIRMWARE_MODE_FULL;

    constant C_CLK_PERIOD_40: time := 25 ns;
    constant C_CLK_PERIOD_160: time := C_CLK_PERIOD_40 / 4;
    constant C_CLK_PERIOD_240: time := C_CLK_PERIOD_40 / 6;

    signal clock_ena: boolean := false;
    signal CLK_TTC_P : std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);
    signal CLK_TTC_N : std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);
    signal DATA_TTC_P : std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);
    signal DATA_TTC_N : std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);
    signal TB_trigger : std_logic_vector(NUM_TB_TRIGGERS(CARD_TYPE)-1 downto 0);
    signal LOL_ADN : std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);
    signal LOS_ADN : std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);
    signal register_map_ttc_monitor : register_map_ttc_monitor_type;
    signal TTC_out : TTC_data_type;
    signal TTC_ToHost_Data_out : TTC_data_type;
    signal LTI_FE_TXUSRCLK_in : std_logic_vector(GBT_NUM - 1 downto 0);
    signal LTI_FE_Data_Transceiver_out : array_32b(0 to GBT_NUM - 1);
    signal LTI_FE_CharIsK_out : array_4b(0 to GBT_NUM - 1);

    signal l0a_counter : std_logic_vector(37 downto 0);
begin

    CLK_TTC_P <= (others => clk160);
    CLK_TTC_N <= (others => not clk160);

    LTI_FE_TXUSRCLK_in <= (others => clk240);

    --Unused, using ttc emulator
    DATA_TTC_P <= (others => '1');
    DATA_TTC_N <= (others => '0');

    TB_trigger <= (others => '0');

    LOL_ADN <= (others => '0');
    LOS_ADN <= (others => '0');



    rst_hw_proc: process
    begin
        reset <= '1';
        wait for 25 ns;
        reset <= '0';
        wait;
    end process;


    -----------------------------------------------------------------------------
    -- Clock Generator
    -----------------------------------------------------------------------------
    clock_generator(clk40, clock_ena, C_CLK_PERIOD_40, "40 MHz clock");
    clock_generator(clk160, clock_ena, C_CLK_PERIOD_160, "160 MHz clock");
    clock_generator(clk240, clock_ena, C_CLK_PERIOD_240, "240 MHz clock");

    register_map_control.TTC_EMU.ENA <= "1";
    register_map_control.TTC_EMU.SEL <= "1";
    register_map_control.TTC_DEC_CTRL.TOHOST_RST <= (others => reset);
    register_map_control.TTC_ECR_MONITOR.CLEAR  <= (others => reset);
    register_map_control.TTC_TTYPE_MONITOR.CLEAR <= (others => reset);
    register_map_control.TTC_BCR_PERIODICITY_MONITOR.CLEAR <= (others => reset);
    register_map_control.TTC_BCR_COUNTER.CLEAR <= (others => reset);
    register_map_control.TTC_DEC_CTRL.TT_BCH_EN <= "1";
    register_map_control.TTC_CDRLOCK_MONITOR.CLEAR <= (others => reset);
    register_map_control.CRFROMHOST_RESET <= (others => reset);
    register_map_control.TTC_DEC_CTRL.B_CHAN_DELAY <= "0000";
    register_map_control.TTC_DELAY <= "0000";
    register_map_control.TTC_DEC_CTRL.BCID_ONBCR <= (others => '0');
    register_map_control.TTC_DEC_CTRL.XL1ID_SW <= (others => '0');
    register_map_control.TTC_DEC_CTRL.ECR_BCR_SWAP <= "0";

    register_map_control.TTC_EMU_TP_DELAY <= x"0000_0000";
    register_map_control.TTC_EMU_L1A_PERIOD <= x"0000_0100";
    register_map_control.TTC_EMU_ECR_PERIOD <= x"0001_0000";
    register_map_control.TTC_EMU_BCR_PERIOD <= std_logic_vector(to_unsigned(3564, 32));

    register_map_control.TTC_EMU_LONG_CHANNEL_DATA <= x"DEAD_BEEF"; --AS: used for individually addressed command
    register_map_control.TTC_EMU_CONTROL.BROADCAST <= (others => '0');

    register_map_control.TTC_EMU_RESET <= (others => reset);

    register_map_control.TTC_EMU_CONTROL.L1A <= "0";
    register_map_control.TTC_EMU_CONTROL.ECR <= "0";
    register_map_control.TTC_EMU_CONTROL.BCR <= "0";
    register_map_control.TTC_EMU_CONTROL.BUSY_IN_ENABLE <= "0";
    register_map_control.TTC_DEC_CTRL.XL1ID_RST <= (others => reset);
    register_map_control.TTC_L1A_DELAY <= (others => '0');
    register_map_control.TTC_ASYNCUSERDATA.WR_EN <= "0";
    register_map_control.TTC_ASYNCUSERDATA.DATA <= (others => '0');


    ttc0: entity work.ttc_wrapper generic map(
            CARD_TYPE => CARD_TYPE,
            ISTESTBEAM => false,
            ITK_TRIGTAG_MODE => 1,
            FIRMWARE_MODE => FIRMWARE_MODE,
            GBT_NUM => GBT_NUM
        )
        port map(
            CLK_TTC_P => CLK_TTC_P,
            CLK_TTC_N => CLK_TTC_N,
            DATA_TTC_P => DATA_TTC_P,
            DATA_TTC_N => DATA_TTC_N,
            TB_trigger => TB_trigger,
            LOL_ADN => LOL_ADN,
            LOS_ADN => LOS_ADN,
            register_map_control => register_map_control,
            register_map_ttc_monitor => register_map_ttc_monitor,
            register_map_control_appreg_clk => register_map_control,
            appreg_clk => clk40,
            TTC_out => TTC_out,
            clk_adn_160 => open,
            clk_ttc_40 => open,
            clk40 => clk40,
            BUSY => open,
            cdrlocked_out => open,
            TTC_ToHost_Data_out => TTC_ToHost_Data_out,
            TTC_BUSY_mon_array => (others => (others => '0')),
            BUSY_IN => '0',
            toHostXoff => (others => '0'),
            LTI_FE_TXUSRCLK_in => LTI_FE_TXUSRCLK_in,
            LTI_FE_Data_Transceiver_out => LTI_FE_Data_Transceiver_out,
            LTI_FE_CharIsK_out => LTI_FE_CharIsK_out
        );

    l0a_count: process(clk40)
        variable last_l0id: std_logic_vector(37 downto 0);
    begin
        if rising_edge(clk40) then
            if reset = '1' then
                l0a_counter <= (others => '1');
            else
                if TTC_out.SL0ID = '1' then
                    check_value(last_l0id, l0a_counter, ERROR, "L0A counter should be the same as L0ID");
                    l0a_counter <= (others => '0');
                else
                    if TTC_out.L0A = '1' then
                        l0a_counter <= l0a_counter + 1;
                    end if;
                end if;
                last_l0id := TTC_out.L0ID;
            end if;
        end if;
    end process;

    check_proc : process
    begin
        uvvm_completed <= '0';
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);
        enable_log_msg(ALL_MESSAGES);
        clock_ena <= true;

        wait for 2 ms;
        check_value(register_map_ttc_monitor.TTC_BCR_PERIODICITY_MONITOR.VALUE, x"0000_0001", ERROR, "BCR periodicity monitor should be 1");
        check_value(TTC_out.L0ID, l0a_counter, ERROR, "L0A counter should be the same as L0ID");
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);
        -- Finish the simulation
        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely
    end process;



end tb;

--ricardo luz, argonne
--based on simulation/FELIG/felig_lpgbt_sim.vhd

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.numeric_std_unsigned.all;
    use IEEE.std_logic_textio.all;

    use std.env.all;
    use std.textio.all;

    use work.FELIX_package.all;
    use work.pcie_package.all;
    use work.axi_stream_package.all;
    use work.type_lib.ALL;

library xpm;
    use xpm.vcomponents.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

entity FELIGlinkwrapperLPGBT_tb is
    generic(
        use_vunit       : boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end FELIGlinkwrapperLPGBT_tb;

architecture Behavioral of FELIGlinkwrapperLPGBT_tb is

    constant GBT_NUM                : integer := 1;

    constant clk40_period           : time      := 25 ns;
    signal clk_en                   : boolean   := false;
    signal clk40                    : std_logic;
    signal clk40_tmp                : std_logic := '0';
    signal clk320                   : std_logic;
    signal clk320_tmp               : std_logic := '0';
    signal clk_prcss                : std_logic := '0';
    signal cnt_div                : std_logic_vector(1 downto 0) := (others => '0');
    signal cnt_div_max            : std_logic_vector(1 downto 0) := (others => '0');

    signal reset                    : std_logic := '0';
    signal reset_done               : std_logic := '0';
    signal reset_clk40              : std_logic := '0';
    signal reset_done_clk40         : std_logic := '0';

    type data16barray               is array (0 to GBT_NUM-1) of std_logic_vector(15 downto 0);
    type data32barray               is array (0 to GBT_NUM-1) of std_logic_vector(31 downto 0);
    type txrx234b_48ch_type         is array (GBT_NUM-1 downto 0) of std_logic_vector(233 downto 0);
    type txrx36b_48ch_type          is array (GBT_NUM-1 downto 0) of std_logic_vector(35 downto 0);

    --lpgbt BE
    signal alignment_done_BE        : std_logic_vector(GBT_NUM-1 downto 0);
    signal data_rdy_BE              : std_logic_vector(GBT_NUM-1 downto 0);
    signal RxSlide_BE               : std_logic_vector(GBT_NUM-1 downto 0);
    signal TX_DATA_BE               : data16barray := (others => ("0000000000000000"));
    signal uplinkData_i_BE          : txrx234b_48ch_type;
    signal fec_error_i_BE           : std_logic_vector(GBT_NUM-1 downto 0);
    signal fec_err_cnt_i_BE         : array_32b(0 to GBT_NUM-1);

    --lpgbt FE
    signal RxSlide_FE               : std_logic_vector(GBT_NUM-1 downto 0);
    signal alignment_done_FE        : std_logic_vector(GBT_NUM-1 downto 0);
    signal sta_headerFlag_out_FE    : std_logic_vector(GBT_NUM-1 downto 0);
    signal TX_DATA_FE               : data32barray := (others => ("00000000000000000000000000000000"));
    signal downLinkData_i_FE        : txrx36b_48ch_type;
    signal tx_flag_out_FE           : std_logic_vector(GBT_NUM-1 downto 0);

    --data
    signal data_to_be               : std_logic_vector(223 downto 0) := (others => '0');
    signal data_to_fe               : std_logic_vector(31 downto 0) := (others => '0');

    --check
    signal aligned                  : std_logic := '0';
    signal good_FE                  : std_logic_vector(GBT_NUM-1 downto 0) := (others => '0');
    signal good_BE                  : std_logic_vector(GBT_NUM-1 downto 0) := (others => '0');
    constant GBT_one                : std_logic_vector(GBT_NUM-1 downto 0) := (others => '1');

    signal timeout                  : boolean := false;
begin

    --clocks

    clk320_tmp <= not clk320_tmp after clk40_period/16; --320 MHZ
    clk320     <= clk320_tmp     when  clk_en else '0';

    cnt_div_max <= "11";
    clk_prcss   <= clk320;
    clk40       <= clk40_tmp;

    process(clk_prcss)
    begin
        if rising_edge(clk_prcss) then
            if cnt_div = cnt_div_max then
                cnt_div <= "00";
                clk40_tmp <= not clk40_tmp;
            else
                cnt_div <= cnt_div + "01";
            end if;
        end if;
    end process;

    --reset

    process(clk40)
    begin
        if clk40'event and clk40='1' then
            reset_clk40         <= reset;
            reset_done_clk40    <= reset_done;
            data_to_be          <= data_to_be + x"1";
            data_to_fe          <= data_to_fe + x"1";
        end if;
    end process;

    -- lpgbt FELIX
    link_loop_BE : for i in 0 to GBT_NUM-1 generate
        signal downlinkUserData_i           : std_logic_vector(31 downto 0);
        signal downlinkEcData_i             : std_logic_vector(1 downto 0);
        signal downlinkIcData_i             : std_logic_vector(1 downto 0);
        signal GT_TX_WORD_CLK               : std_logic;
        signal GT_RX_WORD_CLK               : std_logic;
        signal uplinkDATA                   : std_logic_vector(223 downto 0);
        signal uplinkEC                     : std_logic_vector(1 downto 0);
        signal uplinkIC                     : std_logic_vector(1 downto 0);
        signal TX_aligned_i                 : std_logic;
    begin
        downlinkUserData_i      <= data_to_fe;
        downlinkIcData_i        <= "00";
        downlinkEcData_i        <= "11";
        GT_TX_WORD_CLK          <= clk320;
        GT_RX_WORD_CLK          <= clk320;
        lpgbt_BE: entity work.FLX_LpGBT_BE
            Port map (
                downlinkUserData_i          => downlinkUserData_i,
                downlinkEcData_i            => downlinkEcData_i,
                downlinkIcData_i            => downlinkIcData_i,
                TXCLK40                     => clk40,
                TXCLK320                    => GT_TX_WORD_CLK,
                RXCLK320m                   => GT_RX_WORD_CLK,
                uplinkSelectFEC_i           => '0', --FEC5
                data_rdy                    => data_rdy_BE(i),
                Tx_scrambler_bypass         => '0',
                Tx_Interleaver_bypass       => '0',
                Tx_FEC_bypass               => '0',
                TxData_Out                  => TX_DATA_BE(i),
                rxdatain                    => TX_DATA_FE(i),
                GBT_TX_RST                  => reset_clk40,
                GBT_RX_RST                  => reset_clk40,
                uplinkBypassInterleaver_i   => '0',
                uplinkBypassFECEncoder_i    => '0',
                uplinkBypassScrambler_i     => '0',
                uplinkMulticycleDelay_i     => "011", --register_map_control.GBT_TX_TC_DLY_VALUE1(2 downto 0);
                sta_headerFecLocked_o       => alignment_done_BE(i),
                ctr_clkSlip_o               => RxSlide_BE(i),
                uplinkReady_o               => open,
                uplinkUserData_o            => uplinkData_i_BE(i)(229 downto 0),
                uplinkEcData_o              => uplinkData_i_BE(i)(231 downto 230),
                uplinkIcData_o              => uplinkData_i_BE(i)(233 downto 232),
                fec_error_o                 => fec_error_i_BE(i),
                fec_err_cnt_o               => fec_err_cnt_i_BE(i),
                TX_aligned_i                => TX_aligned_i
            );
        uplinkDATA  <= uplinkData_i_BE(i)(223 downto 0);
        uplinkEC    <= uplinkData_i_BE(i)(231 downto 230);
        uplinkIC    <= uplinkData_i_BE(i)(233 downto 232);
        TX_aligned_i <= not reset_clk40;
    end generate;

    -- lpgbt FELIG
    link_loop_FE : for i in 0 to GBT_NUM-1 generate
        signal GT_TX_WORD_CLK       : std_logic;
        signal GT_RX_WORD_CLK       : std_logic;
        signal FE_UPLINK_USER_DATA  : std_logic_vector(223 downto 0);
        signal FE_UPLINK_IC_DATA    : std_logic_vector(1 downto 0);
        signal FE_UPLINK_EC_DATA    : std_logic_vector(1 downto 0);
        signal downlinkDATA         : std_logic_vector(31 downto 0);
        signal downlinkEC           : std_logic_vector(1 downto 0);
        signal downlinkIC           : std_logic_vector(1 downto 0);
        signal count                : std_logic_vector(2 downto 0) := "000";
        signal data_en              : std_logic;
    begin
        GT_TX_WORD_CLK       <= clk320;
        GT_RX_WORD_CLK       <= clk320;
        FE_UPLINK_USER_DATA  <= data_to_be;
        FE_UPLINK_IC_DATA    <= "10";
        FE_UPLINK_EC_DATA    <= "01";
        lpgbt_FE: entity work.FLX_LpGBT_FE
            Port map
        (
                clk40_in                        => clk40,
                TXCLK320                        => GT_TX_WORD_CLK,
                RXCLK320                        => GT_RX_WORD_CLK,
                rst_uplink_i                    => reset_clk40,
                ctr_clkSlip_s                   => RxSlide_FE(i),
                aligned                         => alignment_done_FE(i),
                sta_headerFlag_o                => sta_headerFlag_out_FE(i),
                dat_upLinkWord_fromGb_s         => TX_DATA_FE(i),
                dat_downLinkWord_fromMgt_s16    => TX_DATA_BE(i),
                rst_dnlink_i                    => reset_clk40,
                sta_mgtRxRdy_s                  => reset_done_clk40,
                downLinkBypassDeinterleaver     => '0',
                downLinkBypassFECDecoder        => '0',
                downLinkBypassDescsrambler      => '0',
                enableFECErrCounter             => '0',
                upLinkScramblerBypass           => '0',
                upLinkInterleaverBypass         => '0',
                fecMode                         => '0', --FEC5
                txDataRate                      => '1', --10.24 Gb/s
                phase_sel                       => "100",
                upLinkData                      => FE_UPLINK_USER_DATA,
                upLinkDataIC                    => FE_UPLINK_IC_DATA,
                upLinkDataEC                    => FE_UPLINK_EC_DATA,
                upLinkDataREADY                 => data_en,
                downLinkData                    => downLinkData_i_FE(i)(31 downto 0),
                downLinkDataIC                  => downLinkData_i_FE(i)(33 downto 32),
                downLinkDataEC                  => downLinkData_i_FE(i)(35 downto 34),
                tx_flag_out                     => tx_flag_out_FE(i)
            );
        downlinkDATA    <= downLinkData_i_FE(i)(31 downto 0);
        downlinkEC      <= downLinkData_i_FE(i)(33 downto 32);
        downlinkIC      <= downLinkData_i_FE(i)(35 downto 34);
        process(GT_TX_WORD_CLK)
        begin
            if GT_TX_WORD_CLK'event and GT_TX_WORD_CLK='1' then
                count <= count + "001";
                if count = "111" then
                    data_en <= '1';
                else
                    data_en <= '0';
                end if;
            end if;
        end process;
    end generate;

    --check_data
    process(clk320)
    begin
        if clk320'event and clk320='1' then
            if alignment_done_FE = GBT_one and alignment_done_BE = GBT_one then
                aligned <= '1';
            else
                aligned <= '0';
            end if;
        end if;
    end process;

    link_check : for i in 0 to GBT_NUM-1 generate
        signal dif_FE                   : integer := 0;
        signal dif_BE                   : integer := 0;

    begin
        process(clk320)
        begin
            if clk320'event and clk320='1' then
                if cnt_div = "10" and clk40 = '1' and aligned = '1' then
                    if dif_FE = 0 and dif_BE = 0 then
                        dif_FE <= to_integer(unsigned(data_to_fe-downLinkData_i_FE(0)(31 downto 0)));
                        dif_BE <= to_integer(unsigned(data_to_be-uplinkData_i_BE(0)(223 downto 0)));
                    else
                        if data_to_fe > downLinkData_i_FE(0)(31 downto 0) then
                            if dif_FE = to_integer(unsigned(data_to_fe-downLinkData_i_FE(0)(31 downto 0))) then
                                good_FE(i) <= '1';
                            else
                                good_FE(i) <= '0';
                            end if;
                        else
                            good_FE(i) <= '0';
                        end if;
                        if data_to_be > uplinkData_i_BE(0)(223 downto 0) then
                            if dif_BE = to_integer(unsigned(data_to_be-uplinkData_i_BE(0)(223 downto 0))) then
                                good_BE(i) <= '1';
                            else
                                good_BE(i) <= '0';
                            end if;
                        else
                            good_BE(i) <= '0';
                        end if;
                    end if;
                elsif aligned = '0' then
                    dif_FE <= 0;
                    dif_BE <= 0;
                end if;
            end if;
        end process;
    end generate;

    --sim process
    mainproc : process
        variable var_FE : std_logic_vector(GBT_NUM-1 downto 0) := (others => '0');
        variable var_BE : std_logic_vector(GBT_NUM-1 downto 0) := (others => '0');
        variable var_br : integer := 0;
    begin
        assert false
            report "START SIMULATION"
            severity NOTE;
        clk_en  <= true;
        wait for 2 us;
        reset   <= '1';
        wait for 2 us;
        reset   <='0';
        reset_done  <= '1';
        wait for 5 us;
        var_br := 0;
        wait until (aligned = '1' and good_FE = GBT_one and good_FE = GBT_one)  or timeout;
        var_FE := good_FE;
        var_BE := good_BE;
        while true loop
            if timeout or data_to_fe > "00000000000000000001000000000000" then
                exit;
            end if;
            if good_BE /= var_BE or var_FE /= good_FE then
                var_br := 1;
                log(ID_MONITOR,"Value changed. good_BE is "&to_string(good_BE)&" it was "&to_string(var_BE)&". good_FE is "&to_string(good_FE)&" it was "&to_string(var_FE), C_SCOPE);
                wait for 25 ns;
                var_BE := good_BE;
                var_FE := good_FE;
            else
                wait for 25 ns;
            end if;
        end loop;

        check_value(var_br, 0, ERROR, "Test if good_BE or good_FE ever changed value. If failed, check output.txt for more detail",C_SCOPE);
        check_value(timeout,false, ERROR, "Timeout check",C_SCOPE);
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        -- Finish the simulation
        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely
        finish;
        wait for 1000 us;
    end process;

    timeoutproc : process
    begin
        wait for 1000 us;
        timeout <= true;
    end process;

end Behavioral;

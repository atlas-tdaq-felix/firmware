--ricardo luz, argonne

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.numeric_std_unsigned.all;
    use IEEE.std_logic_textio.all;

    use std.env.all;
    use std.textio.all;

    use work.FELIX_package.all;
    use work.pcie_package.all;
    use work.axi_stream_package.all;
    use work.type_lib.ALL;

library xpm;
    use xpm.vcomponents.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

entity FELIGexternaltrigger_tb is
    generic(
        use_vunit       : boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end FELIGexternaltrigger_tb;

architecture Behavioral of FELIGexternaltrigger_tb is
    function FM_TO_PROTOCOL(FIRMWARE_MODE:integer)
        return std_logic is
    begin
        if FIRMWARE_MODE = FIRMWARE_MODE_FELIG_GBT then
            return '0';
        elsif FIRMWARE_MODE = FIRMWARE_MODE_FELIG_LPGBT then
            return '1';
        else
            return '0';
        end if;
    end function;
    constant GBT_NUM            : integer := 1;
    constant FIRMWARE_MODE      : integer := 11;
    constant NUMELINKmax        : integer := 112; --name number of epahts per egroup (theoretically 2b epath * 112=224, but never happens)
    constant NUMEGROUPmax       : integer := 7; --name number of egroups

    constant clk40_period       : time      := 25 ns;
    signal clk_en               : boolean   := false;
    signal clk40                : std_logic;
    signal clk40_tmp            : std_logic := '0';
    signal clk320               : std_logic;
    signal clk320_tmp           : std_logic := '0';
    signal clk_prcss            : std_logic := '0';
    signal clkTX                : std_logic_vector(GBT_NUM-1 downto 0);
    signal clkRX                : std_logic_vector(GBT_NUM-1 downto 0);
    signal cnt_div            : std_logic_vector(1 downto 0) := (others => '0');
    signal cnt_div_max        : std_logic_vector(1 downto 0) := (others => '0');

    signal reset                : std_logic;
    signal reset_d              : std_logic;

    signal register_map_control : register_map_control_type;
    --    signal single_l1a_long      : std_logic_vector(0 downto 0) := "0";
    --    signal single_ecr_long      : std_logic_vector(0 downto 0) := "0";
    --    signal single_bcr_long      : std_logic_vector(0 downto 0) := "0";
    signal single_l1a_long      : std_logic := '0';
    signal single_ecr_long      : std_logic := '0';
    signal single_bcr_long      : std_logic := '0';
    signal enable_ttc           : std_logic := '0';
    signal TTCout               : std_logic_vector(9 downto 0) := (others => '0');
    signal TTCout320               : std_logic_vector(9 downto 0) := (others => '0');

    signal lane_control         : array_of_lane_control_type(GBT_NUM-1 downto 0);
    signal lane_monitor         : array_of_lane_monitor_type(GBT_NUM-1 downto 0);
    signal aligned              : std_logic := '0';

    signal L1A                  : std_logic := '0';
    signal L1A_count            : std_logic_vector(7 downto 0) := (others => '0');
    signal l1a_id_register_rst  : std_logic := '0';
    signal txrx_flag            : std_logic := '0';
    signal rx_data              : std_logic_vector(119 downto 0) := (others => '0');
    signal tx_data              : std_logic_vector(227 downto 0) := (others => '0');
    signal tx_eg_data           : array_32b(NUMEGROUPmax-1 downto 0);
    signal data_to_be_checked   : std_logic_vector(7 downto 0) := (others => '0');
    signal tx_rdy               : std_logic := '0';
    signal data_delay           : std_logic_vector(7 downto 0) := (others => '0');
    signal valid_delay          : std_logic := '0';
    signal data_count           : std_logic_vector(31 downto 0) := (others => '0');
    signal data_size            : std_logic_vector(31 downto 0) := (others => '0');

    signal emu_l1a_id           : std_logic_vector(15 downto 0)  := (others => '0');
    signal emu_l1a_id_delay     : std_logic_vector(15 downto 0)  := (others => '0');
    signal emu_l1a              : std_logic := '0';
    signal emu_l1a_40           : std_logic := '0';
    signal next_l1a_id          : std_logic_vector(15 downto 0)  := x"0001";

    signal check_words          : std_logic := '0';
    signal exp_checks           : std_logic_vector(6 downto 0) := (others => '0');
    signal cnt_checks           : std_logic_vector(6 downto 0) := (others => '0');
    signal good                 : std_logic := '0';

    signal count_good_ids       : std_logic_vector(7 downto 0)  := (others => '0');
    signal count_good_data      : std_logic_vector(7 downto 0)  := (others => '0');
    signal count_triggers       : std_logic_vector(7 downto 0) := (others => '0');

    signal test                 : std_logic := '0';
    signal timeout              : boolean := false;
begin

    --clocks

    clk320_tmp <= not clk320_tmp after clk40_period/16; --320 MHZ
    clk320     <= clk320_tmp     when  clk_en else '0';

    cnt_div_max <= "11";
    clk_prcss   <= clk320;
    clk40       <= clk40_tmp;
    process(clk_prcss)
    begin
        if rising_edge(clk_prcss) then
            if cnt_div = cnt_div_max then
                cnt_div <= "00";
                clk40_tmp <= not clk40_tmp;
            else
                cnt_div <= cnt_div + "01";
            end if;
        end if;
    end process;

    --gen lane_control for FELIG

    g_lane_control : for link in 0 to GBT_NUM-1 generate
        signal elink_output_width   : array_of_slv_2_0(NUMELINKmax-1 downto 0) := (others => "111");
        signal elink_enable         : std_logic_vector(NUMELINKmax-1 downto 0) := (others => '1');
        signal data_format          : std_logic_vector(1 downto 0);
    begin
        lane_control(link).global.framegen_reset               <= '0';
        lane_control(link).global.elink_sync                   <= '0';
        lane_control(link).global.framegen_data_select         <= '0';
        lane_control(link).global.emu_data_select              <= '1';
        lane_control(link).global.l1a_source                   <= '1';--EXTERNAL TRIGGER
        lane_control(link).global.loopback_fifo_delay          <= "00010";
        lane_control(link).global.loopback_fifo_reset          <= '0';
        lane_control(link).global.a_ch_bit_sel                 <= "0000000";
        lane_control(link).global.b_ch_bit_sel                 <= "0000001";
        lane_control(link).global.MSB                          <= '1';
        lane_control(link).global.FEC                          <= '0';
        lane_control(link).global.DATARATE                     <= '1';
        lane_control(link).global.aligned                      <= aligned;
        lane_control(link).global.l1a_max_count                <= x"0000000";
        lane_control(link).global.lane_reset                   <= '0';
        lane_control(link).global.l1a_counter_reset            <= l1a_id_register_rst;
        lane_control(link).fmemu_random.FMEMU_RANDOM_CONTROL.SELECT_RANDOM <= "0";
        lane_control(link).fmemu_random.FMEMU_RANDOM_CONTROL.SEED          <= "1000000000"; --0x200
        lane_control(link).fmemu_random.FMEMU_RANDOM_CONTROL.POLYNOMIAL    <= "1001000000"; --0x240
        lane_control(link).fmemu_random.FMEMU_RANDOM_RAM_ADDR              <= "0000000000";
        lane_control(link).fmemu_random.FMEMU_RANDOM_RAM.WE                <= "0";
        lane_control(link).fmemu_random.FMEMU_RANDOM_RAM.CHANNEL_SELECT    <= (others=>'0');
        lane_control(link).fmemu_random.FMEMU_RANDOM_RAM.DATA              <= (others=>'0');

        lane_control(link).emulator(0).output_width  <= "010" when FIRMWARE_MODE = 11 else "000" when FIRMWARE_MODE = 6 else "000";-- 8b 2b
        lane_control(link).emulator(1).output_width  <= "010" when FIRMWARE_MODE = 11 else "000" when FIRMWARE_MODE = 6 else "000";-- 8b 2b
        lane_control(link).emulator(2).output_width  <= "011" when FIRMWARE_MODE = 11 else "001" when FIRMWARE_MODE = 6 else "000";--16b 4b
        lane_control(link).emulator(3).output_width  <= "011" when FIRMWARE_MODE = 11 else "001" when FIRMWARE_MODE = 6 else "000";--16b 4b
        lane_control(link).emulator(4).output_width  <= "100" when FIRMWARE_MODE = 11 else "010" when FIRMWARE_MODE = 6 else "000";--32b 8b
        lane_control(link).emulator(5).output_width  <= "100";--32b
        lane_control(link).emulator(6).output_width  <= "100";--32b

        data_format <= "00";

        g_emu_control : for egroup in lane_control(link).emulator'range generate
            lane_control(link).emulator(egroup).pattern_select   <= "00";
            lane_control(link).emulator(egroup).data_format      <= data_format;
            lane_control(link).emulator(egroup).sw_busy          <= '0';
            lane_control(link).emulator(egroup).reset            <= '0';--emu_reset;
            lane_control(link).emulator(egroup).chunk_length     <= X"003C"; --60+8
            lane_control(link).emulator(egroup).userdata         <= X"ABCD";

            less_that_five : if egroup<5 generate
                elink_enable(egroup*16+7 downto egroup*16)    <= X"FF" when (lane_control(link).emulator(egroup).output_width <= "000") else
                                                                 X"55" when (lane_control(link).emulator(egroup).output_width <= "001") else
                                                                 X"11" when (lane_control(link).emulator(egroup).output_width <= "010") else
                                                                 X"01" when (lane_control(link).emulator(egroup).output_width <= "011") else
                                                                 X"01" when (lane_control(link).emulator(egroup).output_width <= "100");
                elink_enable(egroup*16+15 downto egroup*16+8) <= X"FF" when (lane_control(link).emulator(egroup).output_width <= "000" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1') else
                                                                 X"55" when (lane_control(link).emulator(egroup).output_width <= "001" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1') else
                                                                 X"11" when (lane_control(link).emulator(egroup).output_width <= "010" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1') else
                                                                 X"01" when (lane_control(link).emulator(egroup).output_width <= "011" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1') else
                                                                 X"00";
            end generate less_that_five;
            five : if egroup=5 generate
                elink_enable(egroup*16+7 downto egroup*16)    <= X"FF" when (lane_control(link).emulator(egroup).output_width <= "000" and FIRMWARE_MODE = 11) else
                                                                 X"55" when (lane_control(link).emulator(egroup).output_width <= "001" and FIRMWARE_MODE = 11) else
                                                                 X"11" when (lane_control(link).emulator(egroup).output_width <= "010" and FIRMWARE_MODE = 11) else
                                                                 X"01" when (lane_control(link).emulator(egroup).output_width <= "011" and FIRMWARE_MODE = 11) else
                                                                 X"01" when (lane_control(link).emulator(egroup).output_width <= "100" and FIRMWARE_MODE = 11) else
                                                                 X"00";
                elink_enable(egroup*16+15 downto egroup*16+8) <= X"FF" when (lane_control(link).emulator(egroup).output_width <= "000" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1') else
                                                                 X"55" when (lane_control(link).emulator(egroup).output_width <= "001" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1') else
                                                                 X"11" when (lane_control(link).emulator(egroup).output_width <= "010" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1') else
                                                                 X"01" when (lane_control(link).emulator(egroup).output_width <= "011" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1') else
                                                                 X"00";
            end generate five;
            six : if egroup=6 generate
                elink_enable(egroup*16+7 downto egroup*16)    <= X"FF" when (lane_control(link).emulator(egroup).output_width <= "000" and FIRMWARE_MODE = 11 and lane_control(link).global.FEC = '0') else
                                                                 X"55" when (lane_control(link).emulator(egroup).output_width <= "001" and FIRMWARE_MODE = 11 and lane_control(link).global.FEC = '0') else
                                                                 X"11" when (lane_control(link).emulator(egroup).output_width <= "010" and FIRMWARE_MODE = 11 and lane_control(link).global.FEC = '0') else
                                                                 X"01" when (lane_control(link).emulator(egroup).output_width <= "011" and FIRMWARE_MODE = 11 and lane_control(link).global.FEC = '0') else
                                                                 X"01" when (lane_control(link).emulator(egroup).output_width <= "100" and FIRMWARE_MODE = 11 and lane_control(link).global.FEC = '0') else
                                                                 X"00";
                elink_enable(egroup*16+15 downto egroup*16+8) <= X"FF" when (lane_control(link).emulator(egroup).output_width <= "000" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1' and lane_control(link).global.FEC = '0') else
                                                                 X"55" when (lane_control(link).emulator(egroup).output_width <= "001" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1' and lane_control(link).global.FEC = '0') else
                                                                 X"11" when (lane_control(link).emulator(egroup).output_width <= "010" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1' and lane_control(link).global.FEC = '0') else
                                                                 X"01" when (lane_control(link).emulator(egroup).output_width <= "011" and FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1' and lane_control(link).global.FEC = '0') else
                                                                 X"00";
            end generate six;
            width : for k in 0 to 7 generate
                elink_output_width(egroup*16+k)   <= lane_control(link).emulator(egroup).output_width;
                elink_output_width(egroup*16+k+8) <= lane_control(link).emulator(egroup).output_width when (FIRMWARE_MODE = 11 and lane_control(link).global.DATARATE = '1') else "111";
            end generate width;

        end generate g_emu_control;

        g_elink_control : for epath in lane_control(link).elink'range generate
            lane_control(link).elink(epath).input_width    <= '0' when data_format = "00" else '1';
            lane_control(link).elink(epath).endian_mode    <= '0';
            lane_control(link).elink(epath).enable         <= elink_enable(epath);
            lane_control(link).elink(epath).output_width   <= elink_output_width(epath);
        end generate g_elink_control;

    end generate g_lane_control;

    --Generate FELIX TTC signals

    process(clk40)
    begin
        if rising_edge(clk40) then
            if L1A_count = x"8F" then
                L1A_count <= x"00";
                L1A <= '1';
                count_triggers <= count_triggers + '1';
            else
                L1A_count <= L1A_count + x"01";
                L1A <= '0';
                if count_triggers = x"04" and L1A_count = x"08" then
                    single_ecr_long <= '1';
                    l1a_id_register_rst <= '0';
                    log(ID_MONITOR,"ECR issued", C_SCOPE);
                elsif count_triggers = x"09" and L1A_count = x"1F" then
                    single_ecr_long <= '0';
                    l1a_id_register_rst <= '1';
                    log(ID_MONITOR,"L1A ID register reset used", C_SCOPE);
                else
                    single_ecr_long <= '0';
                    l1a_id_register_rst <= '0';
                end if;
            end if;
        end if;
    end process;

    single_l1a_long <= L1A;
    register_map_control.TTC_EMU.SEL(1)                 <= enable_ttc;
    register_map_control.TTC_EMU.ENA(0)                 <= enable_ttc;
    register_map_control.TTC_EMU_TP_DELAY               <= x"00000040";
    register_map_control.TTC_EMU_L1A_PERIOD             <= x"00000000";
    register_map_control.TTC_EMU_ECR_PERIOD             <= x"00000000";
    register_map_control.TTC_EMU_BCR_PERIOD             <= x"00000dec";
    register_map_control.TTC_EMU_LONG_CHANNEL_DATA      <= x"00000000";
    register_map_control.TTC_EMU_CONTROL.BROADCAST      <= "000000";
    register_map_control.TTC_EMU_RESET(64)              <= reset;
    register_map_control.TTC_EMU_CONTROL.L1A(24)        <= single_l1a_long;
    register_map_control.TTC_EMU_CONTROL.ECR(26)        <= single_ecr_long;
    register_map_control.TTC_EMU_CONTROL.BCR(25)        <= single_bcr_long;
    register_map_control.TTC_EMU_CONTROL.BUSY_IN_ENABLE <= "0";

    TTCEmu: entity work.TTC_Emulator
        port map(
            Clock => clk40,
            Reset => reset,
            register_map_control => register_map_control,
            add_s8 => open,
            add_d8 => open,
            add_strobe => open,
            add_e => open,
            TTCout => TTCout, --      : out std_logic_vector(9 downto 0)
            BUSYIn => '0'
        );


    --cdr ttcout 40 to 320
    process(clk320)
        variable count : integer := 0;
    begin
        if rising_edge(clk320) then
            TTCout320 <= TTCout;
            if count = 7 then
                count := 0;
                txrx_flag <= '1';
            else
                count := count + 1;
                txrx_flag <= '0';
            end if;
        end if;
    end process;

    --FELIG emulator

    rx_data(1) <= TTCout320(1); --b channel
    rx_data(0) <= TTCout320(0); --l1a

    felig_emulator : entity work.emulator
        generic map (
            useGBTdataEmulator       => false,
            sim_emulator             => false,
            LANE_ID                  => 0,
            PROTOCOL                 => FM_TO_PROTOCOL(FIRMWARE_MODE)
        )
        port map (
            clk40                   => clk40,
            lane_rxclk              => clk320,
            lane_txclk              => clk320,
            l1a_int_trigger         => '0',
            l1a_int_int_id          => (others => '0'),
            l1a_trigger_out         => emu_l1a,
            gbt_tx_data_228b_out    => tx_data,
            data_ready_tx_out       => tx_rdy,
            gbt_rx_data_120b_in     => rx_data,
            gbt_tx_flag_in          => txrx_flag,
            gbt_rx_flag_in          => txrx_flag,
            lane_control            => lane_control(0),
            lane_monitor            => lane_monitor(0)--,
        );

    emu_l1a_id  <= lane_monitor(0).global.l1a_id(15 downto 0);

    g_egroups : for eg in 0 to NUMEGROUPmax-1 generate --assuming LPGBT
        tx_eg_data(eg) <= tx_data(32*(eg+1)-1 downto 32*eg);
    end generate;

    data_to_be_checked  <= tx_data(7 downto 0); -- 1 epath of the 8b egroup

    --gen data_valid

    process(clk320)
        variable count_l1a : integer := 0;
    begin
        if rising_edge(clk320) then
            if aligned = '1'  then
                if tx_rdy = '1' then
                    data_delay <= data_to_be_checked;
                    if valid_delay = '1' then
                        data_count <= data_count + '1';
                    else
                        data_count <= (others => '0');
                    end if;

                    if data_count = x"00000002" then
                        data_size <= data_delay + x"00000008";
                    end if;

                    if data_count = data_size - x"00000001" and data_size /= x"00000000" then
                        valid_delay <= '0';
                    end if;
                end if;
                if data_to_be_checked /= data_delay then
                    valid_delay <= '1';
                end if;
            end if;
            if emu_l1a = '1' then
                count_l1a := count_l1a + 1;
                emu_l1a_40 <= '1';
            else
                if count_l1a /= 0 then
                    if count_l1a = 8 then
                        count_l1a := 0;
                        emu_l1a_40 <= '0';
                    else
                        count_l1a := count_l1a + 1;
                    end if;
                end if;
            end if;
        end if;
    end process;

    --check_data (based on FELIGemulator_tb.vhd)

    process(clk40)
        variable v_chunks   : integer := 0;
        variable size       : integer := 100;
        variable next_w     : integer := 100;
        variable next_b     : integer := 100;
        variable flag_check : std_logic := '0';
    begin
        if rising_edge(clk40) then
            if valid_delay = '1' then
                v_chunks := v_chunks + 1;
            else
                v_chunks := 0;
            end if;

            if v_chunks = 1 and data_delay = x"aa" then --checks first byte
                check_words <= '1';
                flag_check := '1';
            elsif v_chunks = 3 then
                size := to_integer(unsigned(data_delay));
            --chunk_size(link,stream) <= data_delay;
            elsif v_chunks = 5 and data_delay = emu_l1a_id(7 downto 0) then
                check_words <= '1';
                next_w      := 10;
                next_b      := size - 2;
            elsif v_chunks = 6 and data_delay = x"bb" then
                check_words <= '1';
            elsif v_chunks = 7 and data_delay = x"aa" then
                check_words <= '1';
            elsif v_chunks = 8 and data_delay = x"8" then
                check_words <= '1';
            elsif v_chunks = next_w and data_delay = std_logic_vector(to_unsigned(next_b,8)) then
                check_words <= '1';
                next_w      := next_w + 2;
                if next_b > 1 then
                    next_b      := next_b - 2;
                end if;
            else
                check_words <= '0';
            end if;

            if check_words = '1' then
                cnt_checks <= cnt_checks + '1';
            end if;

            exp_checks <= std_logic_vector(to_unsigned(5 + size/2, exp_checks'length));

            if cnt_checks = exp_checks and aligned = '1' then
                good <= '1';
            end if;

            if flag_check = '1' and emu_l1a_40 = '1' then
                flag_check := '0';
                cnt_checks <=  (others => '0');
                good <= '0';
                if good = '1' then
                    count_good_data <= count_good_data + '1';
                end if;
            end if;
        end if;
    end process;

    --check l1a_id
    process(clk40)
    begin
        if rising_edge(clk40) then
            emu_l1a_id_delay <= emu_l1a_id;
            if emu_l1a_id_delay /= emu_l1a_id then
                if aligned = '1' then
                    log(ID_MONITOR,"L1A ID is  " & to_string(emu_l1a_id,DEC), C_SCOPE);
                end if;
                if emu_l1a_id = next_l1a_id then
                    count_good_ids <= count_good_ids + '1';
                    next_l1a_id <= next_l1a_id + '1';
                end if;
            end if;
            if single_ecr_long = '1' or l1a_id_register_rst = '1' then
                next_l1a_id <= (others => '0');
            end if;
        end if;
    end process;

    --main process
    mainproc : process
        variable vcnt : std_logic_vector(4 downto 0);
    begin
        assert false
            report "START SIMULATION"
            severity NOTE;
        clk_en  <= true;
        reset <= '0';
        wait for 2 ns;
        reset <= '1';
        wait for 50 ns;
        reset <= '0';
        wait for 50 ns;
        enable_ttc <= '1';
        wait for 50 ns;
        aligned <= '1';

        wait until (count_triggers = x"0F" and L1A_count = x"4F") or timeout;
        check_value(count_good_ids,count_triggers, ERROR, "Comparing count_good_ids with count_triggers",C_SCOPE);
        check_value(count_good_data,count_triggers-'1', ERROR, "Comparing count_good_data with count_triggers",C_SCOPE);
        check_value(timeout,false, ERROR, "Timeout check",C_SCOPE);
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        --Finish the simulation
        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely
        finish;
        wait for 1000 us;
    end process;

    --timeout
    timeoutproc : process
    begin
        wait for 500 us;
        timeout <= true;
    end process;
end Behavioral;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--========================================================================================================================
-- Copyright (c) 2017 by Bitvis AS.  All rights reserved.
-- You should have received a copy of the license file containing the MIT License (see LICENSE.TXT), if not,
-- contact Bitvis AS <support@bitvis.no>.
--
-- UVVM AND ANY PART THEREOF ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
-- WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
-- OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH UVVM OR THE USE OR OTHER DEALINGS IN UVVM.
--========================================================================================================================

------------------------------------------------------------------------------------------
-- VHDL unit     : crc20 (FULL mode CRC implementation)
--
-- Description   : This toplevel testbench instantiates a data generator (crc20_datagen.vhd)
-- which creates random data of random length and a CRC20, generated using UVVM functions
-- The data is also going through the FELIX crc module with the FELIX full mode polynomial
-- And the data is verified in this testbench.
------------------------------------------------------------------------------------------


library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;

    use std.env.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

--library bitvis_vip_sbi;
--use bitvis_vip_sbi.sbi_bfm_pkg.all;

-- Test case entity
entity crc20_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity;

-- Test case architecture
architecture func of crc20_tb is

    -- DSP interface and general control signals
    signal clk250           : std_logic  := '0';
    -- interface
    signal crc_result: std_logic_vector(19 downto 0);
    signal crc_calc: std_logic;
    signal data: std_logic_vector(31 downto 0);
    signal sop, eop: std_logic;
    signal dvalid: std_logic;
    -- output

    -- Interrupt related signals
    signal clock_ena     : boolean   := false;

    constant C_CLK_PERIOD_250     : time := 4 ns;


begin
    -----------------------------------------------------------------------------
    -- Instantiate DUT
    -----------------------------------------------------------------------------
    crc20: entity work.CRC
        --generic map(
        -- Nbits => 32,
        -- CRC_Width => 20,
        -- G_Poly => x"8359f",
        -- G_InitVal => x"fffff"
        -- )
        port map(
            CRC   => crc_result,
            Calc  => crc_calc,
            Clk   => clk250,
            Din   => data,
            Reset => sop);

    datagen: entity work.crc20_datagen
        port map(
            clk => clk250,
            data_out => data,
            sop_out  => sop,
            eop_out  => eop,
            dvalid_out => dvalid
        );


    crc_calc <= dvalid and not eop;

    -----------------------------------------------------------------------------
    -- Clock Generator
    -----------------------------------------------------------------------------
    clock_generator(clk250, clock_ena, C_CLK_PERIOD_250, "250 TB clock");


    ------------------------------------------------
    -- PROCESS: p_main
    ------------------------------------------------
    p_main: process
        constant C_SCOPE     : string  := C_TB_SCOPE_DEFAULT;
        variable crc_check_val: std_logic_vector(19 downto 0);
    begin
        uvvm_completed <= '0';
        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);
        enable_log_msg(ALL_MESSAGES);
        --disable_log_msg(ALL_MESSAGES);
        --enable_log_msg(ID_LOG_HDR);
        clock_ena <= true;

        log(ID_LOG_HDR, "Simulation of TB for CRC20", C_SCOPE);
        ------------------------------------------------------------

        for i in 0 to 200 loop
            wait until eop = '1'; --The CRC arrives when eop is '1', both from the data generator which is usin UVVM for the CRC calculation and the crc module (DUT).
            crc_check_val := data(19 downto 0);
            --In Phase I FELIX the CRC20 module takes 2 clockcycles, so we have to wait 1 more for the check in Phase I. For Phase II remove the next line:
            wait_num_rising_edge(clk250, 1);
            check_value(crc_check_val, crc_result, ERROR, "CRC mismatch", C_SCOPE);
            wait_num_rising_edge(clk250, 1);
        end loop;
        --==================================================================================================
        -- Ending the simulation
        --------------------------------------------------------------------------------------
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        -- Finish the simulation
        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely

    end process p_main;

end func;

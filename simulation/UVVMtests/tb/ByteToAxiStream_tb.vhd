--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;

    use std.env.all;
    use work.axi_stream_package.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

library bitvis_vip_axistream;
    use bitvis_vip_axistream.axistream_bfm_pkg.all;
    use bitvis_vip_axistream.vvc_methods_pkg.all;
    context bitvis_vip_axistream.vvc_context;
library bitvis_vip_scoreboard; -- @suppress "Library 'bitvis_vip_scoreboard' is not available"
library uvvm_vvc_framework;
    use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;


-- Test case entity
entity ByteToAxiStream_tb is
end entity ByteToAxiStream_tb;

-- Test case architecture
architecture func of ByteToAxiStream_tb is


    -- Interrupt related signals
    signal clock_ena     : boolean   := false;

    constant C_CLK_PERIOD_40     : time := 25 ns;
    constant C_CLK_PERIOD_160     : time := 6.25 ns;

    signal clk40, clk160: std_logic;
    signal reset: std_logic;

    signal DataIn1b : std_logic_vector(7 downto 0);
    signal DataInValid1b: std_logic_vector(0 downto 0);
    signal EOP1b: std_logic_vector(0 downto 0);
    signal SOB1b: std_logic_vector(0 downto 0);
    signal EOB1b: std_logic_vector(0 downto 0);
    signal TruncateIn1b: std_logic_vector(0 downto 0);
    signal m_axis1b: axis_32_type;
    signal m_axis_tready_1b: std_logic;
    signal m_axis_prog_empty1b: std_logic; -- @suppress "signal m_axis_prog_empty1b is never read"

    signal DataIn2b : std_logic_vector(15 downto 0);
    signal DataInValid2b: std_logic_vector(1 downto 0);
    signal EOP2b: std_logic_vector(1 downto 0);
    signal SOB2b: std_logic_vector(1 downto 0);
    signal EOB2b: std_logic_vector(1 downto 0);
    signal TruncateIn2b: std_logic_vector(1 downto 0);
    signal m_axis2b: axis_32_type;
    signal m_axis_tready_2b: std_logic;
    signal m_axis_prog_empty2b: std_logic; -- @suppress "signal m_axis_prog_empty2b is never read"

    signal DataIn4b : std_logic_vector(31 downto 0);
    signal DataInValid4b: std_logic_vector(3 downto 0);
    signal EOP4b: std_logic_vector(3 downto 0);
    signal SOB4b: std_logic_vector(3 downto 0);
    signal EOB4b: std_logic_vector(3 downto 0);
    signal TruncateIn4b: std_logic_vector(3 downto 0);
    signal m_axis4b: axis_32_type;
    signal m_axis_tready_4b: std_logic;
    signal m_axis_prog_empty4b: std_logic; -- @suppress "signal m_axis_prog_empty4b is never read"

    signal axistream_vvc_if_1b : t_axistream_if(tdata(31 downto 0), tkeep(3 downto 0), tuser(3 downto 0), tstrb(3 downto 0), tid(0 downto 0), tdest(0 downto 0)) := init_axistream_if_signals(false, 32, 4, 1, 1);
    signal axistream_vvc_if_2b : t_axistream_if(tdata(31 downto 0), tkeep(3 downto 0), tuser(3 downto 0), tstrb(3 downto 0), tid(0 downto 0), tdest(0 downto 0)) := init_axistream_if_signals(false, 32, 4, 1, 1);
    signal axistream_vvc_if_4b : t_axistream_if(tdata(31 downto 0), tkeep(3 downto 0), tuser(3 downto 0), tstrb(3 downto 0), tid(0 downto 0), tdest(0 downto 0)) := init_axistream_if_signals(false, 32, 4, 1, 1);

    constant GC_AXISTREAM_BFM_CONFIG : t_axistream_bfm_config := C_AXISTREAM_BFM_CONFIG_DEFAULT;
    signal clk40_stable : std_logic;
    signal daq_reset : std_logic;
    signal daq_fifo_flush : std_logic;
begin
    -- Instantiate the concurrent procedure that initializes UVVM
    i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

    -----------------------------------------------------------------------------
    -- Clock Generator
    -----------------------------------------------------------------------------
    clock_generator(clk40, clock_ena, C_CLK_PERIOD_40, "40 MHz clock");
    clock_generator(clk160, clock_ena, C_CLK_PERIOD_160, "160 MHz clock");

    reset_proc: process
    begin
        reset <= '1';
        wait for C_CLK_PERIOD_40*26;
        reset <= '0';
        wait;
    end process;

    datagen_1b_proc: process
        variable idle: integer;
    begin

        if reset = '0' then
            for packet in 1 to 100 loop --increment packet loop;
                for byte in 0 to packet-1 loop
                    DataIn1b <= std_logic_vector(to_unsigned(byte,8));
                    DataInValid1b <= "1";
                    EOP1b <= "0";
                    wait until rising_edge(clk40);
                end loop;
                idle := random(3,10);
                for idlecnt in 0 to idle-1 loop
                    DataIn1b <= (others => '0');
                    if idlecnt = 0 then
                        EOP1b <= "1";
                    else
                        EOP1b <= "0";
                    end if;
                    DataInValid1b <= "0";
                    wait until rising_edge(clk40);
                end loop;
            end loop;
        else
            DataIn1b <= (others => '0');
            EOP1b <= "0";
            DataInValid1b <= "0";
            SOB1b <= "0";
            EOB1b <= "0";
            TruncateIn1b <= "0";
            wait until rising_edge(clk40);
        end if;
    end process;

    datagen_2b_proc: process
        variable idle: integer;
        variable bytecnt: integer range 0 to 1;
    begin

        if reset = '0' then
            for packet in 1 to 100 loop --increment packet loop;
                for byte in 0 to packet-1 loop
                    DataIn2b(bytecnt*8+7 downto bytecnt*8) <= std_logic_vector(to_unsigned(byte,8));
                    DataInValid2b(bytecnt) <= '1';
                    EOP2b(bytecnt) <= '0';
                    if bytecnt = 1 then
                        wait until rising_edge(clk40);
                        bytecnt := 0;
                    else
                        bytecnt := 1;
                    end if;
                end loop;
                idle := random(3,10);
                for idlecnt in 0 to idle-1 loop
                    DataIn2b(bytecnt*8+7 downto bytecnt*8) <= x"00";
                    if idlecnt = 0 then
                        EOP2b(bytecnt) <= '1';
                    else
                        EOP2b(bytecnt) <= '0';
                    end if;
                    DataInValid2b(bytecnt) <= '0';
                    if bytecnt = 1 then
                        wait until rising_edge(clk40);
                        bytecnt := 0;
                    else
                        bytecnt := 1;
                    end if;
                end loop;
            end loop;
        else
            DataIn2b <= x"0000";
            EOP2b <= "00";
            DataInValid2b <= "00";
            SOB2b <= "00";
            EOB2b <= "00";
            TruncateIn2b <= "00";
            bytecnt := 0;
            wait until rising_edge(clk40);
        end if;
    end process;


    datagen_4b_proc: process
        variable idle: integer;
        variable bytecnt: integer range 0 to 3;
    begin

        if reset = '0' then
            for packet in 1 to 100 loop --increment packet loop;
                for byte in 0 to packet-1 loop
                    DataIn4b(bytecnt*8+7 downto bytecnt*8) <= std_logic_vector(to_unsigned(byte,8));
                    DataInValid4b(bytecnt) <= '1';
                    EOP4b(bytecnt) <= '0';
                    if bytecnt = 3 then
                        wait until rising_edge(clk40);
                        bytecnt := 0;
                    else
                        bytecnt := bytecnt + 1;
                    end if;
                end loop;
                idle := random(3,10);
                for idlecnt in 0 to idle-1 loop
                    DataIn4b(bytecnt*8+7 downto bytecnt*8) <= x"00";
                    if idlecnt = 0 then
                        EOP4b(bytecnt) <= '1';
                    else
                        EOP4b(bytecnt) <= '0';
                    end if;
                    DataInValid4b(bytecnt) <= '0';
                    if bytecnt = 3 then
                        wait until rising_edge(clk40);
                        bytecnt := 0;
                    else
                        bytecnt := bytecnt + 1;
                    end if;
                end loop;
            end loop;
        else
            DataIn4b <= x"00000000";
            EOP4b <= "0000";
            DataInValid4b <= "0000";
            SOB4b <= "0000";
            EOB4b <= "0000";
            TruncateIn4b <= "0000";
            bytecnt := 0;
            wait until rising_edge(clk40);
        end if;
    end process;

    axi_stream_vvc_1b: entity bitvis_vip_axistream.axistream_vvc
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => false,
            GC_DATA_WIDTH => 32,
            GC_USER_WIDTH => 4,
            GC_ID_WIDTH => 1,
            GC_DEST_WIDTH => 1,
            GC_INSTANCE_IDX => 0,
            GC_AXISTREAM_BFM_CONFIG => GC_AXISTREAM_BFM_CONFIG
        )
        port map (
            clk => clk160,
            axistream_vvc_if => axistream_vvc_if_1b
        );

    axistream_vvc_if_1b.tkeep <= m_axis1b.tkeep;
    axistream_vvc_if_1b.tuser <= m_axis1b.tuser;
    axistream_vvc_if_1b.tdata <= m_axis1b.tdata;
    axistream_vvc_if_1b.tvalid <= m_axis1b.tvalid;
    axistream_vvc_if_1b.tlast <= m_axis1b.tlast;
    m_axis_tready_1b <= axistream_vvc_if_1b.tready;

    axi_stream_vvc_2b: entity bitvis_vip_axistream.axistream_vvc
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => false,
            GC_DATA_WIDTH => 32,
            GC_USER_WIDTH => 4,
            GC_ID_WIDTH => 1,
            GC_DEST_WIDTH => 1,
            GC_INSTANCE_IDX => 1,
            GC_AXISTREAM_BFM_CONFIG => GC_AXISTREAM_BFM_CONFIG
        )
        port map (
            clk => clk160,
            axistream_vvc_if => axistream_vvc_if_2b
        );

    axistream_vvc_if_2b.tkeep <= m_axis2b.tkeep;
    axistream_vvc_if_2b.tuser <= m_axis2b.tuser;
    axistream_vvc_if_2b.tdata <= m_axis2b.tdata;
    axistream_vvc_if_2b.tvalid <= m_axis2b.tvalid;
    axistream_vvc_if_2b.tlast <= m_axis2b.tlast;
    m_axis_tready_2b <= axistream_vvc_if_2b.tready;

    axi_stream_vvc_4b: entity bitvis_vip_axistream.axistream_vvc
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => false,
            GC_DATA_WIDTH => 32,
            GC_USER_WIDTH => 4,
            GC_ID_WIDTH => 1,
            GC_DEST_WIDTH => 1,
            GC_INSTANCE_IDX => 2,
            GC_AXISTREAM_BFM_CONFIG => GC_AXISTREAM_BFM_CONFIG
        )
        port map (
            clk => clk160,
            axistream_vvc_if => axistream_vvc_if_4b
        );

    axistream_vvc_if_4b.tkeep <= m_axis4b.tkeep;
    axistream_vvc_if_4b.tuser <= m_axis4b.tuser;
    axistream_vvc_if_4b.tdata <= m_axis4b.tdata;
    axistream_vvc_if_4b.tvalid <= m_axis4b.tvalid;
    axistream_vvc_if_4b.tlast <= m_axis4b.tlast;
    m_axis_tready_4b <= axistream_vvc_if_4b.tready;




    ------------------------------------------------
    -- PROCESS: p_main
    ------------------------------------------------
    p_main: process
        constant C_SCOPE     : string  := C_TB_SCOPE_DEFAULT;
        variable testdata : t_byte_array(0 to 99);
    begin
        for i in 0 to 99 loop
            testdata(i) := std_logic_vector(to_unsigned(i,8));
        end loop;
        -- Print the configuration to the log
        clk40_stable <= '0';
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);
        enable_log_msg(ALL_MESSAGES);
        disable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        -- Wait for UVVM to finish initialization
        await_uvvm_initialization(VOID);
        for i in 0 to 2 loop --Link num +2 (-1) for AUX axistreams
            shared_axistream_vvc_config(i).bfm_config.max_wait_cycles := 10000;
            shared_axistream_vvc_config(i).bfm_config.max_wait_cycles_severity := ERROR;
            shared_axistream_vvc_config(i).bfm_config.clock_margin_severity := WARNING;
            shared_axistream_vvc_config(i).bfm_config.clock_period := C_CLK_PERIOD_160;
            shared_axistream_vvc_config(i).bfm_config.setup_time := C_CLK_PERIOD_160/4;
            shared_axistream_vvc_config(i).bfm_config.hold_time := C_CLK_PERIOD_160/4;
            shared_axistream_vvc_config(i).bfm_config.clock_period_margin := 0 ns;
            shared_axistream_vvc_config(i).bfm_config.bfm_sync := SYNC_WITH_SETUP_AND_HOLD;
            shared_axistream_vvc_config(i).bfm_config.match_strictness := MATCH_EXACT;
            shared_axistream_vvc_config(i).bfm_config.byte_endianness := FIRST_BYTE_LEFT;
            shared_axistream_vvc_config(i).bfm_config.check_packet_length := true;
            shared_axistream_vvc_config(i).bfm_config.protocol_error_severity := ERROR;
            shared_axistream_vvc_config(i).bfm_config.ready_low_at_word_num := 0;
            shared_axistream_vvc_config(i).bfm_config.ready_default_value := '1';
            shared_axistream_vvc_config(i).bfm_config.ready_low_duration := 0;

        end loop;
        clock_ena <= true;
        clk40_stable <= '1';

        log(ID_LOG_HDR, "Simulation of TB for ByteToAxiStream", C_SCOPE);
        ------------------------------------------------------------
        m_axis_tready_1b <= '1';
        m_axis_tready_2b <= '1';
        m_axis_tready_4b <= '1';
        for i in 0 to 99 loop
            axistream_expect_bytes(AXISTREAM_VVCT, 0, testdata(0 to i),  "expecting axi stream data on 1b interface"); --@suppress
            axistream_expect_bytes(AXISTREAM_VVCT, 1, testdata(0 to i),  "expecting axi stream data on 2b interface"); --@suppress
            axistream_expect_bytes(AXISTREAM_VVCT, 2, testdata(0 to i),  "expecting axi stream data on 4b interface"); --@suppress
        end loop;
        for i in 0 to 99 loop
            axistream_expect_bytes(AXISTREAM_VVCT, 1, testdata(0 to i),  "expecting axi stream data on 2b interface"); --@suppress
            axistream_expect_bytes(AXISTREAM_VVCT, 2, testdata(0 to i),  "expecting axi stream data on 4b interface"); --@suppress
        end loop;
        for i in 0 to 99 loop
            axistream_expect_bytes(AXISTREAM_VVCT, 2, testdata(0 to i),  "expecting axi stream data on 4b interface"); --@suppress
        end loop;
        for i in 0 to 99 loop
            axistream_expect_bytes(AXISTREAM_VVCT, 2, testdata(0 to i),  "expecting axi stream data on 4b interface"); --@suppress
        end loop;
        --wait for 500 us;
        await_completion(AXISTREAM_VVCT,0, 1 ms, "Wait for receive to finish on 1b interface"); --@suppress
        await_completion(AXISTREAM_VVCT,1, 1 ms, "Wait for receive to finish on 2b interface"); --@suppress
        await_completion(AXISTREAM_VVCT,2, 1 ms, "Wait for receive to finish on 4b interface"); --@suppress
        --==================================================================================================
        -- Ending the simulation
        --------------------------------------------------------------------------------------
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        -- Finish the simulation
        std.env.stop;
        wait;  -- to stop completely

    end process p_main;

    resetmgr0: entity work.CRresetManager
        port map(
            clk40         => clk40,
            rst           => reset,
            clk40_stable  => clk40_stable,
            cr_rst        => daq_reset,
            cr_fifo_flush => daq_fifo_flush
        );

    uut1b: entity work.ByteToAxiStream
        generic map(
            BYTES => 1,
            BLOCKSIZE => 1024,
            USE_BUILT_IN_FIFO => '0',
            VERSAL => false
        )
        port map(
            clk40 => clk40,
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            EnableIn => '1',
            DataIn => DataIn1b,
            DataInValid => DataInValid1b,
            GearboxValid => '1',
            ElinkWidth => "010",
            EOP => EOP1b,
            SOB => SOB1b,
            EOB => EOB1b,
            TruncateIn => TruncateIn1b,
            CodingErrorIn => '0',
            FramingErrorIn => '0',
            m_axis => m_axis1b,
            m_axis_tready => m_axis_tready_1b,
            m_axis_aclk => clk160,
            m_axis_prog_empty => m_axis_prog_empty1b
        );

    uut2b: entity work.ByteToAxiStream
        generic map(
            BYTES => 2,
            BLOCKSIZE => 1024,
            USE_BUILT_IN_FIFO => '0',
            VERSAL => false
        )
        port map(
            clk40 => clk40,
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            EnableIn => '1',
            DataIn => DataIn2b,
            DataInValid => DataInValid2b,
            GearboxValid => '1',
            ElinkWidth => "011",
            EOP => EOP2b,
            SOB => SOB2b,
            EOB => EOB2b,
            TruncateIn => TruncateIn2b,
            CodingErrorIn => '0',
            FramingErrorIn => '0',
            m_axis => m_axis2b,
            m_axis_tready => m_axis_tready_2b,
            m_axis_aclk => clk160,
            m_axis_prog_empty => m_axis_prog_empty2b
        );

    uut4b: entity work.ByteToAxiStream
        generic map(
            BYTES => 4,
            BLOCKSIZE => 1024,
            USE_BUILT_IN_FIFO => '0',
            VERSAL => false
        )
        port map(
            clk40 => clk40,
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            EnableIn => '1',
            DataIn => DataIn4b,
            DataInValid => DataInValid4b,
            GearboxValid => '1',
            ElinkWidth => "100",
            EOP => EOP4b,
            SOB => SOB4b,
            EOB => EOB4b,
            TruncateIn => TruncateIn4b,
            CodingErrorIn => '0',
            FramingErrorIn => '0',
            m_axis => m_axis4b,
            m_axis_tready => m_axis_tready_4b,
            m_axis_aclk => clk160,
            m_axis_prog_empty => m_axis_prog_empty4b
        );


end func;

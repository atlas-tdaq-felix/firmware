--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;

    use std.env.all;
    use work.axi_stream_package.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

library bitvis_vip_axistream;
    use bitvis_vip_axistream.axistream_bfm_pkg.all;
    use bitvis_vip_axistream.vvc_methods_pkg.all;
    context bitvis_vip_axistream.vvc_context;
library bitvis_vip_scoreboard; -- @suppress "Library 'bitvis_vip_scoreboard' is not available"
library uvvm_vvc_framework;
    use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

entity DecEgroup_8b10b_Strips_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end DecEgroup_8b10b_Strips_tb;

architecture tb of DecEgroup_8b10b_Strips_tb is
    -- Interrupt related signals
    signal clock_ena     : boolean   := false;
    signal ElinkWidth : std_logic_vector(2 downto 0);

    constant C_CLK_PERIOD_40     : time := 25 ns;
    constant C_CLK_PERIOD_80     : time := 12.5 ns;
    constant C_CLK_PERIOD_160     : time := 6.25 ns;

    signal clk40, clk80, clk160: std_logic;
    signal reset: std_logic;

    signal DataIn32b : std_logic_vector(31 downto 0);
    signal ElinkAligned: std_logic_vector(3 downto 0);
    signal m_axis: axis_32_array_type(0 to 3);
    signal m_axis_tready: axis_tready_array_type(0 to 3);
    signal m_axis_tready2: axis_tready_array_type(0 to 3);
    signal flowcontrol_override : std_logic;
    signal m_axis_prog_empty: axis_tready_array_type(0 to 3); -- @suppress "signal m_axis_prog_empty is never read"
    signal MessageLength: integer;
    signal AlignmentPulseAlign, AlignmentPulseDeAlign: std_logic;
    signal StartFrameGen: std_logic := '0';
    signal NumberOfMessages: integer;

    signal axistream_vvc_if1 : t_axistream_if(tdata(31 downto 0), tkeep(3 downto 0), tuser(3 downto 0), tstrb(3 downto 0), tid(0 downto 0), tdest(0 downto 0)) := init_axistream_if_signals(false, 32, 4, 1, 1);
    signal axistream_vvc_if2 : t_axistream_if(tdata(31 downto 0), tkeep(3 downto 0), tuser(3 downto 0), tstrb(3 downto 0), tid(0 downto 0), tdest(0 downto 0)) := init_axistream_if_signals(false, 32, 4, 1, 1);
    signal axistream_vvc_if3 : t_axistream_if(tdata(31 downto 0), tkeep(3 downto 0), tuser(3 downto 0), tstrb(3 downto 0), tid(0 downto 0), tdest(0 downto 0)) := init_axistream_if_signals(false, 32, 4, 1, 1);
    signal axistream_vvc_if4 : t_axistream_if(tdata(31 downto 0), tkeep(3 downto 0), tuser(3 downto 0), tstrb(3 downto 0), tid(0 downto 0), tdest(0 downto 0)) := init_axistream_if_signals(false, 32, 4, 1, 1);

    constant GC_AXISTREAM_BFM_CONFIG : t_axistream_bfm_config := C_AXISTREAM_BFM_CONFIG_DEFAULT;
    signal IDLES: integer := 0;
    signal clk40_stable : std_logic;
    signal daq_reset : std_logic;
    signal daq_fifo_flush : std_logic;

    signal direct_mode_en : std_logic := '0';
    signal PathEncoding : std_logic_vector(15 downto 0);
    signal tick_1us : std_logic;
    signal tick_counter_rst : std_logic := '0';
    signal tick_counter : unsigned(5 downto 0) := "000000";
    signal dm_start_mask : std_logic_vector(15 downto 0) := x"0000";
    signal dm_capture_length : std_logic_vector(7 downto 0) := x"00";
    signal strips_framegen_en : std_logic;

    constant DIRECT_MODE_TEST_MODE : integer := 1; --not for CI, disable (set 0) before push

begin
    PathEncoding <= x"0000" when (direct_mode_en = '1') else
                    x"1111";  --8b10b mode

    -- Instantiate the concurrent procedure that initializes UVVM
    i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

    -----------------------------------------------------------------------------
    -- Clock Generator
    -----------------------------------------------------------------------------
    clock_generator(clk40, clock_ena, C_CLK_PERIOD_40, "40 MHz clock");
    clock_generator(clk80, clock_ena, C_CLK_PERIOD_80, "80 MHz clock");
    clock_generator(clk160, clock_ena, C_CLK_PERIOD_160, "160 MHz clock");

    prc_tick_1us : process(clk40)
    begin
        if rising_edge(clk40) then
            tick_counter <= tick_counter + 1;
            if (tick_counter_rst = '1') or (tick_1us = '1') then
                tick_counter <= "000000";
            end if;
        end if;
    end process;
    tick_1us <= '1' when (tick_counter = "100111") else '0';  --39


    axi_stream_vvc_1: entity bitvis_vip_axistream.axistream_vvc
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => false,
            GC_DATA_WIDTH => 32,
            GC_USER_WIDTH => 4,
            GC_ID_WIDTH => 1,
            GC_DEST_WIDTH => 1,
            GC_INSTANCE_IDX => 0,
            GC_AXISTREAM_BFM_CONFIG => GC_AXISTREAM_BFM_CONFIG
        )
        port map (
            clk => clk160,
            axistream_vvc_if => axistream_vvc_if1
        );

    axistream_vvc_if1.tkeep <= m_axis(0).tkeep;
    axistream_vvc_if1.tuser <= m_axis(0).tuser;
    axistream_vvc_if1.tdata <= m_axis(0).tdata;
    axistream_vvc_if1.tvalid <= m_axis(0).tvalid;
    axistream_vvc_if1.tlast <= m_axis(0).tlast;
    m_axis_tready(0) <= axistream_vvc_if1.tready;

    axi_stream_vvc_2: entity bitvis_vip_axistream.axistream_vvc
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => false,
            GC_DATA_WIDTH => 32,
            GC_USER_WIDTH => 4,
            GC_ID_WIDTH => 1,
            GC_DEST_WIDTH => 1,
            GC_INSTANCE_IDX => 1,
            GC_AXISTREAM_BFM_CONFIG => GC_AXISTREAM_BFM_CONFIG
        )
        port map (
            clk => clk160,
            axistream_vvc_if => axistream_vvc_if2
        );

    axistream_vvc_if2.tkeep <= m_axis(1).tkeep;
    axistream_vvc_if2.tuser <= m_axis(1).tuser;
    axistream_vvc_if2.tdata <= m_axis(1).tdata;
    axistream_vvc_if2.tvalid <= m_axis(1).tvalid;
    axistream_vvc_if2.tlast <= m_axis(1).tlast;
    m_axis_tready(1) <= axistream_vvc_if2.tready;

    axi_stream_vvc_3: entity bitvis_vip_axistream.axistream_vvc
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => false,
            GC_DATA_WIDTH => 32,
            GC_USER_WIDTH => 4,
            GC_ID_WIDTH => 1,
            GC_DEST_WIDTH => 1,
            GC_INSTANCE_IDX => 2,
            GC_AXISTREAM_BFM_CONFIG => GC_AXISTREAM_BFM_CONFIG
        )
        port map (
            clk => clk160,
            axistream_vvc_if => axistream_vvc_if3
        );

    axistream_vvc_if3.tkeep <= m_axis(2).tkeep;
    axistream_vvc_if3.tuser <= m_axis(2).tuser;
    axistream_vvc_if3.tdata <= m_axis(2).tdata;
    axistream_vvc_if3.tvalid <= m_axis(2).tvalid;
    axistream_vvc_if3.tlast <= m_axis(2).tlast;
    m_axis_tready(2) <= axistream_vvc_if3.tready;

    axi_stream_vvc_4: entity bitvis_vip_axistream.axistream_vvc
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => false,
            GC_DATA_WIDTH => 32,
            GC_USER_WIDTH => 4,
            GC_ID_WIDTH => 1,
            GC_DEST_WIDTH => 1,
            GC_INSTANCE_IDX => 3,
            GC_AXISTREAM_BFM_CONFIG => GC_AXISTREAM_BFM_CONFIG
        )
        port map (
            clk => clk160,
            axistream_vvc_if => axistream_vvc_if4
        );

    axistream_vvc_if4.tkeep <= m_axis(3).tkeep;
    axistream_vvc_if4.tuser <= m_axis(3).tuser;
    axistream_vvc_if4.tdata <= m_axis(3).tdata;
    axistream_vvc_if4.tvalid <= m_axis(3).tvalid;
    axistream_vvc_if4.tlast <= m_axis(3).tlast;
    m_axis_tready(3) <= axistream_vvc_if4.tready;


    framegen0: entity work.DecEgroup_8b10b_framegen
        generic map(
            UseFELIXDataFormat => false -- Just use a counter value
        )port map(
            clk40 => clk40,
            clk80 => clk80,
            clk160 => clk160,
            reset => daq_reset,
            strips_mode_en => strips_framegen_en,
            direct_mode_en => direct_mode_en,
            IDLES => IDLES,
            MessageLength => MessageLength,
            ElinkWidth => ElinkWidth,
            DataOut32b => DataIn32b,
            StartFrameGen => StartFrameGen,
            NumberOfMessages => NumberOfMessages,
            HGTD_ALTIROC_ENCODING => '0'
        );

    ------------------------------------------------
    -- PROCESS: p_main
    ------------------------------------------------

    p_main: process
        constant C_SCOPE     : string  := C_TB_SCOPE_DEFAULT;
        variable testdata : t_byte_array(0 to 255);
        variable tdoffset : integer := 0;
        variable i : integer := 0;
        variable ml : integer := 0;

    begin
        flowcontrol_override <= '0';
        clk40_stable <= '0';
        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);
        enable_log_msg(ALL_MESSAGES);
        disable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        -- Wait for UVVM to finish initialization
        await_uvvm_initialization(VOID);
        clock_ena <= true;
        clk40_stable <= '1';
        reset <= '1'; wait for 25 ns; reset <= '0'; wait for 250 ns;

        for i in 0 to 3 loop
            shared_axistream_vvc_config(i).bfm_config.max_wait_cycles := 10000;
            shared_axistream_vvc_config(i).bfm_config.max_wait_cycles_severity := ERROR;
            shared_axistream_vvc_config(i).bfm_config.clock_period := C_CLK_PERIOD_160;
            shared_axistream_vvc_config(i).bfm_config.setup_time := C_CLK_PERIOD_160/4;
            shared_axistream_vvc_config(i).bfm_config.hold_time  := C_CLK_PERIOD_160/4;
            shared_axistream_vvc_config(i).bfm_config.clock_period_margin := 0 ns;
            shared_axistream_vvc_config(i).bfm_config.bfm_sync := SYNC_WITH_SETUP_AND_HOLD;
            shared_axistream_vvc_config(i).bfm_config.match_strictness := MATCH_EXACT;
            shared_axistream_vvc_config(i).bfm_config.byte_endianness := FIRST_BYTE_LEFT;
            shared_axistream_vvc_config(i).bfm_config.check_packet_length := false;
            shared_axistream_vvc_config(i).bfm_config.protocol_error_severity := ERROR;
        end loop;

        -------------------------------------------------------------------------------
        log(ID_LOG_HDR, "Simulating 8b10b decoder properly with counter-data and checks", C_SCOPE);
        -------------------------------------------------------------------------------
        direct_mode_en <= '0'; --set 8b10b decoded mode
        strips_framegen_en <= '0';

        for i in 0 to 99 loop
            testdata(i) := std_logic_vector(to_unsigned(i,8));
        end loop;
        NumberOfMessages <= 5;

        reset <= '1'; wait for 25 ns; reset <= '0'; wait for 250 ns;

        for w in 0 to 1 loop
            case w is
                when 0 =>
                    ElinkWidth <= "010"; --8b.
                    IDLES <= 0;
                when 1 =>
                    ElinkWidth <= "011"; --16b.
                    IDLES <= 0;
            --when 2 =>
            --    ElinkWidth <= "100"; --32b.
            --    IDLES <= 1; --At least one IDLE is needed in between EOP/SOP to avoid congestion in ByteToAxiStream
            end case;
            reset <= '1'; wait for 25 ns; reset <= '0'; wait for 250 ns;

            if ElinkAligned /= "0000" then
                await_value(ElinkAligned, "0000",0 ps, 1 ms, "Waiting for decoder to misalign");
            end if;
            case w is
                when 0 =>
                    --await_value(ElinkAligned, "1111",0 ps, 1 ms, "Waiting for decoder to align (320Mb/s)");
                    await_value(ElinkAligned, "0101",0 ps, 1 ms, "Waiting for decoder to align (320Mb/s)");
                when 1 =>
                    await_value(ElinkAligned, "0101",0 ps, 1 ms, "Waiting for decoder to align (640Mb/s)");
            --when 2 =>
            --    await_value(ElinkAligned, "0001",0 ps, 1 ms, "Waiting for decoder to align (1280Mb/s)");
            end case;

            for ml in 10 to 20 loop
                MessageLength <= ml + 1;
                wait until rising_edge(clk40); -- ensure loop starts aligned with the clock
                wait for 10 ps;                --   add a little to make sure sigs are pushed off the clock boundary
                StartFrameGen <= '1';
                for i in 0 to NumberOfMessages-1 loop
                    axistream_expect_bytes(AXISTREAM_VVCT, 0, testdata(0 to ml),  "expecting axi stream data on e-link 0 interface"); --@suppress
                    if ElinkWidth = "011" or ElinkWidth = "010" then
                        axistream_expect_bytes(AXISTREAM_VVCT, 2, testdata(0 to ml),  "expecting axi stream data on e-link 2 interface"); --@suppress
                    end if;
                --if ElinkWidth = "010" then
                --   axistream_expect_bytes(AXISTREAM_VVCT, 1, testdata(0 to ml),  "expecting axi stream data on 2nd interface"); --@suppress
                --   axistream_expect_bytes(AXISTREAM_VVCT, 3, testdata(0 to ml),  "expecting axi stream data on 4th interface"); --@suppress
                --   wait for 100 ns;
                --end if;
                end loop;

                await_completion(AXISTREAM_VVCT,0, 1 ms, "Wait for receive to finish on e-link 0 interface"); --@suppress
                if ElinkWidth = "011" or ElinkWidth = "010" then
                    await_completion(AXISTREAM_VVCT,2, 1 ms, "Wait for receive to finish on e-link 2 interface"); --@suppress
                end if;
                --MRMWif ElinkWidth = "010" then
                --MRMW    await_completion(AXISTREAM_VVCT,1, 1 ms, "Wait for receive to finish on 2nd interface"); --@suppress
                --MRMW    await_completion(AXISTREAM_VVCT,3, 1 ms, "Wait for receive to finish on 4th interface"); --@suppress
                --MRMWend if;
                StartFrameGen <= '0';
                wait for 50 ns; --timing can move by this point - need to ensure the deassertion of StartFrameGen is seen
            end loop;
        end loop;

        wait for 2000 ns;
        if (1=2) then
            --------------------------------------------------------------------------------
            log(ID_LOG_HDR, "Simulating 8b10b decoder with stripsish data", C_SCOPE);
            --------------------------------------------------------------------------------
            direct_mode_en <= '0'; --set 8b10b decoded mode
            strips_framegen_en <= '1';
            IDLES <= 0;

            for i in 0 to 99 loop
                testdata(i) := std_logic_vector(to_unsigned(i,8));
            end loop;
            NumberOfMessages <= 15;

            reset <= '1'; wait for 25 ns; reset <= '0'; wait for 250 ns;

            for w in 0 to 1 loop
                case w is
                    when 0 => ElinkWidth <= "010"; --8b.
                    when 1 => ElinkWidth <= "011"; --16b.
                end case;
                reset <= '1'; wait for 25 ns; reset <= '0'; wait for 250 ns;

                if ElinkAligned /= "0000" then
                    await_value(ElinkAligned, "0000",0 ps, 1 ms, "Waiting for decoder to misalign");
                end if;

                await_value(ElinkAligned, "0101",0 ps, 1 ms, "Waiting for decoder to align");

                for ml in 8 to 16 loop
                    MessageLength <= ml + 1;
                    wait until rising_edge(clk40); -- ensure loop starts aligned with the clock
                    wait for 10 ps;                --   add a little to make sure sigs are pushed off the clock boundary
                    StartFrameGen <= '1';
                    for i in 0 to NumberOfMessages-1 loop
                        testdata(0) := std_logic_vector(to_unsigned(i,4)) & x"0"; -- matches datagen
                        axistream_expect_bytes(AXISTREAM_VVCT, 0, testdata(0 to ml),  "expecting axi stream data on e-link 0 interface"); --@suppress
                        axistream_expect_bytes(AXISTREAM_VVCT, 2, testdata(0 to ml),  "expecting axi stream data on e-link 2 interface"); --@suppress
                        if (i=3) or (i=8) or (i=16#d#) or (i=16#e#) then
                            axistream_expect_bytes(AXISTREAM_VVCT, 1, testdata(0 to ml),  "expecting axi stream DCS data on e-link 1 interface"); --@suppress
                            axistream_expect_bytes(AXISTREAM_VVCT, 3, testdata(0 to ml),  "expecting axi stream DCS data on e-link 3 interface"); --@suppress
                        end if;
                    end loop;

                    wait for NumberOfMessages * 200 ns;

                    await_completion(AXISTREAM_VVCT,0, 1 ms, "Wait for receive to finish on e-link 0 interface"); --@suppress
                    await_completion(AXISTREAM_VVCT,2, 1 ms, "Wait for receive to finish on e-link 2 interface"); --@suppress
                    -- DCS e-links:
                    await_completion(AXISTREAM_VVCT,1, 1 ms, "Wait for receive to finish on e-link 1 interface"); --@suppress
                    await_completion(AXISTREAM_VVCT,3, 1 ms, "Wait for receive to finish on e-link 3 interface"); --@suppress
                    StartFrameGen <= '0';
                    wait for 50 ns; --timing can move by this point - need to ensure the deassertion of StartFrameGen is seen
                end loop;
            end loop;

            wait for 2000 ns;
        end if;


        if (1=2) then -- This no longer works -- needs mods to properly capture sigs from 8b10b decoder
            log(ID_LOG_HDR, "Simulating some direct mode", C_SCOPE);
            ---------------------------------------------------
            -- This is very hacky - all manual alignment etc - not fun
            direct_mode_en <= '1';
            for w in 0 to 1 loop
                case w is
                    when 0 => ElinkWidth <= "010"; -- 8b
                        MessageLength <= 47;
                        tdoffset := 0;
                        testdata(0) := x"fa"; -- last idle byte
                        for n in 0 to 99 loop
                            testdata(n+1) := std_logic_vector(to_unsigned(n+tdoffset,8));
                        end loop;
                    when 1 => ElinkWidth <= "011"; --16b
                        MessageLength <= 98;
                        tdoffset := 0;
                        testdata(0) := x"14"; -- last idle bytes
                        testdata(1) := x"fa";  -- l
                        for n in 0 to 99 loop
                            testdata(n+2) := std_logic_vector(to_unsigned(n+tdoffset,8));
                        end loop;
                end case;

                NumberOfMessages <= 1;
                dm_capture_length <= x"01";

                reset <= '1'; wait for 100 ns; reset <= '0'; wait until daq_reset = '0';
                wait until rising_edge(clk40); wait for 10 ps; -- ensure aligned with the clock + offset from edge

                if ElinkAligned /= "0000" then
                    await_value(ElinkAligned, "0000", 0 ps, 1 ms, "Waiting for decoder to misalign");
                end if;
                wait for 250 ns;

                for n in 1 to 1 loop -- the timing jumps in the second loop - too painful to fix
                    wait until rising_edge(clk40); wait for 10 ps; -- ensure loop starts aligned with the clock + offset from edge
                    -- Need to wait for 8b/10b decoder to lock - and relock, because the sim data isn't valid 8b10b (its a counter):
                    case w is
                        when 0 => await_value(ElinkAligned, "1111", 0 ps, 1 ms, "Waiting for decoder to align (320Mb/s)");
                        when 1 => await_value(ElinkAligned, "0101", 0 ps, 1 ms, "Waiting for decoder to align (640Mb/s)");
                    end case;
                    wait for 250 ns;

                    wait until rising_edge(clk40); wait for 10 ps; -- ensure loop starts aligned with the clock + off edge
                    dm_start_mask <= x"0001"; --(0)=not Idle

                    tick_counter_rst <= '1', '0' after 50 ns ; -- reset counter for predicatable behavior

                    wait for 500 ns; -- reduce range by starting closer to then next tick
                    StartFrameGen <= '1';
                    wait for 25 ns;

                    ml := MessageLength-1-tdoffset;
                    --for i in 0 to NumberOfMessages-1 loop
                    axistream_expect_bytes(AXISTREAM_VVCT, 0, testdata(0 to ml),  "expecting axi stream data on 1st interface"); --@suppress
                    if ElinkWidth = "011" or ElinkWidth = "010" then
                        axistream_expect_bytes(AXISTREAM_VVCT, 2, testdata(0 to ml),  "expecting axi stream data on 3rd interface"); --@suppress
                    end if;
                    if ElinkWidth = "010" then
                        axistream_expect_bytes(AXISTREAM_VVCT, 1, testdata(0 to ml),  "expecting axi stream data on 2nd interface"); --@suppress
                        axistream_expect_bytes(AXISTREAM_VVCT, 3, testdata(0 to ml),  "expecting axi stream data on 4th interface"); --@suppress
                    end if;
                    --end loop;

                    await_completion(AXISTREAM_VVCT,0, 1 ms, "Wait for receive to finish on 1st interface"); --@suppress
                    if ElinkWidth = "011" or ElinkWidth = "010" then
                        await_completion(AXISTREAM_VVCT,2, 1 ms, "Wait for receive to finish on 3rd interface"); --@suppress
                    end if;
                    if ElinkWidth = "010" then
                        await_completion(AXISTREAM_VVCT,1, 1 ms, "Wait for receive to finish on 2nd interface"); --@suppress
                        await_completion(AXISTREAM_VVCT,3, 1 ms, "Wait for receive to finish on 4th interface"); --@suppress
                    end if;
                    wait for to_integer(unsigned(dm_capture_length))* 1 us;

                    StartFrameGen <= '0';
                    wait for 50 ns; --timing can move by this point - need to ensure the deassertion of StartFrameGen is seen

                end loop;
                dm_start_mask <= x"0000";
            end loop;
            direct_mode_en <= '0';

            wait for 2000 ns;
        end if;


        --====================================================================================================================
        -- Direct-mode test without checks -  just for visual waveform inspection
        -------------------------------------------------------------------------
        direct_mode_en <= '1';
        strips_framegen_en <= '0';

        --if (1=1) then
        log(ID_LOG_HDR, "Running direct mode test for waveform inspection only", C_SCOPE);
        ----------------------------------------------------------------------

        dm_capture_length <= x"01";
        dm_start_mask <= x"0000";

        flowcontrol_override <= '1';
        wait for 1000 ns;

        for w in 0 to 1 loop
            case w is
                when 0 =>  ElinkWidth <= "010"; --8b.
                when 1 =>  ElinkWidth <= "011"; --16b.
            end case;
            --reset <= '1'; wait for 100 ns; reset <= '0'; wait until daq_reset = '0';
            wait until rising_edge(clk40); wait for 10 ps; -- ensure aligned with the clock + offset from edge
            --if ElinkAligned /= "0000" then
            --    await_value(ElinkAligned, "0000",0 ps, 1 ms, "Waiting for decoder to misalign");
            --end if;
            -- need to wait for 8b/10b decoder to lock - and relock, because the sim data isn't valid 8b10b (its a counter):
            --case w is
            --    when 0 => await_value(ElinkAligned, "1111", 0 ps, 1 ms, "Waiting for decoder to align (320Mb/s)");
            --    when 1 => await_value(ElinkAligned, "0101", 0 ps, 1 ms, "Waiting for decoder to align (640Mb/s)");
            --end case;
            wait for 250 ns;
            dm_start_mask <= x"0001"; --(0)=not Idle
            wait for 250 ns;
            for n in 1 to 9 loop
                wait until rising_edge(clk40); wait for 10 ps; -- ensure loop starts aligned with the clock + off edge
                -- Need to wait for 8b/10b decoder to lock - and relock, because the sim data isn't valid 8b10b (its a counter):
                wait for 20 us;
                StartFrameGen <= '1';
                wait for 25 ns;
                wait for to_integer(unsigned(dm_capture_length))* 1 us + 1 us;
                StartFrameGen <= '0';
                wait for 10 us;
            end loop;
            dm_start_mask <= x"0000";
        end loop;
        --end if;
        ----------------------------------------------------------------------------------------------------------------------


        --==================================================================================================
        -- Ending the simulation
        --------------------------------------------------------------------------------------
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        -- Finish the simulation
        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely

    end process p_main;

    resetmgr0: entity work.CRresetManager
        port map(
            clk40         => clk40,
            rst           => reset,
            clk40_stable  => clk40_stable,
            cr_rst        => daq_reset,
            cr_fifo_flush => daq_fifo_flush
        );


    m_axis_tready2 <= m_axis_tready when (flowcontrol_override = '0') else (others => '1');

    uut: entity work.DecEgroup_8b10b
        generic map(
            BLOCKSIZE => 1024,
            Support32bWidth => '1',
            Support16bWidth => '1',
            Support8bWidth => '1',
            IncludeElinks => "0101",
            --IncludeStripsDCSELinks => '1',
            VERSAL => false
        )
        port map(
            clk40 => clk40,
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            tick_1us_i => tick_1us, --'0',
            dm_start_mask_i => dm_start_mask, --x"0000",
            dm_capture_length_i => dm_capture_length, --x"00",
            DataIn => DataIn32b,
            EgroupCounterClear => '0',
            EnableIn => "1111",
            LinkAligned => '1',
            ElinkWidth => ElinkWidth,
            AlignmentPulseAlign => AlignmentPulseAlign,
            AlignmentPulseDeAlign => AlignmentPulseDeAlign,
            AutoRealign => '1',
            RealignmentEvent => open,
            PathEncoding => PathEncoding,
            ElinkAligned => ElinkAligned,
            DecodingErrors => open,
            MsbFirst => '1',
            ReverseInputBits => "0000",
            HGTD_ALTIROC_DECODING => '0',
            FE_BUSY_out => open,
            m_axis => m_axis,
            m_axis_tready => m_axis_tready2,
            m_axis_aclk => clk160,
            m_axis_prog_empty => m_axis_prog_empty
        );

    pulsegen0: entity work.AlignmentPulseGen
        generic map(
            MAX_VAL_DEALIGN => 2048, --2048 bytes at 8b10b / 80 Mb/s elink.
            MAX_VAL_ALIGN => 10 --2 bytes at 8b10b / 80 Mb/s elink.
        )
        port map(
            clk40 => clk40,
            AlignmentPulseAlign => AlignmentPulseAlign,
            AlignmentPulseDeAlign => AlignmentPulseDeAlign
        );
end  architecture;

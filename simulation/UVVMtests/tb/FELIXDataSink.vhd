--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Filiberto Bonini
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--  ATLAS
--  FELIX UVVM test automation project
--  F. Schreuder (Nikhef)
----------------------------------------------------------------------------------
-- file: FELIXDataSink.vhd
----------------------------------------------------------------------------------
library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

    use work.pcie_package.all;
--use work.lpgbtfpga_package.all;
--use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.axi_stream_package.all;



-- Test bench entity
entity FELIXDataSink is
    generic(
        NUMBER_OF_DESCRIPTORS : integer := 2;
        BLOCKSIZE : integer :=4096;
        GBT_NUM : integer := 12;
        FIRMWARE_MODE : integer := FIRMWARE_MODE_FULL;
        DATA_WIDTH : integer := 256;
        SIM_NUMBER_OF_BLOCKS : integer := 1000;
        APPLY_BACKPRESSURE_AFTER_BLOCKS : integer := 100
    );
    port(
        checker_done         : out std_logic;
        toHostFifo_din       : in slv_array(0 to NUMBER_OF_DESCRIPTORS-2);
        toHostFifo_wr_en     : in std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
        toHostFifo_prog_full : out std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
        toHostFifo_wr_clk    : in std_logic;
        reset                : in std_logic;
        start_check          : out std_logic;
        Trunc                : out std_logic;
        TrailerError         : out std_logic;
        CRCError             : out std_logic;
        ChunkLengthError     : out std_logic;
        register_map_control : in register_map_control_type
    );
end entity;

-- Test bench architecture
architecture arch of FELIXDataSink is

    function to_dstring (a : natural; d : integer range 1 to 9) return string is
        constant vString : string(1 to d) := (others => ' ');
    begin
        if(a >= 10**d) then
            return integer'image(a);
        else
            return vString(1 to vString'length-integer'image(a)'length) & integer'image(a);
        end if;
    end function;

    constant MAX_CHUNK_LENGTH: integer := 2048;

    constant STREAMS_TOHOST: integer := STREAMS_TOHOST_MODE(FIRMWARE_MODE)+1;
    constant HeaderReferenceValue : std_logic_vector(15 downto 0) := "11"&std_logic_vector(to_unsigned((BLOCKSIZE/1024)-1,6))&x"CF";
    --==================================Egroup_reader
    type slv16_array is array (natural range <>) of std_logic_vector(15 downto 0);

    signal backpressure_applied : std_logic;
    signal checker_done_arr: std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
    signal backpressure_applied_arr: std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);

begin

    backpressure_proc: process
    begin
        toHostFifo_prog_full <= (others => '0');
        wait for 40 us;
        toHostFifo_prog_full <= (others => backpressure_applied);
        wait for 40 us;
    end process;

    backpressure_applied <= or backpressure_applied_arr;

    gDescriptors: for descriptor in 0 to NUMBER_OF_DESCRIPTORS-2 generate
        constant C_SCOPE : string := "FELIXDataSink_Descr"&to_string(descriptor);
        signal block_data: slv16_array(0 to BLOCKSIZE/2-1);
        type Bool2DArray is array (natural range<>, natural range <>) of boolean;

        signal ReceivingState_s: Bool2DArray(0 to GBT_NUM, 0 to STREAMS_TOHOST-1); -- @suppress "signal ReceivingState_s is never read"

    begin

        checker: process
            type ByteArray is array (natural range<>) of std_logic_vector(7 downto 0);
            type Chunk2DArray is array (natural range<>, natural range<>) of ByteArray(0 to MAX_CHUNK_LENGTH-1);
            variable chunk_data : ByteArray(0 to MAX_CHUNK_LENGTH-1);
            variable unfinished_chunk_data : Chunk2DArray(0 to GBT_NUM, 0 to STREAMS_TOHOST-1); --Store the data of an unfinished chunk to check / print if when it is finished later in the next block.
            type Int2DArray is array (natural range<>, natural range<>) of integer;


            variable ReceivingState: Bool2DArray(0 to GBT_NUM, 0 to STREAMS_TOHOST-1);

            variable BlockCountArray : Int2DArray(0 to GBT_NUM, 0 to STREAMS_TOHOST-1);
            variable chunk_count: integer;
            variable chunk_length: integer;
            variable chunk_length_bytes, chunk_length_bytes_from_data: integer;
            variable unfinished_chunk_length : Int2DArray(0 to GBT_NUM, 0 to STREAMS_TOHOST-1);
            --variable unfinished_chunk_length_temp : integer range 0 to 1023;
            --variable chunk_index: IntArray(0 to 63);
            variable ElinkID : integer range 0 to STREAMS_TOHOST-1;
            variable GBTID   : integer;

            type SequenceArray is array (natural range<>, natural range<>) of std_logic_vector(4 downto 0);
            variable BlockSequence : SequenceArray(0 to GBT_NUM, 0 to STREAMS_TOHOST-1) := (others => (others => (others => '0')));
            variable BlockSequenceSet : Bool2DArray(0 to GBT_NUM, 0 to STREAMS_TOHOST-1) := (others => (others => false));

            --variable checking: boolean;
            variable trailer: std_logic_vector(31 downto 0);
            variable TrailerType: std_logic_vector(2 downto 0):="000";
            variable progress : integer := 100;
            variable Trunc_v: std_logic;
            variable Error_v: std_logic;
            variable CRCError_v: std_logic;
            variable flags_v : std_logic_vector(2 downto 0);


            pure function check_chunk (data : ByteArray(0 to MAX_CHUNK_LENGTH-1); len : integer) return integer is
                variable chunkString: string(1 to BLOCKSIZE*8);
                variable length_from_data : integer;
                variable length_from_data_slv: std_logic_vector(15 downto 0);
                variable L1ID : std_logic_vector(15 downto 0);
                variable ElinkWidth: integer;
                variable ContainsError: boolean;
            begin
                --check_value(len, 1020, ERROR, "Chunk length must be exactly 1020 bytes", C_SCOPE);
                ContainsError := false;
                check_value(data(0), x"AA",  WARNING, "Chunk byte 0 must be AA", C_SCOPE);
                if data(0) /= x"AA" or data(5) /= x"BB" or data(6) /= x"AA" then
                    ContainsError := true;
                end if;
                length_from_data_slv := data(1) & data(2);
                length_from_data := to_integer(unsigned(length_from_data_slv));
                L1ID := data(3) & data(4);
                check_value(data(5), x"BB",  WARNING, "Chunk byte 5 must be BB", C_SCOPE);
                check_value(data(6), x"AA",  WARNING, "Chunk byte 6 must be AA", C_SCOPE);
                for i in 0 to length_from_data-1 loop
                    check_value(data(i+8), std_logic_vector(to_unsigned(i mod 256,8)),WARNING, "Chunk payload must be a counter value", C_SCOPE);
                    if data(i+8) /= std_logic_vector(to_unsigned(i mod 256,8)) then
                        ContainsError := true;
                        exit; --break from for loop
                    end if;

                end loop;
                ElinkWidth := to_integer(unsigned(data(7)));
                chunkString := (others => ' ');
                for i in 0 to len-1 loop
                    chunkString(i*3+1 to i*3+3) := to_hstring(data(i))&" ";
                end loop;
                if ContainsError then
                    log(ID_SEQUENCER, string'("length: "&to_string(length_from_data)&" L1ID: "&to_hstring(L1ID)&" ElinkWidth: "&to_string(ElinkWidth)));
                    log(ID_SEQUENCER, string'(" raw chunk data:"&chunkString(1 to len*3)));
                    check_value(ContainsError, false, ERROR, "Chunk should not contain an error", C_SCOPE);
                end if;
                return length_from_data + 8; --Header bytes not included in length_from_data.
            end function;
        begin
            while(true) loop
                Trunc        <= '0';
                TrailerError <= '0';
                CRCError     <= '0';
                ChunkLengthError <='0';
                backpressure_applied_arr(descriptor) <= '0';
                start_check <= '0';
                checker_done_arr(descriptor) <= '0';
                BlockSequence := (others => (others => (others => '0')));
                BlockSequenceSet := (others => (others => false));
                --wait for 1 ns;
                log("Wait 10 clock period for reset to be turned off");
                --await_value(reset, '1', 0 ns, 250 ns, TB_ERROR, "await assertion of reset", C_SCOPE);
                await_value(reset, '0', 0 ns, 10 us, TB_ERROR, "await deassertion of reset", C_SCOPE);
                --wait for 25 ns;

                log(ID_LOG_HDR, "Check Egroup block generation...", C_SCOPE);


                chunk_length := 0;
                for i in 0 to GBT_NUM loop
                    for j in 0 to STREAMS_TOHOST-1 loop
                        unfinished_chunk_length(i,j) := 0;
                        BlockCountArray(i,j) := 0;
                        ReceivingState(i,j) := false;
                    end loop;
                end loop;
                --chunk_index := (others => 0);
                for k in 0 to SIM_NUMBER_OF_BLOCKS-1 loop --Number of blocks to check

                    if k > APPLY_BACKPRESSURE_AFTER_BLOCKS then
                        backpressure_applied_arr(descriptor) <= '1'; --start acting like FIFO is full, expect truncation
                    end if;
                    await_value(toHostFifo_wr_en(descriptor), '1', 0 ns, 3 ms, TB_ERROR, "wait for block to be written", C_SCOPE);
                    --wait_num_rising_edge(toHostFifo_wr_clk, 1);
                    for word in 0 to ((8*BLOCKSIZE)/DATA_WIDTH)-1 loop
                        for i in 0 to (DATA_WIDTH/16)-1 loop
                            block_data(i+word*(DATA_WIDTH/16)) <= toHostFifo_din(descriptor)(i*16+15 downto i*16);
                        end loop;
                        if toHostFifo_wr_en(descriptor) /= '1' then
                            check_value(toHostFifo_wr_en(descriptor), '1',  ERROR, "Within 1 block, wr_en should stay high", C_SCOPE);
                        end if;
                        wait_num_rising_edge(toHostFifo_wr_clk, 1);
                    end loop;
                    wait_num_rising_edge(toHostFifo_wr_clk, 1);
                    if toHostFifo_wr_en(descriptor) /= '0' then
                        check_value(toHostFifo_wr_en(descriptor), '0',  ERROR, "After a 1 kb block, wr_en should go low", C_SCOPE);
                    end if;

                    if block_data(1) /= HeaderReferenceValue then
                        check_value(block_data(1), HeaderReferenceValue, ERROR, "Invalid block header", C_SCOPE);
                    end if;

                    ElinkID := to_integer(unsigned(block_data(0)(5 downto 0)));
                    GBTID   := to_integer(unsigned(block_data(0)(10 downto 6)));
                    if(GBTID = 24) then --AUX channel
                        GBTID := GBT_NUM;
                    end if;

                    -- Check if block sequence numbers are incrementing correctly
                    if BlockSequenceSet(GBTID, ElinkID) then
                        check_value( to_integer(unsigned(block_data(0)(15 downto 11))), to_integer(unsigned(BlockSequence(GBTID, ElinkID)) + 1), ERROR, "The block sequence was broken!", C_SCOPE);
                    end if;
                    BlockSequenceSet(GBTID, ElinkID) := true;
                    BlockSequence(GBTID, ElinkID) := block_data(0)(15 downto 11);

                    BlockCountArray(GBTID, ElinkID) := BlockCountArray(GBTID, ElinkID) + 1;  --Count number of blocks per E-link.


                    --Loop for every chunk, starting from the chunk header.

                    chunk_count := 2;
                    loop --break out when block header reached
                        trailer := block_data(chunk_count+1) & block_data(chunk_count);
                        TrailerType := trailer(31 downto 29);
                        Trunc_v := trailer(28);
                        Error_v := trailer(27);
                        CRCError_v := trailer(26);

                        chunk_count := chunk_count + 2;
                        chunk_length := ((to_integer(unsigned(trailer(15 downto 0)))+3)/4)*2; -- in 16 bit words

                        Trunc <= Trunc_v;
                        CRCError <= CRCError_v;
                        TrailerError <= Error_v;

                        -- Check if there aren't any problems in the flags
                        flags_v := Trunc_v & Error_v & CRCError_v;
                        if flags_v /= "000" then
                            tb_warning(string'("chunk_count: "&integer'image(chunk_count)), C_SCOPE);
                            if backpressure_applied_arr(descriptor) = '1' then
                                check_value(Trunc_v, '0', WARNING, "Truncation, backpressure was applied");
                            elsif TrailerType = "101" then
                                check_value(Trunc_v, '0', WARNING, "Truncation, with timeout");
                            else
                                check_value(Trunc_v, '0', ERROR, "Truncation should not occur");
                            end if;
                            check_value(Error_v, '0', ERROR, "Chunk error should not occur");
                            check_value(CRCError_v, '0', ERROR, "CRC Error should not occur");
                        end if;

                        -- Check if something went wrong with the type of the chunk
                        case(TrailerType) is
                            when "000" => NULL;
                            when "001" => --First
                                if ReceivingState(GBTID,ElinkID) then
                                    tb_error("ReceivingState was true while getting first trailer type: chunk_count: "& integer'image(chunk_count), C_SCOPE);
                                end if;
                                ReceivingState(GBTID,ElinkID) := true;
                            when "010" => --Last
                                if ReceivingState(GBTID,ElinkID) = false then
                                    tb_error("ReceivingState was false while getting last trailer type: chunk_count: "& integer'image(chunk_count), C_SCOPE);
                                end if;
                                ReceivingState(GBTID,ElinkID) := false;
                            when "011" => --Both
                                if ReceivingState(GBTID,ElinkID) then
                                    tb_error("ReceivingState was true while getting both trailer type: chunk_count: "& integer'image(chunk_count), C_SCOPE);
                                end if;
                                ReceivingState(GBTID,ElinkID) := false;
                            when "100" => --Middle
                                if ReceivingState(GBTID,ElinkID) = false then
                                    tb_error("ReceivingState was false while getting middle trailer type: chunk_count: "& integer'image(chunk_count), C_SCOPE);
                                end if;
                                ReceivingState(GBTID,ElinkID) := true;
                            when "101" => NULL;--Timeout
                            when "110" => NULL;
                            when "111" => NULL;--OOB
                            when others => NULL;

                        end case;
                        ReceivingState_s <= ReceivingState;


                        if TrailerType = "001" or TrailerType = "100" then --first part of a chunk or middle
                            if TrailerType = "001" then --Start new chunk, unfinished chunk length should be 0.
                                unfinished_chunk_length(GBTID,ElinkID) := 0;
                            end if;
                            --copy block data into unfinished_chunk_data
                            for i in 0 to chunk_length-1 loop
                                unfinished_chunk_data(GBTID,ElinkID)(unfinished_chunk_length(GBTID,ElinkID)*2+i*2)   := block_data((chunk_count) + i)(7 downto 0); --convert to bytes, block_data is per 16 bit
                                unfinished_chunk_data(GBTID,ElinkID)(unfinished_chunk_length(GBTID,ElinkID)*2+i*2+1) := block_data((chunk_count) + i)(15 downto 8);
                            end loop;
                            unfinished_chunk_length(GBTID,ElinkID) := unfinished_chunk_length(GBTID,ElinkID) + chunk_length; --block finished, store length of last chunk for processing in the next block of this ElinkID

                        end if;
                        if TrailerType = "101" and Trunc_v = '0' then --timeout
                            Null; --tb_warning("Block includes timeout trailer", C_SCOPE);
                        else
                            if TrailerType = "010" then
                                chunk_length_bytes := (unfinished_chunk_length(GBTID,ElinkID))*2+to_integer(unsigned(trailer(15 downto 0)));

                                --copy block data into unfinished_chunk_data_temp, until the complete block is handled and it can be stored in the 2D array.
                                chunk_data := unfinished_chunk_data(GBTID, ElinkID);

                                for i in 0 to chunk_length-1 loop
                                    chunk_data((i+unfinished_chunk_length(GBTID,ElinkID))*2)   := block_data((chunk_count) + i)(7 downto 0); --convert to bytes, block_data is per 16 bit
                                    chunk_data((i+unfinished_chunk_length(GBTID,ElinkID))*2+1) := block_data((chunk_count) + i)(15 downto 8);
                                end loop;
                            else
                                if TrailerType = "101" and Trunc_v = '1' then --timeout
                                    tb_warning("Block includes timeout trailer with Truncation", C_SCOPE);
                                end if;

                                chunk_length_bytes := to_integer(unsigned(trailer(15 downto 0)));

                                for i in 0 to chunk_length-1 loop
                                    chunk_data((i)*2)   := block_data((chunk_count) + i)(7 downto 0); --convert to bytes, block_data is per 16 bit
                                    chunk_data((i)*2+1) := block_data((chunk_count) + i)(15 downto 8);
                                end loop;
                            end if;
                            if chunk_length_bytes > 0 and TrailerType /= "001" and TrailerType /= "100" and Trunc_v = '0' then --(first (or middle) part of chunk will be checked on the next block)

                                chunk_length_bytes_from_data := check_chunk(chunk_data, chunk_length_bytes) * to_integer(unsigned(register_map_control.SUPER_CHUNK_FACTOR_LINK(GBTID mod 12))); --SUPER_CHUNK_FACTOR;

                                if chunk_length_bytes /= chunk_length_bytes_from_data then --avoid having long logs due to lots of console output of successful checks...
                                    log(ID_SEQUENCER, string'("GBTID: " & to_string(GBTID) & " ElinkID: " & to_string(ElinkID)& " chunk length: " & to_string(chunk_length*2) & "unfinished_chunk_length: " & to_string(unfinished_chunk_length(GBTID,ElinkID)*2)), C_SCOPE);
                                    log(ID_SEQUENCER, string'("Chunk count: " & to_string(chunk_count)), C_SCOPE);
                                    if chunk_length_bytes = ((chunk_length_bytes_from_data+7)/8)*8 then
                                        check_value(chunk_length_bytes, chunk_length_bytes_from_data, WARNING, "chunk does not have the expected length of "&to_string(chunk_length_bytes_from_data)&" bytes ("&to_string(chunk_length_bytes)&"), but CRToHost in 64b mode was modified to ignore tkeep");
                                    else
                                        if Trunc_v = '1' then --truncation
                                            check_value(chunk_length_bytes, chunk_length_bytes_from_data, WARNING, "chunk does not have the expected length of "&to_string(chunk_length_bytes_from_data)&" bytes ("&to_string(chunk_length_bytes)&"), but this can be expected on truncation");
                                        else
                                            log(ID_SEQUENCER, "Chunk data: ", C_SCOPE);
                                            for i in 0 to chunk_length_bytes/4-1 loop
                                                log(ID_SEQUENCER, string'(to_hstring(chunk_data(i*4+3)) &
                                                                          to_hstring(chunk_data(i*4+2)) &
                                                                          to_hstring(chunk_data(i*4+1)) &
                                                                          to_hstring(chunk_data(i*4+0)) ), C_SCOPE);
                                            end loop;
                                            check_value(chunk_length_bytes, chunk_length_bytes_from_data, ERROR, "chunk does not have the expected length of "&to_string(chunk_length_bytes_from_data)&" bytes ("&to_string(chunk_length_bytes)&")");
                                        end if;
                                    end if;
                                end if;
                            end if;
                            if TrailerType = "010" then
                                unfinished_chunk_length(GBTID,ElinkID) := 0;
                            end if;
                        end if;

                        chunk_count := chunk_count + chunk_length;

                        if chunk_count >= BLOCKSIZE/2 then
                            exit;
                        end if;
                    end loop;


                    if k*100/SIM_NUMBER_OF_BLOCKS /= progress then
                        log(ID_SEQUENCER, "Progress: " & to_string(k*100/SIM_NUMBER_OF_BLOCKS) & "%", C_SCOPE);
                        for i in 0 to GBT_NUM loop
                            if FIRMWARE_MODE = FIRMWARE_MODE_FULL then
                                if BlockCountArray(i,1 ) = 0 then
                                    log(ID_SEQUENCER, string'("Channel: "& to_dstring(i, 2)& ", " & "Block count: " & to_dstring(BlockCountArray(i,0 ),5) ), C_SCOPE);
                                else
                                    log(ID_SEQUENCER, string'("Channel: "& to_dstring(i, 2)& ", " & "Block count AXIs ID0: " & to_dstring(BlockCountArray(i,0 ),5) ), C_SCOPE);
                                    log(ID_SEQUENCER, string'("             " &  "Block count AXIs ID1: " & to_dstring(BlockCountArray(i,1 ),5) ), C_SCOPE);
                                end if;
                            elsif FIRMWARE_MODE = FIRMWARE_MODE_GBT then
                                log(ID_SEQUENCER, string'("GBT: "& to_dstring(i, 2)& "\n" &
                                              "Block count Egr0: " & to_dstring(BlockCountArray(i,7 ),5) &
                                                                     to_dstring(BlockCountArray(i,6 ),5) &
                                                                     to_dstring(BlockCountArray(i,5 ),5) &
                                                                     to_dstring(BlockCountArray(i,4 ),5) &
                                                                     to_dstring(BlockCountArray(i,3 ),5) &
                                                                     to_dstring(BlockCountArray(i,2 ),5) &
                                                                     to_dstring(BlockCountArray(i,1 ),5) &
                                                                     to_dstring(BlockCountArray(i,0 ),5) & "\n" &
                                              "Block count Egr1: " & to_dstring(BlockCountArray(i,15),5) &
                                                                     to_dstring(BlockCountArray(i,14),5) &
                                                                     to_dstring(BlockCountArray(i,13),5) &
                                                                     to_dstring(BlockCountArray(i,12),5) &
                                                                     to_dstring(BlockCountArray(i,11),5) &
                                                                     to_dstring(BlockCountArray(i,10),5) &
                                                                     to_dstring(BlockCountArray(i,9 ),5) &
                                                                     to_dstring(BlockCountArray(i,8 ),5) & "\n" &
                                              "Block count Egr2: " & to_dstring(BlockCountArray(i,23),5) &
                                                                     to_dstring(BlockCountArray(i,22),5) &
                                                                     to_dstring(BlockCountArray(i,21),5) &
                                                                     to_dstring(BlockCountArray(i,20),5) &
                                                                     to_dstring(BlockCountArray(i,19),5) &
                                                                     to_dstring(BlockCountArray(i,18),5) &
                                                                     to_dstring(BlockCountArray(i,17),5) &
                                                                     to_dstring(BlockCountArray(i,16),5) & "\n" &
                                              "Block count Egr3: " & to_dstring(BlockCountArray(i,31),5) &
                                                                     to_dstring(BlockCountArray(i,30),5) &
                                                                     to_dstring(BlockCountArray(i,29),5) &
                                                                     to_dstring(BlockCountArray(i,28),5) &
                                                                     to_dstring(BlockCountArray(i,27),5) &
                                                                     to_dstring(BlockCountArray(i,26),5) &
                                                                     to_dstring(BlockCountArray(i,25),5) &
                                                                     to_dstring(BlockCountArray(i,24),5) & "\n" &
                                              "Block count Egr4: " & to_dstring(BlockCountArray(i,39),5) &
                                                                     to_dstring(BlockCountArray(i,38),5) &
                                                                     to_dstring(BlockCountArray(i,37),5) &
                                                                     to_dstring(BlockCountArray(i,36),5) &
                                                                     to_dstring(BlockCountArray(i,35),5) &
                                                                     to_dstring(BlockCountArray(i,34),5) &
                                                                     to_dstring(BlockCountArray(i,33),5) &
                                                                     to_dstring(BlockCountArray(i,32),5) & "\n"), C_SCOPE);
                            elsif FIRMWARE_MODE = FIRMWARE_MODE_LPGBT then
                                log(ID_SEQUENCER, string'("LPGBT: "& to_dstring(i, 2)& "\n" &
                                              "Block count Egr0: " & to_dstring(BlockCountArray(i,3 ),5) &
                                                                     to_dstring(BlockCountArray(i,2 ),5) &
                                                                     to_dstring(BlockCountArray(i,1 ),5) &
                                                                     to_dstring(BlockCountArray(i,0 ),5) & "\n" &
                                              "Block count Egr1: " & to_dstring(BlockCountArray(i,7 ),5) &
                                                                     to_dstring(BlockCountArray(i,6 ),5) &
                                                                     to_dstring(BlockCountArray(i,5 ),5) &
                                                                     to_dstring(BlockCountArray(i,4 ),5) & "\n" &
                                              "Block count Egr2: " & to_dstring(BlockCountArray(i,11),5) &
                                                                     to_dstring(BlockCountArray(i,10),5) &
                                                                     to_dstring(BlockCountArray(i,9 ),5) &
                                                                     to_dstring(BlockCountArray(i,8 ),5) & "\n" &
                                              "Block count Egr3: " & to_dstring(BlockCountArray(i,15),5) &
                                                                     to_dstring(BlockCountArray(i,14),5) &
                                                                     to_dstring(BlockCountArray(i,13),5) &
                                                                     to_dstring(BlockCountArray(i,12),5) & "\n" &
                                              "Block count Egr4: " & to_dstring(BlockCountArray(i,19),5) &
                                                                     to_dstring(BlockCountArray(i,18),5) &
                                                                     to_dstring(BlockCountArray(i,17),5) &
                                                                     to_dstring(BlockCountArray(i,16),5) & "\n" &
                                              "Block count Egr5: " & to_dstring(BlockCountArray(i,23),5) &
                                                                     to_dstring(BlockCountArray(i,22),5) &
                                                                     to_dstring(BlockCountArray(i,21),5) &
                                                                     to_dstring(BlockCountArray(i,20),5) & "\n" &
                                              "Block count Egr6: " & to_dstring(BlockCountArray(i,27),5) &
                                                                     to_dstring(BlockCountArray(i,26),5) &
                                                                     to_dstring(BlockCountArray(i,25),5) &
                                                                     to_dstring(BlockCountArray(i,24),5) & "\n"), C_SCOPE);
                            end if;
                        end loop;
                        progress := k*100/SIM_NUMBER_OF_BLOCKS;
                    end if;
                end loop; --k, number of blocks to simulate
                checker_done_arr(descriptor) <= '1';
                wait until reset = '1';
            end loop;
        end process;
    end generate gDescriptors;

    checker_done <= and checker_done_arr;

end architecture;


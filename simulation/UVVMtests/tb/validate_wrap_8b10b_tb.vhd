--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Ohad Shaked
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

-- Chuck Benz, Hollis, NH   Copyright (c)2002
--
-- The information and description contained herein is the
-- property of Chuck Benz.
--
-- Permission is granted for any reuse of this information
-- and description as long as this copyright notice is
-- preserved.  Modifications may be made as long as this
-- notice is preserved.

-- 11-OCT-2002: updated with clearer messages, and checking decodeout

--November 2020: Translated to VHDL by Frans Schreuder
--February 2021: Modified for 8b10b wrapper by Ohad Shaked



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--use ieee.numeric_std_unsigned.all;
library uvvm_util;
context uvvm_util.uvvm_util_context;
use std.env.all;

use work.lookup_8b10b.all; --code8b10b lookup table
entity validate_wrap_8b10b_tb is
end entity validate_wrap_8b10b_tb;

architecture test of validate_wrap_8b10b_tb is
    signal encodein_p1, encodein_p2, encodein_p3, encodein_p4: std_logic_vector(8 downto 0);
    signal i,n: integer := 0;
    signal encodeout, encodeout_vr, encodeout_vrev : std_logic_vector(9 downto 0) := (others => '0');
    signal encodeout_v : std_logic_vector(0 to 9) := (others => '0');
    signal decodein_v : std_logic_vector(0 to 9) := (others => '0');
    signal decodeerr, disperr : std_logic := '0';
    signal decodeerr_v, disperr_v : std_logic := '0';
    signal decodeerr_vr, disperr_vr : std_logic := '0';
    signal enc_dispin, enc_dispout : std_logic := '0';
    signal dec_dispin, dec_dispout : std_logic := '0';
    signal decodeout, decodeout_v, decodeout_vr : std_logic_vector(8 downto 0) := (others => '0');
    
    signal code, code_temp: code_type;
    
    signal legal: std_logic_vector(1023 downto 0) := (others => '0');  -- mark every used 10b symbol as legal, leave rest marked as not
    signal mapcode: slv9_array(1023 downto 0) := (others => (others => '0'));
    signal decodein : std_logic_vector(9 downto 0) := (others => '0');
    
    signal clk, reset: std_logic := '1';
    constant clk_period: time := 10 ns;
	
	signal clock_ena     		: boolean   := false;
	signal BUSY_sig			 	: std_logic;
	signal data_code			: std_logic_vector (1 downto 0);
	signal ISK_sig				: std_logic_vector (1 downto 0);
	signal EncOut_rvsd			: std_logic_vector (9 downto 0);
	signal encDataOutrdy		: std_logic;
	signal Xoff_Set_Rdy			: std_logic;
	signal Xoff_Clear_Rdy 		: std_logic;
	signal unused_char			: std_logic_vector (7 downto 0);
	signal dec_code_err			: std_logic;
	signal dec_disp_err			: std_logic;

------------------------------------------------------------------
-- 8b10b encoding parameters:
-------------------------------------------------------------------
-- 8-bit values 
constant Kchar_comma  : std_logic_vector (7 downto 0) := "10111100"; -- K28.5  -- 0xBC
constant Kchar_eop    : std_logic_vector (7 downto 0) := "11011100"; -- K28.6  -- 0xDC
constant Kchar_sop    : std_logic_vector (7 downto 0) := "00111100"; -- K28.1  -- 0x3C
constant Kchar_sob    : std_logic_vector (7 downto 0) := "01011100"; -- K28.2
constant Kchar_eob    : std_logic_vector (7 downto 0) := "01111100"; -- K28.3

constant Kchar_FM_XOFF: std_logic_vector (7 downto 0) := "01011100"; -- K28.2  -- 0x5C
constant Kchar_FM_XON : std_logic_vector (7 downto 0) := "01111100"; -- K28.3  -- 0x7C

-- Unused control symbols:
-- =======================
constant Kchar_unused1: std_logic_vector (7 downto 0) := "00011100"; -- K28.0  -- 0x1C
constant Kchar_unused2: std_logic_vector (7 downto 0) := "10011100"; -- K28.4  -- 0x9C
constant Kchar_unused3: std_logic_vector (7 downto 0) := "11111100"; -- K28.7  -- 0xFC
constant Kchar_unused4: std_logic_vector (7 downto 0) := "11110111"; -- K23.7  -- 0xF7	
constant Kchar_unused5: std_logic_vector (7 downto 0) := "11111011"; -- K27.7  -- 0xFB	
constant Kchar_unused6: std_logic_vector (7 downto 0) := "11111101"; -- K29.7  -- 0xFD	
constant Kchar_unused7: std_logic_vector (7 downto 0) := "11111110"; -- K30.7  -- 0xFE	


begin
  
        -----------------------------------------------------------------------------
    -- Clock Generator
    -----------------------------------------------------------------------------
    clock_generator(clk, clock_ena, clk_period, "100 MHz clock");
    
    reset_proc: process
    begin
        reset <= '1';
        wait for clk_period;
        reset <= '0';
        wait;
    end process;
    


-- Encoder data Code type:
-- 00"data, 01"eop, 10"sop, 11"comma
data_code	<=	"01" when (code.k = '1' and  code.val_8b = Kchar_eop) 	else
				"10" when (code.k = '1' and  code.val_8b = Kchar_sop) 	else
				"11" when (code.k = '1' and  code.val_8b = Kchar_comma) else
				"00";


Xoff_Set_Rdy 	<= '1' when (code.k = '1' and  code.val_8b = Kchar_FM_XON) else '0';
Xoff_Clear_Rdy	<= '1' when (code.k = '1' and  code.val_8b = Kchar_FM_XOFF) else '0';


-- In order to comply the new 8b10b Encoder with the original Phase1 8b10b Encoder - the 8b10b ENC wrapper reverses the Enc dataout bits order  (LSB…MSB).
-- in this standalone TB it is not needed therefore the Enc output is reversed again in order to have the Enc dataout bits aligned MSB….LSB
encodeout <= EncOut_rvsd(0) & EncOut_rvsd(1) & EncOut_rvsd(2) & EncOut_rvsd(3) & EncOut_rvsd(4) & EncOut_rvsd(5) & EncOut_rvsd(6) & EncOut_rvsd(7) & EncOut_rvsd(8) & EncOut_rvsd(9);

-- Encoder:
DUTE: entity work.enc8b10_wrap 
port map ( 	
			clk            => clk,
			rst            => reset,
			dataCode       => data_code,
			dataIN         => code.val_8b,
			dataINrdy      => '1',
			Xoff_Set_Rdy   => Xoff_Set_Rdy,   
			Xoff_Clear_Rdy => Xoff_Clear_Rdy, 
			encDataOut     => EncOut_rvsd,
			encDataOutrdy  => encDataOutrdy
		 );


-- Decoder:
DUTD: entity work.dec_8b10_wrap 
generic map (GENERATE_FEI4B => false)
port map ( 
			RESET          		=> reset,
			RBYTECLK       		=> clk,
			ABCDEIFGHJ_IN  		=> decodein,
			ABCDEIFGHJ_IN_rdy	=> '1',
			HGFEDCBA       		=> decodeout(7 downto 0),
			ISK            		=> ISK_sig,
			BUSY           		=> BUSY_sig,
			code_err 	   		=> dec_code_err,
			disp_err 	   		=> dec_disp_err
		 );


decodeout(8) <= '1' when ISK_sig /= "00" else '0';
unused_char <= encodein_p1(7 downto 0);

    
    pipe_proc: process(clk, reset)
    begin
        if reset = '1' then
            encodein_p1 <= (others => '0');
            encodein_p2 <= (others => '0');
            encodein_p3 <= (others => '0');
            encodein_p4 <= (others => '0');
        elsif rising_edge(clk) then
            encodein_p1 <= code.k&code.val_8b;
            encodein_p2 <= encodein_p1;
            encodein_p3 <= encodein_p2;
            encodein_p4 <= encodein_p3;
        end if;
    end process;
    
    selectCode: process(i)
    begin
        if i < 268 then
            code <= code8b10b(i);
        else
            code <= ('U', "UUUUUUUU", "UUUUUUUUUU", "UUUUUUUUUU", 'U');
        end if;
    end process;


    
    sequencer: process
        variable last_encodein: std_logic_vector(8 downto 0);
    begin
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);
        enable_log_msg(ALL_MESSAGES);
        clock_ena <= true;
        wait until reset = '0';
        
-- Checking all tha datas and the 8b10B Wrapper supported control characters:
        log(ID_SEQUENCER, "\n\nFirst, test by trying all 256 Dx.y)", C_SCOPE);
        log(ID_SEQUENCER, "\n\nNext the 8b10B Wrapper 7 Kx.y supported control characters)", C_SCOPE);
        log(ID_SEQUENCER, "valid inputs, with both + and - starting disparity.", C_SCOPE);
        log(ID_SEQUENCER, "We check that the encoder output and ending disparity is correct.", C_SCOPE);
        log(ID_SEQUENCER, "We also check that the decoder matches.", C_SCOPE);

		wait until encDataOutrdy = '1';
        for il in 0 to 267 loop
			if (code8b10b(il).val_8b /= Kchar_unused1 and code8b10b(il).val_8b /= Kchar_unused2 and code8b10b(il).val_8b /= Kchar_unused3 and
				code8b10b(il).val_8b /= Kchar_unused4 and code8b10b(il).val_8b /= Kchar_unused5 and code8b10b(il).val_8b /= Kchar_unused6 and 
				code8b10b(il).val_8b /= Kchar_unused7 and 
				-- Ignoring also the FM oN/OFF K characters:				
				-- ------------------------------------------
				code8b10b(il).val_8b /= Kchar_FM_XOFF  and code8b10b(il).val_8b /= Kchar_FM_XON) then 
					i <= il;
			end if;

            wait_num_rising_edge(clk, 1);
            decodein <= encodeout;
            wait_num_rising_edge(clk, 1);
            decodein <= encodeout ;
            wait_num_rising_edge(clk, 1);
            decodein <= encodeout ;
            check_value(((encodeout /= code.val_10b_neg) and (encodeout /= code.val_10b_pos)), false, ERROR,  "Check encoding", C_SCOPE);
            decodein <= encodeout ;
            check_value(encodein_p4(8 downto 0), decodeout(8 downto 0), ERROR, "Encoder input should match decoder output", C_SCOPE);
            check_value(decodeerr, '0', ERROR, "Check decode error", C_SCOPE);
            check_value(dec_code_err, '0', ERROR, "Check Decoder error flag", C_SCOPE);
            check_value(disperr, '0', ERROR, "Check disparity error", C_SCOPE);
        end loop;




        ---- Now, having verified all legal codes, lets run some illegal codes
        ---- at the decoder... how to figure illegal codes ?  2048 possible cases,
        ---- lets mark the OK ones...
        legal <= (others => '0');
        mapcode <= (others => (others => '0'));
        for il in 0 to 267 loop
            i <= il;
            wait_num_rising_edge(clk, 1);
            legal(to_integer(unsigned(code.val_10b_neg))) <= '1' ;
            legal(to_integer(unsigned(code.val_10b_pos))) <= '1' ;
            mapcode(to_integer(unsigned(code.val_10b_neg))) <= code.k&code.val_8b;
            mapcode(to_integer(unsigned(code.val_10b_pos))) <= code.k&code.val_8b;
        end loop;

-- ===========================================================================================================================================
--  Driving the decoder with possible legal+ illeagl codes, 
--	without without the unused control characters which are ignored in the Decoder Wrapper and results different decoder output than expected. 			
-- ===========================================================================================================================================

        log(ID_SEQUENCER, "Now lets test all (legal and illegal) codes into the decoder.", C_SCOPE);
        log(ID_SEQUENCER, "checking all possible decode inputs", C_SCOPE) ;
        for il in 0 to 1023 loop
            n <= il;
            wait_num_rising_edge(clk, 1);
            decodein <= std_logic_vector(to_unsigned(n,10));
            wait_num_rising_edge(clk, 1);
            wait_num_rising_edge(clk, 1);
            check_value(((legal(n) = '0')  and  (decodeerr /= '1')), false, WARNING, "Detection of illegal code", C_SCOPE);
-- Skipping the unused control and FM_XOB/XOFF characters which are manipulated in the decoder wrapper:
            if ( decodeout(7 downto 0) /= Kchar_unused1 and decodeout(7 downto 0) /= Kchar_unused2 and decodeout(7 downto 0) /= Kchar_unused3 and
				 decodeout(7 downto 0) /= Kchar_unused4 and decodeout(7 downto 0) /= Kchar_unused5 and decodeout(7 downto 0) /= Kchar_unused6 and 
				 decodeout(7 downto 0) /= Kchar_unused7 and 
				 decodeout(7 downto 0) /= Kchar_FM_XOFF  and decodeout(7 downto 0) /= Kchar_FM_XON) then               
						
						check_value((legal(n) = '1'  and  (mapcode(n) /= decodeout)), false, ERROR, "Check decoder output", C_SCOPE) ;
			end if;
            wait_num_rising_edge(clk, 1);
        end loop;
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_SEQUENCER, "SIMULATION COMPLETED", C_SCOPE);
        std.env.stop;
        wait;
    end process;
   
end test;

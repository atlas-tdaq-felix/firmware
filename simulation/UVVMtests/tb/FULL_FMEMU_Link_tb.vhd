--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- file: FULL_FMEMU_Link_tb
----------------------------------------------------------------------------------
library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

    use work.pcie_package.all;
    use work.lpgbtfpga_package.all;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.axi_stream_package.all;
    use work.interlaken_package.all;

-- Test bench entity
entity FULL_FMEMU_Link_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity;

-- Test bench architecture
architecture arch of FULL_FMEMU_Link_tb is

    constant C_SCOPE : string := "FULL_FMEMU_Link_tb";
    constant C_CLK240_PERIOD : time    := 4.158 ns;
    constant C_CLK40_PERIOD  : time    := C_CLK240_PERIOD*6;
    constant C_CLK160_PERIOD : time    := C_CLK40_PERIOD/4;
    constant C_CLK320_PERIOD : time    := C_CLK40_PERIOD/8;
    constant C_CLK200_PERIOD : time    := 5 ns;

    constant C_CLK250_PERIOD : time    := 4 ns;
    constant C_APPREGCLK_PERIOD : time := 30 ns;
    signal emuToHost_GBTdata: std_logic_vector(119 downto 0); -- @suppress "Unused declaration"

    --signal GBT_DOWNLINK_USER_DATA_ENCODING : array_120b (0 to GBT_NUM/ENDPOINTS-1);
    signal appreg_clk                           : std_logic;

    signal clk40, clk240, clk250, clk160, clk320, apb3_axi_clk  : std_logic:='0';
    signal clock_ena     : boolean   := false;
    signal register_map_control : register_map_control_type;
    constant GTREFCLKS: integer := 2;
    signal GTREFCLK_N_in : std_logic_vector(GTREFCLKS-1 downto 0);
    signal GTREFCLK_P_in : std_logic_vector(GTREFCLKS-1 downto 0);
    constant LINK_NUM : integer := 4;
    constant GTREFCLK1S : integer := 0;
    signal GTREFCLK1_N_in : std_logic_vector(GTREFCLK1S - 1 downto 0); -- @suppress "Null range: The left argument is strictly smaller than the right" -- @suppress "Null range: The left argument is strictly smaller than the right"
    signal GTREFCLK1_P_in : std_logic_vector(GTREFCLK1S - 1 downto 0); -- @suppress "Null range: The left argument is strictly smaller than the right"
    signal TXUSRCLK_OUT : std_logic_vector(LINK_NUM - 1 downto 0);
    signal reset : std_logic;
    signal RXUSRCLK_OUT : std_logic_vector(LINK_NUM-1 downto 0); -- @suppress "signal RXUSRCLK_OUT is never read"
    signal GBT_DOWNLINK_USER_DATA : array_120b(0 to (LINK_NUM-1));
    signal LinkAligned : std_logic_vector(LINK_NUM-1 downto 0); -- @suppress "signal LinkAligned is never read"
    signal TX_P : std_logic_vector(LINK_NUM-1 downto 0);
    signal TX_N : std_logic_vector(LINK_NUM-1 downto 0);
    signal RX_P : std_logic_vector(LINK_NUM-1 downto 0);
    signal RX_N : std_logic_vector(LINK_NUM-1 downto 0);
    signal GTH_FM_RX_33b_out : array_33b(0 to LINK_NUM-1); -- @suppress "signal GTH_FM_RX_33b_out is never read"
    signal LTI_TX_Data_Transceiver_In : array_32b(0 to LINK_NUM - 1);
    signal LTI_TX_TX_CharIsK_in : array_4b(0 to LINK_NUM-1);
    signal TTCin : TTC_data_type := TTC_zero; -- @suppress "signal FMEMU_TXUSRCLK_240 is never read"
    signal FMEMU_TXUSRCLK_240 : std_logic_vector(LINK_NUM-1 downto 0); -- @suppress "signal FMEMU_TXUSRCLK_240 is never read"
    signal FMEMU_tx_din_array : array_32b(0 to LINK_NUM-1);
    signal FMEMU_tx_kin_array : array_4b(0 to LINK_NUM-1);
    signal FMEMU_LTI_RECEIVER_ACTIVE : std_logic_vector(LINK_NUM-1 downto 0); -- @suppress "signal FMEMU_LTI_RECEIVER_ACTIVE is never read"
    signal FMEMU_TTC_lti_data : TTC_data_array_type(0 to LINK_NUM-1);
    signal FMEMU_RXUSRCLK : std_logic_vector(LINK_NUM-1 downto 0); -- @suppress "signal FMEMU_RXUSRCLK is never read"
    signal FMEMU_clk40_lti_gbt : std_logic_vector(LINK_NUM-1 downto 0); -- @suppress "signal FMEMU_clk40_lti_gbt is never read"
    signal FMEMU_RX_120b : array_120b(0 to LINK_NUM-1);-- @suppress "signal FMEMU_RX_120b is never read"
    signal lti_receiver_aligned : std_logic_vector(LINK_NUM-1 downto 0);
    signal register_map_link_monitor : register_map_link_monitor_type; -- @suppress "signal gbt_receiver_aligned is never read"
    signal gbt_receiver_aligned : std_logic_vector(LINK_NUM-1 downto 0); -- @suppress "signal gbt_receiver_aligned is never read"
    constant XoffIn : std_logic_vector(LINK_NUM-1 downto 0) := (others => '0');





begin

    register_map_control.GBT_MODE_CTRL.DESMUX_USE_SW <= "0";

    register_map_control.GBT_TXUSRRDY <= (others => '1');
    register_map_control.GBT_RXUSRRDY <= (others => '1');
    register_map_control.GBT_GTTX_RESET <= (others => reset);
    register_map_control.GBT_GTRX_RESET <= (others => reset);
    register_map_control.GBT_SOFT_RESET <= (others => reset);
    register_map_control.GBT_PLL_RESET.CPLL_RESET <= (others => reset);
    register_map_control.GBT_PLL_RESET.QPLL_RESET <= (others => reset);
    register_map_control.GBT_SOFT_TX_RESET.RESET_GT <= (others => reset);
    register_map_control.GBT_SOFT_RX_RESET.RESET_GT <= (others => reset);
    register_map_control.GBT_SOFT_TX_RESET.RESET_ALL <= (others => reset);
    register_map_control.GBT_SOFT_RX_RESET.RESET_ALL <= (others => reset);


    register_map_control.GBT_TX_TC_DLY_VALUE1 <= REG_GBT_TX_TC_DLY_VALUE1_C;
    register_map_control.GBT_TX_TC_DLY_VALUE2 <= REG_GBT_TX_TC_DLY_VALUE2_C;


    register_map_control.GBT_DATA_TXFORMAT1 <= REG_GBT_DATA_TXFORMAT1_C;

    register_map_control.GBT_TX_RESET <= (others => reset);
    register_map_control.GBT_TX_TC_METHOD <= REG_GBT_TX_TC_METHOD_C;
    register_map_control.GBT_TC_EDGE <= REG_GBT_TC_EDGE_C;


    register_map_control.GBT_CHANNEL_DISABLE <= (others => '0');
    register_map_control.GBT_GENERAL_CTRL <= REG_GBT_GENERAL_CTRL_C;


    register_map_control.GBT_DATA_RXFORMAT1 <= REG_GBT_DATA_RXFORMAT1_C;
    register_map_control.GBT_DATA_RXFORMAT2 <= REG_GBT_DATA_RXFORMAT2_C;

    register_map_control.GBT_RX_RESET <= (others => reset);
    register_map_control.GBT_OUTMUX_SEL <= REG_GBT_OUTMUX_SEL_C;


    clock_generator(clk40, clock_ena, C_CLK40_PERIOD, "40 MHz clock");
    clock_generator(clk160, clock_ena, C_CLK160_PERIOD, "160 MHz clock");
    clock_generator(clk240, clock_ena, C_CLK240_PERIOD, "240 MHz clock");
    clock_generator(clk320, clock_ena, C_CLK320_PERIOD, "320 MHz clock");
    clock_generator(clk250, clock_ena, C_CLK250_PERIOD, "250 MHz clock");
    clock_generator(appreg_clk, clock_ena, C_APPREGCLK_PERIOD, "25 MHz clock");
    clock_generator(apb3_axi_clk, clock_ena, C_CLK200_PERIOD, "200 MHz clock");

    GTREFCLK_N_in <= (others => not clk240);
    GTREFCLK_P_in <= (others => clk240);
    GTREFCLK1_N_in <= (others => '0');
    GTREFCLK1_P_in <= (others => '1');

    sequencer : process
    begin
        --Start with LTI mode for TTC distribution
        register_map_control.LINK_FULLMODE_LTI <= x"FFFFFF";

        clock_ena <= false;
        reset <= '1';
        -- Print the configuration to the log
        --wait for 1 ns;
        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);

        --enable_log_msg(ALL_MESSAGES);
        disable_log_msg(ALL_MESSAGES, NON_QUIET, C_SCOPE);
        enable_log_msg(ID_SEQUENCER, NON_QUIET, C_SCOPE);
        disable_log_msg(ID_POS_ACK, NON_QUIET, C_SCOPE);

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "----- initializing test bench       ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        -- Wait for UVVM to finish initialization
        --await_uvvm_initialization(VOID);

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "-----    end of initialisation        ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        wait for (10*C_CLK40_PERIOD);
        clock_ena <= true;
        wait for (C_CLK40_PERIOD);
        reset <= '0';

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "-- test bench sequence starts here      ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);

        log(ID_LOG_HDR, "Starting simulation of TB for EGROUP using VVCs", C_SCOPE);
        ------------------------------------------------------------

        await_value(lti_receiver_aligned, x"F", 0 ns, 200 us, TB_ERROR, "LTI receivers must align and show no CRC error", C_SCOPE);

        --Start with LTI mode for TTC distribution
        register_map_control.LINK_FULLMODE_LTI <= x"000000";

        await_value(lti_receiver_aligned, x"0", 0 ns, 20 us, TB_ERROR, "LTI receivers must misalign and /or show CRC error", C_SCOPE);
        wait for 100 us;

        --await_value(gbt_receiver_aligned, x"F", 0 ns, 20 ms, TB_ERROR, "GBT receivers must align and show no FEC error", C_SCOPE);

        enable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        enable_log_msg(ID_SEQUENCER);
        enable_log_msg(ID_UVVM_SEND_CMD);

        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely
    --
    end process; -- sequencer


    link_wrapper0: entity work.link_wrapper generic map(
            LINK_NUM => LINK_NUM,
            CARD_TYPE => 712,
            GTHREFCLK_SEL => '0',
            FIRMWARE_MODE => FIRMWARE_MODE_FULL,
            FULL_HALFRATE => false,
            PLL_SEL => '0',
            GTREFCLKS => GTREFCLKS,
            GTREFCLK1S => GTREFCLK1S,
            KCU_LOWER_LATENCY => 0
        )
        port map(
            register_map_control => register_map_control,
            register_map_link_monitor => open,
            clk40 => clk40,
            clk100 => '0',
            apb3_axi_clk => apb3_axi_clk,
            clk40_xtal => clk40,
            GTREFCLK_N_in => GTREFCLK_N_in,
            GTREFCLK_P_in => GTREFCLK_P_in,
            GTREFCLK1_N_in => GTREFCLK1_N_in,
            GTREFCLK1_P_in => GTREFCLK1_P_in,
            rst_hw => reset,
            TXUSRCLK_OUT => TXUSRCLK_OUT,
            GT_REFCLK_OUT => open,
            RXUSRCLK_OUT => RXUSRCLK_OUT,
            GBT_DOWNLINK_USER_DATA => GBT_DOWNLINK_USER_DATA,
            GBT_UPLINK_USER_DATA => open,
            lpGBT_DOWNLINK_USER_DATA => (others => (others => '0')),
            lpGBT_DOWNLINK_IC_DATA => (others => (others => '0')),
            lpGBT_DOWNLINK_EC_DATA => (others => (others => '0')),
            lpGBT_UPLINK_USER_DATA => open,
            lpGBT_UPLINK_EC_DATA => open,
            lpGBT_UPLINK_IC_DATA => open,
            LinkAligned => LinkAligned,
            TX_P => TX_P,
            TX_N => TX_N,
            RX_P => RX_P,
            RX_N => RX_N,
            GTH_FM_RX_33b_out => GTH_FM_RX_33b_out,
            LMK_P => (others => '1'),
            LMK_N => (others => '0'),
            LTI_TX_Data_Transceiver_In => LTI_TX_Data_Transceiver_In,
            Interlaken_Data_Transceiver_Out => open,
            Interlaken_RX_Datavalid_Out => open,
            Interlaken_RX_Gearboxslip => (others => '0'),
            Interlaken_Decoder_Aligned_in => (others => '0'),
            LTI_TX_TX_CharIsK_in => LTI_TX_TX_CharIsK_in,
            FLX182_LTI_GTREFCLK1_out => open
        );

    GBT_DOWNLINK_USER_DATA <= (others => "0101"&"1111"&x"0000_0000_0000_0000_000"&"000"&TTCin.L0A&x"0000_0000");

    l0a_proc: process
    begin
        TTCin.L0A <= '0';
        wait for C_CLK40_PERIOD*39;
        TTCin.L0A <= '1';
        wait for C_CLK40_PERIOD;
    end process;


    LtiTX0: entity work.LTI_FE_Transmitter
        generic map(
            GBT_NUM => LINK_NUM
        )
        port map(
            clk40                   => clk40,
            daq_fifo_flush          => reset,
            TTCin                   => TTCin,
            XoffIn                  => XoffIn,
            LTI_TX_Data_Transceiver => LTI_TX_Data_Transceiver_In,
            LTI_TX_TX_CharIsK       => LTI_TX_TX_CharIsK_in,
            LTI_TXUSRCLK_in         => TXUSRCLK_OUT
        );



    -----------------------------------------
    -- FMEMU part ---------------------------
    -----------------------------------------

    u11: entity work.FullModeTransceiver
        generic map(
            NUM_LINKS        => LINK_NUM)
        --      USE_GREFCLK      => RECOVER_CLK_FROM_RX_GBT)
        port map(
            GTREFCLK0_0_N_IN                   => GTREFCLK_N_in(0),
            GTREFCLK0_0_P_IN                   => GTREFCLK_P_in(0),
            GTREFCLK0_1_N_IN                   => GTREFCLK_N_in(1),
            GTREFCLK0_1_P_IN                   => GTREFCLK_P_in(1),
            gtrxp_in                           => TX_P,
            gtrxn_in                           => TX_N,
            gttxn_out                          => RX_N,
            gttxp_out                          => RX_P,
            TXUSRCLK_OUT                       => FMEMU_TXUSRCLK_240,
            RXUSRCLK_OUT                       => FMEMU_RXUSRCLK,
            txdata_in                          => FMEMU_tx_din_array,
            txcharisk_in                       => FMEMU_tx_kin_array,
            TTCout                             => FMEMU_TTC_lti_data,
            rst_hw                             => reset,
            clk40_out                          => FMEMU_clk40_lti_gbt,
            drpclk_in                          => clk40,
            RX_120b_out                        => FMEMU_RX_120b,
            LTI_RECEIVER_ACTIVE_out            => FMEMU_LTI_RECEIVER_ACTIVE,
            FRAME_LOCKED_O                     => open,
            register_map_control               => register_map_control,
            register_map_link_monitor          => register_map_link_monitor
        );
    g_aligned: for i in 0 to LINK_NUM-1 generate
        lti_receiver_aligned(i) <= FMEMU_TTC_lti_data(i).LTI_decoder_aligned and FMEMU_TTC_lti_data(i).LTI_CRC_valid;
        gbt_receiver_aligned(i) <= register_map_link_monitor.GBT_ALIGNMENT_DONE(i) and not register_map_link_monitor.GBT_ERROR(i);
    end generate;

    FMEMU_tx_din_array <= (others => x"000000BC");
    FMEMU_tx_kin_array <= (others => "0001");



end architecture;


--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company: Nikhef
-- Engineer: Frans Schreuder
--
-- Create Date: 10/30/2019 03:36:26 PM
-- Design Name: crc20_datagen
-- Module Name: crc20 - Behavioral
-- Project Name: FELIX
-- Target Devices:
-- Tool Versions: questasim 2019.1
-- Description:
-- This is a simple datagenerator that utilizes UVVM random and generate_crc functionality
-- it will generate 32b random data packets encapsulated with sop(start of packet) and eop (end of packet)
-- dvalid_out = 0 means that no packet is generated. The last 32b word (marked by eop) contains the crc20
-- in bits 19..0, all other bytes are random.
-- Dependencies:
--
-- Revision:
-- see GIT log.
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

entity crc20_datagen is
    port (
        clk        : in  std_logic;
        data_out   : out std_logic_vector(31 downto 0);
        sop_out    : out std_logic;
        eop_out    : out std_logic;
        dvalid_out : out std_logic
    );
end crc20_datagen;

architecture Behavioral of crc20_datagen is

begin

    datagen_proc: process(clk)
        variable cnt: integer range 0 to 20000:=0;
        variable data: std_logic_vector(31 downto 0) := (others => '0');
        variable crc: std_logic_vector(19 downto 0) := x"fffff";
        constant polynomial: std_logic_vector(20 downto 0) := '1' & x"8359f";

        variable packageLength: integer range 0 to 10000;
        variable idleLength: integer range 0 to 10000;
    --variable rnd: integer range 0 to 1000;
    --constant C_SCOPE     : string  := C_TB_SCOPE_DEFAULT;
    begin

        if rising_edge(clk) then

            eop_out <= '0';
            sop_out <= '0';
            if cnt = 0 then
                sop_out <= '1';
                data := random(32);
                data_out <= data;
                crc := x"fffff";
                crc := generate_crc(data, crc, polynomial);

                dvalid_out <= '1';
                idleLength := random(1, 100);
                packageLength := random(1, 10000);

            elsif cnt < packageLength then
                data := random(32);
                data_out <= data;
                crc := generate_crc(data, crc, polynomial);
                dvalid_out <= '1';
            elsif cnt = packageLength then  --eop, send out CRC20
                data_out <= x"000" & crc;
                eop_out <= '1';
                dvalid_out <= '1';
            else
                data_out <= (others => '0');
                dvalid_out <= '0';
            end if;

            if cnt = packageLength + idleLength - 1 then
                cnt := 0;

            else
                cnt:=cnt + 1;
            end if;
        end if;

    end process;


end Behavioral;

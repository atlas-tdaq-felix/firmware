--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marco
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--  ATLAS
--  FELIX UVVM test automation project
--  M. Trovato
--  ANL
--  January 2023
--  Modified for RD53B by:
--  R. Luz
--  February 2024
----------------------------------------------------------------------------------
-- file: decodingpixeRD53B_tb.vhd.
----------------------------------------------------------------------------------
library IEEE;
    use IEEE.std_logic_1164.all;
    use ieee.numeric_std.all;
    use IEEE.numeric_std_unsigned.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;
    use work.axi_stream_package.all;
    use ieee.std_logic_textio.all;
    use std.textio.all;

    use std.env.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

library xpm;
    use xpm.vcomponents.all;

-- Test bench entity
entity decodingpixelRD53B_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end decodingpixelRD53B_tb;

architecture tb of decodingpixelRD53B_tb is
    function int_to_str(int : integer) return string is
        variable r : string(1 to 1);
    begin
        case int is
            when 0      => r := "0";
            when 1      => r := "1";
            when others => r := "?";
        end case;
        return r;
    end int_to_str;

    constant FIRMWARE_MODE                : integer := FIRMWARE_MODE_PIXEL;
    constant STREAMS_TOHOST               : integer := STREAMS_TOHOST_MODE(FIRMWARE_MODE);

    constant LINK                         : IntArray := (0,1);

    signal CBOPT                          : std_logic_vector(3 downto 0);

    signal register_map_control           : register_map_control_type;

    constant clk40_period                 : time := 25 ns;

    signal clk40                          : std_logic:= '0';
    signal clk40_tmp                      : std_logic:= '0';
    signal clk40_en                       : boolean   := false;
    signal clk160                         : std_logic:= '0';
    signal clk160_tmp                     : std_logic:= '0';
    signal clk160_en                      : boolean   := false;
    signal aclk                           : std_logic:= '0';
    signal aclk_tmp                       : std_logic:= '0';
    signal aclk_en                        : boolean   := false;

    signal reset_dopulse                  : std_logic := '0';
    signal dopulse                        : std_logic := '1';
    signal reset_gbtemu                   : std_logic := '0';
    signal reset                          : std_logic_vector(4 downto 0) := (others => '0');

    signal m_axis_tready                  : axis_tready_array_type(0 to STREAMS_TOHOST-1);

    signal LinkData_in                    : array_224b(0 to 1);
    signal LinkAligned_in                 : std_logic_vector(0 to 1);
    signal m_axis_out                     : axis_32_2d_array_type(0 to 1, 0 to STREAMS_TOHOST-3);
    signal m_axis_tready_in               : axis_tready_2d_array_type(0 to 1, 0 to STREAMS_TOHOST-3);
    signal m_axis_prog_empty_out          : axis_tready_2d_array_type(0 to 1, 0 to STREAMS_TOHOST-3); -- @suppress "signal m_axis_prog_empty_out is never read"

    signal counter_CB                     : std_logic_vector(12 downto 0)  := (others => '0');
    constant counter_CBmax                  : std_logic_vector(12 downto 0)  := (others => '1');

    signal m_axis_data_comp               : axis_32_array_type(0 to 11);
    signal m_axis_dcs_comp                : axis_32_array_type(0 to 11);

    signal m_compare_data                 : std_logic_vector(0 to 11)  := (others => '0');
    signal m_compare_data_all             : std_logic_vector(0 to 11)  := (others => '0');
    signal m_compare_dcs                  : std_logic_vector(0 to 11)  := (others => '0');
    signal m_compare_dcs_noCB             : std_logic_vector(0 to 11)  := (others => '0');
    signal m_compare_dcs_CB2              : std_logic_vector(0 to 11)  := (others => '0');
    signal m_compare_dcs_CB3              : std_logic_vector(0 to 11)  := (others => '0');
    signal m_compare_dcs_CB4              : std_logic_vector(0 to 11)  := (others => '0');
    signal dcs_equal                      : std_logic := '0';
    signal data_equal                     : std_logic := '0';

    signal isERR_mem                      : std_logic_vector(0 to 4) := (others=>'0'); --sane as isERRdata/dcs but for memory
    signal isERR_data_dif                 : std_logic := '0'; --high if pixel data in the lanes tested is different
    signal isERR_dcs_dif                  : std_logic := '0'; --high if dcs data in the lanes tested is different


    --Vunit simulation runs from simulatio/VUnit/vunit_out/modelsim/
    constant fdata                        : string := "../../../64b66b/RD53B_refout_data.mem";
    constant fdcs0                        : string := "../../../64b66b/RD53B_refout_dcs0.mem";
    constant fdcs1                        : string := "../../../64b66b/RD53B_refout_dcs1.mem";
    constant fdcs2                        : string := "../../../64b66b/RD53B_refout_dcs2.mem";
    constant fdcs3                        : string := "../../../64b66b/RD53B_refout_dcs3.mem";
    constant files_size                   : IntArray :=(1384,124,124,124,124);

    type array_14b is array (natural range <>) of std_logic_vector(13 downto 0);
    signal count_mem                      : array_14b(0 to 4) := (others=>(others=>'0'));

    signal timeout                        : boolean := false; -- @suppress "signal timeout is never read"
begin
    -----------------------------------------------------------------------------
    -- Registers
    -----------------------------------------------------------------------------
    --internal emulator
    register_map_control.GBT_TOHOST_FANOUT.SEL(1 downto 0) <= "11";
    register_map_control.FE_EMU_ENA.EMU_TOHOST(0) <= '1';
    register_map_control.FE_EMU_CONFIG.WRADDR <= (others => '0');
    --
    register_map_control.DECODING_DISEGROUP <= "1000000";
    register_map_control.DECODING_MASK64B66BKBLOCK <= x"a";

    g_link_loop_rm : for link_i in 0 to 1 generate
        g_egroup_loop_rm: for egroup_i in 0 to 5 generate
            register_map_control.DECODING_EGROUP_CTRL(link_i)(egroup_i).PATH_ENCODING <= x"00000033";
            register_map_control.DECODING_EGROUP_CTRL(link_i)(egroup_i).EPATH_ENA <= x"03";
        end generate; --stream
    end generate; --link

    m_axis_tready     <= (others => '1');
    -----------------------------------------------------------------------------
    -- Clock Generator
    -----------------------------------------------------------------------------
    clk40_tmp  <= not clk40_tmp  after clk40_period/2; --40 MHZ
    clk40      <= clk40_tmp      when  clk40_en else '0';
    clk160_tmp <= not clk160_tmp after clk40_period/8; --160 MHZ
    clk160     <= clk160_tmp     when  clk160_en else '0';
    aclk_tmp   <= not aclk_tmp   after clk40_period/12; --240 MHZ
    aclk       <= aclk_tmp       when  aclk_en else '0';
    -----------------------------------------------------------------------------
    -- Resets
    -----------------------------------------------------------------------------
    rst_proc: process(clk40)
    begin
        if rising_edge(clk40) then
            if (reset_dopulse = '1') then
                reset <= (others => '0');
                dopulse <= '1';
            elsif(dopulse = '1' and reset(0) = '0') then
                reset(0) <= '1';
                dopulse <= '1';
            elsif(dopulse = '1' and reset(0) = '1') then
                reset(0) <= '0';
                dopulse <= '0';
            else
                reset(0) <= '0';
            end if;
            reset(1) <= reset(0);
            reset(2) <= reset(1);
            reset(3) <= reset(2);
            reset(4) <= reset(3);
        end if;
    end process;

    -----------------------------------------------------------------------------
    -- Emulator
    -----------------------------------------------------------------------------
    g_link_in_loop: for link_i in 0 to 1 generate
        gbtEmuToHost0_link1: entity work.GBTdataEmulator
            generic map(
                EMU_DIRECTION      => "ToHost",
                FIRMWARE_MODE      => FIRMWARE_MODE_PIXEL,
                MEM_DEPTH          => 16384,
                LPGBT_TOHOST_WIDTH => 32,
                LPGBT_TOFE_WIDTH   => 8,
                MemIndLUTToHost    => (0, 1, 2, 3, 4, 5, 6),
                MemIndLUTToHost_f  => "lpGBT_RD53B_ToHostemuram_"&int_to_str(link_i)
            )
            port map(
                clk40                => clk40,
                wrclk                => clk40,
                rst_hw               => reset_gbtemu,
                rst_soft             => reset_gbtemu,
                xoff                 => '0',
                register_map_control => register_map_control,
                GBTdata              => open,
                lpGBTdataToFE        => open,
                lpGBTdataToHost      => LinkData_in(link_i),
                lpGBTECdata          => open,
                lpGBTICdata          => open,
                GBTlinkValid         => LinkAligned_in(link_i));
        g_streamtohost_in_loop: for stream_i in 0 to STREAMS_TOHOST-3 generate
            m_axis_tready_in(link_i, stream_i) <= m_axis_tready(stream_i);
        end generate; --stream
    end generate; --link
    -----------------------------------------------------------------------------
    -- UUT
    -----------------------------------------------------------------------------
    DecodingPixelLinkLPGBT_uut: entity work.DecodingPixelLinkLPGBT
        generic map(
            CARD_TYPE      => 712,
            RD53Version    => "B",
            BLOCKSIZE      => 1024,
            LINK           => LINK,
            SIMU           => 1,
            STREAMS_TOHOST => STREAMS_TOHOST,
            PCIE_ENDPOINT  => 0,
            VERSAL         => false
        )
        port map(
            clk40                => clk40,
            clk160               => clk160,
            daq_reset            => reset(4),
            daq_fifo_flush       => reset(4),
            MsbFirst             => '1',

            LinkData             => LinkData_in,
            LinkAligned          => LinkAligned_in,

            m_axis               => m_axis_out,
            m_axis_tready        => m_axis_tready_in,
            m_axis_aclk          => aclk,
            m_axis_prog_empty    => m_axis_prog_empty_out,
            register_map_control => register_map_control,
            DecoderAligned_out   => open,
            DecoderDeskewed_out  => open,
            cnt_rx_64b66bhdr_out => open,
            rx_soft_err_cnt_out  => open,
            rx_soft_err_rst      => (others => '0')
        );
    -----------------------------------------------------------------------------
    -- Clock Counter
    -----------------------------------------------------------------------------
    -- reset_dopulse can be commented out
    count_CB_and_reset_i : process(clk40)
    begin
        if rising_edge(clk40) then
            if counter_CB = counter_CBmax then
                counter_CB <= counter_CB;
                reset_gbtemu <= '1';
            else
                counter_CB <= counter_CB + x"1";
                if counter_CB = 1 or counter_CB = 3 then
                    reset_gbtemu <= not reset_gbtemu;
                elsif counter_CB = 4 or counter_CB = 2095 or counter_CB = 4207  or counter_CB = 6319 or
                      counter_CB = 8 or counter_CB = 2099 or counter_CB = 4211  or counter_CB = 6323 then
                    reset_dopulse <= not reset_dopulse;

                elsif counter_CB = 10 then
                    register_map_control.DECODING_LINK_CB(0).CBOPT <= x"0";
                elsif counter_CB = 2126 then
                    register_map_control.DECODING_LINK_CB(0).CBOPT <= x"2";
                elsif counter_CB = 4238 then
                    register_map_control.DECODING_LINK_CB(0).CBOPT <= x"3";
                elsif counter_CB = 6350 then
                    register_map_control.DECODING_LINK_CB(0).CBOPT <= x"4";
                end if;
            end if;
        end if;
    end process;
    -----------------------------------------------------------------------------
    -- m_axis Checker
    -----------------------------------------------------------------------------
    CBOPT <= register_map_control.DECODING_LINK_CB(0).CBOPT;

    g_link_m_axis_cheker_loop: for link_i in 0 to 1 generate
        g_streamtohost_in_loop: for egroup in 0 to 5 generate
            m_axis_data_comp(link_i*6+egroup) <= m_axis_out(link_i,egroup*4);
            m_axis_dcs_comp(link_i*6+egroup)  <= m_axis_out(link_i,egroup*4+1);
        end generate;
    end generate;

    g_m_axis_cheker_loop: for i in 0 to 11 generate
        m_compare_data_all(i) <= '0' when m_axis_data_comp(0) = m_axis_data_comp(i) else '1';
        m_compare_dcs_noCB(i) <= '1' when (m_axis_dcs_comp(0) /= m_axis_dcs_comp(i)) and CBOPT = x"0" else '0';
        g_CB2_if: if i < 6 generate
            m_compare_dcs_CB2(i)    <= '1' when m_axis_dcs_comp(0) /= m_axis_dcs_comp(2*i+0) and CBOPT = x"2" else '0';
            m_compare_dcs_CB2(6+i)  <= '1' when m_axis_dcs_comp(1) /= m_axis_dcs_comp(2*i+1) and CBOPT = x"2" else '0';
        end generate;
        g_CB3_if: if i < 4 generate
            m_compare_dcs_CB3(i)    <= '1' when m_axis_dcs_comp(0) /= m_axis_dcs_comp(3*i+0) and CBOPT = x"3" else '0';
            m_compare_dcs_CB3(4+i)  <= '1' when m_axis_dcs_comp(1) /= m_axis_dcs_comp(3*i+1) and CBOPT = x"3" else '0';
            m_compare_dcs_CB3(8+i)  <= '1' when m_axis_dcs_comp(2) /= m_axis_dcs_comp(3*i+2) and CBOPT = x"3" else '0';
        end generate;
        g_CB4_if: if i < 3 generate
            m_compare_dcs_CB4(i)    <= '1' when m_axis_dcs_comp(0) /= m_axis_dcs_comp(4*i+0) and CBOPT = x"4" else '0';
            m_compare_dcs_CB4(3+i)  <= '1' when m_axis_dcs_comp(1) /= m_axis_dcs_comp(4*i+1) and CBOPT = x"4" else '0';
            m_compare_dcs_CB4(6+i)  <= '1' when m_axis_dcs_comp(2) /= m_axis_dcs_comp(4*i+2) and CBOPT = x"4" else '0';
            m_compare_dcs_CB4(9+i)  <= '1' when m_axis_dcs_comp(3) /= m_axis_dcs_comp(4*i+3) and CBOPT = x"4" else '0';
        end generate;
    end generate;

    m_compare_dcs  <= m_compare_dcs_noCB when CBOPT = x"0" else
                      m_compare_dcs_CB2  when CBOPT = x"2" else
                      m_compare_dcs_CB3  when CBOPT = x"3" else
                      m_compare_dcs_CB4  when CBOPT = x"4";

    m_compare_data <= m_compare_data_all                                                     when CBOPT = x"0" else
                      m_compare_data_all(0 to 2) & m_compare_data_all(6 to 8) & "000000"     when CBOPT = x"2" else
                      m_compare_data_all(0 to 1) & m_compare_data_all(6 to 7) & "00000000"   when CBOPT = x"3" else
                      m_compare_data_all(0 to 1) & m_compare_data_all(6 to 6) & "000000000"  when CBOPT = x"4";

    dcs_equal  <= '1' when m_compare_dcs  = "000000000000" else '0';
    data_equal <= '1' when m_compare_data = "000000000000" else '0';

    -----------------------------------------------------------------------------
    -- Data Checker
    -----------------------------------------------------------------------------
    interpretdata_proc: process(aclk)
    begin
        if rising_edge(aclk) then
            if dcs_equal = '0' then
                isERR_dcs_dif <= '1';
            end if;
            if data_equal = '0' then
                isERR_data_dif <= '1';
            end if;
        end if;
    end process;

    g_ref_files_loop: for file_i in 0 to 4 generate
        function file_name (i:integer)
            return string is
        begin
            case i is
                when 0 => return fdata;
                when 1 => return fdcs0;
                when 2 => return fdcs1;
                when 3 => return fdcs2;
                when 4 => return fdcs3;
                when others => return fdcs3;
            end case;
        end function;
        constant fname          : string := file_name(file_i);
        signal emuram_rdaddr    : std_logic_vector(13 downto 0) := (others => '0');
        signal tvalid_rising    : std_logic;
        signal m_axis_comp      : axis_32_type;
        signal m_axis_comp_dly  : axis_32_type;
        signal mem_ref, douta   : std_logic_vector(31 downto 0) := (others => '0'); -- @suppress "signal douta is never read"
    begin

        ref_files_memory : xpm_memory_tdpram
            generic map (  -- @suppress "Generic map uses default values. Missing optional actuals: USE_MEM_INIT_MMI, CASCADE_HEIGHT, SIM_ASSERT_CHK, WRITE_PROTECT"
                ADDR_WIDTH_A         => 14,
                ADDR_WIDTH_B         => 14,
                AUTO_SLEEP_TIME      => 0,
                BYTE_WRITE_WIDTH_A   => 32,
                BYTE_WRITE_WIDTH_B   => 32,
                CLOCKING_MODE        => "independent_clock",
                ECC_MODE             => "no_ecc",
                MEMORY_INIT_FILE     => fname,
                MEMORY_INIT_PARAM    => "0",
                MEMORY_OPTIMIZATION  => "true",
                MEMORY_PRIMITIVE     => "auto",
                MEMORY_SIZE          => 16384*32,
                MESSAGE_CONTROL      => 1,
                READ_DATA_WIDTH_A    => 32,
                READ_DATA_WIDTH_B    => 32,
                READ_LATENCY_A       => 1,
                READ_LATENCY_B       => 1,
                READ_RESET_VALUE_A   => "0",
                READ_RESET_VALUE_B   => "0",
                RST_MODE_A           => "SYNC",
                RST_MODE_B           => "SYNC",
                USE_EMBEDDED_CONSTRAINT => 0,
                USE_MEM_INIT         => 0,
                WAKEUP_TIME          => "disable_sleep",
                WRITE_DATA_WIDTH_A   => 32,
                WRITE_DATA_WIDTH_B   => 32,
                WRITE_MODE_A         => "no_change",
                WRITE_MODE_B         => "no_change"
            )
            port map (
                sleep => '0',
                clka => '0',
                rsta => aclk,
                ena => '1',
                regcea => '1',
                wea => (others =>'0'),
                addra => (others =>'0'),
                dina => (others =>'0'),
                injectsbiterra => '0',
                injectdbiterra => '0',
                douta => douta,
                sbiterra => open,
                dbiterra => open,
                clkb => not aclk,
                rstb => '0',
                enb => '1',
                regceb => '1',
                web => (others =>'0'),
                addrb => emuram_rdaddr,
                dinb => (others =>'0'),
                injectsbiterrb => '0',
                injectdbiterrb => '0',
                doutb => mem_ref,
                sbiterrb => open,
                dbiterrb => open
            );


        m_axis_comp <= m_axis_data_comp(0) when file_i=0 else
                       m_axis_dcs_comp(0)  when file_i=1 else
                       m_axis_dcs_comp(1)  when file_i=2 else
                       m_axis_dcs_comp(2)  when file_i=3 else
                       m_axis_dcs_comp(3) when file_i=4;
        address_counter: process(aclk)
        begin
            if rising_edge(aclk) then
                m_axis_comp_dly <= m_axis_comp;
                tvalid_rising <= m_axis_comp.tvalid;
                if tvalid_rising = '1' then
                    if to_integer(unsigned(emuram_rdaddr)) = files_size(0)-1 then
                        emuram_rdaddr <= emuram_rdaddr;
                    else
                        emuram_rdaddr <= emuram_rdaddr + x"1";
                    end if;
                    count_mem(file_i) <= count_mem(file_i) + x"1";
                end if;
                if(m_axis_comp_dly.tdata /= mem_ref and m_axis_comp_dly.tvalid = '1') then
                    isERR_mem(file_i) <= '1';
                end if;
            end if;
        end process;
    end generate;

    -----------------------------------------------------------------------------
    -- Main Process
    -----------------------------------------------------------------------------
    mainproc : process
        variable var_isERR_data     : std_logic := '0'; --high if simul output for pixel data is different than expected
        variable var_isERR_dcs0     : std_logic := '0'; --high if simul output for dcs data for egroup 0 is different than expected
        variable var_isERR_dcs1     : std_logic := '0'; --high if simul output for dcs data for egroup 1 is different than expected
        variable var_isERR_dcs2     : std_logic := '0'; --high if simul output for dcs data for egroup 2 is different than expected
        variable var_isERR_dcs3     : std_logic := '0'; --high if simul output for dcs data for egroup 3 is different than expected
        variable var_isERR_data_dif : std_logic := '0'; --high if pixel data in the lanes tested is different
        variable var_isERR_dcs_dif  : std_logic := '0'; --high if dcs data in the lanes tested is different
        variable var_isERR_count    : std_logic := '0'; --high if the number of times the files are read is different then expected

    --        procedure printMT (arg: in string := "") is
    --            variable lineMT : line;
    --        begin
    --            std.textio.write(lineMT, arg);
    --            std.textio.writeline(std.textio.output, lineMT);
    --        end;
    begin
        assert false
            report "START SIMULATION"
            severity NOTE;
        clk40_en<= true;
        clk160_en <= true;
        aclk_en<= true;

        wait for 250 us;

        var_isERR_data      := isERR_mem(0);
        var_isERR_dcs0      := isERR_mem(1);
        var_isERR_dcs1      := isERR_mem(2);
        var_isERR_dcs2      := isERR_mem(3);
        var_isERR_dcs3      := isERR_mem(4);
        var_isERR_data_dif  := isERR_data_dif;
        var_isERR_dcs_dif   := isERR_dcs_dif;
        var_isERR_count     := '0' when to_integer(unsigned(count_mem(0))) = files_size(0) and
                               to_integer(unsigned(count_mem(1))) = files_size(1) and
                               to_integer(unsigned(count_mem(2))) = files_size(2) and
                               to_integer(unsigned(count_mem(3))) = files_size(3) and
                               to_integer(unsigned(count_mem(4))) = files_size(4) else
                               '1';

        wait for 250 ns;

        check_value(var_isERR_data    , '0', ERROR, "Error while checking decoded Pixel data against reference.",       C_SCOPE);
        check_value(var_isERR_dcs0    , '0', ERROR, "Error while checking decoded DCS data against reference, lane 0.", C_SCOPE);
        check_value(var_isERR_dcs1    , '0', ERROR, "Error while checking decoded DCS data against reference, lane 1.", C_SCOPE);
        check_value(var_isERR_dcs2    , '0', ERROR, "Error while checking decoded DCS data against reference, lane 2.", C_SCOPE);
        check_value(var_isERR_dcs3    , '0', ERROR, "Error while checking decoded DCS data against reference, lane 3.", C_SCOPE);
        check_value(var_isERR_count   , '0', ERROR, "Number of words checked against reference is not expected.",       C_SCOPE);
        check_value(var_isERR_data_dif, '0', ERROR, "Error while checking decoded Pixel data between lanes.",           C_SCOPE);
        check_value(var_isERR_dcs_dif , '0', ERROR, "Error while checking decoded DCS data between lanes.",             C_SCOPE);

        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        -- Finish the simulation
        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely
        finish;
    end process;

    -----------------------------------------------------------------------------
    -- Timeout
    -----------------------------------------------------------------------------
    timeoutproc : process
    begin
        wait for 1000 us;
        timeout <= true;
    end process;

end tb;

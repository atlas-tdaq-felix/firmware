--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--  ATLAS
--  FELIX UVVM test automation project
--  F. Schreuder
--  Nikhef
--  January 2020
----------------------------------------------------------------------------------
-- file: DecodingGearBox_tb.vhd.
----------------------------------------------------------------------------------
library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;

    use std.env.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

--library uvvm_vvc_framework;
--use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

--use work.vvc_methods_pkg.all;
--use work.td_vvc_framework_common_methods_pkg.all;
--use work.egr_bfm_pkg.all;


-- Test bench entity
entity DecodingGearBox_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end DecodingGearBox_tb;

architecture tb of DecodingGearBox_tb is


    signal reset, clk40, DataOutValid, BitSlip, ElinkAligned: std_logic;
    signal ElinkData: std_logic_vector(31 downto 0);
    signal ElinkData8b: std_logic_vector(7 downto 0); -- @suppress "signal ElinkData8b is never read"
    signal DataOut: std_logic_vector(39 downto 0);
    signal DataOut10b: std_logic_vector(9 downto 0); -- @suppress "signal DataOut10b is never read"
    constant dataIn_40b: std_logic_vector(39 downto 0) := x"98_7654_3305";
    --signal cnt: integer := 0;

    signal ElinkWidth: std_logic_vector(2 downto 0);
    signal InputBits : integer := 32;
    signal OutputBits: integer := 40;

    signal clock_ena     : boolean   := false;
    constant C_CLK_PERIOD_40     : time := 25 ns;
    signal GearboxAligned : std_logic := '0';
    signal AlignmentPulse : std_logic := '0';
    constant ReverseInputBits : std_logic := '0';
    signal reset_gearbox : std_logic;
begin

    ElinkData8b <= ElinkData(7 downto 0);

    -----------------------------------------------------------------------------
    -- Clock Generator
    -----------------------------------------------------------------------------
    clock_generator(clk40, clock_ena, C_CLK_PERIOD_40, "4 TB clock");

    reset_proc: process
    begin
        clock_ena <= true;
        reset <= '1';
        ElinkAligned <= '0';
        wait_num_rising_edge(clk40, 5);
        reset <= '0';
        wait_num_rising_edge(clk40, 1);
        ElinkAligned <= '1';

        wait;
    end process;

    width_proc: process

        procedure SetWidth(Inp: integer) is
        begin
            InputBits <= Inp;

            case Inp is
                when 2  => ElinkWidth <= "000";
                    OutputBits <= 10;
                when 4  => ElinkWidth <= "001";
                    OutputBits <= 10;
                when 8  => ElinkWidth <= "010";
                    OutputBits <= 10;
                when 16 => ElinkWidth <= "011";
                    OutputBits <= 20;
                when 32 => ElinkWidth <= "100";
                    OutputBits <= 40;
                when others => ElinkWidth <= "000";
            end case;

            --After setting the width, the gearbox should first go out of alignment
            reset_gearbox <= '1';
            wait_num_rising_edge(clk40, 1);
            reset_gearbox <= '0';
            wait_num_rising_edge(DataOutValid, 10);
            await_value(GearboxAligned, '1', 0 ns, 1 ms, TB_ERROR, "await alignment of gearbox", C_SCOPE);
            await_stable(GearboxAligned, 1000*C_CLK_PERIOD_40, FROM_NOW, 1000*C_CLK_PERIOD_40, FROM_NOW, TB_ERROR, "await stable alignment of the gearbox for 1000 clk periods", C_SCOPE);

        end procedure;
    begin
        reset_gearbox <= '1';
        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);

        --enable_log_msg(ALL_MESSAGES);
        disable_log_msg(ALL_MESSAGES);
        enable_log_msg (ID_CONSTRUCTOR);
        enable_log_msg(ID_LOG_HDR);
        enable_log_msg(ID_SEQUENCER);
        enable_log_msg(ID_UVVM_SEND_CMD);

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "----- initializing test bench       ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        -- Wait for UVVM to finish initialization
        --await_uvvm_initialization(VOID);

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "-----    end of initialisation        ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        wait_num_rising_edge(clk40, 1);
        SetWidth(8);
        SetWidth(4);
        SetWidth(2);
        SetWidth(16);
        SetWidth(32);
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);
        -- Finish the simulation
        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely

    end process;

    ElinkData_proc: process

        variable shift: integer range 0 to 128:= 39;
        variable dataIn_v: std_logic_vector(39 downto 0);
        variable ElinkData_v: std_logic_vector(InputBits-1 downto 0);

    begin
        dataIn_v := dataIn_40b;
        if shift >= InputBits-1 then
            ElinkData_v(InputBits-1 downto 0) := dataIn_v(shift downto shift-(InputBits-1));
        else
            ElinkData_v(InputBits-1 downto 0) := dataIn_v(shift downto 0) & dataIn_v(OutputBits-1 downto (OutputBits-1) - ((InputBits-2)-shift));
        end if;
        for i in 0 to InputBits-1 loop
            if ReverseInputBits = '1' then
                ElinkData((InputBits-1) -i) <= ElinkData_v(i); -- @suppress "Dead code"
            else
                ElinkData(i) <= ElinkData_v(i);
            end if;
        end loop;
        if shift > InputBits then
            shift := shift - InputBits;
        else
            shift := shift + (OutputBits-InputBits);
        end if;
        wait_num_rising_edge(clk40, 1);
    end process;

    AlignmentPulse_proc: process(clk40, reset)
        variable cnt: integer range 0 to 511;
    begin
        if reset = '1' then
            cnt := 0;
            AlignmentPulse <= '0';
        elsif rising_edge(clk40) then
            if cnt < 15 then
                cnt := cnt + 1;
                AlignmentPulse <= '0';
            else
                cnt := 0;
                AlignmentPulse <= '1';
            end if;
        end if;
    end process;

    bitslip_proc: process(clk40)
    begin
        if rising_edge(clk40) then
            BitSlip <= '0';
            if AlignmentPulse = '1' then
                BitSlip <= not GearboxAligned;
            end if;
            if DataOut(OutputBits-1 downto 0) = dataIn_40b(OutputBits-1 downto 0) and DataOutValid = '1' then
                GearboxAligned <= '1';
            elsif DataOutValid = '1' then
                GearboxAligned <= '0';
            end if;
        end if;
    end process;


    gb0: entity work.DecodingGearBox
        generic map(
            MAX_INPUT => 32,--: integer := 32;
            MAX_OUTPUT => 40,--: integer := 40;
            -- 32, 16, 8, 4, 2
            SUPPORT_INPUT => "11111" --: std_logic_vector(4 downto 0)
        )
        port    map(
            Reset            => reset_gearbox, --: in std_logic; -- Active high reset
            clk40            => clk40, --: in std_logic; -- BC clock

            ELinkData        => ElinkData, --: in std_logic_vector(MAX_INPUT-1 downto 0);
            ElinkAligned     => ElinkAligned, --: in std_logic;
            ElinkWidth       => ElinkWidth, --: in std_logic_vector( 2 downto 0); -- runtime configuration: 0:2, 1:4, 2:8, 3:16, 4:32
            MsbFirst         => '1',
            ReverseInputBits => ReverseInputBits,

            DataOut => DataOut, --: out std_logic_vector(MAX_OUTPUT-1 downto 0); -- Aligned output with set number of bits.
            DataOutValid => DataOutValid, --: out std_logic; --DataOut valid indicator

            BitSlip => BitSlip --: in std_logic --R Triggered by the protocol decoder to shift one bit

        );

    DataOut10b <= DataOut(9 downto 0);

end tb;

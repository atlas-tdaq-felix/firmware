--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use IEEE.MATH_REAL.ALL;

library uvvm_util;
    context uvvm_util.uvvm_util_context;
library bitvis_vip_axistream;
    use bitvis_vip_axistream.axistream_bfm_pkg.all;
    use bitvis_vip_axistream.vvc_methods_pkg.all;
    context bitvis_vip_axistream.vvc_context;
library bitvis_vip_scoreboard; -- @suppress "Library 'bitvis_vip_scoreboard' is not available"
library uvvm_vvc_framework;
    use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;


    use work.axi_stream_package.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;

entity CRToHost_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end CRToHost_tb;

architecture tb of CRToHost_tb is
    -- configuration
    constant C_SCOPE : string                       := "CRToHost_tb";
    constant C_CLK160_PERIOD : time                 := 6.25 ns;
    constant C_CLK40_PERIOD : time                  := 25 ns;
    constant C_CLK250_PERIOD : time                 := 4 ns;
    constant C_CLK400_PERIOD : time                 := 2.5 ns;
    constant LINK_NUM : integer                     := 4;
    constant STREAMS_TOHOST : integer    := 1;
    constant DATA_WIDTH : integer                   := 256;

    constant USE_URAM : boolean := false;

    -- clocks
    signal clk160 : std_logic;
    signal clk40 : std_logic;
    signal clk250 : std_logic;
    signal clk400 : std_logic;
    signal clk_ena : boolean := false;


    signal aresetn : std_logic;
    signal register_map_control : register_map_control_type;

    -- some test data
    --constant TESTDATA : t_slv_array := (x"00", x"FF", x"55", x"AA");
    constant LINK_CONFIG : IntArray(0 to LINK_NUM-1):= (0 to 0 => 1, others => 0);
    constant NUMBER_OF_DESCRIPTORS : integer := 2;
    constant NUMBER_OF_INTERRUPTS : integer := 8;
    constant toHostTimeoutBitn : integer := 16;
    constant BLOCKSIZE : integer := 1024;
    constant FIRMWARE_MODE : integer := FIRMWARE_MODE_FULL;
    signal s_axis : axis_32_2d_array_type(0 to LINK_NUM-1, 0 to STREAMS_TOHOST-1);
    signal m_axis : axis_32_2d_array_type(0 to LINK_NUM-1, 0 to STREAMS_TOHOST-1);
    signal s_axis_prog_empty : axis_tready_2d_array_type(0 to LINK_NUM-1, 0 to STREAMS_TOHOST-1);
    signal s_axis_aux : axis_32_array_type(0 to 1);
    signal m_axis_aux : axis_32_array_type(0 to 1);
    signal s_axis_aux_prog_empty : axis_tready_array_type(0 to 1);
    signal s_axis64 : axis_64_array_type(0 to LINK_NUM-1);
    signal m_axis64 : axis_64_array_type(0 to LINK_NUM-1);
    signal s_axis64_prog_empty : axis_tready_array_type(0 to LINK_NUM-1);
    signal toHostFifo_prog_full : std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
    signal register_map_xoff_monitor : register_map_xoff_monitor_type; -- @suppress "signal register_map_xoff_monitor is never read"
    signal register_map_crtohost_monitor : register_map_crtohost_monitor_type; -- @suppress "signal register_map_crtohost_monitor is never read"
    signal interrupt_call : std_logic_vector(NUMBER_OF_INTERRUPTS-1 downto 4); -- @suppress "signal interrupt_call is never read"
    signal s_axis_tready : axis_tready_2d_array_type(0 to LINK_NUM-1, 0 to STREAMS_TOHOST-1);
    signal m_axis_tready : axis_tready_2d_array_type(0 to LINK_NUM-1, 0 to STREAMS_TOHOST-1);
    signal s_axis_aux_tready : axis_tready_array_type(0 to 1);
    signal m_axis_aux_tready : axis_tready_array_type(0 to 1);
    signal s_axis64_tready : axis_tready_array_type(0 to LINK_NUM-1);
    signal m_axis64_tready : axis_tready_array_type(0 to LINK_NUM-1);
    signal toHostFifo_din : slv_array(0 to NUMBER_OF_DESCRIPTORS-2);
    signal toHostFifo_wr_en : std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
    signal toHostFifo_wr_clk : std_logic;
    signal toHostFifo_rst : std_logic; -- @suppress "signal toHostFifo_rst is never read"
    signal checker_done: std_logic;
    signal reset : std_logic;
    signal start_check: std_logic; -- @suppress "signal start_check is never read"
    signal TrailerError: std_logic; -- @suppress "signal TrailerError is never read"
    signal ChunkLengthError : std_logic; -- @suppress "signal ChunkLengthError is never read"
    signal CRCError : std_logic; -- @suppress "signal CRCError is never read"
    constant SIM_NUMBER_OF_BLOCKS : integer := 5000;
    signal Trunc_s : std_logic; -- @suppress "signal Trunc_s is never read"
    constant GC_AXISTREAM_BFM_CONFIG : t_axistream_bfm_config:= C_AXISTREAM_BFM_CONFIG_DEFAULT;
    --constant dma_enable_in: std_logic_vector(7 downto 0) := x"FD";
    signal CRTOHOST_DMA_DESCRIPTOR_WREN    : std_logic_vector(0 downto 0);
    signal CRTOHOST_DMA_DESCRIPTOR_DESCR   : std_logic_vector(2 downto 0);
    signal CRTOHOST_DMA_DESCRIPTOR_AXIS_ID : std_logic_vector(10 downto 0);
    signal clk40_stable : std_logic;
    signal daq_reset : std_logic;
    signal daq_fifo_flush : std_logic;

begin
    reset <= not aresetn;
    -- Instantiate the concurrent procedure that initializes UVVM
    i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

    g_URAM: if USE_URAM generate
        toHostFifo_wr_clk <= clk160; --Interlaken is never using ultraram, so we can always assign decoding_aclk to guarantee a single clock domain
    else generate --BRAM FIFO's in Virtex7 and Kintex Ultrascal
        g_toHostFifo_wr_clk_160: if FIRMWARE_MODE = FIRMWARE_MODE_GBT or FIRMWARE_MODE = FIRMWARE_MODE_LTDB generate
            toHostFifo_wr_clk <= clk160; --bandwidth is generally low, so we can lower the clock frequency.
        else generate
            toHostFifo_wr_clk <= clk250;
        end generate;
    end generate;

    -- clock generator
    clock_generator(clk160, clk_ena, C_CLK160_PERIOD, "160 MHz clock");
    clock_generator(clk40,  clk_ena, C_CLK40_PERIOD, "40 MHz clock");
    clock_generator(clk250, clk_ena, C_CLK250_PERIOD, "250 MHz clock");
    clock_generator(clk400, clk_ena, C_CLK400_PERIOD, "400 MHz clock");

    register_map_control.TIMEOUT_CTRL.ENABLE <= "1";
    register_map_control.TIMEOUT_CTRL.TIMEOUT <= x"0000FFFF";
    register_map_control.SUPER_CHUNK_FACTOR_LINK <= (others => x"01");

    register_map_control.DISCARD_DATA_FOR_DESCR.DMA_DISABLED <= x"02";
    register_map_control.DISCARD_DATA_FOR_DESCR.FIFO_FULL <= x"00";

    register_map_control.CRTOHOST_DMA_DESCRIPTOR_1.WR_EN <= CRTOHOST_DMA_DESCRIPTOR_WREN;
    register_map_control.CRTOHOST_DMA_DESCRIPTOR_1.DESCR <= CRTOHOST_DMA_DESCRIPTOR_DESCR;
    register_map_control.CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID <= CRTOHOST_DMA_DESCRIPTOR_AXIS_ID;

    CRTOHOST_DMA_DESCRIPTOR_WREN <= "0";
    CRTOHOST_DMA_DESCRIPTOR_DESCR <= "000";
    CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "00000000000";
    --associate_epath_id: process
    --begin
    --    wait until falling_edge(reset);
    --    wait for C_CLK40_PERIOD*10;
    --    CRTOHOST_DMA_DESCRIPTOR_WREN <= "1";
    --    CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "00000000000";
    --    CRTOHOST_DMA_DESCRIPTOR_DESCR   <= "000";
    --    wait for C_CLK40_PERIOD;
    --    CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "00001000000";
    --    CRTOHOST_DMA_DESCRIPTOR_DESCR   <= "001";
    --    wait for C_CLK40_PERIOD;
    --    CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "00010000000";
    --    CRTOHOST_DMA_DESCRIPTOR_DESCR   <= "010";
    --    wait for C_CLK40_PERIOD;
    --    CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "00011000000";
    --    CRTOHOST_DMA_DESCRIPTOR_DESCR   <= "011";
    --    wait for C_CLK40_PERIOD;
    --    CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "00100000000";
    --    CRTOHOST_DMA_DESCRIPTOR_DESCR  <= "000";
    --    wait for C_CLK40_PERIOD;
    --    CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "00101000000";
    --    CRTOHOST_DMA_DESCRIPTOR_DESCR  <= "001";
    --    wait for C_CLK40_PERIOD;
    --    CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "00110000000";
    --    CRTOHOST_DMA_DESCRIPTOR_DESCR   <= "010";
    --    wait for C_CLK40_PERIOD;
    --    CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "00111000000";
    --    CRTOHOST_DMA_DESCRIPTOR_DESCR   <= "011";
    --    wait for C_CLK40_PERIOD;
    --    CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "01000000000";
    --    CRTOHOST_DMA_DESCRIPTOR_DESCR   <= "000";
    --    wait for C_CLK40_PERIOD;
    --    CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "01001000000";
    --    CRTOHOST_DMA_DESCRIPTOR_DESCR   <= "001";
    --    wait for C_CLK40_PERIOD;
    --    CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "01010000000";
    --    CRTOHOST_DMA_DESCRIPTOR_DESCR   <= "010";
    --    wait for C_CLK40_PERIOD;
    --    CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "01011000000";
    --    CRTOHOST_DMA_DESCRIPTOR_DESCR   <= "011";
    --    wait for C_CLK40_PERIOD;
    --    CRTOHOST_DMA_DESCRIPTOR_WREN <= "0";
    --    wait;
    --end process;

    -- device under test

    resetmgr0: entity work.CRresetManager
        port map(
            clk40         => clk40,
            rst           => reset,
            clk40_stable  => clk40_stable,
            cr_rst        => daq_reset,
            cr_fifo_flush => daq_fifo_flush
        );


    dut: entity work.CRToHost
        generic map (
            NUMBER_OF_DESCRIPTORS => NUMBER_OF_DESCRIPTORS,
            NUMBER_OF_INTERRUPTS => NUMBER_OF_INTERRUPTS,
            LINK_NUM => LINK_NUM,
            LINK_CONFIG => LINK_CONFIG,
            toHostTimeoutBitn => toHostTimeoutBitn,
            STREAMS_TOHOST => STREAMS_TOHOST,
            BLOCKSIZE => BLOCKSIZE,
            DATA_WIDTH => DATA_WIDTH,
            FIRMWARE_MODE => FIRMWARE_MODE,
            USE_URAM => USE_URAM
        )
        port map (
            clk40 => clk40,
            aclk_tohost => clk160,
            aclk64_tohost => clk400,
            toHostFifo_wr_clk => toHostFifo_wr_clk,
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            register_map_control => register_map_control,
            register_map_xoff_monitor => register_map_xoff_monitor,
            register_map_crtohost_monitor => register_map_crtohost_monitor,
            interrupt_call => interrupt_call,
            xoff_out => open,
            s_axis => s_axis,
            s_axis_tready => s_axis_tready,
            s_axis_prog_empty => s_axis_prog_empty,
            s_axis_aux => s_axis_aux,
            s_axis_aux_tready => s_axis_aux_tready,
            s_axis_aux_prog_empty => s_axis_aux_prog_empty,
            s_axis64 => s_axis64,
            s_axis64_tready => s_axis64_tready,
            s_axis64_prog_empty => s_axis64_prog_empty,
            dma_enable_in => (others => '1'), --dma_enable_in(NUMBER_OF_DESCRIPTORS-1 downto 0),
            toHostFifo_din => toHostFifo_din,
            toHostFifo_wr_en => toHostFifo_wr_en,
            toHostFifo_prog_full => toHostFifo_prog_full, --"00"&toHostFifo_prog_full(1)&"0",
            toHostFifo_rst => toHostFifo_rst
        );


    -- AXI streaming masters
    gbt_generate: for GBT in 0 to LINK_NUM-1 generate
        signal axistream64_vvc_if : t_axistream_if(
            tdata(63 downto 0),
            tkeep(7 downto 0),
            tuser(3 downto 0),
            tstrb(7 downto 0),
            tid(7 downto 0),
            tdest(0 downto 0)
        ) :=
 init_axistream_if_signals(true, 64, 4, 8, 1);
    begin
        g_AXIs64_VVC: if LINK_CONFIG(GBT) = 1 generate
            axi_stream_vvc_I: entity bitvis_vip_axistream.axistream_vvc
                generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
                    GC_AXISTREAM_BFM_CONFIG => GC_AXISTREAM_BFM_CONFIG,
                    GC_VVC_IS_MASTER => true,
                    GC_DATA_WIDTH => 64,
                    GC_USER_WIDTH => 4,
                    GC_ID_WIDTH => 8,
                    GC_DEST_WIDTH => 1,
                    GC_INSTANCE_IDX => GBT
                )
                port map (
                    clk => clk400,
                    axistream_vvc_if => axistream64_vvc_if
                );

            m_axis64(GBT).tkeep  <= axistream64_vvc_if.tkeep;
            m_axis64(GBT).tdata  <= axistream64_vvc_if.tdata;
            m_axis64(GBT).tvalid <= axistream64_vvc_if.tvalid;
            m_axis64(GBT).tlast  <= axistream64_vvc_if.tlast;
            m_axis64(GBT).tuser  <= axistream64_vvc_if.tuser;
            m_axis64(GBT).tid    <= axistream64_vvc_if.tid;
            axistream64_vvc_if.tready <= m_axis64_tready(GBT);

            fifo0: entity work.Axis64Fifo
                generic map(
                    DEPTH => BLOCKSIZE/2
                )
                port map(
                    s_axis_aresetn => aresetn,
                    s_axis_aclk => clk400,
                    s_axis => m_axis64(GBT),
                    s_axis_tready => m_axis64_tready(GBT),
                    m_axis_aclk => clk400,
                    m_axis => s_axis64(GBT),
                    m_axis_tready => s_axis64_tready(GBT),
                    m_axis_prog_empty => s_axis64_prog_empty(GBT)
                );

        else generate
            s_axis64(GBT).tkeep  <= "00000000";
            s_axis64(GBT).tdata  <= x"0000_0000_0000_0000";
            s_axis64(GBT).tvalid <= '0';
            s_axis64(GBT).tlast  <= '0';
            s_axis64(GBT).tuser  <= "0000";
            s_axis64_prog_empty(GBT) <= '1';
        end generate;


        stream_generate: for STREAM in 0 to STREAMS_TOHOST-1 generate
            signal axistream_vvc_if : t_axistream_if(
                tdata(31 downto 0),
                tkeep(3 downto 0),
                tuser(3 downto 0),
                tstrb(3 downto 0),
                tid(7 downto 0),
                tdest(0 downto 0)
            ) :=
 init_axistream_if_signals(true, 32, 4, 8, 1);
        begin

            g_STREAM0: if STREAM = 0 and LINK_CONFIG(GBT) = 0 generate

                axi_stream_vvc_I: entity bitvis_vip_axistream.axistream_vvc
                    generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
                        GC_AXISTREAM_BFM_CONFIG => GC_AXISTREAM_BFM_CONFIG,
                        GC_VVC_IS_MASTER => true,
                        GC_DATA_WIDTH => 32,
                        GC_USER_WIDTH => 4,
                        GC_ID_WIDTH => 8,
                        GC_DEST_WIDTH => 1,
                        GC_INSTANCE_IDX => GBT
                    )
                    port map (
                        clk => clk160,
                        axistream_vvc_if => axistream_vvc_if
                    );

                m_axis(GBT,STREAM).tkeep <= axistream_vvc_if.tkeep;
                m_axis(GBT,STREAM).tdata <= axistream_vvc_if.tdata;
                m_axis(GBT,STREAM).tvalid <= axistream_vvc_if.tvalid;
                m_axis(GBT,STREAM).tlast <= axistream_vvc_if.tlast;
                m_axis(GBT,STREAM).tuser <= axistream_vvc_if.tuser;
                axistream_vvc_if.tready <= m_axis_tready(GBT,STREAM);

                fifo0: entity work.Axis32Fifo
                    generic map(
                        DEPTH => BLOCKSIZE/2,
                        --CLOCKING_MODE => "independent_clock",
                        --RELATED_CLOCKS => 1,
                        --FIFO_MEMORY_TYPE => "auto",
                        --PACKET_FIFO => "false",
                        USE_BUILT_IN_FIFO => '0',
                        VERSAL => false,
                        BLOCKSIZE => BLOCKSIZE,
                        ISPIXEL => false
                    )
                    port map(
                        s_axis_aresetn => aresetn,
                        s_axis_aclk => clk160,
                        s_axis => m_axis(GBT,STREAM),
                        s_axis_tready => m_axis_tready(GBT,STREAM),
                        m_axis_aclk => clk160,
                        m_axis => s_axis(GBT,STREAM),
                        m_axis_tready => s_axis_tready(GBT,STREAM),
                        m_axis_prog_empty => s_axis_prog_empty(GBT,STREAM)
                    );


            else generate
                s_axis(GBT,STREAM).tvalid <= '0';
                s_axis_prog_empty(GBT,STREAM) <= '1';
            end generate;
        end generate;
    end generate;

    aux_generate: for AUX_NUM in 0 to 1 generate
        signal axistream_aux_vvc_if : t_axistream_if(
            tdata(31 downto 0),
            tkeep(3 downto 0),
            tuser(3 downto 0),
            tstrb(3 downto 0),
            tid(7 downto 0),
            tdest(0 downto 0)
        ) :=
 init_axistream_if_signals(true, 32, 4, 8, 1);
    begin
        axi_stream_vvc_I: entity bitvis_vip_axistream.axistream_vvc
            generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
                GC_AXISTREAM_BFM_CONFIG => GC_AXISTREAM_BFM_CONFIG,
                GC_VVC_IS_MASTER => true,
                GC_DATA_WIDTH => 32,
                GC_USER_WIDTH => 4,
                GC_ID_WIDTH => 8,
                GC_DEST_WIDTH => 1,
                GC_INSTANCE_IDX => LINK_NUM+AUX_NUM
            )
            port map (
                clk => clk160,
                axistream_vvc_if => axistream_aux_vvc_if
            );

        m_axis_aux(AUX_NUM).tkeep  <= axistream_aux_vvc_if.tkeep;
        m_axis_aux(AUX_NUM).tdata  <= axistream_aux_vvc_if.tdata;
        m_axis_aux(AUX_NUM).tvalid <= axistream_aux_vvc_if.tvalid;
        m_axis_aux(AUX_NUM).tlast  <= axistream_aux_vvc_if.tlast;
        m_axis_aux(AUX_NUM).tuser  <= axistream_aux_vvc_if.tuser;
        axistream_aux_vvc_if.tready <= m_axis_aux_tready(AUX_NUM);

        fifo0: entity work.Axis32Fifo
            generic map(
                DEPTH => BLOCKSIZE/2,
                --CLOCKING_MODE => "independent_clock",
                --RELATED_CLOCKS => 1,
                --FIFO_MEMORY_TYPE => "auto",
                --PACKET_FIFO => "false",
                USE_BUILT_IN_FIFO => '0',
                VERSAL => false,
                BLOCKSIZE => BLOCKSIZE,
                ISPIXEL => false
            )
            port map(
                s_axis_aresetn => aresetn,
                s_axis_aclk => clk160,
                s_axis => m_axis_aux(AUX_NUM),
                s_axis_tready => m_axis_aux_tready(AUX_NUM),
                m_axis_aclk => clk160,
                m_axis => s_axis_aux(AUX_NUM),
                m_axis_tready => s_axis_aux_tready(AUX_NUM),
                m_axis_prog_empty => s_axis_aux_prog_empty(AUX_NUM)
            );

    end generate;

    sequencer : process

    begin
        uvvm_completed <= '0';
        clk_ena <= false;
        aresetn <= '0';
        clk40_stable <= '0';

        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);

        --enable_log_msg(ALL_MESSAGES);
        disable_log_msg(ALL_MESSAGES, NON_QUIET, C_SCOPE);
        enable_log_msg(ID_SEQUENCER, NON_QUIET, C_SCOPE);
        enable_log_msg(ID_LOG_HDR);
        disable_log_msg(ID_POS_ACK, NON_QUIET, C_SCOPE);

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "----- initializing test bench       ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        -- Wait for UVVM to finish initialization
        await_uvvm_initialization(VOID);
        for ID in 0 to LINK_NUM+1 loop
            disable_log_msg(AXISTREAM_VVCT, ID, ID_PACKET_DATA); --@suppress
            disable_log_msg(AXISTREAM_VVCT, ID, ID_CMD_INTERPRETER);--@suppress
            disable_log_msg(AXISTREAM_VVCT, ID, ID_CMD_INTERPRETER_WAIT);--@suppress
            disable_log_msg(AXISTREAM_VVCT, ID, ID_PACKET_INITIATE);--@suppress
            disable_log_msg(AXISTREAM_VVCT, ID, ID_PACKET_COMPLETE);--@suppress
            disable_log_msg(AXISTREAM_VVCT, ID, ID_CMD_EXECUTOR);--@suppress
        end loop;
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "-----    end of initialisation        ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        wait for (40*C_CLK160_PERIOD);
        clk_ena <= true;
        wait for (20*C_CLK160_PERIOD);
        clk40_stable <= '1';
        wait for (20*C_CLK160_PERIOD);
        aresetn <= '1';
        wait for (20*C_CLK160_PERIOD);

        for i in 0 to LINK_NUM+1 loop --Link num +2 (-1) for AUX axistreams
            shared_axistream_vvc_config(i).bfm_config.max_wait_cycles := 100000;
            shared_axistream_vvc_config(i).bfm_config.max_wait_cycles_severity := ERROR;
            shared_axistream_vvc_config(i).bfm_config.clock_period_margin := 0 ns;
            shared_axistream_vvc_config(i).bfm_config.bfm_sync := SYNC_ON_CLOCK_ONLY;
            shared_axistream_vvc_config(i).bfm_config.match_strictness := MATCH_EXACT;
            shared_axistream_vvc_config(i).bfm_config.byte_endianness := LOWER_BYTE_LEFT;
            shared_axistream_vvc_config(i).bfm_config.check_packet_length := false;
            shared_axistream_vvc_config(i).bfm_config.protocol_error_severity := ERROR;
        end loop;

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "-- test bench sequence starts here      ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);
        log(ID_LOG_HDR, "Starting simulation of TB for CRToHost using VVCs", C_SCOPE);


        await_value(checker_done, '1', 0 ns, 1000 ms, TB_ERROR, "wait for all tests to finish", C_SCOPE);
        enable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        enable_log_msg(ID_SEQUENCER);
        enable_log_msg(ID_UVVM_SEND_CMD);
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);
        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- checker done in FELIX_DataSink
    end process;

    axis_transmitter: process
        constant MAX_CHUNK_LENGTH: integer := 2048;
        variable CHUNK_LENGTH : integer range 0 to MAX_CHUNK_LENGTH;
        type IntArray is array(0 to LINK_NUM+1) of integer;
        variable TID: std_logic_vector(7 downto 0) := x"00";
        variable L1IDs: IntArray := (others => 0);
        variable r: integer; --contains random number
        variable chunk: t_byte_array(0 to MAX_CHUNK_LENGTH-1); --t_byte_array := (
        --    x"AA",x"00",x"38",x"00",x"00",x"BB",x"AA",x"20",
        --    x"00",x"01",x"02",x"03",x"04",x"05",x"06",x"07",
        --    x"08",x"09",x"0A",x"0B",x"0C",x"0D",x"0E",x"0F",
        --    x"10",x"11",x"12",x"13",x"14",x"15",x"16",x"17",
        --    x"18",x"19",x"1A",x"1B",x"1C",x"1D",x"1E",x"1F",
        --    x"20",x"21",x"22",x"23",x"24",x"25",x"26",x"27",
        --    x"28",x"29",x"2A",x"2B",x"2C",x"2D",x"2E",x"2F",
        --    x"30",x"31",x"32",x"33",x"34",x"35",x"36",x"37"
        --);
        impure function create_chunk(L1ID: integer)return t_byte_array is
            variable temp_chunk: t_byte_array(0 to MAX_CHUNK_LENGTH-1);
            variable len: integer;
            variable len16: std_logic_vector(15 downto 0);
            variable L1ID16: std_logic_vector(15 downto 0);
        begin
            len := random(8, MAX_CHUNK_LENGTH);
            temp_chunk(0) := x"AA";
            len16 := std_logic_vector(to_unsigned(len-8, 16));
            temp_chunk(1) := len16(15 downto 8);
            temp_chunk(2) := len16(7 downto 0);
            L1ID16 := std_logic_vector(to_unsigned(L1ID, 16));
            temp_chunk(3) := L1ID16(15 downto 8);
            temp_chunk(4) := L1ID16(7 downto 0);
            temp_chunk(5) := x"BB";
            temp_chunk(6) := x"AA";
            temp_chunk(7) := x"20";
            for i in 0 to len-9 loop
                temp_chunk(i+8) := std_logic_vector(to_unsigned(i mod 256,8));
            end loop;
            CHUNK_LENGTH := len;
            return temp_chunk;
        end function;
    begin
        wait for (20*C_CLK160_PERIOD);
        wait until aresetn = '1';
        wait for (20*C_CLK160_PERIOD);
        while checker_done = '0' loop
            for ID in 0 to LINK_NUM+1 loop
                r := random(0,9);
                if ID = 0 and r=0 then
                    TID := TID(7 downto 1) & not TID(0); --Alternate between 0 and 1
                end if;
                chunk := create_chunk(L1IDs(ID));
                if(shared_axistream_vvc_status(ID).pending_cmd_cnt) < 950 then --Don't fill the command queue up for more than 1000 per AXIs VCC
                    axistream_transmit_bytes(--@suppress
                        VVCT => AXISTREAM_VVCT,
                        vvc_instance_idx => ID,
                        data_array => chunk(0 to CHUNK_LENGTH-1),
                        user_array => (0 to CHUNK_LENGTH-1 => "00000000"),
                        strb_array => (0 to CHUNK_LENGTH-1 => x"00000000"),
                        id_array => (0 to CHUNK_LENGTH-1 => TID),
                        dest_array => (0 to CHUNK_LENGTH-1 => "0000"),
                        msg => "Sending AXI4 Stream data"); --@suppress
                    L1IDs(ID) := L1IDs(ID)+1;
                end if;
            end loop;
            wait for 1 ns;
        --wait until rising_edge(s_axis64_tready(0)); --Tready goes low for every AXIs packet, wait to prevent the queue from building up.
        end loop;
        wait;
    end process;


    sink0: entity work.FELIXDataSink
        generic map(
            NUMBER_OF_DESCRIPTORS           => NUMBER_OF_DESCRIPTORS,
            BLOCKSIZE                       => BLOCKSIZE,
            GBT_NUM                         => LINK_NUM,
            FIRMWARE_MODE                   => FIRMWARE_MODE,
            DATA_WIDTH                      => DATA_WIDTH,
            SIM_NUMBER_OF_BLOCKS            => SIM_NUMBER_OF_BLOCKS,
            APPLY_BACKPRESSURE_AFTER_BLOCKS => SIM_NUMBER_OF_BLOCKS/10
        )
        port map(
            checker_done         => checker_done,
            toHostFifo_din       => toHostFifo_din,
            toHostFifo_wr_en     => toHostFifo_wr_en,
            toHostFifo_prog_full => toHostFifo_prog_full,
            toHostFifo_wr_clk    => toHostFifo_wr_clk,
            reset                => reset,
            start_check          => start_check,
            Trunc                => Trunc_s,
            TrailerError         => TrailerError,
            CRCError             => CRCError,
            ChunkLengthError     => ChunkLengthError,
            register_map_control => register_map_control
        );

end architecture;

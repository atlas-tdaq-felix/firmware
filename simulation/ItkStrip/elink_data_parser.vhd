--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : Testbench helper module for sending elink data to Strips modules
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : ttc_elink_data_parser.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Wed May 28 20:00:40 2020
-- Last update : Wed May 28 20:00:40 2020
-- Platform    : Default Part Number
-- Standard    : <VHDL-2008>
--------------------------------------------------------------------------------
-- Copyright (c) 2019 BNL
-------------------------------------------------------------------------------
-- Description:
--
-- This module is not synthesizable.
--
-- Loads specified file containing bypass data
-- and returns it frame by frame
--
-------------------------------------------------------------------------------
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use std.textio.all;
    use ieee.std_logic_textio.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

package elink_data_parser is
    constant record_max_length     : natural := 1000;
    constant elink_data_max_length : natural := 20;

    type t_byte_array is array (natural range 0 to elink_data_max_length-1) of std_logic_vector(7 downto 0);
    type t_elink_transaction_record is record
        count : natural range 1 to elink_data_max_length;
        data  : t_byte_array;
    end record t_elink_transaction_record;

    type t_elink_data_array is array (natural range 0 to record_max_length - 1) of t_elink_transaction_record;

    type t_elink_data_record is record
        inputs_count   : natural;
        outputs_count   : natural;
        bytes_total : natural;
        inputs  : t_elink_data_array;
        outputs : t_elink_data_array;
    end record t_elink_data_record;

    type t_elink_file_parser is protected
    -- Loads text file with the provided name, parse and return the dataset
    impure function parse_file(
        file_name_inputs  : string;
        file_name_outputs : string
    ) return t_elink_data_record;
	end protected t_elink_file_parser;
end package elink_data_parser;

package body elink_data_parser is
    type t_elink_file_parser is protected body
    file hfile          : TEXT;
    variable fstatus    : file_open_status;
    variable buf        : LINE;
    --variable ptr        : integer;
    variable data_count : natural := 0;
    variable line_ctr   : natural := 0;
    variable byte_ctr   : natural := 0;
    variable result     : t_elink_data_record;
    variable good       : boolean;

    impure function parse_file(
        file_name_inputs  : string;
        file_name_outputs : string
    ) return t_elink_data_record is
    begin
        -- loading module inputs from file
        log(ID_SEQUENCER, "Loading elink data (inputs) from " & file_name_inputs & " ...");
        data_count := 0;
        result.bytes_total := 0;
        file_open(fstatus, hfile, file_name_inputs, read_mode);
        if fstatus /= open_ok then
            error("Could not open the file: " & file_name_inputs);
        else
            line_ctr := 0;
            byte_ctr := 0;
            result.bytes_total := 0;
            while not (endfile(hfile)) loop
                line_ctr := line_ctr + 1;
                readline(hfile, buf);
                if buf(1) = '-' then
                    byte_ctr := 0;
                    data_count := data_count + 1 when line_ctr /= 1;
						next;
                else
                    hread(buf, result.inputs(data_count).data(byte_ctr), good);
                    if good then
                        byte_ctr := byte_ctr + 1;
                        result.inputs(data_count).count := byte_ctr;
                        result.bytes_total := result.bytes_total + 1;
                    else
                        error("Issue parsing file " & file_name_inputs & "line " & to_string(line_ctr));
                    end if;
                end if;
            end loop;
            data_count := data_count + 1;
            log(ID_SEQUENCER, "Parsed file " & file_name_inputs & " successfully, loaded " & to_string(data_count) & " elink transactions (inputs)");
        end if;
        file_close(hfile);
        result.inputs_count := data_count;

        -- loading module outputs from file
        log(ID_SEQUENCER, "Loading elink data (outputs) from " & file_name_outputs & " ...");
        data_count := 0;
        file_open(fstatus, hfile, file_name_outputs, read_mode);
        if fstatus /= open_ok then
            error("Could not open the file: " & file_name_outputs);
        else
            line_ctr := 0;
            byte_ctr := 0;
            while not (endfile(hfile)) loop
                line_ctr := line_ctr + 1;
                readline(hfile, buf);
                if buf(1) = '-' then
                    byte_ctr := 0;
                    data_count := data_count + 1 when line_ctr /= 1;
						next;
                else
                    hread(buf, result.outputs(data_count).data(byte_ctr), good);
                    if good then
                        byte_ctr := byte_ctr + 1;
                        result.outputs(data_count).count := byte_ctr;
                    else
                        error("Issue parsing file " & file_name_outputs & "line " & to_string(line_ctr));
                    end if;
                end if;
            end loop;
            data_count := data_count + 1;
            log(ID_SEQUENCER, "Parsed file " & file_name_outputs & " successfully, loaded " & to_string(data_count) & " elink transactions (outputs)");
        end if;
        file_close(hfile);

        result.outputs_count := data_count;
        return result;
    end;
	end protected body;
end package body elink_data_parser;

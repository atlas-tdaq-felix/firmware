--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.strips_package.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

library uvvm_vvc_framework;
    use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

library bitvis_vip_axistream;
    context bitvis_vip_axistream.vvc_context;

library bitvis_vip_clock_generator;
    context bitvis_vip_clock_generator.vvc_context;

entity tb_strips_configuration_decoder is
    generic(
        use_vunit: boolean := false -- @suppress "Unused generic: use_vunit is not used in work.tb_strips_configuration_decoder(RTL)"
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity tb_strips_configuration_decoder;

architecture RTL of tb_strips_configuration_decoder is
    constant C_CLK_PERIOD          : time    := 25 ns;
    constant C_CYCLES_RST          : integer := 3;
    --constant C_STABLE_PERIODS      : integer := 10;
    --constant C_MAX_CYCLES_PER_BYTE : integer := 4;
    constant C_USER_WIDTH          : integer := 1;
    constant C_ID_WIDTH            : integer := 1;
    constant C_DEST_WIDTH          : integer := 1;
    constant C_DATA_WIDTH          : integer := 8;
    constant TIMEOUT          : integer := 40;

    signal clk           : std_logic;   -- 40 MHz BC clk
    --signal clk_en        : boolean := false;
    signal rst           : std_logic := '1';

    -- register map control interface
    signal regmap_wr_en_o : std_logic;  -- pulse this field to update
    signal regmap_data_o  : std_logic_vector(15 downto 0); -- new register value
    signal regmap_addr_o  : std_logic_vector(7 downto 0); -- register address

    -- AXI stream data source
    signal axi_in : t_axistream_if(tdata(C_DATA_WIDTH - 1 downto 0),
                                    tkeep((C_DATA_WIDTH / 8) - 1 downto 0),
                                    tuser(C_USER_WIDTH - 1 downto 0),
                                    tstrb((C_DATA_WIDTH / 8) - 1 downto 0),
                                    tid(C_ID_WIDTH - 1 downto 0),
                                    tdest(C_DEST_WIDTH - 1 downto 0)
                                   );

    -- saved signals when regmap_wr_en_o = '1'
    signal regmap_data      : std_logic_vector(regmap_data_o'range);
    signal regmap_addr      : std_logic_vector(regmap_addr_o'range);
    signal regmap_time      : time := 0 ns;
    signal regmap_time_prev : time := 0 ns;     -- @suppress "signal regmap_time_prev is never read"

begin
    -----------------------------------------------------------------------------
    -- Instantiate UVVM entities
    -----------------------------------------------------------------------------
    i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

    i_clock_generator_vvc : entity bitvis_vip_clock_generator.clock_generator_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_INSTANCE_IDX    => 1,
            GC_CLOCK_NAME      => "Clock",
            GC_CLOCK_PERIOD    => C_CLK_PERIOD,
            GC_CLOCK_HIGH_TIME => C_CLK_PERIOD / 2
        )
        port map(
            clk => clk
        );

    i_axistream_vvc_master : entity bitvis_vip_axistream.axistream_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_AXISTREAM_BFM_CONFIG, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => true,
            GC_DATA_WIDTH    => C_DATA_WIDTH,
            GC_USER_WIDTH    => C_USER_WIDTH,
            GC_ID_WIDTH      => C_ID_WIDTH,
            GC_DEST_WIDTH    => C_DEST_WIDTH,
            GC_INSTANCE_IDX  => 2)
        port map(
            clk              => clk,
            axistream_vvc_if => axi_in);

    test : process is

        -- Wait for the mock device to receive a pulse (to latch the data)
        procedure wait_for_pulse(
            signal pulse : std_logic;
            wait_enable  : boolean := true
        ) is
        begin
            if wait_enable then
                await_value(pulse, '1', 0 ns, 100 * C_CLK_PERIOD, ERROR,
                    "Wait for the command to be issued (rising edge)");
                await_value(pulse, '0', C_CLK_PERIOD / 2, 1.5 * C_CLK_PERIOD,
                    ERROR, "Wait for the command to be issued (falling edge)");
            end if;
        end;

        --- Regmap register write: enqueue and verify functions

        procedure verify_regmap_wr(
            addr        : std_logic_vector(7 downto 0);
            data        : std_logic_vector(15 downto 0);
            wait_enable : boolean := true
        ) is
        begin
            wait_for_pulse(regmap_wr_en_o, wait_enable);
            check_value(regmap_addr, addr, ERROR, "Verify register map address");
            check_value(regmap_data, data, ERROR, "Verify register map data");
        end;

        procedure insert_random_delay(max_delay : integer) is
            variable delay_clk_cycles : integer := 5;
        begin
            delay_clk_cycles := random(0, max_delay);
            insert_delay(AXISTREAM_VVCT, 2, delay_clk_cycles * C_CLK_PERIOD);--@suppress
        end;

        procedure enqueue_regmap_wr(
            addr        : std_logic_vector(7 downto 0);
            data        : std_logic_vector(15 downto 0);
            insert_delays : boolean := false
        ) is
            constant max_delay : integer := 5;
        begin
            if insert_delays then
                log(ID_SEQUENCER, "Writing local configuration register " & to_hstring(addr) & " = " & to_hstring(data));
                axistream_transmit(AXISTREAM_VVCT, 2, ITK_CMD_REGMAP_WRITE, "Send Opcode");--@suppress
                insert_random_delay(max_delay);
                axistream_transmit(AXISTREAM_VVCT, 2, data(15 downto 8), "Send data MSB");--@suppress
                insert_random_delay(max_delay);
                axistream_transmit(AXISTREAM_VVCT, 2, data(7 downto 0), "Send data LSB");--@suppress
                insert_random_delay(max_delay);
                axistream_transmit(AXISTREAM_VVCT, 2, addr, "Send address");--@suppress
                insert_random_delay(max_delay);
            else
                axistream_transmit(AXISTREAM_VVCT, 2,--@suppress
                    t_slv_array'(ITK_CMD_REGMAP_WRITE, data(15 downto 8), data(7 downto 0), addr),
                    "Writing local configuration register " & to_hstring(addr) & " = " & to_hstring(data));
            end if;
        end;

        procedure enqueue_and_verify_regmap_wr(
            addr        : std_logic_vector(7 downto 0);
            data        : std_logic_vector(15 downto 0);
            insert_delays : boolean := false
        ) is
        begin
            enqueue_regmap_wr(addr, data, insert_delays);
            verify_regmap_wr(addr, data);
        end;

        procedure verify_timeout is
        begin
            await_completion(AXISTREAM_VVCT, 2, 40 * C_CLK_PERIOD, "Wait until the DUT indicates ready");--@suppress
            wait_num_rising_edge(clk, TIMEOUT + 10);
            check_stable(regmap_wr_en_o, (TIMEOUT + 10) * C_CLK_PERIOD, ERROR,  "Configuration is not changed");
        end;

        -- set RST to a given value and wait C_CYCLES_RST cycles
        procedure set_rst(
            value    : std_logic;
            duration : positive := C_CYCLES_RST
        ) is
        begin
            wait until rising_edge(clk);
            rst <= value;
            wait_num_rising_edge(clk, duration);
        end;

    begin
        ---------------------------------------------------
        -- Actual tests begin here
        ---------------------------------------------------
        await_uvvm_initialization(VOID);
        start_clock(CLOCK_GENERATOR_VVCT, 1, "Start clock generator");--@suppress

        disable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        enable_log_msg(ID_SEQUENCER);
        enable_log_msg(ID_UVVM_SEND_CMD);

        disable_log_msg(CLOCK_GENERATOR_VVCT, 1, ALL_MESSAGES);--@suppress
        enable_log_msg(CLOCK_GENERATOR_VVCT, 1, ID_BFM);--@suppress
        disable_log_msg(AXISTREAM_VVCT, 2, ALL_MESSAGES);--@suppress
        enable_log_msg(AXISTREAM_VVCT, 2, ID_BFM);--@suppress


        log(ID_LOG_HDR, "Check behavior when rst = '1'");
        set_rst('1');
        check_value(regmap_wr_en_o, '0', ERROR, "Nothing is written to the local regmap (regmap_wr_en_o = '0')");
        check_value(axi_in.tready, '1', ERROR, "tready=1 when rst is asserted");

        log(ID_LOG_HDR, "Check that configuration commands are decoded correctly (fast write)");
        set_rst('0');
        wait_num_rising_edge(clk, 5);

        for j in 0 to 20 loop
            enqueue_and_verify_regmap_wr(random(8), random(16), insert_delays => false);
        end loop;

        log(ID_LOG_HDR, "Check that timed out valid commands are dropped");
        set_rst('0');
        wait_num_rising_edge(clk, 5);
        -- TODO: send valid commands slow enough for them to time out
        -- during s_read_opcode or s_read_args
        for j in 0 to 10 loop
            axistream_transmit(AXISTREAM_VVCT, 2, ITK_CMD_REGMAP_WRITE,--@suppress
                "Writing incomplete local configuration command (opcode only)");
            verify_timeout;
            enqueue_and_verify_regmap_wr(random(8), random(16));
            axistream_transmit(AXISTREAM_VVCT, 2, t_slv_array'(ITK_CMD_REGMAP_WRITE, random(8)),--@suppress
                "Writing incomplete local configuration command (opcode + one data byte)");
            verify_timeout;
            enqueue_and_verify_regmap_wr(random(8), random(16));
            axistream_transmit(AXISTREAM_VVCT, 2, t_slv_array'(ITK_CMD_REGMAP_WRITE, random(8), random(8)),--@suppress
                "Writing incomplete local configuration command (opcode + two data bytes)");
            verify_timeout;
            enqueue_and_verify_regmap_wr(random(8), random(16));
        end loop;

        log(ID_LOG_HDR, "Check that configuration commands are decoded correctly (slow write)");
        set_rst('0');
        wait_num_rising_edge(clk, 5);
        for j in 0 to 20 loop
            enqueue_and_verify_regmap_wr(random(8), random(16), insert_delays => true);
        end loop;

        log(ID_LOG_HDR, "Check that invalid commands are ignored");
        axistream_transmit(AXISTREAM_VVCT, 2,--@suppress
            t_slv_array'((x"23", x"F2", x"E7", x"E8")), "Writing invalid commands");
        await_stable(regmap_wr_en_o, 10 ns, FROM_NOW, 1000 ns, FROM_NOW, ERROR,
            "Configuration is not changed");

        -- Ending the simulation
        wait for 1000 ns;                -- to allow some time for completion
        report_alert_counters(FINAL);   -- Report final counters and print conclusion for simulation (Success/Fail)
        uvvm_completed <= '1';
        wait for 1 us;
        stop_clock(CLOCK_GENERATOR_VVCT, 1, "Stop clock generator");--@suppress
        wait;

    end process;

    DUT : entity work.strips_configuration_decoder
        generic map (TIMEOUT => TIMEOUT)
        port map(
            clk => clk,
            rst => rst,
            -- elink command source
            config_data_i => axi_in.tdata,
            config_valid_i => axi_in.tvalid,
            config_ready_o => axi_in.tready,

            -- register map control interface
            regmap_wr_en_o => regmap_wr_en_o,
            regmap_data_o => regmap_data_o,
            regmap_addr_o => regmap_addr_o
        );

    -- saves last register map command
    regmap_mock : process(clk) is
    begin
        if rising_edge(clk) then
            if (rst = '1') then
                regmap_data <= (others => '0');
                regmap_addr <= (others => '0');
                regmap_time      <= 0 ns;
                regmap_time_prev <= 0 ns;
            else
                if ?? regmap_wr_en_o then
                    regmap_data      <= regmap_data_o;
                    regmap_addr      <= regmap_addr_o;
                    regmap_time      <= now;
                    regmap_time_prev <= regmap_time;
                end if;
            end if;
        end if;
    end process;

end architecture RTL;

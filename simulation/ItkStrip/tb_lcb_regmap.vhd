--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.strips_package.all;
    use work.lcb_regmap_package.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

entity tb_lcb_regmap is
    generic(
        use_vunit: boolean := false -- @suppress "Unused generic: use_vunit is not used in work.tb_lcb_regmap(RTL)"
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity tb_lcb_regmap;

architecture RTL of tb_lcb_regmap is
    signal clk      : std_logic;
    signal clk_en   : boolean;
    signal rst      : std_logic;
    signal wr_en    : std_logic := '0';
    signal data_i   : t_register_data := (others => '0');
    signal addr_i   : std_logic_vector(7 downto 0) := (others => '0');
    signal regmap_o : t_register_map;

    constant C_CLK_PERIOD : time    := 25 ns; -- 40 MHz BC clk

begin

    main_test : process is
        -- update a single register on the register map
        procedure write_register (
            name : t_register_name;
            data : t_register_data
        ) is
        begin
            addr_i <= std_logic_vector(to_unsigned(t_register_name'pos(name), addr_i'length));
            data_i <= data;
            wr_en  <= '1';
            wait until rising_edge(clk);
            wr_en  <= '0';
        end;

        -- set a given register bit high
        procedure write_register_bit(
            name : t_register_name;
            bit_id : natural range t_register_data'low to t_register_data'high
        ) is
            variable data : t_register_data;
        begin
            data := (others => '0');
            data(bit_id) := '1';
            write_register(name, data);
        end;

        -- check that the register contains expected value
        procedure check_register(
            name : t_register_name;
            data_expected : t_register_data
        ) is
        begin
            check_value(regmap_o(name), data_expected, ERROR,
            "Register " & to_string(t_register_name'pos(name)) & " contains expected value");
        end;

        -- check that a particular register bit contains expected value
        procedure check_register_bit(
            name : t_register_name;
            bit_id : natural range t_register_data'low to t_register_data'high;
            data_expected : std_logic
        ) is
        begin
            check_value(regmap_o(name)(bit_id), data_expected, ERROR,
            "Register " & to_string(t_register_name'pos(name)) & " bit " & to_string(bit_id)
            & " contains expected value");
        end;

        -- update a single register on the register map and verify its contents
        procedure write_and_check_register(
            name : t_register_name;
            data : t_register_data
        ) is
        begin
            write_register(name, data);
            wait until rising_edge(clk);
            check_register(name, data);
        end;

        -- verify that bit stays high for 1 clk cycle then low
        procedure check_register_pulse_bit (
            name : t_register_name;
            bit_id : natural range t_register_data'low to t_register_data'high
        ) is
        begin
            wait until rising_edge(clk);
            check_register_bit(name, bit_id, '1');
            wait until rising_edge(clk);
            check_register_bit(name, bit_id, '0');
        end;

        -- update a pulsed pit and verify its behavior
        procedure write_and_check_register_pulse_bit (
            name : t_register_name;
            bit_id : natural range t_register_data'low to t_register_data'high
        ) is
        begin
            write_register_bit(name, bit_id);
            check_register_pulse_bit(name, bit_id);
        end;

    begin
        clk_en <= true;
        rst <= '1';
        wait_num_rising_edge(clk, 5);
        rst <= '0';
        wait_num_rising_edge(clk, 5);

        log(ID_LOG_HDR, "Print all defined register map registers");
        for reg in t_register_map'range loop
            log("#define " & to_string(reg) & " "
                & to_hstring(std_logic_vector(
                    to_unsigned(t_register_name'pos(reg), 16))
                ));
        end loop;

        log(ID_LOG_HDR, "Values written to the normal registers propagate to the output");
        write_and_check_register(HCC_MASK, X"ABBA");
        write_and_check_register(TRICKLE_DATA_START, X"CAFE");
        write_and_check_register(TRICKLE_DATA_END, X"FA07");
        write_and_check_register(GATING_BC_START, X"BEEF");
        write_and_check_register(GATING_BC_STOP, X"DEAF");

        log(ID_LOG_HDR, "Written register values are persistent");
        check_register(HCC_MASK, X"ABBA");
        check_register(TRICKLE_DATA_START, X"CAFE");
        check_register(TRICKLE_DATA_END, X"FA07");
        check_register(GATING_BC_START, X"BEEF");
        check_register(GATING_BC_STOP, X"DEAF");

        log(ID_LOG_HDR, "The same registers can be written again");
        write_and_check_register(HCC_MASK, X"CA89");
        write_and_check_register(TRICKLE_DATA_START, X"7F3E");
        write_and_check_register(TRICKLE_DATA_END, X"1010");
        write_and_check_register(GATING_BC_START, X"1EE7");
        write_and_check_register(GATING_BC_STOP, X"8300");

        log(ID_LOG_HDR, "Pulsed bits stay high for one clock cycle and return to low");
        write_and_check_register_pulse_bit(TRICKLE_TRIGGER_PULSE, 0);
        write_and_check_register_pulse_bit(TRICKLE_SET_WRITE_ADDR_PULSE, 0);
        write_register(TRICKLE_TRIGGER_PULSE, X"016F");
        check_register_pulse_bit(TRICKLE_TRIGGER_PULSE, 0);
        check_register(TRICKLE_TRIGGER_PULSE, X"016E");

        -- Ending the simulation
        wait for 100 ns;               -- to allow some time for completion
        report_alert_counters(FINAL);   -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);
        -- Finish the simulation
        uvvm_completed <= '1';
        wait for 1 us;
        clk_en <= false;
        wait;                           -- to stop completely
    end process;

    DUT : entity work.lcb_regmap
        port map(
            clk      => clk,
            rst      => rst,
            wr_en    => wr_en,
            data_i   => data_i,
            addr_i   => addr_i,
            regmap_o => regmap_o
        );

    clock_generator(clk, clk_en, C_CLK_PERIOD, "40 MHz BC clock");

end architecture RTL;

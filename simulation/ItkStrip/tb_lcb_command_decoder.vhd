--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

Library xpm;
    use xpm.vcomponents.all;

    use work.strips_package.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

entity tb_lcb_command_decoder is
    generic(
        use_vunit: boolean := false -- @suppress "Unused generic: use_vunit is not used in work.tb_lcb_command_decoder(RTL)"
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity tb_lcb_command_decoder;

architecture RTL of tb_lcb_command_decoder is
    signal clk           : std_logic;   -- 40 MHz BC clk
    signal clk_en        : boolean := false;
    signal rst           : std_logic := '1';
    -- elink command source
    signal elink_data_i  : std_logic_vector(7 downto 0) := (others => '0'); -- elink data
    signal elink_valid_i : std_logic := '0';   -- elink data is valid this clk cycle
    signal elink_ready_o : std_logic;   -- this module is ready for the next elink data

    -- trickle configuration command source
    signal trickle_data_i  : std_logic_vector(7 downto 0) := (others => '0'); -- trickle configuration data
    signal trickle_valid_i : std_logic := '0'; -- trickle configuration data is valid this clk cycle
    signal trickle_ready_o : std_logic; -- this module is ready for the next elink data

    -- LCB frame generator interface
    signal lcb_cmd_o                    : t_lcb_command; -- which command to execute
    signal lcb_cmd_start_pulse_o        : std_logic; -- pulse for 1 clk cycle to start command
    signal lcb_fast_cmd_data_o          : std_logic_vector(5 downto 0); -- bc & cmd
    signal lcb_l0a_data_o               : std_logic_vector(11 downto 0); -- bcr & L0A mask & L0 tag
    signal lcb_abc_id_o                 : std_logic_vector(3 downto 0); -- ABC* address (0xF = broadcast)
    signal lcb_hcc_id_o                 : std_logic_vector(3 downto 0); -- HCC* address (0xF = broadcast)
    signal lcb_reg_addr_o               : std_logic_vector(7 downto 0); -- register address for reg. read/write
    signal lcb_reg_data_o               : std_logic_vector(31 downto 0); -- register data for reg. write
    signal lcb_ready_i                  : std_logic := '0'; -- LCB encoder ready for a new command
    constant lcb_frame_fifo_almost_full_i : std_logic := '0'; -- LCB encoder FIFO is almost full

    constant C_CLK_PERIOD : time    := 25 ns; -- 40 MHz BC clk
    constant C_CYCLES_RST : integer := 3;
    constant TIMEOUT     : integer := 400;

    -- saved signals when lcb_cmd_start_pulse_o = '1'
    signal lcb_cmd           : t_lcb_command; -- which command to execute
    signal lcb_fast_cmd_data : std_logic_vector(lcb_fast_cmd_data_o'range);
    signal lcb_l0a_data      : std_logic_vector(lcb_l0a_data_o'range);
    signal lcb_abc_id        : std_logic_vector(lcb_abc_id_o'range);
    signal lcb_hcc_id        : std_logic_vector(lcb_hcc_id_o'range);
    signal lcb_reg_addr      : std_logic_vector(lcb_reg_addr_o'range);
    signal lcb_reg_data      : std_logic_vector(lcb_reg_data_o'range);
    signal lcb_time          : time := 0 ns;
    signal lcb_time_prev     : time := 0 ns; -- @suppress "signal lcb_time_prev is never read"

    -- data sources: elink
    signal elink_din          : STD_LOGIC_VECTOR(7 DOWNTO 0) := (others => '0');
    signal elink_wr_en        : STD_LOGIC := '0';
    signal elink_rd_en        : STD_LOGIC := '0';
    signal elink_full         : STD_LOGIC := '0';
    signal elink_almost_full  : STD_LOGIC := '0'; -- @suppress "signal elink_almost_full is never read"
    signal elink_empty        : STD_LOGIC := '1';
    signal elink_valid        : STD_LOGIC := '0';
    signal elink_almost_empty : STD_LOGIC := '1'; -- @suppress "signal elink_almost_empty is never read" -- @suppress "signal elink_almost_empty is never read"
    signal elink_wr_rst_busy  : STD_LOGIC := '0'; -- @suppress "signal elink_wr_rst_busy is never written"
    signal elink_rd_rst_busy  : STD_LOGIC := '0';
    signal elink_en           : boolean := false; -- @suppress "signal elink_en is never read"

    -- data sources: trickle configuration
    signal trickle_din          : STD_LOGIC_VECTOR(7 DOWNTO 0) := (others => '0');
    signal trickle_wr_en        : STD_LOGIC := '0';
    signal trickle_rd_en        : STD_LOGIC := '0';
    signal trickle_full         : STD_LOGIC := '0';
    signal trickle_almost_full  : STD_LOGIC := '0'; -- @suppress "signal trickle_almost_full is never read"
    signal trickle_empty        : STD_LOGIC := '1';
    signal trickle_valid        : STD_LOGIC := '0';
    signal trickle_almost_empty : STD_LOGIC := '1'; -- @suppress "signal trickle_almost_empty is never read"
    constant trickle_wr_rst_busy  : STD_LOGIC := '0';
    signal trickle_rd_rst_busy  : STD_LOGIC := '0';
    signal trickle_en           : boolean := false;

    -- module debug signals
    signal decoder_idle_o               : std_logic; -- @suppress "signal decoder_idle_o is never read"
    signal error_count_o                : std_logic_vector(15 downto 0);

    constant ZERO_ERRORS : std_logic_vector(error_count_o'range) := (others => '0');

    type t_elink_data is array (natural range <>) of std_logic_vector(7 downto 0);
    type t_dst is (ABC, HCC);
--type t_lcb_data_sources is array (natural range <>) of t_lcb_data_source;

begin

    test : process is
        variable random_data_source : t_lcb_data_source := ELINK;

        procedure new_random_data_source is
            variable pos_low, pos_high : integer;
        begin
            pos_low := t_lcb_data_source'pos(t_lcb_data_source'low);
            pos_high := t_lcb_data_source'pos(t_lcb_data_source'high);
            random_data_source := t_lcb_data_source'val(random(pos_low, pos_high));
        end;

        -- push an array of bytes into the selected data source
        procedure enqueue_array(
            data         : t_elink_data;
            signal din   : out STD_LOGIC_VECTOR(7 DOWNTO 0);
            signal wr_en : out STD_LOGIC;
            data_length  : integer := 0
        ) is
        begin
            wr_en <= '1';
            if data_length = 0 then
                for i in data'range loop
                    din <= data(i);
                    wait until rising_edge(clk);
                end loop;
            else
                for i in 0 to data_length-1 loop
                    din <= data(i);
                    wait until rising_edge(clk);
                end loop;
            end if;
            wr_en <= '0';
        end;

        -- Wait for the mock device to receive a pulse (to latch the data)
        procedure wait_for_pulse(
            signal pulse : std_logic;
            wait_enable  : boolean := true
        ) is
        begin
            if wait_enable then
                lcb_ready_i <= '1';
                await_value(pulse, '1', 0 ns, 100 * C_CLK_PERIOD, ERROR,
                    "Wait for the command to be issued (rising edge)");
                await_value(pulse, '0', C_CLK_PERIOD / 2, 1.5 * C_CLK_PERIOD,
                    ERROR, "Wait for the command to be issued (falling edge)");
                lcb_ready_i <= '0';
            end if;
        end;

        -- Enqueue a NOOP
        procedure enqueue_noop(
            data_source : t_lcb_data_source
        ) is
            variable elink_data : t_elink_data(0 to 0);
        begin
            elink_data(0) := ITK_CMD_NOOP;
            case data_source is
                when ELINK =>
                    enqueue_array(elink_data, elink_din, elink_wr_en);
                when TRICKLE =>
                    enqueue_array(elink_data, trickle_din, trickle_wr_en);
                when others =>
                    tb_failure("Unknown data source");
            end case;
        end;

        --- IDLE: enqueue and verify functions

        procedure verify_idle(
            wait_enable : boolean := true
        ) is
        begin
            wait_for_pulse(lcb_cmd_start_pulse_o, wait_enable);
            check_value(t_lcb_command'pos(lcb_cmd), t_lcb_command'pos(LCB_CMD_IDLE),
                ERROR, "Verify LCB command type (LCB_CMD_IDLE)"
            );
        end;

        procedure enqueue_idle(
            data_source : t_lcb_data_source
        ) is
            variable elink_data : t_elink_data(0 to 0);
        begin
            elink_data(0) := ITK_CMD_IDLE;
            case data_source is
                when ELINK =>
                    enqueue_array(elink_data, elink_din, elink_wr_en);
                when TRICKLE =>
                    enqueue_array(elink_data, trickle_din, trickle_wr_en);
                when others =>
                    tb_failure("Unknown data source");
            end case;
        end;

        procedure enqueue_and_verify_idle(
            data_source : t_lcb_data_source
        ) is
        begin
            enqueue_idle(data_source);
            verify_idle;
        end;

        --- Fast command: enqueue and verify functions

        procedure verify_fast(
            data        : std_logic_vector(5 downto 0);
            wait_enable : boolean := true
        ) is
        begin
            wait_for_pulse(lcb_cmd_start_pulse_o, wait_enable);
            check_value(t_lcb_command'pos(lcb_cmd),
                t_lcb_command'pos(LCB_CMD_FAST),
                ERROR, "Verify LCB command type (LCB_CMD_FAST)"
            );
            check_value(lcb_fast_cmd_data, data, ERROR, "Verify fast command data");
        end;

        procedure enqueue_fast(
            data        : std_logic_vector(5 downto 0);
            data_source : t_lcb_data_source
        ) is
            variable elink_data : t_elink_data(0 to 1);
        begin
            elink_data(0) := ITK_CMD_FAST;
            elink_data(1) := "00" & data;
            case data_source is
                when ELINK =>
                    enqueue_array(elink_data, elink_din, elink_wr_en);
                when TRICKLE =>
                    enqueue_array(elink_data, trickle_din, trickle_wr_en);
                when others =>
                    tb_failure("Unknown data source");
            end case;
        end;

        procedure enqueue_and_verify_fast(
            data        : std_logic_vector(5 downto 0);
            data_source : t_lcb_data_source
        ) is
        begin
            enqueue_fast(data, data_source);
            verify_fast(data);
        end;

        --- L0A: enqueue and verify functions

        procedure verify_l0a(
            bcr_mask    : std_logic_vector(4 downto 0);
            tag         : std_logic_vector(6 downto 0);
            wait_enable : boolean := true
        ) is
        begin
            wait_for_pulse(lcb_cmd_start_pulse_o, wait_enable);
            check_value(t_lcb_command'pos(lcb_cmd),
                t_lcb_command'pos(LCB_CMD_L0A),
                ERROR, "Verify LCB command type (LCB_CMD_L0A)"
            );
            check_value(lcb_l0a_data, bcr_mask & tag, ERROR,
                "Verify BCR, mask and tag");
        end;

        procedure enqueue_l0a(
            bcr_mask    : std_logic_vector(4 downto 0);
            tag         : std_logic_vector(6 downto 0);
            data_source : t_lcb_data_source
        ) is
            variable elink_data : t_elink_data(0 to 2);
        begin
            elink_data(0) := ITK_CMD_L0A;
            elink_data(1) := "000" & bcr_mask;
            elink_data(2) := "0" & tag;
            case data_source is
                when ELINK =>
                    enqueue_array(elink_data, elink_din, elink_wr_en);
                when TRICKLE =>
                    enqueue_array(elink_data, trickle_din, trickle_wr_en);
                when others =>
                    tb_failure("Unknown data source");
            end case;
        end;

        procedure enqueue_partial_command
        is
            variable random_command : integer;
            variable elink_data : t_elink_data(0 to 5);
        begin
            -- L0A, Fast, register read (HCC, ABC), register write (HCC, ABC)
            -- 6 total + 2*2 + 2*6 variations
            random_command := random(0, 13);
            case random_command is
                when 0 =>
                    elink_data(0) := ITK_CMD_L0A;
                    enqueue_array(elink_data, elink_din, elink_wr_en, data_length => 1);
                when 1 =>
                    elink_data(0) := ITK_CMD_FAST;
                    enqueue_array(elink_data, elink_din, elink_wr_en, data_length => 1);
                when 2 =>
                    elink_data(0) := ITK_CMD_HCC_REG_READ;
                    enqueue_array(elink_data, elink_din, elink_wr_en, data_length => 1);
                when 3 =>
                    elink_data(0) := ITK_CMD_HCC_REG_WRITE;
                    enqueue_array(elink_data, elink_din, elink_wr_en, data_length => 1);
                when 4 =>
                    elink_data(0) := ITK_CMD_ABC_REG_READ;
                    enqueue_array(elink_data, elink_din, elink_wr_en, data_length => 1);
                when 5 =>
                    elink_data(0) := ITK_CMD_ABC_REG_WRITE;
                    enqueue_array(elink_data, elink_din, elink_wr_en, data_length => 1);
                when 6 =>
                    elink_data(0 to 1) := (ITK_CMD_HCC_REG_READ, random(8));
                    enqueue_array(elink_data, elink_din, elink_wr_en, data_length => 2);
                when 7 =>
                    elink_data(0 to 1) := (ITK_CMD_HCC_REG_WRITE, random(8));
                    enqueue_array(elink_data, elink_din, elink_wr_en, data_length => 2);
                when 8 =>
                    elink_data(0 to 1) := (ITK_CMD_ABC_REG_READ, random(8));
                    enqueue_array(elink_data, elink_din, elink_wr_en, data_length => 2);
                when 9 =>
                    elink_data(0 to 1) := (ITK_CMD_ABC_REG_WRITE, random(8));
                    enqueue_array(elink_data, elink_din, elink_wr_en, data_length => 1);
                when 10 =>
                    elink_data(0 to 3) := (ITK_CMD_HCC_REG_WRITE, random(8), random(8), random(8));
                    enqueue_array(elink_data, elink_din, elink_wr_en, data_length => 4);
                when 11 =>
                    elink_data(0 to 3) := (ITK_CMD_ABC_REG_WRITE, random(8), random(8), random(8));
                    enqueue_array(elink_data, elink_din, elink_wr_en, data_length => 4);
                when 12 =>
                    -- missing part data
                    elink_data(0 to 4) := (ITK_CMD_ABC_REG_WRITE_BLOCK, random(8), random(8), random(8), random(8));
                    enqueue_array(elink_data, elink_din, elink_wr_en, data_length => 5);
                when others =>
                    -- missing part data
                    elink_data(0 to 5) := (ITK_CMD_HCC_REG_WRITE_BLOCK, random(8), random(8), random(8), random(8), random(8));
                    enqueue_array(elink_data, elink_din, elink_wr_en, data_length => 6);
            end case;
        end;

        procedure verify_encoder_timeout is
        begin
            wait_num_rising_edge(clk, TIMEOUT + 10);
            check_stable(lcb_cmd_start_pulse_o, (TIMEOUT + 10) * C_CLK_PERIOD, ERROR, "No command is issued");
        end;

        procedure enqueue_and_verify_l0a(
            bcr_mask    : std_logic_vector(4 downto 0);
            tag         : std_logic_vector(6 downto 0);
            data_source : t_lcb_data_source
        ) is
        begin
            enqueue_l0a(bcr_mask, tag, data_source);
            verify_l0a(bcr_mask, tag);
        end;

        --- Register read: enqueue and verify functions

        procedure verify_reg_read(
            dst         : t_dst;
            addr        : std_logic_vector(7 downto 0);
            hcc_id      : std_logic_vector(3 downto 0);
            abc_id      : std_logic_vector(3 downto 0);
            wait_enable : boolean := true
        ) is
        begin
            wait_for_pulse(lcb_cmd_start_pulse_o, wait_enable);
            case dst is
                when HCC =>
                    check_value(t_lcb_command'pos(lcb_cmd),
                        t_lcb_command'pos(LCB_CMD_HCC_REG_READ),
                        ERROR, "Verify LCB command type (LCB_CMD_HCC_REG_READ)"
                    );
                when ABC =>
                    check_value(t_lcb_command'pos(lcb_cmd),
                        t_lcb_command'pos(LCB_CMD_ABC_REG_READ),
                        ERROR, "Verify LCB command type (LCB_CMD_ABC_REG_READ)"
                    );
                when others =>
                    tb_failure("Unknown destination (ABC/HCC)");
            end case;
            check_value(lcb_abc_id, abc_id, ERROR, "Verify abc_id");
            check_value(lcb_hcc_id, hcc_id, ERROR, "Verify hcc_id");
            check_value(lcb_reg_addr, addr, ERROR, "Verify register address");
        end;

        procedure enqueue_reg_read(
            dst         : t_dst;
            addr        : std_logic_vector(7 downto 0);
            hcc_id      : std_logic_vector(3 downto 0);
            abc_id      : std_logic_vector(3 downto 0);
            data_source : t_lcb_data_source
        ) is
            variable elink_data : t_elink_data(0 to 2);
        begin
            case dst is
                when HCC =>
                    elink_data(0) := ITK_CMD_HCC_REG_READ;
                when ABC =>
                    elink_data(0) := ITK_CMD_ABC_REG_READ;
                when others =>
                    tb_failure("Unknown destination (ABC/HCC)");
            end case;
            elink_data(1) := addr;
            elink_data(2) := hcc_id & abc_id;
            case data_source is
                when ELINK =>
                    enqueue_array(elink_data, elink_din, elink_wr_en);
                when TRICKLE =>
                    enqueue_array(elink_data, trickle_din, trickle_wr_en);
                when others =>
                    tb_failure("Unknown data source");
            end case;
        end;

        procedure enqueue_and_verify_reg_read(
            dst         : t_dst;
            addr        : std_logic_vector(7 downto 0);
            hcc_id      : std_logic_vector(3 downto 0);
            abc_id      : std_logic_vector(3 downto 0);
            data_source : t_lcb_data_source
        ) is
        begin
            enqueue_reg_read(dst, addr, hcc_id, abc_id, data_source);
            verify_reg_read(dst, addr, hcc_id, abc_id);
        end;

        --- Register write: enqueue and verify functions

        procedure verify_reg_write(
            dst         : t_dst;
            addr        : std_logic_vector(7 downto 0);
            data        : std_logic_vector(31 downto 0);
            hcc_id      : std_logic_vector(3 downto 0);
            abc_id      : std_logic_vector(3 downto 0);
            wait_enable : boolean := true
        ) is
        begin
            wait_for_pulse(lcb_cmd_start_pulse_o, wait_enable);
            case dst is
                when HCC =>
                    check_value(t_lcb_command'pos(lcb_cmd),
                        t_lcb_command'pos(LCB_CMD_HCC_REG_WRITE),
                        ERROR, "Verify LCB command type (LCB_CMD_HCC_REG_WRITE)"
                    );
                when ABC =>
                    check_value(t_lcb_command'pos(lcb_cmd),
                        t_lcb_command'pos(LCB_CMD_ABC_REG_WRITE),
                        ERROR, "Verify LCB command type (LCB_CMD_ABC_REG_WRITE)"
                    );
                when others =>
                    tb_failure("Unknown destination (ABC/HCC)");
            end case;
            check_value(lcb_abc_id, abc_id, ERROR, "Verify abc_id");
            check_value(lcb_hcc_id, hcc_id, ERROR, "Verify hcc_id");
            check_value(lcb_reg_addr, addr, ERROR, "Verify register address");
            check_value(lcb_reg_data, data, ERROR, "Verify register data");
        end;

        procedure enqueue_reg_write(
            dst         : t_dst;
            addr        : std_logic_vector(7 downto 0);
            data        : std_logic_vector(31 downto 0);
            hcc_id      : std_logic_vector(3 downto 0);
            abc_id      : std_logic_vector(3 downto 0);
            data_source : t_lcb_data_source;
            special_opcode : std_logic_vector(7 downto 0) := (others => '0')
        ) is
            variable elink_data : t_elink_data(0 to 6);
        begin
            if special_opcode = x"00" then
                case dst is
                    when HCC =>
                        elink_data(0) := ITK_CMD_HCC_REG_WRITE;
                    when ABC =>
                        elink_data(0) := ITK_CMD_ABC_REG_WRITE;
                    when others =>
                        tb_failure("Unknown destination (ABC/HCC)");
                end case;
            else
                elink_data(0) := special_opcode;
            end if;
            elink_data(1) := data(31 downto 24);
            elink_data(2) := data(23 downto 16);
            elink_data(3) := data(15 downto 8);
            elink_data(4) := data(7 downto 0);
            elink_data(5) := addr;
            elink_data(6) := hcc_id & abc_id;
            case data_source is
                when ELINK =>
                    enqueue_array(elink_data, elink_din, elink_wr_en);
                when TRICKLE =>
                    enqueue_array(elink_data, trickle_din, trickle_wr_en);
                when others =>
                    tb_failure("Unknown data source");
            end case;
        end;

        procedure enqueue_and_verify_reg_write(
            dst         : t_dst;
            addr        : std_logic_vector(7 downto 0);
            data        : std_logic_vector(31 downto 0);
            hcc_id      : std_logic_vector(3 downto 0);
            abc_id      : std_logic_vector(3 downto 0);
            data_source : t_lcb_data_source
        ) is
        begin
            enqueue_reg_write(dst, addr, data, hcc_id, abc_id, data_source);
            verify_reg_write(dst, addr, data, hcc_id, abc_id);
        end;

        procedure enqueue_reg_write_block (
            dst         : t_dst;
            addr        : std_logic_vector(7 downto 0);
            data        : std_logic_vector(31 downto 0);
            hcc_id      : std_logic_vector(3 downto 0);
            abc_id      : std_logic_vector(3 downto 0);
            data_source : t_lcb_data_source;
            block_length   : integer range 0 to 255;
            special_opcode : std_logic_vector(7 downto 0) := (others => '0')
        ) is
            variable elink_data : t_elink_data(0 to 3 + 256*4);
            variable v_block_data : std_logic_vector(data'range);
        begin
            if special_opcode = x"00" then
                case dst is
                    when HCC =>
                        elink_data(0) := ITK_CMD_HCC_REG_WRITE_BLOCK;
                    when ABC =>
                        elink_data(0) := ITK_CMD_ABC_REG_WRITE_BLOCK;
                    when others =>
                        tb_failure("Unknown destination (ABC/HCC)");
                end case;
            else
                elink_data(0) := special_opcode;
            end if;
            elink_data(1) := addr;
            elink_data(2) := hcc_id & abc_id;
            elink_data(3) := std_logic_vector(to_unsigned(block_length, 8));
            for i in 0 to block_length loop
                v_block_data := std_logic_vector(unsigned(data) + to_unsigned(i, data'length));
                elink_data(4 + i*4) := v_block_data(31 downto 24);
                elink_data(5 + i*4) := v_block_data(23 downto 16);
                elink_data(6 + i*4) := v_block_data(15 downto 8);
                elink_data(7 + i*4) := v_block_data(7 downto 0);
            end loop;

            case data_source is
                when ELINK =>
                    enqueue_array(elink_data, elink_din, elink_wr_en,
                        data_length => 4 + 4 * (block_length + 1));
                when TRICKLE =>
                    enqueue_array(elink_data, trickle_din, trickle_wr_en,
                        data_length => 4 + 4 * (block_length + 1));
                when others =>
                    tb_failure("Unknown data source");
            end case;
        end;

        procedure enqueue_and_verify_reg_write_block(
            dst         : t_dst;
            addr        : std_logic_vector(7 downto 0);
            data        : std_logic_vector(31 downto 0);
            hcc_id      : std_logic_vector(3 downto 0);
            abc_id      : std_logic_vector(3 downto 0);
            data_source : t_lcb_data_source;
            block_length   : integer range 0 to 255
        ) is
            variable v_addr : std_logic_vector(addr'range);
            variable v_data : std_logic_vector(data'range);
        begin
            enqueue_reg_write_block(dst, addr, data, hcc_id, abc_id, data_source, block_length);
            for i in 0 to block_length loop
                v_addr := std_logic_vector(unsigned(addr) + to_unsigned(i, addr'length));
                v_data := std_logic_vector(unsigned(data) + to_unsigned(i, data'length));
                verify_reg_write(dst, v_addr, v_data, hcc_id, abc_id);
            end loop;
        end;

        -- set RST to a given value and wait C_CYCLES_RST cycles
        procedure set_rst(
            value    : std_logic;
            duration : positive := C_CYCLES_RST
        ) is
        begin
            wait until rising_edge(clk);
            rst <= value;
            wait_num_rising_edge(clk, duration);
        end;

        variable lcb_time_old : time := 0 ns;
        constant invalid_commands : t_elink_data(0 to 3) := (x"23", x"F2", x"E7", x"E8");
    --variable v_random : integer;

    begin
        ---------------------------------------------------
        -- Actual tests begin here
        ---------------------------------------------------
        clk_en <= true;
        disable_log_msg(ID_POS_ACK);

        log(ID_LOG_HDR, "Check behavior when rst = '1'");
        set_rst('1');
        check_value(elink_ready_o, '1', ERROR, "tready=1 when rst=1");
        check_value(trickle_ready_o, '1', ERROR, "Data is flushed out of the trickle configuration controller when rst=1");
        check_value(lcb_cmd_start_pulse_o, '0', ERROR, "No command is issued to LCB encoder (lcb_cmd_start_pulse_o = '0')");


        log(ID_LOG_HDR, "Check that invalid commands increment error_count_o");
        set_rst('0');
        elink_en <= true;
        trickle_en <= true;
        wait_num_rising_edge(clk, 5);
        check_value(error_count_o, ZERO_ERRORS, ERROR, "Initially the error counter is 0");
        enqueue_array(invalid_commands, elink_din, elink_wr_en);
        wait_num_rising_edge(clk, 50);
        check_value(error_count_o, std_logic_vector(to_unsigned(invalid_commands'length, error_count_o'length)),
            ERROR, "Received the expected number of invalid commands");
        set_rst('1');
        wait_num_rising_edge(clk, 5);


        log(ID_LOG_HDR, "Check that individual commands are decoded correctly (from any data source)");
        set_rst('0');
        wait_num_rising_edge(clk, 5);
        for src in t_lcb_data_source loop
            lcb_time_old := lcb_time;
            enqueue_noop(src);
            wait_num_rising_edge(clk, 20);
            check_value(lcb_time, lcb_time_old, ERROR, "NOOP: LCB encoder received no command");
            enqueue_and_verify_idle(src);
            enqueue_and_verify_fast("111111", src);
            enqueue_and_verify_fast("000000", src);
            enqueue_and_verify_l0a("11111", "1111111", src);
            enqueue_and_verify_l0a("00000", "0000000", src);
            for j in 0 to 20 loop
                enqueue_and_verify_fast(random(6), src);
                enqueue_and_verify_l0a(random(5), random(7), src);
                enqueue_and_verify_reg_read(ABC, random(8), random(4), random(4), src);
                enqueue_and_verify_reg_read(HCC, random(8), random(4), random(4), src);
                enqueue_and_verify_reg_write(ABC, random(8), random(32),
                    random(4), random(4), src);
                enqueue_and_verify_reg_write(HCC, random(8), random(32),
                    random(4), random(4), src);
            end loop;
        end loop;
        check_value(error_count_o, ZERO_ERRORS, ERROR, "There were no decoding errors");


        log(ID_LOG_HDR, "Check that individual commands are decoded correctly when data sources switch at random");
        for j in 0 to 20 loop
            new_random_data_source;
            enqueue_noop(random_data_source);
            new_random_data_source;
            enqueue_and_verify_idle(random_data_source);
            new_random_data_source;
            enqueue_and_verify_fast(random(6), random_data_source);
            new_random_data_source;
            enqueue_and_verify_l0a(random(5), random(7), random_data_source);
            new_random_data_source;
            enqueue_and_verify_reg_read(ABC, random(8), random(4), random(4),
                random_data_source);
            new_random_data_source;
            enqueue_and_verify_reg_read(HCC, random(8), random(4), random(4),
                random_data_source);
            new_random_data_source;
            enqueue_and_verify_reg_write(ABC, random(8), random(32),
                random(4), random(4), random_data_source);
            new_random_data_source;
            enqueue_and_verify_reg_write(HCC, random(8), random(32),
                random(4), random(4), random_data_source);
        end loop;
        check_value(error_count_o, ZERO_ERRORS, ERROR, "There were no decoding errors");


        log(ID_LOG_HDR, "Check that ELINK commands are prioritized over TRICKLE commands (1/2)");
        elink_en <= false;
        trickle_en <= false;
        enqueue_fast("011100", TRICKLE);
        enqueue_noop(ELINK);
        enqueue_noop(ELINK);
        enqueue_noop(ELINK);
        enqueue_idle(ELINK);
        wait_num_rising_edge(clk, 5);
        check_value(elink_valid, '1', ERROR, "Elink is ready");
        check_value(trickle_valid, '1', ERROR, "Trickle configuration memory is ready");
        elink_en <= true;
        trickle_en <= true;
        verify_idle;
        verify_fast("011100");
        check_value(error_count_o, ZERO_ERRORS, ERROR, "There were no decoding errors");


        log(ID_LOG_HDR, "Check that ELINK commands are prioritized over TRICKLE commands (2/2)");
        elink_en <= false;
        trickle_en <= false;
        enqueue_fast("000011", TRICKLE);
        enqueue_idle(TRICKLE);
        enqueue_l0a("10110", "1100110", TRICKLE);
        enqueue_reg_read(HCC, x"1A", x"F", x"6", ELINK);
        enqueue_reg_write(ABC, x"03", x"8F921CE4", x"7", x"B", ELINK);
        enqueue_fast("001010", ELINK);
        wait_num_rising_edge(clk, 5);
        check_value(elink_valid, '1', ERROR, "Elink is ready");
        check_value(trickle_valid, '1', ERROR, "Trickle configuration memory is ready");
        elink_en <= true;
        trickle_en <= true;
        -- commands enqueued in ELINK come out first, in order
        verify_reg_read(HCC, x"1A", x"F", x"6");
        verify_reg_write(ABC, x"03", x"8F921CE4", x"7", x"B");
        verify_fast("001010");
        -- commands enqueued in TRICKLE come out second, in order
        verify_fast("000011");
        verify_idle;
        verify_l0a("10110", "1100110");
        check_value(error_count_o, ZERO_ERRORS, ERROR, "There were no decoding errors");


        log(ID_LOG_HDR, "Check that block commands decode correctly (any data source)");
        wait_num_rising_edge(clk, 5);
        for src in t_lcb_data_source loop
            for j in 0 to 20 loop
                enqueue_and_verify_reg_write_block(ABC, random(8), random(32), random(4), random(4), src, random(0,32));
                enqueue_and_verify_reg_write_block(HCC, random(8), random(32), random(4), random(4), src, random(0,32));
            end loop;
        end loop;
        check_value(error_count_o, ZERO_ERRORS, ERROR, "There were no decoding errors");


        log(ID_LOG_HDR, "Verify that the command decoder resets when receiving incomplete command after a timeout");
        set_rst('0');
        wait_num_rising_edge(clk, 5);
        for j in 0 to 200 loop
            enqueue_partial_command;
            verify_encoder_timeout;
        end loop;


        -- Ending the simulation
        wait for 100 ns;                -- to allow some time for completion
        report_alert_counters(FINAL);   -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED");

        -- Finish the simulation
        uvvm_completed <= '1';
        wait for 1 us;
        clk_en <= false;
        wait;                           -- to stop completely
    end process;

    DUT : entity work.lcb_command_decoder
        generic map (TIMEOUT => TIMEOUT)
        port map(
            clk => clk,
            rst => rst,
            -- elink command source
            elink_data_i => elink_data_i,
            elink_valid_i => elink_valid_i,
            elink_ready_o => elink_ready_o,
            -- trickle configuration command source
            trickle_data_i => trickle_data_i,
            trickle_valid_i => trickle_valid_i,
            trickle_ready_o => trickle_ready_o,
            -- LCB frame generator interface
            lcb_cmd_o => lcb_cmd_o,
            lcb_cmd_start_pulse_o => lcb_cmd_start_pulse_o,
            lcb_fast_cmd_data_o => lcb_fast_cmd_data_o,
            lcb_l0a_data_o => lcb_l0a_data_o,
            lcb_abc_id_o => lcb_abc_id_o,
            lcb_hcc_id_o => lcb_hcc_id_o,
            lcb_reg_addr_o => lcb_reg_addr_o,
            lcb_reg_data_o => lcb_reg_data_o,
            lcb_ready_i => lcb_ready_i,
            lcb_frame_fifo_almost_full_i => lcb_frame_fifo_almost_full_i,
            -- debug ports
            decoder_idle_o => decoder_idle_o,
            error_count_o => error_count_o
        );


    -- saves last LCB encoder command
    lcb_encoder_mock : process(clk) is
    begin
        if rising_edge(clk) then
            if (rst = '1') then
                lcb_cmd           <= LCB_CMD_IDLE;
                lcb_fast_cmd_data <= (others => '0');
                lcb_l0a_data      <= (others => '0');
                lcb_abc_id        <= (others => '0');
                lcb_hcc_id        <= (others => '0');
                lcb_reg_addr      <= (others => '0');
                lcb_reg_data      <= (others => '0');
                lcb_time          <= 0 ns;
                lcb_time_prev     <= 0 ns;
            else
                if ?? lcb_cmd_start_pulse_o then
                    lcb_cmd           <= lcb_cmd_o;
                    lcb_fast_cmd_data <= lcb_fast_cmd_data_o;
                    lcb_l0a_data      <= lcb_l0a_data_o;
                    lcb_abc_id        <= lcb_abc_id_o;
                    lcb_hcc_id        <= lcb_hcc_id_o;
                    lcb_reg_addr      <= lcb_reg_addr_o;
                    lcb_reg_data      <= lcb_reg_data_o;
                    lcb_time          <= now;
                    lcb_time_prev     <= lcb_time;
                end if;
            end if;
        end if;
    end process;

    sanity_check : process(clk) is
    begin
        if rising_edge(clk) then
            if (rst = '0') then
                if (?? elink_wr_en) and (?? elink_full) then
                    tb_failure("Attempting to write into a full FIFO (ELINK)");
                end if;
                if (?? trickle_wr_en) and (?? trickle_full) then
                    tb_failure("Attempting to write into a full FIFO (TRICKLE)");
                end if;
                if (?? elink_rd_en) and (?? elink_empty) then
                    tb_failure("Attempting to read from an empty FIFO (ELINK)");
                end if;
                if (?? trickle_rd_en) and (?? trickle_empty) then
                    tb_failure("Attempting to read from an empty FIFO (TRICKLE)");
                end if;
                if (?? elink_wr_en) and (?? elink_wr_rst_busy) then
                    tb_failure("Attempting to write into FIFO with wr_rst_busy = '1' (ELINK)");
                end if;
                if (?? trickle_wr_en) and (?? trickle_wr_rst_busy) then
                    tb_failure("Attempting to write into FIFO with wr_rst_busy = '1' (TRICKLE)");
                end if;
                if (?? elink_rd_en) and (?? elink_rd_rst_busy) then
                    tb_failure("Attempting to read from FIFO with rd_rst_busy = '1' (ELINK)");
                end if;
                if (?? trickle_rd_en) and (?? trickle_rd_rst_busy) then
                    tb_failure("Attempting to read from FIFO with rd_rst_busy = '1' (TRICKLE)");
                end if;
            end if;
        end if;
    end process;

    clock_generator(clk, clk_en, C_CLK_PERIOD, "40 MHz BC clock");


    --elink_fifo : entity work.itk_elink_sim_fifo
    --  port map(
    --    clk => clk,
    --    srst => rst,
    --    din => elink_din,
    --    wr_en => elink_wr_en,
    --    rd_en => elink_rd_en,
    --    dout => elink_data_i,
    --    full => elink_full,
    --    almost_full => elink_almost_full,
    --    empty => elink_empty,
    --    almost_empty => elink_almost_empty,
    --    valid => elink_valid,
    --    wr_rst_busy => elink_wr_rst_busy,
    --    rd_rst_busy => elink_rd_rst_busy
    --  );

    elink_valid_i <= elink_valid when trickle_en else '0';
    elink_rd_en <= '1' when elink_ready_o and elink_valid else '0';

    --trickle_fifo : entity work.itk_elink_sim_fifo
    --  port map(
    --    clk => clk,
    --    srst => rst,
    --    din => trickle_din,
    --    wr_en => trickle_wr_en,
    --    rd_en => trickle_rd_en,
    --    dout => trickle_data_i,
    --    full => trickle_full,
    --    almost_full => trickle_almost_full,
    --    empty => trickle_empty,
    --    almost_empty => trickle_almost_empty,
    --    valid => trickle_valid,
    --    wr_rst_busy => trickle_wr_rst_busy,
    --    rd_rst_busy => trickle_rd_rst_busy
    --  );

    trickle_valid_i <= trickle_valid when trickle_en else '0';
    trickle_rd_en <= '1' when trickle_ready_o and trickle_valid else '0';


    elink_fifo : xpm_fifo_sync
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT"
            DOUT_RESET_VALUE => "0",    -- String
            ECC_MODE => "no_ecc",       -- String
            FIFO_MEMORY_TYPE => "block", -- String
            FIFO_READ_LATENCY => 0,     -- DECIMAL
            FIFO_WRITE_DEPTH => 1024,   -- DECIMAL
            FULL_RESET_VALUE => 0,      -- DECIMAL
            PROG_EMPTY_THRESH => 4,    -- DECIMAL
            PROG_FULL_THRESH => 1023,     -- DECIMAL
            RD_DATA_COUNT_WIDTH => 11,   -- DECIMAL = log2(FIFO_READ_DEPTH)+1.
            -- FIFO_READ_DEPTH =  FIFO_WRITE_DEPTH*WRITE_DATA_WIDTH / READ_DATA_WIDTH
            READ_DATA_WIDTH => 8,      -- DECIMAL
            READ_MODE => "fwft",         -- String
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_ADV_FEATURES => "1808", -- String
            WAKEUP_TIME => 0,           -- DECIMAL
            WRITE_DATA_WIDTH => 8,     -- DECIMAL
            WR_DATA_COUNT_WIDTH => 11    -- DECIMAL = log2(FIFO_WRITE_DEPTH)+1
        )
        port map (
            sleep => '0',
            rst => rst,
            wr_clk => clk,
            wr_en => elink_wr_en,
            din => elink_din,
            full => elink_full,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => elink_rd_rst_busy,
            almost_full => elink_almost_full,
            wr_ack => open,
            rd_en => elink_rd_en,
            dout => elink_data_i,
            empty => elink_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => elink_rd_rst_busy,
            almost_empty => elink_almost_empty,
            data_valid => elink_valid,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );


    trickle_fifo : xpm_fifo_sync
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT"
            DOUT_RESET_VALUE => "0",    -- String
            ECC_MODE => "no_ecc",       -- String
            FIFO_MEMORY_TYPE => "block", -- String
            FIFO_READ_LATENCY => 0,     -- DECIMAL
            FIFO_WRITE_DEPTH => 1024,   -- DECIMAL
            FULL_RESET_VALUE => 0,      -- DECIMAL
            PROG_EMPTY_THRESH => 4,    -- DECIMAL
            PROG_FULL_THRESH => 1023,     -- DECIMAL
            RD_DATA_COUNT_WIDTH => 11,   -- DECIMAL = log2(FIFO_READ_DEPTH)+1.
            -- FIFO_READ_DEPTH =  FIFO_WRITE_DEPTH*WRITE_DATA_WIDTH / READ_DATA_WIDTH
            READ_DATA_WIDTH => 8,      -- DECIMAL
            READ_MODE => "fwft",         -- String
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_ADV_FEATURES => "1808", -- String
            WAKEUP_TIME => 0,           -- DECIMAL
            WRITE_DATA_WIDTH => 8,     -- DECIMAL
            WR_DATA_COUNT_WIDTH => 11    -- DECIMAL = log2(FIFO_WRITE_DEPTH)+1
        )
        port map (
            sleep => '0',
            rst => rst,
            wr_clk => clk,
            wr_en => trickle_wr_en,
            din => trickle_din,
            full => trickle_full,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => trickle_rd_rst_busy,
            almost_full => trickle_almost_full,
            wr_ack => open,
            rd_en => trickle_rd_en,
            dout => trickle_data_i,
            empty => trickle_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => trickle_rd_rst_busy,
            almost_empty => trickle_almost_empty,
            data_valid => trickle_valid,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );

end architecture RTL;

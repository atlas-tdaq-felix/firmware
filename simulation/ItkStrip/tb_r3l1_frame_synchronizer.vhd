--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : ITk Strips package
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : tb_r3l1_frame_synchronizer.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Web May 20 16:32:32 2020
-- Last update : Thu May 21 18:30:50 2020
-- Platform    : Default Part Number
-- Standard    : VHDL-2008
--------------------------------------------------------------------------------
-- Copyright (c) 2020 BNL
-------------------------------------------------------------------------------
-- Description:
-- Set of tests for r3l1_frame_synchronizer
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.MATH_REAL.all;
    use work.strips_package.all;

    use std.env.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

entity tb_r3l1_frame_synchronizer is
    generic(
        use_vunit: boolean := false -- @suppress "Unused generic: use_vunit is not used in work.tb_r3l1_frame_synchronizer(RTL)"
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity tb_r3l1_frame_synchronizer;

architecture RTL of tb_r3l1_frame_synchronizer is
    constant C_CLK_PERIOD_40               : time    := 25 ns;
    --constant C_CYCLES_RST                  : integer := 3;
    --constant C_STABLE_PERIODS              : integer := 10;
    constant C_MAX_FIRST_FRAME_PULSE_DELAY : integer := 6;
    constant C_MAX_FRAME_PULSE_DELAY       : integer := 4;
    --constant C_ONE_FRAME_DELAY             : time    := C_CLK_PERIOD_40 * 4;

    signal clk                 : std_logic; -- BC clock
    signal clk_en              : boolean                      := false;
    signal bcr_i               : std_logic                    := '0'; -- BCR signal
    signal frame_phase_i       : unsigned(frame_phase'range)  := (others => '0'); -- determines the phase of the L0A frame with respect to the BCR pulse
    signal frame_start_pulse_o : std_logic; -- strobe to start L0A frame

begin

    TEST : process is

        -- pulse bcr for a single clock cycle
        procedure pulse_bcr is
        begin
            bcr_i <= '1';
            wait until rising_edge(clk);
            bcr_i <= '0';
            wait until rising_edge(clk);
        end;

        -- verifiy that the frame_start_pulse_o provides pulses with the correct period for the given number of repetitions
        procedure verify_frame_pulse_period(
            repetitions    : positive := 1;
            wait_for_pulse : boolean  := true
        ) is
        begin
            if wait_for_pulse then
                await_value(frame_start_pulse_o, '1', 0 ns, C_MAX_FRAME_PULSE_DELAY * C_CLK_PERIOD_40, ERROR,
                            "frame_start_pulse has to arrive within " & to_string(C_MAX_FRAME_PULSE_DELAY) & " clk cycles");
            end if;
            for i in 0 to repetitions - 1 loop
                for j in 1 to 3 loop
                    wait until rising_edge(clk);
                    check_value(frame_start_pulse_o, '0', ERROR,
                        "frame_start_pulse_o = '0' between the pulses (j = " & to_string(j) & ")");
                end loop;
                wait until rising_edge(clk);
                check_value(frame_start_pulse_o, '1', ERROR, "frame_start_pulse_o = '1' when the next pulse begins");
            end loop;
        end;

        -- verifies that the frame phase is locked to the BCR pulse
        procedure verify_frame_pulse_phase is
        begin
            for i in 0 to 3 loop
                -- frame_start_pulse is supposed to occur frame_phase_i + 1 clk cycle after the BCR
                frame_phase_i <= to_unsigned(i, frame_phase_i'length);
                pulse_bcr;
                wait_num_rising_edge(clk, i+1);
                check_value(frame_start_pulse_o, '1', ERROR, "Pulse starts with the correct phase: frame_phase_i = "
                    & to_string(frame_phase_i));
                log(ID_SEQUENCER, "Verifying that frame_start_pulse_o has the correct period (phase = "
                    & to_string(frame_phase_i) & ")");
                verify_frame_pulse_period(repetitions => 10, wait_for_pulse => false);
            end loop;
        end;

    begin
        disable_log_msg(ID_POS_ACK);
        clk_en <= true;
        wait_num_rising_edge(clk, 2);

        --------------------   Tests begin here -------------------------------

        log(ID_LOG_HDR, "Verifying that the frame start pulses are generated", C_SCOPE);
        await_value(frame_start_pulse_o, '1', 0 ns, C_MAX_FIRST_FRAME_PULSE_DELAY * C_CLK_PERIOD_40, ERROR,
                    "Initial frame_start_pulse has to arrive within " & to_string(C_MAX_FIRST_FRAME_PULSE_DELAY) & " clk cycles");
        log(ID_SEQUENCER, "Verifying that the frame start pulses arrive with the correct period", C_SCOPE);
        verify_frame_pulse_period(10);

        -----------------------------------------------------------------------

        log(ID_LOG_HDR, "Verifying that the frame start pulses synchronize to the BCR with the correct phase", C_SCOPE);
        log(ID_SEQUENCER, "Synchronization works when BCR pulse arrives", C_SCOPE);
        verify_frame_pulse_phase;

        -----------------------------------------------------------------------

        log(ID_LOG_HDR, "SIMULATION COMPLETED");
        report_alert_counters(FINAL);
        uvvm_completed <= '1';
        wait for 1 us;
        clk_en <= false;
        wait;
    end process;

    clock_generator(clk, clk_en, C_CLK_PERIOD_40, "40 MHz BC clock");

    dut : entity work.r3l1_frame_synchronizer
        port map(
            clk                 => clk,
            bcr_i               => bcr_i,
            frame_phase_i       => frame_phase_i,
            frame_start_pulse_o => frame_start_pulse_o
        );


end architecture RTL;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : Testbench simulation for interaction between
--          bypass_aggregator and lcb_scheduler_encoder modules
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : tb_bypass_scheduler_continuous_write.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Tru April 23 20:06:40 2020
-- Last update : Fri Jul  2 19:05:08 2021
-- Platform    : Default Part Number
-- Standard    : <VHDL-2008>
--------------------------------------------------------------------------------
-- Copyright (c) 2019 User Company Name
-------------------------------------------------------------------------------
-- Description:
--
-- Verify that the frames coming from elink through bypass_aggregator and scheduler
-- are scheduled continuously (without IDLE frames inserted)
--
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

library uvvm_vvc_framework;
    use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

library bitvis_vip_axistream;
    context bitvis_vip_axistream.vvc_context;

library bitvis_vip_clock_generator;
    context bitvis_vip_clock_generator.vvc_context;

entity tb_bypass_frame_vvc is
    generic(
        use_vunit: boolean := false -- @suppress "Unused generic: use_vunit is not used in work.tb_bypass_frame_vvc(RTL)"
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity tb_bypass_frame_vvc;

architecture RTL of tb_bypass_frame_vvc is
    constant C_CLK_PERIOD          : time    := 25 ns;
    constant C_CYCLES_RST          : integer := 3;
    --constant C_STABLE_PERIODS      : integer := 10;
    constant C_MAX_CYCLES_PER_BYTE : integer := 4;
    constant C_USER_WIDTH          : integer := 1;
    constant C_ID_WIDTH            : integer := 1;
    constant C_DEST_WIDTH          : integer := 1;
    constant C_DATA_WIDTH          : integer := 8;
    constant C_FRAME_WIDTH         : integer := 16;

    --constant zero_byte_percent       : integer := 20;
    --constant zero_byte_max_count     : integer := 7;
    constant delay_max_clk_cycles    : integer := 5;
    constant delay_occurence_percent : integer := 10;
    constant timeout_clk_cycles    : integer := delay_max_clk_cycles + 20;

    signal clk       : std_logic;       -- BC clock
    signal rst       : std_logic := '0';
    signal frame_dut : std_logic_vector(15 downto 0);

    signal byte_in : t_axistream_if(tdata(C_DATA_WIDTH - 1 downto 0),
                                    tkeep((C_DATA_WIDTH / 8) - 1 downto 0),
                                    tuser(C_USER_WIDTH - 1 downto 0),
                                    tstrb((C_DATA_WIDTH / 8) - 1 downto 0),
                                    tid(C_ID_WIDTH - 1 downto 0),
                                    tdest(C_DEST_WIDTH - 1 downto 0)
                                   );

    signal frame_out : t_axistream_if(tdata(C_FRAME_WIDTH - 1 downto 0),
                                      tkeep((C_FRAME_WIDTH / 8) - 1 downto 0),
                                      tuser(C_USER_WIDTH - 1 downto 0),
                                      tstrb((C_FRAME_WIDTH / 8) - 1 downto 0),
                                      tid(C_ID_WIDTH - 1 downto 0),
                                      tdest(C_DEST_WIDTH - 1 downto 0)
                                     );

begin
    -----------------------------------------------------------------------------
    -- Instantiate DUT
    -----------------------------------------------------------------------------
    dut : entity work.strips_bypass_frame_aggregator
        generic map ( TIMEOUT  => timeout_clk_cycles)
        port map(
            clk           => clk,
            rst           => rst,
            byte_i        => byte_in.tdata,
            byte_valid_i  => byte_in.tvalid,
            byte_ready_o  => byte_in.tready,
            frame_o       => frame_dut,
            frame_valid_o => frame_out.tvalid,
            frame_ready_i => frame_out.tready
        );

    -- vvc and dut have opposite endianness
    frame_out.tdata <= frame_dut(7 downto 0) & frame_dut(15 downto 8);
    frame_out.tkeep <= (others => '1');
    frame_out.tstrb <= (others => '1');
    frame_out.tid   <= (others => '1');
    frame_out.tdest <= (others => '0');
    frame_out.tuser <= (others => '1');
    frame_out.tlast <= '1';
    -----------------------------------------------------------------------------
    -- Instantiate UVVM entities
    -----------------------------------------------------------------------------
    i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

    i_clock_generator_vvc : entity bitvis_vip_clock_generator.clock_generator_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_INSTANCE_IDX    => 1,
            GC_CLOCK_NAME      => "Clock",
            GC_CLOCK_PERIOD    => C_CLK_PERIOD,
            GC_CLOCK_HIGH_TIME => C_CLK_PERIOD / 2
        )
        port map(
            clk => clk
        );

    i_axistream_vvc_master : entity bitvis_vip_axistream.axistream_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_AXISTREAM_BFM_CONFIG, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => true,
            GC_DATA_WIDTH    => C_DATA_WIDTH,
            GC_USER_WIDTH    => C_USER_WIDTH,
            GC_ID_WIDTH      => C_ID_WIDTH,
            GC_DEST_WIDTH    => C_DEST_WIDTH,
            GC_INSTANCE_IDX  => 2)
        port map(
            clk              => clk,
            axistream_vvc_if => byte_in);

    i_axistream_vvc_slave : entity bitvis_vip_axistream.axistream_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_AXISTREAM_BFM_CONFIG, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => false,
            GC_DATA_WIDTH    => C_FRAME_WIDTH,
            GC_USER_WIDTH    => C_USER_WIDTH,
            GC_ID_WIDTH      => C_ID_WIDTH,
            GC_DEST_WIDTH    => C_DEST_WIDTH,
            GC_INSTANCE_IDX  => 3)
        port map(
            clk              => clk,
            axistream_vvc_if => frame_out);

    -----------------------------------------------------------------------------
    -- Test begins here
    -----------------------------------------------------------------------------

    p_main : process is
        variable random_int     : integer := 0;
        variable event_happens  : boolean;
        variable expected_frame : std_logic_vector(15 downto 0);

        -- sets event_happens = true with the probability given by occurence_percent
        procedure roll_for_event(
            occurence_percent : integer
        ) is
        begin
            random_int    := random(0, 99);
            event_happens := (random_int < occurence_percent);
        end;

        -- inserts a randomized time delay
        procedure insert_random_delay is
        begin
            roll_for_event(delay_occurence_percent);
            if event_happens then
                random_int := random(1, delay_max_clk_cycles);
                wait_num_rising_edge(clk, random_int);
            end if;
        end;

        -- send random data
        procedure send_random_data is
        begin
            expected_frame(15 downto 8) := std_logic_vector(to_unsigned(random(0, 255), 8));
            expected_frame(7 downto 0)  := std_logic_vector(to_unsigned(random(0, 255), 8));
            axistream_transmit(AXISTREAM_VVCT, 2, expected_frame(15 downto 8), "Inserting first random byte");--@suppress
            insert_random_delay;
            axistream_transmit(AXISTREAM_VVCT, 2, expected_frame(7 downto 0), "Inserting second random byte");--@suppress
            insert_random_delay;
        end;

    begin
        await_uvvm_initialization(VOID);
        start_clock(CLOCK_GENERATOR_VVCT, 1, "Start clock generator");--@suppress

        -- Print the configuration to the log
        --report_global_ctrl(VOID);
        --report_msg_id_panel(VOID);

        disable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        enable_log_msg(ID_SEQUENCER);
        --enable_log_msg(ID_UVVM_SEND_CMD);

        disable_log_msg(CLOCK_GENERATOR_VVCT, 1, ALL_MESSAGES); --@suppress
        enable_log_msg(CLOCK_GENERATOR_VVCT, 1, ID_BFM);--@suppress
        disable_log_msg(AXISTREAM_VVCT, 2, ALL_MESSAGES);--@suppress
        --enable_log_msg(AXISTREAM_VVCT, 2, ID_BFM);
        disable_log_msg(AXISTREAM_VVCT, 3, ALL_MESSAGES);--@suppress
        --enable_log_msg(AXISTREAM_VVCT, 3, ID_BFM);

        gen_pulse(rst, C_CYCLES_RST * C_CLK_PERIOD, "Reset DUT");

        log(ID_LOG_HDR, "Send some data to the DUT and verify the frame (1/2)", C_SCOPE);
        for i in 0 to 200 loop
            send_random_data;
            axistream_expect(AXISTREAM_VVCT, 3, expected_frame,--@suppress
                "The frame has expected data", ERROR);
        end loop;

        log(ID_LOG_HDR, "Verify that the zero byte frames are correctly passed", C_SCOPE);
        for i in 0 to 30 loop
            expected_frame := x"0000";
            axistream_transmit(AXISTREAM_VVCT, 2, expected_frame, "Inserting zero frame");--@suppress
            axistream_expect(AXISTREAM_VVCT, 3, expected_frame, "Zero frame is received", ERROR);--@suppress
        end loop;

        await_completion(AXISTREAM_VVCT, 2, C_MAX_CYCLES_PER_BYTE * C_CLK_PERIOD * 1000,--@suppress
                "Wait until the DUT is done with the previous requests");

        log(ID_LOG_HDR, "Check that unpaired bytes are dropped correctly due to the timeout", C_SCOPE);
        for i in 0 to 100 loop
            axistream_transmit(AXISTREAM_VVCT, 2, std_logic_vector(to_unsigned(random(1, 255), 8)),--@suppress
                "Inserting random byte");
            await_completion(AXISTREAM_VVCT, 2, C_MAX_CYCLES_PER_BYTE * C_CLK_PERIOD * 4,--@suppress
                "Wait until the DUT indicates ready");
            random_int := random(timeout_clk_cycles + 1, timeout_clk_cycles*2);
            wait_num_rising_edge(clk, random_int);
        end loop;

        log(ID_LOG_HDR, "Send some data to the DUT and verify the frame (2/2)", C_SCOPE);
        for i in 0 to 200 loop
            send_random_data;
            axistream_expect(AXISTREAM_VVCT, 3, expected_frame,--@suppress
                "The frame has expected data", ERROR);
        end loop;

        wait for 1000 ns;                -- to allow some time for completion
        report_alert_counters(FINAL);   -- Report final counters and print conclusion for simulation (Success/Fail)

        uvvm_completed <= '1';
        wait for 1 us;
        stop_clock(CLOCK_GENERATOR_VVCT, 1, "Stop clock generator");--@suppress
        wait;
    end process;

end architecture RTL;

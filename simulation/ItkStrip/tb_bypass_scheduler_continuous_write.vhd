--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : Testbench simulation for interaction between
--          bypass_aggregator and lcb_scheduler_encoder modules
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : tb_bypass_scheduler_continuous_write.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Fri April 24 10:06:40 2020
-- Last update : Wed Sep 30 15:50:30 2020
-- Platform    : Default Part Number
-- Standard    : <VHDL-2008>
--------------------------------------------------------------------------------
-- Copyright (c) 2019 User Company Name
-------------------------------------------------------------------------------
-- Description:
--
-- Verify that the frames coming from elink through bypass_aggregator and scheduler
-- are scheduled continuously (without IDLE frames inserted)
--
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.strips_package.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

library uvvm_vvc_framework;
    use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

library bitvis_vip_axistream;
    context bitvis_vip_axistream.vvc_context;

library bitvis_vip_clock_generator;
    context bitvis_vip_clock_generator.vvc_context;

entity tb_bypass_scheduler_continuous_write is
    generic(
        use_vunit: boolean := false -- @suppress "Unused generic: use_vunit is not used in work.tb_bypass_scheduler_continuous_write(RTL)"
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity tb_bypass_scheduler_continuous_write;

architecture RTL of tb_bypass_scheduler_continuous_write is
    --constant K0         : std_logic_vector(7 downto 0)  := x"78";
    --constant K1         : std_logic_vector(7 downto 0)  := x"55";
    --constant idle_frame : std_logic_vector(15 downto 0) := K0 & K1;

    constant C_CLK_PERIOD          : time    := 25 ns; -- 40 MHz BC clk
    constant C_CYCLES_RST          : integer := 3;
    --constant C_STABLE_PERIODS      : integer := 10;
    --constant C_MAX_CYCLES_PER_BYTE : integer := 4;
    constant C_USER_WIDTH          : integer := 1;
    constant C_ID_WIDTH            : integer := 1;
    constant C_DEST_WIDTH          : integer := 1;
    constant C_DATA_WIDTH          : integer := 8;
    --constant C_FRAME_WIDTH         : integer := 16;

    signal clk : std_logic := '0';

    signal rst                          : std_logic                     := '0';
    --signal lcb_frame_wr_en              : std_logic                     := '0';
    signal lcb_frame_rd_en              : std_logic                     := '0'; -- @suppress "signal lcb_frame_rd_en is never read"
    signal frame_start_pulse_i          : std_logic                     := '0';
    signal trickle_bc_gating_i          : std_logic                     := '0';
    signal trickle_allow_register_cmd_i : std_logic                     := '0';
    signal trickle_bc_gating_en         : std_logic                     := '0';
    signal ttc_l0a_en                   : std_logic                     := '0';
    constant l0a_frame                    : std_logic_vector(11 downto 0) := (others => '0');
    signal lcb_frame                    : std_logic_vector(12 downto 0);
    --signal lcb_frame_din                : std_logic_vector(12 downto 0) := (others => '0');
    signal lcb_frame_full               : std_logic; -- @suppress "signal lcb_frame_full is never read"
    signal lcb_frame_almost_full        : std_logic; -- @suppress "signal lcb_frame_almost_full is never read"
    signal lcb_frame_wr_rst_busy        : std_logic; -- @suppress "signal lcb_frame_wr_rst_busy is never read"
    signal lcb_frame_empty              : std_logic;
    signal lcb_frame_almost_empty       : std_logic;
    signal lcb_frame_rd_rst_busy        : std_logic; -- @suppress "signal lcb_frame_rd_rst_busy is never read"
    signal encoded_frame                : std_logic_vector(15 downto 0);

    --signal lcb_wr_ctr, lcb_rd_ctr : integer                       := 0;
    signal bypass_frame           : std_logic_vector(15 downto 0) := (others => '0'); -- bypass FromHost frame (already 6b8b encoded)
    signal bypass_frame_valid     : std_logic                     := '0'; -- bypass frame is valid this clk cycle
    signal bypass_frame_ready     : std_logic; -- request next bypass frame

    constant frames_count : integer := 127;
    signal frames_checked : integer := 0;

    signal byte_in : t_axistream_if(tdata(C_DATA_WIDTH - 1 downto 0),--@suppress
                                    tkeep((C_DATA_WIDTH / 8) - 1 downto 0),
                                    tuser(C_USER_WIDTH - 1 downto 0),
                                    tstrb((C_DATA_WIDTH / 8) - 1 downto 0),
                                    tid(C_ID_WIDTH - 1 downto 0),
                                    tdest(C_DEST_WIDTH - 1 downto 0)
                                   );

begin
    -----------------------------------------------------------------------------
    -- Instantiate DUTs
    -----------------------------------------------------------------------------
    i_aggregator : entity work.strips_bypass_frame_aggregator
        generic map(
            TIMEOUT => 4000
        )
        port map(
            clk           => clk,
            rst           => rst,
            byte_i        => byte_in.tdata,--@suppress
            byte_valid_i  => byte_in.tvalid,--@suppress
            byte_ready_o  => byte_in.tready,--@suppress
            frame_o       => bypass_frame,
            frame_valid_o => bypass_frame_valid,
            frame_ready_i => bypass_frame_ready
        );

    i_scheduler : entity work.lcb_scheduler_encoder
        port map(
            clk => clk,
            rst => rst,
            l0a_frame_i => l0a_frame,
            lcb_frame_i => lcb_frame,
            lcb_frame_o_rd_en => lcb_frame_rd_en,
            lcb_frame_i_empty => lcb_frame_empty,
            lcb_frame_i_almost_empty => lcb_frame_almost_empty,
            l0a_frame_delay_i => "00",
            bypass_frame_i => bypass_frame,
            bypass_frame_valid_i => bypass_frame_valid,
            bypass_frame_ready_o => bypass_frame_ready,
            frame_start_pulse_i => frame_start_pulse_i,
            trickle_bc_gating_i => trickle_bc_gating_i,
            trickle_allow_register_cmd_i => trickle_allow_register_cmd_i,
            trickle_bc_gating_en => trickle_bc_gating_en,
            ttc_l0a_en => ttc_l0a_en,
            encoded_frame_o => encoded_frame
        );

    lcb_frame                    <= (others => '0');
    lcb_frame_empty              <= '1';
    lcb_frame_almost_empty       <= '1';
    lcb_frame_full               <= '0';
    lcb_frame_almost_full        <= '0';
    lcb_frame_wr_rst_busy        <= '0';
    lcb_frame_rd_rst_busy        <= '0';
    trickle_bc_gating_i          <= '0';
    trickle_allow_register_cmd_i <= '0';
    trickle_bc_gating_en         <= '0';
    ttc_l0a_en                   <= '0';

    -----------------------------------------------------------------------------
    -- Instantiate UVVM entities
    -----------------------------------------------------------------------------
    i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

    i_clock_generator_vvc : entity bitvis_vip_clock_generator.clock_generator_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_INSTANCE_IDX    => 1,
            GC_CLOCK_NAME      => "Clock",
            GC_CLOCK_PERIOD    => C_CLK_PERIOD,
            GC_CLOCK_HIGH_TIME => C_CLK_PERIOD / 2
        )
        port map(
            clk => clk
        );

    i_axistream_vvc_master : entity bitvis_vip_axistream.axistream_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_AXISTREAM_BFM_CONFIG, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => true,
            GC_DATA_WIDTH    => C_DATA_WIDTH,
            GC_USER_WIDTH    => C_USER_WIDTH,
            GC_ID_WIDTH      => C_ID_WIDTH,
            GC_DEST_WIDTH    => C_DEST_WIDTH,
            GC_INSTANCE_IDX  => 2)
        port map(
            clk              => clk,
            axistream_vvc_if => byte_in);--@suppress

    -----------------------------------------------------------------------------
    -- Test begins here
    -----------------------------------------------------------------------------
    p_main : process is
        variable byte_to_send : std_logic_vector(7 downto 0);
    begin
        await_uvvm_initialization(VOID);
        start_clock(CLOCK_GENERATOR_VVCT, 1, "Start clock generator");--@suppress

        -- Print the configuration to the log
        --report_global_ctrl(VOID);
        --report_msg_id_panel(VOID);

        disable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        enable_log_msg(ID_SEQUENCER);
        --enable_log_msg(ID_POS_ACK);
        --enable_log_msg(ID_UVVM_SEND_CMD);

        disable_log_msg(CLOCK_GENERATOR_VVCT, 1, ALL_MESSAGES);--@suppress
        enable_log_msg(CLOCK_GENERATOR_VVCT, 1, ID_BFM);--@suppress
        disable_log_msg(AXISTREAM_VVCT, 2, ALL_MESSAGES);--@suppress
        enable_log_msg(AXISTREAM_VVCT, 2, ID_BFM);--@suppress

        gen_pulse(rst, C_CYCLES_RST * C_CLK_PERIOD, "Reset DUT");

        log(ID_LOG_HDR, "Send incrementing data to the DUT", C_SCOPE);
        for i in 1 to frames_count * 2 loop
            byte_to_send := std_logic_vector(to_unsigned(i, 8));
            axistream_transmit(AXISTREAM_VVCT, 2, byte_to_send, "Send non-zero byte (i=" & to_string(i) & ")");--@suppress
        end loop;

        await_value(frames_checked, frames_count, C_CLK_PERIOD,
                    C_CLK_PERIOD * frames_count * 8, ERROR, "Every scheduled frame has been checked"
                   );
        log(ID_SEQUENCER, "Finished verifying continuous bypass frame streaming");

        wait for 1000 ns;               -- to allow some time for completion
        report_alert_counters(FINAL);   -- Report final counters and print conclusion for simulation (Success/Fail)

        uvvm_completed <= '1';
        wait for 1 us;
        stop_clock(CLOCK_GENERATOR_VVCT, 1, "Stop clock generator");--@suppress
        wait;
    end process;

    -- generate frame_start_pulse_i signal
    frame_start_pulse_generator : process
    begin
        frame_start_pulse_i <= '1';
        wait until rising_edge(clk);
        frame_start_pulse_i <= '0';
        wait_num_rising_edge(clk, 3);
    end process;

    -- scheduled frame checker
    data_checker : process is
        variable expected_frame : std_logic_vector(15 downto 0) := (others => '0');
    begin
        if rst = '0' then
            frames_checked <= 0;
            wait_num_rising_edge(frame_start_pulse_i, 3);
            for i in 0 to frames_count - 1 loop
                wait until rising_edge(frame_start_pulse_i);
                expected_frame(15 downto 8) := std_logic_vector(to_unsigned(2 * i + 1, 8));
                expected_frame(7 downto 0)  := std_logic_vector(to_unsigned(2 * i + 2, 8));
                wait_num_rising_edge(clk, 2);
                check_value(encoded_frame, expected_frame, ERROR, "Scheduled frame matches expected value");
                log(ID_SEQUENCER, "Verified bypass frame " & to_string(i + 1) & " out of " & to_string(frames_count));
                frames_checked              <= frames_checked + 1;
            end loop;
            wait;                       -- stop the checker
        end if;
        wait until rising_edge(clk);
    end process;

end architecture RTL;

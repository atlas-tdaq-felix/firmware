--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : Testbench simulation for r3l1_scheduler_encoder
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : tb_r3l1_scheduler_encoder.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Tue May 19 10:06:40 2020
-- Last update : Thu Aug 12 18:25:47 2021
-- Platform    : Default Part Number
-- Standard    : <VHDL-2008>
--------------------------------------------------------------------------------
-- Copyright (c) 2019 User Company Name
-------------------------------------------------------------------------------
-- Description:
-- 1. Reset, verify that IDLE frames dequeue
-- 2. Enqueue nothing, verify that IDLE frames come out
-- 3. Verify that L1 frames come out when scheduled
-- 4. Verify that R3 frames come out when scheduled
-- 5. Verify that bypass frames come out when scheduled
-- 6. Verify that R3 frames are prioritized over L1 frames
-- 7. Verify that R3 frames are prioritized over bypass frames
-- 8. Verify that L1 frames are prioritized over bypass frames
-- 9. Check that all possible R3 frames are encoded correctly (decode to the original)
-- 10. Check that all possible L1 frames are encoded correctly (decode to the original)
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use work.strips_package.all;

Library xpm;
    use xpm.vcomponents.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

entity tb_r3l1_scheduler_encoder is
    generic(
        use_vunit: boolean := false -- @suppress "Unused generic: use_vunit is not used in work.tb_r3l1_scheduler_encoder(behavioral)"
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity tb_r3l1_scheduler_encoder;

architecture behavioral of tb_r3l1_scheduler_encoder is
    constant K0         : std_logic_vector(7 downto 0)  := x"78";
    constant K1         : std_logic_vector(7 downto 0)  := x"55";
    --constant K2         : std_logic_vector(7 downto 0)  := x"47";
    --constant K3         : std_logic_vector(7 downto 0)  := x"6A";
    constant idle_frame : std_logic_vector(15 downto 0) := K0 & K1;

    constant C_CLK_PERIOD : time                          := 25 ns; -- 40 MHz BC clk
    constant C_CYCLES_RST : integer                       := 3;
    signal clk            : std_logic                     := '0';
    signal clk_en         : boolean                       := false;
    --constant null_l0a     : std_logic_vector(11 downto 0) := "000000000000";
    --constant idle_cmd     : std_logic_vector(12 downto 0) := "1000000000000";
    --constant bypass_cmd   : std_logic_vector(12 downto 0) := "1000000000001";

    signal rst                 : std_logic := '0';
    signal frame_start_pulse_i : std_logic := '0';

    -- R3 frame FIFO signals
    signal r3_frame                 : std_logic_vector(11 downto 0);
    signal r3_frame_wr_en           : std_logic                     := '0';
    signal r3_frame_rd_en           : std_logic                     := '0';
    signal r3_frame_din             : std_logic_vector(11 downto 0) := (others => '0');
    signal r3_frame_full            : std_logic;
    signal r3_frame_wr_rst_busy     : std_logic;
    signal r3_frame_empty           : std_logic;
    signal r3_frame_almost_empty    : std_logic;
    signal r3_frame_rd_rst_busy     : std_logic;
    signal r3_rd_ready, r3_wr_ready : std_logic;
    signal r3_wr_ctr, r3_rd_ctr     : integer                       := 0;

    -- L1 frame FIFO signals
    signal l1_frame                 : std_logic_vector(11 downto 0);
    signal l1_frame_wr_en           : std_logic                     := '0';
    signal l1_frame_rd_en           : std_logic                     := '0';
    signal l1_frame_din             : std_logic_vector(11 downto 0) := (others => '0');
    signal l1_frame_full            : std_logic;
    signal l1_frame_wr_rst_busy     : std_logic;
    signal l1_frame_empty           : std_logic;
    signal l1_frame_almost_empty    : std_logic;
    signal l1_frame_rd_rst_busy     : std_logic;
    signal l1_rd_ready, l1_wr_ready : std_logic;
    signal l1_wr_ctr, l1_rd_ctr     : integer                       := 0;

    -- Bypass frame signals
    signal bypass_frame       : std_logic_vector(15 downto 0) := (others => '0'); -- bypass FromHost frame (already 6b8b encoded)
    signal bypass_frame_valid : std_logic                     := '0'; -- bypass frame is valid this clk cycle
    signal bypass_frame_ready : std_logic; -- request next bypass frame

    -- scheduler output frame
    signal encoded_frame : std_logic_vector(15 downto 0);
begin

    test : process is
        --------------------------------------------------------------------------------
        -- Some macro declarations; keep scrolling to the actual tests
        --------------------------------------------------------------------------------

        --variable delay : time := 0 ns;

        -- places an unencoded L1 frame into the LCB FIFO
        procedure enqueue_l1_frame(
            constant data : std_logic_vector(6 downto 0)
        ) is
        begin
            await_value(l1_wr_ready, '1', 0 ns, 10 * C_CLK_PERIOD,
                        ERROR, "L1 FIFO is ready (wr side)");
            l1_frame_wr_en <= '1';
            l1_frame_din   <= "00000" & data;
            wait until rising_edge(clk);
            l1_frame_wr_en <= '0';
        end;

        -- places an unencoded R3 frame into the LCB FIFO
        procedure enqueue_r3_frame(
            constant data : std_logic_vector(11 downto 0)
        ) is
        begin
            await_value(r3_wr_ready, '1', 0 ns, 10 * C_CLK_PERIOD,
                        ERROR, "R3 FIFO is ready (wr side)");
            r3_frame_wr_en <= '1';
            r3_frame_din   <= data;
            wait until rising_edge(clk);
            r3_frame_wr_en <= '0';
        end;

        -- dequeue a scheduler frame without verifying it
        procedure dequeue_frame is -- @suppress "Unused declaration"
        begin
            frame_start_pulse_i <= '1';
            wait until rising_edge(clk);
            frame_start_pulse_i <= '0';
            wait until rising_edge(clk);
            wait until rising_edge(clk);
        end;

        -- appends words "received idle" to message if encoded frame is IDLE
        function build_msg( -- @suppress "Unused declaration"
            encoded_frame : std_logic_vector(15 downto 0);
            msg           : string
        ) return string is
        begin
            if encoded_frame = idle_frame then
                return msg & " (received idle)";
            else
                return msg;
            end if;
        end;

        -- verify that the correct frame is scheduled (compare to the provided non-encoded frame)
        procedure verify_frame_12 (
            constant expected : std_logic_vector(11 downto 0);
            constant msg      : string := "Encoded frame mismatch" -- @suppress "Unused declaration"
        ) is
        begin
            frame_start_pulse_i <= '1';
            wait until rising_edge(clk);
            frame_start_pulse_i <= '0';
            wait until rising_edge(clk);
            wait until rising_edge(clk);

            if encoded_frame = idle_frame then
                error("Received IDLE instead of the expected frame");
            else
                -- decode the frame and compare it to the original
                check_value(decode_6b8b(encoded_frame(15 downto 8)), expected(11 downto 6), ERROR,
                    "Decoded frame doesn't match the original (upper byte)");
                check_value(decode_6b8b(encoded_frame(7 downto 0)), expected(5 downto 0), ERROR,
                    "Decoded frame doesn't match the original (lower byte)");
            end if;
        end;

        -- verify that the correct frame is scheduled (compare to the provided non-encoded frame)
        procedure verify_frame_7 (
            constant expected : std_logic_vector(6 downto 0)
        ) is
        begin
            verify_frame_12("00000" & expected);
        end;

        -- verify that correct bypass frame is scheduled (compare to the provided encoded frame)
        procedure verify_frame_16(
            constant expected : std_logic_vector(15 downto 0)
        ) is
        begin
            frame_start_pulse_i <= '1';
            wait until rising_edge(clk);
            frame_start_pulse_i <= '0';
            wait until rising_edge(clk);
            check_value(bypass_frame_ready, '1', ERROR, "Bypass frame is not acknowledged");
            wait until rising_edge(clk);
            check_value(encoded_frame, expected, ERROR, "Incorrect bypass frame");
        end;

        -- verify that the number of enqueued and dequeued LCB frames match
        procedure verify_frame_count is
        begin
            wait until rising_edge(clk);
            check_value(l1_wr_ctr, l1_rd_ctr, ERROR, "L1 FIFO - numer of enqueued and dequeued frames match");
            check_value(r3_wr_ctr, r3_rd_ctr, ERROR, "R3 FIFO - numer of enqueued and dequeued frames match");
        end;

        -- enqueue provided L1 frame and verify that the same LCB frame is scheduled
        procedure enqueue_and_verify_l1_frame(
            constant frame : std_logic_vector(6 downto 0)
        ) is
        begin
            enqueue_l1_frame(frame);
            await_value(l1_rd_ready, '1', 0 ns, 10 * C_CLK_PERIOD, ERROR, "L1 FIFO is ready (rd side)");
            verify_frame_7(frame);
            verify_frame_count;
        end;

        -- enqueue provided R3 frame and verify that the same LCB frame is scheduled
        procedure enqueue_and_verify_r3_frame(
            constant frame : std_logic_vector(11 downto 0)
        ) is
        begin
            enqueue_r3_frame(frame);
            await_value(r3_rd_ready, '1', 0 ns, 10 * C_CLK_PERIOD, ERROR, "R3 FIFO is ready (rd side)");
            verify_frame_12(frame);
            verify_frame_count;
        end;

        -- set bypass frame and verify it's scheduled
        procedure set_and_verify_bypass_frame(
            constant frame : std_logic_vector(15 downto 0)
        ) is
        begin
            bypass_frame_valid <= '1';
            bypass_frame <= frame;
            wait until rising_edge(clk);
            verify_frame_16(frame);
            bypass_frame_valid <= '0';
        end;

        -- check that IDLE frame is scheduled
        procedure verify_idle(
            constant msg : string := "Scheduled frame is different from IDLE"
        ) is
        begin
            frame_start_pulse_i <= '1';
            wait until rising_edge(clk);
            frame_start_pulse_i <= '0';
            wait until rising_edge(clk);
            wait until rising_edge(clk);
            check_value(encoded_frame, idle_frame, ERROR, msg);
        end;

        -- set RST to a given value and wait C_CYCLES_RST cycles
        procedure set_rst(
            value    : std_logic;
            duration : positive := C_CYCLES_RST
        ) is
        begin
            wait until rising_edge(clk);
            rst <= value;
            wait_num_rising_edge(clk, duration);
        end;

        -- variables for storing intermediate and random values
        --variable r3l1_frame_var    : std_logic_vector(12 downto 0);
        variable frame_range     : integer;

    begin
        --------------------------------------------------------------------------------
        -- Actual tests begin here
        --------------------------------------------------------------------------------

        clk_en <= true;
        disable_log_msg(ID_POS_ACK);

        ----------------------------------
        log(ID_LOG_HDR, "Checking that IDLE frames are scheduled when rst = 1");
        set_rst('1');
        for i in 0 to 9 loop
            verify_idle;
        end loop;

        ----------------------------------

        log(ID_LOG_HDR, "Verify that IDLE frames are scheduled when no other frames are unavailable");
        set_rst('0');
        bypass_frame_valid   <= '0';
        await_value(l1_wr_ready, '1', 0 ns, 10 * C_CLK_PERIOD, ERROR, "L1 FIFO is ready (wr side)");
        await_value(r3_wr_ready, '1', 0 ns, 10 * C_CLK_PERIOD, ERROR, "R3 FIFO is ready (wr side)");
        for i in 0 to 9 loop
            verify_idle;
        end loop;

        ----------------------------------

        log(ID_LOG_HDR, "Verify that L1 frames are scheduled when available");
        bypass_frame_valid   <= '0';
        for i in 0 to 9 loop
            enqueue_and_verify_l1_frame(random(7));
        end loop;

        ----------------------------------

        log(ID_LOG_HDR, "Verify that R3 frames are scheduled when available");
        bypass_frame_valid   <= '0';
        for i in 0 to 9 loop
            enqueue_and_verify_r3_frame(random(12));
        end loop;

        ----------------------------------

        log(ID_LOG_HDR, "Verify that bypass frames are scheduled when available");
        for i in 0 to 9 loop
            set_and_verify_bypass_frame(random(16));
        end loop;

        ----------------------------------

        log(ID_LOG_HDR, "Verify that R3 frames are prioritized over L1 frames");
        bypass_frame_valid   <= '0';
        enqueue_l1_frame("0110111");
        enqueue_l1_frame("1100011");
        enqueue_l1_frame("1010101");
        enqueue_l1_frame("0101110");
        enqueue_r3_frame(x"213");
        enqueue_r3_frame(x"FEA");
        enqueue_r3_frame(x"081");
        enqueue_r3_frame(x"971");
        await_value(l1_rd_ready, '1', 0 ns, 10 * C_CLK_PERIOD, ERROR, "L1 FIFO becomes valid");
        await_value(r3_rd_ready, '1', 0 ns, 10 * C_CLK_PERIOD, ERROR, "R3 FIFO becomes valid");
        verify_frame_12(x"213");
        verify_frame_12(x"FEA");
        verify_frame_12(x"081");
        verify_frame_12(x"971");
        verify_frame_7("0110111");
        verify_frame_7("1100011");
        verify_frame_7("1010101");
        verify_frame_7("0101110");
        verify_frame_count;

        ----------------------------------

        log(ID_LOG_HDR, "Verify that R3 frames are prioritized over bypass frames");
        bypass_frame_valid   <= '1';
        bypass_frame <= x"CAFE";
        enqueue_r3_frame(x"213");
        enqueue_r3_frame(x"FEA");
        enqueue_r3_frame(x"081");
        enqueue_r3_frame(x"971");
        await_value(r3_rd_ready, '1', 0 ns, 10 * C_CLK_PERIOD, ERROR, "R3 FIFO becomes valid");
        verify_frame_12(x"213");
        verify_frame_12(x"FEA");
        verify_frame_12(x"081");
        verify_frame_12(x"971");
        verify_frame_16(x"CAFE");
        verify_frame_16(x"CAFE");
        verify_frame_count;

        ----------------------------------

        log(ID_LOG_HDR, "Verify that L1 frames are prioritized over bypass frames");
        bypass_frame_valid   <= '1';
        bypass_frame <= x"8211";
        enqueue_l1_frame("0110111");
        enqueue_l1_frame("1100011");
        enqueue_l1_frame("1010101");
        enqueue_l1_frame("0101110");
        await_value(l1_rd_ready, '1', 0 ns, 10 * C_CLK_PERIOD, ERROR, "R3 FIFO becomes valid");
        verify_frame_7("0110111");
        verify_frame_7("1100011");
        verify_frame_7("1010101");
        verify_frame_7("0101110");
        verify_frame_16(x"8211");
        verify_frame_16(x"8211");
        verify_frame_count;

        ----------------------------------

        log(ID_LOG_HDR, "Verify that every possible R3 frame decodes back to itself");
        bypass_frame_valid   <= '0';
        frame_range          := 12;
        for i in 0 to 2**frame_range - 1 loop
            enqueue_and_verify_r3_frame(std_logic_vector(to_unsigned(i, frame_range)));
        end loop;

        ----------------------------------

        log(ID_LOG_HDR, "Verify that every possible L1 frame decodes back to itself");
        bypass_frame_valid   <= '0';
        frame_range          := 7;
        for i in 0 to 2**frame_range - 1 loop
            enqueue_and_verify_l1_frame(std_logic_vector(to_unsigned(i, frame_range)));
        end loop;

        ----------------------------------

        -- Ending the simulation
        wait for 100 ns;                -- to allow some time for completion
        report_alert_counters(FINAL);   -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED");

        -- Finish the simulation
        uvvm_completed <= '1';
        wait for 1 us;
        clk_en <= false;
        wait;                           -- to stop completely
    end process;

    clock_generator(clk, clk_en, C_CLK_PERIOD, "40 MHz BC clock");


    l1_frame_fifo : xpm_fifo_sync
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT"
            DOUT_RESET_VALUE => "0",    -- String
            ECC_MODE => "no_ecc",       -- String
            FIFO_MEMORY_TYPE => "distributed", -- String
            FIFO_READ_LATENCY => 0,     -- DECIMAL
            FIFO_WRITE_DEPTH => 16,   -- DECIMAL
            FULL_RESET_VALUE => 0,      -- DECIMAL
            PROG_EMPTY_THRESH => 4,    -- DECIMAL
            PROG_FULL_THRESH => 15,     -- DECIMAL
            RD_DATA_COUNT_WIDTH => 5,   -- DECIMAL = log2(FIFO_READ_DEPTH)+1.
            -- FIFO_READ_DEPTH =  FIFO_WRITE_DEPTH*WRITE_DATA_WIDTH / READ_DATA_WIDTH
            READ_DATA_WIDTH => 12,      -- DECIMAL
            READ_MODE => "fwft",         -- String
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_ADV_FEATURES => "0801", -- String
            WAKEUP_TIME => 0,           -- DECIMAL
            WRITE_DATA_WIDTH => 12,     -- DECIMAL
            WR_DATA_COUNT_WIDTH => 5    -- DECIMAL = log2(FIFO_WRITE_DEPTH)+1
        )
        port map (
            sleep => '0',
            rst => rst,
            wr_clk => clk,
            wr_en => l1_frame_wr_en,
            din => l1_frame_din,
            full => open,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => l1_frame_wr_rst_busy,
            almost_full => l1_frame_full,
            wr_ack => open,
            rd_en => l1_frame_rd_en,
            dout => l1_frame,
            empty => l1_frame_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => l1_frame_rd_rst_busy,
            almost_empty => l1_frame_almost_empty,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );


    r3_frame_fifo : xpm_fifo_sync
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT"
            DOUT_RESET_VALUE => "0",    -- String
            ECC_MODE => "no_ecc",       -- String
            FIFO_MEMORY_TYPE => "distributed", -- String
            FIFO_READ_LATENCY => 0,     -- DECIMAL
            FIFO_WRITE_DEPTH => 16,   -- DECIMAL
            FULL_RESET_VALUE => 0,      -- DECIMAL
            PROG_EMPTY_THRESH => 4,    -- DECIMAL
            PROG_FULL_THRESH => 15,     -- DECIMAL
            RD_DATA_COUNT_WIDTH => 5,   -- DECIMAL = log2(FIFO_READ_DEPTH)+1.
            -- FIFO_READ_DEPTH =  FIFO_WRITE_DEPTH*WRITE_DATA_WIDTH / READ_DATA_WIDTH
            READ_DATA_WIDTH => 12,      -- DECIMAL
            READ_MODE => "fwft",         -- String
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_ADV_FEATURES => "0801", -- String
            WAKEUP_TIME => 0,           -- DECIMAL
            WRITE_DATA_WIDTH => 12,     -- DECIMAL
            WR_DATA_COUNT_WIDTH => 5    -- DECIMAL = log2(FIFO_WRITE_DEPTH)+1
        )
        port map (
            sleep => '0',
            rst => rst,
            wr_clk => clk,
            wr_en => r3_frame_wr_en,
            din => r3_frame_din,
            full => open,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => r3_frame_wr_rst_busy,
            almost_full => r3_frame_full,
            wr_ack => open,
            rd_en => r3_frame_rd_en,
            dout => r3_frame,
            empty => r3_frame_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => r3_frame_rd_rst_busy,
            almost_empty => r3_frame_almost_empty,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );

    DUT : entity work.r3l1_scheduler_encoder
        port map(
            clk                     => clk,
            rst                     => rst,
            r3_frame_i              => r3_frame,
            r3_frame_o_rd_en        => r3_frame_rd_en,
            r3_frame_i_empty        => r3_frame_empty,
            r3_frame_i_almost_empty => r3_frame_almost_empty,
            l1_frame_i              => l1_frame,
            l1_frame_o_rd_en        => l1_frame_rd_en,
            l1_frame_i_empty        => l1_frame_empty,
            l1_frame_i_almost_empty => l1_frame_almost_empty,
            bypass_frame_i          => bypass_frame,
            bypass_frame_valid_i    => bypass_frame_valid,
            bypass_frame_ready_o    => bypass_frame_ready,
            frame_start_pulse_i     => frame_start_pulse_i,
            encoded_frame_o         => encoded_frame
        );

    l1_rd_ready <= (not l1_frame_empty) and (not l1_frame_rd_rst_busy);
    l1_wr_ready <= (not l1_frame_full) and (not l1_frame_wr_rst_busy);
    r3_rd_ready <= (not r3_frame_empty) and (not r3_frame_rd_rst_busy);
    r3_wr_ready <= (not r3_frame_full) and (not r3_frame_wr_rst_busy);

    fifo_counters : process(clk)    -- keep track of how many data frames passed the frame FIFOs
    begin
        if (rising_edge(clk)) then
            if l1_frame_rd_en = '1' then
                l1_rd_ctr <= l1_rd_ctr + 1;
            end if;
            if l1_frame_wr_en = '1' then
                l1_wr_ctr <= l1_wr_ctr + 1;
            end if;
            if r3_frame_rd_en = '1' then
                r3_rd_ctr <= r3_rd_ctr + 1;
            end if;
            if r3_frame_wr_en = '1' then
                r3_wr_ctr <= r3_wr_ctr + 1;
            end if;
        end if;
    end process;

    sanity_check : process(clk)
    begin
        if rising_edge(clk) then
            if l1_frame_rd_en = '1' then
                check_value(l1_frame_empty, '0', ERROR, "Attempting to read from empty FIFO (l1_frame)");
                check_value(l1_frame_rd_rst_busy, '0', ERROR, "Attempting to read from FIFO while it's reset (l1_frame)");
            end if;

            if l1_frame_wr_en = '1' then
                check_value(l1_frame_full, '0', ERROR, "Attempting to write into a full FIFO (l1_frame)");
                check_value(l1_frame_wr_rst_busy, '0', ERROR, "Attempting write into the FIFO while it's reset (l1_frame)");
            end if;

            if r3_frame_rd_en = '1' then
                check_value(r3_frame_empty, '0', ERROR, "Attempting to read from empty FIFO (r3_frame)");
                check_value(r3_frame_rd_rst_busy, '0', ERROR, "Attempting to read from FIFO while it's reset (r3_frame)");
            end if;

            if r3_frame_wr_en = '1' then
                check_value(r3_frame_full, '0', ERROR, "Attempting to write into a full FIFO (r3_frame)");
                check_value(r3_frame_wr_rst_busy, '0', ERROR, "Attempting write into the FIFO while it's reset (r3_frame)");
            end if;
        end if;
    end process;

end architecture behavioral;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : Testbench simulation for LCB encoder
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : tb_lcb_axi_encoder.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Tue May 28 20:00:40 2020
-- Last update : Thu Aug 12 19:46:38 2021
-- Platform    : Default Part Number
-- Standard    : <VHDL-2008>
--------------------------------------------------------------------------------
-- Copyright (c) 2019 BNL
-------------------------------------------------------------------------------
-- Description:
--
-- Verify that the MUX is forwarding the data to the correct destination
--
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

library uvvm_vvc_framework;
    use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

library bitvis_vip_axistream;
    context bitvis_vip_axistream.vvc_context;

library bitvis_vip_clock_generator;
    context bitvis_vip_clock_generator.vvc_context;

    use work.strips_package.all;
    use work.lcb_regmap_package.all;

    use work.decoder_queue_pkg.all;
    use work.bypass_data_parser.all;
    use work.ttc_l0a_data_parser.all;
    use work.axi_stream_package.all;
    use work.elink_data_parser.all;

entity tb_lcb_axi_encoder is
    generic(
        use_vunit: boolean := false -- @suppress "Unused generic: use_vunit is not used in work.tb_lcb_axi_encoder(RTL)"
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity tb_lcb_axi_encoder;

architecture RTL of tb_lcb_axi_encoder is
    constant C_CLK_PERIOD          : time    := 25 ns;
    constant C_AXI_CLK_PERIOD      : time    := 5 ns;
    constant C_CYCLES_RST          : integer := 3;
    --constant C_STABLE_PERIODS      : integer := 10;
    --constant C_MAX_CYCLES_PER_BYTE : integer := 4;
    constant C_USER_WIDTH          : integer := 1;
    constant C_ID_WIDTH            : integer := 1;
    constant C_DEST_WIDTH          : integer := 1;
    constant C_AXI_DATA_WIDTH      : integer := 8;
    --constant C_FRAME_WIDTH         : integer := 12;
    --constant wait_config_change    : integer := 3;

    signal rst : std_logic := '0';
    signal reset : std_logic := '0';
    signal clk : std_logic;
    signal s_axis_aclk : std_logic;

    -- axistream command sources
    signal config_axi : t_axistream_if(--@suppress
        tdata(C_AXI_DATA_WIDTH - 1 downto 0),
        tkeep((C_AXI_DATA_WIDTH / 8) - 1 downto 0),
        tuser(C_USER_WIDTH - 1 downto 0),
        tstrb((C_AXI_DATA_WIDTH / 8) - 1 downto 0),
        tid(C_ID_WIDTH - 1 downto 0),
        tdest(C_DEST_WIDTH - 1 downto 0)
    );

    signal trickle_axi : t_axistream_if(--@suppress
        tdata(C_AXI_DATA_WIDTH - 1 downto 0),
        tkeep((C_AXI_DATA_WIDTH / 8) - 1 downto 0),
        tuser(C_USER_WIDTH - 1 downto 0),
        tstrb((C_AXI_DATA_WIDTH / 8) - 1 downto 0),
        tid(C_ID_WIDTH - 1 downto 0),
        tdest(C_DEST_WIDTH - 1 downto 0)
    );

    signal command_axi : t_axistream_if(--@suppress
        tdata(C_AXI_DATA_WIDTH - 1 downto 0),
        tkeep((C_AXI_DATA_WIDTH / 8) - 1 downto 0),
        tuser(C_USER_WIDTH - 1 downto 0),
        tstrb((C_AXI_DATA_WIDTH / 8) - 1 downto 0),
        tid(C_ID_WIDTH - 1 downto 0),
        tdest(C_DEST_WIDTH - 1 downto 0)
    );

    signal command_s_axis : axis_8_type;
    signal config_s_axis : axis_8_type;
    signal trickle_s_axis : axis_8_type;

    -- TTC signals
    signal bcr               : std_logic                    := '0';
    signal enable_bcr        : std_logic                    := '0';
    signal bcr_expected      : std_logic                    := '0';
    signal l0a               : std_logic                    := '0';
    signal l0id              : std_logic_vector(6 downto 0) := (others => '0');
    signal frame_start_pulse : std_logic                    := '0';

    -- configuration from global register map
    signal config               : t_strips_config;

    -- decoder
    signal EdataOUT        : std_logic_vector(3 downto 0);
    signal skip_idles_i    : std_logic := '1';
    signal phase_wrt_bcr_o : integer range 0 to 3;
    signal decoder_rst     : std_logic := '0';

    --frame_encoded_o : std_logic_vector(15 downto 0);
    signal is_locked_o   : std_logic;
    signal stopwatch_o   : integer;
    signal stopwatch_p1  : integer;
    signal stopwatch_set : std_logic := '0';
    signal stopwatch_rst : std_logic := '0';

    -- output signals
    signal frame                : t_decoder_record;
    shared variable frame_queue : work.decoder_queue_pkg.t_generic_queue;
    signal frame_valid          : std_logic;

    -- simulation sample frame data
    shared variable bypass       : work.bypass_data_parser.t_bypass_file_parser;
    shared variable ttc_l0a      : work.ttc_l0a_data_parser.t_ttc_l0a_file_parser;
    shared variable elink_parser : work.elink_data_parser.t_elink_file_parser;

    constant repeat_samples                 : integer := 20; -- how many repetitions each test does
    constant low_priority_frame_repetitions : integer := 10; -- how many times to repeat LP frames to test scheduling priority
    constant max_bcr_delay             : integer := 40; -- maximum configured BCR delay for the test
    constant max_l0a_delay : integer := 2**2 - 1;

    constant config_channel : integer := 2;
    constant trickle_channel : integer := 3;
    constant command_channel : integer := 4;

    -- DUT internal signals
    --signal frame_phase_reg : std_logic_vector(15 downto 0); -- not used
    --signal bcr_delay_reg : std_logic_vector(15 downto 0); --not used
    signal dut_regmap :t_register_map;
    signal l0a_trig_i : std_logic_vector(3 downto 0); -- generated externally
    signal  sync_i : std_logic := '0'; --generated externally

begin

    config_s_axis.tvalid  <= config_axi.tvalid;--@suppress
    config_s_axis.tdata   <= config_axi.tdata;--@suppress
    config_s_axis.tlast   <= config_axi.tlast;--@suppress

    command_s_axis.tvalid <= command_axi.tvalid;--@suppress
    command_s_axis.tdata  <= command_axi.tdata;--@suppress
    command_s_axis.tlast  <= command_axi.tlast;--@suppress

    trickle_s_axis.tvalid  <= trickle_axi.tvalid;--@suppress
    trickle_s_axis.tdata   <= trickle_axi.tdata;--@suppress
    trickle_s_axis.tlast   <= trickle_axi.tlast;--@suppress

    DUT : entity work.lcb_axi_encoder
        generic map(
            --DEBUG_en => false,
            USE_ULTRARAM => false,
            DISTR_RAM => false)
        port map(
            daq_reset => rst,
            clk40                => clk,
            --ila_clk              => '0',
            s_axis_aclk      => s_axis_aclk,
            daq_fifo_flush => reset,
            invert_polarity    => '0',

            -- configuration data source
            config_s_axis        => config_s_axis,
            config_tready        => config_axi.tready,--@suppress
            config_almost_full   => open,

            -- command data source
            command_s_axis       => command_s_axis,
            command_tready       => command_axi.tready,--@suppress
            command_almost_full  => open,

            -- trickle configuration data source
            trickle_s_axis       => trickle_s_axis,
            trickle_tready       => trickle_axi.tready,--@suppress
            trickle_almost_full  => open,

            -- TTC signals
            bcr_i                => bcr, --a clock cycle ahead of the nearest sync
            l0a_i                => l0a, --not used
            l0a_trig_i           => l0a_trig_i,
            l0id_i               => l0id,

            sync_i               => sync_i,
            -- configuration from global register map
            config               => config,

            -- For simulation
            frame_start_pulse_o  => frame_start_pulse,
            regmap_o => dut_regmap,

            -- output signals
            EdataOUT             => EdataOUT
        );

    -- LCB/R3L1 frame alignment and decoding module
    i_decoder : entity work.itk_frame_decoder
        port map(
            clk => clk,
            rst => decoder_rst,
            bcr => bcr,
            stopwatch_set => stopwatch_set,
            stopwatch_rst => stopwatch_rst,
            edata_i => EdataOUT,
            skip_idles_i => skip_idles_i,
            frame_encoded_o => frame.encoded,
            is_locked_o => frame.locked,
            is_idle_o => frame.idle,
            is_k2_o => frame.k2,
            is_k3_o => frame.k3,
            frame_decoded_o => frame.decoded,
            frame_next_o => frame_valid,
            phase_wrt_bcr_o => phase_wrt_bcr_o,
            stopwatch_o => stopwatch_o
        );

    stopwatch_pipe: process(clk)
    begin
        if rising_edge(clk) then
            stopwatch_p1 <= stopwatch_o;
        end if;
    end process;

    is_locked_o       <= frame.locked;
    --frame_start_pulse <= << signal DUT.frame_start_pulse : std_logic >>;
    --dut_regmap <= << signal DUT.LCB_inst.regmap : t_register_map >>;
    --frame_phase_reg <= dut_regmap(L0A_FRAME_PHASE); --not used
    --bcr_delay_reg <= dut_regmap(TTC_BCR_DELAY); --not used

    p_test : process is
        variable frame_rnd           : std_logic_vector(11 downto 0); -- L0 frame
        variable frame_sent          : std_logic_vector(11 downto 0); -- L0 frame
        variable next_frame          : t_decoder_record;
        variable next_frame_timeout   : boolean := false;
        variable bypass_data         : t_bypass_data_record;
        variable ttc_l0a_data        : t_l0a_data_record;
        variable l0a_elink_data      : t_elink_data_record;
        variable register_elink_data : t_elink_data_record;
        variable fast_elink_data     : t_elink_data_record;
        variable mask_test_data      : t_elink_data_record;
        variable trickle_test_data_1 : t_elink_data_record;
        variable trickle_test_data_2 : t_elink_data_record;
        variable calibration_data    : t_elink_data_record;

        variable bc_start : integer := 0;
        variable bc_stop : integer := 0;
        variable gating_test_frames : integer := 0;

        -- simulates receiving an L1 trigger
        procedure send_L0A(
            --bcr_i : std_logic;
            mask  : std_logic_vector(3 downto 0);
            tag   : std_logic_vector(6 downto 0);
            randomize_tag : boolean := true
        ) is
            variable bcr_time : integer range 0 to 3;
        begin
            log(ID_SEQUENCER_SUB, "Sending L0A frame: " & to_hstring(bcr_expected & mask & tag));
            wait until rising_edge(sync_i);
            -- determine when to assert BCR as to not affect the frame phase
            --            case frame_phase_reg(1 downto 0) is
            --                when "00"   => bcr_time := 1;
            --                when "01"   => bcr_time := 0;
            --                when "10"   => bcr_time := 3;
            --                when "11"   => bcr_time := 2;
            --                when others => error("Incorrect LCB frame phase");
            --            end case;

            --            for i in mask'range loop
            --                -- assign the expected tag on the first BC, random tags for other BCs
            --                if i = mask'left or (not randomize_tag) then
            --                    l0id <= tag;
            --                else
            --                    l0id <= random(tag'length);
            --                end if;
            --                -- assert BCR during the correct BC of the frame (if needed)
            --                if bcr_time = i then
            --                    bcr <= bcr_i;
            --                else
            --                    bcr <= '0';
            --                end if;
            --                l0a <= mask(i);
            --                l0a_trig_i <= mask(i)&"000";
            --                wait until rising_edge(clk);
            --            end loop;


            --bcr <= '0';
            --l0a <= '0';
            l0id <= tag;-- when (not randomize_tag) else random(tag'length);
            l0a_trig_i <= mask; --"000";
            frame_rnd(11) := bcr_expected;
            wait until rising_edge(clk);
            l0id <= (others => '0');
            l0a_trig_i <= "0000";

        end;

        -- simulates receiving a random L0A trigger (no BCR)
        procedure send_random_L0A is
        begin
            frame_rnd := random(frame_rnd'length);
            while frame_rnd(11 downto 7) = "00000" loop
                frame_rnd := random(frame_rnd'length);
            end loop;

            send_L0A(frame_rnd(10 downto 7), frame_rnd(6 downto 0)); --frame_rnd(11),
        end;

        -- sends ABC* register read command
        procedure send_abc_reg_read (
            hcc_id   : std_logic_vector(3 downto 0) := x"0";
            abc_id   : std_logic_vector(3 downto 0) := x"0";
            addr   : std_logic_vector(7 downto 0) := x"00";
            wait_to_completion : boolean := true) is
            variable hcc_abc_id: std_logic_vector(7 downto 0);
        begin
            hcc_abc_id := hcc_id & abc_id;
            axistream_transmit(AXISTREAM_VVCT, command_channel,--@suppress
                t_slv_array'(ITK_CMD_ABC_REG_READ, addr, hcc_abc_id),
                "Read ABC* register, hcc_id = " & to_hstring(hcc_id)
                & ", abc_id = " & to_hstring(abc_id) & ", addr = " & to_hstring(addr));
            if wait_to_completion then
                await_completion(AXISTREAM_VVCT, command_channel, 500 * C_CLK_PERIOD,--@suppress
                    "Waiting for the command to complete");
            end if;
        end;

        -- sends ABC* register write command
        procedure send_abc_reg_write (
            hcc_id   : std_logic_vector(3 downto 0) := x"0";
            abc_id   : std_logic_vector(3 downto 0) := x"0";
            addr   : std_logic_vector(7 downto 0) := x"00";
            data   : std_logic_vector(31 downto 0) := x"00000000";
            wait_to_completion : boolean := true) is
            variable hcc_abc_id: std_logic_vector(7 downto 0);
        begin
            hcc_abc_id := hcc_id & abc_id;
            axistream_transmit(AXISTREAM_VVCT, command_channel,--@suppress
                t_slv_array'(ITK_CMD_ABC_REG_WRITE, data(31 downto 24), data(23 downto 16),
                data(15 downto 8), data(7 downto 0), addr, hcc_abc_id),
                "Write ABC* register, hcc_id = " & to_hstring(hcc_id)
                & ", abc_id = " & to_hstring(abc_id) & ", addr = " & to_hstring(addr)
                & ", data = " & to_hstring(data));
            if wait_to_completion then
                await_completion(AXISTREAM_VVCT, command_channel, 500 * C_CLK_PERIOD,--@suppress
                    "Waiting for the command to complete");
            end if;
        end;

        -- checks that only IDLE frames have been received
        -- (this procedure works only when decoder ignores idle frames)
        procedure assert_idle_only is
        begin
            check_value(is_locked_o, '1', ERROR, "Frame decoder is locked");
            check_value(frame_queue.is_empty(void), true, ERROR, "Decoder only received IDLEs");
        end;

        -- wait for the frame and verify the contents
        -- returns false if there were no timeout, true if there was
        procedure wait_for_next_frame(
            expiration_time : time := 1000 ns;
            error_on_timeout : boolean := true
        ) is
            variable time_expire : time;
        begin
            time_expire := now + expiration_time;
            wait_loop : while now < time_expire loop
                exit wait_loop when (not frame_queue.is_empty(void));
                --Use falling edge because the behaviour of frame_queue depends on questasim voptargs settings.
                wait until falling_edge(clk);
            end loop;

            if now >= time_expire then
                if error_on_timeout then
                    error("Timeout while waiting for the next frame");
                else
                    log(ID_SEQUENCER_SUB, "Timeout while waiting for the next frame");
                end if;
                next_frame_timeout := true;
            else
                next_frame_timeout := false;
            end if;
        end;

        -- wait for the frame and verify the contents
        procedure dequeue_next_frame(
            expiration_time : time := 1000 ns;
            error_on_timeout : boolean := true
        ) is
        begin
            wait_for_next_frame(expiration_time, error_on_timeout);
            if not next_frame_timeout then
                next_frame := frame_queue.fetch(void);
            end if;
        end;

        -- relock frame decoder, measure the frame phase
        procedure verify_frame_phase(expected : integer range 0 to 3) is
        begin
            gen_pulse(decoder_rst, C_CYCLES_RST * C_CLK_PERIOD, "Reset frame decoder");
            wait until rising_edge(clk);
            --            for i in 0 to 5 loop
            --                bcr <= '1';
            --                wait until rising_edge(clk);
            --                bcr <= '0';
            --                wait_num_rising_edge(clk, 31);
            --            end loop;
            check_value(is_locked_o, '1', ERROR, "Verifying that frame decoder is locked");
            check_value(phase_wrt_bcr_o, expected, ERROR, "Verifying frame phase with respect to BCR signal");
        end;

        -- initialize LCB link configuration
        procedure reset_lcb_configuration is
        begin
            config.GLOBAL_STRIPS_CONFIG.INVERT_AMAC_IN <= (others => '0');
            config.GLOBAL_STRIPS_CONFIG.INVERT_AMAC_OUT <= (others => '0');
            config.GLOBAL_STRIPS_CONFIG.INVERT_AMAC_IN <= (others => '0');
            config.GLOBAL_STRIPS_CONFIG.INVERT_DIN <= (others => '0');
            config.GLOBAL_STRIPS_CONFIG.INVERT_R3L1_OUT <= (others => '0');
            config.GLOBAL_STRIPS_CONFIG.INVERT_LCB_OUT <= (others => '0');
            config.GLOBAL_STRIPS_CONFIG.TEST_MODULE_MASK <= (others => '0');
            config.GLOBAL_STRIPS_CONFIG.TEST_R3L1_TAG <= (others => '0');
            config.GLOBAL_STRIPS_CONFIG.TTC_GENERATE_GATING_ENABLE <= (others => '0');
            config.GLOBAL_STRIPS_CONFIG.TTC_GATING_OVERRIDE <= (others => '0');
            config.GLOBAL_TRICKLE_TRIGGER <= (others => '0');
            config.STRIPS_R3_TRIGGER <= (others => '0');
            config.STRIPS_L1_TRIGGER <= (others => '0');
            config.STRIPS_R3L1_TRIGGER <= (others => '0');
        end;

        -- send bypass frame through elink
        procedure send_bypass_frame(data : std_logic_vector(15 downto 0)) is
            variable bytes : t_slv_array(0 to 1)(7 downto 0) ;
        begin
            log(ID_SEQUENCER, "Sending bypass frame: " & to_hstring(data));
            bytes(0) := data(15 downto 8);
            bytes(1) := data(7 downto 0);
            axistream_transmit(AXISTREAM_VVCT, command_channel, bytes,--@suppress
                "Sending bypass frame data");
        end;

        -- send a single elink command
        procedure send_elink_command(
            data       : work.elink_data_parser.t_byte_array;
            byte_count : natural;
            channel : integer := 4
        ) is
            variable comment : string(21 downto 1);
        begin
            case channel is
                when config_channel   => comment := string'("configuration input  ");
                when trickle_channel  => comment := string'("trickle configuration");
                when command_channel  => comment := string'("command input        ");
                when others =>
                    comment := string'("UNKNOWN CHANNEL      ");
                    error("Unknown LCB data channel is selected");
            end case;
            for i in 0 to byte_count - 1 loop
                axistream_transmit(AXISTREAM_VVCT, channel, data(i),--@suppress
                    "Sending data to the elink: " & comment);
            end loop;
        end;

        -- send a all available elink commands
        procedure send_elink_commands(
            data : t_elink_data_record;
            channel : integer := 4
        ) is
        begin
            for i in 0 to data.inputs_count - 1 loop
                send_elink_command(data.inputs(i).data, data.inputs(i).count, channel => channel);
            end loop;
        end;

        -- send a single elink command
        procedure verify_elink_command_response(
            data            : work.elink_data_parser.t_byte_array;
            byte_count      : natural;
            expiration_time : time := 1000 ns
        ) is
            variable expected_frame : std_logic_vector(15 downto 0);
        begin
            check_value(byte_count mod 2, 0, ERROR, "Response must consist of an integer number of frames");

            for i in 0 to byte_count / 2 - 1 loop
                dequeue_next_frame(expiration_time);
                expected_frame := data(2 * i) & data(2 * i + 1);
                log(ID_SEQUENCER_SUB, "Received frame: " & to_hstring(next_frame.encoded));
                check_value(next_frame.encoded, expected_frame, ERROR, "Received expected LCB frame");
            end loop;
        end;

        -- verify all available elink command responses
        procedure verify_elink_command_responses(
            data            : t_elink_data_record;
            expiration_time : time    := 2000 ns;
            idles_timeout   : integer := 40
        ) is
            variable leading_idles : integer := 0;
        begin
            if skip_idles_i = '0' then
                log(ID_SEQUENCER_SUB, "Skipping leading IDLE frames");
                loop
                    wait_for_next_frame(expiration_time);
                    next_frame := frame_queue.peek(void);
                    if next_frame.idle = '0' then
                        exit;
                    else
                        leading_idles := leading_idles + 1;
                        if leading_idles > idles_timeout then
                            error("Timed out while verifying elink command response");
                        end if;
                        next_frame    := frame_queue.fetch(void);
                    end if;
                end loop;
            end if;

            for i in 0 to data.outputs_count - 1 loop
                verify_elink_command_response(data.outputs(i).data, data.outputs(i).count, expiration_time);
            end loop;
        end;

        -- verify all available elink command responses
        procedure verify_response_length(
            frame_count     : natural;
            expiration_time : time    := 1000 ns;
            idles_timeout   : integer := 40
        ) is
            variable leading_idles : integer := 0;
        begin
            if skip_idles_i = '0' then
                log(ID_SEQUENCER_SUB, "Skipping leading IDLE frames");
                loop
                    wait_for_next_frame(expiration_time);
                    next_frame := frame_queue.peek(void);
                    if next_frame.idle = '0' then
                        exit;
                    else
                        leading_idles := leading_idles + 1;
                        if leading_idles > idles_timeout then
                            error("Timed out while verifying elink command response");
                        end if;
                        next_frame    := frame_queue.fetch(void);
                    end if;
                end loop;
            end if;

            for i in 0 to frame_count - 1 loop
                dequeue_next_frame(expiration_time);
                log(ID_SEQUENCER_SUB, "Received frame: " & to_hstring(next_frame.encoded));
            end loop;

            wait_for_next_frame(expiration_time, error_on_timeout => false);
            check_value(frame_queue.is_empty(void), true, ERROR, "There are no stray frames enqueued");
        end;

        -- send elink data and verify the response is correct
        procedure send_elink_commands_verify_response(data : t_elink_data_record) is
        begin
            for i in 0 to data.inputs_count - 1 loop
                log(ID_SEQUENCER_SUB, "Verifying elink command responses, "
                    & to_string( (i * 100) / data.inputs_count ) & "% done");
                send_elink_command(data.inputs(i).data, data.inputs(i).count);
                verify_elink_command_response(data.outputs(i).data, data.outputs(i).count);
            end loop;
        end;

        -- set local LCB register
        procedure set_register(
            addr : std_logic_vector(7 downto 0);
            data : std_logic_vector(15 downto 0);
            timeout_clk_periods : integer := 500 ) is
        begin
            axistream_transmit(AXISTREAM_VVCT, config_channel,--@suppress
                t_slv_array'(ITK_CMD_REGMAP_WRITE, data(15 downto 8), data(7 downto 0), addr),
                "Updating local register, address = " & to_hstring(addr) & " ("
                & to_string(t_register_name'val(to_integer(unsigned(addr)))) & ")" & ", data = "
                & to_hstring(data));
            await_completion(AXISTREAM_VVCT, config_channel, timeout_clk_periods * C_CLK_PERIOD,--@suppress
                "Waiting for the local registers to be written");
            wait_num_rising_edge(clk, 3);
        end;

        procedure set_register(
            addr : unsigned;
            data : std_logic_vector(15 downto 0);
            timeout_clk_periods : integer := 500) is
        begin
            set_register(std_logic_vector(addr), data, timeout_clk_periods);
        end;

        procedure set_register(
            addr : integer;
            data : std_logic_vector(15 downto 0);
            timeout_clk_periods : integer := 500) is
        begin
            set_register(to_unsigned(addr, 8), data, timeout_clk_periods);
        end;

        -- masks a chip with selected hcc_id and abc_id
        procedure set_abc_mask(
            hcc_id : integer range 0 to 15;
            abc_id : integer range 0 to 15
        ) is
            variable mask : std_logic_vector(15 downto 0) := (others => '0');
            variable start : unsigned(7 downto 0);
        begin
            set_register(t_register_name'pos(HCC_MASK), x"0000");
            mask(hcc_id) := '1';
            start := to_unsigned(t_register_name'pos(ABC_MASK_0), 8);

            for i in 0 to 15 loop
                set_register(start + to_unsigned(i, 8), mask(i) and
                    std_logic_vector(to_unsigned(2**abc_id, 16)));
            end loop;
            wait_num_rising_edge(clk, 50);
        end;

        -- masks a chip with selected hcc_id and abc_id
        procedure reset_abc_hcc_mask is
            variable start : unsigned (7 downto 0);
        begin
            set_register(t_register_name'pos(HCC_MASK), x"0000");

            start := to_unsigned(t_register_name'pos(ABC_MASK_0), 8);
            for i in 0 to 15 loop
                set_register(std_logic_vector(start + to_unsigned(i, 8)), x"0000");
            end loop;

            await_completion(AXISTREAM_VVCT, config_channel, 17 * 20 * C_CLK_PERIOD,--@suppress
                "Waiting for the local registers to be written");
        end;

        -- helper function for check_if_low_priority_sequence_was_interrupted
        procedure check_LP_frame(
            LP_frame            : std_logic_vector(15 downto 0);
            is_LP_frame_encoded : boolean := true;
            check_LP_contents   : boolean := true
        ) is
        begin
            if check_LP_contents then
                if is_LP_frame_encoded then
                    check_value(next_frame.encoded, LP_frame, ERROR, "Received expected low-priority frame");
                else
                    check_value(next_frame.decoded, LP_frame(11 downto 0), ERROR, "Received expected low-priority frame");
                end if;
            end if;
        end;

        -- returns True if low-priority sequence was interrupted, False otherwise
        procedure check_if_low_priority_sequence_was_interrupted(
            LP_frame            : std_logic_vector(15 downto 0);
            HP_frame            : std_logic_vector(15 downto 0);
            LP_frame_count      : integer;
            is_LP_frame_encoded : boolean := true;
            is_HP_frame_encoded : boolean := true;
            check_LP_contents   : boolean := true
        ) is
            variable interrupted : boolean := false;
        begin
            interrupted := false;
            for i in 0 to LP_frame_count loop -- receive and verify all data
                dequeue_next_frame;
                if is_HP_frame_encoded then
                    if next_frame.encoded = HP_frame then
                        interrupted := true when i /= LP_frame_count;
                    else
                        check_LP_frame(LP_frame, is_LP_frame_encoded, check_LP_contents);
                    end if;
                else
                    if next_frame.decoded = HP_frame(11 downto 0) then
                        interrupted := true when i /= LP_frame_count;
                    else
                        check_LP_frame(LP_frame, is_LP_frame_encoded, check_LP_contents);
                    end if;
                end if;
            end loop;
            wait_num_rising_edge(clk, 4 * (LP_frame_count + 10));
            check_value(frame_queue.is_empty(void), true, ERROR, "There are no stray frames enqueued");
            check_value(interrupted, true, ERROR, "Low-priority frame sequence was interrupted by high-priority frame");
        end procedure;

        -- triggers a single trickle trigger pulse
        procedure issue_software_trickle_trigger is
        begin
            set_register(t_register_name'pos(TRICKLE_TRIGGER_PULSE), x"0001");
        end;

        procedure set_stopwatch is
        begin
            stopwatch_set <= '1';
            wait until rising_edge(clk);
            stopwatch_set <= '0';
        end procedure;

        procedure reset_stopwatch is
        begin
            stopwatch_rst <= '1';
            wait until rising_edge(clk);
            stopwatch_rst <= '0';
        end procedure;

        procedure set_encoding(mode : boolean) is
        begin
            if mode then
                log(ID_SEQUENCER, "Configure LCB link to use firmware encoding");
                set_register(t_register_name'pos(ENCODING_ENABLE), x"0001");
            else
                log(ID_SEQUENCER, "Configure LCB link to use software encoding");
                set_register(t_register_name'pos(ENCODING_ENABLE), x"0000");
            end if;
            wait_num_rising_edge(clk, 50);
        end;


        --- checks that low-priority frames respect gating configuration
        procedure verify_bc_gating(
            expected_frame : std_logic_vector(15 downto 0)
        ) is
        begin
            log(ID_SEQUENCER, "Issuing BCR to start gating pulse generation");
            wait until rising_edge(bcr);
            send_L0A( "0000", "0000000"); --'1',
            set_stopwatch;
            dequeue_next_frame;
            check_value(next_frame.decoded, "100000000000", ERROR, "BCR frame received");

            if (CFG_INCLUDE_BCID_GATING = '1') then
                wait_for_next_frame(expiration_time => (bc_stop + 100) * C_CLK_PERIOD);
                log(ID_SEQUENCER, "Received frames, bc_start=" & to_string(bc_start) & ", bc_stop = " & to_string(bc_stop)
                  & ", stopwatch shows " & to_string(stopwatch_p1));
                check_value(stopwatch_p1 >= bc_start, true, ERROR, "Low-priority frames start around the correct time");
                check_value(stopwatch_p1 <= bc_start + 20, true, ERROR, "Low-priority frames start around the correct time");
                reset_stopwatch;
                for i in 0 to gating_test_frames/2 - 1 loop -- half of the frames was supposed to be scheduled
                    dequeue_next_frame;
                    check_value(next_frame.encoded, expected_frame, ERROR, "Verifying low-priority frame contents");
                end loop;
                wait_num_rising_edge(clk, 40);
                check_value(frame_queue.is_empty(void), true, ERROR, "No frames are scheduled after gating interval is over");
                log(ID_SEQUENCER, "Disabling BC gating");
                set_register(t_register_name'pos(GATING_TTC_ENABLE), x"0000");
                for i in 0 to gating_test_frames/2 - 1 loop -- the remaining frames come out because gating is disabled
                    dequeue_next_frame;
                    check_value(next_frame.encoded, expected_frame, ERROR, "Verifying remaining low-priority frame contents");
                end loop;

            else
                for i in 0 to gating_test_frames - 1 loop
                    dequeue_next_frame;
                    check_value(next_frame.encoded, expected_frame, ERROR, "Verifying low-priority frame contents");
                end loop;
                wait_num_rising_edge(clk, 40);
                log(ID_SEQUENCER, "No gating in build - just checking frames");
                for i in 0 to gating_test_frames - 1 loop
                    dequeue_next_frame;
                    check_value(next_frame.encoded, expected_frame, ERROR, "Verifying low-priority frame contents");
                end loop;

            end if;

        end;

        --- checks that register frames are always complete when coming from trickle configuration memory
        procedure verify_register_frame_completion is
            variable register_command_open :boolean := false;
            variable commands_received :integer := 0;
            variable interruption_roll :integer;
        begin
            log(ID_SEQUENCER, "Issuing BCR to start gating pulse generation");
            wait until rising_edge(bcr);
            wait until rising_edge(clk);
            --send_L0A("0000", "0000000"); --'1',
            dequeue_next_frame;
            check_value(next_frame.decoded, "100000000000", ERROR, "BCR frame received");
            register_command_open := false;
            commands_received := 0;
            dequeue_next_frame(expiration_time => (bc_stop + 100) * C_CLK_PERIOD);

            while not next_frame_timeout loop
                interruption_roll := random(1,10);
                if interruption_roll = 1 then
                    -- interrupt with L0A frame
                    send_L0A( mask => "1011", tag => "0100111"); --bcr_i => '0',
                elsif interruption_roll = 2 then
                    -- interrupt with bypass frame
                    axistream_transmit(AXISTREAM_VVCT, command_channel,--@suppress
                        t_slv_array'(x"33", x"33"), "Interrupting with bypass frame 0x3333");
                end if;

                if next_frame.k2 then
                    if next_frame.decoded(4) = '1' then
                        log(ID_SEQUENCER_SUB, "Received register command START frame");
                        check_value(register_command_open, false, ERROR, "Received START frame, expected STOP frame");
                        register_command_open := true;
                    else
                        log(ID_SEQUENCER_SUB, "Received register command STOP frame");
                        check_value(register_command_open, true, ERROR, "Received STOP frame, expected START frame");
                        register_command_open := false;
                        commands_received := commands_received + 1;
                    end if;
                elsif next_frame.encoded = x"3333" then
                    -- bypass frames should never be inserted between start and stop frames
                    check_value(register_command_open, false, ERROR,
                        "Bypass frame inserted between START and STOP frame, corrupted register command");
                end if;

                dequeue_next_frame(expiration_time => 30 * C_CLK_PERIOD, error_on_timeout => false);
            end loop;
            check_value(register_command_open, false, ERROR, "Last register command was complete");
            check_value(commands_received > 0, true, ERROR, "Received at least one register command");
        end;


        variable frame_var          : t_decoder_record;
        variable hcc_id             : integer range 0 to 15;
    begin
        -------- Initialize the testbench -------------
        reset_lcb_configuration;
        await_uvvm_initialization(VOID);
        frame_queue.set_scope(C_TB_SCOPE_DEFAULT);
        start_clock(CLOCK_GENERATOR_VVCT, 1, "Start clock generator (BC clock)");--@suppress
        start_clock(CLOCK_GENERATOR_VVCT, 5, "Start clock generator (AXI clock)");--@suppress
        disable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        enable_log_msg(ID_SEQUENCER);
        enable_log_msg(ID_SEQUENCER_SUB);
        enable_log_msg(ID_UVVM_SEND_CMD);
        disable_log_msg(CLOCK_GENERATOR_VVCT, 1, ALL_MESSAGES);--@suppress
        disable_log_msg(CLOCK_GENERATOR_VVCT, 5, ALL_MESSAGES);--@suppress
        disable_log_msg(AXISTREAM_VVCT, config_channel, ALL_MESSAGES);--@suppress
        disable_log_msg(AXISTREAM_VVCT, trickle_channel, ALL_MESSAGES);--@suppress
        disable_log_msg(AXISTREAM_VVCT, command_channel, ALL_MESSAGES);--@suppress
        enable_log_msg(CLOCK_GENERATOR_VVCT, 1, ID_BFM);--@suppress
        enable_log_msg(CLOCK_GENERATOR_VVCT, 5, ID_BFM);--@suppress

        -- uncomment for extra verbose debug messages
        --enable_log_msg(ID_POS_ACK);
        --enable_log_msg(AXISTREAM_VVCT, config_channel, ID_BFM);
        --enable_log_msg(AXISTREAM_VVCT, command_channel, ID_BFM);

        -------- verify that the LCB module starts up and the frame decoder locks
        log(ID_SEQUENCER, "Waiting for LCB decoder to lock");
        skip_idles_i <= '0';
        reset <= '0';
        gen_pulse(decoder_rst, C_CYCLES_RST * C_CLK_PERIOD, "Reset frame decoder");
        gen_pulse(rst, C_CYCLES_RST * C_CLK_PERIOD, "Reset DUT");
        wait for 1000 ns;
        check_value(is_locked_o, '1', ERROR, "Frame decoder is locked after 1000 ns");
        check_value(frame_queue.is_empty(void), false, ERROR, "Decoder received some frames");
        frame_var    := frame_queue.fetch(void);
        check_value(frame_var.idle, '1', ERROR, "Decoder received some IDLE frames");
        skip_idles_i <= '1';
        frame_queue.flush(void);
        wait until rising_edge(clk);


        --the frame phase is not controlled in the encoder. It is set centrally
        --        -------- verify that LCB frame phase changes in accordance with the configuration
        --
        --        log(ID_LOG_HDR, "Verifing that LCB frame phase can be adjusted using elink configuration");
        --        log(ID_SEQUENCER, "Setting phase to 00");
        --        set_register(t_register_name'pos(L0A_FRAME_PHASE), x"0000");
        --        verify_frame_phase(3);
        --        log(ID_SEQUENCER, "Setting phase to 01");
        --        set_register(t_register_name'pos(L0A_FRAME_PHASE), x"0001");
        --        verify_frame_phase(2);
        --        log(ID_SEQUENCER, "Setting phase to 10");
        --        set_register(t_register_name'pos(L0A_FRAME_PHASE), x"0002");
        --        verify_frame_phase(1);
        --        log(ID_SEQUENCER, "Setting phase to 11");
        --        set_register(t_register_name'pos(L0A_FRAME_PHASE), x"0003");
        --        verify_frame_phase(0);
        --        log(ID_SEQUENCER, "Return phase to 00");
        --        set_register(t_register_name'pos(L0A_FRAME_PHASE), x"0000");
        --        gen_pulse(decoder_rst, C_CYCLES_RST * C_CLK_PERIOD, "Reset frame decoder");
        --        wait for 1000 ns;
        --        wait until rising_edge(clk);



        --the BCR delay is not controlled in the encoder. It is set centrally
        --        -------- verify that L0A BCR delay can be adjusted
        --        log(ID_LOG_HDR, "Verifying that L0A BCR delay can be adjusted using elink configuration");
        --        set_register(t_register_name'pos(L0A_FRAME_PHASE), x"0000");
        --        set_register(t_register_name'pos(TTC_L0A_ENABLE), x"0001");
        --        check_value(frame_phase_reg(1 downto 0), "00", "FRAME_PHASE is set to 00 for this test");--@suppress
        --        for i in 0 to max_bcr_delay loop
        --            set_register(t_register_name'pos(TTC_BCR_DELAY), std_logic_vector(to_unsigned(i, 16)));
        --            wait_num_rising_edge(clk, 50);
        --            send_L0A(bcr_i => '1', mask => "0000", tag => std_logic_vector(to_unsigned(i, 7)), randomize_tag => false);
        --            set_stopwatch;
        --            dequeue_next_frame(expiration_time => max_bcr_delay * C_CLK_PERIOD * 2);
        --            log(ID_SEQUENCER, "Stopwatch measures: " & to_string(stopwatch_p1));
        --            check_value(stopwatch_p1, 10 + ((i + 2) / 4) * 4, ERROR, "Stopwatch shows expected value");
        --            check_value(next_frame.decoded, "10000" & std_logic_vector(to_unsigned(i, 7)), ERROR, "Received expected L0A frame");
        --            reset_stopwatch;
        --            wait until rising_edge(clk);
        --        end loop;
        --        log(ID_SEQUENCER, "Setting BCR delay to 0");
        --        set_register(t_register_name'pos(TTC_BCR_DELAY), x"0000");

        enable_bcr <= '1';

        log(ID_LOG_HDR, "Verifying that overall L0A frame delay can be adjusted");
        set_register(t_register_name'pos(TTC_L0A_ENABLE), x"0001");
        wait_num_rising_edge(clk, 50);
        --check_value(frame_phase_reg(1 downto 0), "00", "FRAME_PHASE is set to 00 for this test");--@suppress --not used
        --check_value(bcr_delay_reg(11 downto 0), x"000", "L0A_BCR_DELAY is set to x000 for this test");--@suppress --not used
        for i in 0 to max_l0a_delay loop
            set_register(t_register_name'pos(L0A_FRAME_DELAY), std_logic_vector(to_unsigned(i, 16)));
            gen_pulse(decoder_rst, C_CYCLES_RST * C_CLK_PERIOD, "Reset frame decoder");
            await_value(is_locked_o, '1', 0 ns, 1 us,  ERROR, "Frame decoder is locked");
            --wait until rising_edge(bcr);
            --wait_num_rising_edge(clk, 3); -- BCR happened a while ago
            send_L0A( mask => "1111", tag => std_logic_vector(to_unsigned(i, 7)), randomize_tag => false); --bcr_i => '0',
            set_stopwatch;
            dequeue_next_frame(expiration_time => max_bcr_delay * C_CLK_PERIOD * 2);
            log(ID_SEQUENCER, "Stopwatch measures: " & to_string(stopwatch_p1));
            check_value(stopwatch_p1, 10 + i, ERROR, "Stopwatch shows expected value");
            check_value(next_frame.decoded, "01111" & std_logic_vector(to_unsigned(i, 7)), ERROR, "Received expected L0A frame");
            reset_stopwatch;
            wait until rising_edge(clk);
        end loop;
        log(ID_SEQUENCER, "Setting BCR delay to 0");
        set_register(t_register_name'pos(L0A_FRAME_DELAY), x"0000");
        gen_pulse(decoder_rst, C_CYCLES_RST * C_CLK_PERIOD, "Reset frame decoder");
        wait for 1000 ns;


        -------- verify that fast commands work
        enable_bcr <= '0';
        log(ID_LOG_HDR, "Verifying that elink commands work (using command decoder)");
        set_encoding(true);
        log(ID_SEQUENCER, "Sending and checking fast commands");
        fast_elink_data := elink_parser.parse_file("../samples/fast_command_inputs.txt",
                                                   "../samples/fast_command_outputs.txt");
        send_elink_commands_verify_response(fast_elink_data);

        -------- verify that elink L0A commands work
        log(ID_SEQUENCER, "Sending and checking L0A commands");
        l0a_elink_data := elink_parser.parse_file("../samples/elink_l0a_command_inputs.txt",
                                                  "../samples/elink_l0a_command_outputs.txt");
        send_elink_commands_verify_response(l0a_elink_data);

        -------- verify that register read/write commands work
        log(ID_SEQUENCER, "Sending and checking register read/write commands");
        register_elink_data := elink_parser.parse_file("../samples/register_command_inputs.txt",
                                                       "../samples/register_command_outputs.txt");
        send_elink_commands_verify_response(register_elink_data);

        -------- verify that register read/write commands can be masked
        if (CFG_INCLUDE_ASIC_MASKS = '1') then
            log(ID_LOG_HDR, "Verifying that register read/write commands are maskable");
            log(ID_SEQUENCER, "Verify that HCC* register r/w commands are skipped for "
                & "masked chips and not skipped for non-masked chips");
            mask_test_data := elink_parser.parse_file("../samples/hcc_mask_test_inputs.txt",
                                                      "../samples/hcc_mask_test_outputs.txt");
            for masked_hcc_id in 0 to 15 loop
                set_register(t_register_name'pos(HCC_MASK), std_logic_vector(to_unsigned(2 ** masked_hcc_id, 16)));
                log(ID_SEQUENCER, "Masking HCC* module " & to_string(masked_hcc_id)
                  & ", sending commands to every HCC* module and its ABC* submodules");
                for i in 0 to mask_test_data.inputs_count - 1 loop
                    hcc_id := i / 4;
                    send_elink_command(mask_test_data.inputs(i).data, mask_test_data.inputs(i).count);
                    if masked_hcc_id = hcc_id then
                        wait for 1000 ns;
                        assert_idle_only;
                    else
                        verify_elink_command_response(mask_test_data.outputs(i).data, mask_test_data.outputs(i).count);
                    end if;
                end loop;
            end loop;

            set_register(t_register_name'pos(HCC_MASK), x"0000");
            log(ID_LOG_HDR, "Verify that ABC* register r/w commands are skipped for "
                & "masked chips and not skipped for non-masked chips");
            set_encoding(true);
            set_abc_mask(hcc_id => 3, abc_id => 5);
            send_abc_reg_read(hcc_id => x"0", abc_id => x"5", addr => x"2B");
            verify_response_length(frame_count => 4);
            send_abc_reg_write(hcc_id => x"3", abc_id => x"4", addr => x"2B", data => x"DEADBEEF");
            verify_response_length(frame_count => 9);
            send_abc_reg_read(hcc_id => x"3", abc_id => x"5", addr => x"2B");
            assert_idle_only;
            send_abc_reg_write(hcc_id => x"3", abc_id => x"5", addr => x"2B", data => x"DEADBEEF");
            assert_idle_only;
            reset_lcb_configuration;
            reset_abc_hcc_mask;
        else
            log(ID_LOG_HDR, "No ASIC reg r/w masking in build - skipping: Verifying that register read/write commands are maskable.");
        end if;


        -------- verify that TTC L0A signals are ignored when disabled
        enable_bcr <= '1';
        log(ID_LOG_HDR, "Verifying response to TTC L0A signals");
        log(ID_SEQUENCER, "Verifying that TTC L0A frames can be disabled");
        set_register(t_register_name'pos(TTC_L0A_ENABLE), x"0000");
        wait_num_rising_edge(clk, 100);
        for i in 0 to repeat_samples - 1 loop
            send_random_L0A;
        end loop;
        wait for 1000 ns;
        assert_idle_only;


        -------- verify that TTC L0A signals are processed when enabled
        log(ID_LOG_HDR, "Enabling L0A frames and verifying them (one by one, random)");
        set_register(t_register_name'pos(TTC_L0A_ENABLE), x"0001");
        wait_num_rising_edge(clk, 50);
        for i in 0 to repeat_samples - 1 loop
            send_random_L0A;
            dequeue_next_frame;
            check_value(next_frame.decoded, frame_rnd, ERROR, "Received expected L0A frame");
        end loop;

        enable_bcr <= '0';

        log(ID_LOG_HDR, "Enabling L0A frames and verifying them (sequence)");
        ttc_l0a_data := ttc_l0a.parse_file("../samples/ttc_l0a_command_inputs.txt",
                                           "../samples/ttc_l0a_command_outputs.txt"
                                         );
        for i in 0 to ttc_l0a_data.count - 1 loop
            send_L0A( ttc_l0a_data.inputs(i).mask, ttc_l0a_data.inputs(i).tag); --ttc_l0a_data.inputs(i).bcr,
            --end loop;
            --for i in 0 to ttc_l0a_data.count - 1 loop
            dequeue_next_frame;
            --check_value(next_frame.encoded, ttc_l0a_data.outputs(i), ERROR, "Received expected L0A frame");
            frame_rnd := frame_rnd(11) & next_frame.decoded(10 downto 0);
            frame_sent := frame_rnd(11) &  ttc_l0a_data.inputs(i).mask & ttc_l0a_data.inputs(i).tag;
            check_value(frame_rnd, frame_sent, ERROR, "Received expected L0A frame");
        end loop;

        enable_bcr <= '1';
        -------- verify bypass frames come through
        log(ID_LOG_HDR, "Verifying that bypass frames are correctly transmitted");
        set_encoding(false);
        bypass_data                     := bypass.parse_file("../samples/r3l1_command_outputs.txt");
        for i in 0 to bypass_data.count - 1 loop
            -- schedule transmission of all bypass data
            send_bypass_frame(bypass_data.data(i));
        end loop;
        for i in 0 to bypass_data.count - 1 loop -- receive and verify all data
            dequeue_next_frame;
            check_value(next_frame.encoded, bypass_data.data(i), ERROR, "Received expected bypass frame");
        end loop;


        -------- verify that trickle configuration can be written and read out
        log(ID_LOG_HDR, "Verifying that trickle configuration data can be written and read out");
        trickle_test_data_1 := elink_parser.parse_file("../samples/trickle_config_inputs_1.txt",
                                                       "../samples/trickle_config_outputs_1.txt");
        set_register(t_register_name'pos(TRICKLE_DATA_START), x"0000");
        set_register(t_register_name'pos(TRICKLE_DATA_END),
            std_logic_vector(to_unsigned(trickle_test_data_1.bytes_total, 16)));
        set_register(t_register_name'pos(TRICKLE_WRITE_ADDR), x"0000");
        set_register(t_register_name'pos(TRICKLE_SET_WRITE_ADDR_PULSE), x"0001");
        wait_num_rising_edge(clk, 50);
        send_elink_commands(trickle_test_data_1, channel => trickle_channel);
        await_completion(AXISTREAM_VVCT, trickle_channel, trickle_test_data_1.bytes_total * 5 * C_CLK_PERIOD,--@suppress
            "Waiting for the trickle configuration memory to be written");
        wait_num_rising_edge(clk, 100); -- wait for the trickle memory to become ready
        issue_software_trickle_trigger;
        verify_elink_command_responses(trickle_test_data_1);


        -------- check that global trickle trigger works
        log(ID_LOG_HDR, "Verifying that trickle configuration is read when global trickle trigger command is issued");
        config.GLOBAL_TRICKLE_TRIGGER <= (others => '1');
        wait until rising_edge(clk);
        config.GLOBAL_TRICKLE_TRIGGER <= (others => '0');
        verify_elink_command_responses(trickle_test_data_1);


        enable_bcr <= '0';
        -------- verify that trickle configuration memory can store multiple sequences and switch between them
        log(ID_LOG_HDR, "Verifying that multiple trickle configuration samples can be stored in memory");
        trickle_test_data_2 := elink_parser.parse_file("../samples/trickle_config_inputs_2.txt",
                                                       "../samples/trickle_config_outputs_2.txt");
        set_register(t_register_name'pos(TRICKLE_WRITE_ADDR),std_logic_vector(
            to_unsigned(1 + trickle_test_data_1.bytes_total, 16)));
        set_register(t_register_name'pos(TRICKLE_SET_WRITE_ADDR_PULSE), x"0001");
        wait_num_rising_edge(clk, 50);
        send_elink_commands(trickle_test_data_2, channel => trickle_channel);
        await_completion(AXISTREAM_VVCT, trickle_channel, trickle_test_data_2.inputs_count * 10 * C_CLK_PERIOD,--@suppress
            "Waiting for the trickle configuration memory to be written");
        wait_num_rising_edge(clk, 100); -- wait for the trickle memory to become ready
        issue_software_trickle_trigger;
        verify_elink_command_responses(trickle_test_data_1);
        set_register(t_register_name'pos(TRICKLE_DATA_START),
            std_logic_vector(to_unsigned(1 + trickle_test_data_1.bytes_total, 16)));
        set_register(t_register_name'pos(TRICKLE_DATA_END),
            std_logic_vector(to_unsigned(1 + trickle_test_data_1.bytes_total + trickle_test_data_2.bytes_total, 16)));
        issue_software_trickle_trigger;
        verify_elink_command_responses(trickle_test_data_2);


        ------ verify that IDLE frames can be stored in trickle configuration memory
        log(ID_LOG_HDR, "Sending a sample calibration sequence");
        calibration_data := elink_parser.parse_file("../samples/trickle_calibration_inputs.txt",
                                                    "../samples/trickle_calibration_outputs.txt");
        set_register(t_register_name'pos(TRICKLE_DATA_START), x"0000");
        set_register(t_register_name'pos(TRICKLE_DATA_END),
            std_logic_vector(to_unsigned(calibration_data.bytes_total, 16)));
        set_register(t_register_name'pos(TRICKLE_WRITE_ADDR), x"0000");
        set_register(t_register_name'pos(TRICKLE_SET_WRITE_ADDR_PULSE), x"0001");
        if (CFG_INCLUDE_BCID_GATING = '0') then
            log(ID_SEQUENCER, "Blocking trickle sequence with gating signal to let thel command decoder catch up");
            set_register(t_register_name'pos(GATING_BC_START), x"0000");
            set_register(t_register_name'pos(GATING_BC_STOP), x"0000");
            set_register(t_register_name'pos(GATING_TTC_ENABLE), x"0000");
        else
            log(ID_SEQUENCER, "No gating in build - using repurposed gating enable to directly block trickle sequence to let the command decoder catch up");
        end if;
        config.GLOBAL_STRIPS_CONFIG.TTC_GENERATE_GATING_ENABLE <= (others => '1');
        wait_num_rising_edge(clk, 50);
        send_elink_commands(calibration_data, channel => trickle_channel);
        await_completion(AXISTREAM_VVCT, trickle_channel, calibration_data.inputs_count * 10 * C_CLK_PERIOD,--@suppress
            "Waiting for the trickle configuration memory to be written");
        wait_num_rising_edge(clk, 100); -- wait for the trickle memory to become ready
        issue_software_trickle_trigger;
        wait_num_rising_edge(clk, 100); -- wait for the trickle configuration frames to queue up
        log(ID_SEQUENCER, "Disabling gating, allowing low-priority frames");
        config.GLOBAL_STRIPS_CONFIG.TTC_GENERATE_GATING_ENABLE <= (others => '0');
        skip_idles_i <= '0';
        wait until rising_edge(clk);
        verify_elink_command_responses(calibration_data);
        skip_idles_i <= '1';
        frame_queue.flush(void);
        wait until rising_edge(clk);


        -- priority checks
        ----- TTC frames take precedence over bypass frames
        log(ID_LOG_HDR, "Verifying that TTC L0A frames take priority over bypass frames");
        set_register(t_register_name'pos(TTC_L0A_ENABLE), x"0001");
        set_encoding(false);
        for i in 0 to low_priority_frame_repetitions - 1 loop -- schedule transmission of all bypass data
            axistream_transmit(AXISTREAM_VVCT, command_channel, t_slv_array'(x"33", x"33"), "Repeating bypass frame 0x3333");--@suppress
        end loop;
        wait_num_rising_edge(clk, 20);
        send_L0A( "0111", "1101110"); --'0',
        check_if_low_priority_sequence_was_interrupted(
            LP_frame => x"3333",
            HP_frame => "0000001111101110",
            LP_frame_count => low_priority_frame_repetitions,
            is_LP_frame_encoded => true,
            is_HP_frame_encoded => false);


        ----- TTC frames take precedence over decoded elink commands
        log(ID_LOG_HDR, "Verifying that TTC L0A frames take priority over command decoder frames");
        set_register(t_register_name'pos(TTC_L0A_ENABLE), x"0001");
        set_encoding(true);
        for i in 0 to low_priority_frame_repetitions - 1 loop -- schedule transmission of all bypass data
            axistream_transmit(AXISTREAM_VVCT, command_channel, t_slv_array'(x"81", x"3B"), "Repeating low-priority frame 0x6A5A");--@suppress
        end loop;
        wait_num_rising_edge(clk, 20);
        send_L0A( "0111", "1101110"); --'0',
        check_if_low_priority_sequence_was_interrupted(
            LP_frame => x"6A5A",
            HP_frame => "0000001111101110",
            LP_frame_count => low_priority_frame_repetitions,
            is_LP_frame_encoded => true,
            is_HP_frame_encoded => false);


        ----- TTC frames take precedence over trickle configuration commands
        log(ID_LOG_HDR, "Verifying that TTC L0A frames take priority over trickle configuration");
        set_register(t_register_name'pos(TTC_L0A_ENABLE), x"0001");
        set_register(t_register_name'pos(TRICKLE_DATA_START), x"0000");
        set_register(t_register_name'pos(TRICKLE_DATA_END), std_logic_vector(
            to_unsigned(2 * low_priority_frame_repetitions, 16)));
        set_register(t_register_name'pos(TRICKLE_WRITE_ADDR), x"0000");
        set_register(t_register_name'pos(TRICKLE_SET_WRITE_ADDR_PULSE), x"0001");
        wait_num_rising_edge(clk, 50);
        for i in 0 to low_priority_frame_repetitions - 1 loop -- schedule transmission of all bypass data
            axistream_transmit(AXISTREAM_VVCT, trickle_channel, t_slv_array'(x"81", x"3B"),--@suppress
                "Repeating low-priority frame 0x6A5A (trickle configuration memory)"
            );
        end loop;
        await_completion(AXISTREAM_VVCT, trickle_channel, low_priority_frame_repetitions * 10 * C_CLK_PERIOD,--@suppress
            "Waiting for the trickle configuration memory to be written");
        wait_num_rising_edge(clk, 10); -- wait for the trickle memory to become ready
        issue_software_trickle_trigger;
        wait_num_rising_edge(clk, 20);
        send_L0A( "0111", "1101110"); --'0',
        check_if_low_priority_sequence_was_interrupted(
            LP_frame => x"6A5A",
            HP_frame => "0000001111101110",
            LP_frame_count => low_priority_frame_repetitions,
            is_LP_frame_encoded => true,
            is_HP_frame_encoded => false);


        ----- bypass frames take precedence over trickle configuration memory
        log(ID_LOG_HDR, "Verifying that bypass frames take priority over trickle configuration");
        set_encoding(false);
        issue_software_trickle_trigger;
        wait_num_rising_edge(clk, 20);
        axistream_transmit(AXISTREAM_VVCT, command_channel, t_slv_array'(x"33", x"33"), "Sending bypass frame 0x3333");--@suppress
        check_if_low_priority_sequence_was_interrupted(
            LP_frame => x"6A5A",
            HP_frame => x"3333",
            LP_frame_count => low_priority_frame_repetitions,
            is_LP_frame_encoded => true,
            is_HP_frame_encoded => true);


        ----- decoded elink commands take precedence over trickle configuration memory
        log(ID_LOG_HDR, "Verifying that command decoder frames take priority over trickle configuration");
        set_encoding(true);
        issue_software_trickle_trigger;
        axistream_transmit(AXISTREAM_VVCT, command_channel, t_slv_array'(x"81", x"1E"), "Sending fast command frame 0x6ACA");--@suppress
        check_if_low_priority_sequence_was_interrupted(
            LP_frame => x"6A5A",
            HP_frame => x"6A1E",
            LP_frame_count => low_priority_frame_repetitions,
            is_LP_frame_encoded => true,
            is_HP_frame_encoded => true);


        if (CFG_INCLUDE_BCID_GATING = '1') then
            -- low-priority frame gating
            ----- bypass frames respect gating signals
            enable_bcr <= '1';

            log(ID_LOG_HDR, "Verifying that bypass frames respect gating signal");
            set_encoding(false);
            gating_test_frames := 40;
            bc_start := 176;
            bc_stop := bc_start + (gating_test_frames / 2)*4;
            shared_axistream_vvc_config(command_channel).bfm_config.max_wait_cycles := bc_stop + 20;
            set_register(t_register_name'pos(TTC_L0A_ENABLE), x"0001");
            set_register(t_register_name'pos(GATING_BC_START), std_logic_vector(to_unsigned(bc_start, 16)));
            set_register(t_register_name'pos(GATING_BC_STOP), std_logic_vector(to_unsigned(bc_stop, 16)));
            set_register(t_register_name'pos(GATING_TTC_ENABLE), x"0001");
            wait_num_rising_edge(clk, 50);
            for i in 0 to gating_test_frames - 1 loop -- schedule transmission of all bypass data
                axistream_transmit(AXISTREAM_VVCT, command_channel, t_slv_array'(x"33", x"33"), "Scheduling bypass frame 0x3333");--@suppress
            end loop;
            verify_bc_gating(expected_frame => x"3333");
        else
            log(ID_LOG_HDR, "No gating in build - skipping: Verifying that bypass frames respect gating signal");
        end if;


        if (CFG_INCLUDE_BCID_GATING = '1') then
            ----- decoded elink commands respect gating signals
            log(ID_LOG_HDR, "Verifying that decoded frames respect gating signal");
            set_encoding(true);
            gating_test_frames := 60;
            bc_start := 631;
            bc_stop := bc_start + (gating_test_frames / 2)*4;
            shared_axistream_vvc_config(command_channel).bfm_config.max_wait_cycles := bc_stop + 20;
            set_register(t_register_name'pos(TTC_L0A_ENABLE), x"0001");
            set_register(t_register_name'pos(GATING_BC_START), std_logic_vector(to_unsigned(bc_start, 16)));
            set_register(t_register_name'pos(GATING_BC_STOP), std_logic_vector(to_unsigned(bc_stop, 16)));
            set_register(t_register_name'pos(GATING_TTC_ENABLE), x"0001");
            wait_num_rising_edge(clk, 50);
            for i in 0 to gating_test_frames - 1 loop -- schedule transmission of all bypass data
                axistream_transmit(AXISTREAM_VVCT, command_channel, t_slv_array'(x"81", x"3A"), "Scheduling fast command");--@suppress
            end loop;
            verify_bc_gating(expected_frame => x"6A3A");
        else
            log(ID_LOG_HDR, "No gating in build - skipping: Verifying that decoded frames respect gating signal");
        end if;


        if (CFG_INCLUDE_BCID_GATING = '1') then
            --- trickle configuration commands respect gating signals
            log(ID_LOG_HDR, "Verifying that trickle configuration respects gating signal");
            gating_test_frames := 10;
            bc_start := 65;
            bc_stop := bc_start + (gating_test_frames / 2)*4;
            shared_axistream_vvc_config(trickle_channel).bfm_config.max_wait_cycles := bc_stop + 20;
            set_register(t_register_name'pos(TTC_L0A_ENABLE), x"0001");
            set_register(t_register_name'pos(GATING_BC_START), std_logic_vector(to_unsigned(bc_start, 16)));
            set_register(t_register_name'pos(GATING_BC_STOP), std_logic_vector(to_unsigned(bc_stop, 16)));
            set_register(t_register_name'pos(GATING_TTC_ENABLE), x"0001");
            set_register(t_register_name'pos(TRICKLE_TRIGGER_RUN), x"0000");
            set_register(t_register_name'pos(TRICKLE_DATA_START), x"0000");
            set_register(t_register_name'pos(TRICKLE_DATA_END), std_logic_vector(to_unsigned(gating_test_frames, 16)));
            set_register(t_register_name'pos(TRICKLE_WRITE_ADDR), x"0000");
            set_register(t_register_name'pos(TRICKLE_SET_WRITE_ADDR_PULSE), x"0001");
            wait_num_rising_edge(clk, 50);
            for i in 0 to gating_test_frames - 1 loop -- schedule transmission of all bypass data
                axistream_transmit(AXISTREAM_VVCT, trickle_channel, t_slv_array'(x"81", x"18"),--@suppress
                "Writing fast command to trickle configuration memory");
            end loop;
            await_completion(AXISTREAM_VVCT, trickle_channel, gating_test_frames * 2 * 5 * C_CLK_PERIOD,--@suppress
            "Waiting for the trickle configuration memory to be written");
            set_register(t_register_name'pos(TRICKLE_TRIGGER_RUN), x"0001");
            verify_bc_gating(expected_frame => x"6AD8");
            -- clean up after using RUN
            set_register(t_register_name'pos(TRICKLE_TRIGGER_RUN), x"0000");
            set_register(t_register_name'pos(GATING_TTC_ENABLE), x"0000");
            await_value(trickle_axi.tready, '1', 0 ns, gating_test_frames * 10 * C_CLK_PERIOD, "Waiting for the trickle configuration to complete (1/2)");--@suppress
            await_value(frame.idle, '1', 0 ns, 5000 * C_CLK_PERIOD, "Waiting for the trickle configuration to complete (2/2)");--@suppress
            frame_queue.flush(void);
            wait for 800 ns;
            wait until rising_edge(clk);
        --frame_queue.reset(VOID);
        else
            log(ID_LOG_HDR, "No gating in build - skipping: Verifying that trickle configuration respects gating signal");
        end if;


        if (CFG_INCLUDE_BCID_GATING = '1') then
            ----- gating guard interval is respected = only complete register read/write command are sent
            log(ID_LOG_HDR, "Register guard interval test: register read/write commands are not interrupted"
            & " by bypass frames and complete when read out of trickle configuration memory");
            register_elink_data := elink_parser.parse_file("../samples/register_command_inputs.txt",
                                                           "../samples/register_command_outputs.txt");
            shared_axistream_vvc_config(trickle_channel).bfm_config.max_wait_cycles := 1000;
            set_encoding(false);
            set_register(t_register_name'pos(TTC_L0A_ENABLE), x"0001");
            set_register(t_register_name'pos(TRICKLE_TRIGGER_RUN), x"0000");
            set_register(t_register_name'pos(GATING_TTC_ENABLE), x"0000");
            set_register(t_register_name'pos(TRICKLE_DATA_START), x"0000");
            set_register(t_register_name'pos(TRICKLE_DATA_END),
            std_logic_vector(to_unsigned(register_elink_data.bytes_total, 16)));
            set_register(t_register_name'pos(TRICKLE_WRITE_ADDR), x"0000");
            set_register(t_register_name'pos(TRICKLE_SET_WRITE_ADDR_PULSE), x"0001");
            wait_num_rising_edge(clk, 50);
            send_elink_commands(register_elink_data, channel => trickle_channel);
            await_completion(AXISTREAM_VVCT, trickle_channel, register_elink_data.bytes_total * 5 * C_CLK_PERIOD,--@suppress
            "Waiting for the trickle configuration memory to be written");
            wait_num_rising_edge(clk, 150);
            -- configure the trickle config + gating
            for i in guard_window_size + 2 to guard_window_size + 52 loop
                bc_start := random(13, 20);
                bc_stop := bc_start + i*3;
                log(ID_SEQUENCER, "Checking frame integrity with bc_start = " & to_string(bc_start) & ", bc_stop = " & to_string(bc_stop));
                set_register(t_register_name'pos(GATING_BC_START), std_logic_vector(to_unsigned(bc_start, 16)), timeout_clk_periods => 5000);
                set_register(t_register_name'pos(GATING_BC_STOP), std_logic_vector(to_unsigned(bc_stop, 16)), timeout_clk_periods => 5000);
                set_register(t_register_name'pos(GATING_TTC_ENABLE), x"0001", timeout_clk_periods => 5000);
                set_register(t_register_name'pos(TRICKLE_TRIGGER_RUN), x"0001", timeout_clk_periods => 5000);
                wait_num_rising_edge(clk, 50);
                verify_register_frame_completion; --needs BCR
                --wait_num_rising_edge(clk, guard_window_size + 10);
                set_register(t_register_name'pos(GATING_TTC_ENABLE), x"0000", timeout_clk_periods => 5000);
                set_register(t_register_name'pos(TRICKLE_TRIGGER_RUN), x"0000", timeout_clk_periods => 5000);
                wait_num_rising_edge(clk, 30);
                await_value(frame.idle, '1', 0 ns, 5000 * C_CLK_PERIOD, "Waiting for the trickle configuration to flush out");  --@suppress
                frame_queue.flush(void);
                --frame_queue.reset(VOID);
                wait for 1500 ns;
                wait until rising_edge(clk);
            end loop;
        else
            log(ID_LOG_HDR, "No gating in build - skipping: Register guard interval test: register read/write commands are not interrupted");
        end if;



        wait for 1000 ns;
        report_alert_counters(FINAL);   -- Report final counters and print conclusion for simulation (Success/Fail)
        uvvm_completed <= '1';
        wait for 1 us;
        stop_clock(CLOCK_GENERATOR_VVCT, 1, "Stop clock generator (BC clk)");--@suppress
        stop_clock(CLOCK_GENERATOR_VVCT, 5, "Stop clock generator (AXI clk)");--@suppress
        wait;
    end process;

    --generate frame_phase and bcr
    --bcr is a clock cycle ahead of the nearest frame_phase pulse
    process(clk)
        variable bcid : integer range 0 to 3563:=0; -- bunch counter
        variable bcid_vec : std_logic_vector(11 downto 0);
    begin
        if rising_edge(clk) then
            bcr <= '0';
            sync_i <= '0';


            if(bcid=3563) then
                bcid:=0;
            else
                bcid:=bcid+1;
            end if;

            if(bcid=0 and enable_bcr = '1') then
                bcr <= '1';
            end if;

            bcr_expected <= bcr;

            bcid_vec := std_logic_vector(to_unsigned(bcid, 12));
            if( bcid_vec(1 downto 0) = "01") then
                sync_i <= '1';
            end if;

        end if;
    end process;

    -----------------------------------------------------------------------------
    -- Instantiate UVVM entities
    -----------------------------------------------------------------------------
    i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

    i_clock_generator_vvc : entity bitvis_vip_clock_generator.clock_generator_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_INSTANCE_IDX    => 1,
            GC_CLOCK_NAME      => "BC Clock",
            GC_CLOCK_PERIOD    => C_CLK_PERIOD,
            GC_CLOCK_HIGH_TIME => C_CLK_PERIOD / 2
        )
        port map(
            clk => clk
        );

    i_command : entity bitvis_vip_axistream.axistream_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_AXISTREAM_BFM_CONFIG, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => true,
            GC_DATA_WIDTH    => C_AXI_DATA_WIDTH,
            GC_USER_WIDTH    => C_USER_WIDTH,
            GC_ID_WIDTH      => C_ID_WIDTH,
            GC_DEST_WIDTH    => C_DEST_WIDTH,
            GC_INSTANCE_IDX  => config_channel)
        port map(
            clk              => s_axis_aclk,
            axistream_vvc_if => config_axi);--@suppress

    i_trickle : entity bitvis_vip_axistream.axistream_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_AXISTREAM_BFM_CONFIG, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => true,
            GC_DATA_WIDTH    => C_AXI_DATA_WIDTH,
            GC_USER_WIDTH    => C_USER_WIDTH,
            GC_ID_WIDTH      => C_ID_WIDTH,
            GC_DEST_WIDTH    => C_DEST_WIDTH,
            GC_INSTANCE_IDX  => trickle_channel)
        port map(
            clk              => s_axis_aclk,
            axistream_vvc_if => trickle_axi);--@suppress

    i_bypass : entity bitvis_vip_axistream.axistream_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_AXISTREAM_BFM_CONFIG, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => true,
            GC_DATA_WIDTH    => C_AXI_DATA_WIDTH,
            GC_USER_WIDTH    => C_USER_WIDTH,
            GC_ID_WIDTH      => C_ID_WIDTH,
            GC_DEST_WIDTH    => C_DEST_WIDTH,
            GC_INSTANCE_IDX  => command_channel)
        port map(
            clk              => s_axis_aclk,
            axistream_vvc_if => command_axi);--@suppress

    i_axi_clock_generator_vvc : entity bitvis_vip_clock_generator.clock_generator_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_INSTANCE_IDX    => 5,
            GC_CLOCK_NAME      => "AXI clock",
            GC_CLOCK_PERIOD    => C_AXI_CLK_PERIOD,
            GC_CLOCK_HIGH_TIME => C_AXI_CLK_PERIOD / 2
        )
        port map(
            clk => s_axis_aclk
        );

    -- Adds decoded valid frames into the queue
    p_queue : process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' then
                frame_queue.reset(VOID);
            else
                if frame_valid = '1' then
                    frame_queue.add(frame);
                end if;
            end if;
        end if;
    end process;

end architecture RTL;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use std.env.stop;

    use work.strips_package.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

library uvvm_vvc_framework;
    use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

library bitvis_vip_axistream;
    context bitvis_vip_axistream.vvc_context;

library bitvis_vip_clock_generator;
    context bitvis_vip_clock_generator.vvc_context;

entity tb_playback_controller is
    generic(
        use_vunit : boolean := false        -- @suppress "Unused generic: use_vunit is not used in work.tb_playback_controller(RTL)"
    );
    port(
        uvvm_completed : out std_logic := '0'
    );
end entity tb_playback_controller;

architecture RTL of tb_playback_controller is
    signal clk              : std_logic;         -- 40 MHz BC clock
    signal rst              : std_logic;
    signal start_pulse_i    : std_logic := '0';  -- toggle this input to initiate the readout
    signal readout_active_o : std_logic;  -- '1' when the readout is in progress

    subtype t_data is std_logic_vector(playback_controller_data'range);  -- 8 bit wide
    subtype t_addr is std_logic_vector(playback_controller_address'range);
    type t_data_array is array (natural range <>) of t_data;

    -- data to be saved to the BRAM
    --signal data_i                 : t_data    := (others => '0');
    -- pulse this signal to write a byte into the memory and advance write_addr by 1
    --signal write_pulse_i          : std_logic := '0';
    -- pulse this signal to load the write_addr of bram from write_addr_start_i
    signal set_write_addr_pulse_i : std_logic := '0';
    -- the memory address that write_ptr of the BRAM will be placed to
    -- when set_write_addr_pulse_i is pulsed
    signal write_addr_start_i     : t_addr    := (others => '0');
    -- the memory address playback should be started from
    signal read_addr_start_i      : t_addr    := (others => '0');
    -- the memory address of the last byte to be read out
    signal read_addr_stop_i       : t_addr    := (others => '0');

    -- output interface
    signal data_o  : std_logic_vector(playback_controller_data'range);  -- data read out from BRAM
    signal valid_o : std_logic;                                         -- indicates data_o is valid this clk cycle
    signal ready_i : std_logic := '0';                                  -- the receiving side indicates it's ready for more data


    constant C_CLK_PERIOD : time    := 25 ns;
    constant C_CYCLES_RST : integer := 3;
    --constant C_STABLE_PERIODS      : integer := 10;
    --constant C_MAX_CYCLES_PER_BYTE : integer := 4;
    constant C_USER_WIDTH : integer := 1;
    constant C_ID_WIDTH   : integer := 1;
    constant C_DEST_WIDTH : integer := 1;
    constant C_DATA_WIDTH : integer := playback_controller_data'length;
    --constant C_FRAME_WIDTH         : integer := 16;

    -- input interface
    signal data_in : t_axistream_if(tdata(C_DATA_WIDTH - 1 downto 0),  --@suppress
                                  tkeep((C_DATA_WIDTH / 8) - 1 downto 0),
                                  tuser(C_USER_WIDTH - 1 downto 0),
                                  tstrb((C_DATA_WIDTH / 8) - 1 downto 0),
                                  tid(C_ID_WIDTH - 1 downto 0),
                                  tdest(C_DEST_WIDTH - 1 downto 0)
                                  );

    constant max_data_legth : natural := 256;

    constant zero_address : t_addr := (others => '0');
    constant zero_data    : t_data := (others => '0');

    -- shared variables to control readout verification
    shared variable data_expected           : t_data_array(0 to 1024) := (others => zero_data);
    shared variable data_expected_count     : natural                 := 0;
    shared variable read_delay_clk_cycles   : natural                 := 0;
    signal verify_data_en, verify_data_done : boolean                 := false;
    signal verify_data_ack                  : std_logic               := '0';

    -- count number of valid words presented by the FIFO
    signal valid_word_count     : natural   := 0;
    signal valid_word_count_rst : std_logic := '0';

    signal playback_loops_i       : std_logic_vector(15 downto 0) := x"0000";
    shared variable loops_prime  : natural                 := 0;

begin

    p_main : process is
        variable random_data : t_data_array(0 to max_data_legth - 1);

        -- prapares an array of random data to write to trickle configuration
        procedure make_random_data(
            length : positive := 32
        ) is
        begin
            for i in 0 to length - 1 loop
                random_data(i) := random(random_data(i)'length);
            end loop;
        end;

        -- initiate the readout
        procedure start_readout is
        begin
            start_pulse_i <= '1';
            wait until rising_edge(clk);
            start_pulse_i <= '0';
        end;

        -- verifies the data read back from the trickle configuration
        procedure verify_data(
            data_exp         : t_data_array;
            delay_clk_cycles : natural := 0
        ) is
        begin
            -- Schedule the expected data verification
            data_expected(data_exp'range) := data_exp;
            data_expected_count           := data_exp'length;
            read_delay_clk_cycles         := delay_clk_cycles;
            verify_data_en                <= true;
            start_readout;
            await_value(verify_data_done, true, 0 ns,
                  (3 + delay_clk_cycles) * data_expected_count * C_CLK_PERIOD * loops_prime,
                  ERROR, "Waiting for the data verification to complete");

            verify_data_en  <= false;
            verify_data_ack <= '1';
            wait until rising_edge(clk);
            verify_data_ack <= '0';
        end;

        -- set RST to a given value and wait C_CYCLES_RST cycles
        procedure set_rst(
            value    : std_logic;
            duration : positive := C_CYCLES_RST
        ) is
        begin
            wait until rising_edge(clk);
            rst <= value;
            wait_num_rising_edge(clk, duration);
        end;

        -- set loop counter
        procedure set_loop_counter(
            loops    : natural
        ) is
        begin
            playback_loops_i <= std_logic_vector(to_unsigned(loops, 16));
            loops_prime := 1 when (loops = 0) else loops;
        end;

        -- moves the BRAM write pointer to addr
        procedure set_write_pointer(
            addr : t_addr
        ) is
        begin
            wait until rising_edge(clk);
            check_value(readout_active_o, '0', ERROR, "readout_active_o must be 0 when write pointer is updated");
            write_addr_start_i     <= addr;
            set_write_addr_pulse_i <= '1';
            wait until rising_edge(clk);
            set_write_addr_pulse_i <= '0';
        end;

        -- writes one byte to the trickle configuration buffer
        procedure write_byte(
            data             : t_data;
            delay_clk_cycles : natural := 0
        ) is
            variable delay : time;
        begin
            axistream_transmit(AXISTREAM_VVCT, 2, std_logic_vector'(data),  --@suppress
                         "Writing " & to_hstring(data));
            if delay_clk_cycles > 0 then
                delay := delay_clk_cycles * C_CLK_PERIOD;
                insert_delay(AXISTREAM_VVCT, 2, delay, "Inserting " &         --@suppress
                     to_string(delay) & " delay", C_SCOPE);
            end if;
        end;

        -- sets the BRAM readout start and end pointers
        procedure set_read_pointers(
            start_addr, stop_addr : t_addr
        ) is
        begin
            read_addr_start_i <= start_addr;
            read_addr_stop_i  <= stop_addr;
        end;

        -- writes an array of bytes to the trickle configuration buffer
        procedure write_array(
            data             : t_data_array;
            start_addr       : t_addr  := zero_address;
            delay_clk_cycles : natural := 0
        ) is
            variable stop_addr : t_addr;
        begin
            set_write_pointer(start_addr);
            for i in data'range loop
                write_byte(data(i), delay_clk_cycles);
            end loop;
            stop_addr := std_logic_vector(unsigned(start_addr) + to_unsigned(data'length, start_addr'length));
            set_read_pointers(start_addr, stop_addr);
        end;

        -- enqueue and verify random data
        procedure write_and_verify(
            byte_count             : positive := 32;
            read_delay_clk_cycles  : natural  := 0;
            write_delay_clk_cycles : natural  := 0
        ) is
            variable data : t_data_array(0 to byte_count - 1);
        begin
            make_random_data(byte_count);
            data := random_data(0 to byte_count - 1);
            write_array(data => data, delay_clk_cycles => write_delay_clk_cycles);
            await_completion(AXISTREAM_VVCT, 2, byte_count * (5 + write_delay_clk_cycles) * C_CLK_PERIOD,  --@suppress
                       "Waiting for the transmission to finish");
            verify_data(data_exp => data, delay_clk_cycles => read_delay_clk_cycles);
        end;

    ------------------------------------------------
    -- Main test process
    ------------------------------------------------
    begin
        await_uvvm_initialization(VOID);
        start_clock(CLOCK_GENERATOR_VVCT, 1, "Start clock generator");  --@suppress

        disable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        enable_log_msg(ID_SEQUENCER);
        disable_log_msg(CLOCK_GENERATOR_VVCT, 1, ALL_MESSAGES);  --@suppress
        disable_log_msg(AXISTREAM_VVCT, 2, ALL_MESSAGES);        --@suppress

        log(ID_LOG_HDR, "Checking behavior when rst = 1", C_SCOPE);
        set_rst('1');
        check_value(readout_active_o, '0', ERROR, "readout_active_o = 0 when rst = 1");
        check_value(data_o, zero_data, ERROR, "data_o = 0 when rst = 1");
        check_value(valid_o, '0', ERROR, "valid_o = 0 when rst = 1");
        check_value(data_in.tready, '1', ERROR, "tready = 1 when rst = 1");
        set_rst('0');

        set_loop_counter(0); -- Note 0 is translated to 1 to allow resetting reg to zero
        log(ID_LOG_HDR, "Writing some data to trickle configuration (fast) and reading it back (fast), 0 loops", C_SCOPE);
        write_and_verify(write_delay_clk_cycles => 0, read_delay_clk_cycles => 0);

        log(ID_LOG_HDR, "Writing some data to trickle configuration (slow) and reading it back (fast), 0 loops", C_SCOPE);
        write_and_verify(write_delay_clk_cycles => 10, read_delay_clk_cycles => 0);

        log(ID_LOG_HDR, "Writing some data to trickle configuration (fast) and reading it back (slow), 0 loops", C_SCOPE);
        write_and_verify(write_delay_clk_cycles => 0, read_delay_clk_cycles => 10);

        log(ID_LOG_HDR, "Writing some data to trickle configuration (slow) and reading it back (slow), 0 loops", C_SCOPE);
        write_and_verify(write_delay_clk_cycles => 10, read_delay_clk_cycles => 10);

        set_loop_counter(1);
        log(ID_LOG_HDR, "Writing some data to trickle configuration (fast) and reading it back (fast), 1 loops", C_SCOPE);
        write_and_verify(write_delay_clk_cycles => 0, read_delay_clk_cycles => 0);

        log(ID_LOG_HDR, "Writing some data to trickle configuration (slow) and reading it back (fast), 1 loops", C_SCOPE);
        write_and_verify(write_delay_clk_cycles => 10, read_delay_clk_cycles => 0);

        log(ID_LOG_HDR, "Writing some data to trickle configuration (fast) and reading it back (slow), 1 loops", C_SCOPE);
        write_and_verify(write_delay_clk_cycles => 0, read_delay_clk_cycles => 10);

        log(ID_LOG_HDR, "Writing some data to trickle configuration (slow) and reading it back (slow), 1 loops", C_SCOPE);
        write_and_verify(write_delay_clk_cycles => 10, read_delay_clk_cycles => 10);

        set_loop_counter(10);
        log(ID_LOG_HDR, "Writing some data to trickle configuration (fast) and reading it back (fast), 10 loops", C_SCOPE);
        write_and_verify(write_delay_clk_cycles => 0, read_delay_clk_cycles => 0);

        log(ID_LOG_HDR, "Writing some data to trickle configuration (slow) and reading it back (fast), 10 loops", C_SCOPE);
        write_and_verify(write_delay_clk_cycles => 10, read_delay_clk_cycles => 0);

        log(ID_LOG_HDR, "Writing some data to trickle configuration (fast) and reading it back (slow), 10 loops", C_SCOPE);
        write_and_verify(write_delay_clk_cycles => 0, read_delay_clk_cycles => 10);

        log(ID_LOG_HDR, "Writing some data to trickle configuration (slow) and reading it back (slow), 10 loops", C_SCOPE);
        write_and_verify(write_delay_clk_cycles => 10, read_delay_clk_cycles => 10);

        -- Finish the simulation
        wait for 1000 ns;                                             -- to allow some time for completion
        report_alert_counters(FINAL);                                 -- Report final counters and print conclusion for simulation (Success/Fail)
        uvvm_completed <= '1';
        wait for 1 us;
        stop_clock(CLOCK_GENERATOR_VVCT, 1, "Stop clock generator");  --@suppress
        wait;                                                         -- to stop completely
    end process;


    -----------------------------------------------------------------------------
    -- Instantiate DUT
    -----------------------------------------------------------------------------
    DUT : entity work.playback_controller
        generic map(
            USE_ULTRARAM => false
        )
        port map(
            clk                    => clk,
            rst                    => rst,
            start_pulse_i          => start_pulse_i,
            readout_active_o       => readout_active_o,
            playback_loops_i       => playback_loops_i,
            data_i                 => data_in.tdata,   --@suppress
            valid_i                => data_in.tvalid,  --@suppress
            ready_o                => data_in.tready,  --@suppress
            set_write_addr_pulse_i => set_write_addr_pulse_i,
            write_addr_start_i     => write_addr_start_i,
            read_addr_start_i      => read_addr_start_i,
            read_addr_stop_i       => read_addr_stop_i,
            data_o                 => data_o,
            valid_o                => valid_o,
            ready_i                => ready_i
        );

    i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

    i_clock_generator_vvc : entity bitvis_vip_clock_generator.clock_generator_vvc
        generic map(  -- @suppress "Generic map uses default values. Missing optional actuals: GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_INSTANCE_IDX    => 1,
            GC_CLOCK_NAME      => "Clock",
            GC_CLOCK_PERIOD    => C_CLK_PERIOD,
            GC_CLOCK_HIGH_TIME => C_CLK_PERIOD / 2
        )
        port map(
            clk => clk
        );

    i_axistream_vvc_master : entity bitvis_vip_axistream.axistream_vvc
        generic map(  -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_AXISTREAM_BFM_CONFIG, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => true,
            GC_DATA_WIDTH    => C_DATA_WIDTH,
            GC_USER_WIDTH    => C_USER_WIDTH,
            GC_ID_WIDTH      => C_ID_WIDTH,
            GC_DEST_WIDTH    => C_DEST_WIDTH,
            GC_INSTANCE_IDX  => 2)
        port map(
            clk              => clk,
            axistream_vvc_if => data_in);     --@suppress


    data_checker : process is
        variable i     : integer := 0;
        variable loop_counter : integer := 0;
    begin
        if verify_data_en then
            loop_counter := loops_prime;
            valid_word_count_rst <= '1';
            wait until rising_edge(clk);
            valid_word_count_rst <= '0';
            wait until rising_edge(clk);
            log(ID_SEQUENCER_SUB, "Data checker is waiting for data");
            while (loop_counter > 0) loop
                i := 0;
                while (i < data_expected_count) loop
                    await_value(valid_o, '1', 0 ns, C_CLK_PERIOD * 10, ERROR, "Waiting for playback controller data to become valid");
                    ready_i <= '1';
                    wait for C_CLK_PERIOD / 2;
                    if ?? valid_o then
                        check_value(readout_active_o, '1', ERROR, "Readout active flag is 1");
                        check_value(data_o, data_expected(i), ERROR, "Value read from playback controller matches the expected value");
                        wait until rising_edge(clk);
                        if read_delay_clk_cycles > 0 then
                            ready_i <= '0';
                            wait_num_rising_edge(clk, read_delay_clk_cycles);
                        end if;
                        i := i + 1;
                    else
                        wait until rising_edge(clk);
                    end if;
                end loop;
                loop_counter := loop_counter - 1;
                wait until rising_edge(clk);
            end loop;
            wait until rising_edge(clk);
            check_value(valid_o, '0', ERROR, "The playback controller FIFO is empty");
            check_value(valid_word_count, data_expected_count * loops_prime, ERROR, "The number of words read out from playback controller was correct");
            wait_num_rising_edge(clk, 2);
            ready_i          <= '0';
            wait_num_rising_edge(clk, 2);
            verify_data_done <= true;
            wait until rising_edge(verify_data_ack);
            verify_data_done <= false;
        end if;
        wait until rising_edge(clk);
    end process;


    -- signal valid_word_count  : natural := 0;
    -- signal valid_word_count_rst : std_logic := '0';
    valid_word_counter : process(clk)
    begin
        if rising_edge(clk) then
            if ?? valid_word_count_rst then
                valid_word_count <= 0;
            else
                valid_word_count <= valid_word_count + 1 when ( ?? valid_o) and ( ?? ready_i);
            end if;
        end if;
    end process;

end architecture RTL;

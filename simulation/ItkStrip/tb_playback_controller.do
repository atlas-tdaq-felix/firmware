onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_playback_controller_vunit/uut_tb/DUT/clk
add wave -noupdate /tb_playback_controller_vunit/uut_tb/DUT/rst
add wave -noupdate /tb_playback_controller_vunit/uut_tb/DUT/readout_active_o
add wave -noupdate /tb_playback_controller_vunit/uut_tb/DUT/data_i
add wave -noupdate /tb_playback_controller_vunit/uut_tb/DUT/valid_i
add wave -noupdate /tb_playback_controller_vunit/uut_tb/DUT/ready_o
add wave -noupdate /tb_playback_controller_vunit/uut_tb/DUT/set_write_addr_pulse_i
add wave -noupdate /tb_playback_controller_vunit/uut_tb/DUT/write_addr_start_i
add wave -noupdate /tb_playback_controller_vunit/uut_tb/DUT/read_addr_start_i
add wave -noupdate /tb_playback_controller_vunit/uut_tb/DUT/data_o
add wave -noupdate /tb_playback_controller_vunit/uut_tb/DUT/valid_o
add wave -noupdate /tb_playback_controller_vunit/uut_tb/DUT/wea
add wave -noupdate /tb_playback_controller_vunit/uut_tb/DUT/readout_active_reg
add wave -noupdate /tb_playback_controller_vunit/uut_tb/DUT/ready_i
add wave -noupdate /tb_playback_controller_vunit/uut_tb/DUT/valid_reg
add wave -noupdate /tb_playback_controller_vunit/uut_tb/DUT/ready_reg
add wave -noupdate /tb_playback_controller_vunit/uut_tb/DUT/data_reg
add wave -noupdate /tb_playback_controller_vunit/uut_tb/DUT/state
add wave -noupdate /tb_playback_controller_vunit/uut_tb/DUT/start_pulse_i
add wave -noupdate -radix unsigned /tb_playback_controller_vunit/uut_tb/DUT/addra
add wave -noupdate -radix unsigned /tb_playback_controller_vunit/uut_tb/DUT/read_addr_stop_i
add wave -noupdate /tb_playback_controller_vunit/uut_tb/DUT/loop_active
add wave -noupdate -radix unsigned /tb_playback_controller_vunit/uut_tb/DUT/loop_counter
add wave -noupdate /tb_playback_controller_vunit/uut_tb/DUT/trickle_loops_i
add wave -noupdate -divider {New Divider}
add wave -noupdate /tb_playback_controller_vunit/uut_tb/uvvm_completed
add wave -noupdate /tb_playback_controller_vunit/uut_tb/trickle_loops
add wave -noupdate /tb_playback_controller_vunit/uut_tb/clk
add wave -noupdate /tb_playback_controller_vunit/uut_tb/rst
add wave -noupdate /tb_playback_controller_vunit/uut_tb/start_pulse_i
add wave -noupdate /tb_playback_controller_vunit/uut_tb/readout_active_o
add wave -noupdate /tb_playback_controller_vunit/uut_tb/set_write_addr_pulse_i
add wave -noupdate /tb_playback_controller_vunit/uut_tb/write_addr_start_i
add wave -noupdate /tb_playback_controller_vunit/uut_tb/read_addr_start_i
add wave -noupdate /tb_playback_controller_vunit/uut_tb/read_addr_stop_i
add wave -noupdate /tb_playback_controller_vunit/uut_tb/data_o
add wave -noupdate /tb_playback_controller_vunit/uut_tb/valid_o
add wave -noupdate /tb_playback_controller_vunit/uut_tb/ready_i
add wave -noupdate /tb_playback_controller_vunit/uut_tb/data_in
add wave -noupdate /tb_playback_controller_vunit/uut_tb/verify_data_en
add wave -noupdate /tb_playback_controller_vunit/uut_tb/verify_data_done
add wave -noupdate /tb_playback_controller_vunit/uut_tb/verify_data_ack
add wave -noupdate /tb_playback_controller_vunit/uut_tb/valid_word_count
add wave -noupdate /tb_playback_controller_vunit/uut_tb/valid_word_count_rst
add wave -noupdate -divider {New Divider}
add wave -noupdate /tb_playback_controller_vunit/uut_tb/data_checker/data_expected_count
add wave -noupdate /tb_playback_controller_vunit/uut_tb/data_checker/loops_hack
add wave -noupdate /tb_playback_controller_vunit/uut_tb/data_checker/read_delay_clk_cycles
add wave -noupdate -radix unsigned /tb_playback_controller_vunit/uut_tb/data_checker/i
add wave -noupdate /tb_playback_controller_vunit/uut_tb/data_checker/loopy
add wave -noupdate -divider {New Divider}
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1163323 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 210
configure wave -valuecolwidth 181
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {1220625 ps}

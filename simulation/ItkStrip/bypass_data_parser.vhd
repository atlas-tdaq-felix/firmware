--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : Testbench helper module for sending elink data to Strips modules
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : bypass_data_parser.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Wed May 27 20:00:40 2020
-- Last update : Wed May 27 20:00:40 2020
-- Platform    : Default Part Number
-- Standard    : <VHDL-2008>
--------------------------------------------------------------------------------
-- Copyright (c) 2019 BNL
-------------------------------------------------------------------------------
-- Description:
--
-- This module is not synthesizable.
--
-- Loads specified file containing bypass data
-- and returns it frame by frame
--
-------------------------------------------------------------------------------
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use std.textio.all;
    use ieee.std_logic_textio.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

package bypass_data_parser is
    constant t_bypass_file_parser_record_max_length : natural := 1000;
    type t_bypass_data_array is array (natural range 0 to t_bypass_file_parser_record_max_length-1) of std_logic_vector(15 downto 0);
    type t_bypass_data_record is record
        count : natural;
        data  : t_bypass_data_array;
    end record t_bypass_data_record;



    type t_bypass_file_parser is protected
    -- Loads text file with the provided name, parse and return the dataset
    impure function parse_file(file_name : string) return t_bypass_data_record;
	end protected t_bypass_file_parser;
end package bypass_data_parser;

package body bypass_data_parser is
    type t_bypass_file_parser is protected body
    file hfile                    : TEXT;
    variable fstatus              : file_open_status;
    variable buf             : LINE;
    --variable ptr                  : integer;
    variable data_count, line_ctr : natural := 0;
    variable result          : t_bypass_data_record;
    variable good                 : boolean;
    variable frame                : std_logic_vector(15 downto 0);

    impure function parse_file(file_name : string) return t_bypass_data_record is
    begin
        log(ID_SEQUENCER, "Loading bypass frame data from " & file_name & " ...");
        data_count := 0;
        file_open(fstatus, hfile, file_name, read_mode);
        if fstatus /= open_ok then
            error("Could not open the file: " & file_name);
        else
            -- parse file, fill the data output
            line_ctr := 0;
            while not (endfile(hfile)) loop
                line_ctr := line_ctr + 1;
                readline(hfile, buf);
                if buf(1) = '-' then
						next;
                else
                    hread(buf, frame, good);
                    if good then
                        result.data(data_count) := frame;
                        data_count             := data_count + 1;
                    else
                        warning("Issue parsing file " & file_name & "line " & to_string(line_ctr));
                    end if;
                end if;
            end loop;
            log(ID_SEQUENCER, "Parsed file " & file_name & " successfully, loaded " & to_string(data_count) & " bypass frames");
        end if;
        file_close(hfile);

        -- Fill out the output data structure and return
        result.count := data_count;
        return result;
    end;
	end protected body;
end package body bypass_data_parser;

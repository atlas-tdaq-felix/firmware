--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : Testbench simulation for R3L1 encoder
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : tb_r3l1_axi_encoder.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Tue May 26 20:00:40 2020
-- Last update : Fri Jan 22 21:04:23 2021
-- Platform    : Default Part Number
-- Standard    : <VHDL-2008>
--------------------------------------------------------------------------------
-- Copyright (c) 2019 BNL
-------------------------------------------------------------------------------
-- Description:
--
-- Verify that the MUX is forwarding the data to the correct destination
--
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

library uvvm_vvc_framework;
    use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

library bitvis_vip_axistream;
    context bitvis_vip_axistream.vvc_context;

library bitvis_vip_clock_generator;
    context bitvis_vip_clock_generator.vvc_context;

    use work.strips_package.all;
    use work.decoder_queue_pkg.all;
    use work.bypass_data_parser.all;
    use work.axi_stream_package.all;
    use work.r3l1_regmap_package.all;

entity tb_r3l1_axi_encoder is
    generic(
        use_vunit: boolean := false -- @suppress "Unused generic: use_vunit is not used in work.tb_r3l1_axi_encoder(RTL)"
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity tb_r3l1_axi_encoder;

architecture RTL of tb_r3l1_axi_encoder is
    constant C_CLK_PERIOD          : time    := 25 ns;
    constant C_AXI_CLK_PERIOD      : time    := 5 ns;
    constant C_CYCLES_RST          : integer := 3;
    --constant C_STABLE_PERIODS      : integer := 10;
    --constant C_MAX_CYCLES_PER_BYTE : integer := 4;
    constant C_USER_WIDTH          : integer := 1;
    constant C_ID_WIDTH            : integer := 1;
    constant C_DEST_WIDTH          : integer := 1;
    constant C_ELINK_WIDTH         : integer := 8;
    --constant C_FRAME_WIDTH         : integer := 12;

    signal rst : std_logic := '0';
    signal clk : std_logic;
    signal s_axis_aclk : std_logic;

    -- axistream data sources
    signal command_link : t_axistream_if(--@suppress
        tdata(C_ELINK_WIDTH - 1 downto 0),
        tkeep((C_ELINK_WIDTH / 8) - 1 downto 0),
        tuser(C_USER_WIDTH - 1 downto 0),
        tstrb((C_ELINK_WIDTH / 8) - 1 downto 0),
        tid(C_ID_WIDTH - 1 downto 0),
        tdest(C_DEST_WIDTH - 1 downto 0)
    );

    signal config_link : t_axistream_if(--@suppress
        tdata(C_ELINK_WIDTH - 1 downto 0),
        tkeep((C_ELINK_WIDTH / 8) - 1 downto 0),
        tuser(C_USER_WIDTH - 1 downto 0),
        tstrb((C_ELINK_WIDTH / 8) - 1 downto 0),
        tid(C_ID_WIDTH - 1 downto 0),
        tdest(C_DEST_WIDTH - 1 downto 0)
    );

    signal command_s_axis : axis_8_type;
    signal config_s_axis : axis_8_type;

    constant command_channel : integer := 2;
    constant config_channel : integer := 3;


    -- TTC signals
    signal bcr        : std_logic                    := '0';
    signal l1_trigger : std_logic_vector(3 downto 0) := "0000"; -- L1 trigger signal
    signal l1_tag     : std_logic_vector(6 downto 0) := (others => '0'); -- L0tag

    -- RoIE signals
    signal r3_trigger  : std_logic                    := '0'; -- R3 trigger signal
    signal r3_tag      : std_logic_vector(6 downto 0) := (others => '0'); -- L0tag
    signal module_mask : std_logic_vector(4 downto 0) := (others => '0'); -- module mask

    -- configuration from global register map
    signal config           : t_strips_config;

    -- decoder
    signal EdataOUT        : std_logic_vector(3 downto 0);
    signal skip_idles_i    : std_logic := '1';
    signal phase_wrt_bcr_o : integer range 0 to 3;
    signal decoder_rst     : std_logic := '0';

    --frame_encoded_o : std_logic_vector(15 downto 0);
    signal is_locked_o : std_logic;

    -- output signals
    signal frame                : t_decoder_record;
    shared variable frame_queue : work.decoder_queue_pkg.t_generic_queue;
    signal frame_valid          : std_logic;

    -- simulation sample frame data
    shared variable bypass            : work.bypass_data_parser.t_bypass_file_parser;
    type t_data is array (natural range <>) of std_logic_vector(11 downto 0);
    constant max_r3l1_samples         : integer := 3; -- how many r3l1 triggers can arrive in quick succession
    constant repeat_samples           : integer := 20; -- how many repetitions each test does
    constant bypass_frame_repetitions : integer := 10; -- how many times to repeat bypass frames to test scheduling priority
begin

    config_s_axis.tvalid  <= config_link.tvalid;--@suppress
    config_s_axis.tdata   <= config_link.tdata;--@suppress
    config_s_axis.tlast   <= config_link.tlast;--@suppress

    command_s_axis.tvalid <= command_link.tvalid;--@suppress
    command_s_axis.tdata  <= command_link.tdata;--@suppress
    command_s_axis.tlast  <= command_link.tlast;--@suppress

    DUT : entity work.r3l1_axi_encoder
        generic map(
            --DEBUG_en => false,
            DISTR_RAM => false)
        port map(
            rst         => rst,
            clk40       => clk,
            --ila_clk     => '0',
            s_axis_aclk => s_axis_aclk,
            daq_fifo_flush => rst,
            invert_polarity => '0',

            -- axistream configuration source
            config_s_axis      => config_s_axis,
            config_tready     => config_link.tready,--@suppress
            config_almost_full  => open,

            -- axistream command source
            command_s_axis    => command_s_axis,
            command_tready     => command_link.tready,--@suppress
            command_almost_full => open,

            -- TTC signals
            bcr_i         => bcr,
            l1_trigger_i  => l1_trigger,
            l1_tag_i      => l1_tag,

            ITk_sync_i   => '0',
            -- RoIE signals
            r3_trigger_i  => r3_trigger,
            r3_tag_i      => r3_tag,
            module_mask_i => module_mask,

            -- configuration from global register map
            config        => config,

            -- output signals
            EdataOUT      => EdataOUT
        );

    -- LCB/R3L1 frame alignment and decoding module
    i_decoder : entity work.itk_frame_decoder
        port map(
            clk => clk,
            rst => decoder_rst,
            bcr => bcr,
            stopwatch_set => '0',
            stopwatch_rst => '0',
            edata_i => EdataOUT,
            skip_idles_i => skip_idles_i,
            frame_encoded_o => frame.encoded,
            is_locked_o => frame.locked,
            is_idle_o => frame.idle,
            is_k2_o => frame.k2,
            is_k3_o => frame.k3,
            frame_decoded_o => frame.decoded,
            frame_next_o => frame_valid,
            phase_wrt_bcr_o => phase_wrt_bcr_o,
            stopwatch_o => open
        );

    is_locked_o <= frame.locked;

    p_test : process is
        variable l1_tag_rnd      : std_logic_vector(6 downto 0); -- L0tag
        variable r3_tag_rnd      : std_logic_vector(6 downto 0); -- L0tag
        variable module_mask_rnd : std_logic_vector(4 downto 0); -- module mask
        variable next_frame      : t_decoder_record;

        variable r3_data : t_data(0 to max_r3l1_samples); -- @suppress "variable r3_data is never read"
        variable l1_data : t_data(0 to max_r3l1_samples); -- @suppress "variable l1_data is never read"

        variable bypass_data : t_bypass_data_record;

        -- generates random R3 / L1 frame data
        procedure generate_r3l1_samples is -- @suppress "Unused declaration"
        begin
            for i in 0 to max_r3l1_samples loop
                r3_data(i) := random(12);
                l1_data(i) := "00000" & random(7);
            end loop;
        end;

        -- simulates receiving an R3 trigger
        procedure send_R3(
            mask     : std_logic_vector(4 downto 0);
            tag      : std_logic_vector(6 downto 0);
            delay_en : boolean := true
        ) is
        begin
            module_mask <= mask;
            r3_tag      <= tag;
            r3_trigger  <= '1';
            if delay_en then
                wait until rising_edge(clk);
                r3_trigger <= '0';
            end if;
        end;

        -- simulates receiving an L1 trigger
        procedure send_L1(
            tag      : std_logic_vector(6 downto 0);
            delay_en : boolean := true
        ) is
        begin
            l1_tag     <= tag;
            l1_trigger <= "1000";
            if delay_en then
                wait until rising_edge(clk);
                l1_trigger <= "0000";
            end if;
        end;

        -- simulates receiving R3 and L1 triggers simultaneously
        procedure send_R3L1(
            mask   : std_logic_vector(4 downto 0);
            tag_R3 : std_logic_vector(6 downto 0);
            tag_L1 : std_logic_vector(6 downto 0)
        ) is
        begin
            send_L1(tag_L1, delay_en => false);
            send_R3(mask, tag_R3, delay_en => false);
            wait until rising_edge(clk);
            r3_trigger <= '0';
            l1_trigger <= "0000";
        end;

        -- simulates receiving a random L1 trigger
        procedure send_random_L1 is
        begin
            l1_tag_rnd := random(l1_tag_rnd'length);
            send_L1(l1_tag_rnd);
        end;

        -- simulates receiving a random R3 trigger
        procedure send_random_R3 is
        begin
            r3_tag_rnd      := random(r3_tag_rnd'length);
            module_mask_rnd := random(module_mask_rnd'length);
            send_R3(module_mask_rnd, r3_tag_rnd);
        end;

        -- sets a single-bit configuration register field of the module to the given value
        procedure config_set(
            address  : t_register_name;
            data     : std_logic_vector(15 downto 0)
        ) is
        begin
            axistream_transmit(AXISTREAM_VVCT, config_channel,--@suppress
                t_slv_array'(ITK_CMD_REGMAP_WRITE, data(15 downto 8), data(7 downto 0),
                    std_logic_vector(to_unsigned(t_register_name'pos(address), 8))),
                "Update configuration register, " & to_string(address) & " = " & to_hstring(data));
            await_completion(AXISTREAM_VVCT, config_channel, 500 * C_CLK_PERIOD,--@suppress
                "Waiting for the command to complete");
            wait_num_rising_edge(clk, 20);
        end;

        -- checks that only IDLE frames have been received
        -- (this procedure works only when decoder ignores idle frames)
        procedure assert_idle_only is
        begin
            check_value(is_locked_o, '1', ERROR, "Frame decoder is locked");
            check_value(frame_queue.is_empty(void), true, ERROR, "Decoder only received IDLEs");
        end;

        -- wait for the frame and verify the contents
        procedure dequeue_next_frame(
            expiration_time : time := 1000 ns
        ) is
            variable time_expire : time;
        begin
            time_expire := now + expiration_time;
            wait_loop : while now < time_expire loop
                exit wait_loop when (not frame_queue.is_empty(void));
                wait until rising_edge(clk);
            end loop;

            if now >= time_expire then
                error("Timeout while waiting for the next frame");
            else
                next_frame := frame_queue.fetch(void);
            end if;
        end;

        -- relock frame decoder, measure the frame phase
        procedure verify_frame_phase(expected :integer range 0 to 3) is
        begin
            gen_pulse(decoder_rst, C_CYCLES_RST * C_CLK_PERIOD, "Reset frame decoder");
            wait until rising_edge(clk);
            for i in 0 to 5 loop
                bcr <= '1';
                wait until rising_edge(clk);
                bcr <= '0';
                wait_num_rising_edge(clk, 31);
            end loop;
            check_value(is_locked_o, '1', ERROR, "Verifying that frame decoder is locked");
            check_value(phase_wrt_bcr_o, expected, ERROR, "Verifying frame phase with respect to BCR signal");
        end;

        -- initialize link configuration
        procedure initialize_config is
        begin
            config.GLOBAL_STRIPS_CONFIG.INVERT_AMAC_IN <= (others => '0');
            config.GLOBAL_STRIPS_CONFIG.INVERT_AMAC_OUT <= (others => '0');
            config.GLOBAL_STRIPS_CONFIG.INVERT_AMAC_IN <= (others => '0');
            config.GLOBAL_STRIPS_CONFIG.INVERT_R3L1_OUT <= (others => '0');
            config.GLOBAL_STRIPS_CONFIG.INVERT_LCB_OUT <= (others => '0');
            config.GLOBAL_STRIPS_CONFIG.TEST_MODULE_MASK <= (others => '0');
            config.GLOBAL_STRIPS_CONFIG.TEST_R3L1_TAG <= (others => '0');
            config.GLOBAL_STRIPS_CONFIG.TTC_GENERATE_GATING_ENABLE <= (others => '0');
            config.GLOBAL_STRIPS_CONFIG.TTC_GATING_OVERRIDE <= (others => '0');
            config.STRIPS_R3_TRIGGER <= (others => '0');
            config.STRIPS_L1_TRIGGER <= (others => '0');
            config.STRIPS_R3L1_TRIGGER <= (others => '0');
        end;

        variable frame_var          : t_decoder_record;
        variable bypass_interrupted : boolean := false;
    begin
        -------- Initialize the testbench -------------
        await_uvvm_initialization(VOID);
        frame_queue.set_scope(C_TB_SCOPE_DEFAULT);
        start_clock(CLOCK_GENERATOR_VVCT, 1, "Start clock generator (BC)");--@suppress
        start_clock(CLOCK_GENERATOR_VVCT, 4, "Start clock generator (AXI stream)");--@suppress
        disable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        enable_log_msg(ID_SEQUENCER);
        --enable_log_msg(ID_POS_ACK);
        --enable_log_msg(ID_UVVM_SEND_CMD);
        disable_log_msg(CLOCK_GENERATOR_VVCT, 1, ALL_MESSAGES);--@suppress
        disable_log_msg(AXISTREAM_VVCT, 2, ALL_MESSAGES);--@suppress
        disable_log_msg(AXISTREAM_VVCT, 3, ALL_MESSAGES);--@suppress
        disable_log_msg(CLOCK_GENERATOR_VVCT, 4, ALL_MESSAGES);--@suppress
        enable_log_msg(CLOCK_GENERATOR_VVCT, 1, ID_BFM);--@suppress
        enable_log_msg(CLOCK_GENERATOR_VVCT, 4, ID_BFM);--@suppress
        --enable_log_msg(AXISTREAM_VVCT, 2, ID_BFM);

        initialize_config;
        -- Load test data from file
        bypass_data := bypass.parse_file("../samples/r3l1_command_outputs.txt");

        -------- verify that the R3L1 module starts up and the frame decoder locks
        log(ID_SEQUENCER, "Waiting for R3L1 decoder to lock");
        skip_idles_i <= '0';
        gen_pulse(decoder_rst, C_CYCLES_RST * C_CLK_PERIOD, "Reset frame decoder");
        gen_pulse(rst, C_CYCLES_RST * C_CLK_PERIOD, "Reset DUT");
        wait for 1000 ns;
        check_value(is_locked_o, '1', ERROR, "Frame decoder is locked after 500 ns");
        check_value(frame_queue.is_empty(void), false, ERROR, "Decoder received some frames");
        frame_var    := frame_queue.fetch(void);
        check_value(frame_var.idle, '1', ERROR, "Decoder received some IDLE frames");
        frame_queue.flush(void);
        skip_idles_i <= '1';
        wait until rising_edge(clk);

        -------- verify that R3 and L1 frames are ignored when disabled
        log(ID_LOG_HDR, "Verifying that R3 and L1 frames can be disabled using configuration");
        config_set(R3_ENABLE, x"0000");
        config_set(L1_ENABLE, x"0000");
        for i in 0 to repeat_samples - 1 loop
            send_random_R3;
            send_random_L1;
        end loop;
        wait for 1000 ns;
        assert_idle_only;

        -------- verify bypass frames come through
        log(ID_LOG_HDR, "Verifying that bypass frames are correctly transmitted");
        for i in 0 to bypass_data.count - 1 loop -- schedule transmission of all bypass data
            axistream_transmit(AXISTREAM_VVCT, command_channel, bypass_data.data(i)(15 downto 8),--@suppress
                               "Sending bypass frame data through the elink (byte 1 out of 2");
            axistream_transmit(AXISTREAM_VVCT, command_channel, bypass_data.data(i)(7 downto 0),--@suppress
                               "Sending bypass frame data through the elink (byte 2 out of 2");
        end loop;
        for i in 0 to bypass_data.count - 1 loop -- receive and verify all data
            dequeue_next_frame;
            check_value(next_frame.encoded, bypass_data.data(i), ERROR, "Received expected bypass frame");
        end loop;

        -------- send some R3 frames and verify they have been received
        log(ID_LOG_HDR, "Enabling R3 frames and verifying them (one by one)");
        config_set(R3_ENABLE, x"0001");
        for i in 0 to repeat_samples - 1 loop
            send_random_R3;
            dequeue_next_frame;
            check_value(next_frame.decoded, module_mask_rnd & r3_tag_rnd, ERROR, "Received expected R3 frame");
        end loop;

        -------- send some L1 triggers and verify they have been received
        log(ID_LOG_HDR, "Enabling L1 frames and verifying them (one by one)");
        config_set(L1_ENABLE, x"0001");
        for i in 0 to repeat_samples - 1 loop
            send_random_L1;
            dequeue_next_frame;
            check_value(next_frame.decoded, "00000" & l1_tag_rnd, ERROR, "Received expected L1 frame");
        end loop;

        -------- send R3 and L1 triggers simultaneously and verify that R3 frames come out first
        log(ID_SEQUENCER, "Verifying that R3 frames take priority over L1 frames");
        for i in 0 to max_r3l1_samples - 1 loop
            send_R3L1(mask => "11011", tag_R3 => "1011101", tag_L1 => "0111010");
        end loop;

        for i in 0 to max_r3l1_samples - 1 loop
            dequeue_next_frame;
            check_value(next_frame.decoded, "110111011101", ERROR, "Received expected R3 frame");
        end loop;

        for i in 0 to max_r3l1_samples - 1 loop
            dequeue_next_frame;
            check_value(next_frame.decoded, "000000111010", ERROR, "Received expected L1 frame");
        end loop;

        -------- verify that R3 frames take priority over bypass frames
        log(ID_SEQUENCER, "Verifying that R3 frames take priority over bypass frames");
        for i in 0 to bypass_frame_repetitions - 1 loop -- schedule transmission of all bypass data
            axistream_transmit(AXISTREAM_VVCT, command_channel, t_slv_array'(x"33", x"33"),--@suppress
                "Repeating bypass frame 0x3333");
        end loop;
        wait_num_rising_edge(clk, 20);
        send_R3("10101", "0101010");

        bypass_interrupted := false;
        for i in 0 to bypass_frame_repetitions loop -- receive and verify all data
            dequeue_next_frame;
            if next_frame.decoded = "101010101010" then
                bypass_interrupted := true;
            else
                check_value(next_frame.encoded, x"3333", ERROR, "Received expected bypass frame");
            end if;
        end loop;
        wait_num_rising_edge(clk, 4 * (bypass_frame_repetitions + 10));
        check_value(frame_queue.is_empty(void), true, ERROR, "There are no stray frames enqueued");
        check_value(bypass_interrupted, true, ERROR, "Bypass frame sequence was interrupted by R3 frame");

        -------- verify that L1 frames take priority over bypass frames
        log(ID_SEQUENCER, "Verifying that L1 frames take priority over bypass frames");
        for i in 0 to bypass_frame_repetitions - 1 loop -- schedule transmission of all bypass data
            axistream_transmit(AXISTREAM_VVCT, command_channel, t_slv_array'(x"33", x"33"),--@suppress
                "Repeating bypass frame 0x3333");
        end loop;
        wait_num_rising_edge(clk, 20);
        send_L1("0101010");

        bypass_interrupted := false;
        for i in 0 to bypass_frame_repetitions loop -- receive and verify all data
            dequeue_next_frame;
            if next_frame.decoded = "000000101010" then
                bypass_interrupted := true;
            else
                check_value(next_frame.encoded, x"3333", ERROR, "Received expected bypass frame");
            end if;
        end loop;
        wait_num_rising_edge(clk, 4 * (bypass_frame_repetitions + 10));
        check_value(frame_queue.is_empty(void), true, ERROR, "There are no stray frames enqueued");
        check_value(bypass_interrupted, true, ERROR, "Bypass frame sequence was interrupted by L1 frame");

        -------- verify R3L1 frame phase changes in accordance with the configuration
        log(ID_LOG_HDR, "Verifing that R3L1 frame phase can be adjusted using elink configuration");
        log(ID_SEQUENCER, "Setting phase to 0b00");
        config_set(L0A_FRAME_PHASE, x"0000");
        verify_frame_phase(3);
        log(ID_SEQUENCER, "Setting phase to 0b01");
        config_set(L0A_FRAME_PHASE, x"0001");
        verify_frame_phase(2);
        log(ID_SEQUENCER, "Setting phase to 0b10");
        config_set(L0A_FRAME_PHASE, x"0002");
        verify_frame_phase(1);
        log(ID_SEQUENCER, "Setting phase to 0b11");
        config_set(L0A_FRAME_PHASE, x"0003");
        verify_frame_phase(0);

        wait for 1000 ns;
        report_alert_counters(FINAL);   -- Report final counters and print conclusion for simulation (Success/Fail)
        uvvm_completed <= '1';
        wait for 1 us;
        stop_clock(CLOCK_GENERATOR_VVCT, 1, "Stop clock generator (BC clock)");--@suppress
        stop_clock(CLOCK_GENERATOR_VVCT, 4, "Stop clock generator (AXI stream clock)");--@suppress
        wait;
    end process;

    -----------------------------------------------------------------------------
    -- Instantiate UVVM entities
    -----------------------------------------------------------------------------
    i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

    i_clock_generator_vvc : entity bitvis_vip_clock_generator.clock_generator_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_INSTANCE_IDX    => 1,
            GC_CLOCK_NAME      => "BC clock",
            GC_CLOCK_PERIOD    => C_CLK_PERIOD,
            GC_CLOCK_HIGH_TIME => C_CLK_PERIOD / 2
        )
        port map(
            clk => clk
        );

    i_axi_clock_generator_vvc : entity bitvis_vip_clock_generator.clock_generator_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_INSTANCE_IDX    => 4,
            GC_CLOCK_NAME      => "AXI clock",
            GC_CLOCK_PERIOD    => C_AXI_CLK_PERIOD,
            GC_CLOCK_HIGH_TIME => C_AXI_CLK_PERIOD / 2
        )
        port map(
            clk => s_axis_aclk
        );

    i_command : entity bitvis_vip_axistream.axistream_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_AXISTREAM_BFM_CONFIG, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => true,
            GC_DATA_WIDTH    => C_ELINK_WIDTH,
            GC_USER_WIDTH    => C_USER_WIDTH,
            GC_ID_WIDTH      => C_ID_WIDTH,
            GC_DEST_WIDTH    => C_DEST_WIDTH,
            GC_INSTANCE_IDX  => command_channel)
        port map(
            clk              => s_axis_aclk,
            axistream_vvc_if => command_link);--@suppress

    i_config : entity bitvis_vip_axistream.axistream_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_AXISTREAM_BFM_CONFIG, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => true,
            GC_DATA_WIDTH    => C_ELINK_WIDTH,
            GC_USER_WIDTH    => C_USER_WIDTH,
            GC_ID_WIDTH      => C_ID_WIDTH,
            GC_DEST_WIDTH    => C_DEST_WIDTH,
            GC_INSTANCE_IDX  => config_channel)
        port map(
            clk              => s_axis_aclk,
            axistream_vvc_if => config_link);--@suppress

    -- Adds decoded valid frames into the queue
    p_queue : process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' then
                frame_queue.reset(VOID);
            else
                if frame_valid = '1' then
                    frame_queue.add(frame);
                end if;
            end if;
        end if;
    end process;

end architecture RTL;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : ITk Strips package
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : tb_l0a_frame_generator.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Tue Feb  4 16:32:32 2020
-- Last update : Thu May 21 18:31:36 2020
-- Platform    : Default Part Number
-- Standard    : VHDL-2008
--------------------------------------------------------------------------------
-- Copyright (c) 2020 BNL
-------------------------------------------------------------------------------
-- Description:
-- Set of tests for l0a_frame_generator
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.MATH_REAL.all;
    use work.strips_package.all;

--library STD;
    use std.env.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

entity tb_l0a_frame_generator is
    generic(
        use_vunit: boolean := false -- @suppress "Unused generic: use_vunit is not used in work.tb_l0a_frame_generator(RTL)"
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity tb_l0a_frame_generator;

architecture RTL of tb_l0a_frame_generator is
    constant C_CLK_PERIOD_40               : time    := 25 ns;
    constant C_CYCLES_RST                  : integer := 3;
    constant C_STABLE_PERIODS              : integer := 10;
    constant C_MAX_FIRST_FRAME_PULSE_DELAY : integer := 6;
    constant C_MAX_FRAME_PULSE_DELAY       : integer := 4;
    constant C_ONE_FRAME_DELAY             : time    := C_CLK_PERIOD_40 * 4;

    signal clk                 : std_logic; -- BC clock
    signal clk_en              : boolean                      := false;
    constant rst                 : std_logic                    := '0';
    signal bcr_i               : std_logic                    := '0'; -- BCR signal
    signal l0a_i               : std_logic                    := '0'; -- L0A signal
    signal bcr_delay_i         : unsigned(full_bcid'range)    := (others => '0'); -- by how many clk cycles delay the BCR signal
    signal frame_phase_i       : unsigned(frame_phase'range)  := (others => '0'); -- determines the phase of the L0A frame with respect to the BCR pulse
    signal l0id_i              : std_logic_vector(6 downto 0) := (others => '0'); -- L0tag
    signal frame_start_pulse_o : std_logic; -- strobe to start L0A frame
    signal l0a_frame_o         : std_logic_vector(11 downto 0); -- formed L0A frame
    signal l0a_checker_en      : boolean                      := false;

    constant l0a_frame_null : std_logic_vector(l0a_frame_o'range) := (others => '0');

    signal l0_tag_expected, l0_tag_actual     : std_logic_vector(L0tag_bits'range)    := (others => '0'); -- @suppress "signal l0_tag_actual is never read"
    signal l0a_mask_expected, l0a_mask_actual : std_logic_vector(L0A_mask_bits'range) := (others => '0'); -- @suppress "signal l0a_mask_actual is never read"
    signal bcr_expected, bcr_actual           : std_logic_vector(BCR_bits'range)      := (others => '0'); -- @suppress "signal bcr_actual is never read"
    signal l0a_trig_i : std_logic_vector(3 downto 0);
    signal ITk_sync_i : std_logic := '0';

begin

    l0_tag_actual   <= l0a_frame_o(L0tag_bits'range);
    l0a_mask_actual <= l0a_frame_o(L0A_mask_bits'range);
    bcr_actual      <= l0a_frame_o(BCR_bits'range);

    TEST : process is
        --        -- set RST to a given value and wait C_CYCLES_RST cycles
        --        procedure set_rst(
        --            value    : std_logic;
        --            duration : positive := C_CYCLES_RST
        --        ) is
        --        begin
        --            wait until rising_edge(clk);
        --            rst <= value;
        --            wait_num_rising_edge(clk, duration);
        --        end;

        -- pulse bcr for a single clock cycle
        --        procedure pulse_bcr is
        --        begin
        --            bcr_i <= '1';
        --            wait until rising_edge(clk);
        --            bcr_i <= '0';
        --            wait until rising_edge(clk);
        --        end;


        -- verifiy that the frame_start_pulse_o provides pulses with the correct period for the given number of repetitions
        procedure verify_frame_pulse_period(
            repetitions    : positive := 1;
            wait_for_pulse : boolean  := true
        ) is
        begin
            if wait_for_pulse then
                await_value(frame_start_pulse_o, '1', 0 ns, C_MAX_FRAME_PULSE_DELAY * C_CLK_PERIOD_40, ERROR,
                            "frame_start_pulse has to arrive within " & to_string(C_MAX_FRAME_PULSE_DELAY) & " clk cycles");
                wait until rising_edge(clk);
            end if;
            for i in 0 to repetitions - 1 loop
                for j in 1 to 3 loop
                    wait until rising_edge(clk);
                    check_value(frame_start_pulse_o, '0', ERROR, "frame_start_pulse_o = '0' between the pulses (j = " & to_string(j) & ")");
                end loop;
                wait until rising_edge(clk);
                check_value(frame_start_pulse_o, '1', ERROR, "frame_start_pulse_o = '1' when the next pulse begins");
            --check_value(l0a_frame_o(BCR_bits'range), "0", ERROR, "BCR is deasserted for this pulse sequence");
            end loop;
        end;

        -- send and verify a single L0A frame
        procedure generate_single_l0a_frame(
            --bcr       : boolean := false; -- generate BCR signal
            --bcr_phase : natural range 0 to 3; -- on which BC to generate a BCR
            l0_tag    : std_logic_vector(L0tag_bits'length-1 downto 0); -- L0A tag to use
            l0a_mask  : std_logic_vector(0 to L0A_mask_bits'length-1) -- L0A mask to use
        ) is
        begin
            log(ID_SEQUENCER_SUB, "Sending L0A frame with mask = " & to_string(l0a_mask) & ", tag = " & to_string(l0_tag) & ", bcr = " & to_string(bcr_expected), C_SCOPE);
            --            --l0id_i <= l0_tag;
            --            for j in l0a_mask'left to l0a_mask'right loop
            ----                if bcr and j = bcr_phase then
            ----                    bcr_i <= '1';
            ----                else
            ----                    bcr_i <= '0';
            ----                end if;
            --                l0a_i <= l0a_mask(j);
            --                l0a_trig_i <= l0a_mask(j)&"000";
            --                if j = l0a_mask'left then
            --                    l0id_i <= l0_tag;
            --                else
            --                    l0id_i <= random(l0id_i'length);
            --                end if;
            --                wait until rising_edge(clk);
            --            end loop;
            wait until rising_edge(ITk_sync_i);
            l0a_trig_i <= l0a_mask;
            l0id_i <= l0_tag;
            wait until rising_edge(ITk_sync_i);
            --wait until rising_edge(clk);
            l0a_trig_i <="0000";
            l0id_i <= (others => '0');

        end;

        -- send a random sequence of emulated TTC signals to the DUT
        procedure generate_and_verify_l0a_frames(
            count      : positive             := 100; -- how many frames to generate
            bcr        : boolean              := false; -- generate BCR signal along with L0A
            bcr_phase  : natural range 0 to 3 := 0; -- on which BC after the pulse to generate a BCR
            bcr_period : positive             := 8 -- how often to issue a bcr
        ) is
            variable l0_tag_tmp   : std_logic_vector(L0tag_bits'range)    := (others => '0');
            variable l0a_mask_tmp : std_logic_vector(L0A_mask_bits'range) := (others => '0');
            variable bcr_tmp      : std_logic_vector(BCR_bits'range)      := (others => '0');
            variable when_data_is_valid  : time := 0 ns;
        begin
            log(ID_SEQUENCER_SUB, "Synchronizing to the beginning of the frame; bcr_phase = " & to_string(bcr_phase), C_SCOPE);

            --wait until rising_edge(frame_start_pulse_o);
            --await_value(frame_start_pulse_o, '1', 0 ns, C_MAX_FRAME_PULSE_DELAY * C_CLK_PERIOD_40, ERROR,
            --            "Waiting for the beginning of the frame");
            --when_data_is_valid := C_ONE_FRAME_DELAY;
            --wait_num_rising_edge(clk, 3);
            l0a_checker_en <= true after C_ONE_FRAME_DELAY + C_CLK_PERIOD_40;

            for i in 0 to count - 1 loop
                await_value(frame_start_pulse_o, '1', 0 ns, C_MAX_FRAME_PULSE_DELAY * C_CLK_PERIOD_40, ERROR,
                            "frame_start_pulse has to arrive within " & to_string(C_MAX_FRAME_PULSE_DELAY) & " clk cycles");
                l0a_mask_tmp := std_logic_vector(to_unsigned(random(1, 15), l0a_mask_expected'length)); -- mask cannot be 0
                l0_tag_tmp   := random(l0_tag_expected'length);
                --bcr_tmp      := "1" when bcr and ((i - integer(floor(real(to_integer(bcr_delay_i)) / 4.0))) rem bcr_period = 0) else "0";
                --        log(ID_SEQUENCER_SUB, "bcr = " & to_string(bcr), C_SCOPE);
                --        log(ID_SEQUENCER_SUB, "i = " & to_string(i), C_SCOPE);
                --        log(ID_SEQUENCER_SUB, "bcr_delay = " & to_string(bcr_delay_i), C_SCOPE);
                --        log(ID_SEQUENCER_SUB, "bcr_period = " & to_string(bcr_period), C_SCOPE);
                --        log(ID_SEQUENCER_SUB, "bcr_tmp = " & to_string(bcr_tmp), C_SCOPE);
                --        log(ID_SEQUENCER_SUB, "math = " & to_string((i - integer(floor(real(to_integer(bcr_delay_i)) / 4.0)))), C_SCOPE);


                l0a_mask_expected <= l0a_mask_tmp;-- after when_data_is_valid;
                l0_tag_expected   <= l0_tag_tmp;-- after when_data_is_valid;
                --bcr_expected      <= bcr_tmp after when_data_is_valid;

                --bcr is generated centrally in sync with the synchronization pulse.
                generate_single_l0a_frame(
                --bcr       => bcr and (i rem bcr_period = 0),
                --bcr_phase => bcr_phase,
                    l0_tag    => l0_tag_tmp,
                    l0a_mask  => l0a_mask_tmp
                );
                l0a_mask_expected <= "0000";
                l0_tag_expected   <= (others => '0');
                wait until rising_edge(clk);

            end loop;

            l0a_checker_en <= false;
        end;

        --        -- Vary the BCR delay,  check that the L0A frames are still generated correctly
        --        -- and the BCR flag shows up with the correct delay
        --        procedure verify_l0a_frames_with_bcr_delay(
        --            l0a_count     : positive := 100; -- how many l0a packets to generate for each delay
        --            bcr_delay_max : positive := 512; -- largest bcr_delay_value to check
        --            bcr_period    : positive := 8 -- how often to issue BCR signals
        --        ) is
        --        --variable bcr_phase : natural range 0 to 3;
        --        begin
        --            frame_phase_i <= "10";
        --            wait_num_rising_edge(clk, 4);
        --            for bcr_delay in 0 to bcr_delay_max loop
        --                bcr_delay_i <= to_unsigned(bcr_delay, bcr_delay_i'length);
        --                generate_and_verify_l0a_frames(
        --                    count      => l0a_count,
        --                    bcr        => true,
        --                    bcr_phase  => 0,
        --                    bcr_period => bcr_period
        --                );
        --            end loop;
        --        end;

        -- verifies that the frame phase is locked to the BCR pulse
        procedure verify_frame_pulse_phase(
            reset : boolean := False    -- assert reset while issuing the BCR pulse
        ) is
        begin


            await_value(bcr_i, '1', 0 ns,  3564 * C_CLK_PERIOD_40, ERROR, "BCR pulse is generated");

            --for i in 0 to 0 loop
            -- frame_start_pulse is supposed to occur frame_phase_i + 1 clk cycle after the BCR
            --frame_phase_i <= to_unsigned(i, frame_phase_i'length);
            --                if reset then
            --                    set_rst('1', 3);
            --                    pulse_bcr;
            --                    set_rst('0', 3);
            --                else
            --                    pulse_bcr;
            --                end if;
            --wait_num_rising_edge(clk, i + 1);
            wait until rising_edge(clk);
            wait until rising_edge(clk);
            --the output needs to be a clock after a BCR
            check_value(frame_start_pulse_o, '1', ERROR, "Pulse starts with the correct phase"); --: frame_phase_i = " & to_string(frame_phase_i)
            log(ID_SEQUENCER, "Verifying that frame_start_pulse_o has the correct in respect to the BCR", C_SCOPE); --period (phase = " & to_string(frame_phase_i) & ")
            verify_frame_pulse_period(repetitions => 10, wait_for_pulse => false);
        --end loop;
        end;

    begin
        disable_log_msg(ID_POS_ACK);
        clk_en <= true;

        --------------------   Tests begin here -------------------------------

        --rst is not used
        --log(ID_LOG_HDR, "Checking behavior when rst = 1", C_SCOPE);
        --set_rst('1');
        --check_value(l0a_frame_o, l0a_frame_null, ERROR, "The output L0A frame is 0x0");
        --check_value(frame_start_pulse_o, '0', ERROR, "There are no frame start pulses");

        --wait_num_rising_edge(clk, C_STABLE_PERIODS);
        --check_stable(frame_start_pulse_o, C_CLK_PERIOD_40 * C_STABLE_PERIODS, ERROR, "There are no frame start pulses");
        --bcr_i <= '1'; --bcr is generated pereodically in sync with frame_phase_i
        --wait until rising_edge(clk);
        --check_stable(frame_start_pulse_o, C_CLK_PERIOD_40 * C_STABLE_PERIODS, ERROR, "There are no frame start pulses");
        --check_stable(l0a_frame_o, C_CLK_PERIOD_40 * 3564, ERROR, "The module doesn't react to BCR input"); --C_STABLE_PERIODS
        --bcr_i <= '0';  --bcr is generated pereodically in sync with frame_phase_i
        --l0a_i <= '1'; --not used
        --l0a_trig_i <= "1000"; --TOOD: use a counter
        --wait until rising_edge(clk);
        --check_stable(l0a_frame_o, C_CLK_PERIOD_40 * C_STABLE_PERIODS, ERROR, "The module doesn't react to L0A input");
        --bcr_i <= '0';
        --l0a_i <= '0';
        --l0a_trig_i <= "0000";

        -----------------------------------------------------------------------

        log(ID_LOG_HDR, "Verifying that the frame start pulses are generated", C_SCOPE);
        --set_rst('0');
        await_value(frame_start_pulse_o, '1', 0 ns, C_MAX_FIRST_FRAME_PULSE_DELAY * C_CLK_PERIOD_40, ERROR,
                    "Initial frame_start_pulse has to arrive within " & to_string(C_MAX_FIRST_FRAME_PULSE_DELAY) & " clk cycles");
        log(ID_SEQUENCER, "Verifying that the frame start pulses arrive with the correct period", C_SCOPE);
        verify_frame_pulse_period(10);

        -----------------------------------------------------------------------

        log(ID_LOG_HDR, "Verifying that the frame start pulses synchronize to the BCR with the correct phase", C_SCOPE);
        log(ID_SEQUENCER, "Synchronization works when BCR pulse arrives and rst = '0", C_SCOPE);
        verify_frame_pulse_phase(reset => False);
        --log(ID_SEQUENCER, "Synchronization works when BCR pulse arrives and rst = '1", C_SCOPE);
        --verify_frame_pulse_phase(reset => True);

        -----------------------------------------------------------------------

        log(ID_LOG_HDR, "Checking the L0A mask and tags are generated correctly", C_SCOPE);
        generate_and_verify_l0a_frames(count => 100, bcr => false);

        -----------------------------------------------------------------------

        --        log(ID_LOG_HDR, "Verifying that the BCR delay is set correctly", C_SCOPE);
        --        verify_l0a_frames_with_bcr_delay(l0a_count => 64, bcr_delay_max => 16*4-1, bcr_period => 16);

        -----------------------------------------------------------------------

        log(ID_LOG_HDR, "SIMULATION COMPLETED");
        report_alert_counters(FINAL);
        uvvm_completed <= '1';
        wait for 1 us;
        clk_en <= false;
        wait;
    end process;

    clock_generator(clk, clk_en, C_CLK_PERIOD_40, "40 MHz BC clock");

    --generate frame_phase and bcr
    --bcr is a clock cycle ahead of the nearest frame_phase pulse
    process(clk)
        variable bcid : integer range 0 to 3563:=0; -- bunch counter
        variable bcid_vec : std_logic_vector(11 downto 0);
    begin
        if(rising_edge(clk)) then
            bcr_i <= '0';
            ITk_sync_i <= '0';


            if(bcid=3563) then
                bcid:=0;
            else
                bcid:=bcid+1;
            end if;

            if(bcid=0) then
                bcr_i <= '1';
            end if;

            if(bcr_i = '1') then
                bcr_expected <= "1";
            else
                bcr_expected <= "0";
            end if;

            bcid_vec := std_logic_vector(to_unsigned(bcid, 12));
            if( bcid_vec(1 downto 0) = "01") then
                ITk_sync_i <= '1';
            end if;

        end if;
    end process;


    --trigger mask and trigger tage generation is done centrally.
    dut : entity work.l0a_frame_generator
        port map(
            clk                 => clk,
            rst                 => rst, -- not used
            bcr_i               => bcr_i, --BCR should be a clock ahead of ITk_sync_i pulse
            l0a_i               => l0a_i, -- not used
            l0a_trig_i          => l0a_trig_i, --the bitmask is generated externally (centrally)
            bcr_delay_i         => bcr_delay_i, --not used
            frame_phase_i       => frame_phase_i, --not used
            l0id_i              => l0id_i, -- centrally-generated trigger tag
            frame_start_pulse_o => frame_start_pulse_o,
            ITk_sync_i          => ITk_sync_i, --generated by the same block as the trigger tags. Synchronous to the BCR (Turn Signal). pulses once in four clock cycles
            l0a_frame_o         => l0a_frame_o
        );

    -- verifies the l0a_frame_o contents
    L0A_frame_checker : process
    begin
        if (rising_edge(clk) and frame_start_pulse_o = '1') then
            check_value(l0a_frame_o(BCR_bits'range), bcr_expected, ERROR, "BCR field matches the expected value");

            if l0a_checker_en then
                log(ID_SEQUENCER_SUB, "L0A frame checker: verifying the previous L0A frame", C_SCOPE);
                check_value(l0a_frame_o(L0A_mask_bits'range), l0a_mask_expected, ERROR, "L0A frame mask matches the expected value");
                check_value(l0a_frame_o(L0tag_bits'range), l0_tag_expected, ERROR, "L0 tag matches the expected value");
            --wait for C_ONE_FRAME_DELAY;
            end if;
        end if;
        wait until rising_edge(clk);
    end process;

end architecture RTL;

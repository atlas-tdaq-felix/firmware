--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               jacopo pinzino
--!               Frans Schreuder
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--========================================================================================================================
-- Copyright (c) 2017 by Bitvis AS.  All rights reserved.
-- You should have received a copy of the license file containing the MIT License (see LICENSE.TXT), if not,
-- contact Bitvis AS <support@bitvis.no>.
--
-- UVVM AND ANY PART THEREOF ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
-- WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
-- OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH UVVM OR THE USE OR OTHER DEALINGS IN UVVM.
--========================================================================================================================

------------------------------------------------------------------------------------------
-- VHDL unit     : Bitvis IRQC Library : irqc_demo_tb
--
-- Description   : See dedicated powerpoint presentation and README-file(s)
------------------------------------------------------------------------------------------


library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

    use work.axi_stream_package.all;


-- Test case entity
entity amac_demo_tb is
    generic(
        use_vunit: boolean := false -- @suppress "Unused generic: use_vunit is not used in work.amac_demo_tb(func)"
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity;


-- Test case architecture
architecture func of amac_demo_tb is


    --entity EndeavourEncoder is
    --    generic (
    --        MAX_OUTPUT : integer := 8;
    --        DEBUG_en : boolean := false;
    --        DISTR_RAM : boolean := false
    --    );
    --    port (
    --        clk40 : in std_logic;
    --        LinkAligned : in std_logic; -- @suppress "Unused port: LinkAligned is not used in work.EndeavourEncoder(Behavioral)"
    --       s_axis_aclk : in std_logic;
    --       aresetn : in std_logic;
    --       rst     : in std_logic;
    --       s_axis : in axis_8_type;
    --       s_axis_tready : out std_logic;
    --       invert_polarity : in std_logic; -- invert link polatiry
    --       amac_signal : out std_logic;
    --       almost_full : out std_logic
    --   );

    -- DSP interface and general control signals
    signal clk40           : std_logic  := '0';
    signal m_axis_aclk           : std_logic  := '0';
    signal reset          : std_logic := '0';
    -- interface
    signal amac_signal : std_logic  := '0';
    signal to_amac : std_logic  := '0';
    signal test_link : std_logic  := '0'; -- @suppress "signal test_link is never read"
    -- output
    signal data_axi_fromhost : axis_8_type;
    signal data_ready_fromhost : std_logic  := '0';
    signal data_axi_tohost : axis_32_type;
    signal data_ready_tohost : std_logic  := '0';

    --signal data_valid : std_logic  := '0';
    --signal data_last : std_logic  := '0';
    --signal data_out : std_logic_vector(31 downto 0) := (others => '0');
    --signal data_user : std_logic_vector(2 downto 0) := (others => '0');
    --signal data_keep : std_logic_vector(3 downto 0) := (others => '0');

    -- Interrupt related signals
    signal clock_ena     : boolean   := false;

    constant C_CLK_PERIOD_40      : time := 25 ns;
    constant C_CLK_PERIOD_250     : time := 4 ns;
    constant LinkAligned          : std_logic := '1';
    signal clk40_stable : std_logic;
    signal daq_reset : std_logic;
    signal daq_fifo_flush : std_logic;
    signal aresetn : std_logic;
--constant BIT1_TIME            : time := 76 * C_CLK_PERIOD_40;
--constant BIT0_TIME            : time := 14 * C_CLK_PERIOD_40;
--constant INTRAWORD_GAP_TIME   : time := 43 * C_CLK_PERIOD_40;
--constant ENDWORD_GAP_TIME     : time := 100 * C_CLK_PERIOD_40;

begin
    -----------------------------------------------------------------------------
    -- Instantiate DUT
    -----------------------------------------------------------------------------

    resetmgr0: entity work.CRresetManager
        port map(
            clk40         => clk40,
            rst           => reset,
            clk40_stable  => clk40_stable,
            cr_rst        => daq_reset,
            cr_fifo_flush => daq_fifo_flush
        );

    Edecoding0: entity work.EndeavourDecoder
        generic map(
            DEBUG_en => FALSE,
            VERSAL => false
        )
        port map
      (
            clk40 => clk40,
            m_axis_aclk => m_axis_aclk, --: in std_logic;
            amac_signal => amac_signal, --: in std_logic_vector(32 downto 0);
            LinkAligned => LinkAligned, --: in std_logic;
            daq_fifo_flush => daq_fifo_flush,
            amac_rst => daq_reset,
            m_axis => data_axi_tohost, --m_axis(i,0), --: out axis_32_type;
            invert_polarity => '0',
            m_axis_tready => data_ready_tohost, --m_axis_tready(i,0) --: in std_logic;
            m_axis_prog_empty => open      );


    Eencoding0: entity work.EndeavourEncoder
        generic map (
            DEBUG_en => false,
            DISTR_RAM => false,
            USE_BUILT_IN_FIFO => '0'
        )
        port map
                (
            clk40 => clk40,
            LinkAligned => LinkAligned,
            s_axis_aclk => m_axis_aclk, --: in std_logic;
            reset => daq_reset,
            s_axis => data_axi_fromhost, --s_axis(i,0),
            s_axis_tready => data_ready_fromhost, --s_axis_tready(i,0),
            invert_polarity => '0',
            amac_signal => to_amac,
            almost_full => open
        );

    aresetn <= not reset;

    AmacChip: entity work.amac_chip
        Port map(
            clk40                  => clk40,
            signal_in              => to_amac,
            signal_out             => amac_signal,
            aresetn                => aresetn
        );


    -----------------------------------------------------------------------------
    -- Clock Generator
    -----------------------------------------------------------------------------
    clock_generator(clk40, clock_ena, C_CLK_PERIOD_40, "40 TB clock");
    clock_generator(m_axis_aclk, clock_ena, C_CLK_PERIOD_250, "250 TB clock");


    ------------------------------------------------
    -- PROCESS: p_main
    ------------------------------------------------
    p_main: process
        constant C_SCOPE     : string  := C_TB_SCOPE_DEFAULT;

        procedure set_inputs_passive is
        begin
            log(ID_SEQUENCER_SUB, "All inputs set passive", C_SCOPE);
        end;



    --variable v_time_stamp   : time := 0 ns;
    --variable v_irq_mask     : std_logic_vector(7 downto 0);
    --variable v_irq_mask_inv : std_logic_vector(7 downto 0);

    begin
        clk40_stable <= '0';
        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);

        enable_log_msg(ALL_MESSAGES);
        --disable_log_msg(ALL_MESSAGES);
        --enable_log_msg(ID_LOG_HDR);

        log(ID_LOG_HDR, "Simulation of TB for Endeveour", C_SCOPE);
        ------------------------------------------------------------

        set_inputs_passive;

        clock_ena <= true; -- to start clock generator
        clk40_stable <= '1';

        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= (others => '0');
        data_axi_fromhost.tvalid <= '0';

        wait for 1000 ns;

        gen_pulse(reset, '1', 10 * C_CLK_PERIOD_40, "Pulsed reset-signal - active for 10T");
        --v_time_stamp := now;  -- time from which irq2cpu should be stable off until triggered
        wait for 10 us;

        test_link <= '1';
        data_ready_tohost <= '1';


        log(ID_LOG_HDR, "Check defaults on output ports", C_SCOPE);
        --------------------------------------------------------------
        check_value(data_axi_tohost.tvalid, '0', ERROR, "Data valid inactive", C_SCOPE);
        check_value(data_axi_tohost.tlast, '0', ERROR, "Data last inactive", C_SCOPE);
        check_value(data_axi_tohost.tdata, X"00", ERROR, "Data out inactive", C_SCOPE);
        check_value(data_axi_tohost.tuser, "000", ERROR, "Data user inactive", C_SCOPE);
        --      check_value(data_axi_tohost.tkeep, X"0", WARNING, "Data keep inactive", C_SCOPE);


        log(ID_LOG_HDR, "Starting simulation", C_SCOPE);

        await_value(data_ready_fromhost, '1', 0 ns, 1 ms, ERROR, "waiting data ready from host", C_SCOPE);

        log(ID_LOG_HDR, "Read Request", C_SCOPE);

        gen_pulse(reset, '1', 10 * C_CLK_PERIOD_40, "Pulsed reset-signal - active for 10T");
        wait for 10 us;

        --  wait until data_ready_fromhost = '1';

        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "00000001";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "first part of message");
        await_value(data_ready_fromhost, '1', 0 ns, 1 ms, ERROR, "waiting data ready from host", C_SCOPE);

        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "00000001";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "second part of message");
        await_value(data_ready_fromhost, '1', 0 ns, 1 ms, ERROR, "waiting data ready from host", C_SCOPE);

        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "00000001";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "Send message byte");
        await_value(data_ready_fromhost, '1', 0 ns, 1 ms, ERROR, "waiting data ready from host", C_SCOPE);


        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "00000010";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "2 byte message");
        await_value(data_ready_fromhost, '1', 0 ns, 1 ms, ERROR, "waiting data ready from host", C_SCOPE);

        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "10111111";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "first part of read req");
        await_value(data_ready_fromhost, '1', 0 ns, 1 ms, ERROR, "waiting data ready from host", C_SCOPE);

        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "10011001";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "second part of read req");
        await_value(data_ready_fromhost, '1', 0 ns, 1 ms, ERROR, "waiting data ready from host", C_SCOPE);


        data_axi_fromhost.tlast <= '1';
        data_axi_fromhost.tdata <= "00000000";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "zero check");
        await_value(data_ready_fromhost, '1', 0 ns, 1 ms, ERROR, "waiting data ready from host", C_SCOPE);

        await_value(to_amac, '1', 0 ns, 1 ms, ERROR, "waiting data to_amac", C_SCOPE);

        wait until data_axi_tohost.tvalid = '1';
        --        await_value(data_axi_tohost.tvalid, '1', 0 ns, 1 ms, ERROR, "waiting data ready from host", C_SCOPE);
        check_value(data_axi_tohost.tvalid, '1', ERROR, "Data valid  reply read 0", C_SCOPE);
        check_value(data_axi_tohost.tdata, X"555555C9", ERROR, "Data out  reply read 0", C_SCOPE);
        check_value(data_axi_tohost.tuser, "0000", ERROR, "Data user  reply read 0", C_SCOPE);
        check_value(data_axi_tohost.tkeep, "1111", ERROR, "Data keep  reply read 0", C_SCOPE);
        check_value(data_axi_tohost.tlast, '0', ERROR, "Data last  reply read 1", C_SCOPE);
        wait until data_axi_tohost.tvalid = '1';
        --        await_value(data_axi_tohost.tvalid, '1', 0 ns, 1 ms, ERROR, "waiting data ready from host", C_SCOPE);
        check_value(data_axi_tohost.tvalid, '1', ERROR, "Data out  reply read 1", C_SCOPE);
        check_value(data_axi_tohost.tdata, X"55", ERROR, "Data out  reply read 1", C_SCOPE);
        check_value(data_axi_tohost.tuser, "0000", ERROR, "Data user  reply read 1", C_SCOPE);
        check_value(data_axi_tohost.tkeep, "0001", ERROR, "Data keep  reply read 1", C_SCOPE);
        check_value(data_axi_tohost.tlast, '1', ERROR, "Data last  reply read 1", C_SCOPE);

        log(ID_LOG_HDR, "Read next Request", C_SCOPE);

        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "00000001";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "first part of message");
        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "00000001";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "second part of message");
        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "00000001";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "Send message byte");
        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "00000001";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "1 byte message");


        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "10010011";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "read next req");


        data_axi_fromhost.tlast <= '1';
        data_axi_fromhost.tdata <= "00000000";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "zero check");

        --        wait until data_axi_tohost.tvalid = '1';
        await_value(data_axi_tohost.tvalid, '1', 0 ns, 1 ms, ERROR, "waiting data ready from host", C_SCOPE);
        check_value(data_axi_tohost.tvalid, '1', ERROR, "Data valid  reply read next 0", C_SCOPE);
        check_value(data_axi_tohost.tdata, X"555555CA", ERROR, "Data out  reply read next 0", C_SCOPE);
        check_value(data_axi_tohost.tuser, "0000", ERROR, "Data user  reply read next 0", C_SCOPE);
        check_value(data_axi_tohost.tkeep, "1111", ERROR, "Data keep  reply read next 0", C_SCOPE);
        check_value(data_axi_tohost.tlast, '0', ERROR, "Data last reply read next 1", C_SCOPE);

        wait for 4 ns;
        wait until data_axi_tohost.tvalid = '1';
        --        await_value(data_axi_tohost.tvalid, '1', 0 ns, 1 ms, ERROR, "waiting data ready from host", C_SCOPE);
        check_value(data_axi_tohost.tvalid, '1', ERROR, "Data valid  reply read next 1", C_SCOPE);
        check_value(data_axi_tohost.tdata, X"55", ERROR, "Data out  reply read next 1", C_SCOPE);
        check_value(data_axi_tohost.tuser, "0000", ERROR, "Data user  reply read next 1", C_SCOPE);
        check_value(data_axi_tohost.tkeep, "0001", ERROR, "Data keep  reply read next 1", C_SCOPE);
        check_value(data_axi_tohost.tlast, '1', ERROR, "Data last  reply read next 1", C_SCOPE);

        log(ID_LOG_HDR, "write Request", C_SCOPE);

        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "00000001";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "first part of message");
        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "00000001";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "second part of message");
        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "00000001";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "Send message byte");
        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "00000111";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "7 byte message");

        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "11110101";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "write req 0-8 bit");
        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "10010111";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "write req 8-16 bit");
        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "11111111";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "write req 16-24 bit");
        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "00000000";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "write req 24-32 bit");
        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "10100101";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "write req 32-40 bit");
        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "10111101";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "write req 40-48 bit");
        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "10101010";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "write req 48-56 bit");


        data_axi_fromhost.tlast <= '1';
        data_axi_fromhost.tdata <= "00000000";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "zero check");


        await_value(data_axi_tohost.tvalid, '1', 0 ns, 1 ms, ERROR, "waiting data ready from host", C_SCOPE);
        check_value(data_axi_tohost.tvalid, '1', ERROR, "Data valid  reply write", C_SCOPE);
        check_value(data_axi_tohost.tdata, X"CB", ERROR, "Data out  reply write", C_SCOPE);
        check_value(data_axi_tohost.tuser, "0000", ERROR, "Data user  reply write", C_SCOPE);
        check_value(data_axi_tohost.tkeep, "0001", ERROR, "Data keep  reply write", C_SCOPE);
        check_value(data_axi_tohost.tlast, '1', ERROR, "Data last  reply write", C_SCOPE);

        log(ID_LOG_HDR, "setID Request", C_SCOPE);


        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "00000001";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "first part of message");
        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "00000001";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "second part of message");
        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "00000001";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "Send message byte");
        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "00000111";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "7 byte message");



        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "11011101";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "setID req 0-8 bit");
        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "10100111";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "setID req 8-16 bit");
        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "11001111";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "setID req 16-24 bit");
        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "00000110";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "setID req 24-32 bit");
        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "00101101";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "setID req 32-40 bit");
        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "11101101";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "setID req 40-48 bit");
        data_axi_fromhost.tlast <= '0';
        data_axi_fromhost.tdata <= "01101010";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "setID req 48-56 bit");


        data_axi_fromhost.tlast <= '1';
        data_axi_fromhost.tdata <= "00000000";
        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "zero check");


        await_value(data_axi_tohost.tvalid, '1', 0 ns, 1 ms, ERROR, "waiting data ready from host", C_SCOPE);
        check_value(data_axi_tohost.tvalid, '1', ERROR, "Data valid  reply SetID", C_SCOPE);
        check_value(data_axi_tohost.tdata, X"CC", ERROR, "Data out  reply SetID", C_SCOPE);
        check_value(data_axi_tohost.tuser, "0000", ERROR, "Data user  reply SetID", C_SCOPE);
        check_value(data_axi_tohost.tkeep, "0001", ERROR, "Data keep  reply SetID", C_SCOPE);
        check_value(data_axi_tohost.tlast, '1', ERROR, "Data last  reply SetID", C_SCOPE);

        --        log(ID_LOG_HDR, "Send Erroneus Request", C_SCOPE);

        --        data_axi_fromhost.tlast <= '1';
        --        data_axi_fromhost.tdata <= "00011101";
        --        gen_pulse(data_axi_fromhost.tvalid, C_CLK_PERIOD_250, "send wrong first 3 bits");

        --        wait until data_axi_tohost.tvalid = '1';
        --        check_value(data_axi_tohost.tvalid, '1', ERROR, "Data valid  reply SetID", C_SCOPE);
        --        check_value(data_axi_tohost.tdata, X"33", ERROR, "Data out  reply SetID", C_SCOPE);
        --        check_value(data_axi_tohost.tuser, "0000", ERROR, "Data user  reply SetID", C_SCOPE);
        --        check_value(data_axi_tohost.tkeep, "0001", ERROR, "Data keep  reply SetID", C_SCOPE);
        --        check_value(data_axi_tohost.tlast, '1', ERROR, "Data last  reply SetID", C_SCOPE);


        --==================================================================================================
        -- Ending the simulation
        --------------------------------------------------------------------------------------
        wait for 1 us;             -- to allow some time for completion
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        -- Finish the simulation
        uvvm_completed <= '1';
        wait for 1 us;


        clock_ena <= false; -- to stop clock generator
        wait;  -- to stop completely

    end process p_main;

end func;

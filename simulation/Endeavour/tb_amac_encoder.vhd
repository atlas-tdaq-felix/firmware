--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

library uvvm_vvc_framework;
    use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

library bitvis_vip_axistream;
    context bitvis_vip_axistream.vvc_context;

library bitvis_vip_clock_generator;
    context bitvis_vip_clock_generator.vvc_context;

    use work.endeavour_package.all;
    use work.axi_stream_package.all;


entity tb_amac_encoder is
    generic(
        use_vunit: boolean := false -- @suppress "Unused generic: use_vunit is not used in work.tb_amac_encoder(func)"
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity;


architecture func of tb_amac_encoder is
    constant C_CLK_PERIOD          : time    := 25 ns;
    constant C_AXI_CLK_PERIOD      : time    := 15 ns;
    constant C_CYCLES_RST          : integer := 3;
    --constant C_STABLE_PERIODS      : integer := 10;
    --constant C_MAX_CYCLES_PER_BYTE : integer := 4;
    constant C_USER_WIDTH          : integer := 4;
    constant C_ID_WIDTH            : integer := 1;
    constant C_DEST_WIDTH          : integer := 1;
    constant C_DATA_WIDTH          : integer := 8;

    signal clk              : std_logic;
    signal m_axis_aclk      : std_logic;
    signal rst              : std_logic := '0';
    signal amac_signal      : std_logic_vector(0 downto 0) := (others => '0'); -- @suppress "signal amac_signal is never read"

    signal data_axi_fromhost : axis_8_type;

    signal encoder : t_axistream_if(tdata(C_DATA_WIDTH - 1 downto 0),
                                    tkeep((C_DATA_WIDTH / 8) - 1 downto 0),
                                    tuser(C_USER_WIDTH - 1 downto 0),
                                    tstrb((C_DATA_WIDTH / 8) - 1 downto 0),
                                    tid(C_ID_WIDTH - 1 downto 0),
                                    tdest(C_DEST_WIDTH - 1 downto 0)
                                   );

--constant C_START_TIMEOUT : time := TICKS_QUIESCENT * C_CLK_PERIOD;
--constant C_ONE_MAX_DURATION : time := TICKS_DAH_MAX * C_CLK_PERIOD;
--constant C_ZERO_MAX_DURATION : time := TICKS_DIT_MAX * C_CLK_PERIOD;
--constant C_ONE_MIN_DURATION : time := TICKS_DAH_MIN * C_CLK_PERIOD;
--constant C_ZERO_MIN_DURATION : time := TICKS_DIT_MIN * C_CLK_PERIOD;
--constant C_BIT_GAP_MAX_DURATION : time := TICKS_BITGAP_MAX * C_CLK_PERIOD;
--constant C_BIT_GAP_MIN_DURATION : time := TICKS_BITGAP_MIN * C_CLK_PERIOD;
--constant C_WORD_GAP_DURATION : time := TICKS_QUIESCENT * C_CLK_PERIOD;


begin

    main_test : process

        --procedure expect_byte(
        --    data : std_logic_vector(7 downto 0);
        --    signal amac : std_logic;
        --    last : boolean := false;
        --    first : boolean := false
        --) is
        --    variable transition_time, start_time : time ;
        --begin
        --    if first then -- this is the first byte in the sequence
        --        if amac = '1' then
        --            error("Expected AMAC line to be low when verifying first byte");
        --            return;
        --        end if;
        --
        --        start_time := now;
        --        wait until rising_edge(amac) for C_START_TIMEOUT;
        --        if now - start_time >= C_START_TIMEOUT then
        --            error("Expected AMAC to begin transmission before the timeout");
        --            return;
        --        end if;
        --    else
        --        if amac = '0' then
        --            error("Expected AMAC line to be high when verifying subsequent bytes");
        --            return;
        --        end if;
        --    end if;
        --
        --    transition_time := now;
        --
        --    for i in data'left downto data'right loop -- verify all bits
        --        if data(i) = '1' then
        --            wait until falling_edge(amac) for C_ONE_MAX_DURATION;
        --            if now - transition_time >= C_ONE_MAX_DURATION then
        --                error("AMAC 'ONE' pulse is too long");
        --                return;
        --            elsif now - transition_time < C_ONE_MIN_DURATION then
        --                error("AMAC 'ONE' pulse is too short");
        --                return;
        --            end if;
        --
        --        else
        --            wait until falling_edge(amac) for C_ZERO_MAX_DURATION;
        --            if now - transition_time >= C_ZERO_MAX_DURATION then
        --                error("AMAC 'ZERO' pulse is too long");
        --                return;
        --            elsif now - transition_time < C_ZERO_MIN_DURATION then
        --                error("AMAC 'ZERO' pulse is too short");
        --                return;
        --            end if;
        --        end if;
        --        transition_time := now;
        --
        --        if i /= data'right then
        --            wait until rising_edge(amac) for C_BIT_GAP_MAX_DURATION;
        --            if now - transition_time >= C_BIT_GAP_MAX_DURATION then
        --                error("AMAC bit gap is too long");
        --                return;
        --            elsif now - transition_time < C_BIT_GAP_MIN_DURATION then
        --                error("AMAC bit gap is too short");
        --                return;
        --            end if;
        --            transition_time := now;
        --        end if;
        --    end loop;
        --
        --    if last then -- this is the last byte in the sequence
        --        wait until rising_edge(amac) for C_WORD_GAP_DURATION;
        --        if now - transition_time < C_WORD_GAP_DURATION then
        --            error("Received unexpected extra data");
        --            return;
        --        end if;
        --    else
        --        wait until rising_edge(amac) for C_BIT_GAP_MAX_DURATION;
        --        if now - transition_time >= C_BIT_GAP_MAX_DURATION then
        --            error("AMAC bit gap is too long");
        --            return;
        --        elsif now - transition_time < C_BIT_GAP_MIN_DURATION then
        --            error("AMAC bit gap is too short");
        --            return;
        --        end if;
        --    end if;
        --
        --    log(ID_SEQUENCER_SUB, "Received 0x" & to_hstring(data) & " from AMAC");
        --end;


        procedure send_and_verify_random_word(
            length_bytes : integer
        ) is
        --variable bytes : t_slv_array(0 to 127)(7 downto 0);
        begin
        --for i in 0 to length_bytes-1 loop
        --    bytes(i) := random(8);
        --end loop;
        --      axistream_transmit(AXISTREAM_VVCT, 3, x”01”, "Send data to DUT"); -- DAN_TEST
        --      axistream_transmit(AXISTREAM_VVCT, 3, x”01”, "Send data to DUT"); -- DAN_TEST DAN_TEST
        --      axistream_transmit(AXISTREAM_VVCT, 3, x”01”, "Send data to DUT"); -- DAN_TEST
        --      axistream_transmit(AXISTREAM_VVCT, 3, x”01”, "Send data to DUT"); -- DAN_TEST
        --axistream_transmit(AXISTREAM_VVCT, 3, bytes(0 to length_bytes-1), "Send data to DUT");--@suppress
        --      axistream_transmit(AXISTREAM_VVCT, 3, x”00”, "Send data to DUT"); -- DAN_TEST
        --await_completion(AXISTREAM_VVCT, 3, C_AXI_CLK_PERIOD * 10 * length_bytes);--@suppress

        --for i in 0 to length_bytes-1 loop
        --    expect_byte(bytes(i), amac_signal(0), first => (i = 0), last => (i = length_bytes-1));
        --end loop;
        end;
    begin

        await_uvvm_initialization(VOID);
        start_clock(CLOCK_GENERATOR_VVCT, 1, "Start clock generator (BC clock)");--@suppress
        start_clock(CLOCK_GENERATOR_VVCT, 2, "Start clock generator (AXI clock)");--@suppress

        -- Print the configuration to the log
        --report_global_ctrl(VOID);
        --report_msg_id_panel(VOID);

        disable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        enable_log_msg(ID_SEQUENCER);
        enable_log_msg(ID_SEQUENCER_SUB);
        --enable_log_msg(ID_UVVM_SEND_CMD);

        disable_log_msg(CLOCK_GENERATOR_VVCT, 1, ALL_MESSAGES);--@suppress
        enable_log_msg(CLOCK_GENERATOR_VVCT, 1, ID_BFM);--@suppress
        disable_log_msg(CLOCK_GENERATOR_VVCT, 2, ALL_MESSAGES);--@suppress
        enable_log_msg(CLOCK_GENERATOR_VVCT, 2, ID_BFM);--@suppress
        disable_log_msg(AXISTREAM_VVCT, 3, ALL_MESSAGES);--@suppress
        enable_log_msg(AXISTREAM_VVCT, 3, ID_BFM);--@suppress


        log(ID_LOG_HDR, "Check that ready='1' when rst='1'", C_SCOPE);
        wait until rising_edge(clk);
        rst <= '1';
        wait_num_rising_edge(clk, 20);
        check_value(encoder.tready, '1', ERROR, "ready_o = '1' when rst = '1'");
        rst <= '0';
        wait_num_rising_edge(clk, 20);

        gen_pulse(rst, C_CYCLES_RST * C_CLK_PERIOD, "Reset DUT");

        wait for 1 us;


        for j in 1 to 10 loop
            log(ID_SEQUENCER, "Sending " & to_string(j) & "-byte words", C_SCOPE);
            for i in 0 to 4 loop
                send_and_verify_random_word(length_bytes => j);
            end loop;
        end loop;


        wait for 1000 ns;                -- to allow some time for completion
        report_alert_counters(FINAL);   -- Report final counters and print conclusion for simulation (Success/Fail)

        uvvm_completed <= '1';
        wait for 1 us;
        stop_clock(CLOCK_GENERATOR_VVCT, 1, "Stop clock generator (BC clock)");--@suppress
        stop_clock(CLOCK_GENERATOR_VVCT, 2, "Stop clock generator (AXI clock)");--@suppress
        wait;

    end process;


    data_axi_fromhost.tdata <= encoder.tdata;
    encoder.tstrb <= (others => '1');
    encoder.tid   <= (others => '1');
    encoder.tdest <= (others => '1');
    encoder.tuser <= (others => '0');
    data_axi_fromhost.tlast <= encoder.tlast;
    data_axi_fromhost.tvalid <= encoder.tvalid;

    DUT : entity work.EndeavourEncoder
        generic map (
            DEBUG_en => false,
            DISTR_RAM => false,
            USE_BUILT_IN_FIFO => '0')
        port map (
            clk40 => clk,
            LinkAligned => '1',
            s_axis_aclk => m_axis_aclk,
            reset  => rst,
            s_axis => data_axi_fromhost,
            s_axis_tready => encoder.tready,
            invert_polarity => '0',
            amac_signal => amac_signal(0),
            almost_full => open
        );

    i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

    i_clock_generator_vvc : entity bitvis_vip_clock_generator.clock_generator_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_INSTANCE_IDX    => 1,
            GC_CLOCK_NAME      => "Clock",
            GC_CLOCK_PERIOD    => C_CLK_PERIOD,
            GC_CLOCK_HIGH_TIME => C_CLK_PERIOD / 2
        )
        port map(
            clk => clk
        );

    i_axi_clock_generator_vvc : entity bitvis_vip_clock_generator.clock_generator_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_INSTANCE_IDX    => 2,
            GC_CLOCK_NAME      => "Clock",
            GC_CLOCK_PERIOD    => C_AXI_CLK_PERIOD,
            GC_CLOCK_HIGH_TIME => C_AXI_CLK_PERIOD / 2
        )
        port map(
            clk => m_axis_aclk
        );

    i_axistream_vvc : entity bitvis_vip_axistream.axistream_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_AXISTREAM_BFM_CONFIG, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => true,
            GC_DATA_WIDTH    => C_DATA_WIDTH,
            GC_USER_WIDTH    => C_USER_WIDTH,
            GC_ID_WIDTH      => C_ID_WIDTH,
            GC_DEST_WIDTH    => C_DEST_WIDTH,
            GC_INSTANCE_IDX  => 3)
        port map(
            clk              => m_axis_aclk,
            axistream_vvc_if => encoder
        );


end func;

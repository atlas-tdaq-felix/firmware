--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

Library xpm;
    use xpm.vcomponents.all;

    use work.endeavour_package.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

entity tb_amac_deglitcher is
    generic(
        use_vunit: boolean := false -- @suppress "Unused generic: use_vunit is not used in work.tb_amac_deglitcher(Behavioral)"
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity tb_amac_deglitcher;

architecture Behavioral of tb_amac_deglitcher is

    constant C_CLK_PERIOD_40      : time := 25 ns;
    constant MAX_GLITCH_TIME      : time := 100 ns;

    signal clk     : std_logic;
    signal clk_en  : boolean;
    signal rst     : std_logic := '0';
    signal datain  : std_logic_vector(1 downto 0) := "00";
    signal dataout : std_logic;

begin

    test : process is

        procedure generate_pulses(
            num_periods   : integer;
            pulse_length_high : integer;
            pulse_length_low : integer
        ) is
            variable done : boolean;
            variable tick : integer;
            --variable tick_time : integer;
            variable phase : integer;
            variable period : integer;
        --variable overflow : boolean;
        begin
            done := false;
            period := pulse_length_high + pulse_length_low;
            tick := 0;
            while (not done) loop
                phase := tick mod period;

                if phase < pulse_length_high and tick < period * num_periods  then
                    datain <= "11";
                else
                    datain <= "00";
                end if;

                if tick >= period * num_periods then
                    done := true;
                end if;

                tick := tick + 1;
                wait until rising_edge(clk);
            end loop;
            datain <= b"00";
        end procedure;

    begin
        disable_log_msg(ID_POS_ACK);
        clk_en <= true;
        log(ID_LOG_HDR, "SIMULATION STARTED");

        gen_pulse(rst, '1', 10 * C_CLK_PERIOD_40, "Keep rst=1 for 10 CLK cycles");

        -- check what happens on positive pulses
        for i in 1 to 6 loop
            wait_num_rising_edge(clk, 20);
            generate_pulses(num_periods => 5, pulse_length_high => i, pulse_length_low => 10);
        end loop;

        -- check what happens on negative pulses
        for i in 1 to 6 loop
            wait_num_rising_edge(clk, 20);
            generate_pulses(num_periods => 5, pulse_length_high => 10, pulse_length_low => i);
        end loop;


        log(ID_LOG_HDR, "SIMULATION COMPLETED");
        report_alert_counters(FINAL);
        uvvm_completed <= '1';
        wait for 1 us;
        clk_en <= false;
        wait;
    end process;

    checker : process is
        variable start_time : time;
        variable end_time : time;
    begin
        if dataout = '1' then
            wait until falling_edge(dataout);
        end if;

        while true loop
            wait until rising_edge(dataout);
            start_time := now;
            wait until falling_edge(dataout);
            end_time := now;
            if (end_time - start_time < MAX_GLITCH_TIME) then
                error("Detected glitch shorter than maximum allowed time");
            end if;
        end loop;
    end process;

    DUT : entity work.EndeavourDeglitcher
        port map (
            clk40   => clk,
            rst     => rst,
            datain  => datain,
            dataout => dataout
        );

    clock_generator(clk, clk_en, C_CLK_PERIOD_40, "40 MHz BC clock");

end architecture Behavioral;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               jacopo pinzino
--!               Frans Schreuder
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 10/30/2019 03:36:26 PM
-- Design Name:
-- Module Name: EndeavourDecoder - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity amac_chip is
    port (
        clk40 : in std_logic;
        signal_in : in std_logic;
        signal_out : out std_logic;
        aresetn : in std_logic
    );
end amac_chip;

architecture Behavioral of amac_chip is

    --------------------------------- SOGLIE DA CAMBIARE IN BASE AL CLOCK --------------------------------------------------------
    --constant TICKS_QUIESCENT : integer := 100;  -- 512 + ~40% = 704 (1011000000)
    --constant TICKS_TX2RX     : integer := 4;    -- =50ns
    constant TICKS_DIT_MIN : integer := 6;     -- (*2 for 80MHz)
    constant TICKS_DIT_MAX : integer := 22;    -- 4+16=20 (*2)
    constant TICKS_DIT_MID : integer := (TICKS_DIT_MIN + TICKS_DIT_MAX)/2; --24  0
    constant TICKS_DAH_MIN : integer := 30;     --4+16+4=24 (*2)
    constant TICKS_DAH_MAX : integer := 124;    --4+16+4+96=120 (*2)
    constant TICKS_DAH_MID : integer := (TICKS_DAH_MIN + TICKS_DAH_MAX)/2; --144 1
    constant TICKS_BITGAP_MIN : integer := 11;   -- (*2)
    constant TICKS_BITGAP_MAX : integer := 75;   -- 64+8=72 (*2)
    constant TICKS_BITGAP_MID : integer := (TICKS_BITGAP_MIN + TICKS_BITGAP_MAX)/2; --
    constant AMACID : std_logic_vector(4 downto 0) := "11001";
    constant DATAREP : std_logic_vector(31 downto 0) := X"55555555";


    type fsm_AMAC_t is (idle, readbit, readgap, idreply0, idreply1, idreplygap, seqreply0, seqreply1, seqreplygap, datareply0, datareply1, datareplygap, errorstate); --, intertransactgap);
    signal fsm_AMAC : fsm_AMAC_t := idle;

    signal nmaxbit, nbitrd, nclock : std_logic_vector(7 downto 0) := (others => '0'); -- @suppress "signal nmaxbit is never read"
    signal command, seqnum : std_logic_vector(2 downto 0) := (others => '0');

    signal signalin_del, signalin_rising, signalin_falling: std_logic;


begin

    amac_proc: process(clk40)
        variable bitreply  : integer range 0 to 31 := 4;

    begin
        if rising_edge(clk40) then ------------------------sistemare il fifo full
            signalin_del <= signal_in;
            signalin_rising  <= signal_in and not(signalin_del);
            signalin_falling  <= signalin_del and not(signal_in);
            signal_out <= '0';

            if (aresetn = '0') then
                fsm_AMAC        <= idle;

                signal_out    <= '0';
                nmaxbit   <= (others => '0');
                nbitrd   <= (others => '0');
                bitreply := 4;
                nclock   <= (others => '0');
                command   <= (others => '0');
                seqnum   <= (others => '0');

            else
                case fsm_AMAC is

                    when idle =>
                        fsm_AMAC <= idle;

                        signal_out    <= '0';
                        nmaxbit   <= (others => '0');
                        nbitrd   <= (others => '0');
                        bitreply := 4;
                        command   <= (others => '0');
                        nclock   <= (others => '0');
                        if(signalin_rising = '1') then
                            fsm_AMAC <= readbit;
                            nclock <= nclock + '1';
                            seqnum <= seqnum + '1';
                        end if;


                    when readbit =>  ----------------------------------------------------------- conto fino a quando il segnale non va a zero per decodificare il bit
                        fsm_AMAC  <= readbit;
                        nclock <= nclock + '1';
                        if (signalin_falling = '1') then
                            nclock <= X"01";
                            nbitrd <= nbitrd + '1';
                            if(nbitrd < X"03") then
                                if (nclock >X"06" and nclock< X"16") then      -------- 0
                                    command <= command(1 downto 0) & '0';
                                    fsm_AMAC <= readgap;
                                elsif (nclock >X"1D" and nclock< X"7C") then   -------- 1
                                    command <= command(1 downto 0) & '1';
                                    fsm_AMAC <= readgap;
                                else                                           -------- error
                                    fsm_AMAC <= errorstate;
                                end if;
                            else
                                fsm_AMAC <= readgap;
                            end if;
                        elsif (nclock > X"7D") then
                            fsm_AMAC <= errorstate;
                        end if;

                    when readgap =>
                        fsm_AMAC  <= readgap;
                        nclock <= nclock + '1';
                        if (nclock > X"4B") then        -------- big gap
                            nclock <= (others => '0');
                            if(AMACID(bitreply) = '0') then
                                fsm_AMAC <= idreply0;
                            else
                                fsm_AMAC <= idreply1;
                            end if;
                            case command is
                                when "101" =>
                                    if(nbitrd /= X"10") then
                                        fsm_AMAC <= errorstate;
                                    end if;
                                when "100" =>
                                    if(nbitrd /= X"08") then
                                        fsm_AMAC <= errorstate;
                                    end if;
                                when "110" | "111" =>
                                    if(nbitrd /= X"38") then
                                        fsm_AMAC <= errorstate;
                                    end if;
                                when "000" =>
                                    if(nbitrd /= X"08") then
                                        fsm_AMAC <= errorstate;
                                    end if;
                                when others =>
                                    fsm_AMAC <= errorstate;
                            end case;
                        elsif (signalin_rising = '1') then
                            if(nclock > X"0B") then       -------- small gap
                                fsm_AMAC  <= readbit;
                                nclock <= X"01";
                            else                          -------- error
                                fsm_AMAC <= errorstate;
                            end if;
                        end if;

                    when idreply0 =>
                        fsm_AMAC  <= idreply0;
                        nclock <= nclock + '1';
                        signal_out <= '1';
                        if(nclock  > TICKS_DIT_MID) then
                            nclock <= (others => '0');
                            if(bitreply = 0) then
                                if(command = "000") then
                                    bitreply := 0;
                                else
                                    bitreply := 2;
                                end if;
                                fsm_AMAC  <= seqreplygap;
                            else
                                bitreply := bitreply - 1;
                                fsm_AMAC <= idreplygap;
                            end if;
                        end if;

                    when idreply1 =>
                        fsm_AMAC  <= idreply1;
                        nclock <= nclock + '1';
                        signal_out <= '1';
                        if(nclock  > TICKS_DAH_MID) then
                            nclock   <= (others => '0');
                            if(bitreply = 0) then
                                if(command = "000") then
                                    bitreply := 0;
                                else
                                    bitreply := 2;
                                end if;
                                fsm_AMAC  <= seqreplygap;
                            else
                                bitreply := bitreply - 1;
                                fsm_AMAC  <= idreplygap;
                            end if;
                        end if;

                    when idreplygap =>
                        fsm_AMAC  <= idreplygap;
                        nclock <= nclock + '1';
                        if(nclock  > TICKS_BITGAP_MID) then
                            nclock   <= (others => '0');
                            if(AMACID(bitreply) = '0') then
                                fsm_AMAC <= idreply0;
                            else
                                fsm_AMAC <= idreply1;
                            end if;
                        end if;

                    when seqreplygap =>
                        fsm_AMAC  <= seqreplygap;
                        nclock <= nclock + '1';
                        if(nclock  > TICKS_BITGAP_MID) then
                            nclock   <= (others => '0');
                            if(seqnum(bitreply) = '0') then
                                fsm_AMAC <= seqreply0;
                            else
                                fsm_AMAC <= seqreply1;
                            end if;
                        end if;

                    when seqreply0 =>
                        fsm_AMAC  <= seqreply0;
                        nclock <= nclock + '1';
                        signal_out <= '1';
                        if(nclock  > TICKS_DIT_MID) then
                            nclock   <= (others => '0');
                            if(bitreply = 0) then
                                if(command = "101" or command ="100") then
                                    bitreply := 31;
                                    fsm_AMAC  <= datareplygap;
                                else
                                    fsm_AMAC  <= idle;
                                end if;
                            else
                                bitreply := bitreply - 1;
                                fsm_AMAC <= seqreplygap;
                            end if;
                        end if;

                    when seqreply1 =>
                        fsm_AMAC  <= seqreply1;
                        nclock <= nclock + '1';
                        signal_out <= '1';
                        if(nclock  > TICKS_DAH_MID) then
                            nclock   <= (others => '0');
                            if(bitreply = 0) then
                                if(command = "101" or command ="100") then
                                    bitreply := 31;
                                    fsm_AMAC  <= datareplygap;
                                else
                                    fsm_AMAC  <= idle;
                                end if;
                            else
                                bitreply := bitreply - 1;
                                fsm_AMAC  <= seqreplygap;
                            end if;
                        end if;

                    when datareplygap =>
                        fsm_AMAC  <= datareplygap;
                        nclock <= nclock + '1';
                        if(nclock  > TICKS_BITGAP_MID) then
                            nclock   <= (others => '0');
                            if(DATAREP(bitreply) = '0') then
                                fsm_AMAC <= datareply0;
                            else
                                fsm_AMAC <= datareply1;
                            end if;
                        end if;

                    when datareply0 =>
                        fsm_AMAC  <= datareply0;
                        nclock <= nclock + '1';
                        signal_out <= '1';
                        if(nclock  > TICKS_DIT_MID) then
                            nclock   <= (others => '0');
                            if(bitreply = 0) then
                                fsm_AMAC  <= idle;
                            else
                                bitreply := bitreply - 1;
                                fsm_AMAC  <= datareplygap;
                            end if;
                        end if;

                    when datareply1 =>
                        fsm_AMAC  <= datareply1;
                        nclock <= nclock + '1';
                        signal_out <= '1';
                        if(nclock  > TICKS_DAH_MID) then
                            nclock   <= (others => '0');
                            if(bitreply = 0) then
                                fsm_AMAC  <= idle;
                            else
                                bitreply := bitreply - 1;
                                fsm_AMAC  <= datareplygap;
                            end if;
                        end if;

                    when errorstate =>           -- wait a little bit for things to settle -- @suppress "Dead state 'errorstate': state does not have outgoing transitions"
                        fsm_AMAC <= errorstate;


                end case;
            end if;
        end if;

    end process;


end Behavioral;

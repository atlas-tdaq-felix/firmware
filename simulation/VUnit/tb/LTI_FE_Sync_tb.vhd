
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
library xpm;
    use xpm.vcomponents.all;
library std;
    use std.env.all;
library lib;
--

library vunit_lib;
    context vunit_lib.vunit_context; --@suppress


entity LTI_FE_Sync_tb is
    generic (
        runner_cfg : string
    );
end entity LTI_FE_Sync_tb;

architecture rtl of LTI_FE_Sync_tb is
    signal clka, clkb: std_logic := '0';
    constant clk_period: time := 4 ns;
    constant width: integer := 33;
    signal din, dout: std_logic_vector(width-1 downto 0) := (others => '0');
    signal latency: std_logic_vector(width-1 downto 0);
begin
    clka <= not clka after clk_period/2;
    clkb <= clka after 1 ns;

    process(clka)
    begin
        if rising_edge(clka) then
            din <= din + 1;
        end if;
    end process;

    process(clkb)
    begin
        if rising_edge(clkb) then
            latency <= din - dout;
        end if;
    end process;

    uut: entity work.LTI_FE_Sync
        generic map(
            width => width
        )
        port map(
            clka => clka,
            clkb => clkb,
            din => din,
            dout => dout,
            training => '1'
        );

    main_proc: process
        variable lat: std_logic_vector(width-1 downto 0);
    begin
        report "VUnit initializing";
        test_runner_setup(runner, runner_cfg);--@suppress
        report "VUnit initialized";
        wait for clk_period*32;
        lat := latency;
        for i in 0 to 10000 loop
            if lat /= latency then
                report "Latency value is not consistent" severity ERROR;
            else
                report "Latency value is consistent: " & to_hstring(latency) severity NOTE;
            end if;
            wait until rising_edge(clkb);
        end loop;
        test_runner_cleanup(runner);--@suppress
        -- Finish the simulation
        std.env.stop;
        wait;
    end process;

end architecture rtl;


--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--========================================================================================================================
-- Copyright (c) 2017 by Bitvis AS.  All rights reserved.
-- You should have received a copy of the license file containing the MIT License (see LICENSE.TXT), if not,
-- contact Bitvis AS <support@bitvis.no>.
--
-- UVVM AND ANY PART THEREOF ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
-- WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
-- OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH UVVM OR THE USE OR OTHER DEALINGS IN UVVM.
--========================================================================================================================

------------------------------------------------------------------------------------------
-- VUnit wrapper for i2c_tb
------------------------------------------------------------------------------------------


library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use std.env.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;
library bitvis_vip_i2c;
    use bitvis_vip_i2c.i2c_bfm_pkg.all;

library lib;
--

library vunit_lib;
    context vunit_lib.vunit_context;

-- Test case entity
entity pex_init_vunit_tb is
    generic (
        runner_cfg : string
    );
end entity;

architecture func of pex_init_vunit_tb is
    signal uvvm_completed : std_logic;
    constant C_CLK_PERIOD_40 : time := 25 ns;
    constant C_SCOPE     : string  := C_TB_SCOPE_DEFAULT;

    signal I2C_SMB, I2C_SMBUS_CFG_nEN, MGMT_PORT_EN, PCIE_PERSTn1, PCIE_PERSTn2, PEX_PERSTn, PEX_SCL, PEX_SDA: std_logic;
    signal PORT_GOOD: std_logic_vector(7 downto 0);
    signal SHPC_INT: std_logic;
    signal clk40: std_logic;
    signal lnk_up0, lnk_up1: std_logic;
    signal sys_reset_n: std_logic;

    constant C_I2C_BFM_CONFIG : t_i2c_bfm_config := (
                                                      enable_10_bits_addressing       => false,
                                                      master_sda_to_scl               => 10 ms,
                                                      master_scl_to_sda               => 10 ms,
                                                      master_stop_condition_hold_time => 1 ms,
                                                      max_wait_scl_change             => 10 ms,
                                                      max_wait_scl_change_severity    => failure,
                                                      max_wait_sda_change             => 10 ms,
                                                      max_wait_sda_change_severity    => failure,
                                                      i2c_bit_time                    => 5 us,
                                                      i2c_bit_time_severity           => failure,
                                                      acknowledge_severity            => failure,
                                                      slave_mode_address              => "0000111010",
                                                      slave_mode_address_severity     => failure,
                                                      slave_rw_bit_severity           => failure,
                                                      reserved_address_severity       => warning,
                                                      match_strictness                => MATCH_EXACT,
                                                      id_for_bfm                      => ID_BFM,
                                                      id_for_bfm_wait                 => ID_BFM_WAIT,
                                                      id_for_bfm_poll                 => ID_BFM_POLL
                                                    );
    signal i2c_if : t_i2c_if;

begin

    i2c_if.scl <= '0' when PEX_SCL = '0' else 'H';
    i2c_if.sda <= '0' when PEX_SDA = '0' else 'H';

    uut_tb: entity work.pex_init
        generic map(
            CARD_TYPE => 712
        )
        port map(
            I2C_SMB           => I2C_SMB           ,--: out    std_logic;
            I2C_SMBUS_CFG_nEN => I2C_SMBUS_CFG_nEN ,--: out    std_logic;
            MGMT_PORT_EN      => MGMT_PORT_EN      ,--: out    std_logic;
            PCIE_PERSTn1      => PCIE_PERSTn1      ,--: out    std_logic;
            PCIE_PERSTn2      => PCIE_PERSTn2      ,--: out    std_logic;
            PEX_PERSTn        => PEX_PERSTn        ,--: out    std_logic;
            PEX_SCL           => PEX_SCL           ,--: out    std_logic;
            PEX_SDA           => PEX_SDA           ,--: inout  std_logic;
            select_bifurcation => (others => '1'),
            PORT_GOOD         => PORT_GOOD         ,--: in     std_logic_vector(7 downto 0);
            SHPC_INT          => SHPC_INT          ,--: out    std_logic;
            clk40             => clk40             ,--: in     std_logic;
            lnk_up0           => lnk_up0           ,--: in     std_logic;
            lnk_up1           => lnk_up1           ,--: in     std_logic;
            --reset_pcie      => --reset_pcie      ,--  : out    std_logic_vector(0 downto 0);
            sys_reset_n       => sys_reset_n       );--: in     std_logic);

    clk40_proc: process is
    begin
        clk40 <= '0';
        wait for C_CLK_PERIOD_40/2;
        clk40 <= '1';
        wait for C_CLK_PERIOD_40/2;

    end process;

    vunit_main : process is

    begin
        report "VUnit initializing";
        test_runner_setup(runner, runner_cfg);
        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);
        enable_log_msg(ALL_MESSAGES);
        report "VUnit initialized";
        log(ID_LOG_HDR, "Simulation of TB for I2C", C_SCOPE);
        sys_reset_n <= '0';
        lnk_up0 <= '0';
        lnk_up1 <= '0';
        PORT_GOOD <= x"00";
        wait for C_CLK_PERIOD_40 * 1000;
        sys_reset_n <= '1';
        lnk_up0 <= '1';
        lnk_up1 <= '1';
        PORT_GOOD <= x"CE";
        wait for C_CLK_PERIOD_40 * 100 * 8000;
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);
        report "VUnit UVVM done, cleaning up";
        test_runner_cleanup(runner);

    end process vunit_main;

    slave_proc: process
        variable v_data_out: t_byte_array(0 to 7);
        constant NUM_WRITES: integer := 13;
        type t_receive_array is array(0 to NUM_WRITES-1) of t_byte_array(0 to 7);
        variable v_data_to_receive: t_receive_array := ((x"03", x"00", x"3C", x"D6", x"00", x"00", x"00", x"03"),
                                                        (x"03", x"00", x"3C", x"D6", x"00", x"00", x"00", x"03"),
                                                        (x"03", x"00", x"3C", x"C0", x"00", x"00", x"00", x"12"),
                                                        (x"03", x"00", x"3C", x"C5", x"01", x"00", x"03", x"03"),
                                                        (x"03", x"00", x"3C", x"D6", x"00", x"00", x"00", x"03"),
                                                        (x"03", x"00", x"3C", x"C0", x"00", x"00", x"00", x"12"),
                                                        (x"03", x"00", x"3C", x"C5", x"01", x"00", x"03", x"03"),
                                                        (x"03", x"00", x"3C", x"D8", x"00", x"00", x"00", x"00"),
                                                        (x"03", x"00", x"3C", x"D9", x"00", x"00", x"00", x"01"),
                                                        (x"03", x"00", x"3C", x"E0", x"00", x"00", x"01", x"01"),
                                                        (x"03", x"00", x"3C", x"E1", x"00", x"00", x"02", x"02"),
                                                        (x"03", x"00", x"3C", x"D6", x"00", x"00", x"00", x"03"),
                                                        (x"03", x"00", x"3C", x"EB", x"00", x"00", x"00", x"01"));


    --constant c_nulldata: t_byte_array(0 to -1) := (others => x"00"); -- @suppress "Null range: The left argument is strictly larger than the right"
    begin
        i2c_if <= init_i2c_if_signals(VOID);

        for write in 0 to NUM_WRITES-1 loop
            i2c_slave_receive(v_data_out, "Receive from master", i2c_if, C_SCOPE, shared_msg_id_panel,C_I2C_BFM_CONFIG);
            check_value(v_data_out, v_data_to_receive(write), ERROR, "Checking received data on slave", C_SCOPE);
        end loop;
        wait;
    end process;

end architecture;



--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--========================================================================================================================
-- Copyright (c) 2017 by Bitvis AS.  All rights reserved.
-- You should have received a copy of the license file containing the MIT License (see LICENSE.TXT), if not,
-- contact Bitvis AS <support@bitvis.no>.
--
-- UVVM AND ANY PART THEREOF ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
-- WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
-- OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH UVVM OR THE USE OR OTHER DEALINGS IN UVVM.
--========================================================================================================================

------------------------------------------------------------------------------------------
-- VUnit wrapper for tb_strips_configuration_decoder
------------------------------------------------------------------------------------------


library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;

library lib;
--

library vunit_lib;
    context vunit_lib.vunit_context;

-- Test case entity
entity tb_strips_configuration_decoder_vunit is
    generic (
        runner_cfg : string
    );
end entity;

architecture func of tb_strips_configuration_decoder_vunit is
    signal uvvm_completed : std_logic;
    constant C_CLK_PERIOD_250 : time := 4 ns;
begin

    uut_tb: entity work.tb_strips_configuration_decoder
        generic map(
            use_vunit => true
        )
        port map(
            uvvm_completed => uvvm_completed
        );


    vunit_main : process is
    begin
        report "VUnit initializing";
        test_runner_setup(runner, runner_cfg);
        report "VUnit initialized";
        while uvvm_completed /= '1' loop
            wait for C_CLK_PERIOD_250;
        end loop;
        report "VUnit UVVM done, cleaning up";
        test_runner_cleanup(runner);

    end process vunit_main;

end architecture;



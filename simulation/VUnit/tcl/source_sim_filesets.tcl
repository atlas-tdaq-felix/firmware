#proc sim_filesets {} {


source $PROJECT_ROOT/scripts/helper/clear_filesets.tcl
source $PROJECT_ROOT/scripts/filesets/64b66b_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/core1990_interlaken_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/crFromHost_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/crToHost_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/decoding_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/encoding_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/encrd53_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/endeavour_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/fullmode_emulator_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/fullmode_fanout_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/fullmode_gbt_core_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/fullmode_toplevel_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/gbt_core_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/gbt_emulator_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/gbt_fanout_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/gbt_toplevel_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/housekeeping_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/itk_strips_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/lpgbt_core_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/lpgbt_fe_core_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/ltittc_wrapper_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/ttc_decoder_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/ttc_emulator_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/wupper_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/bcm_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/felig_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/fmemu_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/fmemu_top_fileset.tcl
source $PROJECT_ROOT/scripts/filesets/lpgbt_fe_core_fileset.tcl

set SIM_ARCH "KU"
if { $SIM_ARCH == "V7" } {
    set XCI_FILES [concat $XCI_FILES $XCI_FILES_V7]
    set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_V7]
} elseif { $SIM_ARCH == "KU" } {
    set XCI_FILES [concat $XCI_FILES $XCI_FILES_KU]
    set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_KU]
} elseif { $SIM_ARCH == "VU37P" } {
    set XCI_FILES [concat $XCI_FILES $XCI_FILES_VU37P]
    set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_VU37P]
} elseif { $SIM_ARCH == "VERSAL" } {
    set XCI_FILES [concat $XCI_FILES $XCI_FILES_VERSAL]
    set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_VERSAL]
}

set SOURCE_FILES ""
foreach VHDL_FILE $VHDL_FILES {
    set SOURCE_FILES [concat $SOURCE_FILES "${PROJECT_ROOT}/sources/${VHDL_FILE}"]
}

foreach VERILOG_FILE $VERILOG_FILES {
    set SOURCE_FILES [concat $SOURCE_FILES "${PROJECT_ROOT}/sources/${VERILOG_FILE}"]
}

foreach SIM_FILE $SIM_FILES {
    set SOURCE_FILES [concat $SOURCE_FILES "${PROJECT_ROOT}/simulation/${SIM_FILE}"]
}

file mkdir  ${PROJECT_ROOT}/simulation/VUnit/vunit_out/modelsim
foreach MEM_FILE $MEM_FILES {
    #For modelsim
    file copy -force ${PROJECT_ROOT}/sources/${MEM_FILE} ${PROJECT_ROOT}/simulation/VUnit/vunit_out/modelsim
    #For GHDL
    file copy -force ${PROJECT_ROOT}/sources/${MEM_FILE} ${PROJECT_ROOT}/simulation/VUnit/
}

file mkdir  ${PROJECT_ROOT}/simulation/VUnit/vunit_out/samples
set samplefiles [glob ${PROJECT_ROOT}/simulation/ItkStrip/samples/*]
file copy -force {*}$samplefiles ${PROJECT_ROOT}/simulation/VUnit/vunit_out/samples

file mkdir  ${PROJECT_ROOT}/simulation/VUnit/vunit_out/64b66b
set samplefiles [glob ${PROJECT_ROOT}/simulation/64b66b/*.txt]
file copy -force {*}$samplefiles ${PROJECT_ROOT}/simulation/VUnit/vunit_out/64b66b

foreach XCI_FILE $XCI_FILES {
    set XCI_FILE [string trimright $XCI_FILE i]
    set XCI_FILE [string trimright $XCI_FILE c]
    set XCI_FILE [string trimright $XCI_FILE x]
    set XCI_FILE [string trimright $XCI_FILE .]
    set SOURCE_FILES [concat $SOURCE_FILES "${PROJECT_ROOT}/sources/ip_cores/sim/${XCI_FILE}_sim_netlist.vhdl"]
}


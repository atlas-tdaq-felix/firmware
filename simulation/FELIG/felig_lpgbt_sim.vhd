--ricardo luz, argonne

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

library ieee;
    use ieee.std_logic_1164.all;
    use IEEE.NUMERIC_STD.ALL;
    use ieee.numeric_std_unsigned.all;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.FELIX_package.all;
    use work.pcie_package.all;

entity felig_lpgbt_sim is
    generic(
        CARD_TYPE       : integer := 712;
        GBT_NUM         : integer := 4
    );
end felig_lpgbt_sim;

architecture Behavioral of felig_lpgbt_sim is

    component clk_wiz_40_0 -- @suppress "Component declaratiechoon is not equal to its matching entity"
        port(
            clk_in2    : in  STD_LOGIC;
            clk_in_sel : in  STD_LOGIC;
            clk40      : out STD_LOGIC;
            clk80      : out STD_LOGIC;
            clk160     : out STD_LOGIC;
            clk320     : out STD_LOGIC;
            clk240     : out STD_LOGIC;
            clk100     : out STD_LOGIC;
            reset      : in  STD_LOGIC;
            locked     : out STD_LOGIC;
            clk_40_in  : in  STD_LOGIC
        );
    end component clk_wiz_40_0;
    signal clk40                : std_logic;
    signal clk40_in             : std_logic;
    signal clk160               : std_logic;
    signal clk240               : std_logic;
    signal clk320               : std_logic;

    constant clk40_period       : time := 25 ns;
    signal clk40_tmp            : std_logic:= '0';
    signal clk_en               : boolean   := false;

    signal reset                : std_logic := '0';
    signal reset_done           : std_logic := '0';
    signal reset_clk40          : std_logic := '0';
    signal reset_done_clk40     : std_logic := '0';

    signal timeout              : boolean := false;

    type data16barray           is array (0 to GBT_NUM-1) of std_logic_vector(15 downto 0);
    type data32barray           is array (0 to GBT_NUM-1) of std_logic_vector(31 downto 0);
    type txrx234b_48ch_type     is array (GBT_NUM-1 downto 0) of std_logic_vector(233 downto 0);
    type txrx36b_48ch_type      is array (GBT_NUM-1 downto 0) of std_logic_vector(35 downto 0);
    --lpgbt BE
    signal alignment_done_f_BE  : std_logic_vector(GBT_NUM-1 downto 0);
    signal data_rdy_BE          : std_logic_vector(GBT_NUM-1 downto 0);
    signal RxSlide_BE           : std_logic_vector(GBT_NUM-1 downto 0);
    signal TX_DATA_BE           : data16barray := (others => ("0000000000000000"));
    signal uplinkData_i_BE      : txrx234b_48ch_type;
    signal fec_error_i_BE       : std_logic_vector(GBT_NUM-1 downto 0);
    signal fec_err_cnt_i_BE     : array_32b(0 to GBT_NUM-1);

    --lpgbt FE
    signal RxSlide_FE               : std_logic_vector(GBT_NUM-1 downto 0);
    signal alignment_done_f_FE      : std_logic_vector(GBT_NUM-1 downto 0);
    signal sta_headerFlag_out_FE    : std_logic_vector(GBT_NUM-1 downto 0);
    signal TX_DATA_FE               : data32barray := (others => ("00000000000000000000000000000000"));
    signal downLinkData_i_FE        : txrx36b_48ch_type;
    signal tx_flag_out_FE           : std_logic_vector(GBT_NUM-1 downto 0);

    --data
    signal data_to_back_end     : std_logic_vector(223 downto 0);-- := (others => '0');
    signal data_to_front_end    : std_logic_vector(31 downto 0);

begin

    --clocks

    clk40_tmp       <= not clk40_tmp        after clk40_period/2; --40 MHZ;
    clk40_in        <= clk40_tmp            when  clk_en else '0';

    clk_25d00 : clk_wiz_40_0
        port map (
            clk_in2 => clk40_in,
            clk_in_sel => '0',
            clk40 => clk40,
            clk80 => open,
            clk160 => clk160,
            clk320 => clk320,
            clk240 => clk240,
            clk100 => open,
            reset => '0',
            locked => open,
            clk_40_in => clk40_in
        );

    process(clk40)
    begin
        if clk40'event and clk40='1' then
            reset_clk40 <= reset;
            reset_done_clk40 <= reset_done;
        end if;
    end process;

    -- lpgbt FELIX
    limk_loop_BE : for i in 0 to GBT_NUM-1 generate
        signal TXCLK40                      : std_logic;
        signal RX_RESET_rxusrclk            : std_logic;
        signal TX_RESET_txusrclk            : std_logic;
        signal downlinkUserData_i           : std_logic_vector(31 downto 0);
        signal downlinkEcData_i             : std_logic_vector(1 downto 0);
        signal downlinkIcData_i             : std_logic_vector(1 downto 0);
        signal uplinkMulticycleDelay        : std_logic_vector(2 downto 0);
        signal GT_TX_WORD_CLK               : std_logic;
        signal GT_RX_WORD_CLK               : std_logic;
        signal GBT_General_ctrl             : std_logic_vector(63 downto 0);
        signal CTRL_FECMODE                 : std_logic;
        signal RX_DATA_32b                  : std_logic_vector(31 downto 0);
        signal uplinkDATA                   : std_logic_vector(223 downto 0);
        signal uplinkEC                     : std_logic_vector(1 downto 0);
        signal uplinkIC                     : std_logic_vector(1 downto 0);
    begin
        TXCLK40                 <= clk40;
        downlinkUserData_i      <= data_to_front_end;
        downlinkIcData_i        <= "10";
        downlinkEcData_i        <= "11";

        GT_TX_WORD_CLK          <= clk320;
        GT_RX_WORD_CLK          <= clk320;

        CTRL_FECMODE            <= '0'; --FEC5
        GBT_General_ctrl        <= (others => '0'); --all 0 is correct

        RX_DATA_32b             <=  TX_DATA_FE(i);--(others => '0');

        TX_RESET_txusrclk       <= reset_clk40;
        RX_RESET_rxusrclk       <= reset_clk40;

        uplinkMulticycleDelay   <= "011"; --(others => '0');

        lpgbt_BE: entity work.FLX_LpGBT_BE
            Port map (
                downlinkUserData_i          => downlinkUserData_i,                  --in
                downlinkEcData_i            => downlinkEcData_i,                    --in
                downlinkIcData_i            => downlinkIcData_i,                    --in
                TXCLK40                     => clk40,                            --in
                TXCLK320                    => GT_TX_WORD_CLK,                      --in
                RXCLK320m                   => GT_RX_WORD_CLK,                      --in
                uplinkSelectFEC_i           => CTRL_FECMODE,                        --in
                data_rdy                    => data_rdy_BE(i),                      --out
                Tx_scrambler_bypass         => '0',                                 --in
                Tx_Interleaver_bypass       => '0',                                 --in
                Tx_FEC_bypass               => '0',                                 --in
                TxData_Out                  => TX_DATA_BE(i),                       --out
                rxdatain                    => RX_DATA_32b,                         --in
                GBT_TX_RST                  => TX_RESET_txusrclk,                   --in
                GBT_RX_RST                  => RX_RESET_rxusrclk,                   --in
                uplinkBypassInterleaver_i   => GBT_General_ctrl(35),                --in
                uplinkBypassFECEncoder_i    => GBT_General_ctrl(36),                --in
                uplinkBypassScrambler_i     => GBT_General_ctrl(37),                --in
                uplinkMulticycleDelay_i     => uplinkMulticycleDelay,               --in
                sta_headerFecLocked_o       => alignment_done_f_BE(i),              --out
                ctr_clkSlip_o               => RxSlide_BE(i),                       --out
                --sta_rxGbRdy_o             => open,                                --out
                uplinkReady_o               => open, --UplinkRdy(i),                --out
                uplinkUserData_o            => uplinkData_i_BE(i)(229 downto 0),    --out
                uplinkEcData_o              => uplinkData_i_BE(i)(231 downto 230),  --out
                uplinkIcData_o              => uplinkData_i_BE(i)(233 downto 232),  --out
                fec_error_o                 => fec_error_i_BE(i),                   --out
                fec_err_cnt_o               => fec_err_cnt_i_BE(i),                 --out
                TX_aligned_i                => '0'
            );
        uplinkDATA  <= uplinkData_i_BE(i)(223 downto 0);
        uplinkEC    <= uplinkData_i_BE(i)(231 downto 230);
        uplinkIC    <= uplinkData_i_BE(i)(233 downto 232);
    end generate;

    -- lpgbt FELIG
    limk_loop_FE : for i in 0 to GBT_NUM-1 generate
        signal txclk40m             : std_logic;
        signal rxrecclk40m          : std_logic;
        signal GT_TX_WORD_CLK       : std_logic;
        signal GT_RX_WORD_CLK       : std_logic;
        signal TX_RESET_i           : std_logic;
        signal RX_DATA_16b          : std_logic_vector(15 downto 0);
        signal rst_dnlink           : std_logic;
        signal rxresetdone          : std_logic;
        signal GBT_General_ctrl     : std_logic_vector(63 downto 0);
        signal CTRL_FECMODE         : std_logic;
        signal CTRL_DATARATE        : std_logic;
        signal FE_UPLINK_USER_DATA  : std_logic_vector(223 downto 0);
        signal FE_UPLINK_IC_DATA    : std_logic_vector(1 downto 0);
        signal FE_UPLINK_EC_DATA    : std_logic_vector(1 downto 0);
        signal downlinkDATA         : std_logic_vector(31 downto 0);
        signal downlinkEC           : std_logic_vector(1 downto 0);
        signal downlinkIC           : std_logic_vector(1 downto 0);
        signal count                : std_logic_vector(2 downto 0) := "000";
        signal data_en              : std_logic;
    begin

        txclk40m             <= clk40;
        rxrecclk40m          <= clk40;
        GT_TX_WORD_CLK       <= clk320;
        GT_RX_WORD_CLK       <= clk320;
        TX_RESET_i           <= reset_clk40;
        RX_DATA_16b          <= TX_DATA_BE(i);
        rst_dnlink           <= reset_clk40;
        rxresetdone          <= reset_done_clk40;
        GBT_General_ctrl     <= (others => '0'); --all 0 is correct
        CTRL_FECMODE         <= '0'; --FEC5
        CTRL_DATARATE        <= '1'; --10.24 Gb/s
        FE_UPLINK_USER_DATA  <= data_to_back_end;--(others => '0');
        FE_UPLINK_IC_DATA    <= "01";
        FE_UPLINK_EC_DATA    <= "10";

        lpgbt_FE: entity work.FLX_LpGBT_FE
            Port map
        (
                clk40_in                        => rxrecclk40m,
                TXCLK320                        => GT_TX_WORD_CLK,                      --in
                RXCLK320                        => GT_RX_WORD_CLK,                      --in
                rst_uplink_i                    => TX_RESET_i,                          --in
                ctr_clkSlip_s                   => RxSlide_FE(i),                       --out
                aligned                         => alignment_done_f_FE(i),              --out
                sta_headerFlag_o                => sta_headerFlag_out_FE(i),            --out
                dat_upLinkWord_fromGb_s         => TX_DATA_FE(i),                       --out
                dat_downLinkWord_fromMgt_s16    => RX_DATA_16b,                         --in
                rst_dnlink_i                    => rst_dnlink,--RL                      --in
                sta_mgtRxRdy_s                  => rxresetdone,                         --in
                downLinkBypassDeinterleaver     => '0',                                 --in
                downLinkBypassFECDecoder        => '0',                                 --in
                downLinkBypassDescsrambler      => '0',                                 --in
                enableFECErrCounter             => '0',                                 --in
                upLinkScramblerBypass           => GBT_General_ctrl(32),                --in
                --upLinkFecBypass                 => GBT_General_ctrl(33),                --in
                upLinkInterleaverBypass         => GBT_General_ctrl(34),                --in
                fecMode                         => CTRL_FECMODE,                        --in
                txDataRate                      => CTRL_DATARATE,                       --in
                phase_sel                       => "100",                               --in
                upLinkData                      => FE_UPLINK_USER_DATA,                 --in
                upLinkDataIC                    => FE_UPLINK_IC_DATA,                   --in
                upLinkDataEC                    => FE_UPLINK_EC_DATA,                   --in
                upLinkDataREADY                 => data_en,
                downLinkData                    => downLinkData_i_FE(i)(31 downto 0),   --out
                downLinkDataIC                  => downLinkData_i_FE(i)(33 downto 32),  --out
                downLinkDataEC                  => downLinkData_i_FE(i)(35 downto 34),  --out
                tx_flag_out                     => tx_flag_out_FE(i)                    --out
            );
        downlinkDATA    <= downLinkData_i_FE(i)(31 downto 0);
        downlinkEC      <= downLinkData_i_FE(i)(33 downto 32);
        downlinkIC      <= downLinkData_i_FE(i)(35 downto 34);

        process(GT_TX_WORD_CLK)
        begin
            if GT_TX_WORD_CLK'event and GT_TX_WORD_CLK='1' then
                count <= count + "001";
                if count = "111" then
                    data_en <= '1';
                else
                    data_en <= '0';
                end if;
            end if;
        end process;

    end generate;

    --sim process
    mainproc : process
    begin
        assert false
            report "START SIMULATION"
            severity NOTE;
        clk_en  <= true;
        data_to_back_end    <= (others => '0');
        data_to_front_end   <= (others => '0');
        wait for 2 us;
        reset   <= '1';
        wait for 2 us;
        reset   <='0';
        reset_done  <= '1';
        wait for 5 us;
        while true loop
            if timeout then
                exit;
            end if;
            data_to_back_end    <= data_to_back_end + x"1";
            data_to_front_end   <= data_to_front_end + x"1";
            wait for 25 ns;
        end loop;
        wait for 1000 us;
    end process;

    timeoutproc : process
    begin
        wait for 1000 us;
        timeout <= true;
    end process;


end Behavioral;

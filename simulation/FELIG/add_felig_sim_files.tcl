#NEEDS TO BE RUN in scripts/FELIX_top/
source ../helper/clear_filesets.tcl

source ../filesets/felig_fileset.tcl

#set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_KU]
set SIM_FILES [concat $SIM_FILES $SIM_FILES_KU]

set scriptdir [pwd]
set firmware_dir $scriptdir/../../

foreach SIM_FILE $SIM_FILES {
    add_files -fileset sim_1 -force -norecurse ${firmware_dir}/simulation/$SIM_FILE
    set_property library work [get_files  ${firmware_dir}/simulation/$SIM_FILE]
    set_property file_type {VHDL 2008} [get_files  ${firmware_dir}/simulation/$SIM_FILE]
}

foreach WCFG_FILE $WCFG_FILES {
   add_files -fileset sim_1 -force -norecurse ${firmware_dir}/simulation/$WCFG_FILE
}

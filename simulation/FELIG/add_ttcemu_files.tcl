#NEEDS TO BE RUN in scripts/FELIX_top/
source ../helper/clear_filesets.tcl

source ../filesets/ttc_emulator_fileset.tcl

set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_KU]

set scriptdir [pwd]
set firmware_dir $scriptdir/../../

foreach VHDL_FILE $VHDL_FILES {
	set file_path [file normalize ${firmware_dir}/sources/${VHDL_FILE}]

	read_vhdl -library work $file_path
	set_property FILE_TYPE {VHDL 2008} [get_files ${file_path}]
	add_file $file_path
}


library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.numeric_std_unsigned.ALL;
    use work.FELIX_package.all;
    use work.pcie_package.all;
    use work.axi_stream_package.all;
    use ieee.std_logic_textio.all;
    use std.textio.all;

    use std.env.all;

    use work.package_pixel.all;

-- Test bench entity
entity Decoding_pixel_tb is
    generic(
        FIRMWARE_MODE                   : integer := FIRMWARE_MODE_PIXEL
    );

end Decoding_pixel_tb;

architecture tb of Decoding_pixel_tb is
    --constant fnameW : string := "RD53A_refout_CB.txt";
    --copy both files to Projects/FLX712_FELIX/FLX712_FELIX.sim/sim_1/behav/xsim
    constant fname                        : string                                         := "../../../../../../simulation/64b66b/RD53A_refout.txt";
    constant fnameCB                      : string                                         := "../../../../../../simulation/64b66b/RD53A_refout_CB.txt";
    constant STREAMS_TOHOST               : integer                                        := STREAMS_TOHOST_MODE(FIRMWARE_MODE);
    constant clk40_period                 : time                                           := 25 ns;
    constant clk160_period                : time                                           := 6.25 ns;
    constant aclk_period                  : time                                           := 4.16667 ns;

    constant LINK                           : IntArray                                       := (0,1);

    --    file fptr_write : text;

    signal data32_ref                     : std_logic_vector(31 downto 0);
    signal data32_ref_dly                 : std_logic_vector(31 downto 0);
    signal data32_ref_dlydly              : std_logic_vector(31 downto 0); -- @suppress "signal data32_ref_dlydly is never read"
    signal isErr                          : std_logic := '0';

    signal register_map_control           : register_map_control_type;

    signal clk40                          : std_logic:= '0';
    signal clk40_tmp                      : std_logic:= '0';
    signal clk40_en                       : boolean   := false;
    signal clk160                         : std_logic:= '0';
    signal clk160_tmp                     : std_logic:= '0';
    signal clk160_en                      : boolean   := false;
    signal aclk                           : std_logic:= '0';
    signal aclk_tmp                       : std_logic:= '0';
    signal aclk_en                        : boolean   := false;
    signal reset_dopulse : std_logic := '0';
    signal dopulse : std_logic := '1';
    signal reset                          : std_logic;

    signal DecoderAligned                 : std_logic_vector(STREAMS_TOHOST-3 downto 0); -- @suppress "signal DecoderAligned is never read"
    signal DecoderAligned_out             : std_logic_vector(2*(STREAMS_TOHOST-2)-1 downto 0);
    signal m_axis                         : axis_32_array_type(0 to STREAMS_TOHOST-2-1);
    signal m_axis_tready                  : axis_tready_array_type(0 to STREAMS_TOHOST-2-1);
    signal m_axis_prog_empty              : axis_tready_array_type(0 to STREAMS_TOHOST-2-1); -- @suppress "signal m_axis_prog_empty is never read"

    signal trigid                         : std_logic_vector(4 downto 0)                    := (others => '0'); -- @suppress "signal trigid is never read"
    signal trigtag                        : std_logic_vector(4 downto 0)                    := (others => '0'); -- @suppress "signal trigtag is never read"
    signal bcid                           : std_logic_vector(14 downto 0)                   := (others => '0'); -- @suppress "signal bcid is never read"
    signal isHDR                          : std_logic                                       := '0'; -- @suppress "signal isHDR is never read"
    signal col                            : std_logic_vector(5 downto 0)                    := (others => '0'); -- @suppress "signal col is never read"
    signal row                            : std_logic_vector(8 downto 0)                    := (others => '0'); -- @suppress "signal row is never read"
    signal onebside                       : std_logic := '0'; -- @suppress "signal onebside is never read"
    signal tot                            : array_4x4b                                      := (others => (others => '0')); -- @suppress "signal tot is never read"



    signal emuToHost_lpGBTdata            : std_logic_vector(223 downto 0);
    signal emuToHost_lpGBTECdata          : std_logic_vector(1 downto 0); -- @suppress "signal emuToHost_lpGBTECdata is never read"
    signal emuToHost_lpGBTICdata          : std_logic_vector(1 downto 0); -- @suppress "signal emuToHost_lpGBTICdata is never read"
    signal emuToHost_GBTlinkValid         : std_logic;

    signal m_axis0_dly                    : axis_32_type;
    signal m_axis0_dly_dly                : axis_32_type; -- @suppress "signal m_axis0_dly_dly is never read"
    signal countHDR                       : std_logic_vector(3 downto 0)                    := x"0";

    signal tvalid_doublerate : std_logic;
    signal start_simuCB0 : boolean := false; -- @suppress "signal start_simuCB0 is never read"
    signal start_simuCB3 : boolean := false; -- @suppress "signal start_simuCB3 is never read"
    signal done_simuCB0  : boolean := false; -- @suppress "signal done_simuCB0 is never read"
    signal done_simuCB3  : boolean := false; -- @suppress "signal done_simuCB3 is never read"
    signal timeout       : boolean := false;

    --after chaning decoder to two links
    signal LinkData_in                    : array_224b(0 to 1);
    signal LinkAligned_in                 : std_logic_vector(0 to 1);
    signal m_axis_out                     : axis_32_2d_array_type(0 to 1, 0 to STREAMS_TOHOST-3);
    signal m_axis_tready_in               : axis_tready_2d_array_type(0 to 1, 0 to STREAMS_TOHOST-3);
    signal m_axis_prog_empty_out          : axis_tready_2d_array_type(0 to 1, 0 to STREAMS_TOHOST-3);
    signal clk40_stable : std_logic;
    signal daq_reset : std_logic;
    signal daq_fifo_flush : std_logic;

begin
    -----------------------------------------------------------------------------
    -- Registers
    -----------------------------------------------------------------------------
    --internal emulator
    register_map_control.GBT_TOHOST_FANOUT.SEL(0 downto 0) <= "1";
    register_map_control.FE_EMU_ENA.EMU_TOHOST(0) <= '1';
    register_map_control.FE_EMU_CONFIG.WRADDR <= (others => '0');
    --
    register_map_control.DECODING_DISEGROUP <= "1111000"; --disable all lane
    --but 0,1,2
    register_map_control.DECODING_MASK64B66BKBLOCK <= x"a";

    --egroup0: link 0 (hit data), 1 (DCS)
    --egroup>0 disabled
    register_map_control.DECODING_EGROUP_CTRL(0)(0).PATH_ENCODING <= x"00000033";
    register_map_control.DECODING_EGROUP_CTRL(0)(0).EPATH_ENA <= x"03";
    register_map_control.DECODING_EGROUP_CTRL(0)(1).PATH_ENCODING <= x"00000033";
    register_map_control.DECODING_EGROUP_CTRL(0)(1).EPATH_ENA <= x"03";
    register_map_control.DECODING_EGROUP_CTRL(0)(2).PATH_ENCODING <= x"00000033";
    register_map_control.DECODING_EGROUP_CTRL(0)(2).EPATH_ENA <= x"03";
    --
    m_axis_tready     <= (others => '1');

    -----------------------------------------------------------------------------
    -- Clock Generator
    -----------------------------------------------------------------------------
    clk40_tmp  <= not clk40_tmp  after clk40_period/2; --12.5ns;
    clk40      <= clk40_tmp      when  clk40_en else '0';
    clk160_tmp <= not clk160_tmp after clk160_period/2; --3.125ns;
    clk160     <= clk160_tmp     when  clk160_en else '0';
    aclk_tmp   <= not aclk_tmp   after aclk_period/2; --3.125ns;
    aclk       <= aclk_tmp       when  aclk_en else '0';

    -----------------------------------------------------------------------------
    -- Resets
    -----------------------------------------------------------------------------
    rst_proc: process(clk40)
    begin
        if rising_edge(clk40) then
            if (reset_dopulse = '1') then
                reset <= '0';
                dopulse <= '1';
            elsif(dopulse = '1' and reset = '0') then
                reset <= '1';
                dopulse <= '1';
            elsif(dopulse = '1' and reset = '1') then
                reset <= '0';
                dopulse <= '0';
            else
                reset <= '0';
            end if;
        end if;
    end process;

    resetmgr0: entity work.CRresetManager
        port map(
            clk40         => clk40,
            rst           => reset,
            clk40_stable  => clk40_stable,
            cr_rst        => daq_reset,
            cr_fifo_flush => daq_fifo_flush
        );

    -----------------------------------------------------------------------------
    -- Emulator
    -----------------------------------------------------------------------------
    gbtEmuToHost0: entity work.GBTdataEmulator
        generic map(
            EMU_DIRECTION      => "ToHost",
            FIRMWARE_MODE      => FIRMWARE_MODE_PIXEL
        )
        port map(
            clk40                => clk40,
            wrclk                => clk40,
            rst_hw               => reset,
            rst_soft             => daq_reset,
            xoff                 => '0',
            register_map_control => register_map_control,
            GBTdata              => open,
            lpGBTdataToFE        => open,
            lpGBTdataToHost      => emuToHost_lpGBTdata,
            lpGBTECdata          => emuToHost_lpGBTECdata,
            lpGBTICdata          => emuToHost_lpGBTICdata,
            GBTlinkValid         => emuToHost_GBTlinkValid);

    --in
    g_link_in_loop: for link_i in 0 to 1 generate
        LinkData_in(link_i)     <= emuToHost_lpGBTdata;
        LinkAligned_in(link_i)  <= emuToHost_GBTlinkValid;
        g_streamtohost_in_loop: for stream_i in 0 to STREAMS_TOHOST-3 generate
            m_axis_tready_in(link_i, stream_i) <= m_axis_tready(stream_i);
        end generate; --stream
    end generate; --link

    g_streamtohost_out_loop: for stream_i in 0 to STREAMS_TOHOST-3 generate
        m_axis(stream_i)            <= m_axis_out(0, stream_i);
        m_axis_prog_empty(stream_i) <= m_axis_prog_empty_out(0, stream_i);
    end generate; --stream
    DecoderAligned <= DecoderAligned_out(STREAMS_TOHOST-3 downto 0);

    -----------------------------------------------------------------------------
    -- UUT
    -----------------------------------------------------------------------------
    DecodingPixelLinkLPGBT_uut: entity work.DecodingPixelLinkLPGBT
        generic map(
            CARD_TYPE      => 712,
            RD53Version    => "A",
            BLOCKSIZE      => 1024,
            LINK           => LINK,
            SIMU           => 1,
            STREAMS_TOHOST => STREAMS_TOHOST,
            PCIE_ENDPOINT  => 0,
            VERSAL         => false
        )
        port map(
            clk40                => clk40,
            clk160               => clk160,
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            MsbFirst             => '1',

            LinkData             => LinkData_in,
            LinkAligned          => LinkAligned_in,

            m_axis               => m_axis_out,
            m_axis_tready        => m_axis_tready_in,
            m_axis_aclk          => aclk,
            m_axis_prog_empty    => m_axis_prog_empty_out,
            register_map_control => register_map_control,
            DecoderAligned_out   => DecoderAligned_out,
            DecoderDeskewed_out  => open,
            cnt_rx_64b66bhdr_out => open,
            rx_soft_err_cnt_out  => open,
            rx_soft_err_rst      => (others => '0')
        );
    -----------------------------------------------------------------------------
    -- Data Checker
    -----------------------------------------------------------------------------
    interpretdata_proc: process(aclk)
    begin
        if rising_edge(aclk) then
            if daq_reset = '1' then -- @suppress "Incomplete reset branch: missing synchronous reset for registers 'bcid', 'col', and 9 more"
                countHDR  <= (others => '0');
                isErr     <= '0';
            else
                data32_ref_dlydly <= data32_ref_dly;
                data32_ref_dly    <= data32_ref;
                m_axis0_dly_dly   <= m_axis0_dly;
                m_axis0_dly       <= m_axis(0);

                if(m_axis(0).tdata /= data32_ref and m_axis(0).tvalid = '1') then
                    isErr <= '1';
                end if;

                if(m_axis(0).tvalid = '1') then
                    --              hwrite(line_out,m_axis(0).tdata,RIGHT,9);
                    --              writeline(fptr_write,line_out);

                    if(m_axis(0).tdata(31 downto 25) = "0000001") then
                        countHDR  <= countHDR + x"1";
                        isHDR     <= '1';
                        trigid    <= m_axis(0).tdata(24 downto 20);
                        trigtag   <= m_axis(0).tdata(19 downto 15);
                        bcid      <= m_axis(0).tdata(14 downto 0);
                        col       <= (others => '0');
                        row       <= (others => '0');
                        onebside  <= '0';
                        tot       <= (others => (others => '0'));
                    else
                        isHDR     <= '0';
                        trigid    <= (others => '0');
                        trigtag   <= (others => '0');
                        bcid      <= (others => '0');
                        col       <= m_axis(0).tdata(31 downto 26);
                        row       <= m_axis(0).tdata(25 downto 17);
                        onebside  <= m_axis(0).tdata(16);
                        tot(0)    <= m_axis(0).tdata(15 downto 12);
                        tot(1)    <= m_axis(0).tdata(11 downto  8);
                        tot(2)    <= m_axis(0).tdata( 7 downto  4);
                        tot(3)    <= m_axis(0).tdata( 3 downto  0);
                    end if;
                else
                    isHDR       <= '0';
                    trigid      <= (others => '0');
                    trigtag     <= (others => '0');
                    bcid        <= (others => '0');
                    col         <= (others => '0');
                    row         <= (others => '0');
                    onebside    <= '0';
                    tot         <= (others => (others => '0'));
                end if;
            end if;
        end if;
    end process;


    tvalid_doublerate_proc: process(aclk)
    begin
        if aclk'event and aclk = '1' then
            tvalid_doublerate <= m_axis(0).tvalid;
        elsif aclk'event and aclk = '0' then
            tvalid_doublerate <= '0';
        end if;
    end process;

    -----------------------------------------------------------------------------
    -- Main Processes
    -----------------------------------------------------------------------------
    mainproc : process
        file fptr           : text open READ_MODE is fname;
        file fptrCB           : text open READ_MODE is fnameCB;

        variable file_line  : line;
        variable good       : boolean; -- @suppress "variable good is never read"
        variable data32     : std_logic_vector(31 downto 0);
        variable count_data                     : integer                                         := 0;


        variable count_dataCB0                     : integer                                         := 0;
        variable countHDRCB0                     : std_logic_vector(3 downto 0)                                         := x"0";
        variable isERRCB0                     : std_logic := '0';
        variable count_dataCB3                     : integer                                         := 0;
        variable countHDRCB3                     : std_logic_vector(3 downto 0)                                         := x"0";
        variable isERRCB3                     : std_logic := '0';

        procedure printMT (arg: in string := "") is
            variable lineMT : line;
        begin
            std.textio.write(lineMT, arg);
            std.textio.writeline(std.textio.output, lineMT);
        end;

    begin
        assert false
            report "START SIMULATION"
            severity NOTE;
        clk40_stable <= '1';
        clk40_en<= true;
        clk160_en <= true;
        aclk_en<= true;

        ------------START SIMULATING CB=0 scenario-------------
        start_simuCB0 <= true;

        readline(fptr, file_line); --skip first line
        --        file_open(fptr_write, fnameW,write_mode);        --

        register_map_control.DECODING_LINK_CB(0).CBOPT <= x"0";
        --        dopulse <= '1'; --start reset chain


        while (not endfile(fptr)) loop
            if timeout then
                exit;
            end if;
            readline(fptr, file_line);
            hread(file_line,data32,good);
            data32_ref <= data32;
            count_data := count_data + 1;
            wait until tvalid_doublerate = '1' or timeout;
        end loop;

        wait for 100 ns;

        --    file_close(fptr);

        -- Finish the simulation
        file_close(fptr);
        --        file_close(fptr_write);

        isErrCB0 := isErr;
        count_dataCB0 := count_data;
        countHDRCB0 := countHDR;
        done_simuCB0 <= true;

        --std.env.stop;
        --wait;  -- to stop completely
        --finish;

        wait for 1000 ns;
        ------------DONE SIMULATING CB=0 scenario-------------
        ------------START SIMULATING CB=3 scenario-------------
        start_simuCB3 <= true;
        readline(fptrCB, file_line); --skip first line
        register_map_control.DECODING_LINK_CB(0).CBOPT <= x"3";
        count_data := 0;
        reset_dopulse <= '1'; --start reset chain
        wait for 100 ns;
        reset_dopulse <= '0';

        while (not endfile(fptrCB)) loop
            if timeout then
                exit;
            end if;
            readline(fptrCB, file_line);
            hread(file_line,data32,good);
            data32_ref <= data32;
            count_data := count_data + 1;
            wait until tvalid_doublerate = '1' or timeout;
        end loop;

        wait for 100 ns;

        -- Finish the simulation
        file_close(fptrCB);

        isErrCB3 := isErr;
        count_dataCB3 := count_data;
        countHDRCB3 := countHDR;
        done_simuCB3 <= true;
        wait for 100 ns;

        printMT("***************");
        printMT("*****CB=0******");
        printMT("***************");
        if isErrCB0 = '0' and countHDRCB0 = x"2" then
            printMT("***Simulation passed, Checked #data = " & integer'image(count_dataCB0));
        else
            printMT("***Simulation failed, Checked #data = " & integer'image(count_dataCB0));
        end if;
        printMT("***************");
        printMT("***************");
        printMT("***************");

        printMT("***************");
        printMT("*****CB=3******");
        printMT("***************");
        if isErrCB3 = '0' and countHDRCB3 = x"2" then
            printMT("***Simulation passed, Checked #data = " & integer'image(count_dataCB3));
        else
            printMT("***Simulation failed, Checked #data = " & integer'image(count_dataCB3));
        end if;
        printMT("***************");
        printMT("***************");
        printMT("***************");


    --        std.env.stop;
    --        wait;  -- to stop completely
    --        finish;

    end process;

    -----------------------------------------------------------------------------
    -- Timeout
    -----------------------------------------------------------------------------
    timeoutproc : process
    begin
        wait for 1000 us;
        timeout <= true;
    end process;


end tb;

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.numeric_std_unsigned.ALL;
    use ieee.std_logic_textio.all;
    use std.textio.all;
    --library textutil;
    use ieee.std_logic_textio.all;

    use std.env.all;

package package_pixel is
    type array_4x4b is array (0 to 3) of std_logic_vector(3 downto 0);

    function to_string (
        a: std_logic_vector
    ) return string;

    function str_to_stdvec(
        inp: string
    ) return std_logic_vector;

    procedure make_inputs (
        constant fnameR : in string;
        constant fnameW : in string;
        signal clk40         : in std_logic;
        signal ElinkDataA_out : out std_logic_vector(31 downto 0);
        signal ElinkDataB_out : out std_logic_vector(31 downto 0);
        signal ElinkDataC_out : out std_logic_vector(31 downto 0);
        signal ElinkDataD_out : out std_logic_vector(31 downto 0);
        signal ElinkDataE_out : out std_logic_vector(31 downto 0);
        signal ElinkDataF_out : out std_logic_vector(31 downto 0);
        signal ElinkDataG_out : out std_logic_vector(31 downto 0);
        signal fileopen       : out std_logic
    );


end package;



package body package_pixel is

    function to_string ( a: std_logic_vector) return string is
        variable b : string (1 to a'length) := (others => NUL);
        variable stri : integer := 1;
    begin
        for i in a'range loop
            b(stri) := std_logic'image(a((i)))(2);
            stri := stri+1;
        end loop;
        return b;
    end function;

    function str_to_stdvec(inp: string) return std_logic_vector is
        variable temp: std_logic_vector(inp'range) := (others => 'X');
    begin

        for i in inp'range loop
            if (inp(i) = '1') then
                temp(i) := '1';
            elsif (inp(i) = '0') then
                temp(i) := '0';
            end if;
        end loop;

        return temp;
    end function str_to_stdvec;

    procedure make_inputs (
        constant fnameR : in string;
        constant fnameW : in string; -- @suppress "Unused declaration"
        signal clk40         : in std_logic;
        signal ElinkDataA_out : out std_logic_vector(31 downto 0);
        signal ElinkDataB_out : out std_logic_vector(31 downto 0);
        signal ElinkDataC_out : out std_logic_vector(31 downto 0);
        signal ElinkDataD_out : out std_logic_vector(31 downto 0);
        signal ElinkDataE_out : out std_logic_vector(31 downto 0);
        signal ElinkDataF_out : out std_logic_vector(31 downto 0);
        signal ElinkDataG_out : out std_logic_vector(31 downto 0);
        signal fileopen       : out std_logic
    ) is
        file fptr: TEXT;
        variable good : boolean;
        variable A : std_ulogic_vector(15 downto 0); -- @suppress "variable A is never read"
        variable data32A    : std_logic_vector(31 downto 0);
        variable data32B    : std_logic_vector(31 downto 0);
        variable data32C    : std_logic_vector(31 downto 0);
        variable data32D    : std_logic_vector(31 downto 0) := (others => '0');
        variable data32E    : std_logic_vector(31 downto 0) := (others => '0');
        variable data32F    : std_logic_vector(31 downto 0) := (others => '0');
        variable data32G    : std_logic_vector(31 downto 0) := (others => '0');

        variable file_line  : line;

        variable ElinkData_tmp : std_logic_vector(31 downto 0);


    begin
        --http://www.pld.guru/_hdl/2/_ref/acc-eda/language_overview/test_benches/reading_and_writing_files_with_text_i_o.htm
        fileopen <= '1';
        report "OPENING FILE";
        file_open(fptr, fnameR,read_mode);        --
        --    file_open(fptr_write, fnameW,write_mode);        --                                                         --

        --    write(fptr_write, "SAVING INFO");
        --    writeline(fptr_write,line_out); --go to next line

        wait until (rising_edge(clk40));
        while not endfile(fptr) loop

            readline(fptr, file_line);
            --iterator, rxdata_mgt_i[31:0] pre_rxdatavalid_i_1 rxheader_mgt_i[1:0] pre_rxheadervalid_i pre_rxdatavalid_i rxdata_to_fifo_i[31:0]
            hread(file_line,A,good);      --reading as it was a 4 HEX number
            hread(file_line,data32A,good);
            assert(good)
                report "Text I/O read error: egroup0 data not there"
                severity ERROR;
            hread(file_line,data32B,good);
            assert(good)
                report "Text I/O read error: egroup1 data not there"
                severity ERROR;
            hread(file_line,data32C,good);
            assert(good)
                report "Text I/O read error: egroup2 data not there"
                severity ERROR;
            hread(file_line,data32D,good);
            hread(file_line,data32E,good);
            hread(file_line,data32F,good);
            hread(file_line,data32G,good);


            assert false
                report ("MGT" & to_string(data32A))
                severity NOTE;
            assert false
                report ("MGT unscrambled" & to_string(data32B))
                severity NOTE;


            ElinkData_tmp := data32B;
            ElinkDataA_out <= data32A;
            ElinkDataB_out <= data32B;
            ElinkDataC_out <= data32C;
            ElinkDataD_out <= data32D;
            ElinkDataE_out <= data32E;
            ElinkDataF_out <= data32F;
            ElinkDataG_out <= data32G;


            --log("CI: " , CI);
            --      log("data: " , to_string(data_i));

            assert false
                report ("Make input for Decoding (Pixel)" & to_string(ElinkData_tmp))
                severity NOTE;

            --write to file
            --      hwrite(line_out,ElinkData_tmp,RIGHT,9);
            --      writeline(fptr_write,line_out);
            --
            wait until (rising_edge(clk40));

        end loop;
        fileopen <= '0';

        wait for 1 us; --shrink if you want to keep reading the same file over more
        --times

        assert false
            report "End of file encountered; exiting."
            severity NOTE;


        report "CLOSING FILE";
        file_close(fptr);
    --    file_close(fptr_write);



    end;

end package body;

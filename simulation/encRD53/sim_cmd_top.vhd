library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.STD_LOGIC_UNSIGNED.ALL;
    use work.FELIX_package.all;
    use work.pcie_package.all;
    use work.axi_stream_package.all;
    use work.centralRouter_package.all;
    use ieee.std_logic_textio.all;
    use std.textio.all;
    use std.env.all;
    use work.rd53_package.ALL;
library XPM;
    use XPM.VCOMPONENTS.all;
entity sim_cmd_top is
    generic(
        RD53Version     : String := "B";
        cmd_top_version : integer := 2
    );end sim_cmd_top;

architecture tb of sim_cmd_top is
    signal ttime                        : integer := 0;

    --cmd_top inputs
    signal cmd_sent_i                   : std_logic := '0';--=1 when we sent a cmd
    signal startread                    : std_logic_vector(4 downto 0) := (others => '0');
    signal counter                      : std_logic_vector (1 downto 0) := (others => '0');
    signal command_in40                 : std_logic_vector(15 downto 0) := (others => '0');
    signal command_rdy40                : std_logic := '0';

    --gen startread(4)
    signal command_rdy_dly              : std_logic;

    --gen counter and cmd_sent_i
    signal ser_data_i                   : std_logic_vector(15 downto 0);
    signal dataout_i                    : std_logic_vector(3 downto 0);
    signal sync_sent_i                  : std_logic;--=1 when we sent a sync
    signal az_sent_i                    : std_logic;

    signal trig_rdy                     : std_logic := '0'; --newtriggerunit output
    signal sync_rdy                     : std_logic := '0'; --sync_timer output
    signal rst_rdy                      : std_logic := '0'; --cmd_top output
    signal cmd_rdy                      : std_logic := '0'; --cmd_top output
    signal az_rdy                       : std_logic := '0'; --az_controller output

    signal trig_data                    : std_logic_vector(15 downto 0) := (others => '0'); --newtriggerunit output
    signal cmd_data                     : std_logic_vector(15 downto 0) := (others => '0'); --cmd_top output
    signal az                           : std_logic_vector(15 downto 0) := (others => '0'); --az_controller output

    signal enAZ_viacmd                  : std_logic;--cmd_top output
    signal az_cmd_type                  : std_logic := '0';--cmd_top output
    signal sync_cmd_type                : std_logic;--cmd_top output

    --gen command_in40 and command_rdy40

    --fifo out
    signal full_fifo                    : std_logic;
    signal almost_full_fifo             : std_logic; -- @suppress "signal almost_full_fifo is never read"
    signal empty_fifo                   : std_logic; -- @suppress "signal empty_fifo is never read"

    --fifo in
    signal rst_fifo                     : std_logic := '0';
    signal wr_en_fifo                   : std_logic;
    signal ReadEnable40                 : std_logic; --cmd_top output
    signal command_in20                 : std_logic_vector(15 downto 0);

    --process
    signal count40                      : std_logic := '0';
    signal command_rdy20                : std_logic;


    --ENCRD53 inputs
    signal command_in                   : std_logic_vector(7 downto 0) := (others => '0');
    signal trigger                      : std_logic := '0';
    signal enAZ_in                      : std_logic := '0';
    signal command_rdy                  : std_logic := '0';
    signal clk40                        : std_logic := '0';
    signal rst                          : std_logic := '1';
    signal CalTrigSeq_in                : std_logic_vector(15 downto 0) := (others => '0');

    --clk40         : in std_logic;
    --rst           : in std_logic;
    --trigger       : in std_logic;
    --command_in    : in std_logic_vector(7 downto 0);
    --command_rdy   : in std_logic;
    --enAZ_in       : in std_logic; --enable AutoZeroing module for SyncFE
    --CalTrigSeq_in : in std_logic_vector(15 downto 0); --calseq from ram

    --
    constant maxreg_size                : integer := 36;
    constant WrReg0_size                : integer := 8;
    constant WrReg1_size                : integer := 36;
    constant RdReg_size                 : integer := 4;
    constant Cmd2wrd_size               : integer := 2;

    signal command_sim                  : std_logic_vector(maxreg_size*8-1 downto 0);
    signal size_sim                     : integer := 8;
    signal time_start                   : integer := 1000;
    signal iterator                     : integer := 0;

    --RAM MEMORY
    signal CalTrigSeq_WRADDR : std_logic_vector(4 downto 0) := (others => '0');
    signal CalTrigSeq_WRDATA : std_logic_vector(15 downto 0) := (others => '0');
    signal CalTrigSeq_WE : std_logic_vector(0 downto 0) := (others => '0');

    --signal CalTrigSeq : std_logic_vector(15 downto 0);
    signal ReadAddrCalTrigSeq : std_logic_vector(4 downto 0);
    constant zeros16bit : std_logic_vector(15 downto 0) := (others=>'0');
    signal RD53B_loopgen_reg : RD53_loopgen_type;
    signal register_map_control : register_map_control_type;
begin

    clk40   <= not clk40 after 12.5ns;


    g_cmd_gen_v1: if cmd_top_version = 1 generate
        cmd_gen_v1: entity work.cmd_top_v1
            generic map(
                reg_depth => 4,
                RD53Version => RD53Version
            )
            port map (
                rst                     => rst,
                clk40                   => clk40,
                startread               => startread(4),
                ReadEnable              => ReadEnable40,
                cmd_sent                => cmd_sent_i,
                counter                 => counter,
                cmd_in                  => command_in40,
                command_rdy             => command_rdy40,

                CalTrigSeq_in           => CalTrigSeq_in,
                ReadAddrCalTrigSeq_out  => ReadAddrCalTrigSeq,

                cmd_data                => cmd_data,
                rst_ready               => rst_rdy,
                cmd_ready               => cmd_rdy,
                sync_cmd_type           => sync_cmd_type,
                az_cmd_type             => az_cmd_type,
                enAZ_viacmd             => enAZ_viacmd,
                --debug
                frequency_out     => open,
                frequency_cnt_out => open,
                freq_en_out       => open,
                cmd_out_cnts_out  => open,
                pointer_out       => open,
                iteration_out     => open,
                cmd_state_is_out  => open,
                cmd_reg0_out      => open,
                cmd_reg1_out      => open,
                cmd_reg2_out      => open,
                cmd_reg3_out      => open,
                --debug
                err_cmdtop_out    => open,
                warn_cmdtop_out   => open
            );
    end generate;

    g_cmd_gen_v2: if cmd_top_version = 2 generate
        cmd_gen_v2: entity work.cmd_top_v2
            generic map(
                RD53Version => RD53Version
            )
            port map (
                rst                     => rst,
                clk40                   => clk40,
                startread               => startread(4),
                ReadEnable              => ReadEnable40,
                cmd_sent                => cmd_sent_i,
                counter                 => counter,
                cmd_in                  => command_in40,
                command_rdy             => command_rdy40,
                RD53B_loopgen_reg       => RD53B_loopgen_reg,
                --                CalTrigSeq_in           => CalTrigSeq_in,
                --                ReadAddrCalTrigSeq_out  => ReadAddrCalTrigSeq,

                cmd_data                => cmd_data,
                rst_ready               => rst_rdy,
                cmd_ready               => cmd_rdy,
                sync_cmd_type           => sync_cmd_type,
                az_cmd_type             => az_cmd_type,
                enAZ_viacmd             => enAZ_viacmd--,
            --            --debug
            --        frequency_out     => open,--
            --        frequency_cnt_out => open,
            --        freq_en_out       => open,
            --        cmd_out_cnts_out  => open,
            --        pointer_out       => open,
            --        iteration_out     => open,
            --        cmd_state_is_out  => open,
            --        cmd_reg0_out      => open,
            --        cmd_reg1_out      => open,
            --        cmd_reg2_out      => open,
            --        cmd_reg3_out      => open,
            --            --debug
            --        err_cmdtop_out    => open,
            --        warn_cmdtop_out   => open
            );
    end generate;
    --#############startread(4)##############
    process(clk40)
    begin
        if rising_edge(clk40) then
            command_rdy_dly <= command_rdy;
            startread(4)    <= startread(3);
            startread(3)    <= startread(2);
            startread(2)    <= startread(1);
            startread(1)    <= startread(0);

        end if;  --clock
    end process;

    startread(0) <= not command_rdy and (command_rdy_dly);

    ----#############GENERATES counter and cmd_sent_i##############
    process(clk40)
    begin
        if rising_edge (clk40) then
            if(rst = '1') then
                counter <= "00";
            else
                counter <= counter + '1';
            end if;
        end if;
    end process;


    process(clk40)--Main State Machine. Priority: trig, sync, cmd
    --Priority (decreasing):
    -- 4) trig,
    -- 3) sync,
    -- 2) (az (autozero)),
    -- 1) cmd
    -- 0) noop
    begin
        if rising_edge(clk40) then
            case counter is
                when "00" =>
                    if (rst = '1') then
                        ser_data_i <= idle;--Constant from rd53_package
                        dataout_i    <= idle(15 downto 12); --Constant from rd53_package
                        sync_sent_i <= '1';
                        az_sent_i   <= '0';
                        cmd_sent_i<= '0';
                    else
                        if (rst_rdy = '1') then
                            ser_data_i  <= cmd_data;
                            dataout_i   <= cmd_data(15 downto 12);
                            sync_sent_i <= '0';
                            az_sent_i   <= '0';
                            cmd_sent_i  <= '1';
                        elsif (trig_rdy = '1') then
                            ser_data_i <= trig_data;
                            dataout_i    <= trig_data(15 downto 12);
                            sync_sent_i <= '0';
                            az_sent_i   <= '0';
                            cmd_sent_i  <= '0';
                        elsif (sync_rdy = '1') then
                            ser_data_i  <= Sync;--Constant from rd53_package
                            dataout_i   <= Sync(15 downto 12);--Constant from rd53_package
                            sync_sent_i <= '1';
                            az_sent_i   <= '0';
                            cmd_sent_i  <= '0';
                        elsif (az_rdy = '1') then
                            ser_data_i  <= az;
                            dataout_i   <= az(15 downto 12);
                            sync_sent_i <= '0';
                            az_sent_i   <= '1';
                            cmd_sent_i  <= '0';
                        elsif (cmd_rdy = '1') then
                            ser_data_i  <= cmd_data;
                            dataout_i   <= cmd_data(15 downto 12);
                            sync_sent_i <= '0';
                            az_sent_i   <= '0';
                            cmd_sent_i  <= '1';
                        else
                            ser_data_i  <= idle;--Constant from rd53_package
                            dataout_i   <= idle(15 downto 12);--Constant from rd53_package
                            sync_sent_i <= '0';
                            az_sent_i   <= '0';
                            cmd_sent_i  <= '0';
                        end if;
                    end if;
                when "01" =>
                    dataout_i    <= ser_data_i(11 downto 8);
                when "10" =>
                    dataout_i    <= ser_data_i(7 downto 4);
                when "11" =>
                    dataout_i    <= ser_data_i(3 downto 0);
                when others =>
                    dataout_i    <= "0000";
            end case;
        end if;
    end process;

    sync_gen: entity work.sync_timer
        generic map(
            freq => 31
        )
        port map (
            clk => clk40,
            sync_sent => sync_sent_i,
            counter => counter,
            sync_cmd_type => sync_cmd_type,
            sync_time => sync_rdy,
            sync_counter => open
        );

    az_gen: entity work.az_controller
        generic map(
            reg_depth => 2,
            freq => 500
        )
        port map (
            rst           =>   rst,
            enAZ_viareg   =>   enAZ_in, --input no probe
            enAZ_viacmd   =>   enAZ_viacmd,
            clk40         =>   clk40,
            az_sent       =>   az_sent_i,
            az_cmd_type   =>   az_cmd_type,
            counter       =>   counter,

            az_out        =>   az,
            az_rdy_out    =>   az_rdy     ,
            --debug
            az_count_ila_out => open,
            az_rdy_ila_out   => open,
            pointer_ila_out  => open,
            state_ila_out    => open,
            az_out_cnts_ila_out => open
        );

    --    trig_gen: entity work.newtriggerunit
    --        generic map(
    --            RD53Version => RD53Version
    --        )
    --        port map (
    --            clk40        =>   clk40,
    --            rst          =>   rst,
    --            trigger      =>   trigger, --input probe10
    --            counter      =>   counter,
    --            trigger_rdy  =>   trig_rdy,
    --            enc_trig     =>   trig_data
    --        );

    ----#############GENERATES command_in40 and command_rdy40##############

    rst_fifo <= rst; --FS: rst_fifo was not written, assigning rst to it.
    wr_en_fifo <= command_rdy20 and not full_fifo;

    fifo_i : xpm_fifo_sync
        generic map (
            FIFO_MEMORY_TYPE => "auto", -- String
            FIFO_WRITE_DEPTH => 1024,   -- DECIMAL
            CASCADE_HEIGHT => 0,        -- DECIMAL
            WRITE_DATA_WIDTH => 16,     -- DECIMAL
            READ_MODE => "fwft",         -- String
            FIFO_READ_LATENCY => 1,     -- DECIMAL
            FULL_RESET_VALUE => 1,      -- DECIMAL
            USE_ADV_FEATURES => "1008", -- almost_full and data_valid
            READ_DATA_WIDTH => 16,      -- DECIMAL
            WR_DATA_COUNT_WIDTH => 1,   -- DECIMAL
            PROG_FULL_THRESH => 10,     -- DECIMAL
            RD_DATA_COUNT_WIDTH => 1,   -- DECIMAL
            PROG_EMPTY_THRESH => 10,    -- DECIMAL
            DOUT_RESET_VALUE => "0",    -- String
            ECC_MODE => "no_ecc",       -- String
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            WAKEUP_TIME => 0            -- DECIMAL
        )
        port map (
            sleep => '0',
            rst => rst_fifo,--in
            wr_clk => clk40,
            wr_en => wr_en_fifo,--in
            din => command_in20,--in
            full => full_fifo,--out
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => open,
            almost_full => almost_full_fifo,--out
            wr_ack => open,
            rd_en => ReadEnable40,--in
            dout => command_in40,
            empty => empty_fifo,--out
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => open,
            almost_empty => open,
            data_valid => command_rdy40,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );

    process(clk40)
    begin
        if rising_edge(clk40) then
            if(command_rdy = '1') then
                if(count40 = '0') then
                    command_in20(15 downto 8) <= command_in(7 downto 0);
                    command_in20(7 downto 0) <= (others => '0');
                    command_rdy20 <= '0';
                    count40 <= '1';
                else
                    command_in20(7 downto 0) <= command_in(7 downto 0);
                    command_rdy20 <= '1';
                    count40 <= '0';
                end if;
            else
                command_rdy20 <= '0';
            end if;
        end if;  --clock
    end process;

    ----#############RAM MEMORY##############


    CalTrigSeq_mem : xpm_memory_tdpram
        generic map ( -- @suppress
            ADDR_WIDTH_A         => f_log2(32),
            ADDR_WIDTH_B         => f_log2(32),
            AUTO_SLEEP_TIME      => 0,
            BYTE_WRITE_WIDTH_A   => 16,
            BYTE_WRITE_WIDTH_B   => 16,
            CLOCKING_MODE        => "independent_clock",
            ECC_MODE             => "no_ecc",
            MEMORY_INIT_FILE     => "none", --../../../../sources/ip_cores/kintexUltrascale/CalPulseTrigSeq.mem",
            --MEMORY_INIT_PARAM    => "817e,6969,6363,a971,a66a,6969,6969,6969,6969,6969,6969,6969,6969,6969,6969,6969,6969,566a,566c,5671,5672,6969,6969,6969,6969,6969,6969,6969,6969,6969,6969,6969", --digital scan sequence
            MEMORY_INIT_PARAM    => "817e,817e,aaaa,63a6,a66c,936a,aaaa,aaaa,aaaa,aaaa,aaaa,aaaa,aaaa,aaaa,aaaa,aaaa,aaaa,aaaa,4e6a,566c,5671,5672,2e74,aaaa,aaaa,aaaa,aaaa,aaaa,aaaa,aaaa,0101,0202",
            MEMORY_OPTIMIZATION  => "true",
            MEMORY_PRIMITIVE     => "auto",
            MEMORY_SIZE          => 16*32,
            MESSAGE_CONTROL      => 1,
            READ_DATA_WIDTH_A    => 16,
            READ_DATA_WIDTH_B    => 16,
            READ_LATENCY_A       => 1,
            READ_LATENCY_B       => 1,
            READ_RESET_VALUE_A   => "0",
            READ_RESET_VALUE_B   => "0",
            RST_MODE_A           => "SYNC",
            RST_MODE_B           => "SYNC",
            USE_EMBEDDED_CONSTRAINT => 0,
            USE_MEM_INIT         => 1,
            WAKEUP_TIME          => "disable_sleep",
            WRITE_DATA_WIDTH_A   => 16,
            WRITE_DATA_WIDTH_B   => 16,
            WRITE_MODE_A         => "write_first",
            WRITE_MODE_B         => "write_first"
        )
        port map (
            sleep => '0',
            clka => clk40,
            rsta => rst,
            ena => '1',
            regcea => '1',
            wea => CalTrigSeq_WE, --always 0
            addra => CalTrigSeq_WRADDR,
            dina => CalTrigSeq_WRDATA,
            injectsbiterra => '0',
            injectdbiterra => '0',
            douta          => open,
            sbiterra       => open,
            dbiterra => open,
            clkb => clk40,
            rstb => rst,
            enb => '1',
            regceb => '1',
            web => "0",
            addrb          => ReadAddrCalTrigSeq,
            dinb           => zeros16bit,
            injectsbiterrb => '0',
            injectdbiterrb => '0',
            doutb => CalTrigSeq_in,
            sbiterrb => open,
            dbiterrb => open
        );


    --    register_map_control.ENCODING_ITKPIX_TRIGGER_GENERATOR.NO_INJECT <= "0";
    --    register_map_control.ENCODING_ITKPIX_TRIGGER_GENERATOR.EDGE_MODE <= "1";
    --    register_map_control.ENCODING_ITKPIX_TRIGGER_GENERATOR.EDGE_DELAY <= "00000";
    --    register_map_control.ENCODING_ITKPIX_TRIGGER_GENERATOR.EDGE_DURATION <= "00010100";
    --    register_map_control.ENCODING_ITKPIX_TRIGGER_GENERATOR.TRIG_DELAY <= "00111010";
    --    register_map_control.ENCODING_ITKPIX_TRIGGER_GENERATOR.TRIG_MULTIPLIER <= "010000";

    --    process(clk40)
    --    begin
    --        if rising_edge(clk40) then
    --            RD53B_loopgen_reg.noInject          <= to_sl(register_map_control.ENCODING_ITKPIX_TRIGGER_GENERATOR.NO_INJECT);
    --            RD53B_loopgen_reg.edgeMode          <= to_sl(register_map_control.ENCODING_ITKPIX_TRIGGER_GENERATOR.EDGE_MODE);
    --            RD53B_loopgen_reg.edgeDelay         <= register_map_control.ENCODING_ITKPIX_TRIGGER_GENERATOR.EDGE_DELAY;
    --            RD53B_loopgen_reg.edgeDuration      <= register_map_control.ENCODING_ITKPIX_TRIGGER_GENERATOR.EDGE_DURATION;
    --            RD53B_loopgen_reg.trigDelay         <= register_map_control.ENCODING_ITKPIX_TRIGGER_GENERATOR.TRIG_DELAY;
    --            RD53B_loopgen_reg.trigMultiplier    <= register_map_control.ENCODING_ITKPIX_TRIGGER_GENERATOR.TRIG_MULTIPLIER;
    --        end if;
    --    end process;

    --    RD53B_loopgen_reg.noInject          <= '0';
    --    RD53B_loopgen_reg.edgeMode          <= '0'; --'1';
    --    RD53B_loopgen_reg.edgeDelay         <= "00000";
    --    RD53B_loopgen_reg.edgeDuration      <= "00010100";
    --    RD53B_loopgen_reg.trigDelay         <= "01111010"; --"00111010";
    --    RD53B_loopgen_reg.trigMultiplier    <= "010000";

    --analog
    RD53B_loopgen_reg.noInject          <= '0';
    RD53B_loopgen_reg.edgeMode          <= '0'; --'1';
    RD53B_loopgen_reg.edgeDelay         <= "00000";
    RD53B_loopgen_reg.edgeDuration      <= "00101000";
    RD53B_loopgen_reg.trigDelay         <= "00111000"; --"00111010";
    RD53B_loopgen_reg.trigMultiplier    <= "010000";

    --ENCODING_ITKPIX_TRIGGER_GENERATOR_NO_INJECT        0
    --ENCODING_ITKPIX_TRIGGER_GENERATOR_EDGE_MODE        0
    --ENCODING_ITKPIX_TRIGGER_GENERATOR_EDGE_DELAY       0
    --ENCODING_ITKPIX_TRIGGER_GENERATOR_EDGE_DURATION    40
    --ENCODING_ITKPIX_TRIGGER_GENERATOR_TRIG_DELAY       56
    --ENCODING_ITKPIX_TRIGGER_GENERATOR_TRIG_MULTIPLIER  16



    ----#############SIMULATION##############

    --rst             not probed
    --command_in      probe0          data, 1 byte per clock
    --command_rdy     probe1          high when data
    --trigger         probe10         always 0
    --enAZ_in         not probed
    --CalTrigSeq_in   probe61         always 817e now from memory

    sim_proc : process(clk40)
    begin
        if clk40'event and clk40='1' then
            ttime <= ttime + 1;
            if ttime = 1 then
                rst <= '1';
            elsif ttime = 10 then
                rst <= '0';
            elsif ttime = 20 then
                command_in      <= (others => '0');
                command_rdy     <= '0';
                trigger         <= '0';
            --CalTrigSeq_in   <= x"817e";
            --            elsif ttime = 50 or ttime = 150 or ttime = 171 or ttime = 192 or ttime = 205 then
            --                trigger         <= '1';
            --            elsif ttime = 51 or ttime = 151 or ttime = 172 or ttime = 193 or ttime = 206 then
            --                trigger         <= '0';
            --            elsif ttime = 29 then
            --                time_start <= ttime+1;
            --                command_sim(WrReg0_size*8-1 downto 0) <= x"66_6a_6a_6c_9c_a5_cc_a6";--4x2 bytes = 64 bits
            --                size_sim <= WrReg0_size;

            --            elsif ttime = 80 then
            --                time_start <= ttime+1;
            --                command_sim(Cmd2wrd_size*8-1 downto 0) <= x"81_7e";--sync
            --                size_sim <= Cmd2wrd_size;

            --            elsif ttime = 100 then
            --                time_start <= ttime+1;
            --                command_sim(Cmd2wrd_size*8-1 downto 0) <= x"5a_6a";--random trigger
            --                size_sim <= Cmd2wrd_size;

            ----          ************test multiple commands back to back************
            ----          --129 then 156 for back to back without idle. 157 with idle WrReg0_size+4 66_6a_6a_6c_9c_a5_cc_a6_65_6a_6a_6a
            --            elsif ttime = 129 then
            --                 time_start <= ttime+1;
            --                 command_sim((WrReg0_size+4)*8-1 downto 0) <= x"66_6a_6a_6c_9c_a5_cc_a6_65_6a_6a_6a";
            --                 size_sim <= WrReg0_size+4;
            --            elsif ttime = 157 then
            --                 time_start <= ttime+1;
            --                 command_sim((WrReg0_size+4)*8-1 downto 0) <= x"66_6a_6a_6c_9c_a5_cc_a6_65_6a_6a_6a";
            --                 size_sim <= WrReg0_size+4;

            ----          ************test six shot commands back to back to back to back to back to back************
            --                        elsif ttime = 129 then
            --                             time_start <= ttime+1;
            --                             command_sim((WrReg0_size+4)*8-1 downto 0) <= x"5a_6a_5c_6a_81_7e_aa_aa_4e_6a_81_7e";--6x2 bytes = 96 bits
            --                             size_sim <= WrReg0_size+4;

            ----          ************test broken long command************
            --            elsif ttime = 129 then
            --                 time_start <= ttime+1;
            --                 command_sim((WrReg0_size-4)*8-1 downto 0) <= x"66_6a_6a_6c";
            --                 size_sim <= WrReg0_size-4;
            --            elsif ttime = 153 then
            --                 time_start <= ttime+1;
            --                 command_sim((WrReg0_size-4)*8-1 downto 0) <= x"9c_a5_cc_a6";
            --                 size_sim <= WrReg0_size-4;

            ----          ************test one short command************
            --            elsif ttime = 128 then
            --                 time_start <= ttime+1;
            --                 command_sim(Cmd2wrd_size*8-1 downto 0) <= x"81_7e";--sync
            --                 size_sim <= Cmd2wrd_size;

            --            ************test short command followed by long command************
            --                        elsif ttime = 128 then
            --                             time_start <= ttime+1;
            --                             command_sim(3*Cmd2wrd_size*8-1 downto 0) <= x"81_7e_65_6a_6a_6a";--sync
            --                             size_sim <= 3*Cmd2wrd_size;

            ----          ************test bad bad good e************
            --            elsif ttime = 128 then
            --                 time_start <= ttime+1;
            --                 command_sim(3*Cmd2wrd_size*8-1 downto 0) <= x"1e_21_81_7d_81_7e";--sync
            --                 size_sim <= 3*Cmd2wrd_size;

            --          ************test good bad bad ************
            --                        elsif ttime = 228 then
            --                             time_start <= ttime+1;
            --                             command_sim(3*Cmd2wrd_size*8-1 downto 0) <= x"81_7e_1e_21_81_7d";--sync
            --                             size_sim <= 3*Cmd2wrd_size;

            ----          ************test good bad good ***********
            --            elsif ttime = 328 then
            --                 time_start <= ttime+1;
            --                 command_sim(3*Cmd2wrd_size*8-1 downto 0) <= x"81_7e_81_7d_81_7e";--sync
            --                 size_sim <= 3*Cmd2wrd_size;

            ----          ************test good bad bad good ***********
            --            elsif ttime = 428 then
            --                 time_start <= ttime+1;
            --                 command_sim(4*Cmd2wrd_size*8-1 downto 0) <= x"81_7e_81_7d_81_7c_81_7e";--sync
            --                size_sim <= 4*Cmd2wrd_size;

            ----          ************test good bad bad bad bad good ***********
            --                        elsif ttime = 528 then
            --                             time_start <= ttime+1;
            --                             command_sim(6*Cmd2wrd_size*8-1 downto 0) <= x"81_7e_81_7d_81_7c_81_7d_81_7c_81_7e";--sync
            --                            size_sim <= 6*Cmd2wrd_size;

            ----          ************test long command with a short command in the middle***********
            --            elsif ttime = 129 then
            --                 time_start <= ttime+1;
            --                 command_sim((WrReg0_size+2)*8-1 downto 0) <= x"66_6a_6a_6c_81_7e_9c_a5_cc_a6";
            --                 size_sim <= WrReg0_size+2;

            ----          ************test broken long command with one short command in the middle************
            --            elsif ttime = 129 then
            --                 time_start <= ttime+1;
            --                 command_sim((WrReg0_size-4)*8-1 downto 0) <= x"66_6a_6a_6c";
            --                 size_sim <= WrReg0_size-4;
            --            elsif ttime = 153 then
            --                 time_start <= ttime+1;
            --                 command_sim(Cmd2wrd_size*8-1 downto 0) <= x"81_7e";--sync
            --                 size_sim <= Cmd2wrd_size;
            --            elsif ttime = 177 then
            --                 time_start <= ttime+1;
            --                 command_sim((WrReg0_size-4)*8-1 downto 0) <= x"9c_a5_cc_a6";
            --                 size_sim <= WrReg0_size-4;

            ----          ************test six short commands interupted by and priority command in the middle************
            --            elsif ttime = 153 then
            --                 time_start <= ttime+1;
            --                 command_sim(Cmd2wrd_size*8-1 downto 0) <= x"81_7e";--sync
            --                 size_sim <= Cmd2wrd_size;
            --            elsif ttime = 277 then
            --                 time_start <= ttime+1;
            --                 command_sim(6*Cmd2wrd_size*8-1 downto 0) <= x"5a_6a_5c_6a_5a_6a_aa_aa_4e_6a_81_7e";--6x2 bytes = 96 bits
            --                 size_sim <= 6*Cmd2wrd_size;

            ----          ************test three short commands interupted by and priority command replacing the last command************
            --            elsif ttime = 153 then
            --                 time_start <= ttime+1;
            --                 command_sim(Cmd2wrd_size*8-1 downto 0) <= x"81_7e";--sync
            --                 size_sim <= Cmd2wrd_size;
            --            elsif ttime = 277 then
            --                 time_start <= ttime+1;
            --                 command_sim(3*Cmd2wrd_size*8-1 downto 0) <= x"5a_6a_5c_6a_5a_6a";--6x2 bytes = 96 bits
            --                 size_sim <= 3*Cmd2wrd_size;

            ----          ************test cal command with priority command replacing the last word************
            --            elsif ttime = 153 then
            --                 time_start <= ttime+1;
            --                 command_sim(Cmd2wrd_size*8-1 downto 0) <= x"81_7e";--sync
            --                 size_sim <= Cmd2wrd_size;
            --            elsif ttime = 277 then
            --                 time_start <= ttime+1;
            --                 command_sim(3*Cmd2wrd_size*8-1 downto 0) <= x"63_a6_a6_6c_93_6a";--6x2 bytes = 96 bits
            --                 size_sim <= 3*Cmd2wrd_size;

            ----          ************test broken long command with one short command in the middle followed by 6 short commands interupted by an external sync************
            --            elsif ttime = 129 then
            --                 time_start <= ttime+1;
            --                 command_sim((WrReg0_size-4)*8-1 downto 0) <= x"66_6a_6a_6c";
            --                 size_sim <= WrReg0_size-4;
            --            elsif ttime = 153 then
            --                 time_start <= ttime+1;
            --                 command_sim(Cmd2wrd_size*8-1 downto 0) <= x"81_7e";--sync
            --                 size_sim <= Cmd2wrd_size;
            --            elsif ttime = 177 then
            --                 time_start <= ttime+1;
            --                 command_sim((WrReg0_size-4)*8-1 downto 0) <= x"9c_a5_cc_a6";
            --                 size_sim <= WrReg0_size-4;
            --            elsif ttime = 277 then
            --                 time_start <= ttime+1;
            --                 command_sim(6*Cmd2wrd_size*8-1 downto 0) <= x"5a_6a_5c_6a_5a_6a_aa_aa_4e_6a_81_7e";--6x2 bytes = 96 bits
            --                 size_sim <= 6*Cmd2wrd_size;

            ----          ************test broken long command with one short command check if valid_dly='0' valid='1' and cmd_sent='1' is ever possible************
            ----          it happens and it breaks the code. fixed
            --            elsif ttime = 129 then
            --                 time_start <= ttime+1;
            --                 command_sim((WrReg0_size-4)*8-1 downto 0) <= x"66_6a_6a_6c";
            --                 size_sim <= WrReg0_size-4;
            --            elsif ttime = 143 then
            --                 time_start <= ttime+1;
            --                 command_sim(Cmd2wrd_size*8-1 downto 0) <= x"81_7e";--sync
            --                 size_sim <= Cmd2wrd_size;
            --            elsif ttime = 177 then
            --                 time_start <= ttime+1;
            --                 command_sim((WrReg0_size-4)*8-1 downto 0) <= x"9c_a5_cc_a6";
            --                 size_sim <= WrReg0_size-4;

            ----          ************test broken long command with one short command in the middle and triggers interupting************
            --            elsif ttime = 129 then --triggers at 143 and 151 bumps this message
            --                 time_start <= ttime+1;
            --                 command_sim((WrReg0_size-4)*8-1 downto 0) <= x"66_6a_6a_6c";
            --                 size_sim <= WrReg0_size-4;
            --            elsif ttime = 153 then --trigger at 166 bumps this message
            --                 time_start <= ttime+1;
            --                 command_sim(Cmd2wrd_size*8-1 downto 0) <= x"81_7e";--sync
            --                 size_sim <= Cmd2wrd_size;
            --                 trigger <= '0';
            --            elsif ttime = 177 then --triggers at 191 and 199 bumps this message
            --                 time_start <= ttime+1;
            --                 command_sim((WrReg0_size-4)*8-1 downto 0) <= x"9c_a5_cc_a6";
            --                 size_sim <= WrReg0_size-4;
            --            elsif ttime = 166 or ttime = 143 or ttime = 151 or ttime = 191 or ttime = 199 then
            --                trigger <= '1';
            --            elsif ttime = 167 or ttime = 144 or ttime = 152 or ttime = 192 or ttime = 200 then
            --                trigger <= '0';

            --          ************test long write command stopped by a normal write command************
            --                        elsif ttime = 129 then
            --                             time_start <= ttime+1;
            --                             command_sim((WrReg1_size - 0)*8-1 downto 0) <= x"66_a6_a6_6a_00_01_02_03_04_05_06_07_08_09_10_11_12_13_14_15_16_17_18_19_20_21_22_23_24_25_26_27_28_29_30_31"; --18x2 bytes = 288 bits
            --                             size_sim <= WrReg1_size - 0;
            --                        elsif ttime = 290 then
            --                             time_start <= ttime+1;
            --                             command_sim(WrReg0_size*8-1 downto 0) <= x"66_6a_6a_6c_9c_a5_cc_a6";--4x2 bytes = 64 bits
            --                             size_sim <= WrReg0_size;

            --            --************test long write command stopped by a normal write command. first word of the long write command shows up before anything else************
            --            elsif ttime = 129 then
            --                 time_start <= ttime+1;
            --                 command_sim(Cmd2wrd_size*8-1 downto 0) <= x"66_a6"; --18x2 bytes = 288 bits
            --                 size_sim <= Cmd2wrd_size;
            --            elsif ttime = 180 then
            --                 time_start <= ttime+1;
            --                 command_sim((WrReg1_size - 2)*8-1 downto 0) <= x"a6_6a_00_01_02_03_04_05_06_07_08_09_10_11_12_13_14_15_16_17_18_19_20_21_22_23_24_25_26_27_28_29_30_31"; --18x2 bytes = 288 bits
            --                 size_sim <= WrReg1_size - 2;
            --            elsif ttime = 290 then
            --                 time_start <= ttime+1;
            --                 command_sim(WrReg0_size*8-1 downto 0) <= x"66_6a_6a_6c_9c_a5_cc_a6";--4x2 bytes = 64 bits
            --                 size_sim <= WrReg0_size;

            --            --************test long write command stopped by a normal write command. first word of the long write command shows up before anything else. first word of normal write command at the end of long write command************
            --            elsif ttime = 129 then
            --                 time_start <= ttime+1;
            --                 command_sim(Cmd2wrd_size*8-1 downto 0) <= x"66_a6"; --18x2 bytes = 288 bits
            --                 size_sim <= Cmd2wrd_size;
            --            elsif ttime = 180 then
            --                 time_start <= ttime+1;
            --                 command_sim((WrReg1_size - 0)*8-1 downto 0) <= x"a6_6a_00_01_02_03_04_05_06_07_08_09_10_11_12_13_14_15_16_17_18_19_20_21_22_23_24_25_26_27_28_29_30_31_66_6a"; --18x2 bytes = 288 bits
            --                 size_sim <= WrReg1_size - 0;
            --            elsif ttime = 290 then
            --                 time_start <= ttime+1;
            --                 command_sim((WrReg0_size -2)*8-1 downto 0) <= x"6a_6c_9c_a5_cc_a6";--4x2 bytes = 64 bits
            --                 size_sim <= WrReg0_size -2;

            --            --************test see description************
            --            elsif ttime = 129 then -- first word of long write command
            --                time_start <= ttime+1;
            --                command_sim(Cmd2wrd_size*8-1 downto 0) <= x"66_a6"; --18x2 bytes = 288 bits
            --                size_sim <= Cmd2wrd_size;
            --            elsif ttime = 180 then -- long write command continues
            --                time_start <= ttime+1;
            --                command_sim((WrReg1_size - 0)*8-1 downto 0) <= x"a6_6a_a6_6a_a6_6c_a6_6a_a6_6c_a6_6a_a6_6c_a6_6b_a6_6c_a6_6a_a6_6c_a6_6a_a6_6c_a6_6a_a6_6c_a6_6a_a6_6c_a6_6a"; --18x2 bytes = 288 bits
            --                size_sim <= WrReg1_size - 0;
            --            elsif ttime = 250 then --long write command ends normal write comand starts
            --                time_start <= ttime+1;
            --                command_sim((2*Cmd2wrd_size)*8-1 downto 0) <= x"a6_6a_66_6a";
            --                size_sim <= 2*Cmd2wrd_size;
            --            elsif ttime = 290 then --sync
            --                time_start <= ttime+1;
            --                command_sim((Cmd2wrd_size)*8-1 downto 0) <= x"81_7e";
            --                size_sim <= Cmd2wrd_size;
            --            elsif ttime = 330 then -- normal write command ends long write command starts
            --                time_start <= ttime+1;
            --                command_sim((WrReg0_size + 2)*8-1 downto 0) <= x"6a_6c_9c_a5_cc_a6_66_a6_a6_6a";
            --                size_sim <= WrReg0_size + 2;
            --            elsif ttime = 360 then --write 4 words of long write command
            --                time_start <= ttime+1;
            --                command_sim(WrReg0_size*8-1 downto 0) <= x"6a_6c_6a_6c_6a_6c_6a_6c";
            --                size_sim <= WrReg0_size;
            --            elsif ttime = 400 then --sends six short commands
            --                time_start <= ttime+1;
            --                command_sim(6*Cmd2wrd_size*8-1 downto 0) <= x"5a_6a_5c_6a_5a_6a_aa_aa_4e_6a_81_7e";
            --                size_sim <= 6*Cmd2wrd_size;
            --            elsif ttime = 480 then --ends with cal command
            --                time_start <= ttime+1;
            --                command_sim(3*Cmd2wrd_size*8-1 downto 0) <= x"63_a6_a6_6c_93_6a";
            --                size_sim <= 3*Cmd2wrd_size;

            --          --************test resets************
            --            elsif ttime = 101 then
            --                 time_start <= ttime+1;
            --                 command_sim(2*Cmd2wrd_size*8-1 downto 0) <= x"ff_ff_81_7e";--reset
            --                 size_sim <= 2*Cmd2wrd_size;
            --            elsif ttime = 181 then
            --                 time_start <= ttime+1;
            --                 command_sim(Cmd2wrd_size*8-1 downto 0) <= x"00_00";--reset
            --                 size_sim <= Cmd2wrd_size;

            --          ************test user command************
            elsif ttime = 149 then
                time_start <= ttime+1;
                command_sim(Cmd2wrd_size*8-1 downto 0) <= x"E0_2f";--x"EC_81"--x"E0_22";--isCalTrigSeq --E0_A2
                size_sim <= Cmd2wrd_size;

            --                      ************test user command after sync************
            --            elsif ttime = 129 then
            --                time_start <= ttime+1;
            --                command_sim(2*Cmd2wrd_size*8-1 downto 0) <= x"81_7e_EC_91";--isCalTrigSeq --E0_A2
            --                size_sim <= 2*Cmd2wrd_size;

            --          ************test user command after cal************
            --                        elsif ttime = 129 then
            --                             time_start <= ttime+1;
            --                             command_sim(4*Cmd2wrd_size*8-1 downto 0) <= x"63_a6_a6_6c_93_6a_E0_22";--isCalTrigSeq --E0_A2
            --                             size_sim <= 4*Cmd2wrd_size;

            --          ************test user command after long write************
            --                        elsif ttime = 129 then
            --                             time_start <= ttime+1;
            --                             command_sim(6*Cmd2wrd_size*8-1 downto 0) <= x"66_a6_a6_6a_6a_6c_9c_a5_cc_a6_E0_22";--isCalTrigSeq --E0_A2
            --                             size_sim <= 6*Cmd2wrd_size;

            --          ************test user command after long write and a pause************
            --                        elsif ttime = 129 then
            --                             time_start <= ttime+1;
            --                             command_sim(5*Cmd2wrd_size*8-1 downto 0) <= x"66_a6_a6_6a_6a_6c_9c_a5_cc_a6";--isCalTrigSeq --E0_A2
            --                             size_sim <= 5*Cmd2wrd_size;
            --                        elsif ttime = 209 then
            --                             time_start <= ttime+1;
            --                             command_sim(Cmd2wrd_size*8-1 downto 0) <= x"E0_22";--isCalTrigSeq --E0_A2
            --                             size_sim <= Cmd2wrd_size;

            --          ************test reset after long write************
            --                        elsif ttime = 129 then
            --                             time_start <= ttime+1;
            --                             command_sim(6*Cmd2wrd_size*8-1 downto 0) <= x"66_a6_a6_6a_6a_6c_9c_a5_cc_a6_ff_ff";--isCalTrigSeq --E0_A2
            --                             size_sim <= 6*Cmd2wrd_size;

            --            --          ************test resets************
            --                        elsif ttime = 101 then
            --                             time_start <= ttime+1;
            --                             command_sim(2*Cmd2wrd_size*8-1 downto 0) <= x"81_7e_ff_ff";--reset
            --                             size_sim <= 2*Cmd2wrd_size;

            --            --          ************test reset after cal************
            --                        elsif ttime = 129 then
            --                             time_start <= ttime+1;
            --                             command_sim(4*Cmd2wrd_size*8-1 downto 0) <= x"63_a6_a6_6c_93_6a_ff_ff";--
            --                             size_sim <= 4*Cmd2wrd_size;

            --           elsif ttime = 399 then
            --                 time_start <= ttime+1;
            --                 command_sim(WrReg0_size*8-1 downto 0) <= x"66_6a_6a_6c_9c_a5_cc_a6";--4x2 bytes = 64 bits
            --                 size_sim <= WrReg0_size;
            --            elsif ttime = 499 then
            --                 time_start <= ttime+1;
            --                 command_sim(RdReg_size*8-1 downto 0) <= x"65_6a_72_c9";--2x2 bytes = 32 bits
            --                 size_sim <= RdReg_size;
            --            elsif ttime = 499 then
            --                 time_start <= ttime+1;
            --                 command_sim(WrReg1_size*8-1 downto 0) <= x"66_a6_a6_6a_9c_a5_cc_a6_9c_a5_cc_a6_9c_a5_cc_a6_9c_a5_cc_a6_9c_a5_cc_a6_9c_a5_cc_a6_9c_a5_cc_a6_9c_a5_cc_a6"; --18x2 bytes = 288 bits
            --                 size_sim <= WrReg1_size;
            --            elsif ttime = 499 then
            --                 time_start <= ttime+1;
            --                 command_sim((WrReg1_size - 0)*8-1 downto 0) <= x"66_a6_a6_6a_00_01_02_03_04_05_06_07_08_09_10_11_12_13_14_15_16_17_18_19_20_21_22_23_24_25_26_27_28_29_30_31"; --18x2 bytes = 288 bits
            --                 size_sim <= WrReg1_size - 0;
            --            elsif ttime = 641 then
            --                 time_start <= ttime+1;
            --                 command_sim(RdReg_size*8-1 downto 0) <= x"65_6a_72_c9";--2x2 bytes = 32 bits
            --                 size_sim <= RdReg_size;
            --            elsif ttime = 799 then
            --                 time_start <= ttime+1;
            --                 command_sim((WrReg1_size - 0)*8-1 downto 0) <= x"66_a6_a6_6a_00_01_02_03_04_05_06_07_08_09_10_11_12_13_14_15_16_17_18_19_20_21_22_23_24_25_26_27_28_29_30_31"; --18x2 bytes = 288 bits
            --                 size_sim <= WrReg1_size - 0;
            --            elsif ttime = 941 then
            --                 time_start <= ttime+1;
            --                 command_sim(RdReg_size*8-1 downto 0) <= x"32_33_34_35";--2x2 bytes = 32 bits
            --                 size_sim <= RdReg_size;
            elsif ttime > time_start and ttime < time_start + size_sim+1 then
                command_in<=command_sim((time_start+size_sim-ttime)*8+7 downto (time_start+size_sim-ttime)*8);
                command_rdy <= '1';
            elsif ttime = time_start + size_sim + 1 then
                command_in <= (others => '0');
                command_rdy <= '0';
            end if;
        end if;
    end process;

end tb;

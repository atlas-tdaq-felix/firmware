
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

library ieee;
    use ieee.std_logic_1164.all;
    use IEEE.NUMERIC_STD.ALL;
    use ieee.numeric_std_unsigned.all;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.FELIX_package.all;
    use work.centralRouter_package.all;
    use work.pcie_package.all;

entity LTITTC_sim_data_generation is
    generic(
        clock_phased    : boolean := false
    );
    port(
        reset               : in std_logic;
        clk40               : in std_logic;
        clk40_LTI           : in std_logic;
        clk160              : in std_logic;
        clk160_LTI          : in std_logic;
        clk240_LTI          : in std_logic;
        en_gen_ttc_data     : in std_logic;
        data_alignment_done : out std_logic;
        data_out_P          : out std_logic;
        data_out_N          : out std_logic
    );
end LTITTC_sim_data_generation;

architecture Behavioral of LTITTC_sim_data_generation is

    constant GBT_NUM                        : integer := 1;
    signal i_cnt                            : integer range 0 to 3;
    signal cnt_IsK                          : integer range 0 to 1024 := 0;
    signal val_IsK                          : integer range 0 to 1024 := 5;
    signal error_IsK                        : std_logic := '0';

    signal clk40_transmitter                : std_logic := '0';
    signal clk160_generation                : std_logic := '0';

    signal BCR_count_en                     : std_logic := '0';

    signal TTC_CONFIG_IN                    : std_logic_vector(31 downto 0) := (others => '0');

    signal ECR_r                            : std_logic;
    signal trigger_r                        : std_logic;
    signal BCR_r                            : std_logic;
    signal L1A_COUNT_read                   : std_logic_vector(31 downto 0);
    signal L1A_AUTO_READY                   : std_logic;
    signal BUSY_PULSE_COUNT                 : std_logic_vector(31 downto 0);
    signal enb                              : std_logic;
    signal web                              : std_logic_vector( 3 downto 0);
    signal addrb                            : std_logic_vector(31 downto 0);
    signal BCR_PERIOD_COUNT_OUT             : std_logic_vector(11 downto 0);
    signal L1A_COUNT_OUT                    : std_logic_vector(37 downto 0);
    signal ORBITID_COUNT_OUT                : std_logic_vector(31 downto 0);

    signal TTCin                            : TTC_data_type := TTC_zero;
    signal toHostXoff                       : std_logic_vector(23 downto 0) := (others => '0');
    signal LTI_TXUSRCLK_in                  : std_logic_vector(GBT_NUM-1 downto 0);

    signal LTI_gen_Data_Transceiver         : array_32b(0 to GBT_NUM - 1);
    signal LTI_gen_CharIsK                  : array_4b(0 to GBT_NUM - 1);

    signal gen_TXP_OUT                      : std_logic_vector(0 downto 0);
    signal gen_TXN_OUT                      : std_logic_vector(0 downto 0);

begin

    clk40_transmitter <= clk40_LTI  when clock_phased else clk40;
    clk160_generation <= clk160_LTI when clock_phased else clk160;

    gen_BCR_count_en : process(clk160_generation)
    begin
        if (rising_edge(clk160_generation)) then
            if (i_cnt < 3) then
                i_cnt  <= i_cnt + 1;
            else
                i_cnt  <= 0;
            end if;
        end if;
    end process;
    BCR_count_en <= '1' when (i_cnt = 3) and en_gen_ttc_data = '1' else '0';

    TTC_CONFIG_IN(31 downto 1) <= (others => '0');
    TTC_CONFIG_IN(0) <= en_gen_ttc_data;

    --took edge_det from https://gitlab.cern.ch/atlas-tdaq-felix/lti_ttc_emulator
    gen_TTC_data : entity work.edge_det
        port map(
            clk_160M             => clk160_generation,
            sys_rst_n            => not reset,
            ECR_push             => '0',
            trigger_push         => '0',
            busy_in              => '0',
            SORB_0               => '0',
            BCR_count_en         => BCR_count_en,
            BCR_PERIOD_IN        => x"00000DEC", --3464
            L1A_COUNT_IN         => x"00000064",
            L1A_PERIOD_IN        => x"00000032",
            TTC_CONFIG_IN        => TTC_CONFIG_IN,
            LUT_CONT_EN          => '0',
            L1A_AUTO_HALT        => '0',
            BUSY_EN              => '0',
            doutb                => x"00000000",
            ECR_r                => ECR_r,
            trigger_r            => trigger_r,
            BCR_r                => BCR_r,
            L1A_COUNT_read       => L1A_COUNT_read,
            L1A_AUTO_READY       => L1A_AUTO_READY,
            BUSY_PULSE_COUNT     => BUSY_PULSE_COUNT,
            enb                  => enb,
            web                  => web,
            addrb                => addrb,
            BCR_PERIOD_COUNT_OUT => BCR_PERIOD_COUNT_OUT,
            L1A_COUNT_OUT        => L1A_COUNT_OUT,
            ORBITID_COUNT_OUT    => ORBITID_COUNT_OUT
        );

    gen_TTCin_proc : process(clk40_transmitter)
    begin
        if rising_edge(clk40_transmitter) then
            --table 2.3 of the phase 2 ttc speficications
            --Header
            TTCin.PT               <= '0';                  --PartitionType: 0=ATLAS partition, 1=Standalone partition
            TTCin.Partition        <= (others => '0');      --Partition: Partition number (0,1,2,3)
            TTCin.BCID             <= BCR_PERIOD_COUNT_OUT; --BCID: Bunch Counter ID associated to the L0A
            TTCin.SyncUserData     <= (others => '0');      --SyncUser: Synchronous user data
            TTCin.SyncGlobalData   <= (others => '0');      --SyncGlobal: Synchronous global data
            TTCin.TS               <= '0';                  --Turn Signal: Pulse every LHC turn
            TTCin.ErrorFlags       <= (others => '0');      --Error Flags: Error flags (to be defined)
            --TTC Message
            TTCin.SL0ID            <= '0';                  --SetL0ID: Set Local FE L0ID command
            TTCin.SOrb             <= '0';                  --SetOrbitID: Set local OrbitID counter command
            TTCin.Sync             <= '0';                  --Sync: Additional synchronisation signal
            TTCin.GRst             <= '0';                  --GReset: Global FE re-initialisation command
            TTCin.L0A              <= trigger_r;            --L0A: L0 Trigger Accept
            TTCin.L0ID             <= L1A_COUNT_OUT;        --L0ID: L0 Identifier associated with the L0A or SetL0ID command
            TTCin.OrbitID          <= ORBITID_COUNT_OUT;    --OrbitID: Orbit identifier associated with L0A or SetOrbitID command
            TTCin.TriggerType      <= (others => '0');      --TriggerType: Trigger type word associated with L0A
            TTCin.LBID             <= (others => '0');      --LBID: Luminosity block identifier
            --Data Message
            TTCin.AsyncUserData    <= (others => '0');      --AsyncUser: Asynchronous user data
        end if;
    end process;

    g_LTI_TXCLK: for i in 0 to GBT_NUM-1 generate
        LTI_TXUSRCLK_in(i) <= clk240_LTI;
    end generate;

    TTC_LTI_Transmitter_inst: entity work.TTC_LTI_Transmitter generic map(
            GBT_NUM => GBT_NUM
        )
        port map(
            clk40 => clk40_transmitter,
            daq_fifo_flush => reset,
            TTCin => TTCin,
            XoffIn => toHostXoff(GBT_NUM-1 downto 0),
            LTI_TX_Data_Transceiver => LTI_gen_Data_Transceiver,
            LTI_TX_TX_CharIsK => LTI_gen_CharIsK,
            LTI_TXUSRCLK_in => LTI_TXUSRCLK_in
        );

    checker_IsK_proc : process(clk240_LTI)
    begin
        if rising_edge(clk240_LTI) then
            if LTI_gen_CharIsK(0) = 1 then
                cnt_IsK <= 0;
                val_IsK <= cnt_IsK;
            else
                cnt_IsK <= cnt_IsK + 1;
            end if;
        end if;
    end process;

    error_IsK <= '0' when val_IsK = 5 else '1';

    LTITTC_sim_genRX_wrapper_inst: entity work.LTITTC_sim_genRX_wrapper
        port map(
            rst_hw                          => reset,
            clk40_in                        => clk40,
            clk240_in                       => clk240_LTI,
            TX_DATA_32b_in                  => LTI_gen_Data_Transceiver(0),
            ISK_in                          => LTI_gen_CharIsK(0),
            alignment_done                  => data_alignment_done,
            TXN_OUT                         => gen_TXN_OUT,
            TXP_OUT                         => gen_TXP_OUT
        );
    data_out_P <= gen_TXP_OUT(0);
    data_out_N <= gen_TXN_OUT(0);

end Behavioral;

----------------------------------------------------------------------------------
--! Company:  Argonne National Laboratory
--! Engineer: Marco Trovato (mtrovato@anl.gov)
----------------------------------------------------------------------------------
--rluz: adapted from sources/ttc/ltittc_wrapper/FLX_LTITTCLink_Wrapper.vhd on 08/05/2024
LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
library XPM;
    use xpm.vcomponents.all;

library UNISIM;
    use UNISIM.VComponents.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;
    use work.centralRouter_package.all;--temp

entity LTITTC_sim_genRX_wrapper is
    Port (
        rst_hw                          : in  std_logic;
        clk40_in                        : in  std_logic;
        clk240_in                       : in  std_logic;
        TX_DATA_32b_in                  : in  std_logic_vector(31 downto 0);
        ISK_in                          : in  std_logic_vector( 3 downto 0);
        alignment_done                  : out std_logic;
        TXN_OUT                         : out std_logic_vector(0 downto 0);
        TXP_OUT                         : out std_logic_vector(0 downto 0)
    );
end LTITTC_sim_genRX_wrapper;


architecture Behavioral of LTITTC_sim_genRX_wrapper is

    signal cpllfbclklost                : std_logic;
    signal cplllock                     : std_logic;
    signal qpll1lock                    : std_logic;
    signal qpll1refclklost              : std_logic; -- @suppress "signal qpll1refclklost is never read"
    signal rxresetdone                  : std_logic;
    signal GT_RXOUTCLK                  : std_logic;
    signal GT_TXOUTCLK                  : std_logic;

    signal RXRESET_AUTO                 : std_logic;
    signal RxCdrLock                    : std_logic;
    signal cdr_cnt                      : std_logic_vector(19 downto 0) := (others=>'0');

    signal RX_DATA_33b_s                : std_logic_vector(32 downto 0);
    signal RX_IsK_OUT                   : std_logic_vector(3 downto 0);
    signal RXByteisAligned              : std_logic_vector(0 downto 0);
    signal Rxdisperr                    : std_logic_vector(3 downto 0);
    signal Rxcommadet                   : std_logic_vector(3 downto 0); -- @suppress "signal Rxcommadet is never read"
    signal Rxnotintable                 : std_logic_vector(3 downto 0);

    signal soft_reset                   : std_logic;
    signal cpll_reset                   : std_logic;
    signal cpllrefclklost               : std_logic; -- @suppress "signal cpllrefclklost is never read"

    signal GBT_CHANNEL_RXRST_AUTOENABLE : std_logic;
    signal General_ctrl                 : std_logic_vector(1 downto 0);
    signal SOFT_RXRST_ALL               : std_logic;
    signal SOFT_TXRST_ALL               : std_logic;
    signal kchar_found                  : std_logic;
    signal kchar_fail_rst               : std_logic;
    signal alignment_done_chk_cnt       : std_logic_vector(12 downto 0) := (others => '0');
    signal alignment_done_a             : std_logic;

    signal GT_RX_WORD_CLK               : std_logic;

    --clk40
    signal RX_DATA_33b_s_32_clk40       : std_logic;
    signal GT_RXByteisAligned_clk40     : std_logic_vector(0 downto 0);
    signal RXByteisAligned_clk40        : std_logic_vector(0 downto 0);
    signal Rxdisperr_a_clk40            : std_logic_vector(3 downto 0);
    signal RxCdrLock_int_clk40          : std_logic_vector(0 downto 0) := "0";
    signal Rxdisperr_clk40              : std_logic_vector(3 downto 0);
    signal GT_RXByteisAligned_f_clk40   : std_logic_vector(0 downto 0);
    signal RxCdrLock_a_clk40            : std_logic;
    signal RxCdrLock_clk40              : std_logic;

    --moved inputs to signals
    signal register_map_control         : register_map_control_type;
    signal DRP_CLK_IN                   : std_logic;
    signal GTREFCLK0_P_IN               : std_logic;
    signal GTREFCLK0_N_IN               : std_logic;
    signal GTREFCLK1_P_IN               : std_logic;
    signal GTREFCLK1_N_IN               : std_logic;

    signal GTREFCLK_REF_P               : std_logic:= '0';
    signal GTREFCLK_REF_N               : std_logic:= '0';

    signal reg_loopback_control         : std_logic_vector(2 downto 0) :=  (others => '0');
    signal reg_channel_disable          : std_logic_vector(0 downto 0) :=  (others => '0');
    signal reg_general_ctrl             : std_logic_vector(1 downto 0) :=  (others => '0');
    signal reg_soft_reset               : std_logic_vector(0 downto 0) :=  (others => '0');
    signal reg_cpll_reset               : std_logic_vector(0 downto 0) :=  (others => '0');
    signal reg_soft_rx_reset            : std_logic_vector(0 downto 0) :=  (others => '0');
    signal reg_soft_tx_reset            : std_logic_vector(0 downto 0) :=  (others => '0');
begin

    alignment_done <= RXByteisAligned_clk40(0) and RxCdrLock_clk40;--RxCdrLock_int_clk40(0); -- changed it because it takes too long for cdr_cnt to be 0 again

    GTREFCLK_gen : OBUFDS
        port map (
            O   => GTREFCLK_REF_P,
            OB  => GTREFCLK_REF_N,
            I   => clk240_in
        );

    GTREFCLK0_P_IN <= GTREFCLK_REF_P;
    GTREFCLK0_N_IN <= GTREFCLK_REF_N;
    GTREFCLK1_P_IN <= GTREFCLK_REF_P;
    GTREFCLK1_N_IN <= GTREFCLK_REF_N;

    register_map_control.LTITTC_CTRL.LTITTC_GTH_LOOPBACK_CONTROL  <= reg_loopback_control;
    register_map_control.LTITTC_CTRL.LTITTC_CHANNEL_DISABLE       <= reg_channel_disable;
    register_map_control.LTITTC_CTRL.LTITTC_GENERAL_CTRL          <= reg_general_ctrl;
    register_map_control.LTITTC_CTRL.LTITTC_SOFT_RESET            <= reg_soft_reset;
    register_map_control.LTITTC_CTRL.LTITTC_CPLL_RESET            <= reg_cpll_reset;
    register_map_control.LTITTC_CTRL.LTITTC_SOFT_RX_RESET         <= reg_soft_rx_reset;
    register_map_control.LTITTC_CTRL.LTITTC_SOFT_TX_RESET         <= reg_soft_tx_reset;

    GBT_CHANNEL_RXRST_AUTOENABLE    <= to_sl(register_map_control.LTITTC_CTRL.LTITTC_CHANNEL_DISABLE);
    General_ctrl                    <= register_map_control.LTITTC_CTRL.LTITTC_GENERAL_CTRL;  -- unused
    soft_reset                      <= to_sl(register_map_control.LTITTC_CTRL.LTITTC_SOFT_RESET) or rst_hw;
    cpll_reset                      <= to_sl(register_map_control.LTITTC_CTRL.LTITTC_CPLL_RESET);
    SOFT_RXRST_ALL                  <= to_sl(register_map_control.LTITTC_CTRL.LTITTC_SOFT_RX_RESET) or rst_hw or RXRESET_AUTO;
    SOFT_TXRST_ALL                  <= to_sl(register_map_control.LTITTC_CTRL.LTITTC_SOFT_TX_RESET) or rst_hw;

    GT_RX_WORD_CLK <= GT_RXOUTCLK;

    xpm_cdc_rxbyteisaligned : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 4,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 1
        )
        port map (
            src_clk => GT_RX_WORD_CLK,
            src_in => RXByteisAligned(0),
            dest_clk => clk40_in,
            dest_out => RXByteisAligned_clk40(0)
        );

    loop_rxdisperr: for i in 0 to 3 generate
        xpm_cdc_Rxdisperr : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 4,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 1
            )
            port map (
                src_clk => GT_RX_WORD_CLK,
                src_in => Rxdisperr(i),
                dest_clk => clk40_in,
                dest_out => Rxdisperr_clk40(i)
            );
    end generate;

    xpm_cdc_rxcdrlock : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 4,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 1
        )
        port map (
            src_clk => GT_RX_WORD_CLK,
            src_in => RxCdrLock,
            dest_clk => clk40_in,
            dest_out => RxCdrLock_clk40
        );


    xpm_cdc_RX_DATA_33b_s_32 : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 4,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 1
        )
        port map (
            src_clk => GT_RX_WORD_CLK,
            src_in => RX_DATA_33b_s(32),
            dest_clk => clk40_in,
            dest_out => RX_DATA_33b_s_32_clk40
        );


    process(clk40_in)
    begin
        if clk40_in'event and clk40_in='1' then
            alignment_done_chk_cnt <= alignment_done_chk_cnt + '1';

            cdr_cnt <=cdr_cnt+'1';

            if alignment_done_chk_cnt="0000000000000" then
                kchar_found <= '0';
                alignment_done_a <= RXByteisAligned_clk40(0);
                Rxdisperr_a_clk40 <= Rxdisperr_clk40;
            else
                kchar_found <= kchar_found or RX_DATA_33b_s_32_clk40;
                alignment_done_a <= RXByteisAligned_clk40(0) and alignment_done_a;
                Rxdisperr_a_clk40(3) <= Rxdisperr_clk40(3) or Rxdisperr_a_clk40(3);
                Rxdisperr_a_clk40(2) <= Rxdisperr_clk40(2) or Rxdisperr_a_clk40(2);
                Rxdisperr_a_clk40(1) <= Rxdisperr_clk40(1) or Rxdisperr_a_clk40(1);
                Rxdisperr_a_clk40(0) <= Rxdisperr_clk40(0) or Rxdisperr_a_clk40(0);
            end if;
            if alignment_done_chk_cnt="1000000000000" then
                GT_RXByteisAligned_clk40(0) <=  RxCdrLock_int_clk40(0) and alignment_done_a;-- and kchar_found(i) and Alignment_flag_enable(i);
            end if;

            if alignment_done_chk_cnt="1000000000001" then
                if kchar_found='0' or GT_RXByteisAligned_clk40="0" then
                    kchar_fail_rst <='1';
                end if;
            else
                kchar_fail_rst <='0';
            end if;

            if General_ctrl(0)='1' then
                GT_RXByteisAligned_f_clk40(0) <= '1';
            else
                GT_RXByteisAligned_f_clk40(0) <=  GT_RXByteisAligned_clk40(0) and GT_RXByteisAligned_f_clk40(0);
            end if;

            if cdr_cnt ="00000000000000000000" then
                RxCdrLock_a_clk40     <= RxCdrLock_clk40;
                RxCdrLock_int_clk40(0) <= RxCdrLock_a_clk40;
            else
                RxCdrLock_a_clk40 <= RxCdrLock_a_clk40 and RxCdrLock_clk40;
            end if;

        end if;
    end process;

    RXRESET_AUTO <= (kchar_fail_rst and GBT_CHANNEL_RXRST_AUTOENABLE);--mt

    LTITTC_sim_genRX_inst: entity work.LTITTC_sim_genRX
        Port map  (
            ---------- Clocks
            --SI5345/TX
            GTREFCLK0_P_IN => GTREFCLK0_P_IN,
            GTREFCLK0_N_IN => GTREFCLK0_N_IN,
            --LMK/RX
            GTREFCLK1_P_IN => GTREFCLK1_P_IN,
            GTREFCLK1_N_IN => GTREFCLK1_N_IN,
            DRP_CLK_IN => clk40_in,

            gt_rxusrclk_in => GT_RX_WORD_CLK,
            gt_rxoutclk_out => GT_RXOUTCLK,
            --gt_txusrclk_in => GT_TX_WORD_CLK,
            gt_txoutclk_out => GT_TXOUTCLK,

            -----------------------------------------
            ---- Control signals
            -----------------------------------------
            loopback_in                  => register_map_control.LTITTC_CTRL.LTITTC_GTH_LOOPBACK_CONTROL,
            reset_all_in                 => soft_reset,
            cpllreset_in                 => cpll_reset,
            reset_tx_pll_and_datapath_in => SOFT_TXRST_ALL,
            reset_tx_datapath_in         => SOFT_TXRST_ALL,
            reset_rx_pll_and_datapath_in => SOFT_RXRST_ALL,
            reset_rx_datapath_in         => SOFT_RXRST_ALL,

            -----------------------------------------
            ---- STATUS signals
            -----------------------------------------
            gt_rxbyteisaligned_out       => RXByteisAligned(0),
            gt_rxdisperr_out             => Rxdisperr,
            gt_rxkdet_out                => RX_IsK_OUT,
            gt_rxcommadet_out            => Rxcommadet,
            gt_rxnotintable_out          => Rxnotintable,

            gt_rxresetdone_out           => rxresetdone,

            gt_cpllfbclklost_out         => cpllfbclklost,
            gt_cplllock_out              => cplllock,
            gt_cpllrefclklost_out        => cpllrefclklost,
            gt_rxcdrlock_out             => RxCdrLock,

            gt_qpll1refclklost_out       => qpll1refclklost,
            gt_qpll1lock_out             => qpll1lock,

            ---------- DATA
            TX_DATA_gt_32b => TX_DATA_32b_in,
            TX_isK_gt      => ISK_in,

            TXN_OUT => TXN_OUT,
            TXP_OUT => TXP_OUT

        );

end Behavioral;

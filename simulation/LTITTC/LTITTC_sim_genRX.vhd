----------------------------------------------------------------------------------
--! Company:  Argonne National Laboratory
--! Engineer: Marco Trovato (mtrovato@anl.gov)
----------------------------------------------------------------------------------
--rluz: adapted from sources/ttc/ltittc_wrapper/gth_ltittclink_wrapper_ku.vhd on 08/05/2024
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
library UNISIM;
    use UNISIM.VCOMPONENTS.ALL;

entity LTITTC_sim_genRX is
    port(
        GTREFCLK0_P_IN                  : in std_logic;
        GTREFCLK0_N_IN                  : in std_logic;
        GTREFCLK1_P_IN                  : in std_logic;
        GTREFCLK1_N_IN                  : in std_logic;
        DRP_CLK_IN                      : in std_logic;
        gt_rxusrclk_in                  : in std_logic;
        gt_rxoutclk_out                 : out std_logic;
        gt_txoutclk_out                 : out std_logic;
        ---- Control signals
        loopback_in                     : in std_logic_vector(2 downto 0);
        reset_all_in                    : in std_logic;
        cpllreset_in                    : in std_logic;
        reset_tx_pll_and_datapath_in    : in std_logic;
        reset_tx_datapath_in            : in std_logic;
        reset_rx_pll_and_datapath_in    : in std_logic;
        reset_rx_datapath_in            : in std_logic;
        ---- STATUS signals
        gt_rxbyteisaligned_out          : out std_logic;
        gt_rxdisperr_out                : out std_logic_vector(3 downto 0);
        gt_rxkdet_out                   : out std_logic_vector(3 downto 0);
        gt_rxcommadet_out               : out std_logic_vector(3 downto 0);
        gt_rxnotintable_out             : out std_logic_vector(3 downto 0);
        gt_rxresetdone_out              : out std_logic;
        gt_cpllfbclklost_out            : out std_logic;
        gt_cplllock_out                 : out std_logic;
        gt_cpllrefclklost_out           : out std_logic;
        gt_rxcdrlock_out                : out std_logic;
        gt_qpll1refclklost_out          : out std_logic;
        gt_qpll1lock_out                : out std_logic;
        ---- TX Data in
        TX_DATA_gt_32b                  : in std_logic_vector(31 downto 0);
        TX_isK_gt                       : in std_logic_vector(3 downto 0);
        ---- TX out
        TXN_OUT                         : out  std_logic_vector(0 downto 0);
        TXP_OUT                         : out  std_logic_vector(0 downto 0)
    );
end LTITTC_sim_genRX;

architecture RTL of LTITTC_sim_genRX is
    signal    GTH_RefClk0               : std_logic; --SI5345/TX
    signal    GTH_RefClk1               : std_logic; --LMK/RX


    signal rxctrl0_out                  : std_logic_vector(15 downto 0);
    signal rxctrl1_out                  : std_logic_vector(15 downto 0);
    signal rxctrl2_out                  : std_logic_vector(7 downto 0);
    signal rxctrl3_out                  : std_logic_vector(7 downto 0);
    signal rxctrl0_d_out                : std_logic_vector(15 downto 0);
    signal rxctrl1_d_out                : std_logic_vector(15 downto 0);
    signal rxctrl2_d_out                : std_logic_vector(7 downto 0);
    signal rxctrl3_d_out                : std_logic_vector(7 downto 0);
    signal rxcommadet_out               : std_logic_vector(0 downto 0); -- @suppress "signal rxcommadet_out is never read"
    signal rxcdrlock_out                : std_logic_vector(0 downto 0);
    signal rxbyteisaligned_out          : std_logic_vector(0 downto 0);
    signal cplllock_out                 : std_logic_vector(0 downto 0);
    signal cpllfbclklost_out            : std_logic_vector(0 downto 0);
    signal rxresetdone_out              : std_logic_vector(0 downto 0);

    signal qpll1lock_out                : std_logic_vector(0 downto 0);
    signal qpll1refclklost_out          : std_logic_vector(0 downto 0);

    signal reset_all                    : std_logic_vector(0 downto 0);

    signal userdata_rx_out              : std_logic_vector(31 downto 0);

    signal reset_rx_pll_and_datapath    : std_logic_vector(0 downto 0);
    signal reset_rx_datapath            : std_logic_vector(0 downto 0);

    signal gt_drp_clk_in                : std_logic_vector(0 downto 0);

    signal gtrefclk0_in                 : std_logic_vector(0 downto 0);
    signal gtrefclk1_in                 : std_logic_vector(0 downto 0);

    signal rxusrclk_in                  : std_logic_vector(0 downto 0);
    signal rxoutclk_out                 : std_logic_vector(0 downto 0);

    signal gt_drp_clk_vec               : std_logic_vector(0 downto 0);

    signal txoutclk_out                 : std_logic_vector(0 downto 0);
    signal reset_tx_pll_and_datapath    : std_logic_vector(0 downto 0);
    signal reset_tx_datapath            : std_logic_vector(0 downto 0);
    signal rxbyterealign_i              : std_logic_vector(0 downto 0) := "0"; -- @suppress "signal rxbyterealign_i is never read"

    signal TXN_i                        : std_logic_vector(0 downto 0) := "0";
    signal TXP_i                        : std_logic_vector(0 downto 0) := "0";
    signal RXN_i                        : std_logic_vector(0 downto 0) := "0";
    signal RXP_i                        : std_logic_vector(0 downto 0) := "0";
    signal RX_DATA_gt_33b               : std_logic_vector(32 downto 0);
begin

    TXN_OUT <= TXN_i;
    TXP_OUT <= TXP_i;
    RXN_i   <= TXN_i; --loopback
    RXP_i   <= TXP_i; --loopback

    ibufds_gtrefclk0_inst: IBUFDS_GTE3
        generic map (
            REFCLK_EN_TX_PATH => '0',
            REFCLK_HROW_CK_SEL => "00",
            REFCLK_ICNTL_RX => "00"
        )
        port map (
            O     => GTH_RefClk0,
            ODIV2 => open,
            CEB   => '0',
            I     => GTREFCLK0_P_IN,
            IB    => GTREFCLK0_N_IN
        );

    ibufds_gtrefclk1_inst: IBUFDS_GTE3
        generic map (
            REFCLK_EN_TX_PATH => '0',
            REFCLK_HROW_CK_SEL => "00",
            REFCLK_ICNTL_RX => "00"
        )
        port map (
            O     => GTH_RefClk1,
            ODIV2 => open,
            CEB   => '0',
            I     => GTREFCLK1_P_IN,
            IB    => GTREFCLK1_N_IN
        );

    --------------------------------
    ---- Reset
    reset_all(0)                    <= reset_all_in;
    reset_tx_pll_and_datapath(0)    <= reset_tx_pll_and_datapath_in;
    reset_tx_datapath(0)            <= reset_tx_datapath_in;
    reset_rx_pll_and_datapath(0)    <= reset_rx_pll_and_datapath_in;
    reset_rx_datapath(0)            <= reset_rx_datapath_in;

    --------------------------------
    ---- Monitor
    gt_rxcdrlock_out                <= rxcdrlock_out(0);
    gt_rxbyteisaligned_out          <= rxcdrlock_out(0) and rxresetdone_out(0); --rxbyteisaligned_out(0);
    gt_rxdisperr_out                <= rxctrl1_out(3 downto 0); --[i] disp err found in byte i. Only [0] should be relevant because of the MGT configuration
    gt_rxkdet_out                   <= rxctrl0_out(3 downto 0); --[i] k character found in byte i. Only [0] should be relevant because of the MGT configuration
    gt_rxcommadet_out               <= rxctrl2_out(3 downto 0); --[i] comma character found in byte i. Only [0] should be relevant because of the MGT configuration
    gt_rxnotintable_out             <= rxctrl3_out(3 downto 0); --[i] not valid character found in byte i

    gt_rxresetdone_out              <= rxresetdone_out(0);

    gt_cpllfbclklost_out            <= cpllfbclklost_out(0);
    gt_cplllock_out                 <= cplllock_out(0);
    gt_cpllrefclklost_out           <= '1'; --cpllrefclklost_out(0);

    gt_rxcdrlock_out                <= rxcdrlock_out(0);

    gt_qpll1refclklost_out          <= qpll1refclklost_out(0);
    gt_qpll1lock_out                <= qpll1lock_out(0);

    --------------------------------
    --MT why 2 clock latency: simplify?
    process(gt_rxusrclk_in)
    begin
        if rising_edge(gt_rxusrclk_in) then
            rxctrl0_out <= rxctrl0_d_out;
            rxctrl1_out <= rxctrl1_d_out;
            rxctrl2_out <= rxctrl2_d_out;
            rxctrl3_out <= rxctrl3_d_out;
            if(rxctrl1_d_out(3 downto 0) = "0000" and rxctrl3_d_out(3 downto 0) = "0000") then
                RX_DATA_gt_33b <= '0' & userdata_rx_out(31 downto 0); --should I include also rxctrl1_out(3,2,1)
            else
                RX_DATA_gt_33b <= RX_DATA_gt_33b;
            end if;
        end if;
    end process;


    gt_drp_clk_in(0)              <= DRP_CLK_IN;
    gtrefclk0_in(0)               <= GTH_RefClk0;
    gtrefclk1_in(0)               <= GTH_RefClk1;

    gt_drp_clk_vec(0)             <= DRP_CLK_IN;

    rxusrclk_in(0)                <= gt_rxusrclk_in;
    gt_rxoutclk_out              <= rxoutclk_out(0);
    gt_txoutclk_out              <= txoutclk_out(0);

    --rluz: adapted from sources/ip_cores/kintexUltrascale/gtwizard_ttc_rxcpll.xci on 08/05/2024
    LTITTC_sim_genRX_gtwizard_inst: entity work.LTITTC_sim_genRX_gtwizard
        PORT MAP (
            gtwiz_userclk_tx_reset_in           => "0",
            gtwiz_userclk_tx_srcclk_out         => open,
            gtwiz_userclk_tx_usrclk_out         => open,
            gtwiz_userclk_tx_usrclk2_out        => txoutclk_out,
            gtwiz_userclk_tx_active_out         => open,
            gtwiz_userclk_rx_reset_in           => "0",
            gtwiz_userclk_rx_srcclk_out         => open,
            gtwiz_userclk_rx_usrclk_out         => open,
            gtwiz_userclk_rx_usrclk2_out        => rxoutclk_out,
            gtwiz_userclk_rx_active_out         => open,
            gtwiz_buffbypass_tx_reset_in        => "0",
            gtwiz_buffbypass_tx_start_user_in   => "0",
            gtwiz_buffbypass_tx_done_out        => open,
            gtwiz_buffbypass_tx_error_out       => open,
            gtwiz_reset_clk_freerun_in          => gt_drp_clk_in,
            gtwiz_reset_all_in                  => reset_all,
            gtwiz_reset_tx_pll_and_datapath_in  => reset_tx_pll_and_datapath,
            gtwiz_reset_tx_datapath_in          => reset_tx_datapath,
            gtwiz_reset_rx_pll_and_datapath_in  => reset_rx_pll_and_datapath,
            gtwiz_reset_rx_datapath_in          => reset_rx_datapath,
            gtwiz_reset_rx_cdr_stable_out       => open,
            gtwiz_reset_tx_done_out             => open,
            gtwiz_reset_rx_done_out             => open,
            gtwiz_userdata_tx_in                => TX_DATA_gt_32b,
            gtwiz_userdata_rx_out               => userdata_rx_out,
            cplllockdetclk_in                   => gt_drp_clk_vec,
            cpllreset_in                        => cpllreset_in,
            --            drpclk_in                           => gt_drp_clk_in,
            gthrxn_in                           => RXN_i,
            gthrxp_in                           => RXP_i,
            --            gtrefclk0_in                        => gtrefclk0_in,
            loopback_in                         => loopback_in,
            rx8b10ben_in                        => "1",
            rxcommadeten_in                     => "1",
            rxmcommaalignen_in                  => "1", --commaalignen_i,
            rxpcommaalignen_in                  => "1", --commaalignen_i,
            tx8b10ben_in                        => "1",
            txctrl0_in                          => (others => '0'),
            txctrl1_in                          => (others => '0'),
            txctrl2_in                          => "0000" & TX_isK_gt,
            cpllfbclklost_out                   => cpllfbclklost_out,
            cplllock_out                        => cplllock_out,
            gthtxn_out                          => TXN_i,
            gthtxp_out                          => TXP_i,
            gtpowergood_out                     => open,
            rxbyteisaligned_out                 => rxbyteisaligned_out,
            rxbyterealign_out                   => rxbyterealign_i,
            rxcdrlock_out                       => rxcdrlock_out,
            rxcommadet_out                      => rxcommadet_out,
            rxctrl0_out                         => rxctrl0_d_out,
            rxctrl1_out                         => rxctrl1_d_out,
            rxctrl2_out                         => rxctrl2_d_out,
            rxctrl3_out                         => rxctrl3_d_out,
            --            rxoutclk_out                        => rxoutclk_out,
            rxpmaresetdone_out                  => open, --rxpmaresetdone_out,
            rxresetdone_out                     => rxresetdone_out,
            --            txoutclk_out                        => txoutclk_out,
            txpmaresetdone_out                  => open, --txpmaresetdone_out,
            txresetdone_out                     => open,
            gtrefclk01_in                       => gtrefclk1_in,
            qpll1outclk_out                     => open,
            qpll1outrefclk_out                  => open,
            qpll1fbclklost_out                  => open,
            qpll1lock_out                       => qpll1lock_out,
            qpll1refclklost_out                 => qpll1refclklost_out,
            txprgdivresetdone_out               => open
        );

    --MT make it a SM: think about disabling the alignment at some point.
    --Commented for now
    process(rxusrclk_in(0))
        variable cnt : integer range 0 to 100;
    begin
        if rxusrclk_in(0)'event and rxusrclk_in(0) = '1' then
            if (rxbyteisaligned_out = "1") then
                if(cnt = 50) then
                    cnt := cnt;
                else
                    cnt := cnt+1;
                end if;
            else
                cnt := 0;
            end if;
        --if(cnt = 50) then
        --    commaalignen_i <= "0";
        --else
        --    commaalignen_i <= "1";
        --end if;
        end if;
    end process;

end RTL;

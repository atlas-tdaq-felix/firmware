
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

library ieee;
    use ieee.std_logic_1164.all;
    use IEEE.NUMERIC_STD.ALL;
    use ieee.numeric_std_unsigned.all;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.FELIX_package.all;
    use work.centralRouter_package.all;
    use work.pcie_package.all;

entity LTITTC_wrapper_sim is
    generic(
        FIRMWARE_MODE   : integer := FIRMWARE_MODE_PIXEL;
        RD53Version     : String  := "B"; --A or B
        CARD_TYPE       : integer := 712;
        GBT_NUM         : integer := 1;
        TTC_SYS_SEL     : std_logic := '1' -- 0: TTC, 1: LTITTC P2P
    );
end LTITTC_wrapper_sim;

architecture Behavioral of LTITTC_wrapper_sim is

    function get_trigtag_mode return integer is
    begin
        if FIRMWARE_MODE = FIRMWARE_MODE_STRIP then
            return 2;           -- ABC*
        elsif FIRMWARE_MODE = FIRMWARE_MODE_PIXEL then
            if RD53Version = "A" then
                return 0;       -- RD53A
            else
                return 1;       -- ITkPixV1 (RD53B) and newer
            end if;
        else
            return -1;
        end if;
    end function;

    constant gen_data                       : boolean := true;

    signal reset                            : std_logic := '0';
    signal global_register_map_40_control   : register_map_control_type;
    signal register_map_ltittc_monitor      : register_map_ltittc_monitor_type;
    signal TTC_path                         : TTC_data_type;

    signal clk40_xtal                       : std_logic;
    signal clk40                            : std_logic;
    signal clk40_wrapper                    : std_logic;
    signal clk40_in                         : std_logic;
    signal clk160                           : std_logic;
    signal clk240                           : std_logic;
    signal clk40_ttc_out                    : std_logic;
    signal clk40_LTI                        : std_logic;
    signal clk40_LTI_in                     : std_logic;
    signal clk160_LTI                       : std_logic;
    signal clk240_LTI                       : std_logic;

    signal TTC_ToHost_Data                  : TTC_ToHost_data_type;
    signal ttc_TTC_BUSY_mon_array           : busyOut_array_type(47 downto 0);
    signal BUSY_OUT_s                       : std_logic := '0';

    signal RX_N_LTITTC                      : std_logic_vector(NUM_LTITTC_INPUTS(CARD_TYPE,TTC_SYS_SEL)-1 downto 0);
    signal RX_P_LTITTC                      : std_logic_vector(NUM_LTITTC_INPUTS(CARD_TYPE,TTC_SYS_SEL)-1 downto 0);
    signal TX_N_LTITTC                      : std_logic_vector(NUM_LTITTC_INPUTS(CARD_TYPE,TTC_SYS_SEL)-1 downto 0);
    signal TX_P_LTITTC                      : std_logic_vector(NUM_LTITTC_INPUTS(CARD_TYPE,TTC_SYS_SEL)-1 downto 0);

    signal GTREFCLK0_LTITTC_P               : std_logic_vector(NUM_LTITTC_INPUTS(CARD_TYPE,TTC_SYS_SEL)-1 downto 0);
    signal GTREFCLK0_LTITTC_N               : std_logic_vector(NUM_LTITTC_INPUTS(CARD_TYPE,TTC_SYS_SEL)-1 downto 0);
    signal GTREFCLK1_LTITTC_P               : std_logic_vector(NUM_LTITTC_INPUTS(CARD_TYPE,TTC_SYS_SEL)-1 downto 0);
    signal GTREFCLK1_LTITTC_N               : std_logic_vector(NUM_LTITTC_INPUTS(CARD_TYPE,TTC_SYS_SEL)-1 downto 0);

    signal LTI_TXUSRCLK                     : std_logic_vector(GBT_NUM-1 downto 0);
    --    signal LTI_TX_Data_Transceiver          : array_32b(0 to GBT_NUM - 1);
    --    signal LTI_TX_TX_CharIsK                : array_4b(0 to GBT_NUM-1);
    --    signal LTI_TX_Data_Transceiver_toLWrap  : array_32b(0 to GBT_NUM - 1);
    --    signal LTI_TX_TX_CharIsK_tolinkwrapper  : array_4b(0 to GBT_NUM-1);

    constant clk40_period                   : time := 25 ns;
    constant clk40_LTI_period               : time := 24.95 ns;
    signal clk40_tmp                        : std_logic:= '0';
    signal clk_en                           : boolean   := false;
    signal clk40_LTI_tmp                    : std_logic:= '0';
    signal clk_LTI_en                       : boolean   := false;

    signal GTREFCLK_REF_P                   : std_logic:= '0';
    signal GTREFCLK_REF_N                   : std_logic:= '0';

    signal select_output                    : std_logic_vector(1 downto 0) :=  (others => '0');
    signal loopback_control                 : std_logic_vector(2 downto 0) :=  (others => '0');
    signal channel_disable                  : std_logic_vector(0 downto 0) :=  (others => '0');
    signal general_ctrl                     : std_logic_vector(1 downto 0) :=  (others => '0');
    signal soft_reset                       : std_logic_vector(0 downto 0) :=  (others => '0');
    signal cpll_reset                       : std_logic_vector(0 downto 0) :=  (others => '0');
    signal soft_rx_reset                    : std_logic_vector(0 downto 0) :=  (others => '0');
    signal soft_tx_reset                    : std_logic_vector(0 downto 0) :=  (others => '0');
    signal reset_TTC2H                      : std_logic_vector(0 downto 0) :=  (others => '0');
    signal clear_ttype                      : std_logic_vector(0 downto 0) :=  (others => '0');
    --signal clear_bcid                       : std_logic_vector(0 downto 0);
    signal clear_sl0id                      : std_logic_vector(0 downto 0) :=  (others => '0');
    signal clear_sorb                       : std_logic_vector(0 downto 0) :=  (others => '0');
    signal clear_grst                       : std_logic_vector(0 downto 0) :=  (others => '0');
    signal clear_sync                       : std_logic_vector(0 downto 0) :=  (others => '0');
    signal clear_l0id_err                   : std_logic_vector(0 downto 0) :=  (others => '0');
    signal clear_bcr_err                    : std_logic_vector(0 downto 0) :=  (others => '0');
    signal clear_crc_err                    : std_logic_vector(0 downto 0) :=  (others => '0');
    signal ttype                            : std_logic_vector(15 downto 0) :=  (others => '0');

    signal reset_data_gen                   : std_logic := '0';
    signal LTITTC_gen_data_P                : std_logic := '0';
    signal LTITTC_gen_data_N                : std_logic := '0';
    signal en_gen_ttc_data                  : std_logic := '0';
    signal data_alignment_done              : std_logic := '0';
    signal aligned                          : std_logic := '0';

    signal timeout                          : boolean := false;
    signal clock_select                     : std_logic := '0';
begin

    --instance

    clk40_wrapper <= clk40 when clock_select = '0' else clk40_ttc_out;

    ltittc_wrapper_inst: entity work.ltittc_wrapper
        generic map(
            CARD_TYPE => CARD_TYPE,
            ITK_TRIGTAG_MODE => get_trigtag_mode,
            GBT_NUM => GBT_NUM
        )
        port map(
            reset_in                    => reset,                               --in
            register_map_control        => global_register_map_40_control,      --in
            register_map_ltittc_monitor => register_map_ltittc_monitor,         --out
            TTC_out                     => TTC_path,                            --out

            clk40_ttc_out               => clk40_ttc_out,                       --out
            clk40_in                    => clk40_wrapper,                       --in
            clk40_xtal_in               => clk40_xtal,                          --in
            clk240_ila                  => clk240,                              --in

            BUSY                        => open,                                --out
            cdrlocked_out               => open,                                --out
            TTC_ToHost_Data_out         => TTC_ToHost_Data,                     --out
            TTC_BUSY_mon_array          => ttc_TTC_BUSY_mon_array(23 downto 0), --in
            BUSY_IN                     => BUSY_OUT_s,                          --in


            GTREFCLK0_P_IN              => GTREFCLK0_LTITTC_P(0),               --in
            GTREFCLK0_N_IN              => GTREFCLK0_LTITTC_N(0),               --in
            GTREFCLK1_P_IN              => GTREFCLK1_LTITTC_P(0),               --in
            GTREFCLK1_N_IN              => GTREFCLK1_LTITTC_N(0),               --in
            RX_P_LTITTC                 => RX_P_LTITTC(0),                      --in
            RX_N_LTITTC                 => RX_N_LTITTC(0),                      --in
            TX_P_LTITTC                 => TX_P_LTITTC(0),                      --out
            TX_N_LTITTC                 => TX_N_LTITTC(0),                      --out

            LTI_TXUSRCLK_in             => LTI_TXUSRCLK,                        --in
            LTI_TX_Data_Transceiver_in  => (others => (others =>'0')),          --in
            LTI_TX_TX_CharIsK_in        => (others => (others =>'0')),          --in
            LTI_TX_Data_Transceiver_out => open,     --out
            LTI_TX_TX_CharIsK_out       => open      --out
        );

    --    ltittc_wrapper_test: entity work.ltittc_wrapper
    --        generic map(
    --            CARD_TYPE => CARD_TYPE,
    --            ITK_TRIGTAG_MODE => get_trigtag_mode,
    --            GBT_NUM => GBT_NUM
    --        )
    --        port map(
    --            reset_in                    => reset,                               --in
    --            register_map_control        => global_register_map_40_control,      --in
    --            register_map_ltittc_monitor => open,         --out
    --            TTC_out                     => open,                            --out

    --            clk40_ttc_out               => clk40_ttc_out,                       --out
    --            clk40_in                    => clk40,                       --in
    --            clk40_xtal_in               => clk40_xtal,                          --in
    --            clk240_ila                  => clk240,                              --in

    --            BUSY                        => open,                                --out
    --            cdrlocked_out               => open,                                --out
    --            TTC_ToHost_Data_out         => open,                     --out
    --            TTC_BUSY_mon_array          => ttc_TTC_BUSY_mon_array(23 downto 0), --in
    --            BUSY_IN                     => BUSY_OUT_s,                          --in


    --            GTREFCLK0_P_IN              => GTREFCLK0_LTITTC_P(0),               --in
    --            GTREFCLK0_N_IN              => GTREFCLK0_LTITTC_N(0),               --in
    --            GTREFCLK1_P_IN              => GTREFCLK1_LTITTC_P(0),               --in
    --            GTREFCLK1_N_IN              => GTREFCLK1_LTITTC_N(0),               --in
    --            RX_P_LTITTC                 => RX_P_LTITTC(0),                      --in
    --            RX_N_LTITTC                 => RX_N_LTITTC(0),                      --in
    --            TX_P_LTITTC                 => open,                      --out
    --            TX_N_LTITTC                 => open,                      --out

    --            LTI_TXUSRCLK_in             => LTI_TXUSRCLK,                        --in
    --            LTI_TX_Data_Transceiver_in  => (others => (others =>'0')),          --in
    --            LTI_TX_TX_CharIsK_in        => (others => (others =>'0')),          --in
    --            LTI_TX_Data_Transceiver_out => open,     --out
    --            LTI_TX_TX_CharIsK_out       => open      --out
    --        );

    --clocks

    clk40_tmp       <= not clk40_tmp        after clk40_period/2; --40 MHZ;
    clk40_in        <= clk40_tmp            when  clk_en else '0';

    clk_25d00 : entity work.clk_wiz_40_0
        port map (
            clk_in2 => clk40_in,
            clk_in_sel => '0',
            clk40 => clk40,
            clk80 => open,
            clk160 => clk160,
            clk320 => open,
            clk240 => clk240,
            clk100 => open,
            reset => '0',
            locked => open,
            clk_40_in => clk40_in
        );

    clk40_LTI_tmp   <= not clk40_LTI_tmp   after clk40_LTI_period/2; --240.474 MHZ;
    clk40_LTI_in    <= clk40_LTI_tmp        when  clk_LTI_en else '0';

    clk_24d95 : entity work.clk_wiz_40_0
        port map (
            clk_in2 => clk40_LTI_in,
            clk_in_sel => '0',
            clk40 => clk40_LTI,
            clk80 => open,
            clk160 => clk160_LTI,
            clk320 => open,
            clk240 => clk240_LTI,
            clk100 => open,
            reset => '0',
            locked => open,
            clk_40_in => clk40_LTI_in
        );

    clk40_xtal      <= clk40;

    GTREFCLK_gen : OBUFDS
        port map (
            O   => GTREFCLK_REF_P,
            OB  => GTREFCLK_REF_N,
            I   => clk240_LTI
        );

    GTREFCLK0_LTITTC_P(0) <= GTREFCLK_REF_P;
    GTREFCLK0_LTITTC_N(0) <= GTREFCLK_REF_N;
    GTREFCLK1_LTITTC_P(0) <= GTREFCLK_REF_P;
    GTREFCLK1_LTITTC_N(0) <= GTREFCLK_REF_N;

    g_LTI_TXCLK: for i in 0 to GBT_NUM-1 generate
        LTI_TXUSRCLK(i) <= clk240;
    end generate;

    --registers

    global_register_map_40_control.LTI_FE_OUTPUT_SELECTOR                   <= select_output; --2bits
    global_register_map_40_control.LTITTC_CTRL.LTITTC_GTH_LOOPBACK_CONTROL  <= loopback_control; --3bits
    global_register_map_40_control.LTITTC_CTRL.LTITTC_CHANNEL_DISABLE       <= channel_disable; --1bit
    global_register_map_40_control.LTITTC_CTRL.LTITTC_GENERAL_CTRL          <= general_ctrl; --2bits
    global_register_map_40_control.LTITTC_CTRL.LTITTC_SOFT_RESET            <= soft_reset; --1bit
    global_register_map_40_control.LTITTC_CTRL.LTITTC_CPLL_RESET            <= cpll_reset; --1bit
    global_register_map_40_control.LTITTC_CTRL.LTITTC_SOFT_RX_RESET         <= soft_rx_reset; --1bit
    global_register_map_40_control.LTITTC_CTRL.LTITTC_SOFT_TX_RESET         <= soft_tx_reset; --1bit
    global_register_map_40_control.LTITTC_CTRL.TOHOST_RST                   <= reset_TTC2H; --1bit
    global_register_map_40_control.LTITTC_TTYPE_MONITOR.CLEAR               <= clear_ttype; --1bit
    --global_register_map_40_control.LTITTC_BCR_ERR_MONITOR.CLEAR             <= clear_bcid; --1bit
    global_register_map_40_control.LTITTC_SL0ID_MONITOR.CLEAR               <= clear_sl0id; --1bit
    global_register_map_40_control.LTITTC_SORB_MONITOR.CLEAR                <= clear_sorb; --1bit
    global_register_map_40_control.LTITTC_GRST_MONITOR.CLEAR                <= clear_grst; --1bit
    global_register_map_40_control.LTITTC_SYNC_MONITOR.CLEAR                <= clear_sync; --1bit
    global_register_map_40_control.LTITTC_L0ID_ERR_MONITOR.CLEAR            <= clear_l0id_err; --1bit
    global_register_map_40_control.LTITTC_BCR_ERR_MONITOR.CLEAR             <= clear_bcr_err; --1bit
    global_register_map_40_control.LTITTC_CRC_ERR_MONITOR.CLEAR             <= clear_crc_err; --1bit
    global_register_map_40_control.LTITTC_TTYPE_MONITOR.REFVALUE            <= ttype; --1bit

    --data_generation

    LTITTC_sim_data_generation_inst : entity work.LTITTC_sim_data_generation
        generic map(
            clock_phased => true
        )
        port map(
            reset               => reset_data_gen,
            clk40               => clk40,
            clk40_LTI           => clk40_LTI,
            clk160              => clk160,
            clk160_LTI          => clk160_LTI,
            clk240_LTI          => clk240_LTI,
            en_gen_ttc_data     => en_gen_ttc_data,
            data_alignment_done => data_alignment_done,
            data_out_P          => LTITTC_gen_data_P,
            data_out_N          => LTITTC_gen_data_N
        );

    g_LTI_genTX_to_RX: for i in 0 to GBT_NUM-1 generate
        RX_P_LTITTC(i) <= LTITTC_gen_data_P;
        RX_N_LTITTC(i) <= LTITTC_gen_data_N;
    end generate;

    aligned <= data_alignment_done and register_map_ltittc_monitor.LTITTC_RX_BYTEISALIGNED(0);

    mainproc : process
    begin
        assert false
            report "START SIMULATION"
            severity NOTE;
        clk_en           <= true;
        clk_LTI_en       <= true;
        wait for 20 ns;
        reset <= '1';
        reset_data_gen <= '1';
        wait for 120 ns;
        reset <= '0';
        wait for 120 ns;
        reset_data_gen <= '0';
        wait until aligned = '1' or timeout = true;
        en_gen_ttc_data <= '1';
        wait for 100 ns;
        clock_select <= '1';
        wait for 500 ns;
        clear_bcr_err <= "1";
        wait for 100 ns;
        clear_bcr_err <= "0";
        wait for 1000 us;
    end process;

    --timeout

    timeoutproc : process
    begin
        wait for 1000 us;
        timeout <= true;
    end process;


end Behavioral;

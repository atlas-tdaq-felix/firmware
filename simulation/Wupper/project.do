
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
#               RHabraken
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set projectEnv [project env]
if { [string length $projectEnv]==0} {
	puts "no project open"
	puts $projectEnv
} else {
	puts "closing project"
	project close
}

vdel -all -lib work
vlib work
vmap work work
project new . FELIX_DMA_Core

# ----------------------------------------------------------
# packages
# ----------------------------------------------------------
project addfile ../../sources/templates/pcie_package.vhd
project addfile ../../sources/packages/centralRouter_package.vhd

# ----------------------------------------------------------
# PCIe DMA top module
# ----------------------------------------------------------
project addfile ../../sources/opencores/wupper_oc_top.vhd
# ----------------------------------------------------------
# dma sources
# ----------------------------------------------------------

project addfile ../../sources/pcie/wupper_core.vhd
project addfile ../../sources/pcie/pcie_slow_clock.vhd
project addfile ../../sources/pcie/pcie_init.vhd
project addfile ../../sources/pcie/pcie_ep_wrap.vhd
project addfile ../../sources/pcie/wupper.vhd
project addfile ../../sources/pcie/intr_ctrl.vhd
project addfile ../../sources/pcie/dma_read_write.vhd

project addfile ../../sources/templates/dma_control.vhd
project addfile ../../sources/pcie/pcie_clocking.vhd

project addfile ../../Projects/pcie_dma_top/pcie_dma_top.srcs/sources_1/ip/clk_wiz_40/clk_wiz_40_funcsim.vhdl
project addfile ../../Projects/pcie_dma_top/pcie_dma_top.srcs/sources_1/ip/pcie_x8_gen3_3_0/pcie_x8_gen3_3_0_funcsim.vhdl

# ----------------------------------------------------------
# example application
# ----------------------------------------------------------

project addfile ../../sources/opencores/application.vhd
project addfile ../../Projects/pcie_dma_top/pcie_dma_top.srcs/sources_1/ip/fifo_256x256/fifo_256x256_funcsim.vhdl

project compileall


# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Alessandra Camplani
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

force -freeze sim:/fmc_wrapper_tb_foremu/dut/TTCEmu/register_map_control.TTC_EMU.SEL 1'h1 0
force -freeze sim:/fmc_wrapper_tb_foremu/dut/TTCEmu/register_map_control.TTC_EMU.ENA 1'h1 0

force -freeze sim:/fmc_wrapper_tb_foremu/dut/TTCEmu/register_map_control.TTC_DEC_CTRL.TT_BCH_EN 1'h1 0
force -freeze sim:/fmc_wrapper_tb_foremu/dut/TT_Bch_En_Reg 1 0

#force -freeze sim:/fmc_wrapper_tb_foremu/dut/TTCEmu/register_map_control.TTC_EMU_ECR_PERIOD 0 {0 ns}
#force -freeze sim:/fmc_wrapper_tb_foremu/dut/TTCEmu/register_map_control.TTC_EMU_L1A_PERIOD 32'h100 {2 ns}
#force -freeze sim:/fmc_wrapper_tb_foremu/dut/TTCEmu/register_map_control.TTC_EMU_BCR_PERIOD 32'hDEB {2 ns}

force -freeze sim:/fmc_wrapper_tb_foremu/dut/TTCEmu/input_ecr_period 0 0
force -freeze sim:/fmc_wrapper_tb_foremu/dut/TTCEmu/input_l1a_period 32'h500 {2 ns}
force -freeze sim:/fmc_wrapper_tb_foremu/dut/TTCEmu/input_bcr_period 32'hdeb 0

#force -freeze sim:/ttc_emulator_tb/dut/register_map_control.TTC_EMU_CONTROL.L1A 1'h1 {10000 ns} -cancel {10100 ns}


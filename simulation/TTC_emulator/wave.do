
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Alessandra Camplani
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /ttc_emulator_tb/Clock
add wave -noupdate /ttc_emulator_tb/Reset
add wave -noupdate /ttc_emulator_tb/dut/en
add wave -noupdate -group Period_signals -radix unsigned -radixshowbase 0 /ttc_emulator_tb/dut/input_bcr_period
add wave -noupdate -group Period_signals -radix unsigned -radixshowbase 0 /ttc_emulator_tb/dut/bcr_cnt
add wave -noupdate -group Period_signals /ttc_emulator_tb/dut/prepare_bcr
add wave -noupdate -group Period_signals -radix unsigned -radixshowbase 0 /ttc_emulator_tb/dut/input_ecr_period
add wave -noupdate -group Period_signals -radix unsigned -radixshowbase 0 /ttc_emulator_tb/dut/ecr_cnt
add wave -noupdate -group Period_signals /ttc_emulator_tb/dut/prepare_ecr
add wave -noupdate -group Period_signals -radix unsigned -radixshowbase 0 /ttc_emulator_tb/dut/input_l1a_period
add wave -noupdate -group Period_signals -radix unsigned -radixshowbase 0 /ttc_emulator_tb/dut/l1a_cnt
add wave -noupdate -group Period_signals -color White /ttc_emulator_tb/dut/l1_accept
add wave -noupdate -group Period_signals /ttc_emulator_tb/dut/prepare_bcr
add wave -noupdate -expand -group {Priority tests} -radix hexadecimal -radixshowbase 0 /ttc_emulator_tb/dut/Bchannel_short_b
add wave -noupdate -expand -group {Priority tests} -radix unsigned -radixshowbase 0 /ttc_emulator_tb/dut/bcr_cnt
add wave -noupdate -expand -group {Priority tests} -color Gold /ttc_emulator_tb/dut/Bch_e_fifo_wr
add wave -noupdate -expand -group {Priority tests} -color Gold -radix hexadecimal -radixshowbase 0 /ttc_emulator_tb/dut/inst_fifo/din
add wave -noupdate -expand -group {Priority tests} -color Gold /ttc_emulator_tb/dut/fifo_empty
add wave -noupdate -expand -group {Priority tests} -color Gold /ttc_emulator_tb/dut/serialization_process
add wave -noupdate -expand -group {Priority tests} -color Gold /ttc_emulator_tb/dut/trigger_read
add wave -noupdate -expand -group {Priority tests} -color Gold /ttc_emulator_tb/dut/fifo_read
add wave -noupdate -expand -group {Priority tests} -color Gold -radix binary -childformat {{{/ttc_emulator_tb/dut/inst_fifo/dout[41]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[40]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[39]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[38]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[37]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[36]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[35]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[34]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[33]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[32]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[31]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[30]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[29]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[28]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[27]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[26]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[25]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[24]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[23]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[22]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[21]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[20]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[19]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[18]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[17]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[16]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[15]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[14]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[13]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[12]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[11]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[10]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[9]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[8]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[7]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[6]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[5]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[4]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[3]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[2]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[1]} -radix hexadecimal} {{/ttc_emulator_tb/dut/inst_fifo/dout[0]} -radix hexadecimal}} -radixshowbase 0 -subitemconfig {{/ttc_emulator_tb/dut/inst_fifo/dout[41]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[40]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[39]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[38]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[37]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[36]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[35]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[34]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[33]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[32]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[31]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[30]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[29]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[28]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[27]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[26]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[25]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[24]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[23]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[22]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[21]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[20]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[19]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[18]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[17]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[16]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[15]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[14]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[13]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[12]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[11]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[10]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[9]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[8]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[7]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[6]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[5]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[4]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[3]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[2]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[1]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0} {/ttc_emulator_tb/dut/inst_fifo/dout[0]} {-color Gold -height 15 -radix hexadecimal -radixshowbase 0}} /ttc_emulator_tb/dut/inst_fifo/dout
add wave -noupdate -expand -group {Priority tests} -color Gold /ttc_emulator_tb/dut/fifo_read_r
add wave -noupdate -expand -group {Priority tests} -color Gold -radix binary -radixshowbase 0 /ttc_emulator_tb/dut/Bchannel_fifo_out_r
add wave -noupdate -expand -group {Priority tests} -radix binary -radixshowbase 0 /ttc_emulator_tb/dut/Bchannel_low_prio
add wave -noupdate -expand -group {Priority tests} -color Magenta -radix unsigned -radixshowbase 0 /ttc_emulator_tb/dut/fifo_bch_cnt
add wave -noupdate -expand -group {Priority tests} -color Magenta /ttc_emulator_tb/dut/fifo_bchan_valid
add wave -noupdate -expand -group {Priority tests} -color {Medium Blue} /ttc_emulator_tb/dut/prepare_ecr
add wave -noupdate -expand -group {Priority tests} -color {Medium Blue} -radix hexadecimal /ttc_emulator_tb/dut/Bchannel_short_e
add wave -noupdate -expand -group {Priority tests} -color White /ttc_emulator_tb/dut/l1_accept
add wave -noupdate -expand -group {Priority tests} -color White /ttc_emulator_tb/dut/l1_accept_r
add wave -noupdate -expand -group {Priority tests} -color White -radix hexadecimal -radixshowbase 0 /ttc_emulator_tb/dut/Bch_long
add wave -noupdate -expand -group {Priority tests} -color White -radix hexadecimal -radixshowbase 0 /ttc_emulator_tb/dut/Bch_long_r
add wave -noupdate -expand -group {Priority tests} -color Gold -radix unsigned -radixshowbase 0 /ttc_emulator_tb/dut/short_bch_cnt
add wave -noupdate -expand -group {Priority tests} -color Gold /ttc_emulator_tb/dut/short_b_bchan_valid
add wave -noupdate -expand -group {Priority tests} -color Gold /ttc_emulator_tb/dut/store_short_b
add wave -noupdate -expand -group {Priority tests} -color Gold -radix hexadecimal -radixshowbase 0 /ttc_emulator_tb/dut/short_b
add wave -noupdate -expand -group {Priority tests} /ttc_emulator_tb/dut/Serial_Bchannel
add wave -noupdate /ttc_emulator_tb/dut/state
add wave -noupdate /ttc_emulator_tb/dut/sm_short_cnt
add wave -noupdate /ttc_emulator_tb/dut/Serial_Bchannel
add wave -noupdate /ttc_emulator_tb/dut/Serial_Bchannel_r
add wave -noupdate -color White /ttc_emulator_tb/dut/l1_accept
add wave -noupdate /ttc_emulator_tb/dut/ecr
add wave -noupdate /ttc_emulator_tb/dut/bcr
add wave -noupdate -radix binary -radixshowbase 0 /ttc_emulator_tb/dut/broad_dec
add wave -noupdate -radix binary -radixshowbase 0 /ttc_emulator_tb/dut/broad
add wave -noupdate /ttc_emulator_tb/dut/broad_done
add wave -noupdate -expand /ttc_emulator_tb/dut/TTCout
add wave -noupdate /ttc_emulator_tb/dut/register_map_control.TTC_EMU_CONTROL.ECR
add wave -noupdate /ttc_emulator_tb/dut/register_map_control.TTC_EMU_CONTROL.BCR
add wave -noupdate /ttc_emulator_tb/dut/register_map_control.TTC_EMU_CONTROL.L1A
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {18293297879 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 201
configure wave -valuecolwidth 73
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {2935851211 fs} {21950744779 fs}

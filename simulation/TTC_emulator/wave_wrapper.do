
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Alessandra Camplani
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /fmc_wrapper_tb_foremu/dut/TTCEmu/Clock
add wave -noupdate -subitemconfig {/fmc_wrapper_tb_foremu/dut/TTCEmu/register_map_control.TTC_EMU.SEL -expand /fmc_wrapper_tb_foremu/dut/TTCEmu/register_map_control.TTC_EMU.ENA -expand} /fmc_wrapper_tb_foremu/dut/TTCEmu/register_map_control.TTC_EMU
add wave -noupdate /fmc_wrapper_tb_foremu/register_map_control.TTC_DEC_CTRL.TT_BCH_EN
add wave -noupdate /fmc_wrapper_tb_foremu/dut/TT_Bch_En_Reg
add wave -noupdate -group {Internal Emu} /fmc_wrapper_tb_foremu/dut/TTCEmu/en
add wave -noupdate -group {Internal Emu} /fmc_wrapper_tb_foremu/dut/TTCEmu/input_l1a_period
add wave -noupdate -group {Internal Emu} /fmc_wrapper_tb_foremu/dut/TTCEmu/input_ecr_period
add wave -noupdate -group {Internal Emu} /fmc_wrapper_tb_foremu/dut/TTCEmu/input_bcr_period
add wave -noupdate -group {Internal Emu} /fmc_wrapper_tb_foremu/dut/TTCEmu/l1_accept
add wave -noupdate -group {Internal Emu} /fmc_wrapper_tb_foremu/dut/TTCEmu/prepare_ecr
add wave -noupdate -group {Internal Emu} /fmc_wrapper_tb_foremu/dut/TTCEmu/prepare_bcr
add wave -noupdate -group {fifo to host data} /fmc_wrapper_tb_foremu/dut/Fifo_ToHostData/clk
add wave -noupdate -group {fifo to host data} /fmc_wrapper_tb_foremu/dut/Fifo_ToHostData/data_count
add wave -noupdate -group {fifo to host data} /fmc_wrapper_tb_foremu/dut/Fifo_ToHostData/din
add wave -noupdate -group {fifo to host data} /fmc_wrapper_tb_foremu/dut/Fifo_ToHostData/dout
add wave -noupdate -group {fifo to host data} /fmc_wrapper_tb_foremu/dut/Fifo_ToHostData/empty
add wave -noupdate -group {fifo to host data} /fmc_wrapper_tb_foremu/dut/Fifo_ToHostData/full
add wave -noupdate -group {fifo to host data} /fmc_wrapper_tb_foremu/dut/Fifo_ToHostData/rd_en
add wave -noupdate -group {fifo to host data} /fmc_wrapper_tb_foremu/dut/Fifo_ToHostData/rd_rst_busy
add wave -noupdate -group {fifo to host data} /fmc_wrapper_tb_foremu/dut/Fifo_ToHostData/srst
add wave -noupdate -group {fifo to host data} /fmc_wrapper_tb_foremu/dut/Fifo_ToHostData/wr_en
add wave -noupdate -group {fifo to host data} /fmc_wrapper_tb_foremu/dut/Fifo_ToHostData/wr_rst_busy
add wave -noupdate /fmc_wrapper_tb_foremu/dut/local_ttc_clk
add wave -noupdate -color Yellow /fmc_wrapper_tb_foremu/dut/TTCEmu/add_e
add wave -noupdate -color Yellow /fmc_wrapper_tb_foremu/dut/TTCEmu/add_s8
add wave -noupdate -color Yellow /fmc_wrapper_tb_foremu/dut/TTCEmu/add_strobe
add wave -noupdate -radix binary -radixshowbase 0 /fmc_wrapper_tb_foremu/dut/TTCEmu/add_d8
add wave -noupdate -color {Cornflower Blue} -radix unsigned -radixshowbase 0 /fmc_wrapper_tb_foremu/dut/TTYPE_counter
add wave -noupdate /fmc_wrapper_tb_foremu/dut/TTCEmu/TTCout
add wave -noupdate -radix binary -radixshowbase 0 /fmc_wrapper_tb_foremu/dut/trigger_type
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {32888183477 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 140
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {31009939532 fs} {33242908400 fs}

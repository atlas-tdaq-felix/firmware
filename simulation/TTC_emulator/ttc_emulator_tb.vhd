--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Alessandra Camplani
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Design     : ttc_emulator_tb
-- Author     : Alessandra Camplani
-- Email      : alessandra.camplani@cern.ch
-- Created    : 21.01.2020
--------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use work.pcie_package.all;

library work;

entity ttc_emulator_tb is
--generic (
--    constant COMMENT_FLAG    : character := '%';
--    --constant ttc_data_in        : string    := "data/ttc_data_in.dat"
--    constant ttc_data_in        : string    := "../../../../../../simulation/TTC_emulator/data/ttc_data_in.dat"
--);
end ttc_emulator_tb;

architecture struct of ttc_emulator_tb is

    signal Clock                        : std_logic                      := '0';
    signal clk160                       : std_logic                      := '0';
    signal Reset                        : std_logic                      := '0';

    signal register_map_control         : register_map_control_type;

    signal TTCout                       : std_logic_vector(9 downto 0)   := (others => '0');
    signal add_s8, add_s8_40            : std_logic_vector(7 downto 0)   := (others => '0');
    signal add_d8, add_d8_40            : std_logic_vector(7 downto 0)   := (others => '0');
    signal add_strobe, add_strobe_40    : std_logic                      := '0';
    signal add_e, add_e_40              : std_logic                      := '0';

    signal TTYPE_counter                : unsigned(31 downto 0) := (others => '0');

    signal TTYPE_cntr_reset_0           : std_logic                      := '0';
    signal TTYPE_cntr_reset_1           : std_logic                      := '0';

    signal counter                      : integer                        := 0;
    signal OCR : std_logic;
    constant OCR_Period: integer := 35640;

    signal SCA_RESET, SOFT_RESET: std_logic;

    --signal L1A,
    signal TTC_EMU_CONTROL : bitfield_ttc_emu_control_w_type;

    type TTCoutBits_type is record
        b0_L1A: std_logic;
        b1_SerialB: std_logic;
        b2_BCR: std_logic;
        b3_ECR: std_logic;
        b4_testpulse: std_logic;
        b5_brc_d4_1: std_logic;
        b6_brc_d4_2: std_logic;
        b7_brc_d4_3: std_logic;
        b8_brc_t2_0: std_logic;
        b9_OCR: std_logic;
    end record;

    signal TTCoutBits : TTCoutBits_type;

    signal TTCout_16b: std_logic_vector(15 downto 0); --with latched broadcast bit added
    signal TTCout_17b: std_logic_vector(16 downto 0); --With extended test pulse inserted at bit 10.

    signal Brcst_latched        : std_logic_vector(5 downto 0) := "000000"; -- needed for the NSW. Controlled with the long commands
    signal brc_strobe, brc_strobe_40: std_logic;

    signal brc_t2                       : std_logic_vector(1 downto 0);
    signal brc_t2_d4_40                 : std_logic_vector(5 downto 0);
    signal brc_d4                       : std_logic_vector(3 downto 0);

    signal add_a14                      : std_logic_vector(13 downto 0);
    signal add_a14_40                   : std_logic_vector(13 downto 0);

    signal EDATA_OUT: std_logic_vector(7 downto 0);

    type EDATA_OUTbits_type is record
        b7_L1A       : std_logic;
        b6_SoftReset : std_logic;
        b5_TestPulse : std_logic;
        b4_ECR       : std_logic;
        b3_BCR_OCR   : std_logic;
        b2_L0A       : std_logic;
        b1_SCA_reset : std_logic;
        b0_ECOR      : std_logic;
    end record;

    signal EDATA_OUTbits : EDATA_OUTbits_type;

    signal ExtendedTestPulse: std_logic;

    signal BCR_MISMATCH_counter: std_logic_vector(31 downto 0);
    signal BCR_Period: std_logic_vector(11 downto 0);
    signal single_l1a: std_logic;

begin

    dut: entity work.TTC_Emulator
        port map(
            Clock                       => Clock,
            Reset                       => Reset,
            register_map_control        => register_map_control,
            add_s8                      => add_s8,
            add_d8                      => add_d8,
            add_strobe                  => add_strobe,
            add_e                       => add_e,
            TTCout                      => TTCout,
            BUSYin                      => '0'
        );

    brc_strobe <= '0';
    brc_t2 <= TTCout(9 downto 8);
    brc_d4 <= TTCout(7 downto 4);
    add_a14 <= (others => '0');


    eprocout8_inst0: entity work.EPROC_OUT8
        generic map(
            egroupID                        => 0,
            EnableFrHo_Egroup0Eproc8_8b10b  => true,
            EnableFrHo_Egroup1Eproc8_8b10b  => true,
            EnableFrHo_Egroup2Eproc8_8b10b  => true,
            EnableFrHo_Egroup3Eproc8_8b10b  => true,
            EnableFrHo_Egroup4Eproc8_8b10b  => true,
            includeNoEncodingCase           => false
        )
        port map(
            bitCLK      => Clock,
            bitCLKx4    => clk160,
            rst40       => Reset,
            rst160      => Reset,
            ENA         => '1',
            swap_outbits=> '0',
            getDataTrig => open,
            ENCODING    => x"4", --TTC-4 option
            EDATA_OUT   => EDATA_OUT,
            TTCin       => TTCout_17b,
            fhCR_REVERSE_10B => '1',
            DATA_IN     => "0000000000",
            DATA_RDY    => '0'
        );

    OCR_proc: process(Clock)
        variable OcrCounter: integer;
    begin
        if rising_edge(Clock) then
            if Reset = '1' then
                OcrCounter := 0;
                OCR <= '0';
            else
                OCR <= '0';
                if OcrCounter < OCR_Period then
                    OcrCounter := OcrCounter + 1;
                    if OcrCounter > (OCR_Period - 10) then --OCR 10 clocks high
                        OCR <= '1';
                    end if;
                else
                    OcrCounter := 0;
                end if;
            end if;
        end if;
    end process;

    single_l1a_proc: process(Clock)
        variable L1ACounter: integer;
    begin
        if rising_edge(Clock) then
            if Reset = '1' then
                L1ACounter := 0;
                single_l1a <= '0';
            else
                single_l1a <= '0';
                if L1ACounter < 255 then
                    L1ACounter := L1ACounter + 1;
                else
                    single_l1a <= '1';
                    L1ACounter := 0;
                end if;
            end if;
        end if;
    end process;

    sca_reset_soft_reset_proc: process(Clock)
        variable reset_counter: integer;
    begin
        if rising_edge(Clock) then
            if Reset = '1' then
                reset_counter := 0;
                SOFT_RESET <= '0';
                SCA_RESET <= '0';
            else
                SOFT_RESET <= '0';
                SCA_RESET <= '0';
                if reset_counter = 50 then
                    SCA_RESET <= '1';
                end if;
                if reset_counter = 60 then
                    SOFT_RESET <= '1';
                end if;
                if reset_counter < 100 then
                    reset_counter := reset_counter + 1;
                end if;
            end if;
        end if;
    end process;

    EDATA_OUTbits.b7_L1A       <= EDATA_OUT(7);
    EDATA_OUTbits.b6_SoftReset <= EDATA_OUT(6);
    EDATA_OUTbits.b5_TestPulse <= EDATA_OUT(5);
    EDATA_OUTbits.b4_ECR       <= EDATA_OUT(4);
    EDATA_OUTbits.b3_BCR_OCR   <= EDATA_OUT(3);
    EDATA_OUTbits.b2_L0A       <= EDATA_OUT(2);
    EDATA_OUTbits.b1_SCA_reset <= EDATA_OUT(1);
    EDATA_OUTbits.b0_ECOR      <= EDATA_OUT(0);

    TTCout_16b(9 downto 0) <= TTCout;
    TTCout_16b(15 downto 10) <= Brcst_latched;

    --Copied and simplified from ttc_fmc_wrapper_xilinx.vhd
    L1ID_gen: process (Clock)
    begin
        if (rising_edge(Clock)) then

            brc_strobe_40 <= brc_strobe;
            brc_t2_d4_40 <= brc_t2 & brc_d4;

            add_strobe_40 <= add_strobe;
            add_e_40 <= add_e;
            add_s8_40 <= add_s8;
            add_d8_40 <= add_d8;
            add_a14_40 <= add_a14;

            if (Reset='1') then --synchronous reset
                Brcst_latched <= "000000";

            else
                if ( add_strobe_40 = '1' and add_e_40 = '1' and add_s8_40 = x"04" and add_a14_40 = "00000000000000") then
                    if(add_d8_40(6) = '1') then
                        Brcst_latched <= Brcst_latched or add_d8_40(5 downto 0);
                    else
                        Brcst_latched <= Brcst_latched and (not add_d8_40(5 downto 0));
                    end if;
                elsif (brc_strobe_40 = '1') then
                    Brcst_latched <= Brcst_latched xor brc_t2_d4_40;
                end if;
            end if; --reset
        end if; --clock
    end process;

    --Copied and simplified from centralRouter.vhd
    ExtendingTestPulse: entity work.ExtendedTestPulse (Behavioral)
        port map(
            clk40               => Clock,
            reset               => Reset,

            TTCin_TestPulse     => TTCout_16b(4),
            ExtendedTestPulse   => ExtendedTestPulse
        );
    process(Clock)
    begin
        if rising_edge(Clock) then
            TTCout_17b(9 downto 0) <= TTCout_16b(9 downto 0);
            TTCout_17b(16 downto 11) <= TTCout_16b(15 downto 10);
        end if;
    end process;

    TTCout_17b(10)    <= ExtendedTestPulse; -- the ExtendedTestPulse already set in one clock delay, like in the "ttcn" process above


    -- Clock generator, also outputting a counter
    sim_basics_clk250 : entity work.simulation_basics
        generic map (
            reset_duration => 10,
            clk_offset     => 0 ns,
            clk_period     => 25 ns
        )
        port map (
            clk     => Clock,
            rst     => Reset,
            counter => counter
        );

    clk160_proc: process
    begin
        clk160 <= '1';
        wait for 12.5 ns;
        clk160 <= '0';
        wait for 12.5 ns;
    end process;




    TTCoutBits.b0_L1A        <= TTCout(0);
    TTCoutBits.b1_SerialB    <= TTCout(1);
    TTCoutBits.b2_BCR        <= TTCout(2);
    TTCoutBits.b3_ECR        <= TTCout(3);
    TTCoutBits.b4_testpulse  <= TTCout(4);
    TTCoutBits.b5_brc_d4_1   <= TTCout(5);
    TTCoutBits.b6_brc_d4_2   <= TTCout(6);
    TTCoutBits.b7_brc_d4_3   <= TTCout(7);
    TTCoutBits.b8_brc_t2_0   <= TTCout(8);
    TTCoutBits.b9_OCR        <= TTCout(9);

    register_map_control.TTC_EMU_CONTROL <= TTC_EMU_CONTROL;
    TTC_EMU_CONTROL.ECR     <= (others => '0');
    TTC_EMU_CONTROL.L1A     <= (others => single_l1a);
    TTC_EMU_CONTROL.BCR     <= (others => '0');
    TTC_EMU_CONTROL.BROADCAST(27) <= '1'; --Enable testpulse
    TTC_EMU_CONTROL.BROADCAST(28) <= SOFT_RESET;
    TTC_EMU_CONTROL.BROADCAST(29) <= '0';
    TTC_EMU_CONTROL.BROADCAST(30) <= SCA_RESET;
    TTC_EMU_CONTROL.BROADCAST(31) <= '0';
    TTC_EMU_CONTROL.BROADCAST(32) <= OCR;
    TTC_EMU_CONTROL.BUSY_IN_ENABLE  <= (others => '0');
    register_map_control.TTC_EMU_L1A_PERIOD <= x"00000100";
    register_map_control.TTC_EMU_ECR_PERIOD <= std_logic_vector(to_unsigned(10000, 32));
    register_map_control.TTC_EMU_BCR_PERIOD <= std_logic_vector(to_unsigned(3564, 32));
    register_map_control.TTC_EMU_LONG_CHANNEL_DATA <= x"00000000";
    register_map_control.TTC_EMU_TP_DELAY <= x"00000040";
    register_map_control.TTC_EMU_RESET(64) <= reset;
    register_map_control.TTC_EMU.SEL <= "1";
    register_map_control.TTC_EMU.ENA <= "1";



    register_map_control.TTC_TTYPE_MONITOR.CLEAR <= (others => '0');
    TTYPE_cntr_reset_0 <= '0';

    --Trigger TYPE counter for monitoring
    TTYPE_CNT: process (Clock)
    begin
        if (rising_edge(Clock)) then --40 MHz clock
            TTYPE_cntr_reset_0 <= to_sl(register_map_control.TTC_TTYPE_MONITOR.CLEAR);
            TTYPE_cntr_reset_1 <= TTYPE_cntr_reset_0;

            if (TTYPE_cntr_reset_0 = '1' and TTYPE_cntr_reset_1 = '0') then
                TTYPE_counter <= (others => '0');
            elsif (add_strobe = '1' and add_e = '1' and add_s8 = x"00") then --data from long b-channel commands
                TTYPE_counter <= TTYPE_counter + 1;
            end if;

        end if;
    end process;


    --BCR PERIODICITY MONITOR
    -- Counts the number of times the BCR period does not match 3564
    BCR_ERR_CNT: process (Clock)
    begin
        if (rising_edge(Clock)) then --40 MHz clock
            if (Reset = '1') then
                BCR_MISMATCH_counter <= (others=>'0');
                --need to update the period counter
                if(TTCoutBits.b2_BCR = '1') then
                    BCR_period <= (others=>'0');
                elsif (BCR_period /= "111111111111") then --Do not roll over the couter
                    BCR_period <= BCR_period + 1;
                end if;
            elsif (TTCoutBits.b2_BCR = '1') then --check if BCR is arriving on time
                BCR_period <= (others=>'0');
                if (BCR_period /= "110111101011") then  --3564 is the number of buckets in an LHC orbit.
                    BCR_MISMATCH_counter <= BCR_MISMATCH_counter + 1;
                end if;
            elsif (BCR_period /= "111111111111") then --Do not roll over the couter
                BCR_period <= BCR_period + 1;
            else --BCR is not arriving at ALL
                if(BCR_MISMATCH_counter /= x"FFFFFFFF") then
                    BCR_MISMATCH_counter <= BCR_MISMATCH_counter + 1;
                end if;
            end if;
        end if;
    end process;

end struct;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Alessandra Camplani
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Design     : file_reader_hex 
-- Author     : Steffen Staerz
-- Comments   : Reads data from a file of hex values (in strings)
--------------------------------------------------------------------------------
-- Details    :
-- based on http://www.pldworld.com/_hdl/2/_ref/acc-eda/language_overview/test_benches/reading_and_writing_files_with_text_i_o.htm
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;
use ieee.std_logic_textio.all;

LIBRARY work;
use work.FELIX_package.all;

entity FILE_READER_HEX is
generic (
    FILENAME        : string := "C:\Users\staerz\Xilinx\EMF_UDP\isim\ARP_request.dat";
    COMMENT_FLAG    : character := '%';
    COUNTER_FLAG    : character := '@';
    BITPERWORD      : integer := 16;
    WORDSPERLINE    : integer := 4
    -- expects: wordperline words with constant bit width of bitsperword each
);
port (
    --  Inputs
    CLK         : in  std_logic;    -- tx/rx clk
    RST         : in  std_logic;    -- synch reset with clk
    REN         : in  std_logic;    -- read enable

    -- calculate number of empty bits: divide bits per cycle by 8 (bits per symbol) and get number of necessary bits for this integer
    EMPTY       : out std_logic_vector(f_log2(div_ceil(BITPERWORD*WORDSPERLINE,8))-1 downto 0) := (others => '0');
    EOP         : out std_logic := '0';
    ERR         : out std_logic := '0';

    DOUT        : out std_logic_vector(BITPERWORD*WORDSPERLINE-1 downto 0) := (others => '0');
    NEXT_STIM   : out integer := 0;

    -- end of file:
    EOF         : out std_logic := '0'
);
end FILE_READER_HEX;

architecture readhex of FILE_READER_HEX is
begin
    READCMD: process--(clk)

        file CMDFILE: TEXT;       -- Define the file 'handle'
    variable FILESTATUS: FILE_OPEN_STATUS; -- File status after opening it
        variable LINE_IN: Line; -- Line buffers
        variable GOOD: boolean;   -- Status of the read operations
        type T_WORD is array (0 to WORDSPERLINE-1) of std_logic_vector(BITPERWORD-1 downto 0);
        variable WORD : T_WORD;
        variable EMPTY_DATA: std_logic_vector(f_log2(div_ceil(BITPERWORD*WORDSPERLINE,8))-1 downto 0);
        variable ERR_DATA: std_logic;
        -- Use a procedure to generate one clock cycle...
        variable EOF_I : boolean := FALSE; -- end of file
        -- variable eop_v : boolean := false; -- end of packet
        variable DUMMY  : character; -- read count_match flag
        variable BUFF   : integer;

--      variable comment : string(1 to 80);
    begin

	-- Open the command file...
    FILE_OPEN(FILESTATUS,CMDFILE,FILENAME,READ_MODE);
    -- check the file status: if it is not ok, complain with error level
    -- simulator will throw a fatal if it's actually severe, but at least you get a hint here
    case FILESTATUS is
    when NAME_ERROR =>
      assert false report "File '" & FILENAME & "' not found!" severity error;
    when MODE_ERROR =>
      assert false report "Cannot open file '" & FILENAME & "' in READ mode!" severity error;
    when STATUS_ERROR =>
      assert false report "Status error with file '" & FILENAME & "'!" severity error;
    when OPEN_OK =>
      -- "nothing to report"
      null;
    end case;
--    FILE_CLOSE(CMDFILE);
--		if rising_edge(clk) then
--			with sensitivity to clk would re-open the file each cycle, so use an infinite loop instead

    -- Open the command file...

--        FILE_OPEN(CMDFILE,FILENAME,READ_MODE);
--      if rising_edge(clk) then
--          with sensitivity to clk would re-open the file each cycle, so use an infinite loop instead

        loop -- of current packet
            wait until CLK = '1';

            if RST = '1' then
                -- re-open the file to start over again
                FILE_CLOSE(CMDFILE);
        -- we assume(!) that there are no issues with the file in the meantime (not to repeat the entire check ...)
                FILE_OPEN(CMDFILE,FILENAME,READ_MODE);
                EOF_I := FALSE;
                NEXT_STIM <= 0;
            elsif REN = '1' and not EOF_I then
                loop
-- this check will cause an error if eof is not respected by the outer module!
-- (as the file was already closed)
                    if not endfile(CMDFILE) then  -- Check EOF
                        readline(CMDFILE,LINE_IN);     -- Read a line from the file
                        EOF_I := FALSE;
                    else
                        FILE_CLOSE(CMDFILE);
                        EOF_I := TRUE;
                        exit;
                    end if;

--  check for commented line
                    if LINE_IN'length > 0 then
                        if LINE_IN(LINE_IN'left) = COMMENT_FLAG then
                            -- read(line_in,comment);
--                          assert false report "comment" severity note;
--  check for valid data line
                        -- read next time
                        elsif LINE_IN(LINE_IN'left) = COUNTER_FLAG then
                            read(LINE_IN, DUMMY);
                            read(LINE_IN, BUFF);
                            NEXT_STIM <= BUFF;
                            exit;
                        elsif LINE_IN'length >= (BITPERWORD/4 + 1)*WORDSPERLINE - 1 then
                            exit;
                        end if;
                    end if;

                end loop;

                -- indicate end of file to outer module
                if EOF_I then
                    EOF <= '1';
                else
                    EOF <= '0';
                end if;

                if LINE_IN'length >= BITPERWORD/4*WORDSPERLINE then
                -- skip everything with less then full length
                    DOUT <= (others => '0');
                    for i in WORDSPERLINE-1 downto 0 loop
                        hread(LINE_IN,WORD(i),GOOD);     -- Read word from line
                        assert GOOD
                            report "Text I/O read error: start comments in line with 'comment_flag' specified in generics"
                            severity ERROR;

                        DOUT((I+1)*BITPERWORD-1 downto i*BITPERWORD) <= WORD(i);
                    end loop;

                    -- try to read empty flag and error flag:
                    read(LINE_IN,EMPTY_DATA,GOOD);
                    read(LINE_IN,ERR_DATA,GOOD);

                    if GOOD then
                        EMPTY <= EMPTY_DATA;
                        ERR <= ERR_DATA;
                        EOP <= '1';
                    else
                        EMPTY <= (others => '0');
                        ERR <= '0';
                        EOP <= '0';
                    end if;

                end if;
            end if;
        end loop; -- of current packet


    end process;

end architecture readhex;

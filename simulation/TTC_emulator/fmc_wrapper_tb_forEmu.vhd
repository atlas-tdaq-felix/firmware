--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Alessandra Camplani
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Design     : fmc_wrapper_tb_forEmu  
-- Author     : Alessandra Camplani
-- Email      : alessandra.camplani@cern.ch
-- Created    : 21.01.2020
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pcie_package.all;
use work.centralRouter_package.all;


library work;

entity fmc_wrapper_tb_forEmu is

end fmc_wrapper_tb_forEmu;

architecture struct of fmc_wrapper_tb_forEmu is

    signal CLK_TTC_P                   : std_logic                      := '0';           -- ADN2812 CDR 160MHz clock output
    signal CLK_TTC_N                   : std_logic                      := '0';           -- ADN2812 CDR 160MHz clock output
    signal DATA_TTC_P                  : std_logic                      := '0';           -- ADN2812 CDR Serial Data output
    signal DATA_TTC_N                  : std_logic                      := '0';           -- ADN2812 CDR Serial Data output
    signal LOL_ADN                     : std_logic                      := '0';           -- ADN2812 CDR Loss of Lock output
    signal LOS_ADN                     : std_logic                      := '0';           -- ADN2812 CDR Loss of Signal output
    signal RESET_N                     : std_logic                      := '0';           -- if low, stop PLL inside wrapper
    signal register_map_control        : register_map_control_type;
    signal register_map_ttc_monitor    : register_map_ttc_monitor_type;
    --== to Central Router ==---
    signal TTC_out                     : std_logic_vector(9 downto 0);
    
    signal clk_adn_160                 : std_logic                      := '0';
    signal clk_ttc_40                  : std_logic                      := '0';            --40Mhz clock rcovered from the TTC signal
    signal clk40                       : std_logic                      := '0';             --clock source to synchronize TTC_out to the rest of the logic
        
    signal BUSY                        : std_logic                      := '0';            --TTC decode errors
    signal cdrlocked_out               : std_logic                      := '0';
    signal TTC_ToHost_Data_out         : TTC_ToHost_data_type;
    signal TTC_BUSY_mon_array          : busyOut_array_type(23 downto 0);
    signal BUSY_IN                     : std_logic                      := '0';

    signal counter_p                   : integer                        := 0;
    signal counter_n                   : integer                        := 0;
    signal counter_40                  : integer                        := 0;

begin

    dut: entity work.ttc_wrapper
        port map(
            --== ttc fmc interface ==--
            CLK_TTC_P                   => CLK_TTC_P,
            CLK_TTC_N                   => CLK_TTC_N,
            DATA_TTC_P                  => DATA_TTC_P,
            DATA_TTC_N                  => DATA_TTC_N,
            LOL_ADN                     => LOL_ADN,
            LOS_ADN                     => LOS_ADN,
            RESET_N                     => RESET_N,
            register_map_control        => register_map_control,
            register_map_ttc_monitor    => register_map_ttc_monitor,
            --== to Central Router ==---
            TTC_out                     => TTC_out,
            
            clk_adn_160                 => clk_adn_160,
            clk_ttc_40                  => clk_ttc_40,
            clk40                       => clk40,
                
            BUSY                        => BUSY,
            cdrlocked_out               => cdrlocked_out,
            TTC_ToHost_Data_out         => TTC_ToHost_Data_out,
            TTC_BUSY_mon_array          => TTC_BUSY_mon_array,
            BUSY_IN                     => BUSY_IN
        );


-- Clock generator, also outputting a counter
    sim_basics_clk160p : entity work.simulation_basics
        generic map (
            reset_duration => 10,
            clk_offset     => 0 ns,
            clk_period     => 6.25 ns
        )
        port map (
            clk     => CLK_TTC_P,
            rst     => open,
            counter => counter_p
        );

-- Clock generator, also outputting a counter
    sim_basics_clk160n : entity work.simulation_basics
        generic map (
            reset_duration => 10,
            clk_offset     => 3.125 ns,
            clk_period     => 6.25 ns
        )
        port map (
            clk     => CLK_TTC_N,
            rst     => open,
            counter => counter_n
        );

-- Clock generator, also outputting a counter
    sim_basics_clk40 : entity work.simulation_basics
        generic map (
            reset_duration => 10,
            clk_offset     => 0 ns,
            clk_period     => 25 ns
        )
        port map (
            clk     => clk40,
            rst     => open,
            counter => counter_40
        );


    RESET_N <=      '1' when counter_p = 0 else
                    '0' when counter_p = 5 else
                    '1';

-- For cyclic mode:
    register_map_control.TTC_EMU_CONTROL.ECR     <= (others => '0');
    register_map_control.TTC_EMU_CONTROL.L1A     <= (others => '0');
    register_map_control.TTC_EMU_CONTROL.BCR     <= (others => '0');
    
    register_map_control.TTC_TTYPE_MONITOR.CLEAR <= (others => '0');




end struct;

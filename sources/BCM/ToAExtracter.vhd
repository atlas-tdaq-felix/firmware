----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 07/29/2022 10:58:42 AM
-- Design Name:
-- Module Name: ToT-ToA-Extracter - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use work.ALL;


entity ToAExtracter is
    generic (DATA_SIZE : integer :=32;
        OTHER_DATA: integer :=0 --This is used for extra bits like BCID, L1A
    );

    Port ( DataIn : in  STD_LOGIC_VECTOR (OTHER_DATA+DATA_SIZE-1 downto 0);
        DataOut: out STD_LOGIC_VECTOR (OTHER_DATA+DATA_SIZE-1 downto 0);
        ToA : out STD_LOGIC_VECTOR (5 downto 0);
        a_clk : in STD_LOGIC;
        reset : in STD_LOGIC
    );

end ToAExtracter;

architecture Behavioral of ToAExtracter is
    signal ToAMap : STD_LOGIC_VECTOR(DATA_SIZE-1 downto 0);
begin

    ToALoop : for ind in 0 to DATA_SIZE -1 generate
        ToAMap(ind) <= OR DataIn( ind downto 0);
    end generate;



    process(a_clk)
    begin
        if rising_edge(a_clk) then
            if reset then
                DataOut <= (others => '0');
                ToA <= "111111";

            else

                DataOut <= DataIn;

                case ToAMap is
                    when "11111111111111111111111111111111" => ToA <= "000000";
                    when "11111111111111111111111111111110" => ToA <= "000001";
                    when "11111111111111111111111111111100" => ToA <= "000010";
                    when "11111111111111111111111111111000" => ToA <= "000011";
                    when "11111111111111111111111111110000" => ToA <= "000100";
                    when "11111111111111111111111111100000" => ToA <= "000101";
                    when "11111111111111111111111111000000" => ToA <= "000110";
                    when "11111111111111111111111110000000" => ToA <= "000111";
                    when "11111111111111111111111100000000" => ToA <= "001000";
                    when "11111111111111111111111000000000" => ToA <= "001001";
                    when "11111111111111111111110000000000" => ToA <= "001010";
                    when "11111111111111111111100000000000" => ToA <= "001011";
                    when "11111111111111111111000000000000" => ToA <= "001100";
                    when "11111111111111111110000000000000" => ToA <= "001101";
                    when "11111111111111111100000000000000" => ToA <= "001110";
                    when "11111111111111111000000000000000" => ToA <= "001111";
                    when "11111111111111110000000000000000" => ToA <= "010000";
                    when "11111111111111100000000000000000" => ToA <= "010001";
                    when "11111111111111000000000000000000" => ToA <= "010010";
                    when "11111111111110000000000000000000" => ToA <= "010011";
                    when "11111111111100000000000000000000" => ToA <= "010100";
                    when "11111111111000000000000000000000" => ToA <= "010101";
                    when "11111111110000000000000000000000" => ToA <= "010110";
                    when "11111111100000000000000000000000" => ToA <= "010111";
                    when "11111111000000000000000000000000" => ToA <= "011000";
                    when "11111110000000000000000000000000" => ToA <= "011001";
                    when "11111100000000000000000000000000" => ToA <= "011010";
                    when "11111000000000000000000000000000" => ToA <= "011011";
                    when "11110000000000000000000000000000" => ToA <= "011100";
                    when "11100000000000000000000000000000" => ToA <= "011101";
                    when "11000000000000000000000000000000" => ToA <= "011110";
                    when "10000000000000000000000000000000" => ToA <= "011111";
                    when "00000000000000000000000000000000" => ToA <= "000000";
                    when others => ToA <= "111111";
                end case;
            end if;
        end if;
    end  process;


end Behavioral;






----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 08/22/2022 01:40:41 PM
-- Design Name:
-- Module Name: ToTMasker - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
    use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ToTMasker is
    Port ( Input : in STD_LOGIC_VECTOR (31 downto 0);
        ToT : in UNSIGNED (5 downto 0);
        ToA : in UNSIGNED (5 downto 0);
        Output : out STD_LOGIC_VECTOR (31 downto 0));
end ToTMasker;

architecture Behavioral of ToTMasker is
    type MaskArr is array (0 to 32) of std_logic_vector (31 downto 0);

    signal RightMask : MaskArr;
    signal LeftMask : MaskArr;
    signal Mask: std_logic_vector (31 downto 0);
    signal Mask1: std_logic_vector (31 downto 0);
    signal Mask2: std_logic_vector (31 downto 0);





begin

    RightMask(0) <= "00000000000000000000000000000000";
    RightMask(1) <= "00000000000000000000000000000001";
    RightMask(2) <= "00000000000000000000000000000011";
    RightMask(3) <= "00000000000000000000000000000111";
    RightMask(4) <= "00000000000000000000000000001111";
    RightMask(5) <= "00000000000000000000000000011111";
    RightMask(6) <= "00000000000000000000000000111111";
    RightMask(7) <= "00000000000000000000000001111111";
    RightMask(8) <= "00000000000000000000000011111111";
    RightMask(9) <= "00000000000000000000000111111111";
    RightMask(10) <= "00000000000000000000001111111111";
    RightMask(11) <= "00000000000000000000011111111111";
    RightMask(12) <= "00000000000000000000111111111111";
    RightMask(13) <= "00000000000000000001111111111111";
    RightMask(14) <= "00000000000000000011111111111111";
    RightMask(15) <= "00000000000000000111111111111111";
    RightMask(16) <= "00000000000000001111111111111111";
    RightMask(17) <= "00000000000000011111111111111111";
    RightMask(18) <= "00000000000000111111111111111111";
    RightMask(19) <= "00000000000001111111111111111111";
    RightMask(20) <= "00000000000011111111111111111111";
    RightMask(21) <= "00000000000111111111111111111111";
    RightMask(22) <= "00000000001111111111111111111111";
    RightMask(23) <= "00000000011111111111111111111111";
    RightMask(24) <= "00000000111111111111111111111111";
    RightMask(25) <= "00000001111111111111111111111111";
    RightMask(26) <= "00000011111111111111111111111111";
    RightMask(27) <= "00000111111111111111111111111111";
    RightMask(28) <= "00001111111111111111111111111111";
    RightMask(29) <= "00011111111111111111111111111111";
    RightMask(30) <= "00111111111111111111111111111111";
    RightMask(31) <= "01111111111111111111111111111111";
    RightMask(32) <= "11111111111111111111111111111111";

    LeftMask(0) <= "11111111111111111111111111111111";
    LeftMask(1) <= "11111111111111111111111111111110";
    LeftMask(2) <= "11111111111111111111111111111100";
    LeftMask(3) <= "11111111111111111111111111111000";
    LeftMask(4) <= "11111111111111111111111111110000";
    LeftMask(5) <= "11111111111111111111111111100000";
    LeftMask(6) <= "11111111111111111111111111000000";
    LeftMask(7) <= "11111111111111111111111110000000";
    LeftMask(8) <= "11111111111111111111111100000000";
    LeftMask(9) <= "11111111111111111111111000000000";
    LeftMask(10) <= "11111111111111111111110000000000";
    LeftMask(11) <= "11111111111111111111100000000000";
    LeftMask(12) <= "11111111111111111111000000000000";
    LeftMask(13) <= "11111111111111111110000000000000";
    LeftMask(14) <= "11111111111111111100000000000000";
    LeftMask(15) <= "11111111111111111000000000000000";
    LeftMask(16) <= "11111111111111110000000000000000";
    LeftMask(17) <= "11111111111111100000000000000000";
    LeftMask(18) <= "11111111111111000000000000000000";
    LeftMask(19) <= "11111111111110000000000000000000";
    LeftMask(20) <= "11111111111100000000000000000000";
    LeftMask(21) <= "11111111111000000000000000000000";
    LeftMask(22) <= "11111111110000000000000000000000";
    LeftMask(23) <= "11111111100000000000000000000000";
    LeftMask(24) <= "11111111000000000000000000000000";
    LeftMask(25) <= "11111110000000000000000000000000";
    LeftMask(26) <= "11111100000000000000000000000000";
    LeftMask(27) <= "11111000000000000000000000000000";
    LeftMask(28) <= "11110000000000000000000000000000";
    LeftMask(29) <= "11100000000000000000000000000000";
    LeftMask(30) <= "11000000000000000000000000000000";
    LeftMask(31) <= "10000000000000000000000000000000";
    LeftMask(32) <= "00000000000000000000000000000000";




    ---Create a Masker
    process(ToA,ToT)
    begin

        --            Mask <= Zeros
        --                    or std_logic_vector(shift_right(unsigned(Ones), to_integer(32-ToA)))
        --                    or std_logic_vector(shift_left(unsigned(Ones), to_integer(ToT+ToA)));

        --            Mask1 <= std_logic_vector(shift_right(unsigned(Ones), to_integer(32-ToA)));
        --            Mask2 <= std_logic_vector(shift_left(unsigned(Ones), to_integer(ToT+ToA)));

        if ToA/="111111" then
            Mask <= RightMask(to_integer(ToA)) or LeftMask(to_integer(ToT+ToA));
        else
            Mask <= (others=>'0');
        end if;

    end process;


    process(Input,Mask)
    begin
        Output <= Input and Mask;
    end process;

end Behavioral;

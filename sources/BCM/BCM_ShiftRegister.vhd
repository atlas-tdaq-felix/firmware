----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 10/16/2023 02:35:44 PM
-- Design Name:
-- Module Name: ShiftReg64 - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
Library UNISIM;
    use UNISIM.vcomponents.all;

entity BCM_ShiftReg is
    Port ( clk : in STD_LOGIC;
        delay : in STD_LOGIC_VECTOR (5 downto 0);
        input : in STD_LOGIC;
        output : out STD_LOGIC);
end BCM_ShiftReg;

architecture Behavioral of BCM_ShiftReg is

    signal out1: std_logic;
    signal out2: std_logic;
    signal out32: std_logic;
begin



    DataDelay_1 : SRLC32E
        generic map(
            INIT => x"0000",
            IS_CLK_INVERTED => '0'
        )
        port map (
            Q => out1, --delayed copy of TTCin
            Q31 => out32,
            A => delay(4 downto 0), --input from control register
            ce => '1', --delay_en,     --input from control register
            clk => clk,
            D => input --latched copy of TTCin
        );

    DataDelay_2 : SRLC32E
        generic map(
            INIT => x"0000",
            IS_CLK_INVERTED => '0'
        )
        port map (
            Q => out2, --delayed copy of TTCin
            Q31 => open,
            A => delay(4 downto 0), --input from control register
            ce => '1', --delay_en,     --input from control register
            clk => clk,
            D => out32 --latched copy of TTCin
        );

    with delay(5) select
    output <= out1 when '0',
              out2 when others;

end Behavioral;

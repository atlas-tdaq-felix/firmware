----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 07/29/2022 10:58:42 AM
-- Design Name:
-- Module Name: ToT-ToA-Extracter - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use work.ALL;


entity ToTToAExtracter is
    generic (DATA_SIZE : integer :=32);

    Port ( DataIn : in STD_LOGIC_VECTOR (DATA_SIZE-1 downto 0);
        ToT : out STD_LOGIC_VECTOR (5 downto 0);
        ToA : out STD_LOGIC_VECTOR (5 downto 0)
    );

end ToTToAExtracter;

architecture Behavioral of ToTToAExtracter is
    signal ToAMap : STD_LOGIC_VECTOR(DATA_SIZE-1 downto 0);
    signal InputExpanded: std_logic_vector(DATA_SIZE downto 0);
begin

    InputExpanded <= '0' & DataIn;

    ToALoop : for ind in 0 to DATA_SIZE -1 generate
        ToAMap(ind) <= OR DataIn( ind downto 0);
    end generate;



    -- Input_Shifted <= std_logic_vector(shift_right(unsigned(DataIn),to_integer(unsigned(ToA))));


    process(ToAMap)
    begin

        case ToAMap is
            when "11111111111111111111111111111111" => ToA <= "000000";
            when "11111111111111111111111111111110" => ToA <= "000001";
            when "11111111111111111111111111111100" => ToA <= "000010";
            when "11111111111111111111111111111000" => ToA <= "000011";
            when "11111111111111111111111111110000" => ToA <= "000100";
            when "11111111111111111111111111100000" => ToA <= "000101";
            when "11111111111111111111111111000000" => ToA <= "000110";
            when "11111111111111111111111110000000" => ToA <= "000111";
            when "11111111111111111111111100000000" => ToA <= "001000";
            when "11111111111111111111111000000000" => ToA <= "001001";
            when "11111111111111111111110000000000" => ToA <= "001010";
            when "11111111111111111111100000000000" => ToA <= "001011";
            when "11111111111111111111000000000000" => ToA <= "001100";
            when "11111111111111111110000000000000" => ToA <= "001101";
            when "11111111111111111100000000000000" => ToA <= "001110";
            when "11111111111111111000000000000000" => ToA <= "001111";
            when "11111111111111110000000000000000" => ToA <= "010000";
            when "11111111111111100000000000000000" => ToA <= "010001";
            when "11111111111111000000000000000000" => ToA <= "010010";
            when "11111111111110000000000000000000" => ToA <= "010011";
            when "11111111111100000000000000000000" => ToA <= "010100";
            when "11111111111000000000000000000000" => ToA <= "010101";
            when "11111111110000000000000000000000" => ToA <= "010110";
            when "11111111100000000000000000000000" => ToA <= "010111";
            when "11111111000000000000000000000000" => ToA <= "011000";
            when "11111110000000000000000000000000" => ToA <= "011001";
            when "11111100000000000000000000000000" => ToA <= "011010";
            when "11111000000000000000000000000000" => ToA <= "011011";
            when "11110000000000000000000000000000" => ToA <= "011100";
            when "11100000000000000000000000000000" => ToA <= "011101";
            when "11000000000000000000000000000000" => ToA <= "011110";
            when "10000000000000000000000000000000" => ToA <= "011111";
            when "00000000000000000000000000000000" => ToA <= "000000";
            when others => ToA <= "111111";
        end case;


    end  process;



    process(InputExpanded, ToA)
    begin

        if ToA = "111111" then ToT <= "100000";
        elsif InputExpanded(to_integer(unsigned(ToA)))='0' then ToT <= "000000";
        elsif InputExpanded(to_integer(unsigned(ToA))+1)='0' then ToT <= "000001";
        elsif InputExpanded(to_integer(unsigned(ToA))+2)='0' then ToT <= "000010";
        elsif InputExpanded(to_integer(unsigned(ToA))+3)='0' then ToT <= "000011";
        elsif InputExpanded(to_integer(unsigned(ToA))+4)='0' then ToT <= "000100";
        elsif InputExpanded(to_integer(unsigned(ToA))+5)='0' then ToT <= "000101";
        elsif InputExpanded(to_integer(unsigned(ToA))+6)='0' then ToT <= "000110";
        elsif InputExpanded(to_integer(unsigned(ToA))+7)='0' then ToT <= "000111";
        elsif InputExpanded(to_integer(unsigned(ToA))+8)='0' then ToT <= "001000";
        elsif InputExpanded(to_integer(unsigned(ToA))+9)='0' then ToT <= "001001";
        elsif InputExpanded(to_integer(unsigned(ToA))+10)='0' then ToT <= "001010";
        elsif InputExpanded(to_integer(unsigned(ToA))+11)='0' then ToT <= "001011";
        elsif InputExpanded(to_integer(unsigned(ToA))+12)='0' then ToT <= "001100";
        elsif InputExpanded(to_integer(unsigned(ToA))+13)='0' then ToT <= "001101";
        elsif InputExpanded(to_integer(unsigned(ToA))+14)='0' then ToT <= "001110";
        elsif InputExpanded(to_integer(unsigned(ToA))+15)='0' then ToT <= "001111";
        elsif InputExpanded(to_integer(unsigned(ToA))+16)='0' then ToT <= "010000";
        elsif InputExpanded(to_integer(unsigned(ToA))+17)='0' then ToT <= "010001";
        elsif InputExpanded(to_integer(unsigned(ToA))+18)='0' then ToT <= "010010";
        elsif InputExpanded(to_integer(unsigned(ToA))+19)='0' then ToT <= "010011";
        elsif InputExpanded(to_integer(unsigned(ToA))+20)='0' then ToT <= "010100";
        elsif InputExpanded(to_integer(unsigned(ToA))+21)='0' then ToT <= "010101";
        elsif InputExpanded(to_integer(unsigned(ToA))+22)='0' then ToT <= "010110";
        elsif InputExpanded(to_integer(unsigned(ToA))+23)='0' then ToT <= "010111";
        elsif InputExpanded(to_integer(unsigned(ToA))+24)='0' then ToT <= "011000";
        elsif InputExpanded(to_integer(unsigned(ToA))+25)='0' then ToT <= "011001";
        elsif InputExpanded(to_integer(unsigned(ToA))+26)='0' then ToT <= "011010";
        elsif InputExpanded(to_integer(unsigned(ToA))+27)='0' then ToT <= "011011";
        elsif InputExpanded(to_integer(unsigned(ToA))+28)='0' then ToT <= "011100";
        elsif InputExpanded(to_integer(unsigned(ToA))+29)='0' then ToT <= "011101";
        elsif InputExpanded(to_integer(unsigned(ToA))+30)='0' then ToT <= "011110";
        elsif InputExpanded(to_integer(unsigned(ToA))+31)='0' then ToT <= "011111";
        else ToT <= "100000";
        end if;
    end process;




end Behavioral;






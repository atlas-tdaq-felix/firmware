----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 08/25/2022 02:16:24 PM
-- Design Name:
-- Module Name: BCM_Output - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
    use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity BCM_Output is
    generic ( AVALIABLE_CLK_CYCLES: integer );
    Port ( ToT : in STD_LOGIC_VECTOR (5 downto 0);
        ToA : in STD_LOGIC_VECTOR (5 downto 0);
        BCID : in STD_LOGIC_VECTOR (11 downto 0);
        L1ID: in STD_LOGIC_VECTOR (31 downto 0);
        L1SubID: in STD_LOGIC_VECTOR (5 downto 0);
        reset : in STD_LOGIC;
        a_clk : in STD_LOGIC;
        Output : out STD_LOGIC_VECTOR (31 downto 0);
        DataValid : out STD_LOGIC;
        LastWord :  out STD_LOGIC;
        ForcePublish: in STD_LOGIC
    );

end BCM_Output;

architecture Behavioral of BCM_Output is

    type ToTToA_arr_type is array ((AVALIABLE_CLK_CYCLES - 1) downto 0) of STD_LOGIC_VECTOR (11 downto 0);

    signal m_L1ID: STD_LOGIC_VECTOR(31 downto 0);
    signal m_L1SubID: STD_LOGIC_VECTOR(5 downto 0);
    signal m_BCID: STD_LOGIC_VECTOR(11 downto 0);
    signal m_ForcePublish: STD_LOGIC;
    signal State: Integer range 0 to (AVALIABLE_CLK_CYCLES-1);
    signal FIFO: ToTToA_arr_type;

begin


    --State Machine
    process(a_clk)
    begin
        if rising_edge(a_clk) then

            if reset='1' then
                State <= (AVALIABLE_CLK_CYCLES-1);
                m_L1ID <= (others=> '0');
                m_L1SubID <= (others=> '0');
                m_BCID <= (others=> '0');
                m_ForcePublish <='0';
            elsif L1ID /= m_L1ID or m_L1SubID /= L1SubID then
                State <=0;
                m_L1ID <= L1ID;
                m_L1SubID <= L1SubID;
                m_BCID <= BCID;
                FIFO(0) <= ToT & ToA;
                if L1ID /=m_L1ID then
                    m_ForcePublish <= ForcePublish;
                else
                    m_ForcePublish <= '0';
                end if;

            elsif State < (AVALIABLE_CLK_CYCLES-1) then
                FIFO(State+1) <= ToT & ToA;
                State <= State + 1;
                m_ForcePublish <= '0';
            end if;
        end if;
    end process;

    process(a_clk)

    begin
        if rising_edge(a_clk) then -- There is no need for delay between the input block so rising edge works

            if reset='1' then
                Output <= (others=> '0');
                DataValid <= '0';
                LastWord <= '0';
            elsif State = 0 then
                Output <= m_L1ID;

                if (FIFO(State)(11 downto 6)="000000") then
                    if m_ForcePublish='1' then
                        LastWord <= '1';
                        DataValid<='1';
                    else
                        LastWord <= '0';
                        DataValid<='0';
                    end if;
                else
                    DataValid<='1';
                    LastWord <= '0';
                end if;
            else
                -- The ToA and ToT can be supressed into a 12 to 10 Bits
                -- comparison, currently not implemented

                -- Output reserved( 2bits ) &  L1SubID (6 Bits) & BCID (12 Bits) & ToT and ToA (12 Bits)
                if (FIFO(State-1)(5 downto 0) /= "111111" and FIFO(State-1)(11 downto 6) /= "000000") then
                    Output <= "11" & m_L1SubID & STD_LOGIC_VECTOR(m_BCID)  & FIFO(State-1);
                    DataValid<= '1';
                else
                    Output <= (others => '0' );
                    DataValid <= '0';
                end if;

                if ( State = (AVALIABLE_CLK_CYCLES-1) ) or (FIFO(State)(11 downto 6)="000000") then
                    LastWord<='1';
                else
                    LastWord<='0';
                end if;
            end if;
        end if;
    end process;



end Behavioral;

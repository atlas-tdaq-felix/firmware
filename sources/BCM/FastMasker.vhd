----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 01/27/2023 03:59:26 PM
-- Design Name:
-- Module Name: FastMasker - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned architecture
    use ieee.numeric_std_unsigned.all;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FastMasker is
    Port ( Input : in STD_LOGIC_VECTOR (31 downto 0);
        Output : out STD_LOGIC_VECTOR (31 downto 0));
end FastMasker;

architecture Behavioral of FastMasker is
begin

    process(Input)
    begin
        Output <= Input;
        for i in 0 to 31 loop
            if (Input(i) = '1') then
                if ( i = 31) then
                    Output <= ( others => '0');
                    exit;

                elsif( Input (i+1) = '0' ) then
                    Output( i downto  0) <= ( others => '0');
                    exit;
                end if;

            end if;
        end loop;
    end process;
end Behavioral;




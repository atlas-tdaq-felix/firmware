--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Weihao Wu
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.





library ieee, UNISIM, work;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity upstream_fanout_selector is
  generic(
    GBT_NUM                      : integer := 24;
    useToFrontendGBTdataEmulator : boolean := true;
    CREnableFromHost             : boolean := true);
  port (
    trig_tx_out             : out    std_logic_vector(3 downto 0);
    fei4_hitor              : in     std_logic_vector(3 downto 0);
    CRoutData_array         : in     txrx120b_type(0 to (GBT_NUM-1));
    GBTdata                 : in     std_logic_vector(119 downto 0);
    TX_120b_in              : out    txrx120b_type(0 to GBT_NUM-1);
    clk40                   : in     std_logic;
    register_map_40_control : in     register_map_control_type);
end entity upstream_fanout_selector;



architecture rtl of upstream_fanout_selector is

  signal s_rx_120b_out_f00  : txrx120b_type(0 to (GBT_NUM-1));
  signal s_rx_120b_out_f01  : txrx120b_type(0 to (GBT_NUM-1));
  signal s_frame_locked_f00 : std_logic_vector(GBT_NUM-1 downto 0);
  signal s_frame_locked_f01 : std_logic_vector(GBT_NUM-1 downto 0);


type txrx48b_type        is array (natural range <>) of std_logic_vector(47 downto 0);
type txrx2b_type           is array(natural range <>) of std_logic_vector(1 downto 0);
signal trig_tx             : std_logic_vector(3 downto 0);
signal trig_tx_r           : std_logic_vector(3 downto 0);
signal trig_tx_2r          : std_logic_vector(3 downto 0);

signal trig_tx_data        : txrx48b_type(0 to 3);
signal tx_48b              : txrx48b_type(0 to 3);
signal txb21               : txrx2b_type(0 to 3);
signal txb3                : std_logic;
--signal txb4                : std_logic;

signal fei4_trig_valid_link: std_logic_vector(3 downto 0);
signal fei4_trig_delay     : std_logic_vector(3 downto 0);
signal fei4_trig           : std_logic;
signal fei4_trig_valid     : std_logic;
signal fei4_trig_cmd       : std_logic;
signal fei4_cmd_fanout     : std_logic;
signal fei4_trig_cmd_vec   : std_logic_vector(79 downto 0);
signal fei4_trig_cmd_vec_r : std_logic_vector(79 downto 0);
signal fei4_trig_latency   : std_logic_vector(7 downto 0);
signal fei4_trig_latency_r : std_logic_vector(7 downto 0);
signal fei4_cal            : std_logic;
signal fei4_cal_r          : std_logic;
signal fei4_cal_2r         : std_logic;
signal vio_fei4_trig_valid : std_logic_vector(0 downto 0);
signal vio_fei4_trig_cmd   : std_logic_vector(0 downto 0);
signal vio_fei4_cmd_fanout : std_logic_vector(0 downto 0);
signal fei4_cal_num        : std_logic_vector(31 downto 0);
signal fei4_cal_cnt        : std_logic_vector(31 downto 0);
signal fei4_cal_interval   : std_logic_vector(31 downto 0);
signal fei4_cal_delay      : std_logic_vector(31 downto 0);
signal fei4_cal_auto_valid : std_logic;
signal fei4_cal_auto_valid_r: std_logic;
signal fei4_cal_auto_valid_2r: std_logic;
signal fei4_cal_auto       : std_logic;
signal vio_fei4_trig_cnt       : std_logic_vector(31 downto 0);
signal vio_fei4_trig_cnt2      : std_logic_vector(31 downto 0);
signal vio_fei4_cal_cnt        : std_logic_vector(31 downto 0);
signal vio_fei4_cal_cnt2       : std_logic_vector(31 downto 0);
signal vio_cnt_clr         : std_logic_Vector(0 downto 0);
signal cnt_clr             : std_logic;

component ila_hitor IS
PORT (
  clk : IN STD_LOGIC;
  probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
  probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
  probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
);
END component;

component vio_cmd_cnt IS
PORT (
  clk : IN STD_LOGIC;
  probe_in0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
  probe_in1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
  probe_in2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
  probe_in3 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
  probe_out0 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
);
END component;

begin


-- generate fei4 trigger pulse, after receiving hitors
process(clk40)
begin
if clk40'event and clk40='1' then
  fei4_trig_valid_link <= register_map_40_control.FEI4_TRIG_LINK(3 downto 0);
  if fei4_trig_delay = x"0" then
    if fei4_trig_valid_link = x"1" and fei4_hitor(0) = '1' then
      fei4_trig <= '1';
      fei4_trig_delay <= fei4_trig_delay + '1';
    elsif fei4_trig_valid_link = x"2" and fei4_hitor(1) = '1' then
      fei4_trig <= '1';
      fei4_trig_delay <= fei4_trig_delay + '1';
    elsif fei4_trig_valid_link = x"4" and fei4_hitor(2) = '1' then
      fei4_trig <= '1';
      fei4_trig_delay <= fei4_trig_delay + '1';
    elsif fei4_trig_valid_link = x"8" and fei4_hitor(3) = '1' then
      fei4_trig <= '1';
      fei4_trig_delay <= fei4_trig_delay + '1';
    else
      fei4_trig <= '0';
      fei4_trig_delay <= x"0";
    end if;
  elsif fei4_trig_delay = x"7" then
    fei4_trig <= '0';
    fei4_trig_delay <= x"0";
  else
    fei4_trig <= '0';
    fei4_trig_delay <= fei4_trig_delay + '1';
  end if;
end if;
end process;

process(clk40)
begin
if rising_edge(clk40) then
  if fei4_trig_latency = x"00" then
    fei4_trig_valid <= fei4_trig;
    fei4_trig_latency_r <= x"00";
  elsif fei4_trig_latency_r = fei4_trig_latency then
    fei4_trig_valid <= '1';
    fei4_trig_latency_r <= x"00";
  elsif fei4_trig = '1' then
    fei4_trig_latency_r <= fei4_trig_latency_r + '1';
    fei4_trig_valid <= '0';
  elsif fei4_trig_latency_r = x"00" then
    fei4_trig_valid <= '0';
    fei4_trig_latency_r <= x"00";
  else
    fei4_trig_valid <= '0';
    fei4_trig_latency_r <= fei4_trig_latency_r + '1';
  end if;
end if;
end process;

fei4_trig_gen_inst: entity work.fei4_fast_cmd_gen
  port map(
    RST     => '0',
    CLK     => clk40,

    CAL     => fei4_cal or fei4_cal_auto,
    TRG     => fei4_trig_valid,
    ECR     => '0',
    BCR     => '0',

    CMD_OUT => fei4_trig_cmd,
    BUSY    => open
);

fei4_fast_cmd_inst: for i in 0 to 79 generate
  process(clk40)
  begin
  if rising_edge(clk40) then
    fei4_trig_cmd_vec(i) <= fei4_trig_cmd;
  end if;
  end process;
end generate;

process(clk40)
begin
if rising_edge(clk40) then
  fei4_trig_cmd_vec_r <= fei4_trig_cmd_vec;
  fei4_cmd_fanout <= register_map_40_control.FEI4_CMD_FO_SEL;
  fei4_trig_latency <= register_map_40_control.FEI4_TRIG_LATENCY;

  fei4_cal_r <= register_map_40_control.FEI4_CALIBRATION;
  fei4_cal_2r <= fei4_cal_r;
  fei4_cal <= fei4_cal_r and (not fei4_cal_2r);
end if;
end process;

-- sending fei4 calibration command repeatedly
process(clk40)
begin
if rising_edge(clk40) then
  fei4_cal_num <= register_map_40_control.FEI4_CAL_NUM;
  fei4_cal_interval <= register_map_40_control.FEI4_CAL_INTERVAL;
  fei4_cal_auto_valid <= register_map_40_control.FEI4_CAL_AUTO_VALID;
  fei4_cal_auto_valid_r <= fei4_cal_auto_valid;
  fei4_cal_auto_valid_2r <= fei4_cal_auto_valid_r;

  if fei4_cal_auto_valid_r = '1' and fei4_cal_auto_valid_2r = '0' then
    fei4_cal_cnt <= (others => '0');
    fei4_cal_delay <= (others => '0');
    fei4_cal_auto <= '0';

  elsif fei4_cal_auto_valid_2r = '1' then
    if fei4_cal_cnt = fei4_cal_num then
      fei4_cal_cnt <= fei4_cal_cnt;
      fei4_cal_delay <= (others => '0');
      fei4_cal_auto <= '0';
    elsif fei4_cal_delay = fei4_cal_interval then
      fei4_cal_delay <= (others => '0');
      fei4_cal_auto <= '1';
      fei4_cal_cnt <= fei4_cal_cnt + '1';
    else
      fei4_cal_delay <= fei4_cal_delay + '1';
      fei4_cal_auto <= '0';
      fei4_cal_cnt <= fei4_cal_cnt;
    end if;
  else
    fei4_cal_delay <= (others => '0');
    fei4_cal_auto <= '0';
    fei4_cal_cnt <= (others => '0');
  end if;
end if;
end process;


---- TX slow control 4-bit data
process(clk40)
begin
if clk40'event and clk40='1' then
  txb3 <= not txb3;
  --txb4 <= not txb4;
  trig_tx <= register_map_40_control.LTDB_PREPROTO_CMDWR_TRIG(3 downto 0); --RDMON
  trig_tx_data(0) <= register_map_40_control.LTDB_PREPROTO_CMDWR_CH0(47 downto 0);
  trig_tx_data(1) <= register_map_40_control.LTDB_PREPROTO_CMDWR_CH1(47 downto 0);
  trig_tx_data(2) <= register_map_40_control.LTDB_PREPROTO_CMDWR_CH2(47 downto 0);
  trig_tx_data(3) <= register_map_40_control.LTDB_PREPROTO_CMDWR_CH3(47 downto 0);
end if;
end process;

u0_tx_gen: for i in 0 to 3 generate
  process(clk40)
  begin
  if clk40'event and clk40='1' then
    trig_tx_out(i) <= trig_tx(i);
    trig_tx_r(i) <= trig_tx(i);
    trig_tx_2r(i) <=trig_tx_r(i);
    if trig_tx_r(i)='1' and trig_tx_2r(i)='0' then
      tx_48b(i) <= trig_tx_data(i);
    else
      tx_48b(i) <= tx_48b(i)(45 downto 0) & "11";
    end if;
  txb21(i) <= tx_48b(i)(47 downto 46);
  end if;
  end process;
end generate;

-- monitor fei4 trigger command
vio_fei4_trig_valid(0) <= fei4_trig_valid;
vio_fei4_trig_cmd(0)   <= fei4_trig_cmd;
vio_fei4_cmd_fanout(0) <= fei4_cmd_fanout;

ila_hitor_inst: ila_hitor
port map (
  clk     => clk40,
  probe0  => vio_fei4_trig_valid,
  probe1  => vio_fei4_trig_cmd,
  probe2  => vio_fei4_cmd_fanout
);

-- fei4 trigger and calibration command counter and monitor
cnt_clr <= vio_cnt_clr(0);
process(clk40, cnt_clr)
begin
if cnt_clr = '1' then
  vio_fei4_trig_cnt <= (others => '0');
  vio_fei4_trig_cnt2 <= (others => '0');
  vio_fei4_cal_cnt <= (others => '0');
  vio_fei4_cal_cnt2 <= (others => '0');
elsif rising_edge(clk40) then
  if fei4_trig = '1' then
    vio_fei4_trig_cnt <= vio_fei4_trig_cnt + '1';
  end if;
  if fei4_trig_valid = '1' then
    vio_fei4_trig_cnt2 <= vio_fei4_trig_cnt2 + '1';
  end if;
  if fei4_cal_auto = '1' then
    vio_fei4_cal_cnt <= vio_fei4_cal_cnt + '1';
  end if;
  if fei4_cal = '1' then
    vio_fei4_cal_cnt2 <= vio_fei4_cal_cnt2 + '1';
  end if;
end if;
end process;

vio_cmd_cnt_inst: vio_cmd_cnt
port map (
  clk        => clk40,
  probe_in0  => vio_fei4_trig_cnt,
  probe_in1  => vio_fei4_trig_cnt2,
  probe_in2  => vio_fei4_cal_cnt,
  probe_in3  => vio_fei4_cal_cnt2,
  probe_out0 => vio_cnt_clr
);

--------------------------------------------------------------------------------
g_emu: if useToFrontendGBTdataEmulator = true generate
 g_CrFromHost: if CREnableFromHost = true generate
    --Generate fanout selector
      g_GBT_emulator_fanout : for i in 0 to (GBT_NUM-1) generate
        GBT_tx_sync : process (clk40)
        begin  -- process
          if rising_edge (clk40) then
            if register_map_40_control.GBT_UPLNK_FO_SEL(i) = '1' then
              TX_120b_in(i)      <= GBTdata;
            elsif fei4_cmd_fanout = '1' then
              TX_120b_in(i)      <= CRoutData_array(i)(119 downto 116) & '0' & txb3 & txb21(i) & fei4_trig_cmd_vec_r & CRoutData_array(i)(31 downto 0);
            else
              TX_120b_in(i)      <= CRoutData_array(i)(119 downto 116) & '0' & txb3 & txb21(i) & CRoutData_array(i)(111 downto 0);
            end if;
          end if;
        end process;
      end generate;
  end generate;
  --Connect only emu
  g_NCrFromHost: if CREnableFromHost = false generate
      g_GBT_emulator_fanout : for i in 0 to (GBT_NUM-1) generate
          TX_120b_in(i)      <= GBTdata;
      end generate;
  end generate;
end generate;

--Connect only CentralRouter
g_Nemu: if useToFrontendGBTdataEmulator = false generate
      TX_120b_in      <= CRoutData_array;
end generate;



end architecture rtl ; -- of upstream_fanout_selector


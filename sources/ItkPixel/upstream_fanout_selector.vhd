--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Weihao Wu
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.





library ieee, UNISIM, work;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity upstream_fanout_selector is
  generic(
    GBT_NUM                      : integer := 24;
    useToFrontendGBTdataEmulator : boolean := true;
    CREnableFromHost             : boolean := true);
  port (
    trig_tx_out             : out    std_logic_vector(3 downto 0);
    fei4_hitor              : in     std_logic_vector(3 downto 0);
    CRoutData_array         : in     txrx120b_type(0 to (GBT_NUM-1));
    GBTdata                 : in     std_logic_vector(119 downto 0);
    TX_120b_in              : out    txrx120b_type(0 to GBT_NUM-1);
    clk40                   : in     std_logic;
    register_map_40_control : in     register_map_control_type);
end entity upstream_fanout_selector;



architecture rtl of upstream_fanout_selector is

  signal s_rx_120b_out_f00  : txrx120b_type(0 to (GBT_NUM-1));
  signal s_rx_120b_out_f01  : txrx120b_type(0 to (GBT_NUM-1));
  signal s_frame_locked_f00 : std_logic_vector(GBT_NUM-1 downto 0);
  signal s_frame_locked_f01 : std_logic_vector(GBT_NUM-1 downto 0);


type txrx48b_type        is array (natural range <>) of std_logic_vector(47 downto 0);
type txrx2b_type           is array(natural range <>) of std_logic_vector(1 downto 0);
signal trig_tx             : std_logic_vector(3 downto 0);
signal trig_tx_r           : std_logic_vector(3 downto 0);
signal trig_tx_2r          : std_logic_vector(3 downto 0);

signal trig_tx_data        : txrx48b_type(0 to 3);
signal tx_48b              : txrx48b_type(0 to 3);
signal txb21               : txrx2b_type(0 to 3);
signal txb3                : std_logic;
--signal txb4                : std_logic;

signal fei4_trig_valid_link: std_logic_vector(3 downto 0);
signal fei4_trig_delay     : std_logic_vector(3 downto 0);
signal fei4_trig           : std_logic;
signal fei4_trig_valid     : std_logic;
signal fei4_trig_cmd       : std_logic;
signal fei4_cmd_fanout     : std_logic;
signal fei4_trig_cmd_vec   : std_logic_vector(79 downto 0);
signal fei4_trig_cmd_vec_r : std_logic_vector(79 downto 0);
signal fei4_trig_latency   : std_logic_vector(7 downto 0);
signal fei4_trig_latency_r : std_logic_vector(7 downto 0);
signal fei4_cal            : std_logic;
signal fei4_cal_r          : std_logic;
signal fei4_cal_2r         : std_logic;
signal vio_fei4_trig_valid : std_logic_vector(0 downto 0);
signal vio_fei4_trig_cmd   : std_logic_vector(0 downto 0);
signal vio_fei4_cmd_fanout : std_logic_vector(0 downto 0);
signal fei4_cal_num        : std_logic_vector(31 downto 0);
signal fei4_cal_cnt        : std_logic_vector(31 downto 0);
signal fei4_cal_interval   : std_logic_vector(31 downto 0);
signal fei4_cal_delay      : std_logic_vector(31 downto 0);
signal fei4_cal_auto_valid : std_logic;
signal fei4_cal_auto_valid_r: std_logic;
signal fei4_cal_auto_valid_2r: std_logic;
signal fei4_cal_auto       : std_logic;
signal vio_fei4_trig_cnt       : std_logic_vector(31 downto 0);
signal vio_fei4_trig_cnt2      : std_logic_vector(31 downto 0);
signal vio_fei4_cal_cnt        : std_logic_vector(31 downto 0);
signal vio_fei4_cal_cnt2       : std_logic_vector(31 downto 0);
signal vio_cnt_clr         : std_logic_Vector(0 downto 0);
signal cnt_clr             : std_logic;


component vio_fei4_cmd_bitshift_ctrl IS
PORT (
clk : IN STD_LOGIC;
probe_out0 : OUT STD_LOGIC_VECTOR(1 DOWNTO 0)
);
END component;

signal fei4_cmd_4b       : std_logic_vector(3 downto 0);
signal fei4_cmd_4b_r     : std_logic_vector(3 downto 0);
signal fei4_cmd_4b_valid : std_logic_vector(3 downto 0);
signal fei4_cmd_bitshfit : std_logic_Vector(1 downto 0);
signal CRoutData_array_r   : txrx120b_type(0 to (GBT_NUM-1));
signal CRoutData_array_2r   : txrx120b_type(0 to (GBT_NUM-1));
signal CRoutData_array_3r   : txrx120b_type(0 to (GBT_NUM-1));

begin


vio_fei4_cmd_bitshift_ctrl_inst: vio_fei4_cmd_bitshift_ctrl
port map (
  clk           => clk40,
  probe_out0    => fei4_cmd_bitshfit
);

gbt_links_manchester: for i in 0 to (GBT_NUM-1) generate
  elinks_manchester: for j in 0 to 19 generate
    process(clk40)
    begin
    if rising_edge (clk40) then   
      CRoutData_array_r(i)(j*4+35 downto j*4+32) <= CRoutData_array(i)(j*4+35 downto j*4+34) & (not CRoutData_array(i)(j*4+33)) & (not CRoutData_array(i)(j*4+32));

      if fei4_cmd_bitshfit = "00" then
        CRoutData_array_3r(i)(j*4+35 downto j*4+32) <= CRoutData_array_2r(i)(j*4+35 downto j*4+32);
      elsif fei4_cmd_bitshfit = "01" then
        CRoutData_array_3r(i)(j*4+35 downto j*4+32) <= CRoutData_array_2r(i)(j*4+34 downto j*4+32) & CRoutData_array_r(i)(j*4+35);
      elsif fei4_cmd_bitshfit = "10" then
        CRoutData_array_3r(i)(j*4+35 downto j*4+32) <= CRoutData_array_2r(i)(j*4+33 downto j*4+32) & CRoutData_array_r(i)(j*4+35 downto j*4+34);
      elsif fei4_cmd_bitshfit = "11" then
        CRoutData_array_3r(i)(j*4+35 downto j*4+32) <= CRoutData_array_2r(i)(j*4+32) & CRoutData_array_r(i)(j*4+35 downto j*4+33);
      end if;
      
    end if;
    end process;
  end generate  elinks_manchester;
  
  process(clk40)
  begin
  if rising_edge (clk40) then   
    CRoutData_array_r(i)(31 downto 0) <= CRoutData_array(i)(31 downto 0);
    CRoutData_array_r(i)(119 downto 112) <= CRoutData_array(i)(119 downto 112);
    
    CRoutData_array_3r(i)(31 downto 0) <= CRoutData_array_2r(i)(31 downto 0);
    CRoutData_array_3r(i)(119 downto 112) <= CRoutData_array_2r(i)(119 downto 112);
  end if;
  end process;
  
  process(clk40)
  begin
  if rising_edge (clk40) then   
    CRoutData_array_2r(i) <= CRoutData_array_r(i);
  end if;
  end process;
  
end generate  gbt_links_manchester;

g_emu: if useToFrontendGBTdataEmulator = true generate
 g_CrFromHost: if CREnableFromHost = true generate
    --Generate fanout selector
      g_GBT_emulator_fanout : for i in 0 to (GBT_NUM-1) generate
        GBT_tx_sync : process (clk40)
        begin  -- process
          if rising_edge (clk40) then
            if register_map_40_control.GBT_UPLNK_FO_SEL(i) = '1' then
              TX_120b_in(i)      <= GBTdata;
--            elsif fei4_cmd_fanout = '1' then
--              TX_120b_in(i)      <= CRoutData_array(i)(119 downto 116) & '0' & txb3 & txb21(i) & fei4_trig_cmd_vec_r & CRoutData_array(i)(31 downto 0);
            else
              TX_120b_in(i)      <= CRoutData_array_3r(i)(119 downto 0); --116) & '0' & txb3 & txb21(i) & CRoutData_array(i)(111 downto 0);
            end if;
          end if;
        end process;
      end generate;
  end generate;
  --Connect only emu
  g_NCrFromHost: if CREnableFromHost = false generate
      g_GBT_emulator_fanout : for i in 0 to (GBT_NUM-1) generate
          TX_120b_in(i)      <= GBTdata;
      end generate;
  end generate;
end generate;

--Connect only CentralRouter
g_Nemu: if useToFrontendGBTdataEmulator = false generate
      TX_120b_in      <= CRoutData_array;
end generate;



end architecture rtl ; -- of upstream_fanout_selector


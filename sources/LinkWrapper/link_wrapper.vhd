--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Alessandro Palombi
--!               mtrovato
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.


library ieee;
    use ieee.std_logic_1164.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;
    use work.interlaken_package.all;
    use work.axi_stream_package.all;
library unisim;
    use unisim.vcomponents.all;
library xpm;
    use xpm.vcomponents.all;

entity link_wrapper is
    generic(
        LINK_NUM                        : integer := 8; -- number of GBT channels
        CARD_TYPE                       : integer := 182;
        GTHREFCLK_SEL                   : std_logic := '0'; -- GREFCLK: '1', MGTREFCLK: '0'
        FIRMWARE_MODE                   : integer := 0;
        FULL_HALFRATE                   : boolean := false;
        PLL_SEL                         : std_logic := '1'; -- 0: CPLL, 1: QPLL
        GTREFCLKS                       : integer := 2;
        GTREFCLK1S        : integer   := 2;
        KCU_LOWER_LATENCY               : integer := 0
    );
    port (
        register_map_control : in register_map_control_type;
        register_map_link_monitor : out register_map_link_monitor_type;
        clk40 : in std_logic;
        clk100: in std_logic;
        --clk240 : in std_logic;
        apb3_axi_clk : in std_logic;
        --clk320 : in std_logic;
        clk40_xtal : in std_logic;
        GTREFCLK_N_in : in std_logic_vector(GTREFCLKS-1 downto 0);
        GTREFCLK_P_in : in std_logic_vector(GTREFCLKS-1 downto 0);
        GTREFCLK1_N_in            : in  std_logic_vector(GTREFCLK1S - 1 downto 0);
        GTREFCLK1_P_in            : in  std_logic_vector(GTREFCLK1S - 1 downto 0);
        rst_hw : in  std_logic;
        --OPTO_LOS : in std_logic_vector(NUM_OPTO_LOS(CARD_TYPE)-1 downto 0);
        TXUSRCLK_OUT              : out std_logic_vector(LINK_NUM - 1 downto 0);
        GT_REFCLK_OUT :out std_logic_vector(LINK_NUM - 1 downto 0);
        RXUSRCLK_OUT : out std_logic_vector(LINK_NUM-1 downto 0);
        GBT_DOWNLINK_USER_DATA : in array_120b(0 to (LINK_NUM-1));
        GBT_UPLINK_USER_DATA : out array_120b(0 to (LINK_NUM-1));
        lpGBT_DOWNLINK_USER_DATA : in array_32b(0 to LINK_NUM-1);
        lpGBT_DOWNLINK_IC_DATA   : in array_2b(0 to LINK_NUM-1);
        lpGBT_DOWNLINK_EC_DATA   : in array_2b(0 to LINK_NUM-1);
        lpGBT_UPLINK_USER_DATA   : out array_224b(0 to LINK_NUM-1);
        lpGBT_UPLINK_EC_DATA     : out array_2b(0 to LINK_NUM-1);
        lpGBT_UPLINK_IC_DATA     : out array_2b(0 to LINK_NUM-1);
        LinkAligned : out std_logic_vector(LINK_NUM-1 downto 0);
        TX_P : out std_logic_vector(LINK_NUM-1 downto 0);
        TX_N : out std_logic_vector(LINK_NUM-1 downto 0);
        RX_P : in std_logic_vector(LINK_NUM-1 downto 0);
        RX_N : in std_logic_vector(LINK_NUM-1 downto 0);
        GTH_FM_RX_33b_out : out array_33b(0 to LINK_NUM-1);
        LMK_P : in std_logic_vector(NUM_LMK(CARD_TYPE)*6-1 downto 0);
        LMK_N : in std_logic_vector(NUM_LMK(CARD_TYPE)*6-1 downto 0);
        --For Interlaken interface
        LTI_TX_Data_Transceiver_In  : in array_32b(0 to LINK_NUM - 1);
        Interlaken_Data_Transceiver_Out : out slv_67_array(0 to LINK_NUM - 1);
        Interlaken_RX_Datavalid_Out     : out std_logic_vector(LINK_NUM - 1 downto 0);
        Interlaken_RX_Gearboxslip       : in std_logic_vector(LINK_NUM - 1 downto 0);
        Interlaken_Decoder_Aligned_in   : in std_logic_vector(LINK_NUM - 1 downto 0);
        LTI_TX_TX_CharIsK_in            : in  array_4b(0 to LINK_NUM-1);
        FLX182_LTI_GTREFCLK1_out        : out std_logic
    );
end entity link_wrapper;


architecture rtl of link_wrapper is

begin

    --For Interlaken, GTREFCLK1 is used in the transceiver, for all other flavours (GBT / LPGBT / FULL) REFCLK1 buffer is instantiated here and forwarded to LTI.
    g_FLX182_Not_Interlaken: if (CARD_TYPE = 182 or CARD_TYPE = 182) and FIRMWARE_MODE /= FIRMWARE_MODE_INTERLAKEN generate
        IBUFDS_GTE5_REF1 : IBUFDS_GTE5
            generic map(
                REFCLK_EN_TX_PATH  => '0',
                REFCLK_HROW_CK_SEL => 0,
                REFCLK_ICNTL_RX    => 0
            )
            port map(
                O     => FLX182_LTI_GTREFCLK1_out,
                ODIV2 => open,
                CEB   => '0',
                I     => GTREFCLK1_P_in(0),
                IB    => GTREFCLK1_N_in(0)
            );
    end generate;


    g_GBTMODE: if FIRMWARE_MODE = FIRMWARE_MODE_GBT or
                FIRMWARE_MODE = FIRMWARE_MODE_FEI4 or
                FIRMWARE_MODE = FIRMWARE_MODE_LTDB generate
        signal TX_FRAME_CLK_I : std_logic_vector(LINK_NUM-1 downto 0);

    begin
        u0: for i in 0 to LINK_NUM-1 generate
        begin
            TX_FRAME_CLK_I(i) <= clk40;
        end generate u0;
        g_V7_KU: if CARD_TYPE = 709 or CARD_TYPE = 710 or CARD_TYPE = 711 or CARD_TYPE = 712 generate
            u2: entity work.FELIX_gbt_wrapper
                generic map(
                    CARD_TYPE => CARD_TYPE,
                    GBT_NUM             => LINK_NUM,
                    GTHREFCLK_SEL       => GTHREFCLK_SEL,
                    PLL_SEL => PLL_SEL,
                    GTREFCLKS => GTREFCLKS)
                port map(
                    rst_hw => rst_hw,
                    register_map_control => register_map_control,
                    register_map_link_monitor => register_map_link_monitor,
                    GTREFCLK_N_IN => GTREFCLK_N_in,
                    GTREFCLK_P_IN => GTREFCLK_P_in,
                    clk40_in => clk40,
                    TX_120b_in                => GBT_DOWNLINK_USER_DATA,
                    RX_120b_out               => GBT_UPLINK_USER_DATA,
                    FRAME_LOCKED_O            => LinkAligned,
                    TX_FRAME_CLK_I            => TX_FRAME_CLK_I,
                    TX_P                      => TX_P,
                    TX_N                      => TX_N,
                    RX_P                      => RX_P,
                    RX_N                      => RX_N,
                    RXUSRCLK_OUT              => RXUSRCLK_OUT);
        end generate g_V7_KU;
        g_versal: if CARD_TYPE = 180 or CARD_TYPE = 181 or CARD_TYPE = 182 or CARD_TYPE = 155 generate
            u2: entity work.FELIX_gbt_wrapper_Versal
                generic map(
                    GBT_NUM               => LINK_NUM)
                port map(
                    RX_FLAG_O => open,
                    TX_FLAG_O => open,
                    rst_hw => rst_hw,
                    register_map_control => register_map_control,
                    register_map_link_monitor => register_map_link_monitor,
                    GTREFCLK_N_IN => GTREFCLK_N_in,
                    GTREFCLK_P_IN => GTREFCLK_P_in,
                    clk40_in => clk40,
                    apb3_axi_clk => apb3_axi_clk,
                    TX_120b_in => GBT_DOWNLINK_USER_DATA,
                    RX_120b_out => GBT_UPLINK_USER_DATA,
                    FRAME_LOCKED_O => LinkAligned,
                    TX_FRAME_CLK_I => TX_FRAME_CLK_I,
                    TX_P => TX_P,
                    TX_N => TX_N,
                    RX_P => RX_P,
                    RX_N => RX_N,
                    RXUSRCLK_OUT => RXUSRCLK_OUT);
        end generate;
        g_128: if CARD_TYPE = 128 generate
            u2: entity work.FELIX_gbt_wrapper_VUP
                generic map(
                    --STABLE_CLOCK_PERIOD   => 24,
                    GBT_NUM               => LINK_NUM,
                    GTHREFCLK_SEL         => GTHREFCLK_SEL,
                    --CARD_TYPE             => CARD_TYPE,
                    PLL_SEL               => PLL_SEL)
                port map(
                    RX_FLAG_O                 => open,
                    TX_FLAG_O                 => open,
                    --REFCLK_CXP1               => open,
                    --REFCLK_CXP2               => open,
                    rst_hw => rst_hw,
                    register_map_control => register_map_control,
                    register_map_link_monitor => register_map_link_monitor,
                    --DRP_CLK_IN                => clk40_xtal,
                    GTREFCLK_N_IN => GTREFCLK_N_in(5 downto 0),
                    GTREFCLK_P_IN => GTREFCLK_P_in(5 downto 0),
                    --GREFCLK_IN                => clk240,
                    clk40_in => clk40,
                    --clk240_in => clk240,
                    TX_120b_in                => GBT_DOWNLINK_USER_DATA,
                    RX_120b_out               => GBT_UPLINK_USER_DATA,
                    FRAME_LOCKED_O            => LinkAligned,
                    TX_FRAME_CLK_I            => TX_FRAME_CLK_I,
                    TX_P                      => TX_P,
                    TX_N                      => TX_N,
                    RX_P                      => RX_P,
                    RX_N                      => RX_N,
                    RXUSRCLK_OUT              => RXUSRCLK_OUT);
        end generate;
    end generate g_GBTMODE;

    g_LPGBTMODE: if FIRMWARE_MODE = FIRMWARE_MODE_LPGBT or
                    FIRMWARE_MODE = FIRMWARE_MODE_PIXEL or
                    FIRMWARE_MODE = FIRMWARE_MODE_BCM_PRIME or
                    FIRMWARE_MODE = FIRMWARE_MODE_STRIP or
                    FIRMWARE_MODE = FIRMWARE_MODE_HGTD_LUMI generate

    begin

        --
        u2: entity work.FELIX_LpGBT_Wrapper
            generic map(
                CARD_TYPE     => CARD_TYPE,
                GBT_NUM       => LINK_NUM,
                GTREFCLKS => GTREFCLKS, --temporarily frozen to 5 for RefClk_Gen instead of GREFCLKS
                --FLX712 only
                CLK_CHIP_SEL => 0,
                KCU_LOWER_LATENCY => KCU_LOWER_LATENCY
            )
            port map(
                rst_hw                    => rst_hw,
                register_map_control      => register_map_control,
                register_map_link_monitor => register_map_link_monitor,
                GTREFCLK_N_IN             => GTREFCLK_N_in, --VC709 will only use (0)
                GTREFCLK_P_IN             => GTREFCLK_P_in,
                CLK40_IN                  => clk40,
                clk100_in                 => clk100,
                LMK_P                     => LMK_P,
                LMK_N                     => LMK_N,

                FELIX_DOWNLINK_USER_DATA  => lpGBT_DOWNLINK_USER_DATA,
                FELIX_DOWNLINK_IC_DATA    =>   lpGBT_DOWNLINK_IC_DATA,
                FELIX_DOWNLINK_EC_DATA    =>   lpGBT_DOWNLINK_EC_DATA,
                FELIX_UPLINK_USER_DATA    =>   lpGBT_UPLINK_USER_DATA,
                FELIX_UPLINK_EC_DATA      =>     lpGBT_UPLINK_EC_DATA,
                FELIX_UPLINK_IC_DATA      =>     lpGBT_UPLINK_IC_DATA,
                LinkAligned               => LinkAligned,
                TX_P                      => TX_P,
                TX_N                      => TX_N,
                RX_P                      => RX_P,
                RX_N                      => RX_N,
                RXUSRCLK_OUT              => RXUSRCLK_OUT,
                TXUSRCLK_OUT              => TXUSRCLK_OUT,

                GT_REFCLK_OUT => GT_REFCLK_OUT
            );
    end generate; --g_LPGBTMODE

    g_FULLMODE: if FIRMWARE_MODE = FIRMWARE_MODE_FULL generate
        signal TX_DATA_33b:array_33b(0 to LINK_NUM-1);
    begin
        g_TX_DATA: for i in 0 to LINK_NUM-1 generate
            TX_DATA_33b(i) <= LTI_TX_TX_CharIsK_in(i)(0) & LTI_TX_Data_Transceiver_In(i);
        end generate g_TX_DATA;
        u2: entity work.FELIX_FM_gbt_wrapper
            generic map(
                GBT_NUM => LINK_NUM,
                CARD_TYPE => CARD_TYPE,
                FULL_HALFRATE => FULL_HALFRATE,
                GTREFCLKS => GTREFCLKS,
                FIRMWARE_MODE => FIRMWARE_MODE
            )
            port map(
                RX_FLAG_O => open,
                TX_FLAG_O => open,
                REFCLK_CXP1 => open,
                REFCLK_CXP2 => open,
                rst_hw => rst_hw,
                register_map_control => register_map_control,
                register_map_link_monitor => register_map_link_monitor,
                DRP_CLK_IN => clk40_xtal,
                GTREFCLK_N_IN => GTREFCLK_N_in,
                GTREFCLK_P_IN => GTREFCLK_P_in,
                clk40_in => clk40,
                clk100_in => clk100,
                TX_120b_in => GBT_DOWNLINK_USER_DATA,
                RXUSERCLK_OUT => RXUSRCLK_OUT,
                TXUSERCLK_OUT => TXUSRCLK_OUT,
                RX_DATA_33b => GTH_FM_RX_33b_out,
                RX_DATA_33b_rdy => LinkAligned,
                TX_DATA_33b => TX_DATA_33b,
                TX_FRAME_CLK_I => clk40,
                TX_P => TX_P,
                TX_N => TX_N,
                RX_P => RX_P,
                RX_N => RX_N);
    end generate; --g_FULLMODE


    g_INTERLAKEN : if FIRMWARE_MODE = FIRMWARE_MODE_INTERLAKEN generate

        --signal rx_usr_clk_quads, tx_usr_clk_quads : std_logic_vector(LINK_NUM / 4 - 1 downto 0);
        signal rxresetdone_quads: std_logic_vector(LINK_NUM / 4 - 1 downto 0);
        signal txresetdone_quads: std_logic_vector(LINK_NUM / 4 - 1 downto 0);
        signal lcplllock_quads: std_logic_vector(LINK_NUM / 4 - 1 downto 0);
        signal rplllock_quads: std_logic_vector(LINK_NUM / 4 - 1 downto 0);
        signal gty_reset: std_logic;
        signal Interlaken_Decoder_Aligned_in_40: std_logic_vector(LINK_NUM-1 downto 0);
        signal rst_tx_datapath_in, rst_rx_datapath_in: std_logic_vector(LINK_NUM / 4 -1 downto 0);
    begin

        gty_reset <= rst_hw or register_map_control.GBT_RX_RESET(0);

        g_lanereset: for i in 0 to LINK_NUM/4-1 generate
            rst_tx_datapath_in(i) <= register_map_control.GBT_GTTX_RESET(i*4);
            rst_rx_datapath_in(i) <= register_map_control.GBT_GTRX_RESET(i*4);
        end generate;


        --For now we don't use LINK_CONFIG for combinations of links.
        il0 : entity work.interlaken_gty
            generic map(
                Lanes             => LINK_NUM,
                BondNumberOfLanes => 1,
                CARD_TYPE         => CARD_TYPE,
                GTREFCLKS         => GTREFCLKS,
                GTREFCLK1S        => GTREFCLK1S
            )
            port map(
                reset                => gty_reset,
                rst_tx_pll_and_datapath_in => register_map_control.GBT_PLL_RESET.QPLL_RESET(LINK_NUM/4-1+48 downto 0+48),
                rst_tx_datapath_in => rst_tx_datapath_in,
                rst_rx_pll_and_datapath_in => register_map_control.GBT_PLL_RESET.QPLL_RESET(LINK_NUM/4-1+48 downto 0+48),
                rst_rx_datapath_in => rst_rx_datapath_in,
                rst_txusr_403M_s     => open,
                rst_rxusr_403M_s     => open,
                GTREFCLK_IN_P        => GTREFCLK_P_in,
                GTREFCLK_IN_N        => GTREFCLK_N_in,
                GTREFCLK1_IN_P       => GTREFCLK1_P_in,
                GTREFCLK1_IN_N       => GTREFCLK1_N_in,
                clk100               => clk100,
                TX_Out_P             => TX_P,
                TX_Out_N             => TX_N,
                RX_In_P              => RX_P,
                RX_In_N              => RX_N,
                TX_User_Clock_s      => TXUSRCLK_OUT,
                RX_User_Clock_s      => RXUSRCLK_OUT,
                loopback_in          => register_map_control.GTH_LOOPBACK_CONTROL,
                Data_Transceiver_In  => LTI_TX_Data_Transceiver_In,
                Interlaken_Data_Transceiver_Out => Interlaken_Data_Transceiver_Out,
                RX_Datavalid_Out     => Interlaken_RX_Datavalid_Out,
                RX_Gearboxslip_In    => Interlaken_RX_Gearboxslip,
                TX_CharIsK_in        => LTI_TX_TX_CharIsK_in,
                rxresetdone_out      => rxresetdone_quads,
                txresetdone_out      => txresetdone_quads,
                lcplllock_out        => lcplllock_quads,
                rplllock_out         => rplllock_quads,
                quad0_gtrefclk1_out  => FLX182_LTI_GTREFCLK1_out
            );

        g_assign_monitor_regs: for i in 0 to LINK_NUM / 4 - 1 generate


            register_map_link_monitor.GBT_RXRESET_DONE(i*4+3 downto i*4) <= (others => rxresetdone_quads(i));
            register_map_link_monitor.GBT_TXRESET_DONE(i*4+3 downto i*4) <= (others => txresetdone_quads(i));
            register_map_link_monitor.GBT_PLL_LOCK.CPLL_LOCK(i*4+3 downto i*4) <= (others => lcplllock_quads(i));
            register_map_link_monitor.GBT_PLL_LOCK.QPLL_LOCK(48+i) <= rplllock_quads(i);
            register_map_link_monitor.GBT_ALIGNMENT_DONE(i*4+3 downto i*4) <= Interlaken_Decoder_Aligned_in(i*4+3 downto i*4);
        end generate;

        cdc_decoder_aligned: xpm_cdc_array_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0,
                WIDTH => LINK_NUM
            )
            port map (
                dest_out => Interlaken_Decoder_Aligned_in_40,
                dest_clk => clk40,
                src_clk => '0',
                src_in => Interlaken_Decoder_Aligned_in
            );

        pll_lock_latch_proc: process(clk40)
        begin
            if rising_edge(clk40) then
                if register_map_control.GBT_PLL_LOL_LATCHED.CLEAR = "1" then
                    register_map_link_monitor.GBT_PLL_LOL_LATCHED.CPLL_LOL_LATCHED <= (others => '0');
                    register_map_link_monitor.GBT_PLL_LOL_LATCHED.QPLL_LOL_LATCHED <= (others => '0');
                end if;
                for i in 0 to LINK_NUM-1 loop
                    if lcplllock_quads(i/4) = '0' then
                        register_map_link_monitor.GBT_PLL_LOL_LATCHED.CPLL_LOL_LATCHED(i) <= '1';
                    end if;
                end loop;
                for i in 0 to LINK_NUM/4 - 1 loop
                    if rplllock_quads(i) = '0' then
                        register_map_link_monitor.GBT_PLL_LOL_LATCHED.QPLL_LOL_LATCHED(i+48) <= '1';
                    end if;
                end loop;

                if register_map_control.GBT_ALIGNMENT_LOST.CLEAR = "1" then
                    register_map_link_monitor.GBT_ALIGNMENT_LOST.ALIGNMENT_LOST <= (others => '0');
                end if;
                for i in 0 to LINK_NUM-1 loop
                    if Interlaken_Decoder_Aligned_in_40(i) = '0' then
                        register_map_link_monitor.GBT_ALIGNMENT_LOST.ALIGNMENT_LOST(i) <= '1';
                    end if;
                end loop;
            end if;
        end process;


    end generate;

end architecture rtl ; -- of link_wrapper


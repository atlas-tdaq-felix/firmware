--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  BNL
--! Engineer: Elena Zhivun <ezhivun@bnl.gov>
--!
-- Created     : Sun May 17 16:31:36 2020
-- Last update : Thu May 21 18:44:04 2020
--!
--! Module Name:    r3l1_scheduler_encoder
--! Project Name:   FELIX
--!
--! Selects which frame is to be sent to the HCC* next, and encodes it in 6b8b.
--! Outputs:
--!   1. R3 frame if valid
--!    2. L1 frame if valid
--!   3. Bypass frame if available
--!   4. Idle frame if none above is available
--!
----------------------------------------------------------------------------------

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use work.strips_package.all;

entity r3l1_scheduler_encoder is
    port(
        clk                      : in  std_logic; -- 40 MHz BC clk domain
        rst                      : in  std_logic; -- synchronous reset
        r3_frame_i              : in  STD_LOGIC_VECTOR(11 downto 0); -- R3 frame FWFT fifo data
        r3_frame_o_rd_en        : out std_logic; -- R3 frame FIFO rd_en signal
        r3_frame_i_empty        : in  std_logic; -- R3 frame FIFO empty signal
        r3_frame_i_almost_empty : in  std_logic; -- R3 frame FIFO almost_empty signal
        l1_frame_i              : in  STD_LOGIC_VECTOR(11 downto 0); -- L1 frame FWFT fifo data
        l1_frame_o_rd_en        : out std_logic; -- L1 frame FIFO rd_en signal
        l1_frame_i_empty        : in  std_logic; -- L1 frame FIFO empty signal
        l1_frame_i_almost_empty : in  std_logic; -- L1 frame FIFO almost_empty signal
        bypass_frame_i       : in std_logic_vector(15 downto 0); -- bypass FromHost frame (already 6b8b encoded)
        bypass_frame_valid_i   : in std_logic; -- bypass frame is valid this clk cycle
        bypass_frame_ready_o   : out std_logic; -- request next bypass frame
        frame_start_pulse_i      : in  std_logic; -- request next frame (40 MHz BC clk domain)
        encoded_frame_o          : out std_logic_vector(15 downto 0) := x"7855"
    );
end entity r3l1_scheduler_encoder;

architecture behavioral of r3l1_scheduler_encoder is
    constant K0 : std_logic_vector(7 downto 0) := x"78";
    constant K1 : std_logic_vector(7 downto 0) := x"55";
    --constant K2 : std_logic_vector(7 downto 0) := x"47";
    --constant K3 : std_logic_vector(7 downto 0) := x"6A";

    constant idle_frame : std_logic_vector(15 downto 0) := K0 & K1;
    constant idle_cmd   : std_logic_vector(12 downto 0) := "1000000000000";
    constant bypass_cmd : std_logic_vector(12 downto 0) := "1000000000001";

    signal encoder_reg         : std_logic_vector(12 downto 0) := idle_cmd;
    signal bypass_frame       : std_logic_vector(15 downto 0) := x"7855";
    signal l1_frame_rd_en_reg : std_logic;
    signal r3_frame_rd_en_reg : std_logic;

    signal encoded_frame_reg :std_logic_vector(encoded_frame_o'range) := idle_frame;

begin
    r3_frame_o_rd_en <= r3_frame_rd_en_reg;
    l1_frame_o_rd_en <= l1_frame_rd_en_reg;


    scheduler : process(clk) is
    begin
        if rising_edge(clk) then
            -- deassert rd_en unless need to dequeue a frame (see below)
            l1_frame_rd_en_reg <= '0';
            r3_frame_rd_en_reg <= '0';
            bypass_frame_ready_o <= '0';
            if rst = '1' then
                encoder_reg <= idle_cmd;
            else
                if frame_start_pulse_i = '1' then
                    if r3_frame_i_empty = '0' then
                        encoder_reg <= '0' & r3_frame_i;
                        -- deassert lcb_frame_rd_en_reg if reading the last frame continuously
                        r3_frame_rd_en_reg <= (not r3_frame_rd_en_reg) or (not r3_frame_i_almost_empty);
                    elsif l1_frame_i_empty = '0' then
                        encoder_reg <= '0' & l1_frame_i;
                        -- deassert lcb_frame_rd_en_reg if reading the last frame continuously
                        l1_frame_rd_en_reg <= (not l1_frame_rd_en_reg) or (not l1_frame_i_almost_empty);
                    elsif bypass_frame_valid_i = '1' then
                        bypass_frame <= bypass_frame_i;
                        bypass_frame_ready_o <= '1';
                        encoder_reg <= bypass_cmd; -- command for bypassing 6b8b encoder
                    else
                        encoder_reg <= idle_cmd;
                    end if;
                end if;
            end if;
        end if;
    end process;

    encoded_frame_o <= encoded_frame_reg;

    encoder : process(clk)  -- 6b8b frame encoder
    begin
        if rising_edge(clk) then
            if encoder_reg(12) = '1' then -- insert a comma
                case encoder_reg is
                    when bypass_cmd =>
                        encoded_frame_reg <= bypass_frame;
                    when others => -- IDLE or unknown command
                        encoded_frame_reg <= idle_frame;
                end case;
            else  -- Encode R3 or L1 frame
                encoded_frame_reg(15 downto 8) <= MAP_6B_8B(to_integer(unsigned(encoder_reg(11 downto 6))));
                encoded_frame_reg(7 downto 0)  <= MAP_6B_8B(to_integer(unsigned(encoder_reg(5 downto 0))));
            end if;
        end if;
    end process;

end architecture behavioral;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company: BNL
--! Engineer: Elena Zhivun
--!
--! Create Date:    04/07/2020
--! Module Name:    lcb_axi_encoder.vhd
--! Project Name:   FELIX
--!
--! Axi-stream version of LCB frame encoder for ITk Strips
----------------------------------------------------------------------------------
--! Use standard library
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;
    use ieee.numeric_std.all;
    use work.strips_package.all;
    use work.lcb_regmap_package.all;

entity lcb_axi_encoder is
    generic(
        --DEBUG_en : boolean := false;
        USE_ULTRARAM : boolean;
        DISTR_RAM : boolean := false;
        USE_BUILT_IN_FIFO : std_logic := '1'
    );
    port(
        daq_reset          : in  std_logic; -- 40 MHz rst
        clk40              : in  std_logic; -- 40 MHz BC clk
        s_axis_aclk        : in  std_logic; -- axi stream clock
        daq_fifo_flush     : in  std_logic;
        invert_polarity  : in  std_logic;

        -- axistream configuration source
        config_s_axis  : in axis_8_type;
        config_tready  : out std_logic;
        config_almost_full : out std_logic;

        -- axistream command source
        command_s_axis  : in axis_8_type;
        command_tready  : out std_logic;
        command_almost_full : out std_logic;

        -- axistream trickle source
        trickle_s_axis  : in axis_8_type;
        trickle_tready  : out std_logic;
        trickle_almost_full : out std_logic;

        -- TTC signals
        bcr_i                : in  std_logic;
        l0a_i                : in  std_logic;
        l0a_trig_i           : in  std_logic_vector(3 downto 0);
        l0id_i               : in  std_logic_vector(6 downto 0);
        sync_i               : in  std_logic;
        -- configuration from global register map
        config               : in  t_strips_config; --! configuration settings -- @suppress "Port name 'config' is a keyword in Verilog and may cause problems in mixed language projects"
        frame_start_pulse_o  : out std_logic; --for simulation
        regmap_o             : out t_register_map;  --for simulation
        -- output signals
        EdataOUT             : out std_logic_vector(3 downto 0)
    );
end entity lcb_axi_encoder;

architecture RTL of lcb_axi_encoder is

    --component strips_lcb_ila
    --  PORT(
    --    clk    : IN std_logic;
    --    probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --    probe1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    --      probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --      probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --      probe4 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    --      probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --      probe6 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    --      probe7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --      probe8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --      probe9 : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    --      probe10 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --      probe11 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --      probe12 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --      probe13 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    --      probe14 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    --      probe15 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    --      probe16 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --      probe17 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --      probe18 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --      probe19 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
    --  );
    --END component strips_lcb_ila;

    signal rst_probe                    : std_logic_vector(0 downto 0); -- @suppress "signal rst_probe is never read"
    signal command_ready_probe          : std_logic_vector(0 downto 0); -- @suppress "signal command_ready_probe is never read"
    signal command_valid_probe          : std_logic_vector(0 downto 0); -- @suppress "signal command_valid_probe is never read"
    signal encoded_frame_probe          : std_logic_vector(15 downto 0); -- @suppress "signal encoded_frame_probe is never read"
    signal command_decoder_error_count  : std_logic_vector(15 downto 0); -- @suppress "signal command_decoder_error_count is never read"
    signal EdataOUT_reg, EdataOUT_port  : std_logic_vector(3 downto 0);
    signal bcr_probe                    : std_logic_vector(0 downto 0); -- @suppress "signal bcr_probe is never read"
    signal l0a_probe                    : std_logic_vector(0 downto 0); -- @suppress "signal l0a_probe is never read"
    signal decoder_idle                 : std_logic; -- @suppress "signal decoder_idle is never read"
    signal frame_start_pulse            : std_logic; -- marks beginning of the LCB frame (new frame will be loaded next CLK cycle)
    signal frame_start_pulse_probe      : std_logic_vector(0 downto 0); -- marks beginning of the LCB frame (new frame will be loaded next CLK cycle) -- @suppress "signal frame_start_pulse_probe is never read"
    signal ttc_l0a_frame_reg            : std_logic_vector(11 downto 0); -- pre-encoded version of L0A frame originated from TTC -- @suppress "signal ttc_l0a_frame_reg is never read"
    signal ttc_l0a_frame_valid_reg      : std_logic; -- is TTC l0a frame valid this clk cycle
    signal ttc_l0a_frame_valid_probe    : std_logic_vector(0 downto 0); -- is TTC l0a frame valid this clk cycle -- @suppress "signal ttc_l0a_frame_valid_probe is never read"
    --signal bc_counter                   : std_logic_vector(11 downto 0);
    signal trickle_trigger_gating       : std_logic; -- BC based trickle trigger and elink gating
    signal trickle_trigger_gating_probe : std_logic_vector(0 downto 0); -- @suppress "signal trickle_trigger_gating_probe is never read"
    signal register_guard         : std_logic;
    signal register_guard_probe      : std_logic_vector(0 downto 0); -- @suppress "signal register_guard_probe is never read"
    signal trickle_trigger         : std_logic;
    signal trickle_trigger_probe    : std_logic_vector(0 downto 0); -- @suppress "signal trickle_trigger_probe is never read"
    signal trickle_ready_probe      : std_logic_vector(0 downto 0); -- @suppress "signal trickle_ready_probe is never read"
    signal trickle_valid_probe      : std_logic_vector(0 downto 0); -- @suppress "signal trickle_valid_probe is never read"
    signal config_ready_probe      : std_logic_vector(0 downto 0); -- @suppress "signal config_ready_probe is never read"
    signal config_valid_probe      : std_logic_vector(0 downto 0); -- @suppress "signal config_valid_probe is never read"
    signal m_axis_command         : axis_8_type;
    signal m_axis_config         : axis_8_type;
    signal m_axis_trickle         : axis_8_type;
    signal m_axis_command_tready     : std_logic;
    signal m_axis_config_tready     : std_logic;
    signal m_axis_trickle_tready     : std_logic;

    signal fifo_flush_n : std_logic;
begin

    fifo_flush_n <= not daq_fifo_flush;

    config_fifo: entity work.Axis8Fifo
        generic map(
            --DEPTH => 256,
            --CLOCKING_MODE => "independent_clocks",
            --RELATED_CLOCKS => 0,
            --FIFO_MEMORY_TYPE => "auto",
            --PACKET_FIFO => "false",
            USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
            DISTR_RAM => DISTR_RAM
        )
        port map(
            s_axis_aresetn => fifo_flush_n,
            s_axis_aclk => s_axis_aclk,
            s_axis => config_s_axis,
            s_axis_tready => config_tready,
            m_axis_aclk => clk40,
            m_axis => m_axis_config,
            m_axis_tready => m_axis_config_tready,
            almost_full => config_almost_full
        );


    command_fifo: entity work.Axis8Fifo
        generic map(
            --DEPTH => 256,
            --CLOCKING_MODE => "independent_clocks",
            --RELATED_CLOCKS => 0,
            --FIFO_MEMORY_TYPE => "auto",
            --PACKET_FIFO => "false",
            USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
            DISTR_RAM => DISTR_RAM
        )
        port map(
            s_axis_aresetn => fifo_flush_n,
            s_axis_aclk => s_axis_aclk,
            s_axis => command_s_axis,
            s_axis_tready => command_tready,
            m_axis_aclk => clk40,
            m_axis => m_axis_command,
            m_axis_tready => m_axis_command_tready,
            almost_full => command_almost_full
        );


    trickle_fifo: entity work.Axis8Fifo
        generic map(
            --DEPTH => 256,
            --CLOCKING_MODE => "independent_clocks",
            --RELATED_CLOCKS => 0,
            --FIFO_MEMORY_TYPE => "auto",
            --PACKET_FIFO => "false",
            USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
            DISTR_RAM => DISTR_RAM
        )
        port map(
            s_axis_aresetn => fifo_flush_n,
            s_axis_aclk => s_axis_aclk,
            s_axis => trickle_s_axis,
            s_axis_tready => trickle_tready,
            m_axis_aclk => clk40,
            m_axis => m_axis_trickle,
            m_axis_tready => m_axis_trickle_tready,
            almost_full => trickle_almost_full
        );

    EdataOUT                     <= EdataOUT_reg;
    EdataOUT_reg           <= not EdataOUT_port when invert_polarity = '1' else EdataOUT_port;

    -- ILA probes
    rst_probe                    <= (others => daq_reset);
    command_valid_probe          <= (others => m_axis_command.tvalid);
    command_ready_probe          <= (others => m_axis_command_tready);
    config_valid_probe           <= (others => m_axis_config.tvalid);
    config_ready_probe           <= (others => m_axis_config_tready);
    trickle_valid_probe          <= (others => m_axis_trickle.tvalid);
    trickle_ready_probe          <= (others => m_axis_trickle_tready);
    l0a_probe                    <= (others => l0a_i);
    bcr_probe                    <= (others => bcr_i);
    frame_start_pulse_probe      <= (others => frame_start_pulse);
    ttc_l0a_frame_valid_probe    <= (others => ttc_l0a_frame_valid_reg);
    trickle_trigger_gating_probe <= (others => trickle_trigger_gating);
    trickle_trigger_probe      <= (others => trickle_trigger);
    register_guard_probe      <= (others => register_guard);

    --ila_probes_gen : if (DEBUG_en) generate
    --  probes : strips_lcb_ila
    --    PORT map(
    --      clk     => ila_clk,
    --      probe0  => rst_probe,
    --      probe1  => m_axis_config.tdata,
    --      probe2  => config_valid_probe,
    --      probe3  => config_ready_probe,
    --      probe4  => EdataOUT_reg,
    --      probe5  => frame_start_pulse_probe,
    --      probe6  => encoded_frame_probe,
    --      probe7  => bcr_probe,
    --      probe8  => l0a_probe,
    --      probe9  => l0id_i,
    --      probe10 => trickle_trigger_probe,
    --      probe11 => trickle_trigger_gating_probe,
    --      probe12 => register_guard_probe,
    --      probe13 => ttc_l0a_frame_reg,
    --      probe14 => m_axis_trickle.tdata,
    --      probe15 => m_axis_command.tdata,
    --      probe16 => trickle_valid_probe,
    --      probe17 => command_valid_probe,
    --      probe18 => trickle_ready_probe,
    --      probe19 => command_ready_probe
    --    );
    --end generate ila_probes_gen;

    LCB_inst : entity work.lcb_wrapper
        generic map(
            USE_ULTRARAM         => USE_ULTRARAM
        )
        port map(
            clk => clk40,
            rst => daq_reset,
            bcr_i => bcr_i,
            l0a_i => l0a_i,
            l0a_trig_i => l0a_trig_i,
            l0id_i => l0id_i,
            sync_i => sync_i,
            -- configuration elink
            config_data_i => m_axis_config.tdata,
            config_valid_i => m_axis_config.tvalid,
            config_ready_o => m_axis_config_tready,
            -- trickle elink
            trickle_data_i => m_axis_trickle.tdata,
            trickle_valid_i => m_axis_trickle.tvalid,
            trickle_ready_o => m_axis_trickle_tready,
            -- command elink
            command_data_i => m_axis_command.tdata,
            command_valid_i => m_axis_command.tvalid,
            command_ready_o => m_axis_command_tready,
            config => config,
            decoder_idle_o => decoder_idle,
            error_count_o => command_decoder_error_count,
            frame_start_pulse_o => frame_start_pulse,
            ttc_l0a_frame_o => ttc_l0a_frame_reg,
            ttc_l0a_frame_valid_o => ttc_l0a_frame_valid_reg,
            trickle_trigger_gating_o => trickle_trigger_gating,
            trickle_trigger_o => trickle_trigger,
            register_guard_o => register_guard,
            regmap_o => regmap_o,
            encoded_frame_o => encoded_frame_probe,
            edata_o => EdataOUT_port
        );

    frame_start_pulse_o <= frame_start_pulse; --output for simulation



end architecture RTL;

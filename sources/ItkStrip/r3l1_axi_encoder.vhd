--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company: BNL
--! Engineer: Elena Zhivun
--!
--! Create Date:    04/07/2020
--! Module Name:    r3l1_axi_encoder.vhd
--! Project Name:   FELIX
--!
--! Axi-stream version of R3L1 frame encoder for ITk Strips
----------------------------------------------------------------------------------
--! Use standard library
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;
    use ieee.numeric_std.all;
    use work.strips_package.all;
    use work.pcie_package.bitfield_global_strips_config_w_type;

entity r3l1_axi_encoder is
    generic(
        --DEBUG_en : boolean := false;
        DISTR_RAM : boolean := false;
        USE_BUILT_IN_FIFO : std_logic := '0'
    );
    port(
        rst       : in  std_logic; -- 40 MHz rst
        clk40            : in  std_logic; -- 40 MHz BC clk
        --ila_clk          : in  std_logic; -- clk for ILAs
        s_axis_aclk       : in  std_logic; -- axi stream clock
        daq_fifo_flush    : in  std_logic;
        invert_polarity : in  std_logic;

        -- axistream configuration source
        config_s_axis  : in axis_8_type;
        config_tready  : out std_logic;
        config_almost_full : out std_logic;

        -- axistream command source
        command_s_axis  : in axis_8_type;
        command_tready  : out std_logic;
        command_almost_full : out std_logic;

        -- TTC signals
        bcr_i          : in  std_logic;
        l1_trigger_i   : in  std_logic_vector(3 downto 0); -- shifted L1 trigger signal
        l1_tag_i       : in  std_logic_vector(6 downto 0); -- L0tag
        ITk_sync_i     : in  std_logic;
        -- RoIE signals
        r3_trigger_i   : in  std_logic; -- R3 trigger signal
        r3_tag_i       : in  std_logic_vector(6 downto 0); -- L0tag
        module_mask_i  : in  std_logic_vector(4 downto 0); -- module mask
        -- configuration from global register map
        config         : in  t_strips_config; --! configuration settings -- @suppress "Port name 'config' is a keyword in Verilog and may cause problems in mixed language projects"
        -- output signals
        EdataOUT       : out std_logic_vector(3 downto 0)
    );
end entity r3l1_axi_encoder;

architecture RTL of r3l1_axi_encoder is

    --component strips_r3l1_ila
    --  PORT(
    --    clk    : IN std_logic;
    --    probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --      probe1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    --      probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --      probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --      probe4 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    --      probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --      probe6 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    --      probe7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --      probe8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --      probe9 : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    --      probe10 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --      probe11 : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    --      probe12 : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    --      probe13 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    --      probe14 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --      probe15 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
    --  );
    --END component strips_r3l1_ila;

    signal rst_probe                    : std_logic_vector(0 downto 0); -- @suppress "signal rst_probe is never read"
    signal config_ready_probe           : std_logic_vector(0 downto 0); -- @suppress "signal config_ready_probe is never read"
    signal config_valid_probe           : std_logic_vector(0 downto 0); -- @suppress "signal config_valid_probe is never read"
    signal command_ready_probe          : std_logic_vector(0 downto 0); -- @suppress "signal command_ready_probe is never read"
    signal command_valid_probe          : std_logic_vector(0 downto 0); -- @suppress "signal command_valid_probe is never read"
    signal encoded_frame_probe          : std_logic_vector(15 downto 0); -- @suppress "signal encoded_frame_probe is never read"
    signal EdataOUT_reg, EdataOUT_port  : std_logic_vector(3 downto 0);
    signal bcr_probe                    : std_logic_vector(0 downto 0); -- @suppress "signal bcr_probe is never read"
    signal frame_start_pulse            : std_logic; -- marks beginning of the LCB frame (new frame will be loaded next CLK cycle)
    signal frame_start_pulse_probe      : std_logic_vector(0 downto 0); -- marks beginning of the LCB frame (new frame will be loaded next CLK cycle) -- @suppress "signal frame_start_pulse_probe is never read"
    signal r3_trigger_probe             : std_logic_vector(0 downto 0); -- @suppress "signal r3_trigger_probe is never read"
    signal l1_trigger_probe             : std_logic_vector(3 downto 0); -- @suppress "signal l1_trigger_probe is never read"
    signal r3_frame_input_probe         : std_logic_vector(11 downto 0); -- @suppress "signal r3_frame_input_probe is never read"
    signal m_axis_command         : axis_8_type;
    signal m_axis_config         : axis_8_type;
    signal m_axis_command_tready     : std_logic;
    signal m_axis_config_tready     : std_logic;

    signal fifo_flush_n : std_logic;
begin

    fifo_flush_n <= not daq_fifo_flush;

    config_fifo: entity work.Axis8Fifo
        generic map(
            --DEPTH => 256,
            --CLOCKING_MODE => "independent_clocks",
            --RELATED_CLOCKS => 0,
            --FIFO_MEMORY_TYPE => "auto",
            --PACKET_FIFO => "false",
            USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
            DISTR_RAM => DISTR_RAM
        )
        port map(
            s_axis_aresetn => fifo_flush_n,
            s_axis_aclk => s_axis_aclk,
            s_axis => config_s_axis,
            s_axis_tready => config_tready,
            m_axis_aclk => clk40,
            m_axis => m_axis_config,
            m_axis_tready => m_axis_config_tready,
            almost_full => config_almost_full
        );


    command_fifo: entity work.Axis8Fifo
        generic map(
            --DEPTH => 256,
            --CLOCKING_MODE => "independent_clocks",
            --RELATED_CLOCKS => 0,
            --FIFO_MEMORY_TYPE => "auto",
            --PACKET_FIFO => "false",
            USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
            DISTR_RAM => DISTR_RAM
        )
        port map(
            s_axis_aresetn => fifo_flush_n,
            s_axis_aclk => s_axis_aclk,
            s_axis => command_s_axis,
            s_axis_tready => command_tready,
            m_axis_aclk => clk40,
            m_axis => m_axis_command,
            m_axis_tready => m_axis_command_tready,
            almost_full => command_almost_full
        );

    EdataOUT                         <= EdataOUT_reg;
    EdataOUT_reg               <= not EdataOUT_port when invert_polarity = '1' else EdataOUT_port;

    rst_probe                         <= (others => rst);
    config_valid_probe                <= (others => m_axis_config.tvalid);
    config_ready_probe                <= (others => m_axis_config_tready);
    command_valid_probe               <= (others => m_axis_command.tvalid);
    command_ready_probe               <= (others => m_axis_command_tready);
    bcr_probe                         <= (others => bcr_i);
    frame_start_pulse_probe           <= (others => frame_start_pulse);
    l1_trigger_probe                  <= l1_trigger_i;
    r3_trigger_probe                  <= (others => r3_trigger_i);
    r3_frame_input_probe(11 downto 7) <= module_mask_i;
    r3_frame_input_probe(6 downto 0)  <= r3_tag_i;

    --ila_probes_gen : if (DEBUG_en) generate
    --  probes : strips_r3l1_ila
    --    PORT map(
    --      clk     => ila_clk,
    --      probe0  => rst_probe,
    --      probe1  => m_axis_command.tdata,
    --      probe2  => command_valid_probe,
    --      probe3  => command_ready_probe,
    --      probe4  => EdataOUT_reg,
    --      probe5  => frame_start_pulse_probe,
    --      probe6  => encoded_frame_probe,
    --      probe7  => bcr_probe,
    --      probe8  => l1_trigger_probe,
    --      probe9  => l1_tag_i,
    --      probe10 => r3_trigger_probe,
    --      probe11 => module_mask_i,
    --      probe12 => r3_tag_i,
    --      probe13 => m_axis_config.tdata,
    --      probe14 => config_valid_probe,
    --      probe15 => config_ready_probe
    --    );
    --end generate ila_probes_gen;

    R3L1_inst : entity work.r3l1_wrapper
        port map(
            clk                 => clk40,
            rst                 => rst,
            -- TTC signals
            bcr_i               => bcr_i,
            l1_trigger_i        => l1_trigger_i,
            l1_tag_i            => l1_tag_i,
            -- RoIE signals
            r3_trigger_i        => r3_trigger_i,
            r3_tag_i            => r3_tag_i,
            module_mask_i       => module_mask_i,

            -- configuration elink
            config_data_i        => m_axis_config.tdata,
            config_valid_i       => m_axis_config.tvalid,
            config_ready_o       => m_axis_config_tready,

            -- command elink
            command_data_i        => m_axis_command.tdata,
            command_valid_i       => m_axis_command.tvalid,
            command_ready_o       => m_axis_command_tready,

            -- configuration from register map
            config              => config,

            -- data output
            encoded_frame_o     => encoded_frame_probe,
            edata_o             => EdataOUT_port,
            frame_start_pulse_o => frame_start_pulse
        );

end architecture RTL;

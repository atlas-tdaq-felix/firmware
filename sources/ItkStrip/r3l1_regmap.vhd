--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.strips_package.all;
    use work.r3l1_regmap_package.all;

entity r3l1_regmap is
    port(
        clk : in std_logic;
        rst : in std_logic;
        wr_en : in std_logic; -- pulse this field to update
        data_i : in t_register_data; -- new register value
        addr_i : in std_logic_vector(7 downto 0); -- register address
        regmap_o : out t_register_map
    );
end entity r3l1_regmap;

architecture RTL of r3l1_regmap is
    constant regmap_default : t_register_map:= (others => (others => '0'));
    constant max_address : natural := t_register_name'pos(t_register_name'high);
    signal regmap : t_register_map:= regmap_default;
begin
    regmap_o <= regmap;

    regmap_update: process (clk) is
        variable field_name : t_register_name;
        variable addr_int  : natural; -- @suppress "The type of a variable has to be constrained in size"
    begin
        if rising_edge(clk) then
            if rst = '1' then
                regmap <= regmap_default;
            else
                -- bring the pulsed fields back to 0
                for field in pulse_map'range loop
                    for bit_id in pulse_map(field)'range loop
                        if pulse_map(field)(bit_id) = '1' then
                            regmap(field)(bit_id) <= '0';
                        end if;
                    end loop;
                end loop;

                -- update the register values accorging to the input
                addr_int := to_integer(unsigned(addr_i));
                if (wr_en = '1') and (addr_int <= max_address) then
                    field_name := t_register_name'val(addr_int);
                    regmap(field_name) <= data_i;
                end if;
            end if;
        end if;
    end process;
end architecture RTL;

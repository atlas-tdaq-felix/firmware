--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use work.strips_package.all;

package r3l1_regmap_package is

    ------ Register map for R3L1 link controller ------
    type t_register_name is (
        L0A_FRAME_PHASE, -- 0x00 (2 bit) L0A frame phase wrt to the BCR signal
        L1_ENABLE,  -- 0x01 (1 bit) enable generating response to L1 signals from TTC
        R3_ENABLE   -- 0x02 (1 bit) enable generating response to R3 signals from TTC
    );

    -- Field type of the register map
    subtype t_register_data is std_logic_vector(15 downto 0);
    type t_register_map is array (t_register_name range t_register_name'low to t_register_name'high) of t_register_data;

    -- which register fields must be pulsed for 1 clk cycle (instead of kept high indefinitely)
    constant pulse_map : t_register_map := (
                                             others => (others => '0')
                                           );

    function to_string(
        constant name : t_register_name
    ) return string;

end package r3l1_regmap_package;

package body r3l1_regmap_package is
    -- overload to_string() to handle t_lcb_command
    function to_string(
        constant name : t_register_name
    ) return string is
    begin
        case name is
            when L0A_FRAME_PHASE => return string'("L0A_FRAME_PHASE");
            when L1_ENABLE => return string'("L1_ENABLE");
            when R3_ENABLE => return string'("R3_ENABLE");

            when others =>
                report "Unknown R3L1 register is referenced" severity FAILURE;
                return string'("(unknown R3L1 register)");
        end case;
    end;
end package body r3l1_regmap_package;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  BNL
--! Engineer: Elena Zhivun <ezhivun@bnl.gov>
--!
-- Created     : Wed Oct 30 10:06:40 2019
-- Last update : Thu May 21 18:43:39 2020
--!
--! Module Name:    lcb_scheduler_encoder
--! Project Name:   FELIX
--!
--! Selects which frame is to be sent to the HCC* next, and encodes it in 6b8b.
--! Outputs:
--!   1. TTC L0A frame if valid
--!    2. Bypass frame if available
--!   2. Frame from LCB fifo if available
--!   3. Idle frame if none above is available
--!
--! To request next frame, assert frame_start_pulse_i for 1 clk cycle
--! When ttc_l0a_en = '0' TTC L0A frames are disabled
--! When ttc_l0a_en = '1' TTC L0A frames are enabled
--! When trickle_bc_gating_en = '0' LCB frames are scheduled when they are available
--! When trickle_bc_gating_en = '1' LCB frames are scheduled only if trickle_bc_gating_i = '1'
--! l0a_frame_delay_i sets L0A and LCB frame delay in BC
--!
----------------------------------------------------------------------------------

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use work.strips_package.all;

entity lcb_scheduler_encoder is
    port(
        clk                          : in  std_logic; -- 40 MHz BC clk domain
        rst                          : in  std_logic; -- synchronous reset
        l0a_frame_i                  : in  STD_LOGIC_VECTOR(11 downto 0); -- L0A frame from TTC system
        lcb_frame_i                  : in  STD_LOGIC_VECTOR(12 downto 0); -- First word fall through fifo data
        lcb_frame_o_rd_en            : out std_logic; -- LCB frame FIFO rd_en signal
        lcb_frame_i_empty            : in  std_logic; -- LCB frame FIFO empty signal
        lcb_frame_i_almost_empty     : in  std_logic; -- LCB frame FIFO almost_empty signal
        l0a_frame_delay_i            : in  std_logic_vector(1 downto 0); -- delay TTC L0A frame (x4 BC units)
        bypass_frame_i               : in  std_logic_vector(15 downto 0); -- bypass FromHost frame (already 6b8b encoded)
        bypass_frame_valid_i         : in  std_logic; -- bypass frame is valid this clk cycle
        bypass_frame_ready_o         : out std_logic; -- request next bypass frame
        frame_start_pulse_i          : in  std_logic; -- request next frame (40 MHz BC clk domain)
        trickle_bc_gating_i          : in  std_logic; -- trickle/lcb gating signal
        trickle_allow_register_cmd_i : in  std_logic; -- allow register commands (read or write)
        trickle_bc_gating_en         : in  std_logic; -- enable the gating signal
        ttc_l0a_en                   : in  std_logic; -- enable L0A frames from TTC system
        encoded_frame_o              : out std_logic_vector(15 downto 0) := x"7855"
    );
end entity lcb_scheduler_encoder;

architecture behavioral of lcb_scheduler_encoder is
    constant K0 : std_logic_vector(7 downto 0) := x"78";
    constant K1 : std_logic_vector(7 downto 0) := x"55";
    constant K2 : std_logic_vector(7 downto 0) := x"47";
    constant K3 : std_logic_vector(7 downto 0) := x"6A";

    constant idle_frame : std_logic_vector(15 downto 0) := K0 & K1;
    constant idle_cmd   : std_logic_vector(12 downto 0) := "1000010000000";
    constant bypass_cmd : std_logic_vector(12 downto 0) := "1000011000000";

    signal encoder_reg               : std_logic_vector(12 downto 0) := idle_cmd;
    signal bypass_frame              : std_logic_vector(15 downto 0) := x"7855";
    signal l0a_valid                 : std_logic;
    signal lcb_frame_rd_en_reg       : std_logic;
    signal allow_low_priority_frames : boolean;

    type t_l0a_array is array (natural range <>) of std_logic_vector(l0a_frame_i'range);
    signal l0a_delayed : t_l0a_array(0 to 3) := (others => (others => '0'));

    signal encoded_frame_reg : std_logic_vector(encoded_frame_o'range) := idle_frame;
    signal lcb_register_command_start : boolean;
    signal lcb_register_command_stop : boolean;
    signal lcb_register_command_open : boolean;
    signal lcb_register_command_counter : integer range 0 to 511;

begin
    lcb_frame_o_rd_en <= lcb_frame_rd_en_reg;

    -- simulation has trouble without l0a_delayed extended to 0
    l0a_delayed(0) <= (others => '0');

    -- BC gating to control low-priority frames
    allow_low_priority_frames <= (trickle_bc_gating_en = '0' or (trickle_bc_gating_en = '1' and trickle_bc_gating_i = '1'));

    -- l0a is valid when BCR or mask is non-zero
    is_l0a_valid : process(l0a_delayed, l0a_frame_delay_i, l0a_frame_i) is
        variable l0a_delay_int : integer range 0 to 3;
    begin
        if l0a_frame_delay_i = "00" then
            l0a_valid <= l0a_frame_i(11) or l0a_frame_i(10) or l0a_frame_i(9) or l0a_frame_i(8) or l0a_frame_i(7);
        else
            l0a_delay_int := to_integer(unsigned(l0a_frame_delay_i));
            l0a_valid  <= l0a_delayed(l0a_delay_int)(11) or l0a_delayed(l0a_delay_int)(10) or l0a_delayed(l0a_delay_int)(9) or l0a_delayed(l0a_delay_int)(8) or l0a_delayed(l0a_delay_int)(7);
        end if;
    end process;

    -- determines if the next frame in LCB FIFO is a register command start frame (contains K2 and Start bit = '1')
    lcb_command_type_start : lcb_register_command_start <= lcb_frame_i(12 downto 6) = "1000000" and lcb_frame_i(4) = '1';
    -- determines if the next frame in LCB FIFO is a register command stop frame (contains K2 and Start bit = '0')
    lcb_command_type_stop : lcb_register_command_stop <= lcb_frame_i(12 downto 6) = "1000000" and lcb_frame_i(4) = '0';

    scheduler : process(clk) is
        variable l0a_delay_int : integer range 0 to 3;
    begin
        if rising_edge(clk) then
            lcb_frame_rd_en_reg  <= '0'; -- deassert rd_en unless need to dequeue an lcb frame (see below)
            bypass_frame_ready_o <= '0';
            if rst = '1' then
                encoder_reg <= idle_cmd;
                lcb_register_command_open <= false;
                lcb_register_command_counter <= 0;
            else
                l0a_delay_int := to_integer(unsigned(l0a_frame_delay_i));
                if frame_start_pulse_i = '1' then
                    if (l0a_valid = '1') and (ttc_l0a_en = '1') then
                        if l0a_frame_delay_i = "00" then
                            encoder_reg <= '0' & l0a_frame_i;
                        else
                            encoder_reg <= '0' & l0a_delayed(l0a_delay_int);
                        end if;
                    elsif (bypass_frame_valid_i = '1') and allow_low_priority_frames and (not lcb_register_command_open) then
                        bypass_frame         <= bypass_frame_i;
                        bypass_frame_ready_o <= '1';
                        encoder_reg          <= bypass_cmd; -- command for bypassing 6b8b encoder
                    elsif (lcb_frame_i_empty = '0') and ((trickle_bc_gating_en = '0') or ((trickle_bc_gating_en = '1') and (trickle_bc_gating_i = '1')
						and (not lcb_register_command_start or trickle_allow_register_cmd_i = '1'))) then
                        encoder_reg         <= lcb_frame_i; -- schedule this LCB frame
                        if lcb_register_command_start then
                            lcb_register_command_open <= true;
                            lcb_register_command_counter <= lcb_register_command_timeout;
                        end if;
                        if lcb_register_command_stop then
                            lcb_register_command_open <= false;
                        end if;
                        -- deassert lcb_frame_rd_en_reg if reading the last frame continuously
                        lcb_frame_rd_en_reg <= (not lcb_frame_rd_en_reg) or (not lcb_frame_i_almost_empty);
                    else
                        encoder_reg <= idle_cmd;
                    end if;
                end if;

                -- timeout for the register command tracker
                if lcb_register_command_open then
                    if lcb_register_command_counter = 0 then
                        lcb_register_command_open <= false; -- this register command timed out
                    else
                        lcb_register_command_counter <= lcb_register_command_counter - 1;
                    end if;
                end if;
            end if;
        end if;
    end process;

    encoded_frame_o <= encoded_frame_reg;

    encoder : process(clk)              -- 6b8b frame encoder
    begin
        if rising_edge(clk) then
            if encoder_reg(12) = '1' then -- insert a comma
                case encoder_reg(11 downto 6) is
                    when "000000" =>
                        encoded_frame_reg(15 downto 8) <= K2;
                        encoded_frame_reg(7 downto 0)  <= MAP_6B_8B(to_integer(unsigned(encoder_reg(5 downto 0))));
                    when "000001" =>
                        encoded_frame_reg(15 downto 8) <= K3;
                        encoded_frame_reg(7 downto 0)  <= MAP_6B_8B(to_integer(unsigned(encoder_reg(5 downto 0))));
                    when "000010" =>    -- command for insering IDLE frame
                        encoded_frame_reg <= idle_frame;
                    when "000011" =>    -- command for insering bypass frame
                        encoded_frame_reg <= bypass_frame;
                    when others =>      -- unknown command, treat as IDLE
                        encoded_frame_reg <= idle_frame;
                end case;
            else                        -- no comma
                encoded_frame_reg(15 downto 8) <= MAP_6B_8B(to_integer(unsigned(encoder_reg(11 downto 6))));
                encoded_frame_reg(7 downto 0)  <= MAP_6B_8B(to_integer(unsigned(encoder_reg(5 downto 0))));
            end if;
        end if;
    end process;

    delay_L0A : process(clk) is
    begin
        if rising_edge(clk) then
            if (rst = '1') then
                l0a_delayed(1 to l0a_delayed'high) <= (others => (others => '0'));
            else
                if frame_start_pulse_i = '1' then
                    l0a_delayed(1)                     <= l0a_frame_i;
                    l0a_delayed(2 to l0a_delayed'high) <= l0a_delayed(1 to l0a_delayed'high - 1);
                end if;
            end if;
        end if;
    end process;
end architecture behavioral;

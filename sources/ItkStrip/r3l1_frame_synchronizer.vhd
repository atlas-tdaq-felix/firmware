--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : ITk Strips package
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : r3l1_frame_synchronizer.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Sun May 17 16:31:36 2020
-- Last update : Thu May 21 18:44:01 2020
-- Platform    : Default Part Number
-- Standard    : VHDL-1993
--------------------------------------------------------------------------------
-- Copyright (c) 2020 BNL
-------------------------------------------------------------------------------
-- Description:
--    Provides frame_start_pulse synchronized to the BCR signals
--    for synchronizing LCB and R3L1 frames
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------


library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.strips_package.all;

entity r3l1_frame_synchronizer is
    port(
        clk : in std_logic; -- 40 MHz BC clock
        -- input BCR signal from TTC block
        -- frame_start_pulse_o will synchronize to this pulse (even if rst='1')
        -- the number of clk cycles between bcr must be divisible by 4
        -- otherwise the period between frame_start_pulse_o will be
        -- inconsistent, and the LCB frame scheduled during the inconsistency will be corrupt
        bcr_i               : in  std_logic;
        -- determines the phase of the L0A frame with respect to the BCR pulse
        -- if the BCR has not yet been received, the phase is random
        -- The BCR will be detected (for the purposes of setting LCB frame phase only) when rst='1'
        -- changing this value after rst is deasserted will result in corrupted LCB frames
        frame_phase_i       : in  unsigned(frame_phase'range);
        -- outputs strobe to start L0A frame; the strobe phase is determined by frame_phase_i
        frame_start_pulse_o : out std_logic
    );
end entity r3l1_frame_synchronizer;

architecture RTL of r3l1_frame_synchronizer is
    signal frame_phase_counter     : unsigned(frame_phase'range) := (others => '0');
    signal frame_start_pulse_reg   : std_logic;

    -- define LCB frame phases
    --constant C_FRAME_PHASE_0 : unsigned(frame_phase_counter'range) := "00";
    --constant C_FRAME_PHASE_1 : unsigned(frame_phase_counter'range) := "01";
    --constant C_FRAME_PHASE_2 : unsigned(frame_phase_counter'range) := "10";
    constant C_FRAME_PHASE_3 : unsigned(frame_phase_counter'range) := "11";

begin
    frame_start_pulse_o <= frame_start_pulse_reg;


    -- Generates frame_start_pulse_o signal
    frame_start_pulse_logic : process(clk) is
    begin
        if rising_edge(clk) then
            if frame_phase_counter = frame_phase_i then
                frame_start_pulse_reg <= '1';
            else
                frame_start_pulse_reg <= '0';
            end if;
        end if;
    end process;


    -- sets phase of LCB frame with respect to bcr_i signal
    frame_phase_logic : process(clk) is
    begin
        if rising_edge(clk) then
            if bcr_i = '1' then
                -- synchronize to the BCR signal
                frame_phase_counter <= (others => '0');
            else
                if frame_phase_counter = C_FRAME_PHASE_3 then
                    frame_phase_counter <= (others => '0');
                else
                    frame_phase_counter <= frame_phase_counter + 1;
                end if;
            end if;
        end if;
    end process;

end architecture RTL;

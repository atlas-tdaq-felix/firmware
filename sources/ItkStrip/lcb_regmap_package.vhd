--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use work.strips_package.all;

package lcb_regmap_package is

    ------ Register map for LCB_link_controller ------
    type t_register_name is (
        L0A_FRAME_PHASE, -- 0x00 (2 bit) L0A frame phase wrt to the BCR signal
        L0A_FRAME_DELAY, -- 0x01 (4 bit) L0A frame additional delay in BC units

        TTC_L0A_ENABLE, -- 0x02 (1 bit) enable TTC L0A trigger
        TTC_BCR_DELAY, -- 0x03 (12 bit) BCR signal from TTC will be delayed by this many BCs
        --
        GATING_TTC_ENABLE, -- 0x04 (1 bit) enables generating trickle gating signal in response to TTC BCR
        GATING_BC_START, -- 0x05 (12 bit) when to enable trickle trigger / FromHost path
        GATING_BC_STOP, -- 0x06 (12 bit) when to disable trickle trigger / FromHost path

        TRICKLE_TRIGGER_PULSE, -- 0x07 (1 bit) issue a single trickle trigger
        TRICKLE_TRIGGER_RUN, -- 0x08 (1 bit) issue a continuous trickle trigger
        TRICKLE_DATA_START, -- 0x09 (14 bit) where to start the readout when triggered
        TRICKLE_DATA_END, -- 0x0A (14 bit) last byte to read out when triggered
        TRICKLE_WRITE_ADDR, -- 0x0B (14 bit) where to move the playback controller wr pointer
        TRICKLE_SET_WRITE_ADDR_PULSE, -- (1 bit) 0x0C write 0x01 here to move the WR pointer

        ENCODING_ENABLE, -- 0x0D (1 bit) 0 = use software encoding, 1 = use firmware encoding

        HCC_MASK,    -- 0x0E (16 bit) Disables register commands addressed to masked HCC* chips
        ABC_MASK_0,  -- 0x0F (16 bit) Disables register commands addressed to masked ABC* chips
        ABC_MASK_1,  -- 0x10 (16 bit)
        ABC_MASK_2,  -- 0x11 (16 bit)
        ABC_MASK_3,  -- 0x12 (16 bit)
        ABC_MASK_4,  -- 0x13 (16 bit)
        ABC_MASK_5,  -- 0x14 (16 bit)
        ABC_MASK_6,  -- 0x15 (16 bit)
        ABC_MASK_7,  -- 0x16 (16 bit)
        ABC_MASK_8,  -- 0x17 (16 bit)
        ABC_MASK_9,  -- 0x18 (16 bit)
        ABC_MASK_A,  -- 0x19 (16 bit)
        ABC_MASK_B,  -- 0x1A (16 bit)
        ABC_MASK_C,  -- 0x1B (16 bit)
        ABC_MASK_D,  -- 0x1C (16 bit)
        ABC_MASK_E,  -- 0x1D (16 bit)
        ABC_MASK_F,  -- 0x1E (16 bit)
        PLAYBACK_LOOPS -- 0x1F (16 bit) -- Number of times to loop over trickle mem (0=1=once)

    );

    -- Field type of the register map
    subtype t_register_data is std_logic_vector(15 downto 0);
    type t_register_map is array (t_register_name range t_register_name'low to t_register_name'high) of t_register_data;

    -- which register fields must be pulsed for 1 clk cycle (instead of kept high indefinitely)
    constant pulse_map : t_register_map := (
                                             TRICKLE_TRIGGER_PULSE => (0 => '1', others => '0'),
                                             TRICKLE_SET_WRITE_ADDR_PULSE => (0 => '1', others => '0'),
                                             others => (others => '0')
                                           );

    function to_string(
        constant name : t_register_name
    ) return string;

end package lcb_regmap_package;

package body lcb_regmap_package is
    -- overload to_string() to handle t_lcb_command
    function to_string(
        constant name : t_register_name
    ) return string is
    begin
        case name is
            when L0A_FRAME_PHASE => return string'("L0A_FRAME_PHASE");
            when L0A_FRAME_DELAY => return string'("L0A_FRAME_DELAY");
            when TTC_L0A_ENABLE => return string'("TTC_L0A_ENABLE");
            when TTC_BCR_DELAY => return string'("TTC_BCR_DELAY");
            when GATING_TTC_ENABLE => return string'("GATING_TTC_ENABLE");
            when GATING_BC_START => return string'("GATING_BC_START");
            when GATING_BC_STOP => return string'("GATING_BC_STOP");
            when TRICKLE_TRIGGER_PULSE => return string'("TRICKLE_TRIGGER_PULSE");
            when TRICKLE_TRIGGER_RUN => return string'("TRICKLE_TRIGGER_RUN");
            when TRICKLE_DATA_START => return string'("TRICKLE_DATA_START");
            when TRICKLE_DATA_END => return string'("TRICKLE_DATA_END");
            when TRICKLE_WRITE_ADDR => return string'("TRICKLE_WRITE_ADDR");
            when TRICKLE_SET_WRITE_ADDR_PULSE => return string'("TRICKLE_SET_WRITE_ADDR_PULSE");
            when ENCODING_ENABLE => return string'("ENCODING_ENABLE");
            when HCC_MASK => return string'("HCC_MASK");
            when ABC_MASK_0 => return string'("ABC_MASK_0");
            when ABC_MASK_1 => return string'("ABC_MASK_1");
            when ABC_MASK_2 => return string'("ABC_MASK_2");
            when ABC_MASK_3 => return string'("ABC_MASK_3");
            when ABC_MASK_4 => return string'("ABC_MASK_4");
            when ABC_MASK_5 => return string'("ABC_MASK_5");
            when ABC_MASK_6 => return string'("ABC_MASK_6");
            when ABC_MASK_7 => return string'("ABC_MASK_7");
            when ABC_MASK_8 => return string'("ABC_MASK_8");
            when ABC_MASK_9 => return string'("ABC_MASK_9");
            when ABC_MASK_A => return string'("ABC_MASK_A");
            when ABC_MASK_B => return string'("ABC_MASK_B");
            when ABC_MASK_C => return string'("ABC_MASK_C");
            when ABC_MASK_D => return string'("ABC_MASK_D");
            when ABC_MASK_E => return string'("ABC_MASK_E");
            when ABC_MASK_F => return string'("ABC_MASK_F");
            when PLAYBACK_LOOPS => return string'("PLAYBACK_LOOPS");

            when others =>
                report "Unknown LCB register is referenced" severity FAILURE;
                return string'("(unknown LCB register)");
        end case;
    end;
end package body lcb_regmap_package;

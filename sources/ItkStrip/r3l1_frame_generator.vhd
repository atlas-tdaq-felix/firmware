--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : ITk Strips package
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : r3l1_frame_generator.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Sun May 17 16:31:36 2020
-- Last update : Wed Aug 11 18:50:45 2021
-- Platform    : Default Part Number
-- Standard    : VHDL-1993
--------------------------------------------------------------------------------
-- Copyright (c) 2020 BNL
-------------------------------------------------------------------------------
-- Description:
--    Generates R3 or L1 frames when trigger_i signal is asserted.
--    The frame type is configured via a generic.
--    R3 frame uses tag_i and module_mask_i ports
--    L1 frame uses only tag_i port
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

Library xpm;
    use xpm.vcomponents.all;

entity r3l1_frame_generator is
    port(
        clk                  : in  std_logic; -- 40 MHz BC clk
        rst                  : in  std_logic; -- synchronous reset
        enable_i             : in  std_logic; -- enables generation of R3/L1 frames
        trigger_i            : in  std_logic; -- R3/L1 trigger signal
        module_mask_i        : in  std_logic_vector(4 downto 0);
        tag_i                : in  std_logic_vector(6 downto 0); -- L0 tag for this frame
        frame_o              : out STD_LOGIC_VECTOR(11 downto 0); -- FIFO data out
        frame_rd_en_i        : in  std_logic; -- FIFO rd_en signal
        frame_empty_o        : out std_logic; -- FIFO empty signal
        frame_almost_empty_o : out std_logic -- FIFO contains one word
    --frame_overflow_o     : out std_logic -- FIFO overflow has occured
    );
end entity r3l1_frame_generator;

architecture RTL of r3l1_frame_generator is
    -- signal frame_wr_rst_busy : std_logic;
    -- signal frame_rd_rst_busy : std_logic;
    signal frame_wr_en : std_logic;
    signal frame_reg   : STD_LOGIC_VECTOR(11 downto 0);
begin

    L1_frame : process(clk) is
    begin
        if rising_edge(clk) then
            if (rst = '1' or enable_i = '0') then
                frame_reg   <= (others => '0');
                frame_wr_en <= '0';
            else
                -- Xilinx IP FIFOs can tolerate overflows w/o data corruption
                frame_wr_en            <= trigger_i;
                frame_reg(6 downto 0)  <= tag_i;
                frame_reg(11 downto 7) <= module_mask_i;
            end if;
        end if;
    end process;

    -- FIFO containing unencoded frames
    --r3l1_frame_fifo : entity work.r3l1_12b_fwft_fifo
    --  port map(
    --    clk          => clk,
    --    srst         => rst,
    --    din          => frame_reg,
    --    wr_en        => frame_wr_en,
    --    rd_en        => frame_rd_en_i,
    --    dout         => frame_o,
    --    full         => open,
    --    almost_empty => frame_almost_empty_o,
    --    empty        => frame_empty_o,
    --    overflow     => frame_overflow_o
    --  );


    xpm_fifo_sync_inst : xpm_fifo_sync
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT"
            DOUT_RESET_VALUE => "0",    -- String
            ECC_MODE => "no_ecc",       -- String
            FIFO_MEMORY_TYPE => "distributed", -- String
            FIFO_READ_LATENCY => 0,     -- DECIMAL
            FIFO_WRITE_DEPTH => 16,   -- DECIMAL
            FULL_RESET_VALUE => 0,      -- DECIMAL
            PROG_EMPTY_THRESH => 4,    -- DECIMAL
            PROG_FULL_THRESH => 15,     -- DECIMAL
            RD_DATA_COUNT_WIDTH => 5,   -- DECIMAL = log2(FIFO_READ_DEPTH)+1.
            -- FIFO_READ_DEPTH =  FIFO_WRITE_DEPTH*WRITE_DATA_WIDTH / READ_DATA_WIDTH
            READ_DATA_WIDTH => 12,      -- DECIMAL
            READ_MODE => "fwft",         -- String
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_ADV_FEATURES => "0801", -- String
            WAKEUP_TIME => 0,           -- DECIMAL
            WRITE_DATA_WIDTH => 12,     -- DECIMAL
            WR_DATA_COUNT_WIDTH => 5    -- DECIMAL = log2(FIFO_WRITE_DEPTH)+1
        )
        port map (
            sleep => '0', -- 1-bit input: Dynamic power saving- If sleep is High, the memory/fifo
            rst => rst, -- 1-bit input: Reset: Must be synchronous to wr_clk. The clock(s) can be-- unstable at the time of applying reset, but reset must be released-- only after the clock(s) is/are stable.
            wr_clk => clk, -- 1-bit input: Write clock: Used for write operation. wr_clk must be a
            wr_en => frame_wr_en, -- 1-bit input: Write Enable: If the FIFO is not full, asserting this
            din => frame_reg, -- WRITE_DATA_WIDTH-bit input: Write Data: The input data bus used when
            full => open, -- 1-bit output: Full Flag: When asserted, this signal indicates that the-- FIFO is full. Write requests are ignored when the FIFO is full,-- initiating a write when the FIFO is full is not destructive to the-- contents of the FIFO.
            prog_full => open, -- 1-bit output: Programmable Full: This signal is asserted when the
            wr_data_count => open, -- WR_DATA_COUNT_WIDTH-bit output: Write Data Count: This bus indicates
            overflow => open, -- 1-bit output: Overflow: This signal indicates that a write request
            wr_rst_busy => open, -- 1-bit output: Write Reset Busy: Active-High indicator that the FIFO
            almost_full => open, -- 1-bit output: Almost Full: When asserted, this signal indicates that
            wr_ack => open, -- 1-bit output: Write Acknowledge: This signal indicates that a write
            rd_en => frame_rd_en_i, -- 1-bit input: Read Enable: If the FIFO is not empty, asserting this
            dout => frame_o, -- READ_DATA_WIDTH-bit output: Read Data: The output data bus is driven
            empty => frame_empty_o, -- 1-bit output: Empty Flag: When asserted, this signal indicates that
            prog_empty => open, -- 1-bit output: Programmable Empty: This signal is asserted when the
            rd_data_count => open, -- RD_DATA_COUNT_WIDTH-bit output: Read Data Count: This bus indicates
            underflow => open, -- 1-bit output: Underflow: Indicates that the read request (rd_en)
            rd_rst_busy => open, -- 1-bit output: Read Reset Busy: Active-High indicator that the FIFO
            almost_empty => frame_almost_empty_o, -- 1-bit output: Almost Empty : When asserted, this signal indicates that
            data_valid => open, -- 1-bit output: Read Data Valid: When asserted, this signal indicates
            injectsbiterr => '0', -- 1-bit input: Single Bit Error Injection: Injects a single bit error if
            injectdbiterr => '0', -- 1-bit input: Double Bit Error Injection: Injects a double bit error if
            sbiterr => open, -- 1-bit output: Single Bit Error: Indicates that the ECC decoder
            dbiterr => open -- 1-bit output: Double Bit Error: Indicates that the ECC decoder
        -- signal causes data (on din) to be written to the FIFO Must be held
        -- active-low when rst or wr_rst_busy or rd_rst_busy is active high

        );

end architecture RTL;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : ITk Strips package
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : strips_package.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Wed Oct 30 16:30:27 2019
-- Last update : Mon Aug 30 16:16:56 2021
-- Platform    : Default Part Number
-- Standard    : VHDL-1993
--------------------------------------------------------------------------------
-- Copyright (c) 2019 User Company Name
-------------------------------------------------------------------------------
-- Description: Shared definitions for communicating with HCC* chip and testbenches
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use std.textio.all;
    use work.pcie_package.all;

package strips_package is

    -- Build settings for LCB encoders:
    constant CFG_INCLUDE_BCID_GATING  : std_logic := '0';
    constant CFG_INCLUDE_ASIC_MASKS   : std_logic := '0';
    constant CFG_SEQRAM_ADDR_WIDTH    : natural := 12; -- 4kB,
    --13; -- 8kB
    --14; --16kB

    constant STRIPS_ENCODING_CFG_MON_REG : std_logic_vector(31 downto 0) :=
                                                                            x"000000" &
                                                                            std_logic_vector(to_unsigned(CFG_SEQRAM_ADDR_WIDTH,4)) &
                                                                            "00" &
                                                                            CFG_INCLUDE_ASIC_MASKS &
                                                                            CFG_INCLUDE_BCID_GATING;


    subtype t_bcid is std_logic_vector(11 downto 0);
    constant full_bcid   : t_bcid := (others => '0');
    constant frame_phase : std_logic_vector(1 downto 0)  := (others => '0');

    -- define field positions in the L0A frame
    constant BCR_bits      : std_logic_vector(11 downto 11) := (others => '0');
    constant L0A_mask_bits : std_logic_vector(10 downto 7) := (others => '0');
    constant L0tag_bits    : std_logic_vector(6 downto 0)  := (others => '0');

    -- playback controller address and data width
    constant playback_controller_address : std_logic_vector(CFG_SEQRAM_ADDR_WIDTH-1 downto 0) := (others => '0'); --4 kB of trickle memory
    constant playback_controller_data : std_logic_vector(7 downto 0) := (others => '0');

    -- LCB encoder commands
    type t_lcb_command is (
        LCB_CMD_IDLE, LCB_CMD_FAST, LCB_CMD_L0A,
        LCB_CMD_ABC_REG_READ, LCB_CMD_ABC_REG_WRITE,
        LCB_CMD_HCC_REG_READ, LCB_CMD_HCC_REG_WRITE
    );

    -- size of the register guard window for trickle trigger generator
    constant guard_window_size : integer range 0 to 4095 := 64;

    -- Maximum delay between register start and stop frames for scheduler
    -- guards against inserting bypass frames into active register commands
    constant lcb_register_command_timeout : integer range 0 to 511 := 511;

    -- ABC* mask (to disable commands to/from damaged chips)
    type t_abc_mask is array (15 downto 0) of std_logic_vector(15 downto 0);

    -- LCB controller data sources
    type t_lcb_data_source is (ELINK, TRICKLE);

    -- LCB controller commands
    subtype t_itk_command is std_logic_vector(7 downto 0);
    constant ITK_CMD_NOOP : t_itk_command := x"00";
    constant ITK_CMD_REGMAP_WRITE : t_itk_command := x"10";
    constant ITK_CMD_IDLE : t_itk_command := x"80";
    constant ITK_CMD_FAST : t_itk_command := x"81";
    constant ITK_CMD_L0A : t_itk_command := x"82";
    constant ITK_CMD_ABC_REG_READ : t_itk_command := x"A0";
    constant ITK_CMD_HCC_REG_READ : t_itk_command := x"A1";
    constant ITK_CMD_ABC_REG_WRITE : t_itk_command := x"A2";
    constant ITK_CMD_HCC_REG_WRITE : t_itk_command := x"A3";
    constant ITK_CMD_ABC_REG_WRITE_BLOCK : t_itk_command := x"B2";
    constant ITK_CMD_HCC_REG_WRITE_BLOCK : t_itk_command := x"B3";

    -- x"C0" - calibration sequence?

    type t_strips_config is record
        GLOBAL_STRIPS_CONFIG : bitfield_global_strips_config_w_type;
        GLOBAL_TRICKLE_TRIGGER : std_logic_vector(64 downto 64);
        STRIPS_R3_TRIGGER : std_logic_vector(64 downto 64);
        STRIPS_L1_TRIGGER : std_logic_vector(64 downto 64);
        STRIPS_R3L1_TRIGGER : std_logic_vector(64 downto 64);
    end record;

    -- 6b8b encoding map
    -- From itsdaq-fw lcb_pkg_globals.vhd
    -- From Paul:
    type slv8_array is array (natural range <>) of std_logic_vector(7 downto 0);
    constant MAP_6B_8B : slv8_array(0 to 63) := (x"59", --  0  0x00
                                                  x"71", --  1  0x01
                                                  x"72", --  2  0x02
                                                  x"c3", --  3  0x03
                                                  x"65", --  4  0x04
                                                  x"c5", --  5  0x05
                                                  x"c6", --  6  0x06
                                                  x"87", --  7  0x07
                                                  x"69", --  8  0x08
                                                  x"c9", --  9  0x09
                                                  x"ca", -- 10  0x0a
                                                  x"8b", -- 11  0x0b
                                                  x"cc", -- 12  0x0c
                                                  x"8d", -- 13  0x0d
                                                  x"8e", -- 14  0x0e
                                                  x"4b", -- 15  0x0f
                                                  x"53", -- 16  0x10
                                                  x"d1", -- 17  0x11
                                                  x"d2", -- 18  0x12
                                                  x"93", -- 19  0x13
                                                  x"d4", -- 20  0x14
                                                  x"95", -- 21  0x15
                                                  x"96", -- 22  0x16
                                                  x"17", -- 23  0x17
                                                  x"d8", -- 24  0x18
                                                  x"99", -- 25  0x19
                                                  x"9a", -- 26  0x1a
                                                  x"1b", -- 27  0x1b
                                                  x"9c", -- 28  0x1c
                                                  x"1d", -- 29  0x1d
                                                  x"1e", -- 30  0x1e
                                                  x"5c", -- 31  0x1f
                                                  x"63", -- 32  0x20
                                                  x"e1", -- 33  0x21
                                                  x"e2", -- 34  0x22
                                                  x"a3", -- 35  0x23
                                                  x"e4", -- 36  0x24
                                                  x"a5", -- 37  0x25
                                                  x"a6", -- 38  0x26
                                                  x"27", -- 39  0x27
                                                  x"e8", -- 40  0x28
                                                  x"a9", -- 41  0x29
                                                  x"aa", -- 42  0x2a
                                                  x"2b", -- 43  0x2b
                                                  x"ac", -- 44  0x2c
                                                  x"2d", -- 45  0x2d
                                                  x"2e", -- 46  0x2e
                                                  x"6c", -- 47  0x2f
                                                  x"74", -- 48  0x30
                                                  x"b1", -- 49  0x31
                                                  x"b2", -- 50  0x32
                                                  x"33", -- 51  0x33
                                                  x"b4", -- 52  0x34
                                                  x"35", -- 53  0x35
                                                  x"36", -- 54  0x36
                                                  x"56", -- 55  0x37
                                                  x"b8", -- 56  0x38
                                                  x"39", -- 57  0x39
                                                  x"3a", -- 58  0x3a
                                                  x"5a", -- 59  0x3b
                                                  x"3c", -- 60  0x3c
                                                  x"4d", -- 61  0x3d
                                                  x"4e", -- 62  0x3e
                                                  x"66"); -- 63  0x3f

    -------- Definitions for simulation and testbenches --------

    function decode_6b8b(
        data : std_logic_vector(7 downto 0);
        report_errors : boolean := true
    ) return std_logic_vector;

    function to_string(
        constant cmd : t_lcb_command
    ) return string;

    -- Decoded LCB / R3L1 frame record (for simulation)
    type t_decoder_record is record
        encoded : std_logic_vector(15 downto 0);
        locked : std_logic;
        idle : std_logic;
        k2 : std_logic;
        k3 : std_logic;
        decoded : std_logic_vector(11 downto 0);
    end record;

end package strips_package;

package body strips_package is

    -- overload to_string() to handle t_lcb_command
    function to_string(
        constant cmd : t_lcb_command
    ) return string is
    begin
        case cmd is
            when LCB_CMD_IDLE => return string'("LCB_CMD_IDLE");
            when LCB_CMD_FAST => return string'("LCB_CMD_FAST");
            when LCB_CMD_L0A => return string'("LCB_CMD_L0A");
            when LCB_CMD_ABC_REG_READ => return string'("LCB_CMD_ABC_REG_READ");
            when LCB_CMD_ABC_REG_WRITE => return string'("LCB_CMD_ABC_REG_WRITE");
            when LCB_CMD_HCC_REG_READ => return string'("LCB_CMD_HCC_REG_READ");
            when LCB_CMD_HCC_REG_WRITE => return string'("LCB_CMD_HCC_REG_WRITE");
            when others =>
                report "Unknown LCB command is referenced" severity FAILURE;
                return string'("(unknown command)");
        end case;
    end;

    -- overload to_string() to handle t_lcb_data_source
    function to_string( -- @suppress "Unused declaration"
        constant cmd : t_lcb_data_source
    ) return string is
    begin
        case cmd is
            when ELINK => return string'("ELINK");
            when TRICKLE => return string'("TRICKLE");
            when others =>
                report "Unknown LCB data source is referenced" severity FAILURE;
                return string'("(unknown data source)");
        end case;
    end;

    -- 6b8b decoding map, simulation use only
    function decode_6b8b(
        data : std_logic_vector(7 downto 0);
        report_errors : boolean := true
    ) return std_logic_vector is
        variable n_ones, n_zeros : integer := 0;
        variable result : std_logic_vector(5 downto 0) := "000000";
    begin
        case data is
            when "01111000" | "01010101" | "01000111" | "01101010" =>
                if report_errors then
                    report "6b8b comma cannot be decoded" severity FAILURE;
                    result := "111111";
                else
                    result := "000000";
                end if;

            when "01011001" =>          -- 0x00
                result := "000000";

            when "01110001" =>          -- 0x01
                result := "000001";

            when "01110010" =>          -- 0x02
                result := "000010";

            when "01100101" =>          -- 0x04
                result := "000100";

            when "01101001" =>          -- 0x10
                result := "001000";

            when "01010011" =>          -- 0x20
                result := "010000";

            when "01100011" =>          -- 0x40
                result := "100000";

            when "01110100" =>          -- 0x60
                result := "110000";

            when "01100110" =>          -- 0x77
                result := "111111";

            when "01001110" =>          -- 0x76
                result := "111110";

            when "01001101" =>          -- 0x75
                result := "111101";

            when "01011010" =>          -- 0x73
                result := "111011";

            when "01010110" =>          -- 0x67
                result := "110111";

            when "01101100" =>          -- 0x57
                result := "101111";

            when "01011100" =>          -- 0x37
                result := "011111";

            when "01001011" =>          -- 0x17
                result := "001111";

            when others =>
                n_ones  := 0;
                n_zeros := 0;
                parity_check : for i in data'LOW to data'HIGH loop
                    if data(i) = '1' then
                        n_ones := n_ones + 1;
                    else
                        n_zeros := n_zeros + 1;
                    end if;
                end loop parity_check;

                if (n_ones /= n_zeros) then
                    if report_errors then
                        report "Untabulated 6b8b word must have equal number of '1' and '0'" severity FAILURE;
                        result := "111111";
                    else
                        result := "000000";
                    end if;
                else
                    return data(5 downto 0);
                end if;
        end case;

        return result;
    end function decode_6b8b;

end package body strips_package;

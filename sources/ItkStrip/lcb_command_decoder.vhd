--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : ITk Strips package
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : lcb_command_decoder.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Thu May 21 18:39:02 2020
-- Last update : Wed Nov 16 12:48:33 2022
-- Platform    : Default Part Number
-- Standard    : < VHDL-1993 >
--------------------------------------------------------------------------------
-- Copyright (c) 2020 User Company Name
-------------------------------------------------------------------------------
-- Description: Decodes commands from host and sends them to
-- LCB frame generator.
--
-- In the past it used to control trickle configuration memory
-- and local register map, but now these functions are obsolete
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------



library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.strips_package.all;

entity lcb_command_decoder is
    generic (
        -- incomplete commands will be dropped if the complete
        -- arguments are not received before the timeout (4000 BC = 100 ms)
        TIMEOUT : integer := 8000
    );
    port(
        clk                          : in  std_logic; -- 40 MHz BC clk
        rst                          : in  std_logic;
        -- elink command source
        elink_data_i                 : in  std_logic_vector(7 downto 0); -- elink data
        elink_valid_i                : in  std_logic; -- elink data is valid this clk cycle
        elink_ready_o                : out std_logic; -- this module is ready for the next elink data

        -- trickle configuration command source
        trickle_data_i               : in  std_logic_vector(7 downto 0); -- trickle configuration data
        trickle_valid_i              : in  std_logic; -- trickle configuration data is valid this clk cycle
        trickle_ready_o              : out std_logic; -- this module is ready for the next elink data

        -- LCB frame generator interface
        lcb_cmd_o                    : out t_lcb_command; -- which command to execute
        lcb_cmd_start_pulse_o        : out std_logic; -- pulse for 1 clk cycle to start command
        lcb_fast_cmd_data_o          : out std_logic_vector(5 downto 0); -- bc & cmd
        lcb_l0a_data_o               : out std_logic_vector(11 downto 0); -- bcr & L0A mask & L0 tag
        lcb_abc_id_o                 : out std_logic_vector(3 downto 0); -- ABC* address (0xF = broadcast)
        lcb_hcc_id_o                 : out std_logic_vector(3 downto 0); -- HCC* address (0xF = broadcast)
        lcb_reg_addr_o               : out std_logic_vector(7 downto 0); -- register address for reg. read/write
        lcb_reg_data_o               : out std_logic_vector(31 downto 0); -- register data for reg. write
        lcb_ready_i                  : in  std_logic; -- LCB encoder ready for a new command
        lcb_frame_fifo_almost_full_i : in  std_logic; -- LCB encoder FIFO is almost full

        -- The command decoder is idle (debug)
        decoder_idle_o               : out std_logic;
        error_count_o                : out std_logic_vector(15 downto 0)
    );
end entity lcb_command_decoder;

architecture RTL of lcb_command_decoder is
    type t_state is (s_init, s_select_src, s_read_opcode, s_wait_for_args,
        s_read_args, s_wait_for_lcb_encoder, s_send_read,
        s_wait_lcb_encoder_return, s_send_write, s_read_block_length,
        s_block_write_loop, s_block_read_data );
    type t_state_array is array (natural range <>) of t_state; -- a return stack of sorts
    signal state             : t_state  := s_init;
    signal return_state       : t_state_array(0 to 3) := (others => s_init);
    constant max_parameter_length : positive   := 6;
    signal args_counter           : integer range -1 to max_parameter_length;
    type t_args_buffer is array (natural range <>) of std_logic_vector(7 downto 0);
    signal args_buffer            : t_args_buffer(0 to max_parameter_length - 1) := (others => (others => '0'));
    type t_data_source is (ELINK, TRICKLE);
    signal data_source            : t_data_source;
    signal lcb_encoder_ready      : boolean;
    signal opcode                 : t_itk_command;
    signal data_i                 : std_logic_vector(7 downto 0);
    signal valid_i                : std_logic;
    signal error_count_reg        : unsigned(error_count_o'range);
    signal ready_o                : std_logic  := '0';
    signal timeout_reg        : unsigned(15 downto 0);
    signal opcode_is_hcc        : boolean;
    signal block_loop_counter     : unsigned(7 downto 0) := (others => '0');
    signal block_next_address     : unsigned(7 downto 0) := (others => '0');
    signal block_data_counter     : integer range 0 to 3 := 0;


begin
    lcb_encoder_ready   <= lcb_ready_i = '1' and lcb_frame_fifo_almost_full_i = '0';
    lcb_fast_cmd_data_o <= args_buffer(0)(lcb_fast_cmd_data_o'range);
    lcb_l0a_data_o      <= args_buffer(1)(4 downto 0) & args_buffer(0)(6 downto 0);
    lcb_hcc_id_o        <= args_buffer(0)(7 downto 4);
    lcb_abc_id_o        <= args_buffer(0)(3 downto 0);
    lcb_reg_addr_o      <= args_buffer(1);
    lcb_reg_data_o      <= args_buffer(5) & args_buffer(4) & args_buffer(3) & args_buffer(2);
    error_count_o       <= std_logic_vector(error_count_reg);

    -- decoding operation type
    process(opcode)
    begin
        case opcode is
            when ITK_CMD_HCC_REG_READ | ITK_CMD_HCC_REG_WRITE | ITK_CMD_HCC_REG_WRITE_BLOCK =>
                opcode_is_hcc <= true;
            when others =>
                opcode_is_hcc <= false;
        end case;
    end process;

    source_selector_mux : process(rst, data_source, elink_data_i, elink_valid_i, trickle_data_i, trickle_valid_i, ready_o) is
    begin
        if rst = '1' then
            data_i          <= (others => '0');
            valid_i         <= '0';
            elink_ready_o   <= '1';
            trickle_ready_o <= '1';
        else
            if data_source = ELINK then
                data_i          <= elink_data_i;
                valid_i         <= elink_valid_i;
                elink_ready_o   <= ready_o;
                trickle_ready_o <= '0';
            else                            -- trickle configuration memory selected
                data_i          <= trickle_data_i;
                valid_i         <= trickle_valid_i;
                elink_ready_o   <= '0';
                trickle_ready_o <= ready_o;
            end if;
        end if;
    end process;

    state_update_logic : process(clk) is
        procedure next_command_if_lcb_ready is
        begin
            if lcb_encoder_ready then
                state <= s_select_src;
            else
                state <= s_wait_for_lcb_encoder;
            end if;
        end;

        procedure goto_return_state is
        begin
            state <= return_state(0);
            return_state(return_state'low to return_state'high - 1) <= return_state(return_state'low+1 to return_state'high);
            return_state(return_state'high) <= s_init; -- reset the state machine when out of stack
        end;

        variable v_loop_return_state : t_state;
    begin
        if rising_edge(clk) then
            if rst = '1' then
                state <= s_init;
                return_state <= (others => s_init);
            else
                case state is
                    when s_init =>
                        state <= s_select_src;
                    when s_select_src =>
                        if elink_valid_i = '1' or trickle_valid_i = '1' then
                            state <= s_read_opcode;
                        else
                            state <= s_select_src;
                        end if;
                    when s_read_opcode =>
                        case t_itk_command'(data_i) is
                            when ITK_CMD_ABC_REG_READ | ITK_CMD_ABC_REG_WRITE |
								ITK_CMD_HCC_REG_READ | ITK_CMD_HCC_REG_WRITE |
								ITK_CMD_FAST | ITK_CMD_L0A  |
								ITK_CMD_ABC_REG_WRITE_BLOCK | ITK_CMD_HCC_REG_WRITE_BLOCK =>
                                if valid_i = '1' then
                                    state <= s_read_args;
                                else
                                    state <= s_wait_for_args;
                                end if;

                            when ITK_CMD_IDLE =>
                                next_command_if_lcb_ready;
                            when ITK_CMD_NOOP =>
                                state <= s_select_src;
                            when others =>
                                state <= s_init;
                        -- synthesis translate_off
                        -- report "Unknown ITk command in s_read_opcode" severity FAILURE;
                        -- synthesis translate_on
                        end case;

                    -- only commands with fixed number of arguments
                    when s_read_args =>
                        if timeout_reg = to_unsigned(TIMEOUT, timeout_reg'length) then
                            state <= s_select_src;
                        else
                            if args_counter = 0 then
                                case opcode is
                                    when ITK_CMD_ABC_REG_WRITE_BLOCK | ITK_CMD_HCC_REG_WRITE_BLOCK =>
                                        state <= s_read_block_length;
                                    when others =>
                                        next_command_if_lcb_ready;
                                end case;
                            else
                                state <= s_read_args;
                            end if;
                        end if;

                    -- go to next state on the stack when ready
                    when s_send_read | s_send_write =>
                        state <= s_wait_lcb_encoder_return;

                    when s_wait_lcb_encoder_return =>  -- @suppress "Dead state 's_wait_lcb_encoder_return': state does not have outgoing transitions"
                        if lcb_encoder_ready then
                            goto_return_state;
                        else
                            state <= s_wait_lcb_encoder_return;
                        end if;

                    when s_wait_for_lcb_encoder =>
                        next_command_if_lcb_ready;

                    when s_read_block_length =>
                        if timeout_reg = to_unsigned(TIMEOUT, timeout_reg'length) then
                            state <= s_select_src;
                        else
                            if valid_i = '1' and ready_o = '1' then
                                state <= s_block_write_loop;
                            else
                                state <= s_read_block_length;
                            end if;
                        end if;

                    when s_block_read_data =>
                        if timeout_reg = to_unsigned(TIMEOUT, timeout_reg'length) then
                            state <= s_select_src;
                        else
                            if valid_i = '1' and ready_o = '1' then
                                if block_data_counter = 0 then
                                    goto_return_state;
                                else
                                    state <= s_block_read_data;
                                end if;
                            end if;
                        end if;

                    when s_block_write_loop =>
                        if block_loop_counter = x"00" then
                            -- this is the last loop
                            v_loop_return_state := s_select_src;
                        else
                            v_loop_return_state := s_block_write_loop;
                        end if;

                        state <= s_block_read_data;
                        case opcode is
                            when ITK_CMD_ABC_REG_WRITE_BLOCK | ITK_CMD_HCC_REG_WRITE_BLOCK =>
                                return_state(0) <= s_send_write;
                                return_state(1) <= v_loop_return_state;
                            when others =>
                                -- synthesis translate_off
                                report "Unexpected block operation " & to_string(opcode) severity FAILURE;
                        -- synthesis translate_on
                        end case;

                    when others =>
                        state <= s_init;
                        -- synthesis translate_off
                        report "Unknown ITk command decoder state" severity FAILURE;
                -- synthesis translate_on
                end case;
            end if;
        end if;
    end process;

    output_update_logic : process(clk) is
        procedure issue_lcb_cmd_if_ready is
        begin
            if lcb_encoder_ready then
                lcb_cmd_start_pulse_o <= '1';
            else
                lcb_cmd_start_pulse_o <= '0';
            end if;
        end;
    begin
        if rising_edge(clk) then
            if rst = '1' then
                error_count_reg <= to_unsigned(0, error_count_reg'length);
                ready_o               <= '1';
                lcb_cmd_o             <= LCB_CMD_IDLE;
                lcb_cmd_start_pulse_o <= '0';
                decoder_idle_o        <= '0';
                opcode           <= ITK_CMD_NOOP;
                data_source <= ELINK;
            end if;

            case state is
                when s_init =>
                    ready_o               <= '0';
                    lcb_cmd_o             <= LCB_CMD_IDLE;
                    lcb_cmd_start_pulse_o <= '0';
                    decoder_idle_o        <= '0';
                    opcode           <= ITK_CMD_NOOP;
                when s_select_src =>
                    ready_o               <= '0';
                    lcb_cmd_start_pulse_o <= '0';
                    decoder_idle_o        <= '0';
                    if elink_valid_i = '1' then
                        data_source <= ELINK;
                        ready_o     <= '1';
                        opcode      <= t_itk_command'(elink_data_i);
                    elsif trickle_valid_i = '1' then
                        data_source <= TRICKLE;
                        ready_o     <= '1';
                        opcode      <= t_itk_command'(trickle_data_i);
                    else
                        decoder_idle_o <= '1';
                    end if;
                when s_read_opcode =>
                    ready_o               <= '0';
                    lcb_cmd_start_pulse_o <= '0';
                    decoder_idle_o        <= '0';
                    timeout_reg <= (others => '0');
                    case opcode is
                        when ITK_CMD_ABC_REG_WRITE | ITK_CMD_HCC_REG_WRITE =>
                            args_counter <= 6; -- (hcc_id & abc_id), addr, data x 4
                        when ITK_CMD_ABC_REG_READ | ITK_CMD_HCC_REG_READ |
							ITK_CMD_ABC_REG_WRITE_BLOCK | ITK_CMD_HCC_REG_WRITE_BLOCK =>
                            args_counter <= 2; -- (hcc_id & abc_id), addr
                        when ITK_CMD_IDLE =>
                            lcb_cmd_o <= LCB_CMD_IDLE;
                            issue_lcb_cmd_if_ready;
                        when ITK_CMD_FAST =>
                            args_counter <= 1; -- (bc & cmd), addr
                        when ITK_CMD_L0A =>
                            args_counter <= 2; -- (bcr & mask), tag
                        when ITK_CMD_NOOP =>
                            null;
                        when others =>
                            error_count_reg <= error_count_reg + to_unsigned(1, error_count_reg'length);
                    -- synthesis translate_off
                    -- report "Unknown ITk command in s_read_opcode" severity FAILURE;
                    -- synthesis translate_on
                    end case;
                when s_read_args =>
                    lcb_cmd_start_pulse_o <= '0';
                    decoder_idle_o        <= '0';
                    timeout_reg <= timeout_reg + to_unsigned(1, timeout_reg'length);

                    if timeout_reg = to_unsigned(TIMEOUT, timeout_reg'length) then
                        ready_o  <= '0';
                    else
                        ready_o  <= valid_i;
                        if valid_i = '1' and ready_o = '1' then
                            args_buffer(0)                     <= data_i;
                            args_buffer(1 to args_buffer'high) <= args_buffer(0 to args_buffer'high - 1);
                            args_counter                       <= args_counter - 1;
                            if args_counter = 1 then
                                ready_o <= '0';
                            end if;
                        end if;

                        if args_counter = 0 then
                            ready_o <= '0';

                            case opcode is
                                -- commands w/o arguments or with with variable
                                -- number of arguments shouldn't be here
                                when ITK_CMD_ABC_REG_WRITE =>
                                    lcb_cmd_o <= LCB_CMD_ABC_REG_WRITE;
                                    issue_lcb_cmd_if_ready;
                                when ITK_CMD_HCC_REG_WRITE =>
                                    lcb_cmd_o <= LCB_CMD_HCC_REG_WRITE;
                                    issue_lcb_cmd_if_ready;
                                when ITK_CMD_ABC_REG_READ =>
                                    lcb_cmd_o <= LCB_CMD_ABC_REG_READ;
                                    issue_lcb_cmd_if_ready;
                                when ITK_CMD_HCC_REG_READ =>
                                    lcb_cmd_o <= LCB_CMD_HCC_REG_READ;
                                    issue_lcb_cmd_if_ready;
                                when ITK_CMD_FAST =>
                                    lcb_cmd_o <= LCB_CMD_FAST;
                                    issue_lcb_cmd_if_ready;
                                when ITK_CMD_L0A =>
                                    issue_lcb_cmd_if_ready;
                                    lcb_cmd_o <= LCB_CMD_L0A;

                                -- extended block commands (ready_o = 1 to read block length next)
                                when ITK_CMD_ABC_REG_WRITE_BLOCK | ITK_CMD_HCC_REG_WRITE_BLOCK =>
                                    ready_o <= '1';

                                when others =>
                                    error_count_reg <= error_count_reg + to_unsigned(1, error_count_reg'length);
                                    -- synthesis translate_off
                                    report "Unknown ITk command in s_read_args (args_counter = 0)" severity FAILURE;
                            -- synthesis translate_on
                            end case;
                        end if;
                    end if;

                when s_wait_for_lcb_encoder | s_wait_lcb_encoder_return =>
                    ready_o        <= '0';
                    decoder_idle_o <= '0';
                    issue_lcb_cmd_if_ready;

                -- prepare and send read command (of a sequence)
                when s_send_read =>
                    ready_o <= '0';
                    lcb_cmd_start_pulse_o <= '0';
                    if opcode_is_hcc then
                        lcb_cmd_o <= LCB_CMD_HCC_REG_READ;
                    else
                        lcb_cmd_o <= LCB_CMD_ABC_REG_READ;
                    end if;

                -- prepare and send write command (of a sequence)
                when s_send_write =>
                    ready_o <= '0';
                    lcb_cmd_start_pulse_o <= '0';
                    if opcode_is_hcc then
                        lcb_cmd_o <= LCB_CMD_HCC_REG_WRITE;
                    else
                        lcb_cmd_o <= LCB_CMD_ABC_REG_WRITE;
                    end if;

                -- load next register value in the data block
                when s_block_read_data =>
                    lcb_cmd_start_pulse_o <= '0';
                    ready_o <= '1';
                    timeout_reg <= timeout_reg + to_unsigned(1, timeout_reg'length);
                    if timeout_reg = to_unsigned(TIMEOUT, timeout_reg'length) then
                        ready_o <= '0';
                    else
                        if valid_i = '1' and ready_o = '1' then
                            ready_o <= '0';
                            if block_data_counter /= 0 then
                                block_data_counter <= block_data_counter - 1;
                            end if;
                            args_buffer(block_data_counter + 2) <= data_i;
                        end if;
                    end if;

                when s_read_block_length =>
                    timeout_reg <= timeout_reg + to_unsigned(1, timeout_reg'length);
                    lcb_cmd_start_pulse_o <= '0';
                    ready_o <= '1';
                    if timeout_reg = to_unsigned(TIMEOUT, timeout_reg'length) then
                        ready_o <= '0';
                    else
                        if valid_i = '1' and ready_o = '1' then
                            ready_o <= '0';
                            block_loop_counter <= unsigned(data_i);
                            block_next_address <= unsigned(args_buffer(1));
                        end if;
                    end if;

                when s_block_write_loop =>
                    ready_o <= '1';
                    block_data_counter <= 3;
                    lcb_cmd_start_pulse_o <= '0';
                    block_loop_counter <= block_loop_counter - 1;
                    block_next_address <= block_next_address + 1;
                    args_buffer(1) <=  std_logic_vector(block_next_address);

                when others =>
                    error_count_reg <= error_count_reg + to_unsigned(1, error_count_reg'length);
                    -- synthesis translate_off
                    report "Unknown ITK command decoder state" severity FAILURE;
            -- synthesis translate_on
            end case;
        end if;
    end process;
end architecture RTL;

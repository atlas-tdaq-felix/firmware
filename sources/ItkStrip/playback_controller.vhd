--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : ITk Strips package
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : playback_controller.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Thu Feb  6 17:04:20 2020
-- Last update : Wed Nov 16 13:09:07 2022
-- Platform    : Default Part Number
-- Standard    : VHDL-1993
--------------------------------------------------------------------------------
-- Copyright (c) 2019 User Company Name
-------------------------------------------------------------------------------
-- Description:
-- Playback controller stores commands for LCB encoder module,
-- which are send out upon receiving an external trigger.
--
-- TODO: write a tutorial here and an example of writing the memory
-- and triggering the readout by software and from trickle trigger
--
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

library xpm;
    use xpm.vcomponents.all;

    use work.strips_package.all;

entity playback_controller is
    generic(
        USE_ULTRARAM : boolean
    );
    port(
        clk              : in  std_logic;   -- 40 MHz BC clock
        rst              : in  std_logic;
        start_pulse_i    : in  std_logic;   -- toggle this input to initiate the readout
        readout_active_o : out std_logic;   -- '1' when the readout is in progress
        playback_loops_i : in std_logic_vector(15 downto 0);

        -- trickle memory write interface

        -- input data to be saved to the BRAM
        data_i                 : in  std_logic_vector(playback_controller_data'range);
        valid_i                : in  std_logic;  -- indicates data_i is valid this clk cycle
        ready_o                : out std_logic;  -- this module is ready for more data
        -- pulse this signal to load the write_addr of bram from write_addr_start_i
        set_write_addr_pulse_i : in  std_logic;
        -- the memory address that write_ptr of the BRAM will be placed to
        -- when set_write_addr_pulse_i is pulsed
        write_addr_start_i     : in  std_logic_vector(playback_controller_address'range);
        -- the memory address playback should be started from
        read_addr_start_i      : in  std_logic_vector(playback_controller_address'range);
        -- the memory address of the last byte to be read out
        read_addr_stop_i       : in  std_logic_vector(playback_controller_address'range);

        -- trickle configuration memory read interface

        -- outputs data read out from BRAM
        data_o  : out std_logic_vector(playback_controller_data'range);
        -- indicates data_o is valid this clk cycle
        valid_o : out std_logic;            -- indicates data_o is valid this clk cycle
        -- the receiving side indicates it's ready for more data
        ready_i : in  std_logic
    );
end entity playback_controller;

architecture RTL of playback_controller is
    -- BRAM signals
    signal wea   : std_logic_vector(0 downto 0);
    signal addra : std_logic_vector(playback_controller_address'range);

    -- output register buffers
    signal readout_active_reg, valid_reg, ready_reg : std_logic;
    signal data_reg                                 : std_logic_vector(playback_controller_data'range);

    -- FIFO mode state
    type t_state is (s_init_readout, s_fetch_next, s_wait, s_readout_complete, s_bram_update);
    signal state : t_state := s_bram_update;

    function f_mem_type(uram : boolean) return string is
    begin
        if uram then
            return "ultra";
        else
            return "block";
        end if;
    end function;

    signal loop_counter : unsigned(15 downto 0) := x"0000";
    signal loop_active  : std_logic;
    signal loops_prime : unsigned(15 downto 0);

begin

    data_o           <= data_reg;
    valid_o          <= valid_reg;
    readout_active_o <= readout_active_reg;
    ready_o          <= ready_reg;
    wea              <= (others => '1') when (ready_reg = '1' and valid_i = '1' and state = s_bram_update) else (others => '0');


    loop_active <= '0' when (loop_counter = x"0000") else '1';
    loops_prime <= x"0001" when (playback_loops_i = x"0000") else unsigned(playback_loops_i);


    fsm_state_var_update : process(clk) is
    begin
        if rising_edge(clk) then
            if (rst = '1') then
                state <= s_bram_update;

                --wea                <= (others => '0');
                addra              <= (others => '0');
                readout_active_reg <= '0';
                valid_reg          <= '0';
                ready_reg          <= '1';
                loop_counter       <= x"0000";


            else
                case state is
                    when s_init_readout =>
                        -- prepare to read out data from BRAM addresses
                        -- read_addr_start_i to read_addr_stop_i (inclusive)
                        state <= s_wait;
                        if ready_i = '1' then
                            if read_addr_start_i = read_addr_stop_i then
                                state <= s_readout_complete;
                            end if;
                        end if;
                        valid_reg          <= '1';
                        readout_active_reg <= '1';
                        ready_reg          <= '0';


                    when s_fetch_next =>
                        ready_reg <= '0';
                        -- fetch the next byte from BRAM
                        if addra = read_addr_stop_i then
                            state <= s_readout_complete;
                        else
                            if ready_i = '1' and valid_reg = '1' then
                                --if (ready_i = '1') then -- MRMW valid should not control flow IMHO
                                addra <= std_logic_vector(unsigned(addra) + to_unsigned(1, addra'length));
                                state <= s_fetch_next;
                            else
                                state <= s_wait;
                            end if;
                        end if;
                        valid_reg <= '1';
                        if addra = read_addr_stop_i then
                            valid_reg <= '0';
                        end if;


                    when s_wait =>
                        -- wait for the receiving FSM to become ready
                        ready_reg <= '0';
                        if ready_i = '1' then
                            valid_reg <= '0'; --MRMW: Fix this! valid should not need dropping
                            --to regulate flow, this should be done by ready_i.
                            addra     <= std_logic_vector(unsigned(addra) + to_unsigned(1, addra'length));
                            if addra = read_addr_stop_i then
                                state <= s_readout_complete;
                            else
                                state <= s_fetch_next;
                            end if;
                        else
                            state <= s_wait;
                        end if;


                    when s_readout_complete =>
                        -- BRAM readout is finished
                        state <= s_bram_update;
                        ready_reg          <= '0';
                        valid_reg          <= '0';
                        readout_active_reg <= '0';
                        if (loop_active = '1') then
                            loop_counter       <= loop_counter - 1;
                            readout_active_reg <= '1';
                        end if;


                    when s_bram_update =>
                        -- wait for the commands to update BRAM contents
                        -- (or for another readout to be initiated)
                        if (start_pulse_i = '1') or (loop_active = '1') then
                            state <= s_init_readout;
                        else
                            state <= s_bram_update;
                        end if;

                        readout_active_reg <= '0';
                        ready_reg          <= '1';
                        if (loop_active = '0') then
                            if set_write_addr_pulse_i = '1' then
                                -- move the write pointer
                                addra <= write_addr_start_i;
                            end if;
                            if valid_i = '1' then
                                -- advance the write pointer
                                addra <= std_logic_vector(unsigned(addra) + to_unsigned(1, addra'length));
                            end if;
                            if start_pulse_i = '1' then
                                loop_counter <= loops_prime;
                                addra        <= read_addr_start_i;
                                ready_reg    <= '0';
                            end if;
                        else  -- if (loop_active = '1') then
                            ready_reg          <= '0';
                            addra              <= read_addr_start_i;
                            readout_active_reg <= '1';
                        end if;


                    when others =>
                        report "Invalid FSM state of playback_controller" severity failure;

                end case;
            end if;
        end if;
    end process;



    -- BRAM instantiation for FLX712
    -- TODO: Versal should use UltraRam with 16 bit address, with 64*8*1024 bits of memory
    xpm_memory_spram_inst : xpm_memory_spram
        generic map (  -- @suppress "Generic map uses default values. Missing optional actuals: USE_MEM_INIT_MMI, CASCADE_HEIGHT, SIM_ASSERT_CHK, WRITE_PROTECT"
            ADDR_WIDTH_A        => playback_controller_address'length,           -- DECIMAL
            AUTO_SLEEP_TIME     => 0,         -- DECIMAL
            BYTE_WRITE_WIDTH_A  => 8,         -- DECIMAL
            ECC_MODE            => "no_ecc",  -- String
            MEMORY_INIT_FILE    => "none",    -- String
            MEMORY_INIT_PARAM   => "0",       -- String
            MEMORY_OPTIMIZATION => "true",    -- String
            MEMORY_PRIMITIVE    => f_mem_type(USE_ULTRARAM),                     -- String
            MEMORY_SIZE         => (2**playback_controller_address'length) * 8,  -- Number of bits
            MESSAGE_CONTROL     => 0,         -- DECIMAL
            READ_DATA_WIDTH_A   => 8,         -- DECIMAL
            READ_LATENCY_A      => 1,         -- DECIMAL
            READ_RESET_VALUE_A  => "0",       -- String
            RST_MODE_A          => "SYNC",    -- String
            USE_MEM_INIT        => 1,         -- DECIMAL
            WAKEUP_TIME         => "disable_sleep",                              -- String
            WRITE_DATA_WIDTH_A  => 8,         -- DECIMAL
            WRITE_MODE_A        => "write_first"                                 -- String
        )
        port map (
            sleep          => '0',            -- 1-bit input: sleep signal to enable the dynamic power saving feature.
            clka           => clk,            -- 1-bit input: Clock signal for port A.
            rsta           => '0',            -- 1-bit input: Reset signal for the final port A output register
            ena            => '1',  -- 1-bit input: Memory enable signal for port A. Must be high on clock-- cycles when read or write operations are initiated. Pipelined-- internally.
            regcea         => '1',            -- 1-bit input: Clock Enable for the last register stage on the output
            wea            => wea,  -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector-- for port A input data port dina. 1 bit wide when word-wide writes-- are used. In byte-wide write configurations, each bit controls the-- writing one byte of dina to address addra. For example, to-- synchronously write only bits [15-8] of dina when WRITE_DATA_WIDTH_A-- is 32, wea would be 4'b0010.
            addra          => addra,          -- ADDR_WIDTH_A-bit input: Address for port A write and read operations.
            dina           => data_i,         -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
            injectsbiterra => '0',            -- 1-bit input: Controls single bit error injection on input data when
            injectdbiterra => '0',            -- 1-bit input: Controls double bit error injection on input data when
            douta          => data_reg,       -- READ_DATA_WIDTH_A-bit output: Data output for port A read operations.
            sbiterra       => open,           -- 1-bit output: Status signal to indicate single bit error occurrence
            dbiterra       => open            -- 1-bit output: Status signal to indicate double bit error occurrence
        );



end architecture RTL;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.strips_package.all;
    use work.pcie_package.all;
    use work.lcb_regmap_package.all;
    use work.pcie_package.bitfield_global_strips_config_w_type;

entity lcb_wrapper is
    generic(
        USE_ULTRARAM : boolean
    );
    port(
        clk        : in std_logic;
        rst        : in std_logic;
        -- TTC signals
        bcr_i      : in std_logic;  -- BCR signal --AP: This signal needs to be derived from LTI Turn singal, not BCID.
        l0a_i      : in std_logic;                     -- L0A signal
        l0a_trig_i : in std_logic_vector(3 downto 0);
        l0id_i     : in std_logic_vector(6 downto 0);  -- L0tag
        sync_i     : in std_logic;                     --AP: ITk sync pulse from the trigger tage generator

        -- link configuration data source
        config_data_i  : in  std_logic_vector(7 downto 0);  -- elink data
        config_valid_i : in  std_logic;                     -- elink data is valid this clk cycle
        config_ready_o : out std_logic;                     -- this module is ready for the next elink data

        -- trickle configuration data source
        trickle_data_i  : in  std_logic_vector(7 downto 0);  -- elink data
        trickle_valid_i : in  std_logic;                     -- elink data is valid this clk cycle
        trickle_ready_o : out std_logic;                     -- this module is ready for the next elink data

        -- elink command data source
        command_data_i  : in  std_logic_vector(7 downto 0);  -- elink data
        command_valid_i : in  std_logic;                     -- elink data is valid this clk cycle
        command_ready_o : out std_logic;                     -- this module is ready for the next elink data

        -- configuration from global register map
        config : in t_strips_config;        -- @suppress "Port name 'config' is a keyword in Verilog and may cause problems in mixed language projects"

        -- debug / auxiliary signals
        decoder_idle_o           : out std_logic;                      -- command decoder is idle
        error_count_o            : out std_logic_vector(15 downto 0);  -- number of errors encountered by the command decoder since last rst
        frame_start_pulse_o      : out std_logic;                      -- marks beginning of the LCB frame (new frame will be loaded next CLK cycle)
        ttc_l0a_frame_o          : out std_logic_vector(11 downto 0);  -- pre-encoded version of L0A frame originated from TTC
        ttc_l0a_frame_valid_o    : out std_logic;                      -- is TTC l0a frame valid this clk cycle
        trickle_trigger_gating_o : out std_logic;                      -- trigger BC gating signal
        trickle_trigger_o        : out std_logic;                      -- combined trickle trigger signal
        register_guard_o         : out std_logic;                      -- trickle register gating signal
        regmap_o                 : out t_register_map;                 --output for simulation
        -- data output for the device
        encoded_frame_o          : out std_logic_vector(15 downto 0);
        edata_o                  : out std_logic_vector(3 downto 0)
    );
end entity lcb_wrapper;

architecture RTL of lcb_wrapper is

    -- LCB frame generator signals
    signal lcb_gen_cmd                : t_lcb_command;                  -- which command to execute
    signal lcb_gen_cmd_start_pulse    : std_logic;                      -- pulse for 1 clk cycle to start command
    signal lcb_gen_fast_cmd_data      : std_logic_vector(5 downto 0);   -- bc & cmd
    signal lcb_gen_l0a_data           : std_logic_vector(11 downto 0);  -- bcr & L0A mask & L0 tag
    signal lcb_gen_abc_id             : std_logic_vector(3 downto 0);   -- ABC* address (0xF = broadcast)
    signal lcb_gen_hcc_id             : std_logic_vector(3 downto 0);   -- HCC* address (0xF = broadcast)
    signal lcb_gen_reg_addr           : std_logic_vector(7 downto 0);   -- register address for reg. read/write
    signal lcb_gen_reg_data           : std_logic_vector(31 downto 0);  -- register data for reg. write
    signal lcb_gen_ready              : std_logic;                      -- indicates it's ready for a new command
    signal lcb_frame                  : std_logic_vector(12 downto 0);  -- FIFO data out
    signal lcb_frame_rd_en            : std_logic;                      -- FIFO rd_en signal
    signal lcb_frame_empty            : std_logic;                      -- FIFO empty signal
    signal lcb_frame_almost_empty     : std_logic;                      -- FIFO contains one word
    signal lcb_frame_fifo_almost_full : std_logic;                      -- FIFO is almost full
    signal abc_mask                   : t_abc_mask;
    signal edata_s                    : std_logic_vector(3 downto 0);

    signal ttc_generate_gating_enable : std_logic;

    signal regmap       : t_register_map;
    signal regmap_wr_en : std_logic;
    signal regmap_data  : std_logic_vector(t_register_data'range);
    signal regmap_addr  : std_logic_vector(7 downto 0);

    signal bypass_frame       : std_logic_vector(15 downto 0);
    signal bypass_frame_ready : std_logic;
    signal bypass_frame_valid : std_logic;

    signal trickle_data_o             : std_logic_vector(playback_controller_data'range);
    signal trickle_valid_o            : std_logic;
    signal trickle_ready_i            : std_logic;
    signal trickle_trigger_start      : std_logic;
    signal trickle_allow_register_cmd : std_logic;
    signal trickle_trigger_gating     : std_logic;

    -- L0A generator signals
    signal ttc_l0a_frame     : std_logic_vector(11 downto 0);
    signal frame_start_pulse : std_logic;

    -- serializer registers
    signal serializer_reg    : std_logic_vector(encoded_frame_o'range) := (others => '0');
    signal encoded_frame_reg : std_logic_vector(encoded_frame_o'range);

    -- bypass command and firmware encoded command lines
    signal encoding          : std_logic;
    signal encoded_cmd       : std_logic_vector(7 downto 0);
    signal encoded_cmd_valid : std_logic;
    signal encoded_cmd_ready : std_logic;

    signal bypass_cmd       : std_logic_vector(7 downto 0);
    signal bypass_cmd_valid : std_logic;
    signal bypass_cmd_ready : std_logic;

    -- edata_o fine delay types and registers
    signal edata_delay     : std_logic_vector(1 downto 0);
    type t_edata_array is array (natural range <>) of std_logic_vector(3 downto 0);
    signal edata_delay_reg : t_edata_array(1 to 3) := (others => (others => '0'));

    signal invert_output : boolean := false;

--constant zero_byte : std_logic_vector(7 downto 0) := (others => '0');
begin

    encoded_frame_o       <= encoded_frame_reg;
    trickle_trigger_o     <= trickle_trigger_start;
    register_guard_o      <= trickle_allow_register_cmd;
    trickle_trigger_start <= regmap(TRICKLE_TRIGGER_PULSE)(0)
                             or config.GLOBAL_TRICKLE_TRIGGER(config.GLOBAL_TRICKLE_TRIGGER'low)
                             or regmap(TRICKLE_TRIGGER_RUN)(0);

    edata_delay <= regmap(L0A_FRAME_DELAY)(edata_delay'range);

    frame_start_pulse_o      <= frame_start_pulse;
    ttc_l0a_frame_o          <= ttc_l0a_frame;
    ttc_l0a_frame_valid_o    <= ttc_l0a_frame(11) or ttc_l0a_frame(10) or ttc_l0a_frame(9) or ttc_l0a_frame(8) or ttc_l0a_frame(7);
    trickle_trigger_gating_o <= trickle_trigger_gating;

    invert_output <= config.GLOBAL_STRIPS_CONFIG.INVERT_LCB_OUT(
                                                                config.GLOBAL_STRIPS_CONFIG.INVERT_LCB_OUT'low) = '1';

    ttc_l0a_generator : entity work.l0a_frame_generator
        port map(
            clk                 => clk,
            rst                 => rst,
            bcr_i               => bcr_i,
            l0a_i               => l0a_i,
            l0a_trig_i          => l0a_trig_i,
            bcr_delay_i         => unsigned(regmap(TTC_BCR_DELAY)(t_bcid'range)),  --AP: THis needs to be a global FELIX tegister
            frame_phase_i       => unsigned(regmap(L0A_FRAME_PHASE)(1 downto 0)),  --AP: This needs to be done somewhere else
            l0id_i              => l0id_i,
            frame_start_pulse_o => frame_start_pulse,
            ITk_sync_i          => sync_i,
            l0a_frame_o         => ttc_l0a_frame
        );

    encoding <= regmap(ENCODING_ENABLE)(0);

    cmd_destination_selector_mux :
    process(encoding, command_data_i, command_valid_i, bypass_cmd_ready, encoded_cmd_ready) is
    begin
        if encoding = '0' then
            encoded_cmd       <= (others => '0');
            encoded_cmd_valid <= '0';
            bypass_cmd        <= command_data_i;
            bypass_cmd_valid  <= command_valid_i;
            command_ready_o   <= bypass_cmd_ready;
        else
            encoded_cmd       <= command_data_i;
            encoded_cmd_valid <= command_valid_i;
            bypass_cmd        <= (others => '0');
            bypass_cmd_valid  <= '0';
            command_ready_o   <= encoded_cmd_ready;
        end if;
    end process;

    bypass_frame_generator : entity work.strips_bypass_frame_aggregator
        generic map(
            TIMEOUT => 4000
        )
        port map(
            clk           => clk,
            rst           => rst,
            byte_i        => bypass_cmd,
            byte_valid_i  => bypass_cmd_valid,
            byte_ready_o  => bypass_cmd_ready,
            frame_o       => bypass_frame,
            frame_valid_o => bypass_frame_valid,
            frame_ready_i => bypass_frame_ready
        );

    configuration_decoder : entity work.strips_configuration_decoder
        generic map(
            TIMEOUT => 4000
        )
        port map(
            clk            => clk,
            rst            => rst,
            config_data_i  => config_data_i,
            config_valid_i => config_valid_i,
            config_ready_o => config_ready_o,
            regmap_wr_en_o => regmap_wr_en,
            regmap_data_o  => regmap_data,
            regmap_addr_o  => regmap_addr
        );

    command_decoder : entity work.lcb_command_decoder
        generic map(
            TIMEOUT => 4000
        )
        port map(
            clk                          => clk,
            rst                          => rst,
            elink_data_i                 => encoded_cmd,
            elink_valid_i                => encoded_cmd_valid,
            elink_ready_o                => encoded_cmd_ready,
            trickle_data_i               => trickle_data_o,
            trickle_valid_i              => trickle_valid_o,
            trickle_ready_o              => trickle_ready_i,
            lcb_cmd_o                    => lcb_gen_cmd,
            lcb_cmd_start_pulse_o        => lcb_gen_cmd_start_pulse,
            lcb_fast_cmd_data_o          => lcb_gen_fast_cmd_data,
            lcb_l0a_data_o               => lcb_gen_l0a_data,
            lcb_abc_id_o                 => lcb_gen_abc_id,
            lcb_hcc_id_o                 => lcb_gen_hcc_id,
            lcb_reg_addr_o               => lcb_gen_reg_addr,
            lcb_reg_data_o               => lcb_gen_reg_data,
            lcb_ready_i                  => lcb_gen_ready,
            lcb_frame_fifo_almost_full_i => lcb_frame_fifo_almost_full,
            decoder_idle_o               => decoder_idle_o,
            error_count_o                => error_count_o
        );

    register_map : entity work.lcb_regmap
        port map (
            clk      => clk,
            rst      => rst,
            wr_en    => regmap_wr_en,
            data_i   => regmap_data,
            addr_i   => regmap_addr,
            regmap_o => regmap
        );

    regmap_o <= regmap;

    playback_controller : entity work.playback_controller
        generic map(
            USE_ULTRARAM => USE_ULTRARAM
        )
        port map(
            clk                    => clk,
            rst                    => rst,
            start_pulse_i          => trickle_trigger_start,
            readout_active_o       => open,
            playback_loops_i       => regmap(PLAYBACK_LOOPS),
            data_i                 => trickle_data_i,
            valid_i                => trickle_valid_i,
            ready_o                => trickle_ready_o,
            set_write_addr_pulse_i => regmap(TRICKLE_SET_WRITE_ADDR_PULSE)(0),
            write_addr_start_i     => regmap(TRICKLE_WRITE_ADDR)(playback_controller_address'range),
            read_addr_start_i      => regmap(TRICKLE_DATA_START)(playback_controller_address'range),
            read_addr_stop_i       => regmap(TRICKLE_DATA_END)(playback_controller_address'range),
            data_o                 => trickle_data_o,
            valid_o                => trickle_valid_o,
            ready_i                => trickle_ready_i
        );

    abc_mask(15) <= regmap(ABC_MASK_F);
    abc_mask(14) <= regmap(ABC_MASK_E);
    abc_mask(13) <= regmap(ABC_MASK_D);
    abc_mask(12) <= regmap(ABC_MASK_C);
    abc_mask(11) <= regmap(ABC_MASK_B);
    abc_mask(10) <= regmap(ABC_MASK_A);
    abc_mask(9)  <= regmap(ABC_MASK_9);
    abc_mask(8)  <= regmap(ABC_MASK_8);
    abc_mask(7)  <= regmap(ABC_MASK_7);
    abc_mask(6)  <= regmap(ABC_MASK_6);
    abc_mask(5)  <= regmap(ABC_MASK_5);
    abc_mask(4)  <= regmap(ABC_MASK_4);
    abc_mask(3)  <= regmap(ABC_MASK_3);
    abc_mask(2)  <= regmap(ABC_MASK_2);
    abc_mask(1)  <= regmap(ABC_MASK_1);
    abc_mask(0)  <= regmap(ABC_MASK_0);

    g_modulemasksen : if (CFG_INCLUDE_ASIC_MASKS = '1') generate
        frame_generator : entity work.lcb_frame_generator
            port map(
                clk                          => clk,
                rst                          => rst,
                cmd_i                        => lcb_gen_cmd,
                cmd_start_pulse_i            => lcb_gen_cmd_start_pulse,
                fast_cmd_data_i              => lcb_gen_fast_cmd_data,
                hcc_mask_i                   => regmap(HCC_MASK),
                abc_mask_i                   => abc_mask,
                l0a_data_i                   => lcb_gen_l0a_data,
                abc_id_i                     => lcb_gen_abc_id,
                hcc_id_i                     => lcb_gen_hcc_id,
                reg_addr_i                   => lcb_gen_reg_addr,
                reg_data_i                   => lcb_gen_reg_data,
                ready_o                      => lcb_gen_ready,
                lcb_frame_o                  => lcb_frame,
                lcb_frame_i_rd_en            => lcb_frame_rd_en,
                lcb_frame_o_empty            => lcb_frame_empty,
                lcb_frame_o_almost_empty     => lcb_frame_almost_empty,
                lcb_frame_fifo_almost_full_o => lcb_frame_fifo_almost_full
            );
    else generate --CFG_INCLUDE_ASIC_MASKS = '0'
        frame_generator : entity work.lcb_frame_generator
            port map(
                clk                          => clk,
                rst                          => rst,
                cmd_i                        => lcb_gen_cmd,
                cmd_start_pulse_i            => lcb_gen_cmd_start_pulse,
                fast_cmd_data_i              => lcb_gen_fast_cmd_data,
                hcc_mask_i                   => (others => '0'),
                abc_mask_i                   => (others => (others => '0')),
                l0a_data_i                   => lcb_gen_l0a_data,
                abc_id_i                     => lcb_gen_abc_id,
                hcc_id_i                     => lcb_gen_hcc_id,
                reg_addr_i                   => lcb_gen_reg_addr,
                reg_data_i                   => lcb_gen_reg_data,
                ready_o                      => lcb_gen_ready,
                lcb_frame_o                  => lcb_frame,
                lcb_frame_i_rd_en            => lcb_frame_rd_en,
                lcb_frame_o_empty            => lcb_frame_empty,
                lcb_frame_o_almost_empty     => lcb_frame_almost_empty,
                lcb_frame_fifo_almost_full_o => lcb_frame_fifo_almost_full
            );
    end generate;




    ttc_generate_gating_enable <=
                                  (regmap(GATING_TTC_ENABLE)(0) or config.GLOBAL_STRIPS_CONFIG.TTC_GENERATE_GATING_ENABLE(config.GLOBAL_STRIPS_CONFIG.TTC_GENERATE_GATING_ENABLE'low))
                                  and
                                  not(config.GLOBAL_STRIPS_CONFIG.TTC_GATING_OVERRIDE(config.GLOBAL_STRIPS_CONFIG.TTC_GATING_OVERRIDE'low));
    g_bcidgating_en : if (CFG_INCLUDE_BCID_GATING = '1') generate
        trickle_trigger_generator : entity work.lcb_trickle_trigger
            generic map (guard_window => guard_window_size)                           -- defined in strips_package.vhd
            port map(
                clk                          => clk,                                    -- in
                rst                          => rst,                                    -- in
                en_i                         => ttc_generate_gating_enable,             -- in
                bcr_i                        => bcr_i,                                  -- in
                bc_start_i                   => regmap(GATING_BC_START)(t_bcid'range),  -- in  t_bcid
                bc_stop_i                    => regmap(GATING_BC_STOP)(t_bcid'range),   -- in  t_bcid
                trickle_trigger_o            => trickle_trigger_gating,                 -- out
                trickle_allow_register_cmd_o => trickle_allow_register_cmd              -- out
            );
    else generate -- CFG_INCLUDE_BCID_GATING = '0'
        --ttc_generate_gating_enable <= '0';
        trickle_trigger_gating     <= '0';
        trickle_allow_register_cmd <= '0';
    end generate;


    scheduler : entity work.lcb_scheduler_encoder
        port map(
            clk                          => clk,
            rst                          => rst,
            l0a_frame_i                  => ttc_l0a_frame,      --(11:0)
            lcb_frame_i                  => lcb_frame,          --(12:0)
            lcb_frame_o_rd_en            => lcb_frame_rd_en,    --out
            lcb_frame_i_empty            => lcb_frame_empty,
            lcb_frame_i_almost_empty     => lcb_frame_almost_empty,
            l0a_frame_delay_i            => regmap(L0A_FRAME_DELAY)(3 downto 2),  --(1:0) --AP: I am not sure why this is needed
            bypass_frame_i               => bypass_frame,                         --(15:0)
            bypass_frame_valid_i         => bypass_frame_valid,
            bypass_frame_ready_o         => bypass_frame_ready,
            frame_start_pulse_i          => frame_start_pulse,
            trickle_bc_gating_i          => trickle_trigger_gating,
            trickle_allow_register_cmd_i => trickle_allow_register_cmd,
            trickle_bc_gating_en         => ttc_generate_gating_enable, -- in
            ttc_l0a_en                   => regmap(TTC_L0A_ENABLE)(0),            --AP: I am not sure why this is needed
            encoded_frame_o              => encoded_frame_reg           --(15:0)
        );

    -- serializes LCB frame for 4-bit elink
    serializer : process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' then
                serializer_reg <= (others => '0');
            else
                if frame_start_pulse = '1' then
                    serializer_reg <= encoded_frame_reg;
                else
                    serializer_reg(15 downto 4) <= serializer_reg(11 downto 0);
                end if;
            end if;
        end if;
    end process;

    -- Saves delayed elink data by up to 3 BC CLK cycles
    edata_delay_proc : process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' then
                edata_delay_reg <= (others => (others => '0'));
            else
                edata_delay_reg(1)                         <= serializer_reg(15 downto 12);
                edata_delay_reg(2 to edata_delay_reg'high) <= edata_delay_reg(1 to edata_delay_reg'high - 1);
            end if;
        end if;
    end process;

    -- Outputs elink data with a configurable delay
    edata_output : process(clk) is
        variable delay_int : integer range 0 to 3;
    begin
        if rising_edge(clk) then
            if (rst = '1') then
                edata_s <= (others => '0');
            else
                delay_int := to_integer(unsigned(edata_delay));
                if delay_int = 0 then
                    edata_s <= serializer_reg(15 downto 12);
                else
                    edata_s <= edata_delay_reg(delay_int);
                end if;
            end if;
        end if;
    end process;

    edata_o <= not edata_s when invert_output else edata_s;

end architecture RTL;

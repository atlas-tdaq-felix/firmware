--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : ITk Strips package
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : strips_bypass_frame_aggregator.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Fri Apr 10 16:31:36 2020
-- Last update : Wed Nov 16 12:41:00 2022
-- Platform    : Default Part Number
-- Standard    : VHDL-1993
--------------------------------------------------------------------------------
-- Copyright (c) 2020 BNL
-------------------------------------------------------------------------------
-- Description:
-- Inputs software-encoded 6b8b data
-- Outputs complete 6b8b encoded frames
-- If an odd number of bytes is received, the last byte is discarded
-- after a configurable timeout in BC units
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity strips_bypass_frame_aggregator is
    generic (
        -- last odd byte will be dropped if the rest of the bypass frame
        -- is not received before the timeout (4000 BC = 100 ms)
        TIMEOUT : integer := 4000
    );
    port(
        -- main clock
        clk           : in  std_logic;
        -- synchronous reset
        rst           : in  std_logic;
        -- byte from elink
        byte_i        : in  std_logic_vector(7 downto 0);
        -- elink byte is valid this clk cycle
        byte_valid_i  : in  std_logic;
        -- elink byte can be stored this clk cycle
        byte_ready_o  : out std_logic;
        -- formed frame output
        frame_o       : out std_logic_vector(15 downto 0);
        -- frame is valid this clk cycle
        frame_valid_o : out std_logic;
        -- receiver is ready for the next frame
        frame_ready_i : in  std_logic
    );
end entity strips_bypass_frame_aggregator;

architecture RTL of strips_bypass_frame_aggregator is
    signal frame_valid_reg : std_logic := '0';
    signal byte_ready_reg  : std_logic := '0';
    --signal second_byte     : std_logic := '0';
    signal frame_reg    : std_logic_vector(frame_o'range) := (others => '0');
    signal timeout_reg    : unsigned(15 downto 0);

    type t_state is (s_init, s_wait_1st_byte, s_wait_2nd_byte, s_output);
    signal state : t_state := s_wait_1st_byte;

--constant zero_byte : std_logic_vector(byte_i'range) := (others => '0');

begin
    frame_valid_o <= frame_valid_reg;
    byte_ready_o  <= byte_ready_reg;
    frame_o <= frame_reg;

    state_update : process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' then
                state <= s_init;
            else
                case state is
                    when s_init =>
                        state <= s_wait_1st_byte;

                    when s_wait_1st_byte =>
                        if byte_ready_reg = '1' and byte_valid_i = '1' then
                            -- received 1st byte of the bypass frame
                            state <= s_wait_2nd_byte;
                        end if;

                    when s_wait_2nd_byte =>
                        if byte_ready_reg = '1' and byte_valid_i = '1' then
                            -- received 2nd byte of the bypass frame
                            state <= s_output;
                        elsif timeout_reg = to_unsigned(TIMEOUT, timeout_reg'length) then
                            -- timeout occured before the 2nd byte was received
                            state <= s_wait_1st_byte;
                        end if;

                    when s_output =>
                        if frame_valid_reg = '1' and frame_ready_i = '1' then
                            -- scheduler accepted the bypass frame
                            state <= s_wait_1st_byte;
                        end if;

                    when others =>
                        report "Invalid FSM state of frame bypass module (state control)" severity failure;
                end case;
            end if;
        end if;
    end process;


    variable_update : process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' then
                frame_valid_reg <= '0';
                byte_ready_reg <= '1';
                frame_reg <= (others => '0');
                timeout_reg <= (others => '0');
            else
                case state is
                    when s_init =>
                        frame_valid_reg <= '0';
                        byte_ready_reg <= '1';
                        frame_reg <= (others => '0');
                        timeout_reg <= (others => '0');

                    when s_wait_1st_byte =>
                        if byte_ready_reg = '1' and byte_valid_i = '1' then
                            -- received 1st byte of the bypass frame
                            frame_reg(15 downto 8) <= byte_i;
                            timeout_reg <= (others => '0');
                            byte_ready_reg <= '1';
                        end if;

                    when s_wait_2nd_byte =>
                        timeout_reg <= timeout_reg + to_unsigned(1, timeout_reg'length);
                        if byte_ready_reg = '1' and byte_valid_i = '1' then
                            -- received 2nd byte of the bypass frame
                            frame_reg(7 downto 0) <= byte_i;
                            frame_valid_reg <= '1';
                            byte_ready_reg <= '0';

                        elsif timeout_reg = to_unsigned(TIMEOUT, timeout_reg'length) then
                            -- timeout occured before the 2nd byte was received
                            frame_valid_reg <= '0';
                            byte_ready_reg <= '1';
                        end if;

                    when s_output =>
                        if frame_valid_reg = '1' and frame_ready_i = '1' then
                            -- scheduler accepted the bypass frame
                            byte_ready_reg <= '1';
                            frame_valid_reg <= '0';
                        end if;

                    when others =>
                        report "Invalid FSM state of frame bypass module (state control)" severity failure;
                end case;
            end if;
        end if;
    end process;

end architecture RTL;

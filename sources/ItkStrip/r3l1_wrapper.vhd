--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.strips_package.all;
    use work.r3l1_regmap_package.all;

entity r3l1_wrapper is
    port(
        clk                 : in  std_logic;
        rst                 : in  std_logic;
        -- TTC signals
        bcr_i               : in  std_logic; -- BCR signal
        l1_trigger_i        : in  std_logic_vector(3 downto 0); --  sihfted L0 trigger signal
        l1_tag_i            : in  std_logic_vector(6 downto 0); -- L0tag
        -- RoIE signals
        r3_trigger_i        : in  std_logic; -- R3 trigger signal
        r3_tag_i            : in  std_logic_vector(6 downto 0); -- L0tag
        module_mask_i       : in  std_logic_vector(4 downto 0); -- module mask
        -- link configuration source
        config_data_i       : in  std_logic_vector(7 downto 0); -- elink data
        config_valid_i      : in  std_logic; -- elink data is valid this clk cycle
        config_ready_o      : out std_logic; -- this module is ready for the next elink data
        -- elink command source
        command_data_i      : in  std_logic_vector(7 downto 0); -- elink data
        command_valid_i     : in  std_logic; -- elink data is valid this clk cycle
        command_ready_o     : out std_logic; -- this module is ready for the next elink data
        -- configuration from global register map
        config              : in  t_strips_config; --! configuration settings -- @suppress "Port name 'config' is a keyword in Verilog and may cause problems in mixed language projects"
        -- data output for the device
        encoded_frame_o     : out std_logic_vector(15 downto 0);
        edata_o             : out std_logic_vector(3 downto 0);
        -- debug / auxiliary signals
        frame_start_pulse_o : out std_logic -- marks beginning of the LCB frame (new frame will be loaded next CLK cycle)
    );
end entity r3l1_wrapper;

architecture RTL of r3l1_wrapper is
    -- R3 frame FIFO signals
    signal r3_frame                  : STD_LOGIC_VECTOR(11 downto 0); -- FIFO data out
    signal r3_frame_rd_en            : std_logic; -- FIFO rd_en signal
    signal r3_frame_empty            : std_logic; -- FIFO empty signal
    signal r3_frame_almost_empty     : std_logic; -- FIFO contains one word

    -- L1 frame FIFO signals
    signal l1_frame                  : STD_LOGIC_VECTOR(11 downto 0); -- FIFO data out
    signal l1_frame_rd_en            : std_logic; -- FIFO rd_en signal
    signal l1_frame_empty            : std_logic; -- FIFO empty signal
    signal l1_frame_almost_empty     : std_logic; -- FIFO contains one word

    -- bypass frame signals
    signal bypass_frame       : STD_LOGIC_VECTOR(15 downto 0);
    signal bypass_frame_ready : std_logic;
    signal bypass_frame_valid : std_logic;

    -- Frame synchronizer generator signals
    signal frame_start_pulse : std_logic;

    -- serializer registers
    signal serializer_reg    : std_logic_vector(encoded_frame_o'range) := (others => '0');
    signal encoded_frame_reg : std_logic_vector(encoded_frame_o'range);
    signal edata_s            : std_logic_vector(3 downto 0);

    signal invert_output : boolean := False;

    constant zero_module_mask : std_logic_vector(module_mask_i'range) := (others => '0');

    signal regmap        : t_register_map := (others => (others => '0'));
    signal regmap_wr_en    :std_logic;
    signal regmap_data     :std_logic_vector(t_register_data'range);
    signal regmap_addr     :std_logic_vector(7 downto 0);
begin

    invert_output     <= config.GLOBAL_STRIPS_CONFIG.INVERT_R3L1_OUT(
                                                                     config.GLOBAL_STRIPS_CONFIG.INVERT_R3L1_OUT'low) = '1';

    encoded_frame_o     <= encoded_frame_reg;
    frame_start_pulse_o <= frame_start_pulse;

    frame_synchronizer : entity work.r3l1_frame_synchronizer
        port map(
            clk                 => clk,
            bcr_i               => bcr_i,
            frame_phase_i       => unsigned(regmap(L0A_FRAME_PHASE)(1 downto 0)),
            frame_start_pulse_o => frame_start_pulse
        );

    configuration_decoder : entity work.strips_configuration_decoder
        generic map(
            TIMEOUT => 4000
        )
        port map(
            clk           => clk,
            rst             => rst,
            config_data_i   => config_data_i,
            config_valid_i  => config_valid_i,
            config_ready_o  => config_ready_o,
            regmap_wr_en_o  => regmap_wr_en,
            regmap_data_o   => regmap_data,
            regmap_addr_o   => regmap_addr
        );

    regmap_controller : entity work.r3l1_regmap
        port map (
            clk     => clk,
            rst     => rst,
            wr_en      => regmap_wr_en,
            data_i     => regmap_data,
            addr_i     => regmap_addr,
            regmap_o   => regmap
        );

    L1_generator : entity work.r3l1_frame_generator
        port map(
            clk                  => clk,
            rst                  => rst,
            enable_i             => regmap(L1_ENABLE)(0),
            trigger_i            => l1_trigger_i(3),
            module_mask_i        => zero_module_mask,
            tag_i                => l1_tag_i,
            frame_o              => l1_frame,
            frame_rd_en_i        => l1_frame_rd_en,
            frame_empty_o        => l1_frame_empty,
            frame_almost_empty_o => l1_frame_almost_empty
        --frame_overflow_o     => open
        );

    R3_generator : entity work.r3l1_frame_generator
        port map(
            clk                  => clk,
            rst                  => rst,
            enable_i             => regmap(R3_ENABLE)(0),
            trigger_i            => r3_trigger_i,
            module_mask_i        => module_mask_i,
            tag_i                => r3_tag_i,
            frame_o              => r3_frame,
            frame_rd_en_i        => r3_frame_rd_en,
            frame_empty_o        => r3_frame_empty,
            frame_almost_empty_o => r3_frame_almost_empty
        --frame_overflow_o     => open
        );

    bypass_frame_generator : entity work.strips_bypass_frame_aggregator
        generic map(
            TIMEOUT => 4000
        )
        port map(
            clk           => clk,
            rst           => rst,
            byte_i        => command_data_i,
            byte_valid_i  => command_valid_i,
            byte_ready_o  => command_ready_o,
            frame_o       => bypass_frame,
            frame_valid_o => bypass_frame_valid,
            frame_ready_i => bypass_frame_ready
        );

    scheduler : entity work.r3l1_scheduler_encoder
        port map(
            clk                     => clk,
            rst                     => rst,
            r3_frame_i              => r3_frame,
            r3_frame_o_rd_en        => r3_frame_rd_en,
            r3_frame_i_empty        => r3_frame_empty,
            r3_frame_i_almost_empty => r3_frame_almost_empty,
            l1_frame_i              => l1_frame,
            l1_frame_o_rd_en        => l1_frame_rd_en,
            l1_frame_i_empty        => l1_frame_empty,
            l1_frame_i_almost_empty => l1_frame_almost_empty,
            bypass_frame_i          => bypass_frame,
            bypass_frame_valid_i    => bypass_frame_valid,
            bypass_frame_ready_o    => bypass_frame_ready,
            frame_start_pulse_i     => frame_start_pulse,
            encoded_frame_o         => encoded_frame_reg
        );

    serializer : process(clk) is
    begin
        if rising_edge(clk) then
            if (rst = '1') then
                edata_s        <= (others => '0');
                serializer_reg <= (others => '0');
            else
                edata_s <= serializer_reg(15 downto 12);
                if (frame_start_pulse = '1') then
                    serializer_reg <= encoded_frame_reg;
                else
                    serializer_reg(15 downto 4) <= serializer_reg(11 downto 0);
                end if;
            end if;
        end if;
    end process;

    edata_o  <= not edata_s when invert_output else edata_s;
end architecture RTL;

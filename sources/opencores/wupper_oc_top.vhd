--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               RHabraken
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.


--!------------------------------------------------------------------------------
--!
--!           NIKHEF - National Institute for Subatomic Physics
--!
--!                       Electronics Department
--!
--!-----------------------------------------------------------------------------
--! @class virtex7_dma_top
--!
--!
--! @author      Andrea Borga    (andrea.borga@nikhef.nl)<br>
--!              Frans Schreuder (frans.schreuder@nikhef.nl)
--!
--!
--! @date        07/01/2015    created
--!
--! @version     1.1
--!
--! @brief
--! Top level design containing a simple application and the PCIe DMA
--! core
--!
--!
--! 11/19/2015 B. Kuschak <brian@skybox.com>
--!          Modifications for KCU105.
--!
--!
--!-----------------------------------------------------------------------------
--! @TODO
--!
--!
--! ------------------------------------------------------------------------------
--
--! @brief ieee



library ieee, UNISIM;
    use ieee.numeric_std.all;
    use UNISIM.VCOMPONENTS.all;
    use ieee.std_logic_unsigned.all;-- @suppress "Deprecated package"
    use ieee.std_logic_1164.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;

entity wupper_oc_top is
    generic(
        NUMBER_OF_INTERRUPTS  : integer := 8;
        NUMBER_OF_DESCRIPTORS : integer := 5;
        CARD_TYPE             : integer := 155;
        BUILD_DATETIME        : std_logic_vector(39 downto 0) := x"0000FE71CE";
        GIT_HASH              : std_logic_vector(159 downto 0) := x"0000000000000000000000000000000000000000";
        COMMIT_DATETIME       : std_logic_vector(39 downto 0) := x"0000FE71CE";
        GIT_TAG               : std_logic_vector(127 downto 0) := x"00000000000000000000000000000000";
        GIT_COMMIT_NUMBER     : integer := 0;
        PCIE_LANES            : integer := 8;
        DATA_WIDTH            : integer := 1024;
        ENDPOINTS             : integer := 2;
        USE_VERSAL_CPM        : boolean := true);
    port (
        leds        : out    std_logic_vector(NUM_LEDS(CARD_TYPE)-1 downto 0);
        pcie_rxn    : in     std_logic_vector((ENDPOINTS*PCIE_LANES)-1 downto 0);
        pcie_rxp    : in     std_logic_vector((ENDPOINTS*PCIE_LANES)-1 downto 0);
        pcie_txn    : out    std_logic_vector((ENDPOINTS*PCIE_LANES)-1 downto 0);
        pcie_txp    : out    std_logic_vector((ENDPOINTS*PCIE_LANES)-1 downto 0); --! PCIe link lanes
        sys_clk_n   : in     std_logic_vector(ENDPOINTS-1 downto 0);
        sys_clk_p   : in     std_logic_vector(ENDPOINTS-1 downto 0); --! 100MHz PCIe reference clock
        SDA         : inout  std_logic;
        SCL         : inout  std_logic;
        i2cmux_rst  : out    std_logic_vector(NUM_I2C_MUXES(CARD_TYPE)-1 downto 0);
        sys_reset_n : in     std_logic; --PCIE PERSTn pin.
        DDR_in : in DDR_in_array_type(0 to NUM_DDR(CARD_TYPE)-1);
        DDR_out : out DDR_out_array_type(0 to NUM_DDR(CARD_TYPE)-1);
        DDR_inout : inout DDR_inout_array_type(0 to NUM_DDR(CARD_TYPE)-1);
        LPDDR_in                  : in     LPDDR_in_array_type(0 to NUM_LPDDR(CARD_TYPE)/2-1);
        LPDDR_out                 : out    LPDDR_out_array_type(0 to NUM_LPDDR(CARD_TYPE)-1);
        LPDDR_inout               : inout  LPDDR_inout_array_type(0 to NUM_LPDDR(CARD_TYPE)-1)
    );
end entity wupper_oc_top;


architecture structure of wupper_oc_top is

    signal leds_s                              : std_logic_vector(ENDPOINTS*8-1 downto 0);
    signal pll_locked                          : std_logic; -- @suppress "signal pll_locked is never read"
    signal appreg_clk                          : std_logic;
    signal register_map_hk_monitor : register_map_hk_monitor_type; -- @suppress "signal register_map_hk_monitor is never written"
    signal register_map_ttc_monitor : register_map_ttc_monitor_type; -- @suppress "signal register_map_ttc_monitor is never written"
    signal register_map_control             : register_map_control_type; --! contains all read/write registers that control the application. The record members are described in pcie_package.vhd -- @suppress "signal register_map_control is never read"
    signal reset_soft : std_logic; -- @suppress "signal reset_soft is never read"
    signal reset_hard : std_logic;
    signal lnk_up : std_logic_vector(1 downto 0); -- @suppress "signal lnk_up is never read"
    signal RXUSRCLK : std_logic_vector(24*ENDPOINTS-1 downto 0);
    signal PCIE_PERSTn: std_logic;


    signal WupperToCPM: WupperToCPM_array_type(0 to 1);
    signal CPMToWupper: CPMToWupper_array_type(0 to 1);
    --signal clk100: std_logic; -- @suppress "signal clk100 is never read"

    function USE_ULTRARAM(c: integer)return boolean is
    begin
        if c = 128 or c = 800 or c = 801 or c = 180 or c = 181 or c = 182 or c = 155 or c = 120 then
            return true;
        else
            return false;
        end if;
    end function;
    signal register_map_gen_board_info : register_map_gen_board_info_type;
    signal Versal_network_device_fromHost_full : STD_LOGIC;
    signal Versal_network_device_fromHost_prog_full : STD_LOGIC;
    signal Versal_network_device_toHost_empty : STD_LOGIC;
    signal Versal_network_device_toHost_prog_empty : STD_LOGIC;
    signal Versal_network_device_tohost_dout : STD_LOGIC_VECTOR ( 31 downto 0 );
    signal Versal_network_device_fromHost_din : STD_LOGIC_VECTOR ( 31 downto 0 );
    signal Versal_network_device_fromHost_wr_en : STD_LOGIC;
    signal Versal_network_device_toHost_rd_en : STD_LOGIC;
    signal Versal_network_device_toHost_eop : STD_LOGIC;
    signal Versal_network_device_fromHost_eop : STD_LOGIC;
    signal Versal_network_device_fromHost_set_carrier : std_logic;
    signal Versal_network_device_toHost_status_carrier : std_logic;
    signal versal_sys_reset_n : std_logic;
    signal axi_miso_ttc_lti : axi_miso_type;
    signal axi_mosi_ttc_lti : axi_mosi_type;
    signal apb3_axi_clk : std_logic;
    signal LTI2cips_gpio : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal cips2LTI_gpio : STD_LOGIC_VECTOR ( 3 downto 0 );

begin
    g_versal: if CARD_TYPE = 180 or CARD_TYPE = 155 or CARD_TYPE = 120 generate
        PCIE_PERSTn <= versal_sys_reset_n;
    else generate
        PCIE_PERSTn <= sys_reset_n;
    end generate;

    RXUSRCLK <= (others => appreg_clk);

    hk0: entity work.housekeeping_module
        generic map (
            CARD_TYPE => CARD_TYPE,
            GBT_NUM => 24,
            ENDPOINTS => ENDPOINTS,
            generateTTCemu => false,
            AUTOMATIC_CLOCK_SWITCH => false,
            FIRMWARE_MODE => 0,
            USE_Si5324_RefCLK => false,
            GENERATE_XOFF => false,
            IncludeDecodingEpath2_HDLC => "0000000",
            IncludeDecodingEpath2_8b10b => "0000000",
            IncludeDecodingEpath4_8b10b => "0000000",
            IncludeDecodingEpath8_8b10b => "0000000",
            IncludeDecodingEpath16_8b10b => "0000000",
            IncludeDecodingEpath32_8b10b => "0000000",
            IncludeDirectDecoding => "0000000",
            IncludeEncodingEpath2_HDLC => "00000",
            IncludeEncodingEpath2_8b10b => "00000",
            IncludeEncodingEpath4_8b10b => "00000",
            IncludeEncodingEpath8_8b10b => "00000",
            IncludeDirectEncoding => "00000",
            BLOCKSIZE => 1024,
            DATA_WIDTH => DATA_WIDTH,
            FULL_HALFRATE => false,
            SUPPORT_HDLC_DELAY => false,
            TTC_SYS_SEL => '0'
        )
        port map(
            MMCM_Locked_in => pll_locked,
            MMCM_OscSelect_in => '0',
            SCL => SCL,
            SDA => SDA,
            SI5345_A => open,
            SI5345_INSEL => open,
            SI5345_OE => open,
            SI5345_RSTN => open,
            SI5345_SEL => open,
            SI5345_nLOL => (others => '1'),
            SI5345_FINC_B => open,
            SI5345_FDEC_B => open,
            SI5345_INTR_B => (others => '0'),
            appreg_clk => appreg_clk,
            emcclk => (others => '0'),
            flash_SEL => open,
            flash_a => open,
            flash_a_msb => open,
            flash_adv => open,
            flash_cclk => open,
            flash_ce => open,
            flash_d => open,
            flash_re => open,
            flash_we => open,
            i2cmux_rst => i2cmux_rst,
            TACH => (others => '0'),
            FAN_FAIL_B => (others => '1'),
            FAN_FULLSP => (others => '1'),
            FAN_OT_B => (others => '1'),
            FAN_PWM => open,
            FF3_PRSNT_B => (others => '0'),
            IOEXPAN_INTR_B => (others => '1'),
            IOEXPAN_RST_B => open,
            clk10_xtal => '0',
            clk40_xtal => appreg_clk,
            leds => open,
            opto_inhibit => open,
            register_map_control => register_map_control,
            register_map_gen_board_info => register_map_gen_board_info,
            register_map_hk_monitor => register_map_hk_monitor,
            rst_soft => reset_soft,
            sys_reset_n => sys_reset_n,
            PCIE_PWRBRK => (others => '0'),
            PCIE_WAKE_B => (others => '0'),
            QSPI_RST_B => open,
            rst_hw => reset_hard,
            CLK40_FPGA2LMK_P => open,
            CLK40_FPGA2LMK_N => open,
            LMK_DATA => open,
            LMK_CLK => open,
            LMK_LE => open,
            LMK_GOE => open,
            LMK_LD => (others =>'0'),
            LMK_SYNCn => open,
            I2C_SMB => open,
            I2C_SMBUS_CFG_nEN => open,
            MGMT_PORT_EN => open,
            PCIE_PERSTn_out => open,
            PEX_PERSTn => open,
            PEX_SCL => open,
            PEX_SDA => open,
            PORT_GOOD => (others => '0'),
            SHPC_INT => open,
            lnk_up => lnk_up,
            select_bifurcation => (others => '0'),
            RXUSRCLK_IN => RXUSRCLK,
            Versal_network_device_fromHost_full_o => Versal_network_device_fromHost_full,
            Versal_network_device_fromHost_prog_full_o => Versal_network_device_fromHost_prog_full,
            Versal_network_device_toHost_empty_o => Versal_network_device_toHost_empty,
            Versal_network_device_toHost_prog_empty_o => Versal_network_device_toHost_prog_empty,
            Versal_network_device_tohost_dout_o => Versal_network_device_tohost_dout,
            Versal_network_device_fromHost_din_i => Versal_network_device_fromHost_din,
            Versal_network_device_fromHost_wr_en_i => Versal_network_device_fromHost_wr_en,
            Versal_network_device_toHost_rd_en_i => Versal_network_device_toHost_rd_en,
            Versal_network_device_toHost_eop_o => Versal_network_device_toHost_eop,
            Versal_network_device_fromHost_eop_i => Versal_network_device_fromHost_eop,
            Versal_network_device_fromHost_set_carrier_i => Versal_network_device_fromHost_set_carrier,
            Versal_network_device_toHost_status_carrier_o => Versal_network_device_toHost_status_carrier,
            versal_sys_reset_n_out => versal_sys_reset_n,
            WupperToCPM => WupperToCPM,
            CPMToWupper => CPMToWupper,
            clk100_out => open,
            DDR_in => DDR_in,
            DDR_out => DDR_out,
            DDR_inout => DDR_inout,
            axi_miso_ttc_lti => axi_miso_ttc_lti,
            axi_mosi_ttc_lti => axi_mosi_ttc_lti,
            apb3_axi_clk => apb3_axi_clk,
            LTI2cips_gpio => LTI2cips_gpio,
            cips2LTI_gpio => cips2LTI_gpio,
            LPDDR_in => LPDDR_in,
            LPDDR_out => LPDDR_out,
            LPDDR_inout => LPDDR_inout
        );

    g_endpoints: for i in 0 to ENDPOINTS-1 generate
        signal ep_register_map_control             : register_map_control_type; --! contains all read/write registers that control the application. The record members are described in pcie_package.vhd
        signal register_map_control_appreg_clk : register_map_control_type; -- @suppress "signal register_map_control_appreg_clk is never read"
        signal reset_soft_appreg_clk : std_logic; -- @suppress "signal reset_soft_appreg_clk is never read"

        signal ep_reset_soft                       : std_logic;
        signal ep_reset_hard                       : std_logic;
        signal fromHostFifo_dout                   : std_logic_vector(DATA_WIDTH-1 downto 0); -- @suppress "signal fromHostFifo_dout is never read"
        signal fromHostFifo_rd_en                  : std_logic;
        signal fromHostFifo_empty                  : std_logic;
        signal fromHostFifo_rd_clk                 : std_logic;
        signal fromHostFifo_rst                    : std_logic;
        signal toHostFifo_wr_clk                   : std_logic;
        signal toHostFifo_rst                      : std_logic;
        --signal fromHostFifo_wr_clk                 : std_logic;

        signal ep_appreg_clk: std_logic;
        signal ep_pll_locked: std_logic; -- @suppress "signal ep_pll_locked is never read"

        signal toHostFifo_din : slv_array(0 to NUMBER_OF_DESCRIPTORS-2);
        signal toHostFifo_prog_full : std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
        signal toHostFifo_wr_en : std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
        signal Versal_network_device_ep_fromHost_full : STD_LOGIC;
        signal Versal_network_device_ep_fromHost_prog_full : STD_LOGIC;
        signal Versal_network_device_ep_toHost_empty : STD_LOGIC;
        signal Versal_network_device_ep_toHost_prog_empty : STD_LOGIC;
        signal Versal_network_device_ep_tohost_dout : STD_LOGIC_VECTOR ( 31 downto 0 );
        signal Versal_network_device_ep_fromHost_din : STD_LOGIC_VECTOR ( 31 downto 0 );
        signal Versal_network_device_ep_fromHost_wr_en : STD_LOGIC;
        signal Versal_network_device_ep_toHost_rd_en : STD_LOGIC;
        signal Versal_network_device_ep_toHost_eop : STD_LOGIC;
        signal Versal_network_device_ep_fromHost_eop : STD_LOGIC;
        signal Versal_network_device_ep_fromHost_set_carrier : std_logic;
        signal Versal_network_device_ep_toHost_status_carrier : std_logic;
        signal clk250: std_logic;

    --COMPONENT fh_dout_ila
    --    PORT (
    --        clk : IN STD_LOGIC;
    --        probe0 : IN STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
    --        probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --        probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
    --    );
    --END COMPONENT  ;

    begin

        g_ep0: if i = 0 generate
            register_map_control <= ep_register_map_control;
            reset_soft <= ep_reset_soft;
            reset_hard <= ep_reset_hard;
            appreg_clk <= ep_appreg_clk;
            pll_locked <= ep_pll_locked;
            Versal_network_device_ep_fromHost_full <= Versal_network_device_fromHost_full;
            Versal_network_device_ep_fromHost_prog_full <= Versal_network_device_fromHost_prog_full;
            Versal_network_device_ep_toHost_empty <= Versal_network_device_toHost_empty;
            Versal_network_device_ep_toHost_prog_empty <= Versal_network_device_toHost_prog_empty;
            Versal_network_device_ep_tohost_dout <= Versal_network_device_tohost_dout;
            Versal_network_device_fromHost_din <= Versal_network_device_ep_fromHost_din;
            Versal_network_device_fromHost_wr_en <= Versal_network_device_ep_fromHost_wr_en;
            Versal_network_device_toHost_rd_en <= Versal_network_device_ep_toHost_rd_en;
            Versal_network_device_ep_toHost_eop <= Versal_network_device_toHost_eop;
            Versal_network_device_fromHost_eop <= Versal_network_device_ep_fromHost_eop;
            Versal_network_device_fromHost_set_carrier <= Versal_network_device_ep_fromHost_set_carrier;
            Versal_network_device_ep_toHost_status_carrier <= Versal_network_device_toHost_status_carrier;
        end generate;

        --! Instantiation of the actual PCI express core. Please note the 40MHz
        --! clock required by the core, the 250MHz clock (fifo_rd_clk and fifo_wr_clk)
        --! are generated from sys_clk_p and _n
        pcie0: entity work.wupper
            generic map(
                NUMBER_OF_INTERRUPTS => NUMBER_OF_INTERRUPTS,
                NUMBER_OF_DESCRIPTORS => NUMBER_OF_DESCRIPTORS,
                BUILD_DATETIME => BUILD_DATETIME,
                CARD_TYPE => CARD_TYPE,
                GIT_HASH => GIT_HASH,
                COMMIT_DATETIME => COMMIT_DATETIME,
                GIT_TAG => GIT_TAG,
                GIT_COMMIT_NUMBER => GIT_COMMIT_NUMBER,
                GBT_NUM => 12,
                FIRMWARE_MODE => 0,
                PCIE_ENDPOINT => i,
                PCIE_LANES => PCIE_LANES,
                DATA_WIDTH => DATA_WIDTH,
                SIMULATION => false,
                BLOCKSIZE => 1024,
                USE_ULTRARAM => USE_ULTRARAM(CARD_TYPE),
                ENABLE_XVC => false
            )
            port map(
                appreg_clk => ep_appreg_clk,
                sync_clk => ep_appreg_clk,
                flush_fifo => open,
                interrupt_call => (others => '0'),
                lnk_up => lnk_up(i),
                pcie_rxn => pcie_rxn(PCIE_LANES*i+(PCIE_LANES-1) downto PCIE_LANES*i),
                pcie_rxp => pcie_rxp(PCIE_LANES*i+(PCIE_LANES-1) downto PCIE_LANES*i),
                pcie_txn => pcie_txn(PCIE_LANES*i+(PCIE_LANES-1) downto PCIE_LANES*i),
                pcie_txp => pcie_txp(PCIE_LANES*i+(PCIE_LANES-1) downto PCIE_LANES*i),
                pll_locked => ep_pll_locked,
                register_map_control_sync => ep_register_map_control,
                register_map_control_appreg_clk => register_map_control_appreg_clk,
                register_map_gen_board_info => register_map_gen_board_info,
                register_map_crtohost_monitor => register_map_crtohost_monitor_c,
                register_map_crfromhost_monitor => register_map_crfromhost_monitor_c,
                register_map_decoding_monitor => register_map_decoding_monitor_c,
                register_map_encoding_monitor => register_map_encoding_monitor_c,
                register_map_gbtemu_monitor => register_map_gbtemu_monitor_c,
                register_map_link_monitor => register_map_link_monitor_c,
                register_map_ttc_monitor => register_map_ttc_monitor,
                register_map_ltittc_monitor => register_map_ltittc_monitor_c,
                register_map_xoff_monitor => register_map_xoff_monitor_c,
                register_map_hk_monitor => register_map_hk_monitor,
                register_map_generators => register_map_generators_c,
                wishbone_monitor => wishbone_monitor_c,
                regmap_mrod_monitor => regmap_mrod_monitor_c,
                ipbus_monitor => ipbus_monitor_c,
                dma_enable_out => open,
                reset_hard => ep_reset_hard,
                reset_soft => ep_reset_soft,
                reset_soft_appreg_clk => reset_soft_appreg_clk,
                sys_clk_n => sys_clk_n(i),
                sys_clk_p => sys_clk_p(i),
                sys_reset_n => PCIE_PERSTn,
                tohost_busy_out => open,
                fromHostFifo_dout => fromHostFifo_dout,
                fromHostFifo_empty => fromHostFifo_empty,
                fromHostFifo_rd_clk => fromHostFifo_rd_clk,
                fromHostFifo_rd_en => fromHostFifo_rd_en,
                fromHostFifo_rst => fromHostFifo_rst,
                toHostFifo_din => toHostFifo_din,
                toHostFifo_prog_full => toHostFifo_prog_full,
                toHostFifo_rst => toHostFifo_rst,
                toHostFifo_wr_clk => toHostFifo_wr_clk,
                toHostFifo_wr_en => toHostFifo_wr_en,
                clk250_out => clk250,
                master_busy_in => '0',
                toHostFifoBusy_out => open,
                Versal_network_device_fromHost_full_i => Versal_network_device_ep_fromHost_full,
                Versal_network_device_fromHost_prog_full_i => Versal_network_device_ep_fromHost_prog_full,
                Versal_network_device_toHost_empty_i => Versal_network_device_ep_toHost_empty,
                Versal_network_device_toHost_prog_empty_i => Versal_network_device_ep_toHost_prog_empty,
                Versal_network_device_tohost_dout_i => Versal_network_device_ep_tohost_dout,
                Versal_network_device_fromHost_din_o => Versal_network_device_ep_fromHost_din,
                Versal_network_device_fromHost_wr_en_o => Versal_network_device_ep_fromHost_wr_en,
                Versal_network_device_toHost_rd_en_o => Versal_network_device_ep_toHost_rd_en,
                Versal_network_device_toHost_eop_i => Versal_network_device_ep_toHost_eop,
                Versal_network_device_fromHost_eop_o => Versal_network_device_ep_fromHost_eop,
                Versal_network_device_fromHost_set_carrier_o => Versal_network_device_ep_fromHost_set_carrier,
                Versal_network_device_toHost_status_carrier_i => Versal_network_device_ep_toHost_status_carrier,
                CPMToWupper => CPMToWupper(i),
                WupperToCPM => WupperToCPM(i)
            );

        toHostFifo_wr_clk <= clk250;
        fromHostFifo_rd_clk <= clk250;
        toHostFifo_rst <= ep_reset_soft or ep_reset_hard;
        fromHostFifo_rst <= ep_reset_soft or ep_reset_hard;

        fromHostFifo_rd_en <= not fromHostFifo_empty;
        --g_ila_ep0: if i = 0 generate
        --    fhila0 : fh_dout_ila
        --        PORT MAP (
        --            clk => clk250,
        --            probe0 => fromHostFifo_dout,
        --            probe1(0) => fromHostFifo_empty,
        --            probe2(0) => fromHostFifo_rd_en
        --        );
        --end generate g_ila_ep0;

        leds_s(8*i+7 downto 8*i) <= ep_register_map_control.STATUS_LEDS;


        g_descr: for descr in 0 to NUMBER_OF_DESCRIPTORS-2 generate

            signal cnt: std_logic_vector(63 downto 0);
            signal out_data: std_logic_vector(DATA_WIDTH-1 downto 0);
        begin


            cnt_combine : for i in 0 to (DATA_WIDTH/cnt'length)-1 generate
                out_data((cnt'length*(i+1))-1 downto cnt'length*i) <= cnt;
            end generate;

            cntProc: process(clk250)
            begin
                if rising_edge(clk250) then
                    if ep_reset_soft = '1' then
                        cnt <= (others => '0');
                    elsif toHostFifo_prog_full(descr) = '0' then
                        cnt <= cnt + 1;
                    end if;
                end if;
            end process;
            toHostFifo_wr_en(descr) <= not toHostFifo_prog_full(descr);
            toHostFifo_din(descr)(DATA_WIDTH-1 downto 0) <= out_data; -- @suppress "Incorrect array size in assignment: expected (<256>) but was (<DATA_WIDTH>)"
        end generate;

    end generate; --g_endpoints

    leds <=  leds_s(NUM_LEDS(CARD_TYPE)-1 downto 0);


end architecture structure ; -- of wupper_oc_top


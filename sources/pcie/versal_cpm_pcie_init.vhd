--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.


library ieee, UNISIM;
    use ieee.numeric_std.all;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

entity versal_cpm_pcie_init is
    port (
        user_clk : in std_logic;
        user_reset : in std_logic;

        cfg_power_state_change_interrupt: in std_logic;
        cfg_power_state_change_ack : out std_logic;

        cfg_flr_in_process: in std_logic_vector(3 downto 0);
        cfg_flr_done: out std_logic_vector(3 downto 0);

        cfg_status_cq_np_req: out std_logic_vector(1 downto 0)
    );
end entity versal_cpm_pcie_init;

architecture rtl of versal_cpm_pcie_init is
    signal  cfg_flr_done_reg0: std_logic_vector(3 downto 0);
    --signal  cfg_vf_flr_done_reg0: std_logic_vector(5 downto 0);
    signal  cfg_flr_done_reg1: std_logic_vector(3 downto 0);
--signal  cfg_vf_flr_done_reg1: std_logic_vector(5 downto 0);
--signal  cfg_vf_flr_func_num_reg: std_logic_vector(7 downto 0);
begin
    pwr_state_proc: process(user_clk)
    begin
        if rising_edge(user_clk) then
            if (user_reset = '1' ) then
                cfg_power_state_change_ack <= '0';
            else
                if ( cfg_power_state_change_interrupt = '1') then
                    cfg_power_state_change_ack <= '1';
                else
                    cfg_power_state_change_ack <= '0';
                end if;
            end if;
        end if;
    end process;


    flr_proc: process(user_clk)
    begin
        if rising_edge(user_clk) then
            if (user_reset = '1') then
                cfg_flr_done_reg0       <= "0000";
                cfg_flr_done_reg1       <= "0000";
            else
                cfg_flr_done_reg0       <= cfg_flr_in_process;
                cfg_flr_done_reg1       <= cfg_flr_done_reg0;
            end if;
        end if;
    end process;

    -- assign function level reset outputs.
    cfg_flr_done(0) <= (not cfg_flr_done_reg1(0)) and cfg_flr_done_reg0(0);
    cfg_flr_done(1) <= (not cfg_flr_done_reg1(1)) and cfg_flr_done_reg0(1);
    cfg_flr_done(2) <= (not cfg_flr_done_reg1(2)) and cfg_flr_done_reg0(2);
    cfg_flr_done(3) <= (not cfg_flr_done_reg1(3)) and cfg_flr_done_reg0(3);

    cfg_status_cq_np_req <= "11";
end rtl;

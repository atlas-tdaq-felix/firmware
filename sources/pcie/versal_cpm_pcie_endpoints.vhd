--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.


--!------------------------------------------------------------------------------

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    --use IEEE.NUMERIC_STD.ALL;
    use work.pcie_package.all;
    use work.FELIX_package.all;

entity versal_cpm_pcie_endpoints is
    Generic (
        CARD_TYPE: integer
    );
    Port (
        DDR_in : in DDR_in_array_type(0 to NUM_DDR(CARD_TYPE)-1);
        DDR_out : out DDR_out_array_type(0 to NUM_DDR(CARD_TYPE)-1);
        DDR_inout : inout DDR_inout_array_type(0 to NUM_DDR(CARD_TYPE)-1);
        LPDDR_in : in LPDDR_in_array_type(0 to NUM_LPDDR(CARD_TYPE)/2-1);
        LPDDR_out : out LPDDR_out_array_type(0 to NUM_LPDDR(CARD_TYPE)-1);
        LPDDR_inout : inout LPDDR_inout_array_type(0 to NUM_LPDDR(CARD_TYPE)-1);
        FPGA_DNA_0 : out STD_LOGIC_VECTOR ( 63 downto 0 );
        M00_AXI_0_araddr : out STD_LOGIC_VECTOR ( 15 downto 0 );
        M00_AXI_0_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
        M00_AXI_0_arready : in STD_LOGIC;
        M00_AXI_0_arvalid : out STD_LOGIC;
        M00_AXI_0_awaddr : out STD_LOGIC_VECTOR ( 15 downto 0 );
        M00_AXI_0_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
        M00_AXI_0_awready : in STD_LOGIC;
        M00_AXI_0_awvalid : out STD_LOGIC;
        M00_AXI_0_bready : out STD_LOGIC;
        M00_AXI_0_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
        M00_AXI_0_bvalid : in STD_LOGIC;
        M00_AXI_0_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
        M00_AXI_0_rready : out STD_LOGIC;
        M00_AXI_0_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
        M00_AXI_0_rvalid : in STD_LOGIC;
        M00_AXI_0_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
        M00_AXI_0_wready : in STD_LOGIC;
        M00_AXI_0_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
        M00_AXI_0_wvalid : out STD_LOGIC;
        FAN_TACH_PIN : out STD_LOGIC_VECTOR ( 0 to 0 );
        TEMP_0 : out STD_LOGIC_VECTOR ( 11 downto 0 );
        VCCAUX_0 : out STD_LOGIC_VECTOR ( 11 downto 0 );
        VCCBRAM_0 : out STD_LOGIC_VECTOR ( 11 downto 0 );
        VCCINT_0 : out STD_LOGIC_VECTOR ( 11 downto 0 );
        Versal_network_device_fromHost_din_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
        Versal_network_device_fromHost_eop_i : in STD_LOGIC;
        Versal_network_device_fromHost_full_o : out STD_LOGIC;
        Versal_network_device_fromHost_prog_full_o : out STD_LOGIC;
        Versal_network_device_fromHost_set_carrier_i : in STD_LOGIC;
        Versal_network_device_fromHost_wr_en_i : in STD_LOGIC;
        Versal_network_device_host_clk : in STD_LOGIC;
        Versal_network_device_toHost_empty_o : out STD_LOGIC;
        Versal_network_device_toHost_eop_o : out STD_LOGIC;
        Versal_network_device_toHost_prog_empty_o : out STD_LOGIC;
        Versal_network_device_toHost_rd_en_i : in STD_LOGIC;
        Versal_network_device_toHost_status_carrier_o : out STD_LOGIC;
        Versal_network_device_tohost_dout_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
        scl_i : in STD_LOGIC;
        scl_o : out STD_LOGIC;
        sda_i : in STD_LOGIC;
        sda_o : out STD_LOGIC;
        gpio2_io_i_0 : in STD_LOGIC_VECTOR ( 20 downto 0 );
        gpio2_io_o_0 : out STD_LOGIC_VECTOR ( 20 downto 0 );
        gpio_io_i_0 : in STD_LOGIC_VECTOR ( 20 downto 0 );
        gpio_io_o_0 : out STD_LOGIC_VECTOR ( 20 downto 0 );
        gpio_io_i_1 : in STD_LOGIC_VECTOR ( 3 downto 0 );
        gpio_io_o_1 : out STD_LOGIC_VECTOR ( 3 downto 0 );
        pl0_resetn : out STD_LOGIC_VECTOR ( 0 to 0 );
        pl0_ref_clk : out STD_LOGIC;
        WupperToCPM: in WupperToCPM_array_type(0 to 1);
        CPMToWupper: out CPMToWupper_array_type(0 to 1);
        clk100_out : out std_logic
    );
end versal_cpm_pcie_endpoints;

architecture Behavioral of versal_cpm_pcie_endpoints is
    --COMPONENT axis_cc_cq_ila
    --    PORT (
    --        clk : IN STD_LOGIC;
    --        probe0 : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    --        probe1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    --        probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --        probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --        probe4 : IN STD_LOGIC_VECTOR(182 DOWNTO 0);
    --        probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --        probe6 : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    --        probe7 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    --        probe8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --        probe9 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --        probe10 : IN STD_LOGIC_VECTOR(80 DOWNTO 0);
    --        probe11 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --        probe12 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --        probe13 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
    --    );
    --END COMPONENT;

    component FLX155_cips_wrapper
        port (
            CH0_DDR4_0_0_act_n : out STD_LOGIC_VECTOR ( 0 to 0 );
            CH0_DDR4_0_0_adr : out STD_LOGIC_VECTOR ( 16 downto 0 );
            CH0_DDR4_0_0_ba : out STD_LOGIC_VECTOR ( 1 downto 0 );
            CH0_DDR4_0_0_bg : out STD_LOGIC_VECTOR ( 1 downto 0 );
            CH0_DDR4_0_0_ck_c : out STD_LOGIC_VECTOR ( 1 downto 0 );
            CH0_DDR4_0_0_ck_t : out STD_LOGIC_VECTOR ( 1 downto 0 );
            CH0_DDR4_0_0_cke : out STD_LOGIC_VECTOR ( 1 downto 0 );
            CH0_DDR4_0_0_cs_n : out STD_LOGIC_VECTOR ( 1 downto 0 );
            CH0_DDR4_0_0_dm_n : inout STD_LOGIC_VECTOR ( 8 downto 0 );
            CH0_DDR4_0_0_dq : inout STD_LOGIC_VECTOR ( 71 downto 0 );
            CH0_DDR4_0_0_dqs_c : inout STD_LOGIC_VECTOR ( 8 downto 0 );
            CH0_DDR4_0_0_dqs_t : inout STD_LOGIC_VECTOR ( 8 downto 0 );
            CH0_DDR4_0_0_odt : out STD_LOGIC_VECTOR ( 1 downto 0 );
            CH0_DDR4_0_0_reset_n : out STD_LOGIC_VECTOR ( 0 to 0 );
            PCIE0_GT_0_grx_n : in STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE0_GT_0_grx_p : in STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE0_GT_0_gtx_n : out STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE0_GT_0_gtx_p : out STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE1_GT_0_grx_n : in STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE1_GT_0_grx_p : in STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE1_GT_0_gtx_n : out STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE1_GT_0_gtx_p : out STD_LOGIC_VECTOR ( 7 downto 0 );
            clk100_out : out STD_LOGIC;
            gpio2_io_i_0 : in STD_LOGIC_VECTOR ( 20 downto 0 );
            gpio2_io_o_0 : out STD_LOGIC_VECTOR ( 20 downto 0 );
            gpio_io_i_0 : in STD_LOGIC_VECTOR ( 20 downto 0 );
            gpio_io_o_0 : out STD_LOGIC_VECTOR ( 20 downto 0 );
            gt_refclk0_0_clk_n : in STD_LOGIC;
            gt_refclk0_0_clk_p : in STD_LOGIC;
            gt_refclk1_0_clk_n : in STD_LOGIC;
            gt_refclk1_0_clk_p : in STD_LOGIC;
            pcie0_cfg_control_flr_done_0 : in STD_LOGIC;
            pcie0_cfg_control_flr_in_process_0 : out STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie0_cfg_control_power_state_change_ack_0 : in STD_LOGIC;
            pcie0_cfg_control_power_state_change_interrupt_0 : out STD_LOGIC;
            pcie0_cfg_interrupt_0_intx_vector : in STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie0_cfg_interrupt_0_pending : in STD_LOGIC_VECTOR ( 15 downto 0 );
            pcie0_cfg_interrupt_0_sent : out STD_LOGIC;
            pcie0_cfg_msi_0_fail : out STD_LOGIC;
            pcie0_cfg_msi_0_function_number : in STD_LOGIC_VECTOR ( 15 downto 0 );
            pcie0_cfg_msi_0_int_vector : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie0_cfg_msi_0_select : in STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie0_cfg_msi_0_sent : out STD_LOGIC;
            pcie0_cfg_msix_0_address : in STD_LOGIC_VECTOR ( 63 downto 0 );
            pcie0_cfg_msix_0_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie0_cfg_msix_0_enable : out STD_LOGIC;
            pcie0_cfg_msix_0_int_vector : in STD_LOGIC;
            pcie0_cfg_msix_0_mask : out STD_LOGIC;
            pcie0_cfg_msix_0_vec_pending : in STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie0_cfg_msix_0_vec_pending_status : out STD_LOGIC;
            pcie0_cfg_status_cq_np_req_0 : in STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie0_m_axis_cq_0_tdata : out STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie0_m_axis_cq_0_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie0_m_axis_cq_0_tlast : out STD_LOGIC;
            pcie0_m_axis_cq_0_tready : in STD_LOGIC;
            pcie0_m_axis_cq_0_tuser : out STD_LOGIC_VECTOR ( 464 downto 0 );
            pcie0_m_axis_cq_0_tvalid : out STD_LOGIC;
            pcie0_m_axis_rc_0_tdata : out STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie0_m_axis_rc_0_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie0_m_axis_rc_0_tlast : out STD_LOGIC;
            pcie0_m_axis_rc_0_tready : in STD_LOGIC;
            pcie0_m_axis_rc_0_tuser : out STD_LOGIC_VECTOR ( 336 downto 0 );
            pcie0_m_axis_rc_0_tvalid : out STD_LOGIC;
            pcie0_s_axis_cc_0_tdata : in STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie0_s_axis_cc_0_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie0_s_axis_cc_0_tlast : in STD_LOGIC;
            pcie0_s_axis_cc_0_tready : out STD_LOGIC;
            pcie0_s_axis_cc_0_tuser : in STD_LOGIC_VECTOR ( 164 downto 0 );
            pcie0_s_axis_cc_0_tvalid : in STD_LOGIC;
            pcie0_s_axis_rq_0_tdata : in STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie0_s_axis_rq_0_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie0_s_axis_rq_0_tlast : in STD_LOGIC;
            pcie0_s_axis_rq_0_tready : out STD_LOGIC;
            pcie0_s_axis_rq_0_tuser : in STD_LOGIC_VECTOR ( 372 downto 0 );
            pcie0_s_axis_rq_0_tvalid : in STD_LOGIC;
            pcie0_user_clk_0 : out STD_LOGIC;
            pcie0_user_lnk_up_0 : out STD_LOGIC;
            pcie0_user_reset_0 : out STD_LOGIC;
            pcie1_cfg_control_flr_done_0 : in STD_LOGIC;
            pcie1_cfg_control_flr_in_process_0 : out STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie1_cfg_control_power_state_change_ack_0 : in STD_LOGIC;
            pcie1_cfg_control_power_state_change_interrupt_0 : out STD_LOGIC;
            pcie1_cfg_interrupt_0_intx_vector : in STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie1_cfg_interrupt_0_pending : in STD_LOGIC_VECTOR ( 15 downto 0 );
            pcie1_cfg_interrupt_0_sent : out STD_LOGIC;
            pcie1_cfg_msi_0_fail : out STD_LOGIC;
            pcie1_cfg_msi_0_function_number : in STD_LOGIC_VECTOR ( 15 downto 0 );
            pcie1_cfg_msi_0_int_vector : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie1_cfg_msi_0_select : in STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie1_cfg_msi_0_sent : out STD_LOGIC;
            pcie1_cfg_msix_0_address : in STD_LOGIC_VECTOR ( 63 downto 0 );
            pcie1_cfg_msix_0_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie1_cfg_msix_0_enable : out STD_LOGIC;
            pcie1_cfg_msix_0_int_vector : in STD_LOGIC;
            pcie1_cfg_msix_0_mask : out STD_LOGIC;
            pcie1_cfg_msix_0_vec_pending : in STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie1_cfg_msix_0_vec_pending_status : out STD_LOGIC;
            pcie1_cfg_status_cq_np_req_0 : in STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie1_m_axis_cq_0_tdata : out STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie1_m_axis_cq_0_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie1_m_axis_cq_0_tlast : out STD_LOGIC;
            pcie1_m_axis_cq_0_tready : in STD_LOGIC;
            pcie1_m_axis_cq_0_tuser : out STD_LOGIC_VECTOR ( 464 downto 0 );
            pcie1_m_axis_cq_0_tvalid : out STD_LOGIC;
            pcie1_m_axis_rc_0_tdata : out STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie1_m_axis_rc_0_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie1_m_axis_rc_0_tlast : out STD_LOGIC;
            pcie1_m_axis_rc_0_tready : in STD_LOGIC;
            pcie1_m_axis_rc_0_tuser : out STD_LOGIC_VECTOR ( 336 downto 0 );
            pcie1_m_axis_rc_0_tvalid : out STD_LOGIC;
            pcie1_s_axis_cc_0_tdata : in STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie1_s_axis_cc_0_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie1_s_axis_cc_0_tlast : in STD_LOGIC;
            pcie1_s_axis_cc_0_tready : out STD_LOGIC;
            pcie1_s_axis_cc_0_tuser : in STD_LOGIC_VECTOR ( 164 downto 0 );
            pcie1_s_axis_cc_0_tvalid : in STD_LOGIC;
            pcie1_s_axis_rq_0_tdata : in STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie1_s_axis_rq_0_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie1_s_axis_rq_0_tlast : in STD_LOGIC;
            pcie1_s_axis_rq_0_tready : out STD_LOGIC;
            pcie1_s_axis_rq_0_tuser : in STD_LOGIC_VECTOR ( 372 downto 0 );
            pcie1_s_axis_rq_0_tvalid : in STD_LOGIC;
            pcie1_user_clk_0 : out STD_LOGIC;
            pcie1_user_lnk_up_0 : out STD_LOGIC;
            pcie1_user_reset_0 : out STD_LOGIC;
            pl0_ref_clk_0 : out STD_LOGIC;
            pl0_resetn_0 : out STD_LOGIC;
            scl_i : in STD_LOGIC;
            scl_o : out STD_LOGIC;
            sda_i : in STD_LOGIC;
            sda_o : out STD_LOGIC;
            sys_clk0_0_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
            sys_clk0_0_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 )
        );
    end component FLX155_cips_wrapper;

    component FLX120_cips_wrapper
        port (
            PCIE0_GT_0_grx_n : in STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE0_GT_0_grx_p : in STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE0_GT_0_gtx_n : out STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE0_GT_0_gtx_p : out STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE1_GT_0_grx_n : in STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE1_GT_0_grx_p : in STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE1_GT_0_gtx_n : out STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE1_GT_0_gtx_p : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_lpddr4_trip1_ca_a : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch0_lpddr4_trip1_ca_b : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch0_lpddr4_trip1_ck_c_a : out STD_LOGIC;
            ch0_lpddr4_trip1_ck_c_b : out STD_LOGIC;
            ch0_lpddr4_trip1_ck_t_a : out STD_LOGIC;
            ch0_lpddr4_trip1_ck_t_b : out STD_LOGIC;
            ch0_lpddr4_trip1_cke_a : out STD_LOGIC;
            ch0_lpddr4_trip1_cke_b : out STD_LOGIC;
            ch0_lpddr4_trip1_cs_a : out STD_LOGIC;
            ch0_lpddr4_trip1_cs_b : out STD_LOGIC;
            ch0_lpddr4_trip1_dmi_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_lpddr4_trip1_dmi_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_lpddr4_trip1_dq_a : inout STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_lpddr4_trip1_dq_b : inout STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_lpddr4_trip1_dqs_c_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_lpddr4_trip1_dqs_c_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_lpddr4_trip1_dqs_t_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_lpddr4_trip1_dqs_t_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_lpddr4_trip1_reset_n : out STD_LOGIC;
            ch0_lpddr4_trip2_ca_a : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch0_lpddr4_trip2_ca_b : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch0_lpddr4_trip2_ck_c_a : out STD_LOGIC;
            ch0_lpddr4_trip2_ck_c_b : out STD_LOGIC;
            ch0_lpddr4_trip2_ck_t_a : out STD_LOGIC;
            ch0_lpddr4_trip2_ck_t_b : out STD_LOGIC;
            ch0_lpddr4_trip2_cke_a : out STD_LOGIC;
            ch0_lpddr4_trip2_cke_b : out STD_LOGIC;
            ch0_lpddr4_trip2_cs_a : out STD_LOGIC;
            ch0_lpddr4_trip2_cs_b : out STD_LOGIC;
            ch0_lpddr4_trip2_dmi_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_lpddr4_trip2_dmi_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_lpddr4_trip2_dq_a : inout STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_lpddr4_trip2_dq_b : inout STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_lpddr4_trip2_dqs_c_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_lpddr4_trip2_dqs_c_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_lpddr4_trip2_dqs_t_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_lpddr4_trip2_dqs_t_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_lpddr4_trip2_reset_n : out STD_LOGIC;
            ch1_lpddr4_trip1_ca_a : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch1_lpddr4_trip1_ca_b : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch1_lpddr4_trip1_ck_c_a : out STD_LOGIC;
            ch1_lpddr4_trip1_ck_c_b : out STD_LOGIC;
            ch1_lpddr4_trip1_ck_t_a : out STD_LOGIC;
            ch1_lpddr4_trip1_ck_t_b : out STD_LOGIC;
            ch1_lpddr4_trip1_cke_a : out STD_LOGIC;
            ch1_lpddr4_trip1_cke_b : out STD_LOGIC;
            ch1_lpddr4_trip1_cs_a : out STD_LOGIC;
            ch1_lpddr4_trip1_cs_b : out STD_LOGIC;
            ch1_lpddr4_trip1_dmi_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_lpddr4_trip1_dmi_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_lpddr4_trip1_dq_a : inout STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_lpddr4_trip1_dq_b : inout STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_lpddr4_trip1_dqs_c_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_lpddr4_trip1_dqs_c_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_lpddr4_trip1_dqs_t_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_lpddr4_trip1_dqs_t_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_lpddr4_trip1_reset_n : out STD_LOGIC;
            ch1_lpddr4_trip2_ca_a : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch1_lpddr4_trip2_ca_b : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch1_lpddr4_trip2_ck_c_a : out STD_LOGIC;
            ch1_lpddr4_trip2_ck_c_b : out STD_LOGIC;
            ch1_lpddr4_trip2_ck_t_a : out STD_LOGIC;
            ch1_lpddr4_trip2_ck_t_b : out STD_LOGIC;
            ch1_lpddr4_trip2_cke_a : out STD_LOGIC;
            ch1_lpddr4_trip2_cke_b : out STD_LOGIC;
            ch1_lpddr4_trip2_cs_a : out STD_LOGIC;
            ch1_lpddr4_trip2_cs_b : out STD_LOGIC;
            ch1_lpddr4_trip2_dmi_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_lpddr4_trip2_dmi_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_lpddr4_trip2_dq_a : inout STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_lpddr4_trip2_dq_b : inout STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_lpddr4_trip2_dqs_c_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_lpddr4_trip2_dqs_c_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_lpddr4_trip2_dqs_t_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_lpddr4_trip2_dqs_t_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_lpddr4_trip2_reset_n : out STD_LOGIC;
            gt_refclk0_0_clk_n : in STD_LOGIC;
            gt_refclk0_0_clk_p : in STD_LOGIC;
            gt_refclk1_0_clk_n : in STD_LOGIC;
            gt_refclk1_0_clk_p : in STD_LOGIC;
            lpddr4_clk1_clk_n : in STD_LOGIC;
            lpddr4_clk1_clk_p : in STD_LOGIC;
            lpddr4_clk2_clk_n : in STD_LOGIC;
            lpddr4_clk2_clk_p : in STD_LOGIC;
            pcie0_cfg_control_flr_done_0 : in STD_LOGIC;
            pcie0_cfg_control_flr_in_process_0 : out STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie0_cfg_control_power_state_change_ack_0 : in STD_LOGIC;
            pcie0_cfg_control_power_state_change_interrupt_0 : out STD_LOGIC;
            pcie0_cfg_interrupt_0_intx_vector : in STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie0_cfg_interrupt_0_pending : in STD_LOGIC_VECTOR ( 15 downto 0 );
            pcie0_cfg_interrupt_0_sent : out STD_LOGIC;
            pcie0_cfg_status_cq_np_req_0 : in STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie0_m_axis_cq_0_tdata : out STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie0_m_axis_cq_0_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie0_m_axis_cq_0_tlast : out STD_LOGIC;
            pcie0_m_axis_cq_0_tready : in STD_LOGIC;
            pcie0_m_axis_cq_0_tuser : out STD_LOGIC_VECTOR ( 464 downto 0 );
            pcie0_m_axis_cq_0_tvalid : out STD_LOGIC;
            pcie0_m_axis_rc_0_tdata : out STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie0_m_axis_rc_0_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie0_m_axis_rc_0_tlast : out STD_LOGIC;
            pcie0_m_axis_rc_0_tready : in STD_LOGIC;
            pcie0_m_axis_rc_0_tuser : out STD_LOGIC_VECTOR ( 336 downto 0 );
            pcie0_m_axis_rc_0_tvalid : out STD_LOGIC;
            pcie0_s_axis_cc_0_tdata : in STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie0_s_axis_cc_0_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie0_s_axis_cc_0_tlast : in STD_LOGIC;
            pcie0_s_axis_cc_0_tready : out STD_LOGIC;
            pcie0_s_axis_cc_0_tuser : in STD_LOGIC_VECTOR ( 164 downto 0 );
            pcie0_s_axis_cc_0_tvalid : in STD_LOGIC;
            pcie0_s_axis_rq_0_tdata : in STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie0_s_axis_rq_0_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie0_s_axis_rq_0_tlast : in STD_LOGIC;
            pcie0_s_axis_rq_0_tready : out STD_LOGIC;
            pcie0_s_axis_rq_0_tuser : in STD_LOGIC_VECTOR ( 372 downto 0 );
            pcie0_s_axis_rq_0_tvalid : in STD_LOGIC;
            pcie0_user_clk_0 : out STD_LOGIC;
            pcie0_user_lnk_up_0 : out STD_LOGIC;
            pcie0_user_reset_0 : out STD_LOGIC;
            pcie1_cfg_control_flr_done_0 : in STD_LOGIC;
            pcie1_cfg_control_flr_in_process_0 : out STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie1_cfg_control_power_state_change_ack_0 : in STD_LOGIC;
            pcie1_cfg_control_power_state_change_interrupt_0 : out STD_LOGIC;
            pcie1_cfg_interrupt_0_intx_vector : in STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie1_cfg_interrupt_0_pending : in STD_LOGIC_VECTOR ( 15 downto 0 );
            pcie1_cfg_interrupt_0_sent : out STD_LOGIC;
            pcie1_cfg_status_cq_np_req_0 : in STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie1_m_axis_cq_0_tdata : out STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie1_m_axis_cq_0_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie1_m_axis_cq_0_tlast : out STD_LOGIC;
            pcie1_m_axis_cq_0_tready : in STD_LOGIC;
            pcie1_m_axis_cq_0_tuser : out STD_LOGIC_VECTOR ( 464 downto 0 );
            pcie1_m_axis_cq_0_tvalid : out STD_LOGIC;
            pcie1_m_axis_rc_0_tdata : out STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie1_m_axis_rc_0_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie1_m_axis_rc_0_tlast : out STD_LOGIC;
            pcie1_m_axis_rc_0_tready : in STD_LOGIC;
            pcie1_m_axis_rc_0_tuser : out STD_LOGIC_VECTOR ( 336 downto 0 );
            pcie1_m_axis_rc_0_tvalid : out STD_LOGIC;
            pcie1_s_axis_cc_0_tdata : in STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie1_s_axis_cc_0_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie1_s_axis_cc_0_tlast : in STD_LOGIC;
            pcie1_s_axis_cc_0_tready : out STD_LOGIC;
            pcie1_s_axis_cc_0_tuser : in STD_LOGIC_VECTOR ( 164 downto 0 );
            pcie1_s_axis_cc_0_tvalid : in STD_LOGIC;
            pcie1_s_axis_rq_0_tdata : in STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie1_s_axis_rq_0_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie1_s_axis_rq_0_tlast : in STD_LOGIC;
            pcie1_s_axis_rq_0_tready : out STD_LOGIC;
            pcie1_s_axis_rq_0_tuser : in STD_LOGIC_VECTOR ( 372 downto 0 );
            pcie1_s_axis_rq_0_tvalid : in STD_LOGIC;
            pcie1_user_clk_0 : out STD_LOGIC;
            pcie1_user_lnk_up_0 : out STD_LOGIC;
            pcie1_user_reset_0 : out STD_LOGIC;
            pl0_ref_clk_0 : out STD_LOGIC;
            pl0_resetn_0 : out STD_LOGIC
        );
    end component FLX120_cips_wrapper;

    component versal_cpm_pcie_wrapper --For FLX182 -- @suppress "Component declaration 'versal_cpm_pcie_wrapper' has none or multiple matching entity declarations"
        port (
            CH0_DDR4_0_0_act_n : out STD_LOGIC_VECTOR ( 0 to 0 );
            CH0_DDR4_0_0_adr : out STD_LOGIC_VECTOR ( 16 downto 0 );
            CH0_DDR4_0_0_ba : out STD_LOGIC_VECTOR ( 1 downto 0 );
            CH0_DDR4_0_0_bg : out STD_LOGIC_VECTOR ( 1 downto 0 );
            CH0_DDR4_0_0_ck_c : out STD_LOGIC_VECTOR ( 1 downto 0 );
            CH0_DDR4_0_0_ck_t : out STD_LOGIC_VECTOR ( 1 downto 0 );
            CH0_DDR4_0_0_cke : out STD_LOGIC_VECTOR ( 1 downto 0 );
            CH0_DDR4_0_0_cs_n : out STD_LOGIC_VECTOR ( 1 downto 0 );
            CH0_DDR4_0_0_dm_n : inout STD_LOGIC_VECTOR ( 8 downto 0 );
            CH0_DDR4_0_0_dq : inout STD_LOGIC_VECTOR ( 71 downto 0 );
            CH0_DDR4_0_0_dqs_c : inout STD_LOGIC_VECTOR ( 8 downto 0 );
            CH0_DDR4_0_0_dqs_t : inout STD_LOGIC_VECTOR ( 8 downto 0 );
            CH0_DDR4_0_0_odt : out STD_LOGIC_VECTOR ( 1 downto 0 );
            CH0_DDR4_0_0_reset_n : out STD_LOGIC_VECTOR ( 0 to 0 );
            FPGA_DNA_0 : out STD_LOGIC_VECTOR ( 63 downto 0 );
            M00_AXI_0_araddr : out STD_LOGIC_VECTOR ( 15 downto 0 );
            M00_AXI_0_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
            M00_AXI_0_arready : in STD_LOGIC;
            M00_AXI_0_arvalid : out STD_LOGIC;
            M00_AXI_0_awaddr : out STD_LOGIC_VECTOR ( 15 downto 0 );
            M00_AXI_0_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
            M00_AXI_0_awready : in STD_LOGIC;
            M00_AXI_0_awvalid : out STD_LOGIC;
            M00_AXI_0_bready : out STD_LOGIC;
            M00_AXI_0_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
            M00_AXI_0_bvalid : in STD_LOGIC;
            M00_AXI_0_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
            M00_AXI_0_rready : out STD_LOGIC;
            M00_AXI_0_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
            M00_AXI_0_rvalid : in STD_LOGIC;
            M00_AXI_0_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
            M00_AXI_0_wready : in STD_LOGIC;
            M00_AXI_0_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
            M00_AXI_0_wvalid : out STD_LOGIC;
            PCIE0_GT_0_grx_n : in STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE0_GT_0_grx_p : in STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE0_GT_0_gtx_n : out STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE0_GT_0_gtx_p : out STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE1_GT_0_grx_n : in STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE1_GT_0_grx_p : in STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE1_GT_0_gtx_n : out STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE1_GT_0_gtx_p : out STD_LOGIC_VECTOR ( 7 downto 0 );
            TEMP_0 : out STD_LOGIC_VECTOR ( 11 downto 0 );
            VCCAUX_0 : out STD_LOGIC_VECTOR ( 11 downto 0 );
            VCCBRAM_0 : out STD_LOGIC_VECTOR ( 11 downto 0 );
            VCCINT_0 : out STD_LOGIC_VECTOR ( 11 downto 0 );
            Versal_network_device_fromHost_din_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
            Versal_network_device_fromHost_eop_i : in STD_LOGIC;
            Versal_network_device_fromHost_full_o : out STD_LOGIC;
            Versal_network_device_fromHost_prog_full_o : out STD_LOGIC;
            Versal_network_device_fromHost_set_carrier_i : in STD_LOGIC;
            Versal_network_device_fromHost_wr_en_i : in STD_LOGIC;
            Versal_network_device_host_clk : in STD_LOGIC;
            Versal_network_device_toHost_empty_o : out STD_LOGIC;
            Versal_network_device_toHost_eop_o : out STD_LOGIC;
            Versal_network_device_toHost_prog_empty_o : out STD_LOGIC;
            Versal_network_device_toHost_rd_en_i : in STD_LOGIC;
            Versal_network_device_toHost_status_carrier_o : out STD_LOGIC;
            Versal_network_device_tohost_dout_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
            clk100_out : out STD_LOGIC;
            gpio2_io_i_0 : in STD_LOGIC_VECTOR ( 20 downto 0 );
            gpio2_io_o_0 : out STD_LOGIC_VECTOR ( 20 downto 0 );
            gpio_io_i_0 : in STD_LOGIC_VECTOR ( 20 downto 0 );
            gpio_io_i_1 : in STD_LOGIC_VECTOR ( 3 downto 0 );
            gpio_io_o_0 : out STD_LOGIC_VECTOR ( 20 downto 0 );
            gpio_io_o_1 : out STD_LOGIC_VECTOR ( 3 downto 0 );
            gt_refclk0_0_clk_n : in STD_LOGIC;
            gt_refclk0_0_clk_p : in STD_LOGIC;
            gt_refclk1_0_clk_n : in STD_LOGIC;
            gt_refclk1_0_clk_p : in STD_LOGIC;
            pcie0_cfg_control_0_err_cor_in : in STD_LOGIC;
            pcie0_cfg_control_0_err_uncor_in : in STD_LOGIC;
            pcie0_cfg_control_0_flr_done : in STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie0_cfg_control_0_flr_in_process : out STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie0_cfg_control_0_hot_reset_in : in STD_LOGIC;
            pcie0_cfg_control_0_hot_reset_out : out STD_LOGIC;
            pcie0_cfg_control_0_power_state_change_ack : in STD_LOGIC;
            pcie0_cfg_control_0_power_state_change_interrupt : out STD_LOGIC;
            pcie0_cfg_interrupt_0_intx_vector : in STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie0_cfg_interrupt_0_pending : in STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie0_cfg_interrupt_0_sent : out STD_LOGIC;
            pcie0_cfg_msix_0_address : in STD_LOGIC_VECTOR ( 63 downto 0 );
            pcie0_cfg_msix_0_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie0_cfg_msix_0_enable : out STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie0_cfg_msix_0_fail : out STD_LOGIC;
            pcie0_cfg_msix_0_function_number : in STD_LOGIC_VECTOR ( 7 downto 0 );
            pcie0_cfg_msix_0_int_vector : in STD_LOGIC;
            pcie0_cfg_msix_0_mask : out STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie0_cfg_msix_0_mint_vector : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie0_cfg_msix_0_sent : out STD_LOGIC;
            pcie0_cfg_msix_0_vec_pending : in STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie0_cfg_msix_0_vec_pending_status : out STD_LOGIC;
            pcie0_cfg_status_0_10b_tag_requester_enable : out STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie0_cfg_status_0_atomic_requester_enable : out STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie0_cfg_status_0_cq_np_req : in STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie0_cfg_status_0_cq_np_req_count : out STD_LOGIC_VECTOR ( 5 downto 0 );
            pcie0_cfg_status_0_current_speed : out STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie0_cfg_status_0_err_cor_out : out STD_LOGIC;
            pcie0_cfg_status_0_err_fatal_out : out STD_LOGIC;
            pcie0_cfg_status_0_err_nonfatal_out : out STD_LOGIC;
            pcie0_cfg_status_0_ext_tag_enable : out STD_LOGIC;
            pcie0_cfg_status_0_function_power_state : out STD_LOGIC_VECTOR ( 11 downto 0 );
            pcie0_cfg_status_0_function_status : out STD_LOGIC_VECTOR ( 15 downto 0 );
            pcie0_cfg_status_0_link_power_state : out STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie0_cfg_status_0_local_error_out : out STD_LOGIC_VECTOR ( 4 downto 0 );
            pcie0_cfg_status_0_local_error_valid : out STD_LOGIC;
            pcie0_cfg_status_0_ltssm_state : out STD_LOGIC_VECTOR ( 5 downto 0 );
            pcie0_cfg_status_0_max_payload : out STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie0_cfg_status_0_max_read_req : out STD_LOGIC_VECTOR ( 2 downto 0 );
            pcie0_cfg_status_0_negotiated_width : out STD_LOGIC_VECTOR ( 2 downto 0 );
            pcie0_cfg_status_0_phy_link_down : out STD_LOGIC;
            pcie0_cfg_status_0_phy_link_status : out STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie0_cfg_status_0_pl_status_change : out STD_LOGIC;
            pcie0_cfg_status_0_rcb_status : out STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie0_cfg_status_0_rq_seq_num0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
            pcie0_cfg_status_0_rq_seq_num1 : out STD_LOGIC_VECTOR ( 5 downto 0 );
            pcie0_cfg_status_0_rq_seq_num_vld0 : out STD_LOGIC;
            pcie0_cfg_status_0_rq_seq_num_vld1 : out STD_LOGIC;
            pcie0_cfg_status_0_rq_tag0 : out STD_LOGIC_VECTOR ( 9 downto 0 );
            pcie0_cfg_status_0_rq_tag1 : out STD_LOGIC_VECTOR ( 9 downto 0 );
            pcie0_cfg_status_0_rq_tag_av : out STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie0_cfg_status_0_rq_tag_vld0 : out STD_LOGIC;
            pcie0_cfg_status_0_rq_tag_vld1 : out STD_LOGIC;
            pcie0_cfg_status_0_rx_pm_state : out STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie0_cfg_status_0_tph_requester_enable : out STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie0_cfg_status_0_tph_st_mode : out STD_LOGIC_VECTOR ( 11 downto 0 );
            pcie0_cfg_status_0_tx_pm_state : out STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie0_m_axis_cq_0_tdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
            pcie0_m_axis_cq_0_tkeep : out STD_LOGIC_VECTOR ( 15 downto 0 );
            pcie0_m_axis_cq_0_tlast : out STD_LOGIC;
            pcie0_m_axis_cq_0_tready : in STD_LOGIC;
            pcie0_m_axis_cq_0_tuser : out STD_LOGIC_VECTOR ( 182 downto 0 );
            pcie0_m_axis_cq_0_tvalid : out STD_LOGIC;
            pcie0_m_axis_rc_0_tdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
            pcie0_m_axis_rc_0_tkeep : out STD_LOGIC_VECTOR ( 15 downto 0 );
            pcie0_m_axis_rc_0_tlast : out STD_LOGIC;
            pcie0_m_axis_rc_0_tready : in STD_LOGIC;
            pcie0_m_axis_rc_0_tuser : out STD_LOGIC_VECTOR ( 160 downto 0 );
            pcie0_m_axis_rc_0_tvalid : out STD_LOGIC;
            pcie0_s_axis_cc_0_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
            pcie0_s_axis_cc_0_tkeep : in STD_LOGIC_VECTOR ( 15 downto 0 );
            pcie0_s_axis_cc_0_tlast : in STD_LOGIC;
            pcie0_s_axis_cc_0_tready : out STD_LOGIC;
            pcie0_s_axis_cc_0_tuser : in STD_LOGIC_VECTOR ( 80 downto 0 );
            pcie0_s_axis_cc_0_tvalid : in STD_LOGIC;
            pcie0_s_axis_rq_0_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
            pcie0_s_axis_rq_0_tkeep : in STD_LOGIC_VECTOR ( 15 downto 0 );
            pcie0_s_axis_rq_0_tlast : in STD_LOGIC;
            pcie0_s_axis_rq_0_tready : out STD_LOGIC;
            pcie0_s_axis_rq_0_tuser : in STD_LOGIC_VECTOR ( 178 downto 0 );
            pcie0_s_axis_rq_0_tvalid : in STD_LOGIC;
            pcie0_user_clk_0 : out STD_LOGIC;
            pcie0_user_lnk_up_0 : out STD_LOGIC;
            pcie0_user_reset_0 : out STD_LOGIC;
            pcie1_cfg_control_0_err_cor_in : in STD_LOGIC;
            pcie1_cfg_control_0_err_uncor_in : in STD_LOGIC;
            pcie1_cfg_control_0_flr_done : in STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie1_cfg_control_0_flr_in_process : out STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie1_cfg_control_0_hot_reset_in : in STD_LOGIC;
            pcie1_cfg_control_0_hot_reset_out : out STD_LOGIC;
            pcie1_cfg_control_0_power_state_change_ack : in STD_LOGIC;
            pcie1_cfg_control_0_power_state_change_interrupt : out STD_LOGIC;
            pcie1_cfg_interrupt_0_intx_vector : in STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie1_cfg_interrupt_0_pending : in STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie1_cfg_interrupt_0_sent : out STD_LOGIC;
            pcie1_cfg_msix_0_address : in STD_LOGIC_VECTOR ( 63 downto 0 );
            pcie1_cfg_msix_0_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie1_cfg_msix_0_enable : out STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie1_cfg_msix_0_function_number : in STD_LOGIC_VECTOR ( 7 downto 0 );
            pcie1_cfg_msix_0_int_vector : in STD_LOGIC;
            pcie1_cfg_msix_0_mask : out STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie1_cfg_msix_0_mint_vector : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie1_cfg_msix_0_vec_pending : in STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie1_cfg_status_0_10b_tag_requester_enable : out STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie1_cfg_status_0_cq_np_req : in STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie1_cfg_status_0_cq_np_req_count : out STD_LOGIC_VECTOR ( 5 downto 0 );
            pcie1_cfg_status_0_current_speed : out STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie1_cfg_status_0_err_cor_out : out STD_LOGIC;
            pcie1_cfg_status_0_err_fatal_out : out STD_LOGIC;
            pcie1_cfg_status_0_err_nonfatal_out : out STD_LOGIC;
            pcie1_cfg_status_0_ext_tag_enable : out STD_LOGIC;
            pcie1_cfg_status_0_function_status : out STD_LOGIC_VECTOR ( 15 downto 0 );
            pcie1_cfg_status_0_link_power_state : out STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie1_cfg_status_0_local_error_out : out STD_LOGIC_VECTOR ( 4 downto 0 );
            pcie1_cfg_status_0_local_error_valid : out STD_LOGIC;
            pcie1_cfg_status_0_ltssm_state : out STD_LOGIC_VECTOR ( 5 downto 0 );
            pcie1_cfg_status_0_max_payload : out STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie1_cfg_status_0_max_read_req : out STD_LOGIC_VECTOR ( 2 downto 0 );
            pcie1_cfg_status_0_negotiated_width : out STD_LOGIC_VECTOR ( 2 downto 0 );
            pcie1_cfg_status_0_phy_link_down : out STD_LOGIC;
            pcie1_cfg_status_0_phy_link_status : out STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie1_cfg_status_0_pl_status_change : out STD_LOGIC;
            pcie1_cfg_status_0_rcb_status : out STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie1_cfg_status_0_rq_seq_num0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
            pcie1_cfg_status_0_rq_seq_num1 : out STD_LOGIC_VECTOR ( 5 downto 0 );
            pcie1_cfg_status_0_rq_seq_num_vld0 : out STD_LOGIC;
            pcie1_cfg_status_0_rq_seq_num_vld1 : out STD_LOGIC;
            pcie1_cfg_status_0_rq_tag0 : out STD_LOGIC_VECTOR ( 9 downto 0 );
            pcie1_cfg_status_0_rq_tag1 : out STD_LOGIC_VECTOR ( 9 downto 0 );
            pcie1_cfg_status_0_rq_tag_av : out STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie1_cfg_status_0_rq_tag_vld0 : out STD_LOGIC;
            pcie1_cfg_status_0_rq_tag_vld1 : out STD_LOGIC;
            pcie1_cfg_status_0_rx_pm_state : out STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie1_cfg_status_0_tph_requester_enable : out STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie1_cfg_status_0_tph_st_mode : out STD_LOGIC_VECTOR ( 11 downto 0 );
            pcie1_cfg_status_0_tx_pm_state : out STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie1_m_axis_cq_0_tdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
            pcie1_m_axis_cq_0_tkeep : out STD_LOGIC_VECTOR ( 15 downto 0 );
            pcie1_m_axis_cq_0_tlast : out STD_LOGIC;
            pcie1_m_axis_cq_0_tready : in STD_LOGIC;
            pcie1_m_axis_cq_0_tuser : out STD_LOGIC_VECTOR ( 182 downto 0 );
            pcie1_m_axis_cq_0_tvalid : out STD_LOGIC;
            pcie1_m_axis_rc_0_tdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
            pcie1_m_axis_rc_0_tkeep : out STD_LOGIC_VECTOR ( 15 downto 0 );
            pcie1_m_axis_rc_0_tlast : out STD_LOGIC;
            pcie1_m_axis_rc_0_tready : in STD_LOGIC;
            pcie1_m_axis_rc_0_tuser : out STD_LOGIC_VECTOR ( 160 downto 0 );
            pcie1_m_axis_rc_0_tvalid : out STD_LOGIC;
            pcie1_s_axis_cc_0_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
            pcie1_s_axis_cc_0_tkeep : in STD_LOGIC_VECTOR ( 15 downto 0 );
            pcie1_s_axis_cc_0_tlast : in STD_LOGIC;
            pcie1_s_axis_cc_0_tready : out STD_LOGIC;
            pcie1_s_axis_cc_0_tuser : in STD_LOGIC_VECTOR ( 80 downto 0 );
            pcie1_s_axis_cc_0_tvalid : in STD_LOGIC;
            pcie1_s_axis_rq_0_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
            pcie1_s_axis_rq_0_tkeep : in STD_LOGIC_VECTOR ( 15 downto 0 );
            pcie1_s_axis_rq_0_tlast : in STD_LOGIC;
            pcie1_s_axis_rq_0_tready : out STD_LOGIC;
            pcie1_s_axis_rq_0_tuser : in STD_LOGIC_VECTOR ( 178 downto 0 );
            pcie1_s_axis_rq_0_tvalid : in STD_LOGIC;
            pcie1_user_clk_0 : out STD_LOGIC;
            pcie1_user_lnk_up_0 : out STD_LOGIC;
            pcie1_user_reset_0 : out STD_LOGIC;
            pl0_ref_clk_0 : out STD_LOGIC;
            pl0_resetn : out STD_LOGIC_VECTOR ( 0 to 0 );
            scl_i : in STD_LOGIC;
            scl_o : out STD_LOGIC;
            sda_i : in STD_LOGIC;
            sda_o : out STD_LOGIC;
            sys_clk0_0_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
            sys_clk0_0_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
            FAN_TACH_PIN : out STD_LOGIC_VECTOR ( 0 to 0 )
        );
    end component versal_cpm_pcie_wrapper;


    signal CPMToWupper_s:  CPMToWupper_array_type(0 to 1);
    signal pcie0_cfg_control_flr_done : STD_LOGIC_VECTOR(3 downto 0);
    signal pcie0_cfg_control_flr_in_process : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal pcie0_cfg_control_power_state_change_ack : STD_LOGIC;
    signal pcie0_cfg_control_power_state_change_interrupt : STD_LOGIC;
    signal pcie1_cfg_control_flr_done : STD_LOGIC_VECTOR(3 downto 0);
    signal pcie1_cfg_control_flr_in_process : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal pcie1_cfg_control_power_state_change_ack : STD_LOGIC;
    signal pcie1_cfg_control_power_state_change_interrupt : STD_LOGIC;
    signal pcie0_cfg_status_cq_np_req : std_logic_vector(1 downto 0);
    signal pcie1_cfg_status_cq_np_req : std_logic_vector(1 downto 0);
begin

    init0: entity work.versal_cpm_pcie_init
        port map(
            user_clk                         => CPMToWupper_s(0).user_clk_0,
            user_reset                       => CPMToWupper_s(0).user_reset_0,
            cfg_power_state_change_interrupt => pcie0_cfg_control_power_state_change_interrupt,
            cfg_power_state_change_ack       => pcie0_cfg_control_power_state_change_ack,
            cfg_flr_in_process               => pcie0_cfg_control_flr_in_process,
            cfg_flr_done                     => pcie0_cfg_control_flr_done,
            cfg_status_cq_np_req             => pcie0_cfg_status_cq_np_req
        );
    init1: entity work.versal_cpm_pcie_init
        port map(
            user_clk                         => CPMToWupper_s(1).user_clk_0,
            user_reset                       => CPMToWupper_s(1).user_reset_0,
            cfg_power_state_change_interrupt => pcie1_cfg_control_power_state_change_interrupt,
            cfg_power_state_change_ack       => pcie1_cfg_control_power_state_change_ack,
            cfg_flr_in_process               => pcie1_cfg_control_flr_in_process,
            cfg_flr_done                     => pcie1_cfg_control_flr_done,
            cfg_status_cq_np_req             => pcie1_cfg_status_cq_np_req
        );

    CPMToWupper <= CPMToWupper_s;
    --g_ilas: for i in 0 to 1 generate
    --    ila0 : axis_cc_cq_ila
    --        PORT MAP (
    --            clk => CPMToWupper_s(i).user_clk_0,
    --            probe0 => CPMToWupper_s(i).m_axis_cq_0_tdata ,
    --            probe1 => CPMToWupper_s(i).m_axis_cq_0_tkeep ,
    --            probe2(0) => CPMToWupper_s(i).m_axis_cq_0_tlast ,
    --            probe3(0) => WupperToCPM(i).m_axis_cq_0_tready  ,
    --            probe4 => CPMToWupper_s(i).m_axis_cq_0_tuser ,
    --            probe5(0) => CPMToWupper_s(i).m_axis_cq_0_tvalid,
    --            probe6 => WupperToCPM(i).s_axis_cc_0_tdata   ,
    --            probe7 => WupperToCPM(i).s_axis_cc_0_tkeep   ,
    --            probe8(0) => WupperToCPM(i).s_axis_cc_0_tlast   ,
    --            probe9(0) => CPMToWupper_s(i).s_axis_cc_0_tready,
    --            probe10 =>WupperToCPM(i).s_axis_cc_0_tuser   ,
    --            probe11(0) =>WupperToCPM(i).s_axis_cc_0_tvalid,
    --            probe12(0) => CPMToWupper_s(i).user_reset_0,
    --            probe13(0) => CPMToWupper_s(i).user_lnk_up_0
    --        );
    --end generate;
    g_flx182: if CARD_TYPE = 182 generate
        --! TODO: Re-enable CPM in case of Versal Prime if we need it, but with DDR, I2C etc.
        cpm0: versal_cpm_pcie_wrapper --@suppress
            port map(
                CH0_DDR4_0_0_act_n   => DDR_out(0).act_n,
                CH0_DDR4_0_0_adr     => DDR_out(0).adr,
                CH0_DDR4_0_0_ba      => DDR_out(0).ba,
                CH0_DDR4_0_0_bg      => DDR_out(0).bg,
                CH0_DDR4_0_0_ck_c    => DDR_out(0).ck_c,
                CH0_DDR4_0_0_ck_t    => DDR_out(0).ck_t,
                CH0_DDR4_0_0_cke     => DDR_out(0).cke,
                CH0_DDR4_0_0_cs_n    => DDR_out(0).cs_n,
                CH0_DDR4_0_0_dm_n    => DDR_inout(0).dm_n,
                CH0_DDR4_0_0_dq      => DDR_inout(0).dq,
                CH0_DDR4_0_0_dqs_c   => DDR_inout(0).dqs_c,
                CH0_DDR4_0_0_dqs_t   => DDR_inout(0).dqs_t,
                CH0_DDR4_0_0_odt     => DDR_out(0).odt,
                CH0_DDR4_0_0_reset_n => DDR_out(0).reset_n,
                FPGA_DNA_0           => FPGA_DNA_0,
                M00_AXI_0_araddr     => M00_AXI_0_araddr,
                M00_AXI_0_arprot     => M00_AXI_0_arprot,
                M00_AXI_0_arready => M00_AXI_0_arready,
                M00_AXI_0_arvalid    => M00_AXI_0_arvalid,
                M00_AXI_0_awaddr     => M00_AXI_0_awaddr,
                M00_AXI_0_awprot     => M00_AXI_0_awprot,
                M00_AXI_0_awready => M00_AXI_0_awready,
                M00_AXI_0_awvalid    => M00_AXI_0_awvalid,
                M00_AXI_0_bready     => M00_AXI_0_bready,
                M00_AXI_0_bresp => M00_AXI_0_bresp,
                M00_AXI_0_bvalid => M00_AXI_0_bvalid,
                M00_AXI_0_rdata => M00_AXI_0_rdata,
                M00_AXI_0_rready     => M00_AXI_0_rready,
                M00_AXI_0_rresp => M00_AXI_0_rresp,
                M00_AXI_0_rvalid => M00_AXI_0_rvalid,
                M00_AXI_0_wdata      => M00_AXI_0_wdata,
                M00_AXI_0_wready => M00_AXI_0_wready,
                M00_AXI_0_wstrb      => M00_AXI_0_wstrb,
                M00_AXI_0_wvalid     => M00_AXI_0_wvalid,
                PCIE0_GT_0_grx_n                    => WupperToCPM(0).PCIE_rx_n,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE0_GT_0_grx_p                    => WupperToCPM(0).PCIE_rx_p,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE0_GT_0_gtx_n                    => CPMToWupper_s(0).PCIE_tx_n,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE0_GT_0_gtx_p                    => CPMToWupper_s(0).PCIE_tx_p,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE1_GT_0_grx_n                    => WupperToCPM(1).PCIE_rx_n,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE1_GT_0_grx_p                    => WupperToCPM(1).PCIE_rx_p,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE1_GT_0_gtx_n                    => CPMToWupper_s(1).PCIE_tx_n, --: out STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE1_GT_0_gtx_p                    => CPMToWupper_s(1).PCIE_tx_p, --: out STD_LOGIC_VECTOR ( 7 downto 0 );
                FAN_TACH_PIN                        => FAN_TACH_PIN,
                TEMP_0                              => TEMP_0,
                VCCAUX_0                            => VCCAUX_0,
                VCCBRAM_0                           => VCCBRAM_0,
                VCCINT_0                            => VCCINT_0,
                Versal_network_device_fromHost_din_i => Versal_network_device_fromHost_din_i,
                Versal_network_device_fromHost_eop_i => Versal_network_device_fromHost_eop_i,
                Versal_network_device_fromHost_full_o => Versal_network_device_fromHost_full_o,
                Versal_network_device_fromHost_prog_full_o => Versal_network_device_fromHost_prog_full_o,
                Versal_network_device_fromHost_set_carrier_i => Versal_network_device_fromHost_set_carrier_i,
                Versal_network_device_fromHost_wr_en_i => Versal_network_device_fromHost_wr_en_i,
                Versal_network_device_host_clk => Versal_network_device_host_clk,
                Versal_network_device_toHost_empty_o => Versal_network_device_toHost_empty_o,
                Versal_network_device_toHost_eop_o => Versal_network_device_toHost_eop_o,
                Versal_network_device_toHost_prog_empty_o => Versal_network_device_toHost_prog_empty_o,
                Versal_network_device_toHost_rd_en_i => Versal_network_device_toHost_rd_en_i,
                Versal_network_device_toHost_status_carrier_o => Versal_network_device_toHost_status_carrier_o,
                Versal_network_device_tohost_dout_o => Versal_network_device_tohost_dout_o,
                clk100_out => clk100_out,
                gpio2_io_i_0 => gpio2_io_i_0,
                gpio2_io_o_0 => gpio2_io_o_0,
                gpio_io_i_0 => gpio_io_i_0,
                gpio_io_i_1 => gpio_io_i_1,
                gpio_io_o_0 => gpio_io_o_0,
                gpio_io_o_1 => gpio_io_o_1,
                gt_refclk0_0_clk_n                  => WupperToCPM(0).gtrefclk_n,--: in STD_LOGIC;
                gt_refclk0_0_clk_p                  => WupperToCPM(0).gtrefclk_p,--: in STD_LOGIC;
                gt_refclk1_0_clk_n                  => WupperToCPM(1).gtrefclk_n,--: in STD_LOGIC;
                gt_refclk1_0_clk_p                  => WupperToCPM(1).gtrefclk_p,--: in STD_LOGIC;
                pcie0_cfg_control_0_err_cor_in => '0',
                pcie0_cfg_control_0_err_uncor_in => '0',
                pcie0_cfg_control_0_flr_done => pcie0_cfg_control_flr_done,
                pcie0_cfg_control_0_flr_in_process => pcie0_cfg_control_flr_in_process,
                pcie0_cfg_control_0_hot_reset_in => '0',
                pcie0_cfg_control_0_hot_reset_out => open,
                pcie0_cfg_control_0_power_state_change_ack => pcie0_cfg_control_power_state_change_ack,
                pcie0_cfg_control_0_power_state_change_interrupt => pcie0_cfg_control_power_state_change_interrupt,
                pcie0_cfg_interrupt_0_intx_vector   => WupperToCPM(0).cfg_interrupt_0_intx_vector  ,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie0_cfg_interrupt_0_pending       => WupperToCPM(0).cfg_interrupt_0_pending(3 downto 0)      ,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie0_cfg_interrupt_0_sent          => CPMToWupper_s(0).cfg_interrupt_0_sent         ,--: out STD_LOGIC;
                pcie0_cfg_msix_0_address            => WupperToCPM(0).cfg_msix_0_address           ,--: in STD_LOGIC_VECTOR ( 63 downto 0 );
                pcie0_cfg_msix_0_data               => WupperToCPM(0).cfg_msix_0_data              ,--: in STD_LOGIC_VECTOR ( 31 downto 0 );
                pcie0_cfg_msix_0_enable             => CPMToWupper_s(0).cfg_msix_0_enable            ,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie0_cfg_msix_0_fail               => open, --CPMToWupper_s(0).cfg_msix_0_fail              ,--: out STD_LOGIC;
                pcie0_cfg_msix_0_function_number    => WupperToCPM(0).cfg_msix_0_function_number(7 downto 0)   ,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                pcie0_cfg_msix_0_int_vector         => WupperToCPM(0).cfg_msix_0_int_vector        ,--: in STD_LOGIC;
                pcie0_cfg_msix_0_mask               => CPMToWupper_s(0).cfg_msix_0_mask              ,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie0_cfg_msix_0_mint_vector        => WupperToCPM(0).cfg_msix_0_mint_vector       ,--: in STD_LOGIC_VECTOR ( 31 downto 0 );
                pcie0_cfg_msix_0_sent               => open, --CPMToWupper_s(0).cfg_msix_0_sent              ,--: out STD_LOGIC;
                pcie0_cfg_msix_0_vec_pending        => WupperToCPM(0).cfg_msix_0_vec_pending       ,--: in STD_LOGIC_VECTOR ( 1 downto 0 );
                pcie0_cfg_msix_0_vec_pending_status => open, --CPMToWupper_s(0).cfg_msix_0_vec_pending_status,--: out STD_LOGIC;
                pcie0_cfg_status_0_10b_tag_requester_enable => open,
                pcie0_cfg_status_0_atomic_requester_enable => open,
                pcie0_cfg_status_0_cq_np_req => pcie0_cfg_status_cq_np_req,
                pcie0_cfg_status_0_cq_np_req_count => open,
                pcie0_cfg_status_0_current_speed => open,
                pcie0_cfg_status_0_err_cor_out => open,
                pcie0_cfg_status_0_err_fatal_out => open,
                pcie0_cfg_status_0_err_nonfatal_out => open,
                pcie0_cfg_status_0_ext_tag_enable => open,
                pcie0_cfg_status_0_function_power_state => open,
                pcie0_cfg_status_0_function_status => open,
                pcie0_cfg_status_0_link_power_state => open,
                pcie0_cfg_status_0_local_error_out => open,
                pcie0_cfg_status_0_local_error_valid => open,
                pcie0_cfg_status_0_ltssm_state => open,
                pcie0_cfg_status_0_max_payload => open,
                pcie0_cfg_status_0_max_read_req => open,
                pcie0_cfg_status_0_negotiated_width => open,
                pcie0_cfg_status_0_phy_link_down => open,
                pcie0_cfg_status_0_phy_link_status => open,
                pcie0_cfg_status_0_pl_status_change => open,
                pcie0_cfg_status_0_rcb_status => open,
                pcie0_cfg_status_0_rq_seq_num0 => open,
                pcie0_cfg_status_0_rq_seq_num1 => open,
                pcie0_cfg_status_0_rq_seq_num_vld0 => open,
                pcie0_cfg_status_0_rq_seq_num_vld1 => open,
                pcie0_cfg_status_0_rq_tag0 => open,
                pcie0_cfg_status_0_rq_tag1 => open,
                pcie0_cfg_status_0_rq_tag_av => open,
                pcie0_cfg_status_0_rq_tag_vld0 => open,
                pcie0_cfg_status_0_rq_tag_vld1 => open,
                pcie0_cfg_status_0_rx_pm_state => open,
                pcie0_cfg_status_0_tph_requester_enable => open,
                pcie0_cfg_status_0_tph_st_mode => open,
                pcie0_cfg_status_0_tx_pm_state => open,
                pcie0_m_axis_cq_0_tdata             => CPMToWupper_s(0).m_axis_cq_0_tdata(511 downto 0)            ,--: out STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie0_m_axis_cq_0_tkeep             => CPMToWupper_s(0).m_axis_cq_0_tkeep(15 downto 0)            ,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie0_m_axis_cq_0_tlast             => CPMToWupper_s(0).m_axis_cq_0_tlast            ,--: out STD_LOGIC;
                pcie0_m_axis_cq_0_tready            => WupperToCPM(0).m_axis_cq_0_tready           ,--: in STD_LOGIC;
                pcie0_m_axis_cq_0_tuser             => CPMToWupper_s(0).m_axis_cq_0_tuser(182 downto 0)            ,--: out STD_LOGIC_VECTOR ( 182 downto 0 );
                pcie0_m_axis_cq_0_tvalid            => CPMToWupper_s(0).m_axis_cq_0_tvalid           ,--: out STD_LOGIC;
                pcie0_m_axis_rc_0_tdata             => CPMToWupper_s(0).m_axis_rc_0_tdata(511 downto 0)            ,--: out STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie0_m_axis_rc_0_tkeep             => CPMToWupper_s(0).m_axis_rc_0_tkeep(15 downto 0)            ,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie0_m_axis_rc_0_tlast             => CPMToWupper_s(0).m_axis_rc_0_tlast            ,--: out STD_LOGIC;
                pcie0_m_axis_rc_0_tready            => WupperToCPM(0).m_axis_rc_0_tready           ,--: in STD_LOGIC;
                pcie0_m_axis_rc_0_tuser             => CPMToWupper_s(0).m_axis_rc_0_tuser(160 downto 0)            ,--: out STD_LOGIC_VECTOR ( 160 downto 0 );
                pcie0_m_axis_rc_0_tvalid            => CPMToWupper_s(0).m_axis_rc_0_tvalid           ,--: out STD_LOGIC;
                pcie0_s_axis_cc_0_tdata             => WupperToCPM(0).s_axis_cc_0_tdata(511 downto 0)            ,--: in STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie0_s_axis_cc_0_tkeep             => WupperToCPM(0).s_axis_cc_0_tkeep(15 downto 0)            ,--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie0_s_axis_cc_0_tlast             => WupperToCPM(0).s_axis_cc_0_tlast            ,--: in STD_LOGIC;
                pcie0_s_axis_cc_0_tready            => CPMToWupper_s(0).s_axis_cc_0_tready           ,--: out STD_LOGIC;
                pcie0_s_axis_cc_0_tuser             => WupperToCPM(0).s_axis_cc_0_tuser(80 downto 0)            ,--: in STD_LOGIC_VECTOR ( 80 downto 0 );
                pcie0_s_axis_cc_0_tvalid            => WupperToCPM(0).s_axis_cc_0_tvalid           ,--: in STD_LOGIC;
                pcie0_s_axis_rq_0_tdata             => WupperToCPM(0).s_axis_rq_0_tdata(511 downto 0)            ,--: in STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie0_s_axis_rq_0_tkeep             => WupperToCPM(0).s_axis_rq_0_tkeep(15 downto 0)            ,--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie0_s_axis_rq_0_tlast             => WupperToCPM(0).s_axis_rq_0_tlast            ,--: in STD_LOGIC;
                pcie0_s_axis_rq_0_tready            => CPMToWupper_s(0).s_axis_rq_0_tready           ,--: out STD_LOGIC;
                pcie0_s_axis_rq_0_tuser             => WupperToCPM(0).s_axis_rq_0_tuser(178 downto 0)            ,--: in STD_LOGIC_VECTOR ( 178 downto 0 );
                pcie0_s_axis_rq_0_tvalid            => WupperToCPM(0).s_axis_rq_0_tvalid           ,--: in STD_LOGIC;
                pcie0_user_clk_0                    => CPMToWupper_s(0).user_clk_0                   ,--: out STD_LOGIC;
                pcie0_user_lnk_up_0                 => CPMToWupper_s(0).user_lnk_up_0                ,--: out STD_LOGIC;
                pcie0_user_reset_0                  => CPMToWupper_s(0).user_reset_0                 ,--: out STD_LOGIC;
                pcie1_cfg_control_0_err_cor_in => '0',
                pcie1_cfg_control_0_err_uncor_in => '0',
                pcie1_cfg_control_0_flr_done => pcie1_cfg_control_flr_done,
                pcie1_cfg_control_0_flr_in_process => pcie1_cfg_control_flr_in_process,
                pcie1_cfg_control_0_hot_reset_in => '0',
                pcie1_cfg_control_0_hot_reset_out => open,
                pcie1_cfg_control_0_power_state_change_ack => pcie1_cfg_control_power_state_change_ack,
                pcie1_cfg_control_0_power_state_change_interrupt => pcie1_cfg_control_power_state_change_interrupt,
                pcie1_cfg_interrupt_0_intx_vector   => WupperToCPM(1).cfg_interrupt_0_intx_vector  ,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie1_cfg_interrupt_0_pending       => WupperToCPM(1).cfg_interrupt_0_pending(3 downto 0)      ,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie1_cfg_interrupt_0_sent          => CPMToWupper_s(1).cfg_interrupt_0_sent         ,--: out STD_LOGIC;
                pcie1_cfg_msix_0_address            => WupperToCPM(1).cfg_msix_0_address           ,--: in STD_LOGIC_VECTOR ( 63 downto 0 );
                pcie1_cfg_msix_0_data               => WupperToCPM(1).cfg_msix_0_data              ,--: in STD_LOGIC_VECTOR ( 31 downto 0 );
                pcie1_cfg_msix_0_enable             => CPMToWupper_s(1).cfg_msix_0_enable            ,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
                --pcie1_cfg_msix_0_fail               => CPMToWupper_s(1).cfg_msix_0_fail              ,
                pcie1_cfg_msix_0_function_number    => WupperToCPM(1).cfg_msix_0_function_number(7 downto 0)   ,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                pcie1_cfg_msix_0_int_vector         => WupperToCPM(1).cfg_msix_0_int_vector        ,--: in STD_LOGIC;
                pcie1_cfg_msix_0_mask               => CPMToWupper_s(1).cfg_msix_0_mask              ,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie1_cfg_msix_0_mint_vector        => WupperToCPM(1).cfg_msix_0_mint_vector       ,--: in STD_LOGIC_VECTOR ( 31 downto 0 );
                --pcie1_cfg_msix_0_sent               => CPMToWupper_s(1).cfg_msix_0_sent             ,
                pcie1_cfg_msix_0_vec_pending        => WupperToCPM(1).cfg_msix_0_vec_pending       ,--: in STD_LOGIC_VECTOR ( 1 downto 0 );
                pcie1_cfg_status_0_10b_tag_requester_enable => open,
                pcie1_cfg_status_0_cq_np_req => pcie1_cfg_status_cq_np_req,
                pcie1_cfg_status_0_cq_np_req_count => open,
                pcie1_cfg_status_0_current_speed => open,
                pcie1_cfg_status_0_err_cor_out => open,
                pcie1_cfg_status_0_err_fatal_out => open,
                pcie1_cfg_status_0_err_nonfatal_out => open,
                pcie1_cfg_status_0_ext_tag_enable => open,
                pcie1_cfg_status_0_function_status => open,
                pcie1_cfg_status_0_link_power_state => open,
                pcie1_cfg_status_0_local_error_out => open,
                pcie1_cfg_status_0_local_error_valid => open,
                pcie1_cfg_status_0_ltssm_state => open,
                pcie1_cfg_status_0_max_payload => open,
                pcie1_cfg_status_0_max_read_req => open,
                pcie1_cfg_status_0_negotiated_width => open,
                pcie1_cfg_status_0_phy_link_down => open,
                pcie1_cfg_status_0_phy_link_status => open,
                pcie1_cfg_status_0_pl_status_change => open,
                pcie1_cfg_status_0_rcb_status =>open,
                pcie1_cfg_status_0_rq_seq_num0 => open,
                pcie1_cfg_status_0_rq_seq_num1 => open,
                pcie1_cfg_status_0_rq_seq_num_vld0 => open,
                pcie1_cfg_status_0_rq_seq_num_vld1 => open,
                pcie1_cfg_status_0_rq_tag0 => open,
                pcie1_cfg_status_0_rq_tag1 => open,
                pcie1_cfg_status_0_rq_tag_av => open,
                pcie1_cfg_status_0_rq_tag_vld0 => open,
                pcie1_cfg_status_0_rq_tag_vld1 => open,
                pcie1_cfg_status_0_rx_pm_state => open,
                pcie1_cfg_status_0_tph_requester_enable => open,
                pcie1_cfg_status_0_tph_st_mode => open,
                pcie1_cfg_status_0_tx_pm_state => open,
                --pcie1_cfg_msix_0_vec_pending_status => CPMToWupper_s(1).cfg_msix_0_vec_pending_status,
                pcie1_m_axis_cq_0_tdata             => CPMToWupper_s(1).m_axis_cq_0_tdata(511 downto 0)            ,--: out STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie1_m_axis_cq_0_tkeep             => CPMToWupper_s(1).m_axis_cq_0_tkeep(15 downto 0)            ,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie1_m_axis_cq_0_tlast             => CPMToWupper_s(1).m_axis_cq_0_tlast            ,--: out STD_LOGIC;
                pcie1_m_axis_cq_0_tready            => WupperToCPM(1).m_axis_cq_0_tready           ,--: in STD_LOGIC;
                pcie1_m_axis_cq_0_tuser             => CPMToWupper_s(1).m_axis_cq_0_tuser(182 downto 0)            ,--: out STD_LOGIC_VECTOR ( 182 downto 0 );
                pcie1_m_axis_cq_0_tvalid            => CPMToWupper_s(1).m_axis_cq_0_tvalid           ,--: out STD_LOGIC;
                pcie1_m_axis_rc_0_tdata             => CPMToWupper_s(1).m_axis_rc_0_tdata(511 downto 0)            ,--: out STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie1_m_axis_rc_0_tkeep             => CPMToWupper_s(1).m_axis_rc_0_tkeep(15 downto 0)            ,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie1_m_axis_rc_0_tlast             => CPMToWupper_s(1).m_axis_rc_0_tlast            ,--: out STD_LOGIC;
                pcie1_m_axis_rc_0_tready            => WupperToCPM(1).m_axis_rc_0_tready           ,--: in STD_LOGIC;
                pcie1_m_axis_rc_0_tuser             => CPMToWupper_s(1).m_axis_rc_0_tuser(160 downto 0)            ,--: out STD_LOGIC_VECTOR ( 160 downto 0 );
                pcie1_m_axis_rc_0_tvalid            => CPMToWupper_s(1).m_axis_rc_0_tvalid           ,--: out STD_LOGIC;
                pcie1_s_axis_cc_0_tdata             => WupperToCPM(1).s_axis_cc_0_tdata(511 downto 0)            ,--: in STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie1_s_axis_cc_0_tkeep             => WupperToCPM(1).s_axis_cc_0_tkeep(15 downto 0)            ,--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie1_s_axis_cc_0_tlast             => WupperToCPM(1).s_axis_cc_0_tlast            ,--: in STD_LOGIC;
                pcie1_s_axis_cc_0_tready            => CPMToWupper_s(1).s_axis_cc_0_tready           ,--: out STD_LOGIC;
                pcie1_s_axis_cc_0_tuser             => WupperToCPM(1).s_axis_cc_0_tuser(80 downto 0)            ,--: in STD_LOGIC_VECTOR ( 80 downto 0 );
                pcie1_s_axis_cc_0_tvalid            => WupperToCPM(1).s_axis_cc_0_tvalid           ,--: in STD_LOGIC;
                pcie1_s_axis_rq_0_tdata             => WupperToCPM(1).s_axis_rq_0_tdata(511 downto 0)            ,--: in STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie1_s_axis_rq_0_tkeep             => WupperToCPM(1).s_axis_rq_0_tkeep(15 downto 0)            ,--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie1_s_axis_rq_0_tlast             => WupperToCPM(1).s_axis_rq_0_tlast            ,--: in STD_LOGIC;
                pcie1_s_axis_rq_0_tready            => CPMToWupper_s(1).s_axis_rq_0_tready           ,--: out STD_LOGIC;
                pcie1_s_axis_rq_0_tuser             => WupperToCPM(1).s_axis_rq_0_tuser(178 downto 0)            ,--: in STD_LOGIC_VECTOR ( 178 downto 0 );
                pcie1_s_axis_rq_0_tvalid            => WupperToCPM(1).s_axis_rq_0_tvalid           ,--: in STD_LOGIC;
                pcie1_user_clk_0                    => CPMToWupper_s(1).user_clk_0                   ,--: out STD_LOGIC;
                pcie1_user_lnk_up_0                 => CPMToWupper_s(1).user_lnk_up_0                ,--: out STD_LOGIC;
                pcie1_user_reset_0                  => CPMToWupper_s(1).user_reset_0                 , --: out STD_LOGIC
                pl0_ref_clk_0                       => pl0_ref_clk,
                pl0_resetn => pl0_resetn,
                scl_i => scl_i,
                scl_o => scl_o,
                sda_i => sda_i,
                sda_o => sda_o,
                sys_clk0_0_clk_n => DDR_in(0).sys_clk_n,
                sys_clk0_0_clk_p => DDR_in(0).sys_clk_p
            );
    end generate g_flx182;

    g_flx155: if CARD_TYPE = 155 generate
        cpm0: FLX155_cips_wrapper
            port map(
                CH0_DDR4_0_0_act_n   => DDR_out(0).act_n,
                CH0_DDR4_0_0_adr     => DDR_out(0).adr,
                CH0_DDR4_0_0_ba      => DDR_out(0).ba,
                CH0_DDR4_0_0_bg      => DDR_out(0).bg,
                CH0_DDR4_0_0_ck_c    => DDR_out(0).ck_c,
                CH0_DDR4_0_0_ck_t    => DDR_out(0).ck_t,
                CH0_DDR4_0_0_cke     => DDR_out(0).cke,
                CH0_DDR4_0_0_cs_n    => DDR_out(0).cs_n,
                CH0_DDR4_0_0_dm_n    => DDR_inout(0).dm_n,
                CH0_DDR4_0_0_dq      => DDR_inout(0).dq,
                CH0_DDR4_0_0_dqs_c   => DDR_inout(0).dqs_c,
                CH0_DDR4_0_0_dqs_t   => DDR_inout(0).dqs_t,
                CH0_DDR4_0_0_odt     => DDR_out(0).odt,
                CH0_DDR4_0_0_reset_n => DDR_out(0).reset_n,
                PCIE0_GT_0_grx_n                    => WupperToCPM(0).PCIE_rx_n,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE0_GT_0_grx_p                    => WupperToCPM(0).PCIE_rx_p,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE0_GT_0_gtx_n                    => CPMToWupper_s(0).PCIE_tx_n,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE0_GT_0_gtx_p                    => CPMToWupper_s(0).PCIE_tx_p,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE1_GT_0_grx_n                    => WupperToCPM(1).PCIE_rx_n,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE1_GT_0_grx_p                    => WupperToCPM(1).PCIE_rx_p,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE1_GT_0_gtx_n                    => CPMToWupper_s(1).PCIE_tx_n, --: out STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE1_GT_0_gtx_p                    => CPMToWupper_s(1).PCIE_tx_p, --: out STD_LOGIC_VECTOR ( 7 downto 0 );
                clk100_out => clk100_out,
                gpio2_io_i_0 => gpio2_io_i_0,
                gpio2_io_o_0 => gpio2_io_o_0,
                gpio_io_i_0 => gpio_io_i_0,
                gpio_io_o_0 => gpio_io_o_0,
                gt_refclk0_0_clk_n                  => WupperToCPM(0).gtrefclk_n,--: in STD_LOGIC;
                gt_refclk0_0_clk_p                  => WupperToCPM(0).gtrefclk_p,--: in STD_LOGIC;
                gt_refclk1_0_clk_n                  => WupperToCPM(1).gtrefclk_n,--: in STD_LOGIC;
                gt_refclk1_0_clk_p                  => WupperToCPM(1).gtrefclk_p,--: in STD_LOGIC;
                pcie0_cfg_control_flr_done_0        => pcie0_cfg_control_flr_done(0),
                pcie0_cfg_control_flr_in_process_0  => pcie0_cfg_control_flr_in_process,
                pcie0_cfg_control_power_state_change_ack_0 => pcie0_cfg_control_power_state_change_ack,
                pcie0_cfg_control_power_state_change_interrupt_0 => pcie0_cfg_control_power_state_change_interrupt,
                pcie0_cfg_interrupt_0_intx_vector   => WupperToCPM(0).cfg_interrupt_0_intx_vector  ,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie0_cfg_interrupt_0_pending       => WupperToCPM(0).cfg_interrupt_0_pending      ,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie0_cfg_interrupt_0_sent          => CPMToWupper_s(0).cfg_interrupt_0_sent         ,--: out STD_LOGIC;
                pcie0_cfg_msi_0_fail               => open, --CPMToWupper_s(0).cfg_msix_0_fail              ,--: out STD_LOGIC;
                pcie0_cfg_msi_0_function_number     => WupperToCPM(0).cfg_msix_0_function_number   ,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                pcie0_cfg_msi_0_int_vector => (others => '0'),
                pcie0_cfg_msi_0_select => "0000",
                pcie0_cfg_msi_0_sent => open,
                pcie0_cfg_msix_0_address            => WupperToCPM(0).cfg_msix_0_address           ,--: in STD_LOGIC_VECTOR ( 63 downto 0 );
                pcie0_cfg_msix_0_data               => WupperToCPM(0).cfg_msix_0_data              ,--: in STD_LOGIC_VECTOR ( 31 downto 0 );
                pcie0_cfg_msix_0_enable             => CPMToWupper_s(0).cfg_msix_0_enable(0)            ,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie0_cfg_msix_0_int_vector         => WupperToCPM(0).cfg_msix_0_int_vector        ,--: in STD_LOGIC;
                pcie0_cfg_msix_0_mask               => CPMToWupper_s(0).cfg_msix_0_mask(0)              ,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
                --pcie0_cfg_msix_0_mint_vector        => WupperToCPM(0).cfg_msix_0_mint_vector       ,--: in STD_LOGIC_VECTOR ( 31 downto 0 );
                --pcie0_cfg_msix_0_sent               => open, --CPMToWupper_s(0).cfg_msix_0_sent              ,--: out STD_LOGIC;
                pcie0_cfg_msix_0_vec_pending        => WupperToCPM(0).cfg_msix_0_vec_pending       ,--: in STD_LOGIC_VECTOR ( 1 downto 0 );
                pcie0_cfg_msix_0_vec_pending_status => open, --CPMToWupper_s(0).cfg_msix_0_vec_pending_status,--: out STD_LOGIC;
                pcie0_cfg_status_cq_np_req_0        => pcie0_cfg_status_cq_np_req,
                pcie0_m_axis_cq_0_tdata             => CPMToWupper_s(0).m_axis_cq_0_tdata            ,--: out STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie0_m_axis_cq_0_tkeep             => CPMToWupper_s(0).m_axis_cq_0_tkeep            ,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie0_m_axis_cq_0_tlast             => CPMToWupper_s(0).m_axis_cq_0_tlast            ,--: out STD_LOGIC;
                pcie0_m_axis_cq_0_tready            => WupperToCPM(0).m_axis_cq_0_tready           ,--: in STD_LOGIC;
                pcie0_m_axis_cq_0_tuser             => CPMToWupper_s(0).m_axis_cq_0_tuser            ,--: out STD_LOGIC_VECTOR ( 182 downto 0 );
                pcie0_m_axis_cq_0_tvalid            => CPMToWupper_s(0).m_axis_cq_0_tvalid           ,--: out STD_LOGIC;
                pcie0_m_axis_rc_0_tdata             => CPMToWupper_s(0).m_axis_rc_0_tdata            ,--: out STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie0_m_axis_rc_0_tkeep             => CPMToWupper_s(0).m_axis_rc_0_tkeep            ,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie0_m_axis_rc_0_tlast             => CPMToWupper_s(0).m_axis_rc_0_tlast            ,--: out STD_LOGIC;
                pcie0_m_axis_rc_0_tready            => WupperToCPM(0).m_axis_rc_0_tready           ,--: in STD_LOGIC;
                pcie0_m_axis_rc_0_tuser             => CPMToWupper_s(0).m_axis_rc_0_tuser            ,--: out STD_LOGIC_VECTOR ( 160 downto 0 );
                pcie0_m_axis_rc_0_tvalid            => CPMToWupper_s(0).m_axis_rc_0_tvalid           ,--: out STD_LOGIC;
                pcie0_s_axis_cc_0_tdata             => WupperToCPM(0).s_axis_cc_0_tdata            ,--: in STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie0_s_axis_cc_0_tkeep             => WupperToCPM(0).s_axis_cc_0_tkeep            ,--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie0_s_axis_cc_0_tlast             => WupperToCPM(0).s_axis_cc_0_tlast            ,--: in STD_LOGIC;
                pcie0_s_axis_cc_0_tready            => CPMToWupper_s(0).s_axis_cc_0_tready           ,--: out STD_LOGIC;
                pcie0_s_axis_cc_0_tuser             => WupperToCPM(0).s_axis_cc_0_tuser            ,--: in STD_LOGIC_VECTOR ( 80 downto 0 );
                pcie0_s_axis_cc_0_tvalid            => WupperToCPM(0).s_axis_cc_0_tvalid           ,--: in STD_LOGIC;
                pcie0_s_axis_rq_0_tdata             => WupperToCPM(0).s_axis_rq_0_tdata            ,--: in STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie0_s_axis_rq_0_tkeep             => WupperToCPM(0).s_axis_rq_0_tkeep            ,--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie0_s_axis_rq_0_tlast             => WupperToCPM(0).s_axis_rq_0_tlast            ,--: in STD_LOGIC;
                pcie0_s_axis_rq_0_tready            => CPMToWupper_s(0).s_axis_rq_0_tready           ,--: out STD_LOGIC;
                pcie0_s_axis_rq_0_tuser             => WupperToCPM(0).s_axis_rq_0_tuser            ,--: in STD_LOGIC_VECTOR ( 178 downto 0 );
                pcie0_s_axis_rq_0_tvalid            => WupperToCPM(0).s_axis_rq_0_tvalid           ,--: in STD_LOGIC;
                pcie0_user_clk_0                    => CPMToWupper_s(0).user_clk_0                   ,--: out STD_LOGIC;
                pcie0_user_lnk_up_0                 => CPMToWupper_s(0).user_lnk_up_0                ,--: out STD_LOGIC;
                pcie0_user_reset_0                  => CPMToWupper_s(0).user_reset_0                 ,--: out STD_LOGIC;
                pcie1_cfg_control_flr_done_0        => pcie1_cfg_control_flr_done(0),
                pcie1_cfg_control_flr_in_process_0  => pcie1_cfg_control_flr_in_process,
                pcie1_cfg_control_power_state_change_ack_0 => pcie1_cfg_control_power_state_change_ack,
                pcie1_cfg_control_power_state_change_interrupt_0 => pcie1_cfg_control_power_state_change_interrupt,
                pcie1_cfg_interrupt_0_intx_vector   => WupperToCPM(1).cfg_interrupt_0_intx_vector  ,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie1_cfg_interrupt_0_pending       => WupperToCPM(1).cfg_interrupt_0_pending      ,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie1_cfg_interrupt_0_sent          => CPMToWupper_s(1).cfg_interrupt_0_sent         ,--: out STD_LOGIC;
                pcie1_cfg_msi_0_fail => open,
                --pcie1_cfg_msix_0_fail               => CPMToWupper_s(1).cfg_msix_0_fail              ,
                pcie1_cfg_msi_0_function_number     => WupperToCPM(1).cfg_msix_0_function_number   ,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                pcie1_cfg_msi_0_int_vector => (others => '0'),
                pcie1_cfg_msi_0_select => "0000",
                pcie1_cfg_msi_0_sent => open,
                pcie1_cfg_msix_0_address            => WupperToCPM(1).cfg_msix_0_address           ,--: in STD_LOGIC_VECTOR ( 63 downto 0 );
                pcie1_cfg_msix_0_data               => WupperToCPM(1).cfg_msix_0_data              ,--: in STD_LOGIC_VECTOR ( 31 downto 0 );
                pcie1_cfg_msix_0_enable             => CPMToWupper_s(1).cfg_msix_0_enable(0)            ,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie1_cfg_msix_0_int_vector         => WupperToCPM(1).cfg_msix_0_int_vector        ,--: in STD_LOGIC;
                pcie1_cfg_msix_0_mask               => CPMToWupper_s(1).cfg_msix_0_mask(0)              ,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
                --pcie1_cfg_msix_0_mint_vector        => WupperToCPM(1).cfg_msix_0_mint_vector       ,--: in STD_LOGIC_VECTOR ( 31 downto 0 );
                --pcie1_cfg_msix_0_sent               => CPMToWupper_s(1).cfg_msix_0_sent             ,
                pcie1_cfg_msix_0_vec_pending        => WupperToCPM(1).cfg_msix_0_vec_pending       ,--: in STD_LOGIC_VECTOR ( 1 downto 0 );
                pcie1_cfg_msix_0_vec_pending_status => open,
                pcie1_cfg_status_cq_np_req_0        => pcie1_cfg_status_cq_np_req,
                --pcie1_cfg_msix_0_vec_pending_status => CPMToWupper_s(1).cfg_msix_0_vec_pending_status,
                pcie1_m_axis_cq_0_tdata             => CPMToWupper_s(1).m_axis_cq_0_tdata            ,--: out STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie1_m_axis_cq_0_tkeep             => CPMToWupper_s(1).m_axis_cq_0_tkeep            ,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie1_m_axis_cq_0_tlast             => CPMToWupper_s(1).m_axis_cq_0_tlast            ,--: out STD_LOGIC;
                pcie1_m_axis_cq_0_tready            => WupperToCPM(1).m_axis_cq_0_tready           ,--: in STD_LOGIC;
                pcie1_m_axis_cq_0_tuser             => CPMToWupper_s(1).m_axis_cq_0_tuser            ,--: out STD_LOGIC_VECTOR ( 182 downto 0 );
                pcie1_m_axis_cq_0_tvalid            => CPMToWupper_s(1).m_axis_cq_0_tvalid           ,--: out STD_LOGIC;
                pcie1_m_axis_rc_0_tdata             => CPMToWupper_s(1).m_axis_rc_0_tdata            ,--: out STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie1_m_axis_rc_0_tkeep             => CPMToWupper_s(1).m_axis_rc_0_tkeep            ,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie1_m_axis_rc_0_tlast             => CPMToWupper_s(1).m_axis_rc_0_tlast            ,--: out STD_LOGIC;
                pcie1_m_axis_rc_0_tready            => WupperToCPM(1).m_axis_rc_0_tready           ,--: in STD_LOGIC;
                pcie1_m_axis_rc_0_tuser             => CPMToWupper_s(1).m_axis_rc_0_tuser            ,--: out STD_LOGIC_VECTOR ( 160 downto 0 );
                pcie1_m_axis_rc_0_tvalid            => CPMToWupper_s(1).m_axis_rc_0_tvalid           ,--: out STD_LOGIC;
                pcie1_s_axis_cc_0_tdata             => WupperToCPM(1).s_axis_cc_0_tdata            ,--: in STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie1_s_axis_cc_0_tkeep             => WupperToCPM(1).s_axis_cc_0_tkeep            ,--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie1_s_axis_cc_0_tlast             => WupperToCPM(1).s_axis_cc_0_tlast            ,--: in STD_LOGIC;
                pcie1_s_axis_cc_0_tready            => CPMToWupper_s(1).s_axis_cc_0_tready           ,--: out STD_LOGIC;
                pcie1_s_axis_cc_0_tuser             => WupperToCPM(1).s_axis_cc_0_tuser            ,--: in STD_LOGIC_VECTOR ( 80 downto 0 );
                pcie1_s_axis_cc_0_tvalid            => WupperToCPM(1).s_axis_cc_0_tvalid           ,--: in STD_LOGIC;
                pcie1_s_axis_rq_0_tdata             => WupperToCPM(1).s_axis_rq_0_tdata            ,--: in STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie1_s_axis_rq_0_tkeep             => WupperToCPM(1).s_axis_rq_0_tkeep            ,--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie1_s_axis_rq_0_tlast             => WupperToCPM(1).s_axis_rq_0_tlast            ,--: in STD_LOGIC;
                pcie1_s_axis_rq_0_tready            => CPMToWupper_s(1).s_axis_rq_0_tready           ,--: out STD_LOGIC;
                pcie1_s_axis_rq_0_tuser             => WupperToCPM(1).s_axis_rq_0_tuser            ,--: in STD_LOGIC_VECTOR ( 178 downto 0 );
                pcie1_s_axis_rq_0_tvalid            => WupperToCPM(1).s_axis_rq_0_tvalid           ,--: in STD_LOGIC;
                pcie1_user_clk_0                    => CPMToWupper_s(1).user_clk_0                   ,--: out STD_LOGIC;
                pcie1_user_lnk_up_0                 => CPMToWupper_s(1).user_lnk_up_0                ,--: out STD_LOGIC;
                pcie1_user_reset_0                  => CPMToWupper_s(1).user_reset_0                 , --: out STD_LOGIC
                pl0_ref_clk_0                       => pl0_ref_clk,
                pl0_resetn_0 => pl0_resetn(0),
                scl_i => scl_i,
                scl_o => scl_o,
                sda_i => sda_i,
                sda_o => sda_o,
                sys_clk0_0_clk_n => DDR_in(0).sys_clk_n,
                sys_clk0_0_clk_p => DDR_in(0).sys_clk_p
            );
    end generate g_flx155;

    g_flx120: if CARD_TYPE = 120 generate
        cpm0: FLX120_cips_wrapper
            port map(

                PCIE0_GT_0_grx_n                    => WupperToCPM(0).PCIE_rx_n,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE0_GT_0_grx_p                    => WupperToCPM(0).PCIE_rx_p,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE0_GT_0_gtx_n                    => CPMToWupper_s(0).PCIE_tx_n,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE0_GT_0_gtx_p                    => CPMToWupper_s(0).PCIE_tx_p,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE1_GT_0_grx_n                    => WupperToCPM(1).PCIE_rx_n,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE1_GT_0_grx_p                    => WupperToCPM(1).PCIE_rx_p,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE1_GT_0_gtx_n                    => CPMToWupper_s(1).PCIE_tx_n, --: out STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE1_GT_0_gtx_p                    => CPMToWupper_s(1).PCIE_tx_p, --: out STD_LOGIC_VECTOR ( 7 downto 0 );
                ch0_lpddr4_trip1_ca_a =>    LPDDR_out(0).ca_a,
                ch0_lpddr4_trip1_ca_b =>    LPDDR_out(0).ca_b,
                ch0_lpddr4_trip1_ck_c_a =>  LPDDR_out(0).ck_c_a(0),
                ch0_lpddr4_trip1_ck_c_b =>  LPDDR_out(0).ck_c_b(0),
                ch0_lpddr4_trip1_ck_t_a =>  LPDDR_out(0).ck_t_a(0),
                ch0_lpddr4_trip1_ck_t_b =>  LPDDR_out(0).ck_t_b(0),
                ch0_lpddr4_trip1_cke_a =>   LPDDR_out(0).cke_a(0),
                ch0_lpddr4_trip1_cke_b =>   LPDDR_out(0).cke_b(0),
                ch0_lpddr4_trip1_cs_a =>    LPDDR_out(0).cs_a(0),
                ch0_lpddr4_trip1_cs_b =>    LPDDR_out(0).cs_b(0),
                ch0_lpddr4_trip1_dmi_a =>   LPDDR_inout(0).dmi_a,
                ch0_lpddr4_trip1_dmi_b =>   LPDDR_inout(0).dmi_b,
                ch0_lpddr4_trip1_dq_a =>    LPDDR_inout(0).dq_a,
                ch0_lpddr4_trip1_dq_b =>    LPDDR_inout(0).dq_b,
                ch0_lpddr4_trip1_dqs_c_a => LPDDR_inout(0).dqs_c_a,
                ch0_lpddr4_trip1_dqs_c_b => LPDDR_inout(0).dqs_c_b,
                ch0_lpddr4_trip1_dqs_t_a => LPDDR_inout(0).dqs_t_a,
                ch0_lpddr4_trip1_dqs_t_b => LPDDR_inout(0).dqs_t_b,
                ch0_lpddr4_trip1_reset_n => LPDDR_out(0).reset_n(0),
                ch0_lpddr4_trip2_ca_a =>    LPDDR_out(1).ca_a,
                ch0_lpddr4_trip2_ca_b =>    LPDDR_out(1).ca_b,
                ch0_lpddr4_trip2_ck_c_a =>  LPDDR_out(1).ck_c_a(0),
                ch0_lpddr4_trip2_ck_c_b =>  LPDDR_out(1).ck_c_b(0),
                ch0_lpddr4_trip2_ck_t_a =>  LPDDR_out(1).ck_t_a(0),
                ch0_lpddr4_trip2_ck_t_b =>  LPDDR_out(1).ck_t_b(0),
                ch0_lpddr4_trip2_cke_a =>   LPDDR_out(1).cke_a(0),
                ch0_lpddr4_trip2_cke_b =>   LPDDR_out(1).cke_b(0),
                ch0_lpddr4_trip2_cs_a =>    LPDDR_out(1).cs_a(0),
                ch0_lpddr4_trip2_cs_b =>    LPDDR_out(1).cs_b(0),
                ch0_lpddr4_trip2_dmi_a =>   LPDDR_inout(1).dmi_a,
                ch0_lpddr4_trip2_dmi_b =>   LPDDR_inout(1).dmi_b,
                ch0_lpddr4_trip2_dq_a =>    LPDDR_inout(1).dq_a,
                ch0_lpddr4_trip2_dq_b =>    LPDDR_inout(1).dq_b,
                ch0_lpddr4_trip2_dqs_c_a => LPDDR_inout(1).dqs_c_a,
                ch0_lpddr4_trip2_dqs_c_b => LPDDR_inout(1).dqs_c_b,
                ch0_lpddr4_trip2_dqs_t_a => LPDDR_inout(1).dqs_t_a,
                ch0_lpddr4_trip2_dqs_t_b => LPDDR_inout(1).dqs_t_b,
                ch0_lpddr4_trip2_reset_n => LPDDR_out(1).reset_n(0),
                ch1_lpddr4_trip1_ca_a =>    LPDDR_out(2).ca_a,
                ch1_lpddr4_trip1_ca_b =>    LPDDR_out(2).ca_b,
                ch1_lpddr4_trip1_ck_c_a =>  LPDDR_out(2).ck_c_a(0),
                ch1_lpddr4_trip1_ck_c_b =>  LPDDR_out(2).ck_c_b(0),
                ch1_lpddr4_trip1_ck_t_a =>  LPDDR_out(2).ck_t_a(0),
                ch1_lpddr4_trip1_ck_t_b =>  LPDDR_out(2).ck_t_b(0),
                ch1_lpddr4_trip1_cke_a =>   LPDDR_out(2).cke_a(0),
                ch1_lpddr4_trip1_cke_b =>   LPDDR_out(2).cke_b(0),
                ch1_lpddr4_trip1_cs_a =>    LPDDR_out(2).cs_a(0),
                ch1_lpddr4_trip1_cs_b =>    LPDDR_out(2).cs_b(0),
                ch1_lpddr4_trip1_dmi_a =>   LPDDR_inout(2).dmi_a,
                ch1_lpddr4_trip1_dmi_b =>   LPDDR_inout(2).dmi_b,
                ch1_lpddr4_trip1_dq_a =>    LPDDR_inout(2).dq_a,
                ch1_lpddr4_trip1_dq_b =>    LPDDR_inout(2).dq_b,
                ch1_lpddr4_trip1_dqs_c_a => LPDDR_inout(2).dqs_c_a,
                ch1_lpddr4_trip1_dqs_c_b => LPDDR_inout(2).dqs_c_b,
                ch1_lpddr4_trip1_dqs_t_a => LPDDR_inout(2).dqs_t_a,
                ch1_lpddr4_trip1_dqs_t_b => LPDDR_inout(2).dqs_t_b,
                ch1_lpddr4_trip1_reset_n => LPDDR_out(2).reset_n(0),
                ch1_lpddr4_trip2_ca_a =>    LPDDR_out(3).ca_a,
                ch1_lpddr4_trip2_ca_b =>    LPDDR_out(3).ca_b,
                ch1_lpddr4_trip2_ck_c_a =>  LPDDR_out(3).ck_c_a(0),
                ch1_lpddr4_trip2_ck_c_b =>  LPDDR_out(3).ck_c_b(0),
                ch1_lpddr4_trip2_ck_t_a =>  LPDDR_out(3).ck_t_a(0),
                ch1_lpddr4_trip2_ck_t_b =>  LPDDR_out(3).ck_t_b(0),
                ch1_lpddr4_trip2_cke_a =>   LPDDR_out(3).cke_a(0),
                ch1_lpddr4_trip2_cke_b =>   LPDDR_out(3).cke_b(0),
                ch1_lpddr4_trip2_cs_a =>    LPDDR_out(3).cs_a(0),
                ch1_lpddr4_trip2_cs_b =>    LPDDR_out(3).cs_b(0),
                ch1_lpddr4_trip2_dmi_a =>   LPDDR_inout(3).dmi_a,
                ch1_lpddr4_trip2_dmi_b =>   LPDDR_inout(3).dmi_b,
                ch1_lpddr4_trip2_dq_a =>    LPDDR_inout(3).dq_a,
                ch1_lpddr4_trip2_dq_b =>    LPDDR_inout(3).dq_b,
                ch1_lpddr4_trip2_dqs_c_a => LPDDR_inout(3).dqs_c_a,
                ch1_lpddr4_trip2_dqs_c_b => LPDDR_inout(3).dqs_c_b,
                ch1_lpddr4_trip2_dqs_t_a => LPDDR_inout(3).dqs_t_a,
                ch1_lpddr4_trip2_dqs_t_b => LPDDR_inout(3).dqs_t_b,
                ch1_lpddr4_trip2_reset_n => LPDDR_out(3).reset_n(0),
                gt_refclk0_0_clk_n                  => WupperToCPM(0).gtrefclk_n,--: in STD_LOGIC;
                gt_refclk0_0_clk_p                  => WupperToCPM(0).gtrefclk_p,--: in STD_LOGIC;
                gt_refclk1_0_clk_n                  => WupperToCPM(1).gtrefclk_n,--: in STD_LOGIC;
                gt_refclk1_0_clk_p                  => WupperToCPM(1).gtrefclk_p,--: in STD_LOGIC;
                lpddr4_clk1_clk_n => LPDDR_in(0).sys_clk_n(0),
                lpddr4_clk1_clk_p => LPDDR_in(0).sys_clk_p(0),
                lpddr4_clk2_clk_n => LPDDR_in(1).sys_clk_n(0),
                lpddr4_clk2_clk_p => LPDDR_in(1).sys_clk_p(0),
                pcie0_cfg_control_flr_done_0 => pcie0_cfg_control_flr_done(0),
                pcie0_cfg_control_flr_in_process_0 => pcie0_cfg_control_flr_in_process,
                pcie0_cfg_control_power_state_change_ack_0 => pcie0_cfg_control_power_state_change_ack,
                pcie0_cfg_control_power_state_change_interrupt_0 => pcie0_cfg_control_power_state_change_interrupt,
                pcie0_cfg_interrupt_0_intx_vector   => WupperToCPM(0).cfg_interrupt_0_intx_vector  ,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie0_cfg_interrupt_0_pending       => WupperToCPM(0).cfg_interrupt_0_pending      ,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie0_cfg_interrupt_0_sent          => CPMToWupper_s(0).cfg_interrupt_0_sent         ,--: out STD_LOGIC;
                pcie0_cfg_status_cq_np_req_0 => pcie0_cfg_status_cq_np_req,
                --pcie0_cfg_msi_0_fail               => open, --CPMToWupper_s(0).cfg_msix_0_fail              ,--: out STD_LOGIC;
                --pcie0_cfg_msi_0_function_number     => WupperToCPM(0).cfg_msix_0_function_number   ,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                --pcie0_cfg_msi_0_int_vector => (others => '0'),
                --pcie0_cfg_msi_0_select => "0000",
                --pcie0_cfg_msi_0_sent => open,
                --pcie0_cfg_msix_0_address            => WupperToCPM(0).cfg_msix_0_address           ,--: in STD_LOGIC_VECTOR ( 63 downto 0 );
                --pcie0_cfg_msix_0_data               => WupperToCPM(0).cfg_msix_0_data              ,--: in STD_LOGIC_VECTOR ( 31 downto 0 );
                --pcie0_cfg_msix_0_enable             => CPMToWupper_s(0).cfg_msix_0_enable(0)            ,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
                --pcie0_cfg_msix_0_int_vector         => WupperToCPM(0).cfg_msix_0_int_vector        ,--: in STD_LOGIC;
                --pcie0_cfg_msix_0_mask               => CPMToWupper_s(0).cfg_msix_0_mask(0)              ,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
                --pcie0_cfg_msix_0_mint_vector        => WupperToCPM(0).cfg_msix_0_mint_vector       ,--: in STD_LOGIC_VECTOR ( 31 downto 0 );
                --pcie0_cfg_msix_0_sent               => open, --CPMToWupper_s(0).cfg_msix_0_sent              ,--: out STD_LOGIC;
                --pcie0_cfg_msix_0_vec_pending        => WupperToCPM(0).cfg_msix_0_vec_pending       ,--: in STD_LOGIC_VECTOR ( 1 downto 0 );
                --pcie0_cfg_msix_0_vec_pending_status => open, --CPMToWupper_s(0).cfg_msix_0_vec_pending_status,--: out STD_LOGIC;
                pcie0_m_axis_cq_0_tdata             => CPMToWupper_s(0).m_axis_cq_0_tdata            ,--: out STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie0_m_axis_cq_0_tkeep             => CPMToWupper_s(0).m_axis_cq_0_tkeep            ,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie0_m_axis_cq_0_tlast             => CPMToWupper_s(0).m_axis_cq_0_tlast            ,--: out STD_LOGIC;
                pcie0_m_axis_cq_0_tready            => WupperToCPM(0).m_axis_cq_0_tready           ,--: in STD_LOGIC;
                pcie0_m_axis_cq_0_tuser             => CPMToWupper_s(0).m_axis_cq_0_tuser            ,--: out STD_LOGIC_VECTOR ( 182 downto 0 );
                pcie0_m_axis_cq_0_tvalid            => CPMToWupper_s(0).m_axis_cq_0_tvalid           ,--: out STD_LOGIC;
                pcie0_m_axis_rc_0_tdata             => CPMToWupper_s(0).m_axis_rc_0_tdata            ,--: out STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie0_m_axis_rc_0_tkeep             => CPMToWupper_s(0).m_axis_rc_0_tkeep            ,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie0_m_axis_rc_0_tlast             => CPMToWupper_s(0).m_axis_rc_0_tlast            ,--: out STD_LOGIC;
                pcie0_m_axis_rc_0_tready            => WupperToCPM(0).m_axis_rc_0_tready           ,--: in STD_LOGIC;
                pcie0_m_axis_rc_0_tuser             => CPMToWupper_s(0).m_axis_rc_0_tuser            ,--: out STD_LOGIC_VECTOR ( 160 downto 0 );
                pcie0_m_axis_rc_0_tvalid            => CPMToWupper_s(0).m_axis_rc_0_tvalid           ,--: out STD_LOGIC;
                pcie0_s_axis_cc_0_tdata             => WupperToCPM(0).s_axis_cc_0_tdata            ,--: in STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie0_s_axis_cc_0_tkeep             => WupperToCPM(0).s_axis_cc_0_tkeep            ,--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie0_s_axis_cc_0_tlast             => WupperToCPM(0).s_axis_cc_0_tlast            ,--: in STD_LOGIC;
                pcie0_s_axis_cc_0_tready            => CPMToWupper_s(0).s_axis_cc_0_tready           ,--: out STD_LOGIC;
                pcie0_s_axis_cc_0_tuser             => WupperToCPM(0).s_axis_cc_0_tuser            ,--: in STD_LOGIC_VECTOR ( 80 downto 0 );
                pcie0_s_axis_cc_0_tvalid            => WupperToCPM(0).s_axis_cc_0_tvalid           ,--: in STD_LOGIC;
                pcie0_s_axis_rq_0_tdata             => WupperToCPM(0).s_axis_rq_0_tdata            ,--: in STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie0_s_axis_rq_0_tkeep             => WupperToCPM(0).s_axis_rq_0_tkeep            ,--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie0_s_axis_rq_0_tlast             => WupperToCPM(0).s_axis_rq_0_tlast            ,--: in STD_LOGIC;
                pcie0_s_axis_rq_0_tready            => CPMToWupper_s(0).s_axis_rq_0_tready           ,--: out STD_LOGIC;
                pcie0_s_axis_rq_0_tuser             => WupperToCPM(0).s_axis_rq_0_tuser            ,--: in STD_LOGIC_VECTOR ( 178 downto 0 );
                pcie0_s_axis_rq_0_tvalid            => WupperToCPM(0).s_axis_rq_0_tvalid           ,--: in STD_LOGIC;
                pcie0_user_clk_0                    => CPMToWupper_s(0).user_clk_0                   ,--: out STD_LOGIC;
                pcie0_user_lnk_up_0                 => CPMToWupper_s(0).user_lnk_up_0                ,--: out STD_LOGIC;
                pcie0_user_reset_0                  => CPMToWupper_s(0).user_reset_0                 ,--: out STD_LOGIC;
                pcie1_cfg_control_flr_done_0 => pcie1_cfg_control_flr_done(0),
                pcie1_cfg_control_flr_in_process_0 => pcie1_cfg_control_flr_in_process,
                pcie1_cfg_control_power_state_change_ack_0 => pcie1_cfg_control_power_state_change_ack,
                pcie1_cfg_control_power_state_change_interrupt_0 => pcie1_cfg_control_power_state_change_interrupt,
                pcie1_cfg_interrupt_0_intx_vector   => WupperToCPM(1).cfg_interrupt_0_intx_vector  ,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie1_cfg_interrupt_0_pending       => WupperToCPM(1).cfg_interrupt_0_pending      ,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie1_cfg_interrupt_0_sent          => CPMToWupper_s(1).cfg_interrupt_0_sent         ,--: out STD_LOGIC;
                pcie1_cfg_status_cq_np_req_0 => pcie1_cfg_status_cq_np_req,
                --pcie1_cfg_msi_0_fail => open,
                --pcie1_cfg_msix_0_fail               => CPMToWupper_s(1).cfg_msix_0_fail              ,
                --pcie1_cfg_msi_0_function_number     => WupperToCPM(1).cfg_msix_0_function_number   ,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                --pcie1_cfg_msi_0_int_vector => (others => '0'),
                --pcie1_cfg_msi_0_select => "0000",
                --pcie1_cfg_msi_0_sent => open,
                --pcie1_cfg_msix_0_address            => WupperToCPM(1).cfg_msix_0_address           ,--: in STD_LOGIC_VECTOR ( 63 downto 0 );
                --pcie1_cfg_msix_0_data               => WupperToCPM(1).cfg_msix_0_data              ,--: in STD_LOGIC_VECTOR ( 31 downto 0 );
                --pcie1_cfg_msix_0_enable             => CPMToWupper_s(1).cfg_msix_0_enable(0)            ,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
                --pcie1_cfg_msix_0_int_vector         => WupperToCPM(1).cfg_msix_0_int_vector        ,--: in STD_LOGIC;
                --pcie1_cfg_msix_0_mask               => CPMToWupper_s(1).cfg_msix_0_mask(0)              ,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
                --pcie1_cfg_msix_0_mint_vector        => WupperToCPM(1).cfg_msix_0_mint_vector       ,--: in STD_LOGIC_VECTOR ( 31 downto 0 );
                --pcie1_cfg_msix_0_sent               => CPMToWupper_s(1).cfg_msix_0_sent             ,
                --pcie1_cfg_msix_0_vec_pending        => WupperToCPM(1).cfg_msix_0_vec_pending       ,--: in STD_LOGIC_VECTOR ( 1 downto 0 );
                --pcie1_cfg_msix_0_vec_pending_status => open,
                --pcie1_cfg_msix_0_vec_pending_status => CPMToWupper_s(1).cfg_msix_0_vec_pending_status,
                pcie1_m_axis_cq_0_tdata             => CPMToWupper_s(1).m_axis_cq_0_tdata            ,--: out STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie1_m_axis_cq_0_tkeep             => CPMToWupper_s(1).m_axis_cq_0_tkeep            ,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie1_m_axis_cq_0_tlast             => CPMToWupper_s(1).m_axis_cq_0_tlast            ,--: out STD_LOGIC;
                pcie1_m_axis_cq_0_tready            => WupperToCPM(1).m_axis_cq_0_tready           ,--: in STD_LOGIC;
                pcie1_m_axis_cq_0_tuser             => CPMToWupper_s(1).m_axis_cq_0_tuser            ,--: out STD_LOGIC_VECTOR ( 182 downto 0 );
                pcie1_m_axis_cq_0_tvalid            => CPMToWupper_s(1).m_axis_cq_0_tvalid           ,--: out STD_LOGIC;
                pcie1_m_axis_rc_0_tdata             => CPMToWupper_s(1).m_axis_rc_0_tdata            ,--: out STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie1_m_axis_rc_0_tkeep             => CPMToWupper_s(1).m_axis_rc_0_tkeep            ,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie1_m_axis_rc_0_tlast             => CPMToWupper_s(1).m_axis_rc_0_tlast            ,--: out STD_LOGIC;
                pcie1_m_axis_rc_0_tready            => WupperToCPM(1).m_axis_rc_0_tready           ,--: in STD_LOGIC;
                pcie1_m_axis_rc_0_tuser             => CPMToWupper_s(1).m_axis_rc_0_tuser            ,--: out STD_LOGIC_VECTOR ( 160 downto 0 );
                pcie1_m_axis_rc_0_tvalid            => CPMToWupper_s(1).m_axis_rc_0_tvalid           ,--: out STD_LOGIC;
                pcie1_s_axis_cc_0_tdata             => WupperToCPM(1).s_axis_cc_0_tdata            ,--: in STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie1_s_axis_cc_0_tkeep             => WupperToCPM(1).s_axis_cc_0_tkeep            ,--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie1_s_axis_cc_0_tlast             => WupperToCPM(1).s_axis_cc_0_tlast            ,--: in STD_LOGIC;
                pcie1_s_axis_cc_0_tready            => CPMToWupper_s(1).s_axis_cc_0_tready           ,--: out STD_LOGIC;
                pcie1_s_axis_cc_0_tuser             => WupperToCPM(1).s_axis_cc_0_tuser            ,--: in STD_LOGIC_VECTOR ( 80 downto 0 );
                pcie1_s_axis_cc_0_tvalid            => WupperToCPM(1).s_axis_cc_0_tvalid           ,--: in STD_LOGIC;
                pcie1_s_axis_rq_0_tdata             => WupperToCPM(1).s_axis_rq_0_tdata            ,--: in STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie1_s_axis_rq_0_tkeep             => WupperToCPM(1).s_axis_rq_0_tkeep            ,--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie1_s_axis_rq_0_tlast             => WupperToCPM(1).s_axis_rq_0_tlast            ,--: in STD_LOGIC;
                pcie1_s_axis_rq_0_tready            => CPMToWupper_s(1).s_axis_rq_0_tready           ,--: out STD_LOGIC;
                pcie1_s_axis_rq_0_tuser             => WupperToCPM(1).s_axis_rq_0_tuser            ,--: in STD_LOGIC_VECTOR ( 178 downto 0 );
                pcie1_s_axis_rq_0_tvalid            => WupperToCPM(1).s_axis_rq_0_tvalid           ,--: in STD_LOGIC;
                pcie1_user_clk_0                    => CPMToWupper_s(1).user_clk_0                   ,--: out STD_LOGIC;
                pcie1_user_lnk_up_0                 => CPMToWupper_s(1).user_lnk_up_0                ,--: out STD_LOGIC;
                pcie1_user_reset_0                  => CPMToWupper_s(1).user_reset_0                 , --: out STD_LOGIC
                pl0_ref_clk_0                       => pl0_ref_clk,
                pl0_resetn_0 => pl0_resetn(0)
            );
    end generate g_flx120;



end Behavioral;

--based on https://gitlab.cern.ch/YARR/YARR/-/blob/master/src/libRd53b/Rd53bTriggerLoop.cpp
--rluz at anl dot gov
--april 2024

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use IEEE.numeric_std_unsigned.ALL;
    use work.rd53_package.ALL;

entity RD53B_TriggerLoop is
    port (
        --rst                     : in  std_logic;
        clk40                   : in  std_logic;
        count_in                : in  std_logic_vector( 6 downto 0);

        noInject                : in  std_logic;

        edgeMode                : in  std_logic;
        edgeDelay               : in  std_logic_vector( 4 downto 0);
        edgeDuration            : in  std_logic_vector( 7 downto 0);

        trigDelay               : in  std_logic_vector( 7 downto 0);
        trigMultiplier          : in  std_logic_vector( 5 downto 0); --max is 63. can be changed. if so change trigstream function accordingly

        loop_out                : out std_logic_vector(15 downto 0)
    );

end RD53B_TriggerLoop;

architecture Behavioral of RD53B_TriggerLoop is
    function gencal(chipId          : in std_logic_vector(4 downto 0);
        mode            : in std_logic;
        edgeDelay       : in std_logic_vector(4 downto 0);
        edgeDuration    : in std_logic_vector(7 downto 0);
        auxPar          : in std_logic;
        auxDelay        : in std_logic_vector(4 downto 0))
        return std_logic_vector is --(47 downto 0)
        variable twentyB : std_logic_vector(19 downto 0);
    begin
        twentyB := mode & edgeDelay & edgeDuration & auxPar & auxDelay; --figure 32 rd53b atlas manual
        return CalB & enc5to8(chipId) & enc5to8(twentyB(19 downto 15)) & enc5to8(twentyB(14 downto 10)) & enc5to8(twentyB(9 downto 5)) & enc5to8(twentyB(4 downto 0)); --6 bytes
    end;

    function trigpos(trigDelay      : in  std_logic_vector(7 downto 0))
        return std_logic_vector is --(7 downto 0)
        variable val : std_logic_vector(7 downto 0);
        variable delay : std_logic_vector(4 downto 0);
    begin
        if trigDelay < "00011111" then --dividing by 8
            delay := "00011";
        else
            delay := trigDelay(7 downto 3);
        end if;
        val:= "000"&(delay + "00010"); --adding 2 to account for sync and cal
        return val + val; --multipled by 2 because in the cpp original each count is 32 b not 16 b
    end;

    function trigmul(trigMultiplier : in  std_logic_vector(5 downto 0))
        return std_logic_vector is --(7 downto 0)
        variable val : std_logic_vector(7 downto 0);
    begin
        val := "00000"&trigMultiplier(5 downto 3) + "00000001";--added one to use < in if statement below
        return val + val; --multipled by 2 because in the cpp original each count is 32 b not 16 b added
    end;

    function trigstream(trigDelay          : in  std_logic_vector(7 downto 0);
        trigMultiplier     : in  std_logic_vector(5 downto 0))
        return std_logic_vector is --(63 downto 0)
        variable in_to_val : std_logic_vector(63 downto 0) := (others => '0');
        variable val : std_logic_vector(63 downto 0) := (others => '0');
    begin
        in_to_val(to_integer(unsigned(trigMultiplier))) := '1';
        val := in_to_val - "1";
        if      trigDelay(1 downto 0) = "00" then return val;
        elsif   trigDelay(1 downto 0) = "01" then return val(62 downto 0) & "0";
        elsif   trigDelay(1 downto 0) = "10" then return val(61 downto 0) & "00";
        elsif   trigDelay(1 downto 0) = "11" then return val(60 downto 0) & "000";
        else    return val;
        end if;
    end;

    function trigout(stream     : in  std_logic_vector(3 downto 0);
        count      : in  std_logic_vector(5 downto 0))
        return std_logic_vector is --(15 downto 0)
        variable in_to_val : std_logic_vector(63 downto 0) := (others => '0');
        variable val : std_logic_vector(63 downto 0) := (others => '0');
    begin
        if stream = "0000" then
            return encTrigger(stream) & x"aa";
        else
            return encTrigger(stream) & encTTag(count);
        end if;
    end;

    function sync_position(tr_pos     : in  std_logic_vector(7 downto 0);
        tr_mult           : in  std_logic_vector(7 downto 0);
        sync_default      : in  std_logic_vector(7 downto 0))
        return std_logic_vector is --(7 downto 0)
    begin
        if sync_default > tr_pos - x"02" and sync_default < tr_pos + tr_mult + x"01" then
            return tr_pos - x"02";
        elsif sync_default + tr_mult + x"03" > tr_pos - x"01" and sync_default + tr_mult + x"03" < tr_pos + tr_mult + x"01" then
            return tr_pos - tr_mult - x"05";
        else
            return sync_default;
        end if;
    end;

    constant id16       :   std_logic_vector(4 downto 0) := "10000";

    signal count    : std_logic_vector( 7 downto 0);
    signal calword  : std_logic_vector(47 downto 0) := (others => '0');
    signal armword  : std_logic_vector(47 downto 0) := (others => '0');
    signal calnoij  : std_logic_vector(47 downto 0) := x"aaaaaaaaaaaa";
    signal tr_pos   : std_logic_vector( 7 downto 0) := (others => '0');
    signal tr_mul   : std_logic_vector( 7 downto 0) := (others => '0');
    signal trigger  : std_logic_vector(63 downto 0) := (others => '0');
    signal tstream  : std_logic_vector(63 downto 0) := (others => '0');
    signal ctofour  : std_logic_vector( 1 downto 0) := (others => '0');
    signal ttagcnt  : std_logic_vector( 5 downto 0) := (others => '0');
    signal sync_pos : std_logic_vector( 7 downto 0) := (others => '0');
begin

    count <= "0" & count_in;

    process(clk40)
    begin
        if rising_edge(clk40) then
            --if (rst = '1') then
            --    calword <= (others => '0');
            --    armword <= (others => '0');
            --    tr_pos  <= (others => '0');
            --    tr_mul  <= (others => '0');
            --    trigger <= (others => '0');
            --    tstream <= (others => '0');
            --    ctofour <= (others => '0');
            --    ttagcnt <= (others => '0');
            --else
            if count = x"00" then
                loop_out <= Sync;
                if noInject then
                    calword <= calnoij;
                    armword <= calnoij;
                else
                    if edgeMode then
                        calword <= gencal(id16,'1',"00000",edgeDuration,'0',"00000"); --calWords = genCal(16, 1, 0, m_edgeDuration, 0, 0);
                        armword <= calnoij;
                    else
                        calword <= gencal(id16,'0',edgeDelay,"00000001",'0',"00000"); --calWords = genCal(16, 0, m_calEdgeDelay, 1, 0, 0);
                        armword <= gencal(id16,'1',"00000","00000000",'0',"00000"); --armWords = genCal(16, 1, 0, 0, 0, 0);;
                    end if;
                end if;
                tr_pos <= trigpos(trigDelay) - "00000001"; -- minus 1 since one of the initial syncs was removed
                tr_mul <= trigmul(trigMultiplier); --max is 16 16b words
                tstream <= trigstream(trigDelay,trigMultiplier);
                ctofour <= "00";
            elsif count = x"01" then
                --loop_out <= Sync;
                loop_out <= PLLLockB;
                sync_pos <= sync_position(tr_pos,tr_mul,x"20");
            elsif count = sync_pos then
                loop_out <= Sync;
            elsif count = sync_pos + tr_mul + x"03" then
                loop_out <= Sync;
            elsif count = x"02" then
                loop_out <= calword(47 downto 32);
            elsif count = x"03" then
                loop_out <= calword(31 downto 16);
            elsif count = x"04" then
                loop_out <= calword(15 downto  0);
            elsif count >= tr_pos and count < tr_pos + tr_mul and trigMultiplier /= "000000" then --sending trigger
                --from Rd53bTriggerLoop.cpp:// Special case: if trigger multiplier = 0, no trigger should be sent in command line
                loop_out <= trigout(tstream(3 downto 0),ttagcnt);
                if ctofour = "11" then
                    tstream <= "0000" & tstream(63 downto 4);
                    if ttagcnt = "110101" then
                        ttagcnt <= "000000";
                    else
                        ttagcnt <= ttagcnt + "000001";
                    end if;
                    ctofour <= "00";
                else
                    ctofour <= ctofour + "01";
                end if;
            elsif count = x"3d" then --m_trigWord[1] = 0xAAAA0000 | armWords[0]; m_trigWord[0] = ((uint32_t)armWords[1]<<16) | armWords[2];
                loop_out <= armword(47 downto 32);
            elsif count = x"3e" then
                loop_out <= armword(31 downto 16);
            elsif count = x"3f" then
                loop_out <= armword(15 downto  0);
            else
                loop_out <= PLLLockB;
            end if;
        end if;
    --end if;
    end process;

end Behavioral;

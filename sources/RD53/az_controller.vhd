--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer: Marco Trovato
--
-- Create Date: 01/18/2019
-- Design Name:
-- Module Name: az_controller - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use IEEE.numeric_std_unsigned.ALL;
    use work.rd53_package.ALL;

entity az_controller is
    Generic (
        constant reg_depth    : positive := 2;
        constant freq : positive := 500  --500x100ns = 50usec

    );

    Port (
        rst              : in STD_LOGIC;
        enAZ_viareg      : in STD_LOGIC;
        enAZ_viacmd      : in STD_LOGIC;
        clk40            : in STD_LOGIC;
        az_sent          : in STD_LOGIC;
        az_cmd_type      : in STD_LOGIC;
        counter          : in STD_LOGIC_VECTOR(1 downto 0);

        az_out           : out STD_LOGIC_VECTOR(15 downto 0);
        az_rdy_out       : out STD_LOGIC           ;

        --debug
        az_count_ila_out : out std_logic_vector(9 downto 0);
        az_rdy_ila_out   : out std_logic;
        pointer_ila_out  : out std_logic;
        state_ila_out    : out std_logic;
        az_out_cnts_ila_out : out std_logic
    );
end az_controller;

architecture Behavioral of az_controller is
    type state_type is (preparing,
        send_command
    );

    type reg_type is array (0 to reg_depth - 1) of STD_LOGIC_VECTOR(15 downto 0);
    signal az_reg : reg_type := (others =>(others => '0'));
    signal az_outof_fifo32 : STD_LOGIC_VECTOR(31 downto 0);
    signal pointer : STD_LOGIC_VECTOR(0 downto 0) := (others => '0');
    signal state: state_type := preparing;
    signal az_out_cnts : STD_LOGIC_VECTOR(0 downto 0) := (others => '0');
    signal az_rdy: STD_LOGIC;
    signal az_count : natural range 0 to 1000;
begin

    az_outof_fifo32 <= AZ_PULSEA;

    process(clk40)
    begin
        if (rising_edge(clk40)) then
            if (rst = '1' or (enAZ_viareg = '0' and enAZ_viacmd = '0')) then
                az_rdy <= '0';
                az_count <= 0;
            elsif (counter = "01") then
                if (az_sent = '1' or az_cmd_type = '1') then
                    az_rdy <= '0';
                    az_count <= 0;
                elsif (az_count = freq) then
                    az_rdy <= '1';
                    az_count <= az_count;
                else
                    az_rdy <= '0';
                    az_count <= az_count + 1;
                end if;
            else
                az_rdy <= az_rdy;
                az_count <= az_count;
            end if;
        end if;
    end process;

    process(clk40)
    begin

        if rising_edge(clk40) then
            if (rst = '1') then
                az_reg     <= (others =>(others => '0'));
                pointer    <= (others => '0');
                state      <= preparing;
                az_out     <= (others => '0');
                az_rdy_out <= '0';
                az_out_cnts <= (others => '0');
            else
                case state is
                    when preparing =>
                        az_out_cnts <= "0";
                        if(az_rdy = '1') then

                            if(pointer = "0") then
                                state <= preparing;
                                az_reg(0) <= az_outof_fifo32(31 downto 16);
                                pointer <= "1";
                                az_rdy_out <= '0';
                                az_out <= GlobalPulseA;
                            elsif (pointer = "1") then
                                state <= send_command;
                                az_reg(1) <= az_outof_fifo32(15 downto 0);
                                pointer <= "1";
                                az_rdy_out <= '1';
                                az_out <= az_reg(0);
                            end if;
                        end if;
                    when send_command =>
                        if(counter = "01") then
                            if (az_sent = '1') then
                                if (pointer = "0") then
                                    state <= preparing;
                                    az_rdy_out <= '0';
                                else
                                    case az_out_cnts is
                                        when "0" =>
                                            az_out <= az_reg(1);
                                            az_out_cnts <= "1";
                                        when others =>
                                            az_out_cnts <= "0";
                                    end case;
                                    if (pointer = az_out_cnts) then
                                        state <= preparing;
                                        az_rdy_out <= '0';
                                        pointer  <= "0";
                                    else
                                        state <= send_command;
                                        az_rdy_out <= '1';
                                    end if;
                                end if;
                            end if;
                        end if;
                end case;

            end if;
        end if;
    end process;

    az_count_ila_out <= std_logic_vector(to_unsigned(az_count,az_count_ila_out'length));
    az_rdy_ila_out <= az_rdy;
    pointer_ila_out <= pointer(0);
    state_ila_out <= '0' when state = preparing else '1';
    az_out_cnts_ila_out <= az_out_cnts(0);


end Behavioral;

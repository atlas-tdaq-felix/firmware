--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer: Alexander Paramonov, Marco Trovato, Tianxing Zheng
--
-- Create Date: 03/31/2018 11:54:05 AM
-- Design Name:
-- Module Name: sync_timer - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sync_timer is
    Generic (
        constant freq : positive := 31
    );
    port (
        clk: in STD_LOGIC;
        sync_sent: in STD_LOGIC;
        counter: in STD_LOGIC_VECTOR(1 downto 0);
        sync_cmd_type: in STD_LOGIC;
        sync_time: out STD_LOGIC;
        --outputs for simulation
        sync_counter : out Integer
    );
end sync_timer;

architecture Behavioral of sync_timer is

    signal sync_rdy: STD_LOGIC;
    signal sync_count : natural range 0 to 64;
begin
    sync_counter <= sync_count;
    process(clk)
    begin
        if (rising_edge(clk)) then
            if counter = "01" then
                if (sync_sent = '1' or sync_cmd_type = '1') then
                    sync_rdy <= '0';
                    sync_count <= 0;
                elsif (sync_count = freq) then
                    sync_rdy <= '1';
                else
                    sync_rdy <= '0';
                    sync_count <= sync_count + 1;
                end if;
            end if;
        end if;

    end process;

    sync_time <= sync_rdy;

end Behavioral;

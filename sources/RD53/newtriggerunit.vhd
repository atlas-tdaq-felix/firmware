--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer: Alexander Paramonov, Marco Trovato, Tianxing Zheng
--
-- Create Date: 02/28/2018 06:32:11 PM
-- Design Name:
-- Module Name: newtriggerunit - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.numeric_std.ALL;
    use IEEE.numeric_std_unsigned.ALL;
    use work.rd53_package.ALL;

entity newtriggerunit is
    Generic (
        RD53Version : String := "A" --A or B
    );
    Port (
        --clk40 : in  STD_LOGIC;
        --rst : in  STD_LOGIC;
        trigger : in  STD_LOGIC_VECTOR(3 downto 0);
        tag : in  STD_LOGIC_VECTOR(6 downto 0);
        --counter : in STD_LOGIC_VECTOR(1 downto 0);
        --trigger_rdy   : out  STD_LOGIC;
        enc_trig : out STD_LOGIC_VECTOR(15 downto 0)
    );
end newtriggerunit;

architecture Behavioral of newtriggerunit is

    --signal trig_sr40 : STD_LOGIC_VECTOR(3 downto 0);
    signal DATA_RD53A32 : array64_std_logic_vector_7;
--signal count_trig: STD_LOGIC_VECTOR(4 downto 0) := (others => '0');


begin
    DATA_RD53A32(0)  <= DATA_00;
    DATA_RD53A32(1)  <= DATA_01;
    DATA_RD53A32(2)  <= DATA_02;
    DATA_RD53A32(3)  <= DATA_03;
    DATA_RD53A32(4)  <= DATA_04;
    DATA_RD53A32(5)  <= DATA_05;
    DATA_RD53A32(6)  <= DATA_06;
    DATA_RD53A32(7)  <= DATA_07;
    DATA_RD53A32(8)  <= DATA_08;
    DATA_RD53A32(9)  <= DATA_09;
    DATA_RD53A32(10) <= DATA_10;
    DATA_RD53A32(11) <= DATA_11;
    DATA_RD53A32(12) <= DATA_12;
    DATA_RD53A32(13) <= DATA_13;
    DATA_RD53A32(14) <= DATA_14;
    DATA_RD53A32(15) <= DATA_15;
    DATA_RD53A32(16) <= DATA_16;
    DATA_RD53A32(17) <= DATA_17;
    g_Data18_RD53A: if RD53Version = "A" generate
        DATA_RD53A32(18) <= DATA_18A;
    end generate;
    g_Data18_RD53B: if RD53Version = "B" generate
        DATA_RD53A32(18) <= DATA_18B;
    end generate;
    DATA_RD53A32(19) <= DATA_19;
    DATA_RD53A32(20) <= DATA_20;
    DATA_RD53A32(21) <= DATA_21;
    DATA_RD53A32(22) <= DATA_22;
    DATA_RD53A32(23) <= DATA_23;
    DATA_RD53A32(24) <= DATA_24;
    DATA_RD53A32(25) <= DATA_25;
    DATA_RD53A32(26) <= DATA_26;
    DATA_RD53A32(27) <= DATA_27;
    DATA_RD53A32(28) <= DATA_28;
    DATA_RD53A32(29) <= DATA_29;
    DATA_RD53A32(30) <= DATA_30;
    DATA_RD53A32(31) <= DATA_31;

    DATA_RD53A32(32) <= x"63";
    DATA_RD53A32(33) <= x"5a";
    DATA_RD53A32(34) <= x"5c";
    DATA_RD53A32(35) <= x"aa";
    DATA_RD53A32(36) <= x"65";
    DATA_RD53A32(37) <= x"69";
    DATA_RD53A32(38) <= Trigger_01;
    DATA_RD53A32(39) <= Trigger_02;
    DATA_RD53A32(40) <= Trigger_03;
    DATA_RD53A32(41) <= Trigger_04;
    DATA_RD53A32(42) <= Trigger_05;
    DATA_RD53A32(43) <= Trigger_06;
    DATA_RD53A32(44) <= Trigger_07;
    DATA_RD53A32(45) <= Trigger_08;
    DATA_RD53A32(46) <= Trigger_09;
    DATA_RD53A32(47) <= Trigger_10;
    DATA_RD53A32(48) <= Trigger_11;
    DATA_RD53A32(49) <= Trigger_12;
    DATA_RD53A32(50) <= Trigger_13;
    DATA_RD53A32(51) <= Trigger_14;
    DATA_RD53A32(52) <= Trigger_15;
    DATA_RD53A32(53) <= x"66";


    process (trigger, tag, DATA_RD53A32) --(clk40)
        variable encoded_trig: STD_LOGIC_VECTOR(7 downto 0);
    --variable current_trig: STD_LOGIC_VECTOR(3 downto 0);
    begin
        --        if rising_edge(clk40) then
        --            current_trig := trig_sr40(2 downto 0) & trigger;
        --            if(rst = '1') then
        --                trig_sr40 <= "0000";
        --                trigger_rdy <= '0';
        --                count_trig <= (others => '0');
        --            else
        --                trig_sr40(3 downto 1) <= trig_sr40(2 downto 0);
        --                trig_sr40(0) <= trigger;
        --            end if;



        --            if (counter = "11" AND current_trig /= "0000") then
        --                trigger_rdy <= '1';
        --                count_trig <= count_trig + '1';

        case trigger is --current_trig is
            --when "0000" => encoded_trig := X"00"; --0000
            when "0001" => encoded_trig := Trigger_01; --000T
            when "0010" => encoded_trig := Trigger_02; --00T0
            when "0011" => encoded_trig := Trigger_03; --00TT
            when "0100" => encoded_trig := Trigger_04; --0T00
            when "0101" => encoded_trig := Trigger_05; --0T0T
            when "0110" => encoded_trig := Trigger_06; --0TT0
            when "0111" => encoded_trig := Trigger_07; --0TTT
            when "1000" => encoded_trig := Trigger_08; --T000
            when "1001" => encoded_trig := Trigger_09; --T00T
            when "1010" => encoded_trig := Trigger_10; --T0T0
            when "1011" => encoded_trig := Trigger_11; --T0TT
            when "1100" => encoded_trig := Trigger_12; --TT00
            when "1101" => encoded_trig := Trigger_13; --TT0T
            when "1110" => encoded_trig := Trigger_14; --TTT0
            when "1111" => encoded_trig := Trigger_15; --TTTT
            when others => encoded_trig := X"00";
        end case;

        enc_trig(15 downto 8) <= encoded_trig;
        enc_trig(7 downto 0) <= DATA_RD53A32(to_integer(unsigned(tag))); --DATA_RD53A32(to_integer(unsigned(count_trig)));
    --            else
    --                trigger_rdy <= '0';
    --            end if;

    --        end if;
    end process;

end Behavioral;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer: Alexander Paramonov, Marco Trovato, Tianxing Zheng
--
-- Create Date: 03/27/2018 09:36:28 PM
-- Design Name:
-- Module Name:
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description: Module(s) assume that continuous data is received @40 MHz clk.
-- If bursts of data are interrupted (e.g: command_rdy ____--_----____)
-- downlink commands may be spoiled
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 1) adapted EPROC_OUT4_ENCRD53A to axistream. Now operates on a single clk (clk40)
--N.B: input 8b rather than 10b
-- 2) removed commented lines useful for simulation or for deprecated module
-- (e.g: autozeroing)

-- on clk40 ony
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.std_logic_1164.ALL;
    use IEEE.numeric_std_unsigned.ALL;

library UNISIM;
    use UNISIM.VCOMPONENTS.all;
    use work.newtriggerunit;
    use work.sync_timer;
    use work.cmd_top_v1;
    use work.cmd_top_v2;
    use work.az_controller;
    use work.rd53_package.all;
    use work.FELIX_package.all;
library XPM;
    use XPM.VCOMPONENTS.all;


entity EncRD53 is
    generic (
        DEBUGGING_RD53                  : boolean := false; --save resources
        RD53Version                     : String := "A" --A or B
    );
    port (
        clk40       : in std_logic;
        rst         : in std_logic;
        --trigger     : in std_logic;
        TTCin       : in TTC_data_type;
        command_in  : in std_logic_vector(7 downto 0);
        command_rdy : in std_logic;
        dataout     : out std_logic_vector(3 downto 0);
        readyout    : out std_logic;

        enAZ_in     : in std_logic; --enable AutoZeroing module for SyncFE

        --calseq from ram
        CalTrigSeq_in       : in std_logic_vector(15 downto 0);
        ReadAddrCalTrigSeq_out : out std_logic_vector(4 downto 0);
        --
        RD53B_loopgen_reg   : in RD53_loopgen_type;

        --debug info/from to regmap
        ref_cmd_in            : in std_logic_vector(15 downto 0);
        cnt_cmd_out            : out std_logic_vector(31 downto 0);
        cnt_trig_cmd_out       : out std_logic_vector(31 downto 0);
        ref_dly_genCalTrig_in  : in std_logic_vector(7 downto 0);
        err_genCalTrig_dly_out : out std_logic_vector(7 downto 0);
        cnt_time_firstTolastTrig_out : out std_logic_vector(31 downto 0)


    );
end EncRD53;

--TTCin.ITk_sync (1b)
--TTCin.ITk_trig (4b)
--TTCin.ITk_tag (7b); 6 lowest bits are used for ITkPix 5b are used for RD53A



architecture Behavioral of EncRD53 is
    signal sync_rdy                     : std_logic;
    signal rst_rdy                      : std_logic;
    signal cmd_rdy                      : std_logic;
    signal trig_data                    : std_logic_vector(15 downto 0);
    signal cmd_data                     : std_logic_vector(15 downto 0);
    signal sync_sent_i                  : std_logic;--=1 when we sent a sync
    signal cmd_sent_i                   : std_logic;--=1 when we sent a cmd
    signal sync_cmd_type                : std_logic;
    signal ser_data_i                   : std_logic_vector(15 downto 0);
    signal ser_data_v_i                 : std_logic;
    signal dataout_i                    : std_logic_vector(3 downto 0);
    signal counter                      :  std_logic_vector (1 downto 0):="00";
    signal az_sent_i                    : STD_LOGIC;
    signal az_cmd_type                  : STD_LOGIC := '0';
    signal enAZ_viacmd                  : std_logic;
    signal az                           : STD_LOGIC_VECTOR(15 downto 0) := (others => '0');  --32b AZ
    signal az_rdy                       : STD_LOGIC := '0';
    signal ReadEnable40                 : std_logic;
    signal command_in40                 : std_logic_vector(15 downto 0);
    signal command_rdy40                : std_logic;
    signal rst_fifo                     : std_logic := '0';
    signal wr_en_fifo                   : std_logic;
    signal full_fifo                    : std_logic;
    signal almost_full_fifo             : std_logic; -- @suppress "signal almost_full_fifo is never read"
    signal empty_fifo                   : std_logic; -- @suppress "signal empty_fifo is never read"
    signal count40                      : std_logic := '0';
    signal command_rdy20                : std_logic;
    signal command_in20                 : std_logic_vector(15 downto 0);
    signal command_rdy_dly              : std_logic;
    signal startread                    : std_logic_vector(4 downto 0);
    signal cnt_cmd                      : std_logic_vector(31 downto 0);
    signal cnt_trig_cmd                 : std_logic_vector(31 downto 0);
    signal cnt_time_firstTolastTrig     : std_logic_vector(31 downto 0);
    signal err_genCalTrig_dly           : std_logic_vector(7 downto 0);
    signal cnt_time_twoTrig_ila         : std_logic_vector(31 downto 0); -- @suppress "signal cnt_time_twoTrig_ila is never read" -- @suppress "signal cnt_genCalTrig_dly_ila is never read"
    signal cnt_genCalTrig_dly_ila       : std_logic_vector(7 downto 0); -- @suppress "signal cnt_genCalTrig_dly_ila is never read"
    signal cnt_trig_ila                 : std_logic_vector(31 downto 0); -- @suppress "signal cnt_trig_ila is never read"
    signal state_chkdly_genCalTrig_ila  : std_logic_vector(1 downto 0);   -- @suppress "signal state_chkdly_genCalTrig_ila is never read"
    signal state_trigTime_ila           : std_logic_vector(1 downto 0); -- @suppress "signal state_trigTime_ila is never read"
    signal cnt_genCal_fromSM_ila        : std_logic_vector(31 downto 0); -- @suppress "signal cnt_genCal_fromSM_ila is never read"
    signal cnt_datav_fromCal_ila        : std_logic_vector(7 downto 0); -- @suppress "signal cnt_datav_fromCal_ila is never read"
    signal err_cmdtop_i                 : std_logic_vector(7 downto 0); -- @suppress "signal err_cmdtop_i is never read"
    signal warn_cmdtop_i                : std_logic_vector(7 downto 0); -- @suppress "signal warn_cmdtop_i is never read"
    signal az_count_ila_out             : std_logic_vector(9 downto 0); -- @suppress "signal az_count_ila_out is never read"
    signal az_rdy_ila_out               : std_logic; -- @suppress "signal az_rdy_ila_out is never read"
    signal az_pointer_ila_out           : std_logic; -- @suppress "signal az_pointer_ila_out is never read"
    signal az_state_ila_out             : std_logic; -- @suppress "signal az_state_ila_out is never read" -- @suppress "signal az_out_cnts_ila_out is never read"
    signal az_out_cnts_ila_out          : std_logic; -- @suppress "signal az_out_cnts_ila_out is never read"

    signal  frequency_i                 : STD_LOGIC_VECTOR(5 downto 0);  -- @suppress "signal frequency_i is never read"
    signal  frequency_cnt_i             : STD_LOGIC_VECTOR(11 downto 0); -- @suppress "signal frequency_cnt_i is never read"
    signal  freq_en_i                   : STD_LOGIC; -- @suppress "signal freq_en_i is never read"
    signal  cmd_out_cnts_i              : STD_LOGIC_VECTOR(4 downto 0); -- @suppress "signal cmd_out_cnts_i is never read" -- @suppress "signal cmd_out_cnts_i is never read" -- @suppress "signal pointer_i is never read"
    signal  pointer_i                   : STD_LOGIC_VECTOR(4 downto 0); -- @suppress "signal pointer_i is never read"
    signal  iteration_i                 : STD_LOGIC_VECTOR(6 downto 0); -- @suppress "signal iteration_i is never read"
    signal  cmd_state_is_i              : STD_LOGIC_VECTOR(1 downto 0); -- @suppress "signal cmd_state_is_i is never read"
    signal  cmd_reg0_i                  : STD_LOGIC_VECTOR(15 downto 0);  -- @suppress "signal cmd_reg0_i is never read"
    signal  cmd_reg1_i                  : STD_LOGIC_VECTOR(15 downto 0);  -- @suppress "signal cmd_reg1_i is never read"
    signal  cmd_reg2_i                  : STD_LOGIC_VECTOR(15 downto 0);  -- @suppress "signal cmd_reg2_i is never read"
    signal  cmd_reg3_i                  : STD_LOGIC_VECTOR(15 downto 0);  -- @suppress "signal cmd_reg3_i is never read"
    signal  ReadAddrCalTrigSeq_tmp      : std_logic_vector(4 downto 0);

begin


    readyout <= '1';     --TO DO: remove the FIFO and properly drive readyout

    process(clk40)
    begin
        if rising_edge(clk40) then
            if(command_rdy = '1') then
                if(count40 = '0') then
                    command_in20(15 downto 8) <= command_in(7 downto 0);
                    command_in20(7 downto 0) <= (others => '0');
                    command_rdy20 <= '0';
                    count40 <= '1';
                else
                    command_in20(7 downto 0) <= command_in(7 downto 0);
                    command_rdy20 <= '1';
                    count40 <= '0';
                end if;
            else
                command_rdy20 <= '0';
            end if;
        end if;  --clock
    end process;

    rst_fifo <= rst; --FS: rst_fifo was not written, assigning rst to it.


    fifo_i : xpm_fifo_sync
        generic map (
            FIFO_MEMORY_TYPE => "auto", -- String
            FIFO_WRITE_DEPTH => 1024,   -- DECIMAL
            CASCADE_HEIGHT => 0,        -- DECIMAL
            WRITE_DATA_WIDTH => 16,     -- DECIMAL
            READ_MODE => "fwft",         -- String
            FIFO_READ_LATENCY => 1,     -- DECIMAL
            FULL_RESET_VALUE => 1,      -- DECIMAL
            USE_ADV_FEATURES => "1008", -- almost_full and data_valid
            READ_DATA_WIDTH => 16,      -- DECIMAL
            WR_DATA_COUNT_WIDTH => 1,   -- DECIMAL
            PROG_FULL_THRESH => 10,     -- DECIMAL
            RD_DATA_COUNT_WIDTH => 1,   -- DECIMAL
            PROG_EMPTY_THRESH => 10,    -- DECIMAL
            DOUT_RESET_VALUE => "0",    -- String
            ECC_MODE => "no_ecc",       -- String
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            WAKEUP_TIME => 0            -- DECIMAL
        )
        port map (
            sleep => '0',
            rst => rst_fifo,
            wr_clk => clk40,
            wr_en => wr_en_fifo,
            din => command_in20,
            full => full_fifo,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => open,
            almost_full => almost_full_fifo,
            wr_ack => open,
            rd_en => ReadEnable40,
            dout => command_in40,
            empty => empty_fifo,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => open,
            almost_empty => open,
            data_valid => command_rdy40,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );

    wr_en_fifo <= command_rdy20 and not full_fifo;

    trig_gen: entity newtriggerunit generic map(
            RD53Version => RD53Version
        )
        port map (
            --clk40        =>   clk40,
            --rst          =>   rst,
            trigger      =>   TTCin.ITk_trig ,--trigger,
            tag         => TTCin.ITk_tag(6 downto 0),
            --counter      =>   counter,
            --trigger_rdy  =>   trig_rdy,
            enc_trig     =>   trig_data
        );

    az_gen: entity az_controller
        generic map(
            reg_depth => 2,
            freq => 500
        )
        port map (
            rst           =>   rst,
            enAZ_viareg   =>   enAZ_in,
            enAZ_viacmd   =>   enAZ_viacmd,
            clk40         =>   clk40,
            az_sent       =>   az_sent_i,
            az_cmd_type   =>   az_cmd_type,
            counter       =>   counter,

            az_out        =>   az,
            az_rdy_out    =>   az_rdy     ,
            --debug
            az_count_ila_out => az_count_ila_out,
            az_rdy_ila_out   => az_rdy_ila_out,
            pointer_ila_out  => az_pointer_ila_out,
            state_ila_out    => az_state_ila_out,
            az_out_cnts_ila_out => az_out_cnts_ila_out
        );


    sync_gen: entity sync_timer     generic map(
            freq => 31
        )
        port map (
            clk => clk40,
            sync_sent => sync_sent_i,
            counter => counter,
            sync_cmd_type => sync_cmd_type,
            sync_time => sync_rdy,
            sync_counter => open    );

    process(clk40)
    begin
        if rising_edge(clk40) then
            command_rdy_dly <= command_rdy;
            startread(4)    <= startread(3);
            startread(3)    <= startread(2);
            startread(2)    <= startread(1);
            startread(1)    <= startread(0);

        end if;  --clock
    end process;

    startread(0) <= not command_rdy and (command_rdy_dly);
    ReadAddrCalTrigSeq_out <= ReadAddrCalTrigSeq_tmp;

    g_cmd_gen_RD53A: if RD53Version = "A" generate
        cmd_gen: entity cmd_top_v1     generic map(
                reg_depth => 4,
                RD53Version => RD53Version
            )
            port map (
                rst           =>   rst,
                clk40         =>   clk40,
                startread     =>   startread(4),
                ReadEnable    =>   ReadEnable40,
                cmd_sent      =>   cmd_sent_i,
                counter       =>   counter,
                cmd_in        =>   command_in40,
                command_rdy   =>   command_rdy40,

                CalTrigSeq_in =>   CalTrigSeq_in,
                ReadAddrCalTrigSeq_out => ReadAddrCalTrigSeq_tmp,

                cmd_data      =>   cmd_data,
                rst_ready     =>   rst_rdy,
                cmd_ready     =>   cmd_rdy,
                sync_cmd_type =>   sync_cmd_type,
                az_cmd_type   =>   az_cmd_type,
                enAZ_viacmd   =>   enAZ_viacmd,
                --debug
                frequency_out     => frequency_i(4 downto 0),
                frequency_cnt_out => frequency_cnt_i,
                freq_en_out       => freq_en_i,
                cmd_out_cnts_out  => cmd_out_cnts_i,
                pointer_out       => pointer_i,
                iteration_out     => iteration_i,
                cmd_state_is_out  => cmd_state_is_i,
                cmd_reg0_out      => cmd_reg0_i,
                cmd_reg1_out      => cmd_reg1_i,
                cmd_reg2_out      => cmd_reg2_i,
                cmd_reg3_out      => cmd_reg3_i,
                --debug
                err_cmdtop_out    => err_cmdtop_i,
                warn_cmdtop_out   => warn_cmdtop_i
            );
    end generate;

    g_cmd_gen_RD53B: if RD53Version = "B" generate
        ReadAddrCalTrigSeq_tmp <= (others => '0');
        cmd_gen: entity cmd_top_v2     generic map(
                --reg_depth => 4,
                RD53Version => RD53Version
            )
            port map (
                rst                 => rst,
                clk40               => clk40,
                startread           => startread(4),
                ReadEnable          => ReadEnable40,
                cmd_sent            => cmd_sent_i,
                counter             => counter,
                cmd_in              => command_in40,
                command_rdy         => command_rdy40,
                RD53B_loopgen_reg   => RD53B_loopgen_reg,
                --CalTrigSeq_in =>   CalTrigSeq_in,
                --ReadAddrCalTrigSeq_out => ReadAddrCalTrigSeq_tmp,

                cmd_data            => cmd_data,
                rst_ready           => rst_rdy,
                cmd_ready           => cmd_rdy,
                sync_cmd_type       => sync_cmd_type,
                az_cmd_type         => az_cmd_type,
                enAZ_viacmd         => enAZ_viacmd,
                --debug
                frequency_out       => frequency_i(4 downto 0),
                frequency_cnt_out   => frequency_cnt_i,
                freq_en_out         => freq_en_i,
                cmd_out_cnts_out    => cmd_out_cnts_i,
                pointer_out         => pointer_i,
                iteration_out       => iteration_i,
                cmd_state_is_out    => cmd_state_is_i,
                cmd_reg0_out        => cmd_reg0_i,
                cmd_reg1_out        => cmd_reg1_i,
                cmd_reg2_out        => cmd_reg2_i,
                cmd_reg3_out        => cmd_reg3_i,
                --debug
                err_cmdtop_out      => err_cmdtop_i, --counts data that was sent to the chip that is not valid, for example: a data byte not part of table 32 of the manual
                warn_cmdtop_out     => warn_cmdtop_i --counts encoding headers rejected by the encoder and not sent to the chip
            );
    end generate;

    process(clk40)
    begin
        if rising_edge (clk40) then
            if(TTCin.ITk_sync = '1') then
                counter <= "01";
            else
                counter <= counter + '1';
            end if;
        end if;
    end process;

    dataout <= dataout_i;
    process(clk40)--Main State Machine. Priority: trig, sync, cmd
    --Priority (decreasing):
    -- 4) trig,
    -- 3) sync,
    -- 2) (az (autozero)),
    -- 1) cmd
    -- 0) noop
    begin
        if rising_edge(clk40) then
            case counter is
                when "00" =>
                    if (rst = '1') then
                        ser_data_i <= idle;
                        dataout_i    <= idle(15 downto 12);
                        sync_sent_i <= '1';
                        az_sent_i   <= '0';
                        cmd_sent_i<= '0';
                    else
                        if (rst_rdy = '1') then
                            ser_data_i  <= cmd_data;
                            dataout_i   <= cmd_data(15 downto 12);
                            sync_sent_i <= '0';
                            az_sent_i   <= '0';
                            cmd_sent_i  <= '1';
                        elsif (TTCin.ITk_trig /= "0000") then ---(trig_rdy = '1') then
                            ser_data_i <= trig_data;
                            dataout_i    <= trig_data(15 downto 12);
                            sync_sent_i <= '0';
                            az_sent_i   <= '0';
                            cmd_sent_i  <= '0';
                        elsif (sync_rdy = '1') then
                            ser_data_i  <= Sync;
                            dataout_i   <= Sync(15 downto 12);
                            sync_sent_i <= '1';
                            az_sent_i   <= '0';
                            cmd_sent_i  <= '0';
                        elsif (az_rdy = '1') then
                            ser_data_i  <= az;
                            dataout_i   <= az(15 downto 12);
                            sync_sent_i <= '0';
                            az_sent_i   <= '1';
                            cmd_sent_i  <= '0';
                        elsif (cmd_rdy = '1') then
                            ser_data_i  <= cmd_data;
                            dataout_i   <= cmd_data(15 downto 12);
                            sync_sent_i <= '0';
                            az_sent_i   <= '0';
                            cmd_sent_i  <= '1';
                        else
                            ser_data_i  <= idle;
                            dataout_i   <= idle(15 downto 12);
                            sync_sent_i <= '0';
                            az_sent_i   <= '0';
                            cmd_sent_i  <= '0';
                        end if;
                    end if;
                when "01" =>
                    dataout_i    <= ser_data_i(11 downto 8);
                when "10" =>
                    dataout_i    <= ser_data_i(7 downto 4);
                when "11" =>
                    dataout_i    <= ser_data_i(3 downto 0);
                when others =>
                    dataout_i    <= "0000";
            end case;
        end if;
    end process;

    ser_data_v_i <= '1' when counter = "01" else '0';
    g_DebbuggingModule: if DEBUGGING_RD53 generate--and RD53Version = "A" generate --RL FLX-2221 debug
        RD53A_DebuggingModule_i: entity work.RD53A_DebuggingModule port map (
                rst                         => rst,
                clk40                       => clk40,
                data_in                     => ser_data_i,
                datav_in                    => ser_data_v_i,
                ref_cmd_in                  => ref_cmd_in,
                cnt_cmd_out                 => cnt_cmd,
                cnt_trig_cmd_out            => cnt_trig_cmd,
                cnt_time_firstTolastTrig_out => cnt_time_firstTolastTrig,
                ref_dly_genCalTrig_in       => ref_dly_genCalTrig_in,
                err_genCalTrig_dly_out      => err_genCalTrig_dly,

                --debug
                state_trigTime_ila          => state_trigTime_ila,
                cnt_time_twoTrig_ila        => cnt_time_twoTrig_ila,
                cnt_trig_ila                => cnt_trig_ila,
                cnt_genCalTrig_dly_ila      => cnt_genCalTrig_dly_ila,
                cnt_genCal_fromSM_ila       => cnt_genCal_fromSM_ila,
                state_chkdly_genCalTrig_ila => state_chkdly_genCalTrig_ila,
                cnt_datav_fromCal_ila       => cnt_datav_fromCal_ila

            );
        cnt_cmd_out <= cnt_cmd;
        cnt_trig_cmd_out <= cnt_trig_cmd;
        cnt_time_firstTolastTrig_out <= cnt_time_firstTolastTrig;
        err_genCalTrig_dly_out <= err_genCalTrig_dly;


    --        ila_gen : if ENDPOINT_N = 0  and LINK_N = 0 generate --RL: if used it needs generics all the way to felix_top. see commit 4aaba0411932143a5749ca271a52e80b855d49ec
    --            ila_0_RD53A_in_i : entity work.ila_0_RD53A
    --                PORT MAP (
    --                    clk        => clk40,
    --                    probe0     => command_in,
    --                    probe1(0)  => command_rdy,
    --                    probe2(0)  => count40,
    --                    probe3     => command_in20,
    --                    probe4(0)  => command_rdy20,
    --                    probe5     => command_in40,
    --                    probe6(0)  => command_rdy40,
    --                    probe7(0)  => ReadEnable40,
    --                    probe8(0)  => almost_full_fifo,
    --                    probe9(0)  => full_fifo,
    --                    probe10(0) => trigger,
    --                    probe11    => counter,
    --                    probe12(0) => trig_rdy,
    --                    probe13    => trig_data,
    --                    probe14(0) => sync_cmd_type,
    --                    probe15(0) => sync_sent_i,
    --                    probe16(0) => sync_rdy,
    --                    probe17(0) => cmd_sent_i,
    --                    probe18    => cmd_data,
    --                    probe19(0) => cmd_rdy,
    --                    probe20    => dataout_i,
    --                    probe21    => ser_data_i,
    --                    probe22(0) => az_rdy,
    --                    probe23    => frequency_i,
    --                    probe24    => frequency_cnt_i ,
    --                    probe25(0) => freq_en_i       ,
    --                    probe26    => cmd_out_cnts_i  ,
    --                    probe27    => pointer_i       ,
    --                    probe28    => iteration_i     ,
    --                    probe29    => cmd_state_is_i  ,
    --                    probe30    => cmd_reg0_i      ,
    --                    probe31    => cmd_reg1_i      ,
    --                    probe32    => cmd_reg2_i      ,
    --                    probe33    => cmd_reg3_i      ,
    --                    probe34(0) => empty_fifo      ,
    --                    probe35 => startread, --waitcnt_before_start,

    --                    probe36(0) => rst_rdy, --(others => '0'), --din_debug_cmdcheck,
    --                    probe37 => (others => '0'), --wr_en_debug_cmdcheck,
    --                    probe38 => (others => '0'), --rd_en_debug_cmdcheck,
    --                    probe39 => (others => '0'), --dout_debug_cmdcheck,
    --                    probe40 => (others => '0'), --full_debug_cmdcheck,
    --                    probe41 => (others => '0'), --overflow_debug_cmdcheck,
    --                    probe42 => (others => '0'), --empty_debug_cmdcheck,
    --                    probe43 => (others => '0'), --valid_debug_cmdcheck,
    --                    probe44 => (others => '0'), --underflow_debug_cmdcheck,
    --                    probe45 => (others => '0'), --wr_rst_busy_debug_cmdcheck,
    --                    probe46 => (others => '0'), --rd_rst_busy_debug_cmdcheck,
    --                    probe47    => cnt_trig_cmd,
    --                    probe48    => err_genCalTrig_dly,
    --                    probe49 => state_trigTime_ila,
    --                    probe50 => cnt_time_twoTrig_ila,
    --                    probe51 => cnt_trig_ila,
    --                    probe52 => cnt_genCalTrig_dly_ila,
    --                    probe53 => cnt_genCal_fromSM_ila,
    --                    probe54 => (others => '0'), --state_chkdly_genCalTrig_ila,
    --                    probe55    => err_cmdtop_i,
    --                    probe56    => warn_cmdtop_i,
    --                    probe57    => (others => '0'), --cnt_wrreg,
    --                    probe58    => (others => '0'), --cnt_wrreg_cmdin20,
    --                    probe59    => (others => '0'), --cnt_cal,
    --                    probe60    => (others => '0'), --cnt_cal_cmdin20,
    --                    probe61    => CalTrigSeq_in,
    --                    probe62    => ReadAddrCalTrigSeq_tmp
    --                );
    --        end generate;
    end generate;



--

end Behavioral;

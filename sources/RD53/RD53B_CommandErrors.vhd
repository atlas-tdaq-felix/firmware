--rluz at anl dot gov

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use IEEE.numeric_std_unsigned.ALL;
    use work.rd53_package.ALL;

entity RD53B_CommandErrors is
    port (
        rst                 : in std_logic;
        clk                 : in std_logic;
        --warn
        to_wcounter         : in std_logic;
        --error
        to_ecounter         : in std_logic;
        to_ecounter_fword   : in std_logic;
        to_ecounter_cmd     : in std_logic_vector(15 downto 0);
        to_ecounter_cmd_r   : in std_logic;
        --output
        err_out              : out std_logic_vector(7 downto 0);
        warn_out             : out std_logic_vector(7 downto 0)

    );
end RD53B_CommandErrors;

architecture Behavioral of RD53B_CommandErrors is
    constant RD53Version    : String := "B"; --B only
    signal is_data          : std_logic := '0';
    signal is_first_word    : std_logic := '0';
-----------------------------------------------------------------
--For RD53B encoder (cmd_top_v2.vhd)
--In this new version:
--      - err_out: counts data that was sent to the chip that is not valid, for example: a data byte not part of table 32 of the manual
--      - warn_out: counts encoding headers rejected by the encoder and not sent to the chip
--Checks 16 bit words not individual bytes
-----------------------------------------------------------------

begin

    process(clk)
        variable err_i          : std_logic_vector(7 downto 0) := (others=>'0');
        variable warn_i         : std_logic_vector(7 downto 0) := (others=>'0');
        variable first_byte     : std_logic_vector(7 downto 0) := (others=>'0');
        variable second_byte    : std_logic_vector(7 downto 0) := (others=>'0');
    begin
        if rising_edge(clk) then
            if (rst = '1') then
                err_i           := (others => '0');
                warn_i          := (others => '0');
                first_byte      := (others => '0');
                second_byte     := (others => '0');
                is_data         <= '0';
                is_first_word   <= '0';
            else
                --err_out
                is_data <= to_ecounter and to_ecounter_cmd_r;
                is_first_word <= to_ecounter_fword and to_ecounter_cmd_r;
                if is_data = '1' and err_i /= x"FF" then
                    first_byte  := to_ecounter_cmd(15 downto 8);
                    second_byte := to_ecounter_cmd( 7 downto 0);
                    if is_first_word = '1'then
                        if (to_ecounter_cmd = Sync) or
                           (to_ecounter_cmd = PLLLockB) or
                           (isTrigger(first_byte) and isTriggerTag(second_byte,RD53Version)) or
                           (first_byte = ReadTriggerB and isData(second_byte,RD53Version) = '1') or
                           (first_byte = ClearB and isData(second_byte,RD53Version) = '1') or
                           (first_byte = GlobalPulseB and isData(second_byte,RD53Version) = '1') or
                           (first_byte = CalB and isData(second_byte,RD53Version) = '1') or
                           (first_byte = WrRegB and isData(second_byte,RD53Version) = '1') or
                           (first_byte = RdRegB and isData(second_byte,RD53Version) = '1') or
                           (isReset(to_ecounter_cmd,RD53Version)) then
                            err_i := err_i;
                        else
                            err_i := err_i + x"01";
                        end if;
                    else
                        if (to_ecounter_cmd = Sync) or
                           (to_ecounter_cmd = PLLLockB) or
                           (isTrigger(first_byte) and isTriggerTag(second_byte,RD53Version)) or
                           (first_byte = ClearB and isData(second_byte,RD53Version) = '1') or
                           (first_byte = GlobalPulseB and isData(second_byte,RD53Version) = '1') or
                            --Above are all the short words that can be send mid long words
                           (isData(first_byte,RD53Version) = '1' and isData(second_byte,RD53Version) = '1') then
                            --Right now just checks if the byte is one of the 32 valid bytes.
                            --Further checks can be implemented for words that end or start with zeros.
                            err_i := err_i;
                        else
                            err_i := err_i + x"01";
                        end if;
                    end if; -- to_ecounter_fword
                end if; --to_ecounter

                --warn_out
                if to_wcounter = '1' and warn_i /= x"FF" then
                    warn_i := warn_i + x"01";
                end if;
            end if;
            err_out <= err_i;
            warn_out <= warn_i;
        end if;
    end process;

end Behavioral;

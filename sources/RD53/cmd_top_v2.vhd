--rluz at anl dot gov

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use IEEE.numeric_std_unsigned.ALL;
    use work.rd53_package.ALL;
    use work.FELIX_package.ALL;

entity cmd_top_v2 is
    Generic (
        --constant reg_depth    : positive := 4;
        RD53Version : String := "B" --entity only works for B. Generic was kept for historical reasons
    );
    Port (
        rst                     : in  std_logic;
        clk40                   : in  std_logic;
        startread               : in  std_logic; --not needed. start read is done inside this entity instead -- @suppress "Unused port: startread is not used in work.cmd_top_v2(Behavioral)"
        ReadEnable              : out std_logic;
        cmd_sent                : in  std_logic;
        counter                 : in  std_logic_vector( 1 downto 0);
        cmd_in                  : in  std_logic_vector(15 downto 0);
        command_rdy             : in  std_logic;
        RD53B_loopgen_reg       : in  RD53_loopgen_type;
        --CalTrigSeq_in           : in  std_logic_vector(15 downto 0);
        --ReadAddrCalTrigSeq_out  : out std_logic_vector( 4 downto 0);
        cmd_data                : out std_logic_vector(15 downto 0);
        rst_ready               : out std_logic;
        cmd_ready               : out std_logic;
        sync_cmd_type           : out std_logic;
        az_cmd_type             : out std_logic; --not needed. only RD53A
        enAZ_viacmd             : out std_logic; --not needed. only RD53A
        --debug
        frequency_out           : out std_logic_vector( 4 downto 0);
        frequency_cnt_out       : out std_logic_vector(11 downto 0);
        freq_en_out             : out std_logic;
        cmd_out_cnts_out        : out std_logic_vector( 4 downto 0);
        pointer_out             : out std_logic_vector( 4 downto 0);
        iteration_out           : out std_logic_vector( 6 downto 0);
        cmd_state_is_out        : out std_logic_vector( 1 downto 0);
        cmd_reg0_out            : out std_logic_vector(15 downto 0);
        cmd_reg1_out            : out std_logic_vector(15 downto 0);
        cmd_reg2_out            : out std_logic_vector(15 downto 0);
        cmd_reg3_out            : out std_logic_vector(15 downto 0);
        err_cmdtop_out          : out std_logic_vector( 7 downto 0);
        warn_cmdtop_out         : out std_logic_vector( 7 downto 0)

    );
end cmd_top_v2;

architecture Behavioral of cmd_top_v2 is
    signal cmd_valid        : std_logic := '0';
    signal cmd_fifo         : std_logic_vector(15 downto 0) := (others=>'0');
    signal cmd_valid_dly    : std_logic := '0';
    signal cmd_fifo_dly     : std_logic_vector(15 downto 0) := (others=>'0');
    type state_type is (
        idle,
        send_cmd,
        user_cmd
    );
    signal state            : state_type := idle;
    signal read_en          : std_logic := '0';
    signal cmd_size         : std_logic_vector( 2 downto 0) := (others=>'0');
    signal cmd_cnt          : std_logic_vector( 2 downto 0) := (others=>'0');
    signal cmd_out          : std_logic_vector(15 downto 0) := (others=>'0');
    signal cmd_rdy          : std_logic := '0';
    signal sync_out         : std_logic := '0';
    signal cmd_send         : std_logic := '0';
    signal first_wrd        : std_logic := '0'; --first word after valid goes down
    signal WrCmdLong        : std_logic := '0';
    signal rst_rdy          : std_logic := '0';
    signal iter_nbr         : std_logic_vector( 6 downto 0) := (others=>'0');
    signal iter_cnt         : std_logic_vector( 6 downto 0) := (others=>'0');
    signal user_cnt         : std_logic_vector( 6 downto 0) := (others=>'0');
    signal freq             : std_logic_vector( 4 downto 0) := (others=>'0');
    signal freq_bef         : std_logic_vector( 4 downto 0) := (others=>'0');

    -- frequency from cmd_top_v1.vhd
    signal frequency_cnt        : std_logic_vector(11 downto 0) := (others => '0');
    signal frequency_cnt_extra  : std_logic_vector(35 downto 0) := (others => '0'); -- @suppress "signal frequency_cnt_extra is never read"
    signal freq_en_pipe         : std_logic_vector( 3 downto 0) := (others => '0');
    signal freq_en              : std_logic := '0';
    signal freq_sclr            : std_logic := '0';
    signal freq_cnt_load        : std_logic := '0';

    --erro and warning counter flags
    signal to_wcounter          : std_logic := '0';
    signal to_ecounter          : std_logic := '0';
    signal to_ecounter_fword    : std_logic := '0';
    signal err_cmdtop           : std_logic_vector( 7 downto 0);
    signal warn_cmdtop          : std_logic_vector( 7 downto 0);

    signal loop_out             : std_logic_vector(15 downto 0);
    signal loop_done            : std_logic := '0';
begin

    --in
    cmd_valid <= command_rdy;
    cmd_fifo  <= cmd_in(15 downto 0);

    --out
    ReadEnable              <= read_en;
    --ReadAddrCalTrigSeq_out  <= user_cnt(4 downto 0);
    cmd_data                <= cmd_out;
    rst_ready               <= rst_rdy;
    cmd_ready               <= cmd_rdy;
    sync_cmd_type           <= sync_out;
    az_cmd_type             <= '0';--not needed. only RD53A
    enAZ_viacmd             <= '0';--not needed. only RD53A

    --out debug
    frequency_out       <= freq;
    frequency_cnt_out   <= frequency_cnt;
    freq_en_out         <= freq_en;
    cmd_out_cnts_out    <= "00" & cmd_cnt;
    pointer_out         <= "00" & cmd_size;
    iteration_out       <= iter_cnt;
    cmd_state_is_out    <= "00" when state = idle else
                           "01" when state = send_cmd else
                           "10" when state = user_cmd else
                           "11";
    cmd_reg0_out        <= cmd_fifo;
    cmd_reg1_out        <= cmd_fifo_dly;
    cmd_reg2_out        <= cmd_send & first_wrd & WrCmdLong & cmd_valid & cmd_valid_dly & "00000000000";
    cmd_reg3_out        <= (others=>'0');
    err_cmdtop_out      <= err_cmdtop;
    warn_cmdtop_out     <= warn_cmdtop;

    --other
    cmd_send <= first_wrd or (cmd_sent and (not loop_done) ) or freq_en; --freq_en only used when user cmd
    --state machine
    process(clk40)
    begin
        if rising_edge(clk40) then
            if (rst = '1') then
                state    <= idle;
                read_en  <= '0';
                cmd_size <= (others=>'0');
                cmd_cnt  <= (others=>'0');
                cmd_out  <= (others=>'0');
                cmd_rdy  <= '0';
                sync_out <= '0';
                cmd_valid_dly <= '0';
                cmd_fifo_dly <= (others=>'0');
                first_wrd <= '0';
                WrCmdLong <= '0';
                rst_rdy <= '0';
                iter_nbr <= (others=>'0');
                iter_cnt <= (others=>'0');
                user_cnt <= (others=>'0');
                freq     <= (others=>'0');
                freq_bef <= (others=>'0');
                to_wcounter <= '0';
                to_ecounter <= '0';
                to_ecounter_fword <= '0';
                loop_done <= '0';
            else
                case state is
                    when idle =>
                        if  cmd_valid = '1' and counter = "01" then -- there were issues if counter was '00". this way there are always a minimum of 4 clocks between valid going up and data being sent
                            read_en         <= '1';
                            cmd_fifo_dly    <= cmd_fifo;
                            if isCmdOrTrig(cmd_fifo,RD53Version) then
                                state           <= send_cmd;
                                cmd_size        <= std_logic_vector(to_unsigned(numFrameCommand(cmd_fifo, RD53Version), cmd_size'length)); --anything with size bigger than 1 is long. only write commands have size 4
                                cmd_valid_dly   <= '1';
                                first_wrd       <= '1';
                                iter_nbr        <= (others=>'0');
                                freq_bef        <= (others=>'0');
                                to_wcounter <= '0';
                            elsif isCalTrigSeq(cmd_fifo(15 downto 12)) then
                                state       <= user_cmd;
                                first_wrd   <= '1'; --not needed since freq_en will be set to 1. let me keep it anyway
                                iter_nbr    <= cmd_fifo(11 downto 5) - "0000001"; --this way 1 is 1 and 0 is 2^7-1, like in the original cmd_top.vhd
                                freq_bef    <= cmd_fifo( 4 downto 0);
                                to_wcounter <= '0';
                            else
                                state       <= idle;
                                first_wrd   <= '0';
                                iter_nbr    <= (others=>'0');
                                freq_bef    <= (others=>'0');
                                to_wcounter <= '1';
                            end if;--cmd_type
                        else
                            state <= idle;
                            read_en  <= '0';
                            cmd_size <= (others=>'0');
                            cmd_valid_dly <= '0';
                            first_wrd <= '0';
                            iter_nbr  <= (others=>'0');
                            freq      <= (others=>'0');
                            freq_bef  <= (others=>'0');
                            to_wcounter <= '0';
                        end if; --cmd_valid
                        cmd_cnt  <= (others=>'0');
                        cmd_out  <= (others=>'0');
                        rst_rdy  <= '0';
                        cmd_rdy  <= '0';
                        sync_out <= '0';
                        WrCmdLong <= '0';
                        rst_rdy <= '0';
                        iter_cnt <= (others=>'0');
                        user_cnt <= (others=>'0');
                        to_ecounter <= '0';
                        to_ecounter_fword <= '0';
                        loop_done <= '0';
                    when send_cmd =>
                        if counter = "01" and cmd_send = '1' then
                            if cmd_cnt /= cmd_size then
                                state <= send_cmd;
                                cmd_out <= cmd_fifo_dly;
                                to_wcounter <= '0';
                                to_ecounter <= '1';
                                cmd_valid_dly <= cmd_valid; --saves state
                                if  cmd_valid_dly = '1'then
                                    read_en <= cmd_valid; --set to 0 when sending the last work (cmd_valid_dly = '1' and cmd_valid ='0')
                                    cmd_fifo_dly <= cmd_fifo;
                                    if isReset(cmd_fifo_dly, RD53Version) then
                                        rst_rdy <= '1';
                                        cmd_rdy <= '0';
                                    elsif isCalTrigSeq(cmd_fifo_dly(15 downto 12)) and WrCmdLong = '1' then --makes sure that if a user command comes during a long commmand (either broken or not), xEXXX is not sent out
                                        rst_rdy <= '0';
                                        cmd_rdy <= '0';
                                    else
                                        rst_rdy <= '0';
                                        cmd_rdy <= '1';
                                    end if;
                                    sync_out <= isSyncSTD(cmd_fifo_dly);
                                    first_wrd <= '0';
                                    if numFrameCommand(cmd_fifo_dly, RD53Version) = 1 and cmd_size /= "001" then --if short command shows up in tge middle of long
                                        cmd_cnt <= cmd_cnt;
                                        to_ecounter_fword <= (not cmd_cnt(0)) and  (not cmd_cnt(1)) and (not cmd_cnt(2));
                                    elsif numFrameCommand(cmd_fifo_dly, RD53Version) = 4 and isWrCmdLong(cmd_fifo, RD53Version) = '1' then
                                        WrCmdLong <= '1';
                                        cmd_cnt <= "001";
                                        to_ecounter_fword <= '1';
                                    elsif WrCmdLong = '1' then --if it's a write command long keep cmd_cnt value until WrCmdLong goes to 0 (new long command shows up)
                                        if isLongCommand(cmd_fifo_dly,RD53Version) then --checks for new long cmd when sending long write commands
                                            cmd_size <= std_logic_vector(to_unsigned(numFrameCommand(cmd_fifo_dly, RD53Version), cmd_size'length));
                                            cmd_cnt  <= "001";
                                            to_ecounter_fword <= '1';
                                            WrCmdLong <= '0';
                                        elsif isCalTrigSeq(cmd_fifo_dly(15 downto 12)) then
                                            state           <= user_cmd;
                                            cmd_fifo_dly    <= cmd_fifo;
                                            iter_nbr        <= cmd_fifo(11 downto 5) - "0000001"; --this way 1 is 1 and 0 is 2^7-1, like in the original cmd_top.vhd
                                            freq_bef        <= cmd_fifo( 4 downto 0);
                                            cmd_size        <= (others=>'0');
                                            cmd_cnt         <= (others=>'0');
                                            WrCmdLong       <= '0';
                                            to_ecounter_fword <= (not cmd_cnt(0)) and  (not cmd_cnt(1)) and (not cmd_cnt(2));
                                        else
                                            cmd_size <= cmd_size;
                                            cmd_cnt <= cmd_cnt;
                                            WrCmdLong <= WrCmdLong;
                                            to_ecounter_fword <= (not cmd_cnt(0)) and  (not cmd_cnt(1)) and (not cmd_cnt(2));
                                        end if;
                                    else
                                        cmd_cnt <= cmd_cnt + "001";
                                        to_ecounter_fword <= (not cmd_cnt(0)) and  (not cmd_cnt(1)) and (not cmd_cnt(2));
                                    end if;
                                else --cmd_valid_dly = '0'
                                    read_en <= cmd_valid; --cmd_valid = '1' and cmd_valid_dly = '0' happens very rarely but it needs to be accounted for
                                    if cmd_valid = '1' then -- happens very rarely. see comment above
                                        cmd_fifo_dly <= cmd_fifo;
                                        first_wrd <= '1';
                                        if numFrameCommand(cmd_fifo_dly, RD53Version) = 4 and isWrCmdLong(cmd_fifo, RD53Version) = '1' then
                                            WrCmdLong <= '1';
                                        end if;
                                    else -- both '0' when waiting for more data
                                        cmd_fifo_dly <= cmd_fifo_dly;
                                        first_wrd <= '0';
                                    end if;
                                    rst_rdy  <= '0';
                                    cmd_rdy  <= '0';
                                    cmd_cnt  <= cmd_cnt;
                                    sync_out <= '0';
                                    to_ecounter_fword <= (not cmd_cnt(0)) and  (not cmd_cnt(1)) and (not cmd_cnt(2));
                                end if; --cmd_valid_dly
                            else -- cmd_cnt = cmd_size
                                if cmd_valid_dly = '1' and numFrameCommand(cmd_fifo_dly, RD53Version) /= 5 then--new command showed up. /= 5 guarantees it's actually a command
                                    read_en <= '1';
                                    cmd_fifo_dly <= cmd_fifo;
                                    state <= send_cmd;
                                    cmd_size <= std_logic_vector(to_unsigned(numFrameCommand(cmd_fifo_dly, RD53Version), cmd_size'length));
                                    cmd_cnt  <= "001";
                                    cmd_out <= cmd_fifo_dly;
                                    to_ecounter <= '1';
                                    to_ecounter_fword <= '1';
                                    if isReset(cmd_fifo_dly, RD53Version) then
                                        rst_rdy <= '1';
                                        cmd_rdy <= '0';
                                    else
                                        rst_rdy <= '0';
                                        cmd_rdy <= '1';
                                    end if;
                                    sync_out <= isSyncSTD(cmd_fifo_dly);
                                    cmd_valid_dly <= cmd_valid;
                                    if numFrameCommand(cmd_fifo_dly, RD53Version) = 4 and isWrCmdLong(cmd_fifo, RD53Version) = '1' then
                                        WrCmdLong <= '1';
                                    end if; --else not needed because it would be WrCmdLong <= WrCmdLong
                                elsif cmd_valid_dly = '1' and isCalTrigSeq(cmd_fifo(15 downto 12)) then--new command shows up and it's a usercmd
                                    state           <= user_cmd;
                                    cmd_fifo_dly    <= cmd_fifo;
                                    --first_wrd   <= '1'; --not needed since freq_en will be set to 1
                                    iter_nbr        <= cmd_fifo(11 downto 5) - "0000001"; --this way 1 is 1 and 0 is 2^7-1, like in the original cmd_top.vhd
                                    freq_bef        <= cmd_fifo( 4 downto 0);
                                    read_en         <= '1';
                                    cmd_size        <= (others=>'0');
                                    cmd_cnt         <= (others=>'0');
                                    cmd_out         <= (others=>'0');
                                    rst_rdy         <= '0';
                                    cmd_rdy         <= '0';
                                    sync_out        <= '0';
                                    WrCmdLong       <= '0';
                                    to_ecounter         <= '0';
                                    to_ecounter_fword   <= '0';
                                else --either no new command (cmd_valid_dly = '0') or wrong command (cmd_valid_dly = '1')
                                    state <= idle;
                                    read_en  <= '0';
                                    cmd_size <= (others=>'0');
                                    cmd_cnt  <= (others=>'0');
                                    cmd_out  <= (others=>'0');
                                    rst_rdy  <= '0';
                                    cmd_rdy  <= '0';
                                    sync_out <= '0';
                                    WrCmdLong <= '0';
                                    if cmd_valid_dly = '1' then
                                        to_wcounter <= '1';
                                    else
                                        to_wcounter <= '0';
                                    end if;
                                    to_ecounter         <= '0';
                                    to_ecounter_fword   <= '0';
                                end if;
                                first_wrd <= '0';
                            end if;-- cmd_cnt /= cmd_size
                        --after a break during transmission of long commands
                        elsif counter = "01" and cmd_send = '0' and cmd_valid = '1' and cmd_valid_dly = '0' and cmd_cnt /= cmd_size then
                            state <= send_cmd;
                            cmd_out <= cmd_fifo_dly;
                            cmd_valid_dly <= cmd_valid;
                            read_en <= '1';
                            cmd_fifo_dly <= cmd_fifo;
                            cmd_rdy  <= '0';
                            cmd_cnt  <= cmd_cnt;
                            sync_out <= '0';
                            first_wrd <= '1';
                            to_wcounter <= '0';
                            if numFrameCommand(cmd_fifo_dly, RD53Version) = 4 and isWrCmdLong(cmd_fifo, RD53Version) = '1' then
                                WrCmdLong <= '1';
                            end if; --else not needed because it would be WrCmdLong <= WrCmdLong
                            to_wcounter <= '0';
                            to_ecounter <= '0';
                            to_ecounter_fword <= '0';
                        else
                            state <= send_cmd;
                            read_en <= '0';
                            cmd_size <= cmd_size;
                            cmd_cnt  <= cmd_cnt;
                            cmd_out  <= cmd_out;
                            cmd_rdy  <= cmd_rdy;
                            sync_out <= sync_out;
                            first_wrd <= first_wrd;
                            WrCmdLong <= WrCmdLong;
                            to_wcounter <= '0';
                            to_ecounter <= '0';
                            to_ecounter_fword <= '0';
                        end if; --counter = "01" and cmd_send = '1'
                    when user_cmd => --can user commands be sent while long commands are being sent? should the user command kill the long write command?
                        if counter = "01" and cmd_send = '1' then
                            first_wrd <= '0';
                            if user_cnt = "0111111" then
                                user_cnt <= "0000000";
                                --cmd_rdy <= '0';
                                if iter_cnt = iter_nbr then
                                    state <= idle;
                                    loop_done <= '0';
                                else
                                    state <= user_cmd;
                                    iter_cnt <= iter_cnt + "0000001";
                                    loop_done <= '1';
                                end if;
                            else
                                --cmd_rdy <= '1';
                                loop_done <= '0';
                                user_cnt <= user_cnt + "0000001";
                                iter_cnt <= iter_cnt;
                            end if; --user_cnt
                            cmd_out <= loop_out;--CalTrigSeq_in;
                            cmd_rdy <= '1';
                            sync_out <= isSyncSTD(cmd_out); --avoids sync pulses in between the cal command and the trigger command
                        elsif counter = "01" and loop_done <= '1' then
                            loop_done <= '0';
                            cmd_rdy <= '0';
                        else
                            first_wrd <= first_wrd;
                        end if; --counter = "01" and cmd_send = '1'
                        freq    <= freq_bef;
                        read_en <= '0';
                end case; --state machine
            end if; --rst
        end if; --clk40
    end process;


    --frequency counters from cmd_top_v1.vhd
    freq_counter : dsp_counter
        PORT MAP (
            CLK             => clk40,
            CE              => '1',
            SCLR            => freq_sclr,
            UP              => '0',
            LOAD            => freq_cnt_load,
            L(47 downto 12) => (others => '0'),
            L(11 downto  7) => freq,
            L( 6 downto  0) => "1111110",
            Q(47 downto 12) => frequency_cnt_extra,
            Q(11 downto  0) => frequency_cnt
        );

    process(clk40)
    begin
        if rising_edge(clk40) then
            if (rst = '1') then
                freq_en_pipe(0) <= '0';
                freq_cnt_load   <= '0';
                freq_sclr       <= '1';
            elsif freq = "00000" then
                freq_en_pipe(0) <= '0';
                freq_cnt_load   <= '1';
                freq_sclr       <= '1';
            elsif freq /= "00000" and frequency_cnt = "000000000000" then
                freq_en_pipe(0) <= '1';
                freq_cnt_load   <= '1';
                freq_sclr       <= '0';
            else
                freq_en_pipe(0) <= '0';
                freq_cnt_load   <= '0';
                freq_sclr       <= '0';
            end if;
        end if;
    end process;

    process(clk40)
    begin
        if rising_edge(clk40) then
            freq_en_pipe(3) <= freq_en_pipe(2);
            freq_en_pipe(2) <= freq_en_pipe(1);
            freq_en_pipe(1) <= freq_en_pipe(0);
        end if;
    end process;

    freq_en <= freq_en_pipe(0) or freq_en_pipe(1) or freq_en_pipe(2) or freq_en_pipe(3);

    RD53B_CommandErrors_i: entity work.RD53B_CommandErrors
        port map (
            rst                 => rst,
            clk                 => clk40,
            --warn
            to_wcounter         => to_wcounter,
            --error
            to_ecounter         => to_ecounter,
            to_ecounter_fword   => to_ecounter_fword,
            to_ecounter_cmd     => cmd_out,
            to_ecounter_cmd_r   => cmd_rdy,
            --ouputs
            err_out             => err_cmdtop,
            warn_out            => warn_cmdtop
        );

    RD53B_TriggerLoop_i: entity work.RD53B_TriggerLoop
        port map(
            --rst                 => rst,
            clk40               => clk40,
            count_in            => user_cnt,

            noInject            => RD53B_loopgen_reg.noInject,--false,--bool m_noInject = false; //changed

            edgeMode            => RD53B_loopgen_reg.edgeMode,--true,--bool m_edgeMode = true; //changed
            edgeDelay           => RD53B_loopgen_reg.edgeDelay,--"00000",--uint32_t m_calEdgeDelay = 0; //unchanged
            edgeDuration        => RD53B_loopgen_reg.edgeDuration,--"00010100",--uint32_t m_edgeDuration = 20; //changed

            trigDelay           => RD53B_loopgen_reg.trigDelay,--"00111010",--uint32_t m_trigDelay = 58; //changed
            trigMultiplier      => RD53B_loopgen_reg.trigMultiplier,--"010000",--uint32_t m_trigMultiplier = 16; //unchanged

            loop_out            => loop_out
        );

--    RD53B_TriggerLoop_ii: entity work.RD53B_TriggerLoop
--        port map(
--            --rst                 => rst,
--            clk40               => clk40,
--            count_in            => user_cnt,

--            noInject            => '0',--bool m_noInject = false; //changed

--            edgeMode            => '0',--bool m_edgeMode = true; //changed
--            edgeDelay           => "00000",--uint32_t m_calEdgeDelay = 0; //unchanged
--            edgeDuration        => "00010100",--uint32_t m_edgeDuration = 20; //changed

--            trigDelay           => "00011111",--uint32_t m_trigDelay = 58; //changed
--            trigMultiplier      => "111111",--uint32_t m_trigMultiplier = 16; //unchanged

--            loop_out            => open
--        );

end Behavioral;

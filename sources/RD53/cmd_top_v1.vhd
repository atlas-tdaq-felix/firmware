--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer: Alexander Paramonov, Marco Trovato, Tianxing Zheng
--
-- Create Date: 04/11/2018 09:42:32 PM
-- Design Name:
-- Module Name: cmd_top - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use IEEE.numeric_std_unsigned.ALL;
    use work.rd53_package.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--library xil_defaultlib;-- declear for using the components
--use xil_defaultlib.fifo_generator_0;

entity cmd_top_v1 is
    Generic (
        constant reg_depth    : positive := 4;
        RD53Version : String := "A" --A or B
    );
    Port (
        rst              : in STD_LOGIC;
        clk40            : in STD_LOGIC;
        startread        : in std_logic;
        ReadEnable       : out std_logic;
        cmd_sent         : in STD_LOGIC;
        counter          : in STD_LOGIC_VECTOR(1 downto 0);
        cmd_in           : in STD_LOGIC_VECTOR(15 downto 0);
        command_rdy      : in STD_LOGIC;
        CalTrigSeq_in       : in std_logic_vector(15 downto 0);
        ReadAddrCalTrigSeq_out : out std_logic_vector(4 downto 0);
        cmd_data          : out STD_LOGIC_VECTOR(15 downto 0);
        rst_ready         : out STD_LOGIC;
        cmd_ready         : out STD_LOGIC;
        sync_cmd_type     : out STD_LOGIC;
        az_cmd_type       : out STD_LOGIC;
        enAZ_viacmd       : out STD_LOGIC;
        --debug
        frequency_out     : out STD_LOGIC_VECTOR(4 downto 0);
        frequency_cnt_out : out STD_LOGIC_VECTOR(11 downto 0);
        freq_en_out       : out STD_LOGIC;
        cmd_out_cnts_out  : out STD_LOGIC_VECTOR(4 downto 0);
        pointer_out       : out STD_LOGIC_VECTOR(4 downto 0);
        iteration_out     : out STD_LOGIC_VECTOR(6 downto 0);
        cmd_state_is_out  : out STD_LOGIC_VECTOR(1 downto 0);
        cmd_reg0_out      : out STD_LOGIC_VECTOR(15 downto 0);
        cmd_reg1_out      : out STD_LOGIC_VECTOR(15 downto 0);
        cmd_reg2_out      : out STD_LOGIC_VECTOR(15 downto 0);
        cmd_reg3_out      : out STD_LOGIC_VECTOR(15 downto 0);
        err_cmdtop_out    : out STD_LOGIC_VECTOR(7 downto 0);
        warn_cmdtop_out    : out STD_LOGIC_VECTOR(7 downto 0)

    );
end cmd_top_v1;

architecture Behavioral of cmd_top_v1 is

    type reg_type is array (0 to reg_depth - 1) of STD_LOGIC_VECTOR(15 downto 0);
    type state_type is (wait_before_read,
        read_from_fifo,
        send_command
    );

    signal cmd_reg : reg_type := (others =>(others => '0'));
    signal cmd_outof_fifo : STD_LOGIC_VECTOR(15 downto 0);
    signal pointer : STD_LOGIC_VECTOR(4 downto 0):= (others => '0');
    signal state: state_type := read_from_fifo;--better to have initial value
    signal cmd_valid    : STD_LOGIC;
    signal fifo_rd_en   : STD_LOGIC;
    signal cmd_out_cnts : STD_LOGIC_VECTOR(4 downto 0) := (others => '0');
    signal cmd_data_i : STD_LOGIC_VECTOR(15 downto 0)     := (others => '0');
    signal iteration  : STD_LOGIC_VECTOR(6 downto 0) := (others => '0');
    signal frequency  : STD_LOGIC_VECTOR(4 downto 0) := (others => '0');
    signal frequency_cnt  : STD_LOGIC_VECTOR(11 downto 0) := (others => '0');     --
    signal frequency_cnt_extra  : STD_LOGIC_VECTOR(35 downto 0) := (others => '0');     --     -- @suppress "signal frequency_cnt_extra is never read"
    signal freq_en_pipe : STD_LOGIC_VECTOR(3 downto 0) := "0000";
    signal freq_en : STD_LOGIC := '0';
    signal freq_sclr : STD_LOGIC := '0';
    signal freq_cnt_load : STD_LOGIC := '0';
    signal ReadAddrCalTrigSeq : std_logic_vector(4 downto 0);
    --debug
    signal cmd_state_is : STD_LOGIC_VECTOR(1 downto 0);
    signal is_read_from_fifo : std_logic;

begin

    ReadAddrCalTrigSeq_out <= ReadAddrCalTrigSeq;


    --debug

    frequency_out <= frequency;
    frequency_cnt_out <= frequency_cnt;
    freq_en_out <= freq_en;
    cmd_out_cnts_out <= cmd_out_cnts;
    pointer_out <= pointer;
    iteration_out <= iteration;
    cmd_state_is_out <= cmd_state_is;

    cmd_reg0_out <= cmd_reg(0);
    cmd_reg1_out <= cmd_reg(1);
    cmd_reg2_out <= cmd_reg(2);
    cmd_reg3_out <= cmd_reg(3);

    ReadEnable <= fifo_rd_en;

    --18 bits to 16 bits
    cmd_outof_fifo <= cmd_in(15 downto 0);
    cmd_valid <= command_rdy;

    freq_counter : dsp_counter
        PORT MAP (
            CLK    => clk40,
            CE    => '1',
            SCLR  => freq_sclr,
            UP    => '0',
            LOAD  => freq_cnt_load,
            L(47 downto 12)    => (others => '0'),
            L(11 downto  7)    => frequency,
            L(6 downto 0)         => "1111111", --255+2 (=1 1111111, N.B frequency=0 not allowed) to 4095+2 (11111 1111111)
            -- -> range=[9.8,155.6] kHz depending on frequency
            Q(47 downto 12)    => frequency_cnt_extra,
            Q(11 downto  0)    => frequency_cnt
        );


    process(clk40)
    begin
        if rising_edge(clk40) then
            if (rst = '1') then
                freq_en_pipe(0) <= '0';
                freq_cnt_load   <= '0';
                freq_sclr       <= '1';
            elsif frequency = "00000" then
                --always enable cmd_out_cnts when frequency is not set

                freq_en_pipe(0) <= '0'; --MT Feb 7, was 1
                freq_cnt_load   <= '1';
                freq_sclr       <= '1';
            elsif frequency /= "00000" and frequency_cnt = "000000000000" then
                freq_en_pipe(0) <= '1';
                freq_cnt_load   <= '1';
                freq_sclr       <= '0';
            else
                freq_en_pipe(0) <= '0';
                freq_cnt_load   <= '0';
                freq_sclr       <= '0';
            end if;
        end if;
    end process;

    process(clk40)
    begin
        if rising_edge(clk40)
    then
            freq_en_pipe(3) <= freq_en_pipe(2);
            freq_en_pipe(2) <= freq_en_pipe(1);
            freq_en_pipe(1) <= freq_en_pipe(0);
        end if;
    end process;
    freq_en <= freq_en_pipe(0) or freq_en_pipe(1) or freq_en_pipe(2) or freq_en_pipe(3);

    cmd_data <= cmd_data_i;

    cmd_state_is <= "00" when state = wait_before_read else
                    "01" when state = read_from_fifo else
                    "10";
    is_read_from_fifo <= '1' when state = read_from_fifo else
                         '0';

    process(clk40)
    begin
        if rising_edge(clk40) then
            if (rst = '1') then
                state   <= wait_before_read;
                cmd_reg <= (others =>(others => '0'));
                pointer <= (others=>'0');
                fifo_rd_en <= '0';
                cmd_data_i  <= (others=>'0');
                cmd_ready <= '0';
                sync_cmd_type <= '0';
                az_cmd_type <= '0';
                enAZ_viacmd   <= '0';
                cmd_out_cnts <= (others=>'0');
                iteration <= (others => '0');
                frequency   <= (others => '0');
                ReadAddrCalTrigSeq <= (others=>'0');
                rst_ready <= '0';
            else
                enAZ_viacmd   <= enAZ_viacmd;
                case state is
                    when wait_before_read =>
                        if(startread = '0') then
                            state   <= wait_before_read;
                            cmd_reg <= (others =>(others => '0'));
                            pointer <= (others=>'0');
                            fifo_rd_en <= '0';
                            cmd_data_i  <= (others=>'0');
                            cmd_ready <= '0';
                            sync_cmd_type <= '0';
                            az_cmd_type <= '0';
                            cmd_out_cnts <= (others=>'0');
                            ReadAddrCalTrigSeq <= (others=>'0');
                            iteration <= (others => '0');
                            frequency   <= (others => '0');
                        else
                            state <= read_from_fifo;
                            cmd_reg <= (others =>(others => '0'));
                            pointer <= (others=>'0');
                            fifo_rd_en <= '1';
                            cmd_data_i  <= (others=>'0');
                            cmd_ready <= '0';
                            sync_cmd_type <= '0';
                            az_cmd_type <= '0';
                            cmd_out_cnts <= (others=>'0');
                            ReadAddrCalTrigSeq <= (others=>'0');
                            iteration <= (others => '0');
                            frequency   <= (others => '0');
                        end if;
                    when read_from_fifo =>
                        cmd_out_cnts <= (others => '0');
                        ReadAddrCalTrigSeq <= (others=>'0');
                        if(cmd_valid = '1' and fifo_rd_en = '1') then
                            if(pointer = "00000") then
                                case numFrameCommand(cmd_outof_fifo, RD53Version) is
                                    -- =1 frame message -> ready to send
                                    when 1 =>
                                        state         <= send_command;
                                        cmd_reg(0)    <= cmd_outof_fifo;
                                        fifo_rd_en    <= '0';
                                        if ( isSync(cmd_outof_fifo) ) then
                                            sync_cmd_type <= '1';
                                        else
                                            sync_cmd_type <= '0';
                                        end if;
                                        az_cmd_type   <= '0';
                                        pointer       <= "00000";
                                        if ( isReset(cmd_outof_fifo, RD53Version) ) then
                                            rst_ready     <= '1';
                                            cmd_ready     <= '0';
                                        else
                                            rst_ready     <= '0';
                                            cmd_ready     <= '1';
                                        end if;
                                        cmd_data_i    <= cmd_outof_fifo;
                                        iteration     <= "0000001";
                                        frequency     <= (others => '0');
                                    -- >1 frame message -> keep reading
                                    when 2|3|4 =>
                                        state <= read_from_fifo;
                                        cmd_reg(0) <= cmd_outof_fifo;
                                        fifo_rd_en <= '1';
                                        sync_cmd_type <= '0';
                                        az_cmd_type <= '0'; --will assert next cycle if AZ
                                        pointer <= "00001";
                                        rst_ready <= '0';
                                        cmd_ready <= '0';
                                        cmd_data_i <= cmd_outof_fifo;
                                        iteration <= "0000001";
                                        frequency   <= (others => '0');
                                    when others =>  --special sequences (not in the RD53 Manuals): F/W CalPulse+trigger, enAZSeq
                                        if( isCalTrigSeq(cmd_outof_fifo(15 downto 12)) ) then
                                            -- =1 frame message -> ready to send. This message is only used to define parameters (En, frequency, #iteration) for the sequence retrieved from memory. This message will not be sent to the chip
                                            state <= send_command;
                                            ReadAddrCalTrigSeq <= "00000";
                                            cmd_reg <= (others => (others => '0'));
                                            fifo_rd_en <= '0';     --do not read as the data is internally generated
                                            sync_cmd_type <= '0';
                                            az_cmd_type <= '0';
                                            pointer <= "11111";
                                            rst_ready <= '0';
                                            cmd_ready <= '0';
                                            cmd_data_i <= (others=>'0');
                                            iteration  <= cmd_outof_fifo(11 downto 5);
                                            frequency <= cmd_outof_fifo(4 downto 0);
                                        elsif( isAZSeq(cmd_outof_fifo(15 downto 13), RD53Version ) ) then
                                            -- =1 frame message -> wait before read. Only 4 msb are used to activate/deactivate the AZ module. This message will not be sent to the chip
                                            -- enabling of AZ module will be preserved until a new enAZSeq will be issued or reset
                                            state         <= wait_before_read;
                                            cmd_reg       <= (others => (others => '0'));
                                            fifo_rd_en    <= '1';
                                            sync_cmd_type <= '0';
                                            az_cmd_type   <= '0';
                                            if(cmd_outof_fifo(12) = '1') then
                                                enAZ_viacmd   <= '1';
                                            else
                                                enAZ_viacmd   <= '0';
                                            end if;
                                            pointer       <= "00000";
                                            rst_ready <= '0';
                                            cmd_ready     <= '0';
                                            cmd_data_i    <= (others => '0');
                                            iteration     <= (others => '0');
                                            frequency     <= (others => '0');
                                        else
                                            state <= read_from_fifo;
                                            cmd_reg(0) <= cmd_outof_fifo;
                                            fifo_rd_en <= '1';
                                            sync_cmd_type <= '0';
                                            az_cmd_type <= '0';
                                            pointer <= "00000"; --do not advance
                                            rst_ready <= '0';
                                            cmd_ready <= '0';
                                            cmd_data_i <= cmd_outof_fifo;
                                            iteration <= "0000001";
                                        end if;
                                end case; --numFrameCommand(cmd_reg(0), RD53Version)

                            elsif (pointer = "00001") then
                                rst_ready <= '0';
                                case numFrameCommand(cmd_reg(0), RD53Version) is
                                    --(2-1)x16b message left -> ready to send
                                    when 2 =>
                                        state <= send_command;
                                        cmd_reg(1) <= cmd_outof_fifo;
                                        fifo_rd_en <= '0';
                                        sync_cmd_type <= '0';
                                        if ( isAZPulse(cmd_outof_fifo, RD53Version) ) then
                                            az_cmd_type <= '1';
                                        else
                                            az_cmd_type <= '0';
                                        end if;
                                        pointer <= "00001";
                                        rst_ready <= '0';
                                        cmd_ready <= '1';
                                        cmd_data_i <= cmd_reg(0);
                                        iteration <= "0000001";
                                        frequency   <= (others => '0');
                                    -- >(2-1)x16b messages left -> keep reading
                                    when 3|4 =>
                                        state <= read_from_fifo;
                                        cmd_reg(1) <= cmd_outof_fifo;
                                        fifo_rd_en <= '1';
                                        sync_cmd_type <= '0';
                                        az_cmd_type <= '0';
                                        pointer <= "00010";
                                        cmd_ready <= '0';
                                        cmd_data_i <= cmd_reg(0);
                                        iteration <= "0000001";
                                        frequency   <= (others => '0');
                                    when others => --never happens
                                end case; --numFrameCommand(cmd_reg(0), RD53Version)
                            elsif (pointer = "00010") then
                                rst_ready <= '0';
                                case numFrameCommand(cmd_reg(0), RD53Version) is
                                    --(3-2)x16b message left -> ready to send
                                    when 3 =>
                                        state <= send_command;
                                        cmd_reg(2) <= cmd_outof_fifo;
                                        fifo_rd_en <= '0';
                                        sync_cmd_type <= '0';
                                        az_cmd_type <= '0';
                                        pointer <= "00010";
                                        cmd_ready <= '1';
                                        cmd_data_i <= cmd_reg(0);
                                        iteration <= "0000001";
                                        frequency   <= (others => '0');
                                    -- >(3-2)x16b message left -> keep reading
                                    when 4 =>
                                        state <= read_from_fifo;
                                        cmd_reg(2) <= cmd_outof_fifo;
                                        fifo_rd_en <= '1';
                                        sync_cmd_type <= '0';
                                        az_cmd_type <= '0';
                                        pointer <= "00011";
                                        cmd_ready <= '0';
                                        cmd_data_i <= cmd_reg(0);
                                        iteration <= "0000001";
                                        frequency   <= (others => '0');
                                    when others => --never happens
                                end case; --numFrameCommand(cmd_reg(0), RD53Version)
                            elsif (pointer = "00011") then
                                rst_ready <= '0';
                                case numFrameCommand(cmd_reg(0), RD53Version) is
                                    --(4-3)x16b message left -> ready to send
                                    when 4 =>
                                        state <= send_command;
                                        cmd_reg(3) <= cmd_outof_fifo;
                                        fifo_rd_en <= '0';
                                        sync_cmd_type <= '0';
                                        az_cmd_type <= '0';
                                        pointer <= "00011";
                                        cmd_ready <= '1';
                                        cmd_data_i <= cmd_reg(0);
                                        iteration <= "0000001";
                                        frequency   <= (others => '0');
                                    when others =>
                                end case; --numFrameCommand(cmd_reg(0), RD53Version)
                            end if; --if(pointer = "00000")
                        else
                            state         <= state;
                            fifo_rd_en    <= fifo_rd_en;
                            cmd_reg       <= cmd_reg;
                            rst_ready     <= '0';
                            cmd_ready     <= '0';
                            cmd_data_i    <= cmd_data_i;
                            sync_cmd_type <= '0';
                            az_cmd_type   <= '0';
                            pointer       <= pointer;
                            iteration     <= iteration;
                            frequency     <= frequency;
                        end if; --if(cmd_valid = '1' and fifo_rd_en = '1')

                    when send_command =>
                        if(counter = "01") then
                            if (cmd_sent = '1' or freq_en = '1') then
                                if (pointer = "00000") then
                                    if(cmd_valid = '1') then
                                        state <= read_from_fifo;
                                        fifo_rd_en <= '1';
                                    else
                                        state <= wait_before_read;
                                        fifo_rd_en <= '0';
                                    end if;
                                    cmd_reg <= (others =>(others=> '0'));
                                    rst_ready <= '0';
                                    cmd_ready <= '0';
                                    cmd_data_i  <= (others=>'0');
                                    sync_cmd_type <= '0';
                                    az_cmd_type <= '0';
                                    pointer <= "00000";
                                    ReadAddrCalTrigSeq <= (others=>'0');
                                    iteration <= (others => '0');
                                    frequency   <= (others => '0');
                                else
                                    if (cmd_out_cnts = pointer and iteration = "0000001") then
                                        if(cmd_valid = '1') then
                                            state <= read_from_fifo;
                                            fifo_rd_en <= '1';
                                        else
                                            state <= wait_before_read;
                                            fifo_rd_en <= '0';
                                        end if;
                                        cmd_reg <= (others =>(others=> '0'));
                                        rst_ready <= '0';
                                        cmd_ready <= '0';
                                        cmd_data_i <= (others=>'0');
                                        sync_cmd_type <= '0';
                                        az_cmd_type <= '0';
                                        pointer  <= "00000";
                                        cmd_out_cnts <= (others=>'0');
                                        ReadAddrCalTrigSeq <= (others=>'0');
                                        iteration <= (others=>'0');
                                        frequency   <= frequency;
                                    elsif (cmd_out_cnts = pointer) then
                                        state <= send_command;
                                        fifo_rd_en <= '0';
                                        cmd_reg <= cmd_reg;
                                        rst_ready <= '0';
                                        cmd_ready <= '0';
                                        cmd_data_i <= (others=>'0');
                                        sync_cmd_type <= '0';
                                        az_cmd_type <= '0';
                                        pointer <= pointer;
                                        cmd_out_cnts <= (others=>'0');
                                        ReadAddrCalTrigSeq <= (others=>'0');
                                        iteration <= iteration - "0000001";
                                        frequency   <= frequency;
                                    else
                                        state <= send_command;
                                        fifo_rd_en <= '0';
                                        cmd_reg <= cmd_reg;
                                        rst_ready <= '0';
                                        cmd_ready <= '1';
                                        if (frequency /= "00000") then
                                            cmd_data_i <= CalTrigSeq_in;
                                            ReadAddrCalTrigSeq <= ReadAddrCalTrigSeq + "00001";
                                            if(CalTrigSeq_in = Sync) then
                                                sync_cmd_type <= '1';
                                            else
                                                sync_cmd_type <= '0';
                                            end if;
                                        else
                                            cmd_data_i <= cmd_reg(to_integer(unsigned(cmd_out_cnts+1)));
                                            ReadAddrCalTrigSeq <= (others => '0');
                                            sync_cmd_type <= '0';
                                        end if;
                                        pointer <= pointer;
                                        cmd_out_cnts <= cmd_out_cnts + "00001";
                                        iteration <= iteration;
                                        az_cmd_type <= '0';
                                        frequency   <= frequency;
                                    end if; -- if (cmd_out_cnts = pointer and iteration = "0000001")
                                end if; --if (pointer = "00000")
                            end if; --if (cmd_sent = '1' or freq_en = '1')
                        end if; --if (counter = 01)
                end case; --the state machine
            end if; --reset
        end if; --clock

    end process;


    RD53A_CommandErrors_i: entity work.RD53A_CommandErrors
        generic map(
            RD53Version => RD53Version
        )
        port map (
            rst                  => rst,
            clk                  => clk40,
            data_in              => cmd_outof_fifo,
            datav_in             => cmd_valid and fifo_rd_en,
            is_read_from_fifo_in => is_read_from_fifo,
            pointer_in           => pointer,
            err_out              => err_cmdtop_out,
            warn_out             => warn_cmdtop_out
        );

end Behavioral;

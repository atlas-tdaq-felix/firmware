--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.numeric_std_unsigned.ALL;

library UNISIM;
    use UNISIM.VCOMPONENTS.all;
    use work.rd53_package.ALL;

entity RD53A_MsrGenCalToTrigDly is
    port (
        rst         : in std_logic;
        clk       : in std_logic;
        --    counter_in     : in std_logic_vector(1 downto 0);
        data_in     : in std_logic_vector(15 downto 0);
        datav_in    : in std_logic;
        ref_dly_genCalTrig_in : in std_logic_vector(7 downto 0);
        err_genCalTrig_dly_out : out std_logic_vector(7 downto 0);
        --ila
        cnt_genCalTrig_dly_ila : out std_logic_vector(7 downto 0);
        cnt_genCal_fromSM_ila       : out std_logic_vector(31 downto 0);
        state_chkdly_genCalTrig_ila : out std_logic_vector(1 downto 0);
        cnt_datav_fromCal_ila : out std_logic_vector(7 downto 0)
    );
end RD53A_MsrGenCalToTrigDly;

architecture Behavioral of RD53A_MsrGenCalToTrigDly is


    type state_chkdly_genCalTrig_type is (st_wait_genCalTrig,
        st_cntdly_genCalTrig,
        st_wait_tofinish
    );

    signal state_chkdly_genCalTrig: state_chkdly_genCalTrig_type := st_wait_genCalTrig;

    signal cnt_genCalTrig_dly : std_logic_vector(7 downto 0);
    signal err_genCalTrig_dly : std_logic_vector(7 downto 0); --increase if cnt_dly doesn't match reference, saturates at 255
    signal cnt_genCal_fromSM : std_logic_vector(31 downto 0); --cnt number of
    --gencal as seen
    --by SM, sanity check
    signal cnt_datav_fromCal : std_logic_vector(7 downto 0);


begin

    state_chkdly_genCalTrig_ila <= "00" when state_chkdly_genCalTrig = st_wait_genCalTrig else
                                   "01" when state_chkdly_genCalTrig = st_cntdly_genCalTrig else
                                   "10";
    cnt_genCalTrig_dly_ila <= cnt_genCalTrig_dly;
    err_genCalTrig_dly_out <= err_genCalTrig_dly;
    cnt_genCal_fromSM_ila <= cnt_genCal_fromSM;
    cnt_datav_fromCal_ila <= cnt_datav_fromCal;

    chck_dly_genCalTrig: process(clk)
    begin
        if rising_edge(clk) then
            if (rst = '1') then
                state_chkdly_genCalTrig  <= st_wait_genCalTrig;
                cnt_genCalTrig_dly       <= (others => '0');
                err_genCalTrig_dly       <= (others => '0');
                cnt_genCal_fromSM        <= (others => '0');
                cnt_datav_fromCal        <= (others => '0');
            elsif (datav_in = '1') then
                case state_chkdly_genCalTrig is
                    when st_wait_genCalTrig =>
                        err_genCalTrig_dly <= err_genCalTrig_dly;
                        if (data_in = CalA) then
                            state_chkdly_genCalTrig <= st_cntdly_genCalTrig;
                            if(cnt_genCalTrig_dly /= x"FF") then
                                cnt_genCalTrig_dly <= cnt_genCalTrig_dly + x"01"; --counting from [cal to trig[
                            end if;
                            if(cnt_genCal_fromSM /= x"FFFFFFFF") then
                                cnt_genCal_fromSM <= cnt_genCal_fromSM + x"00000001";
                            end if;
                            cnt_datav_fromCal       <= x"01";
                        else
                            state_chkdly_genCalTrig <= st_wait_genCalTrig;
                            cnt_genCalTrig_dly <= (others => '0');
                            cnt_genCal_fromSM <= cnt_genCal_fromSM;
                            cnt_datav_fromCal       <= (others => '0');
                        end if;

                    when st_cntdly_genCalTrig =>
                        cnt_genCal_fromSM <= cnt_genCal_fromSM;
                        cnt_datav_fromCal <= cnt_datav_fromCal + x"01";
                        if( cnt_genCalTrig_dly = x"FF" or           --waited too long...trigger won't come
               (data_in(15 downto 8) = Trigger_01 or
                data_in(15 downto 8) = Trigger_02 or
                data_in(15 downto 8) = Trigger_03 or
                data_in(15 downto 8) = Trigger_04 or
                data_in(15 downto 8) = Trigger_05 or
                data_in(15 downto 8) = Trigger_06 or
                data_in(15 downto 8) = Trigger_07 or
                data_in(15 downto 8) = Trigger_08 or
                data_in(15 downto 8) = Trigger_09 or
                data_in(15 downto 8) = Trigger_10 or
                data_in(15 downto 8) = Trigger_11 or
                data_in(15 downto 8) = Trigger_12 or
                data_in(15 downto 8) = Trigger_13 or
                data_in(15 downto 8) = Trigger_14 or
                data_in(15 downto 8) = Trigger_15)) then

                            --check only if trigger arrived
                            if(cnt_genCalTrig_dly /= ref_dly_genCalTrig_in and err_genCalTrig_dly /= x"3F") then
                                err_genCalTrig_dly <= err_genCalTrig_dly + x"01";
                            end if;

                            cnt_genCalTrig_dly <= (others => '0');
                            state_chkdly_genCalTrig   <= st_wait_tofinish; --st_wait_genCalTrig;



                        else
                            err_genCalTrig_dly <= err_genCalTrig_dly;
                            if(cnt_genCalTrig_dly /= x"FF") then
                                cnt_genCalTrig_dly <= cnt_genCalTrig_dly + x"01";
                            end if;
                            state_chkdly_genCalTrig   <= st_cntdly_genCalTrig;
                        end if;

                    when st_wait_tofinish =>
                        cnt_genCalTrig_dly       <= (others => '0');
                        err_genCalTrig_dly       <= err_genCalTrig_dly;
                        cnt_genCal_fromSM        <= cnt_genCal_fromSM;
                        --allow for the calpulse+trigger+(rearm) to complete
                        --assumption: m_trigWord[15] = 0x69696363; so 15*32b=30*16b to complete the sequence (see Rd53ATriggerLoop.cpp)
                        if(cnt_datav_fromCal /= x"1E") then
                            state_chkdly_genCalTrig  <= st_wait_tofinish;
                            cnt_datav_fromCal         <= cnt_datav_fromCal + x"01";
                        else
                            state_chkdly_genCalTrig  <= st_wait_genCalTrig;
                            cnt_datav_fromCal        <= (others => '0');
                        end if;
                end case; --the state machine
            end if; --reset
        end if; --clock

    end process;

end Behavioral;

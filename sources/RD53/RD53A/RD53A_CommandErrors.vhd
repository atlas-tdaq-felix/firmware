--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.numeric_std_unsigned.ALL;

library UNISIM;
    use UNISIM.VCOMPONENTS.all;
    use work.rd53_package.ALL;

entity RD53A_CommandErrors is
    Generic (
        RD53Version : String := "A" --A or B
    );

    port (
        rst                  : in std_logic;
        clk                  : in std_logic;
        data_in              : in std_logic_vector(15 downto 0);
        datav_in             : in std_logic;
        is_read_from_fifo_in : in std_logic;
        pointer_in           : in std_logic_vector(4 downto 0);
        err_out              : out std_logic_vector(7 downto 0);
        warn_out             : out std_logic_vector(7 downto 0)

    );
end RD53A_CommandErrors;

architecture Behavioral of RD53A_CommandErrors is


-----------------------------------------------------------------
--count errors/warnings in commands towards RD53A or B based on the manuals.
--Only checking 16b at the time
--Functionality can be expanded by checking the actual sequence of commands
-----------------------------------------------------------------

begin

    process(clk)
        variable err_i : std_logic_vector(7 downto 0) := (others=>'0');
        variable warn_i : std_logic_vector(7 downto 0) := (others=>'0');
    begin
        if rising_edge(clk) then
            if (rst = '1') then
                err_i := (others => '0');
                warn_i := (others => '0');
            else
                if(is_read_from_fifo_in = '1' and datav_in = '1') then
                    if( pointer_in = "00000" and (isCommand(data_in, RD53Version) = '0' and isTrigger(data_in(15 downto 8)) = False and isCalTrigSeq(data_in(15 downto 12)) = False )) then
                        if(err_i /= x"FF") then
                            err_i := err_i + x"01";
                        end if;
                    elsif( pointer_in /= "00000" and ((isData(data_in(15 downto 8), RD53Version) = '0' or isData(data_in(7 downto 0), RD53Version) = '0')) ) then
                        if(err_i /= x"FF") then
                            err_i := err_i + x"01";
                        end if;
                    else
                        err_i := err_i;
                    end if; --if( pointer_in = "00000" and ...
                end if; --if(is_read_from_fifo_in = '1' and datav_in = '1')

                if( is_read_from_fifo_in = '1' and datav_in = '0' and pointer_in /= "00000") then
                    if(warn_i /= x"FF") then
                        warn_i := warn_i + x"01";
                    end if;
                else
                    warn_i := warn_i;
                end if; --if( is_read_from_fifo_in = '1'
            end if; --if (rst = '1')
        end if; --if rising_edge(clk40)
        err_out <= err_i;
        warn_out <= warn_i;
    end process;

end Behavioral;

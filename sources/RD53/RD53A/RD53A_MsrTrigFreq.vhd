--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.numeric_std_unsigned.ALL;

library UNISIM;
    use UNISIM.VCOMPONENTS.all;
    use work.rd53_package.ALL;

entity RD53A_MsrTrigFreq is
    port (
        rst                          : in std_logic;
        clk                        : in std_logic;
        data_in                      : in std_logic_vector(15 downto 0);
        datav_in                     : in std_logic;
        --    counter_in                   : in std_logic_vector( 1 downto 0);

        cnt_time_firstTolastTrig_out : out std_logic_vector(31 downto 0);
        --debug
        state_trigTime_ila           :  out std_logic_vector(1 downto 0);
        cnt_time_twoTrig_ila         : out std_logic_vector(31 downto 0);
        cnt_trig_ila                 : out std_logic_vector(31 downto 0)
    --
    );
end RD53A_MsrTrigFreq;

architecture Behavioral of RD53A_MsrTrigFreq is

    --cnt_time_firstTolastTrig = 40 MHz # clocks between first and last trigger train issued.
    --cnt_trig_cmd = # triggers issued via command
    --Ntrig_pertrain = # triggers per train (known from YARR SW)
    --<frequency>=((cnt_trig_cmd/Ntrig_pertrain)/(cnt_time_firstTolastTrig*(25*4)))*10^6 [KHZ] can be derived

    signal cnt_trig : std_logic_vector(31 downto 0);
    signal cnt_time_firstTolastTrig : std_logic_vector(31 downto 0);
    signal cnt_time_twoTrig : std_logic_vector(31 downto 0);
    type state_trigTime_type is (st_trigTime_wait,
        st_trigTime_count,
        st_trigTime_done);
    signal state_trigTime : state_trigTime_type := st_trigTime_wait;



begin

    process(clk)
    begin
        if rising_edge (clk) then
            if(rst = '1') then
                cnt_time_firstTolastTrig <= (others => '0');
                cnt_time_twoTrig <= (others => '0');
                cnt_trig     <= (others => '0');
                state_trigTime <= st_trigTime_wait;
                cnt_time_firstTolastTrig_out <= (others => '0');
            --      elsif (counter = "01") then
            elsif (datav_in = '1') then
                case state_trigTime is
                    when st_trigTime_wait =>
                        if (data_in(15 downto 8) = Trigger_01 or
                data_in(15 downto 8) = Trigger_02 or
                data_in(15 downto 8) = Trigger_03 or
                data_in(15 downto 8) = Trigger_04 or
                data_in(15 downto 8) = Trigger_05 or
                data_in(15 downto 8) = Trigger_06 or
                data_in(15 downto 8) = Trigger_07 or
                data_in(15 downto 8) = Trigger_08 or
                data_in(15 downto 8) = Trigger_09 or
                data_in(15 downto 8) = Trigger_10 or
                data_in(15 downto 8) = Trigger_11 or
                data_in(15 downto 8) = Trigger_12 or
                data_in(15 downto 8) = Trigger_13 or
                data_in(15 downto 8) = Trigger_14 or
                data_in(15 downto 8) = Trigger_15) then
                            cnt_time_firstTolastTrig         <= (others => '0');
                            cnt_time_twoTrig <= (others => '0');
                            cnt_trig         <= cnt_trig + x"00000001";
                            state_trigTime   <= st_trigTime_count;
                        else
                            cnt_time_firstTolastTrig         <= (others => '0');
                            cnt_time_twoTrig <= (others => '0');
                            cnt_trig         <= cnt_trig;
                            state_trigTime   <= st_trigTime_wait;
                        end if;

                    when st_trigTime_count =>
                        if (data_in(15 downto 8) = Trigger_01 or
                data_in(15 downto 8) = Trigger_02 or
                data_in(15 downto 8) = Trigger_03 or
                data_in(15 downto 8) = Trigger_04 or
                data_in(15 downto 8) = Trigger_05 or
                data_in(15 downto 8) = Trigger_06 or
                data_in(15 downto 8) = Trigger_07 or
                data_in(15 downto 8) = Trigger_08 or
                data_in(15 downto 8) = Trigger_09 or
                data_in(15 downto 8) = Trigger_10 or
                data_in(15 downto 8) = Trigger_11 or
                data_in(15 downto 8) = Trigger_12 or
                data_in(15 downto 8) = Trigger_13 or
                data_in(15 downto 8) = Trigger_14 or
                data_in(15 downto 8) = Trigger_15) then
                            if (cnt_time_twoTrig < x"FF000000") then
                                cnt_time_firstTolastTrig <= cnt_time_firstTolastTrig + cnt_time_twoTrig + x"00000001";
                            end if;
                            cnt_time_twoTrig <= (others => '0');
                            if ( cnt_trig /= x"FFFFFFFF") then
                                cnt_trig <= cnt_trig + x"00000001";
                            end if;
                            state_trigTime <= st_trigTime_count;
                        else
                            cnt_time_firstTolastTrig <= cnt_time_firstTolastTrig;
                            cnt_trig <= cnt_trig;
                            if (cnt_time_twoTrig < x"00FFFFFF") then --1.7x10^7 x 100 ns ~1.7sec
                                cnt_time_twoTrig <= cnt_time_twoTrig + x"00000001";
                                state_trigTime <= st_trigTime_count;
                            else --done waiting. Probably done with triggers
                                cnt_time_twoTrig <= (others => '0');
                                state_trigTime <= st_trigTime_done;
                            end if;
                        end if;
                    when st_trigTime_done =>
                        cnt_time_firstTolastTrig <= cnt_time_firstTolastTrig;
                        cnt_time_twoTrig <= (others => '0');
                        cnt_trig     <= cnt_trig;
                        state_trigTime <= st_trigTime_wait;
                        cnt_time_firstTolastTrig_out <= cnt_time_firstTolastTrig;
                end case;
            else
                cnt_time_firstTolastTrig <= cnt_time_firstTolastTrig;
                cnt_time_twoTrig <= cnt_time_twoTrig;
                cnt_trig <= cnt_trig;
                state_trigTime <= state_trigTime;
            end if; --if(rst = '1')
        end if; --if rising_edge (clk)
    end process;
    state_trigTime_ila <= "00" when state_trigTime = st_trigTime_wait else
                          "01" when state_trigTime = st_trigTime_count else
                          "10";

    --cnt_time_firstTolastTrig_out <= cnt_time_firstTolastTrig when state_trigTime = st_trigTime_done;

    cnt_time_twoTrig_ila <= cnt_time_twoTrig;
    cnt_trig_ila <= cnt_trig;

end Behavioral;

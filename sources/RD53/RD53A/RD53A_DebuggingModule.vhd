--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.numeric_std_unsigned.ALL;

library UNISIM;
    use UNISIM.VCOMPONENTS.all;


entity RD53A_DebuggingModule is
    port (
        rst                          : in std_logic;
        clk40                        : in std_logic;

        data_in                      : in std_logic_vector(15 downto 0);
        datav_in                     : in std_logic;
        ref_cmd_in                   : in std_logic_vector(15 downto 0);
        cnt_cmd_out                  : out std_logic_vector(31 downto 0);
        cnt_trig_cmd_out             : out std_logic_vector(31 downto 0);
        cnt_time_firstTolastTrig_out : out std_logic_vector(31 downto 0);
        ref_dly_genCalTrig_in        : in std_logic_vector(7 downto 0);
        err_genCalTrig_dly_out       : out std_logic_vector(7 downto 0);

        --ila
        state_trigTime_ila           : out std_logic_vector(1 downto 0);
        cnt_time_twoTrig_ila         : out std_logic_vector(31 downto 0);
        cnt_trig_ila                 : out std_logic_vector(31 downto 0);
        cnt_genCalTrig_dly_ila       : out std_logic_vector(7 downto 0);
        cnt_genCal_fromSM_ila        : out std_logic_vector(31 downto 0);
        state_chkdly_genCalTrig_ila  : out std_logic_vector(1 downto 0);
        cnt_datav_fromCal_ila : out std_logic_vector(7 downto 0)


    );
end RD53A_DebuggingModule;

architecture Behavioral of RD53A_DebuggingModule is

--1) count # of issued triggers

--2) Trigger frequency

--3) compute distance between first trigger and genCal and compare it with a
--user defined reference. If that doesn't match increase error counters




begin

    RD53A_CntIssuedGivenCommand_i : entity work.RD53A_CntIssuedGivenCommand
        port map (
            rst              => rst,
            clk              => clk40,
            data_in          => data_in,
            datav_in         => datav_in,
            ref_cmd_in       => ref_cmd_in,
            cnt_cmd_out      => cnt_cmd_out
        );

    RD53A_CntIssuedTrigger_i : entity work.RD53A_CntIssuedTrigger port map (
            rst              => rst,
            clk              => clk40,
            data_in          => data_in,
            datav_in         => datav_in,
            cnt_trig_cmd_out => cnt_trig_cmd_out
        );

    RD53A_MsrTrigFreq_i : entity work.RD53A_MsrTrigFreq port map (
            rst                          => rst,
            clk                          => clk40,
            data_in                      => data_in,
            datav_in                    => datav_in,
            cnt_time_firstTolastTrig_out => cnt_time_firstTolastTrig_out,
            --debug
            state_trigTime_ila           => state_trigTime_ila,
            cnt_time_twoTrig_ila         => cnt_time_twoTrig_ila,
            cnt_trig_ila                 => cnt_trig_ila
        --
        );

    RD53A_MsrGenCalToTrigDly_i : entity work.RD53A_MsrGenCalToTrigDly port map (
            rst                         => rst,
            clk                         => clk40,
            data_in                     => data_in,
            datav_in                    => datav_in,
            ref_dly_genCalTrig_in       => ref_dly_genCalTrig_in,
            err_genCalTrig_dly_out      => err_genCalTrig_dly_out,
            --ila
            cnt_genCalTrig_dly_ila      => cnt_genCalTrig_dly_ila,
            cnt_genCal_fromSM_ila       => cnt_genCal_fromSM_ila,
            state_chkdly_genCalTrig_ila => state_chkdly_genCalTrig_ila,
            cnt_datav_fromCal_ila       => cnt_datav_fromCal_ila
        );



end Behavioral;

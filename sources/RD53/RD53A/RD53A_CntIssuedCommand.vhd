--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.numeric_std_unsigned.ALL;

library UNISIM;
    use UNISIM.VCOMPONENTS.all;

    use work.rd53_package.ALL;

entity RD53A_CntIssuedCommand is
    port (
        rst              : in std_logic;
        clk            : in std_logic;
        data_in          : in std_logic_vector(15 downto 0);
        datav_in         : in std_logic;
        cnt_ecr_out      : out std_logic_vector(31 downto 0);
        cnt_bcr_out      : out std_logic_vector(31 downto 0);
        cnt_glbpls_out   : out std_logic_vector(31 downto 0);
        cnt_cal_out      : out std_logic_vector(31 downto 0);
        cnt_wrreg_out    : out std_logic_vector(31 downto 0);
        cnt_rdreg_out    : out std_logic_vector(31 downto 0);
        cnt_noop_out     : out std_logic_vector(31 downto 0);
        cnt_sync_out     : out std_logic_vector(31 downto 0)


    );
end RD53A_CntIssuedCommand;

architecture Behavioral of RD53A_CntIssuedCommand is


    -----------------------------------------------------------------
    --count # issued command matching the reference.
    -----------------------------------------------------------------

    --# expected: TO DO

    signal cnt_ecr: std_logic_vector(31 downto 0);
    signal cnt_bcr: std_logic_vector(31 downto 0);
    signal cnt_glbpls: std_logic_vector(31 downto 0);
    signal cnt_cal: std_logic_vector(31 downto 0);
    signal cnt_wrreg: std_logic_vector(31 downto 0);
    signal cnt_rdreg: std_logic_vector(31 downto 0);
    signal cnt_noop: std_logic_vector(31 downto 0);
    signal cnt_sync: std_logic_vector(31 downto 0);


begin

    process(clk)
    begin
        if rising_edge (clk) then
            if(rst = '1') then
                cnt_ecr    <= (others => '0');
                cnt_bcr    <= (others => '0');
                cnt_glbpls <= (others => '0');
                cnt_cal    <= (others => '0');
                cnt_wrreg  <= (others => '0');
                cnt_rdreg  <= (others => '0');
                cnt_noop   <= (others => '0');
                cnt_sync   <= (others => '0');

            else
                if (datav_in = '1' and data_in = ECRA and cnt_ecr /= x"0FFFFFFF") then
                    cnt_ecr <= cnt_ecr + x"00000001";
                end if;
                if (datav_in = '1' and data_in = BCRA and cnt_bcr /= x"0FFFFFFF") then
                    cnt_bcr <= cnt_bcr + x"00000001";
                end if;
                if (datav_in = '1' and data_in = GlobalPulseA and cnt_glbpls /= x"0FFFFFFF") then
                    cnt_glbpls <= cnt_glbpls + x"00000001";
                end if;
                if (datav_in = '1' and data_in = CalA and cnt_cal /= x"0FFFFFFF") then
                    cnt_cal <= cnt_cal + x"00000001";
                end if;
                if (datav_in = '1' and data_in = WrRegA and cnt_wrreg /= x"0FFFFFFF") then
                    cnt_wrreg <= cnt_wrreg + x"00000001";
                end if;
                if (datav_in = '1' and data_in = RdRegA and cnt_rdreg /= x"0FFFFFFF") then
                    cnt_rdreg <= cnt_rdreg + x"00000001";
                end if;
                if (datav_in = '1' and data_in = NoopA and cnt_noop /= x"0FFFFFFF") then
                    cnt_noop <= cnt_noop + x"00000001";
                end if;
                if (datav_in = '1' and data_in = Sync and cnt_sync /= x"0FFFFFFF") then
                    cnt_sync <= cnt_sync + x"00000001";
                end if;
            end if; --if(rst = '1')
        end if; --clock
    end process;
    cnt_ecr_out    <= cnt_ecr;
    cnt_bcr_out    <= cnt_bcr;
    cnt_glbpls_out <= cnt_glbpls;
    cnt_cal_out    <= cnt_cal;
    cnt_wrreg_out  <= cnt_wrreg;
    cnt_rdreg_out  <= cnt_rdreg;
    cnt_noop_out   <= cnt_noop;
    cnt_sync_out   <= cnt_sync;


end Behavioral;

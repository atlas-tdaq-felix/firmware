--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.numeric_std_unsigned.ALL;

library UNISIM;
    use UNISIM.VCOMPONENTS.all;

    use work.rd53_package.ALL;

entity RD53A_CntIssuedCommand8b is
    port (
        rst              : in std_logic;
        clk            : in std_logic;
        data_in          : in std_logic_vector(7 downto 0);
        datav_in         : in std_logic;
        cnt_ecr_out      : out std_logic_vector(31 downto 0);
        cnt_bcr_out      : out std_logic_vector(31 downto 0);
        cnt_glbpls_out   : out std_logic_vector(31 downto 0);
        cnt_cal_out      : out std_logic_vector(31 downto 0);
        cnt_wrreg_out    : out std_logic_vector(31 downto 0);
        cnt_rdreg_out    : out std_logic_vector(31 downto 0);
        cnt_noop_out     : out std_logic_vector(31 downto 0);
        cnt_sync_out     : out std_logic_vector(31 downto 0)


    );
end RD53A_CntIssuedCommand8b;

architecture Behavioral of RD53A_CntIssuedCommand8b is


    -----------------------------------------------------------------
    --8b->16b gearbox, then count # issued command matching the reference.
    -----------------------------------------------------------------

    signal data16b : std_logic_vector(15 downto 0);
    signal data16bv : std_logic;

begin

    process(clk)
        variable count : std_logic;
    begin
        if rising_edge(clk) then
            if(rst = '1') then
                data16b <= (others => '0');
                count := '0';
                data16bv <= '0';
            elsif(datav_in = '1') then
                if(count = '0') then
                    data16b(15 downto 8) <= data_in(7 downto 0);
                    data16b(7 downto 0) <= (others => '0');
                    data16bv <= '0';
                    count := '1';
                else
                    data16b(7 downto 0) <= data_in(7 downto 0);
                    data16bv <= '1';
                    count := '0';
                end if;
            else
                data16bv <= '0';
            end if;
        end if;  --clock
    end process;


    RD53A_CntIssuedCommand_i : entity work.RD53A_CntIssuedCommand
        port map (
            rst              => rst,
            clk              => clk,
            data_in          => data16b,
            datav_in         => data16bv,
            cnt_ecr_out      => cnt_ecr_out      ,
            cnt_bcr_out      => cnt_bcr_out      ,
            cnt_glbpls_out   => cnt_glbpls_out   ,
            cnt_cal_out      => cnt_cal_out      ,
            cnt_wrreg_out    => cnt_wrreg_out    ,
            cnt_rdreg_out    => cnt_rdreg_out    ,
            cnt_noop_out     => cnt_noop_out     ,
            cnt_sync_out     => cnt_sync_out


        );




end Behavioral;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
    use ieee.std_logic_1164.all;
    use IEEE.numeric_std_unsigned.ALL;


package rd53_package is


    type array32_std_logic_vector_7 is array (0 to 31) of std_logic_vector(7 downto 0);
    type array64_std_logic_vector_7 is array (0 to 63) of std_logic_vector(7 downto 0);



    --RD53A/B see manuals
    constant HDR_RD53A_7b  : std_logic_vector(6 downto 0) := "0000001";

    constant DATA_00  : STD_LOGIC_VECTOR(7 downto 0):= X"6A";
    constant DATA_01  : STD_LOGIC_VECTOR(7 downto 0):= X"6C";
    constant DATA_02  : STD_LOGIC_VECTOR(7 downto 0):= X"71";
    constant DATA_03  : STD_LOGIC_VECTOR(7 downto 0):= X"72";
    constant DATA_04  : STD_LOGIC_VECTOR(7 downto 0):= X"74";
    constant DATA_05  : STD_LOGIC_VECTOR(7 downto 0):= X"8B";
    constant DATA_06  : STD_LOGIC_VECTOR(7 downto 0):= X"8D";
    constant DATA_07  : STD_LOGIC_VECTOR(7 downto 0):= X"8E";
    constant DATA_08  : STD_LOGIC_VECTOR(7 downto 0):= X"93";
    constant DATA_09  : STD_LOGIC_VECTOR(7 downto 0):= X"95";
    constant DATA_10  : STD_LOGIC_VECTOR(7 downto 0):= X"96";
    constant DATA_11  : STD_LOGIC_VECTOR(7 downto 0):= X"99";
    constant DATA_12  : STD_LOGIC_VECTOR(7 downto 0):= X"9A";
    constant DATA_13  : STD_LOGIC_VECTOR(7 downto 0):= X"9C";
    constant DATA_14  : STD_LOGIC_VECTOR(7 downto 0):= X"A3";
    constant DATA_15  : STD_LOGIC_VECTOR(7 downto 0):= X"A5";
    constant DATA_16  : STD_LOGIC_VECTOR(7 downto 0):= X"A6";
    constant DATA_17  : STD_LOGIC_VECTOR(7 downto 0):= X"A9";
    constant DATA_19  : STD_LOGIC_VECTOR(7 downto 0):= X"AC";
    constant DATA_20  : STD_LOGIC_VECTOR(7 downto 0):= X"B1";
    constant DATA_21  : STD_LOGIC_VECTOR(7 downto 0):= X"B2";
    constant DATA_22  : STD_LOGIC_VECTOR(7 downto 0):= X"B4";
    constant DATA_23  : STD_LOGIC_VECTOR(7 downto 0):= X"C3";
    constant DATA_24  : STD_LOGIC_VECTOR(7 downto 0):= X"C5";
    constant DATA_25  : STD_LOGIC_VECTOR(7 downto 0):= X"C6";
    constant DATA_26  : STD_LOGIC_VECTOR(7 downto 0):= X"C9";
    constant DATA_27  : STD_LOGIC_VECTOR(7 downto 0):= X"CA";
    constant DATA_28  : STD_LOGIC_VECTOR(7 downto 0):= X"CC";
    constant DATA_29  : STD_LOGIC_VECTOR(7 downto 0):= X"D1";
    constant DATA_30  : STD_LOGIC_VECTOR(7 downto 0):= X"D2";
    constant DATA_31  : STD_LOGIC_VECTOR(7 downto 0):= X"D4";
    ---RD53A
    constant DATA_18A  : STD_LOGIC_VECTOR(7 downto 0):= X"AA";
    ---RD53B
    constant DATA_18B  : STD_LOGIC_VECTOR(7 downto 0):= X"59";


    constant Trigger_01  : STD_LOGIC_VECTOR(7 downto 0):= X"2B"; --000T
    constant Trigger_02  : STD_LOGIC_VECTOR(7 downto 0):= X"2D"; --00T0
    constant Trigger_03  : STD_LOGIC_VECTOR(7 downto 0):= X"2E"; --00TT
    constant Trigger_04  : STD_LOGIC_VECTOR(7 downto 0):= X"33"; --0T00
    constant Trigger_05  : STD_LOGIC_VECTOR(7 downto 0):= X"35"; --0T0T
    constant Trigger_06  : STD_LOGIC_VECTOR(7 downto 0):= X"36"; --0TT0
    constant Trigger_07  : STD_LOGIC_VECTOR(7 downto 0):= X"39"; --0TTT
    constant Trigger_08  : STD_LOGIC_VECTOR(7 downto 0):= X"3A"; --T000
    constant Trigger_09  : STD_LOGIC_VECTOR(7 downto 0):= X"3C"; --T00T
    constant Trigger_10  : STD_LOGIC_VECTOR(7 downto 0):= X"4B"; --T0T0
    constant Trigger_11  : STD_LOGIC_VECTOR(7 downto 0):= X"4D"; --T0TT
    constant Trigger_12  : STD_LOGIC_VECTOR(7 downto 0):= X"4E"; --TT00
    constant Trigger_13  : STD_LOGIC_VECTOR(7 downto 0):= X"53"; --TT0T
    constant Trigger_14  : STD_LOGIC_VECTOR(7 downto 0):= X"55"; --TTT0
    constant Trigger_15  : STD_LOGIC_VECTOR(7 downto 0):= X"56"; --TTTT

    constant AZ_PULSEA : std_logic_vector(31 downto 0) := x"5C5C6A74"; --RD53A only
    --ad hoc cmd = "1110" & Iteration(7b) & frequency(5b) will trigger Calinj
    --below. N.B: frequency = "000000" not allowed (it will cause freq_en to be
    --always enable)
    constant CalTrigSeqEn   : STD_LOGIC_VECTOR(3 downto 0):="1110";
    constant enAZSeqA        : STD_LOGIC_VECTOR(2 downto 0):="000";  --RD53A only

    ------------RD53A and RD53B command protocols (check manual).------------
    ------------Default values for first 16b or 8b are given------------
    ------------Commands are sorted by expected command size------------

    --RD53A and RD53B
    ---command size: 1 frame=1x16b
    constant Sync          : STD_LOGIC_VECTOR(15 downto 0):="1000000101111110"; -- x817e
    constant idle          : std_logic_vector(15 downto 0):= x"AAAA";
    --RD53A only
    ---1 frame=1x16b
    constant ECRA          : STD_LOGIC_VECTOR(15 downto 0):="0101101001011010"; -- x5a5a
    constant BCRA          : STD_LOGIC_VECTOR(15 downto 0):="0101100101011001"; -- x5959
    constant NoopA         : STD_LOGIC_VECTOR(15 downto 0):="0110100101101001"; -- x6969
    ---2 frames=2x16b
    constant GlobalPulseA  : STD_LOGIC_VECTOR(15 downto 0):="0101110001011100"; -- x5c5c
    ---3 frames=3x16b
    constant RdRegA        : STD_LOGIC_VECTOR(15 downto 0):="0110010101100101"; -- x6565
    constant CalA          : STD_LOGIC_VECTOR(15 downto 0):="0110001101100011"; -- x6363
    ---4 frames=4x16b
    constant WrRegA        : STD_LOGIC_VECTOR(15 downto 0):="0110011001100110"; -- x6666

    --RD53B
    ---1 frame=1x16b
    constant PLLLockB      : STD_LOGIC_VECTOR(15 downto 0):="1010101010101010"; -- xaaaa
    constant ClearB        : STD_LOGIC_VECTOR( 7 downto 0):="01011010";         -- x5a
    constant GlobalPulseB  : STD_LOGIC_VECTOR( 7 downto 0):="01011100";         -- x5c
    ---2 frames=2x16b
    constant ReadTriggerB  : STD_LOGIC_VECTOR( 7 downto 0):="01101001";         -- x69
    constant RdRegB      : STD_LOGIC_VECTOR( 7 downto 0):="01100101";         -- x65
    ---3 frames=3x16b
    constant CalB          : STD_LOGIC_VECTOR( 7 downto 0):="01100011";         -- x63
    ---4 frames=4x16b (will allocate 4 frames also for the broadcast WrReg, which
    ---is 3 frames long)
    constant WrRegB       : STD_LOGIC_VECTOR( 7 downto 0):="01100110";          -- x66
    constant ResetHB      : STD_LOGIC_VECTOR(15 downto 0):="1111111111111111";  -- xFFFF
    constant ResetLB      : STD_LOGIC_VECTOR(15 downto 0):= (others => '0');  -- x0

    function isTrigger (cmd_in : in std_logic_vector(7 downto 0)) return boolean;
    function isData (cmd_in : in std_logic_vector(7 downto 0); RD53Version : String) return std_logic;
    function isCommand (cmd_in : in std_logic_vector(15 downto 0); RD53Version : String) return std_logic;
    function isReset (cmd_in : in std_logic_vector(15 downto 0); RD53Version : String) return boolean;
    function numFrameCommand (cmd_in : in std_logic_vector(15 downto 0); RD53Version : String) return integer;
    function isAZSeq (cmd_in : in std_logic_vector(2 downto 0); RD53Version : String) return boolean;
    function isCalTrigSeq (cmd_in : in std_logic_vector(3 downto 0)) return boolean;
    function isAZPulse (cmd_in : in std_logic_vector(15 downto 0); RD53Version : String) return boolean;
    function isSync (cmd_in : in std_logic_vector(15 downto 0)) return boolean;
    function sumBitsInArray(v: std_logic_vector) return natural;
    function isHit(v: std_logic_vector(3 downto 0)) return std_logic;
    function isWrCmdLong (cmd_in : in std_logic_vector(15 downto 0); RD53Version : String) return std_logic;
    function isCmdOrTrig (cmd_in : in std_logic_vector(15 downto 0); RD53Version : String) return std_logic;
    function isSyncSTD (cmd_in : in std_logic_vector(15 downto 0)) return std_logic;
    function isLongCommand (cmd_in : in std_logic_vector(15 downto 0); RD53Version : String) return std_logic; --only implemented for RD53B
    function isTriggerTag (cmd_in : in std_logic_vector(7 downto 0); RD53Version : String) return boolean;
    function enc5to8 (cmd_in : in std_logic_vector(4 downto 0)) return std_logic_vector;
    function encTrigger(cmd_in : in std_logic_vector(3 downto 0)) return std_logic_vector;
    function encTTag(cmd_in : in std_logic_vector(5 downto 0)) return std_logic_vector;

    COMPONENT dsp_counter
        PORT (
            CLK : IN STD_LOGIC;
            CE : IN STD_LOGIC;
            SCLR : IN STD_LOGIC;
            UP : IN STD_LOGIC;
            LOAD : IN STD_LOGIC;
            L : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
            Q : OUT STD_LOGIC_VECTOR(47 DOWNTO 0)
        );
    END COMPONENT;
end package rd53_package ;

package body rd53_package is


    function isTrigger (cmd_in : in std_logic_vector(7 downto 0)) return boolean is
        variable result : boolean;
        alias input : std_logic_vector(cmd_in'RANGE) is cmd_in;
    begin
        if ( input = Trigger_01 or input = Trigger_02 or input = Trigger_03 or input = Trigger_04 or input = Trigger_05 or input = Trigger_06 or input = Trigger_07 or input = Trigger_08 or input = Trigger_09 or input = Trigger_10 or input = Trigger_11 or input = Trigger_12 or input = Trigger_13 or input = Trigger_14 or input = Trigger_15 ) then
            result := True;
        else
            result := False;
        end if;
        return result;
    end;

    function isData (cmd_in : in std_logic_vector(7 downto 0); RD53Version : String) return std_logic is
    begin
        if ( ( cmd_in = DATA_00 or cmd_in = DATA_01 or cmd_in = DATA_02 or cmd_in = DATA_03 or cmd_in = DATA_04 or cmd_in = DATA_05 or cmd_in = DATA_06 or cmd_in = DATA_07 or cmd_in = DATA_08 or cmd_in = DATA_09 or cmd_in = DATA_10 or cmd_in = DATA_11 or cmd_in = DATA_12 or cmd_in = DATA_13 or cmd_in = DATA_14 or cmd_in = DATA_15 or cmd_in = DATA_16 or cmd_in = DATA_17 or cmd_in = DATA_19 or cmd_in = DATA_20 or cmd_in = DATA_21 or cmd_in = DATA_22 or cmd_in = DATA_23 or cmd_in = DATA_24 or cmd_in = DATA_25 or cmd_in = DATA_26 or cmd_in = DATA_27 or cmd_in = DATA_28 or cmd_in = DATA_29 or cmd_in = DATA_30 or cmd_in = DATA_31 ) or
       ( RD53Version = "A" and ( cmd_in = DATA_18A )) or
       ( RD53Version = "B" and ( cmd_in = DATA_18B )) )
    then
            return '1';
        else
            return '0';
        end if;
    end;

    function isCommand (cmd_in : in std_logic_vector(15 downto 0); RD53Version : String) return std_logic is
    begin
        if ( ( cmd_in = Sync ) or
       ( RD53Version = "A" and ( cmd_in = ECRA or cmd_in = BCRA or cmd_in = GlobalPulseA or cmd_in = CalA or cmd_in = WrRegA or cmd_in = RdRegA or cmd_in = NoopA)) or
       ( RD53Version = "B" and ( cmd_in = PLLLockB or cmd_in(15 downto 8) = ClearB or cmd_in(15 downto 8) = GlobalPulseB or cmd_in(15 downto 8) = ReadTriggerB or cmd_in(15 downto 8) = RdRegB or cmd_in(15 downto 8) = CalB or cmd_in(15 downto 8) = WrRegB or cmd_in(15 downto 0) = ResetHB or cmd_in(15 downto 0) = ResetLB)) )
  then
            return '1';
        else
            return '0';
        end if;
    end;

    function isReset (cmd_in : in std_logic_vector(15 downto 0); RD53Version : String) return boolean is
    begin
        if ( RD53Version = "B" and ( cmd_in(15 downto 0) = ResetHB or cmd_in(15 downto 0) = ResetLB) )
  then
            return True;
        else
            return False;
        end if;
    end;

    function numFrameCommand (cmd_in : in std_logic_vector(15 downto 0); RD53Version : String) return integer is
        variable nframes : integer;
    begin
        if ( ( isTrigger( cmd_in(15 downto 8) ) or cmd_in = Sync ) or
       (RD53Version = "A" and ( cmd_in = ECRA or cmd_in = BCRA or cmd_in = NoopA )) or
       (RD53Version = "B" and ( cmd_in = PLLLockB or cmd_in(15 downto 8) = ClearB or cmd_in(15 downto 8) = GlobalPulseB  or cmd_in(15 downto 0) = ResetHB or cmd_in(15 downto 0) = ResetLB)) )
  then
            nframes := 1;
        elsif ( (RD53Version = "A" and ( cmd_in = GlobalPulseA )) or
           (RD53Version = "B" and ( cmd_in(15 downto 8) = ReadTriggerB or cmd_in(15 downto 8) = RdRegB )) ) then
            nframes := 2;
        elsif ( (RD53Version = "A" and ( cmd_in = RdRegA or cmd_in = CalA )) or
           (RD53Version = "B" and ( cmd_in(15 downto 8) = CalB )) ) then
            nframes := 3;
        elsif ( (RD53Version = "A" and ( cmd_in = WrRegA )) or
           (RD53Version = "B" and ( cmd_in(15 downto 8) = WrRegB )) ) then
            nframes := 4;
        else
            nframes := 5;
        end if;
        return nframes;
    end;

    function isAZSeq (cmd_in : in std_logic_vector(2 downto 0); RD53Version : String) return boolean is
        variable result : boolean;
        alias input : std_logic_vector(cmd_in'RANGE) is cmd_in;
    begin
        if ( RD53Version = "A" and input = enAZSeqA ) then
            result := True;
        else
            result := False;
        end if;
        return result;
    end;

    function isCalTrigSeq (cmd_in : in std_logic_vector(3 downto 0)) return boolean is
        --tested only for RD53A. Can be used also for B after changing the
        --CalTrigSeq_mem content via sw. Testing
        variable result : boolean;
        alias input : std_logic_vector(cmd_in'RANGE) is cmd_in;
    begin
        if ( input = CalTrigSeqEn ) then
            result := True;
        else
            result := False;
        end if;
        return result;
    end;

    function isAZPulse (cmd_in : in std_logic_vector(15 downto 0); RD53Version : String) return boolean is
        variable result : boolean;
        alias input : std_logic_vector(cmd_in'RANGE) is cmd_in;
    begin
        if ( RD53Version = "A" and input = AZ_PULSEA ) then
            result := True;
        else
            result := False;
        end if;
        return result;
    end;

    function isSync (cmd_in : in std_logic_vector(15 downto 0)) return boolean is
        variable result : boolean;
        alias input : std_logic_vector(cmd_in'RANGE) is cmd_in;
    begin
        if ( input = Sync ) then
            result := True;
        else
            result := False;
        end if;
        return result;
    end;

    function sumBitsInArray(v: std_logic_vector) return natural is
        variable h: natural;
    begin
        h := 0;
        for i in v'range loop
            if v(i) = '1' then
                h := h + 1;
            end if;
        end loop;
        return h;
    end function sumBitsInArray;

    function isHit(v: std_logic_vector(3 downto 0)) return std_logic is
        variable h: std_logic;
    begin
        h := '1' when v /= x"F" else '0';
        return h;
    end function isHit;

    function isWrCmdLong (cmd_in : in std_logic_vector(15 downto 0); RD53Version : String) return std_logic is
    begin
        if RD53Version = "B" and cmd_in(15) = '1' and numFrameCommand(cmd_in, RD53Version) /= 1 then --checks it's not a short command
            return '1';
        else
            return '0';
        end if;
    end;

    function isCmdOrTrig (cmd_in : in std_logic_vector(15 downto 0); RD53Version : String) return std_logic is
    begin
        if isTrigger(cmd_in(15 downto 8)) or isCommand(cmd_in,RD53Version) = '1' then
            return '1';
        else
            return '0';
        end if;
    end;

    function isSyncSTD (cmd_in : in std_logic_vector(15 downto 0)) return std_logic is
        alias input : std_logic_vector(cmd_in'RANGE) is cmd_in;
    begin
        if ( input = Sync ) then
            return '1';
        else
            return '0';
        end if;
    end;

    function isLongCommand (cmd_in : in std_logic_vector(15 downto 0); RD53Version : String) return std_logic is --only implemented for B
    begin
        if RD53Version = "B" and ( cmd_in(15 downto 8) = ReadTriggerB or cmd_in(15 downto 8) = RdRegB or cmd_in(15 downto 8) = CalB or cmd_in(15 downto 8) = WrRegB) then
            return '1';
        else
            return '0';
        end if;
    end;

    function isTriggerTag (cmd_in : in std_logic_vector(7 downto 0); RD53Version : String) return boolean is --RL table 34 July 2023 https://gitlab.cern.ch/rd53/RD53B_Manual/-/jobs/26952323/artifacts/file/RD53B_ATLAS_Main.pdf
        variable result : boolean;
    --alias input : std_logic_vector(cmd_in'RANGE) is cmd_in;
    begin
        if isTrigger(cmd_in) then
            result := True;
        elsif isData(cmd_in, RD53Version) = '1' then
            result := True;
        elsif cmd_in = PLLLockB(15 downto 8) or cmd_in = ClearB or cmd_in = GlobalPulseB or cmd_in = ReadTriggerB or cmd_in = RdRegB or cmd_in = CalB or cmd_in = WrRegB then
            result := True;
        else
            result := False;
        end if;
        return result;
    end;

    function enc5to8(cmd_in : in std_logic_vector(4 downto 0)) return std_logic_vector is
    begin
        case cmd_in is
            when "00000"    => return DATA_00;
            when "00001"    => return DATA_01;
            when "00010"    => return DATA_02;
            when "00011"    => return DATA_03;

            when "00100"    => return DATA_04;
            when "00101"    => return DATA_05;
            when "00110"    => return DATA_06;
            when "00111"    => return DATA_07;

            when "01000"    => return DATA_08;
            when "01001"    => return DATA_09;
            when "01010"    => return DATA_10;
            when "01011"    => return DATA_11;

            when "01100"    => return DATA_12;
            when "01101"    => return DATA_13;
            when "01110"    => return DATA_14;
            when "01111"    => return DATA_15;

            when "10000"    => return DATA_16;
            when "10001"    => return DATA_17;
            when "10010"    => return DATA_18B;
            when "10011"    => return DATA_19;

            when "10100"    => return DATA_20;
            when "10101"    => return DATA_21;
            when "10110"    => return DATA_22;
            when "10111"    => return DATA_23;

            when "11000"    => return DATA_24;
            when "11001"    => return DATA_25;
            when "11010"    => return DATA_26;
            when "11011"    => return DATA_27;

            when "11100"    => return DATA_28;
            when "11101"    => return DATA_29;
            when "11110"    => return DATA_30;
            when "11111"    => return DATA_31;
            when others     => return x"AA";

        end case;
    end;

    function encTrigger(cmd_in : in std_logic_vector(3 downto 0)) return std_logic_vector is
    begin
        case cmd_in is
            when "0000"     => return PLLLockB(7 downto 0); --not allowed
            when "0001"     => return Trigger_01;
            when "0010"     => return Trigger_02;
            when "0011"     => return Trigger_03;

            when "0100"     => return Trigger_04;
            when "0101"     => return Trigger_05;
            when "0110"     => return Trigger_06;
            when "0111"     => return Trigger_07;

            when "1000"     => return Trigger_08;
            when "1001"     => return Trigger_09;
            when "1010"     => return Trigger_10;
            when "1011"     => return Trigger_11;

            when "1100"     => return Trigger_12;
            when "1101"     => return Trigger_13;
            when "1110"     => return Trigger_14;
            when "1111"     => return Trigger_15;
            when others     => return x"AA";
        end case;
    end;

    function encTTag(cmd_in : in std_logic_vector(5 downto 0)) return std_logic_vector is
    begin
        case cmd_in is
            when "000000"   => return DATA_00;
            when "000001"   => return DATA_01;
            when "000010"   => return DATA_02;
            when "000011"   => return DATA_03;

            when "000100"   => return DATA_04;
            when "000101"   => return DATA_05;
            when "000110"   => return DATA_06;
            when "000111"   => return DATA_07;

            when "001000"   => return DATA_08;
            when "001001"   => return DATA_09;
            when "001010"   => return DATA_10;
            when "001011"   => return DATA_11;

            when "001100"   => return DATA_12;
            when "001101"   => return DATA_13;
            when "001110"   => return DATA_14;
            when "001111"   => return DATA_15;

            when "010000"   => return DATA_16;
            when "010001"   => return DATA_17;
            when "010010"   => return DATA_18B;
            when "010011"   => return DATA_19;

            when "010100"   => return DATA_20;
            when "010101"   => return DATA_21;
            when "010110"   => return DATA_22;
            when "010111"   => return DATA_23;

            when "011000"   => return DATA_24;
            when "011001"   => return DATA_25;
            when "011010"   => return DATA_26;
            when "011011"   => return DATA_27;

            when "011100"   => return DATA_28;
            when "011101"   => return DATA_29;
            when "011110"   => return DATA_30;
            when "011111"   => return DATA_31;

            when "100000"   => return CalB;
            when "100001"   => return ClearB;
            when "100010"   => return GlobalPulseB;
            when "100011"   => return PLLLockB(7 downto 0);

            when "100100"   => return RdRegB;
            when "100101"   => return ReadTriggerB;
            when "100110"   => return Trigger_01;
            when "100111"   => return Trigger_02;

            when "101000"   => return Trigger_03;
            when "101001"   => return Trigger_04;
            when "101010"   => return Trigger_05;
            when "101011"   => return Trigger_06;

            when "101100"   => return Trigger_07;
            when "101101"   => return Trigger_08;
            when "101110"   => return Trigger_09;
            when "101111"   => return Trigger_10;

            when "110000"   => return Trigger_11;
            when "110001"   => return Trigger_12;
            when "110010"   => return Trigger_13;
            when "110011"   => return Trigger_14;

            when "110100"   => return Trigger_15;
            when "110101"   => return WrRegB;
            when others     => return x"AA";
        end case;
    end;

end package body rd53_package;



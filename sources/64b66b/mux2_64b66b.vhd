--original by Marco Trovado. changed and moved from decoding_64b66b.vhd by Ricardo Luz.
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use IEEE.numeric_std_unsigned.ALL;
    use work.pcie_package.all;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.package_64b66b.all;

library UNISIM;
    use UNISIM.VComponents.all;

entity mux2_64b66b is
    Port (
        cbopt_clk160                    :  in std_logic_vector(3 downto 0);
        clk160_in                       :  in std_logic;
        en_clk160_in                    :  in std_logic;
        DecoderAlignedDec_clk160        :  in array_inv_6b(0 to 1);
        EgroupAggrData                  :  in array_data64V_64b66b_type(0 to 5);
        EgroupDecData_clk160            :  in array_2d_data64V_64b66b_type(0 to 1, 0 to 5);
        sep_clk160                      :  in array_inv_6b(0 to 1);
        sep_Aggr                        :  in std_logic_vector(0 to 5);
        rx_sep_nb_Aggr                  :  in array_3b(0 to 5);

        DecoderAlignedMUX2_clk160       : out std_logic_vector(0 to 5);
        EgroupMUX2Data_clk160_out       : out array_2d_data64V_64b66b_type(0 to 1, 0 to 5);

        sepMUX2_clk160                  : out array_inv_6b(0 to 1);
        rx_sep_nbMUX2_clk160            : out array_2d_3b(0 to 1, 0 to 5)
    );
end mux2_64b66b;

architecture Behavioral of mux2_64b66b is
    signal EgroupMUX2Data_clk160        : array_2d_data64V_64b66b_type(0 to 1, 0 to 5)      := (others => (others => data64V_64b66b_type_zero ));
    signal en_clk160                    : std_logic;
    signal sep_out                      : array_inv_6b(0 to 1) := (others => (others=> '0'));
    signal rx_sep_nb_out                : array_2d_3b(0 to 1, 0 to 5) := (others => (others=> (others=> '0')));
begin

    EgroupMUX2Data_clk160_out <= EgroupMUX2Data_clk160;
    en_clk160 <= en_clk160_in;
    sepMUX2_clk160 <= sep_out;
    rx_sep_nbMUX2_clk160 <= rx_sep_nb_out;

    findata_proc: process(clk160_in)
    begin
        if rising_edge(clk160_in) then
            case cbopt_clk160 is
                when x"2" =>
                    for link in 0 to 1 loop
                        DecoderAlignedMUX2_clk160(link*3+0)         <= DecoderAlignedDec_clk160(link)(0) and DecoderAlignedDec_clk160(link)(1);
                        DecoderAlignedMUX2_clk160(link*3+1)         <= DecoderAlignedDec_clk160(link)(2) and DecoderAlignedDec_clk160(link)(3);
                        DecoderAlignedMUX2_clk160(link*3+2)         <= DecoderAlignedDec_clk160(link)(4) and DecoderAlignedDec_clk160(link)(5);
                        EgroupMUX2Data_clk160(link,0)               <= EgroupAggrData(link*3+0);
                        EgroupMUX2Data_clk160(link,1)               <= EgroupAggrData(link*3+1);
                        EgroupMUX2Data_clk160(link,2)               <= EgroupAggrData(link*3+2);
                        sep_out(link)(0)                            <= sep_Aggr(link*3+0);
                        sep_out(link)(1)                            <= sep_Aggr(link*3+1);
                        sep_out(link)(2)                            <= sep_Aggr(link*3+2);
                        rx_sep_nb_out(link,0)                       <= rx_sep_nb_Aggr(link*2+0);
                        rx_sep_nb_out(link,1)                       <= rx_sep_nb_Aggr(link*2+1);
                        rx_sep_nb_out(link,2)                       <= rx_sep_nb_Aggr(link*2+2);
                    end loop;
                when x"3" =>
                    for link in 0 to 1 loop
                        DecoderAlignedMUX2_clk160(link*2+0)         <= DecoderAlignedDec_clk160(link)(0) and DecoderAlignedDec_clk160(link)(1) and DecoderAlignedDec_clk160(link)(2);
                        DecoderAlignedMUX2_clk160(link*2+1)         <= DecoderAlignedDec_clk160(link)(3) and DecoderAlignedDec_clk160(link)(4) and DecoderAlignedDec_clk160(link)(5);
                        EgroupMUX2Data_clk160(link,0)               <= EgroupAggrData(link*2+0);
                        EgroupMUX2Data_clk160(link,1)               <= EgroupAggrData(link*2+1);
                        sep_out(link)(0)                            <= sep_Aggr(link*2+0);
                        sep_out(link)(1)                            <= sep_Aggr(link*2+1);
                        rx_sep_nb_out(link,0)                       <= rx_sep_nb_Aggr(link*2+0);
                        rx_sep_nb_out(link,1)                       <= rx_sep_nb_Aggr(link*2+1);
                    end loop;
                    DecoderAlignedMUX2_clk160(4 to 5)               <= "11";
                when x"4" =>
                    DecoderAlignedMUX2_clk160(0)                    <= DecoderAlignedDec_clk160(0)(0) and DecoderAlignedDec_clk160(0)(1) and DecoderAlignedDec_clk160(0)(2) and DecoderAlignedDec_clk160(0)(3);
                    DecoderAlignedMUX2_clk160(1)                    <= DecoderAlignedDec_clk160(0)(4) and DecoderAlignedDec_clk160(0)(5) and DecoderAlignedDec_clk160(1)(0) and DecoderAlignedDec_clk160(1)(1);
                    DecoderAlignedMUX2_clk160(2)                    <= DecoderAlignedDec_clk160(1)(2) and DecoderAlignedDec_clk160(1)(3) and DecoderAlignedDec_clk160(1)(4) and DecoderAlignedDec_clk160(1)(5);
                    DecoderAlignedMUX2_clk160(3 to 5)               <= "111";
                    EgroupMUX2Data_clk160(0,0)                      <= EgroupAggrData(0);
                    EgroupMUX2Data_clk160(0,1)                      <= EgroupAggrData(1);
                    EgroupMUX2Data_clk160(1,0)                      <= EgroupAggrData(2);
                    sep_out(0)(0)                                   <= sep_Aggr(0);
                    sep_out(0)(1)                                   <= sep_Aggr(1);
                    sep_out(1)(0)                                   <= sep_Aggr(2);
                    rx_sep_nb_out(0,0)                              <= rx_sep_nb_Aggr(0);
                    rx_sep_nb_out(0,1)                              <= rx_sep_nb_Aggr(1);
                    rx_sep_nb_out(1,0)                              <= rx_sep_nb_Aggr(2);
                when others =>
                    for link in 0 to 1 loop
                        for egroup in 0 to 5 loop
                            EgroupMUX2Data_clk160(link,egroup).data     <= EgroupDecData_clk160(link,egroup).data;
                            EgroupMUX2Data_clk160(link,egroup).valid    <= en_clk160 and EgroupDecData_clk160(link,egroup).valid;
                            sep_out(link)(egroup)                       <= sep_clk160(link)(egroup);
                            rx_sep_nb_out(link,egroup)                  <= rx_sep_nbMUX2_clk160(link,egroup);
                        end loop;
                    end loop;
            end case;
        end if;
    end process;

end Behavioral;

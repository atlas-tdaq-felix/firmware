--------------------------------------------------------------------------------------------------
--  Description:  1) Block sync: looking for a number of 01 or 10 in a row; 2) Lane initialization
--------------------------------------------------------------------------------------------------
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    --use work.axi_stream_package.all;
    use ieee.numeric_std.all;
    use IEEE.numeric_std_unsigned.ALL;
    use work.pcie_package.all;
    --use work.lpgbtfpga_package.all;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;

library UNISIM;
    use UNISIM.VComponents.all;


entity laneInit_64b66b is
    port (
        useEmulator            : in std_logic;
        reset                  : in std_logic;
        clk40_in               : in std_logic;
        header_in              : in std_logic_vector(1 downto 0);
        header_v_in            : in std_logic;
        gearboxslip_out        : out std_logic;
        enable_err_detect_out  : out std_logic;
        lane_up_out            : out std_logic
    );
end laneInit_64b66b;

architecture Behavioral of laneInit_64b66b is
    signal sh_cnt_max         : std_logic_vector(15 downto 0);
    signal blocksync_i        : std_logic;
    signal lossofsync_tmp_i : std_logic;

begin
    sh_cnt_max <= x"0010" when useEmulator = '1' else x"EA60"; --16 else 60000

    block_sync_sm_64b66b_i : entity work.block_sync_sm_64b66b
        generic map (
            SH_INVALID_CNT_MAX => 16
        )
        port map
        (
            sh_cnt_max_in     => sh_cnt_max,
            clk40_in          => clk40_in,
            reset             => reset,
            blocksync_out     => blocksync_i,
            gearboxslip_out   => gearboxslip_out,
            header_in         => header_in,
            header_v_in       => header_v_in
        );

    process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            lossofsync_tmp_i <= not blocksync_i;
        end if;
    end process;

    -----------------------------------------
    -----------LANE INITIALIZATION-----------
    -----------------------------------------
    laneInit_sm_64b66b_i : entity work.laneInit_sm_64b66b
        port map
     (
            CLK40_IN              => clk40_in,
            reset                 => reset,
            lossofsync            => lossofsync_tmp_i,
            enable_err_detect_out => enable_err_detect_out,
            lane_up_out           => lane_up_out


        );

end Behavioral;



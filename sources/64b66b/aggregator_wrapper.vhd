--original by Marco Trovado. changed and moved from decoding_64b66b.vhd by Ricardo Luz.
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use IEEE.numeric_std_unsigned.ALL;
    use work.pcie_package.all;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.package_64b66b.all;

library UNISIM;
    use UNISIM.VComponents.all;

entity aggregator_wrapper is
    Port (
        cbopt                       :  in std_logic_vector(3 downto 0);
        clk160_in                   :  in std_logic;
        --reset                       :  in std_logic;
        --DecoderAlignedMUX2_clk160   :  in std_logic_vector(0 to 5);
        EgroupDecData_clk160        :  in array_2d_data64V_64b66b_type(0 to 1, 0 to 5);
        sep_clk160                  :  in array_inv_6b(0 to 1);
        rx_sep_nb_clk160            :  in array_2d_3b(0 to 1, 0 to 5);
        EgroupAggrData              : out array_data64V_64b66b_type(0 to 5);
        sep_Aggr                    : out std_logic_vector(0 to 5);
        rx_sep_nb_Aggr              : out array_3b(0 to 5)
    );
end aggregator_wrapper;

architecture Behavioral of aggregator_wrapper is
    signal EgroupAggrData_CB2    : array_data64V_64b66b_type(0 to 5) := (others => data64V_64b66b_type_zero);--all used
    signal EgroupAggrData_CB3    : array_data64V_64b66b_type(0 to 5) := (others => data64V_64b66b_type_zero);--4 used
    signal EgroupAggrData_CB4    : array_data64V_64b66b_type(0 to 5) := (others => data64V_64b66b_type_zero);--3 used

    signal sep_CB2          : std_logic_vector(0 to 5) := (others => '0');
    signal sep_CB3          : std_logic_vector(0 to 5) := (others => '0');
    signal sep_CB4          : std_logic_vector(0 to 5) := (others => '0');

    signal rx_sep_nb_CB2    : array_3b(0 to 5) := (others => (others => '0'));
    signal rx_sep_nb_CB3    : array_3b(0 to 5) := (others => (others => '0'));
    signal rx_sep_nb_CB4    : array_3b(0 to 5) := (others => (others => '0'));
begin
    EgroupAggrData <= EgroupAggrData_CB2 when cbopt = x"2" else
                      EgroupAggrData_CB3 when cbopt = x"3" else
                      EgroupAggrData_CB4 when cbopt = x"4" else
                      (others => data64V_64b66b_type_zero);

    sep_Aggr <= sep_CB2 when cbopt = x"2" else
                sep_CB3 when cbopt = x"3" else
                sep_CB4 when cbopt = x"4" else
                (others => '0');

    rx_sep_nb_Aggr <= rx_sep_nb_CB2 when cbopt = x"2" else
                      rx_sep_nb_CB3 when cbopt = x"3" else
                      rx_sep_nb_CB4 when cbopt = x"4" else
                      (others => (others => '0'));

    g_link_inside_pair: for link in 0 to 1 generate
        --CB 2
        aggregator_64b66b_20: entity work.aggregator_64b66b_RD53B
            port map(
                clk160_in       => clk160_in,
                --reset           => reset or (not DecoderAlignedMUX2_clk160(3*link+0)),
                data_lane_in(0) => EgroupDecData_clk160(link,0),
                data_lane_in(1) => EgroupDecData_clk160(link,1),
                data_lane_in(2) => data64V_64b66b_type_zero,
                data_lane_in(3) => data64V_64b66b_type_zero,
                data_lane_out   => EgroupAggrData_CB2(3*link+0),
                sep_in(0)       => sep_clk160(link)(0),
                sep_in(1)       => sep_clk160(link)(1),
                sep_in(2)       => '0',
                sep_in(3)       => '0',
                rx_sep_nb_in(0) => rx_sep_nb_clk160(link,0),
                rx_sep_nb_in(1) => rx_sep_nb_clk160(link,1),
                rx_sep_nb_in(2) => (others => '0'),
                rx_sep_nb_in(3) => (others => '0'),
                sep_out         => sep_CB2(3*link+0),
                rx_sep_nb_out   => rx_sep_nb_CB2(3*link+0)
            );

        aggregator_64b66b_21: entity work.aggregator_64b66b_RD53B
            port map(
                clk160_in       => clk160_in,
                --reset           => reset or (not DecoderAlignedMUX2_clk160(3*link+1)),
                data_lane_in(0) => EgroupDecData_clk160(link,2),
                data_lane_in(1) => EgroupDecData_clk160(link,3),
                data_lane_in(2) => data64V_64b66b_type_zero,
                data_lane_in(3) => data64V_64b66b_type_zero,
                data_lane_out   => EgroupAggrData_CB2(3*link+1),
                sep_in(0)       => sep_clk160(link)(2),
                sep_in(1)       => sep_clk160(link)(3),
                sep_in(2)       => '0',
                sep_in(3)       => '0',
                rx_sep_nb_in(0) => rx_sep_nb_clk160(link,2),
                rx_sep_nb_in(1) => rx_sep_nb_clk160(link,3),
                rx_sep_nb_in(2) => (others => '0'),
                rx_sep_nb_in(3) => (others => '0'),
                sep_out         => sep_CB2(3*link+1),
                rx_sep_nb_out   => rx_sep_nb_CB2(3*link+1)
            );

        aggregator_64b66b_22: entity work.aggregator_64b66b_RD53B
            port map(
                clk160_in       => clk160_in,
                --reset           => reset or (not DecoderAlignedMUX2_clk160(3*link+2)),
                data_lane_in(0) => EgroupDecData_clk160(link,4),
                data_lane_in(1) => EgroupDecData_clk160(link,5),
                data_lane_in(2) => data64V_64b66b_type_zero,
                data_lane_in(3) => data64V_64b66b_type_zero,
                data_lane_out   => EgroupAggrData_CB2(3*link+2),
                sep_in(0)       => sep_clk160(link)(4),
                sep_in(1)       => sep_clk160(link)(5),
                sep_in(2)       => '0',
                sep_in(3)       => '0',
                rx_sep_nb_in(0) => rx_sep_nb_clk160(link,4),
                rx_sep_nb_in(1) => rx_sep_nb_clk160(link,5),
                rx_sep_nb_in(2) => (others => '0'),
                rx_sep_nb_in(3) => (others => '0'),
                sep_out         => sep_CB2(3*link+2),
                rx_sep_nb_out   => rx_sep_nb_CB2(3*link+2)
            );
        --CB 3
        aggregator_64b66b_30: entity work.aggregator_64b66b_RD53B
            port map(
                clk160_in       => clk160_in,
                --reset           => reset or (not DecoderAlignedMUX2_clk160(2*link+0)),
                data_lane_in(0) => EgroupDecData_clk160(link,0),
                data_lane_in(1) => EgroupDecData_clk160(link,1),
                data_lane_in(2) => EgroupDecData_clk160(link,2),
                data_lane_in(3) => data64V_64b66b_type_zero,
                data_lane_out   => EgroupAggrData_CB3(2*link+0),
                sep_in(0)       => sep_clk160(link)(0),
                sep_in(1)       => sep_clk160(link)(1),
                sep_in(2)       => sep_clk160(link)(2),
                sep_in(3)       => '0',
                rx_sep_nb_in(0) => rx_sep_nb_clk160(link,0),
                rx_sep_nb_in(1) => rx_sep_nb_clk160(link,1),
                rx_sep_nb_in(2) => rx_sep_nb_clk160(link,2),
                rx_sep_nb_in(3) => (others => '0'),
                sep_out         => sep_CB3(2*link+0),
                rx_sep_nb_out   => rx_sep_nb_CB3(2*link+0)
            );

        aggregator_64b66b_31: entity work.aggregator_64b66b_RD53B
            port map(
                clk160_in       => clk160_in,
                --reset           => reset or (not DecoderAlignedMUX2_clk160(2*link+1)),
                data_lane_in(0) => EgroupDecData_clk160(link,3),
                data_lane_in(1) => EgroupDecData_clk160(link,4),
                data_lane_in(2) => EgroupDecData_clk160(link,5),
                data_lane_in(3) => data64V_64b66b_type_zero,
                data_lane_out   => EgroupAggrData_CB3(2*link+1),
                sep_in(0)       => sep_clk160(link)(3),
                sep_in(1)       => sep_clk160(link)(4),
                sep_in(2)       => sep_clk160(link)(5),
                sep_in(3)       => '0',
                rx_sep_nb_in(0) => rx_sep_nb_clk160(link,3),
                rx_sep_nb_in(1) => rx_sep_nb_clk160(link,4),
                rx_sep_nb_in(2) => rx_sep_nb_clk160(link,5),
                rx_sep_nb_in(3) => (others => '0'),
                sep_out         => sep_CB3(2*link+1),
                rx_sep_nb_out   => rx_sep_nb_CB3(2*link+1)
            );
    end generate;--g_link_inside_pair: for link in 0 to 1 generate

    --CB 4
    aggregator_64b66b_40: entity work.aggregator_64b66b_RD53B
        port map(
            clk160_in       => clk160_in,
            --reset           => reset or (not DecoderAlignedMUX2_clk160(0)),
            data_lane_in(0) => EgroupDecData_clk160(0,0),
            data_lane_in(1) => EgroupDecData_clk160(0,1),
            data_lane_in(2) => EgroupDecData_clk160(0,2),
            data_lane_in(3) => EgroupDecData_clk160(0,3),
            data_lane_out   => EgroupAggrData_CB4(0),
            sep_in(0)       => sep_clk160(0)(0),
            sep_in(1)       => sep_clk160(0)(1),
            sep_in(2)       => sep_clk160(0)(2),
            sep_in(3)       => sep_clk160(0)(3),
            rx_sep_nb_in(0) => rx_sep_nb_clk160(0,0),
            rx_sep_nb_in(1) => rx_sep_nb_clk160(0,1),
            rx_sep_nb_in(2) => rx_sep_nb_clk160(0,2),
            rx_sep_nb_in(3) => rx_sep_nb_clk160(0,3),
            sep_out         => sep_CB4(0),
            rx_sep_nb_out   => rx_sep_nb_CB4(0)
        );
    aggregator_64b66b_41: entity work.aggregator_64b66b_RD53B
        port map(
            clk160_in       => clk160_in,
            --reset           => reset or (not DecoderAlignedMUX2_clk160(1)),
            data_lane_in(0) => EgroupDecData_clk160(0,4),
            data_lane_in(1) => EgroupDecData_clk160(0,5),
            data_lane_in(2) => EgroupDecData_clk160(1,0),
            data_lane_in(3) => EgroupDecData_clk160(1,1),
            data_lane_out   => EgroupAggrData_CB4(1),
            sep_in(0)       => sep_clk160(0)(4),
            sep_in(1)       => sep_clk160(0)(5),
            sep_in(2)       => sep_clk160(1)(0),
            sep_in(3)       => sep_clk160(1)(1),
            rx_sep_nb_in(0) => rx_sep_nb_clk160(0,4),
            rx_sep_nb_in(1) => rx_sep_nb_clk160(0,5),
            rx_sep_nb_in(2) => rx_sep_nb_clk160(1,0),
            rx_sep_nb_in(3) => rx_sep_nb_clk160(1,1),
            sep_out         => sep_CB4(1),
            rx_sep_nb_out   => rx_sep_nb_CB4(1)
        );
    aggregator_64b66b_42: entity work.aggregator_64b66b_RD53B
        port map(
            clk160_in       => clk160_in,
            --reset           => reset or (not DecoderAlignedMUX2_clk160(2)),
            data_lane_in(0) => EgroupDecData_clk160(1,2),
            data_lane_in(1) => EgroupDecData_clk160(1,3),
            data_lane_in(2) => EgroupDecData_clk160(1,4),
            data_lane_in(3) => EgroupDecData_clk160(1,5),
            data_lane_out   => EgroupAggrData_CB4(2),
            sep_in(0)       => sep_clk160(1)(2),
            sep_in(1)       => sep_clk160(1)(3),
            sep_in(2)       => sep_clk160(1)(4),
            sep_in(3)       => sep_clk160(1)(5),
            rx_sep_nb_in(0) => rx_sep_nb_clk160(1,2),
            rx_sep_nb_in(1) => rx_sep_nb_clk160(1,3),
            rx_sep_nb_in(2) => rx_sep_nb_clk160(1,4),
            rx_sep_nb_in(3) => rx_sep_nb_clk160(1,5),
            sep_out         => sep_CB4(2),
            rx_sep_nb_out   => rx_sep_nb_CB4(2)
        );

end Behavioral;

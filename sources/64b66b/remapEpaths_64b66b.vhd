--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    --use ieee.std_logic_unsigned.all;
    use work.FELIX_package.all;
    use work.package_64b66b.all;

entity remapEpaths_64b66b is
    Port (
        CBOPT        : in  std_logic_vector(3 downto 0);
        DataIn       : in  array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5);
        AlignedIn    : in  array_inv_6b(0 to 1);
        DataOut      : out array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5);
        AlignedOut   : out array_inv_6b(0 to 1)
    );
end remapEpaths_64b66b;


architecture Behavioral of remapEpaths_64b66b is

    type Integer_2d_Array is array (0 to 1, 0 to 5) of integer range 0 to 5; --link, egroup
    signal tonew : Integer_2d_Array;

begin
    --par/opt nobond bond=2 bond=3a* bond=4
    --CBOPT      0     2       3       4
    --*: assumption: different channels from the same chip will occupy contiguous egroups. Possible Permutations (chip Vs Egr #): a) chip1: 0-2, chip2 3-5, chip3 6; b) chip1: 0-2, chip3: 3, chip2: 4-6 c) chip3: 0, chip1: 1-3, chip2: 4-6; Permuting chip# is not relevant
    remap_proc: process(CBOPT)
    begin
        case CBOPT is
            when x"2" =>
                tonew(0,0)         <= 0;
                tonew(0,1)         <= 1;
                tonew(0,2)         <= 2;
                tonew(0,3)         <= 3;
                tonew(0,4)         <= 4;
                tonew(0,5)         <= 5;
                tonew(1,0)         <= 0;
                tonew(1,1)         <= 1;
                tonew(1,2)         <= 2;
                tonew(1,3)         <= 3;
                tonew(1,4)         <= 4;
                tonew(1,5)         <= 5;
            when x"3" =>
                tonew(0,0)         <= 0;
                tonew(0,1)         <= 1;
                tonew(0,2)         <= 2;
                tonew(0,3)         <= 3;
                tonew(0,4)         <= 4;
                tonew(0,5)         <= 5;
                tonew(1,0)         <= 0;
                tonew(1,1)         <= 1;
                tonew(1,2)         <= 2;
                tonew(1,3)         <= 3;
                tonew(1,4)         <= 4;
                tonew(1,5)         <= 5;
            when x"4" =>
                tonew(0,0)         <= 0;
                tonew(0,1)         <= 1;
                tonew(0,2)         <= 2;
                tonew(0,3)         <= 3;
                tonew(0,4)         <= 4;
                tonew(0,5)         <= 5;
                tonew(1,0)         <= 0;
                tonew(1,1)         <= 1;
                tonew(1,2)         <= 2;
                tonew(1,3)         <= 3;
                tonew(1,4)         <= 4;
                tonew(1,5)         <= 5;
            when others =>
                for link in 0 to 1 loop
                    for egroup in 0 to 5 loop
                        tonew(link,egroup) <= egroup;
                    end loop;
                end loop;
        end case;
    end process;

    loop_proc_L: for link in 0 to 1 generate
        loop_proc_E: for egroup in 0 to 5 generate
        begin
            DataOut(link,egroup)    <= DataIn(link,tonew(link,egroup));
            AlignedOut(link)(egroup) <= AlignedIn(link)(tonew(link,egroup));
        end generate;
    end generate;

end Behavioral;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--implementation: assumes header on lane0. TO DO: remove this assumption, so
--will be easier to port to RD53B

-- Use standard library
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.numeric_std.all;
    use IEEE.numeric_std_unsigned.ALL;
    use work.FELIX_package.all;
    use work.package_64b66b.all;
    use work.rd53_package.all;


entity aggregator_64b66b_RD53A is
    port    (
        clk160_in        : in std_logic;
        reset         : in std_logic;
        data_lane_in  : in array_data32VL_64b66b_type(0 to 3);
        data_lane_out : out data32VL_64b66b_type

    );
end aggregator_64b66b_RD53A;

architecture rtl of aggregator_64b66b_RD53A is
    type   smtype  is (IDLE,AGGRGTNG,DONE);
    signal state         : smtype;
    signal state_i : std_logic_vector(1 downto 0);
    signal sel : integer range 0 to 3 := 0;
    signal data_lane_dly_i        : array_data32VL_64b66b_type(0 to 3);
begin

    state_i <= "00" when state = IDLE     else
               "01" when state = AGGRGTNG else
               "10" when state = DONE else
               "11";
    SEL_proc : process(clk160_in)
    begin
        if clk160_in'event and clk160_in='1' then
            if (reset = '1') then
                state                 <= IDLE;
                data_lane_dly_i       <= (others => data32VL_64b66b_type_zero);
                sel                   <= 0;
            else
                data_lane_dly_i       <= data_lane_in;
                case state is
                    when IDLE =>
                        for ilane in 0 to 0 loop --was for ilane in 0 to 3 but didn't work
                            if(data_lane_in(ilane).data(31 downto 25) = HDR_RD53A_7b and data_lane_in(ilane).valid = '1') then
                                sel   <= ilane;
                                state <= AGGRGTNG;
                            else
                                sel   <= 0;
                                state <= IDLE;
                            end if;
                        end loop;

                    when AGGRGTNG =>
                        if((data_lane_in(0).data(31 downto 24) = x"1E" and data_lane_in(0).valid = '1') or
                           (data_lane_in(1).data(31 downto 24) = x"1E" and data_lane_in(1).valid = '1') or
                           (data_lane_in(2).data(31 downto 24) = x"1E" and data_lane_in(2).valid = '1') or
                           (data_lane_in(3).data(31 downto 24) = x"1E" and data_lane_in(3).valid = '1')) then
                            if (sel /= 3) then
                                sel  <= sel+1;
                            else
                                sel  <= 0;
                            end if;
                            state    <= DONE;
                        elsif(data_lane_in(0).valid = '1' or data_lane_in(1).valid = '1' or data_lane_in(2).valid = '1' or data_lane_in(3).valid = '1') then
                            if (sel /= 3) then
                                sel  <= sel+1;
                            else
                                sel  <= 0;
                            end if;
                            state    <= AGGRGTNG;
                        else
                            sel      <= sel;
                            state    <= AGGRGTNG;
                        end if;
                    when DONE =>
                        if(data_lane_in(0).valid = '1' or data_lane_in(1).valid = '1' or data_lane_in(2).valid = '1' or data_lane_in(3).valid = '1') then
                            if (sel /= 3) then
                                sel  <= sel+1;
                            else
                                sel  <= 0;
                            end if;
                            state    <= DONE;
                        else
                            sel      <= 0;
                            state <= IDLE;
                        end if;
                end case;
            end if; --if (reset = '1')
        end if; --if clk160_in'event and clk160_in='1'
    end process;

    process(clk160_in)
    begin
        if clk160_in'event and clk160_in = '1' then
            if(state_i /= "00") then
                data_lane_out <= data_lane_dly_i(sel);
            else
                data_lane_out <= data32VL_64b66b_type_zero;
            end if;
        end if;
    end process;

end rtl;

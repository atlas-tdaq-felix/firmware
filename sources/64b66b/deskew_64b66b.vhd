--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    --use work.axi_stream_package.all;
    use ieee.numeric_std.all;
    use IEEE.numeric_std_unsigned.ALL;
    use work.pcie_package.all;
    --use work.lpgbtfpga_package.all;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.package_64b66b.all;

library UNISIM;
    use UNISIM.VComponents.all;

--//TO DO: add protection against delay between lanes larger than 5 clks
--///////////////////////////////////////////////////////////////////////////////
--//  Description:
--/////
--//channel bonding logic: TWO SM + delay elements
--
--/////////DESKEW SM ilane (ilane=0,...3)
--////
--//inputs:
--// DATA_IN                                   : EN=DATA[68], DATAV [64], DATA[63:0] (HDRV=DATA[67], HDR=DATA[66:65] carried over and not used for logic)
--// foundCB                                          : foundCB from all lanes
--// deskew_lost (to be driven)                       : from CHECK DESKEW LOGIC
--// move_alldelay                                    : will pause all lanes for two clocks (simultaneous gearbox)
--//output:
--// foundCB[ilane]                                   : found CB from this lane
--// deskew_delay (internal to this module)           : used to shift data
--//implementaton
--// 1.  searching: when CB found
--//  a. if last CB symbol found delay = 0         -> alldone
--//  b. if not last                               -> cbfound
--// 2. cbfound: deskew_delay+1. When last CB symbol found -> alldone
--// 3. alldone: either preserve delays or move them to account for real gearbox pauses or the one triggered by move all delay. If deskew lost    -> searching
--
--/////////MOVE_ALLDELAY_SM
--////
--//inputs:
--// foundCB                                          : from DESKEW_SM
--//output:
--// move_alldelay
--//implementation
--// 1. start: wait for foundCB=1111 -> count
--// 2. count (synced with alldone of DESKEW_SM): count up to 65. If 0,1 move_alldelay=1, otherwise 0
--
--//commented out since it's not needed
--// /////////DESKEW LOST LOGIC
--// ////
--// //inputs:
--// // foundCB
--// //output:
--// // deskew_lost
--// //implementation
--// // if !(all CB or not all CB) deskew_lost
entity deskew_64b66b is
    port (
        clk40_in           : in  std_logic;
        reset           : in  std_logic;

        dis_lane_in     : in  std_logic_vector( 3 downto 0);
        data_lane0_in   : in  data64VHHVE_64b66b_type;
        data_lane1_in   : in  data64VHHVE_64b66b_type;
        data_lane2_in   : in  data64VHHVE_64b66b_type;
        data_lane3_in   : in  data64VHHVE_64b66b_type;
        isdeskew_out    : out std_logic;
        data_lane0_out  : out data64VHHVE_64b66b_type;
        data_lane1_out  : out data64VHHVE_64b66b_type;
        data_lane2_out  : out data64VHHVE_64b66b_type;
        data_lane3_out  : out data64VHHVE_64b66b_type
    );
end deskew_64b66b;
architecture Behavioral of deskew_64b66b is
    type deskew_state_type is (searching,
        cbfound,
        alldone
    );
    type delay_state_type is  (start,
        count
    );
    type array_deskew_sm  is array (natural range <>) of deskew_state_type;

    signal reset_i                        : std_logic; --reset || reset_int_i
    signal deskew_sm                      : array_deskew_sm(0 to 3) := (others => searching);
    signal move_alldelay_sm               : delay_state_type := start;
    signal counter                        : integer range 0 to 65 := 0; --std_logic_vector(6 downto 0);
    signal deskew_delay                   : array_4b_rev(0 to 3);
    signal deskew_delay_en                : array_4b_rev(0 to 3);
    signal deskew_lost                    : std_logic;
    signal move_alldelay                  : std_logic;
    signal foundCB                        : std_logic_vector(0 to 3);
    signal foundCBAllMask                 : std_logic;
    signal data_out_i                     : array_data64VHHVE_64b66b_type(0 to 3);
    signal data_i                         : array_2d_data64VHHVE_64b66b_type(0 to 3, 0 to 5); --(link#,#clk delay)
    signal enable_delay                   : std_logic_vector(0 to 3);
    signal dis_output                     : std_logic_vector(0 to 3);
begin

    data_i(0, 0) <= data_lane0_in;
    data_i(1, 0) <= data_lane1_in;
    data_i(2, 0) <= data_lane2_in;
    data_i(3, 0) <= data_lane3_in;

    reset_i   <= reset;

    --DESKEW SM x4
    g_deskew_sm: for il in 0 to 3 generate
    begin
        process(clk40_in)
        begin
            if rising_edge(clk40_in) then
                if (reset_i = '1') then
                    dis_output(il)      <= '0';
                    foundCB(il)         <= '0';
                    deskew_delay(il)    <= (others => '0');
                    deskew_delay_en(il) <= (others => '0');
                    deskew_sm(il)       <= searching;
                else
                    case (deskew_sm(il)) is
                        when searching =>
                            if(data_i(il, 0).data(63 downto 48) = CB_CHARACTER and data_i(il, 0).valid = '1' and data_i(il, 0).en = '1') then --found CB only for this lane
                                foundCB(il)         <= '1';
                                deskew_delay(il)    <= x"0";
                                deskew_delay_en(il) <= x"0";
                                deskew_sm(il)       <= cbfound;
                            else
                                foundCB(il)         <= '0';
                                deskew_delay(il)    <= x"0";
                                deskew_delay_en(il) <= x"0";
                                deskew_sm(il)       <= searching;
                            end if;
                        when cbfound =>
                            --stay here one extra clock for when all foundCB to allow movedelay to arrive in sync with alldone
                            if(foundCBAllMask = '1') then
                                foundCB(il)         <= foundCB(il);
                                deskew_delay(il)    <= deskew_delay(il);
                                deskew_delay_en(il) <= deskew_delay_en(il);
                                deskew_sm(il)       <= alldone;
                            else
                                --if not all CB found stay here and count until all CB found
                                foundCB(il)         <= foundCB(il);
                                deskew_delay(il)    <= deskew_delay(il) + x"1";
                                deskew_delay_en(il) <= deskew_delay_en(il) + x"1";
                                deskew_sm(il)       <= cbfound;
                            end if;
                        when alldone =>
                            --stay here unless the deskew is lost
                            if(deskew_lost = '1') then
                                foundCB(il)         <= '0';
                                deskew_delay(il)    <= x"0"; --deskew is broken, no need to rely on previously acquired delay
                                deskew_delay_en(il) <= x"0";
                                deskew_sm(il)       <= searching;
                            else
                                deskew_delay_en(il) <= deskew_delay_en(il); --do not move the en anymore: data will be available every 2 40 MHZ for all lanes
                                if(move_alldelay = '1') then --pause all lanes for two clocks simulating a symultaneous gearbox  for all lanes. Always disable output
                                    --two cases: 1) lane is not pausing, 2) lane is pausing
                                    --N.B: operating at 40 MHZ to be consistent with move_alldelay so en_in used
                                    if(data_i(il, 0).valid = '1') then --lane is not pausing write and increase delay to avoid data loss
                                        dis_output(il)   <= '1'; --disable output
                                        foundCB(il)      <= foundCB(il);
                                        deskew_delay(il) <= deskew_delay(il) + x"1";
                                        deskew_sm(il)    <= alldone;
                                    else --lane pausing, do not write (data invalid) and do not touch the delay (not writing not reading -> nothing moves)
                                        dis_output(il)   <= '1';
                                        foundCB(il)      <= foundCB(il);
                                        deskew_delay(il) <= deskew_delay(il);
                                        deskew_sm(il)    <= alldone;
                                    end if; -- if(data_i(il, 0)(64))
                                else --normal operations: output always enabled
                                    if(data_i(il, 0).valid = '1') then --lane is not pausing write and increase delay to avoid data loss
                                        dis_output(il)   <= '0';
                                        foundCB(il)      <= foundCB(il);
                                        deskew_delay(il) <= deskew_delay(il);
                                        deskew_sm(il)    <= alldone;
                                    else --lane pausing, do not write (data invalid) decrease delay to avoid data losses
                                        dis_output(il)   <= '0';
                                        foundCB(il)      <= foundCB(il);
                                        deskew_delay(il) <= deskew_delay(il) - x"1";
                                        deskew_sm(il)    <= alldone;
                                    end if; --if(data_i(il, 0)(64) = '1')
                                end if; -- if(move_alldelay = '1')
                            end if; --if(deskew_lost = '1')
                    end case; --case (deskew_sm)
                end if; --if(reset)
            end if; --if rising_edge(clk40_in)
        end process;
    end generate; -- g_deskew_sm

    foundCBAllMask <= (foundCB(0) or dis_lane_in(0)) and (foundCB(1) or dis_lane_in(1)) and (foundCB(2) or dis_lane_in(2)) and (foundCB(3) or dis_lane_in(3));

    isdeskew_out <= '1' when ( ((deskew_sm(0) = alldone) or dis_lane_in(0) = '1') and
                               ((deskew_sm(1) = alldone) or dis_lane_in(1) = '1') and
                               ((deskew_sm(2) = alldone) or dis_lane_in(2) = '1') and
                               ((deskew_sm(3) = alldone) or dis_lane_in(3) = '1') ) else
                    '0';

    enable_delay(0)    <= data_i(0, 0).valid;
    enable_delay(1)    <= data_i(1, 0).valid;
    enable_delay(2)    <= data_i(2, 0).valid;
    enable_delay(3)    <= data_i(3, 0).valid;

    --move_alldelay up 2 clks every 64 from when DESKEW SM arrives in all done
    process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            if (reset_i = '1') then
                move_alldelay              <= '0';
                counter                    <= 0;
                move_alldelay_sm           <= start;
            else
                case (move_alldelay_sm) is
                    when start =>
                        if(foundCBAllMask = '1') then
                            move_alldelay        <= '1';
                            counter              <= counter + 1;
                            move_alldelay_sm     <= count; --start;
                        else
                            move_alldelay        <= '0';
                            counter              <= 0;
                            move_alldelay_sm     <= start;
                        end if;
                    when count => -- @suppress "Dead state 'count': state does not have outgoing transitions"
                        if(counter <= 1) then
                            move_alldelay        <= '1';
                            counter              <= counter + 1;
                            move_alldelay_sm     <= count;
                        elsif(counter = 65) then --65
                            move_alldelay        <= '0';
                            counter              <= 0;
                            move_alldelay_sm     <= count;
                        else
                            move_alldelay        <= '0';
                            counter              <= counter + 1;
                            move_alldelay_sm     <= count;
                        end if;
                end case;
            end if;
        end if;
    end process;

    g_delay: for il in 0 to 3 generate
    begin
        process(clk40_in)
        begin
            if rising_edge(clk40_in) then
                if (reset_i = '1') then
                    data_i(il, 5)   <= data64VHHVE_64b66b_type_zero;
                    data_i(il, 4)   <= data64VHHVE_64b66b_type_zero;
                    data_i(il, 3)   <= data64VHHVE_64b66b_type_zero;
                    data_i(il, 2)   <= data64VHHVE_64b66b_type_zero;
                    data_i(il, 1)   <= data64VHHVE_64b66b_type_zero;
                elsif (enable_delay(il) = '1') then
                    data_i(il, 5)   <= data_i(il, 4);
                    data_i(il, 4)   <= data_i(il, 3);
                    data_i(il, 3)   <= data_i(il, 2);
                    data_i(il, 2)   <= data_i(il, 1);
                    data_i(il, 1)   <= data_i(il, 0);
                end if;
            end if;
        end process;
    end generate;


    --TO DO: write check SM here to drive deskew_lost
    deskew_lost <= '0';

    g_finalassignment: for il in 0 to 3 generate
    begin
        data_out_i(il)  <= data64VHHVE_64b66b_type_zero when (reset_i = '1')        else
                           data64VHHVE_64b66b_type_zero when (dis_output(il) = '1') else
                           data_i(il, 0)                when deskew_delay(il) = x"0" else
                           data_i(il, 1)                when deskew_delay(il) = x"1" else
                           data_i(il, 2)                when deskew_delay(il) = x"2" else
                           data_i(il, 3)                when deskew_delay(il) = x"3" else
                           data_i(il, 4)                when deskew_delay(il) = x"4" else
                           data_i(il, 5)                when deskew_delay(il) = x"5" else
                           data64VHHVE_64b66b_type_zero;
    end generate;
    data_lane0_out <= data_out_i(0);
    data_lane1_out <= data_out_i(1);
    data_lane2_out <= data_out_i(2);
    data_lane3_out <= data_out_i(3);

end Behavioral;



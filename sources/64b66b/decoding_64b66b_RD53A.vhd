--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company: Argonne National Laboratory
-- Engineer: Marco Trovato
--
-- Create Date: 06/06/2019 08:38:50 AM
-- Design Name:
-- Module Name: decoding_64b66b - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    --use work.axi_stream_package.all;
    use ieee.numeric_std.all;
    use IEEE.numeric_std_unsigned.ALL;
    use work.pcie_package.all;
    --use work.lpgbtfpga_package.all;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.package_64b66b.all;

library UNISIM;
    use UNISIM.VComponents.all;

entity decoding_64b66b_RD53A is --NOTE: this is decoding_64b66b before channel bonding was added for RD53B
    generic (
        RD53Version                 : String := "A" --A or B
    );
    Port (
        useEmulator                 :  in std_logic                     ;

        cbopt                       :  in std_logic_vector(3 downto 0)  ; --0: no CB, 3: Egroups={0,1,2}, {3,4,5} CB
        mask_k_char                 :  in std_logic_vector(0 to 3)      ;
        MsbFirst                    :  in std_logic                     ;

        reset                       :  in std_logic;
        clk40_in                    :  in std_logic                     ;
        clk160_in                     :  in std_logic                     ;

        LinkAligned_in              :  in std_logic                     ;
        DataIn                      :  in std_logic_vector(223 downto 0); --LPGBT Data
        DataOut                     : out array_data32VL_64b66b_type(0 to 5);
        BytesKeepOut                : out array_4b(0 to 5)              ;
        DataDCSOut                  : out array_data32VL_64b66b_type(0 to 5);
        DecoderAligned_out          : out std_logic_vector(0 to 5)      ;
        DecoderDeskewed_out         : out std_logic_vector(0 to 5)      ;

        soft_err                    : out std_logic_vector(0 to 5)      ;
        soft_err_cnt                : out array_32b(0 to 5)              ;
        soft_err_rst                :  in std_logic                     ; --Acitve high reset

        --debug (regmap)
        cnt_rx_64b66bhdr_out            : out std_logic_vector(31 downto 0); --summer over all egroups
        ref_packet_in                   : in std_logic_vector(31 downto 0);

        EgroupGB1Data_dbg               : out array_data32VHHV_64b66b_type(0 to 5);
        EgroupUnscrData_dbg             : out array_data32VHHV_64b66b_type(0 to 5);
        EgroupGB2Data_dbg               : out array_data64VHHVE_64b66b_type(0 to 5);
        EgroupRmpData_dbg               : out array_data64VHHVE_64b66b_type(0 to 5);
        EgroupDecData_dbg               : out array_data64V_64b66b_type(0 to 5);
        EgroupDecDataSplit_dbg          : out array_data32VL_64b66b_type(0 to 5); --array_34b(0 to 6);
        DCSDecData_dbg                  : out array_data64V_64b66b_type(0 to 5);
        EgroupMUX1Data_dbg              : out array_data64VHHVE_64b66b_type(0 to 5);
        EgroupBondData_3_dbg            : out array_data64VHHVE_64b66b_type(0 to 5);
        DecoderAlignedRmp_dbg           : out std_logic_vector(0 to 5);
        DecoderAlignedMUX1_dbg          : out std_logic_vector(0 to 5);
        DecoderAlignedBond_3_dbg        : out std_logic_vector(0 to 1);

        EgroupDecDataSplit_clk160_dbg   : out array_data32VL_64b66b_type(0 to 5); --array_34b(0 to 6);
        EgroupMUX2Data_clk160_dbg       : out array_data32VL_64b66b_type(0 to 5); --array_34b(0 to 6);
        EgroupAggrData_30_dbg           : out data32VL_64b66b_type; --std_logic_vector(33 downto 0);
        EgroupAggrData_31_dbg           : out data32VL_64b66b_type; --std_logic_vector(33 downto 0);
        DCSDecDataSplit_clk160_dbg      : out array_data32VL_64b66b_type(0 to 5); --array_34b(0 to 6);
        DecoderAlignedDec_clk160_dbg    : out std_logic_vector(0 to 5)
    --

    );
end decoding_64b66b_RD53A;


architecture Behavioral of decoding_64b66b_RD53A is

    signal clk40_i                  : std_logic;
    signal clk160                   : std_logic;
    signal cbopt_clk160             : std_logic_vector(3 downto 0)            := x"0";
    signal en_clk160                : std_logic                               := '0'; --=1 every 4 clk160s
    signal gearboxslip_i            : std_logic_vector(0 to 5);
    signal lane_up_i                : std_logic_vector(0 to 5);
    signal cnt_rx_64b66bhdr_i       : array_32b(0 to 5);
    signal enable_err_detect_i      : std_logic_vector(0 to 5);
    signal reset_or_not_aligned_i   : std_logic_vector(0 to 1);
    signal foundCBchar              : std_logic_vector(0 to 5); -- @suppress "signal foundCBchar is never read"
    signal foundHDRchar             : std_logic_vector(0 to 5); -- @suppress "signal foundHDRchar is never read"
    signal soft_err_cnt_i           : array_32b(0 to 5)                        := (others => (others => '0'));
    -------------------
    ------clk40--------
    -------------------
    signal DecoderAligned            : std_logic_vector(0 to 5);
    signal DecoderDeskewed           : std_logic_vector(0 to 5);
    signal DecoderAlignedRmp         : std_logic_vector(0 to 5);
    signal DecoderAlignedMUX1        : std_logic_vector(0 to 5)                := (others => '0');
    signal DecoderAlignedBond_3      : std_logic_vector(0 to 1)                := (others => '0');

    signal EgroupGB1Data             : array_data32VHHV_64b66b_type(0 to 5);
    signal EgroupUnscrData           : array_data32VHHV_64b66b_type(0 to 5);
    signal EgroupGB2Data             : array_data64VHHVE_64b66b_type(0 to 5);
    signal EgroupRmpData             : array_data64VHHVE_64b66b_type(0 to 5);
    signal EgroupDecData             : array_data64V_64b66b_type(0 to 5);
    signal sep                       : std_logic_vector(0 to 5);
    signal rx_sep_nb                 : array_3b(0 to 5);
    signal DCSDecData                : array_data64V_64b66b_type(0 to 5);
    signal EgroupDecDataSplit        : array_data32VL_64b66b_type(0 to 5);
    signal EgroupBytesKeep           : array_4b(0 to 5)                  ;
    signal DCSDecDataSplit           : array_data32VL_64b66b_type(0 to 5);
    signal EgroupMUX1Data            : array_data64VHHVE_64b66b_type(0 to 5)   := (others => data64VHHVE_64b66b_type_zero);
    signal EgroupBondData_3          : array_data64VHHVE_64b66b_type(0 to 5);

    --added to not dupliate files
    signal EgroupGB2Data_tmp             : array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5);
    signal EgroupRmpData_tmp             : array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5);
    signal DecoderAligned_tmp            : array_inv_6b(0 to 1);
    signal DecoderAlignedRmp_tmp         : array_inv_6b(0 to 1);
    -------------------
    ------clk160--------
    -------------------
    signal EgroupDecDataSplit_clk160    : array_data32VL_64b66b_type(0 to 5)      := (others => data32VL_64b66b_type_zero);
    signal EgroupBytesKeep_clk160       : array_4b(0 to 5);
    signal EgroupMUX2Data_clk160        : array_data32VL_64b66b_type(0 to 5)      := (others => data32VL_64b66b_type_zero);
    signal EgroupMUX2BytesKeep_clk160   : array_4b(0 to 5)                        := (others => (others => '0'));
    signal EgroupAggrData_30            : data32VL_64b66b_type                    := data32VL_64b66b_type_zero;
    signal EgroupAggrData_31            : data32VL_64b66b_type                    := data32VL_64b66b_type_zero;
    signal DCSDecDataSplit_clk160       : array_data32VL_64b66b_type(0 to 5)      := (others => data32VL_64b66b_type_zero);
    signal EgroupDCSDataNew_clk160      : array_data32VL_64b66b_type(0 to 5)      := (others => data32VL_64b66b_type_zero);

    signal DecoderAlignedDec_clk160     : std_logic_vector(0 to 5)                := (others => '0');
    signal DecoderAlignedMUX2_clk160    : std_logic_vector(0 to 1)                := (others => '0');

begin

    cboptclk160_proc: process(clk160)
    begin
        if rising_edge(clk160) then
            cbopt_clk160 <= cbopt;
        end if;
    end process;

    clk40_i <= clk40_in;
    clk160  <= clk160_in;

    --------------------------------------
    ------  LANE INITIALIZATION ---------
    --------------------------------------
    DecoderAligned_out <= DecoderAligned;
    g_LPGBT_Egroups3: for egroup in 0 to 5 generate
        signal lane_reset: std_logic;
    begin
        lane_reset             <= reset or (not LinkAligned_in);
        DecoderAligned(egroup) <= lane_up_i(egroup);
        laneInit_64b66b_i : entity work.laneInit_64b66b
            port map
          (
                useEmulator               => useEmulator,
                reset                     => lane_reset,
                clk40_in                  => clk40_i,
                header_in                 => EgroupUnscrData(egroup).hdr,
                header_v_in               => EgroupUnscrData(egroup).hdr_valid,
                gearboxslip_out           => gearboxslip_i(egroup),
                enable_err_detect_out     => enable_err_detect_i(egroup),
                lane_up_out               => lane_up_i(egroup)
            );
    end generate;

    --------------------------------------
    ------------ MAIN DATASTREAM ---------
    ------------------CLK40---------------
    --------------------------------------
    g_LPGBT_Egroups_gearbox: for egroup in 0 to 5 generate
    begin
        decodingGearbox_64b66b_i: entity work.decodingGearbox_64b66b
            generic map(
                IN_WIDTH               => 32,
                GB_OUT_WIDTH           => 66
            )
            port map(
                MsbFirst               => MsbFirst,
                Reset                  => reset or not LinkAligned_in,
                clk40                  => clk40_i,
                DataIn                 => DataIn(egroup*32+31 downto egroup*32),
                LinkAligned            => LinkAligned_in,
                DataOut                => EgroupGB1Data(egroup).data,
                DataOutValid           => EgroupGB1Data(egroup).valid,
                HeaderOut              => EgroupGB1Data(egroup).hdr,
                HeaderOutValid         => EgroupGB1Data(egroup).hdr_valid,
                BitSlip                => gearboxslip_i(egroup)
            );
    end generate;

    g_LPGBT_Egroups_descr: for egroup in 0 to 5 generate
        signal system_reset: std_logic;
    begin
        system_reset <= reset or (not LinkAligned_in) or (not DecoderAligned(egroup));
        descrambler_64b66b_i : entity work.descrambler_64b66b
            generic map (
                RX_DATA_WIDTH  => 32
            )
            port map
          (
                clk40_in       => clk40_i,
                reset          => system_reset,
                data_in        => EgroupGB1Data(egroup).data,
                data_v_in      => EgroupGB1Data(egroup).valid,
                data_out       => EgroupUnscrData(egroup).data
            );
    end generate;

    g_LPGBT_Egroups_gb2: for egroup in 0 to 5 generate
        delay_descrambler: process(clk40_i)
        begin
            if rising_edge(clk40_i) then
                EgroupUnscrData(egroup).valid     <= EgroupGB1Data(egroup).valid;
                EgroupUnscrData(egroup).hdr       <= EgroupGB1Data(egroup).hdr;
                EgroupUnscrData(egroup).hdr_valid <= EgroupGB1Data(egroup).hdr_valid;
            end if;
        end process;

        foundCBchar(egroup)  <= '1' when EgroupUnscrData(egroup).data(31 downto 20) = x"784"    else '0';
        foundHDRchar(egroup) <= '1' when EgroupUnscrData(egroup).data(31 downto 25) = "0000001" else '0';

        gearbox32to64_64b66b_i : entity work.gearbox32to64_64b66b
            port map
       (
                reset    => reset or (not LinkAligned_in) or (not DecoderAligned(egroup)),
                clk40_in => clk40_i,
                data_in  => EgroupUnscrData(egroup),
                data_out => EgroupGB2Data(egroup)
            );

    end generate;

    g_remap_tmp: for egroup in 0 to 5 generate
    begin
        EgroupGB2Data_tmp(0,egroup)    <= EgroupGB2Data(egroup);
        DecoderAligned_tmp(0)(egroup)   <= DecoderAligned(egroup);
        EgroupRmpData(egroup)           <= EgroupRmpData_tmp(0,egroup);
        DecoderAlignedRmp(egroup)       <= DecoderAlignedRmp_tmp(0)(egroup);
    end generate;

    remapEpaths_64b66b_i: entity work.remapEpaths_64b66b
        port map(
            CBOPT       => cbopt,
            DataIn      => EgroupGB2Data_tmp,
            AlignedIn   => DecoderAligned_tmp,
            DataOut     => EgroupRmpData_tmp,
            AlignedOut  => DecoderAlignedRmp_tmp
        );

    sync_reset_proc: process(clk40_i)
    begin
        if rising_edge(clk40_i) then
            reset_or_not_aligned_i(0) <= reset or (not DecoderAlignedBond_3(0));
            reset_or_not_aligned_i(1) <= reset or (not DecoderAlignedBond_3(1));
        end if;
    end process;

    deskew_64b66b_30: entity work.deskew_64b66b
        port map(
            clk40_in       => clk40_i,
            reset          => reset_or_not_aligned_i(0),
            dis_lane_in    => "1000",
            data_lane0_in  => EgroupRmpData(0),
            data_lane1_in  => EgroupRmpData(1),
            data_lane2_in  => EgroupRmpData(2),
            data_lane3_in  => data64VHHVE_64b66b_type_zero,
            isdeskew_out   => DecoderDeskewed(0),
            data_lane0_out => EgroupBondData_3(0),
            data_lane1_out => EgroupBondData_3(1),
            data_lane2_out => EgroupBondData_3(2),
            data_lane3_out => open
        );

    deskew_64b66b_31: entity work.deskew_64b66b
        port map(
            clk40_in       => clk40_i,
            reset          => reset_or_not_aligned_i(1),
            dis_lane_in    => "1000",
            data_lane0_in  => EgroupRmpData(3),
            data_lane1_in  => EgroupRmpData(4),
            data_lane2_in  => EgroupRmpData(5),
            data_lane3_in  => data64VHHVE_64b66b_type_zero,
            isdeskew_out   => DecoderDeskewed(1),
            data_lane0_out => EgroupBondData_3(3),
            data_lane1_out => EgroupBondData_3(4),
            data_lane2_out => EgroupBondData_3(5),
            data_lane3_out => open
        );

    newdata_proc: process(DecoderAlignedRmp, DecoderAlignedBond_3, EgroupRmpData, EgroupBondData_3, cbopt, DecoderDeskewed(0), DecoderDeskewed(1))
    begin
        case cbopt is
            when x"3" =>
                DecoderAlignedBond_3(0)    <= DecoderAlignedRmp(0); -- temporarily relying on lane 0 only since BOB has issues with lane2 and DecoderAlignedRmp(1) and DecoderAlignedRmp(2);
                DecoderAlignedBond_3(1)    <= DecoderAlignedRmp(3) and DecoderAlignedRmp(4) and DecoderAlignedRmp(5);
                for egroup in 0 to 5 loop
                    EgroupMUX1Data(egroup) <= EgroupBondData_3(egroup);
                end loop;
                DecoderAlignedMUX1(0)      <= DecoderAlignedBond_3(0);
                DecoderAlignedMUX1(1)      <= DecoderAlignedBond_3(0);
                DecoderAlignedMUX1(2)      <= DecoderAlignedBond_3(0);
                DecoderAlignedMUX1(3)      <= DecoderAlignedBond_3(1);
                DecoderAlignedMUX1(4)      <= DecoderAlignedBond_3(1);
                DecoderAlignedMUX1(5)      <= DecoderAlignedBond_3(1);
                DecoderDeskewed_out(0)     <= DecoderDeskewed(0);
                DecoderDeskewed_out(1)     <= DecoderDeskewed(0);
                DecoderDeskewed_out(2)     <= DecoderDeskewed(0);
                DecoderDeskewed_out(3)     <= DecoderDeskewed(1);
                DecoderDeskewed_out(4)     <= DecoderDeskewed(1);
                DecoderDeskewed_out(5)     <= DecoderDeskewed(1);

            when others => --include nobond
                DecoderAlignedBond_3(0)    <= DecoderAlignedRmp(0);
                DecoderAlignedBond_3(1)    <= DecoderAlignedRmp(1);
                EgroupMUX1Data             <= EgroupRmpData;
                DecoderAlignedMUX1         <= DecoderAlignedRmp;
                DecoderDeskewed_out        <= (others => '1');
        end case;
    end process;

    soft_err_cnt <= soft_err_cnt_i;
    g_LPGBT_Egroups2: for egroup in 0 to 5 generate
        symbolDecoding_64b66b_i : entity work.symbolDecoding_64b66b
            port map
          (
                reset                    => reset or (not LinkAligned_in),
                clk40_in                 => clk40_i,
                enable_err_detect_in     => enable_err_detect_i(egroup),
                mask_k_char_in           => mask_k_char,
                lane_up_in               => DecoderAlignedMUX1(egroup),
                data_in                  => EgroupMUX1Data(egroup),
                data_out                 => EgroupDecData(egroup),
                sep_out                  => sep(egroup),
                sep_nb_out               => rx_sep_nb(egroup),
                datadcs_out              => DCSDecData(egroup),
                soft_err_out             => soft_err(egroup)
            );

        err_cnt: process(clk40_i)
        begin
            if rising_edge(clk40_i) then
                if soft_err_rst = '1' then
                    soft_err_cnt_i(egroup) <= (others => '0');
                elsif soft_err(egroup) = '1' and soft_err_cnt_i(egroup) /= x"FFFF_FFFF" then
                    soft_err_cnt_i(egroup) <= soft_err_cnt_i(egroup) + x"1";
                else
                    soft_err_cnt_i(egroup) <= soft_err_cnt_i(egroup);
                end if;
            end if;
        end process;

        RD53Interface_i : entity work.RD53Interface
            generic map (
                RD53Version          => RD53Version
            )
            PORT MAP (
                Reset                => reset or (not LinkAligned_in),
                clk_in               => clk40_i,
                data_in              => EgroupDecData(egroup),
                sep_in               => sep(egroup),
                rx_sep_nb            => rx_sep_nb(egroup),
                data_k_in            => DCSDecData(egroup),
                data_out             => EgroupDecDataSplit(egroup),
                byteskeep_out        => EgroupBytesKeep(egroup),
                datadcs_out          => DCSDecDataSplit(egroup),
                --debug info
                cnt_rx_64b66bhdr_out => cnt_rx_64b66bhdr_i(egroup),
                ref_packet_in        => ref_packet_in

            );

    end generate;
    --sum over all egroups
    cnt_rx_64b66bhdr_out <= std_logic_vector(unsigned(cnt_rx_64b66bhdr_i(0)) + unsigned(cnt_rx_64b66bhdr_i(1)) + unsigned(cnt_rx_64b66bhdr_i(2)) + unsigned(cnt_rx_64b66bhdr_i(3)) + unsigned(cnt_rx_64b66bhdr_i(4)) + unsigned(cnt_rx_64b66bhdr_i(5)));

    --------------------------------------
    --------  MAIN DATASTREAM ------------
    ---------------CLK160-----------------
    --------------------------------------
    --clk160_in=clk160 and clk40 since are generated from the same MMCM, whose source is ttc clk, so this simple crossing plus en_clk160 should work
    cross_clk40to160: process(clk160)
    begin
        if rising_edge(clk160) then
            EgroupDecDataSplit_clk160 <= EgroupDecDataSplit;
            EgroupBytesKeep_clk160    <= EgroupBytesKeep;
            DecoderAlignedDec_clk160  <= DecoderAlignedMUX1;
            DCSDecDataSplit_clk160    <= DCSDecDataSplit;
        end if;
    end process;

    enclk160_proc : process(clk160)
        variable count : integer range 0 to 3 := 0;
    begin
        if rising_edge(clk160) then
            if(count=0) then
                en_clk160 <= '1';
                count   := count + 1;
            elsif (count=3) then
                count   := 0;
                en_clk160 <= '0';
            else
                count   := count + 1;
                en_clk160 <= '0';
            end if;
        end if;
    end process;

    aggregator_64b66b_30: entity work.aggregator_64b66b_RD53A
        port map(
            clk160_in       => clk160,
            reset           => reset or (not DecoderAlignedMUX2_clk160(0)),
            data_lane_in(0) => EgroupDecDataSplit_clk160(0),
            data_lane_in(1) => EgroupDecDataSplit_clk160(1),
            data_lane_in(2) => EgroupDecDataSplit_clk160(2),
            data_lane_in(3) => data32VL_64b66b_type_zero,
            data_lane_out   => EgroupAggrData_30
        );
    aggregator_64b66b_31: entity work.aggregator_64b66b_RD53A
        port map(
            clk160_in       => clk160,
            reset           => reset or (not DecoderAlignedMUX2_clk160(1)),
            data_lane_in(0) => EgroupDecDataSplit_clk160(3),
            data_lane_in(1) => EgroupDecDataSplit_clk160(4),
            data_lane_in(2) => EgroupDecDataSplit_clk160(5),
            data_lane_in(3) => data32VL_64b66b_type_zero,
            data_lane_out   => EgroupAggrData_31
        );

    findata_proc: process(clk160)
    begin
        if rising_edge(clk160) then
            case cbopt_clk160 is
                when x"3" =>
                    DecoderAlignedMUX2_clk160(0)            <= DecoderAlignedDec_clk160(0) and DecoderAlignedDec_clk160(1) and DecoderAlignedDec_clk160(2);
                    DecoderAlignedMUX2_clk160(1)            <= DecoderAlignedDec_clk160(3) and DecoderAlignedDec_clk160(4) and DecoderAlignedDec_clk160(5);
                    EgroupMUX2Data_clk160(0)                <= EgroupAggrData_30;
                    EgroupMUX2Data_clk160(1)                <= EgroupAggrData_31;
                    EgroupMUX2BytesKeep_clk160              <= EgroupBytesKeep_clk160;
                when others =>
                    for egroup in 0 to 5 loop
                        EgroupMUX2Data_clk160(egroup).data  <= EgroupDecDataSplit_clk160(egroup).data;
                        EgroupMUX2Data_clk160(egroup).valid <= en_clk160 and EgroupDecDataSplit_clk160(egroup).valid;
                        EgroupMUX2Data_clk160(egroup).tlast <= en_clk160 and EgroupDecDataSplit_clk160(egroup).tlast;
                        EgroupMUX2BytesKeep_clk160          <= EgroupBytesKeep_clk160;
                    end loop;
            end case;
        end if;
    end process;

    dcsdatanew_proc: process(clk160)
    begin
        if rising_edge(clk160) then
            for egroup in 0 to 5 loop
                EgroupDCSDataNew_clk160(egroup).data  <= DCSDecDataSplit_clk160(egroup).data;
                EgroupDCSDataNew_clk160(egroup).valid <= en_clk160 and DCSDecDataSplit_clk160(egroup).valid;
            end loop;
        end if;
    end process;

    DataOut                      <= EgroupMUX2Data_clk160;
    BytesKeepOut                 <= EgroupMUX2BytesKeep_clk160;
    DataDCSOut                   <= EgroupDCSDataNew_clk160;

    --debug
    EgroupGB1Data_dbg            <= EgroupGB1Data;
    EgroupUnscrData_dbg          <= EgroupUnscrData;
    EgroupGB2Data_dbg            <= EgroupGB2Data;
    EgroupRmpData_dbg            <= EgroupRmpData;
    EgroupDecData_dbg            <= EgroupDecData;
    EgroupDecDataSplit_dbg       <= EgroupDecDataSplit;
    DCSDecData_dbg               <= DCSDecData;
    EgroupMUX1Data_dbg           <= EgroupMUX1Data;
    EgroupBondData_3_dbg         <= EgroupBondData_3;
    DecoderAlignedRmp_dbg        <= DecoderAlignedRmp;
    DecoderAlignedMUX1_dbg       <= DecoderAlignedMUX1;
    DecoderAlignedBond_3_dbg     <= DecoderAlignedBond_3;
    EgroupDecDataSplit_clk160_dbg  <= EgroupDecDataSplit_clk160;
    EgroupMUX2Data_clk160_dbg      <= EgroupMUX2Data_clk160;
    EgroupAggrData_30_dbg        <= EgroupAggrData_30;
    EgroupAggrData_31_dbg        <= EgroupAggrData_31;
    DCSDecDataSplit_clk160_dbg     <= DCSDecDataSplit_clk160;
    DecoderAlignedDec_clk160_dbg   <= DecoderAlignedDec_clk160;

end Behavioral;

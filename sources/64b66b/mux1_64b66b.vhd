--original by Marco Trovado. changed and moved from decoding_64b66b.vhd by Ricardo Luz.
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use IEEE.numeric_std_unsigned.ALL;
    use work.pcie_package.all;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.package_64b66b.all;

library UNISIM;
    use UNISIM.VComponents.all;

entity mux1_64b66b is
    Port (
        cbopt                   :  in std_logic_vector(3 downto 0)  ;
        DecoderAlignedRmp       :  in array_inv_6b(0 to 1);
        EgroupRmpData           :  in array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5);
        DecoderDeskewed_in      :  in std_logic_vector(0 to 5);
        EgroupBondData          :  in array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5);

        DecoderDeskewed_out     : out array_inv_6b(0 to 1);
        DecoderAlignedBond_out  : out std_logic_vector(0 to 5);
        DecoderAlignedMUX1_out  : out array_inv_6b(0 to 1);
        EgroupMUX1Data_out      : out array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5)
    );
end mux1_64b66b;

architecture Behavioral of mux1_64b66b is
    signal DecoderAlignedBond   : std_logic_vector(0 to 5) := (others => '0');
    signal DecoderAlignedMUX1   : array_inv_6b(0 to 1) := (others => (others => '0'));
    signal EgroupMUX1Data       : array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5)   := (others => (others => data64VHHVE_64b66b_type_zero));
begin

    DecoderAlignedBond_out  <= DecoderAlignedBond;
    DecoderAlignedMUX1_out  <= DecoderAlignedMUX1;
    EgroupMUX1Data_out      <= EgroupMUX1Data;

    newdata_proc: process(DecoderAlignedRmp, DecoderAlignedBond, EgroupRmpData, EgroupBondData, cbopt, DecoderDeskewed_in)
    begin
        case cbopt is
            when x"2" =>
                for link in 0 to 1 loop
                    DecoderAlignedBond(link*3+0)       <= DecoderAlignedRmp(link)(0) and DecoderAlignedRmp(link)(1);
                    DecoderAlignedBond(link*3+1)       <= DecoderAlignedRmp(link)(2) and DecoderAlignedRmp(link)(3);
                    DecoderAlignedBond(link*3+2)       <= DecoderAlignedRmp(link)(4) and DecoderAlignedRmp(link)(5);
                    for egroup in 0 to 5 loop
                        EgroupMUX1Data(link,egroup)    <= EgroupBondData(link,egroup);
                    end loop;
                    DecoderAlignedMUX1(link)(0)        <= DecoderAlignedBond(link*3+0);
                    DecoderAlignedMUX1(link)(1)        <= DecoderAlignedBond(link*3+0);
                    DecoderAlignedMUX1(link)(2)        <= DecoderAlignedBond(link*3+1);
                    DecoderAlignedMUX1(link)(3)        <= DecoderAlignedBond(link*3+1);
                    DecoderAlignedMUX1(link)(4)        <= DecoderAlignedBond(link*3+2);
                    DecoderAlignedMUX1(link)(5)        <= DecoderAlignedBond(link*3+2);
                    DecoderDeskewed_out(link)(0)       <= DecoderDeskewed_in(link*3+0);
                    DecoderDeskewed_out(link)(1)       <= DecoderDeskewed_in(link*3+0);
                    DecoderDeskewed_out(link)(2)       <= DecoderDeskewed_in(link*3+1);
                    DecoderDeskewed_out(link)(3)       <= DecoderDeskewed_in(link*3+1);
                    DecoderDeskewed_out(link)(4)       <= DecoderDeskewed_in(link*3+2);
                    DecoderDeskewed_out(link)(5)       <= DecoderDeskewed_in(link*3+2);
                end loop; --for link in 0 to 1 loop
            when x"3" =>
                for link in 0 to 1 loop
                    DecoderAlignedBond(link*2+0)       <= DecoderAlignedRmp(link)(0) and DecoderAlignedRmp(link)(1) and DecoderAlignedRmp(link)(2); -- old comment: temporarily relying on lane 0 only since BOB has issues with lane2
                    DecoderAlignedBond(link*2+1)       <= DecoderAlignedRmp(link)(3) and DecoderAlignedRmp(link)(4) and DecoderAlignedRmp(link)(5);
                    for egroup in 0 to 5 loop
                        EgroupMUX1Data(link,egroup)    <= EgroupBondData(link,egroup);
                    end loop;
                    DecoderAlignedMUX1(link)(0)        <= DecoderAlignedBond(link*2+0);
                    DecoderAlignedMUX1(link)(1)        <= DecoderAlignedBond(link*2+0);
                    DecoderAlignedMUX1(link)(2)        <= DecoderAlignedBond(link*2+0);
                    DecoderAlignedMUX1(link)(3)        <= DecoderAlignedBond(link*2+1);
                    DecoderAlignedMUX1(link)(4)        <= DecoderAlignedBond(link*2+1);
                    DecoderAlignedMUX1(link)(5)        <= DecoderAlignedBond(link*2+1);
                    DecoderDeskewed_out(link)(0)       <= DecoderDeskewed_in(link*2+0);
                    DecoderDeskewed_out(link)(1)       <= DecoderDeskewed_in(link*2+0);
                    DecoderDeskewed_out(link)(2)       <= DecoderDeskewed_in(link*2+0);
                    DecoderDeskewed_out(link)(3)       <= DecoderDeskewed_in(link*2+1);
                    DecoderDeskewed_out(link)(4)       <= DecoderDeskewed_in(link*2+1);
                    DecoderDeskewed_out(link)(5)       <= DecoderDeskewed_in(link*2+1);
                end loop; --for link in 0 to 1 loop
                DecoderAlignedBond(4 to 5) <= "11";
            when x"4" =>
                DecoderAlignedBond(0)           <= DecoderAlignedRmp(0)(0) and DecoderAlignedRmp(0)(1) and DecoderAlignedRmp(0)(2) and DecoderAlignedRmp(0)(3);
                DecoderAlignedBond(1)           <= DecoderAlignedRmp(0)(4) and DecoderAlignedRmp(0)(5) and DecoderAlignedRmp(1)(0) and DecoderAlignedRmp(1)(1);
                DecoderAlignedBond(2)           <= DecoderAlignedRmp(1)(2) and DecoderAlignedRmp(1)(3) and DecoderAlignedRmp(1)(4) and DecoderAlignedRmp(1)(5);
                DecoderAlignedBond(3 to 5)      <= "111";
                for egroup in 0 to 5 loop
                    EgroupMUX1Data(0,egroup)    <= EgroupBondData(0,egroup);
                    EgroupMUX1Data(1,egroup)    <= EgroupBondData(1,egroup);
                end loop;
                DecoderAlignedMUX1(0)(0)        <= DecoderAlignedBond(0);
                DecoderAlignedMUX1(0)(1)        <= DecoderAlignedBond(0);
                DecoderAlignedMUX1(0)(2)        <= DecoderAlignedBond(0);
                DecoderAlignedMUX1(0)(3)        <= DecoderAlignedBond(0);
                DecoderAlignedMUX1(0)(4)        <= DecoderAlignedBond(1);
                DecoderAlignedMUX1(0)(5)        <= DecoderAlignedBond(1);
                DecoderAlignedMUX1(1)(0)        <= DecoderAlignedBond(1);
                DecoderAlignedMUX1(1)(1)        <= DecoderAlignedBond(1);
                DecoderAlignedMUX1(1)(2)        <= DecoderAlignedBond(2);
                DecoderAlignedMUX1(1)(3)        <= DecoderAlignedBond(2);
                DecoderAlignedMUX1(1)(4)        <= DecoderAlignedBond(2);
                DecoderAlignedMUX1(1)(5)        <= DecoderAlignedBond(2);
                DecoderDeskewed_out(0)(0)       <= DecoderDeskewed_in(0);
                DecoderDeskewed_out(0)(1)       <= DecoderDeskewed_in(0);
                DecoderDeskewed_out(0)(2)       <= DecoderDeskewed_in(0);
                DecoderDeskewed_out(0)(3)       <= DecoderDeskewed_in(0);
                DecoderDeskewed_out(0)(4)       <= DecoderDeskewed_in(1);
                DecoderDeskewed_out(0)(5)       <= DecoderDeskewed_in(1);
                DecoderDeskewed_out(1)(0)       <= DecoderDeskewed_in(1);
                DecoderDeskewed_out(1)(1)       <= DecoderDeskewed_in(1);
                DecoderDeskewed_out(1)(2)       <= DecoderDeskewed_in(2);
                DecoderDeskewed_out(1)(3)       <= DecoderDeskewed_in(2);
                DecoderDeskewed_out(1)(4)       <= DecoderDeskewed_in(2);
                DecoderDeskewed_out(1)(5)       <= DecoderDeskewed_in(2);
            when others => --includes no cb
                DecoderAlignedBond              <= (others => '1');
                EgroupMUX1Data                  <= EgroupRmpData;
                DecoderAlignedMUX1              <= DecoderAlignedRmp;
                DecoderDeskewed_out             <= (others =>(others => '1'));
        end case;
    end process;

end Behavioral;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--RD53B aggregator by Ricardo Luz

-- Use standard library
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.numeric_std.all;
    use IEEE.numeric_std_unsigned.ALL;
    use work.FELIX_package.all;
    use work.package_64b66b.all;
    use work.rd53_package.all;


entity aggregator_64b66b_RD53B is
    port    (
        clk160_in     : in std_logic;
        data_lane_in  : in array_data64V_64b66b_type(0 to 3);
        data_lane_out : out data64V_64b66b_type;
        sep_in        : in std_logic_vector(0 to 3);
        rx_sep_nb_in  : in array_3b(0 to 3);
        sep_out       : out std_logic;
        rx_sep_nb_out : out std_logic_vector(2 downto 0)
    );
end aggregator_64b66b_RD53B;
architecture rtl of aggregator_64b66b_RD53B is
    signal count                  : std_logic_vector(0 to 2) := "111";
    signal data_lane_mono         : array_data64V_64b66b_type(0 to 3) := (others => data64V_64b66b_type_zero);
    signal sep_mono               : std_logic_vector(0 to 3) := (others => '0');
    signal rx_sep_nb_mono         : array_3b(0 to 3) := (others => (others => '0'));
    signal data_lane_out_i        : data64V_64b66b_type := data64V_64b66b_type_zero;
    signal sep_out_i              : std_logic := '0';
    signal rx_sep_nb_out_i        : std_logic_vector(2 downto 0) := (others => '0');
begin

    data_lane_out <= data_lane_out_i;
    sep_out <= sep_out_i;
    rx_sep_nb_out <= rx_sep_nb_out_i;

    select_mono : process(clk160_in)
    begin
        if clk160_in'event and clk160_in='1' then
            if count = "111" then
                data_lane_mono  <= data_lane_in;
                sep_mono        <= sep_in;
                rx_sep_nb_mono  <= rx_sep_nb_in;
                if data_lane_in(0).valid = '1' then
                    count <= "000";
                else
                    count <= count;
                end if;
            else
                count <=count + "001";
            end if;
        end if;
    end process;

    data_lane_out_i.data <= data_lane_mono(0).data when count = "000" or count = "001" else
                            data_lane_mono(1).data when count = "010" or count = "011" else
                            data_lane_mono(2).data when count = "100" or count = "101" else
                            data_lane_mono(3).data when count = "110" or count = "111" else
                            data64V_64b66b_type_zero.data;

    data_lane_out_i.valid <= data_lane_mono(0).valid when count = "001" else
                             data_lane_mono(1).valid when count = "011" else
                             data_lane_mono(2).valid when count = "101" else
                             data_lane_mono(3).valid when count = "111" else
                             data64V_64b66b_type_zero.valid;

    sep_out_i <= sep_mono(0) when count = "000" or count = "001" else
                 sep_mono(1) when count = "010" or count = "011" else
                 sep_mono(2) when count = "100" or count = "101" else
                 sep_mono(3) when count = "110" or count = "111" else
                 '0';

    rx_sep_nb_out_i <= rx_sep_nb_mono(0) when count = "000" or count = "001" else
                       rx_sep_nb_mono(1) when count = "010" or count = "011" else
                       rx_sep_nb_mono(2) when count = "100" or count = "101" else
                       rx_sep_nb_mono(3) when count = "110" or count = "111" else
                       (others => '0');

end rtl;

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

library UNISIM;
    use UNISIM.VCOMPONENTS.all;


entity block_sync_sm_64b66b is
    generic (
        SH_INVALID_CNT_MAX : integer := 16
    );
    port (
        sh_cnt_max_in      : in std_logic_vector(15 downto 0);
        clk40_in           : in std_logic;
        reset              : in std_logic;
        blocksync_out      : out std_logic;
        gearboxslip_out    : out std_logic;
        header_in          : in std_logic_vector(1 downto 0);
        header_v_in        : in std_logic
    );
end block_sync_sm_64b66b;

architecture Behavioral of block_sync_sm_64b66b is
    constant BEGIN_R_ST                   : std_logic_vector(5 downto 0) := "100000";
    constant TEST_SH_ST                   : std_logic_vector(5 downto 0) := "010000";
    constant SH_VALID_ST                  : std_logic_vector(5 downto 0) := "001000";
    constant SH_INVALID_ST                : std_logic_vector(5 downto 0) := "000100";
    constant SLIP_R_ST                    : std_logic_vector(5 downto 0) := "000010";
    constant SYNC_DONE_R_ST               : std_logic_vector(5 downto 0) := "000001";

    signal state                          : std_logic_vector(5 downto 0) := (others => '0');
    signal next_begin_c                   : std_logic;
    signal next_sh_invalid_c              : std_logic;
    signal next_sh_valid_c                : std_logic;
    signal next_slip_c                    : std_logic;
    signal next_sync_done_c               : std_logic;
    signal next_test_sh_c                 : std_logic;
    signal sh_count_equals_max_i          : std_logic;
    signal sh_invalid_cnt_equals_max_i    : std_logic;
    signal sh_invalid_cnt_equals_zero_i   : std_logic;
    signal slip_done_i                    : std_logic;
    signal sync_found_i                   : std_logic;
    signal begin_r                        : std_logic;
    signal sh_invalid_r                   : std_logic;
    signal sh_valid_r                     : std_logic;
    signal slip_count_i                   : std_logic_vector(15 downto 0);
    signal slip_r                         : std_logic;
    signal sync_done_r                    : std_logic;
    signal sync_header_count_i            : std_logic_vector(15 downto 0);
    signal sync_header_invalid_count_i    : std_logic_vector( 9 downto 0);
    signal test_sh_r                      : std_logic;
    signal reset_r                        : std_logic;
    signal reset_r2                       : std_logic;
    signal slip_pulse_i                   : std_logic;
    signal blocksync_out_i                : std_logic;
    signal gearboxslip_out_i            : std_logic;


begin

    gearboxslip_out   <= gearboxslip_out_i;
    blocksync_out     <= blocksync_out_i;
    sync_found_i      <= '1' when (header_in = "01" or header_in = "10") else '0';

    process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            reset_r  <= reset after 1 ns;
            reset_r2 <= reset_r after 1 ns;
        end if;
    end process;

    -----------------------------------
    -----------State Machine-----------
    -----------------------------------
    process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            if(reset_r2 = '1') then
                begin_r      <= '1' after 1 ns;
                test_sh_r    <= '0' after 1 ns;
                sh_valid_r   <= '0' after 1 ns;
                sh_invalid_r <= '0' after 1 ns;
                slip_r       <= '0' after 1 ns;
                sync_done_r  <= '0' after 1 ns;
            else
                begin_r      <= next_begin_c after 1 ns;
                test_sh_r    <= next_test_sh_c after 1 ns;
                sh_valid_r   <= next_sh_valid_c after 1 ns;
                sh_invalid_r <= next_sh_invalid_c after 1 ns;
                slip_r       <= next_slip_c after 1 ns;
                sync_done_r  <= next_sync_done_c after 1 ns;
            end if;
        end if;
    end process;

    state <= begin_r & test_sh_r & sh_valid_r & sh_invalid_r & slip_r & sync_done_r;
    process(all)
        variable  next_begin_c_v,next_test_sh_c_v,next_sh_valid_c_v,next_sh_invalid_c_v,next_slip_c_v,next_sync_done_c_v : std_logic;
    begin
        next_begin_c_v      := '0';
        next_test_sh_c_v    := '0';
        next_sh_valid_c_v   := '0';
        next_sh_invalid_c_v := '0';
        next_slip_c_v       := '0';
        next_sync_done_c_v  := '0';

        case state is
            when BEGIN_R_ST =>
                next_test_sh_c_v        := '1';
            when TEST_SH_ST =>
                if(header_v_in = '1') then
                    if(sync_found_i = '1') then
                        next_sh_valid_c_v   := '1';
                    else
                        next_sh_invalid_c_v := '1';
                    end if;
                else
                    next_test_sh_c_v      := '1';
                end if;
            when SH_VALID_ST =>
                if(sh_count_equals_max_i = '1') then
                    if(sh_invalid_cnt_equals_zero_i = '1') then
                        next_sync_done_c_v  := '1';
                    elsif(sh_invalid_cnt_equals_max_i = '1' or blocksync_out_i = '0') then
                        next_slip_c_v       := '1';
                    else
                        next_begin_c_v      := '1';
                    end if;
                else
                    next_test_sh_c_v      := '1';
                end if;
            when SH_INVALID_ST =>
                if(sh_invalid_cnt_equals_max_i = '1') then
                    next_slip_c_v         := '1';
                else
                    if(blocksync_out_i = '0') then
                        next_slip_c_v       := '1';
                    elsif(sh_count_equals_max_i = '1') then
                        next_begin_c_v      := '1';
                    else
                        next_test_sh_c_v    := '1';
                    end if;
                end if;
            when SLIP_R_ST =>
                if(slip_done_i = '1') then
                    next_begin_c_v        := '1';
                else
                    next_slip_c_v         := '1';
                end if;
            when SYNC_DONE_R_ST =>
                next_begin_c_v          := '1';
            when others =>
                next_begin_c_v          := '1';
        end case;
        next_begin_c      <= next_begin_c_v      ;
        next_test_sh_c    <= next_test_sh_c_v    ;
        next_sh_valid_c   <= next_sh_valid_c_v   ;
        next_sh_invalid_c <= next_sh_invalid_c_v ;
        next_slip_c       <= next_slip_c_v       ;
        next_sync_done_c  <= next_sync_done_c_v  ;

    end process;


    ----------------------------------------------------------------
    -----------Counter keep track of sync headers counted-----------
    ----------------------------------------------------------------

    process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            if (begin_r = '1') then
                sync_header_count_i   <=  (others => '0')               after 1 ns;
            elsif (sh_valid_r = '1' or sh_invalid_r = '1') then
                sync_header_count_i    <=  sync_header_count_i + x"0001" after 1 ns;
            end if;
        end if;
    end process;

    sh_count_equals_max_i <= '1' when sync_header_count_i = sh_cnt_max_in else '0';

    ----------------------------------------------------------------
    -----------Counter keep track of invalid sync headers-----------
    ----------------------------------------------------------------

    process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            if (begin_r = '1') then
                sync_header_invalid_count_i   <=  (others => '0')                            after 1 ns;
            elsif (sh_invalid_r = '1') then
                sync_header_invalid_count_i    <=  sync_header_invalid_count_i + "0000000001" after 1 ns;
            end if;
        end if;
    end process;
    sh_invalid_cnt_equals_max_i  <= '1' when (sync_header_invalid_count_i = SH_INVALID_CNT_MAX) else '0';
    sh_invalid_cnt_equals_zero_i <= '1' when (sync_header_invalid_count_i = "0000000000")       else '0';

    slip_pulse_i <= next_slip_c and not slip_r;
    process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            gearboxslip_out_i   <=  slip_pulse_i;
        end if;
    end process;

    process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            if( slip_r = '0') then
                slip_count_i   <=  (others => '0')                                 after 1 ns;
            else
                slip_count_i   <=  slip_count_i(14 downto 0) & gearboxslip_out_i after 1 ns;
            end if;
        end if;
    end process;
    slip_done_i <= slip_count_i(15);

    process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            if(reset_r2 = '1' or slip_r = '1') then
                blocksync_out_i   <= '0'  after 1 ns;
            elsif (sync_done_r = '1') then
                blocksync_out_i   <=  '1' after 1 ns;
            end if;
        end if;
    end process;

end Behavioral;



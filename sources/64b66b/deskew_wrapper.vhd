--original by Marco Trovado. changed and moved from decoding_64b66b.vhd by Ricardo Luz.
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use IEEE.numeric_std_unsigned.ALL;
    use work.pcie_package.all;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.package_64b66b.all;

library UNISIM;
    use UNISIM.VComponents.all;

entity deskew_wrapper is
    Port (
        cbopt               :  in std_logic_vector(3 downto 0)  ;
        clk40_i             :  in std_logic;
        reset               :  in std_logic;
        DecoderAlignedBond  :  in std_logic_vector(0 to 5);
        EgroupRmpData       :  in array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5);
        DecoderDeskewed     : out std_logic_vector(0 to 5);
        EgroupBondData      : out array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5)
    );
end deskew_wrapper;

architecture Behavioral of deskew_wrapper is
    signal reset_or_not_aligned_i    : std_logic_vector(0 to 5); -- max is 6 for CB 2. 4 for CB 3. 3 for CB 4
    signal EgroupBondData_P_2        : array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5);
    signal EgroupBondData_P_3        : array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5);
    signal EgroupBondData_P_4        : array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5);
    signal DecoderDeskewed_P_2       : std_logic_vector(0 to 5) := (others=> '0');
    signal DecoderDeskewed_P_3       : std_logic_vector(0 to 5) := (others=> '0'); --only 4 used
    signal DecoderDeskewed_P_4       : std_logic_vector(0 to 5) := (others=> '0'); --only 3 used
begin

    sync_reset_proc: process(clk40_i)
    begin
        if rising_edge(clk40_i) then
            reset_or_not_aligned_i(0) <= reset or (not DecoderAlignedBond(0));
            reset_or_not_aligned_i(1) <= reset or (not DecoderAlignedBond(1));
            reset_or_not_aligned_i(2) <= reset or (not DecoderAlignedBond(2));
            reset_or_not_aligned_i(3) <= reset or (not DecoderAlignedBond(3));
            reset_or_not_aligned_i(4) <= reset or (not DecoderAlignedBond(4));
            reset_or_not_aligned_i(5) <= reset or (not DecoderAlignedBond(5));
        end if;
    end process;

    g_link_loop: for link in 0 to 1 generate
        --CB 2
        deskew_64b66b_20: entity work.deskew_64b66b
            port map(
                clk40_in       => clk40_i,
                reset          => reset_or_not_aligned_i(link*3+0),
                dis_lane_in    => "1100",
                data_lane0_in  => EgroupRmpData(link,0),
                data_lane1_in  => EgroupRmpData(link,1),
                data_lane2_in  => data64VHHVE_64b66b_type_zero,
                data_lane3_in  => data64VHHVE_64b66b_type_zero,
                isdeskew_out   => DecoderDeskewed_P_2(link*3+0),
                data_lane0_out => EgroupBondData_P_2(link,0),
                data_lane1_out => EgroupBondData_P_2(link,1),
                data_lane2_out => open,
                data_lane3_out => open
            );

        deskew_64b66b_21: entity work.deskew_64b66b
            port map(
                clk40_in       => clk40_i,
                reset          => reset_or_not_aligned_i(link*3+1),
                dis_lane_in    => "1100",
                data_lane0_in  => EgroupRmpData(link,2),
                data_lane1_in  => EgroupRmpData(link,3),
                data_lane2_in  => data64VHHVE_64b66b_type_zero,
                data_lane3_in  => data64VHHVE_64b66b_type_zero,
                isdeskew_out   => DecoderDeskewed_P_2(link*3+1),
                data_lane0_out => EgroupBondData_P_2(link,2),
                data_lane1_out => EgroupBondData_P_2(link,3),
                data_lane2_out => open,
                data_lane3_out => open
            );

        deskew_64b66b_22: entity work.deskew_64b66b
            port map(
                clk40_in       => clk40_i,
                reset          => reset_or_not_aligned_i(link*3+2),
                dis_lane_in    => "1100",
                data_lane0_in  => EgroupRmpData(link,4),
                data_lane1_in  => EgroupRmpData(link,5),
                data_lane2_in  => data64VHHVE_64b66b_type_zero,
                data_lane3_in  => data64VHHVE_64b66b_type_zero,
                isdeskew_out   => DecoderDeskewed_P_2(link*3+2),
                data_lane0_out => EgroupBondData_P_2(link,4),
                data_lane1_out => EgroupBondData_P_2(link,5),
                data_lane2_out => open,
                data_lane3_out => open
            );

        --CB 3
        deskew_64b66b_30: entity work.deskew_64b66b
            port map(
                clk40_in       => clk40_i,
                reset          => reset_or_not_aligned_i(link*2+0),
                dis_lane_in    => "1000",
                data_lane0_in  => EgroupRmpData(link,0),
                data_lane1_in  => EgroupRmpData(link,1),
                data_lane2_in  => EgroupRmpData(link,2),
                data_lane3_in  => data64VHHVE_64b66b_type_zero,
                isdeskew_out   => DecoderDeskewed_P_3(link*2+0),
                data_lane0_out => EgroupBondData_P_3(link,0),
                data_lane1_out => EgroupBondData_P_3(link,1),
                data_lane2_out => EgroupBondData_P_3(link,2),
                data_lane3_out => open
            );

        deskew_64b66b_31: entity work.deskew_64b66b
            port map(
                clk40_in       => clk40_i,
                reset          => reset_or_not_aligned_i(link*2+1),
                dis_lane_in    => "1000",
                data_lane0_in  => EgroupRmpData(link,3),
                data_lane1_in  => EgroupRmpData(link,4),
                data_lane2_in  => EgroupRmpData(link,5),
                data_lane3_in  => data64VHHVE_64b66b_type_zero,
                isdeskew_out   => DecoderDeskewed_P_3(link*2+1),
                data_lane0_out => EgroupBondData_P_3(link,3),
                data_lane1_out => EgroupBondData_P_3(link,4),
                data_lane2_out => EgroupBondData_P_3(link,5),
                data_lane3_out => open
            );

    end generate; --g_link_loop: for link in 0 to 1 generate

    --CB 4
    deskew_64b66b_40: entity work.deskew_64b66b
        port map(
            clk40_in       => clk40_i,
            reset          => reset_or_not_aligned_i(0),
            dis_lane_in    => "0000",
            data_lane0_in  => EgroupRmpData(0,0),
            data_lane1_in  => EgroupRmpData(0,1),
            data_lane2_in  => EgroupRmpData(0,2),
            data_lane3_in  => EgroupRmpData(0,3),
            isdeskew_out   => DecoderDeskewed_P_4(0),
            data_lane0_out => EgroupBondData_P_4(0,0),
            data_lane1_out => EgroupBondData_P_4(0,1),
            data_lane2_out => EgroupBondData_P_4(0,2),
            data_lane3_out => EgroupBondData_P_4(0,3)
        );

    deskew_64b66b_41: entity work.deskew_64b66b
        port map(
            clk40_in       => clk40_i,
            reset          => reset_or_not_aligned_i(1),
            dis_lane_in    => "0000",
            data_lane0_in  => EgroupRmpData(0,4),
            data_lane1_in  => EgroupRmpData(0,5),
            data_lane2_in  => EgroupRmpData(1,0),
            data_lane3_in  => EgroupRmpData(1,1),
            isdeskew_out   => DecoderDeskewed_P_4(1),
            data_lane0_out => EgroupBondData_P_4(0,4),
            data_lane1_out => EgroupBondData_P_4(0,5),
            data_lane2_out => EgroupBondData_P_4(1,0),
            data_lane3_out => EgroupBondData_P_4(1,1)
        );

    deskew_64b66b_42: entity work.deskew_64b66b
        port map(
            clk40_in       => clk40_i,
            reset          => reset_or_not_aligned_i(2),
            dis_lane_in    => "0000",
            data_lane0_in  => EgroupRmpData(1,2),
            data_lane1_in  => EgroupRmpData(1,3),
            data_lane2_in  => EgroupRmpData(1,4),
            data_lane3_in  => EgroupRmpData(1,5),
            isdeskew_out   => DecoderDeskewed_P_4(2),
            data_lane0_out => EgroupBondData_P_4(1,2),
            data_lane1_out => EgroupBondData_P_4(1,3),
            data_lane2_out => EgroupBondData_P_4(1,4),
            data_lane3_out => EgroupBondData_P_4(1,5)
        );

    DecoderDeskewed <=  DecoderDeskewed_P_2 when cbopt = x"2" else
                       DecoderDeskewed_P_3 when cbopt = x"3" else
                       DecoderDeskewed_P_4 when cbopt = x"4" else
                       (others => '0');

    EgroupBondData  <=  EgroupBondData_P_2 when cbopt = x"2" else
                       EgroupBondData_P_3 when cbopt = x"3" else
                       EgroupBondData_P_4 when cbopt = x"4" else
                       (others => (others => data64VHHVE_64b66b_type_zero));
end Behavioral;

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use work.package_64b66b.all;

library UNISIM;
    use UNISIM.VCOMPONENTS.all;


entity symbolDecoding_64b66b is
    port (
        reset                : in std_logic;
        clk40_in             : in std_logic;
        enable_err_detect_in : in std_logic;
        mask_k_char_in       : in std_logic_vector(0 to 3);
        lane_up_in           : in std_logic;
        data_in              : in data64VHHVE_64b66b_type;
        data_out             : out data64V_64b66b_type;
        sep_out              : out std_logic;
        sep_nb_out           : out std_logic_vector(2 downto 0);
        datadcs_out          : out data64V_64b66b_type;
        soft_err_out         : out std_logic
    );
end symbolDecoding_64b66b;

architecture Behavioral of symbolDecoding_64b66b is
    signal   soft_err_detect : std_logic;
    signal   sync_header_r   : std_logic_vector(1 downto 0)  := (others => '0');
    signal   rxdatavalid_r   : std_logic                     := '0';
    signal   sep_r           : std_logic;
    signal   sep7_r          : std_logic;
    signal   sep_nb_r        : std_logic_vector(2 downto 0);
    signal   data_i          : data64VHHVE_64b66b_type       := data64VHHVE_64b66b_type_zero;
    signal   sep_c_i         : std_logic;
    signal   sep7_c_i        : std_logic;
    signal   userk_c_i       : std_logic;
    signal   na_idle_c_i     : std_logic;
    signal   idle_c_i        : std_logic;
    signal   cc_c_i          : std_logic;
    signal   cb_c_i          : std_logic;
    signal   valid_c_i       : std_logic;
    signal   valid_d_i       : std_logic;
    signal   illegal_btf_i   : std_logic;

    signal   reset_reg       : std_logic;
    signal   pe_data_i       : data64V_64b66b_type;
    signal   user_k          : std_logic;
    signal mask_k            : std_logic;
    signal user_k_blk_no_i   : std_logic_vector(0 to 3);

begin
    process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            reset_reg  <=  reset;
        end if;
    end process;


    process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            if(reset_reg = '1') then
                data_i     <= data64VHHVE_64b66b_type_zero;
            elsif(data_in.en = '1') then
                data_i     <= data_in;
            end if;
        end if;
    end process;

    valid_c_i    <= '1' when ((data_i.hdr = "10") and data_i.valid = '1')                          else '0';
    valid_d_i    <= '1' when ((data_i.hdr = "01") and data_i.valid = '1')                          else '0';
    na_idle_c_i  <= '1' when (valid_c_i = '1'     and (data_i.data(63 downto 48) = NA_IDLE_BTF))   else '0';
    cc_c_i       <= '1' when (valid_c_i = '1'     and ((data_i.data(63 downto 48) = CC_BTF_SA0)    or
                                                        (data_i.data(63 downto 48) = CC_BTF_SA1)))  else '0';
    cb_c_i       <= '1' when (valid_c_i = '1'     and (data_i.data(63 downto 48) = CB_CHARACTER))  else '0';
    idle_c_i     <= '1' when ((data_i.hdr = "10") and (data_i.data(63 downto 48) = IDLE_BTF))      else '0';
    userk_c_i    <= '1' when (valid_c_i = '1'     and ((data_i.data(63 downto 56) = USER_K_BTF0)   or
                                                        (data_i.data(63 downto 56) = USER_K_BTF1)   or
                                                        (data_i.data(63 downto 56) = USER_K_BTF2)   or
                                                        (data_i.data(63 downto 56) = USER_K_BTF3)   or
                                                        (data_i.data(63 downto 56) = USER_K_BTF4)   or
                                                        (data_i.data(63 downto 56) = USER_K_BTF5)   or
                                                        (data_i.data(63 downto 56) = USER_K_BTF6)   or
                                                        (data_i.data(63 downto 56) = USER_K_BTF7)   or
                                                        (data_i.data(63 downto 56) = USER_K_BTF8))) else '0';
    sep_c_i      <= '1' when (valid_c_i = '1'     and (data_i.data(63 downto 56) = SEP_BTF))       else '0';
    sep7_c_i     <= '1' when (valid_c_i = '1'     and (data_i.data(63 downto 56) = SEP7_BTF))      else '0';


    illegal_btf_i   <= lane_up_in and
                       valid_c_i  and
                       not (na_idle_c_i or (idle_c_i and data_i.valid) or cc_c_i or cb_c_i or sep_c_i or sep7_c_i or userk_c_i ) when reset_reg = '1'
                         else '0';

    process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            if(reset_reg = '1') then
                rxdatavalid_r   <=  '0';
                sync_header_r   <=  "00";
                sep_r           <= '0';
                sep7_r          <= '0';
                sep_nb_r        <= (others => '0');

            elsif(data_in.en = '1') then
                rxdatavalid_r   <=  data_i.valid;
                sep_r           <= sep_c_i;
                sep7_r          <=  sep7_c_i;

                if(data_i.valid = '1') then
                    sync_header_r <=  data_i.hdr;
                end if;

                if(sep_c_i = '1') then
                    sep_nb_r <=  data_i.data(50 downto 48);
                elsif(sep7_c_i = '1') then
                    sep_nb_r <=  "111";
                else
                    sep_nb_r <=  (others => '0');
                end if;
            end if;
        end if;
    end process;

    process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            if(reset_reg = '1') then
                pe_data_i.data  <= (others => '0');
            elsif(data_in.en = '1') then
                if(sep_c_i = '1') then
                    pe_data_i.data     <=  x"0000" & data_i.data(47 downto 0);
                elsif(sep7_c_i = '1') then
                    pe_data_i.data     <=  x"00" & data_i.data(55 downto 0);
                elsif(valid_d_i = '1' and not (sep_c_i = '1' or sep7_c_i = '1')) then
                    pe_data_i.data     <=  data_i.data;
                elsif(userk_c_i = '1') then
                    pe_data_i.data     <=  data_i.data;
                else
                    pe_data_i.data  <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    pe_data_i.valid  <= '1' when ( ((sync_header_r = "01") and rxdatavalid_r = '1') or
                                   (sep7_r = '1')                                   or
                                   (sep_r  = '1') )                                 else
                        '0';

    process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            if(reset_reg = '1') then
                user_k      <= '0';
                user_k_blk_no_i  <= (others => '0');
            elsif(data_in.en = '1') then
                user_k <= userk_c_i;
                if (data_i.hdr = "10") then
                    case (data_i.data(63 downto 56)) is
                        when USER_K_BTF0 => user_k_blk_no_i <=  x"0";
                        when USER_K_BTF1 => user_k_blk_no_i <=  x"1";
                        when USER_K_BTF2 => user_k_blk_no_i <=  x"2";
                        when USER_K_BTF3 => user_k_blk_no_i <=  x"3";
                        when USER_K_BTF4 => user_k_blk_no_i <=  x"4";
                        when USER_K_BTF5 => user_k_blk_no_i <=  x"5";
                        when USER_K_BTF6 => user_k_blk_no_i <=  x"6";
                        when USER_K_BTF7 => user_k_blk_no_i <=  x"7";
                        when USER_K_BTF8 => user_k_blk_no_i <=  x"8";
                        when others      => user_k_blk_no_i <=  x"f";
                    end case;
                end if;
            end if;
        end if;
    end process;

    soft_err_detect <= '1' when (((data_i.hdr(0) = data_i.hdr(1)) or illegal_btf_i = '1') and data_i.valid = '1') else '0';
    process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            if(reset_reg = '1') then
                soft_err_out <= '0';
            elsif(data_in.en = '1') then
                if(enable_err_detect_in = '1' and soft_err_detect = '1') then
                    soft_err_out   <=  '1';
                else
                    soft_err_out   <= '0';
                end if;
            end if;
        end if;
    end process;

    sep_out           <= sep_r;
    sep_nb_out        <= sep_nb_r;

    mask_k            <= '0' when (user_k_blk_no_i = mask_k_char_in) else '1';
    data_out.data     <= pe_data_i.data when (user_k='0') else (others => '0');
    data_out.valid    <= pe_data_i.valid and data_in.en;
    datadcs_out.data  <= pe_data_i.data when (user_k='1') else (others => '0');
    datadcs_out.valid <= (user_k and data_in.en) and mask_k;



end Behavioral;


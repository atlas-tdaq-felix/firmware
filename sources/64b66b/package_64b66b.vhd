library ieee;
    use ieee.std_logic_1164.all;
    use IEEE.numeric_std_unsigned.ALL;


package package_64b66b is
    constant CC_BTF_SA0        : std_logic_vector(15 downto 0) :=   x"7890";
    constant CC_BTF_SA1        : std_logic_vector(15 downto 0) :=   x"7880";
    constant NA_IDLE_BTF       : std_logic_vector(15 downto 0) :=   x"7830";
    constant CB_CHARACTER      : std_logic_vector(15 downto 0) :=   x"7840";
    constant IDLE_BTF          : std_logic_vector(15 downto 0) :=   x"7810";
    constant SEP_BTF           : std_logic_vector( 7 downto 0) :=   x"1E";
    constant SEP7_BTF          : std_logic_vector( 7 downto 0) :=   x"E1";
    constant USER_K_BTF0       : std_logic_vector( 7 downto 0) :=   x"D2";
    constant USER_K_BTF1       : std_logic_vector( 7 downto 0) :=   x"99";
    constant USER_K_BTF2       : std_logic_vector( 7 downto 0) :=   x"55";
    constant USER_K_BTF3       : std_logic_vector( 7 downto 0) :=   x"B4";
    constant USER_K_BTF4       : std_logic_vector( 7 downto 0) :=   x"CC";
    constant USER_K_BTF5       : std_logic_vector( 7 downto 0) :=   x"66";
    constant USER_K_BTF6       : std_logic_vector( 7 downto 0) :=   x"33";
    constant USER_K_BTF7       : std_logic_vector( 7 downto 0) :=   x"4B";
    constant USER_K_BTF8       : std_logic_vector( 7 downto 0) :=   x"87";

    type data32VL_64b66b_type is record
        data      : std_logic_vector(31 downto 0);
        valid     : std_logic;
        tlast     : std_logic;
    end record;

    type data32VHHV_64b66b_type is record
        data      : std_logic_vector(31 downto 0);
        valid     : std_logic;
        hdr       : std_logic_vector( 1 downto 0);
        hdr_valid  : std_logic;
    end record;

    type data64V_64b66b_type is record
        data      : std_logic_vector(63 downto 0);
        valid     : std_logic;
    end record;

    type data64VHHVE_64b66b_type is record
        data      : std_logic_vector(63 downto 0);
        valid     : std_logic;
        hdr       : std_logic_vector( 1 downto 0);
        hdr_valid  : std_logic;
        en        : std_logic;
    end record;

    constant data32VL_64b66b_type_zero : data32VL_64b66b_type := (
                                                                   tlast    => '0',
                                                                   valid    => '0',
                                                                   data     => (others => '0')
                                                                 );

    constant data32VHHV_64b66b_type_zero : data32VHHV_64b66b_type := (
                                                                       data     => (others => '0'),
                                                                       valid    => '0',
                                                                       hdr      => (others => '0'),
                                                                       hdr_valid => '0'
                                                                     );

    constant data64V_64b66b_type_zero : data64V_64b66b_type := (
                                                                 data     => (others => '0'),
                                                                 valid    => '0'
                                                               );

    constant data64VHHVE_64b66b_type_zero : data64VHHVE_64b66b_type := (
                                                                         data     => (others => '0'),
                                                                         valid    => '0',
                                                                         hdr      => (others => '0'),
                                                                         hdr_valid => '0',
                                                                         en       => '0'
                                                                       );

    type array_data32VL_64b66b_type    is array (natural range <>) of data32VL_64b66b_type;
    type array_4b_rev  is array (natural range <>) of std_logic_vector(0 to 3);
    type array_data32VHHV_64b66b_type  is array (natural range <>) of data32VHHV_64b66b_type;
    type array_data64V_64b66b_type     is array (natural range <>) of data64V_64b66b_type;
    type array_data64VHHVE_64b66b_type is array (natural range <>) of data64VHHVE_64b66b_type;
    type array_2d_data64VHHVE_64b66b_type is array (natural range <>, natural range <>) of data64VHHVE_64b66b_type;

    type array_2d_data32VHHV_64b66b_type     is array (natural range <>, natural range <>) of data32VHHV_64b66b_type;
    type array_2d_data64V_64b66b_type        is array (natural range <>, natural range <>) of data64V_64b66b_type;
    type array_2d_data32VL_64b66b_type       is array (natural range <>, natural range <>) of data32VL_64b66b_type;

end package package_64b66b ;

package body package_64b66b is


end package body package_64b66b ;



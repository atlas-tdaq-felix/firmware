--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company: Argonne National Laboratory
-- Engineer: Marco Trovato
--
-- Create Date: 06/06/2019 08:38:50 AM
-- Design Name:
-- Module Name: decoding_64b66b - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    --use work.axi_stream_package.all;
    use ieee.numeric_std.all;
    use IEEE.numeric_std_unsigned.ALL;
    use work.pcie_package.all;
    --use work.lpgbtfpga_package.all;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.package_64b66b.all;

library UNISIM;
    use UNISIM.VComponents.all;

entity decoding_64b66b is
    generic (
        RD53Version : String := "B"--A or B
    );
    Port (
        useEmulator                     :  in std_logic_vector(0 to 1);

        cbopt                           :  in std_logic_vector(3 downto 0); --2, 3, or 4 for CB with the that number of channels.0 or
        mask_k_char                     :  in array_4b(0 to 1);
        MsbFirst                        :  in std_logic;

        reset                           :  in std_logic;
        clk40_in                        :  in std_logic;
        clk160_in                       :  in std_logic;

        LinkAligned_in                  :  in std_logic_vector(0 to 1);
        DataIn                          :  in array_224b(0 to 1); --LPGBT Data
        DataOut                         : out array_2d_data32VL_64b66b_type(0 to 1, 0 to 5);
        BytesKeepOut                    : out array_2d_4b(0 to 1, 0 to 5);
        DataDCSOut                      : out array_2d_data32VL_64b66b_type(0 to 1, 0 to 5);
        DecoderAligned_out              : out array_inv_6b(0 to 1);
        DecoderDeskewed_out             : out array_inv_6b(0 to 1);

        soft_err                        : out array_inv_6b(0 to 1);
        soft_err_cnt                    : out array_2d_32b(0 to 1, 0 to 5);
        soft_err_rst                    :  in std_logic_vector(0 to 1); --Active high reset

        --debug (regmap)
        cnt_rx_64b66bhdr_out            : out array_32b(0 to 1); --summer over all egroups
        ref_packet_in                   :  in array_32b(0 to 1);

        EgroupGB1Data_dbg               : out array_2d_data32VHHV_64b66b_type(0 to 1, 0 to 5);
        EgroupUnscrData_dbg             : out array_2d_data32VHHV_64b66b_type(0 to 1, 0 to 5);
        EgroupGB2Data_dbg               : out array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5);
        EgroupRmpData_dbg               : out array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5);
        EgroupDecData_dbg               : out array_2d_data64V_64b66b_type(0 to 1, 0 to 5);
        DCSDecData_dbg                  : out array_2d_data64V_64b66b_type(0 to 1, 0 to 5);
        EgroupMUX1Data_dbg              : out array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5);
        EgroupBondData_dbg              : out array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5);
        DecoderAlignedRmp_dbg           : out array_inv_6b(0 to 1);
        DecoderAlignedMUX1_dbg          : out array_inv_6b(0 to 1);
        DecoderAlignedBond_dbg          : out std_logic_vector(0 to 5);

        EgroupAggrData_dbg              : out array_data64V_64b66b_type(0 to 5);--out array_data32VL_64b66b_type(0 to 5); --std_logic_vector(33 downto 0);
        DCSDecDataSplit_clk160_dbg      : out array_2d_data32VL_64b66b_type(0 to 1, 0 to 5); --array_34b(0 to 5);
        DecoderAlignedDec_clk160_dbg    : out array_inv_6b(0 to 1);--std_logic_vector(0 to 5)

        EgroupDecData_clk160_dbg        : out array_2d_data64V_64b66b_type(0 to 1, 0 to 5);
        EgroupMUX2Data_clk160_dbg       : out array_2d_data64V_64b66b_type(0 to 1, 0 to 5);
        EgroupMUX2DataSplit_clk160_dbg  : out array_2d_data32VL_64b66b_type(0 to 1, 0 to 5)
    --

    );
end decoding_64b66b;


architecture Behavioral of decoding_64b66b is

    signal clk40_i                   : std_logic;
    signal clk160                    : std_logic;
    signal cbopt_clk160              : array_4b(1 downto 0) := (others => (others => '0'));
    signal cbopt_clk40               : array_4b(3 downto 0) := (others => (others => '0'));
    signal en_clk160                 : std_logic := '0';
    signal enable_err_detect_i       : array_inv_6b(0 to 1);
    -------------------
    ------clk40--------
    -------------------
    signal DecoderAligned            : array_inv_6b(0 to 1);
    signal DecoderAlignedMUX1        : array_inv_6b(0 to 1) := (others => (others => '0'));
    signal EgroupGB1Data             : array_2d_data32VHHV_64b66b_type(0 to 1, 0 to 5);
    signal EgroupUnscrData           : array_2d_data32VHHV_64b66b_type(0 to 1, 0 to 5);
    signal EgroupGB2Data             : array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5);
    signal EgroupRmpData             : array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5);
    signal EgroupDecData             : array_2d_data64V_64b66b_type(0 to 1, 0 to 5);
    signal DCSDecData                : array_2d_data64V_64b66b_type(0 to 1, 0 to 5);
    signal EgroupMUX1Data            : array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5)  := (others => (others => data64VHHVE_64b66b_type_zero));
    signal EgroupBondData            : array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5);
    signal DecoderAlignedBond        : std_logic_vector(0 to 5);
    signal DecoderAlignedRmp         : array_inv_6b(0 to 1);
    signal DecoderDeskewed_vector    : std_logic_vector(0 to 5);
    signal sep                       : array_inv_6b(0 to 1);
    signal rx_sep_nb                 : array_2d_3b(0 to 1, 0 to 5);
    signal soft_err_cnt_i            : array_2d_32b(0 to 1, 0 to 5)                        := (others =>(others => (others => '0')));
    -------------------
    ------clk160--------
    -------------------
    signal EgroupBytesKeep_clk160       : array_2d_4b(0 to 1, 0 to 5)                      := (others => (others => "0000"));
    signal DCSDecDataSplit_clk160       : array_2d_data32VL_64b66b_type(0 to 1, 0 to 5)    := (others => (others => data32VL_64b66b_type_zero));
    signal EgroupDCSDataNew_clk160      : array_2d_data32VL_64b66b_type(0 to 1, 0 to 5)    := (others => (others => data32VL_64b66b_type_zero));
    signal DecoderAlignedDec_clk160     : array_inv_6b(0 to 1)                             := (others => (others => '0'));
    --signal DecoderAlignedMUX2_clk160    : std_logic_vector(0 to 5)                         := (others => '0');
    signal EgroupAggrData               : array_data64V_64b66b_type(0 to 5)               := (others => data64V_64b66b_type_zero);
    signal EgroupDecData_clk160         : array_2d_data64V_64b66b_type(0 to 1, 0 to 5);
    signal DCSDecData_clk160            : array_2d_data64V_64b66b_type(0 to 1, 0 to 5);
    signal EgroupMUX2Data_clk160        : array_2d_data64V_64b66b_type(0 to 1, 0 to 5)    := (others => (others => data64V_64b66b_type_zero));
    signal EgroupMUX2DataSplit_clk160   : array_2d_data32VL_64b66b_type(0 to 1, 0 to 5)    := (others => (others => data32VL_64b66b_type_zero));

    --SEPERATOR
    signal sep_clk160                   : array_inv_6b(0 to 1);
    signal rx_sep_nb_clk160             : array_2d_3b(0 to 1, 0 to 5);
    signal sep_Aggr                     : std_logic_vector(0 to 5);
    signal rx_sep_nb_Aggr               : array_3b(0 to 5);
    signal sepMUX2_clk160               : array_inv_6b(0 to 1);
    signal rx_sep_nbMUX2_clk160         : array_2d_3b(0 to 1, 0 to 5);

begin

    --    cboptclk160_proc: process(clk160)
    --    begin
    --        if rising_edge(clk160) then
    --            cbopt_clk160 <= cbopt;
    --        end if;
    --    end process;

    clk40_i <= clk40_in;
    clk160  <= clk160_in;

    DecoderAligned_out <= DecoderAligned;

    g_links_upto_remaper: for link in 0 to 1 generate
        signal gearboxslip_i             : std_logic_vector(0 to 5);
        signal lane_up_i                 : std_logic_vector(0 to 5);
    begin
        --------------------------------------
        ------  LANE INITIALIZATION ---------
        --------------------------------------

        g_LPGBT_Egroups_laneInit: for egroup in 0 to 5 generate
            signal lane_reset: std_logic;
        begin
            lane_reset                      <= reset or (not LinkAligned_in(link));
            DecoderAligned(link)(egroup)    <= lane_up_i(egroup);
            laneInit_64b66b_i : entity work.laneInit_64b66b
                port map
              (
                    useEmulator               => useEmulator(link),
                    reset                     => lane_reset,
                    clk40_in                  => clk40_i,
                    header_in                 => EgroupUnscrData(link,egroup).hdr,
                    header_v_in               => EgroupUnscrData(link,egroup).hdr_valid,
                    gearboxslip_out           => gearboxslip_i(egroup),
                    enable_err_detect_out     => enable_err_detect_i(link)(egroup),
                    lane_up_out               => lane_up_i(egroup)
                );
        end generate;

        --------------------------------------
        ------------ MAIN DATASTREAM ---------
        ------------------CLK40---------------
        --------------------------------------

        g_LPGBT_Egroups_decodingGearbox: for egroup in 0 to 5 generate
        begin
            decodingGearbox_64b66b_i: entity work.decodingGearbox_64b66b
                generic map(
                    IN_WIDTH               => 32,
                    GB_OUT_WIDTH           => 66
                )
                port map(
                    MsbFirst               => MsbFirst,
                    Reset                  => reset or (not LinkAligned_in(link)),
                    clk40                  => clk40_i,
                    DataIn                 => DataIn(link)(egroup*32+31 downto egroup*32),
                    LinkAligned            => LinkAligned_in(link),
                    DataOut                => EgroupGB1Data(link,egroup).data,
                    DataOutValid           => EgroupGB1Data(link,egroup).valid,
                    HeaderOut              => EgroupGB1Data(link,egroup).hdr,
                    HeaderOutValid         => EgroupGB1Data(link,egroup).hdr_valid,
                    BitSlip                => gearboxslip_i(egroup)
                );
        end generate;


        g_LPGBT_Egroups_descrambler: for egroup in 0 to 5 generate
            signal system_reset: std_logic;
        begin
            system_reset <= reset or (not LinkAligned_in(link)) or (not DecoderAligned(link)(egroup));
            descrambler_64b66b_i : entity work.descrambler_64b66b
                generic map (
                    RX_DATA_WIDTH  => 32
                )
                port map
              (
                    clk40_in       => clk40_i,
                    reset          => system_reset,
                    data_in        => EgroupGB1Data(link,egroup).data,
                    data_v_in      => EgroupGB1Data(link,egroup).valid,
                    data_out       => EgroupUnscrData(link,egroup).data
                );
        end generate;

        g_LPGBT_Egroups_gearbox32to64: for egroup in 0 to 5 generate
            delay_descrambler: process(clk40_i)
            begin
                if rising_edge(clk40_i) then
                    EgroupUnscrData(link,egroup).valid     <= EgroupGB1Data(link,egroup).valid;
                    EgroupUnscrData(link,egroup).hdr       <= EgroupGB1Data(link,egroup).hdr;
                    EgroupUnscrData(link,egroup).hdr_valid <= EgroupGB1Data(link,egroup).hdr_valid;
                end if;
            end process;

            gearbox32to64_64b66b_i : entity work.gearbox32to64_64b66b
                port map
                (
                    reset    => reset or (not LinkAligned_in(link)) or (not DecoderAligned(link)(egroup)),
                    clk40_in => clk40_i,
                    data_in  => EgroupUnscrData(link,egroup),
                    data_out => EgroupGB2Data(link,egroup)
                );

        end generate;

    end generate; --g_links_upto_remaper: for link in 0 to 1 generate

    cboptclk40_fanout_proc: process(clk40_i)
    begin
        if rising_edge(clk40_i) then
            cbopt_clk40(3)<= cbopt;
            cbopt_clk40(2)<= cbopt;
            cbopt_clk40(1)<= cbopt;
            cbopt_clk40(0)<= cbopt;
        end if;
    end process;

    remapEpaths_64b66b_i: entity work.remapEpaths_64b66b
        port map(
            CBOPT       => cbopt_clk40(0),
            DataIn      => EgroupGB2Data,
            AlignedIn   => DecoderAligned,
            DataOut     => EgroupRmpData,
            AlignedOut  => DecoderAlignedRmp
        );

    deskew_wrapper_i: entity work.deskew_wrapper
        port map(
            cbopt               => cbopt_clk40(2),
            clk40_i             => clk40_i,
            reset               => reset,
            DecoderAlignedBond  => DecoderAlignedBond,
            EgroupRmpData       => EgroupRmpData,
            DecoderDeskewed     => DecoderDeskewed_vector,
            EgroupBondData      => EgroupBondData
        );

    mux1_64b66b_i: entity work.mux1_64b66b
        port map(
            cbopt                   => cbopt_clk40(2),
            DecoderAlignedRmp       => DecoderAlignedRmp,
            EgroupRmpData           => EgroupRmpData,
            DecoderDeskewed_in      => DecoderDeskewed_vector,
            EgroupBondData          => EgroupBondData,
            DecoderDeskewed_out     => DecoderDeskewed_out,
            DecoderAlignedBond_out  => DecoderAlignedBond,
            DecoderAlignedMUX1_out  => DecoderAlignedMUX1,
            EgroupMUX1Data_out      => EgroupMUX1Data
        );

    g_links_symbolDecoding: for link in 0 to 1 generate
    begin

        soft_err_cnt <= soft_err_cnt_i;
        g_LPGBT_Egroups_symbolDecoding: for egroup in 0 to 5 generate
            symbolDecoding_64b66b_i : entity work.symbolDecoding_64b66b
                port map
          (
                    reset                    => reset or (not LinkAligned_in(link)),
                    clk40_in                 => clk40_i,
                    enable_err_detect_in     => enable_err_detect_i(link)(egroup),
                    mask_k_char_in           => mask_k_char(link),
                    lane_up_in               => DecoderAlignedMUX1(link)(egroup),
                    data_in                  => EgroupMUX1Data(link,egroup),
                    data_out                 => EgroupDecData(link,egroup),
                    sep_out                  => sep(link)(egroup),
                    sep_nb_out               => rx_sep_nb(link,egroup),
                    datadcs_out              => DCSDecData(link,egroup),
                    soft_err_out             => soft_err(link)(egroup)
                );

            err_cnt: process(clk40_i)
            begin
                if rising_edge(clk40_i) then
                    if soft_err_rst(link) = '1' then
                        soft_err_cnt_i(link,egroup) <= (others => '0');
                    elsif soft_err(link)(egroup) = '1' and soft_err_cnt_i(link,egroup) /= x"FFFF_FFFF" then
                        soft_err_cnt_i(link,egroup) <= soft_err_cnt_i(link,egroup) + x"1";
                    else
                        soft_err_cnt_i(link,egroup) <= soft_err_cnt_i(link,egroup);
                    end if;
                end if;
            end process;

        end generate;

    end generate; --g_links_symbolDecoding: for link in 0 to NumLinksIN-1 generate

    --------------------------------------
    --------  MAIN DATASTREAM ------------
    ---------------CLK160-----------------
    --------------------------------------
    --clk160_in=clk160 and clk40 since are generated from the same MMCM, whose source is ttc clk, so this simple crossing plus en_clk160 should work
    cross_clk40to160: process(clk160)
    begin
        if rising_edge(clk160) then
            EgroupDecData_clk160      <= EgroupDecData;
            DecoderAlignedDec_clk160  <= DecoderAlignedMUX1;
            for link in 0 to 1 loop
                for egroup in 0 to 5 loop
                    DCSDecData_clk160(link,egroup).data  <= DCSDecData(link,egroup).data;
                    DCSDecData_clk160(link,egroup).valid <= en_clk160 and DCSDecData(link,egroup).valid;
                end loop;
            end loop;
            sep_clk160                <= sep;
            rx_sep_nb_clk160          <= rx_sep_nb;
        end if;
    end process;

    enclk160_proc : process(clk160)
        variable count : integer range 0 to 3 := 0;
    begin
        if rising_edge(clk160) then
            cbopt_clk160(1) <= cbopt_clk40(3);
            cbopt_clk160(0) <= cbopt_clk40(3);
            if(count=0) then
                en_clk160 <= '1';
                count   := count + 1;
            elsif (count=3) then
                count   := 0;
                en_clk160 <= '0';
            else
                count   := count + 1;
                en_clk160 <= '0';
            end if;
        end if;
    end process;

    aggregator_wrapper_i: entity work.aggregator_wrapper
        port map(
            cbopt                       => cbopt_clk160(0),
            clk160_in                   => clk160,
            --reset                       => reset,
            --DecoderAlignedMUX2_clk160   => DecoderAlignedMUX2_clk160,
            EgroupDecData_clk160        => EgroupDecData_clk160,
            sep_clk160                  => sep_clk160,
            rx_sep_nb_clk160            => rx_sep_nb_clk160,
            EgroupAggrData              => EgroupAggrData,
            sep_Aggr                    => sep_Aggr,
            rx_sep_nb_Aggr              => rx_sep_nb_Aggr
        );

    mux2_64b66b_i: entity work.mux2_64b66b
        port map(
            cbopt_clk160                => cbopt_clk160(1),
            clk160_in                   => clk160,
            en_clk160_in                => en_clk160,
            DecoderAlignedDec_clk160    => DecoderAlignedDec_clk160,
            EgroupAggrData              => EgroupAggrData,
            EgroupDecData_clk160        => EgroupDecData_clk160,
            sep_clk160                  => sep_clk160,
            sep_Aggr                    => sep_Aggr,
            rx_sep_nb_Aggr              => rx_sep_nb_Aggr,

            DecoderAlignedMUX2_clk160   => open, --DecoderAlignedMUX2_clk160,
            EgroupMUX2Data_clk160_out   => EgroupMUX2Data_clk160,
            sepMUX2_clk160              => sepMUX2_clk160,
            rx_sep_nbMUX2_clk160        => rx_sep_nbMUX2_clk160

        );


    g_links_RD53Interface: for link in 0 to 1 generate
        signal cnt_rx_64b66bhdr_i        : array_32b(0 to 5);
    begin

        g_LPGBT_Egroups_RD53Interface: for egroup in 0 to 5 generate
            RD53Interface_i : entity work.RD53Interface
                generic map (
                    RD53Version          => RD53Version
                )
                PORT MAP (
                    Reset                => reset or (not LinkAligned_in(link)),
                    clk_in               => clk160,
                    data_in              => EgroupMUX2Data_clk160(link,egroup),
                    sep_in               => sepMUX2_clk160(link)(egroup),
                    rx_sep_nb            => rx_sep_nbMUX2_clk160(link,egroup),
                    data_k_in            => DCSDecData_clk160(link,egroup),
                    data_out             => EgroupMUX2DataSplit_clk160(link,egroup),
                    byteskeep_out        => EgroupBytesKeep_clk160(link,egroup),
                    datadcs_out          => DCSDecDataSplit_clk160(link,egroup),
                    --debug info
                    cnt_rx_64b66bhdr_out => cnt_rx_64b66bhdr_i(egroup),
                    ref_packet_in        => ref_packet_in(link)
                );
        end generate;--g_LPGBT_Egroups_RD53Interface: for egroup in 0 to 5 generate
        --sum over all egroups
        cnt_rx_64b66bhdr_out(link) <= std_logic_vector(unsigned(cnt_rx_64b66bhdr_i(0)) + unsigned(cnt_rx_64b66bhdr_i(1)) + unsigned(cnt_rx_64b66bhdr_i(2)) + unsigned(cnt_rx_64b66bhdr_i(3)) + unsigned(cnt_rx_64b66bhdr_i(4)) + unsigned(cnt_rx_64b66bhdr_i(5)));

    end generate; --g_links_RD53Interface: for link in 0 to NumLinksIN-1 generate

    EgroupDCSDataNew_clk160         <= DCSDecDataSplit_clk160;
    DataOut                         <= EgroupMUX2DataSplit_clk160;
    BytesKeepOut                    <= EgroupBytesKeep_clk160;
    DataDCSOut                      <= EgroupDCSDataNew_clk160;

    --debug
    EgroupGB1Data_dbg               <= EgroupGB1Data;
    EgroupUnscrData_dbg             <= EgroupUnscrData;
    EgroupGB2Data_dbg               <= EgroupGB2Data;
    EgroupRmpData_dbg               <= EgroupRmpData;
    EgroupDecData_dbg               <= EgroupDecData;
    DCSDecData_dbg                  <= DCSDecData;
    EgroupMUX1Data_dbg              <= EgroupMUX1Data;
    EgroupBondData_dbg              <= EgroupBondData;
    DecoderAlignedRmp_dbg           <= DecoderAlignedRmp;
    DecoderAlignedMUX1_dbg          <= DecoderAlignedMUX1;
    DecoderAlignedBond_dbg          <= DecoderAlignedBond;
    EgroupMUX2Data_clk160_dbg       <= EgroupMUX2Data_clk160;
    EgroupAggrData_dbg              <= EgroupAggrData;
    DCSDecDataSplit_clk160_dbg      <= DCSDecDataSplit_clk160;
    DecoderAlignedDec_clk160_dbg    <= DecoderAlignedDec_clk160;
    EgroupDecData_clk160_dbg        <= EgroupDecData_clk160;
    EgroupMUX2DataSplit_clk160_dbg  <= EgroupMUX2DataSplit_clk160;

end Behavioral;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               Frans Schreuder
--!               Julia Narevicius
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.
--! Engineer: juna
--!
--! Create Date:    07/07/2014
--! Module Name:    GBTdataEmulator
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;
--use work.memoryInitStrings.all;
--use work.txt_util.all;
library xpm;
    use xpm.vcomponents.all;

--! E-link data emulator
entity GBTdataEmulator is
    Generic (
        EMU_DIRECTION       : string := "ToHost";  -- ToHost or ToFrontEnd
        FIRMWARE_MODE       : integer := 0;
        MEM_DEPTH           : integer := 16384;
        LPGBT_TOHOST_WIDTH  : integer := 32;
        LPGBT_TOFE_WIDTH    : integer := 8;
        MemIndLUTToHost     : IntArray := (0, 1, 2, 0, 1, 2, 0);
        MemIndLUTToHost_f   : string := "lpGBT_ToHostemuram_"
    );
    Port (
        clk40                   : in  std_logic;
        wrclk                   : in  std_logic;
        rst_hw                  : in  std_logic;
        rst_soft                : in  std_logic;
        xoff                    : in  std_logic;
        register_map_control    : in  register_map_control_type;
        ---------
        GBTdata                 : out std_logic_vector(119 downto 0);
        lpGBTdataToFE           : out std_logic_vector(31 downto 0);
        lpGBTdataToHost         : out std_logic_vector(223 downto 0);
        lpGBTECdata             : out std_logic_vector(1 downto 0);
        lpGBTICdata             : out std_logic_vector(1 downto 0);
        GBTlinkValid            : out std_logic
    );
end GBTdataEmulator;

architecture Behavioral of GBTdataEmulator is


    --type StringsArray is array (natural range <>) of integer;
    --emulating use case of 2x RD53A SCC w/ 3 lanes (to be bonded) plus a spare
    --RD53A with 1 lane. Each RD53A has the same data for now. Data differ for
    --different lanes of a given RD53A. Bonding can be exercised .
    --constant MemIndLUTToHost  : StringsArray := (0, 1, 2, 0, 1, 2, 0);
    --constant MemIndLUTToHost  : StringsArray := (2, 3, 4, 5, 0, 0, 0);
    --all downlink are equivalent for now
    constant MemIndLUTToFE  : IntArray := (0, 0, 0, 0);


    signal emuram_rdaddr, emuram_wraddr : std_logic_vector(f_log2(MEM_DEPTH)-1 downto 0) := (others => '0');
    signal RESET    : std_logic := '1';
    signal rst_fall : std_logic;
    signal ena      : std_logic := '0';
    --signal emuram_wrdata : std_logic_vector(15 downto 0) := (others=>'0');
    signal emuram_wrdata : std_logic_vector(LPGBT_TOHOST_WIDTH-1 downto 0) := (others=>'0'); --15 downto 0 used if GBT flavor

    signal GBTdata_s : std_logic_vector(119 downto 0);
    signal lpGBTdataToHost_s : std_logic_vector(223 downto 0);
    signal lpGBTdataToFE_s : std_logic_vector(31 downto 0);

    type we_array_type is array (0 to 6) of std_logic_vector(0 downto 0);
    signal wea : we_array_type :=((others=>'0'),(others=>'0'),(others=>'0'),(others=>'0'),(others=>'0'),(others=>'0'),(others=>'0'));

    constant web        : std_logic_vector(0 downto 0) := (others=>'0');
    constant zeros32bit : std_logic_vector(31 downto 0) := (others=>'0');
    constant zeros8bit : std_logic_vector(7 downto 0) := (others=>'0');
    constant addr_max_gbt   : std_logic_vector(f_log2(MEM_DEPTH)-1 downto 0) := "11111111111011"; -- 16379 (5 x 3276)
    constant addr_max_pixel   : std_logic_vector(f_log2(MEM_DEPTH)-1 downto 0) := "01111110010100"; ----8084 --"01111111111011"; --8187=8192-5. Memory loaded only with 8192, That's all I have
    constant addrnotena_max_gbt : std_logic_vector(f_log2(MEM_DEPTH)-1 downto 0) := "00000000000100";
    constant addrnotena_max_pixel : std_logic_vector(f_log2(MEM_DEPTH)-1 downto 0) := "00000000000000";

    signal addr_max : std_logic_vector(f_log2(MEM_DEPTH)-1 downto 0);
    signal addrnotena_max : std_logic_vector(f_log2(MEM_DEPTH)-1 downto 0);
    signal PixelLinkValid : std_logic;

    function int_to_str(int : integer) return string is
        variable r : string(1 to 1);
    begin
        case int is
            when 0    => r := "0";
            when 1    => r := "1";
            when 2    => r := "2";
            when 3    => r := "3";
            when 4    => r := "4";
            when 5    => r := "5";
            when 6    => r := "6";
            when 7    => r := "7";
            when 8    => r := "8";
            when 9    => r := "9";
            when others => r := "?";
        end case;
        return r;
    end int_to_str;

begin

    GBTlinkValid <= PixelLinkValid when FIRMWARE_MODE = FIRMWARE_MODE_PIXEL else '1';

    addr_max <= addr_max_gbt when (FIRMWARE_MODE = FIRMWARE_MODE_GBT or FIRMWARE_MODE = FIRMWARE_MODE_LTDB or FIRMWARE_MODE = FIRMWARE_MODE_FEI4 or FIRMWARE_MODE = FIRMWARE_MODE_LPGBT or FIRMWARE_MODE = FIRMWARE_MODE_HGTD_LUMI) else addr_max_pixel;
    addrnotena_max <= addrnotena_max_gbt when (FIRMWARE_MODE = FIRMWARE_MODE_GBT or FIRMWARE_MODE = FIRMWARE_MODE_LTDB or FIRMWARE_MODE = FIRMWARE_MODE_FEI4 or FIRMWARE_MODE = FIRMWARE_MODE_LPGBT or FIRMWARE_MODE = FIRMWARE_MODE_HGTD_LUMI) else addrnotena_max_pixel;
    --
    -- global reset

    rst_fall_pulse: entity work.pulse_fall_pw01 port map(
            clk => clk40,
            trigger => rst_soft,
            pulseout => rst_fall
        );

    --
    RESET_latch: process(clk40, rst_hw)
    begin
        if rst_hw = '1' then
            RESET <= '1';
        elsif clk40'event and clk40 = '1' then
            if rst_soft = '1' then
                RESET <= '1';
            elsif rst_fall = '1' then
                RESET <= '0';
            end if;
        end if;
    end process;

    --RESET <= rst_hw or rst_soft;

    -- data out enable
    process(clk40, RESET)
    begin
        if RESET = '1' then
            ena <= '0';
        elsif clk40'event and clk40 = '1' then
            if EMU_DIRECTION = "ToHost" then
                ena <= to_sl(register_map_control.FE_EMU_ENA.EMU_TOHOST) and (not xoff);
            else
                ena <= to_sl(register_map_control.FE_EMU_ENA.EMU_TOFRONTEND) and (not xoff);
            end if;
        end if;
    end process;
    --
    --
    process(wrclk,RESET)
    begin
        if RESET = '1' then
            wea             <= ((others=>'0'),(others=>'0'),(others=>'0'),(others=>'0'),(others=>'0'),(others=>'0'),(others=>'0'));
            emuram_wraddr   <= (others=>'0');
            emuram_wrdata   <= (others=>'0');
        elsif wrclk'event and wrclk = '1' then
            wea(0)(0) <= register_map_control.FE_EMU_CONFIG.WE(47); --
            wea(1)(0) <= register_map_control.FE_EMU_CONFIG.WE(48); --
            wea(2)(0) <= register_map_control.FE_EMU_CONFIG.WE(49); --
            wea(3)(0) <= register_map_control.FE_EMU_CONFIG.WE(50); --
            wea(4)(0) <= register_map_control.FE_EMU_CONFIG.WE(51); --
            wea(5)(0) <= register_map_control.FE_EMU_CONFIG.WE(52); --
            wea(6)(0) <= register_map_control.FE_EMU_CONFIG.WE(53); --
            emuram_wraddr   <= register_map_control.FE_EMU_CONFIG.WRADDR;   -- 14 bit
            --GBT: 15 downto 0, lpGBTToHost: 31 downto 0; lpGBTToFE: 7 downto 0
            if (FIRMWARE_MODE = FIRMWARE_MODE_GBT or FIRMWARE_MODE = FIRMWARE_MODE_LTDB or FIRMWARE_MODE = FIRMWARE_MODE_FEI4) then
                emuram_wrdata(15 downto 0)   <= register_map_control.FE_EMU_CONFIG.WRDATA(15 downto 0);
                emuram_wrdata(31 downto 16)  <= (others => '0');
            elsif (EMU_DIRECTION = "ToHost") then
                emuram_wrdata   <= register_map_control.FE_EMU_CONFIG.WRDATA(LPGBT_TOHOST_WIDTH-1 downto 0);
            else
                emuram_wrdata(7 downto 0)   <= register_map_control.FE_EMU_CONFIG.WRDATA(7 downto 0);
                emuram_wrdata(31 downto 8)  <= (others => '0');
            end if;
        end if;
    end process;

    -- address counter
    address_counter: process(clk40)
    begin
        if clk40'event and clk40 = '1' then
            if ena = '1' then
                if emuram_rdaddr = addr_max then
                    emuram_rdaddr <= (others => '0');
                    PixelLinkValid <= '1';
                else
                    emuram_rdaddr <= emuram_rdaddr + 1;
                    if emuram_rdaddr <= "00000000000001" then
                        PixelLinkValid <= '0';
                    else
                        PixelLinkValid <= '1';
                    end if;
                end if;
            else
                if emuram_rdaddr >= addrnotena_max then
                    emuram_rdaddr <= (others => '0');
                else
                    emuram_rdaddr <= emuram_rdaddr + 1;
                end if;
                PixelLinkValid <= '0';
            end if;
        end if;
    end process;
    --

    --
    g_gbt_ram: if FIRMWARE_MODE = FIRMWARE_MODE_GBT or
              FIRMWARE_MODE = FIRMWARE_MODE_LTDB or
              FIRMWARE_MODE = FIRMWARE_MODE_FEI4 generate

        g_emurams: for i in 0 to 4 generate


        begin
            emuram_00 : xpm_memory_sdpram
                generic map (  -- @suppress "Generic map uses default values. Missing optional actuals: USE_MEM_INIT_MMI, CASCADE_HEIGHT, SIM_ASSERT_CHK, WRITE_PROTECT"
                    ADDR_WIDTH_A => 14,
                    ADDR_WIDTH_B => 14,
                    AUTO_SLEEP_TIME => 0,
                    BYTE_WRITE_WIDTH_A => 16,
                    CLOCKING_MODE => "independent_clock",
                    ECC_MODE => "no_ecc",
                    MEMORY_INIT_FILE => "emurom_"&int_to_str(i)&".mem",
                    MEMORY_INIT_PARAM => "0",
                    MEMORY_OPTIMIZATION => "true",
                    MEMORY_PRIMITIVE => "auto",
                    MEMORY_SIZE => 16384*16,
                    MESSAGE_CONTROL => 0,
                    READ_DATA_WIDTH_B => 16,
                    READ_LATENCY_B => 2,
                    READ_RESET_VALUE_B => "0",
                    RST_MODE_A => "SYNC",
                    RST_MODE_B => "SYNC",
                    USE_EMBEDDED_CONSTRAINT => 0,
                    USE_MEM_INIT => 1,
                    WAKEUP_TIME => "disable_sleep",
                    WRITE_DATA_WIDTH_A => 16,
                    WRITE_MODE_B => "no_change"
                )
                port map (
                    sleep => '0',
                    clka => wrclk,
                    ena => '1',
                    wea => wea(i),
                    addra => emuram_wraddr,
                    dina => emuram_wrdata(15 downto 0),
                    injectsbiterra => '0',
                    injectdbiterra => '0',
                    clkb => clk40,
                    rstb => RESET,
                    enb => '1',
                    regceb => '1',
                    addrb => emuram_rdaddr,
                    doutb => GBTdata_s(15+i*16 downto i*16),
                    sbiterrb => open,
                    dbiterrb => open
                );
        end generate;
        GBTdata_s(119 downto 112) <= "0101" & "11" & "11"; --GBTdata_s(49 downto 48); --"00";
        GBTdata <= GBTdata_s(119 downto 112) & GBTdata_s(79 downto 0) & zeros32bit;
    end generate; --g_gbt_ram
    --strip and needs a different data width and .coe
    --lpgbt: what does it need
    g_lpgbt_tohostram: if EMU_DIRECTION = "ToHost" and
                     (FIRMWARE_MODE = FIRMWARE_MODE_PIXEL or
                      FIRMWARE_MODE = FIRMWARE_MODE_STRIP or
                      FIRMWARE_MODE = FIRMWARE_MODE_BCM_PRIME or
                      FIRMWARE_MODE = FIRMWARE_MODE_HGTD_LUMI or
                      FIRMWARE_MODE = FIRMWARE_MODE_LPGBT) generate

        g_LPGBT_Egroups_ToHostEmu: for egroup in 0 to 6 generate

            lpGBT_ToHostemuram_00 : xpm_memory_tdpram
                generic map (  -- @suppress "Generic map uses default values. Missing optional actuals: USE_MEM_INIT_MMI, CASCADE_HEIGHT, SIM_ASSERT_CHK, WRITE_PROTECT"
                    ADDR_WIDTH_A         => f_log2(MEM_DEPTH),
                    ADDR_WIDTH_B         => f_log2(MEM_DEPTH),
                    AUTO_SLEEP_TIME      => 0,
                    BYTE_WRITE_WIDTH_A   => LPGBT_TOHOST_WIDTH,
                    BYTE_WRITE_WIDTH_B   => LPGBT_TOHOST_WIDTH,
                    CLOCKING_MODE        => "independent_clock",
                    ECC_MODE             => "no_ecc",
                    MEMORY_INIT_FILE     => MemIndLUTToHost_f & int_to_str(MemIndLUTToHost(egroup)) & ".mem",--"lpGBT_ToHostemuram_" & int_to_str(MemIndLUTToHost(egroup)) & ".mem",
                    MEMORY_INIT_PARAM    => "0", -->4k bits lpGBT_ToHostemuram_00_init,
                    MEMORY_OPTIMIZATION  => "true",
                    MEMORY_PRIMITIVE     => "auto",
                    MEMORY_SIZE          => MEM_DEPTH*LPGBT_TOHOST_WIDTH,
                    MESSAGE_CONTROL      => 1,
                    READ_DATA_WIDTH_A    => LPGBT_TOHOST_WIDTH,
                    READ_DATA_WIDTH_B    => LPGBT_TOHOST_WIDTH,
                    READ_LATENCY_A       => 1,
                    READ_LATENCY_B       => 1,
                    READ_RESET_VALUE_A   => "0",
                    READ_RESET_VALUE_B   => "0",
                    RST_MODE_A           => "SYNC",
                    RST_MODE_B           => "SYNC",
                    USE_EMBEDDED_CONSTRAINT => 0,
                    USE_MEM_INIT         => 1,
                    WAKEUP_TIME          => "disable_sleep",
                    WRITE_DATA_WIDTH_A   => LPGBT_TOHOST_WIDTH,
                    WRITE_DATA_WIDTH_B   => LPGBT_TOHOST_WIDTH,
                    WRITE_MODE_A         => "write_first",
                    WRITE_MODE_B         => "write_first"
                )
                port map (
                    sleep => '0',
                    clka => wrclk,
                    rsta => '0',
                    ena => '1',
                    regcea => '1',
                    wea => wea(egroup),
                    addra => emuram_wraddr,
                    dina => emuram_wrdata(LPGBT_TOHOST_WIDTH-1 downto 0),
                    injectsbiterra => '0',
                    injectdbiterra => '0',
                    douta => open,
                    sbiterra => open,
                    dbiterra => open,
                    clkb => clk40,
                    rstb => '0',
                    enb => '1',
                    regceb => '1',
                    web => web,
                    addrb => emuram_rdaddr,
                    dinb => zeros32bit(LPGBT_TOHOST_WIDTH-1 downto 0),
                    injectsbiterrb => '0',
                    injectdbiterrb => '0',
                    doutb => lpGBTdataToHost_s((LPGBT_TOHOST_WIDTH-1)+LPGBT_TOHOST_WIDTH*egroup downto LPGBT_TOHOST_WIDTH*egroup),
                    sbiterrb => open,
                    dbiterrb => open
                );
        end generate;  --  g_LPGBT_Egroups_ToHostEmu

        lpGBTdataToHost       <=  lpGBTdataToHost_s(223 downto 0);
        lpGBTECdata           <=  "11";
        lpGBTICdata           <=  "11";

    end generate; --lpGBT_ToHostemuram_0


    g_lpgbt_toferam: if EMU_DIRECTION = "ToFrontEnd" and
                     (FIRMWARE_MODE = FIRMWARE_MODE_PIXEL or
                      FIRMWARE_MODE = FIRMWARE_MODE_STRIP or
                      FIRMWARE_MODE = FIRMWARE_MODE_BCM_PRIME or
                      FIRMWARE_MODE = FIRMWARE_MODE_HGTD_LUMI or
                      FIRMWARE_MODE = FIRMWARE_MODE_LPGBT) generate

        g_LPGBT_Egroups_ToFEEmu: for egroup in 0 to 3 generate
            lpGBT_ToFEemuram_00 : xpm_memory_tdpram
                generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: USE_MEM_INIT_MMI, CASCADE_HEIGHT, SIM_ASSERT_CHK, WRITE_PROTECT"
                    ADDR_WIDTH_A         => f_log2(MEM_DEPTH),
                    ADDR_WIDTH_B         => f_log2(MEM_DEPTH),
                    AUTO_SLEEP_TIME      => 0,
                    BYTE_WRITE_WIDTH_A   => LPGBT_TOFE_WIDTH,
                    BYTE_WRITE_WIDTH_B   => LPGBT_TOFE_WIDTH,
                    CLOCKING_MODE        => "independent_clock",
                    ECC_MODE             => "no_ecc",
                    MEMORY_INIT_FILE     => "lpGBT_ToFEemuram_" & int_to_str(MemIndLUTToFE(egroup)) & ".mem",
                    MEMORY_INIT_PARAM    => "0", --lpGBT_ToFEemuram_00_init,
                    MEMORY_OPTIMIZATION  => "true",
                    MEMORY_PRIMITIVE     => "auto",
                    MEMORY_SIZE          => MEM_DEPTH*LPGBT_TOFE_WIDTH,
                    MESSAGE_CONTROL      => 1,
                    READ_DATA_WIDTH_A    => LPGBT_TOFE_WIDTH,
                    READ_DATA_WIDTH_B    => LPGBT_TOFE_WIDTH,
                    READ_LATENCY_A       => 1,
                    READ_LATENCY_B       => 1,
                    READ_RESET_VALUE_A   => "0",
                    READ_RESET_VALUE_B   => "0",
                    RST_MODE_A           => "SYNC",
                    RST_MODE_B           => "SYNC",
                    USE_EMBEDDED_CONSTRAINT => 0,
                    USE_MEM_INIT         => 1,
                    WAKEUP_TIME          => "disable_sleep",
                    WRITE_DATA_WIDTH_A   => LPGBT_TOFE_WIDTH,
                    WRITE_DATA_WIDTH_B   => LPGBT_TOFE_WIDTH,
                    WRITE_MODE_A         => "write_first",
                    WRITE_MODE_B         => "write_first"
                )
                port map (
                    sleep => '0',
                    clka => wrclk,
                    rsta => '0',
                    ena => '1',
                    regcea => '1',
                    wea => wea(egroup),
                    addra => emuram_wraddr,
                    dina => emuram_wrdata(LPGBT_TOFE_WIDTH-1 downto 0),
                    injectsbiterra => '0',
                    injectdbiterra => '0',
                    douta => open,
                    sbiterra => open,
                    dbiterra => open,
                    clkb => clk40,
                    rstb => '0',
                    enb => '1',
                    regceb => '1',
                    web => web,
                    addrb => emuram_rdaddr,
                    dinb => zeros8bit(LPGBT_TOFE_WIDTH-1 downto 0),
                    injectsbiterrb => '0',
                    injectdbiterrb => '0',
                    doutb => lpGBTdataToFE_s((LPGBT_TOFE_WIDTH-1)+LPGBT_TOFE_WIDTH*egroup downto LPGBT_TOFE_WIDTH*egroup),
                    sbiterrb => open,
                    dbiterrb => open
                );
        end generate; --g_LPGBT_Egroups_ToFEEmu

        lpGBTdataToFE         <=  lpGBTdataToFE_s(31 downto 0);
        lpGBTECdata           <=  "11";
        lpGBTICdata           <=  "11";

    end generate; --g_lpgbt_tohostram


--

end Behavioral;


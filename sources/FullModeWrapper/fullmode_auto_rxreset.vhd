--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer: Rene Habraken
--
-- Create Date:    05/28/2021
-- Design Name:
-- Module Name:     fullmode_auto_rxreset
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:   Auto RX reset in case of disparity error per channel
--                for FULL mode
-- Revision:
-- Additional Comments:
--
----------------------------------------------------------------------------------
LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std_unsigned.all;
    use work.pcie_package.all;
    use work.FELIX_gbt_package.all;
library xpm;
    use xpm.vcomponents.all;

entity fullmode_auto_rxreset is
    generic(
        GBT_NUM : integer
    );
    port(
        clk40_in : in std_logic;
        register_map_control : in register_map_control_type;
        GBT_ALIGNMENT_DONE : out std_logic_vector(23 downto 0);
        GT_RXUSRCLK : in std_logic_vector(GBT_NUM-1 downto 0);
        RxDisperr_in         : in std_logic_vector(95 downto 0);
        RxDisperr_out        : out std_logic_vector(95 downto 0);
        RXRESET_AUTO         : out std_logic_vector(GBT_NUM-1 downto 0);
        GBT_RXCDR_LOCK       : out std_logic_vector(23 downto 0);
        rxcdrlock_in         : in  std_logic_vector(GBT_NUM-1 downto 0);
        RXByteisAligned      : in std_logic_vector(GBT_NUM-1 downto 0)

    );
end entity fullmode_auto_rxreset;

architecture rtl of fullmode_auto_rxreset is
    signal TIMEOUTfromRM : std_logic_vector(31 downto 0);
    signal ENABLEfromRM : std_logic_vector(32 downto 32);
    signal trig_alignment_regmap_cnt :std_logic_vector(31 downto 0);
    signal alignment_done_chk_cnt    :std_logic_vector(12 downto 0);
    signal GT_RXByteisAligned, GT_RXByteisAligned_f   :std_logic_vector(23 downto 0);
    signal disperr_found        :std_logic_vector(95 downto 0);
    signal disperr_trig         :std_logic_vector(95 downto 0);
    signal chaX_aligned         :std_logic_vector(23 downto 0);
    signal cdr_cnt              :std_logic_vector(19 downto 0);
    signal alignment_done_a          :std_logic_vector(47 downto 0);
    signal General_ctrl        :std_logic_vector(63 downto 0);
    signal RxCdrLock_int       :std_logic_vector(23 downto 0);
    signal RxCdrLock_a : std_logic_vector(GBT_NUM-1 downto 0);
    signal RxDisperr_40: std_logic_vector(95 downto 0);
begin

    xpm_cdc_array_RxDispErr_in: xpm_cdc_array_single
        generic map (
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0,
            WIDTH => 96
        )
        port map (
            src_clk => '0',
            src_in => RxDisperr_in,
            dest_clk => clk40_in,
            dest_out => RxDisperr_40
        );

    process(clk40_in)
    begin
        if clk40_in'event and clk40_in='1' then
            TIMEOUTfromRM <= register_map_control.FULLMODE_AUTO_RX_RESET.TIMEOUT;
            ENABLEfromRM <= register_map_control.FULLMODE_AUTO_RX_RESET.ENABLE;
        end if;
    end process;

    General_ctrl           <= register_map_control.GBT_GENERAL_CTRL;  -- unused
    GBT_RXCDR_LOCK <= RxCdrLock_int;
    GBT_ALIGNMENT_DONE(23 downto 0) <= (chaX_aligned(23 downto 0) and GT_RXByteisAligned(23 downto 0))         when ENABLEfromRM = "1"  else
                                       GT_RXByteisAligned_f(23 downto 0)  when General_ctrl(2)='0' else
                                       GT_RXByteisAligned(23 downto 0);
    ----------------------------------

    process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            alignment_done_chk_cnt <= alignment_done_chk_cnt + '1';
        end if;
    end process;

    process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            cdr_cnt <=cdr_cnt+'1';
        end if;
    end process;


    rxalign_auto : for i in GBT_NUM-1 downto 0 generate
        signal RXByteisAligned_40: std_logic;
    begin

        RXRESET_AUTO(i) <= '1' when disperr_trig(4*i+3 downto 4*i) /= "0000" else '0';


        xpm_cdc_single_RXByteisAligned: xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,   -- DECIMAL; range: 2-10
                INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
                SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
                SRC_INPUT_REG => 0   -- DECIMAL; 0=do not register input, 1=register input
            )
            port map (
                src_clk => '0', -- 1-bit input: optional; required when SRC_INPUT_REG = 1
                src_in => RXByteisAligned(i), -- 1-bit input: Input signal to be synchronized to dest_clk domain.
                dest_clk => clk40_in, -- 1-bit input: Clock signal for the destination clock domain.
                dest_out => RXByteisAligned_40 -- 1-bit output: src_in synchronized to the destination clock domain. This output
            );
        process(clk40_in)
        begin
            if rising_edge(clk40_in) then
                if alignment_done_chk_cnt="0000000000000" then
                    alignment_done_a(i) <= RXByteisAligned_40;
                else
                    alignment_done_a(i) <= RXByteisAligned_40 and alignment_done_a(i);
                end if;
                if alignment_done_chk_cnt="1000000000000" then
                    GT_RXByteisAligned(i) <=  RxCdrLock_int(i) and alignment_done_a(i);-- and kchar_found(i) and Alignment_flag_enable(i);
                end if;
                if (General_ctrl(1)='1') then
                    GT_RXByteisAligned_f(i) <= '1';
                else
                    GT_RXByteisAligned_f(i) <=  GT_RXByteisAligned(i) and GT_RXByteisAligned_f(i);
                end if;
            end if;
        end process;

        process(clk40_in)
        begin
            if rising_edge(clk40_in) then
                if cdr_cnt ="00000000000000000000" then
                    RxCdrLock_a(i)     <= rxcdrlock_in(i);
                else
                    RxCdrLock_a(i) <= RxCdrLock_a(i) and rxcdrlock_in(i);
                end if;
                if cdr_cnt="00000000000000000000" then
                    RxCdrLock_int(i) <=RxCdrLock_a(i);
                end if;
            end if;
        end process;


        process(clk40_in)
        begin
            if clk40_in'event and clk40_in='1' then
                trig_alignment_regmap_cnt <= trig_alignment_regmap_cnt + '1';
                if trig_alignment_regmap_cnt="0000000000000" then
                    -- reset disparity error flags
                    disperr_found (4*i+3 downto 4*i) <= "0000";
                    disperr_trig <= (others=> '0');
                elsif trig_alignment_regmap_cnt= TIMEOUTfromRM then
                    -- write the channels with disp err to regmap mon. and trigger auto rst
                    -- update register GBT_ALIGNMENT_DONE
                    RxDisperr_out(4*i+3 downto 4*i) <= disperr_found(4*i+3 downto 4*i);
                    disperr_trig <= disperr_found;
                    trig_alignment_regmap_cnt <= (others => '0');
                    if disperr_found(4*i+3 downto 4*i) = "0000" then
                        chaX_aligned(i) <= '1';
                    else
                        chaX_aligned(i) <= '0';
                    end if;
                else
                    -- store occuring disp errors during count loop
                    disperr_found(4*i+3 downto 4*i) <= disperr_found(4*i+3 downto 4*i) or RxDisperr_40(4*i+3 downto 4*i);
                    disperr_trig <= (others=> '0');
                end if;
            end if;
        end process;

    end generate;


end architecture rtl;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               Weihao Wu
--!               Kai Chen
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer: Kai Chen
--
-- Create Date:    10/20/2016
-- Design Name:
-- Module Name:     FELIX_FM_gbt_wrapper  -- Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:   The TOP MODULE FOR FELIX GBT FULL mode
--                RX buffer is used in transceiver
--                The TTC 240M clock is used for rx_user_clk
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM, xpm;
    use UNISIM.VComponents.all;
    use xpm.vcomponents.all;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;

entity FELIX_FM_gbt_wrapper is
    Generic (
        GBT_NUM               : integer := 24;
        CARD_TYPE             : integer:=712;
        FULL_HALFRATE         : boolean:= false;
        GTREFCLKS             : integer := 2; -- number of reference clocks
        FIRMWARE_MODE         : integer := FIRMWARE_MODE_FULL
    );
    Port (

        -------------------
        ---- For debug
        -------------------
        -- For Latency test
        RX_FLAG_O    : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_FLAG_O    : out std_logic_vector(GBT_NUM-1 downto 0);
        REFCLK_CXP1  : out std_logic;
        REFCLK_CXP2  : out std_logic;

        rst_hw       : in std_logic;

        register_map_control        : in   register_map_control_type;
        register_map_link_monitor    : out  register_map_link_monitor_type;

        -- GTH REFCLK, DRPCLK, GREFCLK
        DRP_CLK_IN                      : in std_logic;
        GTREFCLK_N_IN                   : in std_logic_vector(GTREFCLKS-1 downto 0);
        GTREFCLK_P_IN                   : in std_logic_vector(GTREFCLKS-1 downto 0);

        clk40_in                        : in std_logic;
        clk100_in                       : in std_logic;

        -- for CentralRouter
        TX_120b_in                      : in  array_120b;
        RXUSERCLK_OUT                   : out std_logic_vector(GBT_NUM-1 downto 0);
        TXUSERCLK_OUT                   : out std_logic_vector(GBT_NUM-1 downto 0);
        RX_DATA_33b                     : out array_33b;
        RX_DATA_33b_rdy                 : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_DATA_33b                     : in  array_33b;

        TX_FRAME_CLK_I                  : in std_logic;

        -- GTH Data pins
        TX_P   : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_N   : out std_logic_vector(GBT_NUM-1 downto 0);
        RX_P   : in  std_logic_vector(GBT_NUM-1 downto 0);
        RX_N   : in  std_logic_vector(GBT_NUM-1 downto 0)

    );
end FELIX_FM_gbt_wrapper;

architecture Behavioral of FELIX_FM_gbt_wrapper is

    signal txusrrdy            : std_logic_vector(23 downto 0);
    signal rxusrrdy            : std_logic_vector(23 downto 0);
    signal gttx_reset          : std_logic_vector(23 downto 0);
    signal cpll_reset          : std_logic_vector(23 downto 0);
    signal GT_TXUSRCLK         : std_logic_vector(23 downto 0);
    signal txresetdone         : std_logic_vector(23 downto 0);
    signal rxresetdone         : std_logic_vector(23 downto 0);
    signal txfsmresetdone      : std_logic_vector(23 downto 0);
    signal rxfsmresetdone      : std_logic_vector(23 downto 0);
    signal cpllfbclklost       : std_logic_vector(23 downto 0);
    signal cplllock            : std_logic_vector(23 downto 0);
    signal rxcdrlock           : std_logic_vector(GBT_NUM-1 downto 0);
    signal rxcdrlock_40        : std_logic_vector(GBT_NUM-1 downto 0);
    signal TX_RESET            : std_logic_vector(23 downto 0);
    signal TX_RESET_i          : std_logic_vector(23 downto 0);
    signal GT_TX_WORD_CLK      : std_logic_vector(23 downto 0);
    signal TX_TC_METHOD        : std_logic_vector(23 downto 0);

    type data20barray is array (0 to GBT_NUM-1) of std_logic_vector(19 downto 0);
    signal TX_DATA_20b  : data20barray := (others => ("00000000000000000000"));

    signal SOFT_TXRST_GT       :std_logic_vector(23 downto 0);
    signal SOFT_RXRST_GT       :std_logic_vector(23 downto 0);
    signal SOFT_TXRST_ALL      :std_logic_vector(5 downto 0);
    signal SOFT_RXRST_ALL      :std_logic_vector(5 downto 0);
    --signal TX_OPT              :std_logic_vector(95 downto 0);
    signal DATA_TXFORMAT       :std_logic_vector(47 downto 0);
    signal DATA_TXFORMAT_i     :std_logic_vector(47 downto 0);

    SIGNAL TX_TC_DLY_VALUE      : std_logic_vector(95 downto 0);
    signal GTH_RefClk           : std_logic_vector(23 downto 0);

    signal CXP1_GTH_RefClk      :std_logic;
    signal CXP2_GTH_RefClk      :std_logic;
    --signal gtrx_reset_i         :std_logic_vector(23 downto 0);
    signal GT_RXOUTCLK          :std_logic_vector(GBT_NUM-1 downto 0);
    signal GT_TXOUTCLK          :std_logic_vector(23 downto 0);
    signal RXByteisAligned      :std_logic_vector(GBT_NUM-1 downto 0);
    signal RXByteisAligned_40   : std_logic_vector(GBT_NUM-1 downto 0);
    signal TC_EDGE              :std_logic_vector(23 downto 0);
    signal RxDisperr            :std_logic_vector(95 downto 0);
    signal GT_RxDisperr         :std_logic_vector(95 downto 0);
    signal TX_120b_in_i         :array_120b(0 to (GBT_NUM-1));
    signal TX_120b_in_wc        :array_120b(0 to (GBT_NUM-1)); --synchronized to TX word clock using XPM CDC
    signal GT_RXUSRCLK          : std_logic_vector((GBT_NUM)-1 downto 0);


    signal RX_DATA_33b_s        : array_33b(0 to GBT_NUM-1);
    signal GBT_ALIGNMENT_DONE : std_logic_vector(23 downto 0);
    signal RXRESET_AUTO : std_logic_vector(23 downto 0);
    signal GBT_RXCDR_LOCK : std_logic_vector(23 downto 0);
    signal gtrx_reset : std_logic_vector(23 downto 0);
    signal soft_reset : std_logic_vector(23 downto 0);
    signal rst_hw_23 : std_logic_vector(23 downto 0);
    signal qpll_reset : std_logic_vector(5 downto 0);
    signal tx8b10ben_txusrclk : std_logic_vector(GBT_NUM-1 downto 0);
    signal rxrate_ch : array_8b(0 to 3);
    signal Txrate_ch : array_8b(0 to 3);


begin
    rst_hw_23 <= (others => rst_hw);

    g_tx8b10ben_sync: for i in 0 to GBT_NUM-1 generate
        cdc0: xpm_cdc_single generic map(
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map(
                src_clk => '0',
                src_in => register_map_control.LINK_FULLMODE_LTI(i),
                dest_clk => GT_TXUSRCLK(i),
                dest_out => tx8b10ben_txusrclk(i)
            );
    end generate;



    g_709a: if CARD_TYPE = 709 or CARD_TYPE = 710 generate
        g_RXUSRCLK_BUFG: for i in 0 to GBT_NUM-1 generate
            buf0: BUFG port map (
                    O => GT_RXUSRCLK(i),
                    I => GT_RXOUTCLK(i)
                );
            RXUSERCLK_OUT(i) <= GT_RXUSRCLK(i);
            TXUSERCLK_OUT(i) <= GT_TXUSRCLK(i);
        end generate;
    end generate;

    g_712a: if CARD_TYPE = 711 or CARD_TYPE = 712 or CARD_TYPE = 128 or CARD_TYPE = 800 generate
        g_RXUSRCLK_BUFG: for i in 0 to GBT_NUM-1 generate
            buf0: BUFG_GT     generic map(
                    SIM_DEVICE => "ULTRASCALE",
                    STARTUP_SYNC => "FALSE"
                )
                port map(
                    O => GT_RXUSRCLK(i),
                    CE => '1',
                    CEMASK => '0',
                    CLR => '0',
                    CLRMASK => '0',
                    DIV => "000",
                    I => GT_RXOUTCLK(i)
                );
            RXUSERCLK_OUT(i) <= GT_RXUSRCLK(i);
            TXUSERCLK_OUT(i) <= GT_TXUSRCLK(i);
        end generate;
    end generate;
    RX_FLAG_O <= (others => '0');  -- unused

    --IBUFDS_GTE2

    REFCLK_CXP1 <= CXP1_GTH_RefClk;
    REFCLK_CXP2 <= CXP2_GTH_RefClk;
    g_709b: if CARD_TYPE = 709 or CARD_TYPE = 710 generate
        ibufds_instq2_clk0 : IBUFDS_GTE2
            generic map(
                CLKCM_CFG => true,
                CLKRCV_TRST => true,
                CLKSWING_CFG => "11"
            )
            port map   (
                O               =>   CXP1_GTH_RefClk,
                ODIV2           =>  open,
                CEB             =>   '0',
                I               =>   GTREFCLK_P_IN(0),
                IB              =>   GTREFCLK_N_IN(0)
            );

        ibufds_instq8_clk0 : IBUFDS_GTE2
            generic map(
                CLKCM_CFG => true,
                CLKRCV_TRST => true,
                CLKSWING_CFG => "11"
            )
            port map    (
                O               =>   CXP2_GTH_RefClk,
                ODIV2           =>  open,
                CEB             =>   '0',
                I               =>   GTREFCLK_P_IN(1),
                IB              =>   GTREFCLK_N_IN(1)
            );
    end generate;
    g_712b: if CARD_TYPE = 711 or CARD_TYPE = 712 generate
        ibufds_instq2_clk0: IBUFDS_GTE3
            generic map (
                REFCLK_EN_TX_PATH => '0',   -- Refer to Transceiver User Guide
                REFCLK_HROW_CK_SEL => "00", -- Refer to Transceiver User Guide
                REFCLK_ICNTL_RX => "00"     -- Refer to Transceiver User Guide
            )
            port map (
                O     => CXP1_GTH_RefClk,
                ODIV2 => open,
                CEB   => '0',
                I               =>   GTREFCLK_P_IN(0),
                IB              =>   GTREFCLK_N_IN(0)
            );
        ibufds_instq8_clk0: IBUFDS_GTE3
            generic map (
                REFCLK_EN_TX_PATH => '0',   -- Refer to Transceiver User Guide
                REFCLK_HROW_CK_SEL => "00", -- Refer to Transceiver User Guide
                REFCLK_ICNTL_RX => "00"     -- Refer to Transceiver User Guide
            )
            port map (
                O     => CXP2_GTH_RefClk,
                ODIV2 => open,
                CEB   => '0',
                I               =>   GTREFCLK_P_IN(1),
                IB              =>   GTREFCLK_N_IN(1)
            );
    end generate;
    g_709or712b: if CARD_TYPE = 709 or CARD_TYPE = 710 or CARD_TYPE = 711 or CARD_TYPE = 712 generate
        g_GBTNUM24: if GBT_NUM > 8 and GBT_NUM <= 24 generate
            GTH_RefClk(0) <= CXP1_GTH_RefClk;
            GTH_RefClk(1) <= CXP1_GTH_RefClk;
            GTH_RefClk(2) <= CXP1_GTH_RefClk;
            GTH_RefClk(3) <= CXP1_GTH_RefClk;
            GTH_RefClk(4) <= CXP1_GTH_RefClk;
            GTH_RefClk(5) <= CXP1_GTH_RefClk;
            GTH_RefClk(6) <= CXP1_GTH_RefClk;
            GTH_RefClk(7) <= CXP1_GTH_RefClk;
            GTH_RefClk(8) <= CXP1_GTH_RefClk;
            GTH_RefClk(9) <= CXP1_GTH_RefClk;
            GTH_RefClk(10) <= CXP1_GTH_RefClk;
            GTH_RefClk(11) <= CXP1_GTH_RefClk;

            GTH_RefClk(12) <= CXP2_GTH_RefClk;
            GTH_RefClk(13) <= CXP2_GTH_RefClk;
            GTH_RefClk(14) <= CXP2_GTH_RefClk;
            GTH_RefClk(15) <= CXP2_GTH_RefClk;
            GTH_RefClk(16) <= CXP2_GTH_RefClk;
            GTH_RefClk(17) <= CXP2_GTH_RefClk;
            GTH_RefClk(18) <= CXP2_GTH_RefClk;
            GTH_RefClk(19) <= CXP2_GTH_RefClk;
            GTH_RefClk(20) <= CXP2_GTH_RefClk;
            GTH_RefClk(21) <= CXP2_GTH_RefClk;
            GTH_RefClk(22) <= CXP2_GTH_RefClk;
            GTH_RefClk(23) <= CXP2_GTH_RefClk;
        end generate;

        g_GBTNUM8: if GBT_NUM <= 8 generate
            GTH_RefClk(0) <= CXP1_GTH_RefClk;
            GTH_RefClk(1) <= CXP1_GTH_RefClk;
            GTH_RefClk(2) <= CXP1_GTH_RefClk;
            GTH_RefClk(3) <= CXP1_GTH_RefClk;

            GTH_RefClk(4) <= CXP2_GTH_RefClk;
            GTH_RefClk(5) <= CXP2_GTH_RefClk;
            GTH_RefClk(6) <= CXP2_GTH_RefClk;
            GTH_RefClk(7) <= CXP2_GTH_RefClk;
        end generate;
    end generate;
    g_128b: if CARD_TYPE = 128 or CARD_TYPE = 800 generate
        signal GTREFCLK: std_logic_vector(5 downto 0);
    begin
        g_IBUFDS_GTE4_i: for i in 0 to 5 generate
            gtrefclk0_clk0: IBUFDS_GTE4
                generic map (
                    REFCLK_EN_TX_PATH => '0',   -- Refer to Transceiver User Guide
                    REFCLK_HROW_CK_SEL => "00", -- Refer to Transceiver User Guide
                    REFCLK_ICNTL_RX => "00"     -- Refer to Transceiver User Guide
                )
                port map (
                    O     => GTREFCLK(i),
                    ODIV2 => open,
                    CEB   => '0',
                    I               =>   GTREFCLK_P_IN(i),
                    IB              =>   GTREFCLK_N_IN(i)
                );
            GTH_RefClk(i*4+0) <= GTREFCLK(i);
            GTH_RefClk(i*4+1) <= GTREFCLK(i);
            GTH_RefClk(i*4+2) <= GTREFCLK(i);
            GTH_RefClk(i*4+3) <= GTREFCLK(i);
        end generate;
    end generate;

    txusrrdy(23 downto 0)           <= register_map_control.GBT_TXUSRRDY(23 downto 0);
    rxusrrdy(23 downto 0)           <= register_map_control.GBT_RXUSRRDY(23 downto 0);
    gttx_reset(23 downto 0)         <= register_map_control.GBT_GTTX_RESET(23 downto 0);
    gtrx_reset(23 downto 0)         <= register_map_control.GBT_GTRX_RESET(23 downto 0);
    soft_reset(23 downto 0)         <= register_map_control.GBT_SOFT_RESET(23 downto 0) or rst_hw_23;
    cpll_reset(23 downto 0)         <= register_map_control.GBT_PLL_RESET.CPLL_RESET(23 downto 0);
    qpll_reset(5 downto 0)          <= register_map_control.GBT_PLL_RESET.QPLL_RESET(53 downto 48) or rst_hw_23(5 downto 0);
    SOFT_TXRST_GT(23 downto 0)      <= register_map_control.GBT_SOFT_TX_RESET.RESET_GT(23 downto 0);
    SOFT_RXRST_GT(23 downto 0)      <= register_map_control.GBT_SOFT_RX_RESET.RESET_GT(23 downto 0) or RXRESET_AUTO;
    SOFT_TXRST_ALL(5 downto 0)      <= register_map_control.GBT_SOFT_TX_RESET.RESET_ALL(53 downto 48);
    SOFT_RXRST_ALL(5 downto 0)      <= register_map_control.GBT_SOFT_RX_RESET.RESET_ALL(53 downto 48);


    TX_TC_DLY_VALUE(47 downto 0)    <= register_map_control.GBT_TX_TC_DLY_VALUE1;
    TX_TC_DLY_VALUE(95 downto 48)   <= register_map_control.GBT_TX_TC_DLY_VALUE2;

    --TX_OPT(47 downto 0)             <= x"000000555555"; --Register removed in RM 4.0 register_map_control.GBT_TX_OPT;
    DATA_TXFORMAT(47 downto 0)      <= register_map_control.GBT_DATA_TXFORMAT1;

    TX_RESET(23 downto 0)           <= register_map_control.GBT_TX_RESET(23 downto 0);
    TX_TC_METHOD(23 downto 0)       <= register_map_control.GBT_TX_TC_METHOD(23 downto 0);
    TC_EDGE(23 downto 0)            <= register_map_control.GBT_TC_EDGE(23 downto 0);

    register_map_link_monitor.GBT_VERSION.DATE            <=  GBT_VERSION(63 downto 48);
    register_map_link_monitor.GBT_VERSION.GBT_VERSION(35 downto 32)     <=  GBT_VERSION(23 downto 20);
    register_map_link_monitor.GBT_VERSION.GTH_IP_VERSION(19 downto 16)  <=  GBT_VERSION(19 downto 16);
    register_map_link_monitor.GBT_VERSION.RESERVED        <=  GBT_VERSION(15 downto 3);
    register_map_link_monitor.GBT_VERSION.GTHREFCLK_SEL   <=  GBT_VERSION(2 downto 2);
    register_map_link_monitor.GBT_VERSION.RX_CLK_SEL      <=  (others => '0');
    register_map_link_monitor.GBT_VERSION.PLL_SEL         <=  GBT_VERSION(0 downto 0);

    --
    --
    register_map_link_monitor.GBT_TXRESET_DONE(23 downto 0)        <= txresetdone(23 downto 0);
    register_map_link_monitor.GBT_RXRESET_DONE(23 downto 0)        <= rxresetdone(23 downto 0);
    register_map_link_monitor.GBT_TXFSMRESET_DONE(23 downto 0)     <= txfsmresetdone(23 downto 0);--txpmaresetdone(11 downto 0);
    register_map_link_monitor.GBT_RXFSMRESET_DONE(23 downto 0)     <= rxfsmresetdone(23 downto 0);--rxpmaresetdone(11 downto 0);
    register_map_link_monitor.GBT_CPLL_FBCLK_LOST(23 downto 0)     <= cpllfbclklost(23 downto 0);
    register_map_link_monitor.GBT_PLL_LOCK.CPLL_LOCK(23 downto 0)  <= cplllock(23 downto 0);
    register_map_link_monitor.GBT_PLL_LOCK.QPLL_LOCK(53 downto 48) <= (others => '0');
    register_map_link_monitor.GBT_RXCDR_LOCK(23 downto 0)          <= GBT_RXCDR_LOCK;
    register_map_link_monitor.GBT_ALIGNMENT_DONE(23 downto 0)      <= GBT_ALIGNMENT_DONE; --GT_RXByteisAligned(23 downto 0);  --alignment_done(11 downto 0);

    ----------------------------------
    -- Registers for FULL mode
    ----------------------------------
    register_map_link_monitor.GBT_FM_RX_DISP_ERROR1      <= GT_RxDisperr(47 downto 0);
    register_map_link_monitor.GBT_FM_RX_DISP_ERROR2      <= GT_RxDisperr(95 downto 48);
    register_map_link_monitor.GBT_FM_RX_NOTINTABLE1      <= (others => '0');  -- unused
    register_map_link_monitor.GBT_FM_RX_NOTINTABLE2      <= (others => '0');  -- unused


    --------------------------------------------
    --  Some unused registers only for GBT mode
    --------------------------------------------
    register_map_link_monitor.GBT_CLK_SAMPLED     <= (others => '0');
    register_map_link_monitor.GBT_RX_IS_HEADER    <= (others => '0');
    register_map_link_monitor.GBT_RX_IS_DATA      <= (others => '0');
    register_map_link_monitor.GBT_RX_HEADER_FOUND <= (others => '0');
    register_map_link_monitor.GBT_OUT_MUX_STATUS  <= (others => '0');
    register_map_link_monitor.GBT_ERROR           <= (others => '0');
    register_map_link_monitor.GBT_GBT_TOPBOT_C    <= (others => '0');



    -------
    datamod_gen1 : if DYNAMIC_DATA_MODE_EN='1' generate
        DATA_TXFORMAT_i <= DATA_TXFORMAT;
    end generate;

    datamod_gen2 : if DYNAMIC_DATA_MODE_EN='0' generate
        DATA_TXFORMAT_i <= GBT_DATA_TXFORMAT_PACKAGE(47 downto 0);
    end generate;

    autorxreset0: entity work.fullmode_auto_rxreset     generic map(
            GBT_NUM => GBT_NUM
        )
        port map(
            clk40_in => clk40_in,
            register_map_control => register_map_control,
            GBT_ALIGNMENT_DONE => GBT_ALIGNMENT_DONE,
            GT_RXUSRCLK => GT_RXUSRCLK,
            RxDisperr_in => RxDisperr,
            RxDisperr_out => GT_RxDisperr,
            RXRESET_AUTO => RXRESET_AUTO(GBT_NUM-1 downto 0),
            GBT_RXCDR_LOCK => GBT_RXCDR_LOCK,
            rxcdrlock_in => rxcdrlock_40,
            RXByteisAligned => RXByteisAligned_40
        );

    xpm_cdc_RxByteIsAligned : component xpm_cdc_array_single
        generic map(
            DEST_SYNC_FF   => 2,
            INIT_SYNC_FF   => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG  => 0,
            WIDTH          => GBT_NUM
        )
        port map(
            src_clk  => '0',
            src_in   => RXByteisAligned,
            dest_clk => clk40_in,
            dest_out => RXByteisAligned_40
        );

    xpm_cdc_rxcdrlock : component xpm_cdc_array_single
        generic map(
            DEST_SYNC_FF   => 2,
            INIT_SYNC_FF   => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG  => 0,
            WIDTH          => GBT_NUM
        )
        port map(
            src_clk  => '0',
            src_in   => rxcdrlock,
            dest_clk => clk40_in,
            dest_out => rxcdrlock_40
        );



    fullModeAutoRxReset_cnt_g: for i in 0 to GBT_NUM-1 generate
        signal RXRESET_AUTO_CNT: std_logic_vector(31 downto 0);
        signal RXRESET_AUTO_p1: std_logic;
    begin
        cnt_proc: process(clk40_in)
        begin
            if rising_edge(clk40_in) then
                RXRESET_AUTO_p1 <= RXRESET_AUTO(i);
                if rst_hw = '1' or register_map_control.GT_AUTO_RX_RESET_CNT(i).CLEAR = "1" then
                    RXRESET_AUTO_CNT <= (others => '0');
                elsif RXRESET_AUTO(i) = '1' and RXRESET_AUTO_p1 = '0' then
                    RXRESET_AUTO_CNT <= RXRESET_AUTO_CNT + 1;
                end if;
            end if;
        end process;

        register_map_link_monitor.GT_AUTO_RX_RESET_CNT(i).VALUE <= RXRESET_AUTO_CNT;
    end generate;
    g_addGBTTX: if FIRMWARE_MODE = FIRMWARE_MODE_FULL generate
        gbtTx : for i in GBT_NUM-1 downto 0 generate
            signal DATA_TXFORMAT_sync : std_logic_vector(1 downto 0);
            signal TX_TC_DLY_VALUE_sync : std_logic_vector(2 downto 0);
            signal TC_EDGE_sync: std_logic;
            signal TX_RESET_sync: std_logic;
            signal TX_TC_METHOD_sync: std_logic;
        begin

            sync_TXFORMAT : xpm_cdc_array_single
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0,
                    WIDTH => 2
                )
                port map (
                    src_clk => '0',
                    src_in => DATA_TXFORMAT_i(2*i+1 downto 2*i),
                    dest_clk => GT_TX_WORD_CLK(i),
                    dest_out => DATA_TXFORMAT_sync
                );

            sync_TX_TC_DLY_VALUE : xpm_cdc_array_single
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0,
                    WIDTH => 3
                )
                port map (
                    src_clk => '0',
                    src_in => TX_TC_DLY_VALUE(4*i+2 downto 4*i),
                    dest_clk => GT_TX_WORD_CLK(i),
                    dest_out => TX_TC_DLY_VALUE_sync
                );

            sync_TC_EDGE : xpm_cdc_single
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0
                )
                port map (
                    src_clk => '0',
                    src_in => TC_EDGE(i),
                    dest_clk => GT_TX_WORD_CLK(i),
                    dest_out => TC_EDGE_sync
                );

            sync_TX_TC_METHOD : xpm_cdc_single
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0
                )
                port map (
                    src_clk => '0',
                    src_in => TX_TC_METHOD(i),
                    dest_clk => GT_TX_WORD_CLK(i),
                    dest_out => TX_TC_METHOD_sync
                );

            TX_RESET_i(i) <= TX_RESET(i) or (not txresetdone(i)) or (not txfsmresetdone(i));-- for V7

            sync_TX_RESET : xpm_cdc_single
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0
                )
                port map (
                    src_clk => '0',
                    src_in => TX_RESET_i(i),
                    dest_clk => GT_TX_WORD_CLK(i),
                    dest_out => TX_RESET_sync
                );

            process(TX_FRAME_CLK_I)
            begin
                if rising_edge(TX_FRAME_CLK_I) then
                    TX_120b_in_i(i) <= TX_120b_in(i);
                end if;
            end process;

            sync_TX_120b_in : xpm_cdc_array_single
                generic map (
                    DEST_SYNC_FF => 6, --Equivalent to 1 40 MHz cycle.
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0,
                    WIDTH => 120
                )
                port map (
                    src_clk => clk40_in,
                    src_in => TX_120b_in(i),
                    dest_clk => GT_TX_WORD_CLK(i),
                    dest_out => TX_120b_in_wc(i)
                );

            gbtTx_inst: entity work.gbtTx_FELIX
                generic map (
                    channel                     => i
                )
                Port map (
                    TX_FLAG => TX_FLAG_O(i),
                    TX_RESET_I => TX_RESET_sync,
                    TX_FRAMECLK_I => TX_FRAME_CLK_I,
                    TX_WORDCLK_I => GT_TX_WORD_CLK(i),
                    --Tx_latopt_scr => TX_OPT(24+i),
                    --TX_LATOPT_TC => TX_OPT(i),
                    TX_TC_METHOD => TX_TC_METHOD_sync,
                    TC_EDGE => TC_EDGE_sync,
                    DATA_MODE_CFG => DATA_TXFORMAT_sync,
                    TX_TC_DLY_VALUE => TX_TC_DLY_VALUE_sync,
                    TX_HEADER_I => TX_120b_in_i(i)(119 downto 116),
                    TX_DATA_84b_I => TX_120b_in_wc(i)(115 downto 32),
                    TX_EXTRA_DATA_WIDEBUS_I => TX_120b_in_wc(i)(31 downto 0),
                    TX_DATA_20b_O => TX_DATA_20b(i)
                );

        end generate gbtTx;
    end generate g_addGBTTX;



    g_709c: if CARD_TYPE = 709 or CARD_TYPE = 710 generate
        clk_generate_709: for i in 0 to GBT_NUM/4-1 generate

            GTTXOUTCLK_BUFG: BUFG
                port map (
                    O => GT_TXUSRCLK(4*i),
                    I => GT_TXOUTCLK(4*i)
                );
            GT_TX_WORD_CLK(i*4) <= GT_TXUSRCLK(i*4);
            GT_TX_WORD_CLK(i*4+1) <= GT_TXUSRCLK(i*4);
            GT_TX_WORD_CLK(i*4+2) <= GT_TXUSRCLK(i*4);
            GT_TX_WORD_CLK(i*4+3) <= GT_TXUSRCLK(i*4);

            GT_TXUSRCLK(i*4+1) <= GT_TXUSRCLK(i*4);
            GT_TXUSRCLK(i*4+2) <= GT_TXUSRCLK(i*4);
            GT_TXUSRCLK(i*4+3) <= GT_TXUSRCLK(i*4);
        end generate;
    end generate;

    g_712c: if CARD_TYPE = 711 or CARD_TYPE = 712 or CARD_TYPE = 128 or CARD_TYPE = 800 generate
        clk_generate_712 : for i in (GBT_NUM-1) downto 0 generate

            GTTXOUTCLK_BUFG: BUFG_GT
                generic map(
                    SIM_DEVICE => "ULTRASCALE",
                    STARTUP_SYNC => "FALSE"
                )
                port map (
                    O       => GT_TXUSRCLK(i),
                    CE      => '1',           -- 1-bit input: Buffer enable
                    CEMASK  => '0',   -- 1-bit input: CE Mask
                    CLR     => '0',         -- 1-bit input: Asynchronous clear
                    CLRMASK => '0', -- 1-bit input: CLR Mask
                    DIV     => "000",         -- 3-bit input: Dynamic divide Value
                    I       => GT_TXOUTCLK(i)              -- 1-bit input: Buffer
                );
            GT_TX_WORD_CLK(i) <= GT_TXUSRCLK(i);
        end generate;
    end generate;

    ---------------------------------
    -- GTH TX using CPLL
    -- GTH RX using CPLL
    ---------------------------------
    g_notversal: if CARD_TYPE /= 180 and CARD_TYPE /= 181 and CARD_TYPE /= 182 and CARD_TYPE /= 155 generate --Versal has a transceiver block design that goes per quad rather than per channel
        GTH_inst: for i in (GBT_NUM-1) downto 0 generate
            signal SOFT_TXRST_GT_g, SOFT_RXRST_GT_g: std_logic;
            signal SOFT_TXRST_GT_g_sync, SOFT_RXRST_GT_g_sync: std_logic;
        begin
            SOFT_TXRST_GT_g             <= SOFT_TXRST_GT(i) or SOFT_TXRST_ALL(i/4) or rst_hw;
            SOFT_RXRST_GT_g             <= SOFT_RXRST_GT(i) or SOFT_RXRST_ALL(i/4) or rst_hw;
            xpm_SOFT_RXRST_GT_sync : xpm_cdc_sync_rst
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT => 1,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0
                )
                port map (
                    src_rst => SOFT_RXRST_GT_g,
                    dest_clk => DRP_CLK_IN,
                    dest_rst => SOFT_RXRST_GT_g_sync
                );

            xpm_SOFT_TXRST_GT_sync : xpm_cdc_sync_rst
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT => 1,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0
                )
                port map (
                    src_rst => SOFT_TXRST_GT_g,
                    dest_clk => DRP_CLK_IN,
                    dest_rst => SOFT_TXRST_GT_g_sync
                );
            g_709d: if CARD_TYPE = 709 or CARD_TYPE = 710 generate
                g_FULL_GBT: if FIRMWARE_MODE = FIRMWARE_MODE_FULL generate
                    GTH_FM_TOP_INST: if FULL_HALFRATE = false generate
                        gth1: entity work.gth_fullmode_wrapper_v7
                            Port map (
                                ---------- Clocks
                                GTH_RefClk => GTH_RefClk(i),
                                DRP_CLK_IN => DRP_CLK_IN,
                                gt0_rxusrclk_in => GT_RXUSRCLK(i),
                                gt0_rxoutclk_out => GT_RXOUTCLK(i),
                                gt0_txusrclk_in => GT_TXUSRCLK(i),
                                gt0_txoutclk_out => GT_TXOUTCLK(i),
                                gt_txresetdone_out => txresetdone(i),
                                gt_rxresetdone_out => rxresetdone(i),
                                gt_txfsmresetdone_out => txfsmresetdone(i),
                                gt_rxfsmresetdone_out => rxfsmresetdone(i),
                                gt_cpllfbclklost_out => cpllfbclklost(i),
                                gt_cplllock_out => cplllock(i),
                                gt_rxcdrlock_out => rxcdrlock(i),
                                gt0_rxdisperr_out => RxDisperr(4*i+3 downto 4*i),
                                gt0_rxnotintable_out => open, --RxNotIntable(16*i+3 downto 4*i),
                                gt_rxbyteisaligned_out => RXByteisAligned(i),
                                gt_cpllreset_in => cpll_reset(i),
                                gt_txuserrdy_in => txusrrdy(i),
                                gt_rxuserrdy_in => rxusrrdy(i),
                                GTTX_RESET_IN => gttx_reset(i), -- or rst_hw,
                                GTRX_RESET_IN => gtrx_reset(i),
                                SOFT_TXRST_ALL => SOFT_TXRST_GT_g_sync,
                                SOFT_RXRST_ALL => SOFT_RXRST_GT_g_sync,
                                RX_DATA_gt0_33b => RX_DATA_33b_s(i),
                                TX_DATA_gt0_20b => TX_DATA_20b(i),
                                TX_DATA_gt0_33b => TX_DATA_33b(i),
                                tx8b10ben_in => tx8b10ben_txusrclk(i),
                                RXN_IN => RX_N(i),
                                RXP_IN => RX_P(i),
                                TXN_OUT => TX_N(i),
                                TXP_OUT => TX_P(i)

                            );
                    end generate GTH_FM_TOP_INST;
                    GTH_FM_TOP_INST_48G: if FULL_HALFRATE generate
                    begin
                        gth1: entity work.gth_fullmode_wrapper_48g_v7
                            Port map (
                                ---------- Clocks
                                GTH_RefClk => GTH_RefClk(i),
                                DRP_CLK_IN => DRP_CLK_IN,
                                gt0_rxusrclk_in => GT_RXUSRCLK(i),
                                gt0_rxoutclk_out => GT_RXOUTCLK(i),
                                gt0_txusrclk_in => GT_TXUSRCLK(i),
                                gt0_txoutclk_out => GT_TXOUTCLK(i),
                                gt_txresetdone_out => txresetdone(i),
                                gt_rxresetdone_out => rxresetdone(i),
                                gt_txfsmresetdone_out => txfsmresetdone(i),
                                gt_rxfsmresetdone_out => rxfsmresetdone(i),
                                gt_cpllfbclklost_out => cpllfbclklost(i),
                                gt_cplllock_out => cplllock(i),
                                gt_rxcdrlock_out => rxcdrlock(i),
                                gt0_rxdisperr_out => RxDisperr(4*i+3 downto 4*i),
                                gt0_rxnotintable_out => open, --RxNotIntable(16*i+3 downto 4*i),
                                gt_rxbyteisaligned_out => RXByteisAligned(i),
                                gt_cpllreset_in => cpll_reset(i),
                                gt_txuserrdy_in => txusrrdy(i),
                                gt_rxuserrdy_in => rxusrrdy(i),
                                GTTX_RESET_IN => gttx_reset(i), -- or rst_hw,
                                GTRX_RESET_IN => gtrx_reset(i),
                                SOFT_TXRST_ALL => SOFT_TXRST_GT_g_sync,
                                SOFT_RXRST_ALL => SOFT_RXRST_GT_g_sync,
                                RX_DATA_gt0_33b => RX_DATA_33b_s(i),
                                TX_DATA_gt0_20b => TX_DATA_20b(i),
                                RXN_IN => RX_N(i),
                                RXP_IN => RX_P(i),
                                TXN_OUT => TX_N(i),
                                TXP_OUT => TX_P(i)

                            );
                    end generate GTH_FM_TOP_INST_48G;
                end generate g_FULL_GBT;
            end generate g_709d;
            g_712d: if CARD_TYPE = 711 or CARD_TYPE = 712 generate
                g_FULL_GBT: if FIRMWARE_MODE = FIRMWARE_MODE_FULL generate
                    GTH_FM_TOP_INST:if FULL_HALFRATE = false generate
                    begin
                        gth1: entity work.gth_fullmode_wrapper_ku
                            Port map(
                                ---------- Clocks
                                GTH_RefClk => GTH_RefClk(i),
                                DRP_CLK_IN => DRP_CLK_IN,
                                gt0_rxusrclk_in => GT_RXUSRCLK(i), --clk240_in,
                                gt0_rxoutclk_out => GT_RXOUTCLK(i),
                                gt0_txusrclk_in => GT_TXUSRCLK(i),
                                gt0_txoutclk_out => GT_TXOUTCLK(i),
                                reset_all_in => soft_reset(i),
                                cpllreset_in => cpll_reset(i downto i),
                                rxcommadeten_in => '1',
                                reset_tx_pll_and_datapath_in => SOFT_TXRST_GT_g,
                                reset_tx_datapath_in => SOFT_TXRST_GT_g,
                                reset_rx_pll_and_datapath_in => qpll_reset(i/4),
                                reset_rx_datapath_in => SOFT_RXRST_GT_g,
                                gt_cplllock_out => cplllock(i),
                                gt_cpllfbclklost_out => cpllfbclklost(i),
                                gt_txresetdone_out => txresetdone(i),
                                gt_rxresetdone_out => rxresetdone(i),
                                gt_txfsmresetdone_out => txfsmresetdone(i),
                                gt_rxfsmresetdone_out => rxfsmresetdone(i),
                                gt_rxcdrlock_out => rxcdrlock(i),
                                gt_rxbyteisaligned_out => RXByteisAligned(i),
                                gt0_rxdisperr_out => RxDisperr(4*i+3 downto 4*i),
                                RX_DATA_gt0_33b => RX_DATA_33b_s(i), --gt_rx_data_33b(4*i),  --RX_DATA_33b(4*i),
                                TX_DATA_gt0_20b => TX_DATA_20b(i),
                                TX_DATA_gt0_33b => TX_DATA_33b(i),
                                tx8b10ben_in => tx8b10ben_txusrclk(i),
                                RXN_IN => RX_N(i),
                                RXP_IN => RX_P(i),
                                TXN_OUT => TX_N(i),
                                TXP_OUT => TX_P(i)

                            );
                    end generate GTH_FM_TOP_INST;
                    GTH_FM_TOP_48G: if FULL_HALFRATE generate
                    begin
                        gth1: entity work.gth_fullmode_wrapper_48g_ku
                            Port map(
                                ---------- Clocks
                                GTH_RefClk => GTH_RefClk(i),
                                DRP_CLK_IN => DRP_CLK_IN,
                                gt0_rxusrclk_in => GT_RXUSRCLK(i), --clk240_in,
                                gt0_rxoutclk_out => GT_RXOUTCLK(i),
                                gt0_txusrclk_in => GT_TXUSRCLK(i),
                                gt0_txoutclk_out => GT_TXOUTCLK(i),
                                reset_all_in => soft_reset(i),
                                cpllreset_in => cpll_reset(i downto i),
                                rxcommadeten_in => '1',
                                reset_tx_pll_and_datapath_in => SOFT_TXRST_GT_g,
                                reset_tx_datapath_in => SOFT_TXRST_GT_g,
                                reset_rx_pll_and_datapath_in => qpll_reset(i/4),
                                reset_rx_datapath_in => SOFT_RXRST_GT_g,
                                gt_cplllock_out => cplllock(i),
                                gt_cpllfbclklost_out => cpllfbclklost(i),
                                gt_txresetdone_out => txresetdone(i),
                                gt_rxresetdone_out => rxresetdone(i),
                                gt_txfsmresetdone_out => txfsmresetdone(i),
                                gt_rxfsmresetdone_out => rxfsmresetdone(i),
                                gt_rxcdrlock_out => rxcdrlock(i),
                                gt_rxbyteisaligned_out => RXByteisAligned(i),
                                gt0_rxdisperr_out => RxDisperr(4*i+3 downto 4*i),
                                RX_DATA_gt0_33b => RX_DATA_33b_s(i), --gt_rx_data_33b(4*i),  --RX_DATA_33b(4*i),
                                TX_DATA_gt0_20b => TX_DATA_20b(i),
                                RXN_IN => RX_N(i),
                                RXP_IN => RX_P(i),
                                TXN_OUT => TX_N(i),
                                TXP_OUT => TX_P(i)

                            );
                    end generate GTH_FM_TOP_48G;
                end generate g_FULL_GBT;
            end generate g_712d;
            g_128d: if CARD_TYPE = 128 or CARD_TYPE = 800 or CARD_TYPE = 801 generate
                GTH_FM_TOP_INST:if FULL_HALFRATE = false generate
                begin
                    gth1: entity work.gth_fullmode_wrapper_vup
                        Port map(
                            ---------- Clocks
                            GTH_RefClk => GTH_RefClk(i),
                            DRP_CLK_IN => DRP_CLK_IN,
                            gt0_rxusrclk_in => GT_RXUSRCLK(i), --clk240_in,
                            gt0_rxoutclk_out => GT_RXOUTCLK(i),
                            gt0_txusrclk_in => GT_TXUSRCLK(i),
                            gt0_txoutclk_out => GT_TXOUTCLK(i),
                            reset_all_in => soft_reset(i),
                            cpllreset_in => cpll_reset(i downto i),
                            rxcommadeten_in => '1',
                            reset_tx_pll_and_datapath_in => SOFT_TXRST_GT_g,
                            reset_tx_datapath_in => SOFT_TXRST_GT_g,
                            reset_rx_pll_and_datapath_in => qpll_reset(i/4),
                            reset_rx_datapath_in => SOFT_RXRST_GT_g,
                            gt_cplllock_out => cplllock(i),
                            gt_cpllfbclklost_out => cpllfbclklost(i),
                            gt_txresetdone_out => txresetdone(i),
                            gt_rxresetdone_out => rxresetdone(i),
                            gt_txfsmresetdone_out => txfsmresetdone(i),
                            gt_rxfsmresetdone_out => rxfsmresetdone(i),
                            gt_rxcdrlock_out => rxcdrlock(i),
                            gt_rxbyteisaligned_out => RXByteisAligned(i),
                            gt0_rxdisperr_out => RxDisperr(4*i+3 downto 4*i),
                            RX_DATA_gt0_33b => RX_DATA_33b_s(i), --gt_rx_data_33b(4*i),  --RX_DATA_33b(4*i),
                            TX_DATA_gt0_20b => TX_DATA_20b(i),
                            RXN_IN => RX_N(i),
                            RXP_IN => RX_P(i),
                            TXN_OUT => TX_N(i),
                            TXP_OUT => TX_P(i)

                        );
                end generate;
                GTH_FM_TOP_48G: if FULL_HALFRATE generate
                begin
                    gth1: entity work.gth_fullmode_wrapper_48g_vup
                        Port map(
                            ---------- Clocks
                            GTH_RefClk => GTH_RefClk(i),
                            DRP_CLK_IN => DRP_CLK_IN,
                            gt0_rxusrclk_in => GT_RXUSRCLK(i), --clk240_in,
                            gt0_rxoutclk_out => GT_RXOUTCLK(i),
                            gt0_txusrclk_in => GT_TXUSRCLK(i),
                            gt0_txoutclk_out => GT_TXOUTCLK(i),
                            reset_all_in => soft_reset(i),
                            cpllreset_in => cpll_reset(i downto i),
                            rxcommadeten_in => '1',
                            reset_tx_pll_and_datapath_in => SOFT_TXRST_GT_g,
                            reset_tx_datapath_in => SOFT_TXRST_GT_g,
                            reset_rx_pll_and_datapath_in => qpll_reset(i/4),
                            reset_rx_datapath_in => SOFT_RXRST_GT_g,
                            gt_cplllock_out => cplllock(i),
                            gt_cpllfbclklost_out => cpllfbclklost(i),
                            gt_txresetdone_out => txresetdone(i),
                            gt_rxresetdone_out => rxresetdone(i),
                            gt_txfsmresetdone_out => txfsmresetdone(i),
                            gt_rxfsmresetdone_out => rxfsmresetdone(i),
                            gt_rxcdrlock_out => rxcdrlock(i),
                            gt_rxbyteisaligned_out => RXByteisAligned(i),
                            gt0_rxdisperr_out => RxDisperr(4*i+3 downto 4*i),
                            RX_DATA_gt0_33b => RX_DATA_33b_s(i), --gt_rx_data_33b(4*i),  --RX_DATA_33b(4*i),
                            TX_DATA_gt0_20b => TX_DATA_20b(i),
                            RXN_IN => RX_N(i),
                            RXP_IN => RX_P(i),
                            TXN_OUT => TX_N(i),
                            TXP_OUT => TX_P(i)

                        );
                end generate;
            end generate;
        end generate;
    end generate; --not versal
    g_versal: if CARD_TYPE = 180 or CARD_TYPE = 181 or CARD_TYPE = 182 or CARD_TYPE = 155 generate
    begin
        g_versal_quads: for quad in 0 to GBT_NUM/4-1 generate
            signal SOFT_TXRST_GT_g, SOFT_RXRST_GT_g: std_logic;
            signal SOFT_TXRST_GT_g_sync, SOFT_RXRST_GT_g_sync: std_logic; -- @suppress "signal SOFT_TXRST_GT_g_sync is never read" -- @suppress "signal SOFT_RXRST_GT_g_sync is never read"
            signal soft_reset_sync: std_logic;
            signal lcpll_lock_01: std_logic_vector(1 downto 0);
            signal lcpll_lock: std_logic;
            --signal rxusrclk_quad, txusrclk_quad: std_logic;
            signal ch0_rxctrl0: std_logic_vector(15 downto 0);
            signal ch0_rxctrl1: std_logic_vector(15 downto 0);
            signal ch0_rxctrl2: std_logic_vector(7 downto 0); -- @suppress "Signal ch0_rxctrl2 is never read"
            signal ch0_rxctrl3: std_logic_vector(7 downto 0); -- @suppress "Signal ch0_rxctrl3 is never read"
            signal ch0_userdata_rx: std_logic_vector(127 downto 0);
            signal ch0_userdata_tx: std_logic_vector(127 downto 0);
            signal ch1_rxctrl0: std_logic_vector(15 downto 0);
            signal ch1_rxctrl1: std_logic_vector(15 downto 0);
            signal ch1_rxctrl2: std_logic_vector(7 downto 0); -- @suppress "Signal ch1_rxctrl2 is never read"
            signal ch1_rxctrl3: std_logic_vector(7 downto 0); -- @suppress "Signal ch1_rxctrl3 is never read"
            signal ch1_userdata_rx: std_logic_vector(127 downto 0);
            signal ch1_userdata_tx: std_logic_vector(127 downto 0);
            signal ch2_rxctrl0: std_logic_vector(15 downto 0);
            signal ch2_rxctrl1: std_logic_vector(15 downto 0);
            signal ch2_rxctrl2: std_logic_vector(7 downto 0); -- @suppress "Signal ch2_rxctrl2 is never read"
            signal ch2_rxctrl3: std_logic_vector(7 downto 0); -- @suppress "Signal ch2_rxctrl3 is never read"
            signal ch2_userdata_rx: std_logic_vector(127 downto 0);
            signal ch2_userdata_tx: std_logic_vector(127 downto 0);
            signal ch3_rxctrl0: std_logic_vector(15 downto 0);
            signal ch3_rxctrl1: std_logic_vector(15 downto 0);
            signal ch3_rxctrl2: std_logic_vector(7 downto 0); -- @suppress "Signal ch3_rxctrl2 is never read"
            signal ch3_rxctrl3: std_logic_vector(7 downto 0); -- @suppress "Signal ch3_rxctrl3 is never read"
            signal ch3_userdata_rx: std_logic_vector(127 downto 0);
            signal ch3_userdata_tx: std_logic_vector(127 downto 0);
            signal K28_5_aligned: std_logic_vector(3 downto 0);
            component transceiver_versal_full_wrapper is -- @suppress "Component declaration 'transceiver_versal_full_wrapper' has none or multiple matching entity declarations"
                port (
                    GT_REFCLK0_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
                    GT_REFCLK0_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
                    GT_Serial_grx_n : in STD_LOGIC_VECTOR ( 3 downto 0 );
                    GT_Serial_grx_p : in STD_LOGIC_VECTOR ( 3 downto 0 );
                    GT_Serial_gtx_n : out STD_LOGIC_VECTOR ( 3 downto 0 );
                    GT_Serial_gtx_p : out STD_LOGIC_VECTOR ( 3 downto 0 );
                    INTF0_RX0_ch_rxrate_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                    INTF0_RX1_ch_rxrate_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                    INTF0_RX2_ch_rxrate_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                    INTF0_RX3_ch_rxrate_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                    INTF0_TX0_ch_txrate_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                    INTF0_TX1_ch_txrate_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                    INTF0_TX2_ch_txrate_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                    INTF0_TX3_ch_txrate_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                    QUAD0_hsclk0_lcplllock_0 : out STD_LOGIC;
                    QUAD0_hsclk1_lcplllock_0 : out STD_LOGIC;
                    ch0_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
                    ch0_rxcdrlock_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch0_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch0_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch0_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch0_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch0_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch0_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch0_rxgearboxslip_ext_0 : in STD_LOGIC;
                    ch0_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
                    ch0_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch0_rxpolarity_ext_0 : in STD_LOGIC;
                    ch0_rxresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch0_rxslide_ext_0 : in STD_LOGIC;
                    ch0_rxvalid_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch0_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch0_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch0_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch0_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch0_txheader_ext_0 : in STD_LOGIC_VECTOR ( 5 downto 0 );
                    ch0_txpolarity_ext_0 : in STD_LOGIC;
                    ch0_txresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch0_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
                    ch1_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
                    ch1_rxcdrlock_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch1_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch1_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch1_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch1_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch1_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch1_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch1_rxgearboxslip_ext_0 : in STD_LOGIC;
                    ch1_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
                    ch1_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch1_rxpolarity_ext_0 : in STD_LOGIC;
                    ch1_rxresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch1_rxslide_ext_0 : in STD_LOGIC;
                    ch1_rxvalid_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch1_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch1_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch1_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch1_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch1_txpolarity_ext_0 : in STD_LOGIC;
                    ch1_txresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch1_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
                    ch2_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
                    ch2_rxcdrlock_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch2_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch2_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch2_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch2_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch2_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch2_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch2_rxgearboxslip_ext_0 : in STD_LOGIC;
                    ch2_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
                    ch2_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch2_rxpolarity_ext_0 : in STD_LOGIC;
                    ch2_rxresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch2_rxslide_ext_0 : in STD_LOGIC;
                    ch2_rxvalid_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch2_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch2_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch2_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch2_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch2_txpolarity_ext_0 : in STD_LOGIC;
                    ch2_txresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch2_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
                    ch3_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
                    ch3_rxcdrlock_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch3_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch3_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch3_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch3_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch3_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch3_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch3_rxgearboxslip_ext_0 : in STD_LOGIC;
                    ch3_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
                    ch3_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch3_rxpolarity_ext_0 : in STD_LOGIC;
                    ch3_rxresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch3_rxslide_ext_0 : in STD_LOGIC;
                    ch3_rxvalid_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch3_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch3_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch3_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch3_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch3_txpolarity_ext_0 : in STD_LOGIC;
                    ch3_txresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch3_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
                    gt_reset_gt_bridge_ip_0 : in STD_LOGIC;
                    gtwiz_freerun_clk_0 : in STD_LOGIC;
                    reset_rx_datapath_in_0 : in STD_LOGIC;
                    reset_rx_pll_and_datapath_in_0 : in STD_LOGIC;
                    reset_tx_datapath_in_0 : in STD_LOGIC;
                    reset_tx_pll_and_datapath_in_0 : in STD_LOGIC;
                    rx_resetdone_out_gt_bridge_ip_0 : out STD_LOGIC;
                    rxusrclk0_out : out STD_LOGIC;
                    rxusrclk1_out : out STD_LOGIC;
                    rxusrclk2_out : out STD_LOGIC;
                    rxusrclk3_out : out STD_LOGIC;
                    tx_resetdone_out_gt_bridge_ip_0 : out STD_LOGIC;
                    txusrclk0_out : out STD_LOGIC;
                    txusrclk1_out : out STD_LOGIC;
                    txusrclk2_out : out STD_LOGIC;
                    txusrclk3_out : out STD_LOGIC
                );
            end component transceiver_versal_full_wrapper;
        begin
            SOFT_TXRST_GT_g             <= SOFT_TXRST_GT(quad*4) or SOFT_TXRST_ALL(quad) or rst_hw;
            SOFT_RXRST_GT_g             <= SOFT_RXRST_GT(quad*4) or SOFT_RXRST_ALL(quad) or rst_hw;
            xpm_SOFT_RXRST_GT_sync : xpm_cdc_sync_rst
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT => 1,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0
                )
                port map (
                    src_rst => SOFT_RXRST_GT_g,
                    dest_clk => clk100_in,
                    dest_rst => SOFT_RXRST_GT_g_sync
                );

            xpm_SOFT_TXRST_GT_sync : xpm_cdc_sync_rst
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT => 1,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0
                )
                port map (
                    src_rst => SOFT_TXRST_GT_g,
                    dest_clk => clk100_in,
                    dest_rst => SOFT_TXRST_GT_g_sync
                );

            xpm_soft_reset_sync : xpm_cdc_sync_rst
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT => 1,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0
                )
                port map (
                    src_rst => soft_reset(quad*4),
                    dest_clk => clk100_in,
                    dest_rst => soft_reset_sync
                );

            RX_DATA_33b_s(quad*4+0) <= ch0_rxctrl0(0) & ch0_userdata_rx(31 downto 0);
            RX_DATA_33b_s(quad*4+1) <= ch1_rxctrl0(0) & ch1_userdata_rx(31 downto 0);
            RX_DATA_33b_s(quad*4+2) <= ch2_rxctrl0(0) & ch2_userdata_rx(31 downto 0);
            RX_DATA_33b_s(quad*4+3) <= ch3_rxctrl0(0) & ch3_userdata_rx(31 downto 0);
            lcpll_lock <= lcpll_lock_01(0) and lcpll_lock_01(1);
            cplllock(quad*4+3 downto quad*4) <= (others => lcpll_lock);

            RXUSERCLK_OUT(quad*4+3 downto quad*4) <= GT_RXUSRCLK(quad*4+3 downto quad*4);
            TXUSERCLK_OUT(quad*4+3 downto quad*4) <= GT_TXUSRCLK(quad*4+3 downto quad*4);
            GT_TX_WORD_CLK(quad*4+3 downto quad*4) <= GT_TXUSRCLK(quad*4+3 downto quad*4);

            g_fullGBT: if FIRMWARE_MODE = FIRMWARE_MODE_FULL generate
                signal ch0_txctrl0, ch1_txctrl0, ch2_txctrl0, ch3_txctrl0 : std_logic_vector(15 downto 0);
                signal ch0_txctrl1, ch1_txctrl1, ch2_txctrl1, ch3_txctrl1 : std_logic_vector(15 downto 0);
                signal ch0_txctrl2, ch1_txctrl2, ch2_txctrl2, ch3_txctrl2 : std_logic_vector(7 downto 0);
            begin


                protocol_select_proc: process(all)
                begin
                    ch0_txctrl0 <= (others => '0');
                    ch0_txctrl1 <= (others => '0');
                    ch0_txctrl2 <= (others => '0');
                    ch1_txctrl0 <= (others => '0');
                    ch1_txctrl1 <= (others => '0');
                    ch1_txctrl2 <= (others => '0');
                    ch2_txctrl0 <= (others => '0');
                    ch2_txctrl1 <= (others => '0');
                    ch2_txctrl2 <= (others => '0');
                    ch3_txctrl0 <= (others => '0');
                    ch3_txctrl1 <= (others => '0');
                    ch3_txctrl2 <= (others => '0');
                    ch0_userdata_tx(127 downto 20) <= (others => '0');
                    ch1_userdata_tx(127 downto 20) <= (others => '0');
                    ch2_userdata_tx(127 downto 20) <= (others => '0');
                    ch3_userdata_tx(127 downto 20) <= (others => '0');
                    if tx8b10ben_txusrclk(quad*4+0) = '0' then --Transmit GBT protocl, 4.8Gb/s
                        ch0_userdata_tx(19 downto 0) <= TX_DATA_20b(quad*4+0)(19 downto 0);
                    else
                        ch0_txctrl2(0) <= TX_DATA_33b(quad*4+0)(32);
                        ch0_userdata_tx(31 downto 0) <= TX_DATA_33b(quad*4+0)(31 downto 0);
                    end if;

                    if tx8b10ben_txusrclk(quad*4+1) = '0' then --Transmit GBT protocl, 4.8Gb/s
                        ch1_userdata_tx(19 downto 0) <= TX_DATA_20b(quad*4+1)(19 downto 0);
                    else
                        ch1_txctrl2(0) <= TX_DATA_33b(quad*4+1)(32);
                        ch1_userdata_tx(31 downto 0) <= TX_DATA_33b(quad*4+1)(31 downto 0);
                    end if;

                    if tx8b10ben_txusrclk(quad*4+2) = '0' then --Transmit GBT protocl, 4.8Gb/s
                        ch2_userdata_tx(19 downto 0) <= TX_DATA_20b(quad*4+2)(19 downto 0);

                    else
                        ch2_txctrl2(0) <= TX_DATA_33b(quad*4+2)(32);
                        ch2_userdata_tx(31 downto 0) <= TX_DATA_33b(quad*4+2)(31 downto 0);
                    end if;

                    if tx8b10ben_txusrclk(quad*4+3) = '0' then --Transmit GBT protocl, 4.8Gb/s
                        ch3_userdata_tx(19 downto 0) <= TX_DATA_20b(quad*4+3)(19 downto 0);
                    else
                        ch3_txctrl2(0) <= TX_DATA_33b(quad*4+3)(32);
                        ch3_userdata_tx(31 downto 0) <= TX_DATA_33b(quad*4+3)(31 downto 0);
                    end if;

                end process;
                RxDispErr(quad*16+15 downto quad*16) <= ch3_rxctrl1(3 downto 0)&
                                                        ch2_rxctrl1(3 downto 0)&
                                                        ch1_rxctrl1(3 downto 0)&
                                                        ch0_rxctrl1(3 downto 0);
                rxcdrlock(quad*4+3 downto quad*4) <= RXByteisAligned(quad*4+3 downto quad*4+0) and K28_5_aligned;


                g_check_ch: for i in 0 to 3 generate
                    check_k28_5_proc: process(GT_RXUSRCLK(quad*4+i))
                        variable cnt: unsigned(15 downto 0) := x"0000";
                        variable K28_5_found_v: std_logic := '0';
                    begin
                        if rising_edge(GT_RXUSRCLK(quad*4+i)) then
                            cnt := cnt + 1;
                            if RX_DATA_33b_s(quad*4+i)(7 downto 0) = x"BC" and RX_DATA_33b_s(quad*4+i)(32) = '1' then
                                K28_5_found_v := '1';
                            end if;
                            if cnt = x"0000" then
                                if K28_5_found_v = '1' then
                                    K28_5_aligned(i) <= '1';
                                else
                                    K28_5_aligned(i) <= '0';
                                end if;
                                K28_5_found_v := '0';
                            end if;
                        end if;
                    end process;
                end generate;

                g_ratesel_cdc: for i in 0 to 3 generate
                begin
                    --rate_sel "0000" to transmit 4.8Gb GBT, "0001" to transmit 9.6Gb 8b10b
                    rate_sel_RX_sync: xpm_cdc_array_single generic map(
                            DEST_SYNC_FF => 2,
                            INIT_SYNC_FF => 0,
                            SIM_ASSERT_CHK => 0,
                            SRC_INPUT_REG => 0,
                            WIDTH => 8
                        )
                        port map(
                            src_clk => '0',
                            src_in => "0000000"&register_map_control.LINK_FULLMODE_LTI(quad*4+i),
                            dest_clk => GT_RXUSRCLK(quad*4+i),
                            dest_out => rxrate_ch(i)
                        );
                    rate_sel_TX_sync: xpm_cdc_array_single generic map(
                            DEST_SYNC_FF => 2,
                            INIT_SYNC_FF => 0,
                            SIM_ASSERT_CHK => 0,
                            SRC_INPUT_REG => 0,
                            WIDTH => 8
                        )
                        port map(
                            src_clk => '0',
                            src_in => "0000000"&register_map_control.LINK_FULLMODE_LTI(quad*4+i),
                            dest_clk => GT_TXUSRCLK(quad*4+i),
                            dest_out => txrate_ch(i)
                        );

                end generate;

                -- Channels are flipped to use the same pinout as the FLX-712
                -- Transceiver channel -> Data
                -- CH0 -> CH3
                -- CH1 -> CH2
                -- CH2 -> CH1
                -- CH3 -> CH0
                RXByteisAligned(quad*4+3 downto quad*4+0) <= rxresetdone(quad*4+3 downto quad*4+0); --No RXByteIsAligned on Versal IP, K28.5 decoding is done in a process
                txfsmresetdone(quad*4+3 downto quad*4+0) <=  txresetdone(quad*4+3 downto quad*4+0); --Drives GBT reset logic.
                versal_fm_quad: transceiver_versal_full_wrapper
                    port map(
                        GT_REFCLK0_clk_n => GTREFCLK_N_IN(quad downto quad), --in STD_LOGIC_VECTOR ( 0 to 0 );
                        GT_REFCLK0_clk_p => GTREFCLK_P_IN(quad downto quad), --in STD_LOGIC_VECTOR ( 0 to 0 );
                        GT_Serial_grx_n => RX_N(quad*4+3 downto quad*4), --in STD_LOGIC_VECTOR ( 3 downto 0 );
                        GT_Serial_grx_p => RX_P(quad*4+3 downto quad*4), --in STD_LOGIC_VECTOR ( 3 downto 0 );
                        GT_Serial_gtx_n => TX_N(quad*4+3 downto quad*4), --out STD_LOGIC_VECTOR ( 3 downto 0 );
                        GT_Serial_gtx_p => TX_P(quad*4+3 downto quad*4), --out STD_LOGIC_VECTOR ( 3 downto 0 );
                        gtwiz_freerun_clk_0 => clk100_in,
                        ch0_loopback_0 =>  "000",
                        ch0_rxcdrlock_ext_0 => open, --out STD_LOGIC;
                        ch0_rxctrl0_ext_0 => ch3_rxctrl0, --out STD_LOGIC_VECTOR ( 15 downto 0 );
                        ch0_rxctrl1_ext_0 => ch3_rxctrl1, --out STD_LOGIC_VECTOR ( 15 downto 0 );
                        ch0_rxctrl2_ext_0 => ch3_rxctrl2, --out STD_LOGIC_VECTOR ( 7 downto 0 );
                        ch0_rxctrl3_ext_0 => ch3_rxctrl3, --out STD_LOGIC_VECTOR ( 7 downto 0 );
                        ch0_rxdata_ext_0 => ch3_userdata_rx, --out STD_LOGIC_VECTOR ( 127 downto 0 );
                        ch0_rxdatavalid_ext_0 => open, --out STD_LOGIC_VECTOR ( 1 downto 0 );
                        ch0_rxgearboxslip_ext_0 => '0', --in STD_LOGIC;
                        ch0_rxheader_ext_0 => open, --out STD_LOGIC_VECTOR ( 5 downto 0 );
                        ch0_rxheadervalid_ext_0 => open, --out STD_LOGIC_VECTOR ( 1 downto 0 );
                        ch0_rxpolarity_ext_0 => '0' , --in STD_LOGIC;
                        ch0_rxresetdone_ext_0(0) => rxresetdone(quad*4+3), --out STD_LOGIC;
                        ch0_rxslide_ext_0 => '0', --in STD_LOGIC;
                        ch0_rxvalid_ext_0 => open, --out STD_LOGIC;
                        ch0_txctrl0_ext_0 => ch3_txctrl0 , --in STD_LOGIC_VECTOR ( 15 downto 0 );
                        ch0_txctrl1_ext_0 => ch3_txctrl1 , --in STD_LOGIC_VECTOR ( 15 downto 0 );
                        ch0_txctrl2_ext_0 => ch3_txctrl2, --in STD_LOGIC_VECTOR ( 7 downto 0 );
                        ch0_txdata_ext_0 => ch3_userdata_tx, --in STD_LOGIC_VECTOR ( 127 downto 0 );
                        ch0_txheader_ext_0 => "000000", --in STD_LOGIC_VECTOR ( 5 downto 0 );
                        ch0_txpolarity_ext_0 => '0', --in STD_LOGIC;
                        ch0_txresetdone_ext_0(0) => txresetdone(quad*4+3), --out STD_LOGIC;
                        ch0_txsequence_ext_0 => "0000000", --in STD_LOGIC_VECTOR ( 6 downto 0 );

                        ch1_loopback_0 =>  "000",
                        ch1_rxcdrlock_ext_0 => open, --out STD_LOGIC;
                        ch1_rxctrl0_ext_0 => ch2_rxctrl0, --out STD_LOGIC_VECTOR ( 15 downto 0 );
                        ch1_rxctrl1_ext_0 => ch2_rxctrl1, --out STD_LOGIC_VECTOR ( 15 downto 0 );
                        ch1_rxctrl2_ext_0 => ch2_rxctrl2, --out STD_LOGIC_VECTOR ( 7 downto 0 );
                        ch1_rxctrl3_ext_0 => ch2_rxctrl3, --out STD_LOGIC_VECTOR ( 7 downto 0 );
                        ch1_rxdata_ext_0 => ch2_userdata_rx, --out STD_LOGIC_VECTOR ( 127 downto 0 );
                        ch1_rxdatavalid_ext_0 => open, --out STD_LOGIC_VECTOR ( 1 downto 0 );
                        ch1_rxgearboxslip_ext_0 => '0', --in STD_LOGIC;
                        ch1_rxheader_ext_0 => open, --out STD_LOGIC_VECTOR ( 5 downto 0 );
                        ch1_rxheadervalid_ext_0 => open, --out STD_LOGIC_VECTOR ( 1 downto 0 );
                        ch1_rxpolarity_ext_0 => '0' , --in STD_LOGIC;
                        ch1_rxresetdone_ext_0(0) => rxresetdone(quad*4+2), --out STD_LOGIC;
                        ch1_rxslide_ext_0 => '0', --in STD_LOGIC;
                        ch1_rxvalid_ext_0 => open, --out STD_LOGIC;
                        ch1_txctrl0_ext_0 => ch2_txctrl0 , --in STD_LOGIC_VECTOR ( 15 downto 0 );
                        ch1_txctrl1_ext_0 => ch2_txctrl1 , --in STD_LOGIC_VECTOR ( 15 downto 0 );
                        ch1_txctrl2_ext_0 => ch2_txctrl2, --in STD_LOGIC_VECTOR ( 7 downto 0 );
                        ch1_txdata_ext_0 => ch2_userdata_tx, --in STD_LOGIC_VECTOR ( 127 downto 0 );
                        --ch1_txheader_ext_0 => "000000", --in STD_LOGIC_VECTOR ( 5 downto 0 );
                        ch1_txpolarity_ext_0 => '0', --in STD_LOGIC;
                        ch1_txresetdone_ext_0(0) => txresetdone(quad*4+2), --out STD_LOGIC;
                        ch1_txsequence_ext_0 => "0000000", --in STD_LOGIC_VECTOR ( 6 downto 0 );

                        ch2_loopback_0 =>  "000",
                        ch2_rxcdrlock_ext_0 => open, --out STD_LOGIC;
                        ch2_rxctrl0_ext_0 => ch1_rxctrl0, --out STD_LOGIC_VECTOR ( 15 downto 0 );
                        ch2_rxctrl1_ext_0 => ch1_rxctrl1, --out STD_LOGIC_VECTOR ( 15 downto 0 );
                        ch2_rxctrl2_ext_0 => ch1_rxctrl2, --out STD_LOGIC_VECTOR ( 7 downto 0 );
                        ch2_rxctrl3_ext_0 => ch1_rxctrl3, --out STD_LOGIC_VECTOR ( 7 downto 0 );
                        ch2_rxdata_ext_0 => ch1_userdata_rx, --out STD_LOGIC_VECTOR ( 127 downto 0 );
                        ch2_rxdatavalid_ext_0 => open, --out STD_LOGIC_VECTOR ( 1 downto 0 );
                        ch2_rxgearboxslip_ext_0 => '0', --in STD_LOGIC;
                        ch2_rxheader_ext_0 => open, --out STD_LOGIC_VECTOR ( 5 downto 0 );
                        ch2_rxheadervalid_ext_0 => open, --out STD_LOGIC_VECTOR ( 1 downto 0 );
                        ch2_rxpolarity_ext_0 => '0' , --in STD_LOGIC;
                        ch2_rxresetdone_ext_0(0) => rxresetdone(quad*4+1), --out STD_LOGIC;
                        ch2_rxslide_ext_0 => '0', --in STD_LOGIC;
                        ch2_rxvalid_ext_0 => open, --out STD_LOGIC;
                        ch2_txctrl0_ext_0 => ch1_txctrl0 , --in STD_LOGIC_VECTOR ( 15 downto 0 );
                        ch2_txctrl1_ext_0 => ch1_txctrl1 , --in STD_LOGIC_VECTOR ( 15 downto 0 );
                        ch2_txctrl2_ext_0 => ch1_txctrl2, --in STD_LOGIC_VECTOR ( 7 downto 0 );
                        ch2_txdata_ext_0 => ch1_userdata_tx, --in STD_LOGIC_VECTOR ( 127 downto 0 );
                        --ch2_txheader_ext_0 => "000000", --in STD_LOGIC_VECTOR ( 5 downto 0 );
                        ch2_txpolarity_ext_0 => '0', --in STD_LOGIC;
                        ch2_txresetdone_ext_0(0) => txresetdone(quad*4+1), --out STD_LOGIC;
                        ch2_txsequence_ext_0 => "0000000", --in STD_LOGIC_VECTOR ( 6 downto 0 );

                        ch3_loopback_0 =>  "000",
                        ch3_rxcdrlock_ext_0 => open, --out STD_LOGIC;
                        ch3_rxctrl0_ext_0 => ch0_rxctrl0, --out STD_LOGIC_VECTOR ( 15 downto 0 );
                        ch3_rxctrl1_ext_0 => ch0_rxctrl1, --out STD_LOGIC_VECTOR ( 15 downto 0 );
                        ch3_rxctrl2_ext_0 => ch0_rxctrl2, --out STD_LOGIC_VECTOR ( 7 downto 0 );
                        ch3_rxctrl3_ext_0 => ch0_rxctrl3, --out STD_LOGIC_VECTOR ( 7 downto 0 );
                        ch3_rxdata_ext_0 => ch0_userdata_rx, --out STD_LOGIC_VECTOR ( 127 downto 0 );
                        ch3_rxdatavalid_ext_0 => open, --out STD_LOGIC_VECTOR ( 1 downto 0 );
                        ch3_rxgearboxslip_ext_0 => '0', --in STD_LOGIC;
                        ch3_rxheader_ext_0 => open, --out STD_LOGIC_VECTOR ( 5 downto 0 );
                        ch3_rxheadervalid_ext_0 => open, --out STD_LOGIC_VECTOR ( 1 downto 0 );
                        ch3_rxpolarity_ext_0 => '0' , --in STD_LOGIC;
                        ch3_rxresetdone_ext_0(0) => rxresetdone(quad*4+0), --out STD_LOGIC;
                        ch3_rxslide_ext_0 => '0', --in STD_LOGIC;
                        ch3_rxvalid_ext_0 => open, --out STD_LOGIC;
                        ch3_txctrl0_ext_0 => ch0_txctrl0 , --in STD_LOGIC_VECTOR ( 15 downto 0 );
                        ch3_txctrl1_ext_0 => ch0_txctrl1 , --in STD_LOGIC_VECTOR ( 15 downto 0 );
                        ch3_txctrl2_ext_0 => ch0_txctrl2, --in STD_LOGIC_VECTOR ( 7 downto 0 );
                        ch3_txdata_ext_0 => ch0_userdata_tx, --in STD_LOGIC_VECTOR ( 127 downto 0 );
                        --ch3_txheader_ext_0 => "000000", --in STD_LOGIC_VECTOR ( 5 downto 0 );
                        ch3_txpolarity_ext_0 => '0', --in STD_LOGIC;
                        ch3_txresetdone_ext_0(0) => txresetdone(quad*4+0), --out STD_LOGIC;
                        ch3_txsequence_ext_0 => "0000000", --in STD_LOGIC_VECTOR ( 6 downto 0 );

                        gt_reset_gt_bridge_ip_0 => soft_reset_sync, --in STD_LOGIC;
                        reset_rx_datapath_in_0 => SOFT_RXRST_GT_g, --in STD_LOGIC;
                        reset_rx_pll_and_datapath_in_0 => qpll_reset(quad), --in STD_LOGIC;
                        reset_tx_datapath_in_0 => SOFT_TXRST_GT_g, --in STD_LOGIC;
                        reset_tx_pll_and_datapath_in_0 => qpll_reset(quad), --in STD_LOGIC;
                        rx_resetdone_out_gt_bridge_ip_0 => open, --out STD_LOGIC;
                        tx_resetdone_out_gt_bridge_ip_0 => open, --out STD_LOGIC;
                        rxusrclk0_out => GT_RXUSRCLK(quad*4+3),
                        rxusrclk1_out => GT_RXUSRCLK(quad*4+2),
                        rxusrclk2_out => GT_RXUSRCLK(quad*4+1),
                        rxusrclk3_out => GT_RXUSRCLK(quad*4+0),
                        txusrclk0_out => GT_TXUSRCLK(quad*4+3),
                        txusrclk1_out => GT_TXUSRCLK(quad*4+2),
                        txusrclk2_out => GT_TXUSRCLK(quad*4+1),
                        txusrclk3_out => GT_TXUSRCLK(quad*4+0),
                        QUAD0_hsclk0_lcplllock_0 => lcpll_lock_01(1),
                        QUAD0_hsclk1_lcplllock_0 => lcpll_lock_01(0),
                        INTF0_RX0_ch_rxrate_0 => rxrate_ch(quad*4+3),
                        INTF0_RX1_ch_rxrate_0 => rxrate_ch(quad*4+2),
                        INTF0_RX2_ch_rxrate_0 => rxrate_ch(quad*4+1),
                        INTF0_RX3_ch_rxrate_0 => rxrate_ch(quad*4+0),
                        INTF0_TX0_ch_txrate_0 => txrate_ch(quad*4+3),
                        INTF0_TX1_ch_txrate_0 => txrate_ch(quad*4+2),
                        INTF0_TX2_ch_txrate_0 => txrate_ch(quad*4+1),
                        INTF0_TX3_ch_txrate_0 => txrate_ch(quad*4+0)
                    );
            end generate g_fullGBT;
        end generate;
    end generate;


    RX_DATA_33b <= RX_DATA_33b_s;
    RX_DATA_33b_rdy <= GBT_ALIGNMENT_DONE(GBT_NUM-1 downto 0);
    RXUSERCLK_OUT <= GT_RXUSRCLK;
end Behavioral;

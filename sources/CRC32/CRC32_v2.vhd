--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

---------------------------------------------------------------------------
--
-- Designed by : Peter Jansweijer.
-- Updated by  : Frans Schreuder
-- Company     : NIKHEF.
-- Design info : .
---------------------------------------------------------------------------

---------------------------------------------------------------------------
-- Entity declaration of 'crc32'.
---------------------------------------------------------------------------

library ieee ;
    use ieee.std_logic_1164.ALL ;
    use ieee.numeric_std.all ;

entity crc32 is
    port(
        Clk     : in     std_logic ;
        Clr_CRC : in     std_logic ;
        Din     : in     std_logic_Vector(31 downto 0) ;
        CRC     : out    std_logic_Vector(31 downto 0) ;
        Calc    : in     std_logic ) ;
end  crc32 ;

---------------------------------------------------------------------------
-- Architecture 'a0' of 'crc32'
---------------------------------------------------------------------------

architecture rtl of crc32 is
-- CRC-32 is defined as:
-- Width   32
-- Poly    04C11DB7 = x^32+x^26+x^23+x^22+x^16+x^12+x^11+x^10+x^8+x^7+x^5+x^4+x^2+x^1+1
-- Init    0xFFFFFFFF
-- RefIn  True (Means D0 is shifted in first, D31 last)
-- RefOut  True (Means CRC0 x^31 and CRC31 = x^0)
-- XorOut  0xFFFFFFFF (Output is ones complemented).
-- Check  Din    CRC
--  1st  0x34333231  0x9BE3E0A3
--  2nd  0x38373635  0x9AE0DAAF
--  3rd  0x00000039  0x77D55834

-- if the Calculated CRC value is fed ones complemented into the data input then
-- the result should be 0xFFFFFFFF
--  4th  0x882AA7CB  0xFFFFFFFF (note 0x882AA7CB is 0x77D55834 Xor 0xFFFFFFFF)
-- Update: Clr_CRC can be high while Calc is '1', meaning stream can continue.

begin
    process (Clk)
        variable M: std_Logic_Vector (31 Downto 0);
        variable Reg: std_Logic_Vector (31 Downto 0);
    begin
        if Rising_Edge(Clk) then
            if Clr_CRC = '1' then
                -- Initial CRC value is 0xFFFFFFFF
                Reg := (Others => '1');
            end if;
            if Calc = '1' then

                -- Note that D0 is shifted in first, D31 is shifted in last
                -- (REFIN = True)
                For k In 0 To 31 Loop
                    M(k) := Din(k) XOR Reg(k);
                end Loop;

                -- This is the result of 32 shifts in a CRC32
                -- LFSR that uses the polynomal
                -- x^32+x^26+x^23+x^22+x^16+x^12+x^11+x^10+x^8+x^7+x^5+x^4+x^2+x^1+1 = 0x1.04C11DB7

                Reg(0)  := M(0) Xor M(1) Xor M(2) Xor M(4) Xor M(7) Xor M(8) Xor M(16) Xor M(22) Xor M(23) Xor M(26) Xor M(6) Xor M(20) Xor M(3);
                Reg(1)  := M(1) Xor M(2) Xor M(3) Xor M(5) Xor M(8) Xor M(9) Xor M(17) Xor M(23) Xor M(24) Xor M(27) Xor M(7) Xor M(21) Xor M(4);
                Reg(2)  := M(2) Xor M(3) Xor M(4) Xor M(6) Xor M(9) Xor M(10) Xor M(18) Xor M(24) Xor M(25) Xor M(28) Xor M(0) Xor M(8) Xor M(22) Xor M(5);
                Reg(3)  := M(3) Xor M(4) Xor M(5) Xor M(7) Xor M(10) Xor M(11) Xor M(19) Xor M(25) Xor M(26) Xor M(29) Xor M(1) Xor M(9) Xor M(23) Xor M(6);
                Reg(4)  := M(4) Xor M(5) Xor M(6) Xor M(8) Xor M(11) Xor M(12) Xor M(20) Xor M(26) Xor M(27) Xor M(30) Xor M(2) Xor M(10) Xor M(24) Xor M(7);
                Reg(5)  := M(5) Xor M(6) Xor M(7) Xor M(9) Xor M(12) Xor M(13) Xor M(21) Xor M(27) Xor M(28) Xor M(31) Xor M(0) Xor M(3) Xor M(11) Xor M(25) Xor M(8);
                Reg(6)  := M(10) Xor M(13) Xor M(14) Xor M(16) Xor M(28) Xor M(29) Xor M(0) Xor M(2) Xor M(12) Xor M(23) Xor M(9) Xor M(20) Xor M(3);
                Reg(7)  := M(11) Xor M(14) Xor M(15) Xor M(17) Xor M(29) Xor M(30) Xor M(1) Xor M(3) Xor M(13) Xor M(24) Xor M(10) Xor M(21) Xor M(4);
                Reg(8)  := M(12) Xor M(15) Xor M(16) Xor M(18) Xor M(30) Xor M(31) Xor M(2) Xor M(4) Xor M(14) Xor M(25) Xor M(0) Xor M(11) Xor M(22) Xor M(5);
                Reg(9)  := M(13) Xor M(17) Xor M(19) Xor M(20) Xor M(31) Xor M(4) Xor M(5) Xor M(7) Xor M(8) Xor M(15) Xor M(0) Xor M(2) Xor M(12) Xor M(22);
                Reg(10) := M(14) Xor M(18) Xor M(21) Xor M(22) Xor M(26) Xor M(4) Xor M(5) Xor M(9) Xor M(2) Xor M(13) Xor M(0) Xor M(7);
                Reg(11) := M(15) Xor M(19) Xor M(22) Xor M(23) Xor M(27) Xor M(5) Xor M(6) Xor M(10) Xor M(3) Xor M(14) Xor M(1) Xor M(8);
                Reg(12) := M(16) Xor M(20) Xor M(23) Xor M(24) Xor M(28) Xor M(6) Xor M(7) Xor M(11) Xor M(4) Xor M(15) Xor M(2) Xor M(9);
                Reg(13) := M(17) Xor M(21) Xor M(24) Xor M(25) Xor M(29) Xor M(7) Xor M(8) Xor M(12) Xor M(5) Xor M(16) Xor M(3) Xor M(10) Xor M(0);
                Reg(14) := M(18) Xor M(22) Xor M(25) Xor M(26) Xor M(30) Xor M(8) Xor M(9) Xor M(6) Xor M(17) Xor M(4) Xor M(11) Xor M(13) Xor M(0) Xor M(1);
                Reg(15) := M(19) Xor M(23) Xor M(26) Xor M(27) Xor M(31) Xor M(9) Xor M(10) Xor M(7) Xor M(18) Xor M(5) Xor M(12) Xor M(14) Xor M(1) Xor M(2);
                Reg(16) := M(16) Xor M(23) Xor M(24) Xor M(26) Xor M(27) Xor M(28) Xor M(10) Xor M(11) Xor M(22) Xor M(7) Xor M(19) Xor M(13) Xor M(4) Xor M(15) Xor M(1);
                Reg(17) := M(17) Xor M(24) Xor M(25) Xor M(27) Xor M(28) Xor M(29) Xor M(11) Xor M(12) Xor M(23) Xor M(8) Xor M(20) Xor M(14) Xor M(5) Xor M(16) Xor M(2);
                Reg(18) := M(18) Xor M(25) Xor M(26) Xor M(28) Xor M(29) Xor M(30) Xor M(12) Xor M(13) Xor M(24) Xor M(9) Xor M(21) Xor M(15) Xor M(6) Xor M(17) Xor M(3) Xor M(0);
                Reg(19) := M(19) Xor M(26) Xor M(27) Xor M(29) Xor M(30) Xor M(31) Xor M(13) Xor M(14) Xor M(25) Xor M(10) Xor M(22) Xor M(16) Xor M(7) Xor M(18) Xor M(4) Xor M(0) Xor M(1);
                Reg(20) := M(22) Xor M(27) Xor M(28) Xor M(30) Xor M(31) Xor M(14) Xor M(15) Xor M(11) Xor M(17) Xor M(16) Xor M(19) Xor M(4) Xor M(5) Xor M(6) Xor M(0) Xor M(3) Xor M(7);
                Reg(21) := M(22) Xor M(26) Xor M(28) Xor M(29) Xor M(31) Xor M(15) Xor M(17) Xor M(12) Xor M(18) Xor M(5) Xor M(2) Xor M(0) Xor M(3);
                Reg(22) := M(22) Xor M(26) Xor M(27) Xor M(29) Xor M(30) Xor M(20) Xor M(13) Xor M(19) Xor M(18) Xor M(7) Xor M(8) Xor M(2);
                Reg(23) := M(23) Xor M(27) Xor M(28) Xor M(30) Xor M(31) Xor M(21) Xor M(14) Xor M(20) Xor M(19) Xor M(8) Xor M(9) Xor M(0) Xor M(3);
                Reg(24) := M(24) Xor M(26) Xor M(28) Xor M(29) Xor M(31) Xor M(23) Xor M(15) Xor M(16) Xor M(21) Xor M(8) Xor M(9) Xor M(10) Xor M(2) Xor M(7) Xor M(3) Xor M(6);
                Reg(25) := M(25) Xor M(26) Xor M(27) Xor M(29) Xor M(30) Xor M(20) Xor M(23) Xor M(24) Xor M(17) Xor M(9) Xor M(10) Xor M(11) Xor M(1) Xor M(2) Xor M(6);
                Reg(26) := M(26) Xor M(27) Xor M(28) Xor M(30) Xor M(31) Xor M(21) Xor M(24) Xor M(25) Xor M(18) Xor M(10) Xor M(11) Xor M(12) Xor M(2) Xor M(3) Xor M(7);
                Reg(27) := M(27) Xor M(28) Xor M(29) Xor M(31) Xor M(23) Xor M(25) Xor M(19) Xor M(20) Xor M(16) Xor M(11) Xor M(12) Xor M(13) Xor M(7) Xor M(2) Xor M(6) Xor M(1) Xor M(0);
                Reg(28) := M(28) Xor M(29) Xor M(30) Xor M(22) Xor M(23) Xor M(24) Xor M(21) Xor M(16) Xor M(17) Xor M(12) Xor M(13) Xor M(14) Xor M(4) Xor M(6) Xor M(0);
                Reg(29) := M(29) Xor M(30) Xor M(31) Xor M(23) Xor M(24) Xor M(25) Xor M(22) Xor M(17) Xor M(18) Xor M(13) Xor M(14) Xor M(15) Xor M(7) Xor M(5) Xor M(1) Xor M(0);
                Reg(30) := M(30) Xor M(31) Xor M(24) Xor M(25) Xor M(22) Xor M(20) Xor M(18) Xor M(19) Xor M(14) Xor M(15) Xor M(7) Xor M(4) Xor M(3);
                Reg(31) := M(31) Xor M(25) Xor M(22) Xor M(21) Xor M(19) Xor M(15) Xor M(7) Xor M(6) Xor M(5) Xor M(3) Xor M(2) Xor M(1) Xor M(0);

            end if;
        end if;

        -- Output should be Xored with 0xFFFFFFFF ( is ones compleneted)
        -- (REFOut = True)
        CRC <= Not Reg;

    end Process;
end  rtl; -- of crc32

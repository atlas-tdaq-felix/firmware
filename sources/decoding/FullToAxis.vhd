--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    --use ieee.std_logic_unsigned.all;
    use work.pcie_package.all;
    use work.axi_stream_package.all;
    use work.FELIX_package.all;
library XPM;
    use xpm.vcomponents.all;

entity FullToAxis is
    generic(
        BLOCKSIZE : integer;
        VERSAL : boolean
    );
    port (
        clk240 : in std_logic;
        FMdin : in std_logic_vector(32 downto 0);
        FMdin_is_big_endian : in std_logic;
        aclk : in std_logic;
        daq_reset : in std_logic;
        daq_fifo_flush : in std_logic;
        m_axis : out axis_32_type;
        m_axis_tready : in std_logic;
        m_axis_prog_empty : out std_logic;
        FE_BUSY_out : out std_logic;
        path_ena : in std_logic;
        super_chunk_factor_link : in std_logic_vector(7 downto 0);
        Use32bSOP: in std_logic --Register setting: 1 to check for 0x0000003C for Start of Chunk, when '0' only check 0x3C.
    );
end FullToAxis;

architecture rtl of FullToAxis is

    signal s_axis, s_axis_p1, s_axis_p2, s_axis_p3, s_axis_p4, s_axis_fifo: axis_32_type;
    signal s_axis_tready : std_logic;
    signal s_axis_tuser  : std_logic_vector(3 downto 0);

    signal busy_240: std_logic;
    signal chunk_error_240: std_logic;


    signal tlast: std_logic;
    signal fifo_resetn: std_logic;
    signal trunc, trunc_handled: std_logic;

    signal PathEnable_240: std_logic; --CDC to clk240 domain using xpm_cdc_single
    signal EnableTlast : std_logic;
    signal daq_reset_240: std_logic;
    signal daq_fifo_flush_240: std_logic;
    signal invalidate_data : std_logic; --Add an extra tlast/tvalid/chunk_error to the fifo.
    signal chunk_busy: std_logic; --1 when we are in between SOP/EOP. Ignore any other data.
    signal Use32bSOP_clk240: std_logic; --Register setting: 1 to check for 0x0000003C for Start of Chunk, when '0' only check 0x3C.
    signal chunk_contains_data: std_logic; --Indication that data has been written into the FIFO but has not been ended with tlast (may be garbage from a link reset)
    signal super_chunk_factor_link_240: std_logic_vector(7 downto 0);
    signal FMdin_is_big_endian_240 : std_logic;
    signal crc_din : std_logic_vector(31 downto 0);

begin

    xpm_cdc_sync_aresetn_240 : xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 2,
            INIT => 0,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0
        )
        port map (
            src_rst => daq_reset, -- 1-bit input: Source reset signal.
            dest_clk => clk240, -- 1-bit input: Destination clock.
            dest_rst => daq_reset_240 -- 1-bit output: src_rst synchronized to the destination clock domain. This output
        );

    xpm_cdc_sync_daq_fifo_flush_240 : xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 2,
            INIT => 0,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0
        )
        port map (
            src_rst => daq_fifo_flush, -- 1-bit input: Source reset signal.
            dest_clk => clk240, -- 1-bit input: Destination clock.
            dest_rst => daq_fifo_flush_240 -- 1-bit output: src_rst synchronized to the destination clock domain. This output
        );

    xpm_cdc_PathEnable : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map (
            src_clk => '0',
            src_in => path_ena,
            dest_clk => clk240,
            dest_out => PathEnable_240
        );


    xpm_cdc_32bSOP : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map (
            src_clk => '0',
            src_in => Use32bSOP,
            dest_clk => clk240,
            dest_out => Use32bSOP_clk240
        );

    xpm_cdc_superchunk_factor : xpm_cdc_array_single
        generic map (
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0,
            WIDTH => 8
        )
        port map (
            src_clk => '0',
            src_in => super_chunk_factor_link,
            dest_clk => clk240,
            dest_out => super_chunk_factor_link_240
        );

    sync_endianness: xpm_cdc_single
        generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => '0',
            src_in => FMdin_is_big_endian,
            dest_clk => clk240,
            dest_out => FMdin_is_big_endian_240
        );

    superchunk_proc: process(clk240, daq_reset_240)
        variable superchunk_cnt: unsigned(super_chunk_factor_link_240'range);
    begin
        if daq_reset_240 = '1' then
            EnableTlast <= '1';
            superchunk_cnt := (others => '0');
        elsif rising_edge(clk240) then
            if superchunk_cnt = unsigned(super_chunk_factor_link_240)-1 then
                EnableTlast <= '1';
            else
                EnableTlast <= '0';
            end if;
            if PathEnable_240 = '1' and FMdin(32) = '1' and FMdin(7 downto 0) = Kchar_eop and chunk_busy = '1' then
                if superchunk_cnt < unsigned(super_chunk_factor_link_240)-1 then
                    superchunk_cnt := superchunk_cnt + 1;
                else
                    superchunk_cnt := (others => '0');
                end if;
            end if;

        end if;
    end process;

    toAxis_proc: process(PathEnable_240, FMdin, EnableTlast, chunk_busy, FMdin_is_big_endian_240)
    begin
        tlast <= '0';
        s_axis.tlast <= '0'; --added later in pipelining.
        if PathEnable_240 = '1' then
            if FMdin(32) = '1' then
                s_axis.tvalid <= '0'; --other comma characters are ignored.
                if FMdin(7 downto 0) = Kchar_eop then
                    tlast <= EnableTlast;
                end if;
            else
                s_axis.tvalid <= chunk_busy;
            end if;
        else
            s_axis.tvalid <= '0';
        end if;

        if FMdin_is_big_endian_240 = '1' then
            s_axis.tdata <= FMdin(7 downto 0) & FMdin(15 downto 8) & FMdin(23 downto 16) & FMdin(31 downto 24);
        else
            s_axis.tdata <= FMdin(31 downto 0);
        end if;
    end process;

    crc_din <= FMdin(31 downto 0);


    busyDetect: process(clk240)
    begin
        if rising_edge(clk240) then
            if daq_reset_240 = '1' then
                busy_240 <= '0';
            else
                if PathEnable_240 = '1' then
                    if FMdin(32) = '1' then
                        if FMdin(7 downto 0) = Kchar_sob then
                            busy_240 <= '1';
                        end if;
                        if FMdin(7 downto 0) = Kchar_eob then
                            busy_240 <= '0';
                        end if;
                        if FMdin(7 downto 0) = Kchar_eop then
                            busy_240 <= FMdin(28); --Contains the busy state of the elink
                        end if;
                    end if;
                else
                    busy_240 <= '0';
                end if;
            end if;
        end if;
    end process;

    s_axis_tuser(2) <= busy_240;
    FE_BUSY_out <= busy_240;


    chunk_check: process(clk240, daq_reset_240)
        variable sop_error, eop_error: std_logic;
    begin
        if daq_reset_240 = '1' then
            chunk_error_240 <= '0';
            invalidate_data <= '0';
            chunk_contains_data <= '0';
            chunk_busy <= '0';
        elsif rising_edge(clk240) then
            invalidate_data <= '0';
            if s_axis_fifo.tvalid = '1' then
                if s_axis_fifo.tlast = '0' then
                    chunk_contains_data <= '1';
                else
                    chunk_contains_data <= '0';
                end if;
            end if;
            if PathEnable_240 = '0' then
                invalidate_data <= chunk_contains_data;
                sop_error := '0';
                eop_error := '0';
                chunk_busy <= '0';
                chunk_error_240 <= '0';
            else
                if FMdin(32) = '1' then
                    if FMdin(7 downto 0) = Kchar_sop and ((FMdin(31 downto 8) = x"000000") or (FMdin(31 downto 8) = x"DEADBE") or Use32bSOP_clk240 = '0') then
                        if chunk_busy = '1' then
                            sop_error := '1';
                            invalidate_data <= chunk_contains_data;
                        end if;
                        chunk_busy <= '1';
                    end if;
                    if FMdin(7 downto 0) = Kchar_eop then
                        if chunk_busy = '0' then
                            eop_error := '1';
                        end if;
                        chunk_busy <= '0';
                    end if;
                end if;
            end if;
            if sop_error = '1' or eop_error = '1' then
                chunk_error_240 <= '1';
                sop_error := '0';
                eop_error := '0';
            end if;
            if s_axis_fifo.tlast = '1' then
                chunk_error_240 <= '0';
            end if;
        end if;
    end process;

    s_axis_tuser(1) <= chunk_error_240 or invalidate_data;

    crc_block: block
        signal crc_out: std_logic_vector(19 downto 0);
        signal crc_calc: std_logic;
        signal crc_start: std_logic;
        signal crc_error_240: std_logic;
    begin
        -- CRC module takes 2 clock cycles to calculate CRC
        crc20_0: entity work.CRC
            --generic map(
            --  Nbits     => 32,
            --  CRC_Width => 20,
            --  G_Poly    => x"8359f",
            --  G_InitVal => x"fffff"
            --  )
            port map(
                CRC   => crc_out,  --in sync with din_r
                Calc  => crc_calc,
                Clk   => clk240,
                Din   => crc_din,
                Reset => crc_start);

        crc_calc <= not FMdin(32);

        s_axis_tuser(0) <= crc_error_240;

        crc_proc: process(clk240)
            variable crc_check_p1, crc_check_p2: std_logic;
            variable crc_check_val: std_logic_vector(19 downto 0);
            variable chunk_busy_p: std_logic_vector(2 downto 0); --pipelined version of chunk_busy
        begin
            if rising_edge(clk240) then
                if daq_reset_240 = '1' then
                    crc_error_240 <= '0';
                    crc_check_p1 := '0';
                    crc_check_p2 := '0';
                    chunk_busy_p := "000";
                    crc_check_val := (others => '0');
                    crc_start <= '0';
                else
                    if (FMdin(32) = '1' and FMdin(7 downto 0) = Kchar_sop) and (FMdin(31 downto 8) = x"000000" or Use32bSOP_clk240 = '0') then
                        crc_start <= '1';
                    else
                        crc_start <= '0';
                    end if;

                    if (s_axis_fifo.tlast = '1' and s_axis_fifo.tvalid = '1' and s_axis_tready = '1') or (chunk_busy_p(2) = '0' and chunk_busy = '0') then
                        crc_error_240 <= '0'; --Clear on tlast or 3 cycles after chunk_busy goes low.
                    end if;

                    if crc_check_p2 = '1' then
                        if crc_check_val /= crc_out then
                            crc_error_240 <= '1';
                        end if;
                    end if;
                    crc_check_p2 := crc_check_p1;

                    if(FMdin(32) = '1' and FMdin(7 downto 0) = Kchar_eop) then
                        crc_check_p1 := '1';
                        crc_check_val := FMdin(27 downto 8);
                    else
                        crc_check_p1 := '0';
                    end if;
                    chunk_busy_p := chunk_busy_p(1 downto 0) & chunk_busy;
                end if;
            end if;
        end process;

    end block;

    trunc_proc: process(clk240, daq_reset_240)
        variable s_axis_tvalid_p4 : std_logic; --Delay tvalid 4 pipelines to be in line with s_axis_p4, but not invalidated by tready.
        variable s_axis_tvalid_p3 : std_logic;
        variable s_axis_tvalid_p2 : std_logic;
        variable s_axis_tvalid_p1 : std_logic;
    begin
        if daq_reset_240 = '1' then
            trunc <= '0';
            trunc_handled <= '0';
            s_axis_tvalid_p1:= '0';
            s_axis_tvalid_p2:= '0';
            s_axis_tvalid_p3:= '0';
            s_axis_tvalid_p4:= '0';
        elsif rising_edge(clk240) then
            if trunc = '1' and s_axis_tready = '1' then
                trunc_handled <= '1';
            end if;
            if s_axis_p4.tlast = '1' and s_axis_p4.tvalid = '1' and s_axis_tready = '1' then
                trunc <= '0'; --Clear on tlast
                trunc_handled <= '0';
            end if;
            if s_axis_tvalid_p4 = '1' and s_axis_tready = '0' then --We lost data, set truncation
                trunc <= '1';
                trunc_handled <= '0';
            end if;
            s_axis_tvalid_p4 := s_axis_tvalid_p3;
            s_axis_tvalid_p3 := s_axis_tvalid_p2;
            s_axis_tvalid_p2 := s_axis_tvalid_p1;
            s_axis_tvalid_p1 := s_axis.tvalid;

        end if;
    end process;

    s_axis_tuser(3) <= trunc or (s_axis_fifo.tvalid and not s_axis_tready) or invalidate_data;


    --Pipeline the slave axi bus to align with the CRC module which takes 2 pipelines.
    axi_pipe_proc: process(clk240)
    begin
        if rising_edge(clk240) then
            if daq_reset_240 = '1' then
                --notTruncated <= '1';
                s_axis_p1.tvalid <= '0';
                s_axis_p2.tvalid <= '0';
                s_axis_p3.tvalid <= '0';
                s_axis_p4.tvalid <= '0';
            else --if s_axis_tready = '1' then --Stall pipeline if tready is low.
                --if trunc = '1' then
                --    notTruncated <= '0';
                --elsif s_axis_p4.tlast = '1' then
                --    notTruncated <= '1';
                --end if;
                s_axis_p1 <= s_axis;
                s_axis_p2 <= s_axis_p1;
                s_axis_p2.tlast  <= tlast;
                s_axis_p3 <= s_axis_p2;
                s_axis_p4 <= s_axis_p3;
            end if;
        end if;
    end process;

    fifo_resetn <= not daq_fifo_flush_240; -- and PathEnable_240;

    s_axis_fifo.tvalid <= ((s_axis_p4.tvalid ) or invalidate_data or trunc) and (not trunc_handled) and PathEnable_240;
    s_axis_fifo.tdata <= s_axis_p4.tdata;
    s_axis_fifo.tuser <= s_axis_tuser;
    s_axis_fifo.tlast <= s_axis_p4.tlast or trunc or (s_axis_p4.tvalid and not s_axis_tready) or invalidate_data;
    s_axis_fifo.tkeep <= (others => '1');

    fifo0: entity work.Axis32Fifo
        generic map(
            DEPTH => 2048, --8kB AXIs FIFO
            --CLOCKING_MODE => "independent_clock",
            --RELATED_CLOCKS => 0,
            --FIFO_MEMORY_TYPE => "auto",
            --PACKET_FIFO => "false",
            USE_BUILT_IN_FIFO => '0',
            VERSAL => VERSAL,
            BLOCKSIZE => BLOCKSIZE,
            ISPIXEL => false
        )
        port map (
            -- axi stream slave
            s_axis_aresetn => fifo_resetn,
            s_axis_aclk => clk240,
            s_axis => s_axis_fifo,
            s_axis_tready => s_axis_tready,

            -- axi stream master
            m_axis_aclk => aclk,
            m_axis => m_axis,
            m_axis_tready => m_axis_tready,
            m_axis_prog_empty => m_axis_prog_empty
        );

end architecture;

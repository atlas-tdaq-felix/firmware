--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 10/30/2019 03:36:26 PM
-- Design Name:
-- Module Name: EndeavourDecoder - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;
library XPM;
    use XPM.VCOMPONENTS.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
--check that all signals are retimed @m_axis_aclk=clk. Remove one clock
entity ByteToAxiStream32b is
    generic(
        BLOCKSIZE           : integer := 1024;
        USE_BUILT_IN_FIFO   : std_logic := '0';
        VERSAL              : boolean;
        EGROUP              : integer := 0;
        STREAM              : integer := 1 --STREAM=1 is default => FIFO DEPTH => BLOCKSIZE/2,
    );
    port (
        clk : in std_logic;                       --BC clock for DataIn
        daq_reset : in std_logic;                    --Acitve high reset
        daq_fifo_flush : in std_logic;
        EnableIn : in std_logic;                    --Enable epath (register map
        --retimed @clk)
        DataIn : in std_logic_vector(31 downto 0);  --32b Data from Protocol Decoder
        DataInValid : in std_logic;                 --Data valid from Protocol Decoder
        EOP : in std_logic;                         --will be converted to tlast
        BytesKeep : in std_logic_vector(3 downto 0); --Bytes to keep
        ElinkBusy : in std_logic;                   --Add to tuser -- @suppress "Unused port: ElinkBusy is not used in work.ByteToAxiStream32b(Behavioral)"
        TruncateIn : in std_logic; -- @suppress "Unused port: TruncateIn is not used in work.ByteToAxiStream32b(Behavioral)"
        m_axis : out axis_32_type;                  --FIFO read port (axi stream)
        m_axis_tready : in std_logic;               --FIFO read tready (axi stream)
        m_axis_aclk : in std_logic;                 --FIFO read clock (axi stream)
        m_axis_prog_empty : out std_logic           --Indication that the FIFO contains a block.

    );
end ByteToAxiStream32b;

architecture Behavioral of ByteToAxiStream32b is

    signal s_axis_aresetn: std_logic;
    signal s_axis: axis_32_type;
    signal s_axis_tready: std_logic;


    --signal EnableIn_aclk: std_logic;
    signal m_axis_s: axis_32_type;
    signal m_axis_prog_empty_s: std_logic;

    signal EnableIn_aclk : std_logic;


    --link  eg  cb0  cb2  cb3  cb4
    --  0    0   y    y    y    y
    --  0    1   y    y    y    y
    --  0    2   y    y    n    n
    --  0    3   y    n    n    n
    --  0    4   y    n    n    n
    --  0    5   y    n    n    n
    --  1    0   y    y    y    y
    --  1    1   y    y    y    n
    --  1    2   y    y    n    n
    --  1    3   y    n    n    n
    --  1    4   y    n    n    n
    --  1    5   y    n    n    n

    function fifo_depth (egroup:integer; stream:integer)
        return integer is
    begin
        if stream = 0 then
            if egroup < 3 then
                return 0;
            else
                return 1;
            end if;
        else
            return 1;
        end if;

    end function;

    constant halfdepth : integer := fifo_depth(EGROUP,STREAM); --LINK is not used currently, can be removed eventually

begin

    toaxisfourbytes_proc: process(daq_reset, clk)
    begin
        if daq_reset = '1' then
            s_axis.tdata <= (others => '0');
            s_axis.tvalid <= '0';
            s_axis.tlast <= '0';
            s_axis.tkeep <= "0000";
            s_axis.tuser <= "0000";
        elsif rising_edge(clk) then
            if s_axis_tready = '1' then
                s_axis.tdata  <= DataIn;
                s_axis.tvalid <= DataInValid;
                s_axis.tlast  <= EOP;
                s_axis.tkeep  <= BytesKeep;
                s_axis.tuser  <= "0000";
            end if;
        end if;
    end process;



    s_axis_aresetn <= not daq_fifo_flush;

    gen_fifo_full_depth_i: if halfdepth = 0 generate
        fifo0: entity work.Axis32Fifo
            generic map(
                DEPTH            => BLOCKSIZE, --RL changed from BLOCKSIZE/2 to BLOCKSIZE for data stream-- Divide by 2 because this is 32b. Will contain 2 blocks.
                --CLOCKING_MODE    => "independent_clock",
                --RELATED_CLOCKS   => 0,
                --FIFO_MEMORY_TYPE => "auto",
                --PACKET_FIFO      => "false",
                USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
                VERSAL           => VERSAL,
                BLOCKSIZE        => BLOCKSIZE,
                ISPIXEL          => true
            )
            port map(
                -- axi stream slave
                s_axis_aresetn => s_axis_aresetn,
                s_axis_aclk => clk,
                s_axis => s_axis,
                s_axis_tready => s_axis_tready,

                -- axi stream master
                m_axis_aclk => m_axis_aclk,
                m_axis => m_axis_s,
                m_axis_tready => m_axis_tready,
                m_axis_prog_empty => m_axis_prog_empty_s
            );
    end generate;

    gen_fifo_half_depth_i: if halfdepth = 1 generate
        fifo0: entity work.Axis32Fifo
            generic map(
                DEPTH            => BLOCKSIZE/2,-- Divide by 2 because this is 32b. Will contain 2 blocks.
                --CLOCKING_MODE    => "independent_clock",
                --RELATED_CLOCKS   => 0,
                --FIFO_MEMORY_TYPE => "auto",
                --PACKET_FIFO      => "false",
                USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
                VERSAL           => VERSAL,
                BLOCKSIZE        => BLOCKSIZE,
                ISPIXEL          => true
            )
            port map(
                -- axi stream slave
                s_axis_aresetn => s_axis_aresetn,
                s_axis_aclk => clk,
                s_axis => s_axis,
                s_axis_tready => s_axis_tready,

                -- axi stream master
                m_axis_aclk => m_axis_aclk,
                m_axis => m_axis_s,
                m_axis_tready => m_axis_tready,
                m_axis_prog_empty => m_axis_prog_empty_s
            );
    end generate;

    EnableIn_40to240: process(m_axis_aclk)
    begin
        if rising_edge(m_axis_aclk) then
            EnableIn_aclk <= EnableIn;
        end if;
    end process;

    m_axis.tdata <= m_axis_s.tdata;
    m_axis.tvalid <= m_axis_s.tvalid and EnableIn_aclk;
    m_axis.tlast <= m_axis_s.tlast;
    m_axis.tkeep <= m_axis_s.tkeep;
    m_axis.tuser <= m_axis_s.tuser;
    m_axis_prog_empty <= m_axis_prog_empty_s or not EnableIn_aclk;

end Behavioral;

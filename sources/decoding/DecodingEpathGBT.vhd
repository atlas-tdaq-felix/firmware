--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!               Frans Schreuder
--!               Ton Fleuren
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 10/30/2019 03:36:26 PM
-- Design Name:
-- Module Name: EndeavourDecoder - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DecodingEpathGBT is
    generic (
        MAX_INPUT      : integer := 8;
        INCLUDE_16b    : std_logic := '0'; --Use data from 2 8-bit E-links, with alternating bits.
        INCLUDE_8b     : std_logic := '1';
        INCLUDE_4b     : std_logic := '1';
        INCLUDE_2b     : std_logic := '1';
        INCLUDE_8b10b  : std_logic := '1';
        INCLUDE_HDLC   : std_logic := '1';
        INCLUDE_DIRECT : std_logic := '1';
        BLOCKSIZE      : integer := 1024;
        USE_BUILT_IN_FIFO : std_logic := '0';
        GENERATE_FEI4B : boolean := false;
        VERSAL : boolean := false
    );
    port (
        clk40 : in std_logic; --BC clock for DataIn
        daq_reset : in std_logic; --Acitve high reset
        daq_fifo_flush : in std_logic;
        EpathEnable : in std_logic; --From register map
        EpathEncoding : in std_logic_vector(3 downto 0); --0: direct, 1: 8b10b, 2: HDLC
        ElinkWidth : in std_logic_vector(2 downto 0); --runtime configuration: 0:2, 1:4, 2:8, 3:16, 4:32
        MsbFirst         : in std_logic; --Default 1, make 0 to reverse the bit order
        ReverseInputBits : in std_logic; --Default 0, reverse the bits of the input Elink
        EnableTruncation : in std_logic; --Default 0: 1 to enable truncation mechanism in HDLC decoder for chunks > 12 bytes
        ElinkData : in std_logic_vector(MAX_INPUT-1 downto 0);
        ElinkAligned : in std_logic;
        DecoderAligned : out std_logic;
        AlignmentPulseAlign : in std_logic; --2 pulses to realign if FLAG found
        AlignmentPulseDeAlign : in std_logic; --2 pulses to realign if FLAG found
        AutoRealign : in std_logic; --In case of out of table 8b10b characters, the decoder should misalign and start realignment.
        RealignmentEvent : out std_logic; --In case of data errors, the 8b10b decoder is realigning.
        FE_BUSY_out : out std_logic; --BUSY request from front-end
        m_axis : out axis_32_type;  --FIFO read port (axi stream)
        m_axis_tready : in std_logic; --FIFO read tready (axi stream)
        m_axis_aclk : in std_logic; --FIFO read clock (axi stream)
        m_axis_prog_empty : out std_logic

    );
end DecodingEpathGBT;

architecture Behavioral of DecodingEpathGBT is

    function NUM_DECODERS(I16b: std_logic) return integer is
    begin
        if I16b = '1' then
            return 2;
        else
            return 1;
        end if;
    end function;
    signal GearBoxDataOut: std_logic_vector(NUM_DECODERS(INCLUDE_16b)*10-1 downto 0);
    signal GearBoxDataOutValid: std_logic;
    --signal GearBoxOutputWidth: std_logic_vector(2 downto 0);
    signal BitSlip, BitSlip8b10b: std_logic;
    signal Decoder8b10bDataOut : std_logic_vector(NUM_DECODERS(INCLUDE_16b)*8-1 downto 0);
    signal Decoder8b10bDataOutValid : std_logic_vector(NUM_DECODERS(INCLUDE_16b)-1 downto 0);
    signal Decoder8b10bEOP : std_logic_vector(NUM_DECODERS(INCLUDE_16b)-1 downto 0);
    signal Decoder8b10bSOB : std_logic_vector(NUM_DECODERS(INCLUDE_16b)-1 downto 0);
    signal Decoder8b10bEOB : std_logic_vector(NUM_DECODERS(INCLUDE_16b)-1 downto 0);
    signal Decoder8b10bCodingError: std_logic_vector(NUM_DECODERS(INCLUDE_16b)-1 downto 0);
    signal Decoder8b10bFramingError: std_logic_vector(NUM_DECODERS(INCLUDE_16b)-1 downto 0);

    signal DecoderHDLCAligned : std_logic;
    signal DecoderHDLCDataOut : std_logic_vector(7 downto 0);
    signal DecoderHDLCDataOutValid : std_logic;
    signal DecoderHDLCEOP : std_logic;
    signal TruncateHDLC : std_logic;

    signal ByteToAxiStreamAligned : std_logic;
    signal ByteToAxiStreamDataOut : std_logic_vector(NUM_DECODERS(INCLUDE_16b)*8-1 downto 0);
    signal ByteToAxiStreamDataOutValid : std_logic_vector(NUM_DECODERS(INCLUDE_16b)-1 downto 0);
    signal ByteToAxiStreamGearboxValid : std_logic;
    signal ByteToAxiStreamEOP : std_logic_vector(NUM_DECODERS(INCLUDE_16b)-1 downto 0);
    signal ByteToAxiStreamSOB : std_logic_vector(NUM_DECODERS(INCLUDE_16b)-1 downto 0);
    signal ByteToAxiStreamEOB : std_logic_vector(NUM_DECODERS(INCLUDE_16b)-1 downto 0);
    signal ByteToAxiStreamTruncate : std_logic;
    signal ByteToAxiStreamCodingError: std_logic;
    signal ByteToAxiStreamFramingError: std_logic;
    signal ElinkData_s: std_logic_vector(MAX_INPUT-1 downto 0);
    signal GearBoxDataOutValid_p1  : std_logic;
    signal DecoderHDLCGearboxValid : std_logic;
    signal DecoderAligned8b10b_out: std_logic_vector(1 downto 0);
    signal dispin, dispout, dispout_p1: std_logic_vector(1 downto 0);
begin


    g_16bNSW: if INCLUDE_16b = '1' and MAX_INPUT >= 16 generate
        inputSwitch_proc: process(ElinkData, ElinkWidth)
            variable ElinkData_v, ElinkData_s_v: std_logic_vector(15 downto 0);
        begin
            if ElinkWidth = "011" then
                ElinkData_v(MAX_INPUT-1 downto 0) := ElinkData;
                ElinkData_s_v(15 downto 0) := ElinkData_v(7) & ElinkData_v(15) & --! The NSW ROC ASIC is alternating the bits of the 2 8-bit E-Links
                                              ElinkData_v(6) & ElinkData_v(14) &
                                              ElinkData_v(5) & ElinkData_v(13) &
                                              ElinkData_v(4) & ElinkData_v(12) &
                                              ElinkData_v(3) & ElinkData_v(11) &
                                              ElinkData_v(2) & ElinkData_v(10) &
                                              ElinkData_v(1) & ElinkData_v(9) &
                                              ElinkData_v(0) & ElinkData_v(8);
                ElinkData_s <= ElinkData_s_v(MAX_INPUT-1 downto 0);
            else
                ElinkData_s <= ElinkData;
            end if;
        end process;
    --GearBoxOutputWidth <= "010" when ((EpathEncoding = "0001") and (ElinkWidth = "011")) else --20 bits to decoder for 8b10b on 16-b E-Link (2 decoders).
    --                      "001" when (EpathEncoding = "0001") else --10 bits to decoder for 8b10b
    --                      "000"; --8-bits to decoder for HDLC and direct
    end generate g_16bNSW;
    g_anythingelse: if INCLUDE_16b = '0' or MAX_INPUT < 16 generate
        ElinkData_s <= ElinkData;
    --GearBoxOutputWidth <= "001" when (EpathEncoding = "0001") else "000";
    end generate g_anythingelse;

    gearbox0: entity work.DecodingGearBox
        generic map(
            MAX_INPUT => MAX_INPUT, --: integer := 32;
            MAX_OUTPUT => NUM_DECODERS(INCLUDE_16b)*10, --: integer := 66;
            -- 32, 16, 8, 4, 2
            SUPPORT_INPUT => "0" & INCLUDE_16b & INCLUDE_8b & INCLUDE_4b & INCLUDE_2b
        )
        port map(
            Reset            => daq_reset,
            clk40            => clk40,

            ELinkData        => ElinkData_s,
            ElinkAligned     => ElinkAligned,
            ElinkWidth       => ElinkWidth,
            MsbFirst         => MsbFirst,
            ReverseInputBits => ReverseInputBits,

            DataOut          => GearBoxDataOut,
            DataOutValid     => GearBoxDataOutValid,

            BitSlip           => BitSlip
        );

    with EpathEncoding select BitSlip <=
    '0' when "0000",
    BitSlip8b10b when "0001",
    '0' when others;

    g_include8b10b: if INCLUDE_8b10b = '1' generate
        signal BitSlip8b10b_1: std_logic;
        signal BitSlip8b10b_2: std_logic;
        signal ISK_DelimiterB_out_p1: std_logic_vector(1 downto 0);
        signal ISK_DelimiterB_out: std_logic_vector(1 downto 0);
        signal CharIsK_out: std_logic_vector(1 downto 0);
        signal CharIsK_out_p1: std_logic_vector(1 downto 0);
        signal ISK_SOC_out: std_logic_vector(1 downto 0);
        signal ISK_SOC_out_p1: std_logic_vector(1 downto 0);
        signal ISK_SOC_in: std_logic_vector(1 downto 0);
        signal ISK_EOC_out: std_logic_vector(1 downto 0);
        signal ISK_EOC_out_p1: std_logic_vector(1 downto 0);
        signal ISK_EOC_in: std_logic_vector(1 downto 0);
        signal ISK_DelimiterB_in: std_logic_vector(1 downto 0);
        signal DecoderAligned8b10b_in: std_logic_vector(1 downto 0);
        signal CharIsK_in: std_logic_vector(1 downto 0);
        signal RealignmentEvent0, RealignmentEvent1: std_logic;

        signal dec0_BitSlipIn, dec1_BitSlipIn: std_logic;
    begin
        --        Decoder8b10bAligned <= DecoderAligned8b10b_out;
        dec0_BitSlipIn <= BitSlip8b10b_2 when  ElinkWidth = "011" and INCLUDE_16b = '1' else '0';
        dec1_BitSlipIn <= BitSlip8b10b_1 when  ElinkWidth = "011" and INCLUDE_16b = '1' else '0';
        ISK_SOC_in(0) <= ISK_SOC_out(1) when ElinkWidth = "011" and INCLUDE_16b = '1' else '0';
        ISK_SOC_in(1) <= ISK_SOC_out_p1(0) when ElinkWidth = "011" and INCLUDE_16b = '1' else '0';
        --ISK_EOC_in(0) <= ISK_EOC_out_p1(1) when ElinkWidth = "011" and INCLUDE_16b = '1' else '0';
        ISK_EOC_in(0) <= ISK_EOC_out(1) when ElinkWidth = "011" and INCLUDE_16b = '1' else '0';
        ISK_EOC_in(1) <= ISK_EOC_out_p1(0) when ElinkWidth = "011" and INCLUDE_16b = '1' else '0';
        dispin(0) <= dispout(1) when ElinkWidth = "011" and INCLUDE_16b = '1' else dispout_p1(0);
        dispin(1) <= dispout_p1(0) when ElinkWidth = "011" and INCLUDE_16b = '1' else dispout_p1(1);
        RealignmentEvent <= RealignmentEvent0 or RealignmentEvent1 when ElinkWidth = "011" and INCLUDE_16b = '1' else RealignmentEvent0;


        DecoderLinkSharing_proc: process( clk40 )
        begin
            if rising_edge( clk40 ) then
                if GearBoxDataOutValid_p1 = '1' then
                    ISK_DelimiterB_out_p1 <= ISK_DelimiterB_out; -- the 16 and 8 bit decoders need 1 cycle delay on the second character on the (max. length 3) of the delimiter
                    CharIsK_out_p1 <= CharIsK_out;
                    ISK_EOC_out_p1 <= ISK_EOC_out;
                    ISK_SOC_out_p1 <= ISK_SOC_out;
                end if;

                if GearBoxDataOutValid = '1' then
                    dispout_p1 <= dispout;
                end if;

                if ElinkWidth = "011" and INCLUDE_16b = '1' then    --16-bit E-Link, share SOC/EOC/comma among 2 decoders

                    ISK_DelimiterB_in(0) <= ISK_DelimiterB_out_p1(1); -- second delimiter character shifted by 1 decoder, one cycle ago when wrapping around
                    ISK_DelimiterB_in(1) <= ISK_DelimiterB_out(0);
                    -- last delimiter character not shared, signal is only used inside the decoders

                    DecoderAligned8b10b_in(0) <= DecoderAligned8b10b_out(1);
                    DecoderAligned8b10b_in(1) <= DecoderAligned8b10b_out(0);

                    CharIsK_in(0) <= CharIsK_out_p1(1); -- needed for SOC detection
                    CharIsK_in(1) <= CharIsK_out(0);


                else --Single decoders for 8-bit E-Links, use no alignment sharing.
                    ISK_DelimiterB_in(0) <= ISK_DelimiterB_out_p1(0); -- second delimiter character, output of same decoder 1 cyclecyle ago
                    ISK_DelimiterB_in(1) <= ISK_DelimiterB_out_p1(1);

                    DecoderAligned8b10b_in(0) <= '0';
                    DecoderAligned8b10b_in(1) <= '0';

                    CharIsK_in(0) <= CharIsK_out_p1(0);
                    CharIsK_in(1) <= CharIsK_out_p1(1);


                end if;
            end if;
        end process;


        decoder8b10b0: entity work.Decoder8b10b
            generic map(
                GENERATE_FEI4B => GENERATE_FEI4B, --: boolean := false;
                GENERATE_LCB_ENC => false, --: boolean := false -- DG @ UBC
                AWAIT_SOP => true    )
            port map(
                DataIn => GearBoxDataOut(9 downto 0),
                DataInValid => GearBoxDataOutValid,
                BitSlip => BitSlip8b10b_1,
                AlignmentPulseAlign => AlignmentPulseAlign,
                AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                AutoRealign => AutoRealign,
                RealignmentEvent => RealignmentEvent0,
                --            DecoderNum => '0',
                reset => daq_reset,
                clk40 => clk40,
                HGTD_ALTIROC_DECODING => '0',
                DataOut => Decoder8b10bDataOut(7 downto 0),
                DataOutValid => Decoder8b10bDataOutValid(0),
                EOP => Decoder8b10bEOP(0),
                ElinkSOB => Decoder8b10bSOB(0),
                ElinkEOB => Decoder8b10bEOB(0),
                DecoderAligned_out => DecoderAligned8b10b_out(0),
                DecoderAligned_in => DecoderAligned8b10b_in(0),
                CodingError => Decoder8b10bCodingError(0),
                DisparityError => open,
                FramingError => Decoder8b10bFramingError(0),
                dispout => dispout(0),
                dispin => dispin(0),
                Char_SOC_in => ISK_SOC_in(0),
                Char_SOC_out => ISK_SOC_out(0),
                Char_EOC_in => ISK_EOC_in(0),
                Char_EOC_out => ISK_EOC_out(0),
                ISK_DelimiterA_in => '1',
                ISK_DelimiterB_in => ISK_DelimiterB_in(0),
                ISK_DelimiterA_out => open,
                ISK_DelimiterB_out => ISK_DelimiterB_out(0),
                CharIsK_in => CharIsK_in(0),
                CharIsK_out => CharIsK_out(0),
                BitSlipIn => dec0_BitSlipIn
            );
        g_second8b10bdecoder: if INCLUDE_16b = '1' generate
        begin
            decoder8b10b0: entity work.Decoder8b10b
                generic map(
                    GENERATE_FEI4B => GENERATE_FEI4B, --: boolean := false;
                    GENERATE_LCB_ENC => false, --: boolean := false -- DG @ UBC
                    AWAIT_SOP => true    )
                port map(
                    DataIn => GearBoxDataOut(19 downto 10),
                    DataInValid => GearBoxDataOutValid,
                    BitSlip => BitSlip8b10b_2,
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    AutoRealign => AutoRealign,
                    RealignmentEvent => RealignmentEvent1,
                    --DecoderNum => DecoderNum_16b,
                    reset => daq_reset,
                    clk40 => clk40,
                    HGTD_ALTIROC_DECODING => '0',
                    DataOut => Decoder8b10bDataOut(15 downto 8),
                    DataOutValid => Decoder8b10bDataOutValid(1),
                    EOP => Decoder8b10bEOP(1),
                    ElinkSOB => Decoder8b10bSOB(1),
                    ElinkEOB => Decoder8b10bEOB(1),
                    DecoderAligned_out => DecoderAligned8b10b_out(1),
                    DecoderAligned_in => DecoderAligned8b10b_in(1),
                    CodingError => Decoder8b10bCodingError(1),
                    DisparityError => open,
                    FramingError => Decoder8b10bFramingError(1),
                    dispout => dispout(1),
                    dispin => dispin(1),
                    Char_SOC_in => ISK_SOC_in(1),
                    Char_SOC_out => ISK_SOC_out(1),
                    Char_EOC_in => ISK_EOC_in(1),
                    Char_EOC_out => ISK_EOC_out(1),
                    ISK_DelimiterA_in => '1',
                    ISK_DelimiterB_in => ISK_DelimiterB_in(1),
                    ISK_DelimiterA_out => open,
                    ISK_DelimiterB_out => ISK_DelimiterB_out(1),
                    CharIsK_in => CharIsK_in(1),
                    CharIsK_out => CharIsK_out(1),
                    BitSlipIn => dec1_BitSlipIn
                );
            BitSlip8b10b <= BitSlip8b10b_1 or BitSlip8b10b_2 when ElinkWidth = "011" else
                            BitSlip8b10b_1;
        else generate
            BitSlip8b10b <= BitSlip8b10b_1;
        end generate g_second8b10bdecoder;
    end generate; --INCLUDE_8b10b

    g_includeHDLC: if INCLUDE_HDLC = '1' generate
        signal ElinkData_HDLC: std_logic_vector(1 downto 0);
    begin
        ElinkData_HDLC <= ElinkData(1 downto 0) when ReverseInputBits = '0' else ElinkData(0) & ElinkData(1);
        DecoderHDLCAligned <= '1';
        decoderHDLC0: entity work.DecoderHDLC
            generic map(
                g_WORD_SIZE => 8,
                g_DELIMITER => x"7E",
                g_IDLE => x"7F"
            )
            port map(
                clk40 => clk40,
                ena => EpathEnable,
                reset => daq_reset,
                DataIn => ElinkData_HDLC,
                DataOut => DecoderHDLCDataOut,
                DataOutValid => DecoderHDLCDataOutValid,
                GearboxValidOut => DecoderHDLCGearboxValid,
                EOP => DecoderHDLCEOP,
                TruncateHDLC => TruncateHDLC,
                EnableTruncation => EnableTruncation
            );
    end generate; --INCLUDE_HDLC

    decoding_mux: process(clk40)
    begin
        if rising_edge(clk40) then
            ByteToAxiStreamAligned <= '0';
            ByteToAxiStreamDataOut <= (others => '0');
            ByteToAxiStreamDataOutValid <= (others => '0');
            ByteToAxiStreamEOP <= (others => '0');
            ByteToAxiStreamSOB <= (others => '0');
            ByteToAxiStreamEOB <= (others => '0');
            ByteToAxiStreamTruncate <= '0';
            ByteToAxiStreamCodingError <= '0';
            ByteToAxiStreamFramingError <= '0';
            ByteToAxiStreamGearboxValid <= '0';
            GearBoxDataOutValid_p1 <= GearBoxDataOutValid;

            if EpathEncoding = "0000" and INCLUDE_DIRECT = '1' and EpathEnable = '1' then
                ByteToAxiStreamAligned <= ElinkAligned;
                ByteToAxiStreamDataOut(7 downto 0) <= GearBoxDataOut(7 downto 0);
                ByteToAxiStreamDataOutValid(0) <= GearBoxDataOutValid;
                ByteToAxiStreamGearboxValid <= GearBoxDataOutValid;
            end if;

            if EpathEncoding = "0001" and INCLUDE_8b10b = '1'  and EpathEnable = '1' then
                ByteToAxiStreamDataOut <= Decoder8b10bDataOut;
                ByteToAxiStreamDataOutValid(0) <= Decoder8b10bDataOutValid(0);
                ByteToAxiStreamGearboxValid <= GearBoxDataOutValid_p1;
                ByteToAxiStreamEOP <= Decoder8b10bEOP;
                ByteToAxiStreamSOB<= Decoder8b10bSOB;
                ByteToAxiStreamEOB<= Decoder8b10bEOB;
                if INCLUDE_16b = '1' and ElinkWidth = "011" then
                    ByteToAxiStreamDataOut <= Decoder8b10bDataOut(7 downto 0) & Decoder8b10bDataOut(15 downto 8); --swap the 2 bytes
                    ByteToAxiStreamAligned <= and DecoderAligned8b10b_out;
                    ByteToAxiStreamCodingError <= or Decoder8b10bCodingError;
                    ByteToAxiStreamFramingError <= or Decoder8b10bFramingError;
                    ByteToAxiStreamDataOutValid <= Decoder8b10bDataOutValid(0) & Decoder8b10bDataOutValid(1);
                    ByteToAxiStreamEOP <= Decoder8b10bEOP(0) & Decoder8b10bEOP(1);
                else
                    ByteToAxiStreamAligned <= DecoderAligned8b10b_out(0);
                    ByteToAxiStreamCodingError <= Decoder8b10bCodingError(0);
                    ByteToAxiStreamFramingError <= Decoder8b10bFramingError(0);
                end if;
            end if;

            if EpathEncoding = "0010" and INCLUDE_HDLC = '1'  and EpathEnable = '1' then
                ByteToAxiStreamAligned <= DecoderHDLCAligned;
                ByteToAxiStreamDataOut(7 downto 0) <= DecoderHDLCDataOut;
                ByteToAxiStreamDataOutValid(0) <= DecoderHDLCDataOutValid;
                ByteToAxiStreamGearboxValid <= DecoderHDLCGearboxValid;
                ByteToAxiStreamEOP(0) <= DecoderHDLCEOP;
                ByteToAxiStreamTruncate <= TruncateHDLC;
            end if;


        end if;
    end process;

    DecoderAligned <= ByteToAxiStreamAligned; --To registermap for status.

    toAxis0: entity work.ByteToAxiStream
        generic map(
            BYTES => NUM_DECODERS(INCLUDE_16b),
            BLOCKSIZE => BLOCKSIZE,
            USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
            VERSAL => VERSAL
        )
        port map(
            clk40 => clk40,
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            EnableIn => EpathEnable,
            DataIn => ByteToAxiStreamDataOut,
            DataInValid => ByteToAxiStreamDataOutValid,
            GearboxValid => ByteToAxiStreamGearboxValid,
            ElinkWidth => ElinkWidth,
            EOP => ByteToAxiStreamEOP,
            SOB => ByteToAxiStreamSOB,
            EOB => ByteToAxiStreamEOB,
            TruncateIn => (others => ByteToAxiStreamTruncate),
            CodingErrorIn => ByteToAxiStreamCodingError,
            FramingErrorIn => ByteToAxiStreamFramingError,
            m_axis => m_axis,
            m_axis_tready => m_axis_tready,
            m_axis_aclk => m_axis_aclk,
            m_axis_prog_empty => m_axis_prog_empty
        );

    FE_BUSY_out_proc: process(clk40)
    begin
        if rising_edge(clk40) then
            if daq_reset = '1' or EpathEnable = '0' or ByteToAxiStreamAligned = '0' then
                FE_BUSY_out <= '0';
            else

                for i in 0 to NUM_DECODERS(INCLUDE_16b)-1 loop --Handle start-of-busy from the decoders
                    if ByteToAxiStreamSOB(i) = '1' then
                        FE_BUSY_out <= '1';
                    end if;
                end loop;
                for i in 0 to NUM_DECODERS(INCLUDE_16b)-1 loop --handle end-of-busy from the decoders
                    if ByteToAxiStreamEOB(i) = '1' then
                        FE_BUSY_out <= '0';
                    end if;
                end loop;
            end if;
        end if;
    end process;


end Behavioral;

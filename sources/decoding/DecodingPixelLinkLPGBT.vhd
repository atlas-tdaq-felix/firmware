--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas---tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Ricardo Luz
--!               Frans Schreuder
--!               Marco Trovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;
    use work.package_64b66b.all;

library XPM;
    use XPM.VComponents.all;

entity DecodingPixelLinkLPGBT is
    generic (
        CARD_TYPE      : integer := 712; -- @suppress "Unused generic: CARD_TYPE is not used in work.DecodingPixelLinkLPGBT(Behavioral)"
        RD53Version    : String := "B"; --A or B -- @suppress "Unused generic: SIMU is not used in work.DecodingPixelLinkLPGBT(Behavioral)"
        BLOCKSIZE      : integer := 1024;
        LINK           : IntArray := (0,1);
        SIMU           : integer := 0; -- @suppress "Unused generic: SIMU is not used in work.DecodingPixelLinkLPGBT(Behavioral)"
        STREAMS_TOHOST : integer := 1;
        PCIE_ENDPOINT  : integer := 0; -- @suppress "Unused generic: PCIE_ENDPOINT is not used in work.DecodingPixelLinkLPGBT(Behavioral)"
        VERSAL         : boolean
    );
    port (
        clk40                          :  in std_logic;                        --BC clock for DataIn
        clk160                         :  in std_logic;                        --aggregator clock
        daq_reset                      :  in std_logic;                        --Acitve high reset
        daq_fifo_flush                 :  in std_logic;
        MsbFirst                       :  in std_logic;                        --Default 1, maske 0 to reverse the bit order
        LinkData                       :  in array_224b(0 to 1);   --LPGBT Payload
        LinkAligned                    :  in std_logic_vector(0 to 1);         --LPGBT Link Aligned
        m_axis                         : out axis_32_2d_array_type(0 to 1, 0 to STREAMS_TOHOST-3);      --FIFO read port (axi stream)
        m_axis_tready                  :  in axis_tready_2d_array_type(0 to 1, 0 to STREAMS_TOHOST-3);   --FIFO read tready (axi stream)
        m_axis_aclk                    :  in std_logic;                        --FIFO read clock (axi stream)
        m_axis_prog_empty              : out axis_tready_2d_array_type(0 to 1, 0 to STREAMS_TOHOST-3);
        register_map_control           :  in register_map_control_type;       --Settings (From Wupper)
        --register_map_decoding_monitor  : out register_map_decoding_monitor_type

        DecoderAligned_out             : out std_logic_vector(2*(STREAMS_TOHOST-2)-1 downto 0);
        DecoderDeskewed_out            : out std_logic_vector(2*(STREAMS_TOHOST-2)-1 downto 0);
        cnt_rx_64b66bhdr_out           : out array_32b(0 to 1);
        rx_soft_err_cnt_out            : out array_2d_32b(0 to 1, 0 to 5);
        rx_soft_err_rst                :  in std_logic_vector(0 to 1)
    );
end DecodingPixelLinkLPGBT;

architecture Behavioral of DecodingPixelLinkLPGBT is
    --decoder registers
    signal useEmulator          : std_logic_vector(0 to 1);
    signal CBOPT                : std_logic_vector(3 downto 0);
    signal mask_k_char          : array_4b(0 to 1);
    signal ref_packet_in        : array_32b(0 to 1);
    signal ref_packet_in_40     : array_32b(0 to 1);

    --decoder output
    signal EgroupData_clk160            : array_2d_data32VL_64b66b_type(0 to 1, 0 to 5) := (others => (others => data32VL_64b66b_type_zero));
    signal ByteToAxiStreamKeep_clk160   : array_2d_4b(0 to 1, 0 to 5)                   := (others => (others => (others => '0')));
    signal EgroupDCSData_clk160         : array_2d_data32VL_64b66b_type(0 to 1, 0 to 5) := (others => (others => data32VL_64b66b_type_zero));
    signal DecoderAligned               : array_inv_6b(0 to 1)                          := (others => (others => '0'));
    signal DecoderDeskewed              : array_inv_6b(0 to 1)                          := (others => (others => '0'));
    signal rx_soft_err_i                : array_inv_6b(0 to 1)                          := (others => (others => '0')); -- @suppress "signal rx_soft_err_i is never read"
    signal rx_soft_err_cnt_reg          : array_2d_32b(0 to 1, 0 to 5)                  := (others => (others => (others => '0')));

    --
    --debug
    --clkd40
    signal EgroupGB1Data_dbg           : array_2d_data32VHHV_64b66b_type(0 to 1, 0 to 5); -- @suppress "signal EgroupGB1Data_dbg is never read"
    signal EgroupUnscrData_dbg         : array_2d_data32VHHV_64b66b_type(0 to 1, 0 to 5); -- @suppress "signal EgroupUnscrData_dbg is never read"
    signal EgroupGB2Data_dbg           : array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5); -- @suppress "signal EgroupGB2Data_dbg is never read"
    signal EgroupRmpData_dbg           : array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5); -- @suppress "signal EgroupRmpData_dbg is never read"
    signal EgroupDecData_dbg           : array_2d_data64V_64b66b_type(0 to 1, 0 to 5); -- @suppress "signal EgroupDecData_dbg is never read"
    --signal EgroupDecDataSplit_dbg      : array_2d_data32VL_64b66b_type(0 to 1, 0 to 5); -- @suppress "signal EgroupDecDataSplit_dbg is never read"
    signal EgroupMUX1Data_dbg          : array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5); -- @suppress "signal EgroupMUX1Data_dbg is never read"
    signal EgroupBondData_dbg          : array_2d_data64VHHVE_64b66b_type(0 to 1, 0 to 5); -- @suppress "signal EgroupBondData_dbg is never read"
    signal DecoderAlignedRmp_dbg       : array_inv_6b(0 to 1); -- @suppress "signal DecoderAlignedRmp_dbg is never read"
    signal DecoderAlignedMUX1_dbg      : array_inv_6b(0 to 1); -- @suppress "signal DecoderAlignedMUX1_dbg is never read"
    signal DecoderAlignedBond_dbg      : std_logic_vector(0 to 5); -- @suppress "signal DecoderAlignedBond_3_dbg is never read" -- @suppress "signal DecoderAlignedBond_dbg is never read"
    signal DCSDecData_dbg              : array_2d_data64V_64b66b_type(0 to 1, 0 to 5); -- @suppress "signal DCSDecData_dbg is never read"
    --clk160
    signal EgroupAggrData_dbg          : array_data64V_64b66b_type(0 to 5);-- @suppress "signal EgroupAggrData_30_dbg is never read" -- @suppress "signal EgroupAggrData_dbg is never read"
    signal DCSDecDataSplit_clk160_dbg    : array_2d_data32VL_64b66b_type(0 to 1, 0 to 5); -- @suppress "signal DCSDecDataSplit_clk160_dbg is never read"
    signal DecoderAlignedDec_clk160_dbg :  array_inv_6b(0 to 1);  -- @suppress "signal DecoderAlignedDec_clk160_dbg is never read"

    signal EgroupDecData_clk160_dbg      : array_2d_data64V_64b66b_type(0 to 1, 0 to 5); -- @suppress "signal EgroupDecData_clk160_dbg is never read"
    signal EgroupMUX2Data_clk160_dbg     : array_2d_data64V_64b66b_type(0 to 1, 0 to 5); -- @suppress "signal EgroupMUX2Data_clk160_dbg is never read"
    signal EgroupMUX2DataSplit_clk160_dbg : array_2d_data32VL_64b66b_type(0 to 1, 0 to 5); -- @suppress "signal EgroupMUX2DataSplit_clk160_dbg is never read"

    --axi stream
    signal m_axis_dbg                  : axis_32_2d_array_type(0 to 1, 0 to STREAMS_TOHOST-3);
    signal m_axis_tready_dbg           : axis_tready_2d_array_type(0 to 1, 0 to STREAMS_TOHOST-3);
    signal m_axis_prog_empty_dbg       : axis_tready_2d_array_type(0 to 1, 0 to STREAMS_TOHOST-3); -- @suppress "signal m_axis_prog_empty_dbg is never read"
    signal cnt_rx_64b66bhdr_160_i      : array_32b(0 to 1); -- @suppress "signal cnt_rx_64b66bhdr_160_i is never read"
    signal cnt_rx_maxishdr_i           : array_32b(0 to 1); -- @suppress "signal cnt_rx_maxishdr_i is never read"
begin
    rx_soft_err_cnt_out     <= rx_soft_err_cnt_reg;

    useEmulator             <= register_map_control.GBT_TOHOST_FANOUT.SEL(LINK(0)) &
                               register_map_control.GBT_TOHOST_FANOUT.SEL(LINK(1));

    cdc_CBOPT : xpm_cdc_array_single
        generic map (
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0,
            WIDTH => 4
        )
        port map (
            dest_out => CBOPT,  --ignores the possibility of the two Links having different channel bonding configuration
            dest_clk => clk160,
            src_clk => clk40,
            src_in => register_map_control.DECODING_LINK_CB(LINK(0) mod 12).CBOPT
        );

    mask_k_char             <= register_map_control.DECODING_MASK64B66BKBLOCK &
                               register_map_control.DECODING_MASK64B66BKBLOCK;
    ref_packet_in_40        <= register_map_control.YARR_DEBUG_ALLEGROUP_TOHOST(LINK(0) mod 12).REF_PACKET(63 downto 32) &
                               register_map_control.YARR_DEBUG_ALLEGROUP_TOHOST(LINK(1) mod 12).REF_PACKET(63 downto 32);

    g_refpacket_cdc: for i in 0 to 1 generate
        cdc_ref_packet_in : xpm_cdc_array_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0,
                WIDTH => 32
            )
            port map (
                dest_out => ref_packet_in(i),
                dest_clk => clk160,
                src_clk => clk40,
                src_in => ref_packet_in_40(i)
            );
    end generate;



    g_decoding_RD53B: if RD53Version = "B" generate
    begin
        decoding_64b66b_0: entity work.decoding_64b66b
            generic map(
                RD53Version                     => RD53Version
            )
            Port map(
                useEmulator                     => useEmulator,
                cbopt                           => CBOPT,
                mask_k_char                     => mask_k_char,
                MsbFirst                        => MsbFirst,
                reset                           => daq_reset,
                clk40_in                        => clk40,
                clk160_in                       => clk160,
                LinkAligned_in                  => LinkAligned,
                DataIn                          => LinkData,
                DataOut                         => EgroupData_clk160,
                BytesKeepOut                    => ByteToAxiStreamKeep_clk160,
                DataDCSOut                      => EgroupDCSData_clk160,
                DecoderAligned_out              => DecoderAligned,
                DecoderDeskewed_out             => DecoderDeskewed,
                soft_err                        => rx_soft_err_i,
                soft_err_cnt                    => rx_soft_err_cnt_reg,
                soft_err_rst                    => rx_soft_err_rst,
                --debug (regmap)
                cnt_rx_64b66bhdr_out            => cnt_rx_64b66bhdr_out,
                ref_packet_in                   => ref_packet_in,
                EgroupGB1Data_dbg               => EgroupGB1Data_dbg,
                EgroupUnscrData_dbg             => EgroupUnscrData_dbg,
                EgroupGB2Data_dbg               => EgroupGB2Data_dbg,
                EgroupRmpData_dbg               => EgroupRmpData_dbg,
                EgroupDecData_dbg               => EgroupDecData_dbg,
                DCSDecData_dbg                  => DCSDecData_dbg,
                EgroupMUX1Data_dbg              => EgroupMUX1Data_dbg,
                EgroupBondData_dbg              => EgroupBondData_dbg,
                DecoderAlignedRmp_dbg           => DecoderAlignedRmp_dbg,
                DecoderAlignedMUX1_dbg          => DecoderAlignedMUX1_dbg,
                DecoderAlignedBond_dbg          => DecoderAlignedBond_dbg,
                EgroupAggrData_dbg              => EgroupAggrData_dbg,
                DCSDecDataSplit_clk160_dbg      => DCSDecDataSplit_clk160_dbg,
                DecoderAlignedDec_clk160_dbg    => DecoderAlignedDec_clk160_dbg,
                EgroupDecData_clk160_dbg        => EgroupDecData_clk160_dbg,
                EgroupMUX2Data_clk160_dbg       => EgroupMUX2Data_clk160_dbg,
                EgroupMUX2DataSplit_clk160_dbg  => EgroupMUX2DataSplit_clk160_dbg
            );
    end generate; --g_decoding_RD53B: if RD53Version = "B" generate

    g_decoding_RD53A: if RD53Version = "A" generate --no CB or CB 3. CB 2 and CB 4 not implemented
    begin
        g_links_decRD53A: for link_i in 0 to 1 generate
            signal EgroupData_clk160_tmp            : array_data32VL_64b66b_type(0 to 5) := (others => data32VL_64b66b_type_zero);
            signal ByteToAxiStreamKeep_clk160_tmp   : array_4b(0 to 5)                   := (others => (others => '0'));
            signal EgroupDCSData_clk160_tmp         : array_data32VL_64b66b_type(0 to 5) := (others => data32VL_64b66b_type_zero);
            signal rx_soft_err_cnt_reg_tmp          : array_32b(0 to 5)                  := (others => (others => '0')); -- @suppress "signal rx_soft_err_cnt_reg_tmp is never read"
        begin

            g_egroup_decRD53A: for egroup in 0 to 5 generate
            begin
                EgroupData_clk160(link_i,egroup)            <= EgroupData_clk160_tmp(egroup);
                ByteToAxiStreamKeep_clk160(link_i,egroup)   <= ByteToAxiStreamKeep_clk160_tmp(egroup);
                EgroupDCSData_clk160(link_i,egroup)         <= EgroupDCSData_clk160_tmp(egroup);
            end generate;

            decoding_64b66b_0: entity work.decoding_64b66b_RD53A
                generic map(
                    RD53Version                     => RD53Version
                )
                Port map(
                    useEmulator                     => register_map_control.GBT_TOHOST_FANOUT.SEL(LINK(link_i)),
                    cbopt                           => CBOPT,
                    mask_k_char                     => mask_k_char(link_i),
                    MsbFirst                        => MsbFirst,
                    reset                           => daq_reset,
                    clk40_in                        => clk40,
                    clk160_in                       => clk160,
                    LinkAligned_in                  => LinkAligned(link_i),
                    DataIn                          => LinkData(link_i),
                    DataOut                         => EgroupData_clk160_tmp,
                    BytesKeepOut                    => ByteToAxiStreamKeep_clk160_tmp,
                    DataDCSOut                      => EgroupDCSData_clk160_tmp,
                    DecoderAligned_out              => DecoderAligned(link_i),
                    DecoderDeskewed_out             => DecoderDeskewed(link_i),
                    soft_err                        => open, --rx_soft_err_i,
                    soft_err_cnt                    => rx_soft_err_cnt_reg_tmp,
                    soft_err_rst                    => rx_soft_err_rst(link_i),
                    --debug (regmap)
                    cnt_rx_64b66bhdr_out            => cnt_rx_64b66bhdr_out(link_i),
                    ref_packet_in                   => ref_packet_in(link_i),
                    --ila
                    EgroupGB1Data_dbg               => open,
                    EgroupUnscrData_dbg             => open,
                    EgroupGB2Data_dbg               => open,
                    EgroupRmpData_dbg               => open,
                    EgroupDecData_dbg               => open,
                    EgroupDecDataSplit_dbg          => open,
                    DCSDecData_dbg                  => open,
                    EgroupMUX1Data_dbg              => open,
                    EgroupBondData_3_dbg            => open,
                    DecoderAlignedRmp_dbg           => open,
                    DecoderAlignedMUX1_dbg          => open,
                    DecoderAlignedBond_3_dbg        => open,
                    EgroupDecDataSplit_clk160_dbg   => open,
                    EgroupMUX2Data_clk160_dbg       => open,
                    EgroupAggrData_30_dbg           => open,
                    EgroupAggrData_31_dbg           => open,
                    DCSDecDataSplit_clk160_dbg      => open,
                    DecoderAlignedDec_clk160_dbg    => open
                );
        end generate; -- g_links_decRD53A: for link in 0 to 1 generate
    end generate;

    g_link_loop: for link_i in 0 to 1 generate
    begin
        CntRcvdPckts_64b66b_i: entity work.cntRcvdPckts_64b66b port map (
                rst                         => daq_reset,
                clk40                       => clk160,
                data_in                     => EgroupData_clk160(link_i,0).data(31 downto 25),
                datav_in                    => EgroupData_clk160(link_i,0).valid,
                ref_packet_in               => ref_packet_in(link_i)(31 downto 25),
                cnt_packets_out             => cnt_rx_64b66bhdr_160_i(link_i)
            );
        --cnt_rx_64b66bhdr_160_out <= cnt_rx_64b66bhdr_160_i; to regmap

        CntRcvdPckts_64b66b_2_i: entity work.cntRcvdPckts_64b66b port map (
                rst                         => daq_reset,
                clk40                       => m_axis_aclk,
                data_in                     => m_axis_dbg(link_i,0).tdata(31 downto 25),
                datav_in                    => m_axis_dbg(link_i,0).tvalid and m_axis_tready_dbg(link_i,0),
                ref_packet_in               => ref_packet_in(link_i)(31 downto 25),
                cnt_packets_out             => cnt_rx_maxishdr_i(link_i)
            );

        g_LPGBT_ByteToAxiStream: for egroup in 0 to 5 generate
        begin
            g_axisindex_2to3: for i in 2 to 3 generate
                m_axis(link_i,egroup*4+i).tvalid <= '0';
                m_axis(link_i,egroup*4+i).tdata  <= (others => '0');
                m_axis(link_i,egroup*4+i).tlast  <= '0';
                m_axis(link_i,egroup*4+i).tkeep  <= (others => '0');
                m_axis(link_i,egroup*4+i).tuser  <= (others => '0');
                m_axis_prog_empty(link_i,egroup*4+i) <= '1';
            end generate;
            g_axisindex_0to1: for i in 0 to 1 generate
                signal m_axis_s                    : axis_32_type;
                signal m_axis_tready_s             : std_logic;
                signal m_axis_prog_empty_s         : std_logic;
                signal ByteToAxiStreamData         : std_logic_vector(32 downto 0);
                signal ByteToAxiStreamEOP          : std_logic;
                signal ByteToAxiStreamKeep         : std_logic_vector(3 downto 0);
                signal ByteToAxiStreamElinkBusy    : std_logic;
                signal ByteToAxiStreamTruncate     : std_logic;

                signal EpathEncoding               : std_logic_vector(3 downto 0);
                signal EpathEnable                 : std_logic;
            begin
                EpathEncoding <= register_map_control.DECODING_EGROUP_CTRL(LINK(link_i) mod 12)(egroup).PATH_ENCODING(14+4*i downto 11+4*i);
                EpathEnable <= register_map_control.DECODING_EGROUP_CTRL(LINK(link_i) mod 12)(egroup).EPATH_ENA(i);

                decaligned_proc: if ( i=0 ) generate
                    DecoderAligned_out(link_i*(STREAMS_TOHOST-2)+egroup*4+3 downto link_i*(STREAMS_TOHOST-2)+egroup*4) <= ("000" & DecoderAligned(link_i)(egroup)) when (EpathEncoding = "0011" and EpathEnable = '1') else "0000";
                    DecoderDeskewed_out(link_i*(STREAMS_TOHOST-2)+egroup*4+3 downto link_i*(STREAMS_TOHOST-2)+egroup*4) <= ("000" & DecoderDeskewed(link_i)(egroup)) when (EpathEncoding = "0011" and EpathEnable = '1') else "0000";
                end generate;
                --i=0 => hit data, i=1 => dcs
                m_axis(link_i,egroup*4+i)                   <= m_axis_s;
                m_axis_tready_s                             <= m_axis_tready(link_i,egroup*4+i);
                m_axis_prog_empty(link_i,egroup*4+i)        <= m_axis_prog_empty_s;

                --debug
                m_axis_dbg(link_i,egroup*4+i)               <= m_axis_s;
                m_axis_tready_dbg(link_i,egroup*4+i)        <= m_axis_tready(link_i,egroup*4+i);
                m_axis_prog_empty_dbg(link_i,egroup*4+i)    <= m_axis_prog_empty_s;
                decoding_mux: process(clk160)
                begin
                    if rising_edge(clk160) then
                        if EpathEncoding = "0011" and EpathEnable = '1' then
                            if (i = 0) then
                                ByteToAxiStreamData         <= EgroupData_clk160(link_i,egroup).valid & EgroupData_clk160(link_i,egroup).data;
                                ByteToAxiStreamEOP          <= EgroupData_clk160(link_i,egroup).tlast;
                                ByteToAxiStreamKeep         <= ByteToAxiStreamKeep_clk160(link_i,egroup);
                                ByteToAxiStreamElinkBusy    <= '0'; --to do: from the RD53 service frame with statusbit=5.
                                ByteToAxiStreamTruncate     <= '0';
                            else
                                ByteToAxiStreamData         <= EgroupDCSData_clk160(link_i,egroup).valid & EgroupDCSData_clk160(link_i,egroup).data;
                                ByteToAxiStreamEOP          <= EgroupDCSData_clk160(link_i,egroup).tlast;
                                ByteToAxiStreamKeep         <= "1111";
                                ByteToAxiStreamElinkBusy    <= '0';
                                ByteToAxiStreamTruncate     <= '0';
                            end if;
                        else
                            ByteToAxiStreamData      <= (others=>'0');
                            ByteToAxiStreamEOP          <= '0';
                            ByteToAxiStreamKeep         <= (others => '0');
                            ByteToAxiStreamElinkBusy    <= '0';
                            ByteToAxiStreamTruncate     <= '0';
                        end if; --if EpathEncoding(egroup*4+i) = "0011" and EpathEnable(egroup*4+i) = '1'
                    end if;
                end process;

                toAxis0: entity work.ByteToAxiStream32b
                    generic map(
                        BLOCKSIZE           => BLOCKSIZE,
                        USE_BUILT_IN_FIFO   => '0',
                        VERSAL              => VERSAL,
                        EGROUP              => egroup,
                        STREAM              => i
                    )
                    port map(
                        clk                 => clk160,
                        daq_reset           => daq_reset,
                        daq_fifo_flush      => daq_fifo_flush,
                        EnableIn            => EpathEnable,
                        DataIn              => ByteToAxiStreamData(31 downto 0),
                        DataInValid         => ByteToAxiStreamData(32),
                        EOP                 => ByteToAxiStreamEOP,
                        BytesKeep           => ByteToAxiStreamKeep,
                        ElinkBusy           => ByteToAxiStreamElinkBusy,
                        TruncateIn          => ByteToAxiStreamTruncate,
                        m_axis              => m_axis_s,
                        m_axis_tready       => m_axis_tready_s,
                        m_axis_aclk         => m_axis_aclk,
                        m_axis_prog_empty   => m_axis_prog_empty_s
                    );
            end generate; -- g_axisindex_0to1: for i in 0 to 1 generate
        end generate; --g_LPGBT_ByteToAxiStream: for egroup in 0 to 5 generate

    --            debug ILAS
    --            g_ILA_DECODINGPIXELLINKLPGBT: if SIMU = 0 and PCIE_ENDPOINT = 0 and LINK = (0,1) generate
    --                --64b66b decoding
    --                ila_pixellinklpgbt_1_i: entity work.ila_pixellinklpgbt_1
    --                    port map
    --                    (
    --                        clk         => clk40,
    --                        probe0      => LinkData(link_i),
    --                        probe1      => EgroupGB1Data_dbg(link_i,0).hdr_valid & EgroupGB1Data_dbg(link_i,0).hdr & EgroupGB1Data_dbg(link_i,0).valid & EgroupGB1Data_dbg(link_i,0).data, --array_data32VHHV_64b66b_type 1+2+1+32
    --                        probe2      => EgroupGB1Data_dbg(link_i,1).hdr_valid & EgroupGB1Data_dbg(link_i,1).hdr & EgroupGB1Data_dbg(link_i,1).valid & EgroupGB1Data_dbg(link_i,1).data,
    --                        probe3      => EgroupGB1Data_dbg(link_i,2).hdr_valid & EgroupGB1Data_dbg(link_i,2).hdr & EgroupGB1Data_dbg(link_i,2).valid & EgroupGB1Data_dbg(link_i,2).data,
    --                        probe4      => EgroupGB1Data_dbg(link_i,3).hdr_valid & EgroupGB1Data_dbg(link_i,3).hdr & EgroupGB1Data_dbg(link_i,3).valid & EgroupGB1Data_dbg(link_i,3).data,
    --                        probe5      => EgroupGB1Data_dbg(link_i,4).hdr_valid & EgroupGB1Data_dbg(link_i,4).hdr & EgroupGB1Data_dbg(link_i,4).valid & EgroupGB1Data_dbg(link_i,4).data,
    --                        probe6      => EgroupGB1Data_dbg(link_i,5).hdr_valid & EgroupGB1Data_dbg(link_i,5).hdr & EgroupGB1Data_dbg(link_i,5).valid & EgroupGB1Data_dbg(link_i,5).data,
    --                        --probe7      => EgroupGB1Data_dbg(link_i,6).hdr_valid & EgroupGB1Data_dbg(link_i,6).hdr & EgroupGB1Data_dbg(link_i,6).valid & EgroupGB1Data_dbg(link_i,6).data,
    --                        probe7      => EgroupUnscrData_dbg(link_i,0).hdr_valid & EgroupUnscrData_dbg(link_i,0).hdr & EgroupUnscrData_dbg(link_i,0).valid & EgroupUnscrData_dbg(link_i,0).data, --array_data32VHHV_64b66b_type 1+2+1+32
    --                        probe8      => EgroupUnscrData_dbg(link_i,1).hdr_valid & EgroupUnscrData_dbg(link_i,1).hdr & EgroupUnscrData_dbg(link_i,1).valid & EgroupUnscrData_dbg(link_i,1).data,
    --                        probe9      => EgroupUnscrData_dbg(link_i,2).hdr_valid & EgroupUnscrData_dbg(link_i,2).hdr & EgroupUnscrData_dbg(link_i,2).valid & EgroupUnscrData_dbg(link_i,2).data,
    --                        probe10     => EgroupUnscrData_dbg(link_i,3).hdr_valid & EgroupUnscrData_dbg(link_i,3).hdr & EgroupUnscrData_dbg(link_i,3).valid & EgroupUnscrData_dbg(link_i,3).data,
    --                        probe11     => EgroupUnscrData_dbg(link_i,4).hdr_valid & EgroupUnscrData_dbg(link_i,4).hdr & EgroupUnscrData_dbg(link_i,4).valid & EgroupUnscrData_dbg(link_i,4).data,
    --                        probe12     => EgroupUnscrData_dbg(link_i,5).hdr_valid & EgroupUnscrData_dbg(link_i,5).hdr & EgroupUnscrData_dbg(link_i,5).valid & EgroupUnscrData_dbg(link_i,5).data,
    --                        --probe14     => EgroupUnscrData_dbg(link_i,6).hdr_valid & EgroupUnscrData_dbg(link_i,6).hdr & EgroupUnscrData_dbg(link_i,6).valid & EgroupUnscrData_dbg(link_i,6).data,
    --                        probe13     => EgroupGB2Data_dbg(link_i,0).en & EgroupGB2Data_dbg(link_i,0).hdr_valid & EgroupGB2Data_dbg(link_i,0).hdr & EgroupGB2Data_dbg(link_i,0).valid & EgroupGB2Data_dbg(link_i,0).data, --array_data64VHHVE_64b66b_type 1+1+2+1+64
    --                        probe14     => EgroupGB2Data_dbg(link_i,1).en & EgroupGB2Data_dbg(link_i,1).hdr_valid & EgroupGB2Data_dbg(link_i,1).hdr & EgroupGB2Data_dbg(link_i,1).valid & EgroupGB2Data_dbg(link_i,1).data,
    --                        probe15     => EgroupGB2Data_dbg(link_i,2).en & EgroupGB2Data_dbg(link_i,2).hdr_valid & EgroupGB2Data_dbg(link_i,2).hdr & EgroupGB2Data_dbg(link_i,2).valid & EgroupGB2Data_dbg(link_i,2).data,
    --                        probe16     => EgroupGB2Data_dbg(link_i,3).en & EgroupGB2Data_dbg(link_i,3).hdr_valid & EgroupGB2Data_dbg(link_i,3).hdr & EgroupGB2Data_dbg(link_i,3).valid & EgroupGB2Data_dbg(link_i,3).data,
    --                        probe17     => EgroupGB2Data_dbg(link_i,4).en & EgroupGB2Data_dbg(link_i,4).hdr_valid & EgroupGB2Data_dbg(link_i,4).hdr & EgroupGB2Data_dbg(link_i,4).valid & EgroupGB2Data_dbg(link_i,4).data,
    --                        probe18     => EgroupGB2Data_dbg(link_i,5).en & EgroupGB2Data_dbg(link_i,5).hdr_valid & EgroupGB2Data_dbg(link_i,5).hdr & EgroupGB2Data_dbg(link_i,5).valid & EgroupGB2Data_dbg(link_i,5).data,
    --                        --probe21     => EgroupGB2Data_dbg(link_i,6).en & EgroupGB2Data_dbg(link_i,6).hdr_valid & EgroupGB2Data_dbg(link_i,6).hdr & EgroupGB2Data_dbg(link_i,6).valid & EgroupGB2Data_dbg(link_i,6).data,
    --                        probe19     => EgroupRmpData_dbg(link_i,0).en & EgroupRmpData_dbg(link_i,0).hdr_valid & EgroupRmpData_dbg(link_i,0).hdr & EgroupRmpData_dbg(link_i,0).valid & EgroupRmpData_dbg(link_i,0).data, --array_data64VHHVE_64b66b_type 1+1+2+1+64
    --                        probe20     => EgroupRmpData_dbg(link_i,1).en & EgroupRmpData_dbg(link_i,1).hdr_valid & EgroupRmpData_dbg(link_i,1).hdr & EgroupRmpData_dbg(link_i,1).valid & EgroupRmpData_dbg(link_i,1).data,
    --                        probe21     => EgroupRmpData_dbg(link_i,2).en & EgroupRmpData_dbg(link_i,2).hdr_valid & EgroupRmpData_dbg(link_i,2).hdr & EgroupRmpData_dbg(link_i,2).valid & EgroupRmpData_dbg(link_i,2).data,
    --                        probe22     => EgroupRmpData_dbg(link_i,3).en & EgroupRmpData_dbg(link_i,3).hdr_valid & EgroupRmpData_dbg(link_i,3).hdr & EgroupRmpData_dbg(link_i,3).valid & EgroupRmpData_dbg(link_i,3).data,
    --                        probe23     => EgroupRmpData_dbg(link_i,4).en & EgroupRmpData_dbg(link_i,4).hdr_valid & EgroupRmpData_dbg(link_i,4).hdr & EgroupRmpData_dbg(link_i,4).valid & EgroupRmpData_dbg(link_i,4).data,
    --                        probe24     => EgroupRmpData_dbg(link_i,5).en & EgroupRmpData_dbg(link_i,5).hdr_valid & EgroupRmpData_dbg(link_i,5).hdr & EgroupRmpData_dbg(link_i,5).valid & EgroupRmpData_dbg(link_i,5).data,
    --                        --probe28     => EgroupRmpData_dbg(link_i,6).en & EgroupRmpData_dbg(link_i,6).hdr_valid & EgroupRmpData_dbg(link_i,6).hdr & EgroupRmpData_dbg(link_i,6).valid & EgroupRmpData_dbg(link_i,6).data,
    --                        probe25     => EgroupDecData_dbg(link_i,0).valid & EgroupDecData_dbg(link_i,0).data, --array_data64V_64b66b_type 1+64
    --                        probe26     => EgroupDecData_dbg(link_i,1).valid & EgroupDecData_dbg(link_i,1).data,
    --                        probe27     => EgroupDecData_dbg(link_i,2).valid & EgroupDecData_dbg(link_i,2).data,
    --                        probe28     => EgroupDecData_dbg(link_i,3).valid & EgroupDecData_dbg(link_i,3).data,
    --                        probe29     => EgroupDecData_dbg(link_i,4).valid & EgroupDecData_dbg(link_i,4).data,
    --                        probe30     => EgroupDecData_dbg(link_i,5).valid & EgroupDecData_dbg(link_i,5).data,
    --                        --probe35     => EgroupDecData_dbg(link_i,6).valid & EgroupDecData_dbg(link_i,6).data,
    --                        probe31     => EgroupMUX1Data_dbg(link_i,0).en & EgroupMUX1Data_dbg(link_i,0).hdr_valid & EgroupMUX1Data_dbg(link_i,0).hdr & EgroupMUX1Data_dbg(link_i,0).valid & EgroupMUX1Data_dbg(link_i,0).data, --array_data64VHHVE_64b66b_type 1+1+2+1+64
    --                        probe32     => EgroupMUX1Data_dbg(link_i,1).en & EgroupMUX1Data_dbg(link_i,1).hdr_valid & EgroupMUX1Data_dbg(link_i,1).hdr & EgroupMUX1Data_dbg(link_i,1).valid & EgroupMUX1Data_dbg(link_i,1).data,
    --                        probe33     => EgroupMUX1Data_dbg(link_i,2).en & EgroupMUX1Data_dbg(link_i,2).hdr_valid & EgroupMUX1Data_dbg(link_i,2).hdr & EgroupMUX1Data_dbg(link_i,2).valid & EgroupMUX1Data_dbg(link_i,2).data,
    --                        probe34     => EgroupMUX1Data_dbg(link_i,3).en & EgroupMUX1Data_dbg(link_i,3).hdr_valid & EgroupMUX1Data_dbg(link_i,3).hdr & EgroupMUX1Data_dbg(link_i,3).valid & EgroupMUX1Data_dbg(link_i,3).data,
    --                        probe35     => EgroupMUX1Data_dbg(link_i,4).en & EgroupMUX1Data_dbg(link_i,4).hdr_valid & EgroupMUX1Data_dbg(link_i,4).hdr & EgroupMUX1Data_dbg(link_i,4).valid & EgroupMUX1Data_dbg(link_i,4).data,
    --                        probe36     => EgroupMUX1Data_dbg(link_i,5).en & EgroupMUX1Data_dbg(link_i,5).hdr_valid & EgroupMUX1Data_dbg(link_i,5).hdr & EgroupMUX1Data_dbg(link_i,5).valid & EgroupMUX1Data_dbg(link_i,5).data,
    --                        --probe42     => EgroupMUX1Data_dbg(link_i,6).en & EgroupMUX1Data_dbg(link_i,6).hdr_valid & EgroupMUX1Data_dbg(link_i,6).hdr & EgroupMUX1Data_dbg(link_i,6).valid & EgroupMUX1Data_dbg(link_i,6).data,
    --                        probe37     => EgroupBondData_dbg(link_i,0).en & EgroupBondData_dbg(link_i,0).hdr_valid & EgroupBondData_dbg(link_i,0).hdr & EgroupBondData_dbg(link_i,0).valid & EgroupBondData_dbg(link_i,0).data, --array_data64VHHVE_64b66b_type 1+1+2+1+64
    --                        probe38     => EgroupBondData_dbg(link_i,1).en & EgroupBondData_dbg(link_i,1).hdr_valid & EgroupBondData_dbg(link_i,1).hdr & EgroupBondData_dbg(link_i,1).valid & EgroupBondData_dbg(link_i,1).data,
    --                        probe39     => EgroupBondData_dbg(link_i,2).en & EgroupBondData_dbg(link_i,2).hdr_valid & EgroupBondData_dbg(link_i,2).hdr & EgroupBondData_dbg(link_i,2).valid & EgroupBondData_dbg(link_i,2).data,
    --                        probe40     => EgroupBondData_dbg(link_i,3).en & EgroupBondData_dbg(link_i,3).hdr_valid & EgroupBondData_dbg(link_i,3).hdr & EgroupBondData_dbg(link_i,3).valid & EgroupBondData_dbg(link_i,3).data,
    --                        probe41     => EgroupBondData_dbg(link_i,4).en & EgroupBondData_dbg(link_i,4).hdr_valid & EgroupBondData_dbg(link_i,4).hdr & EgroupBondData_dbg(link_i,4).valid & EgroupBondData_dbg(link_i,4).data,
    --                        probe42     => EgroupBondData_dbg(link_i,5).en & EgroupBondData_dbg(link_i,5).hdr_valid & EgroupBondData_dbg(link_i,5).hdr & EgroupBondData_dbg(link_i,5).valid & EgroupBondData_dbg(link_i,5).data,
    --                        probe43     => --DecoderAligned(link_i)(6) &
    --                                       DecoderAligned(link_i)(5) & DecoderAligned(link_i)(4) & DecoderAligned(link_i)(3) & DecoderAligned(link_i)(2) & DecoderAligned(link_i)(1) & DecoderAligned(link_i)(0),
    --                        probe44     => --DecoderAlignedRmp_dbg(link_i)(6) &
    --                                       DecoderAlignedRmp_dbg(link_i)(5) & DecoderAlignedRmp_dbg(link_i)(4) & DecoderAlignedRmp_dbg(link_i)(3) & DecoderAlignedRmp_dbg(link_i)(2) & DecoderAlignedRmp_dbg(link_i)(1) & DecoderAlignedRmp_dbg(link_i)(0),
    --                        probe45     => --DecoderAlignedMUX1_dbg(link_i)(6) &
    --                                       DecoderAlignedMUX1_dbg(link_i)(5) & DecoderAlignedMUX1_dbg(link_i)(4) & DecoderAlignedMUX1_dbg(link_i)(3) & DecoderAlignedMUX1_dbg(link_i)(2) & DecoderAlignedMUX1_dbg(link_i)(1) & DecoderAlignedMUX1_dbg(link_i)(0),
    --                        probe46     => DecoderAlignedBond_dbg(5) & DecoderAlignedBond_dbg(4) & DecoderAlignedBond_dbg(3) & DecoderAlignedBond_dbg(2) & DecoderAlignedBond_dbg(1) & DecoderAlignedBond_dbg(0),
    --                        probe47(0)  => LinkAligned(link_i),
    --                        probe48     => --rx_soft_err_i(link_i)(6) &
    --                                       rx_soft_err_i(link_i)(5) & rx_soft_err_i(link_i)(4) & rx_soft_err_i(link_i)(3) & rx_soft_err_i(link_i)(2) & rx_soft_err_i(link_i)(1) & rx_soft_err_i(link_i)(0),
    --                        probe49     => --DecoderDeskewed(link_i)(6) &
    --                                       DecoderDeskewed(link_i)(5) & DecoderDeskewed(link_i)(4) & DecoderDeskewed(link_i)(3) & DecoderDeskewed(link_i)(2) & DecoderDeskewed(link_i)(1) & DecoderDeskewed(link_i)(0),
    --                        probe50     => --rx_soft_err_cnt_reg(link_i,6) &
    --                                       rx_soft_err_cnt_reg(link_i,5) & rx_soft_err_cnt_reg(link_i,4) & rx_soft_err_cnt_reg(link_i,3) & rx_soft_err_cnt_reg(link_i,2) & rx_soft_err_cnt_reg(link_i,1) & rx_soft_err_cnt_reg(link_i,0)
    --                    );

    --                ila_pixellinklpgbt_2_i: entity work.ila_pixellinklpgbt_2
    --                    port map
    --                    (
    --                        clk         => clk160,
    --                        probe0      => EgroupDecData_clk160_dbg(link_i,0).valid & EgroupDecData_clk160_dbg(link_i,0).data, --array_data32VL_64b66b_type 1+1+32
    --                        probe1      => EgroupDecData_clk160_dbg(link_i,1).valid & EgroupDecData_clk160_dbg(link_i,1).data,
    --                        probe2      => EgroupDecData_clk160_dbg(link_i,2).valid & EgroupDecData_clk160_dbg(link_i,2).data,
    --                        probe3      => EgroupDecData_clk160_dbg(link_i,3).valid & EgroupDecData_clk160_dbg(link_i,3).data,
    --                        probe4      => EgroupDecData_clk160_dbg(link_i,4).valid & EgroupDecData_clk160_dbg(link_i,4).data,
    --                        probe5      => EgroupDecData_clk160_dbg(link_i,5).valid & EgroupDecData_clk160_dbg(link_i,5).data,
    --                        --probe6      => EgroupDecData_clk160_dbg(link_i,6).valid & EgroupDecData_clk160_dbg(link_i,6).data,
    --                        probe6      => EgroupMUX2Data_clk160_dbg(link_i,0).valid & EgroupMUX2Data_clk160_dbg(link_i,0).data, --array_data32VL_64b66b_type 1+1+32
    --                        probe7      => EgroupMUX2Data_clk160_dbg(link_i,1).valid & EgroupMUX2Data_clk160_dbg(link_i,1).data,
    --                        probe8      => EgroupMUX2Data_clk160_dbg(link_i,2).valid & EgroupMUX2Data_clk160_dbg(link_i,2).data,
    --                        probe9     => EgroupMUX2Data_clk160_dbg(link_i,3).valid & EgroupMUX2Data_clk160_dbg(link_i,3).data,
    --                        probe10     => EgroupMUX2Data_clk160_dbg(link_i,4).valid & EgroupMUX2Data_clk160_dbg(link_i,4).data,
    --                        probe11     => EgroupMUX2Data_clk160_dbg(link_i,5).valid & EgroupMUX2Data_clk160_dbg(link_i,5).data,
    --                        --probe13     => EgroupMUX2Data_clk160_dbg(link_i,6).valid & EgroupMUX2Data_clk160_dbg(link_i,6).data,
    --                        probe12     => EgroupAggrData_dbg(2).valid & EgroupAggrData_dbg(2).data & EgroupAggrData_dbg(1).valid & EgroupAggrData_dbg(1).data & EgroupAggrData_dbg(0).valid & EgroupAggrData_dbg(0).data,
    --                        probe13     => EgroupAggrData_dbg(5).valid & EgroupAggrData_dbg(5).data & EgroupAggrData_dbg(4).valid & EgroupAggrData_dbg(4).data & EgroupAggrData_dbg(3).valid & EgroupAggrData_dbg(3).data,
    --                        probe14     => EgroupData_clk160(link_i,0).tlast & EgroupData_clk160(link_i,0).valid & EgroupData_clk160(link_i,0).data, --array_data32VL_64b66b_type 1+1+32
    --                        probe15     => EgroupData_clk160(link_i,1).tlast & EgroupData_clk160(link_i,1).valid & EgroupData_clk160(link_i,1).data,
    --                        probe16     => EgroupData_clk160(link_i,2).tlast & EgroupData_clk160(link_i,2).valid & EgroupData_clk160(link_i,2).data,
    --                        probe17     => EgroupData_clk160(link_i,3).tlast & EgroupData_clk160(link_i,3).valid & EgroupData_clk160(link_i,3).data,
    --                        probe18     => EgroupData_clk160(link_i,4).tlast & EgroupData_clk160(link_i,4).valid & EgroupData_clk160(link_i,4).data,
    --                        probe19     => EgroupData_clk160(link_i,5).tlast & EgroupData_clk160(link_i,5).valid & EgroupData_clk160(link_i,5).data,
    --                        --probe22     => EgroupData_clk160(link_i,6).tlast & EgroupData_clk160(link_i,6).valid & EgroupData_clk160(link_i,6).data,
    --                        probe20     => DCSDecDataSplit_clk160_dbg(link_i,0).tlast & DCSDecDataSplit_clk160_dbg(link_i,0).valid & DCSDecDataSplit_clk160_dbg(link_i,0).data, --array_data32VL_64b66b_type 1+1+32
    --                        probe21     => DCSDecDataSplit_clk160_dbg(link_i,1).tlast & DCSDecDataSplit_clk160_dbg(link_i,1).valid & DCSDecDataSplit_clk160_dbg(link_i,1).data,
    --                        probe22     => DCSDecDataSplit_clk160_dbg(link_i,2).tlast & DCSDecDataSplit_clk160_dbg(link_i,2).valid & DCSDecDataSplit_clk160_dbg(link_i,2).data,
    --                        probe23     => DCSDecDataSplit_clk160_dbg(link_i,3).tlast & DCSDecDataSplit_clk160_dbg(link_i,3).valid & DCSDecDataSplit_clk160_dbg(link_i,3).data,
    --                        probe24     => DCSDecDataSplit_clk160_dbg(link_i,4).tlast & DCSDecDataSplit_clk160_dbg(link_i,4).valid & DCSDecDataSplit_clk160_dbg(link_i,4).data,
    --                        probe25     => DCSDecDataSplit_clk160_dbg(link_i,5).tlast & DCSDecDataSplit_clk160_dbg(link_i,5).valid & DCSDecDataSplit_clk160_dbg(link_i,5).data,
    --                        --probe29     => DCSDecDataSplit_clk160_dbg(link_i,6).tlast & DCSDecDataSplit_clk160_dbg(link_i,6).valid & DCSDecDataSplit_clk160_dbg(link_i,6).data,
    --                        probe26     => --DecoderAlignedDec_clk160_dbg(link_i)(6) &
    --                                       DecoderAlignedDec_clk160_dbg(link_i)(5) & DecoderAlignedDec_clk160_dbg(link_i)(4) & DecoderAlignedDec_clk160_dbg(link_i)(3) & DecoderAlignedDec_clk160_dbg(link_i)(2) & DecoderAlignedDec_clk160_dbg(link_i)(1) & DecoderAlignedDec_clk160_dbg(link_i)(0),
    --                        probe27     => cnt_rx_64b66bhdr_160_i(link_i),
    --                        probe28     => EgroupMUX2DataSplit_clk160_dbg(link_i,0).tlast & EgroupMUX2DataSplit_clk160_dbg(link_i,0).valid & EgroupMUX2DataSplit_clk160_dbg(link_i,0).data, --array_data32VL_64b66b_type 1+1+32
    --                        probe29     => EgroupMUX2DataSplit_clk160_dbg(link_i,1).tlast & EgroupMUX2DataSplit_clk160_dbg(link_i,1).valid & EgroupMUX2DataSplit_clk160_dbg(link_i,1).data,
    --                        probe30     => EgroupMUX2DataSplit_clk160_dbg(link_i,2).tlast & EgroupMUX2DataSplit_clk160_dbg(link_i,2).valid & EgroupMUX2DataSplit_clk160_dbg(link_i,2).data,
    --                        probe31     => EgroupMUX2DataSplit_clk160_dbg(link_i,3).tlast & EgroupMUX2DataSplit_clk160_dbg(link_i,3).valid & EgroupMUX2DataSplit_clk160_dbg(link_i,3).data,
    --                        probe32     => EgroupMUX2DataSplit_clk160_dbg(link_i,4).tlast & EgroupMUX2DataSplit_clk160_dbg(link_i,4).valid & EgroupMUX2DataSplit_clk160_dbg(link_i,4).data,
    --                        probe33     => EgroupMUX2DataSplit_clk160_dbg(link_i,5).tlast & EgroupMUX2DataSplit_clk160_dbg(link_i,5).valid & EgroupMUX2DataSplit_clk160_dbg(link_i,5).data
    --                        --probe38     => EgroupMUX2DataSplit_clk160_dbg(link_i,6).tlast & EgroupMUX2DataSplit_clk160_dbg(link_i,6).valid & EgroupMUX2DataSplit_clk160_dbg(link_i,6).data
    --                    );

    --                ila_pixellinklpgbt_3_i: entity work.ila_pixellinklpgbt_3
    --                    port map
    --                    (
    --                        clk         => m_axis_aclk,
    --                        probe0      => m_axis_dbg(link_i,0).tlast & m_axis_dbg(link_i,0).tvalid & m_axis_dbg(link_i,0).tdata,
    --                        probe1      => m_axis_dbg(link_i,4).tlast & m_axis_dbg(link_i,4).tvalid & m_axis_dbg(link_i,4).tdata,
    --                        probe2      => m_axis_dbg(link_i,8).tlast & m_axis_dbg(link_i,8).tvalid & m_axis_dbg(link_i,8).tdata,
    --                        probe3      => m_axis_dbg(link_i,12).tlast & m_axis_dbg(link_i,12).tvalid & m_axis_dbg(link_i,12).tdata,
    --                        probe4      => m_axis_dbg(link_i,16).tlast & m_axis_dbg(link_i,16).tvalid & m_axis_dbg(link_i,16).tdata,
    --                        probe5      => m_axis_dbg(link_i,20).tlast & m_axis_dbg(link_i,20).tvalid & m_axis_dbg(link_i,20).tdata,
    --                        --probe6      => m_axis_dbg(link_i,24).tlast & m_axis_dbg(link_i,24).tvalid & m_axis_dbg(link_i,24).tdata,
    --                        probe6      => m_axis_dbg(link_i,1).tlast & m_axis_dbg(link_i,1).tvalid & m_axis_dbg(link_i,1).tdata,
    --                        probe7      => m_axis_dbg(link_i,5).tlast & m_axis_dbg(link_i,5).tvalid & m_axis_dbg(link_i,5).tdata,
    --                        probe8      => m_axis_dbg(link_i,9).tlast & m_axis_dbg(link_i,9).tvalid & m_axis_dbg(link_i,9).tdata,
    --                        probe9      => m_axis_dbg(link_i,13).tlast & m_axis_dbg(link_i,13).tvalid & m_axis_dbg(link_i,13).tdata,
    --                        probe10     => m_axis_dbg(link_i,17).tlast & m_axis_dbg(link_i,17).tvalid & m_axis_dbg(link_i,17).tdata,
    --                        probe11     => m_axis_dbg(link_i,21).tlast & m_axis_dbg(link_i,21).tvalid & m_axis_dbg(link_i,21).tdata,
    --                        --probe13     => m_axis_dbg(link_i,25).tlast & m_axis_dbg(link_i,25).tvalid & m_axis_dbg(link_i,25).tdata,
    --                        probe12     => m_axis_tready_dbg(link_i,0) &  m_axis_tready_dbg(link_i,4) &  m_axis_tready_dbg(link_i,8) &  m_axis_tready_dbg(link_i,12) &  m_axis_tready_dbg(link_i,16) &  m_axis_tready_dbg(link_i,20),
    --                        -- & m_axis_tready_dbg(link_i,24),
    --                        probe13     => m_axis_tready_dbg(link_i,1) &  m_axis_tready_dbg(link_i,5) &  m_axis_tready_dbg(link_i,9) &  m_axis_tready_dbg(link_i,13) &  m_axis_tready_dbg(link_i,17) &  m_axis_tready_dbg(link_i,21),
    --                        -- & m_axis_tready_dbg(link_i,25),
    --                        probe14     => m_axis_prog_empty_dbg(link_i,0) &  m_axis_prog_empty_dbg(link_i,4) &  m_axis_prog_empty_dbg(link_i,8) &  m_axis_prog_empty_dbg(link_i,12) &  m_axis_prog_empty_dbg(link_i,16) &  m_axis_prog_empty_dbg(link_i,20),
    --                        -- & m_axis_prog_empty_dbg(link_i,24),
    --                        probe15     => m_axis_prog_empty_dbg(link_i,1) &  m_axis_prog_empty_dbg(link_i,5) &  m_axis_prog_empty_dbg(link_i,9) &  m_axis_prog_empty_dbg(link_i,13) &  m_axis_prog_empty_dbg(link_i,17) &  m_axis_prog_empty_dbg(link_i,21),
    --                        -- & m_axis_prog_empty_dbg(link_i,25),
    --                        probe16     => cnt_rx_maxishdr_i(link_i)
    --                    );
    --            end generate;
    end generate; --g_link_loop: for link in 0 to 1 generate

end Behavioral;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

-------------------------------------------------------
--! @file
--! @author Julian Mendez <julian.mendez@cern.ch> (CERN - EP-ESE-BE)
--! @date April, 2017
--! @version 1.0
--! @brief SCA control - EC Field deserializer
-------------------------------------------------------

--! Include the IEEE VHDL standard library
library ieee;
    --! Use STD_Logic to define vector types
    use ieee.std_logic_1164.all;
    --! Used to convert std_logic_vector to integer and manage fifo pointer
    use ieee.numeric_std.all;

--! @brief SCA_Deserializer Entity - EC Field deserializer
--! @details
--! The EC_Deserializer deserializes the data and removes the zeros
--! inserted as specified by the HDLC standard (bit stuffing).
--! Additionally, it detects the delimiter pattern to inform the
--! core when a new frame is received.
entity DecoderHDLC is
    generic (
        g_WORD_SIZE         : integer := 8;                                     --! Size of the words to be stored into the external FIFO
        g_DELIMITER         : std_logic_vector(7 downto 0) := "01111110";       --! Delimiter pattern
        g_IDLE              : std_logic_vector(7 downto 0) := "01111111"        --! IDLE pattern, "11111111" for IC, "01111111" for EC
    );
    port (
        clk40            : in  std_logic;                                    --! Rx clock (Rx_frameclk_o from GBT-FPGA IP): must be a multiple of the LHC frequency
        ena              : in  std_logic;                                    --! Rx clock enable signal must be used in case of multi-cycle path(clk40 > LHC frequency). By default: always enabled

        reset             : in  std_logic;                                    --! Reset the deserializer

        -- Data
        DataIn              : in  std_logic_vector(1 downto 0);                 --! (RX) Couple of bits from the GBT-Frame - EC line
        DataOut              : out std_logic_vector((g_WORD_SIZE-1) downto 0);   --! Deserialized/decoded data

        -- Control & status
        DataOutValid             : out std_logic;                                    --! Write request to the external FIFO
        GearboxValidOut          : out std_logic;
        EOP   : out std_logic;                                     --! Delimiter detected flag (new command received)
        TruncateHDLC : out std_logic;
        EnableTruncation : in std_logic --Enable truncation mechanism when a chunk is over 12 bytes
    );
end DecoderHDLC;

--! @brief SCA_Deserializer Architecture - SCA Field deserializer
--! @details
--! The SCA_Deserializer architecture describes the logic required to
--! deserialize the input and decode the "HDLC" frame. The decoding
--! consists in removing the '0' inserted every 5 consecutive high level bits
--! bits and detecting/removing the delimiter packets (SOF/EOF).
architecture behavioral of DecoderHDLC is
    signal delimiter_found : std_logic;
    signal DataValid : std_logic;
    signal DataOut_s : std_logic_vector(g_WORD_SIZE-1 downto 0);
    signal DataOut_p1 : std_logic_vector(g_WORD_SIZE-1 downto 0);
    signal ongoing_s, ongoing_p1 : std_logic;
    signal EOP_p0: std_logic;
    signal GearboxValidShift: std_logic_vector(3 downto 0);

begin                 --========####   Architecture Body   ####========--

    --! @brief Deserializer process: manages the decoding/deserialization
    --! @details
    --! The deserialization is based on a counter, used to control the DataValid
    --! signal (set to '1' when cnter value >= to the word size). The counter
    --! is reseted when a delimiter is received (to synchronize the system) and
    --! incremented when a valid bit is received (not a '0' to be removed).
    --! Finally, because of the size of the serial frame (2 bits and not only one),
    --! the logic of the process is duplicated once.
    --! An on-going signal is used to know wether the delimiter packet received
    --! is a start or end of frame.
    deserializer: process(clk40)

        variable reg                : std_logic_vector(g_WORD_SIZE-1 downto 0);
        variable reg_no_destuffing  : std_logic_vector(g_WORD_SIZE-1 downto 0);
        variable cnter              : integer range 0 to g_WORD_SIZE;
        variable ongoing            : std_logic;

    begin
        if rising_edge(clk40) then
            if reset = '1' then
                cnter := 0;
                reg := (others => '0');
                reg_no_destuffing := (others => '0');
                ongoing   := '0';
                DataValid <= '0';
                DataOut_s <= (others => '0');
                ongoing_s <= '0';
                delimiter_found <= '0';
            elsif ena = '1' then
                ongoing_s <= ongoing;
                DataValid             <= '0';
                delimiter_found    <= '0';

                -- Data(MSB)
                reg(g_WORD_SIZE-1 downto 0)     := DataIn(1) & reg(g_WORD_SIZE-1 downto 1);
                reg_no_destuffing   := DataIn(1) & reg_no_destuffing(g_WORD_SIZE-1 downto 1);

                if reg_no_destuffing(7 downto 1) = "0111110" then
                    reg(7 downto 0) := reg(6 downto 0) & '0';

                elsif ongoing  = '1' then
                    cnter           := cnter + 1;

                end if;

                if reg_no_destuffing(7 downto 0) = g_DELIMITER then
                    cnter               := 0;
                    ongoing             := '1';
                    delimiter_found   <= '1';

                elsif reg_no_destuffing(7 downto 0) = x"FF" or reg_no_destuffing(7 downto 0) = g_IDLE then
                    ongoing             := '0';

                end if;

                if cnter >= g_WORD_SIZE and ongoing  = '1' then
                    cnter               := 0;
                    DataOut_s              <= reg;
                    DataValid             <= '1';
                end if;

                -- Data(0)
                reg(g_WORD_SIZE-1 downto 0)        := DataIn(0) & reg(g_WORD_SIZE-1 downto 1);
                reg_no_destuffing   := DataIn(0) & reg_no_destuffing(g_WORD_SIZE-1 downto 1);

                if reg_no_destuffing(7 downto 1) = "0111110" then
                    reg(7 downto 0) := reg(6 downto 0) & '0';

                elsif ongoing  = '1' then
                    cnter           := cnter + 1;

                end if;

                if reg_no_destuffing(7 downto 0) = g_DELIMITER then
                    cnter               := 0;
                    ongoing             := '1';
                    delimiter_found   <= '1';

                elsif reg_no_destuffing(7 downto 0) = x"FF" or reg_no_destuffing(7 downto 0) = g_IDLE then
                    ongoing             := '0';

                end if;

                if cnter >= g_WORD_SIZE and ongoing  = '1' then
                    cnter               := 0;
                    DataOut_s              <= reg;
                    DataValid             <= '1';
                end if;
            end if;

        end if;

    end process;

    --Pipeline process to align EOP with last data byte (for axi stream).
    --Also implement truncation mechanism with a fixed maximum number of bytes of 12.

    pipeline: process(clk40)
        variable TruncateCnt: integer range 0 to 15;
        variable Truncate : std_logic;
    begin
        if rising_edge(clk40) then
            if reset = '1' then
                DataOut <= (others => '0');
                DataOut_p1 <= (others => '0');
                DataOutValid <= '0';
                EOP_p0 <= '0';
                EOP <= '0';
                ongoing_p1 <= '0';
                TruncateHDLC <= '0';
                Truncate := '0';
                GearboxValidShift <= "0000";
                TruncateCnt := 0;
            else
                DataOutValid <= '0';
                GearboxValidShift <= GearboxValidShift(2 downto 0) & GearboxValidShift(3);
                if DataValid = '1' or delimiter_found = '1' then
                    GearboxValidShift <= "0001";
                    DataOutValid <= ongoing_p1;
                    if TruncateCnt < 12 then
                        TruncateCnt := TruncateCnt + 1;
                    else
                        Truncate := EnableTruncation;
                    end if;
                    if delimiter_found = '1' then
                        TruncateCnt := 0;
                        Truncate := '0';
                    end if;

                    -- MW: if ongoing_p1 is not cleared when sending EOP there will be a 1-byte message "received" when a new delimiter is detected
                    ongoing_p1 <= ongoing_s and not delimiter_found;
                    EOP_p0 <= delimiter_found;
                    DataOut_p1 <= DataOut_s;
                    DataOut <= DataOut_p1;

                    TruncateHDLC <= Truncate;
                end if;
                EOP <= EOP_p0; --Pipeline 1 more, we will do the EOP alignment to tlast in ByteToAxiStream.

            end if;
        end if;
    end process;

    GearboxValidOut <= GearboxValidShift(0);

end behavioral;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Filiberto Bonini
--!               Marius Wensing
--!               mtrovato
--!               Elena Zhivun
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 06/06/2019 08:38:50 AM
-- Design Name:
-- Module Name: decoding - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use work.pcie_package.all;
    use work.lpgbtfpga_package.all;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.interlaken_package.slv_67_array;
    use work.interlaken_package.slv_16_array;
library xpm;
    use xpm.vcomponents.all;

entity decoding is
    generic (
        CARD_TYPE               : integer := 712;
        GBT_NUM                 : integer := 4; --Number of transceiver links
        FIRMWARE_MODE           : integer := 0;
        STREAMS_TOHOST          : integer := 1;           --Number of E-links (1 for FULL mode)
        BLOCKSIZE               : integer := 1024;
        LOCK_PERIOD             : integer := 20480; --Maximum chunk size of 2048 bytes on slowest E-link supported, used for locking 8b10b decoders
        IncludeDecodingEpath2_HDLC      : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath2_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath4_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath8_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath16_8b10b    : std_logic_vector(6 downto 0) := "0000000"; --lpGBT only -- @suppress "Unused generic: IncludeDecodingEpath16_8b10b is not used in work.decoding(Behavioral)"
        IncludeDecodingEpath32_8b10b    : std_logic_vector(6 downto 0) := "0000000";  --lpGBT only -- @suppress "Unused generic: IncludeDecodingEpath32_8b10b is not used in work.decoding(Behavioral)"
        IncludeDirectDecoding           : std_logic_vector(6 downto 0) := "0000000";
        RD53Version             : String := "B"; --A or B
        PCIE_ENDPOINT  : integer := 0;
        VERSAL : boolean := false;
        AddFULLMODEForDUNE : boolean := false

    );
    Port (
        RXUSRCLK                       : in  std_logic_vector(GBT_NUM-1 downto 0); --Data clock for FULL mode
        FULL_UPLINK_USER_DATA          : in  array_33b(0 to GBT_NUM-1); --Full mode data input
        GBT_UPLINK_USER_DATA           : in  array_120b(0 to GBT_NUM-1);   --GBT data input
        lpGBT_UPLINK_USER_DATA         : in  array_224b(0 to GBT_NUM-1); --lpGBT data input -- @suppress "Unused port: lpGBT_UPLINK_USER_DATA is not used in work.decoding(Behavioral)"
        lpGBT_UPLINK_EC_DATA           : in  array_2b(0 to GBT_NUM-1);   --lpGBT EC data input
        lpGBT_UPLINK_IC_DATA           : in  array_2b(0 to GBT_NUM-1);   --lpGBT IC data input
        LinkAligned                    : in  std_logic_vector(GBT_NUM-1 downto 0); --Transceiver aligned
        clk160                         : in  std_logic; -- Used for driving aclk and internal processing
        clk240                         : in  std_logic; -- Used for driving aclk and internal processing
        clk250                         : in  std_logic; -- Used for driving aclk and internal processing
        clk40                          : in  std_logic; -- LHC BC Clock
        clk365                         : in  std_logic; -- To drive AXIs64 clock, for logic on 25Gb/s interfaces
        aclk_out                       : out std_logic; -- Driven by decoding
        daq_reset                      : in  std_logic; -- Active high reset
        daq_fifo_flush                 : in  std_logic; -- Active high fifo flush, guaranteed to occur when clocks are stable.
        m_axis                         : out axis_32_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1); --Towards CRToHost
        m_axis_tready                  : in  axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1); --From CRToHost
        m_axis_prog_empty              : out axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1); --From CRToHost
        m_axis_noSC                    : out axis_32_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1);     --Replication of axi stream for DUNE (no superchunk)
        m_axis_noSC_tready             : in  axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1); --Replication of axi stream for DUNE (no superchunk)
        m_axis_noSC_prog_empty         : out axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1); --Replication of axi stream for DUNE (no superchunk)
        TTC_ToHost_Data_in             : in TTC_data_type;
        TTCin                          : in TTC_data_type;
        FE_BUSY_out                    : out array_57b(0 to GBT_NUM-1) := (others => (others => '0'));
        ElinkBusyIn                    : in array_57b(0 to GBT_NUM-1);
        DmaBusyIn                      : in std_logic;
        FifoBusyIn                     : in std_logic;
        BusySumIn                      : in std_logic;
        m_axis_aux                     : out axis_32_array_type(0 to 1);
        m_axis_aux_prog_empty          : out axis_tready_array_type(0 to 1);
        m_axis_aux_tready              : in axis_tready_array_type(0 to 1);
        register_map_control           : in  register_map_control_type; --Settings (From Wupper)
        register_map_decoding_monitor  : out register_map_decoding_monitor_type;
        Interlaken_RX_Data_In          : in  slv_67_array(0 to GBT_NUM - 1);
        Interlaken_RX_Datavalid        : in  std_logic_vector(GBT_NUM - 1 downto 0);
        Interlaken_RX_Gearboxslip      : out std_logic_vector(GBT_NUM - 1 downto 0);
        Interlaken_Decoder_Aligned_out : out  std_logic_vector(GBT_NUM - 1 downto 0);
        m_axis64                       : out axis_64_array_type(0 to GBT_NUM - 1);
        m_axis64_tready                : in  axis_tready_array_type(0 to GBT_NUM - 1);
        m_axis64_prog_empty            : out axis_tready_array_type(0 to GBT_NUM - 1);
        toHost_axis64_aclk_out         : out std_logic
    --temporary. cbopt will be via reg
    --        CBOPT : in std_logic_vector(3 downto 0);
    --        DIS_LANE_IN                          : in std_logic_vector(3 downto 0);
    --        mask_k_char         : in std_logic_vector(0 to 3)
    );

    signal tick_counter : unsigned(5 downto 0) := (others => '0');
    signal tick_1us : std_logic;

end decoding;



architecture Behavioral of decoding is

    signal aclk_s : std_logic;
    type RealignmentEvent_type is array(0 to GBT_NUM-1) of std_logic_vector(STREAMS_TOHOST-1 downto 0);
    signal RealignmentEvent: RealignmentEvent_type;
begin

    -- Centralised counter to reduce number of counter bits used in each e-group block
    prc_tick_1us : process(clk40)
    begin
        if rising_edge(clk40) then
            tick_1us <= '0';
            tick_counter <= tick_counter - 1;
            if (tick_counter = 0) then
                tick_1us <= '1';
                tick_counter <= "100111"; --39
            end if;
        end if;
    end process;


    aclk_out <= aclk_s;

    g_aclk_160: if TOHOST_AXI_STREAM_FREQ(FIRMWARE_MODE) = 160 generate
        aclk_s <= clk160;
    elsif  TOHOST_AXI_STREAM_FREQ(FIRMWARE_MODE) = 240 generate
        aclk_s <= clk240;
    elsif  TOHOST_AXI_STREAM_FREQ(FIRMWARE_MODE) = 250 generate
        aclk_s <= clk250;
    else generate
        error_proc: process
        begin
            report "Unable to calculate the correct ToHost AXI Stream clock frequency based on FIRMWARE_MODE" severity error;
            wait;
        end process;
    end generate;

    --For all firmware modes, there will be a BUSY virtual E-link and a TTCToHost virtual E-link:
    TTCToHostVirtualElink0: entity work.TTCToHostVirtualElink
        generic map(
            BLOCKSIZE => BLOCKSIZE,
            VERSAL => VERSAL
        )
        port map(
            clk40                => clk40,
            daq_reset            => daq_reset,
            daq_fifo_flush       => daq_fifo_flush,
            TTC_ToHost_Data_in   => TTC_ToHost_Data_in,
            Enable               => register_map_control.TTC_TOHOST_ENABLE(0),
            IncludeAsyncUserData => register_map_control.TTC_TOHOST_CONTROL.INCLUDE_ASYNC_USER_DATA(0),
            IncludeSyncUserData => register_map_control.TTC_TOHOST_CONTROL.INCLUDE_SYNC_USER_DATA(1),
            IncludeSyncGlobalData => register_map_control.TTC_TOHOST_CONTROL.INCLUDE_SYNC_GLOBAL_DATA(2),
            m_axis               => m_axis_aux(0),
            m_axis_prog_empty    => m_axis_aux_prog_empty(0),
            m_axis_tready        => m_axis_aux_tready(0),
            m_axis_aclk          => aclk_s
        );


    BusyVirtualElink0: entity work.BusyVirtualElink
        generic map(
            GBT_NUM => GBT_NUM,
            BLOCKSIZE => BLOCKSIZE
        )
        port map(
            clk => clk40,
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            Orbit => TTC_ToHost_Data_in.OrbitId,
            BCID => TTC_ToHost_Data_in.BCID,
            ElinkBusyIn => ElinkBusyIn,
            SoftBusyIn => register_map_control.TTC_DEC_CTRL.MASTER_BUSY(0),
            DmaBusyIn => DmaBusyIn,
            FifoBusyIn => FifoBusyIn,
            BusySumIn => BusySumIn,
            Enable => register_map_control.BUSY_TOHOST_ENABLE(0),
            m_axis => m_axis_aux(1),
            m_axis_prog_empty => m_axis_aux_prog_empty(1),
            m_axis_tready => m_axis_aux_tready(1),
            m_axis_aclk => aclk_s
        );

    g_realignment_counters: for link in 0 to GBT_NUM-1 generate
        signal ELINK_REALIGNMENT_STATUS: std_logic_vector(STREAMS_TOHOST-1 downto 0);
        signal ELINK_REALIGNMENT_COUNTER: std_logic_vector(31 downto 0);
    begin
        realignment_cnt_proc: process(clk40)
            variable IncrementCounter: std_logic;
        begin
            if rising_edge(clk40) then
                if daq_reset = '1' or to_sl(register_map_control.ELINK_REALIGNMENT.CLEAR_REALIGNMENT_STATUS) = '1' then
                    ELINK_REALIGNMENT_STATUS <= (others => '0');
                    ELINK_REALIGNMENT_COUNTER <= (others => '0');
                else
                    IncrementCounter := '0';
                    for i in 0 to STREAMS_TOHOST-1 loop
                        if RealignmentEvent(link)(i) = '1' then
                            IncrementCounter := '1';
                            ELINK_REALIGNMENT_STATUS(i) <= '1';
                        end if;
                    end loop;
                    if IncrementCounter = '1' then
                        ELINK_REALIGNMENT_COUNTER <= ELINK_REALIGNMENT_COUNTER + 1;
                    end if;
                end if;
            end if;
        end process;
        g_lt12: if link < 12 generate
            register_map_decoding_monitor.ELINK_REALIGNMENT_STATUS(link)(STREAMS_TOHOST-1 downto 0) <= ELINK_REALIGNMENT_STATUS;
            register_map_decoding_monitor.ELINK_REALIGNMENT_COUNT(link) <= ELINK_REALIGNMENT_COUNTER;
        end generate;
    end generate g_realignment_counters;


    g_gbtmode: if FIRMWARE_MODE = FIRMWARE_MODE_GBT or
                  FIRMWARE_MODE = FIRMWARE_MODE_LTDB or
                  FIRMWARE_MODE = FIRMWARE_MODE_FEI4
                  generate
        signal AlignmentPulseAlign: std_logic;
        signal AlignmentPulseDeAlign: std_logic;
    begin
        g_GBT_Links: for link in 0 to GBT_NUM-1 generate
            signal EpathEnableAUX, EpathEnableEC, EpathEnableIC : std_logic;
            signal BitSwappingAUX, BitSwappingEC, BitSwappingIC : std_logic;
            signal MsbFirst : std_logic;
        begin

            --The aligned status bits have more bits than we need, to account for wide mode etc.
            register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(57 downto STREAMS_TOHOST) <= (others => '0');


            EpathEnableEC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).EC_ENABLE);
            EpathEnableIC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_ENABLE);
            EpathEnableAUX <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).AUX_ENABLE);
            BitSwappingEC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).EC_BIT_SWAPPING);
            --! The IC bits in the GBTx ASIC are reversed with respect to the other HDLC E-Links (e.g. EC channel), See FLX-2243
            BitSwappingIC <= not to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_BIT_SWAPPING);
            BitSwappingAUX <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).AUX_BIT_SWAPPING);
            MsbFirst <= to_sl(register_map_control.DECODING_REVERSE_10B);

            EpathEC: entity work.DecodingEpathGBT
                generic map(
                    MAX_INPUT      => 2,
                    INCLUDE_16b    => '0',
                    INCLUDE_8b     => '0',
                    INCLUDE_4b     => '0',
                    INCLUDE_2b     => '1',
                    INCLUDE_8b10b  => '1',
                    INCLUDE_HDLC   => '1',
                    INCLUDE_DIRECT => '0',
                    BLOCKSIZE      => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '1',
                    GENERATE_FEI4B => false,
                    VERSAL => VERSAL
                )
                port map(
                    clk40 => clk40,
                    daq_reset => daq_reset,
                    daq_fifo_flush => daq_fifo_flush,
                    EpathEnable => EpathEnableEC,
                    EpathEncoding => register_map_control.MINI_EGROUP_TOHOST(link).EC_ENCODING,
                    ElinkWidth => "000",
                    MsbFirst   => MsbFirst,
                    ReverseInputBits => BitSwappingEC,
                    EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_EC_TRUNCATION),
                    ElinkData  => GBT_UPLINK_USER_DATA(link)(113 downto 112),
                    ElinkAligned => LinkAligned(link),
                    DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-2),
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                    RealignmentEvent => RealignmentEvent(link)(STREAMS_TOHOST-2),
                    FE_BUSY_out => open,
                    m_axis => m_axis(link,STREAMS_TOHOST-2),
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-2),
                    m_axis_aclk => aclk_s,
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-2)
                );

            EpathIC: entity work.DecodingEpathGBT
                generic map(
                    MAX_INPUT      => 2,
                    INCLUDE_16b    => '0',
                    INCLUDE_8b     => '0',
                    INCLUDE_4b     => '0',
                    INCLUDE_2b     => '1',
                    INCLUDE_8b10b  => '1',
                    INCLUDE_HDLC   => '1',
                    INCLUDE_DIRECT => '0',
                    BLOCKSIZE      => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '1',
                    GENERATE_FEI4B => false,
                    VERSAL => VERSAL
                )
                port map(
                    clk40 => clk40,
                    daq_reset => daq_reset,
                    daq_fifo_flush => daq_fifo_flush,
                    EpathEnable => EpathEnableIC,
                    EpathEncoding => "0010", --Only support HDLC
                    ElinkWidth => "000",
                    MsbFirst   => MsbFirst,
                    ReverseInputBits => BitSwappingIC,
                    EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_IC_TRUNCATION),
                    ElinkData  => GBT_UPLINK_USER_DATA(link)(115 downto 114),
                    ElinkAligned => LinkAligned(link),
                    DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-1),
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                    RealignmentEvent => RealignmentEvent(link)(STREAMS_TOHOST-1),
                    FE_BUSY_out => open,
                    m_axis => m_axis(link,STREAMS_TOHOST-1),
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-1),
                    m_axis_aclk => aclk_s,
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-1)
                );

            g_ltdb: if FIRMWARE_MODE = FIRMWARE_MODE_LTDB generate
                g_ltdb_prog_empty: for i in 0 to STREAMS_TOHOST-4 generate
                    m_axis_prog_empty(link,i) <= '1';
                    m_axis(link,i) <= (
                        tdata => x"0000_0000",
                        tvalid => '0',
                        tlast => '0',
                        tkeep => "0000",
                        tuser  => "0000",
                        tid => "00000000"
                    );
                end generate;
                EpathAUX: entity work.DecodingEpathGBT
                    generic map(
                        MAX_INPUT      => 2,
                        INCLUDE_16b    => '0',
                        INCLUDE_8b     => '0',
                        INCLUDE_4b     => '0',
                        INCLUDE_2b     => '1',
                        INCLUDE_8b10b  => '1',
                        INCLUDE_HDLC   => '1',
                        INCLUDE_DIRECT => '0',
                        BLOCKSIZE      => BLOCKSIZE,
                        USE_BUILT_IN_FIFO => '1',
                        GENERATE_FEI4B => false,
                        VERSAL => VERSAL
                    )
                    port map(
                        clk40 => clk40,
                        daq_reset => daq_reset,
                        daq_fifo_flush => daq_fifo_flush,
                        EpathEnable => EpathEnableAUX,
                        EpathEncoding => "0010", --Only HDLC supported
                        ElinkWidth => "000",
                        MsbFirst   => MsbFirst,
                        ReverseInputBits => BitSwappingAUX,
                        EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_AUX_TRUNCATION),
                        ElinkData  => GBT_UPLINK_USER_DATA(link)(111 downto 110),
                        ElinkAligned => LinkAligned(link),
                        DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-3),
                        AlignmentPulseAlign => AlignmentPulseAlign,
                        AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                        AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                        RealignmentEvent => RealignmentEvent(link)(STREAMS_TOHOST-3),
                        FE_BUSY_out => open,
                        m_axis => m_axis(link,STREAMS_TOHOST-3),
                        m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-3),
                        m_axis_aclk => aclk_s,
                        m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-3)
                    );
            end generate;

            g_noltdb: if FIRMWARE_MODE /= FIRMWARE_MODE_LTDB and
                         FIRMWARE_MODE /= FIRMWARE_MODE_PIXEL generate
                g_Egroups: for egroup in 0 to 4 generate
                    signal ElinkWidth: std_logic_vector(2 downto 0);
                    signal PathEnable: std_logic_vector(7 downto 0);
                    signal PathEncoding : std_logic_vector(31 downto 0);
                    signal ReverseElinks : std_logic_vector(7 downto 0);
                    signal m_axis_s : axis_32_array_type(0 to 7);
                    signal m_axis_tready_s : axis_tready_array_type(0 to 7);
                    signal m_axis_prog_empty_s : axis_tready_array_type(0 to 7);
                    signal EnableEgroupTruncation : std_logic;
                    signal DecoderAligned: std_logic_vector(7 downto 0);
                begin
                    --! Map axis 32 bus for 1 E-group to the complete 2D array.
                    g_axisindex: for i in 0 to 7 generate
                        m_axis(link,egroup*8+i) <= m_axis_s(i);
                        m_axis_tready_s(i) <= m_axis_tready(link,egroup*8+i);
                        m_axis_prog_empty(link,egroup*8+i) <= m_axis_prog_empty_s(i);
                    end generate;

                    PathEncoding <= register_map_control.DECODING_EGROUP_CTRL(link mod 12)(egroup).PATH_ENCODING;
                    ReverseElinks <= register_map_control.DECODING_EGROUP_CTRL(link mod 12)(egroup).REVERSE_ELINKS;
                    ElinkWidth <= register_map_control.DECODING_EGROUP_CTRL(link mod 12)(egroup).EPATH_WIDTH;
                    PathEnable <= register_map_control.DECODING_EGROUP_CTRL(link mod 12)(egroup).EPATH_ENA;
                    EnableEgroupTruncation <= to_sl(register_map_control.DECODING_EGROUP_CTRL(link mod 12)(egroup).ENABLE_TRUNCATION);

                    --! Instantiate one Egroup.
                    eGroup0: entity work.DecodingEgroupGBT
                        generic map(
                            INCLUDE_16b    => IncludeDecodingEpath16_8b10b(egroup),
                            INCLUDE_8b     => IncludeDecodingEpath8_8b10b(egroup),
                            INCLUDE_4b     => IncludeDecodingEpath4_8b10b(egroup),
                            INCLUDE_2b     => IncludeDecodingEpath2_8b10b(egroup) or IncludeDecodingEpath2_HDLC(egroup),
                            INCLUDE_8b10b  => IncludeDecodingEpath2_8b10b(egroup) or IncludeDecodingEpath4_8b10b(egroup) or IncludeDecodingEpath8_8b10b(egroup),
                            INCLUDE_HDLC   => IncludeDecodingEpath2_HDLC(egroup),
                            INCLUDE_DIRECT => IncludeDirectDecoding(egroup),
                            BLOCKSIZE      => BLOCKSIZE,
                            USE_BUILT_IN_FIFO => x"AA",
                            GENERATE_FEI4B => (FIRMWARE_MODE = FIRMWARE_MODE_FEI4),
                            VERSAL => VERSAL
                        --LOCK_PERIOD    => LOCK_PERIOD
                        )
                        port map(
                            clk40 => clk40,
                            daq_reset => daq_reset,
                            daq_fifo_flush => daq_fifo_flush,

                            EpathEnable => PathEnable,
                            EpathEncoding => PathEncoding,
                            ElinkWidth => ElinkWidth,
                            MsbFirst         => MsbFirst,
                            ReverseInputBits => ReverseElinks,
                            EnableTruncation => EnableEgroupTruncation,
                            DecoderAligned => DecoderAligned,

                            EGroupData => GBT_UPLINK_USER_DATA(link)((2+egroup)*16+15 downto (2+egroup)*16),
                            GBTAligned => LinkAligned(link),

                            FE_BUSY_out => FE_BUSY_out(link)(8*egroup+7 downto 8*egroup),
                            m_axis => m_axis_s,
                            m_axis_tready => m_axis_tready_s,
                            m_axis_aclk => aclk_s,
                            m_axis_prog_empty => m_axis_prog_empty_s,
                            AlignmentPulseAlign => AlignmentPulseAlign,
                            AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                            AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                            RealignmentEvent => RealignmentEvent(link)(egroup*8+7 downto egroup*8)

                        );
                    register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(egroup*8+7 downto egroup*8) <= DecoderAligned;
                end generate;
            end generate;
        end generate;

        pulsegen0: entity work.AlignmentPulseGen
            generic map(
                MAX_VAL_DEALIGN => LOCK_PERIOD, --2048 bytes at 8b10b / 80 Mb/s elink.
                MAX_VAL_ALIGN => 20 --2 bytes at 8b10b / 80 Mb/s elink.
            )
            port map(
                clk40 => clk40,
                AlignmentPulseAlign => AlignmentPulseAlign,
                AlignmentPulseDeAlign => AlignmentPulseDeAlign
            );
    end generate;

    g_pixelmode: if FIRMWARE_MODE = FIRMWARE_MODE_PIXEL generate
        signal AlignmentPulseAlign: std_logic;
        signal AlignmentPulseDeAlign: std_logic;

        signal MsbFirst : std_logic;
    begin

        MsbFirst <= to_sl(register_map_control.DECODING_REVERSE_10B);
        g_LPGBT_Links: for link in 0 to GBT_NUM-1 generate
            signal EpathEnableEC, EpathEnableIC : std_logic;
            signal BitSwappingEC, BitSwappingIC : std_logic;
        begin
            --TODO: Include TTC ToHost virtual E-link for link=0
            --TODO: Include XOFF/BUSY virtual E-link for link=0
            EpathEnableEC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).EC_ENABLE);
            EpathEnableIC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_ENABLE);
            BitSwappingEC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).EC_BIT_SWAPPING);
            BitSwappingIC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_BIT_SWAPPING);

            EpathEC: entity work.DecodingEpathGBT
                generic map(
                    MAX_INPUT      => 2,
                    INCLUDE_16b    => '0',
                    INCLUDE_8b     => '0',
                    INCLUDE_4b     => '0',
                    INCLUDE_2b     => '1',
                    INCLUDE_8b10b  => '1',
                    INCLUDE_HDLC   => '1',
                    INCLUDE_DIRECT => '0',
                    BLOCKSIZE      => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '1',
                    GENERATE_FEI4B => false,
                    VERSAL => VERSAL
                )
                port map(
                    clk40 => clk40,
                    daq_reset => daq_reset,
                    daq_fifo_flush => daq_fifo_flush,
                    EpathEnable => EpathEnableEC,
                    EpathEncoding => register_map_control.MINI_EGROUP_TOHOST(link).EC_ENCODING,
                    ElinkWidth => "000",
                    MsbFirst   => MsbFirst,
                    ReverseInputBits => BitSwappingEC,
                    EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_EC_TRUNCATION),
                    ElinkData  => lpGBT_UPLINK_EC_DATA(link),
                    ElinkAligned => LinkAligned(link),
                    DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-2),
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                    RealignmentEvent => RealignmentEvent(link)(STREAMS_TOHOST-2),
                    FE_BUSY_out => open,
                    m_axis => m_axis(link,STREAMS_TOHOST-2),
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-2),
                    m_axis_aclk => aclk_s,
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-2)
                );

            EpathIC: entity work.DecodingEpathGBT
                generic map(
                    MAX_INPUT      => 2,
                    INCLUDE_16b    => '0',
                    INCLUDE_8b     => '0',
                    INCLUDE_4b     => '0',
                    INCLUDE_2b     => '1',
                    INCLUDE_8b10b  => '0',
                    INCLUDE_HDLC   => '1',
                    INCLUDE_DIRECT => '0',
                    BLOCKSIZE      => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '1',
                    GENERATE_FEI4B => false,
                    VERSAL => VERSAL
                )
                port map(
                    clk40 => clk40,
                    daq_reset => daq_reset,
                    daq_fifo_flush => daq_fifo_flush,
                    EpathEnable => EpathEnableIC,
                    EpathEncoding => "0010", --Only support HDLC
                    ElinkWidth => "000",
                    MsbFirst   => MsbFirst,
                    ReverseInputBits => BitSwappingIC,
                    EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_IC_TRUNCATION),
                    ElinkData  => lpGBT_UPLINK_IC_DATA(link),
                    ElinkAligned => LinkAligned(link),
                    DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-1),
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                    RealignmentEvent => RealignmentEvent(link)(STREAMS_TOHOST-1),
                    FE_BUSY_out => open,
                    m_axis => m_axis(link,STREAMS_TOHOST-1),
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-1),
                    m_axis_aclk => aclk_s,
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-1)
                );
        end generate; -- g_LPGBT_Links

        g_LPGBT_Links_Decoding: for link in 0 to (GBT_NUM)/2-1 generate
            signal m_axis_s             : axis_32_2d_array_type(0 to 1, 0 to STREAMS_TOHOST-3);
            signal m_axis_tready_s      : axis_tready_2d_array_type(0 to 1, 0 to STREAMS_TOHOST-3);
            signal m_axis_prog_empty_s  : axis_tready_2d_array_type(0 to 1, 0 to STREAMS_TOHOST-3);

            signal DecoderAligned       : std_logic_vector(2*(STREAMS_TOHOST-2)-1 downto 0);
            signal DecoderDeskewed      : std_logic_vector(2*(STREAMS_TOHOST-2)-1 downto 0);

            signal cnt_rx_64b66bhdr_i   : array_32b(0 to 1);--std_logic_vector(31 downto 0);
            signal rx_soft_err_cnt_i    : array_2d_32b(0 to 1, 0 to 5);--array_32b(0 to 6);
            signal rx_soft_err_rst      : std_logic_vector(0 to 1);
        begin
            --check it against the unchanged version of decoding to see if I
            --screwed up something
            g_LPGBT_twolinks: for j in 0 to 1 generate --j represents the two links being decoded
                signal SoftErrorEgroupIndex : integer range 0 to 5;
            begin
                g_LPGBT_egroup: for egroup in 0 to 5 generate
                begin
                    --! Map axis 32 bus for 1 E-group to the complete 2D array.
                    g_axisindex: for i in 0 to 3 generate
                        m_axis(2*link+j,egroup*4+i)             <= m_axis_s(j,egroup*4+i);
                        m_axis_tready_s(j,egroup*4+i)           <= m_axis_tready(2*link+j,egroup*4+i);
                        m_axis_prog_empty(2*link+j,egroup*4+i)  <= m_axis_prog_empty_s(j,egroup*4+i);
                    end generate;--i
                end generate;--egroup

                register_map_decoding_monitor.DECODING_LINK_ALIGNED(2*link+j)(STREAMS_TOHOST-3 downto 0)        <= DecoderAligned((j+1)*(STREAMS_TOHOST-2)-1 downto j*(STREAMS_TOHOST-2)); --55 downto 28 when j=1, 27 downto 0 when j=0
                register_map_decoding_monitor.DECODING_LINK_ALIGNED(2*link+j)(57 downto STREAMS_TOHOST)         <= (others => '0');
                register_map_decoding_monitor.DECODING_LINK_ALIGNED(2*link+j)(STREAMS_TOHOST-3 downto 0)        <= DecoderAligned((j+1)*(STREAMS_TOHOST-2)-1 downto j*(STREAMS_TOHOST-2)); --55 downto 28 when j=1, 27 downto 0 when j=0
                g_limit6: if link < 6 generate --6 instead of 12
                    register_map_decoding_monitor.DECODING_LINK_CB(2*link+j).DESKEWED(61 downto STREAMS_TOHOST+4)   <= (others => '0');
                    register_map_decoding_monitor.YARR_DEBUG_ALLEGROUP_TOHOST(2*link+j).CNT_RX_PACKET               <= cnt_rx_64b66bhdr_i(j);
                    register_map_decoding_monitor.DECODING_LINK_CB(2*link+j).DESKEWED(STREAMS_TOHOST+1 downto 4)    <= DecoderDeskewed((j+1)*(STREAMS_TOHOST-2)-1 downto j*(STREAMS_TOHOST-2));
                end generate;
                SoftErrorEgroupIndex                                            <= to_integer(unsigned(register_map_control.LINK_ERRORS(2*link+j).EGROUP_SELECT));
                register_map_decoding_monitor.LINK_ERRORS(2*link+j).COUNT       <= rx_soft_err_cnt_i(j,SoftErrorEgroupIndex);
                rx_soft_err_rst(j)                                              <= register_map_control.LINK_ERRORS(2*link+j).CLEAR_COUNTERS(register_map_control.LINK_ERRORS(2*link+j).CLEAR_COUNTERS'low);
            end generate;--j

            Link0: entity work.DecodingPixelLinkLPGBT
                generic map(
                    CARD_TYPE => CARD_TYPE,
                    RD53Version    => RD53Version,
                    BLOCKSIZE => BLOCKSIZE,
                    LINK => (2*link+0,2*link+1),
                    SIMU => 0,
                    STREAMS_TOHOST => STREAMS_TOHOST,
                    PCIE_ENDPOINT => PCIE_ENDPOINT,
                    VERSAL         => VERSAL
                )
                port map(
                    clk40                         => clk40,                                             --in
                    clk160                        => clk160,
                    daq_reset                     => daq_reset,
                    daq_fifo_flush                => daq_fifo_flush,
                    MsbFirst                      => MsbFirst,                                          --in
                    LinkData                      => lpGBT_UPLINK_USER_DATA(2*link+0)(223 downto 0) &   --in    link done
                                                     lpGBT_UPLINK_USER_DATA(2*link+1)(223 downto 0),
                    LinkAligned                   => LinkAligned(2*link+0) & LinkAligned(2*link+1),     --in    link done
                    m_axis                        => m_axis_s,                                          --out   link done
                    m_axis_tready                 => m_axis_tready_s,                                   --in    link done
                    m_axis_aclk                   => aclk_s,                                            --in
                    m_axis_prog_empty             => m_axis_prog_empty_s,                               --out   link done
                    register_map_control          => register_map_control,                              --in
                    --register_map_decoding_monitor => register_map_decoding_monitor
                    DecoderAligned_out            => DecoderAligned,                                    --out link
                    DecoderDeskewed_out           => DecoderDeskewed,                                   --out link
                    cnt_rx_64b66bhdr_out          => cnt_rx_64b66bhdr_i,                                --out link
                    rx_soft_err_cnt_out           => rx_soft_err_cnt_i,                                  --out link
                    rx_soft_err_rst               => rx_soft_err_rst
                );

        end generate; -- g_LPGBT_Links_Decoding

        pulsegen0: entity work.AlignmentPulseGen
            generic map(
                MAX_VAL_DEALIGN => LOCK_PERIOD, --2048 bytes at 8b10b / 80 Mb/s elink.
                MAX_VAL_ALIGN => 20 --2048 bytes at 8b10b / 80 Mb/s elink.
            )
            port map(
                clk40 => clk40,
                AlignmentPulseAlign => AlignmentPulseAlign,
                AlignmentPulseDeAlign => AlignmentPulseDeAlign
            );

    end generate; --g_lpgbtmode


    g_bcmmode: if FIRMWARE_MODE = FIRMWARE_MODE_BCM_PRIME generate
        signal AlignmentPulseAlign: std_logic;
        signal AlignmentPulseDeAlign: std_logic;
        constant NUM_ELINK : integer := ((STREAMS_TOHOST-2)/4);
    begin
        g_LPGBT_Links: for link in 0 to GBT_NUM-1 generate
            signal EpathEnableEC, EpathEnableIC : std_logic;
            signal BitSwappingEC, BitSwappingIC : std_logic;
            signal m_axis_s : axis_32_array_type(0 to STREAMS_TOHOST-3);
            signal m_axis_tready_s : axis_tready_array_type(0 to STREAMS_TOHOST-3);
            signal m_axis_prog_empty_s : axis_tready_array_type(0 to STREAMS_TOHOST-3);
            signal MsbFirst : std_logic;
        begin

            -- TODO: ADD Register Asigments

            --The aligned status bits have more bits than we need, to account for wide mode etc.
            -- register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(57 downto STREAMS_TOHOST) <= (others => '0');
            -- register_map_decoding_monitor.DECODING_LINK_CB(link).DESKEWED(61 downto STREAMS_TOHOST+4) <= (others => '0');
            -- register_map_decoding_monitor.YARR_DEBUG_ALLEGROUP_TOHOST(link).CNT_RX_PACKET <= cnt_rx_64b66bhdr_i;
            EpathEnableEC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).EC_ENABLE);
            EpathEnableIC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_ENABLE);
            BitSwappingEC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).EC_BIT_SWAPPING);
            BitSwappingIC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_BIT_SWAPPING);
            MsbFirst <= to_sl(register_map_control.DECODING_REVERSE_10B);

            EpathEC: entity work.DecodingEpathGBT
                generic map(
                    MAX_INPUT      => 2,
                    INCLUDE_16b    => '0',
                    INCLUDE_8b     => '0',
                    INCLUDE_4b     => '0',
                    INCLUDE_2b     => '1',
                    INCLUDE_8b10b  => '1',
                    INCLUDE_HDLC   => '1',
                    INCLUDE_DIRECT => '0',
                    BLOCKSIZE      => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '1',
                    GENERATE_FEI4B => false,
                    VERSAL => VERSAL
                )
                port map(
                    clk40 => clk40,
                    daq_reset => daq_reset,
                    daq_fifo_flush => daq_fifo_flush,
                    EpathEnable => EpathEnableEC,
                    EpathEncoding => register_map_control.MINI_EGROUP_TOHOST(link).EC_ENCODING,
                    ElinkWidth => "000",
                    MsbFirst   => MsbFirst,
                    ReverseInputBits => BitSwappingEC,
                    EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_EC_TRUNCATION),
                    ElinkData  => lpGBT_UPLINK_EC_DATA(link),
                    ElinkAligned => LinkAligned(link),
                    DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-2),
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                    RealignmentEvent => RealignmentEvent(link)(STREAMS_TOHOST-2),
                    FE_BUSY_out => open,
                    m_axis => m_axis(link,STREAMS_TOHOST-2),
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-2),
                    m_axis_aclk => aclk_s,
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-2)
                );

            EpathIC: entity work.DecodingEpathGBT
                generic map(
                    MAX_INPUT      => 2,
                    INCLUDE_16b    => '0',
                    INCLUDE_8b     => '0',
                    INCLUDE_4b     => '0',
                    INCLUDE_2b     => '1',
                    INCLUDE_8b10b  => '0',
                    INCLUDE_HDLC   => '1',
                    INCLUDE_DIRECT => '0',
                    BLOCKSIZE      => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '1',
                    GENERATE_FEI4B => false,
                    VERSAL => VERSAL
                )
                port map(
                    clk40 => clk40,
                    daq_reset => daq_reset,
                    daq_fifo_flush => daq_fifo_flush,
                    EpathEnable => EpathEnableIC,
                    EpathEncoding => "0010", --Only support HDLC
                    ElinkWidth => "000",
                    MsbFirst   => MsbFirst,
                    ReverseInputBits => BitSwappingIC,
                    EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_IC_TRUNCATION),
                    ElinkData  => lpGBT_UPLINK_IC_DATA(link),
                    ElinkAligned => LinkAligned(link),
                    DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-1),
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                    RealignmentEvent => RealignmentEvent(link)(STREAMS_TOHOST-1),
                    FE_BUSY_out => open,
                    m_axis => m_axis(link,STREAMS_TOHOST-1),
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-1),
                    m_axis_aclk => aclk_s,
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-1)
                );

            --check it against the unchanged version of decoding to see if I
            --screwed up something

            g_LPGBT_egroup: for egroup in 0 to (NUM_ELINK-1) generate
            begin
                --! Map axis 32 bus for 1 E-group to the complete 2D array.
                g_axisindex: for i in 0 to 3 generate
                    m_axis(link,egroup*4+i)            <= m_axis_s(egroup*4+i);
                    m_axis_tready_s(egroup*4+i)           <= m_axis_tready(link,egroup*4+i);
                    m_axis_prog_empty(link,egroup*4+i) <= m_axis_prog_empty_s(egroup*4+i);
                end generate;

            end generate;


            Link0: entity work.DecodingBCMLinkLPGBT
                generic map(
                    --CARD_TYPE => CARD_TYPE,
                    BLOCKSIZE => BLOCKSIZE,
                    LINK => link,
                    --SIMU => 0,
                    STREAMS_TOHOST => STREAMS_TOHOST,
                    AVALIABLE_CLK_CYCLES => TOHOST_AXI_STREAM_FREQ(FIRMWARE_MODE_BCM_PRIME)/40,
                    --PCIE_ENDPOINT => PCIE_ENDPOINT,
                    VERSAL => VERSAL
                )
                port map(
                    clk40                         => clk40,
                    daq_reset                     => daq_reset,
                    daq_fifo_flush                => daq_fifo_flush,
                    LinkData                      => lpGBT_UPLINK_USER_DATA(link)(223 downto 0),
                    LinkAligned                   => LinkAligned(link),
                    m_axis                        => m_axis_s,
                    m_axis_tready                 => m_axis_tready_s,
                    m_axis_aclk                   => aclk_s,
                    m_axis_prog_empty             => m_axis_prog_empty_s,
                    register_map_control          => register_map_control,
                    TTC_ToHostData                => TTC_ToHost_Data_in,--,
                    L1AWindow                     => register_map_control.DECODING_BCM_PRIME_L1A(link).WINDOW,
                    L1ADelay                      => register_map_control.DECODING_BCM_PRIME_L1A(link).DELAY
                --DecoderAligned                => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-3 downto 0),
                --DecoderDeskewed               => register_map_decoding_monitor.DECODING_LINK_CB(link).DESKEWED(STREAMS_TOHOST+1 downto 4)
                );
            register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-3 downto 0) <= (others => '1');
            register_map_decoding_monitor.DECODING_LINK_CB(link).DESKEWED(STREAMS_TOHOST+1 downto 4) <= (others => '1');

        end generate; -- g_LPGBT_Links


        -- Ismet?? Understand what this does ??? ---
        pulsegen0: entity work.AlignmentPulseGen
            generic map(
                MAX_VAL_DEALIGN => LOCK_PERIOD, --2048 bytes at 8b10b / 80 Mb/s elink.
                MAX_VAL_ALIGN => 20 --2048 bytes at 8b10b / 80 Mb/s elink.
            )
            port map(
                clk40 => clk40,
                AlignmentPulseAlign => AlignmentPulseAlign,
                AlignmentPulseDeAlign => AlignmentPulseDeAlign
            );

    end generate; --g_bcmmode


    g_fullmode: if FIRMWARE_MODE = FIRMWARE_MODE_FULL generate
        g_decoding: for i in 0 to GBT_NUM-1 generate
            signal fullmode_FE_BUSY: std_logic;
        begin
            FE_BUSY_SYNC: xpm_cdc_single generic map(
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0
                ) port map(
                    src_clk => RXUSRCLK(i),
                    src_in => fullmode_FE_BUSY,
                    dest_clk => clk40,
                    dest_out => FE_BUSY_out(i)(0)
                );

            decoding0: entity work.FullToAxis
                generic map(
                    BLOCKSIZE => BLOCKSIZE,
                    VERSAL => VERSAL
                )
                port map(
                    clk240 => RXUSRCLK(i),
                    FMdin => FULL_UPLINK_USER_DATA(i), --: in std_logic_vector(32 downto 0);
                    FMdin_is_big_endian => to_sl(register_map_control.DECODING_ENDIANNESS_FULL_MODE),
                    --LinkAligned => LinkAligned(i), --: in std_logic;
                    aclk => aclk_s, --: in std_logic;
                    daq_reset => daq_reset,
                    daq_fifo_flush => daq_fifo_flush,
                    m_axis => m_axis(i,0), --: out axis_32_type;
                    m_axis_tready => m_axis_tready(i,0), --: in std_logic;
                    m_axis_prog_empty => m_axis_prog_empty(i,0),
                    FE_BUSY_out => fullmode_FE_BUSY,
                    path_ena => register_map_control.DECODING_EGROUP_CTRL(i)(0).EPATH_ENA(0), --: in std_logic;
                    super_chunk_factor_link => register_map_control.SUPER_CHUNK_FACTOR_LINK(i),
                    Use32bSOP => register_map_control.FULLMODE_32B_SOP(0)
                );
            g_NoSuperchunkforDune: if AddFULLMODEForDUNE generate
                decodingNoSC0: entity work.FullToAxis
                    generic map(
                        BLOCKSIZE => BLOCKSIZE,
                        VERSAL => VERSAL
                    )
                    port map(
                        clk240 => RXUSRCLK(i),
                        FMdin => FULL_UPLINK_USER_DATA(i), --: in std_logic_vector(32 downto 0);
                        FMdin_is_big_endian => to_sl(register_map_control.DECODING_ENDIANNESS_FULL_MODE),
                        --LinkAligned => LinkAligned(i), --: in std_logic;
                        aclk => aclk_s, --: in std_logic;
                        daq_reset => daq_reset,
                        daq_fifo_flush => daq_fifo_flush,
                        m_axis => m_axis_noSC(i,0), --: out axis_32_type;
                        m_axis_tready => m_axis_noSC_tready(i,0), --: in std_logic;
                        m_axis_prog_empty => m_axis_noSC_prog_empty(i,0),
                        FE_BUSY_out => open,
                        path_ena => register_map_control.DECODING_EGROUP_CTRL(i)(0).EPATH_ENA(0), --: in std_logic;
                        super_chunk_factor_link => x"01",
                        Use32bSOP => register_map_control.FULLMODE_32B_SOP(0)
                    );
            end generate g_NoSuperchunkforDune;


            register_map_decoding_monitor.DECODING_LINK_ALIGNED(i)(57 downto 1) <= (others => '0');
            register_map_decoding_monitor.DECODING_LINK_ALIGNED(i)(0) <= LinkAligned(i);

        --CR_TOHOST_GBT_MON(i).EPATH0_ALMOST_FULL(0) <= not m_axis_tready(i,0);

        end generate;
    end generate;

    g_strips: if FIRMWARE_MODE = FIRMWARE_MODE_STRIP generate
        signal AlignmentPulseAlign : std_logic;
        signal AlignmentPulseDeAlign: std_logic;
        signal amac_invert_polarity_in : std_logic;
    begin
        amac_invert_polarity_in <= register_map_control.GLOBAL_STRIPS_CONFIG.INVERT_AMAC_IN(
                                                                                            register_map_control.GLOBAL_STRIPS_CONFIG.INVERT_AMAC_IN'low);
        g_GBT_Links: for link in 0 to GBT_NUM-1 generate
            signal amac_rst, EpathEnableIC : std_logic;
            signal BitSwappingIC : std_logic;
            signal MsbFirst : std_logic;
            signal fromDeglitcher : std_logic;
            signal rx_8b10b_err_cnt_i : array_32b(0 to 6);
            signal SoftErrorEgroupIndex: integer range 0 to 6;
            signal EgroupCounterClear: std_logic;
        begin
            --TODO: Include TTC ToHost virtual E-link for link=0
            --TODO: Include XOFF/BUSY virtual E-link for link=0

            --The aligned status bits have more bits than we need, to account for wide mode etc.
            register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(57 downto 42) <= (others => '0');
            amac_rst <= not to_sl(register_map_control.MINI_EGROUP_TOHOST(link).EC_ENABLE);
            EpathEnableIC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_ENABLE);
            BitSwappingIC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_BIT_SWAPPING);
            MsbFirst <= '1'; --to_sl(register_map_control.DECODING_REVERSE_10B);

            EDeglitcher: entity work.EndeavourDeglitcher
                port map
                (
                    clk40   => clk40,
                    rst     => amac_rst,
                    datain  => lpGBT_UPLINK_EC_DATA(link),
                    dataout => fromDeglitcher
                );

            Edecoding0: entity work.EndeavourDecoder
                generic map (
                    DEBUG_en => false,
                    VERSAL => VERSAL
                )
                port map
                (
                    clk40 => clk40,
                    m_axis_aclk => aclk_s, --: in std_logic;
                    amac_signal => fromDeglitcher, --: in std_logic_vector(32 downto 0);
                    LinkAligned => LinkAligned(link), --: in std_logic;
                    daq_fifo_flush => daq_fifo_flush,
                    amac_rst => amac_rst,
                    m_axis => m_axis(link,STREAMS_TOHOST-2), --m_axis(i,0), --: out axis_32_type;
                    invert_polarity => amac_invert_polarity_in,
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-2), --m_axis_tready(i,0) --: in std_logic;
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-2)
                );

            EpathIC: entity work.DecodingEpathGBT
                generic map(
                    MAX_INPUT => 2,
                    INCLUDE_16b    => '0',
                    INCLUDE_8b => '0',
                    INCLUDE_4b => '0',
                    INCLUDE_2b => '1',
                    INCLUDE_8b10b => '1',
                    INCLUDE_HDLC => '1',
                    INCLUDE_DIRECT => '0',
                    BLOCKSIZE => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '1',
                    GENERATE_FEI4B => false,
                    VERSAL => VERSAL
                )
                port map(
                    clk40 => clk40,
                    daq_reset => daq_reset,
                    daq_fifo_flush => daq_fifo_flush,
                    EpathEnable => EpathEnableIC,
                    EpathEncoding => "0010", --Only support HDLC
                    ElinkWidth => "000",
                    MsbFirst   => MsbFirst,
                    ReverseInputBits => BitSwappingIC,
                    EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_IC_TRUNCATION),
                    ElinkData  => lpGBT_UPLINK_IC_DATA(link),
                    ElinkAligned => LinkAligned(link),
                    DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-1),
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                    RealignmentEvent => RealignmentEvent(link)(STREAMS_TOHOST-1),
                    FE_BUSY_out => open,
                    m_axis => m_axis(link,STREAMS_TOHOST-1),
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-1),
                    m_axis_aclk => aclk_s,
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-1)
                );

            g_LPGBT_Egroups: for egroup in 0 to 6 generate
                signal ElinkWidth: std_logic_vector(2 downto 0);
                signal PathEnable: std_logic_vector(3 downto 0);     --4elinks
                --signal not_PathEnable : std_logic_vector(3 downto 0);
                signal ReverseElinks : std_logic_vector(3 downto 0);

                signal EGroupData : std_logic_vector(31 downto 0);
                signal m_axis_s : axis_32_array_type(0 to 3);
                signal m_axis_tready_s : axis_tready_array_type(0 to 3);
                signal m_axis_prog_empty_s : axis_tready_array_type(0 to 3);
                signal DecoderAligned : std_logic_vector(3 downto 0);

                signal PathEncoding : std_logic_vector(15 downto 0);--akamensh
                constant ILA_OK : std_logic := '0';

            --
            begin
                ----------------------------------------------------------------------
                -- Strips data decoding is the same as lpGBT mode
                -- only 8b10b 320 Mbps and 640 Mbps support is required
                ----------------------------------------------------------------------

                --Shouldn't we have different registers for LPGBT?
                PathEncoding <= register_map_control.DECODING_EGROUP_CTRL(link)(egroup).PATH_ENCODING(26 downto 11);
                ReverseElinks <= (others => '0'); --register_map_control.DECODING_EGROUP_CTRL(link)(egroup).REVERSE_ELINKS(46 downto 43);
                ElinkWidth <= '0' & register_map_control.DECODING_EGROUP_CTRL(link)(egroup).EPATH_WIDTH(9 downto 8); --never 1280Mb/s - saves ~10% LUTS!
                PathEnable <= register_map_control.DECODING_EGROUP_CTRL(link)(egroup).EPATH_ENA(3 downto 0);
                EGroupData <= lpGBT_UPLINK_USER_DATA(link)(egroup*32+31 downto egroup*32);
                register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(egroup*4+3 downto egroup*4) <= DecoderAligned;
                --not_PathEnable <= not PathEnable;

                --ILA_OK <= '1' when (egroup=0) and (link=3) else '0'; --allows setting subset of decoders to have ILAs
                --ILA_OK <= '0'; --Disable ILA before merging to phase2/master

                decoderEgroup8b10b: entity work.DecEgroup_8b10b
                    generic map(
                        ILA_ENABLE => ILA_OK,
                        BLOCKSIZE => BLOCKSIZE,
                        Support32bWidth => '0',
                        Support16bWidth => '1',
                        Support8bWidth => '1',
                        IncludeElinks => "0101",
                        VERSAL => VERSAL
                    )
                    port map(
                        clk40 => clk40,
                        daq_reset => daq_reset,
                        daq_fifo_flush => daq_fifo_flush,
                        EgroupCounterClear => EgroupCounterClear,
                        tick_1us_i => tick_1us,
                        dm_start_mask_i   => register_map_control.DIRECT_MODE_CTRL.START_SOURCE_MASK,
                        dm_capture_length_i => x"0" & register_map_control.DIRECT_MODE_CTRL.CAPTURE_LENGTH,

                        DataIn =>  EGroupData,

                        EnableIn => PathEnable,
                        LinkAligned => LinkAligned(link),
                        ElinkWidth => ElinkWidth,
                        AlignmentPulseAlign => AlignmentPulseAlign,
                        AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                        AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                        RealignmentEvent => RealignmentEvent(link)(egroup*4+3 downto egroup*4),
                        PathEncoding => PathEncoding,
                        ElinkAligned => DecoderAligned,
                        DecodingErrors => rx_8b10b_err_cnt_i(egroup),
                        MsbFirst => MsbFirst,
                        ReverseInputBits => ReverseElinks,
                        HGTD_ALTIROC_DECODING => '0',
                        FE_BUSY_out => open, --ITk strips FE don't report BUSY
                        m_axis => m_axis_s,
                        m_axis_tready => m_axis_tready_s,
                        m_axis_aclk => aclk_s,
                        m_axis_prog_empty => m_axis_prog_empty_s
                    );


                elinks_8b10b : for elink in 0 to 3 generate
                    --! Map axis 32 bus for 1 E-group to the complete 2D array.
                    m_axis(link, egroup*4 + elink) <= m_axis_s(elink);
                    m_axis_tready_s(elink) <= m_axis_tready(link, egroup*4 + elink);
                    m_axis_prog_empty(link, egroup*4 + elink) <= m_axis_prog_empty_s(elink);

                end generate;

            end generate; -- g_LPGBT_Egroups
            SoftErrorEgroupIndex <= to_integer(unsigned(register_map_control.LINK_ERRORS(link).EGROUP_SELECT));
            register_map_decoding_monitor.LINK_ERRORS(link).COUNT <= rx_8b10b_err_cnt_i(SoftErrorEgroupIndex);
            EgroupCounterClear <= register_map_control.LINK_ERRORS(link).CLEAR_COUNTERS(register_map_control.LINK_ERRORS(link).CLEAR_COUNTERS'low);
        end generate; -- ITk strips decoder

        pulsegen0: entity work.AlignmentPulseGen
            generic map(
                MAX_VAL_DEALIGN => LOCK_PERIOD, --2048 bytes at 8b10b / 80 Mb/s elink.
                MAX_VAL_ALIGN => 20
            )
            port map(
                clk40 => clk40,
                AlignmentPulseAlign => AlignmentPulseAlign,
                AlignmentPulseDeAlign => AlignmentPulseDeAlign
            );
    end generate;

    g_lpgbt8b10b: if FIRMWARE_MODE = FIRMWARE_MODE_LPGBT generate
        signal AlignmentPulseAlign: std_logic;
        signal AlignmentPulseDeAlign: std_logic;
    begin
        g_lpGBT_Links: for link in 0 to GBT_NUM-1 generate
            signal EpathEnableEC, EpathEnableIC : std_logic;
            signal BitSwappingEC, BitSwappingIC : std_logic;
            signal MsbFirst : std_logic;
            signal EgroupCounterClear: std_logic;
            signal rx_8b10b_err_cnt_i : array_32b(0 to 6);
            signal SoftErrorEgroupIndex: integer range 0 to 6;
        begin

            --The aligned status bits have more bits than we need, to account for wide mode etc.
            register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(57 downto STREAMS_TOHOST) <= (others => '0');


            EpathEnableEC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).EC_ENABLE);
            EpathEnableIC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_ENABLE);
            --! Disable bit swapping features to save resources
            BitSwappingEC <= '0'; --to_sl(register_map_control.MINI_EGROUP_TOHOST(link).EC_BIT_SWAPPING);
            BitSwappingIC <= '0'; --to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_BIT_SWAPPING);
            MsbFirst <= '1';--to_sl(register_map_control.DECODING_REVERSE_10B);

            EpathEC: entity work.DecodingEpathGBT
                generic map(
                    MAX_INPUT      => 2,
                    INCLUDE_16b    => '0',
                    INCLUDE_8b     => '0',
                    INCLUDE_4b     => '0',
                    INCLUDE_2b     => '1',
                    INCLUDE_8b10b  => '1',
                    INCLUDE_HDLC   => '1',
                    INCLUDE_DIRECT => '0',
                    BLOCKSIZE      => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '1',
                    GENERATE_FEI4B => false,
                    VERSAL => VERSAL
                )
                port map(
                    clk40 => clk40,
                    daq_reset => daq_reset,
                    daq_fifo_flush => daq_fifo_flush,
                    EpathEnable => EpathEnableEC,
                    EpathEncoding => register_map_control.MINI_EGROUP_TOHOST(link).EC_ENCODING,
                    ElinkWidth => "000",
                    MsbFirst   => MsbFirst,
                    ReverseInputBits => BitSwappingEC,
                    EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_EC_TRUNCATION),
                    ElinkData  => lpGBT_UPLINK_EC_DATA(link),
                    ElinkAligned => LinkAligned(link),
                    DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-2),
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                    RealignmentEvent => RealignmentEvent(link)(STREAMS_TOHOST-2),
                    FE_BUSY_out => open,
                    m_axis => m_axis(link,STREAMS_TOHOST-2),
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-2),
                    m_axis_aclk => aclk_s,
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-2)
                );

            EpathIC: entity work.DecodingEpathGBT
                generic map(
                    MAX_INPUT      => 2,
                    INCLUDE_16b    => '0',
                    INCLUDE_8b     => '0',
                    INCLUDE_4b     => '0',
                    INCLUDE_2b     => '1',
                    INCLUDE_8b10b  => '1',
                    INCLUDE_HDLC   => '1',
                    INCLUDE_DIRECT => '0',
                    BLOCKSIZE      => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '1',
                    GENERATE_FEI4B => false,
                    VERSAL => VERSAL
                )
                port map(
                    clk40 => clk40,
                    daq_reset => daq_reset,
                    daq_fifo_flush => daq_fifo_flush,
                    EpathEnable => EpathEnableIC,
                    EpathEncoding => "0010", --Only support HDLC
                    ElinkWidth => "000",
                    MsbFirst   => MsbFirst,
                    ReverseInputBits => BitSwappingIC,
                    EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_IC_TRUNCATION),
                    ElinkData  => lpGBT_UPLINK_IC_DATA(link),
                    ElinkAligned => LinkAligned(link),
                    DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-1),
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                    RealignmentEvent => RealignmentEvent(link)(STREAMS_TOHOST-1),
                    FE_BUSY_out => open,
                    m_axis => m_axis(link,STREAMS_TOHOST-1),
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-1),
                    m_axis_aclk => aclk_s,
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-1)
                );

            g_Egroups: for egroup in 0 to 6 generate
                signal ElinkWidth: std_logic_vector(2 downto 0);
                signal PathEnable: std_logic_vector(3 downto 0);
                signal PathEncoding : std_logic_vector(15 downto 0);
                signal ReverseElinks : std_logic_vector(3 downto 0);
                signal m_axis_s : axis_32_array_type(0 to 3);
                signal m_axis_tready_s : axis_tready_array_type(0 to 3);
                signal m_axis_prog_empty_s : axis_tready_array_type(0 to 3);
            begin
                --! Map axis 32 bus for 1 E-group to the complete 2D array.
                g_axisindex: for i in 0 to 3 generate
                    m_axis(link,egroup*4+i) <= m_axis_s(i);
                    m_axis_tready_s(i) <= m_axis_tready(link,egroup*4+i);
                    m_axis_prog_empty(link,egroup*4+i) <= m_axis_prog_empty_s(i);
                end generate;
                --TODO: Path Encoding and Reverse Elinks not implemented in DecEgroup_8b10b
                PathEncoding <= register_map_control.DECODING_EGROUP_CTRL(link)(egroup).PATH_ENCODING(26 downto 11);
                ReverseElinks <= (others => '0') ; --register_map_control.DECODING_EGROUP_CTRL(link)(egroup).REVERSE_ELINKS(46 downto 43);
                ElinkWidth <= register_map_control.DECODING_EGROUP_CTRL(link)(egroup).EPATH_WIDTH;
                PathEnable <= register_map_control.DECODING_EGROUP_CTRL(link)(egroup).EPATH_ENA(3 downto 0);

                --! Instantiate one Egroup.
                eGroup0: entity work.DecEgroup_8b10b
                    generic map(
                        BLOCKSIZE => BLOCKSIZE,
                        Support32bWidth  => IncludeDecodingEpath32_8b10b(egroup),
                        Support16bWidth  => IncludeDecodingEpath16_8b10b(egroup),
                        Support8bWidth   => IncludeDecodingEpath8_8b10b(egroup),
                        IncludeElinks => "1111",
                        VERSAL => VERSAL
                    )
                    port map(
                        clk40             => clk40,
                        daq_reset         => daq_reset,
                        daq_fifo_flush    => daq_fifo_flush,
                        EgroupCounterClear => EgroupCounterClear,
                        tick_1us_i => tick_1us,
                        dm_start_mask_i   => register_map_control.DIRECT_MODE_CTRL.START_SOURCE_MASK,
                        dm_capture_length_i => x"0" & register_map_control.DIRECT_MODE_CTRL.CAPTURE_LENGTH,
                        DataIn            => lpGBT_UPLINK_USER_DATA(link)(egroup*32+31 downto egroup*32),
                        EnableIn          => PathEnable,
                        LinkAligned       => LinkAligned(link),
                        ElinkWidth        => ElinkWidth,
                        AlignmentPulseAlign => AlignmentPulseAlign,
                        AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                        AutoRealign       => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                        RealignmentEvent  => RealignmentEvent(link)(egroup*4+3 downto egroup*4),
                        PathEncoding      => PathEncoding,
                        ElinkAligned      => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(egroup*4+3 downto egroup*4),
                        DecodingErrors    => rx_8b10b_err_cnt_i(egroup),
                        MsbFirst          => MsbFirst,
                        ReverseInputBits  => ReverseElinks,
                        HGTD_ALTIROC_DECODING => to_sl(register_map_control.DECODING_HGTD_ALTIROC),
                        FE_BUSY_out       => FE_BUSY_out(link)(4*egroup+3 downto 4*egroup),
                        m_axis            => m_axis_s,
                        m_axis_tready     => m_axis_tready_s,
                        m_axis_aclk       => aclk_s,
                        m_axis_prog_empty => m_axis_prog_empty_s
                    );
            end generate g_Egroups;
            SoftErrorEgroupIndex <= to_integer(unsigned(register_map_control.LINK_ERRORS(link).EGROUP_SELECT));
            register_map_decoding_monitor.LINK_ERRORS(link).COUNT <= rx_8b10b_err_cnt_i(SoftErrorEgroupIndex);
            EgroupCounterClear <= register_map_control.LINK_ERRORS(link).CLEAR_COUNTERS(register_map_control.LINK_ERRORS(link).CLEAR_COUNTERS'low);
        end generate g_lpGBT_Links;

        pulsegen0: entity work.AlignmentPulseGen
            generic map(
                MAX_VAL_DEALIGN => LOCK_PERIOD, --2048 bytes at 8b10b / 80 Mb/s elink.
                MAX_VAL_ALIGN => 20 --2 bytes at 8b10b / 80 Mb/s elink.
            )
            port map(
                clk40 => clk40,
                AlignmentPulseAlign => AlignmentPulseAlign,
                AlignmentPulseDeAlign => AlignmentPulseDeAlign
            );
    end generate g_lpgbt8b10b;

    g_hgtdlumi: if FIRMWARE_MODE = FIRMWARE_MODE_HGTD_LUMI generate
        signal AlignmentPulseAlign: std_logic;
        signal AlignmentPulseDeAlign: std_logic;
    begin

        g_links: for link in 0 to GBT_NUM-1 generate
            signal EpathEnableEC, EpathEnableIC : std_logic;
            signal BitSwappingEC, BitSwappingIC : std_logic;
            signal MsbFirst : std_logic;

            signal raw_mode : std_logic;
            signal sync_word : std_logic_vector(15 downto 0);
            signal lhc_turns : std_logic_vector(9 downto 0);
            signal trig_latency : std_logic_vector(8 downto 0);
            signal debug_datasource : std_logic;

            signal test_case_selection : std_logic_vector(3 downto 0); --10 test cases. HGTD_LUMI_EMULATOR_TEST_CASE
            signal W1_const : std_logic_vector(6 downto 0); --for test case 0, 1, 3, 5, 6, 7, 8, 9. std_value = "0000001" = 0x1. HGTD_LUMI_EMULATOR_W1_CONST
            signal W2_const : std_logic_vector(4 downto 0); --for test case 0, 1, 3, 5, 6, 7, 8, 9. std_value = "00001" = 0x1. HGTD_LUMI_EMULATOR_W2_CONST
            signal fault_bcid : std_logic_vector(11 downto 0); --for test case 5, 6. says from which BCID we want the fault to occur. std_value = "001111101000" = 1000 = 0x3E8. HGTD_LUMI_EMULATOR_FAULT_BCID
            signal n_faults : std_logic_vector(11 downto 0); --for test case 5, 6, 9. says for how many BCIDs/turns we want the fault to occur. std_value = "000000001010" = 10 = 0xA. HGTD_LUMI_EMULATOR_N_FAULTS
            signal corrupted_word : std_logic_vector(15 downto 0); --for test case 6. std_value = ?. HGTD_LUMI_EMULATOR_CORRUPT_WORD
            signal n_turn : std_logic_vector(9 downto 0); --for test case 7, 9. says at which turn we want the fault to occur. std_value = "0000000100" = 0x4. HGTD_LUMI_EMULATOR_N_TURN
            signal W1_const2 : std_logic_vector(6 downto 0); --for test case 8. std_value = "0000010" = 0x2. HGTD_LUMI_EMULATOR_W1_CONST2
            signal W2_const2 : std_logic_Vector(4 downto 0); --for test case 8. std_value = "00010" = 0x2. HGTD_LUMI_EMULATOR_W2_CONST2
            signal shift_bits : std_logic_vector(3 downto 0);

        begin
            -- get configuration from register bank (registered per-link to reduce fanout)
            debug_datasource <= to_sl(register_map_control.DECODING_HGTD_LUMI_CONF.DEBUG_DATASOURCE) when rising_edge(clk40);
            raw_mode <= to_sl(register_map_control.DECODING_HGTD_LUMI_CONF.RAW_MODE) when rising_edge(clk40);
            sync_word <= register_map_control.DECODING_HGTD_LUMI_CONF.SYNC_WORD(15 downto 0) when rising_edge(clk40);
            lhc_turns <= register_map_control.DECODING_HGTD_LUMI_CONF.LHC_TURNS(34 downto 25) when rising_edge(clk40);
            trig_latency <= register_map_control.DECODING_HGTD_LUMI_CONF.TRIG_LAT(24 downto 16) when rising_edge(clk40);

            --HGTD Emulator registers
            test_case_selection <= register_map_control.HGTD_LUMI_EMULATOR.TEST_CASE(3 downto 0) when rising_edge(clk40);
            W1_const <= register_map_control.HGTD_LUMI_EMULATOR.W1_CONST(10 downto 4) when rising_edge(clk40);
            W2_const <= register_map_control.HGTD_LUMI_EMULATOR.W2_CONST(15 downto 11) when rising_edge(clk40);
            fault_bcid <= register_map_control.HGTD_LUMI_EMULATOR.FAULT_BCID(27 downto 16) when rising_edge(clk40);
            n_faults <= register_map_control.HGTD_LUMI_EMULATOR.N_FAULTS(39 downto 28) when rising_edge(clk40);
            corrupted_word <= register_map_control.HGTD_LUMI_EMULATOR.CORRUPT_WORD(55 downto 40) when rising_edge(clk40);
            n_turn <= register_map_control.HGTD_LUMI_EMULATOR2.N_TURN(9 downto 0) when rising_edge(clk40);
            W1_const2 <= register_map_control.HGTD_LUMI_EMULATOR2.W1_CONST2(16 downto 10) when rising_edge(clk40);
            W2_const2 <= register_map_control.HGTD_LUMI_EMULATOR2.W2_CONST2(21 downto 17) when rising_edge(clk40);
            shift_bits <= register_map_control.HGTD_LUMI_EMULATOR2.SHIFT_BITS(25 downto 22) when rising_edge(clk40);

            --The aligned status bits have more bits than we need, to account for wide mode etc.
            register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(57 downto STREAMS_TOHOST) <= (others => '0');

            EpathEnableEC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).EC_ENABLE);
            EpathEnableIC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_ENABLE);
            BitSwappingEC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).EC_BIT_SWAPPING);
            BitSwappingIC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_BIT_SWAPPING);
            MsbFirst <= to_sl(register_map_control.DECODING_REVERSE_10B);

            EpathEC: entity work.DecodingEpathGBT
                generic map(
                    MAX_INPUT      => 2,
                    INCLUDE_16b    => '0',
                    INCLUDE_8b     => '0',
                    INCLUDE_4b     => '0',
                    INCLUDE_2b     => '1',
                    INCLUDE_8b10b  => '1',
                    INCLUDE_HDLC   => '1',
                    INCLUDE_DIRECT => '0',
                    BLOCKSIZE      => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '1',
                    GENERATE_FEI4B => false,
                    VERSAL => VERSAL
                )
                port map(
                    clk40 => clk40,
                    daq_reset => daq_reset,
                    daq_fifo_flush => daq_fifo_flush,
                    EpathEnable => EpathEnableEC,
                    EpathEncoding => register_map_control.MINI_EGROUP_TOHOST(link).EC_ENCODING,
                    ElinkWidth => "000",
                    MsbFirst   => MsbFirst,
                    ReverseInputBits => BitSwappingEC,
                    EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_EC_TRUNCATION),
                    ElinkData  => lpGBT_UPLINK_EC_DATA(link),
                    ElinkAligned => LinkAligned(link),
                    DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-2),
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                    RealignmentEvent => open,
                    FE_BUSY_out => open,
                    m_axis => m_axis(link,STREAMS_TOHOST-2),
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-2),
                    m_axis_aclk => aclk_s,
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-2)
                );

            EpathIC: entity work.DecodingEpathGBT
                generic map(
                    MAX_INPUT      => 2,
                    INCLUDE_16b    => '0',
                    INCLUDE_8b     => '0',
                    INCLUDE_4b     => '0',
                    INCLUDE_2b     => '1',
                    INCLUDE_8b10b  => '1',
                    INCLUDE_HDLC   => '1',
                    INCLUDE_DIRECT => '0',
                    BLOCKSIZE      => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '1',
                    GENERATE_FEI4B => false,
                    VERSAL => VERSAL
                )
                port map(
                    clk40 => clk40,
                    EpathEnable => EpathEnableIC,
                    EpathEncoding => "0010", --Only support HDLC
                    ElinkWidth => "000",
                    MsbFirst   => MsbFirst,
                    ReverseInputBits => BitSwappingIC,
                    EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_IC_TRUNCATION),
                    ElinkData  => lpGBT_UPLINK_IC_DATA(link),
                    ElinkAligned => LinkAligned(link),
                    DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-1),
                    AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    m_axis => m_axis(link,STREAMS_TOHOST-1),
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-1),
                    m_axis_aclk => aclk_s,
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-1),
                    daq_reset => daq_reset,
                    daq_fifo_flush => daq_fifo_flush,
                    RealignmentEvent => open,
                    FE_BUSY_out => open
                );

            g_quads: for quad in 0 to 3 generate
                signal quad_data : std_logic_vector(63 downto 0);
                signal quad_reverse_bits : std_logic_vector(3 downto 0);
                signal quad_aligned : std_logic_vector(3 downto 0);
                signal quad_enable_aggregate : std_logic_vector(3 downto 0);
                signal quad_enable_event : std_logic_vector(3 downto 0);
                signal aggregate_m_axis : axis_32_array_type(3 downto 0);
                signal aggregate_m_axis_tready : std_logic_vector(3 downto 0);
                signal aggregate_m_axis_prog_empty : std_logic_vector(3 downto 0);
                signal event_m_axis : axis_32_array_type(3 downto 0);
                signal event_m_axis_tready : std_logic_vector(3 downto 0);
                signal event_m_axis_prog_empty : std_logic_vector(3 downto 0);
            begin
                g_last_quad: if quad = 3 generate
                    quad_data <= x"00000000" & lpGBT_UPLINK_USER_DATA(link)(64*quad+31 downto 64*quad);
                    quad_enable_aggregate <= "00" & register_map_control.DECODING_EGROUP_CTRL(link)(quad).EPATH_ENA(1 downto 0);
                    quad_enable_event <= "00" & register_map_control.DECODING_EGROUP_CTRL(link)(quad).EPATH_ENA(3 downto 2);
                    quad_reverse_bits <= "00" & register_map_control.DECODING_EGROUP_CTRL(link)(quad).REVERSE_ELINKS(44 downto 43);
                    register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(quad*8+3 downto quad*8) <= quad_aligned(1 downto 0) & quad_aligned(1 downto 0);

                    --! Map axis 32 bus for 1 E-group to the complete 2D array.
                    g_axisindex: for i in 0 to 1 generate
                        m_axis(link,quad*8+i) <= aggregate_m_axis(i);
                        aggregate_m_axis_tready(i) <= m_axis_tready(link,quad*8+i);
                        m_axis_prog_empty(link,quad*8+i) <= aggregate_m_axis_prog_empty(i);
                        m_axis(link,quad*8+i+2) <= event_m_axis(i);
                        event_m_axis_tready(i) <= m_axis_tready(link,quad*8+i+2);
                        m_axis_prog_empty(link,quad*8+i+2) <= event_m_axis_prog_empty(i);
                    end generate;
                end generate g_last_quad;

                g_not_last_quad: if quad /= 3 generate
                    quad_data <= lpGBT_UPLINK_USER_DATA(link)(64*quad+63 downto 64*quad);
                    quad_enable_aggregate <= register_map_control.DECODING_EGROUP_CTRL(link)(quad).EPATH_ENA(3 downto 0);
                    quad_enable_event <= register_map_control.DECODING_EGROUP_CTRL(link)(quad).EPATH_ENA(7 downto 4);
                    quad_reverse_bits <= register_map_control.DECODING_EGROUP_CTRL(link)(quad).REVERSE_ELINKS(46 downto 43);
                    register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(quad*8+7 downto quad*8) <= quad_aligned & quad_aligned;

                    --! Map axis 32 bus for 1 E-group to the complete 2D array.
                    g_axisindex: for i in 0 to 3 generate
                        m_axis(link,quad*8+i) <= aggregate_m_axis(i);
                        aggregate_m_axis_tready(i) <= m_axis_tready(link,quad*8+i);
                        m_axis_prog_empty(link,quad*8+i) <= aggregate_m_axis_prog_empty(i);
                        m_axis(link,quad*8+i+4) <= event_m_axis(i);
                        event_m_axis_tready(i) <= m_axis_tready(link,quad*8+i+4);
                        m_axis_prog_empty(link,quad*8+i+4) <= event_m_axis_prog_empty(i);
                    end generate;
                end generate g_not_last_quad;

                quad0: entity work.HGTDLumiQuadDecoder
                    generic map (
                        VERSAL => VERSAL,
                        USE_BUILT_IN_FIFO => '1',
                        BLOCKSIZE => BLOCKSIZE,
                        IMPLEMENT_DATASOURCE => true
                    )
                    port map (
                        clk => clk40,
                        rst => daq_reset,
                        link_aligned => LinkAligned(link),
                        elink_data => quad_data,
                        reverse_bits => quad_reverse_bits,
                        enable_aggregate => quad_enable_aggregate,
                        enable_event => quad_enable_event,
                        aligned => quad_aligned,
                        aggregate_m_axis => aggregate_m_axis,
                        aggregate_m_axis_aclk => aclk_s,
                        aggregate_m_axis_tready => aggregate_m_axis_tready,
                        aggregate_m_axis_prog_empty => aggregate_m_axis_prog_empty,
                        event_m_axis => event_m_axis,
                        event_m_axis_aclk => aclk_s,
                        event_m_axis_tready => event_m_axis_tready,
                        event_m_axis_prog_empty => event_m_axis_prog_empty,
                        TTCin => TTCin,
                        align_seq => sync_word,
                        conf_lhcturns => lhc_turns,
                        trig_latency => trig_latency,
                        enable_datasource => debug_datasource,
                        raw_mode => raw_mode,
                        test_case_selection => test_case_selection,
                        W1_const => W1_const,
                        W2_const => W2_const,
                        fault_bcid => fault_bcid,
                        n_faults => n_faults,
                        corrupted_word => corrupted_word,
                        n_turn => n_turn,
                        W1_const2 => W1_const2,
                        W2_const2 => W2_const2,
                        shift_bits => shift_bits,
                        num_turns => lhc_turns
                    );
            end generate g_quads;
        end generate g_links;

        pulsegen0: entity work.AlignmentPulseGen
            generic map(
                MAX_VAL_DEALIGN => LOCK_PERIOD, --2048 bytes at 8b10b / 80 Mb/s elink.
                MAX_VAL_ALIGN => 20 --2 bytes at 8b10b / 80 Mb/s elink.
            )
            port map(
                clk40 => clk40,
                AlignmentPulseAlign => AlignmentPulseAlign,
                AlignmentPulseDeAlign => AlignmentPulseDeAlign
            );
    end generate g_hgtdlumi;

    g_interlaken: if FIRMWARE_MODE = FIRMWARE_MODE_INTERLAKEN generate
        signal FlowControl: slv_16_array(0 to GBT_NUM-1); -- @suppress "signal FlowControl is never read", @TODO
        signal Decoder_Lock, Descrambler_Lock, HealthLane : std_logic_vector(GBT_NUM-1 downto 0);
        signal decoder_error_sync                         : std_logic_vector(GBT_NUM-1 downto 0);
        signal descrambler_error_badsync                  : std_logic_vector(GBT_NUM-1 downto 0);
        signal descrambler_error_statemismatch            : std_logic_vector(GBT_NUM-1 downto 0);
        signal descrambler_error_nosync                   : std_logic_vector(GBT_NUM-1 downto 0);
        signal burst_crc24_error                          : std_logic_vector(GBT_NUM-1 downto 0);
        signal meta_crc32_error                           : std_logic_vector(GBT_NUM-1 downto 0);
        signal crc24_error_count                          : work.interlaken_package.slv_array(0 to GBT_NUM-1)(15 downto 0);
        signal crc32_error_count                          : work.interlaken_package.slv_array(0 to GBT_NUM-1)(15 downto 0);
        signal error_truncation                           : std_logic_vector(GBT_NUM-1 downto 0);
        signal HealthInterface: std_logic_vector(11 downto 0);
        --signal Detected_Lane_Number : slv_4_array(0 to GBT_NUM-1);
        signal MetaFrameLength : std_logic_vector(11 downto 0);
    --signal Autodetect_Lane_Number: std_logic;
    begin

        toHost_axis64_aclk_out <= clk365;

        MetaFrameLength <= register_map_control.INTERLAKEN_CONTROL.PACKET_LENGTH;
        --Autodetect_Lane_Number <= register_map_control.INTERLAKEN_CONTROL.AUTODETECT_LANE_NUMBER(12);
        g_unbonded_channels: for i in 0 to GBT_NUM-1 generate
            signal daq_reset_sync : std_logic;
        begin

            xpm_cdc_sync_rst_inst : xpm_cdc_sync_rst
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT => 0,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0
                )
                port map (
                    dest_rst => daq_reset_sync,
                    dest_clk => RXUSRCLK(i),
                    src_rst => daq_reset
                );

            il0: entity work.interlaken_receiver_channel generic map(
                    MetaFrameLength => 2048 --to_integer(unsigned(MetaFrameLength))
                )
                port map(
                    clk => RXUSRCLK(i),
                    reset => daq_reset_sync,
                    RX_Data_In => Interlaken_RX_Data_In(i),
                    FlowControl => FlowControl(i),
                    RX_Datavalid => Interlaken_RX_Datavalid(i),
                    Bitslip => Interlaken_RX_Gearboxslip(i),
                    m_axis_deburst => open,
                    m_axis_aclk => clk365,
                    m_axis => m_axis64(i),
                    m_axis_tready => m_axis64_tready(i),
                    m_axis_prog_empty => m_axis64_prog_empty(i),
                    Descrambler_lock => Descrambler_Lock(i) ,
                    Decoder_Lock => Decoder_Lock(i),
                    decoder_error_sync => decoder_error_sync(i),
                    descrambler_error_badsync => descrambler_error_badsync(i),
                    descrambler_error_statemismatch => descrambler_error_statemismatch(i),
                    descrambler_error_nosync => descrambler_error_nosync(i),
                    burst_crc24_error => burst_crc24_error(i),
                    meta_crc32_error => meta_crc32_error(i),
                    crc24_error_count => crc24_error_count(i),
                    crc32_error_count => crc32_error_count(i),
                    error_truncation => error_truncation(i),
                    --Autodetect_Lane_Number => Autodetect_Lane_Number,
                    --Detected_Lane_Number => Detected_Lane_Number,
                    HealthLane => HealthLane(i) ,
                    HealthInterface => HealthInterface(i)
                );
        end generate;
        register_map_decoding_monitor.INTERLAKEN_CONTROL.HEALTH_INTERFACE <= HealthInterface(0 downto 0);

        g_monitor: for i in 0 to GBT_NUM-1 generate
            signal decoder_error_sync_latch, descrambler_error_badsync_latch,
                descrambler_error_statemismatch_latch, descrambler_error_nosync_latch,
                burst_crc24_error_latch, meta_crc32_error_latch: std_logic_vector(0 downto 0);
            signal clear_status: std_logic;
        begin
            --@TODO: Create dedicated interlaken registers for monitoring with a more logical meaning.
            Interlaken_Decoder_Aligned_out(i) <= Decoder_Lock(i) and Descrambler_Lock(i);
            register_map_decoding_monitor.DECODING_LINK_ALIGNED(i)(0) <= Decoder_Lock(i) and Descrambler_Lock(i);

            sync_clear_status: xpm_cdc_single generic map(
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0
                )
                port map(
                    src_clk => '0',
                    src_in => to_sl(register_map_control.INTERLAKEN_STATUS(i).CLEAR_STATUS),
                    dest_clk => RXUSRCLK(i),
                    dest_out => clear_status
                );

            latch_status_proc: process(RXUSRCLK(i))
            begin
                if rising_edge(RXUSRCLK(i)) then
                    if clear_status = '1' then
                        decoder_error_sync_latch <= "0";
                        descrambler_error_badsync_latch <= "0";
                        descrambler_error_statemismatch_latch <= "0";
                        descrambler_error_nosync_latch <= "0";
                        burst_crc24_error_latch <= "0";
                        meta_crc32_error_latch <= "0";
                    else
                        if decoder_error_sync(i) = '1' then
                            decoder_error_sync_latch <= "1";
                        end if;
                        if descrambler_error_badsync(i) = '1' then
                            descrambler_error_badsync_latch <= "1";
                        end if;
                        if descrambler_error_statemismatch(i) = '1' then
                            descrambler_error_statemismatch_latch <= "1";
                        end if;
                        if descrambler_error_nosync(i) = '1' then
                            descrambler_error_nosync_latch <= "1";
                        end if;
                        if burst_crc24_error(i) = '1' then
                            burst_crc24_error_latch <= "1";
                        end if;
                        if meta_crc32_error(i) = '1' then
                            meta_crc32_error_latch <= "1";
                        end if;
                    end if;
                end if;
            end process;


            --register_map_decoding_monitor.DECODING_LINK_ALIGNED(i)(1) <= Descrambler_Lock(i);
            --register_map_decoding_monitor.DECODING_LINK_ALIGNED(i)(2) <= HealthLane(i);
            --register_map_decoding_monitor.DECODING_LINK_ALIGNED(i)(3) <= HealthInterface;
            --register_map_decoding_monitor.DECODING_LINK_ALIGNED(i)(7 downto 4) <= Detected_Lane_Number(i);
            register_map_decoding_monitor.INTERLAKEN_STATUS(i).DECODER_ERROR_SYNC <= decoder_error_sync_latch;
            register_map_decoding_monitor.INTERLAKEN_STATUS(i).DESCRAMBLER_ERROR_BADSYNC <= descrambler_error_badsync_latch;
            register_map_decoding_monitor.INTERLAKEN_STATUS(i).DESCRAMBLER_ERROR_STATEMISMATCH <= descrambler_error_statemismatch_latch;
            register_map_decoding_monitor.INTERLAKEN_STATUS(i).DESCRAMBLER_ERROR_NOSYNC <= descrambler_error_nosync_latch;
            register_map_decoding_monitor.INTERLAKEN_STATUS(i).BURST_CRC24_ERROR <= burst_crc24_error_latch;
            register_map_decoding_monitor.INTERLAKEN_STATUS(i).META_CRC32_ERROR <= meta_crc32_error_latch;
            register_map_decoding_monitor.INTERLAKEN_STATUS(i).DECODER_ALIGNED <= Decoder_Lock(i downto i);
            register_map_decoding_monitor.INTERLAKEN_STATUS(i).DESCRAMBLER_ALIGNED <= Descrambler_Lock(i downto i);
            register_map_decoding_monitor.INTERLAKEN_STATUS(i).HEALTH_LANE <= HealthLane(i downto i);

        --register_map_decoding_monitor.INTERLAKEN_STATUS(i).HEALTH_INTERFACE(3) <= HealthInterface;
        --register_map_decoding_monitor.INTERLAKEN_STATUS(i).DETECTED_LANE <= Detected_Lane_Number(i);

        end generate g_monitor;
    end generate g_interlaken;

end Behavioral;

#!/bin/sh
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Mark Donszelmann
#               Mesfin Gebyehu
#               Thei Wijnen
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#
# Script to rebuild the derived files from templates
#

#
# firmware directory:
firmware_dir=../..
# sources directory:
sources_dir=$firmware_dir/sources
# template directory:
template_dir=$sources_dir/templates

# WupperCodeGen
echo $scriptdir
wuppercodegen_dir=$firmware_dir/WupperCodeGen
wuppercodegen=$wuppercodegen_dir/wuppercodegen/cli.py

current_registers=$template_dir/yaml/regmap/yaml/registers-$1.yaml
$wuppercodegen --version
echo "Generating html documentation for current version..."
$wuppercodegen $current_registers $wuppercodegen_dir/input/registers.html.template registers-$1.html

#Below seems to expect the documention repo to also be cloned as "../../../documents"
regdoc=../../../documents/regmap
cp -p registers-$1.html $regdoc


#!/bin/sh -e
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Mark Donszelmann
#               Andrea Borga
#               Mesfin Gebyehu
#               Thei Wijnen
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# build the documentation from the registermap.tex file
prev_version=5.2
current_version=5.3

# firmware directory:
firmware_dir=../../..
# sources directory:
sources_dir=$firmware_dir/sources
# template directory:
template_dir=$sources_dir/templates
# generated sources directory
generated_dir=$template_dir/generated

wuppercodegen_dir=$firmware_dir/WupperCodeGen
wuppercodegen=$wuppercodegen_dir/wuppercodegen/cli.py

prev_registers=$template_dir/yaml/regmap/yaml/registers-${prev_version}.yaml
current_registers=$template_dir/yaml/regmap/yaml/registers-${current_version}.yaml

$wuppercodegen --version
$wuppercodegen $current_registers  $template_dir/jinja/registermap.tex.template $template_dir/docs/registermap-${current_version}.tex

cd $template_dir/docs/
latex registers-${current_version}.tex
latex registers-${current_version}.tex
dvipdf registers-${current_version}.dvi

# Below seems to expect the documention repo to also be cloned as "../../../../documents"
regdoc=../../../../documents/regmap
cp -p registers-${current_version}.pdf $regdoc
cp -p registers-${current_version}.tex $regdoc
cp -p registermap-${current_version}.tex $regdoc

cp *.html $regdoc

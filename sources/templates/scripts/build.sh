#!/bin/sh
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Mark Donszelmann
#               Andrea Borga
#               Soo Ryu
#               Mesfin Gebyehu
#               RHabraken
#               Rene
#               Thei Wijnen
#               Elena Zhivun
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


#
# Script to rebuild the derived files from templates
#
prev_version=5.2
current_version=5.3
#next_version=5.0

#
# firmware directory:
firmware_dir=../../..
# sources directory:
sources_dir=$firmware_dir/sources
# template directory:
template_dir=$sources_dir/templates
# generated sources directory
generated_dir=$template_dir/generated

# WupperCodeGen
echo $scriptdir
wuppercodegen_dir=$firmware_dir/WupperCodeGen
wuppercodegen=$wuppercodegen_dir/wuppercodegen/cli.py

prev_registers=$template_dir/yaml/regmap/yaml/registers-${prev_version}.yaml
current_registers=$template_dir/yaml/regmap/yaml/registers-${current_version}.yaml
#next_registers=$template_dir/registers-${next_version}.yaml
$wuppercodegen --version
echo "Previous version: $prev_version"
echo "Current  version: $current_version"
#echo "Next     version: $next_version"
echo "Generating pcie_package.vhd and dma_control.vhd and regmap_sync.vhd and wupper.vhd for current version..."
#$wuppercodegen $current_registers $template_dir/version.txt.template $template_dir/version.txt
$wuppercodegen $current_registers $template_dir/jinja/pcie_package.vhd.template $generated_dir/pcie_package.vhd
#Generate a complete dma_control.vhd, valid for all modes
$wuppercodegen $current_registers $template_dir/jinja/dma_control.vhd.template $generated_dir/dma_control.vhd

#Generate a set of dma_control.vhd files with registers left out depending on their mode
for mode in $(seq 0 15); do
    echo Generating dma_control_${mode}.vhd for firmware mode ${mode}
    $wuppercodegen $current_registers $template_dir/jinja/dma_control.vhd.template $generated_dir/dma_control_${mode}.vhd --fw_mode ${mode}
done


$wuppercodegen $current_registers $template_dir/jinja/register_map_sync.vhd.template $generated_dir/register_map_sync.vhd
$wuppercodegen $current_registers $template_dir/jinja/wupper.vhd.template $generated_dir/wupper.vhd
echo "Generating html documentation for previous, current and next version..."
$wuppercodegen $prev_registers $wuppercodegen_dir/input/registers.html.template ${template_dir}/docs/html/registers-${prev_version}.html
$wuppercodegen $current_registers $wuppercodegen_dir/input/registers.html.template ${template_dir}/docs/html/registers-${current_version}.html
#$wuppercodegen $next_registers $wuppercodegen_dir/input/registers.html.template ../docs/html/registers-${next_version}.html
echo "Generating diff between previous and current version..."
$wuppercodegen --diff $prev_registers $current_registers $wuppercodegen_dir/input/registers-diff.html.template ${template_dir}/docs/html/registers-diff-${prev_version}-${current_version}.html
#echo "Generating diff between current and next version..."
#$wuppercodegen --diff $current_registers $next_registers $wuppercodegen_dir/input/registers-diff.html.template registers-diff-${current_version}-${next_version}.html
cd ${firmware_dir}/scripts/helper
source ./run_vsg_fix.sh

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               Julia Narevicius
--!               RHabraken
--!               Filiberto Bonini
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.
--! Engineer: juna
--!
--! Create Date:    07/09/2016
--! Module Name:    FullModeDataEmulator
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use work.pcie_package.all;
    use work.axi_stream_package.all;
    use work.FELIX_package.all;
library xpm;
    use xpm.vcomponents.all;

--! E-link data emulator
entity FullModeDataEmulator is
    generic (
        GBT_NUM: integer := 24;
        BLOCKSIZE : integer;
        AddFULLMODEForDUNE : boolean := false;
        VERSAL : boolean
    );
    port (
        appreg_clk                         : in  std_logic;
        register_map_control               : in  register_map_control_type; --! configuration settings, 64 bit per EGROUP (7 EGROUPS total)
        register_map_control_appreg_clk    : in  register_map_control_type; --! configuration settings, 64 bit per EGROUP (7 EGROUPS total)
        register_map_gbtemu_monitor        : out register_map_gbtemu_monitor_type;
        FE_BUSY_out                        : out array_57b(0 to GBT_NUM-1) := (others => (others => '0'));

        --
        clk40   : in  std_logic;
        clk240  : in  std_logic;
        aclk              : in  std_logic;
        daq_reset         : in  std_logic;
        daq_fifo_flush    : in  std_logic;
        L1A_IN            : in  std_logic;
        m_axis            : out axis_32_2d_array_type(0 to GBT_NUM-1, 0 to 0);
        m_axis_tready     : in axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to 0);
        m_axis_prog_empty      : out axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to 0);
        m_axis_noSC            : out axis_32_2d_array_type(0 to GBT_NUM-1, 0 to 0);
        m_axis_noSC_tready     : in axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to 0);
        m_axis_noSC_prog_empty : out axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to 0)
    );
end FullModeDataEmulator;


architecture Behavioral of FullModeDataEmulator is


    signal daq_reset_sync, daq_reset_sync_appregclk : std_logic;
    signal daq_fifo_flush_sync: std_logic;
    signal emuram_rdaddr, emuram_wraddr : std_logic_vector(12 downto 0) := (others => '0');
    signal ena,ena_r,out_rdy,out_rdy_r : std_logic := '0';
    signal path_ena, path_ena_40: std_logic_vector(GBT_NUM-1 downto 0);
    signal emuram_wrdata,emuram_rddata : std_logic_vector(35 downto 0) := (others=>'0');
    signal wea : std_logic_vector(0 downto 0) := (others=>'0');

    signal FMdout_s, FMdout_logic_s, FMdout_ram_s: std_logic_vector(32 downto 0);
    --signal FMdout_rdy_s: std_logic;

    signal emuram_douta : std_logic_vector(35 downto 0);

    signal FMdout_p0_s, FMdout_p1_s: std_logic_vector(32 downto 0);
    signal crc_start, crc_calc: std_logic;
    signal cnt: unsigned(15 downto 0);
    signal chunkSize: std_logic_vector(13 downto 0); -- in 32b words
    signal idleCount: std_logic_vector(13 downto 0); -- in 32b words
    signal crc_out: std_logic_vector(19 downto 0);
    signal useLogicEmu : std_logic;
    signal L1A_triggered : std_logic;
    signal L1A6x, L1A: std_logic;

begin

    ---------------------------------------------------------------------------------------
    -- reset cdc
    ---------------------------------------------------------------------------------------
    --

    sync_daq_reset: xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 2,
            INIT => 1,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0
        )
        port map (
            src_rst => daq_reset,
            dest_clk => clk240,
            dest_rst => daq_reset_sync
        );

    sync_daq_fifo_flush: xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 2,
            INIT => 0,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0
        )
        port map (
            src_rst => daq_fifo_flush,
            dest_clk => clk240,
            dest_rst => daq_fifo_flush_sync
        );

    sync_aresetn_appregclk : xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 2,
            INIT => 1,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0
        )
        port map (
            src_rst => daq_reset,
            dest_clk => appreg_clk,
            dest_rst => daq_reset_sync_appregclk
        );

    sync_idlecnt: xpm_cdc_array_single
        generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0,
            WIDTH => 14
        )
        port map(
            src_clk => '0',
            src_in => register_map_control.FE_EMU_LOGIC.IDLES(31 downto 18),
            dest_clk => clk240,
            dest_out => idleCount
        );

    sync_chunksize: xpm_cdc_array_single
        generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0,
            WIDTH => 14
        )
        port map(
            src_clk => '0',
            src_in => register_map_control.FE_EMU_LOGIC.CHUNK_LENGTH(15 downto 2),
            dest_clk => clk240,
            dest_out => chunkSize
        );

    sync_use_logic_emu: xpm_cdc_single
        generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => '0',
            src_in => register_map_control.FE_EMU_LOGIC.ENA(32),
            dest_clk => clk240,
            dest_out => useLogicEmu
        );

    selectRamOrLogic: process(FMdout_logic_s, FMdout_ram_s, useLogicEmu)
    begin
        if useLogicEmu = '1' then
            FMdout_s <= FMdout_logic_s;
        else
            FMdout_s <= FMdout_ram_s;
        end if;
    end process;



    sync_l1a_triggered: xpm_cdc_single
        generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => '0',
            src_in => register_map_control.FE_EMU_LOGIC.L1A_TRIGGERED(33),
            dest_clk => clk240,
            dest_out => L1A_triggered
        );

    sync_l1a: xpm_cdc_single
        generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => '0',
            src_in => L1A_IN,
            dest_clk => clk240,
            dest_out => L1A6x
        );

    --Make sure a L1A can only occur once every 40 MHz clock cycle.
    process(clk240, daq_reset_sync)
        variable shift_6x: std_logic_vector(5 downto 0);
    begin
        if daq_reset_sync = '1' then
            L1A <= '0';
            shift_6x := "000001";
        elsif rising_edge(clk240) then
            if L1A6x = '1' and shift_6x(0) = '1' then
                L1A <= '1';
            else
                L1A <= '0';
            end if;
            shift_6x := shift_6x(4 downto 0) & shift_6x(5); --rotate
        end if;
    end process;



    --
    ---------------------------------------------------------------------------------------
    -- loading new contents
    ---------------------------------------------------------------------------------------
    process(appreg_clk, daq_reset_sync_appregclk)
    begin
        if daq_reset_sync_appregclk = '1' then
            wea(0)          <= '0';
            emuram_wrdata   <= "0001" & Kchar_comma & Kchar_comma & Kchar_comma & Kchar_comma;
            emuram_wraddr   <= "0000000000000";
        elsif rising_edge(appreg_clk) then
            wea(0)          <= register_map_control_appreg_clk.FE_EMU_CONFIG.WE(47); -- (0 to 6 are used for GBT emus)
            emuram_wraddr   <= register_map_control_appreg_clk.FE_EMU_CONFIG.WRADDR(45 downto 33);  -- 10 bit address
            emuram_wrdata   <= register_map_control_appreg_clk.FE_EMU_CONFIG.WRDATA(32) &
                               register_map_control_appreg_clk.FE_EMU_CONFIG.WRDATA(32) &
                               register_map_control_appreg_clk.FE_EMU_CONFIG.WRDATA(32) &
                               register_map_control_appreg_clk.FE_EMU_CONFIG.WRDATA(32 downto 0);   -- 36 bit data
        end if;
    end process;
    --
    ---------------------------------------------------------------------------------------
    -- cdc
    ---------------------------------------------------------------------------------------

    sync_ena : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map (
            src_clk => '0',
            src_in => to_sl(register_map_control.FE_EMU_ENA.EMU_TOHOST),
            dest_clk => clk240,
            dest_out => ena
        );
    sync_path_ena : xpm_cdc_array_single
        generic map (
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0,
            WIDTH => GBT_NUM
        )
        port map (
            src_clk => '0',
            src_in => path_ena_40,
            dest_clk => clk240,
            dest_out => path_ena
        );

    g_path_ena_40: for i in 0 to GBT_NUM-1 generate
        path_ena_40(i) <= register_map_control.DECODING_EGROUP_CTRL(i)(0).EPATH_ENA(0) and to_sl(register_map_control.FE_EMU_ENA.EMU_TOHOST);
    end generate;

    ena_reg: process(clk240)
        variable ena_cnt: integer range 0 to 63;
    begin
        if rising_edge (clk240) then
            if daq_reset_sync = '1' then
                ena_cnt := 63;
                ena_r       <= '0';
                out_rdy     <= '0';
                out_rdy_r   <= '0';
            else
                if ena_cnt /= 0 then
                    ena_cnt := ena_cnt - 1; --Wait for FIFO reset to be done.
                    ena_r       <= '0';
                    out_rdy     <= '0';
                    out_rdy_r   <= '0';
                else
                    ena_r       <= ena;
                    out_rdy     <= ena_r;
                    out_rdy_r   <= out_rdy;
                end if;
            end if;
        end if;
    end process;
    --
    ---------------------------------------------------------------------------------------
    -- reading from enulator
    ---------------------------------------------------------------------------------------
    address_counter: process(clk240)
    begin
        if rising_edge (clk240) then
            if ena_r = '1' then
                emuram_rdaddr <= emuram_rdaddr + 1;
            else
                emuram_rdaddr <= (others => '0'); -- have to contain a comma
            end if;
        end if;
    end process;
    --
    ---------------------------------------------------------------------------------------
    -- emulator ram
    ---------------------------------------------------------------------------------------

    emuram_00 : xpm_memory_tdpram
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: USE_MEM_INIT_MMI, CASCADE_HEIGHT, SIM_ASSERT_CHK, WRITE_PROTECT"
            ADDR_WIDTH_A => 13,
            ADDR_WIDTH_B => 13,
            AUTO_SLEEP_TIME => 0,
            BYTE_WRITE_WIDTH_A => 36,
            BYTE_WRITE_WIDTH_B => 36,
            CLOCKING_MODE => "independent_clock",
            ECC_MODE => "no_ecc",
            MEMORY_INIT_FILE => "fmemuram.mem",
            MEMORY_INIT_PARAM => "0",
            MEMORY_OPTIMIZATION => "true",
            MEMORY_PRIMITIVE => "auto",
            MEMORY_SIZE => 8192*36,
            MESSAGE_CONTROL => 0,
            READ_DATA_WIDTH_A => 36,
            READ_DATA_WIDTH_B => 36,
            READ_LATENCY_A => 2,
            READ_LATENCY_B => 2,
            READ_RESET_VALUE_A => "0",
            READ_RESET_VALUE_B => "0",
            RST_MODE_A => "SYNC",
            RST_MODE_B => "SYNC",
            USE_EMBEDDED_CONSTRAINT => 0,
            USE_MEM_INIT => 1,
            WAKEUP_TIME => "disable_sleep",
            WRITE_DATA_WIDTH_A => 36,
            WRITE_DATA_WIDTH_B => 36,
            WRITE_MODE_A => "no_change",
            WRITE_MODE_B => "no_change"
        )
        port map (
            sleep => '0',
            clka => appreg_clk,
            rsta => daq_reset_sync_appregclk,
            ena => '1',
            regcea => '1',
            wea => wea,
            addra => emuram_wraddr,
            dina => emuram_wrdata,
            injectsbiterra => '0',
            injectdbiterra => '0',
            douta => emuram_douta,
            sbiterra => open,
            dbiterra => open,
            clkb => clk240,
            rstb => daq_reset_sync,
            enb => '1',
            regceb => '1',
            web => "0",
            addrb => emuram_rdaddr,
            dinb => (others => '0'),
            injectsbiterrb => '0',
            injectdbiterrb => '0',
            doutb => emuram_rddata,
            sbiterrb => open,
            dbiterrb => open
        );



    register_map_gbtemu_monitor.FE_EMU_READ.DATA <= emuram_douta(32 downto 0);
    --
    out_sel: process(clk240)
    begin
        if rising_edge (clk240) then
            if out_rdy_r = '1' then
                FMdout_ram_s <= emuram_rddata(32 downto 0);
            else
                FMdout_ram_s <= '1' & x"000000" & Kchar_comma;
            end if;
        end if;
    end process;


    --This version is intended for simulation, because it takes a lot of time to change properties of the blockram model everytime you want to change a parameter
    --In therory it could be used on a real device as well, but in that case some constants must be tied to registers.
    logic_emu: process(clk240)
        variable L1ID: std_logic_vector(31 downto 0);
        variable clen: std_logic_vector(15 downto 0);
        variable clenW: std_logic_vector(15 downto 0);
        variable datacnt: std_logic_vector(7 downto 0);
        variable L1A_count: integer range 0 to 255;
    begin
        if rising_edge(clk240) then
            if daq_reset_sync = '1' or ena_r = '0' then
                FMdout_p0_s <= '1' & x"000000" & Kchar_comma;
                FMdout_p1_s <= '1' & x"000000" & Kchar_comma;
                FMdout_logic_s <= '1' & x"000000" & Kchar_comma;
                cnt <= (others => '0');
                L1ID := (others => '0');
                crc_calc <= '0';
                crc_start <= '0';
                datacnt := x"00";
                L1A_count := 0;
            else
                if L1A = '1' and L1A_triggered = '1' then --When a L1A trigger occurs, increment a counter, to queue chunks until it reaches 0 again.
                    if L1A_count /= 255 then
                        L1A_count := L1A_count + 1;
                    end if;
                end if;
                cnt <= cnt + 1;
                if cnt = x"0000" then --SOP
                    crc_calc <= '0';
                    crc_start <= '0';
                    if(L1A_triggered = '1') then
                        if L1A_count /= 0 then
                            FMdout_p0_s <= '1' & x"000000" & Kchar_sop;
                            L1A_count := L1A_count - 1;
                        else
                            FMdout_p0_s <= '1' & x"000000" & Kchar_comma;
                            cnt <= (others => '0');
                        end if;
                    else
                        FMdout_p0_s <= '1' & x"000000" & Kchar_sop;
                    end if;
                end if;
                if cnt = x"0001" then --First word of header
                    crc_start <= '1';
                    crc_calc <= '1';
                    clenW := chunkSize-x"0002"; --excluding 8 byte header
                    clen := clenW(13 downto 0) & "00"; --in Bytes
                    FMdout_p0_s <= '0' & L1ID(15 downto 8) & clen(7 downto 0) & x"0" & clen(11 downto 8) & x"AA";
                end if;
                if cnt = 2 then --Second word of header
                    crc_start <= '0';
                    crc_calc <= '1';
                    FMdout_p0_s <= '0' & x"10" & x"AA" & x"BB"  & L1ID(7 downto 0);
                    L1ID := L1ID + 1;
                    datacnt := x"00";
                end if;
                if cnt > 2 and cnt < (1+to_integer(unsigned(chunkSize))) then --data
                    crc_start <= '0';
                    crc_calc <= '1';
                    FMdout_p0_s(7 downto 0) <= datacnt;
                    datacnt := datacnt + 1;

                    FMdout_p0_s(15 downto 8) <= datacnt;
                    datacnt := datacnt + 1;

                    FMdout_p0_s(23 downto 16) <= datacnt;
                    datacnt := datacnt + 1;

                    FMdout_p0_s(31 downto 24) <= datacnt;
                    datacnt := datacnt + 1;

                    FMdout_p0_s(32) <= '0';
                end if;
                if cnt = (x"0001"+unsigned(chunkSize)) then  --End of packet
                    crc_start <= '0';
                    crc_calc <= '0';
                    FMdout_p0_s <= '1' & x"000000" & Kchar_eop;
                    if(L1A_triggered = '1') then --Stop counting and start waiting for an L1A
                        cnt <= (others => '0');
                    end if;
                end if;
                if cnt > (1+unsigned(chunkSize)) and cnt < (1+unsigned(chunkSize)+unsigned(idleCount)) then
                    crc_start <= '0';
                    crc_calc <= '0';
                    FMdout_p0_s <= '1' & Kchar_comma & Kchar_comma & Kchar_comma & Kchar_comma;
                end if;
                if cnt >= (x"0001"+unsigned(chunkSize)+unsigned(idleCount)) then
                    crc_start <= '0';
                    crc_calc <= '0';
                    FMdout_p0_s <= '1' & Kchar_comma & Kchar_comma & Kchar_comma & Kchar_comma;
                    cnt <= (others => '0');
                end if;
                FMdout_p1_s <= FMdout_p0_s;
                FMdout_logic_s <= FMdout_p1_s;
                if(FMdout_p1_s(32) = '1' and FMdout_p1_s(7 downto 0) = Kchar_eop) then
                    FMdout_logic_s(27 downto 8) <= crc_out;
                end if;

            end if;
        end if;
    end process;

    -- CRC module takes 2 clock cycles to calculate CRC
    crc20_0: entity work.CRC
        port map(
            CRC   => crc_out,  --in sync with din_r
            Calc  => crc_calc,
            Clk   => clk240,
            Din   => FMdout_p0_s(31 downto 0),
            Reset => crc_start);



    -- Fifo to cross clock domain to clk250.
    g_FIFOS: for i in 0 to GBT_NUM-1 generate
        signal fullmode_FE_BUSY: std_logic;
    begin
        FE_BUSY_SYNC: xpm_cdc_single generic map(
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            ) port map(
                src_clk => clk240,
                src_in => fullmode_FE_BUSY,
                dest_clk => clk40,
                dest_out => FE_BUSY_out(i)(0)
            );

        ToAxi0: entity work.FullToAxis
            generic map(
                BLOCKSIZE => BLOCKSIZE,
                VERSAL => VERSAL
            )
            port map(
                clk240 => clk240,
                FMdin => FMdout_s,
                FMdin_is_big_endian => to_sl(register_map_control.DECODING_ENDIANNESS_FULL_MODE),
                --LinkAligned       => FMdout_rdy_s,
                aclk => aclk,
                daq_reset => daq_reset_sync,
                daq_fifo_flush => daq_fifo_flush_sync,
                m_axis => m_axis(i,0),
                m_axis_tready => m_axis_tready(i,0),
                m_axis_prog_empty => m_axis_prog_empty(i,0),
                FE_BUSY_out => fullmode_FE_BUSY,
                path_ena => path_ena(i),
                super_chunk_factor_link => register_map_control.SUPER_CHUNK_FACTOR_LINK(i),
                Use32bSOP => register_map_control.FULLMODE_32B_SOP(0)
            );

        g_AdditionalFullModeForDUNE: if AddFULLMODEForDUNE generate
            ToAxi0: entity work.FullToAxis
                generic map(
                    BLOCKSIZE => BLOCKSIZE,
                    VERSAL => VERSAL
                )
                port map(
                    clk240 => clk240,
                    FMdin => FMdout_s,
                    FMdin_is_big_endian => to_sl(register_map_control.DECODING_ENDIANNESS_FULL_MODE),
                    --LinkAligned       => FMdout_rdy_s,
                    aclk => aclk,
                    daq_reset => daq_reset_sync,
                    daq_fifo_flush => daq_fifo_flush_sync,
                    m_axis => m_axis_noSC(i,0),
                    m_axis_tready => m_axis_noSC_tready(i,0),
                    m_axis_prog_empty => m_axis_noSC_prog_empty(i,0),
                    FE_BUSY_out => open,
                    path_ena => path_ena(i),
                    super_chunk_factor_link => x"01",
                    Use32bSOP => register_map_control.FULLMODE_32B_SOP(0)
                );

        end generate;
    end generate;


end Behavioral;


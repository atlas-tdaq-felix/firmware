-- This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
-- Copyright (C) 2001-2022 CERN for the benefit of the ATLAS collaboration.
-- Authors:
--               Marius Wensing
--
--   Licensed under the Apache License, Version 2.0 (the "License");
--   you may not use this file except in compliance with the License.
--   You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--   Unless required by applicable law or agreed to in writing, software
--   distributed under the License is distributed on an "AS IS" BASIS,
--   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--   See the License for the specific language governing permissions and
--   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use work.axi_stream_package.ALL;

entity HGTDLumiAggregatorChannel is
    port (
        clk : in std_logic;
        rst : in std_logic;

        data : in std_logic_vector(15 downto 0);
        m_axis : out axis_32_type;
        m_axis_tready : in std_logic;
        synced : out std_logic;
        align_seq : in std_logic_vector(15 downto 0);

        -- interface to DPRAM
        dpram_rddata : in std_logic_vector(31 downto 0);
        dpram_wrdata : out std_logic_vector(31 downto 0);

        conf_lhcturns : in std_logic_vector(9 downto 0)
    );
end HGTDLumiAggregatorChannel;

architecture rtl of HGTDLumiAggregatorChannel is
    constant AXIS_NULL : axis_32_type := (
                                           tdata => x"00000000",
                                           tvalid => '0',
                                           tkeep => "1111",
                                           tlast => '0',
                                           tuser => (others => '0'),
                                           tid => (others => '0')
                                         );

    -- decoder
    signal dec_data : std_logic_vector(11 downto 0);
    signal dec_err : std_logic_vector(1 downto 0);
    --signal dec_isk : std_logic_vector(1 downto 0);
    signal dec_sync_seq_detect : std_logic;
    signal dec_data_r : std_logic_vector(11 downto 0);
    signal dec_err_r : std_logic_vector(1 downto 0);
    --signal dec_isk_r : std_logic_vector(1 downto 0);
    signal dec_sync_seq_detect_r : std_logic;

    signal synced_i : std_logic;
    signal first_turn : std_logic;
    signal bcid_counter : integer range 0 to 3563;
    signal turn_counter : integer range 0 to 1023;
    signal misalign_counter : integer range 0 to 255;
    signal error_counter : integer range 0 to 4095;
    signal misalign_counter_last : integer range 0 to 255;
    signal error_counter_last : integer range 0 to 4095;
    signal desync_counter : integer range 0 to 15;
    signal in_packet : std_logic;
    signal packet_truncated : std_logic;
begin

    -- instantiate both decoders
    dec_lsb: entity work.decoder_6b8b
        port map (
            data_in => data(7 downto 0),
            data_out => dec_data(5 downto 0),
            data_isk => open, --dec_isk(0),
            data_err => dec_err(0)
        );

    dec_msb: entity work.decoder_6b8b
        port map (
            data_in => data(15 downto 8),
            data_out => dec_data(11 downto 6),
            data_isk => open, --dec_isk(1),
            data_err => dec_err(1)
        );

    -- detect sync sequence
    dec_sync_seq_detect <= '1' when data = align_seq else '0';

    -- sync output
    synced <= synced_i;

    -- register decoder outputs (for timing)
    process (clk) begin
        if rising_edge(clk) then
            dec_data_r <= dec_data;
            dec_err_r <= dec_err;
            --dec_isk_r <= dec_isk;
            dec_sync_seq_detect_r <= dec_sync_seq_detect;
        end if;
    end process;

    -- aggregate and count
    process (clk, rst)
        variable W1old : unsigned(16 downto 0);
        variable W1sum : unsigned(16 downto 0);
        variable W2old : unsigned(14 downto 0);
        variable W2sum : unsigned(14 downto 0);
        variable W1add : unsigned(6 downto 0);
        variable W2add : unsigned(4 downto 0);
    begin
        if rst = '1' then
            synced_i <= '0';
            bcid_counter <= 0;
            turn_counter <= 0;
            error_counter <= 0;
            misalign_counter <= 0;
            error_counter_last <= 0;
            misalign_counter_last <= 0;
            dpram_wrdata <= (others => '0');
            desync_counter <= 0;
            first_turn <= '1';
        else
            if rising_edge(clk) then
                if synced_i = '0' then
                    -- wait until we have found the align_seq
                    if dec_sync_seq_detect_r = '1' then
                        synced_i <= '1';
                        first_turn <= '1';
                        bcid_counter <= 1;      -- after the sync word the next bcid will be 1 (always)
                    end if;
                else
                    -- simply count turns and BCID
                    if bcid_counter = 3563 then
                        bcid_counter <= 0;
                        if turn_counter = to_integer(unsigned(conf_lhcturns)) - 1 then
                            turn_counter <= 0;
                        else
                            turn_counter <= turn_counter + 1;
                            first_turn <= '0';
                        end if;
                    else
                        bcid_counter <= bcid_counter + 1;
                    end if;

                    -- check for misalignment and errors
                    if (bcid_counter = 0) then
                        if dec_sync_seq_detect_r = '0' then
                            if desync_counter = 15 then
                                synced_i <= '0';      -- desynchronize the channel after 16 lost syncs in a row
                                desync_counter <= 0;
                            else
                                desync_counter <= desync_counter + 1;
                            end if;

                            if misalign_counter < 255 then
                                misalign_counter <= misalign_counter + 1;
                            end if;
                        else
                            desync_counter <= 0;
                        end if;
                    end if;
                    if dec_err_r /= "00" then
                        error_counter <= error_counter + 1;      -- question: counting "word" errors or byte errors? (i.e. +0/+1/+2)???
                    end if;

                    -- reset counters but save their last value to send out to DAQ
                    if (turn_counter = 0) and (bcid_counter = 0) then
                        misalign_counter_last <= misalign_counter;
                        error_counter_last <= error_counter;
                        if dec_sync_seq_detect_r = '0' then
                            misalign_counter <= 1;
                        else
                            misalign_counter <= 0;
                        end if;
                        if dec_err_r /= "00" then
                            error_counter <= 1;
                        else
                            error_counter <= 0;
                        end if;
                    end if;

                    -- sum up data
                    if bcid_counter > 0 then
                        if turn_counter = 0 then
                            -- in the first LHC turn we empty the old value
                            W1old := (others => '0');
                            W2old := (others => '0');
                        else
                            W1old := unsigned(dpram_rddata(31 downto 15));
                            W2old := unsigned(dpram_rddata(14 downto 0));
                        end if;

                        if dec_err_r /= "00" then --check if word is corrupted
                            W1add := "0000000";
                            W2add := "00000";
                        else
                            W1add := unsigned(dec_data_r(6 downto 0));
                            W2add := unsigned(dec_data_r(11 downto 7));
                        end if;

                        --W1add := unsigned(dec_data_r(6 downto 0));
                        --W2add := unsigned(dec_data_r(11 downto 7));

                        W1sum := W1old + resize(W1add, 17);
                        W2sum := W2old + resize(W2add, 15);

                        dpram_wrdata <= std_logic_vector(W1sum) & std_logic_vector(W2sum);
                    else
                        dpram_wrdata <= x"DEADBEEF";
                    end if;
                end if;
            end if;
        end if;
    end process;

    -- data output process
    -- TODO: the truncation flag handling does NOT capture all possible situations (e.g. tready=0 in last transaction), need to improve that
    process (clk, rst) begin
        if rst = '1' then
            m_axis <= AXIS_NULL;
            in_packet <= '0';
            packet_truncated <= '0';
        else
            if rising_edge(clk) then
                -- only output if we are synced
                if (synced_i = '1') and (first_turn = '0') then
                    if turn_counter = 0 then
                        if bcid_counter = 0 then
                            -- memorize that we are sending a packet
                            in_packet <= '1';

                            -- packet header
                            m_axis.tdata <= "10" &
                                            std_logic_vector(to_unsigned(misalign_counter_last, 8)) &
                                            std_logic_vector(to_unsigned(error_counter_last, 12)) &
                                            conf_lhcturns;
                            m_axis.tvalid <= '1';
                            m_axis.tkeep <= "1111";
                            m_axis.tuser <= packet_truncated & "000";
                            m_axis.tlast <= '0';

                            -- set truncated flag if tready is low
                            if m_axis_tready = '0' then
                                packet_truncated <= '1';
                            end if;
                        else
                            m_axis.tdata <= dpram_rddata;
                            m_axis.tvalid <= '1';
                            m_axis.tkeep <= "1111";
                            m_axis.tuser <= packet_truncated & "000";
                            m_axis.tlast <= '0';

                            -- set truncated flag if tready is low
                            if m_axis_tready = '0' then
                                packet_truncated <= '1';
                            end if;
                        end if;
                    elsif (turn_counter = 1) and (bcid_counter = 0) and (in_packet = '1') then
                        -- finalize packet
                        m_axis.tdata <= (others => '0');
                        m_axis.tvalid <= '1';
                        m_axis.tkeep <= "1111";
                        m_axis.tuser <= packet_truncated & "000";
                        m_axis.tlast <= '1';
                        in_packet <= '0';

                        -- reset truncated flag
                        packet_truncated <= '0';
                    else
                        m_axis <= AXIS_NULL;
                    end if;
                else
                    m_axis <= AXIS_NULL;
                end if;
            end if;
        end if;
    end process;

end architecture;

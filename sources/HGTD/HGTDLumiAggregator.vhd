-- This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
-- Copyright (C) 2001-2022 CERN for the benefit of the ATLAS collaboration.
-- Authors:
--               Marius Wensing
--
--   Licensed under the Apache License, Version 2.0 (the "License");
--   you may not use this file except in compliance with the License.
--   You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--   Unless required by applicable law or agreed to in writing, software
--   distributed under the License is distributed on an "AS IS" BASIS,
--   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--   See the License for the specific language governing permissions and
--   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use work.axi_stream_package.ALL;

library XPM;
    use XPM.VCOMPONENTS.ALL;

entity HGTDLumiAggregator is
    generic (
        USE_BUILT_IN_FIFO : std_logic := '0';
        VERSAL : boolean := true;
        BLOCKSIZE : integer := 1024
    );
    port (
        clk : in std_logic;
        rst : in std_logic;

        align_seq : in std_logic_vector(15 downto 0);

        -- channel A
        chA_data : in std_logic_vector(15 downto 0);
        chA_enable : in std_logic;
        chA_m_axis_aclk : in std_logic;
        chA_m_axis : out axis_32_type;
        chA_m_axis_tready : in std_logic;
        chA_m_axis_prog_empty : out std_logic;
        chA_synced : out std_logic;

        -- channel B
        chB_data : in std_logic_vector(15 downto 0);
        chB_enable : in std_logic;
        chB_m_axis_aclk : in std_logic;
        chB_m_axis : out axis_32_type;
        chB_m_axis_tready : in std_logic;
        chB_m_axis_prog_empty : out std_logic;
        chB_synced : out std_logic;

        -- configuration
        conf_lhcturns : in std_logic_vector(9 downto 0)
    );
end HGTDLumiAggregator;

architecture rtl of HGTDLumiAggregator is
    -- channel resets
    signal chA_rst : std_logic;
    signal chB_rst : std_logic;

    -- dual-port memory ports
    constant DPRAM_RDADDR_OFFSET : integer := 3;
    signal dpram_wraddr : std_logic_vector(11 downto 0);    -- fit all BCs
    signal dpram_wrdata : std_logic_vector(63 downto 0);    -- memory for 20 Hz rate
    signal dpram_wren : std_logic;
    signal dpram_rdaddr : std_logic_vector(11 downto 0);
    signal dpram_rddata : std_logic_vector(63 downto 0);

    -- AXIS to FIFOs
    signal chA_fifo_axis : axis_32_type;
    signal chA_fifo_axis_tready : std_logic;
    signal chB_fifo_axis : axis_32_type;
    signal chB_fifo_axis_tready : std_logic;
begin

    chA_rst <= rst or not chA_enable;
    chB_rst <= rst or not chB_enable;

    -- DPRAM instance (shared between two decoders for efficient URAM usage)
    dpram: xpm_memory_sdpram
        generic map (
            ADDR_WIDTH_A => 12,
            ADDR_WIDTH_B => 12,
            AUTO_SLEEP_TIME => 0,
            BYTE_WRITE_WIDTH_A => 64,
            CASCADE_HEIGHT => 0,
            CLOCKING_MODE => "common_clock",
            ECC_MODE => "no_ecc",
            MEMORY_INIT_FILE => "none",
            MEMORY_INIT_PARAM => "0",
            MEMORY_OPTIMIZATION => "true",
            MEMORY_PRIMITIVE => "auto",
            MEMORY_SIZE => 262144,
            MESSAGE_CONTROL => 0,
            READ_DATA_WIDTH_B => 64,
            READ_LATENCY_B => 2,
            READ_RESET_VALUE_B => "0",
            RST_MODE_A => "SYNC",
            RST_MODE_B => "SYNC",
            SIM_ASSERT_CHK => 0,
            USE_EMBEDDED_CONSTRAINT => 0,
            USE_MEM_INIT => 1,
            WAKEUP_TIME => "disable_sleep",
            WRITE_DATA_WIDTH_A => 64,
            WRITE_MODE_B => "no_change"
        )
        port map (
            dbiterrb => open,
            doutb => dpram_rddata,
            sbiterrb => open,
            addra => dpram_wraddr,
            addrb => dpram_rdaddr,
            clka => clk,
            clkb => clk,
            dina => dpram_wrdata,
            ena => '1',
            enb => '1',
            injectdbiterra => '0',
            injectsbiterra => '0',
            regceb => '1',
            rstb => rst,
            sleep => '0',
            wea(0) => dpram_wren
        );

    -- memory address counters
    -- both address counters have a periodicity of 3564 (i.e. the LHC bunch number)
    -- to allow the decoder to sum up the lumi data the write address is usually a few
    -- (constant) clock cycles behind the read address
    process (clk, rst) begin
        if rst = '1' then
            dpram_wraddr <= (others => '0');
            dpram_rdaddr <= std_logic_vector(to_unsigned(DPRAM_RDADDR_OFFSET mod 3564, 12));
        else
            if rising_edge(clk) then
                -- increment read address
                if to_integer(unsigned(dpram_rdaddr)) = 3563 then
                    dpram_rdaddr <= (others => '0');
                else
                    dpram_rdaddr <= std_logic_vector(unsigned(dpram_rdaddr) + 1);
                end if;

                -- increment write address
                if to_integer(unsigned(dpram_wraddr)) = 3563 then
                    dpram_wraddr <= (others => '0');
                else
                    dpram_wraddr <= std_logic_vector(unsigned(dpram_wraddr) + 1);
                end if;
            end if;
        end if;
    end process;

    -- assign addresses
    dpram_wren <= '1';    -- we will write every clock cycle

    -- channel A
    chA: entity work.HGTDLumiAggregatorChannel
        port map (
            clk => clk,
            rst => chA_rst,
            align_seq => align_seq,
            data => chA_data,
            m_axis => chA_fifo_axis,
            m_axis_tready => chA_fifo_axis_tready,
            synced => chA_synced,
            dpram_rddata => dpram_rddata(31 downto 0),
            dpram_wrdata => dpram_wrdata(31 downto 0),
            conf_lhcturns => conf_lhcturns
        );

    chA_fifo: entity work.Axis32Fifo
        generic map (
            DEPTH => BLOCKSIZE/2,
            -- CLOCKING_MODE => "independent_clock",
            -- RELATED_CLOCKS => 0,
            -- FIFO_MEMORY_TYPE => "auto",
            -- PACKET_FIFO => "false",
            USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
            VERSAL => VERSAL,
            BLOCKSIZE => BLOCKSIZE,
            ISPIXEL => false
        )
        port map (
            s_axis_aresetn => not chA_rst,
            s_axis_aclk => clk,
            s_axis => chA_fifo_axis,
            s_axis_tready => chA_fifo_axis_tready,
            m_axis_aclk => chA_m_axis_aclk,
            m_axis => chA_m_axis,
            m_axis_tready => chA_m_axis_tready,
            m_axis_prog_empty => chA_m_axis_prog_empty
        );

    -- channel B
    chB: entity work.HGTDLumiAggregatorChannel
        port map (
            clk => clk,
            rst => chB_rst,
            align_seq => align_seq,
            data => chB_data,
            m_axis => chB_fifo_axis,
            m_axis_tready => chB_fifo_axis_tready,
            synced => chB_synced,
            dpram_rddata => dpram_rddata(63 downto 32),
            dpram_wrdata => dpram_wrdata(63 downto 32),
            conf_lhcturns => conf_lhcturns
        );

    chB_fifo: entity work.Axis32Fifo
        generic map (
            DEPTH => BLOCKSIZE/2,
            -- CLOCKING_MODE => "independent_clock",
            -- RELATED_CLOCKS => 0,
            -- FIFO_MEMORY_TYPE => "auto",
            -- PACKET_FIFO => "false",
            USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
            VERSAL => VERSAL,
            BLOCKSIZE => BLOCKSIZE,
            ISPIXEL => false
        )
        port map (
            s_axis_aresetn => not chB_rst,
            s_axis_aclk => clk,
            s_axis => chB_fifo_axis,
            s_axis_tready => chB_fifo_axis_tready,
            m_axis_aclk => chB_m_axis_aclk,
            m_axis => chB_m_axis,
            m_axis_tready => chB_m_axis_tready,
            m_axis_prog_empty => chB_m_axis_prog_empty
        );

end architecture;

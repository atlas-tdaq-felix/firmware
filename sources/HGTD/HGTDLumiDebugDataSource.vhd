--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use work.code6b8b_package.ALL;

entity HGTDLumiDebugDataSource is
    generic(
        N_IN : integer := 24; --for test case 4
        poly: std_logic_vector  := "110111001010110000101010"); --for test case 4
    port (

        clk : in std_logic;
        align_seq : in std_logic_vector(15 downto 0); --sync word
        data : out std_logic_vector(15 downto 0);

        ----the following inputs will be set by software in registers-----------------------
        test_case_selection : in std_logic_vector(3 downto 0); --10 test cases. HGTD_LUMI_EMULATOR_TEST_CASE
        W1_const : in std_logic_vector(6 downto 0); --for test case 0, 1, 3, 5, 6, 7, 8, 9. std_value = "0000001" = 0x1. HGTD_LUMI_EMULATOR_W1_CONST
        W2_const : in std_logic_vector(4 downto 0); --for test case 0, 1, 3, 5, 6, 7, 8, 9. std_value = "00001" = 0x1. HGTD_LUMI_EMULATOR_W2_CONST
        fault_bcid : in std_logic_vector(11 downto 0); --for test case 5, 6. says from which BCID we want the fault to occur. std_value = "001111101000" = 1000 = 0x3E8. HGTD_LUMI_EMULATOR_FAULT_BCID
        n_faults : in std_logic_vector(11 downto 0); --for test case 5, 6, 9. says for how many BCIDs/turns we want the fault to occur. std_value = "000000001010" = 10 = 0xA. HGTD_LUMI_EMULATOR_N_FAULTS
        corrupted_word : in std_logic_vector(15 downto 0); --for test case 6. std_value = ?. HGTD_LUMI_EMULATOR_CORRUPT_WORD
        n_turn : in std_logic_vector(9 downto 0); --for test case 7, 9. says at which turn we want the fault to occur. std_value = "0000000100" = 0x4. HGTD_LUMI_EMULATOR_N_TURN
        W1_const2 : in std_logic_vector(6 downto 0); --for test case 8. std_value = "0000010" = 0x2. HGTD_LUMI_EMULATOR_W1_CONST2
        W2_const2 : in std_logic_Vector(4 downto 0); --for test case 8. std_value = "00010" = 0x2. HGTD_LUMI_EMULATOR_W2_CONST2
        shift_bits : in std_logic_vector(3 downto 0); --for test case 9. std_value = "0010" = 0x2. HGTD_LUMI_EMULATOR_SHIFT_BITS
        num_turns : in std_logic_vector(9 downto 0) --from DECODING_HGTD_LUMI_CONF_LHC_TURNS
    );

end HGTDLumiDebugDataSource;

architecture rtl of HGTDLumiDebugDataSource is

    subtype BCID_RANGE is integer range 0 to 3563;
    signal bcid : BCID_RANGE := 0;
    signal lfsr_out: std_logic_vector(11 downto 0) := poly(0 to 11); --for test case 4
    signal lfsr_reg : std_logic_vector(N_IN-1 downto 0) := (0 => '1', 1=>'1', 2=>'1', 5=>'1', 7=>'1', 11=>'1', 13=>'1', 17=>'1', 19=>'1', 23=>'1', OTHERS =>'0'); --for test case 4
    signal current_turn : std_logic_vector(9 downto 0) := (others => '0'); --for test case 7, 9

begin

    process (clk)

        variable W1 : std_logic_vector(6 downto 0);
        variable W2 : std_logic_vector(4 downto 0);
        variable data_vect : std_logic_vector(15 downto 0);
        variable current_word : std_logic_vector(11 downto 0); -- test case 9
        variable removed_bits : std_logic_vector(14 downto 0); --test case 9, max shift is 15 bits, need to define like this because dynamic definition is not allowed
        variable current_16b_word : std_logic_vector(15 downto 0); --test case 10

    begin

        if rising_edge(clk) then

            case(to_integer(unsigned(test_case_selection))) is

                when 0 =>  -- 0 sends constants set by software
                    if bcid = 0 then
                        data_vect := align_seq; --send sync word at BCID 0
                    else
                        W1 := W1_const;
                        W2 := W2_const;
                        data_vect := encode_6b8b(W2 & W1(6), '0') & encode_6b8b(W1(5 downto 0), '0');
                    end if;

                when 1 =>  -- 1 sends increment counter from set values
                    if bcid = 0 then
                        data_vect := align_seq;
                        W1 := W1_const;
                        W2 := W2_const;
                    else
                        W1 := std_logic_vector(unsigned(W1) + 1);
                        W2 := std_logic_vector(unsigned(W2) + 1);
                        data_vect := encode_6b8b(W2 & W1(6), '0') & encode_6b8b(W1(5 downto 0), '0');
                    end if;

                when 2 =>  -- 2 sends BCID
                    if bcid = 0 then
                        data_vect := align_seq;
                    else
                        W1 := std_logic_vector(to_unsigned(bcid mod 128, W1'length)); -- W1 uses the lower 7 bits
                        W2 := std_logic_vector(to_unsigned(bcid / 128, W2'length));  -- W2 uses the upper 5 bits
                        data_vect := encode_6b8b(W2 & W1(6), '0') & encode_6b8b(W1(5 downto 0), '0');
                    end if;

                when 3 =>  -- 3 sends constant for half of the BCIDs and increments for the other half
                    if bcid = 0 then
                        data_vect := align_seq;
                    elsif bcid >=1 and bcid <= (BCID_RANGE'high /2) then --1781
                        W1 := W1_const;
                        W2 := W2_const;
                        data_vect := encode_6b8b(W2 & W1(6), '0') & encode_6b8b(W1(5 downto 0), '0');
                    else
                        W1 := std_logic_vector(unsigned(W1) + 1);
                        W2 := std_logic_vector(unsigned(W2) + 1);
                        data_vect := encode_6b8b(W2 & W1(6), '0') & encode_6b8b(W1(5 downto 0), '0');
                    end if;

                when 4 =>  -- 4 sends random
                    if bcid = 0 then
                        data_vect := align_seq;
                    else
                        for I in 1 to N_IN-1 loop
                            if poly(I) = '1' then
                                lfsr_reg(I) <= lfsr_reg(I-1) xor lfsr_reg(N_IN-1);
                            else
                                lfsr_reg(I) <= lfsr_reg(I-1);
                            end if;
                        end loop;
                        lfsr_reg(0) <= lfsr_reg(N_IN-1);
                        for I in 0 to 11 loop
                            lfsr_out(I) <= lfsr_reg(I);
                        end loop;
                        W1 := lfsr_out(6 downto 0); --value from previous cycle
                        W2 := lfsr_out(11 downto 7); --value from previous cycle
                        data_vect := encode_6b8b(W2 & W1(6), '0') & encode_6b8b(W1(5 downto 0), '0');
                    end if;

                when 5 =>  -- 5 sends sync at wrong BCIDs
                    if bcid = 0 then
                        data_vect := align_seq;
                    elsif bcid >= to_integer(unsigned(fault_bcid)) and bcid < to_integer(unsigned(fault_bcid)) + to_integer(unsigned(n_faults)) then
                        data_vect := align_seq;
                    else
                        W1 := W1_const;
                        W2 := W2_const;
                        data_vect := encode_6b8b(W2 & W1(6), '0') & encode_6b8b(W1(5 downto 0), '0');
                    end if;

                when 6 => -- 6 sends corrupt encoded words for certain BCIDs for all turns
                    if bcid = 0 then
                        data_vect := align_seq;
                    elsif bcid >= to_integer(unsigned(fault_bcid)) and bcid < to_integer(unsigned(fault_bcid)) + to_integer(unsigned(n_faults)) then
                        data_vect := corrupted_word; --no encoding
                    else
                        W1 := W1_const;
                        W2 := W2_const;
                        data_vect := encode_6b8b(W2 & W1(6), '0') & encode_6b8b(W1(5 downto 0), '0');
                    end if;

                when 7 => -- 7 sends word instead of idle after tot LHC_TURNS
                    if bcid = 0 then
                        if to_integer(unsigned(current_turn)) >= to_integer(unsigned(n_turn)) and to_integer(unsigned(current_turn)) < to_integer(unsigned(n_turn)) + to_integer(unsigned(n_faults)) then
                            W1 := W1_const;
                            W2 := W2_const;
                            data_vect := encode_6b8b(W2 & W1(6), '0') & encode_6b8b(W1(5 downto 0), '0');
                        else
                            data_vect := align_seq;
                        end if;
                        if unsigned(current_turn) /= 0 then
                            current_turn <= std_logic_vector(unsigned(current_turn) + 1);
                        end if;
                    else
                        W1 := W1_const;
                        W2 := W2_const;
                        data_vect := encode_6b8b(W2 & W1(6), '0') & encode_6b8b(W1(5 downto 0), '0');
                    end if;

                when 8 => --8 sends 2 constant values half and half
                    if bcid = 0 then
                        data_vect := align_seq;
                    elsif bcid >=1 and bcid <= (BCID_RANGE'high /2) then
                        W1 := W1_const;
                        W2 := W2_const;
                        data_vect := encode_6b8b(W2 & W1(6), '0') & encode_6b8b(W1(5 downto 0), '0');
                    else
                        W1 := W1_const2;
                        W2 := W2_const2;
                        data_vect := encode_6b8b(W2 & W1(6), '0') & encode_6b8b(W1(5 downto 0), '0');
                    end if;

                when 9 => -- 9 shift sync and sends constants, firstly encode and then shift
                    if to_integer(unsigned(current_turn)) >= to_integer(unsigned(n_turn)) and to_integer(unsigned(current_turn)) < to_integer(unsigned(n_turn)) + to_integer(unsigned(n_faults)) then
                        if bcid = 0 then
                            if to_integer(unsigned(current_turn)) = to_integer(unsigned(n_turn)) then
                                current_16b_word := align_seq;
                                removed_bits(to_integer(unsigned(shift_bits))-1 downto 0) := current_16b_word(to_integer(unsigned(shift_bits))-1 downto 0);
                                data_vect := current_16b_word srl to_integer(unsigned(shift_bits));
                            else
                                data_vect := removed_bits(to_integer(unsigned(shift_bits))-1 downto 0) & align_seq(15 downto to_integer(unsigned(shift_bits))); --removed bits coming from BCID 3563 of the previous turn
                                removed_bits(to_integer(unsigned(shift_bits))-1 downto 0) := align_seq(to_integer(unsigned(shift_bits))-1 downto 0);
                            end if;
                            current_turn <= std_logic_vector(unsigned(current_turn) + 1);
                        else
                            W1 := W1_const;
                            W2 := W2_const;
                            current_16b_word := encode_6b8b(W2 & W1(6), '0') & encode_6b8b(W1(5 downto 0), '0');
                            data_vect := removed_bits(to_integer(unsigned(shift_bits))-1 downto 0) & current_16b_word(15 downto to_integer(unsigned(shift_bits)));
                            removed_bits(to_integer(unsigned(shift_bits))-1 downto 0) := current_16b_word(to_integer(unsigned(shift_bits))-1 downto 0);
                        end if;
                    else
                        if bcid = 0 then
                            data_vect := align_seq;
                            current_turn <= std_logic_vector(unsigned(current_turn) + 1);
                        else
                            W1 := W1_const;
                            W2 := W2_const;
                            data_vect := encode_6b8b(W2 & W1(6), '0') & encode_6b8b(W1(5 downto 0), '0');
                        end if;
                    end if;

                when 10 => -- 10 is a revision of 6 sends corrupt encoded words for certain BCIDs for certain turns
                    if bcid = 0 then
                        data_vect := align_seq;
                        if unsigned(current_turn) /= 0 then
                            current_turn <= std_logic_vector(unsigned(current_turn) + 1);
                        end if;
                    elsif bcid >= to_integer(unsigned(fault_bcid)) and bcid < to_integer(unsigned(fault_bcid)) + to_integer(unsigned(n_faults)) then
                        if to_integer(unsigned(current_turn)) >= to_integer(unsigned(n_turn)) and to_integer(unsigned(current_turn)) < to_integer(unsigned(n_turn)) + to_integer(unsigned(n_faults)) then
                            data_vect := corrupted_word; --no encoding
                        else
                            W1 := W1_const;
                            W2 := W2_const;
                            data_vect := encode_6b8b(W2 & W1(6), '0') & encode_6b8b(W1(5 downto 0), '0');
                        end if;
                    else
                        W1 := W1_const;
                        W2 := W2_const;
                        data_vect := encode_6b8b(W2 & W1(6), '0') & encode_6b8b(W1(5 downto 0), '0');
                    end if;

                when others =>
                    if bcid = 0 then
                        data_vect := align_seq;
                    else
                        W1 := std_logic_vector(to_unsigned(bcid mod 128, 7));
                        W2 := std_logic_vector(to_unsigned(bcid mod 32, 5));
                        data_vect := encode_6b8b(W2 & W1(6), '0') & encode_6b8b(W1(5 downto 0), '0');
                    end if;

            end case;
            -- count BCID
            if bcid < 3563 then
                bcid <= bcid + 1;
            else
                bcid <= 0;
            end if;

            -- output data
            for I in 0 to 15 loop
                data(I) <= data_vect(I);
            end loop;

        end if;

    end process;


end architecture;

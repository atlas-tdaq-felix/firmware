--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

entity align_6b8b is
    port (
        clk : in std_logic;
        rst : in std_logic;

        reverse_bits : in std_logic;
        link_aligned : in std_logic;
        data_in : in std_logic_vector(15 downto 0);
        data_out : out std_logic_vector(15 downto 0);

        align_seq : in std_logic_vector(15 downto 0);
        aligned : out std_logic
    );
end align_6b8b;

architecture rtl of align_6b8b is
    signal data_in_rev : std_logic_vector(15 downto 0);
    signal shiftreg : std_logic_vector(30 downto 0);
    signal seq_found : std_logic_vector(15 downto 0);
    signal slot_use : std_logic_vector(15 downto 0);
begin
    -- reverse input bits
    process (data_in) begin
        for I in 0 to 15 loop
            data_in_rev(I) <= data_in(15-I);
        end loop;
    end process;

    -- shift in new data
    process (clk, rst) begin
        if rst = '1' then
            shiftreg <= (others => '0');
            slot_use <= "0000000000000000";
            aligned <= '0';
            data_out <= (others => '0');
        else
            if rising_edge(clk) then
                if link_aligned = '0' then
                    shiftreg <= (others => '0');
                    slot_use <= "0000000000000000";
                    data_out <= (others => '0');
                    aligned <= '0';
                else
                    if reverse_bits = '1' then
                        shiftreg <= shiftreg(14 downto 0) & data_in_rev;
                    else
                        shiftreg <= shiftreg(14 downto 0) & data_in;
                    end if;

                    if seq_found /= "0000000000000000" then
                        aligned <= '1';
                        slot_use <= seq_found;
                    end if;

                    case slot_use is
                        when "0000000000000001" => data_out <= shiftreg(15 downto 0);
                        when "0000000000000010" => data_out <= shiftreg(16 downto 1);
                        when "0000000000000100" => data_out <= shiftreg(17 downto 2);
                        when "0000000000001000" => data_out <= shiftreg(18 downto 3);
                        when "0000000000010000" => data_out <= shiftreg(19 downto 4);
                        when "0000000000100000" => data_out <= shiftreg(20 downto 5);
                        when "0000000001000000" => data_out <= shiftreg(21 downto 6);
                        when "0000000010000000" => data_out <= shiftreg(22 downto 7);
                        when "0000000100000000" => data_out <= shiftreg(23 downto 8);
                        when "0000001000000000" => data_out <= shiftreg(24 downto 9);
                        when "0000010000000000" => data_out <= shiftreg(25 downto 10);
                        when "0000100000000000" => data_out <= shiftreg(26 downto 11);
                        when "0001000000000000" => data_out <= shiftreg(27 downto 12);
                        when "0010000000000000" => data_out <= shiftreg(28 downto 13);
                        when "0100000000000000" => data_out <= shiftreg(29 downto 14);
                        when "1000000000000000" => data_out <= shiftreg(30 downto 15);
                        when others => data_out <= (others => '0');
                    end case;
                end if;
            end if;
        end if;
    end process;

    -- find sequence
    seq_compare: for I in 0 to 15 generate
        seq_found(I) <= '1' when shiftreg(15+I downto I) = align_seq else '0';
    end generate;

end architecture;

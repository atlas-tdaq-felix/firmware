-- This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
-- Copyright (C) 2001-2022 CERN for the benefit of the ATLAS collaboration.
-- Authors:
--               Marius Wensing
--
--   Licensed under the Apache License, Version 2.0 (the "License");
--   you may not use this file except in compliance with the License.
--   You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--   Unless required by applicable law or agreed to in writing, software
--   distributed under the License is distributed on an "AS IS" BASIS,
--   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--   See the License for the specific language governing permissions and
--   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use work.axi_stream_package.ALL;
    use work.FELIX_package.ALL;

library XPM;
    use XPM.VCOMPONENTS.ALL;

entity HGTDLumiEvent is
    generic (
        USE_BUILT_IN_FIFO : std_logic := '0';
        VERSAL : boolean := true;
        BLOCKSIZE : integer := 1024
    );
    port (
        clk : in std_logic;
        rst : in std_logic;

        align_seq : in std_logic_vector(15 downto 0);

        chA_enable : in std_logic;
        chA_data : in std_logic_vector(15 downto 0);
        chA_aligned : in std_logic;
        chB_enable : in std_logic;
        chB_data : in std_logic_vector(15 downto 0);
        chB_aligned : in std_logic;
        chC_enable : in std_logic;
        chC_data : in std_logic_vector(15 downto 0);
        chC_aligned : in std_logic;
        chD_enable : in std_logic;
        chD_data : in std_logic_vector(15 downto 0);
        chD_aligned : in std_logic;

        chA_m_axis : out axis_32_type;
        chA_m_axis_aclk : in std_logic;
        chA_m_axis_tready : in std_logic;
        chA_m_axis_prog_empty : out std_logic;
        chB_m_axis : out axis_32_type;
        chB_m_axis_aclk : in std_logic;
        chB_m_axis_tready : in std_logic;
        chB_m_axis_prog_empty : out std_logic;
        chC_m_axis : out axis_32_type;
        chC_m_axis_aclk : in std_logic;
        chC_m_axis_tready : in std_logic;
        chC_m_axis_prog_empty : out std_logic;
        chD_m_axis : out axis_32_type;
        chD_m_axis_aclk : in std_logic;
        chD_m_axis_tready : in std_logic;
        chD_m_axis_prog_empty : out std_logic;

        TTCin : in TTC_data_type;

        trig_latency : in std_logic_vector(8 downto 0)
    );
end HGTDLumiEvent;

architecture rtl of HGTDLumiEvent is
    constant AXIS_NULL : axis_32_type := (
                                           tdata => x"00000000",
                                           tvalid => '0',
                                           tkeep => "1111",
                                           tlast => '0',
                                           tuser => (others => '0'),
                                           tid => (others => '0')
                                         );

    signal dpram_rdaddr : std_logic_vector(8 downto 0);
    signal dpram_rddata : std_logic_vector(63 downto 0);
    signal dpram_wraddr : std_logic_vector(8 downto 0);
    signal dpram_wrdata : std_logic_vector(63 downto 0);
    signal dpram_wren : std_logic;

    signal trigfifo_din : std_logic_vector(109 downto 0);
    signal trigfifo_dout : std_logic_vector(109 downto 0);
    signal trigfifo_wren : std_logic;
    signal trigfifo_rden : std_logic;
    signal trigfifo_full : std_logic;
    signal trigfifo_empty : std_logic;

    -- AXIS to FIFOs
    signal chA_fifo_axis : axis_32_type;
    signal chA_fifo_axis_tready : std_logic; -- @suppress "signal chA_fifo_axis_tready is never read"
    signal chB_fifo_axis : axis_32_type;
    signal chB_fifo_axis_tready : std_logic; -- @suppress "signal chB_fifo_axis_tready is never read"
    signal chC_fifo_axis : axis_32_type;
    signal chC_fifo_axis_tready : std_logic; -- @suppress "signal chC_fifo_axis_tready is never read"
    signal chD_fifo_axis : axis_32_type;
    signal chD_fifo_axis_tready : std_logic; -- @suppress "signal chD_fifo_axis_tready is never read"

    signal output_generator_step : integer range 0 to 1 := 0;

    -- channel BCIDs
    signal chA_bcid : unsigned(9 downto 0);    -- we need 10 bits
    signal chB_bcid : unsigned(9 downto 0);    -- we need 10 bits
    signal chC_bcid : unsigned(9 downto 0);    -- we need 10 bits
    signal chD_bcid : unsigned(9 downto 0);    -- we need 10 bits
begin

    -- count channel BCIDs
    process (clk, rst) begin
        if rst = '1' then
            chA_bcid <= (others => '0');
            chB_bcid <= (others => '0');
            chC_bcid <= (others => '0');
            chD_bcid <= (others => '0');
        else
            if rising_edge(clk) then
                if chA_data = align_seq then
                    -- when we received the IDLE word the next BCID will be 1
                    chA_bcid <= to_unsigned(1, chA_bcid'length);
                else
                    -- otherwise just count up
                    chA_bcid <= chA_bcid + 1;
                end if;

                if chB_data = align_seq then
                    -- when we received the IDLE word the next BCID will be 1
                    chB_bcid <= to_unsigned(1, chB_bcid'length);
                else
                    -- otherwise just count up
                    chB_bcid <= chB_bcid + 1;
                end if;

                if chC_data = align_seq then
                    -- when we received the IDLE word the next BCID will be 1
                    chC_bcid <= to_unsigned(1, chC_bcid'length);
                else
                    -- otherwise just count up
                    chC_bcid <= chC_bcid + 1;
                end if;

                if chD_data = align_seq then
                    -- when we received the IDLE word the next BCID will be 1
                    chD_bcid <= to_unsigned(1, chD_bcid'length);
                else
                    -- otherwise just count up
                    chD_bcid <= chD_bcid + 1;
                end if;
            end if;
        end if;
    end process;

    -- dual-port memory for data storage
    dpram: xpm_memory_sdpram
        generic map (
            ADDR_WIDTH_A => 9,
            ADDR_WIDTH_B => 9,
            AUTO_SLEEP_TIME => 0,
            BYTE_WRITE_WIDTH_A => 64,
            CASCADE_HEIGHT => 0,
            CLOCKING_MODE => "common_clock",
            ECC_MODE => "no_ecc",
            MEMORY_INIT_FILE => "none",
            MEMORY_INIT_PARAM => "0",
            MEMORY_OPTIMIZATION => "true",
            MEMORY_PRIMITIVE => "auto",
            MEMORY_SIZE => 32768,
            MESSAGE_CONTROL => 0,
            READ_DATA_WIDTH_B => 64,
            READ_LATENCY_B => 1,
            READ_RESET_VALUE_B => "0",
            RST_MODE_A => "SYNC",
            RST_MODE_B => "SYNC",
            SIM_ASSERT_CHK => 0,
            USE_EMBEDDED_CONSTRAINT => 0,
            USE_MEM_INIT => 1,
            WAKEUP_TIME => "disable_sleep",
            WRITE_DATA_WIDTH_A => 64,
            WRITE_MODE_B => "no_change"
        )
        port map (
            dbiterrb => open,
            doutb => dpram_rddata,
            sbiterrb => open,
            addra => dpram_wraddr,
            addrb => dpram_rdaddr,
            clka => clk,
            clkb => clk,
            dina => dpram_wrdata,
            ena => '1',
            enb => '1',
            injectdbiterra => '0',
            injectsbiterra => '0',
            regceb => '1',
            rstb => rst,
            sleep => '0',
            wea(0) => dpram_wren
        );

    -- count read pointer
    process (clk, rst) begin
        if rst = '1' then
            dpram_rdaddr <= (others => '0');
        else
            if rising_edge(clk) then
                dpram_rdaddr <= std_logic_vector(unsigned(dpram_rdaddr) + 1);
            end if;
        end if;
    end process;

    -- write address is always trig_latency ahead
    dpram_wraddr <= std_logic_vector(unsigned(dpram_rdaddr) + unsigned(trig_latency));

    -- always write into circular buffer
    dpram_wrdata <= chD_data & chC_data & chB_data & chA_data;
    dpram_wren <= '1';

    -- trigger FIFO
    trigfifo: xpm_fifo_sync
        generic map (
            CASCADE_HEIGHT => 0,
            DOUT_RESET_VALUE => "0",
            ECC_MODE => "no_ecc",
            FIFO_MEMORY_TYPE => "auto",
            FIFO_READ_LATENCY => 0,
            FIFO_WRITE_DEPTH => 64,
            FULL_RESET_VALUE => 0,
            PROG_EMPTY_THRESH => 10,
            PROG_FULL_THRESH => 10,
            RD_DATA_COUNT_WIDTH => 1,
            READ_DATA_WIDTH => 110,
            READ_MODE => "fwft",
            SIM_ASSERT_CHK => 0,
            USE_ADV_FEATURES => "0707",
            WAKEUP_TIME => 0,
            WRITE_DATA_WIDTH => 110,
            WR_DATA_COUNT_WIDTH => 1
        )
        port map (
            almost_empty => open,
            almost_full => open,
            data_valid => open,
            dbiterr => open,
            dout => trigfifo_dout,
            empty => trigfifo_empty,
            full => trigfifo_full,
            overflow => open,
            prog_empty => open,
            prog_full => open,
            rd_data_count => open,
            rd_rst_busy => open,
            sbiterr => open,
            underflow => open,
            wr_ack => open,
            wr_data_count => open,
            wr_rst_busy => open,
            din => trigfifo_din,
            injectdbiterr => '0',
            injectsbiterr => '0',
            rd_en => trigfifo_rden,
            rst => rst,
            sleep => '0',
            wr_clk => clk,
            wr_en => trigfifo_wren
        );

    -- fill trigger FIFO when there is a trigger
    trigfifo_wren <= TTCin.L1A and not trigfifo_full;
    trigfifo_din <= std_logic_vector(chD_bcid) & dpram_rddata(63 downto 48) &      -- 10 + 16 = 26 bit
                    std_logic_vector(chC_bcid) & dpram_rddata(47 downto 32) &      -- 10 + 16 = 26 bit
                    std_logic_vector(chB_bcid) & dpram_rddata(31 downto 16) &      -- 10 + 16 = 26 bit
                    std_logic_vector(chA_bcid) & dpram_rddata(15 downto 0) &      -- 10 + 16 = 26 bit
                    TTCin.L1Id(5 downto 0);              -- 6 bit
    -- total: 110 bit

    -- event generator
    process (clk, rst) begin
        if rst = '1' then
            chA_fifo_axis <= AXIS_NULL;
            chB_fifo_axis <= AXIS_NULL;
            chC_fifo_axis <= AXIS_NULL;
            chD_fifo_axis <= AXIS_NULL;
            output_generator_step <= 0;
        else
            if rising_edge(clk) then
                -- whenever there is an event available
                if trigfifo_empty = '0' then
                    if output_generator_step = 0 then
                        chA_fifo_axis.tdata <= "10" & chA_aligned & "0000000000000" & trigfifo_dout(21 downto 6);
                        chB_fifo_axis.tdata <= "10" & chB_aligned & "0000000000000" & trigfifo_dout(47 downto 32);
                        chC_fifo_axis.tdata <= "10" & chC_aligned & "0000000000000" & trigfifo_dout(73 downto 58);
                        chD_fifo_axis.tdata <= "10" & chD_aligned & "0000000000000" & trigfifo_dout(99 downto 84);
                        chA_fifo_axis.tuser <= (others => '0');
                        chB_fifo_axis.tuser <= (others => '0');
                        chC_fifo_axis.tuser <= (others => '0');
                        chD_fifo_axis.tuser <= (others => '0');
                        chA_fifo_axis.tkeep <= "1111";
                        chB_fifo_axis.tkeep <= "1111";
                        chC_fifo_axis.tkeep <= "1111";
                        chD_fifo_axis.tkeep <= "1111";
                        chA_fifo_axis.tlast <= '0';
                        chB_fifo_axis.tlast <= '0';
                        chC_fifo_axis.tlast <= '0';
                        chD_fifo_axis.tlast <= '0';
                        chA_fifo_axis.tvalid <= chA_enable;
                        chB_fifo_axis.tvalid <= chB_enable;
                        chC_fifo_axis.tvalid <= chC_enable;
                        chD_fifo_axis.tvalid <= chD_enable;
                        output_generator_step <= 1;
                    elsif output_generator_step = 1 then
                        chA_fifo_axis.tdata <= x"0000" & trigfifo_dout(31 downto 22) & trigfifo_dout(5 downto 0);
                        chB_fifo_axis.tdata <= x"0000" & trigfifo_dout(57 downto 48) & trigfifo_dout(5 downto 0);
                        chC_fifo_axis.tdata <= x"0000" & trigfifo_dout(83 downto 74) & trigfifo_dout(5 downto 0);
                        chD_fifo_axis.tdata <= x"0000" & trigfifo_dout(109 downto 100) & trigfifo_dout(5 downto 0);
                        chA_fifo_axis.tuser <= (others => '0');
                        chB_fifo_axis.tuser <= (others => '0');
                        chC_fifo_axis.tuser <= (others => '0');
                        chD_fifo_axis.tuser <= (others => '0');
                        chA_fifo_axis.tkeep <= "0011";
                        chB_fifo_axis.tkeep <= "0011";
                        chC_fifo_axis.tkeep <= "0011";
                        chD_fifo_axis.tkeep <= "0011";
                        chA_fifo_axis.tlast <= '1';
                        chB_fifo_axis.tlast <= '1';
                        chC_fifo_axis.tlast <= '1';
                        chD_fifo_axis.tlast <= '1';
                        chA_fifo_axis.tvalid <= chA_enable;
                        chB_fifo_axis.tvalid <= chB_enable;
                        chC_fifo_axis.tvalid <= chC_enable;
                        chD_fifo_axis.tvalid <= chD_enable;
                        output_generator_step <= 0;
                    end if;
                else
                    chA_fifo_axis <= AXIS_NULL;
                    chB_fifo_axis <= AXIS_NULL;
                    chC_fifo_axis <= AXIS_NULL;
                    chD_fifo_axis <= AXIS_NULL;
                end if;
            end if;
        end if;
    end process;

    trigfifo_rden <= '1' when (trigfifo_empty = '0') and (output_generator_step = 1) else '0';

    -- AXIS FIFOs
    chA_fifo: entity work.Axis32Fifo
        generic map (
            DEPTH => BLOCKSIZE/2,
            -- CLOCKING_MODE => "independent_clock",
            -- RELATED_CLOCKS => 0,
            -- FIFO_MEMORY_TYPE => "auto",
            -- PACKET_FIFO => "false",
            USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
            VERSAL => VERSAL,
            BLOCKSIZE => BLOCKSIZE,
            ISPIXEL => false
        )
        port map (
            s_axis_aresetn => not rst and chA_enable,
            s_axis_aclk => clk,
            s_axis => chA_fifo_axis,
            s_axis_tready => chA_fifo_axis_tready,
            m_axis_aclk => chA_m_axis_aclk,
            m_axis => chA_m_axis,
            m_axis_tready => chA_m_axis_tready,
            m_axis_prog_empty => chA_m_axis_prog_empty
        );

    chB_fifo: entity work.Axis32Fifo
        generic map (
            DEPTH => BLOCKSIZE/2,
            -- CLOCKING_MODE => "independent_clock",
            -- RELATED_CLOCKS => 0,
            -- FIFO_MEMORY_TYPE => "auto",
            -- PACKET_FIFO => "false",
            USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
            VERSAL => VERSAL,
            BLOCKSIZE => BLOCKSIZE,
            ISPIXEL => false
        )
        port map (
            s_axis_aresetn => not rst and chB_enable,
            s_axis_aclk => clk,
            s_axis => chB_fifo_axis,
            s_axis_tready => chB_fifo_axis_tready,
            m_axis_aclk => chB_m_axis_aclk,
            m_axis => chB_m_axis,
            m_axis_tready => chB_m_axis_tready,
            m_axis_prog_empty => chB_m_axis_prog_empty
        );

    chC_fifo: entity work.Axis32Fifo
        generic map (
            DEPTH => BLOCKSIZE/2,
            -- CLOCKING_MODE => "independent_clock",
            -- RELATED_CLOCKS => 0,
            -- FIFO_MEMORY_TYPE => "auto",
            -- PACKET_FIFO => "false",
            USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
            VERSAL => VERSAL,
            BLOCKSIZE => BLOCKSIZE,
            ISPIXEL => false
        )
        port map (
            s_axis_aresetn => not rst and chC_enable,
            s_axis_aclk => clk,
            s_axis => chC_fifo_axis,
            s_axis_tready => chC_fifo_axis_tready,
            m_axis_aclk => chC_m_axis_aclk,
            m_axis => chC_m_axis,
            m_axis_tready => chC_m_axis_tready,
            m_axis_prog_empty => chC_m_axis_prog_empty
        );

    chD_fifo: entity work.Axis32Fifo
        generic map (
            DEPTH => BLOCKSIZE/2,
            -- CLOCKING_MODE => "independent_clock",
            -- RELATED_CLOCKS => 0,
            -- FIFO_MEMORY_TYPE => "auto",
            -- PACKET_FIFO => "false",
            USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
            VERSAL => VERSAL,
            BLOCKSIZE => BLOCKSIZE,
            ISPIXEL => false
        )
        port map (
            s_axis_aresetn => not rst and chD_enable,
            s_axis_aclk => clk,
            s_axis => chD_fifo_axis,
            s_axis_tready => chD_fifo_axis_tready,
            m_axis_aclk => chD_m_axis_aclk,
            m_axis => chD_m_axis,
            m_axis_tready => chD_m_axis_tready,
            m_axis_prog_empty => chD_m_axis_prog_empty
        );


end architecture;

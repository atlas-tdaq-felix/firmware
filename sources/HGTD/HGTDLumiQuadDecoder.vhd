-- This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
-- Copyright (C) 2001-2022 CERN for the benefit of the ATLAS collaboration.
-- Authors:
--               Marius Wensing
--
--   Licensed under the Apache License, Version 2.0 (the "License");
--   you may not use this file except in compliance with the License.
--   You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--   Unless required by applicable law or agreed to in writing, software
--   distributed under the License is distributed on an "AS IS" BASIS,
--   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--   See the License for the specific language governing permissions and
--   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use work.axi_stream_package.ALL;
    use work.FELIX_package.ALL;

entity HGTDLumiQuadDecoder is
    generic (
        USE_BUILT_IN_FIFO : std_logic := '0';
        VERSAL : boolean := true;
        BLOCKSIZE : integer := 1024;
        IMPLEMENT_DATASOURCE : boolean := true
    );
    port (
        clk : in std_logic;
        rst : in std_logic;

        -- lpGBT link aligned
        link_aligned : in std_logic;

        -- reverse input bits
        reverse_bits : in std_logic_vector(3 downto 0) := "0000";

        -- elink data (4 x 16 bit)
        elink_data : in std_logic_vector(63 downto 0);

        -- enable mask
        enable_aggregate : in std_logic_vector(3 downto 0);
        enable_event : in std_logic_vector(3 downto 0);

        -- align status
        aligned : out std_logic_vector(3 downto 0);

        -- AXI stream outputs
        aggregate_m_axis : out axis_32_array_type(3 downto 0);
        aggregate_m_axis_aclk : in std_logic;
        aggregate_m_axis_tready : in std_logic_vector(3 downto 0);
        aggregate_m_axis_prog_empty : out std_logic_vector(3 downto 0);
        event_m_axis : out axis_32_array_type(3 downto 0);
        event_m_axis_aclk : in std_logic;
        event_m_axis_tready : in std_logic_vector(3 downto 0);
        event_m_axis_prog_empty : out std_logic_vector(3 downto 0);

        TTCin : in TTC_data_type;

        align_seq : in std_logic_vector(15 downto 0);
        conf_lhcturns : in std_logic_vector(9 downto 0);
        trig_latency : in std_logic_vector(8 downto 0);
        enable_datasource : in std_logic;
        raw_mode : in std_logic; -- @suppress "Unused port: raw_mode is not used in work.HGTDLumiQuadDecoder(rtl)"

        test_case_selection : in std_logic_vector(3 downto 0); --from HGTD_LUMI_EMULATOR_TEST_CASE
        W1_const : in std_logic_vector(6 downto 0); --from HGTD_LUMI_EMULATOR_W1_CONST
        W2_const : in std_logic_vector(4 downto 0); --from HGTD_LUMI_EMULATOR_W2_CONST
        fault_bcid : in std_logic_vector(11 downto 0); --from HGTD_LUMI_EMULATOR_FAULT_BCID
        n_faults : in std_logic_vector(11 downto 0); --from HGTD_LUMI_EMULATOR_N_FAULTS
        corrupted_word : in std_logic_vector(15 downto 0); --from HGTD_LUMI_EMULATOR_CORRUPT_WORD
        n_turn : in std_logic_vector(9 downto 0); --from HGTD_LUMI_EMULATOR_N_TURN
        W1_const2 : in std_logic_vector(6 downto 0); --from HGTD_LUMI_EMULATOR_W1_CONST2
        W2_const2 : in std_logic_Vector(4 downto 0); --from HGTD_LUMI_EMULATOR_W2_CONST2
        shift_bits : in std_logic_vector(3 downto 0); --from HGTD_LUMI_EMULATOR_SHIFT_BITS
        num_turns : in std_logic_vector(9 downto 0) --from DECODING_HGTD_LUMI_CONF_LHC_TURNS

    );
end HGTDLumiQuadDecoder;

architecture rtl of HGTDLumiQuadDecoder is
    signal link_aligned_mux : std_logic;
    signal elink_data_mux : std_logic_vector(63 downto 0);
    signal elink_data_aligned : std_logic_vector(63 downto 0);
    signal elink_is_aligned : std_logic_vector(3 downto 0);
    signal elink_issynced : std_logic_vector(3 downto 0); -- @suppress "signal elink_issynced is never read"
    signal datasource_data : std_logic_vector(15 downto 0);
begin

    -- debug data source
    datasource_generate: if IMPLEMENT_DATASOURCE generate
        datasource: entity work.HGTDLumiDebugDataSource
            port map (
                clk => clk,
                align_seq => align_seq,
                data => datasource_data,
                test_case_selection => test_case_selection,
                W1_const => W1_const,
                W2_const => W2_const,
                fault_bcid => fault_bcid,
                n_faults => n_faults,
                corrupted_word => corrupted_word,
                n_turn => n_turn,
                W1_const2 => W1_const2,
                W2_const2 => W2_const2,
                shift_bits => shift_bits,
                num_turns => num_turns
            );
    end generate;

    -- mux
    process (clk) begin
        if rising_edge(clk) then
            if (enable_datasource = '1') and IMPLEMENT_DATASOURCE then
                elink_data_mux <= datasource_data & datasource_data & datasource_data & datasource_data;
                link_aligned_mux <= '1';
            else
                elink_data_mux <= elink_data;
                link_aligned_mux <= link_aligned;
            end if;
        end if;
    end process;

    align_generate: for I in 0 to 3 generate
        align_i: entity work.align_6b8b
            port map (
                clk => clk,
                rst => rst,
                reverse_bits => reverse_bits(I),
                link_aligned => link_aligned_mux,
                data_in => elink_data_mux(16*I+15 downto 16*I),
                data_out => elink_data_aligned(16*I+15 downto 16*I),
                align_seq => align_seq,
                aligned => elink_is_aligned(I)
            );
    end generate;

    aggregate0: entity work.HGTDLumiAggregator
        generic map (
            USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
            VERSAL => VERSAL,
            BLOCKSIZE => BLOCKSIZE
        )
        port map (
            clk => clk,
            rst => rst,
            align_seq => align_seq,
            chA_enable => enable_aggregate(0),
            chA_data => elink_data_aligned(15 downto 0),
            chA_m_axis_aclk => aggregate_m_axis_aclk,
            chA_m_axis => aggregate_m_axis(0),
            chA_m_axis_tready => aggregate_m_axis_tready(0),
            chA_m_axis_prog_empty => aggregate_m_axis_prog_empty(0),
            chA_synced => elink_issynced(0),
            chB_enable => enable_aggregate(1),
            chB_data => elink_data_aligned(31 downto 16),
            chB_m_axis_aclk => aggregate_m_axis_aclk,
            chB_m_axis => aggregate_m_axis(1),
            chB_m_axis_tready => aggregate_m_axis_tready(1),
            chB_m_axis_prog_empty => aggregate_m_axis_prog_empty(1),
            chB_synced => elink_issynced(1),
            conf_lhcturns => conf_lhcturns
        );

    aggregate1: entity work.HGTDLumiAggregator
        generic map (
            USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
            VERSAL => VERSAL,
            BLOCKSIZE => BLOCKSIZE
        )
        port map (
            clk => clk,
            rst => rst,
            align_seq => align_seq,
            chA_enable => enable_aggregate(2),
            chA_data => elink_data_aligned(47 downto 32),
            chA_m_axis_aclk => aggregate_m_axis_aclk,
            chA_m_axis => aggregate_m_axis(2),
            chA_m_axis_tready => aggregate_m_axis_tready(2),
            chA_m_axis_prog_empty => aggregate_m_axis_prog_empty(2),
            chA_synced => elink_issynced(2),
            chB_enable => enable_aggregate(3),
            chB_data => elink_data_aligned(63 downto 48),
            chB_m_axis_aclk => aggregate_m_axis_aclk,
            chB_m_axis => aggregate_m_axis(3),
            chB_m_axis_tready => aggregate_m_axis_tready(3),
            chB_m_axis_prog_empty => aggregate_m_axis_prog_empty(3),
            chB_synced => elink_issynced(3),
            conf_lhcturns => conf_lhcturns
        );

    event: entity work.HGTDLumiEvent
        generic map (
            USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
            VERSAL => VERSAL,
            BLOCKSIZE => BLOCKSIZE
        )
        port map (
            clk => clk,
            rst => rst,
            align_seq => align_seq,
            chA_enable => enable_event(0),
            chA_data => elink_data_aligned(15 downto 0),
            chA_aligned => elink_is_aligned(0),
            chB_enable => enable_event(1),
            chB_data => elink_data_aligned(31 downto 16),
            chB_aligned => elink_is_aligned(1),
            chC_enable => enable_event(2),
            chC_data => elink_data_aligned(47 downto 32),
            chC_aligned => elink_is_aligned(2),
            chD_enable => enable_event(3),
            chD_data => elink_data_aligned(63 downto 48),
            chD_aligned => elink_is_aligned(3),
            chA_m_axis => event_m_axis(0),
            chA_m_axis_aclk => event_m_axis_aclk,
            chA_m_axis_tready => event_m_axis_tready(0),
            chA_m_axis_prog_empty => event_m_axis_prog_empty(0),
            chB_m_axis => event_m_axis(1),
            chB_m_axis_aclk => event_m_axis_aclk,
            chB_m_axis_tready => event_m_axis_tready(1),
            chB_m_axis_prog_empty => event_m_axis_prog_empty(1),
            chC_m_axis => event_m_axis(2),
            chC_m_axis_aclk => event_m_axis_aclk,
            chC_m_axis_tready => event_m_axis_tready(2),
            chC_m_axis_prog_empty => event_m_axis_prog_empty(2),
            chD_m_axis => event_m_axis(3),
            chD_m_axis_aclk => event_m_axis_aclk,
            chD_m_axis_tready => event_m_axis_tready(3),
            chD_m_axis_prog_empty => event_m_axis_prog_empty(3),
            TTCin => TTCin,
            trig_latency => trig_latency
        );

    aligned <= elink_is_aligned;

end architecture;

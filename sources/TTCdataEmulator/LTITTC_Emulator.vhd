--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Israel Grayzman
--!               Thei Wijnen
--!               Alessandra Camplani
--!               Frans Schreuder
--!               Ohad Shaked
--!               Ali Skaf
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Design     : ttc_emulator_v2.1
-- Author     : Alessandra Camplani
-- Email      : alessandra.camplani@cern.ch
-- Created    : 22.01.2020
-- Revised by : Ali Skaf
-- Email      : ali.skaf@uni-goettingen.de
-- V2.1: provide OCR and long Bchannel support
-- Last edited; 27.06.2022

--------------------------------------------------------------------------------

--------------------------------------------------------------------------------

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use ieee.numeric_std_unsigned.all;

    use work.pcie_package.all;
    use work.FELIX_package.all;

Library xpm;
    use xpm.vcomponents.all;

entity LTITTC_Emulator is
    generic(
        ITK_TRIGTAG_MODE            : integer := -1    -- -1: not implemented, 0: RD53A, 1: ITkPixV1, 2: ABC*
    );
    port(
        Clock                   : in  std_logic;
        Reset                   : in  std_logic;
        register_map_control    : in  register_map_control_type;

        TTCout                  : out TTC_data_type;
        ttc2h_tag               : out std_logic_vector(7 downto 0);
        ttc2h_tag_valid         : out std_logic;
        BUSYIn                  : in std_logic
    );
end LTITTC_Emulator;

architecture Behavioral of LTITTC_Emulator is

    signal      input_tpulse_period    : unsigned(31 downto 0) := to_unsigned(64, 32); --default value is 64;
    -- (unused) signal      input_tpulse_period_r  : unsigned(31 downto 0);
    signal      input_l1a_period    : unsigned(31 downto 0);
    -- (unused) signal      input_l1a_period_r  : unsigned(31 downto 0);
    signal      input_ecr_period    : unsigned(31 downto 0);
    signal      input_ecr_period_r  : unsigned(31 downto 0);
    signal      input_bcr_period    : unsigned(31 downto 0);
    --signal      input_bcr_period_r  : unsigned(31 downto 0);

    signal      input_long_Bch      : std_logic_vector(31 downto 0);    -- default ...
    signal      input_broadcast     : std_logic_vector(5 downto 0);

    signal      set_default         : std_logic;
    -- signal      cycle_mode          : std_logic;

    signal      single_l1a_long     : std_logic := '0';
    signal      single_l1a          : std_logic := '0';
    signal      single_ecr_long     : std_logic := '0';
    signal      single_ecr          : std_logic := '0';
    signal      single_bcr_long     : std_logic := '0';
    signal      single_bcr          : std_logic := '0';
    signal      single_ocr_long     : std_logic := '0';
    signal      single_ocr          : std_logic := '0';

    signal         test_pulse, tpmode : std_logic := '0';   -- AS: Test_Pulse sent to output before L1A by a number of BC's specified in a tpulse_cnt "register" TBD (TTC_EMU_TP_PERIOD)
    -- AS: the tpulse_en (enable) is derived from rising edge of BROADCAST(5)

    signal      en          : std_logic := '0';
    signal      user_reset          : std_logic := '0';

    signal      l1a_cnt             : unsigned(31 downto 0) := (others => '0');
    signal      ecr_cnt             : unsigned(31 downto 0) := (others => '0');

    signal      l1_accept               : std_logic := '0';
    signal ttype : std_logic_vector(15 downto 0);
    signal l0id: std_logic_vector(37 downto 0);
    signal l1id: std_logic_vector(23 downto 0);
    signal xl1id: std_logic_vector(7 downto 0);
    signal Partition : std_logic_vector(1 downto 0);
    signal PT : std_logic;
    signal SyncGlobalData : std_logic_vector(15 downto 0);
    signal SyncUserData : std_logic_vector(15 downto 0);
    signal bcid : std_logic_vector(11 downto 0);
    signal AsyncUserData : std_logic_vector(63 downto 0);
    constant lbid : std_logic_vector(15 downto 0) := x"0000";
    signal orbit : std_logic_vector(31 downto 0);
    signal GRst, GRST_p1 : std_logic;
    signal Sync, SYNC_p1 : std_logic;
    signal ocr : std_logic;
    signal bcr: std_logic;
    signal ITk_sync : std_logic;
    signal ITk_tag : std_logic_vector(6 downto 0);
    signal ITk_trig : std_logic_vector(3 downto 0);
    signal ExtendedTestPulse_s : std_logic;

    signal ecr : std_logic;

begin

    AsyncUserData <= register_map_control.TTC_ASYNCUSERDATA.DATA;
    SyncGlobalData <= register_map_control.TTC_EMU_CONTROL.SYNC_GLOBAL_DATA;
    SyncUserData <= register_map_control.TTC_EMU_CONTROL.SYNC_USER_DATA;
    PT <= to_sl(register_map_control.TTC_EMU_CONTROL.PT);
    Partition <= register_map_control.TTC_EMU_CONTROL.PARTITION;


    grst_sync_pipe: process(Clock)
    begin
        if rising_edge(Clock) then
            GRST_p1 <= to_sl(register_map_control.TTC_EMU_CONTROL.GRST);
            SYNC_p1 <= to_sl(register_map_control.TTC_EMU_CONTROL.SYNC);
        end if;
    end process;

    GRst <= to_sl(register_map_control.TTC_EMU_CONTROL.GRST) and not GRST_p1;
    Sync <= to_sl(register_map_control.TTC_EMU_CONTROL.SYNC) and not SYNC_p1;

    -- if both ttc selector and ttc enable are high, than the enable will be active
    en <= to_sl(register_map_control.TTC_EMU.SEL) and to_sl(register_map_control.TTC_EMU.ENA);

    -- temporary here, I am not yet sure I want to keep it..
    process(Clock)
    begin
        if rising_edge(Clock) then
            if Reset = '1' or user_reset = '1' then
                input_long_Bch      <= (others => '1');
                input_broadcast     <= (others => '0');
                set_default         <= '0';
                single_l1a_long     <= '0';
                single_ecr_long     <= '0';
                single_bcr_long     <= '0';
                input_l1a_period    <= (others => '0');
                input_ecr_period    <= (others => '0');
                input_bcr_period    <= to_unsigned(3564, 32);
                input_tpulse_period <= (others => '0');
            elsif en = '1' then

                input_tpulse_period <= unsigned(register_map_control.TTC_EMU_TP_DELAY); --AS: added to pcie-package..
                input_l1a_period    <= unsigned(register_map_control.TTC_EMU_L1A_PERIOD);
                input_ecr_period    <= unsigned(register_map_control.TTC_EMU_ECR_PERIOD);
                input_bcr_period    <= unsigned(register_map_control.TTC_EMU_BCR_PERIOD);

                input_long_Bch      <= (register_map_control.TTC_EMU_LONG_CHANNEL_DATA); --AS: used for individually addressed command
                input_broadcast     <= (register_map_control.TTC_EMU_CONTROL.BROADCAST);

                set_default         <= to_sl(register_map_control.TTC_EMU_RESET);

                single_l1a_long     <= to_sl(register_map_control.TTC_EMU_CONTROL.L1A);
                single_ecr_long     <= to_sl(register_map_control.TTC_EMU_CONTROL.ECR);
                single_bcr_long     <= to_sl(register_map_control.TTC_EMU_CONTROL.BCR);
                single_ocr_long     <= to_sl(register_map_control.TTC_EMU_CONTROL.SORB);
            end if;
        end if;
    end process;


    ------------------------------------------------------------------------
    -- Low to high detection for single requests of L1A, ECR and BCR -------
    -- as well as reset. ---------------------------------------------------
    ------------------------------------------------------------------------

    tpmode <= input_broadcast(0);

    l1a_detect: entity work.hilo_detect
        generic map (
            lohi    => true
        )
        port map (
            clk     => Clock,     -- clock
            sig_in  => single_l1a_long,     -- input signal
            sig_out => single_l1a      -- output signal
        );

    ecr_detect: entity work.hilo_detect
        generic map (
            lohi    => true
        )
        port map (
            clk     => Clock,     -- clock
            sig_in  => single_ecr_long,     -- input signal
            sig_out => single_ecr      -- output signal
        );

    bcr_detect: entity work.hilo_detect
        generic map (
            lohi    => true
        )
        port map (
            clk     => Clock,     -- clock
            sig_in  => single_bcr_long,     -- input signal
            sig_out => single_bcr      -- output signal
        );

    ocr_detect: entity work.hilo_detect
        generic map (
            lohi    => true
        )
        port map (
            clk     => Clock,     -- clock
            sig_in  => single_ocr_long,     -- input signal
            sig_out => single_ocr      -- output signal
        );

    reset_detect: entity work.hilo_detect
        generic map (
            lohi    => true
        )
        port map (
            clk     => Clock,     -- clock
            sig_in  => set_default,     -- input signal
            sig_out => user_reset      -- output signal
        );

    ------------------------------------------------------------------------------------------
    ----- test pulse logic; L1A single mode and cycle mode with adjustable frequency ---------
    ------------------------------------------------------------------------------------------

    process(Clock)
        variable l1_accept_v: std_logic;
        variable ecr_occured: std_logic;
    begin
        if rising_edge(Clock) then
            if Reset = '1' or user_reset = '1' then
                l1_accept_v := '0';
                l1_accept <= '0';
                l1a_cnt <= input_l1a_period;
                l0id <= (others => '0');
                ecr_occured := '0';
                l1id <= (others => '0');
                xl1id <= (others => '0');
                test_pulse <= '0';                --AS: test_pulse is set only for 1 clock! (can be modified if required)
            elsif en = '1' and (BUSYIn = '0' or register_map_control.TTC_EMU_CONTROL.BUSY_IN_ENABLE = "0") then
                test_pulse <= '0';                --AS: test_pulse is set only for 1 clock! (can be modified if required)
                l1_accept_v := '0';

                if input_l1a_period /= x"0000_0000" then
                    if tpmode = '1' then
                        if l1a_cnt = (input_tpulse_period + 58) then --It takes 58 clock cycles to transmit the Test pulse through the fifo and serializer.
                            test_pulse <= '1';
                        end if;
                    end if;
                    if l1a_cnt > x"0000_0001" then
                        l1a_cnt <= l1a_cnt -1;
                    else
                        l1_accept_v := '1';
                        l1a_cnt <= input_l1a_period;
                    end if;
                else --signle L1A mode
                    test_pulse <= '0';
                    l1_accept_v := '0';
                    if tpmode = '1' then
                        if single_l1a = '1' then
                            test_pulse <= '1';
                            l1a_cnt <= input_tpulse_period + 59; --It takes 58 clock cycles to transmit the Test pulse through the fifo and serializer.
                        else
                            if l1a_cnt /= x"0000_0000" then
                                l1a_cnt <= l1a_cnt - 1;
                            end if;
                            if l1a_cnt = x"0000_0001" then
                                l1_accept_v := '1';
                            end if;
                        end if;
                    else
                        l1_accept_v := single_l1a;
                    end if;
                end if;

            end if;
            if ecr = '1' then
                ecr_occured := '1';
            end if;
            if l1_accept_v = '1' then
                l0id <= l0id + 1;
                l1id <= l1id + 1;
                if ecr_occured = '1' then
                    l1id <= (others => '0');
                    xl1id <= xl1id + 1;
                    ecr_occured := '0';
                end if;
            end if;
            if register_map_control.TTC_DEC_CTRL.XL1ID_RST = "1" then
                xl1id <= x"00";
            end if;
            l1_accept <= l1_accept_v;
        end if;
    end process;


    etp0: entity work.ExtendedTestPulse port map(
            clk40 => Clock,
            reset => Reset,
            TTCin_TestPulse => test_pulse,
            ExtendedTestPulse => ExtendedTestPulse_s
        );

    tag_gen_enable: if ITK_TRIGTAG_MODE >= 0 generate
        tag_gen: entity work.itk_trigtag_generator
            generic map (
                MODE => ITK_TRIGTAG_MODE
            )
            port map (
                clk             => Clock,
                rst             => Reset,
                enc_trig        => itk_trig,
                enc_sync        => itk_sync,
                enc_tag         => itk_tag,
                ttc2h_tag       => ttc2h_tag,
                ttc2h_tag_valid => ttc2h_tag_valid,
                ttc_l0a         => l1_accept,--,
                ttc_bcr         => bcr
            --ttc_bcr         => turnsignal,
            --ttc_phase       => "00" -- TODO: connect to something meaningful in the regbank
            );
    else generate
        itk_trig <= "0000";
        itk_sync <= '0';
        itk_tag <= (others => '0');
    --itk_ttc2h_tag <= (others => '0');
    end generate;
    ---------------------------------------------------------------
    ----- Short Bchannel generation 1st step ----------------------
    ---------------------------------------------------------------



    -- TS (BCR) and Sorb
    process(Clock)
        variable ocr_half_bcr_period_trig: std_logic;
    begin
        if rising_edge(Clock) then
            if Reset = '1' or user_reset = '1' then
                bcr <= '0';
                bcid <= x"000";
                ocr_half_bcr_period_trig := '0';
                ocr <= '0';
                orbit <= (others => '0');
            else
                ocr <= '0';

                if en = '1' then
                    if input_bcr_period(11 downto 0) /= x"000" then
                        if single_ocr = '1' then
                            ocr_half_bcr_period_trig := '1';
                        end if;
                        if unsigned(bcid) = (input_bcr_period(11 downto 0) - 1) then
                            bcid <= x"000";
                            bcr <= '1';
                            orbit <= orbit + 1;
                        else
                            bcid <= bcid + 1;
                            bcr <= single_bcr;
                        end if;
                        if unsigned(bcid) = ('0' & input_bcr_period(11 downto 1)) then --At half the BCR period, issue an OCR if triggered.
                            if ocr_half_bcr_period_trig = '1' then
                                ocr_half_bcr_period_trig := '0';
                                ocr <= '1';

                            end if;
                        end if;
                    else
                        bcr <= single_bcr;
                        ocr <= single_ocr;
                    end if;
                else
                    bcr <= '0';
                end if;
                if ocr = '1' then
                    orbit <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    process(Clock)
    begin
        if rising_edge(Clock) then
            if Reset = '1' or user_reset = '1' then
                ecr <= '0';
                ecr_cnt <= input_ecr_period + 1;
                input_ecr_period_r <= (others => '0');
            else
                if en = '1' then
                    if input_ecr_period /= x"0000_0000" then
                        input_ecr_period_r <= input_ecr_period;
                        if input_ecr_period = input_ecr_period_r then
                            if ecr_cnt = x"0000_0001" then
                                ecr_cnt <= input_ecr_period + 1;
                                ecr <= '1';
                            else
                                ecr_cnt <= ecr_cnt - 1;
                                ecr <= single_ecr;
                            end if;
                        else
                            ecr_cnt <= input_ecr_period + 1;
                        end if;
                    else
                        ecr <= single_ecr;
                    end if;
                else
                    ecr <= '0';
                end if;
            end if;
        end if;
    end process;


    ttype   <= input_long_Bch(15 downto 0) when l1_accept = '1' else x"0000";


    TTCout <= (
        PT                  => PT, --: std_logic;
        Partition           => Partition, --: std_logic_vector(1 downto 0);
        BCID                => bcid, --: std_logic_vector(11 downto 0);
        SyncUserData        => SyncUserData, --: std_logic_vector(15 downto 0);
        SyncGlobalData      => SyncGlobalData, --: std_logic_vector(15 downto 0);
        TS                  => bcr, --: std_logic;
        ErrorFlags          => (others => '0'), --: std_logic_vector(3 downto 0);
        SL0ID               => ecr, --: std_logic;
        SOrb                => ocr, --: std_logic;
        Sync                => Sync, --: std_logic;
        GRst                => GRst, --: std_logic;
        L0A                 => l1_accept, --: std_logic;
        L0ID                => l0id, --: std_logic_vector(37 downto 0);
        OrbitID             => orbit, --: std_logic_vector(31 downto 0);
        TriggerType         => ttype, --: std_logic_vector(15 downto 0);
        LBID                => lbid, --: std_logic_vector(15 downto 0);
        AsyncUserData       => AsyncUserData, --: std_logic_vector(63 downto 0);
        XOFF                => '0', --: std_logic;
        CRC                 => x"0000", --: std_logic_vector(15 downto 0);
        D16_2               => x"50", --: std_logic_vector(7 downto 0);
        LTI_decoder_aligned => '1', --: std_logic;
        LTI_CRC_valid       => '1', --: std_logic;
        L1A                 => l1_accept, --: std_logic;
        Bchan               => '0', --: std_logic;
        BCR                 => bcr,
        ECR                 => ecr,
        Brcst               => SyncUserData(5 downto 0), --: std_logic_vector(5 downto 0);
        Brcst_latched       => SyncUserData(5 downto 0), --: std_logic_vector(5 downto 0);
        ExtendedTestPulse   => ExtendedTestPulse_s, --: std_logic;
        L1Id                => l1id, --: std_logic_vector(23 downto 0);
        XL1ID               => xl1id,
        ITk_sync            => ITk_sync, --: std_logic;
        ITk_tag            => '0'&ITk_tag, --: std_logic_vector(7 downto 0);
        ITk_trig            => ITk_trig  --: std_logic_vector(3 downto 0);
    );





end Behavioral;

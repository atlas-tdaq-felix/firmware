--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               Thei Wijnen
--!               Alessandra Camplani
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

-------------------------------------------------------------------------------
-- design     : delay_chain.vhd
-- author     : steffen staerz
-- maintainer : alessandra camplani
-- email      : alessandra.camplani@cern.ch
-- comments   : delays a signal in the 'clk' clock domain (it's a shift register!)
-------------------------------------------------------------------------------
-- details    :
-- the delay is determined by several generics:
-- d_width (>= 1) determines the width of the signal to be delayed
-- d_depth (>= 1) determines the depth of the delay chain (shift register)
-- on_reset gives the default value on reset ('0' by default)
--
-- delay_chain can also be used to synchronise in a single bit signal
-- don't use it for synchronising std_logic_vectors over clock domains!
-- for this purpose use dc fifos instead!
-------------------------------------------------------------------------------
--
-- instantiation template (for delaying a std_logic):
--
--  [inst_name]: entity work.delay_chain
--  generic map (
--      d_depth     => [positive := 3],     -- number of clock cycles it shell be delayed
--      on_reset    => [std_logic := '0'],  -- initial and 'on reset'-value
--      ram_style   => [string := "auto"]   -- ram style used
--  )
--  port map (
--      clk         => [in  std_logic],     -- clock
--      rst         => [in  std_logic],     -- sync reset
--      sig_in(0)   => [in  std_logic],     -- input signal
--      sig_out(0)  => [out std_logic]      -- delayed output signal
--  );
--
-- instantiation template (for delaying a std_logic_vector):
--
--  [inst_name]: entity work.delay_chain
--  generic map (
--      d_width     => [positive := 1],     -- width of the signal to be delayed
--      d_depth     => [positive := 3],     -- number of clock cycles it shell be delayed
--      on_reset    => [std_logic := '0'],  -- initial and 'on reset'-value
--      ram_style   => [string := "auto"]   -- ram style used
--  )
--  port map (
--      clk         => [in  std_logic],                             -- clock
--      rst         => [in  std_logic],                             -- sync reset
--      sig_in      => [in  std_logic_vector(d_width-1 downto 0)],  -- input signal
--      sig_out     => [out std_logic_vector(d_width-1 downto 0)]   -- delayed output signal
--  );
--
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;

entity delay_chain is
    generic (
        d_width     : positive := 1;    -- width of the signal to be delayed
        d_depth     : positive := 3;    -- number of clock cycles it shell be delayed
        on_reset    : std_logic := '0'; -- initial and 'on reset'-value
        ram_style   : string := "auto"  -- ram style used
    );
    port (
        clk         : in  std_logic;
        rst         : in  std_logic;
        sig_in      : in  std_logic_vector(d_width-1 downto 0);
        sig_out     : out std_logic_vector(d_width-1 downto 0)
    );
end delay_chain;

architecture behavioral of delay_chain is
    type delay_reg_t is array (d_depth-1 downto 0) of std_logic_vector(d_width-1 downto 0);
    signal delay_reg : delay_reg_t := (others => (others => on_reset));
    attribute ramstyle : string;
    attribute ramstyle of delay_reg : signal is ram_style;
begin
    sig_out <= delay_reg(0);

    gen_d_depth_one: if d_depth = 1 generate
        process(clk)
        begin
            if rising_edge(clk) then
                if rst = '1' then
                    delay_reg(0) <= (others => on_reset);
                else
                    delay_reg(0) <= sig_in;
                end if;
            end if;
        end process;
    end generate;
    -- else:
    gen_d_depth_more: if d_depth > 1 generate
        process(clk)
        begin
            if rising_edge(clk) then
                if rst = '1' then
                    delay_reg <= (others => (others => on_reset));
                else
                    -- shift right and insert sig_in on the left
                    delay_reg(d_depth-1 downto 0) <= sig_in & delay_reg(d_depth-1 downto 1);
                end if;
            end if;
        end process;
    end generate;
end behavioral;

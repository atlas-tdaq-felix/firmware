--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Ali Skaf
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--=====================================================================================
-- Company      : CERN - University of Goettingen
-- Project      : Felix
-- Module      : TTC_emulator_v 2.1
-- Design      : signal_delay.vhd
-- Author       : Ali Skaf
-- Email        : ali.skaf@uni-goettingen.de
-- Created      : 6.10.2021
-- Description  : delay an input pulse sig_in for a numner of clks equal to count_in + 1
--======================================================================================

library ieee, unisim;
    use ieee.std_logic_1164.all;
    --use IEEE.NUMERIC_STD.ALL;
    --use IEEE.STD_LOGIC_UNSIGNED.ALL;
    use unisim.vcomponents.all;

entity signal_delay is
    port (
        clk           : in  std_logic;
        count_in      : in  std_logic_vector(3 downto 0);
        sig_in        : in  std_logic;
        sig_out       : out std_logic
    );
end signal_delay;

--========================================================================================
architecture behavioral of signal_delay is


begin
    TTC_SRL : SRL16E
        generic map(
            INIT => x"0000",
            IS_CLK_INVERTED => '0'
        )
        port map (
            Q => sig_out,
            A0 => count_in(0),
            A1 => count_in(1),
            A2 => count_in(2),
            A3 => count_in(3),
            ce => '1',
            clk => clk,
            D => sig_in
        );

end behavioral;
--========================================================================================


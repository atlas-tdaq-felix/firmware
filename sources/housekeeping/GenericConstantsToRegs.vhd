--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               Israel Grayzman
--!               Enrico Gamberini
--!               RHabraken
--!               Mesfin Gebyehu
--!               Rene
--!               Thei Wijnen
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.





library ieee, UNISIM;
    use ieee.numeric_std.all;
    use UNISIM.VCOMPONENTS.all;
    use ieee.numeric_std_unsigned.all;
    use ieee.std_logic_1164.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;
    use work.strips_package.all;

entity GenericConstantsToRegs is
    generic(
        GBT_NUM                         : integer := 24;
        ENDPOINTS                       : integer := 1; --Number of PCIe endpoints on the card.
        OPTO_TRX                        : integer := 2;
        generateTTCemu                  : boolean := false;
        AUTOMATIC_CLOCK_SWITCH          : boolean := false;
        FIRMWARE_MODE                   : integer := 0;
        USE_Si5324_RefCLK               : boolean := false;
        GENERATE_XOFF                   : boolean := true; -- FromHost Xoff transmission enabled on Full mode busy
        IncludeDecodingEpath2_HDLC      : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath2_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath4_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath8_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath16_8b10b    : std_logic_vector(6 downto 0) := "0000000"; --lpGBT only
        IncludeDecodingEpath32_8b10b    : std_logic_vector(6 downto 0) := "0000000"; --lpGBT only
        IncludeDirectDecoding           : std_logic_vector(6 downto 0) := "0000000";
        IncludeEncodingEpath2_HDLC      : std_logic_vector(4 downto 0) := "00000";
        IncludeEncodingEpath2_8b10b     : std_logic_vector(4 downto 0) := "00000";
        IncludeEncodingEpath4_8b10b     : std_logic_vector(4 downto 0) := "00000";
        IncludeEncodingEpath8_8b10b     : std_logic_vector(4 downto 0) := "00000";
        IncludeDirectEncoding           : std_logic_vector(4 downto 0) := "00000";
        BLOCKSIZE                       : integer := 1024;
        CHUNK_TRAILER_32B               : boolean := false;
        DATA_WIDTH                      : integer := 256;
        FULL_HALFRATE                   : boolean := false;
        SUPPORT_HDLC_DELAY              : boolean := false);
    port (
        AUTOMATIC_CLOCK_SWITCH_ENABLED : out    std_logic_vector(0 downto 0);
        register_map_gen_board_info    : out    register_map_gen_board_info_type);
end entity GenericConstantsToRegs;



architecture rtl of GenericConstantsToRegs is
    constant STREAMS_TOHOST: integer := STREAMS_TOHOST_MODE(FIRMWARE_MODE);
    constant GROUP_CONFIG_FROMHOST : IntArray(0 to 7) := STREAMS_FROMHOST_MODE(FIRMWARE_MODE);
    constant STREAMS_FROMHOST: integer := sum(GROUP_CONFIG_FROMHOST);

    function IC_INDEX(i : integer) return integer is
    begin
        if i > 1 then
            return i-1;
        else
            return 0;
        end if;
    end function;

    function EC_INDEX(i : integer) return integer is
    begin
        if i > 1 then
            return i-2;
        else
            return 0;
        end if;
    end function;
    function CONV (X :boolean) return std_logic_vector is
    begin
        case X is
            when false => return "0";
            when true => return "1";
        end case;
    end CONV;

begin


    register_map_gen_board_info.GENERATE_TTC_EMU           <= CONV(generateTTCemu);
    --register_map_gen_board_info.TTC_EMU_CONST.TTC_TEST_MODE              <= CONV(TTC_test_mode);
    register_map_gen_board_info.GENERATE_GBT                             <= "1";                                -- 1 when the GBT is included in the design
    --register_map_gen_board_info.GBT_MAPPING                              <= std_logic_vector(to_unsigned(GBT_MAPPING,8));      -- CXP-to-GBT mapping:
    --   0: NORMAL CXP1 1-12 CXP2 13-24
    --   1: ALTERNATE CXP1 1-4,9-12,17-20
    register_map_gen_board_info.OPTO_TRX_NUM                                 <= std_logic_vector(to_unsigned(OPTO_TRX,8));         -- Number of optical transceivers in the design
    register_map_gen_board_info.NUM_OF_CHANNELS                          <= std_logic_vector(to_unsigned(GBT_NUM,8));          -- Number of GBT Channels
    register_map_gen_board_info.BLOCKSIZE                                <= std_logic_vector(to_unsigned(BLOCKSIZE, 16));
    register_map_gen_board_info.CHUNK_TRAILER_32B                        <= CONV(CHUNK_TRAILER_32B);
    g_egroupsth: for i in 0 to 6 generate
        register_map_gen_board_info.INCLUDE_EGROUP(i).TOHOST_HDLC   <= IncludeDecodingEpath2_HDLC(i downto i);
        register_map_gen_board_info.INCLUDE_EGROUP(i).TOHOST_02     <= IncludeDecodingEpath2_8b10b(i downto i);
        register_map_gen_board_info.INCLUDE_EGROUP(i).TOHOST_04     <= IncludeDecodingEpath4_8b10b(i downto i);
        register_map_gen_board_info.INCLUDE_EGROUP(i).TOHOST_08     <= IncludeDecodingEpath8_8b10b(i downto i);
        register_map_gen_board_info.INCLUDE_EGROUP(i).TOHOST_16     <= IncludeDecodingEpath16_8b10b(i downto i);
        register_map_gen_board_info.INCLUDE_EGROUP(i).TOHOST_32     <= IncludeDecodingEpath32_8b10b(i downto i);
    end generate;
    g_egroupsfh: for i in 0 to 4 generate
        register_map_gen_board_info.INCLUDE_EGROUP(i).FROMHOST_HDLC <= IncludeEncodingEpath2_HDLC(i downto i);
        register_map_gen_board_info.INCLUDE_EGROUP(i).FROMHOST_02   <= IncludeEncodingEpath2_8b10b(i downto i);
        register_map_gen_board_info.INCLUDE_EGROUP(i).FROMHOST_04   <= IncludeEncodingEpath4_8b10b(i downto i);
        register_map_gen_board_info.INCLUDE_EGROUP(i).FROMHOST_08   <= IncludeEncodingEpath8_8b10b(i downto i);
    end generate;


    register_map_gen_board_info.NUMBER_OF_PCIE_ENDPOINTS <= std_logic_vector(to_unsigned(ENDPOINTS, 2));

    register_map_gen_board_info.AXI_STREAMS_TOHOST.IC_INDEX            <= std_logic_vector(to_unsigned(IC_INDEX(STREAMS_TOHOST),8));
    register_map_gen_board_info.AXI_STREAMS_TOHOST.EC_INDEX            <= std_logic_vector(to_unsigned(EC_INDEX(STREAMS_TOHOST),8));
    register_map_gen_board_info.AXI_STREAMS_TOHOST.NUMBER_OF_STREAMS   <= std_logic_vector(to_unsigned(STREAMS_TOHOST,8));
    register_map_gen_board_info.AXI_STREAMS_FROMHOST.IC_INDEX          <= std_logic_vector(to_unsigned(IC_INDEX(STREAMS_FROMHOST),8));
    register_map_gen_board_info.AXI_STREAMS_FROMHOST.EC_INDEX          <= std_logic_vector(to_unsigned(EC_INDEX(STREAMS_FROMHOST),8));
    register_map_gen_board_info.AXI_STREAMS_FROMHOST.NUMBER_OF_STREAMS <= std_logic_vector(to_unsigned(STREAMS_FROMHOST,8));

    register_map_gen_board_info.FIRMWARE_MODE <= std_logic_vector(to_unsigned(FIRMWARE_MODE,5));

    register_map_gen_board_info.FROMHOST_DATA_FORMAT        <= "100" when DATA_WIDTH = 256 else
                                                               "101" when DATA_WIDTH = 512 else
                                                               "110" when DATA_WIDTH = 1024 else
                                                               "XXX";
    register_map_gen_board_info.TOHOST_DATA_FORMAT          <= "01"; -- Use subchunk headers
    register_map_gen_board_info.WIDE_MODE                   <= "0";                                    -- GBT is configured in Wide mode
    AUTOMATIC_CLOCK_SWITCH_ENABLED <= CONV(AUTOMATIC_CLOCK_SWITCH);                      -- 1 when the automatic clock switch is enabled in the design

    register_map_gen_board_info.CR_GENERICS.FROM_HOST_INCLUDED   <= "1";
    register_map_gen_board_info.CR_GENERICS.DIRECT_MODE_INCLUDED(1) <= (or IncludeDirectDecoding) and (or IncludeDirectEncoding);
    register_map_gen_board_info.CR_GENERICS.XOFF_INCLUDED        <= CONV(GENERATE_XOFF);
    register_map_gen_board_info.FULLMODE_HALFRATE                <= CONV(FULL_HALFRATE);
    register_map_gen_board_info.SUPPORT_HDLC_DELAY               <= CONV(SUPPORT_HDLC_DELAY);

    register_map_gen_board_info.GTREFCLK_SOURCE <= "0" & CONV(USE_Si5324_RefCLK);

    register_map_gen_board_info.STRIPS_ENCODING_CFG <= (
        SEQRAM_ADDR_WIDTH => std_logic_vector(to_unsigned(CFG_SEQRAM_ADDR_WIDTH,4)),
        INCLUDE_ASIC_MASKS => (others => CFG_INCLUDE_ASIC_MASKS),
        INCLUDE_BCID_GATING => (others => CFG_INCLUDE_BCID_GATING)
    );

end architecture rtl ; -- of GenericConstantsToRegs


--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               Israel Grayzman
--!               Mesfin Gebyehu
--!               RHabraken
--!               Thei Wijnen
--!               Frans Schreuder
--!               Filiberto Bonini
--!               Shelfali Saxena
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.


--!------------------------------------------------------------------------------
--!
--!           NIKHEF - National Institute for Subatomic Physics
--!
--!                       Electronics Department
--!
--!-----------------------------------------------------------------------------
--! @class clock_and_reset
--!
--!
--! @author      Andrea Borga    (andrea.borga@nikhef.nl)<br>
--!              Frans Schreuder (frans.schreuder@nikhef.nl)
--!
--!
--! @date        07/01/2015    created
--!
--! @version     1.0
--!
--! @brief
--! Clock and Reset instantiates an MMCM. It generates clocks of 40,
--! 80, 160 and 320 MHz.
--! Additionally a reset signal is issued when the MMCM is not locked.
--! Reset_out is synchronous to 40MHz
--!
--!
--!-----------------------------------------------------------------------------
--! @TODO
--!
--!
--! ------------------------------------------------------------------------------
--
--! @brief ieee



library ieee, UNISIM, xpm;
    use ieee.numeric_std.all;
    use UNISIM.VCOMPONENTS.all;
    use XPM.VCOMPONENTS.all;
    use ieee.numeric_std_unsigned.all;
    use ieee.std_logic_1164.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;

entity clock_and_reset is
    generic(
        APP_CLK_FREQ           : integer := 200;
        USE_BACKUP_CLK         : boolean := true; -- true to use 100/200 mhz board crystal, false to use TTC clock
        AUTOMATIC_CLOCK_SWITCH : boolean := false;
        CARD_TYPE              : integer;
        FIRMWARE_MODE          : integer := 0);
    port (
        MMCM_Locked_out      : out    std_logic;
        MMCM_OscSelect_out   : out    std_logic;
        app_clk_in_n         : in     std_logic;
        app_clk_in_p         : in     std_logic;
        --cdrlocked_in         : in     std_logic;
        clk10_xtal           : out    std_logic;
        clk100               : out    std_logic;
        clk160               : out    std_logic;
        clk240               : out    std_logic;
        clk250               : out    std_logic;
        clk320               : out    std_logic;
        clk365               : out    std_logic;
        clk40                : out    std_logic;
        clk40_xtal           : out    std_logic;
        clk80                : out    std_logic;
        clk_adn_160          : in     std_logic;
        clk_adn_160_out_n    : out    std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0);
        clk_adn_160_out_p    : out    std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0);
        clk_ttc_40           : in     std_logic;
        clk40_stable_out     : out    std_logic;
        --clk_ttcfx_mon1       : out    std_logic;
        --clk_ttcfx_mon2       : out    std_logic;
        clk_ttcfx_ref_out_n  : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        clk_ttcfx_ref_out_p  : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        register_map_control : in     register_map_control_type;
        reset_out            : out    std_logic; --! Active high reset out (synchronous to clk40)
        sys_reset_n          : in     std_logic); --! Active low reset input.
end entity clock_and_reset;


architecture rtl of clock_and_reset is


    component clk_wiz_40_0 -- @suppress "Component declaration is not equal to its matching entity"
        port(
            clk_in2    : in  STD_LOGIC;
            clk_in_sel : in  STD_LOGIC;
            clk40      : out STD_LOGIC;
            clk80      : out STD_LOGIC;
            clk160     : out STD_LOGIC;
            clk320     : out STD_LOGIC;
            clk240     : out STD_LOGIC;
            clk100     : out STD_LOGIC;
            reset      : in  STD_LOGIC;
            locked     : out STD_LOGIC;
            clk_40_in  : in  STD_LOGIC
        );
    end component clk_wiz_40_0;

    component clk_wiz_250
        port(
            clk_in40_2 : in  STD_LOGIC;
            clk_in_sel : in  STD_LOGIC;
            clk250     : out STD_LOGIC;
            clk365     : out STD_LOGIC;
            reset      : in  STD_LOGIC;
            locked     : out STD_LOGIC;
            clk_in40_1 : in  STD_LOGIC
        );
    end component clk_wiz_250;

    component clk_wiz_100_0
        port(
            clk40     : out STD_LOGIC;
            clk10     : out STD_LOGIC;
            reset     : in  STD_LOGIC;
            locked    : out STD_LOGIC;
            clk_in1_p : in  STD_LOGIC;
            clk_in1_n : in  STD_LOGIC
        );
    end component clk_wiz_100_0;

    component clk_wiz_200_0
        port(
            clk40     : out STD_LOGIC;
            clk10     : out STD_LOGIC;
            reset     : in  STD_LOGIC;
            locked    : out STD_LOGIC;
            clk_in1_p : in  STD_LOGIC;
            clk_in1_n : in  STD_LOGIC
        );
    end component clk_wiz_200_0;

    component clk_wiz_156_0
        port(
            clk40     : out STD_LOGIC;
            clk10     : out STD_LOGIC;
            reset     : in  STD_LOGIC;
            locked    : out STD_LOGIC;
            clk_in1_p : in  STD_LOGIC;
            clk_in1_n : in  STD_LOGIC
        );
    end component clk_wiz_156_0;


    signal reset_in        : std_logic;

    signal clk40_s         : std_logic;
    signal clk160_s        : std_logic;
    signal locked_s        : std_logic;
    signal clk_xtal_40_s   : std_logic;
    signal clk160_out_s    : std_logic;
    signal clk_ttcfx_ref_s : std_logic;

    --signal ttc_locked       : std_logic;
    --signal ttcCounter       : std_logic_vector(7 downto 0);
    --signal ttcCounterXtal   : std_logic_vector(7 downto 0);
    --signal ttcCounterXtalP1 : std_logic_vector(7 downto 0);
    --signal xtalCounter      : std_logic_vector(7 downto 0);
    signal OscSelect        : std_logic;
    constant TTC_SRC        : std_logic  := '1';
    constant XTAL_SRC       : std_logic  := '0';

    signal unused_app_clk : std_logic; -- @suppress "signal unused_app_clk is never read"
    attribute dont_touch : string;
    attribute dont_touch of unused_app_clk : signal is "true";
    signal app_clk_200M : std_logic;
    signal OscSelect_p4 : std_logic;
    signal OscSelect_p2 : std_logic;
    signal OscSelect_Switched: std_logic;
    signal reset_in_40 : std_logic;


begin


    -- Main MMCM
    clk0 : clk_wiz_40_0
        port map (
            clk_in2 => clk_xtal_40_s,
            clk_in_sel => OscSelect,
            clk40 => clk40_s,
            clk80 => clk80,
            clk160 => clk160_s,
            clk320 => clk320,
            clk240 => clk240,
            clk100 => clk100,
            reset => reset_in,
            locked => locked_s,
            -- Clock in ports
            clk_40_in => clk_ttc_40
        );

    cdc_oscselect_4: xpm_cdc_single
        generic map(
            DEST_SYNC_FF => 4,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => '0',
            src_in => OscSelect,
            dest_clk => clk40_s,
            dest_out => OscSelect_p4
        );

    cdc_oscselect_2: xpm_cdc_single
        generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => '0',
            src_in => OscSelect,
            dest_clk => clk40_s,
            dest_out => OscSelect_p2
        );

    cdc_reset_in_40: xpm_cdc_async_rst
        generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            RST_ACTIVE_HIGH => 1
        )
        port map(
            src_arst => reset_in,
            dest_clk => clk40,
            dest_arst => reset_in_40
        );

    --4 pipeline cdc'ed OscSelect xor 2 pipelined cdc'ed OscSelect creates a 2 clock pulse when it switches.
    OscSelect_Switched <= OscSelect_p2 xor OscSelect_p4;


    --After reset, clock switch or locked going down, wait 1023 clk40 cycles before indicating a stable clock.
    --This is needed for Built-in FIFO resets, they can only occur with a stable clock. See FLX-1834 / FLXUSERS-292
    clk40_stable_proc: process(clk40, OscSelect_Switched, reset_in_40, locked_s)
        variable stable_cnt: integer range 0 to 1023;
    begin
        if OscSelect_Switched = '1' or reset_in_40 = '1' or locked_s = '0' then
            stable_cnt := 0;
            clk40_stable_out <= '0';
        elsif rising_edge(clk40) then
            if stable_cnt /= 1023 then
                stable_cnt := stable_cnt + 1;
            else
                clk40_stable_out <= '1';
            end if;
        end if;
    end process;


    clk250_0 : clk_wiz_250
        port map (

            clk_in40_2 => clk_xtal_40_s,
            clk_in_sel => OscSelect,
            clk250 => clk250,
            clk365 => clk365,
            reset => reset_in,
            locked => open,
            -- Clock in ports
            clk_in40_1 => clk_ttc_40
        );

    -- FLX-709 Si570 @ 156.25MHz
    g_156M: if(APP_CLK_FREQ = 156) generate
        clk0 : clk_wiz_156_0
            port map (
                -- Clock out ports
                clk40 => clk_xtal_40_s,
                clk10 => clk10_xtal,
                -- Status and control signals
                reset => '0',
                locked => open,
                -- Clock in ports
                clk_in1_p => app_clk_in_p,
                clk_in1_n => app_clk_in_n
            );
    end generate;

    -- FLX-711 local clock
    g_200M: if(APP_CLK_FREQ = 200 and CARD_TYPE /= 182 and CARD_TYPE /= 155) generate
        clk0 : clk_wiz_200_0
            port map (
                -- Clock out ports
                clk40 => clk_xtal_40_s,
                clk10 => clk10_xtal,
                -- Status and control signals
                reset => '0',
                locked => open,
                -- Clock in ports
                clk_in1_p => app_clk_in_p,
                clk_in1_n => app_clk_in_n
            );
    end generate;

    -- FLX-182 local clock
    g_200M_FLX182: if(APP_CLK_FREQ = 200 and (CARD_TYPE = 182 or CARD_TYPE = 155 or CARD_TYPE = 120)) generate
        COMPONENT clk_wiz_200_se
            PORT (
                reset : IN STD_LOGIC;
                locked : OUT STD_LOGIC;
                clk_in1 : IN STD_LOGIC;
                clk40 : OUT STD_LOGIC;
                clk10 : OUT STD_LOGIC
            );
        END COMPONENT;
        signal app_clk_in: std_logic;
    begin
        IBUFDS0: IBUFDS port map( -- @suppress "Generic map uses default values. Missing optional actuals: CAPACITANCE, CCIO_EN_M, CCIO_EN_S, DIFF_TERM, DQS_BIAS, IBUF_DELAY_VALUE, IBUF_LOW_PWR, IFD_DELAY_VALUE, IOSTANDARD"
                O => app_clk_in,
                I => app_clk_in_p,
                IB => app_clk_in_n
            );
        BUFG0: BUFG port map(
                O => app_clk_200M,
                I => app_clk_in
            );

        clk0 : clk_wiz_200_se
            port map (
                -- Status and control signals
                reset => '0',
                locked => open,
                -- Clock in ports
                clk_in1 => app_clk_200M,
                -- Clock out ports
                clk40 => clk_xtal_40_s,
                clk10 => clk10_xtal
            );
    end generate;

    -- FLX-710 local clock
    g_100M: if(APP_CLK_FREQ = 100) generate
        clk0 : clk_wiz_100_0
            port map (
                -- Clock out ports
                clk40 => clk_xtal_40_s,
                clk10 => clk10_xtal,
                -- Status and control signals
                reset => '0',
                locked => open,
                -- Clock in ports
                clk_in1_p => app_clk_in_p,
                clk_in1_n => app_clk_in_n
            );
    end generate;

    --instantiate the buffer for the clock, in order to pass bitgen
    g1b: if(USE_BACKUP_CLK = false and AUTOMATIC_CLOCK_SWITCH = false) generate
        signal unused_app_clk_temp: std_logic;
    begin
        buf0: IBUFDS -- @suppress "Generic map uses default values. Missing optional actuals: CAPACITANCE, DIFF_TERM, DQS_BIAS, IBUF_DELAY_VALUE, IBUF_LOW_PWR, IFD_DELAY_VALUE, IOSTANDARD"
            port map (
                O => unused_app_clk_temp,
                I => app_clk_in_p,
                Ib => app_clk_in_n
            );
        bufg0: BUFG
            port map (
                O => unused_app_clk,
                I => unused_app_clk_temp
            );
    end generate;

    MMCM_Locked_out <= locked_s;
    MMCM_OscSelect_out <= OscSelect;

    reset_in <= not sys_reset_n;

    clk40_xtal <= clk_xtal_40_s;
    clk40  <= clk40_s;
    clk160 <= clk160_s;
    -- clk_ttcfx_ref_s <= clk40_s;

    --Save one BUFG by using the clock switch in the MMCM to create the 40 MHz reference clock for the Si5345
    -- mux0: BUFGMUX_CTRL
    -- port map
    -- (
    --   I0 => clk_xtal_40_s,
    --   I1 => clk_ttc_40,
    --   O  => clk_ttcfx_ref_s,
    --   S  => OscSelect
    -- );

    mux0 : BUFGCTRL
        generic map (
            INIT_OUT => 0,                -- Initial value of BUFGCTRL output, 0-1
            PRESELECT_I0 => FALSE,        -- BUFGCTRL output uses I0 input, FALSE, TRUE
            PRESELECT_I1 => FALSE,        -- BUFGCTRL output uses I1 input, FALSE, TRUE
            -- Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
            IS_CE0_INVERTED => '0',       -- Optional inversion for CE0
            IS_CE1_INVERTED => '0',       -- Optional inversion for CE1
            IS_I0_INVERTED => '0',        -- Optional inversion for I0
            IS_I1_INVERTED => '0',        -- Optional inversion for I1
            IS_IGNORE0_INVERTED => '0',   -- Optional inversion for IGNORE0
            IS_IGNORE1_INVERTED => '0',   -- Optional inversion for IGNORE1
            IS_S0_INVERTED => '0',        -- Optional inversion for S0
            IS_S1_INVERTED => '0',        -- Optional inversion for S1
            SIM_DEVICE => SIM_DEVICE(CARD_TYPE)  -- VERSAL_PRIME, VERSAL_PRIME_ES1
        )
        port map (
            O => clk_ttcfx_ref_s,             -- 1-bit output: Clock output
            CE0 => '1',         -- 1-bit input: Clock enable input for I0
            CE1 => '1',         -- 1-bit input: Clock enable input for I1
            I0 => clk_ttc_40,           -- 1-bit input: Primary clock
            I1 => clk_xtal_40_s,           -- 1-bit input: Secondary clock
            IGNORE0 => '0', -- 1-bit input: Clock ignore input for I0
            IGNORE1 => '0', -- 1-bit input: Clock ignore input for I1
            S0 => OscSelect,           -- 1-bit input: Clock select for I0
            S1 => not OscSelect            -- 1-bit input: Clock select for I1
        );

    -- input to the Si5345 from Main MMCM 40 MHz
    g_NUM_SI5345: for i in 0 to NUM_SI5345(CARD_TYPE)-1 generate
        g_clk200out_interlaken: if i = 0 and FIRMWARE_MODE = FIRMWARE_MODE_INTERLAKEN generate
            --For Interlaken on FLX182 we output the 200 MHz directly to SI5345A IN2 for clock synthesis
            OBUFDS_ttcfx_ref_out : OBUFDS
                generic map -- @suppress "Generic map uses default values. Missing optional actuals: CAPACITANCE, SLEW"
            ( IOSTANDARD => "DEFAULT")
                port map (
                    O  => clk_ttcfx_ref_out_p(i),
                    OB => clk_ttcfx_ref_out_n(i),
                    I  => app_clk_200M
                );
        else generate
            OBUFDS_ttcfx_ref_out : OBUFDS
                generic map -- @suppress "Generic map uses default values. Missing optional actuals: CAPACITANCE, SLEW"
            ( IOSTANDARD => "DEFAULT")
                port map (
                    O  => clk_ttcfx_ref_out_p(i),
                    OB => clk_ttcfx_ref_out_n(i),
                    I  => clk_ttcfx_ref_s --clk_ttc_40 --                    I  => clk_ttc_40 --clk_ttc_40    -- clock from the TTC decoder module (phase aligned)
                );
        end generate;
    end generate;

    g2a: if (AUTOMATIC_CLOCK_SWITCH = false) generate
        g2c: if(USE_BACKUP_CLK = false) generate
            OscSelect <= TTC_SRC;
        end generate;
        g2d: if(USE_BACKUP_CLK) generate
            OscSelect <= XTAL_SRC;
        end generate;
    end generate;

    -- autoclock switch turned into manual select
    g2b: if (AUTOMATIC_CLOCK_SWITCH) generate
        --ttc_locked <= cdrlocked_in;
        OscSelect <= XTAL_SRC when register_map_control.MMCM_MAIN.LCLK_SEL = "1" else  TTC_SRC; --ttc_locked;
    end generate;


    reset_out <= not locked_s;

    -- 160 Mhz to the Si5324 (FLX-709 only)
    g_160_out_bufgmux0: if(AUTOMATIC_CLOCK_SWITCH) generate
        signal CE1, CE0: std_logic;
    begin
        CE1 <= OscSelect;
        CE0 <= not OscSelect;
        bufgmux_160_0: BUFGCTRL
            generic map(
                SIM_DEVICE => SIM_DEVICE(CARD_TYPE)
            )
            port map (
                O => clk160_out_s, -- insert clock output
                CE0 => CE0,
                CE1 => CE1,
                I0 => clk160_s, -- insert clock input used when select (S) is Low
                I1 => clk_adn_160, -- insert clock input used when select (S) is High
                IGNORE0 => '0',
                IGNORE1 => '0',
                S0 => '1',
                S1 => '1'      );
    end generate;

    g_160_out_xtal: if(USE_BACKUP_CLK and AUTOMATIC_CLOCK_SWITCH = false) generate
        clk160_out_s <= clk160_s;
    end generate;

    g_160_out_adn: if(USE_BACKUP_CLK = false and AUTOMATIC_CLOCK_SWITCH = false) generate
        clk160_out_s <= clk_adn_160;
    end generate;

    --OBUFDS to route the 160 MHz clock into the Si5324 jitter cleaner (VC709) to create the GBT Reference clock
    g_NUM_SI5324: for i in 0 to NUM_SI5324(CARD_TYPE)-1 generate
        OBUF160: OBUFDS
            generic map (
                CAPACITANCE => "DONT_CARE",
                IOSTANDARD => "LVDS",
                SLEW       => "FAST")
            port map(
                O => clk_adn_160_out_p(i),
                OB => clk_adn_160_out_n(i),
                I => clk160_out_s);
    end generate;

end architecture rtl ; -- of clock_and_reset


--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Rene Habraken
--!               Frans Schreuder
--!               RHabraken
--!               Mesfin Gebyehu
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.


--!------------------------------------------------------------------------------
--!
--!           NIKHEF - National Institute for Subatomic Physics
--!
--!                       Electronics Department
--!
--!-----------------------------------------------------------------------------
--! @class FMemuRAM
--!
--!
--! @author      Rene Habraken(r.habraken@science.ru.nl)<br>
--!              Frans Schreuder (frans.schreuder@nikhef.nl)
--!
--!
--! @date        08/05/2017    created
--!
--! @version     1.0
--!
--! @brief
--!
--! Instantiates a true dual port ram with a readout state process
--! Enable in and Valid out.
--!
--! @detail
--!
--!-----------------------------------------------------------------------------
--! @TODO
--!
--!
--! ------------------------------------------------------------------------------
--
--! @brief ieee



library ieee, UNISIM;
    use ieee.numeric_std.all;
    use UNISIM.VCOMPONENTS.all;
    use ieee.numeric_std_unsigned.all;
    use ieee.std_logic_1164.all;
    use work.pcie_package.all;

entity FMemuRAM_vhd is
    port (
        EMU32b_EN                       : in     std_logic;
        EMU32b_dout                     : out    std_logic_vector(31 downto 0);
        EMU32b_dvalid                   : out    std_logic;
        appreg_clk                      : in     std_logic;
        clk240                          : in     std_logic;
        register_map_control_appreg_clk : in     register_map_control_type;
        wea               : in    std_logic_vector(0 DOWNTO 0);
        rst                             : in     std_logic);
end entity FMemuRAM_vhd;



architecture rtl of FMemuRAM_vhd is

    COMPONENT DPram_32b -- @suppress "Component declaration 'DPram_32b' has none or multiple matching entity declarations"
        PORT (
            clka : IN std_logic;
            wea : IN std_logic_vector(0 DOWNTO 0);
            addra : IN std_logic_vector(9 DOWNTO 0);
            dina : IN std_logic_vector(31 DOWNTO 0);
            douta : OUT std_logic_vector(31 DOWNTO 0);
            clkb : IN std_logic;
            web : IN std_logic_vector(0 DOWNTO 0);
            addrb : IN std_logic_vector(9 DOWNTO 0);
            dinb : IN std_logic_vector(31 DOWNTO 0);
            doutb : OUT std_logic_vector(31 DOWNTO 0)
        );
    END COMPONENT;

    signal data_rd_addr: std_logic_vector(9 downto 0);
    signal sEMU32b_EN_r1, sEMU32b_EN_r2, sEMU32b_EN_r3: std_logic;
    signal we_lut: std_logic_vector(0 downto 0);

begin

    we_lut <= wea and register_map_control_appreg_clk.FMEMU_CONFIG.WE;
    RAM_0 : DPram_32b
        PORT MAP (
            clka => appreg_clk,
            wea => we_lut, --register_map_control_appreg_clk.GBT_FM_EMU_CONFIG_WE_ARRAY(0 downto 0),
            addra => register_map_control_appreg_clk.FMEMU_CONFIG_WRADDR(9 downto 0),
            dina => register_map_control_appreg_clk.FMEMU_CONFIG.WRDATA(31 downto 0),
            douta => open, --register_map_emu_monitor.GBT_FM_EMU_READ(31 downto 0),
            clkb => clk240,
            web => "0" ,
            addrb => data_rd_addr,
            dinb => (others => '0'),
            doutb => EMU32b_dout
        );

    data_addr: process(clk240)
    begin
        if rising_edge (clk240) then
            if (rst = '1') then
                data_rd_addr <= (others=>'1');
            elsif EMU32b_EN = '1' then
                data_rd_addr <= data_rd_addr + 1; -- flips over
            end if;
            sEMU32b_EN_r1 <= EMU32b_EN;
            sEMU32b_EN_r2 <= sEMU32b_EN_r1;
            sEMU32b_EN_r3 <= sEMU32b_EN_r2;
        end if;
    end process;

    EMU32b_dvalid <= sEMU32b_EN_r3;

end architecture rtl ; -- of FMemuRAM


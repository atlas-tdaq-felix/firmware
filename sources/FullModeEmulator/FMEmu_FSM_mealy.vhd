--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Mesfin Gebyehu
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company: Radboud University Nijmegen
-- Engineer: Rene Habraken
--
-- Create Date: 01/25/2019 02:28:15 PM
-- Design Name: Finite State Machine for Full mode emulator
-- Module Name: FMEmu_FSM - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
--  State Machine Options:
--  Clock : CLK240 (Rising edge).
--  State assignment : Enumerate.
--  State decoding : Case construct.
--  Actions on transitions : Comb.(in 1 Block).
--  Actions on states : Comb.(in 1 Block).

-- Datatype:
--    - 00: intermediate word of current packet
--    - 01: start of packet
--    - 10: end of packet
--    - 11: idle
----------------------------------------------------------------------------------


library ieee, UNISIM;
    use ieee.numeric_std.all;
    use UNISIM.VCOMPONENTS.all;
    use ieee.std_logic_unsigned.all; -- @suppress "Deprecated package"
    use ieee.std_logic_1164.all;
    use work.pcie_package.all;
library xpm;
    use xpm.vcomponents.all;


entity FMEmu_FSM is
    port (
        CLK240              : in     std_logic;
        REC_CLK40           : in     std_logic;
        register_map_control: in     register_map_control_type;
        FMEMU_EVENT_INFO_L1ID : out std_logic_vector(31 downto 0);
        FMEMU_EVENT_INFO_BCID : out std_logic_vector(31 downto 0);
        Xoff_nXon           : in     std_logic_vector(0 downto 0);
        --Xoff_nXon_en        : in     std_logic_vector(0 downto 0);
        L1A_Incr            : in     std_logic; -- signal coming from ttc decoders
        BCR_in            : in     std_logic; -- signal coming from ttc decoders
        ECR_in              : in     std_logic; -- signal coming from ttc decoders
        RST40               : in     std_logic;
        RST240              : in     std_logic;
        RST_RAM             : out     std_logic;
        CRC                 : in     std_logic_vector(31 downto 0);
        DataToCRC           : out    std_logic_vector(31 downto 0);
        CALC_CRC            : out    std_logic;
        CLR_CRC             : out    std_logic;
        CRC_EN              : out    std_logic;
        RAM_EN              : out    std_logic;
        RAM_DOUT            : in     std_logic_vector(31 downto 0);
        RAM_DVALID          : in     std_logic;
        EMUout              : out    std_logic_vector(31 downto 0);
        EMUout_dtype        : out    std_logic_vector(2 downto 0);
        sL1A_CNT_debug    : out    std_logic_vector(15 downto 0);
        BUSY_ON             : out    std_logic);
end entity FMEmu_FSM;


architecture fsm of FMEmu_FSM is

    signal sBUSY_TH_HIGH_RM                       : unsigned(15 downto 0) := (OTHERS => '1');
    signal sBUSY_TH_LOW_RM                        : unsigned(15 downto 0) := (OTHERS => '0');

    signal sINCL_CRC32_RM                         : std_logic_vector(0 downto 0) := (OTHERS => '0');

    signal sTTC_MODE_RM                           : std_logic_vector(0 downto 0) := (OTHERS => '0');
    signal sEMU_START_r1            : std_logic_vector(0 downto 0) := (OTHERS => '0');
    signal sEMU_START_r2, sEMU_START_r3           : std_logic_vector(0 downto 0) := (OTHERS => '0');
    signal sEMU_START_pulse                       : std_logic_vector(0 downto 0) := (OTHERS => '0');

    --signal sBUSY_ON_IsSet                         : std_logic := '0';
    signal sIDLE_CNT                 : std_logic_vector(15 downto 0) := (OTHERS => '0');
    signal sWORD_CNT               : unsigned(15 downto 0) := (OTHERS => '0');
    signal sL1ID_low                         : unsigned(23 downto 0) := (OTHERS => '0');
    signal sL1ID_high                       : unsigned(7 downto 0) := (OTHERS => '0');
    signal sBCID, sBCID_RM, sBCID_r1              : unsigned(31 downto 0) := (OTHERS => '0');
    signal sBCID_240 : std_logic_vector(31 downto 0);
    signal Xoff_nXon_240 : std_logic;
    signal sL1A_CNT                    : unsigned(15 downto 0) := (OTHERS => '0');
    signal sL1A_CNT_RM                            : unsigned(15 downto 0) := (OTHERS => '0');
    signal sBUSY_ENA_RM : std_logic := '0';
    signal sL1A_CNT_RM_d                            : unsigned(15 downto 0) := (OTHERS => '0');
    signal sCONSTANT_CHUNK_LENGTH_RM                       : std_logic_vector(0 downto 0) := (OTHERS => '0');

    signal sL1A_Decr                              : std_logic := '0';
    signal sL1A_Incr_r, sL1A_Incr_r1              : std_logic := '0';
    signal sXoff_nXon                                   : std_logic_vector(0 downto 0) := (OTHERS => '0');
    --signal sXon_TTC_r                             : std_logic_vector(0 downto 0) := (OTHERS => '0');
    --signal sXon_TTC_pulse                         : std_logic_vector(0 downto 0) := (OTHERS => '0');
    signal FFU_FM_EMU_T, FFU_FM_EMU_T_d  : std_logic_vector(0 downto 0) := (OTHERS => '0');

    type state_type is (SETUP_EMU, PAUSE_EMU, INIT_RAM, SEND_SOP,SEND_L1ID, SEND_BCID, SEND_DATA,SEND_CRC, SEND_EOP) ;
    signal fmemu_state  : state_type  := SETUP_EMU;
    --  signal next_state : state_type ;

    signal rBCR       : std_logic_vector(0 downto 0) := (others => '0');
    signal rECR       : std_logic_vector(0 downto 0) := (others => '0');
    signal rECR_d, rECR_dd       : std_logic_vector(0 downto 0) := (others => '0');
    signal rEMU_START   : std_logic_vector(0 downto 0) := (others => '0');
    signal rTTC_MODE     : std_logic_vector(0 downto 0) := (others => '0');
    signal rXONXOFF     : std_logic_vector(0 downto 0) := (others => '0');
    signal rINLC_CRC32   : std_logic_vector(0 downto 0) := (others => '0');
    signal rBUSY_TH_HIGH   : std_logic_vector(7 downto 0) := (others => '0');
    signal rBUSY_TH_LOW   : std_logic_vector(7 downto 0) := (others => '0');
    signal rBUSY_ENA      : std_logic := '0';
    signal rL1A_CNT     : std_logic_vector(15 downto 0) := (others => '0');
    signal rCONSTANT_CHUNK_LENGTH  : std_logic_vector(0 downto 0) := (others => '0');
    signal rIDLE_CNT    : std_logic_vector(15 downto 0) := (others => '0');
    --signal rL1ID      : std_logic_vector(31 downto 0) := (others => '0');
    signal rBCID           : std_logic_vector(31 downto 0) := (others => '0');
    signal rWORD_CNT    : std_logic_vector(15 downto 0) := (others => '0'); -- @suppress "signal rWORD_CNT is never read"

    signal vL1A_CntUP    : std_logic := '0';
    signal vL1A_CntDN   : std_logic := '0';

    signal rg_doutb     : std_logic_vector(15 downto 0);
    signal rg_enb     : std_logic := '0';
    signal L1ID_cnten    : std_logic := '0';
    signal sTTC_MODE_d  : std_logic_vector(0 downto 0) := (others => '0');
    signal BUSY_ON_t    : std_logic:='0';
    signal BUSY_ON_d    : std_logic:='0';
    signal Xoff_nXon_RM  : std_logic_vector(0 downto 0) := (others => '0');
    signal RST_RAM_FSM  :  std_logic:='0';
    --signal L1A_Incr_fifo  : std_logic_vector(0 downto 0);
    signal L1A_Incr_d     : std_logic:= '0';
    --signal L1A_Incr_din  : std_logic_vector(0 downto 0);
    signal L1A_wr_en, L1A_rd_en, L1A_full, L1A_empty    : std_logic;
    signal ECR_wr_en, ECR_rd_en, ECR_full, ECR_empty    : std_logic;
    signal ECR_in_240: std_logic;


begin
    -- register used in FMEmu
    rBCR       <= register_map_control.FMEMU_CONTROL.BCR;
    rECR       <= register_map_control.FMEMU_CONTROL.ECR;
    rEMU_START     <= register_map_control.FMEMU_CONTROL.EMU_START;
    rTTC_MODE     <= register_map_control.FMEMU_CONTROL.TTC_MODE;
    rXONXOFF     <= register_map_control.FMEMU_CONTROL.XONXOFF;
    rINLC_CRC32   <= register_map_control.FMEMU_CONTROL.INLC_CRC32;
    rBUSY_TH_HIGH   <= register_map_control.FMEMU_COUNTERS.BUSY_TH_HIGH;
    rBUSY_TH_LOW   <= register_map_control.FMEMU_COUNTERS.BUSY_TH_LOW;
    rL1A_CNT     <= register_map_control.FMEMU_COUNTERS.L1A_CNT; -- 0xFFFF send data continuously
    rCONSTANT_CHUNK_LENGTH  <= register_map_control.FMEMU_CONTROL.CONSTANT_CHUNK_LENGTH; -- default '0' --> data from emu ram
    rIDLE_CNT    <= register_map_control.FMEMU_COUNTERS.IDLE_CNT;
    rBUSY_ENA       <= register_map_control.FMEMU_CONTROL.FE_BUSY_ENABLE(0);
    FFU_FM_EMU_T(0 downto 0)  <= register_map_control.FMEMU_CONTROL.FFU_FM_EMU_T(16 downto 16);
    -- rL1ID      <= register_map_control.FMEMU_EVENT_INFO.L1ID;     -- initialise L1ID from regmap
    -- rBCID           <= register_map_control.FMEMU_EVENT_INFO.BCID;     -- initialise BCID from regmap
    -- rL1ID      <= register_map_control.FMEMU_EVENT_INFO.L1ID;     -- initialise L1ID from regmap
    rBCID           <= (others => '0');
    rWORD_CNT    <= register_map_control.FMEMU_COUNTERS.WORD_CNT;
    -- status L1ID counter & BCID cpounter
    FMEMU_EVENT_INFO_L1ID <= std_logic_vector(sL1ID_low(7 downto 0) & sL1ID_low(15 downto 8) & sL1ID_low(23 downto 16) & sL1ID_high );
    FMEMU_EVENT_INFO_BCID <= std_logic_vector(sBCID);
    --register_map_monitor.register_map_generators.FMEMU_EVENT_INFO.L1ID <= std_logic_vector(sL1ID_high & sL1ID_low);
    --register_map_monitor.register_map_generators.FMEMU_EVENT_INFO.BCID <= std_logic_vector(sBCID);

    --move sBCID from 40 MHz clock domain towards 240 using Gray counter
    cdc_bcid0 : xpm_cdc_gray
        generic map (
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            REG_OUTPUT => 0,
            SIM_ASSERT_CHK => 0,
            SIM_LOSSLESS_GRAY_CHK => 0,
            WIDTH => 32
        )
        port map (
            src_clk => REC_CLK40,
            src_in_bin => std_logic_vector(sBCID),
            dest_clk => CLK240,
            dest_out_bin => sBCID_240
        );

    cdc_Xoff_nXon : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 1
        )
        port map (
            src_clk => REC_CLK40,
            src_in => Xoff_nXon(0),
            dest_clk => CLK240,
            dest_out => Xoff_nXon_240
        );


    RST_RAM <= RST240 or RST_RAM_FSM;

    u7: entity work.Random_gen_fmemu
        generic map (
            LANE_ID     => 1)
        port map(
            rst                  => RST240,
            clk40                => REC_CLK40,
            clk240               => CLK240,
            register_map_control => register_map_control,
            rg_rst         => '0',
            rg_enb         => rg_enb,
            rg_doutb             => rg_doutb);

    L1A_Fifo : xpm_fifo_async
        generic map (
            FIFO_MEMORY_TYPE => "auto", -- String
            FIFO_WRITE_DEPTH => 16,   -- DECIMAL
            CASCADE_HEIGHT => 0,
            RELATED_CLOCKS => 0,        -- DECIMAL
            WRITE_DATA_WIDTH => 1,     -- DECIMAL
            READ_MODE => "fwft",         -- String
            FIFO_READ_LATENCY => 1,     -- DECIMAL
            FULL_RESET_VALUE => 0,      -- DECIMAL
            USE_ADV_FEATURES => "0000", -- String
            READ_DATA_WIDTH => 1,      -- DECIMAL
            CDC_SYNC_STAGES => 2,       -- DECIMAL
            WR_DATA_COUNT_WIDTH => 4,   -- DECIMAL
            PROG_FULL_THRESH => 8,     -- DECIMAL
            RD_DATA_COUNT_WIDTH => 4,   -- DECIMAL
            PROG_EMPTY_THRESH => 8,    -- DECIMAL
            DOUT_RESET_VALUE => "0",    -- String
            ECC_MODE => "no_ecc",       -- String
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            WAKEUP_TIME => 0            -- DECIMAL
        )
        port map (
            sleep => '0',
            rst => RST40,
            wr_clk => REC_CLK40,
            wr_en => L1A_wr_en,
            din => "1",
            full => L1A_full,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => open,
            almost_full => open,
            wr_ack => open,
            rd_clk => CLK240,
            rd_en => L1A_rd_en,
            dout => open,
            empty => L1A_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => open,
            almost_empty => open,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );
    L1A_wr_en <= (not L1A_full) and L1A_Incr;
    L1A_rd_en <= not L1A_empty;
    L1A_Incr_d <= L1A_rd_en;

    ECR_Fifo : xpm_fifo_async
        generic map (
            FIFO_MEMORY_TYPE => "auto", -- String
            FIFO_WRITE_DEPTH => 16,   -- DECIMAL
            CASCADE_HEIGHT => 0,
            RELATED_CLOCKS => 0,        -- DECIMAL
            WRITE_DATA_WIDTH => 1,     -- DECIMAL
            READ_MODE => "fwft",         -- String
            FIFO_READ_LATENCY => 1,     -- DECIMAL
            FULL_RESET_VALUE => 0,      -- DECIMAL
            USE_ADV_FEATURES => "0000", -- String
            READ_DATA_WIDTH => 1,      -- DECIMAL
            CDC_SYNC_STAGES => 2,       -- DECIMAL
            WR_DATA_COUNT_WIDTH => 4,   -- DECIMAL
            PROG_FULL_THRESH => 8,     -- DECIMAL
            RD_DATA_COUNT_WIDTH => 4,   -- DECIMAL
            PROG_EMPTY_THRESH => 8,    -- DECIMAL
            DOUT_RESET_VALUE => "0",    -- String
            ECC_MODE => "no_ecc",       -- String
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            WAKEUP_TIME => 0            -- DECIMAL
        )
        port map (
            sleep => '0',
            rst => RST40,
            wr_clk => REC_CLK40,
            wr_en => ECR_wr_en,
            din => "1",
            full => ECR_full,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => open,
            almost_full => open,
            wr_ack => open,
            rd_clk => CLK240,
            rd_en => ECR_rd_en,
            dout => open,
            empty => ECR_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => open,
            almost_empty => open,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );
    ECR_wr_en <= ECR_in and not ECR_full;
    ECR_rd_en <= not ECR_empty;
    ECR_in_240 <= ECR_rd_en;

    --*******************************
    -- process to monitor: L1A_CNT
    process (CLK240, RST240) is
    begin
        if (RST240 = '1') then
            sL1A_Incr_r <= '0';
        elsif (rising_edge(CLK240) ) then

            sL1A_Incr_r <= L1A_Incr_d; -- from TTC information
            sL1A_Incr_r1 <= sL1A_Incr_r;

            vL1A_CntDN <= sL1A_Decr;
            if sL1A_Incr_r1 = '0' and sL1A_Incr_r = '1' then -- create cnt dn pulse
                vL1A_CntUP <= '1';
            else
                vL1A_CntUP <= '0';
            end if;

            sTTC_MODE_d <= sTTC_MODE_RM;
            sL1A_CNT_RM_d <= sL1A_CNT_RM;
            if (sTTC_MODE_RM = "0") then
                if( (fmemu_state = SETUP_EMU) or (sL1A_CNT_RM_d /= sL1A_CNT_RM) ) then
                    sL1A_CNT <= sL1A_CNT_RM;
                else
                    if(vL1A_CntDN ='1'  ) then
                        sL1A_CNT <= sL1A_CNT - 1;
                    else
                        sL1A_CNT <= sL1A_CNT;
                    end if;
                end if;
            else
                if(sTTC_MODE_d = "0" and sTTC_MODE_RM = "1") then
                    sL1A_CNT <= (others => '0');
                else
                    if ( (vL1A_CntUP = '1') and (vL1A_CntDN ='0') and (sL1A_CNT < x"FFFE") ) then -- only increment in TTC mode
                        sL1A_CNT <= sL1A_CNT + 1;
                    elsif ( (vL1A_CntUP = '1') and (vL1A_CntDN ='1') ) then
                        sL1A_CNT <= sL1A_CNT;
                    elsif ( (vL1A_CntUP = '0') and (vL1A_CntDN ='1') and (sL1A_CNT /= x"0000")) then
                        sL1A_CNT <= sL1A_CNT - 1;
                    end if;
                end if;
            end if;
        end if;
    end process;

    --*******************************
    -- busy on logic with
    BUSY_ON <= BUSY_ON_t; -- and sDATA_SRC_SEL_RM(0);
    --  process (CLK240) is
    --  begin
    --    if (RST = '1') then
    --    BUSY_ON_t <= '0';
    --    sBUSY_ON_IsSet <= '0';
    --    elsif (rising_edge(CLK240) ) then
    --    BUSY_ON_d <= BUSY_ON_t;
    --    if (sL1A_CNT >= sBUSY_TH_HIGH_RM) then
    --      BUSY_ON_t <= '1';
    --      sBUSY_ON_IsSet <= '1';
    --    elsif ( (sL1A_CNT >= sBUSY_TH_LOW_RM) and (sBUSY_ON_IsSet = '1') ) then
    --      BUSY_ON_t <= '1';
    --      sBUSY_ON_IsSet <= '1';
    --    elsif (sBUSY_ON_IsSet = '1') then
    --      BUSY_ON_t <= '0';
    --      sBUSY_ON_IsSet <= '0';
    --    else
    --      BUSY_ON_t <= '0';
    --      sBUSY_ON_IsSet <= '0';
    --    end if;
    --    end if;
    --  end process;
    ---- for debug only
    sL1A_CNT_debug <=  std_logic_vector(sL1A_CNT);
    process (CLK240, RST240) is
    begin
        if (RST240 = '1') then
            BUSY_ON_t <= '0';
            --sBUSY_ON_IsSet <= '0';
            BUSY_ON_d <= '0';
        elsif (rising_edge(CLK240) ) then
            BUSY_ON_d <= BUSY_ON_t;
            if(sTTC_MODE_RM = "1" ) then
                if (sL1A_CNT >= sBUSY_TH_HIGH_RM) then
                    BUSY_ON_t <= sBUSY_ENA_RM;
                elsif (sL1A_CNT <= sBUSY_TH_LOW_RM) then
                    BUSY_ON_t <= '0';
                end if;
            else
                BUSY_ON_t <= '0';
            end if;
        end if;
    end process;


    --*******************************
    -- process to monitor: BCID_update
    process (REC_CLK40, RST40) is
    begin
        if (RST40 = '1') then
            sBCID <= (OTHERS => '0');
        elsif (rising_edge(REC_CLK40)) then
            sBCID_RM <= unsigned(rBCID);     -- initialise BCID from regmap
            if (rBCR = "1" or BCR_in = '1') then
                sBCID <= x"00000000";
            else
                sBCID_r1 <= sBCID_RM;
                if (sBCID_r1 /= sBCID_RM) then
                    sBCID <= sBCID_RM;
                else
                    sBCID <= sBCID + 1;
                end if;
            end if;
        end if;
    end process;


    ----######################################
    --- handle XonXoff
    process (CLK240) is
    begin
        if (rising_edge(CLK240)) then

            sEMU_START_r1 <= rEMU_START;
            sEMU_START_r2 <= sEMU_START_r1;
            sEMU_START_r3 <= sEMU_START_r2;
            if (sEMU_START_r3 = "0" and sEMU_START_r2 = "1") then
                sEMU_START_pulse <= "1";
            else
                sEMU_START_pulse <= "0";
            end if;

            if (rTTC_MODE = "1") then
                if(rXONXOFF = "1") then
                    sXoff_nXon <= Xoff_nXon_RM; --sXon_TTC_pulse;
                else
                    sXoff_nXon <= "0";
                end if;
            else -- (register_map_control.XXX.TTC_MODE = '0')
                sXoff_nXon <= rXONXOFF; -- default '0' --> regmap mode
            end if;
        end if ; -- Clock
    end process;


    ----######################################
    -- L1ID count
    process (CLK240)
    begin
        if (rising_edge(CLK240)) then
            rECR_d <= rECR;
            rECR_dd <= rECR_d;
            if (RST240 = '1') then
                sL1ID_low <= (others => '0'); -- (OTHERS => '0');
                sL1ID_high <= (others => '0'); -- (OTHERS => '0');
            elsif (rECR_dd = "0" and rECR_d = "1") then
                sL1ID_low <= (OTHERS => '0'); --sL1ID_low;
                sL1ID_high <= sL1ID_high + 1;
            elsif(ECR_in_240 = '1') then
                sL1ID_low <= (OTHERS => '0');
                sL1ID_high <= sL1ID_high + 1;
            elsif(L1ID_cnten = '1') then
                if (rECR_d = "0") then
                    sL1ID_low <= sL1ID_low + 1; -- (OTHERS => '0');
                else
                    sL1ID_low <= (others => '0'); --sL1ID_low;
                end if;
                sL1ID_high <= sL1ID_high;
            else
                sL1ID_low <= sL1ID_low; -- (OTHERS => '0');
                sL1ID_high <= sL1ID_high;
            end if;
        end if;
    end process;


    ----######################################
    -- pcie clock to 240M clock synch
    process (CLK240)
    begin
        if (rising_edge(CLK240)) then
            sINCL_CRC32_RM     <= rINLC_CRC32;
            sBUSY_TH_HIGH_RM   <= x"FF" & unsigned(rBUSY_TH_HIGH);
            sBUSY_TH_LOW_RM   <= x"00" & unsigned(rBUSY_TH_LOW);
            sTTC_MODE_RM     <= rTTC_MODE; -- default '0' --> regmap mode
            sCONSTANT_CHUNK_LENGTH_RM   <= rCONSTANT_CHUNK_LENGTH; -- default '0' --> data from emu ram
            sL1A_CNT_RM <= unsigned(rL1A_CNT); -- 0xFFFF send data continuously
            Xoff_nXon_RM(0) <= Xoff_nXon_240;
            CRC_EN <= rINLC_CRC32(0);
            sBUSY_ENA_RM <= rBUSY_ENA;

        end if;
    end process;

    process (CLK240)
    begin
        if (rising_edge(CLK240)) then
            FFU_FM_EMU_T_d     <= FFU_FM_EMU_T;
        --FFU_FM_EMU_T_pulse_d <= FFU_FM_EMU_T_pulse;
        --if(FFU_FM_EMU_T_d = "0" and FFU_FM_EMU_T="1") then
        --FFU_FM_EMU_T_pulse <= "1";
        --elsif(fmemu_state = SEND_EOP ) then
        --FFU_FM_EMU_T_pulse <= "0";
        --end if;
        end if;
    end process;


    ----######################################
    -- fmemu states transaction
    process (CLK240)
    begin
        if (rising_edge(CLK240)) then
            if (RST240 = '1' ) then
                fmemu_state <= SETUP_EMU;
                EMUout_dtype <="011"; -- default send idle
                EMUout <= x"FFFF0000";
                DataToCRC <= x"FFFFFFFF";
                CALC_CRC <= '0';
                CLR_CRC <=  '0';
                RAM_EN <=  '0';
                sL1A_Decr <=  '0';
                rg_enb <= '0';
                L1ID_cnten <= '0';
                RST_RAM_FSM <= '0';
            else
                if(BUSY_ON_d = '0' and BUSY_ON_t = '1' and sBUSY_ENA_RM = '1') then
                    EMUout_dtype <="100"; -- Busy on (SOB)
                elsif(BUSY_ON_d = '1' and BUSY_ON_t = '0' and sBUSY_ENA_RM = '1') then
                    EMUout_dtype <="101"; -- Busy off (EOB)
                elsif(sXoff_nXon = "0") then
                    EMUout_dtype <="011"; -- default send idle
                    EMUout <= x"FFFF0000";
                    DataToCRC <= x"FFFFFFFF";
                    CALC_CRC <= '0';
                    CLR_CRC <=  '0';
                    RAM_EN <=  '0';
                    sL1A_Decr <=  '0';
                    rg_enb <= '0';
                    L1ID_cnten <= '0';
                    RST_RAM_FSM <= '0';
                    --      if(BUSY_ON_d = '0' and BUSY_ON_t = '1') then
                    --        EMUout_dtype <="100"; -- default send idle
                    --      elsif(BUSY_ON_d = '1' and BUSY_ON_t = '0') then
                    --        EMUout_dtype <="101"; -- default send idle
                    --      elsif(sXoff_nXon = "0") then
                    case fmemu_state is
                        -- SETUP_EMU -----------------------------------------------------------
                        -- Init:
                        when SETUP_EMU =>
                            EMUout_dtype <="011"; -- default send idle
                            sIDLE_CNT <= rIDLE_CNT;
                            CLR_CRC <=  '1';

                            -- fmemu_state logic
                            if (sTTC_MODE_RM = "0") then -- in regmap mode wait for start pulse and L1A count.
                                if(sEMU_START_pulse = "1" and sL1A_CNT /= x"0000") then
                                    fmemu_state <= PAUSE_EMU;
                                else
                                    fmemu_state <= SETUP_EMU; -- prevent start of emu from reg map without L1A_CNT set to other value than '0'
                                end if;
                            elsif(sL1A_CNT /= x"0000") then
                                fmemu_state <= PAUSE_EMU;
                            else
                                fmemu_state <= SETUP_EMU; -- prevent start of emu from reg map without L1A_CNT set to other value than '0'
                            end if;

                        -- PAUSE_EMU ---  --------------------------------------------------------
                        when PAUSE_EMU =>
                            EMUout_dtype <= "011";
                            EMUout <= x"FFFFFFFF";
                            DataToCRC <= x"FFFFFFFF";
                            RST_RAM_FSM <= '1';
                            if(FFU_FM_EMU_T_d = "0") then
                                if (sIDLE_CNT /= x"0000") then
                                    sIDLE_CNT <= sIDLE_CNT - 1;
                                else
                                    sIDLE_CNT <= sIDLE_CNT;
                                end if;
                                -- fmemu_state logic
                                if (sIDLE_CNT /= "0000000000000000") then
                                    fmemu_state <= PAUSE_EMU ; -- wait for Xon, L1A trigger (in TTC mode) or finish idles
                                else
                                    fmemu_state <= INIT_RAM;
                                end if;
                            else
                                fmemu_state <= SETUP_EMU;
                            end if;

                        when INIT_RAM =>
                            EMUout_dtype <= "011";
                            fmemu_state <= SEND_SOP;
                            RAM_EN <= '1';
                        when SEND_SOP =>
                            EMUout_dtype <= "001"; -- start of packet
                            -- fmemu_state logic
                            RAM_EN <= '1';
                            fmemu_state <= SEND_L1ID ; --always send L1ID and BCID
                        --            RST_RAM_FSM <= '1';
                        -- SEND_l1ID ---  --------------------------------------------------------
                        -- send L1ID + data type to FIFO34
                        when SEND_L1ID =>
                            sIDLE_CNT <= rIDLE_CNT; -- reset IDLE counter to reg map value
                            EMUout_dtype <= "000"; -- start of packet
                            EMUout <= std_logic_vector(sL1ID_low(7 downto 0) & sL1ID_low(15 downto 8) & sL1ID_low(23 downto 16) & sL1ID_high);
                            DataToCRC <= std_logic_vector(sL1ID_low(7 downto 0) & sL1ID_low(15 downto 8) & sL1ID_low(23 downto 16) & sL1ID_high);
                            CALC_CRC <= '1';
                            L1ID_cnten <= '1';
                            if(sCONSTANT_CHUNK_LENGTH_RM = "0") then
                                sWORD_CNT <= unsigned(rg_doutb); --(rWORD_CNT);
                            else
                                sWORD_CNT <= unsigned(rWORD_CNT);
                            end if;
                            rg_enb <= '1';
                            -- "trigger L1A_CNT decrease in clocked process
                            if (sL1A_CNT /= x"FFFF") then -- 0xFFFF set by regmap and loop data continiously
                                sL1A_Decr <= '1';
                            end if;

                            -- fmemu_state logic
                            RAM_EN <= '1';
                            fmemu_state <= SEND_BCID ; --always send L1ID and BCID

                        -- SEND_BCID ---  --------------------------------------------------------
                        -- send BCID + data type to FIFO34
                        when SEND_BCID =>
                            EMUout_dtype <= "000"; -- intermediate word of current packet
                            EMUout <= std_logic_vector(sBCID_240);
                            DataToCRC <= std_logic_vector(sBCID_240);
                            CALC_CRC <= '1';
                            RAM_EN <= '1';
                            fmemu_state <= SEND_DATA ;

                        -- SEND_DATA ---  --------------------------------------------------------
                        -- send payload + data type to FIFO34
                        when SEND_DATA =>
                            --  if  (sDATA_SRC_SEL_RM = "0") then-- default val, send RAM data
                            if (sWORD_CNT <= 2) then
                                RAM_EN <= '0';
                            else
                                RAM_EN <= '1';
                            end if;

                            if (RAM_DVALID = '1') then
                                EMUout_dtype <= "000"; -- intermediate word of current packet
                                EMUout <= RAM_DOUT;
                                DataToCRC <= RAM_DOUT;
                                CALC_CRC <= '1';
                                if(sWORD_CNT /= x"00000000") then
                                    sWORD_CNT <= sWORD_CNT - 1;
                                else
                                    sWORD_CNT <= sWORD_CNT;
                                end if;
                            else
                                EMUout_dtype <= "011"; -- intermediate word of current packet
                                EMUout <= RAM_DOUT;
                                DataToCRC <= RAM_DOUT;
                                CALC_CRC <= '0';
                                sWORD_CNT <= sWORD_CNT;
                            end if;
                            --  end if;

                            -- fmemu_state logic
                            if (sWORD_CNT /= x"0000") then
                                fmemu_state <= SEND_DATA ;
                            elsif (sINCL_CRC32_RM = "0") then
                                fmemu_state <= SEND_EOP;
                            else
                                fmemu_state <= SEND_CRC ;
                            end if ;
                        when SEND_CRC =>
                            RST_RAM_FSM <= '1';
                            EMUout_dtype <= "000"; -- End Of Packet
                            EMUout <= CRC;
                            fmemu_state <= SEND_EOP ;
                        when SEND_EOP =>
                            EMUout_dtype <= "010"; -- End Of Packet
                            -- next_state logic
                            if (sL1A_CNT = x"0000") then
                                fmemu_state <= SETUP_EMU ;
                            else --(sL1A_CNT /= x"0000")
                                fmemu_state <= PAUSE_EMU ;
                            end if ;
                    end case;
                end if;
            end if;
        end if;
    end process;
end architecture fsm ; -- of FMEmu_FSM


--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Rene Habraken
--!               RHabraken
--!               Mesfin Gebyehu
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.


--!------------------------------------------------------------------------------
--!
--!           NIKHEF - National Institute for Subatomic Physics
--!
--!                       Electronics Department
--!
--!-----------------------------------------------------------------------------
--! @class felix_top
--!
--!
--! @author      Andrea Borga    (andrea.borga@nikhef.nl)<br>
--!              Frans Schreuder (frans.schreuder@nikhef.nl)
--!
--!
--! @date        07/01/2015    created
--!
--! @version     1.0
--!
--! @brief
--! Top level for the FELIX project, containing GBT, CentralRouter and PCIe DMA core
--!
--!
--!
--! @detail
--!
--!-----------------------------------------------------------------------------
--! @TODO
--!
--!
--! ------------------------------------------------------------------------------
--
--! @brief ieee



library ieee, UNISIM;
    use ieee.numeric_std.all;
    use UNISIM.VCOMPONENTS.all;
    use ieee.std_logic_unsigned.all; -- @suppress "Deprecated package"
    use ieee.std_logic_1164.all;
    use work.FELIX_gbt_package.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;
    use work.FMTransceiverPackage.all;
library xpm;
    use xpm.vcomponents.all;

entity FMEmu_top is
    generic(
        NUMBER_OF_INTERRUPTS            : integer := 8;
        NUMBER_OF_DESCRIPTORS           : integer := 8;
        APP_CLK_FREQ                    : integer := 200;
        BUILD_DATETIME                  : std_logic_vector(39 downto 0) := x"0000FE71CE";
        GBT_NUM                         : integer := 24; -- number of GBT channels
        AUTOMATIC_CLOCK_SWITCH          : boolean := true;
        USE_BACKUP_CLK                  : boolean := true; -- true to use 100/200 mhz board crystal, false to use TTC clock
        CARD_TYPE                       : integer := 712;
        GIT_HASH                        : std_logic_vector(159 downto 0) := x"0000000000000000000000000000000000000000";
        GIT_TAG                         : std_logic_vector(127 downto 0) := x"00000000000000000000000000000000";
        GIT_COMMIT_NUMBER               : integer := 0;
        COMMIT_DATETIME                 : std_logic_vector(39 downto 0) := x"0000FE71CE";
        FIRMWARE_MODE                   : integer := FIRMWARE_MODE_FMEMU;
        USE_Si5324_RefCLK               : boolean := false;
        GENERATE_XOFF                   : boolean := false; -- FromHost Xoff transmission enabled on Full mode busy
        TTC_SYS_SEL                     : std_logic := '0'; -- 0: TTC, 1: LTITTC P2P
        DATA_WIDTH                      : integer := 256;
        PCIE_LANES                      : integer := 8;
        BLOCKSIZE                       : integer := 1024;
        ENDPOINTS                       : integer := 2;
        GTREFCLKS                       : integer := 2;
        SIMULATION                      : boolean := false;
        USE_VERSAL_CPM                  : boolean := false; --set to true for BNL182
        ENABLE_XVC                      : boolean := false
    );
    port (
        BUSY_OUT                  : out    std_logic_vector(NUM_BUSY_OUTPUTS(CARD_TYPE)-1 downto 0);
        CLK40_FPGA2LMK_N          : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        CLK40_FPGA2LMK_P          : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        CLK_TTC_N                 : in     std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0); -- @suppress "Unused port: CLK_TTC_N is not used in work.FMEmu_top(structure)"
        CLK_TTC_P                 : in     std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0); -- @suppress "Unused port: CLK_TTC_P is not used in work.FMEmu_top(structure)"
        DATA_TTC_N                : in     std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0); -- @suppress "Unused port: DATA_TTC_N is not used in work.FMEmu_top(structure)"
        DATA_TTC_P                : in     std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0); -- @suppress "Unused port: DATA_TTC_P is not used in work.FMEmu_top(structure)"
        GTREFCLK_Si5324_N_IN      : in     std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0); -- @suppress "Unused port: GTREFCLK_Si5324_N_IN is not used in work.FMEmu_top(structure)"
        GTREFCLK_Si5324_P_IN      : in     std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0); -- @suppress "Unused port: GTREFCLK_Si5324_P_IN is not used in work.FMEmu_top(structure)"
        --OPTO_LOS                  : in     std_logic_vector(NUM_OPTO_LOS(CARD_TYPE)-1 downto 0);
        I2C_SMB                   : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        I2C_SMBUS_CFG_nEN         : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        I2C_nRESET_PCIe           : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        LMK_CLK                   : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_DATA                  : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_GOE                   : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_LD                    : in     std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_LE                    : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_SYNCn                 : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LOL_ADN                   : in     std_logic_vector(NUM_ADN(CARD_TYPE)-1 downto 0); -- @suppress "Unused port: LOL_ADN is not used in work.FMEmu_top(structure)"
        LOS_ADN                   : in     std_logic_vector(NUM_ADN(CARD_TYPE)-1 downto 0); -- @suppress "Unused port: LOS_ADN is not used in work.FMEmu_top(structure)"
        MGMT_PORT_EN              : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        NT_PORTSEL                : out    std_logic_vector(NUM_NT_PORTSEL(CARD_TYPE)-1 downto 0);
        PCIE_PERSTn_out           : out    std_logic_vector(NUM_PEX(CARD_TYPE)*2-1 downto 0);
        PEX_PERSTn                : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        PEX_SCL                   : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        PEX_SDA                   : inout  std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        select_bifurcation        : in std_logic_vector(NUM_BIFURCATION_SELECT(CARD_TYPE)-1 downto 0);
        PORT_GOOD                 : in     std_logic_vector(NUM_PEX(CARD_TYPE)*8-1 downto 0);
        Perstn_open               : in     std_logic_vector(NUM_PEX(CARD_TYPE)*2-1 downto 0);  -- @suppress "Unused port: Perstn_open is not used in work.FMEmu_top(structure)"
        GTREFCLK_N_IN             : in     std_logic_vector(GTREFCLKS-1 downto 0);
        GTREFCLK_P_IN             : in     std_logic_vector(GTREFCLKS-1 downto 0);
        GTREFCLK1_N_IN            : in     std_logic_vector(NUM_GTREFCLK1S(GTREFCLKS,FIRMWARE_MODE,CARD_TYPE)-1 downto 0); -- @suppress "Unused port: GTREFCLK1_N_IN is not used in work.FMEmu_top(structure)"
        GTREFCLK1_P_IN            : in     std_logic_vector(NUM_GTREFCLK1S(GTREFCLKS,FIRMWARE_MODE,CARD_TYPE)-1 downto 0); -- @suppress "Unused port: GTREFCLK1_P_IN is not used in work.FMEmu_top(structure)"
        LMK_N                     : in     std_logic_vector(NUM_LMK(CARD_TYPE)*8-1 downto 0); -- @suppress "Unused port: LMK_N is not used in work.FMEmu_top(structure)"
        LMK_P                     : in     std_logic_vector(NUM_LMK(CARD_TYPE)*8-1 downto 0); -- @suppress "Unused port: LMK_P is not used in work.FMEmu_top(structure)"
        RX_N                      : in     std_logic_vector(GBT_NUM-1 downto 0);
        RX_P                      : in     std_logic_vector(GBT_NUM-1 downto 0);
        RX_N_LTITTC               : in     std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0); -- @suppress "Unused port: RX_N_LTITTC is not used in work.FMEmu_top(structure)"
        RX_P_LTITTC               : in     std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0); -- @suppress "Unused port: RX_P_LTITTC is not used in work.FMEmu_top(structure)"
        TX_N                      : out    std_logic_vector(GBT_NUM-1 downto 0);
        TX_P                      : out    std_logic_vector(GBT_NUM-1 downto 0);
        SCL                       : inout  std_logic;
        SDA                       : inout  std_logic;
        SHPC_INT                  : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        SI5345_A                  : out    std_logic_vector(NUM_SI5345(CARD_TYPE)*2-1 downto 0);
        SI5345_INSEL              : out    std_logic_vector(NUM_SI5345(CARD_TYPE)*2-1 downto 0);
        SI5345_OE                 : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_RSTN               : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_SEL                : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_nLOL               : in     std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_FINC_B             : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_FDEC_B             : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_INTR_B             : in     std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        STN0_PORTCFG              : out    std_logic_vector(NUM_STN0_PORTCFG(CARD_TYPE)-1 downto 0);
        STN1_PORTCFG              : out    std_logic_vector(NUM_STN1_PORTCFG(CARD_TYPE)-1 downto 0);
        SmaOut                    : out    std_logic_vector(NUM_SMA(CARD_TYPE)-1 downto 0);
        TACH                      : in     std_logic_vector(NUM_TACH(CARD_TYPE)-1 downto 0);
        FAN_FAIL_B                : in     std_logic_vector(NUM_FAN_FAIL(CARD_TYPE)-1 downto 0);
        FAN_FULLSP                : in     std_logic_vector(NUM_FAN_FAIL(CARD_TYPE)-1 downto 0);
        FAN_OT_B                  : in     std_logic_vector(NUM_FAN_FAIL(CARD_TYPE)-1 downto 0);
        FAN_PWM                   : out    std_logic_vector(NUM_FAN_PWM(CARD_TYPE)-1 downto 0);
        FF3_PRSNT_B               : in     std_logic_vector(NUM_FF3_PRSTN(CARD_TYPE)-1 downto 0);
        IOEXPAN_INTR_B            : in     std_logic_vector(NUM_IOEXP(CARD_TYPE)-1 downto 0);
        IOEXPAN_RST_B             : out    std_logic_vector(NUM_IOEXP(CARD_TYPE)-1 downto 0);
        TESTMODE                  : out    std_logic_vector(NUM_TESTMODE(CARD_TYPE)-1 downto 0);
        UPSTREAM_PORTSEL          : out    std_logic_vector(NUM_UPSTREAM_PORTSEL(CARD_TYPE)-1 downto 0);
        app_clk_in_n              : in     std_logic;
        app_clk_in_p              : in     std_logic;
        clk_adn_160_out_n         : out    std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0);
        clk_adn_160_out_p         : out    std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0);
        clk40_ttc_ref_out_n       : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0); -- Towards Si5345 CLKIN
        clk40_ttc_ref_out_p       : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0); -- Towards Si5345 CLKIN
        emcclk                    : in     std_logic_vector(NUM_EMCCLK(CARD_TYPE)-1 downto 0);
        i2cmux_rst                : out    std_logic_vector(NUM_I2C_MUXES(CARD_TYPE)-1 downto 0);
        leds                      : out    std_logic_vector(NUM_LEDS(CARD_TYPE)-1 downto 0);
        flash_SEL                 : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_a                   : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)*25-1 downto 0);
        flash_a_msb               : inout  std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)*2-1 downto 0);
        flash_adv                 : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_cclk                : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_ce                  : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_d                   : inout  std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)*16-1 downto 0);
        flash_re                  : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_we                  : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        opto_inhibit              : out    std_logic_vector(NUM_OPTO_LOS(CARD_TYPE)-1 downto 0);
        si5324_resetn             : out    std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0);
        pcie_rxn                  : in     std_logic_vector((ENDPOINTS*PCIE_LANES)-1 downto 0);
        pcie_rxp                  : in     std_logic_vector((ENDPOINTS*PCIE_LANES)-1 downto 0);
        pcie_txn                  : out    std_logic_vector((ENDPOINTS*PCIE_LANES)-1 downto 0);
        pcie_txp                  : out    std_logic_vector((ENDPOINTS*PCIE_LANES)-1 downto 0); --! PCIe link lanes
        sys_clk_n                 : in     std_logic_vector(ENDPOINTS-1 downto 0);
        sys_clk_p                 : in     std_logic_vector(ENDPOINTS-1 downto 0); --! 100MHz PCIe reference clock
        sys_reset_n               : in     std_logic; --! Active-low system reset from PCIe interface);
        PCIE_PWRBRK               : in     std_logic_vector(NUM_PCIE_PWRBRK(CARD_TYPE)-1 downto 0);
        PCIE_WAKE_B               : in     std_logic_vector(NUM_PCIE_PWRBRK(CARD_TYPE)-1 downto 0);
        QSPI_RST_B                : out    std_logic_vector(NUM_QSPI_RST(CARD_TYPE)-1 downto 0);
        TB_trigger_P              : in     std_logic_vector(NUM_TB_TRIGGERS(CARD_TYPE)-1 downto 0); -- @suppress "Unused port: TB_trigger_P is not used in work.FMEmu_top(structure)"
        TB_trigger_N              : in     std_logic_vector(NUM_TB_TRIGGERS(CARD_TYPE)-1 downto 0); -- @suppress "Unused port: TB_trigger_N is not used in work.FMEmu_top(structure)"
        uC_reset_N                : out    std_logic_vector(NUM_UC_RESET_N(CARD_TYPE)-1 downto 0);
        DDR_in                    : in     DDR_in_array_type(0 to NUM_DDR(CARD_TYPE)-1);
        DDR_out                   : out    DDR_out_array_type(0 to NUM_DDR(CARD_TYPE)-1);
        DDR_inout                 : inout  DDR_inout_array_type(0 to NUM_DDR(CARD_TYPE)-1);
        LPDDR_in                  : in     LPDDR_in_array_type(0 to NUM_LPDDR(CARD_TYPE)/2-1);
        LPDDR_out                 : out    LPDDR_out_array_type(0 to NUM_LPDDR(CARD_TYPE)-1);
        LPDDR_inout               : inout  LPDDR_inout_array_type(0 to NUM_LPDDR(CARD_TYPE)-1)
    );
end entity FMEmu_top;


architecture structure of FMEmu_top is
    function USE_ULTRARAM(brd: integer) return boolean is
    begin
        if brd = 180 or brd = 181 or brd = 182 or brd = 128 then
            return true;
        else
            return false;
        end if;
    end function;

    signal rst_hw                                : std_logic;
    --signal clk_adn_160                         : std_logic;
    signal global_appreg_clk                   : std_logic;
    signal global_reset_soft_appreg_clk        : std_logic;
    signal global_rst_soft_40                  : std_logic;

    signal global_register_map_control_appreg_clk : register_map_control_type;
    --signal global_register_map_40_control      : register_map_control_type;
    signal register_map_gen_board_info         : register_map_gen_board_info_type;
    signal register_map_link_monitor           : register_map_link_monitor_type;
    signal register_map_hk_monitor             : register_map_hk_monitor_type;
    signal clk40                                 : std_logic;
    --signal clk_ttc_40_s                          : std_logic;
    signal clk10_xtal                            : std_logic;
    signal clk40_xtal                            : std_logic;
    signal MMCM_Locked_out                       : std_logic;
    signal MMCM_OscSelect_out                    : std_logic;
    signal reset                                 : std_logic;
    --signal ttc_TTC_BUSY_mon_array                : busyOut_array_type(23 downto 0);
    --signal TTC_BUSY_mon_array                    : busyOut_array_type(GBT_NUM-1 downto 0);
    signal RX_120b                             : array_120b(0 to GBT_NUM-1);
    signal TXUSRCLK_240                        : std_logic_vector(GBT_NUM-1 downto 0);
    signal EMU32b_EN                           : std_logic_vector(GBT_NUM-1 downto 0);
    type EMU32b_dout_type                      is array(GBT_NUM-1 downto 0) of std_logic_vector(31 downto 0);
    signal EMU32b_dout                         : EMU32b_dout_type;
    signal EMU32b_dvalid                       : std_logic_vector(GBT_NUM-1 downto 0);
    type CRC_type                              is array(GBT_NUM-1 downto 0) of std_logic_vector(31 downto 0);
    signal CRC                                 : CRC_type;
    signal DataToCRC                           : CRC_type;
    signal Clr_CRC                             : std_logic_vector(GBT_NUM-1 downto 0);
    signal Calc                                : std_logic_vector(GBT_NUM-1 downto 0);
    type fifo34_din_type                       is array(GBT_NUM-1 downto 0) of std_logic_vector(31 downto 0);
    signal fifo34_din                          : fifo34_din_type;
    type fifo34_dtype_type                     is array(GBT_NUM-1 downto 0) of std_logic_vector(2 downto 0);
    signal fifo34_dtype                        : fifo34_dtype_type;
    type sL1A_CNT_debug_type                     is array(GBT_NUM-1 downto 0) of std_logic_vector(15 downto 0);
    signal sL1A_CNT_debug                      : sL1A_CNT_debug_type; -- @suppress "signal sL1A_CNT_debug is never read"
    signal tx_din_array                        : array_32b(0 to GBT_NUM-1);
    signal tx_kin_array                        : array_4b(0 to GBT_NUM-1);
    signal Xoff_nXon                : std_logic_vector(GBT_NUM-1 downto 0);
    signal BUSY_OUT_fsm               : std_logic_vector(GBT_NUM-1 downto 0);
    signal RST_RAM               : std_logic_vector(GBT_NUM-1 downto 0);
    type FMEMU_EVENT_INFO_type                      is array(GBT_NUM-1 downto 0) of std_logic_vector(31 downto 0);
    signal FMEMU_EVENT_INFO_L1ID  : FMEMU_EVENT_INFO_type;
    signal FMEMU_EVENT_INFO_BCID  : FMEMU_EVENT_INFO_type;
    signal register_map_generators : register_map_generators_type;

    signal RXUSRCLK                            : std_logic_vector(GBT_NUM-1 downto 0);
    signal versal_sys_reset_n : std_logic;
    signal PCIE_PERSTn : std_logic;

    signal WupperToCPM: WupperToCPM_array_type(0 to 1);
    signal CPMToWupper: CPMToWupper_array_type(0 to 1);



    COMPONENT ila_gbt_rx_frame -- @suppress "Component declaration "ila_gbt_rx_frame" has none or multiple matching entity declarations"
        PORT (
            clk : IN STD_LOGIC;
            probe0 : IN STD_LOGIC_VECTOR(79 DOWNTO 0)
        );
    END COMPONENT  ;
    signal lnk_up : std_logic_vector(1 downto 0);
    signal LTI_RECEIVER_ACTIVE : std_logic_vector(GBT_NUM-1 downto 0);

    signal TTC_lti_data : TTC_data_array_type(0 to GBT_NUM-1);
    signal clk40_lti_gbt: std_logic_vector(GBT_NUM-1 downto 0);
    signal clk160: std_logic;
    signal axi_mosi_ttc_lti : axi_mosi_type;
    signal apb3_axi_clk : std_logic;
    signal cips2LTI_gpio : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal axi_miso_ttc_lti : axi_miso_type;
    signal LTI2cips_gpio : STD_LOGIC_VECTOR ( 3 downto 0 );
begin

    NT_PORTSEL <= (others => '1');
    TESTMODE <= (others => '0');
    UPSTREAM_PORTSEL <= (others => '0');
    g_SNT0_PORTCFG: if NUM_STN0_PORTCFG(CARD_TYPE) = 2 generate
        STN0_PORTCFG <= "0Z";
    end generate;
    g_SNT1_PORTCFG: if NUM_STN1_PORTCFG(CARD_TYPE) = 2 generate
        STN1_PORTCFG <= "01";
    end generate;
    SmaOut <= (others => '0');
    I2C_nRESET_PCIe <= (others => '1');
    uC_reset_N <= (others => '1');

    g_versal: if CARD_TYPE = 180 generate
        PCIE_PERSTn <= versal_sys_reset_n;
    end generate;

    g_noVersal: if CARD_TYPE /= 180 generate
        PCIE_PERSTn <= sys_reset_n;
    end generate;

    clk0: entity work.clock_and_reset
        generic map(
            APP_CLK_FREQ           => APP_CLK_FREQ,
            USE_BACKUP_CLK         => USE_BACKUP_CLK,
            AUTOMATIC_CLOCK_SWITCH => AUTOMATIC_CLOCK_SWITCH,
            CARD_TYPE              => CARD_TYPE,
            FIRMWARE_MODE          => FIRMWARE_MODE)
        port map(
            MMCM_Locked_out      => MMCM_Locked_out,
            MMCM_OscSelect_out   => MMCM_OscSelect_out,
            app_clk_in_n         => app_clk_in_n,
            app_clk_in_p         => app_clk_in_p,
            --cdrlocked_in         => cdrlocked_out,
            clk10_xtal           => clk10_xtal,
            clk100               => open,
            clk160               => clk160,
            clk240               => open,
            clk250               => open,
            clk320               => open,
            clk365               => open,
            clk40                => clk40,
            clk40_xtal           => clk40_xtal,
            clk80                => open,
            clk_adn_160          => clk160,
            clk_adn_160_out_n    => clk_adn_160_out_n,
            clk_adn_160_out_p    => clk_adn_160_out_p,
            clk_ttc_40           => clk40_xtal,
            clk40_stable_out     => open,
            clk_ttcfx_ref_out_n  => clk40_ttc_ref_out_n,
            clk_ttcfx_ref_out_p  => clk40_ttc_ref_out_p,
            register_map_control => global_register_map_control_appreg_clk,
            reset_out            => rst_hw,
            sys_reset_n          => PCIE_PERSTn);

    g_endpoints: for pcie_endpoint in 0 to ENDPOINTS-1 generate
        --signal decoding_aclk64                     : std_logic; --!TODO: Connect to 400MHz clock -- @suppress "signal decoding_aclk64 is never written"
        signal rst_soft_40                         : std_logic;
        signal reset_soft_appreg_clk               : std_logic;

        signal appreg_clk: std_logic;

        signal register_map_control_appreg_clk       : register_map_control_type;
        signal wupper_clk250 : std_logic;

    begin

        g_assign_endpoint0: if pcie_endpoint = 0 generate
            global_appreg_clk <= appreg_clk;
            global_register_map_control_appreg_clk <= register_map_control_appreg_clk;
            global_reset_soft_appreg_clk <= reset_soft_appreg_clk;
            global_rst_soft_40 <= rst_soft_40;
        end generate;


        pcie0: entity work.wupper
            generic map(
                NUMBER_OF_INTERRUPTS => NUMBER_OF_INTERRUPTS,
                NUMBER_OF_DESCRIPTORS => NUMBER_OF_DESCRIPTORS,
                BUILD_DATETIME => BUILD_DATETIME,
                CARD_TYPE => CARD_TYPE,
                GIT_HASH => GIT_HASH,
                COMMIT_DATETIME => COMMIT_DATETIME,
                GIT_TAG => GIT_TAG,
                GIT_COMMIT_NUMBER => GIT_COMMIT_NUMBER,
                GBT_NUM => GBT_NUM,
                FIRMWARE_MODE => FIRMWARE_MODE,
                PCIE_ENDPOINT => pcie_endpoint,
                PCIE_LANES => PCIE_LANES,
                DATA_WIDTH => DATA_WIDTH,
                SIMULATION => SIMULATION,
                BLOCKSIZE => BLOCKSIZE,
                USE_ULTRARAM => USE_ULTRARAM(CARD_TYPE),
                ENABLE_XVC => ENABLE_XVC,
                FROMHOSTFIFO_FWFT => false
            )
            port map(
                appreg_clk => appreg_clk,
                sync_clk => clk40,
                flush_fifo => open,
                interrupt_call => (others => '0'),
                lnk_up => lnk_up(pcie_endpoint),
                pcie_rxn => pcie_rxn(pcie_endpoint*8+(PCIE_LANES-1) downto pcie_endpoint*8),
                pcie_rxp => pcie_rxp(pcie_endpoint*8+(PCIE_LANES-1) downto pcie_endpoint*8),
                pcie_txn => pcie_txn(pcie_endpoint*8+(PCIE_LANES-1) downto pcie_endpoint*8),
                pcie_txp => pcie_txp(pcie_endpoint*8+(PCIE_LANES-1) downto pcie_endpoint*8),
                pll_locked => open,
                register_map_control_sync => open,
                register_map_control_appreg_clk => register_map_control_appreg_clk,
                register_map_gen_board_info => register_map_gen_board_info,
                register_map_crtohost_monitor => register_map_crtohost_monitor_c,
                register_map_crfromhost_monitor => register_map_crfromhost_monitor_c,
                register_map_decoding_monitor => register_map_decoding_monitor_c,
                register_map_encoding_monitor => register_map_encoding_monitor_c,
                register_map_gbtemu_monitor => register_map_gbtemu_monitor_c,
                register_map_link_monitor => register_map_link_monitor,
                register_map_ttc_monitor => register_map_ttc_monitor_c,
                register_map_ltittc_monitor => register_map_ltittc_monitor_c,
                register_map_xoff_monitor => register_map_xoff_monitor_c,
                register_map_hk_monitor => register_map_hk_monitor,
                register_map_generators => register_map_generators,
                wishbone_monitor => wishbone_monitor_c,
                regmap_mrod_monitor => regmap_mrod_monitor_c,
                ipbus_monitor => ipbus_monitor_c,
                dma_enable_out => open,
                reset_hard => open,
                reset_soft => rst_soft_40,
                reset_soft_appreg_clk => reset_soft_appreg_clk,
                --reset_hw_in => rst_hw,
                sys_clk_n => sys_clk_n(pcie_endpoint),
                sys_clk_p => sys_clk_p(pcie_endpoint),
                sys_reset_n => PCIE_PERSTn,
                tohost_busy_out => open,
                fromHostFifo_dout => open,
                fromHostFifo_empty => open,
                fromHostFifo_rd_clk => wupper_clk250,
                fromHostFifo_rd_en => '0',
                fromHostFifo_rst => reset_soft_appreg_clk,
                toHostFifo_din => (others => (others => '0')),
                toHostFifo_prog_full => open,
                toHostFifo_rst => reset_soft_appreg_clk,
                toHostFifo_wr_clk => wupper_clk250,
                toHostFifo_wr_en => (others => '0'),
                clk250_out => wupper_clk250,
                master_busy_in => '0',
                toHostFifoBusy_out => open,
                Versal_network_device_fromHost_full_i => '0',
                Versal_network_device_fromHost_prog_full_i => '0',
                Versal_network_device_toHost_empty_i => '0',
                Versal_network_device_toHost_prog_empty_i => '0',
                Versal_network_device_tohost_dout_i => (others => '0'),
                Versal_network_device_fromHost_din_o => open,
                Versal_network_device_fromHost_wr_en_o => open,
                Versal_network_device_toHost_rd_en_o => open,
                Versal_network_device_toHost_eop_i => '0',
                Versal_network_device_fromHost_eop_o => open,
                Versal_network_device_fromHost_set_carrier_o => open,
                Versal_network_device_toHost_status_carrier_i => '0',
                CPMToWupper => CPMToWupper(pcie_endpoint),
                WupperToCPM => WupperToCPM(pcie_endpoint)
            );


    end generate;

    hk0: entity work.housekeeping_module
        generic map(
            CARD_TYPE => CARD_TYPE,
            GBT_NUM => GBT_NUM/ENDPOINTS,
            ENDPOINTS => ENDPOINTS,
            generateTTCemu => false,
            AUTOMATIC_CLOCK_SWITCH => AUTOMATIC_CLOCK_SWITCH,
            FIRMWARE_MODE => FIRMWARE_MODE,
            USE_Si5324_RefCLK => USE_Si5324_RefCLK,
            GENERATE_XOFF => GENERATE_XOFF,
            IncludeDecodingEpath2_HDLC => (others => '0'),
            IncludeDecodingEpath2_8b10b => (others => '0'),
            IncludeDecodingEpath4_8b10b => (others => '0'),
            IncludeDecodingEpath8_8b10b => (others => '0'),
            IncludeDecodingEpath16_8b10b => (others => '0'),
            IncludeDecodingEpath32_8b10b => (others => '0'),
            IncludeDirectDecoding => (others => '0'),
            IncludeEncodingEpath2_HDLC      => (others => '0'),
            IncludeEncodingEpath2_8b10b     => (others => '0'),
            IncludeEncodingEpath4_8b10b     => (others => '0'),
            IncludeEncodingEpath8_8b10b     => (others => '0'),
            IncludeDirectEncoding => (others => '0'),
            BLOCKSIZE => BLOCKSIZE,
            DATA_WIDTH => DATA_WIDTH,
            FULL_HALFRATE => false,
            SUPPORT_HDLC_DELAY => false,
            TTC_SYS_SEL => TTC_SYS_SEL
        )
        port map(
            MMCM_Locked_in => MMCM_Locked_out,
            MMCM_OscSelect_in => MMCM_OscSelect_out,
            SCL => SCL,
            SDA => SDA,
            SI5345_A => SI5345_A,
            SI5345_INSEL => SI5345_INSEL,
            SI5345_OE => SI5345_OE,
            SI5345_RSTN => SI5345_RSTN,
            SI5345_SEL => SI5345_SEL,
            SI5345_nLOL => SI5345_nLOL,
            SI5345_FINC_B => SI5345_FINC_B,
            SI5345_FDEC_B => SI5345_FDEC_B,
            SI5345_INTR_B => SI5345_INTR_B,
            appreg_clk => global_appreg_clk,
            emcclk => emcclk,
            flash_SEL => flash_SEL,
            flash_a => flash_a,
            flash_a_msb => flash_a_msb,
            flash_adv => flash_adv,
            flash_cclk => flash_cclk,
            flash_ce => flash_ce,
            flash_d => flash_d,
            flash_re => flash_re,
            flash_we => flash_we,
            i2cmux_rst => i2cmux_rst,
            TACH => TACH,
            FAN_FAIL_B => FAN_FAIL_B,
            FAN_FULLSP => FAN_FULLSP,
            FAN_OT_B => FAN_OT_B,
            FAN_PWM => FAN_PWM,
            FF3_PRSNT_B => FF3_PRSNT_B,
            IOEXPAN_INTR_B => IOEXPAN_INTR_B,
            IOEXPAN_RST_B => IOEXPAN_RST_B,
            clk10_xtal => clk10_xtal,
            clk40_xtal => clk40_xtal,
            leds => leds,
            opto_inhibit => opto_inhibit,
            --opto_los => OPTO_LOS,
            register_map_control => global_register_map_control_appreg_clk,
            register_map_gen_board_info => register_map_gen_board_info,
            register_map_hk_monitor => register_map_hk_monitor,
            rst_soft => global_reset_soft_appreg_clk,
            sys_reset_n => PCIE_PERSTn,
            PCIE_PWRBRK => PCIE_PWRBRK,
            PCIE_WAKE_B => PCIE_WAKE_B,
            QSPI_RST_B => QSPI_RST_B,
            rst_hw => rst_hw,
            CLK40_FPGA2LMK_P => CLK40_FPGA2LMK_P,
            CLK40_FPGA2LMK_N => CLK40_FPGA2LMK_N,
            LMK_DATA => LMK_DATA,
            LMK_CLK => LMK_CLK,
            LMK_LE => LMK_LE,
            LMK_GOE => LMK_GOE,
            LMK_LD => LMK_LD,
            LMK_SYNCn => LMK_SYNCn,
            I2C_SMB => I2C_SMB,
            I2C_SMBUS_CFG_nEN => I2C_SMBUS_CFG_nEN,
            MGMT_PORT_EN => MGMT_PORT_EN,
            PCIE_PERSTn_out => PCIE_PERSTn_out,
            PEX_PERSTn => PEX_PERSTn,
            PEX_SCL => PEX_SCL,
            PEX_SDA => PEX_SDA,
            PORT_GOOD => PORT_GOOD,
            SHPC_INT => SHPC_INT,
            lnk_up => lnk_up,
            select_bifurcation => select_bifurcation,
            RXUSRCLK_IN => RXUSRCLK,
            Versal_network_device_fromHost_full_o => open,
            Versal_network_device_fromHost_prog_full_o => open,
            Versal_network_device_toHost_empty_o => open,
            Versal_network_device_toHost_prog_empty_o => open,
            Versal_network_device_tohost_dout_o => open,
            Versal_network_device_fromHost_din_i => (others => '0'),
            Versal_network_device_fromHost_wr_en_i => '0',
            Versal_network_device_toHost_rd_en_i => '0',
            Versal_network_device_toHost_eop_o => open,
            Versal_network_device_fromHost_eop_i => '0',
            Versal_network_device_fromHost_set_carrier_i => '0',
            Versal_network_device_toHost_status_carrier_o => open,
            versal_sys_reset_n_out => versal_sys_reset_n,
            WupperToCPM => WupperToCPM,
            CPMToWupper => CPMToWupper,
            clk100_out => open,
            DDR_in => DDR_in,
            DDR_out => DDR_out,
            DDR_inout => DDR_inout,
            axi_miso_ttc_lti => axi_miso_ttc_lti,
            axi_mosi_ttc_lti => axi_mosi_ttc_lti,
            apb3_axi_clk => apb3_axi_clk,
            LTI2cips_gpio => LTI2cips_gpio,
            cips2LTI_gpio => cips2LTI_gpio,
            LPDDR_in => LPDDR_in,
            LPDDR_out => LPDDR_out,
            LPDDR_inout => LPDDR_inout
        );

    --    g_TB_TRIGGER_IBUF: for i in 0 to NUM_TB_TRIGGERS(CARD_TYPE) - 1 generate
    --        ibufds_testbeam : IBUFDS
    --            generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CAPACITANCE, DIFF_TERM, and 4 more"
    --                IOSTANDARD => "LVDS"
    --            )
    --            port map
    --        (
    --                O               =>   tb_trigger_i(i),
    --                I               =>   TB_trigger_P(i),
    --                IB              =>   TB_trigger_N(i)
    --            );
    --    end generate;

    --TTCLTI: if  TTC_SYS_SEL = '1' and (CARD_TYPE = 711 or CARD_TYPE = 712) generate
    --    ltittc0: entity work.ltittc_wrapper
    --        generic map(
    --            CARD_TYPE => CARD_TYPE,
    --            ITK_TRIGTAG_MODE => get_trigtag_mode
    --        )
    --        port map(
    --            reset_in                    => '0',
    --            register_map_control        => global_register_map_40_control,
    --            register_map_ltittc_monitor => register_map_ltittc_monitor,
    --            TTC_out                     => open,
    --            --
    --            clk40_ttc_out               => clk_ttc_40_s,
    --            clk40_in                    => clk40,
    --            clk40_xtal_in               => clk40_xtal,
    --
    --            BUSY                        => open,
    --            cdrlocked_out               => open,
    --            --          TTC_ToHost_Data_out      => TTC_ToHost_Data,
    --            --MT temporary open : need to change CRtohost--
    --            --TTCMSG_ToHost_Data_out      => open,
    --            --USRMSG_ToHost_Data_out      => open,
    --            --
    --            TTC_BUSY_mon_array          => ttc_TTC_BUSY_mon_array(23 downto 0),
    --            BUSY_IN                     => '0',
    --            --SI5345 OUT5 connected to bank 232 TX REFCLK
    --            GTREFCLK0_P_IN         => GTREFCLK_P_IN(2),
    --            GTREFCLK0_N_IN         => GTREFCLK_N_IN(2),
    --            --LMK OUT6 connected to bank 231 to RX REFCLK
    --            GTREFCLK1_P_IN         => LMK_P(6),
    --            GTREFCLK1_N_IN         => LMK_N(6),
    --            --bank 232 from .xdc
    --            RX_P_LTITTC                    => RX_P_LTITTC(0),
    --            RX_N_LTITTC                    => RX_N_LTITTC(0)
    --
    --        );
    --end generate;

    --TTC: if  TTC_SYS_SEL = '0' generate
    --    ttc0: entity work.ttc_wrapper
    --        generic map (
    --            CARD_TYPE                => CARD_TYPE,
    --            FIRMWARE_MODE            => FIRMWARE_MODE,
    --            ISTESTBEAM               => false,
    --            ITK_TRIGTAG_MODE         => get_trigtag_mode
    --        )
    --        port map(
    --            CLK_TTC_P                => CLK_TTC_P,
    --            CLK_TTC_N                => CLK_TTC_N,
    --            DATA_TTC_P               => DATA_TTC_P,
    --            DATA_TTC_N               => DATA_TTC_N,
    --            TB_trigger               => tb_trigger_i,
    --
    --            LOL_ADN => LOL_ADN,
    --            LOS_ADN => LOS_ADN,
    --            --RESET_N                  => sys_reset_n,
    --            register_map_control => global_register_map_40_control,
    --            register_map_ttc_monitor => register_map_ttc_monitor,
    --            TTC_out                  => open,
    --            clk_adn_160              => clk_adn_160,
    --            clk_ttc_40               => clk_ttc_40_s,
    --            clk40                    => clk40,
    --            BUSY                     => open,
    --            cdrlocked_out            => open,
    --            TTC_ToHost_Data_out      => open,
    --            TTC_BUSY_mon_array => ttc_TTC_BUSY_mon_array(23 downto 0),
    --            BUSY_IN => '0');
    --end generate;

    --    busy0: entity work.ttc_busy
    --        generic map(
    --            GBT_NUM => GBT_NUM)
    --        port map(
    --            mclk                 => clk40,
    --            mrst => global_rst_soft_40,
    --            register_map_control => global_register_map_40_control,
    --            BUSY_REQUESTs => (others => (others => '0')),
    --            ANY_BUSY_REQ_OUT => open,
    --            TTC_BUSY_mon_array => TTC_BUSY_mon_array,
    --            DMA_BUSY_in => '0',
    --            FIFO_BUSY_in => '0',
    --            BUSY_INTERRUPT_out => open);

    BUSY_OUT <= (others => '0');

    si5324_resetn <= (others => (not rst_hw));

    register_map_generators.FMEMU_EVENT_INFO.L1ID <= FMEMU_EVENT_INFO_L1ID(0);
    register_map_generators.FMEMU_EVENT_INFO.BCID <= FMEMU_EVENT_INFO_BCID(0);



    FSM_COMP: for i in 0 to GBT_NUM-1 generate
        signal XoffElink : std_logic_vector(1 downto 0);
        signal XoffIndex : integer range 0 to 118;
        signal TTCin : std_logic_vector(2 downto 0); --TTC option 1, 2 or 3 for L1A, BCR and ECR
        signal TTCIndex  : integer range 0 to 118;

        signal L1A, BCR, ECR, Xoff: std_logic;
        signal reset_clk40_lti_gbt: std_logic;
        signal reset_txusrclk: std_logic;
    begin
        process(clk40_lti_gbt(i))
        begin
            if rising_edge(clk40_lti_gbt(i)) then
                XoffIndex <= to_integer(unsigned(global_register_map_control_appreg_clk.FMEMU_CONTROL.XONXOFF_BITNR(55 downto 49)&'0'));
                TTCIndex <= to_integer(unsigned(global_register_map_control_appreg_clk.FMEMU_CONTROL.L1A_BITNR(63 downto 58)&'0'&'0'));
            end if;

        end process;

        reset_clk40_sync: xpm_cdc_async_rst generic map(
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                RST_ACTIVE_HIGH => 1
            )
            port map(
                src_arst => reset,
                dest_clk => clk40_lti_gbt(i),
                dest_arst => reset_txusrclk
            );

        reset_txusrclk_sync: xpm_cdc_async_rst generic map(
                DEST_SYNC_FF => 6,
                INIT_SYNC_FF => 0,
                RST_ACTIVE_HIGH => 1
            )
            port map(
                src_arst => reset,
                dest_clk => TXUSRCLK_240(i),
                dest_arst => reset_clk40_lti_gbt
            );



        XoffElink <= RX_120b(i)(1+XoffIndex downto XoffIndex);
        TTCin <= RX_120b(i)(2+TTCIndex downto TTCIndex);

        Xoff_decoder_top_inst: entity work.Xoff_decoder_top
            generic map (
                DinWidth => 2 --maximum width of the elink
            )
            Port map(
                DIN     => XoffElink,--RX_120b(i)(33 downto 32),
                Clk     => clk40_lti_gbt(i),
                Reset    => reset_clk40_lti_gbt,
                DATA_OUT   => open, --DATA_OUT_DEC,
                DATA_RDY   => open, --DATA_RDY_DEC,
                Xoff_nXon    => Xoff_nXon(i)
            );

        ttc_or_lti_sel: process(clk40_lti_gbt(i))
        begin
            if rising_edge(clk40_lti_gbt(i)) then
                if LTI_RECEIVER_ACTIVE(i) = '1' then
                    Xoff <= TTC_lti_data(i).XOFF;
                    L1A <= TTC_lti_data(i).L0A;
                    BCR <= TTC_lti_data(i).TS;
                    ECR <= TTC_lti_data(i).SL0ID;
                else
                    Xoff <= Xoff_nXon(i);
                    L1A <= TTCin(0);
                    BCR <= TTCin(1);
                    ECR <= TTCin(2);
                end if;

            end if;
        end process;




        u7: entity work.FMEmu_FSM
            port map(
                CLK240               => TXUSRCLK_240(i),                       --  : in     std_logic;
                REC_CLK40            => clk40_lti_gbt(i),
                register_map_control => global_register_map_control_appreg_clk,--  : in     register_map_control_type;
                FMEMU_EVENT_INFO_L1ID => FMEMU_EVENT_INFO_L1ID(i), --pcie0_register_map_monitor,
                FMEMU_EVENT_INFO_BCID => FMEMU_EVENT_INFO_BCID(i), --pcie0_register_map_monitor,
                Xoff_nXon(0)         => Xoff, --"0",                          --  : in     std_logic_vector(0 downto 0);
                --Xoff_nXon_en      => pcie0_register_map_40_control.XOFF_ENABLE(i downto i),
                L1A_Incr             => L1A,--RX_120b(i)(64),               --  : in     std_logic; -- signal coming from ttc decoders
                BCR_in               => BCR,--'0', --RX_120b(0)(65),               --  : in     std_logic; -- signal coming from ttc decoders
                ECR_in               => ECR, --RX_120b(0)(66),               --  : in     std_logic; -- signal coming from ttc decoders
                RST40                => reset_clk40_lti_gbt,                        --  : in     std_logic;
                RST240               => reset_txusrclk,
                CRC                  => CRC(i),                          --  : in     std_logic_vector(31 downto 0);
                RST_RAM              => RST_RAM(i),
                CRC_EN               => open, --CRC_EN(i),                       --  : out    std_logic;
                DataToCRC            => DataToCRC(i),                    --  : out    std_logic_vector(31 downto 0);
                CALC_CRC             => Calc(i),                         --  : out    std_logic;
                CLR_CRC              => Clr_CRC(i),                      --  : out    std_logic;
                RAM_EN               => EMU32b_EN(i),                    --  : out    std_logic;
                RAM_DOUT             => EMU32b_dout(i),                  --  : in     std_logic_vector(31 downto 0);
                RAM_DVALID           => EMU32b_dvalid(i),                --  : in     std_logic;
                EMUout               => fifo34_din(i),                   --  : out    std_logic_vector(31 downto 0);
                EMUout_dtype         => fifo34_dtype(i),                 --  : out    std_logic_vector(1 downto 0);
                sL1A_CNT_debug       => sL1A_CNT_debug(i),
                BUSY_ON              => BUSY_OUT_fsm(i)
            );

        u5: entity work.FMchannelTXctrl
            port map(
                clk240     => TXUSRCLK_240(i),
                rst        => reset_txusrclk,
                busy       => BUSY_OUT_fsm(i),
                fifo_dout  => fifo34_din(i), --fifo34_dout,
                fifo_dtype => fifo34_dtype(i), --fifo_dtype,
                dout       => tx_din_array(i), --tx_dout,
                kout       => tx_kin_array(i)
            ); --tx_kout);


        u9: entity work.FMemuRAM_vhd
            port map(
                EMU32b_EN                       => EMU32b_EN(i),
                EMU32b_dout                     => EMU32b_dout(i),
                EMU32b_dvalid                   => EMU32b_dvalid(i),
                appreg_clk                      => global_appreg_clk,
                clk240                          => TXUSRCLK_240(i),
                register_map_control_appreg_clk => global_register_map_control_appreg_clk,
                wea                => global_register_map_control_appreg_clk.FMEMU_CONFIG.CHANNEL_SELECT(32+i downto 32+i),
                rst                             => RST_RAM(i)
            );

        u3: entity work.crc32
            port map(
                Clk     => TXUSRCLK_240(i),
                Clr_CRC => Clr_CRC(i),
                Din     => DataToCRC(i),
                CRC     => CRC(i),
                Calc    => Calc(i)
            );
    end generate FSM_COMP;


    u11: entity work.FullModeTransceiver
        generic map(
            NUM_LINKS        => GBT_NUM)
        --      USE_GREFCLK      => RECOVER_CLK_FROM_RX_GBT)
        port map(
            GTREFCLK0_0_N_IN                   => GTREFCLK_N_IN(0),
            GTREFCLK0_0_P_IN                   => GTREFCLK_P_IN(0),
            GTREFCLK0_1_N_IN                   => GTREFCLK_N_IN(1),
            GTREFCLK0_1_P_IN                   => GTREFCLK_P_IN(1),
            gtrxp_in                           => RX_P,
            gtrxn_in                           => RX_N,
            gttxn_out                          => TX_N,
            gttxp_out                          => TX_P,
            TXUSRCLK_OUT                       => TXUSRCLK_240,
            RXUSRCLK_OUT                       => RXUSRCLK,
            txdata_in                          => tx_din_array,
            txcharisk_in                       => tx_kin_array,
            TTCout                             => TTC_lti_data,
            rst_hw                             => reset,
            clk40_out                          => clk40_lti_gbt,
            drpclk_in                          => clk40,
            RX_120b_out                        => RX_120b,
            LTI_RECEIVER_ACTIVE_out            => LTI_RECEIVER_ACTIVE,
            FRAME_LOCKED_O                     => open,
            register_map_control               => global_register_map_control_appreg_clk,
            register_map_link_monitor          => register_map_link_monitor
        );

    reset <= global_rst_soft_40 or rst_hw;


    g_ilas: for i in 0 to GBT_NUM-1 generate
        g_lt4: if i < 4 generate
            ila_rx: ila_gbt_rx_frame
                PORT map(
                    clk => clk40_lti_gbt(i),
                    probe0 => RX_120b(i)(111 downto 32)

                );
        end generate;
    end generate;


end architecture structure ; -- of felix_fullmode_top_bnl711


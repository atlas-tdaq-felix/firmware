--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Julia Narevicius
--!               Israel Grayzman
--!               Fabrizio Alfonsi
--!               Nico Giangiacomi
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  University and INFN Bologna
--! Engineer: Nico Giangiacomi
--!
--! Create Date:    02/02/2020
--! Module Name:    EndeavorEncoderBlock
--! Project Name:   FELIX
--! Project description: Wrapper for EndeavorEncoderBlock
----------------------------------------------------------------------------------
--! Use standard library
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.STD_LOGIC_ARITH.all; -- @suppress "Deprecated package"
    use ieee.STD_LOGIC_UNSIGNED.all; -- @suppress "Deprecated package"

    use work.axi_stream_package.all;
    use work.endeavour_package.all;
    use work.pcie_package.all;
    use work.axi_stream_package.all;
    use work.all;

--! a wrap for Endeavor Encoding
entity EndeavorEncoderBlock is
    port (
        reset : in std_logic; --Active high reset
        clk40 : in std_logic; --BC clock for DataIn

        DataIn : in std_logic_vector(7 downto 0); --8b Data from AxiStreamtoByte
        DataInValid : in std_logic; -- Data validated by AxiStreamtoByte
        EOP_in : in std_logic; --End of Packet from AxiStreamtoByte


        readyOut : out std_logic; --m_axis_tready toward AxiStreamToByte
        DataOut : out std_logic_vector(9 downto 0) --Towards GearBox
    );
end EndeavorEncoderBlock;

architecture Behavioral of EndeavorEncoderBlock is

    type t_state is (s_idle, s_next_bit, s_send_bit, s_bit_gap, s_word_gap);

    signal state : t_state := s_idle;

    signal amac_out : std_logic;

    signal bits_to_send: std_logic_vector(7 downto 0) := (others => '0');
    signal bits_left : integer range 0 to 8 := 0;
    signal ticks_left : integer range 0 to 127 := 0;
    signal end_of_chunk : boolean := false;

    signal rst: std_logic;
    signal readyOut1: std_logic;

begin

    rst <= reset;

    readyOut <= readyOut1;

    DataOut <= (others => amac_out);

    --amac_signal <= amac_signal_s;
    --amac_signal_s <= not amac_out when invert_polarity = '1' else amac_out;

    state_update : process(clk40) is
    begin
        if rising_edge(clk40) then
            if rst = '1' then
                state <= s_idle;
            else
                case state is
                    when s_idle =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            state <= s_next_bit;
                        else
                            state <= s_idle;
                        end if;

                    when s_next_bit =>
                        state <= s_send_bit;

                    when s_send_bit =>
                        if ticks_left = 0 then
                            if bits_left = 0 then
                                if end_of_chunk then
                                    state <= s_word_gap;
                                else
                                    state <= s_bit_gap;
                                end if;
                            else
                                state <= s_bit_gap;
                            end if;
                        end if;

                    when s_bit_gap =>
                        if ticks_left = 0 then
                            if bits_left = 0 then
                                state <= s_idle;
                            else
                                state <= s_next_bit;
                            end if;
                        else
                            state <= s_bit_gap;
                        end if;

                    when s_word_gap =>
                        if ticks_left = 0 then
                            state <= s_idle;
                        else
                            state <= s_word_gap;
                        end if;

                    when others =>
                        -- pragma synthesis off
                        report "Invalid state of AMAC signal encoder (state update)" severity failure;
                -- pragma synthesis on
                end case;
            end if;
        end if;
    end process;


    variable_update : process(clk40) is
    begin
        if rising_edge(clk40) then
            if rst = '1' then
                amac_out <= '0';
                readyOut1 <= '0';
                ticks_left <= 0;
                bits_left <= 0;
                end_of_chunk <= false;
                bits_to_send <= (others => '0');
            else
                case state is
                    when s_idle =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            --end_of_chunk <= m_axis.tlast = '1';
                            end_of_chunk <= EOP_in = '1';
                            bits_to_send <= DataIn;
                            readyOut1 <= '0';
                            bits_left <= 8;
                        else
                            readyOut1 <= '1';
                            amac_out <= '0';
                            ticks_left <= 0;
                            bits_left <= 0;
                            end_of_chunk <= false;
                        end if;

                    when s_next_bit =>
                        amac_out <= '1';
                        bits_left <= bits_left - 1;
                        bits_to_send <= bits_to_send(bits_to_send'high-1 downto bits_to_send'low) & '0';
                        if bits_to_send(bits_to_send'left) = '1' then
                            ticks_left <= TICKS_DAH_MID;
                        else
                            ticks_left <= TICKS_DIT_MID;
                        end if;

                    when s_send_bit =>
                        if ticks_left = 0 then
                            if bits_left = 0 then
                                if end_of_chunk then
                                    ticks_left <= TICKS_QUIESCENT;
                                else
                                    ticks_left <= TICKS_BITGAP_MID;
                                end if;
                            else
                                ticks_left <= TICKS_BITGAP_MID;
                            end if;
                            amac_out <= '0';
                        else
                            amac_out <= '1';
                            ticks_left <= ticks_left - 1;
                        end if;

                    when s_bit_gap | s_word_gap =>
                        amac_out <= '0';
                        if ticks_left = 0 then
                            null;
                        else
                            ticks_left <= ticks_left - 1;
                        end if;

                    when others =>
                        -- pragma synthesis off
                        report "Invalid state of AMAC signal encoder (variable update)" severity failure;
                -- pragma synthesis on
                end case;
            end if;
        end if;
    end process;

end Behavioral;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Julia Narevicius
--!               Israel Grayzman
--!               Fabrizio Alfonsi
--!               Nico Giangiacomi
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  University and INFN Bologna
--! Engineer: Nico Giangiacomi
--!
--! Create Date:    02/02/2020
--! Module Name:    EndeavorEncoderBlock
--! Project Name:   FELIX
--! Project description: Wrapper for EndeavorEncoderBlock
----------------------------------------------------------------------------------
--! Use standard library
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.STD_LOGIC_ARITH.all; -- @suppress "Deprecated package"
    use ieee.STD_LOGIC_UNSIGNED.all; -- @suppress "Deprecated package"

    use work.axi_stream_package.all;
    use work.endeavour_package.all;
    use work.pcie_package.all;
    use work.axi_stream_package.all;
    use work.all;

--! a wrap for Endeavor Encoding
entity EndeavorFrameMSG is
    port (
        reset : in std_logic; --Active high reset
        clk40 : in std_logic; --BC clock for DataIn

        DataIn : in std_logic_vector(7 downto 0); --8b Data from AxiStreamtoByte
        DataInValid : in std_logic; -- Data validated by AxiStreamtoByte
        EOP_in : in std_logic; --End of Packet from AxiStreamtoByte

        readyIn    : in std_logic; --message Parser not ready

        readyOut : out std_logic; --m_axis_tready toward AxiStreamToByte

        Message_ID_1_out : out std_logic_vector(7 downto 0);
        Message_ID_2_out : out std_logic_vector(7 downto 0);
        Message_frame_action_out : out std_logic_vector(7 downto 0);
        Message_frame_payload_length_out : out std_logic_vector(7 downto 0);
        Message_frame_payload_1_out : out std_logic_vector(7 downto 0);
        Message_frame_payload_2_out : out std_logic_vector(7 downto 0);
        Message_frame_payload_3_out : out std_logic_vector(7 downto 0);
        Message_frame_payload_4_out : out std_logic_vector(7 downto 0);
        Message_frame_payload_5_out : out std_logic_vector(7 downto 0);
        Message_frame_payload_6_out : out std_logic_vector(7 downto 0);
        Message_frame_payload_7_out : out std_logic_vector(7 downto 0);
        Message_frame_payload_8_out : out std_logic_vector(7 downto 0);

        Downstream_Data_Message_Valid_out : out std_logic;
        Downstream_Data_Message_Ready_out : out std_logic;
        Downstream_EOP_IDLE_out : out std_logic;

        DataOut : out std_logic_vector(9 downto 0) --Towards GearBox
    );
end EndeavorFrameMSG;

architecture Behavioral of EndeavorFrameMSG is


    type t_state is (s_idle, s_get_frame_msg_id_1, s_get_frame_msg_id_2, s_get_frame_action, s_get_frame_payload_length, s_get_frame_pilot, s_empty_fifo, s_get_frame_payload_1, s_get_frame_payload_2, s_get_frame_payload_3, s_get_frame_payload_4, s_get_frame_payload_5, s_get_frame_payload_6, s_get_frame_payload_7, s_get_frame_payload_8, s_frame_send, s_check_frame_pilot);

    signal state : t_state := s_idle;
    signal amac_out : std_logic;
    signal rst: std_logic;
    signal readyOut1: std_logic;
    signal Message_ID_1: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_ID_2: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_action: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_length: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_1: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_2: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_3: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_4: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_5: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_6: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_7: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_8: std_logic_vector(7 downto 0) := (others => '0');

    signal Downstream_Data_Message_Valid: std_logic := '0';
    signal Downstream_Data_Message_Ready: std_logic := '0';
    constant Downstream_EOP_IDLE: std_logic :=  '0';

    signal Message_frame_pilot: std_logic_vector(7 downto 0) := (others => '0');

begin

    Message_ID_1_out <= Message_ID_1;
    Message_ID_2_out <= Message_ID_2;
    Message_frame_action_out <= Message_frame_action;
    Message_frame_payload_length_out <= Message_frame_payload_length;
    Message_frame_payload_1_out <= Message_frame_payload_1;
    Message_frame_payload_2_out <= Message_frame_payload_2;
    Message_frame_payload_3_out <= Message_frame_payload_3;
    Message_frame_payload_4_out <= Message_frame_payload_4;
    Message_frame_payload_5_out <= Message_frame_payload_5;
    Message_frame_payload_6_out <= Message_frame_payload_6;
    Message_frame_payload_7_out <= Message_frame_payload_7;
    Message_frame_payload_8_out <= Message_frame_payload_8;

    Downstream_Data_Message_Valid_out <= Downstream_Data_Message_Valid;
    Downstream_Data_Message_Ready_out <= Downstream_Data_Message_Ready;
    Downstream_EOP_IDLE_out <= Downstream_EOP_IDLE;

    rst <= reset;

    readyOut <= readyOut1;

    DataOut <= (others => amac_out);

    --amac_signal <= amac_signal_s;
    --amac_signal_s <= not amac_out when invert_polarity = '1' else amac_out;

    state_update : process(clk40) is
    begin
        if rising_edge(clk40) then
            if rst = '1' then
                state <= s_idle;
            else
                case state is
                    when s_idle =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            state <= s_get_frame_msg_id_2;
                        else
                            state <= s_idle;
                        end if;

                    when s_get_frame_msg_id_1 =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            state <= s_get_frame_msg_id_2;
                        else
                            state <= s_get_frame_msg_id_1;
                        end if;

                    when s_get_frame_msg_id_2 =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            state <= s_get_frame_action;
                        else
                            state <= s_get_frame_msg_id_2;
                        end if;

                    when s_get_frame_action =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            state <= s_get_frame_payload_length;
                        else
                            state <= s_get_frame_action;
                        end if;

                    when s_get_frame_payload_length =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            state <= s_get_frame_payload_1;
                        else
                            state <= s_get_frame_payload_length;
                        end if;


                    when s_get_frame_payload_1 =>
                        if DataInValid = '1' and readyOut1 = '1' and Message_frame_payload_length = x"1" then
                            state <= s_get_frame_pilot;
                        elsif DataInValid = '1' and readyOut1 = '1' then
                            state <= s_get_frame_payload_2;
                        else
                            state <= s_get_frame_payload_1;
                        end if;

                    when s_get_frame_payload_2 =>
                        if DataInValid = '1' and readyOut1 = '1' and Message_frame_payload_length = x"2" then
                            state <= s_get_frame_pilot;
                        elsif DataInValid = '1' and readyOut1 = '1' then
                            state <= s_get_frame_payload_3;
                        else
                            state <= s_get_frame_payload_2;
                        end if;

                    when s_get_frame_payload_3 =>
                        if DataInValid = '1' and readyOut1 = '1' and Message_frame_payload_length = x"3" then
                            state <= s_get_frame_pilot;
                        elsif DataInValid = '1' and readyOut1 = '1' then
                            state <= s_get_frame_payload_4;
                        else
                            state <= s_get_frame_payload_3;
                        end if;

                    when s_get_frame_payload_4 =>
                        if DataInValid = '1' and readyOut1 = '1' and Message_frame_payload_length = x"4" then
                            state <= s_get_frame_pilot;
                        elsif DataInValid = '1' and readyOut1 = '1' then
                            state <= s_get_frame_payload_5;
                        else
                            state <= s_get_frame_payload_4;
                        end if;

                    when s_get_frame_payload_5 =>
                        if DataInValid = '1' and readyOut1 = '1' and Message_frame_payload_length = x"5" then
                            state <= s_get_frame_pilot;
                        elsif DataInValid = '1' and readyOut1 = '1' then
                            state <= s_get_frame_payload_6;
                        else
                            state <= s_get_frame_payload_5;
                        end if;

                    when s_get_frame_payload_6 =>
                        if DataInValid = '1' and readyOut1 = '1' and Message_frame_payload_length = x"6" then
                            state <= s_get_frame_pilot;
                        elsif DataInValid = '1' and readyOut1 = '1' then
                            state <= s_get_frame_payload_7;
                        else
                            state <= s_get_frame_payload_6;
                        end if;

                    when s_get_frame_payload_7 =>
                        if DataInValid = '1' and readyOut1 = '1' and Message_frame_payload_length = x"7" then
                            state <= s_get_frame_pilot;
                        elsif DataInValid = '1' and readyOut1 = '1' then
                            state <= s_get_frame_payload_8;
                        else
                            state <= s_get_frame_payload_7;
                        end if;

                    when s_get_frame_payload_8 =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            state <= s_get_frame_pilot;
                        else
                            state <= s_get_frame_payload_8;
                        end if;

                    when s_get_frame_pilot =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            state <= s_check_frame_pilot;
                        else
                            state <= s_get_frame_pilot;
                        end if;

                    --when s_check_frame_pilot =>
                    --    if Message_frame_pilot = x"a" then
                    --      state <= s_frame_send;
                    --    else
                    --      state <= s_frame_send;
                    --    end if;

                    when s_check_frame_pilot =>
                        if Message_frame_pilot = x"0" then
                            state <= s_frame_send;
                        else
                            state <= s_empty_fifo;
                        end if;

                    when s_empty_fifo =>
                        if EOP_in = '0' then
                            state <= s_empty_fifo;
                        else
                            state <= s_idle;
                        end if;

                    when s_frame_send =>
                        if Downstream_Data_Message_Valid = '1' and readyIn = '1' then
                            state <= s_idle;
                        else
                            state <= s_frame_send;
                        end if;

                    when others =>
                        -- pragma synthesis off
                        report "Invalid state of Frame MSG (state update)" severity failure;
                -- pragma synthesis on
                end case;
            end if;
        end if;
    end process;

    variable_update : process(clk40) is
    begin
        if rising_edge(clk40) then
            if rst = '1' then
                amac_out <= '0';
                readyOut1 <= '0';
                Downstream_Data_Message_Ready <= '0';
                Downstream_Data_Message_Valid <= '0';
                Message_ID_1 <= (others => '0');
                Message_ID_2 <= (others => '0');
                Message_frame_action <= (others => '0');
                Message_frame_payload_1 <= (others => '0');
                Message_frame_payload_2 <= (others => '0');
                Message_frame_payload_3 <= (others => '0');
                Message_frame_payload_4 <= (others => '0');
                Message_frame_payload_5 <= (others => '0');
                Message_frame_payload_6 <= (others => '0');
                Message_frame_payload_7 <= (others => '0');
                Message_frame_payload_8 <= (others => '0');
                Message_frame_payload_length <= (others => '0');
                Message_frame_pilot <= (others => '0');
            else
                case state is
                    when s_idle =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            --end_of_chunk <= m_axis.tlast = '1';
                            Message_ID_1 <= DataIn;
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';

                        else
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';

                        end if;

                    when s_get_frame_msg_id_2 =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            Message_ID_2 <= DataIn;
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';
                        else
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';

                        end if;

                    when s_get_frame_action =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            Message_frame_action <= DataIn;
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';
                        else
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';

                        end if;

                    when s_get_frame_payload_length =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            Message_frame_payload_length <= DataIn;
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';
                        else
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';

                        end if;

                    when s_get_frame_payload_1 =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            Message_frame_payload_1 <= DataIn;
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';
                        else
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';

                        end if;

                    when s_get_frame_payload_2 =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            Message_frame_payload_2 <= DataIn;
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';
                        else
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';

                        end if;

                    when s_get_frame_payload_3 =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            Message_frame_payload_3 <= DataIn;
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';
                        else
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';

                        end if;

                    when s_get_frame_payload_4 =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            Message_frame_payload_4 <= DataIn;
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';
                        else
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';

                        end if;

                    when s_get_frame_payload_5 =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            Message_frame_payload_5 <= DataIn;
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';
                        else
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';

                        end if;

                    when s_get_frame_payload_6 =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            Message_frame_payload_6 <= DataIn;
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';
                        else
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';

                        end if;

                    when s_get_frame_payload_7 =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            Message_frame_payload_7 <= DataIn;
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';
                        else
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';

                        end if;

                    when s_get_frame_payload_8 =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            Message_frame_payload_8 <= DataIn;
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';
                        else
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';

                        end if;

                    when s_get_frame_pilot =>
                        if DataInValid = '1' and readyOut1 = '1' then
                            Message_frame_pilot <= DataIn;
                            readyOut1 <= '0';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';
                        else
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';

                        end if;

                    when s_check_frame_pilot =>
                        if Message_frame_pilot = x"0" then
                            readyOut1 <= '0';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';
                        else
                            readyOut1 <= '0';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';

                        end if;

                    when s_empty_fifo =>
                        if EOP_in = '0' then
                            Message_ID_2 <= DataIn;
                            readyOut1 <= '1';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';
                        else
                            readyOut1 <= '0';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';

                        end if;

                    when s_frame_send =>
                        if Downstream_Data_Message_Valid = '1' and readyIn = '1' then
                            Message_ID_2 <= DataIn;
                            readyOut1 <= '0';
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';

                            Message_ID_1 <= (others => '0');
                            Message_ID_2 <= (others => '0');
                            Message_frame_action <= (others => '0');
                            Message_frame_payload_length <= (others => '0');
                            Message_frame_payload_1 <= (others => '0');
                            Message_frame_payload_2 <= (others => '0');
                            Message_frame_payload_3 <= (others => '0');
                            Message_frame_payload_4 <= (others => '0');
                            Message_frame_payload_5 <= (others => '0');
                            Message_frame_payload_6 <= (others => '0');
                            Message_frame_payload_7 <= (others => '0');
                            Message_frame_payload_8 <= (others => '0');

                        else
                            Downstream_Data_Message_Valid <= '1';
                            Downstream_Data_Message_Ready <= '1';
                            readyOut1 <= '0';
                        end if;

                    when others =>
                        -- pragma synthesis off
                        report "Invalid state of AMAC signal encoder (variable update)" severity failure;
                -- pragma synthesis on
                end case;
            end if;
        end if;
    end process;

end Behavioral;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Jacopo Pinzino
--!               jacopo pinzino
--!               Nico Giangiacomi
--!               Frans Schreuder
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer: jacopo pinzino
--            Elena Zhivun <ezhivun@bnl.gov>
--
-- Create Date: 10/30/2019 03:36:26 PM
-- Design Name:
-- Module Name: EndeavourDecoder - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;
    use ieee.numeric_std.all;
    use IEEE.numeric_std_unsigned.ALL;

    use work.endeavour_package.all;
    use work.pcie_package.all;

entity EndeavourDecoder is
    generic (
        DEBUG_en : boolean := false;
        VERSAL : boolean
    );
    port (
        clk40 : in std_logic;  --(lp)GBT BC clock
        m_axis_aclk : in std_logic; --CRToHost Clock
        amac_signal : in std_logic; --Connect to Deglitcher
        LinkAligned : in std_logic; --(lp)GBT link aligned -- @suppress "Unused port: LinkAligned is not used in work.EndeavourDecoder(Behavioral)"
        daq_fifo_flush : in std_logic; --Active high fifo reset in clk40 domain
        amac_rst : in std_logic;
        m_axis : out axis_32_type; --Output to CRToHost
        invert_polarity : in std_logic; -- invert link polatiry
        m_axis_tready : in std_logic; --from CRToHost
        m_axis_prog_empty : out std_logic
    );
end EndeavourDecoder;

architecture Behavioral of EndeavourDecoder is

    type t_decoder_state is (s_idle, s_wait_bit, s_latch_bit, s_wait_gap,
        s_send_axi, s_wait_axi);

    signal state : t_decoder_state := s_idle;
    signal return_state : t_decoder_state := s_idle;

    type t_axi_state is (s_idle, s_send);
    signal axi_state : t_axi_state := s_idle;

    signal s_axis : axis_32_type;
    signal s_axis_tready : std_logic;
    signal amac_signal_s : std_logic;
    signal amac_signal_del: std_logic;
    signal amac_signal_rising: std_logic;
    signal amac_signal_falling: std_logic;

    signal amac_data  : std_logic_vector(31 downto 0) := (others => '0');
    signal decoding_error : boolean := false;
    signal axi_data     : std_logic_vector(31 downto 0) := (others => '0');
    signal axi_bits     : integer range 0 to 32;
    signal axi_error    : boolean := false;
    signal axi_valid   : boolean := false;
    signal axi_ready   : boolean := false;
    signal axi_tlast   : std_logic := '0';

    signal bit_length : integer range 0 to 255 := 0;
    signal bit_counter : integer range 0 to 32 := 0;


    signal rst_probe : std_logic_vector(0 downto 0); -- @suppress "signal rst_probe is never read"
    signal amac_signal_probe : std_logic_vector(0 downto 0); -- @suppress "signal amac_signal_probe is never read"
    signal s_axis_tvalid_probe : std_logic_vector(0 downto 0); -- @suppress "signal s_axis_tvalid_probe is never read"
    signal s_axis_tready_probe : std_logic_vector(0 downto 0); -- @suppress "signal s_axis_tready_probe is never read"
    signal aresetn : std_logic;



--component ila_endeavour_decoder
--  PORT(
--    clk    : IN std_logic;
--    probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    probe2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--    probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
--  );
--END component ila_endeavour_decoder;

begin

    ila_probes_gen : if DEBUG_en generate
        rst_probe(0) <= amac_rst;
        amac_signal_probe(0) <= amac_signal_s;
        s_axis_tvalid_probe(0) <= s_axis.tvalid;
        s_axis_tready_probe(0) <= s_axis_tready;
    --ila_probe: ila_endeavour_decoder
    --port map (
    --  clk => clk40,
    --  probe0 => rst_probe,
    --  probe1 => amac_signal_probe,
    --  probe2 => s_axis.tdata,
    --  probe3 => s_axis_tvalid_probe,
    --  probe4 => s_axis_tready_probe
    --);
    end generate ila_probes_gen;

    amac_signal_s <= not amac_signal when invert_polarity = '1' else amac_signal;
    aresetn <= not daq_fifo_flush;

    fifoaxi32: entity work.Axis32Fifo
        generic map(
            DEPTH => 512,
            --CLOCKING_MODE => "independent_clock",
            --RELATED_CLOCKS => 1,
            --FIFO_MEMORY_TYPE => "auto",
            --PACKET_FIFO => "false",
            USE_BUILT_IN_FIFO => '1',
            VERSAL => VERSAL,
            BLOCKSIZE => 1024,
            ISPIXEL => false
        )
        port map(
            s_axis_aresetn => aresetn,
            s_axis_aclk => clk40,
            s_axis => s_axis,
            s_axis_tready => s_axis_tready,
            m_axis_aclk => m_axis_aclk,
            m_axis => m_axis,
            m_axis_tready => m_axis_tready,
            m_axis_prog_empty => m_axis_prog_empty
        );


    amac_edge_finder : process(clk40)
    begin
        if rising_edge(clk40) then
            amac_signal_del <= amac_signal_s;
            amac_signal_rising  <= amac_signal_s and not(amac_signal_del);
            amac_signal_falling  <= amac_signal_del and not(amac_signal_s);
        end if;
    end process;


    ----------------------------------------------------------------------------------
    -- State machine for decoding AMAC bit patterns
    ----------------------------------------------------------------------------------

    endeavour_decoder_state_update : process(clk40)
    begin
        if rising_edge(clk40) then

            if amac_rst = '1' then
                state        <= s_idle;
                return_state <= s_idle;

            else

                case state is

                    -- wait until a rising signal from amac
                    when s_idle =>
                        if (amac_signal_rising = '1') then
                            state <= s_wait_bit;
                        else
                            state <= s_idle;
                        end if;

                    -- measure the length of the signal, waiting for the signal going down or if it exceeds a threshold
                    when s_wait_bit =>
                        if (amac_signal_falling = '1') then
                            state <= s_latch_bit;
                        elsif (bit_length > TICKS_DAH_MAX) then
                            state <= s_send_axi; ----- the line is up too long, I rise the chunk error
                            return_state <= s_idle;
                        else
                            state  <= s_wait_bit;
                        end if;

                    ------------decoding of the signal
                    when s_latch_bit =>
                        state <= s_wait_gap;

                    ----------Waiting for the end of the gap
                    when s_wait_gap =>
                        if (amac_signal_rising = '1') then
                            if bit_counter = 32 then
                                state <= s_send_axi;
                                return_state  <= s_wait_bit;
                            else
                                state  <= s_wait_bit;
                            end if;
                        else
                            if (bit_length > TICKS_BITGAP_MAX) then
                                state <= s_send_axi; --- the gap last long so the message is finished
                                return_state <= s_idle;
                            else
                                state  <= s_wait_gap;
                            end if;
                        end if;

                    when s_send_axi =>
                        if axi_ready then
                            state  <= return_state;
                        else
                            state <= s_wait_axi;
                        end if;

                    when s_wait_axi =>  -- wait for the AXI sending FSM to become ready
                        if axi_ready then
                            state <= s_wait_bit;
                        else
                            state <= return_state;
                        end if;

                    when others  =>
                        -- pragma synthesis off
                        report "Invalid state of AMAC signal decoder (state update)" severity failure;
                -- pragma synthesis on
                end case;
            end if;
        end if;

    end process;


    endeavour_decoder_variable_update : process(clk40) is
        variable new_bit : std_logic;
    --variable new_amac_data : std_logic_vector(amac_data'range);

    begin
        if rising_edge(clk40) then

            if amac_rst = '1' then
                amac_data   <= (others => '0');
                axi_data      <= (others => '0');
                axi_tlast     <= '1';
                axi_bits      <= 0;
                axi_error     <= false;
                axi_valid     <= false;
                bit_length    <= 0;
                bit_counter   <= 0;
                decoding_error <= false;

            else

                case state is

                    when s_idle =>
                        amac_data   <= (others => '0');
                        axi_valid     <= false;
                        bit_length    <= 0; -- measured bit length in 40MHz ticks
                        bit_counter   <= 0; -- how many bits have been received
                        axi_tlast     <= '1';
                        decoding_error <= false;

                    -- measure the length of the signal, waiting for the signal going down or if it exceeds a threshold
                    when s_wait_bit =>
                        axi_valid <= false;
                        bit_length <= bit_length + 1;
                        if (amac_signal_falling = '1') then
                            null;
                        elsif (bit_length > TICKS_DAH_MAX) then
                            -- the line is up too long, stop receiving, rise the chunk error
                            axi_bits <= bit_counter;
                            axi_data <= amac_data;
                            axi_tlast <= '1';
                            axi_error <= true;
                        end if;

                    ------------ decoding of the AMAC signal
                    when s_latch_bit =>
                        axi_valid <= false;
                        if bit_length <= TICKS_DAH_MAX and bit_length >= TICKS_DAH_MIN then
                            -- within expected duration for the logic 1
                            new_bit := '1';
                        elsif bit_length <= TICKS_DIT_MAX and bit_length >= TICKS_DIT_MIN then
                            -- within expected duration for the logic 0
                            new_bit := '0';
                        elsif bit_length < TICKS_DIT_MIN then
                            -- shorter than expected duration for the logic 0
                            new_bit := '0';
                            decoding_error <= true;
                        else
                            -- longer than expected duration for the logic 0 but shorter than 1
                            new_bit := '1';
                            decoding_error <= true;
                        end if;

                        amac_data <= amac_data(amac_data'high-1 downto 0) & new_bit;
                        bit_counter <= bit_counter + 1;
                        bit_length <= 0;

                    ----------Waiting for the end of the gap
                    when s_wait_gap =>
                        axi_valid <= false;
                        bit_length <= bit_length + 1;

                        if (amac_signal_rising = '1') then
                            bit_length <= 0;
                            if (bit_length < TICKS_BITGAP_MIN) then
                                -- the gap between bits is too short
                                decoding_error <= true;
                            end if;

                            if bit_counter = 32 then -- received a complete 32 bit word
                                axi_bits <= 32; -- TODO: make this a constant or generic
                                axi_data <= amac_data;
                                axi_error <= decoding_error or (bit_length < TICKS_BITGAP_MIN);
                                axi_tlast <= '0';
                                bit_counter <= 0;
                                amac_data <= (others => '0');
                            end if;
                        else
                            if (bit_length > TICKS_BITGAP_MAX) then
                                -- the gap last long so the message is finished
                                axi_bits <= bit_counter;
                                axi_data <= amac_data;
                                axi_error <= decoding_error;
                                axi_tlast <= '1';
                                bit_counter <= 0;
                                amac_data <= (others => '0');
                            end if;
                        end if;

                    when s_send_axi =>
                        axi_valid <= true;

                    when s_wait_axi =>  -- wait for the AXI sending FSM to become ready
                        -- hold axi_valid = true until the receiver is ready
                        if axi_ready then
                            axi_valid <= false;
                        else
                            axi_valid <= true;
                        end if;

                    when others  =>
                        -- pragma synthesis off
                        report "Invalid state of AMAC signal decoder (variable update)" severity failure;
                -- pragma synthesis on
                end case;
            end if;
        end if;
    end process;

    ----------------------------------------------------------------------------------
    -- State machine for sending the data to the AXI stream CDC buffer
    ----------------------------------------------------------------------------------

    axi_sender_state_update : process(clk40) is
    begin
        if rising_edge(clk40) then
            if amac_rst = '1' then
                axi_state <= s_idle;
            else
                case axi_state is
                    when s_idle =>
                        if axi_valid then
                            if axi_bits = 0 then
                                axi_state <= s_idle;
                            else
                                axi_state <= s_send;
                            end if;
                        else
                            axi_state <= s_idle;
                        end if;

                    when s_send =>
                        if s_axis.tvalid = '1' and s_axis_tready = '1' then
                            axi_state <= s_idle;
                        else
                            axi_state <= s_send;
                        end if;

                    when others =>
                        -- pragma synthesis off
                        report "Invalid state of AXI sender (state update)" severity failure;
                -- pragma synthesis on
                end case;
            end if;
        end if;
    end process;


    axi_sender_variable_update : process(clk40) is
        procedure s_axi_not_sending is
        begin
            s_axis.tvalid <= '0';
            s_axis.tlast <= '1';
            s_axis.tdata <= (others => '0');
            s_axis.tuser <= (others => '0');
            s_axis.tkeep <= (others => '0');
        end;

    begin
        if rising_edge(clk40) then
            if amac_rst = '1' then
                axi_ready <= false;
                s_axi_not_sending;
            else
                case axi_state is
                    when s_idle =>
                        if axi_valid then
                            -- decoder process requests sending word
                            if axi_bits = 0 then
                                axi_ready <= true;
                                s_axi_not_sending;

                            else
                                axi_ready <= false;
                                s_axis.tlast <= axi_tlast;
                                s_axis.tvalid <= '1';
                                -- load AMAC data into the registers in little endian order
                                if axi_bits < 8 + 1 then
                                    s_axis.tdata <= x"000000" & axi_data(7 downto 0);
                                    s_axis.tkeep <= "0001";
                                elsif axi_bits < 16 + 1 then
                                    s_axis.tdata <= x"0000" & axi_data(7 downto 0) & axi_data(15 downto 8);
                                    s_axis.tkeep <= "0011";
                                elsif axi_bits < 24 + 1 then
                                    s_axis.tdata <= x"00" & axi_data(7 downto 0) & axi_data(15 downto 8) & axi_data(23 downto 16);
                                    s_axis.tkeep <= "0111";
                                else
                                    s_axis.tdata <= axi_data(7 downto 0) & axi_data(15 downto 8) & axi_data(23 downto 16) & axi_data(31 downto 24);
                                    s_axis.tkeep <= "1111";
                                end if;

                                -- set chunk error flag if there was an error
                                if axi_error then
                                    s_axis.tuser <= "0010";
                                else
                                    s_axis.tuser <= "0000";
                                end if;
                            end if;
                        else
                            -- no data to send
                            axi_ready <= true;
                            s_axi_not_sending;
                        end if;

                    when s_send =>
                        if s_axis.tvalid = '1' and s_axis_tready = '1' then
                            s_axis.tvalid <= '0';
                            axi_ready <= true;
                        else
                            s_axis.tvalid <= '1';
                            axi_ready <= false;
                        end if;

                    when others =>
                        -- pragma synthesis off
                        report "Invalid state of AXI sender (variable update)" severity failure;
                -- pragma synthesis on
                end case;
            end if;
        end if;
    end process;

end Behavioral;

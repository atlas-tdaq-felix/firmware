--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Jacopo Pinzino
--!               Elena Zhivun
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer: jacopo pinzino, Elena Zhivun <ezhivun@bnl.gov>
--
-- Create Date: 11/06/2019 11:51:07 AM
-- Design Name:
-- Module Name: EndeavourEncoder - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use ieee.STD_LOGIC_ARITH.all; -- @suppress "Deprecated package"
    use ieee.STD_LOGIC_UNSIGNED.all; -- @suppress "Deprecated package"

    use work.endeavour_package.all;
    use work.pcie_package.all;

entity EndeavourEncoder is
    generic (
        DEBUG_en : boolean := false;
        DISTR_RAM : boolean := false;
        USE_BUILT_IN_FIFO : std_logic := '0'
    );
    port (
        clk40 : in std_logic;
        LinkAligned : in std_logic; -- @suppress "Unused port: LinkAligned is not used in work.EndeavourEncoder(Behavioral)"
        s_axis_aclk : in std_logic;
        reset : in std_logic;
        s_axis : in axis_8_type;
        s_axis_tready : out std_logic;
        invert_polarity : in std_logic; -- invert link polatiry
        amac_signal : out std_logic;
        almost_full : out std_logic
    );
end EndeavourEncoder;

architecture Behavioral of EndeavourEncoder is



    --signal m_axis : axis_8_type;
    --signal m_axis_tready : std_logic;


    signal rst_probe : std_logic_vector(0 downto 0); -- @suppress "signal rst_probe is never read"
    signal amac_signal_s : std_logic;

    --component ila_endeavour_encoder
    --  PORT(
    --    clk    : IN std_logic;
    --    probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --    probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --    probe2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    --    probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --    probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
    --  );
    --END component ila_endeavour_encoder;

    signal AxiStreamToByteDataOut : std_logic_vector(7 downto 0);
    signal AxiStreamToByteDataValidOut : std_logic;
    signal AxiStreamToByteEOPOut : std_logic;
    signal AxiStreamToByteTreadyin : std_logic;





    signal EndeavorEncoderELINKOUT : std_logic_vector(9 downto 0);

    signal Message_frame_action_interconnect: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_length_interconnect: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_1_interconnect: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_2_interconnect: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_3_interconnect: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_4_interconnect: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_5_interconnect: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_6_interconnect: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_7_interconnect: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_8_interconnect: std_logic_vector(7 downto 0) := (others => '0');

    signal Message_frame_Readyin_interconnect: std_logic := '0';

    --signal Message_frame_Data_out_interconnect: std_logic_vector(9 downto 0);

    signal Downstream_Data_Message_Valid_interconnect: std_logic := '0';
    --signal Downstream_Data_Message_Ready_interconnect: std_logic := '0';

    signal Downstream_Data_Message_Paser_Data_Valid_interconnect: std_logic := '0';
    --signal Downstream_Data_Message_Paser_Data_Ready_interconnect: std_logic := '0';
    signal Downstream_Data_Message_Paser_EOP_IDLE_interconnect: std_logic :=  '0';
    signal Message_Paser_Data_out_interconnect: std_logic_vector(7 downto 0);
    signal Message_Paser_Readyin_interconnect: std_logic := '0';



begin

    ila_probes_gen : if DEBUG_en generate
        rst_probe(0) <= reset;
    --amac_signal_probe(0) <= amac_signal_s;
    --m_axis_tvalid_probe(0) <= m_axis.tvalid;
    --m_axis_tready_probe(0) <= m_axis_tready;
    --ila_probe: ila_endeavour_encoder
    --port map (
    --  clk => clk40,
    --  probe0 => rst_probe,
    --  probe1 => amac_signal_probe,
    --  probe2 => m_axis_out.tdata,
    --  probe3 => m_axis_tvalid_probe,
    --  probe4 => m_axis_tready_probe
    --);
    end generate ila_probes_gen;


    fromAxis0: entity work.AxiStreamToByte
        generic map(
            BYTES => 1,
            --BLOCKSIZE => BLOCKSIZE,
            USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
            DISTR_RAM => DISTR_RAM
        )
        port map(
            clk40 => clk40,
            reset => reset,
            EnableIn => '1',
            s_axis => s_axis,
            s_axis_tready => s_axis_tready,
            s_axis_aclk => s_axis_aclk,

            m_axis_tready => AxiStreamToByteTreadyin,
            almost_full => almost_full,

            DataOut => AxiStreamToByteDataOut,
            DataOutValid(0) => AxiStreamToByteDataValidOut,
            EOP(0) => AxiStreamToByteEOPOut
        );




    EndeavorFrameMSG0: entity work.EndeavorFrameMSG
        port map(
            reset => reset,
            clk40 => clk40,
            DataIn => AxiStreamToByteDataOut,
            DataInValid => AxiStreamToByteDataValidOut,
            EOP_in => AxiStreamToByteEOPOut,
            readyIn => Message_frame_Readyin_interconnect,
            readyOut => AxiStreamToByteTreadyin,
            Message_ID_1_out => open,
            Message_ID_2_out => open,
            Message_frame_action_out => Message_frame_action_interconnect,
            Message_frame_payload_length_out => Message_frame_payload_length_interconnect,
            Message_frame_payload_1_out => Message_frame_payload_1_interconnect,
            Message_frame_payload_2_out => Message_frame_payload_2_interconnect,
            Message_frame_payload_3_out => Message_frame_payload_3_interconnect,
            Message_frame_payload_4_out => Message_frame_payload_4_interconnect,
            Message_frame_payload_5_out => Message_frame_payload_5_interconnect,
            Message_frame_payload_6_out => Message_frame_payload_6_interconnect,
            Message_frame_payload_7_out => Message_frame_payload_7_interconnect,
            Message_frame_payload_8_out => Message_frame_payload_8_interconnect,
            Downstream_Data_Message_Valid_out => Downstream_Data_Message_Valid_interconnect,
            Downstream_Data_Message_Ready_out => open,
            Downstream_EOP_IDLE_out => open,
            DataOut => open
        );



    EndeavorFrameParser0: entity work.EndeavorFrameParser
        port map(
            reset => reset,
            clk40 => clk40,
            Message_frame_action_in => Message_frame_action_interconnect,
            Message_frame_payload_length_in => Message_frame_payload_length_interconnect,
            Message_frame_payload_1_in => Message_frame_payload_1_interconnect,
            Message_frame_payload_2_in => Message_frame_payload_2_interconnect,
            Message_frame_payload_3_in => Message_frame_payload_3_interconnect,
            Message_frame_payload_4_in => Message_frame_payload_4_interconnect,
            Message_frame_payload_5_in => Message_frame_payload_5_interconnect,
            Message_frame_payload_6_in => Message_frame_payload_6_interconnect,
            Message_frame_payload_7_in => Message_frame_payload_7_interconnect,
            Message_frame_payload_8_in => Message_frame_payload_8_interconnect,
            DataInValid => Downstream_Data_Message_Valid_interconnect,
            readyIn => Message_Paser_Readyin_interconnect,
            Downstream_Data_Message_Valid_out => Downstream_Data_Message_Paser_Data_Valid_interconnect,
            Downstream_Data_Message_Ready_out => open,
            Downstream_EOP_IDLE_out => Downstream_Data_Message_Paser_EOP_IDLE_interconnect,
            readyOut_to_frame => Message_frame_Readyin_interconnect,
            DataOut => Message_Paser_Data_out_interconnect
        );






    EndeavorEncoderBlock0: entity work.EndeavorEncoderBlock
        port map(
            reset => reset,
            clk40 => clk40,
            DataIn => Message_Paser_Data_out_interconnect,
            DataInValid => Downstream_Data_Message_Paser_Data_Valid_interconnect,
            EOP_in => Downstream_Data_Message_Paser_EOP_IDLE_interconnect,
            readyOut => Message_Paser_Readyin_interconnect,
            DataOut => EndeavorEncoderELINKOUT
        );



    amac_signal <= amac_signal_s;
    amac_signal_s <= not EndeavorEncoderELINKOUT(0) when invert_polarity = '1' else EndeavorEncoderELINKOUT(0);









end Behavioral;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               jacopo pinzino
--!               Frans Schreuder
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer: jacopo pinzino
--
-- Create Date: 16/04/2020 11:51:07 AM
-- Design Name:
-- Module Name: Endeavourdeglitcher - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
-- Removes glitches that are shorter than 100 ns
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;
    use ieee.numeric_std.all;
    use IEEE.numeric_std_unsigned.ALL;
    use work.pcie_package.all;



entity EndeavourDeglitcher is
    port (
        clk40     : in std_logic; --(lp)GBT BC clock
        rst       : in std_logic; -- active high reset in BC clk domain
        datain : in std_logic_vector(1 downto 0); --Connect to ELINK bits
        dataout : out std_logic --Output to Decoder
    );
end EndeavourDeglitcher;

architecture Behavioral of EndeavourDeglitcher is

    --constant BIGGAP : std_logic_vector(7 downto 0) := X"30";
    signal datain_state         : std_logic := '0';
    signal onecount             : std_logic_vector(3 downto 0);

begin

    deglitch : process(clk40)
    ----- the process filters the gliches on the 2 bit line and it transfors the signal from 2 bit to 1 bit mantaing the same clock frequensy and so reducing to half the length signal
    ----- (indeed in the decoder all the clock lenght are half of the one in the encoder). A counter increas when on the line there is a 11 and decrease with a 00; 01 and 10 are considered neutral
    ----- 2 thresholds of 7 and 1 are considered to transmit respectively a 1 and a 0
    begin

        if rising_edge(clk40) then

            if rst = '1' then
                onecount <= "0000";
                datain_state <= '0';
                dataout <= '0';
            else
                if (datain = "11") and (onecount < "0111") then
                    onecount <= onecount + x"2";
                elsif (datain = "00") and (onecount > "0001") then
                    onecount <= onecount - x"2";
                end if;

                if (datain_state = '1') and (onecount < "0001") then
                    datain_state <= '0';
                    dataout <= '0';
                elsif (datain_state = '0') and (onecount > "0111") then
                    datain_state <= '1';
                    dataout <= '1';
                else
                    dataout <= datain_state;
                end if;
            end if;
        end if;
    end process;
end Behavioral;

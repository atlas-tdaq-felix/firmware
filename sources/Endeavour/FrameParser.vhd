--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Julia Narevicius
--!               Israel Grayzman
--!               Fabrizio Alfonsi
--!               Nico Giangiacomi
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  University and INFN Bologna
--! Engineer: Nico Giangiacomi
--!
--! Create Date:    02/02/2020
--! Module Name:    EndeavorFrameParser
--! Project Name:   FELIX
--! Project description: Wrapper for EndeavorFrameParser
----------------------------------------------------------------------------------
--! Use standard library
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;

    use work.axi_stream_package.all;
    use work.endeavour_package.all;
    use work.pcie_package.all;
    use work.axi_stream_package.all;
    use work.all;

--! a wrap for Endeavor Encoding
entity EndeavorFrameParser is
    port (
        reset : in std_logic; --Active high reset
        clk40 : in std_logic; --BC clock for DataIn
        --EnableIn : in std_logic;

        --Message_ID_1_in : in std_logic_vector(7 downto 0);
        --Message_ID_2_in : in std_logic_vector(7 downto 0);
        Message_frame_action_in : in std_logic_vector(7 downto 0);
        Message_frame_payload_length_in : in std_logic_vector(7 downto 0);
        Message_frame_payload_1_in : in std_logic_vector(7 downto 0);
        Message_frame_payload_2_in : in std_logic_vector(7 downto 0);
        Message_frame_payload_3_in : in std_logic_vector(7 downto 0);
        Message_frame_payload_4_in : in std_logic_vector(7 downto 0);
        Message_frame_payload_5_in : in std_logic_vector(7 downto 0);
        Message_frame_payload_6_in : in std_logic_vector(7 downto 0);
        Message_frame_payload_7_in : in std_logic_vector(7 downto 0);
        Message_frame_payload_8_in : in std_logic_vector(7 downto 0);

        DataInValid : in std_logic; -- Data validated Frame MSG
        --EOP_in : in std_logic; --End of Packet from Frame MSG

        --toHostXoff : in std_logic;

        readyIn    : in std_logic; --EndeavorBlock not ready

        Downstream_Data_Message_Valid_out : out std_logic;
        Downstream_Data_Message_Ready_out : out std_logic;
        Downstream_EOP_IDLE_out : out std_logic;


        readyOut_to_frame : out std_logic; --m_axis_tready toward Frame MSG
        DataOut : out std_logic_vector(7 downto 0) --Towards EndeaverBlock
    );
end EndeavorFrameParser;

architecture Behavioral of EndeavorFrameParser is

    type t_state is (s_idle, s_next_bit, s_send_bit, s_bit_gap, s_send_bytes, s_pause, s_next_pause);

    signal state : t_state := s_idle;

    signal bytes_left : integer range 0 to 8 := 0;
    signal Pause_time : integer range 0 to 8 := 0;

    signal ticks_left : integer range 0 to 255 := 0;

    signal rst: std_logic;

    signal readyOut1: std_logic;

    signal First_Send_Flag: std_logic := '0';

    signal Downstream_Data_Message_Valid: std_logic := '0';
    signal Downstream_Data_Message_Ready: std_logic := '0';
    signal Downstream_EOP_IDLE: std_logic :=  '0';

    signal Message_frame_payload_length: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_2: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_3: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_4: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_5: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_6: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_7: std_logic_vector(7 downto 0) := (others => '0');
    signal Message_frame_payload_8: std_logic_vector(7 downto 0) := (others => '0');

    signal Data_out_to_encoder_holder: std_logic_vector(7 downto 0) := (others => '0');
begin

    DataOut <= Data_out_to_encoder_holder;

    --Message_ID_1 <= Message_ID_1_in;
    --Message_ID_2 <= Message_ID_2_in;

    --Message_frame_action <= Message_frame_action_in;
    Message_frame_payload_length <= Message_frame_payload_length_in;

    Downstream_Data_Message_Valid_out <= Downstream_Data_Message_Valid;
    Downstream_Data_Message_Ready_out <= Downstream_Data_Message_Ready;
    Downstream_EOP_IDLE_out <= Downstream_EOP_IDLE;

    rst <= reset;

    readyOut_to_frame <= readyOut1;

    --amac_signal <= amac_signal_s;
    --amac_signal_s <= not amac_out when invert_polarity = '1' else amac_out;


    state_update : process(clk40) is
    begin
        if rising_edge(clk40) then
            if rst = '1' then
                state <= s_idle;
            else
                case state is
                    when s_idle =>
                        if DataInValid = '1' and readyOut1 = '1' and Message_frame_action_in = "00000001" then
                            state <= s_send_bytes;
                        elsif DataInValid = '1' and readyOut1 = '1' and Message_frame_action_in = "00000010" then
                            state <= s_next_pause;
                        else
                            state <= s_idle;
                        end if;


                    when s_pause =>
                        if ticks_left = 0 then
                            if Pause_time = 0 then
                                state <= s_idle;
                            else
                                state <= s_next_pause;
                            end if;
                        else
                            state <= s_pause;
                        end if;

                    when s_next_pause =>
                        state <= s_pause;

                    when s_send_bytes =>
                        if First_Send_Flag = '1' then
                            state <= s_send_bytes;
                        elsif Downstream_Data_Message_Valid = '1' and readyIn = '1' and bytes_left = 0 then
                            state <= s_idle;
                        elsif Downstream_Data_Message_Valid = '1' and readyIn = '1' then
                            state <= s_send_bytes;

                        else
                            state <= s_send_bytes;
                        end if;



                    when others =>
                        -- pragma synthesis off
                        state <= s_idle;
                        report "Invalid state of AMAC signal encoder (state update)" severity failure;
                -- pragma synthesis on
                end case;
            end if;
        end if;
    end process;


    variable_update : process(clk40) is
    begin
        if rising_edge(clk40) then
            if rst = '1' then
                --amac_out <= '0';
                readyOut1 <= '0';
                ticks_left <= 0;
                --bits_left <= 0;
                First_Send_Flag <= '0';
                Data_out_to_encoder_holder <= (others => '0');
                Downstream_Data_Message_Ready <= '0';
                Downstream_Data_Message_Valid <= '0';
                Downstream_EOP_IDLE <= '0';
                Message_frame_payload_2 <= (others => '0');
                Message_frame_payload_3 <= (others => '0');
                Message_frame_payload_4 <= (others => '0');
                Message_frame_payload_5 <= (others => '0');
                Message_frame_payload_6 <= (others => '0');
                Message_frame_payload_7 <= (others => '0');
                Message_frame_payload_8 <= (others => '0');
                Pause_time <= 0;
                bytes_left <= 0;
            else
                case state is
                    when s_idle =>
                        if DataInValid = '1' and readyOut1 = '1' and Message_frame_action_in = "00000001" then
                            --end_of_chunk <= m_axis.tlast = '1';
                            --SEND ENDEAVOR MESSAGE
                            --end_of_chunk <= EOP_in = '1';
                            bytes_left <= TO_INTEGER(unsigned(Message_frame_payload_length_in));
                            readyOut1 <= '0';
                            --bits_left <= 8;

                            First_Send_Flag <= '1';

                            if Message_frame_payload_length_in ="00000001" then
                                Downstream_EOP_IDLE <= '1';
                            else
                                Downstream_EOP_IDLE <= '0';
                            end if;

                            --Message_ID_1 <= Message_ID_1_in;
                            ----Message_ID_2 <= Message_ID_2_in;

                            --Message_frame_payload_1 <= Message_frame_payload_1_in;
                            Message_frame_payload_2 <= Message_frame_payload_2_in;
                            Message_frame_payload_3 <= Message_frame_payload_3_in;
                            Message_frame_payload_4 <= Message_frame_payload_4_in;
                            Message_frame_payload_5 <= Message_frame_payload_5_in;
                            Message_frame_payload_6 <= Message_frame_payload_6_in;
                            Message_frame_payload_7 <= Message_frame_payload_7_in;
                            Message_frame_payload_8 <= Message_frame_payload_8_in;

                            Data_out_to_encoder_holder <= Message_frame_payload_1_in;

                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';
                        --Downstream_EOP_IDLE <= '0';

                        elsif DataInValid = '1' and readyOut1 = '1' and Message_frame_action_in = "00000010" then
                            --end_of_chunk <= m_axis.tlast = '1';
                            --IDLE PAUSE
                            --end_of_chunk <= EOP_in = '1';
                            bytes_left <= TO_INTEGER(unsigned(Message_frame_payload_length));
                            --bits_to_send <= Message_frame_payload_length_in;
                            readyOut1 <= '0';
                            --bits_left <= 8;
                            Pause_time <= TO_INTEGER(unsigned(Message_frame_payload_1_in));

                            --Message_frame_payload_1 <= Message_frame_payload_1_in;
                            Message_frame_payload_2 <= Message_frame_payload_2_in;
                            Message_frame_payload_3 <= Message_frame_payload_3_in;
                            Message_frame_payload_4 <= Message_frame_payload_4_in;
                            Message_frame_payload_5 <= Message_frame_payload_5_in;
                            Message_frame_payload_6 <= Message_frame_payload_6_in;
                            Message_frame_payload_7 <= Message_frame_payload_7_in;
                            Message_frame_payload_8 <= Message_frame_payload_8_in;

                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';

                            Downstream_EOP_IDLE <= '1';

                        else
                            readyOut1 <= '1';
                            --amac_out <= '0';
                            ticks_left <= 0;
                            --bits_left <= 0;
                            --end_of_chunk <= false;

                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';
                            Downstream_EOP_IDLE <= '1';

                        end if;


                    when s_pause =>
                        Downstream_Data_Message_Valid <= '0';
                        Downstream_Data_Message_Ready <= '0';
                        readyOut1 <= '0';
                        Downstream_EOP_IDLE <= '1';
                        if ticks_left = 0 then
                            null;
                        else
                            ticks_left <= ticks_left - 1;
                        end if;

                    when s_next_pause =>
                        Downstream_Data_Message_Valid <= '0';
                        Downstream_Data_Message_Ready <= '0';
                        Downstream_EOP_IDLE <= '1';

                        readyOut1 <= '0';
                        ticks_left <= TICKS_QUIESCENT;
                        if Pause_time = 0 then
                            null;
                        else
                            Pause_time <= Pause_time - 1;
                        end if;

                    when s_send_bytes =>
                        if First_Send_Flag = '1' then
                            Downstream_Data_Message_Valid <= '1';
                            Downstream_Data_Message_Ready <= '1';
                            readyOut1 <= '0';
                            if bytes_left = 1 then
                                Downstream_EOP_IDLE <= '1';
                            else
                                Downstream_EOP_IDLE <= '0';
                            end if;
                            bytes_left <= bytes_left - 1;
                            First_Send_Flag <= '0';

                        elsif Downstream_Data_Message_Valid = '1' and readyIn = '1' and bytes_left = 0 then
                            Downstream_Data_Message_Valid <= '0';
                            Downstream_Data_Message_Ready <= '0';
                            readyOut1 <= '0';
                            Downstream_EOP_IDLE <= '1';
                            First_Send_Flag <= '0';

                        elsif Downstream_Data_Message_Valid = '1' and readyIn = '1' then
                            Downstream_Data_Message_Valid <= '1';
                            Downstream_Data_Message_Ready <= '1';
                            readyOut1 <= '0';
                            First_Send_Flag <= '0';
                            if bytes_left = 1 then
                                Downstream_EOP_IDLE <= '1';
                            else
                                Downstream_EOP_IDLE <= '0';
                            end if;

                            bytes_left <= bytes_left - 1;
                            --Data_out_to_encoder_holder <= Message_frame_payload_1;
                            Data_out_to_encoder_holder <= Message_frame_payload_2;
                            Message_frame_payload_2 <= Message_frame_payload_3;
                            Message_frame_payload_3 <= Message_frame_payload_4;
                            Message_frame_payload_4 <= Message_frame_payload_5;
                            Message_frame_payload_5 <= Message_frame_payload_6;
                            Message_frame_payload_6 <= Message_frame_payload_7;
                            Message_frame_payload_7 <= Message_frame_payload_8;
                            Message_frame_payload_8 <= (others => '0');

                        else
                            Downstream_Data_Message_Valid <= '1';
                            Downstream_Data_Message_Ready <= '1';
                            readyOut1 <= '0';
                            Downstream_EOP_IDLE <= '0';
                            First_Send_Flag <= '0';
                        end if;


                    when others =>
                        -- pragma synthesis off
                        report "Invalid state of Endeavor Frame Parser (variable update)" severity failure;
                -- pragma synthesis on
                end case;
            end if;
        end if;
    end process;

end Behavioral;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Jochem Leijenhorst
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.


----------------------------------------------------------------------------------
--! Company:  Nikhef
--! Engineer: Jochem Leijenhorst
--!
--! Create Date:    26/03/2024
--! Module Name:    HIFIFO
--! Project Name:   FELIX
----------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

    use work.FELIX_package.all;


library xpm;
    use xpm.vcomponents.all;


-- The hififo (header inserting fifo) is a fifo that saves a spot in the memory for a header.
-- To save the current address as the header location, new_chunk must be asserted.
-- This will leave a gap in the memory between the previously written word and the word currently being written.
-- To set the header, set_header must be asserted.
-- This will store the data given through din into the gap that was left by asserting new_chunk.
--
-- When an unwritten header is reached by the read pointer,
-- the empty flag will be asserted and no more words will be read until the header is set by asserting set_header.
-- This is to prevent outputting an empty header gap as data.
--
-- A few details:
-- The fifo is First-Word Fall-Through
-- Asserting both set_header and new_chunk in the same clock cycle is supported.
-- Asserting new_chunk for more than one clock cycle without asserting set_header leave gaps in the memory.
-- Asserting set_header without previously asserting new_chunk is undefined behaviour.
--
--
-- An example of operation (chronological):
--
--                       | new_chunk
-- din                   |   | set_header
-- ----------------------|---|---|
-- final word of chunk0  | 0 | 0 |
-- chunk0 header         | 0 | 1 | <---
-- first word of chunk1  | 1 | 0 | <---
-- next words of chunk1..| 0 | 0 |     Both of these are allowed
-- last word of chunk1   | 0 | 0 |     and produce the same result.
-- chunk1 header         | 1 | 1 | <---
-- first word of chunk2  | 0 | 0 |
--
-- Resulting data:
-- | chunk0 header ||chunk0 data|| final word of chunk0 | chunk1 header | first word of chunk1 ||chunk1 data|| final word of chunk1 | etc.
entity HIFIFO is

    generic (
        -- Common module generics
        USE_URAM                : boolean := false; --true for ultraram, false to use block ram and independent clock. For ultraram wr_clk and rd_clk should be the same
        -- CASCADE_HEIGHT           : integer  := 0;       -- ???
        FIFO_WRITE_DEPTH        : integer  := 2048;
        WRITE_DATA_WIDTH        : integer  := 32;
        READ_DATA_WIDTH         : integer  := 32;
        PROG_FULL_THRESH        : integer  := 10;

        --! DO NOT EDIT THESE PLS
        FIFO_READ_DEPTH         : integer := FIFO_WRITE_DEPTH * WRITE_DATA_WIDTH / READ_DATA_WIDTH;
        WRITE_POINTER_WIDTH     : integer := f_log2(FIFO_WRITE_DEPTH-1) + 1;
        READ_POINTER_WIDTH      : integer := f_log2(FIFO_READ_DEPTH-1) + 1
    );

    port (
        rst             : in std_logic;

        wr_clk          : in std_logic;
        wr_en           : in std_logic;
        din             : in std_logic_vector(WRITE_DATA_WIDTH-1 downto 0);
        full            : out std_logic;
        prog_full       : out std_logic;
        set_header      : in std_logic;
        new_chunk       : in std_logic;
        wr_data_count   : out std_logic_vector(WRITE_POINTER_WIDTH-1 downto 0);

        rd_en           : in std_logic;
        dout            : out std_logic_vector(READ_DATA_WIDTH-1 downto 0);
        empty           : out std_logic;
        almost_empty    : out std_logic;
        rd_data_count   : out std_logic_vector(READ_POINTER_WIDTH-1 downto 0)

    );

end HIFIFO;


architecture Behavioral of HIFIFO is


    -- _wr means converted to the write clock domain.
    -- _rd means converted to the read clock domain.
    signal readPointer          : std_logic_vector(READ_POINTER_WIDTH-1 downto 0);
    signal readPointer_wr       : std_logic_vector(WRITE_POINTER_WIDTH-1 downto 0);
    signal readPointerPlus1     : std_logic_vector(READ_POINTER_WIDTH-1 downto 0);
    signal writePointer         : std_logic_vector(WRITE_POINTER_WIDTH-1 downto 0);
    signal headPointer_rd       : std_logic_vector(READ_POINTER_WIDTH-1 downto 0);
    signal writePointerPlus1    : std_logic_vector(WRITE_POINTER_WIDTH-1 downto 0);
    signal writePointerPlus2    : std_logic_vector(WRITE_POINTER_WIDTH-1 downto 0);
    signal writePointer_rd      : std_logic_vector(READ_POINTER_WIDTH-1 downto 0);


    signal newWriteAddr         : std_logic_vector(WRITE_POINTER_WIDTH-1 downto 0);
    signal memWriteAddr         : std_logic_vector(WRITE_POINTER_WIDTH-1 downto 0);
    signal headPointer          : std_logic_vector(WRITE_POINTER_WIDTH-1 downto 0);

    -- Keep track of writing the header for setting the empty flag.
    -- We don't want the read pointer to pass the headPointer when the header is still empty.
    signal isHeaderSet      : std_logic;
    signal isHeaderSet_rd   : std_logic;

    signal rst_rd           : std_logic;

    signal full_s           : std_logic;
    signal prog_full_s      : std_logic;
    signal wr_data_count_s  : std_logic_vector(WRITE_POINTER_WIDTH-1 downto 0);

    signal memWriteAddr_p1  : std_logic_vector(WRITE_POINTER_WIDTH-1 downto 0);
    signal wr_en_p1         : std_logic;
    signal din_p1           : std_logic_vector(WRITE_DATA_WIDTH-1 downto 0);

    -- FSM for the stages of the output
    -- The output is loaded into the register within the memory block whenever it is ready
    -- It is ready when the first memory output word is written to
    type output_state is (none_ready, buffer_ready, output_ready, both_ready);
    signal state : output_state;
    signal nextState : output_state;

    signal memoryEmpty : std_logic;
    signal memoryGoingEmpty : std_logic;
    signal memoryReadEnable : std_logic;

    -- Activates the clk of the last output register of the memory.
    signal outputBuffer : std_logic;
    signal doReadCount : std_logic;

    function MEMORY_PRIMITIVE(uram: boolean) return string is
    begin
        if uram then
            return "ultra";
        else
            return "block";
        end if;
    end function;

    function CLOCKING_MODE(uram: boolean) return string is
    begin
        if uram then
            return "common_clock";
        else
            return "independent_clock";
        end if;
    end function;

    function WRITE_MODE_A(uram: boolean) return string is
    begin
        if uram then
            return "no_change";
        else
            return "write_first";
        end if;
    end function;

    function WRITE_MODE_B(uram: boolean) return string is
    begin
        if uram then
            return "no_change";
        else
            return "read_first";
        end if;
    end function;


begin

    full_s <= '1' when writePointerPlus1 = readPointer_wr or rst = '1' else '0';
    full <= full_s;

    wr_data_count <= wr_data_count_s;

    --Use the same mechanism as for Empty to say that rd_data_count is '0'
    rd_data_count <= (others => '0') when state = none_ready or state = buffer_ready else (writePointer_rd - readPointer)+2 ;

    prog_full_s <= '1' when wr_data_count_s >= PROG_FULL_THRESH or full_s = '1' else '0';

    writePointerPlus1 <= writePointer + 1;
    writePointerPlus2 <= writePointer + 2;

    newWriteAddr <= writePointerPlus1 when new_chunk = '1' else writePointer;
    memWriteAddr <= headPointer when set_header = '1' else newWriteAddr;


    writeCounter: process(wr_clk)
    begin
        if rising_edge(wr_clk) then
            if rst = '1' then
                writePointer <= (others => '0');
                headPointer <= (others => '0');
                isHeaderSet <= '0';
                prog_full <= '1';
            else
                prog_full <= prog_full_s;
                if set_header then
                    isHeaderSet <= '1';
                end if;
                if wr_en = '1' and full_s = '0' then
                    if new_chunk = '1' then
                        headPointer <= writePointer;
                        writePointer <= writePointerPlus2;
                        isHeaderSet <= '0';
                    elsif set_header = '0' then
                        -- Increasing the write pointer when setting the header would leave gaps in the memory.
                        writePointer <= writePointerPlus1;
                    end if;
                end if;
            end if;
        end if;
    end process;


    readCounter: process(wr_clk)
    begin
        if rising_edge(wr_clk) then
            if rst_rd = '1' then
                readPointer <= (others => '0');
                readPointerPlus1 <= std_logic_vector(to_unsigned(1, READ_POINTER_WIDTH));
            else
                if doReadCount = '1' then
                    readPointer <= readPointer + 1;
                    readPointerPlus1 <= readPointerPlus1 + 1;
                end if;
            end if;
        end if;
    end process;


    writePipe: process(wr_clk)
    begin
        if rising_edge(wr_clk) then
            memWriteAddr_p1 <= memWriteAddr;
            wr_en_p1 <= wr_en;
            din_p1 <= din;
            wr_data_count_s <= writePointer - readPointer_wr;
        end if;
    end process;

    -- The fifo should be seen as empty when the read pointer is on the header location
    -- when the header hasn't been set yet.
    -- TODO: clarify gigantic logic statements.
    memoryGoingEmpty <= '1' when (readPointerPlus1 = writePointer_rd or (readPointerPlus1 = headPointer_rd and isHeaderSet_rd = '0')) and doReadCount = '1' else '0';

    memoryEmptyDriver: process(wr_clk)
    begin
        if rising_edge(wr_clk) then
            if rst_rd = '1' then
                memoryEmpty <= '1';
            else
                if memoryGoingEmpty = '1' or (memoryEmpty = '1' and (readPointer = writePointer_rd or (readPointer = headPointer_rd and isHeaderSet_rd = '0'))) then
                    memoryEmpty <= '1';
                else
                    memoryEmpty <= '0';
                end if;

            end if;
        end if;
    end process;


    -- There are 2 registers/flipflops on the output of the memory.
    -- Let's call them buffer and output:
    --
    -- memory-->[buffer]-->[output]--dout-->
    --
    -- The output register's clock is activated by regceb which is driven by outputBuffer.

    -- There is an output available whenever the output register has valid data.
    -- That's the case in the output_ready and both_ready states.
    -- Empty should be asserted in all other states.
    empty <= '1' when state = none_ready or state = buffer_ready else '0';
    almost_empty <= '1' when state = output_ready or state = none_ready or state = buffer_ready else '0';

    -- outputBuffer activates the output register's clock, which will copy the value from the buffer register
    -- to the output register during the clock cycles in which outputBuffer is asserted.
    -- This should only happen when we have valid data in the buffer register and the last output has been read.
    outputBuffer <= '1' when state = buffer_ready or (state = both_ready and rd_en = '1') else '0';

    -- doReadCount enables the counter, which increments the readPointer on every clock cycle.
    -- The address should be incremented whenever the previous value is valid (memoryEmpty = '0'),
    -- and, if we're in the both_ready state, the last value has been read.
    doReadCount <= '1' when memoryEmpty = '0' and not (rd_en = '0' and state = both_ready) else '0';

    -- memoryReadEnable basically controls the clock of the buffer register.
    -- It needs to activate whenever a new value is available to be read from the memory.
    -- This is basically all the time, except when the last value has not been read yet in the both_ready state.
    -- If we do enable the read in those conditions, a value will be lost, because the address has already been incremented.
    memoryReadEnable <= '0' when state = both_ready and rd_en = '0' else '1';


    FSM: process(state, memoryEmpty, rd_en)
    begin
        case state is
            when none_ready =>
                -- The none_ready state indicates that there is neither valid data in the buffer nor the output register.
                if memoryEmpty = '0' then
                    nextState <= buffer_ready;
                else
                    nextState <= none_ready;
                end if;

            when buffer_ready =>
                -- buffer_ready means there is valid data available in the buffer register but not in the output register.
                -- outputBuffer is always 1 in this state.
                -- This means that we need to always leave this state immediately to not lose any data.
                if memoryEmpty = '0' then
                    nextState <= both_ready;
                else
                    nextState <= output_ready;
                end if;

            -- This is where rd_en starts mattering.
            when output_ready =>
                -- output_ready means there is invalid data in the buffer register, but valid data in the output register.
                -- The output register is directly connected to dout, which means that the fifo can now be read from.
                if memoryEmpty = '0' then
                    if rd_en = '0' then
                        nextState <= both_ready;
                    else
                        nextState <= buffer_ready;
                    end if;
                else
                    if rd_en = '0' then
                        nextState <= output_ready;
                    else
                        nextState <= none_ready;
                    end if;
                end if;
            when both_ready =>
                -- both_ready means there is valid data in both the buffer and the output register.
                -- This comfortable state is only left when both rd_en and empty are asserted.
                if memoryEmpty = '1' and rd_en = '1' then
                    nextState <= output_ready;
                else
                    nextState <= both_ready;
                end if;
        end case;
    end process;

    FSM_set: process(wr_clk)
    begin
        if rising_edge(wr_clk) then
            if rst_rd = '1' then
                state <= none_ready;
            else
                state <= nextState;
            end if;
        end if;
    end process;


    memory: xpm_memory_tdpram
        generic map (

            -- Common module generics
            MEMORY_SIZE              => WRITE_DATA_WIDTH * FIFO_WRITE_DEPTH,
            MEMORY_PRIMITIVE         => MEMORY_PRIMITIVE(USE_URAM),
            CLOCKING_MODE            => "common_clock",
            ECC_MODE                 => "no_ecc",
            MEMORY_INIT_FILE         => "none",
            MEMORY_INIT_PARAM        => "",
            USE_MEM_INIT             => 1,
            USE_MEM_INIT_MMI         => 0,
            WAKEUP_TIME              => "disable_sleep",
            AUTO_SLEEP_TIME          => 0,
            MESSAGE_CONTROL          => 0,
            USE_EMBEDDED_CONSTRAINT  => 0,
            MEMORY_OPTIMIZATION      => "true",
            CASCADE_HEIGHT           => 0,
            SIM_ASSERT_CHK           => 0,
            WRITE_PROTECT            => 1,

            -- Port A module generics
            WRITE_DATA_WIDTH_A  => WRITE_DATA_WIDTH,
            READ_DATA_WIDTH_A   => WRITE_DATA_WIDTH,
            BYTE_WRITE_WIDTH_A  => WRITE_DATA_WIDTH,
            ADDR_WIDTH_A        => WRITE_POINTER_WIDTH,
            READ_RESET_VALUE_A  => "0",
            READ_LATENCY_A      => 1,
            WRITE_MODE_A        => WRITE_MODE_A(USE_URAM),
            RST_MODE_A          => "SYNC",

            -- Port B module generics
            WRITE_DATA_WIDTH_B  => READ_DATA_WIDTH,
            READ_DATA_WIDTH_B   => READ_DATA_WIDTH,
            BYTE_WRITE_WIDTH_B  => READ_DATA_WIDTH,
            ADDR_WIDTH_B        => READ_POINTER_WIDTH,
            READ_RESET_VALUE_B  => "0",
            READ_LATENCY_B      => 2,
            WRITE_MODE_B        => WRITE_MODE_B(USE_URAM),
            RST_MODE_B          => "SYNC"

        )
        port map (

            -- Common module ports
            sleep          => '0',

            -- Port A module ports
            clka           => wr_clk,
            rsta           => rst,
            ena            => '1',
            regcea         => '1',
            wea            => (others => wr_en_p1),
            addra          => memWriteAddr_p1,
            dina           => din_p1,
            injectsbiterra => '0',
            injectdbiterra => '0',
            douta          => open,
            sbiterra       => open,
            dbiterra       => open,

            -- Port B module ports
            clkb           => wr_clk,
            rstb           => rst_rd,
            enb            => memoryReadEnable, --! yoo maybe do something with the reset here
            regceb         => outputBuffer,
            web            => "0",
            addrb          => readPointer,
            dinb           => (others => '0'),
            injectsbiterrb => '0',
            injectdbiterrb => '0',
            doutb          => dout,
            sbiterrb       => open,
            dbiterrb       => open
        );


    rst_rd <= rst;

    isHeaderSet_rd <= isHeaderSet;

    checkMoreThan: if WRITE_POINTER_WIDTH < READ_POINTER_WIDTH generate

        headPointer_rd(READ_POINTER_WIDTH-1 downto READ_POINTER_WIDTH - WRITE_POINTER_WIDTH) <= headPointer;

        writePointer_rd(READ_POINTER_WIDTH-1 downto READ_POINTER_WIDTH - WRITE_POINTER_WIDTH) <= writePointer;
        headPointer_rd(READ_POINTER_WIDTH - WRITE_POINTER_WIDTH - 1 downto 0) <= (others => '0');
        writePointer_rd(READ_POINTER_WIDTH - WRITE_POINTER_WIDTH - 1 downto 0) <= (others => '0');


        readPointer_wr <= readPointer(READ_POINTER_WIDTH-1 downto (READ_POINTER_WIDTH - WRITE_POINTER_WIDTH));

    else generate


        headPointer_rd <= headPointer(WRITE_POINTER_WIDTH-1 downto (WRITE_POINTER_WIDTH - READ_POINTER_WIDTH));

        writePointer_rd <= writePointer(WRITE_POINTER_WIDTH-1 downto (WRITE_POINTER_WIDTH - READ_POINTER_WIDTH));

        readPointer_wr(WRITE_POINTER_WIDTH-1 downto (WRITE_POINTER_WIDTH - READ_POINTER_WIDTH)) <= readPointer;
        readPointer_wr(WRITE_POINTER_WIDTH - READ_POINTER_WIDTH - 1 downto 0) <= (others => '0');
    end generate;

end architecture;



--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Julia Narevicius
--!               Frans Schreuder
--!               Enrico Gamberini
--!               William Wulff
--!               Thei Wijnen
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.


library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use work.FELIX_package.all;
library xpm;
    use xpm.vcomponents.all;

--! reading from Downstream thchs order
entity CRToHostPCIeManager is
    Generic (
        GBT_NUM : integer := 1;
        DATA_WIDTH: integer;
        BLOCKSIZE : integer := 1024;
        NUMBER_OF_DESCRIPTORS_TOHOST : integer:= 1
    );
    Port (
        clk : in  std_logic;
        clk40 : in std_logic;
        rst : in  std_logic;
        thch_hasBlock_array : in  std_logic_vector ((GBT_NUM-1) downto 0); -- complete blocks are ready to be read
        PCIe_prog_full_array : in std_logic_vector(NUMBER_OF_DESCRIPTORS_TOHOST-1 downto 0);
        ---
        thch_sel       : out  integer range 0 to GBT_NUM-1; -- selecting a channel to read from (for writing to crOUTfifo)
        thch_re_array  : out  std_logic_vector ((GBT_NUM-1) downto 0);
        output_select  : out integer range 0 to NUMBER_OF_DESCRIPTORS_TOHOST-1;
        discard_block  : out std_logic; --Block is read from channel fifo, but discarded and not written into Wupper FIFO.
        EpathIDs       : in array_11b(0 to GBT_NUM-1);
        dma_enable_in  : in std_logic_vector(NUMBER_OF_DESCRIPTORS_TOHOST-1 downto 0);
        DISCARD_ON_DMA_DISABLE : in std_logic_vector(NUMBER_OF_DESCRIPTORS_TOHOST-1 downto 0);
        DISCARD_ON_PROG_FULL : in std_logic_vector(NUMBER_OF_DESCRIPTORS_TOHOST-1 downto 0);
        CRTOHOST_DMA_DESCRIPTOR_WR_EN      : in std_logic_vector(0 downto 0);
        CRTOHOST_DMA_DESCRIPTOR_DESCR      : in std_logic_vector(2 downto 0);
        CRTOHOST_DMA_DESCRIPTOR_DESCR_READ : out std_logic_vector(2 downto 0);
        CRTOHOST_DMA_DESCRIPTOR_AXIS_ID    : in std_logic_vector(10 downto 0)
    );
end CRToHostPCIeManager;

architecture Behavioral of CRToHostPCIeManager is

    signal FMCHcount  : std_logic_vector(5 downto 0) := (others => '0'); -- counts to maximum possible channel number (0 to 31) (double clocks)
    constant NUMBER_OF_256_PER_BLOCK : integer := BLOCKSIZE / 32;
    signal BLOCKcount : integer range 0 to (NUMBER_OF_256_PER_BLOCK*2)-1:=0; -- std_logic_vector(5 downto 0) := (others => '0'); -- counts 256-bit words in block (0 to 31)

    signal thch_hasBlock_array_full : std_logic_vector(GBT_NUM-1 downto 0) := (others => '0');
    signal thch_reN_array : std_logic_vector(GBT_NUM-1 downto 0);
    signal thch_hasBlock, block_done, thch_hasBlock_1clk, thch_hasBlock_s, re_s, countENA : std_logic;
    signal rst_state , rst_clk40: std_logic := '1';
    signal thch_sel_int : integer range 0 to GBT_NUM;
    signal output_select_s: integer range 0 to NUMBER_OF_DESCRIPTORS_TOHOST-1;
    signal next_output_select_s: integer range 0 to NUMBER_OF_DESCRIPTORS_TOHOST-1;
    signal EpathID: std_logic_vector(10 downto 0);
    signal AssociateEpathIdCNT: integer range 0 to GBT_NUM-1 := 0;
    signal AssociateEpathId, AssociateEpathId_p1, AssociateEpathId_p2: integer range 0 to GBT_NUM;
    signal EpathIDAssociated : std_logic_vector(GBT_NUM-1 downto 0);
    signal AssociatedDMAChannel: array_3b(0 to GBT_NUM-1) := (others => (others => '0'));
    signal doutb: std_logic_vector(2 downto 0);
    signal thch_hasBlock_s_and_prog_full : std_logic;
    signal thch_hasBlock_1clk_and_prog_full : std_logic;
    signal dma_enable_sync, DISCARD_ON_DMA_DISABLE_sync, DISCARD_ON_PROG_FULL_sync: std_logic_vector(NUMBER_OF_DESCRIPTORS_TOHOST-1 downto 0);

--attribute MARK_DEBUG : string;
--attribute MARK_DEBUG of FMCHcount, thch_hasBlock_array_full, thch_hasBlock_s_and_prog_full, thch_hasBlock_1clk_and_prog_full, dma_enable_sync, DISCARD_ON_DMA_DISABLE_sync, DISCARD_ON_PROG_FULL_sync, thch_sel, thch_re_array, output_select, discard_block: signal is "TRUE";
begin

    -----------------------------------------------------
    -- reset state
    -----------------------------------------------------
    sync_rst : xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 2,
            INIT => 1,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0
        )
        port map (
            src_rst => rst,
            dest_clk => clk,
            dest_rst => rst_state
        );

    sync_rst40 : xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 2,
            INIT => 1,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0
        )
        port map (
            src_rst => rst,
            dest_clk => clk40,
            dest_rst => rst_clk40
        );

    sync_dma_enable: xpm_cdc_array_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0,
            WIDTH => NUMBER_OF_DESCRIPTORS_TOHOST
        )
        port map(
            src_clk => '0',
            src_in => dma_enable_in,
            dest_clk => clk,
            dest_out => dma_enable_sync
        );

    sync_DISCARD_ON_DMA_DISABLE: xpm_cdc_array_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0,
            WIDTH => NUMBER_OF_DESCRIPTORS_TOHOST
        )
        port map(
            src_clk => '0',
            src_in => DISCARD_ON_DMA_DISABLE,
            dest_clk => clk,
            dest_out => DISCARD_ON_DMA_DISABLE_sync
        );

    sync_DISCARD_ON_PROG_FULL: xpm_cdc_array_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0,
            WIDTH => NUMBER_OF_DESCRIPTORS_TOHOST
        )
        port map(
            src_clk => '0',
            src_in => DISCARD_ON_PROG_FULL,
            dest_clk => clk,
            dest_out => DISCARD_ON_PROG_FULL_sync
        );

    -----------------------------------------------------
    -- FMCH COUNTER 3 bit (31 CHs maximum)
    -----------------------------------------------------
    FMCH_counter: process(clk)
    begin
        if clk'event and clk = '1' then
            if rst_state = '1' then
                FMCHcount <= (others => '0');
            else
                if countENA = '1' then
                    if FMCHcount = (std_logic_vector(to_unsigned((GBT_NUM-1), 5)) & '1') then
                        FMCHcount <= (others => '0');
                    else
                        FMCHcount <= FMCHcount + 1;
                    end if;
                else --(countENA = '0')
                -- FMCHcount holds its value on the next clock
                end if;
            end if;
        end if;
    end process;
    --
    thch_sel <= to_integer(unsigned(FMCHcount(5 downto 1)));
    thch_sel_int <= to_integer(unsigned(FMCHcount(5 downto 1)));
    --
    thch_hasBlock_array_full((GBT_NUM-1) downto 0) <= thch_hasBlock_array; -- GBT_NUM bit
    --
    thch_hasBlock   <= thch_hasBlock_array_full(to_integer(unsigned(FMCHcount(5 downto 1))));

    next_output_select_s <= to_integer(unsigned(AssociatedDMAChannel(to_integer(unsigned(FMCHcount(5 downto 1))))));

    thch_hasBlock_s <= thch_hasBlock and
                       (not FMCHcount(0)) and
                       (not rst_state) and
                       (
                        (not PCIe_prog_full_array(next_output_select_s)) or --Start reading when target FIFO is not full
                        (DISCARD_ON_DMA_DISABLE_sync(next_output_select_s) and (not dma_enable_sync(next_output_select_s))) or --Or we should discard when FIFO is FULL and DMA disabled
                        (DISCARD_ON_PROG_FULL_sync(next_output_select_s))                                               --Or we should discard when the FIFO is FULL
                        
                      ) and
                       EpathIDAssociated(to_integer(unsigned(FMCHcount(5 downto 1))));

    thch_rdy_pulse: entity work.pulse_pdxx_pwxx
        generic map(
            pd=>0,
            pw=>1
        )
        port map(
            clk => clk,
            trigger => thch_hasBlock_s,
            pulseout => thch_hasBlock_1clk
        );

    thch_hasBlock_s_and_prog_full <= thch_hasBlock_s and PCIe_prog_full_array(next_output_select_s); --We have allowed a block to pass through while the FIFO is full

    thch_rdy_and_prog_full_pulse: entity work.pulse_pdxx_pwxx
        generic map(
            pd=>0,
            pw=>1
        )
        port map(
            clk => clk,
            trigger => thch_hasBlock_s_and_prog_full,
            pulseout => thch_hasBlock_1clk_and_prog_full
        );

    output_select <= output_select_s;

    select_output_proc: process(clk)
    begin
        if rising_edge(clk) then
            if rst_state = '1' then
                output_select_s <= 0;
                discard_block <= '0';
            else
                if thch_hasBlock_1clk = '1' then
                    if thch_hasBlock_1clk_and_prog_full = '1' then
                        discard_block <= '1';
                    else
                        discard_block <= '0';
                    end if;

                    if next_output_select_s < NUMBER_OF_DESCRIPTORS_TOHOST then
                        output_select_s <= next_output_select_s;
                    else
                        output_select_s <= 0;
                    end if;
                end if;
            end if;
        end if;
    end process;

    --
    g_256: if DATA_WIDTH = 256 generate
        block_done <= '1' when (BLOCKcount = NUMBER_OF_256_PER_BLOCK) else '0'; -- 1KByte = 256 x 32
    end generate;
    g_512: if DATA_WIDTH = 512 generate
        block_done <= '1' when (BLOCKcount = NUMBER_OF_256_PER_BLOCK/2) else '0'; -- 1KByte = 512 x 16
    end generate;
    g_1024: if DATA_WIDTH = 1024 generate
        block_done <= '1' when (BLOCKcount = NUMBER_OF_256_PER_BLOCK/4) else '0'; -- 1KByte = 1024 x 8
    end generate;
    --
    reN: entity work.ReMuxN
        generic map( N => GBT_NUM)
        port map(
            clk      => clk,
            rst      => rst_state,
            sel      => thch_sel_int,
            trigON   => thch_hasBlock_1clk,
            trigOFF  => block_done,
            re       => re_s,
            reNbit   => thch_reN_array
        );
    --
    countENA        <= not re_s;
    thch_re_array   <= thch_reN_array((GBT_NUM-1) downto 0);
    --

    -----------------------------------------------------
    -- BLOCK WORD COUNTER
    -----------------------------------------------------
    BLOCKcounter : process(clk)
    begin
        if clk'event and clk = '1' then
            if rst_state = '1' then
                BLOCKcount <= 1; --(others => '0');
            else
                if re_s = '1' then
                    if block_done = '1' then
                        BLOCKcount <= 1; --(others => '0');
                    else
                        BLOCKcount <= BLOCKcount + 1;
                    end if;
                else
                    BLOCKcount <= 1; --(others => '0');
                end if;
            end if;
        end if;
    end process;

    --


    associate_epath_id_proc: process(clk)
    begin

        if rising_edge(clk) then
            if rst = '1' then
                AssociateEpathId <= GBT_NUM;
                AssociateEpathId_p1 <= GBT_NUM;
                AssociateEpathId_p2 <= GBT_NUM;
                AssociateEpathIdCNT <= 0;
                EpathIDAssociated <= (others => '0');
                EpathID <= (others => '0');
            else
                if AssociateEpathIdCNT < GBT_NUM-1 then
                    AssociateEpathIdCNT <= AssociateEpathIdCNT + 1;
                else
                    AssociateEpathIdCNT <= 0;
                end if;
                if thch_hasBlock_array(AssociateEpathIdCNT) = '1' and thch_reN_array(AssociateEpathIdCNT) = '0' then
                    AssociateEpathId <= AssociateEpathIdCNT;
                    EpathID <= EpathIDs(AssociateEpathIdCNT);
                else
                    AssociateEpathId <= GBT_NUM;
                end if;
                AssociateEpathId_p1 <= AssociateEpathId;
                AssociateEpathId_p2 <= AssociateEpathId_p1;
                if AssociateEpathId_p2 /= GBT_NUM then
                    AssociatedDMAChannel(AssociateEpathId_p2) <= doutb;
                    EpathIDAssociated(AssociateEpathId_p2) <= '1';
                end if;
                for i in 0 to GBT_NUM-1 loop
                    if thch_reN_array(i) = '1' then
                        EpathIDAssociated(i) <= '0';
                    end if;
                end loop;
            end if;
        end if;
    end process;

    target_fifo_lookup_memory : xpm_memory_tdpram
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: USE_MEM_INIT_MMI, CASCADE_HEIGHT, SIM_ASSERT_CHK, WRITE_PROTECT"
            ADDR_WIDTH_A => 11,
            ADDR_WIDTH_B => 11,
            AUTO_SLEEP_TIME => 0,
            BYTE_WRITE_WIDTH_A => 3,
            BYTE_WRITE_WIDTH_B => 3,
            CLOCKING_MODE => "independent_clock",
            ECC_MODE => "no_ecc",
            MEMORY_INIT_FILE => "none",
            MEMORY_INIT_PARAM => "0",
            MEMORY_OPTIMIZATION => "true",
            MEMORY_PRIMITIVE => "auto",
            MEMORY_SIZE => 2048*3,
            MESSAGE_CONTROL => 0,
            READ_DATA_WIDTH_A => 3,
            READ_DATA_WIDTH_B => 3,
            READ_LATENCY_A => 2,
            READ_LATENCY_B => 2,
            READ_RESET_VALUE_A => "0",
            READ_RESET_VALUE_B => "0",
            RST_MODE_A => "SYNC",
            RST_MODE_B => "SYNC",
            USE_EMBEDDED_CONSTRAINT => 0,
            USE_MEM_INIT => 0,
            WAKEUP_TIME => "disable_sleep",
            WRITE_DATA_WIDTH_A => 3,
            WRITE_DATA_WIDTH_B => 3,
            WRITE_MODE_A => "no_change",
            WRITE_MODE_B => "no_change"
        )
        port map (
            sleep => '0',
            clka => clk40,
            rsta => rst_clk40,
            ena => '1',
            regcea => '1',
            wea => CRTOHOST_DMA_DESCRIPTOR_WR_EN,
            addra => CRTOHOST_DMA_DESCRIPTOR_AXIS_ID,
            dina => CRTOHOST_DMA_DESCRIPTOR_DESCR,
            injectsbiterra => '0',
            injectdbiterra => '0',
            douta => CRTOHOST_DMA_DESCRIPTOR_DESCR_READ,
            sbiterra => open,
            dbiterra => open,
            clkb => clk,
            rstb => rst_state,
            enb => '1',
            regceb => '1',
            web => "0",
            addrb => EpathID,
            dinb => "000",
            injectsbiterrb => '0',
            injectdbiterrb => '0',
            doutb => doutb,
            sbiterrb => open,
            dbiterrb => open
        );

end Behavioral;


--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Julia Narevicius
--!               Israel Grayzman
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.
--! Engineer: juna
--!
--! Create Date:    09/11/2014
--! Module Name:    CRresetManager
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
library xpm;
    use xpm.vcomponents.all;


--!
entity CRresetManager is
    port (
        clk40           : in  std_logic;
        rst             : in  std_logic;
        clk40_stable    : in  std_logic;
        cr_rst          : out std_logic;
        cr_fifo_flush   : out std_logic
    );
end CRresetManager;

architecture Behavioral of CRresetManager is

    --
    constant fifoFLUSHcount_max : std_logic_vector (5 downto 0) := "100000";
    constant commonRSTcount_max : std_logic_vector (5 downto 0) := (others=>'1');
    signal cr_rst_rr,fifoFLUSH : std_logic := '1';
    signal rstTimerCount : std_logic_vector (5 downto 0) := (others=>'0');
    --
    signal rst40: std_logic;
begin

    --Sychronize rst to 40MHz clock domain
    xpm_cdc_sync_rst_inst : xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 2,   -- DECIMAL; range: 2-10
            INIT => 1,           -- DECIMAL; 0=initialize synchronization registers to 0, 1=initialize
            -- synchronization registers to 1
            INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        )
        port map (
            src_rst => rst, -- 1-bit input: Source reset signal.
            dest_clk => clk40, -- 1-bit input: Destination clock.
            dest_rst => rst40 -- 1-bit output: src_rst synchronized to the destination clock domain. This output
        );
    ------------------------------------------------------------
    --
    ------------------------------------------------------------
    --
    rstTimerCounter: process(clk40,rst40)
    begin
        if rst40 = '1' then
            rstTimerCount <= (others=>'0');
        elsif rising_edge(clk40) then
            if rstTimerCount = commonRSTcount_max then -- stop counting
                rstTimerCount <= rstTimerCount; -- freese counter
            else
                rstTimerCount <= rstTimerCount + 1;
            end if;
        end if;
    end process;
    --
    cr_rst_out: process(clk40, rst40)
    begin
        if rst40 = '1' then
            cr_rst_rr <= '1';
        elsif rising_edge(clk40) then
            if rstTimerCount = commonRSTcount_max then
                cr_rst_rr <= '0';
            else
                cr_rst_rr <= cr_rst_rr;
            end if;
        end if;
    end process;

    -- Don't flush FIFOs when the clock is not stable, see FLX-1834 and FLXUSERS-292

    crFifoFlush: process(clk40,rst40, clk40_stable)
    begin
        if rst40 = '1' or clk40_stable = '0' then
            fifoFLUSH <= '0';
        elsif rising_edge(clk40) then
            if rstTimerCount = fifoFLUSHcount_max then
                fifoFLUSH <= '0';
            elsif rstTimerCount > fifoFLUSHcount_max-20  and rstTimerCount < fifoFLUSHcount_max-1 then
                fifoFLUSH <= '1';
            else
                fifoFLUSH <= '0';
            end if;
        end if;
    end process;
    --
    cr_rst          <= cr_rst_rr;
    cr_fifo_flush   <= fifoFLUSH;
--

end Behavioral;


--! Copyright [2020] [Frans Schreuder]
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.
library vunit_lib;
    context vunit_lib.vunit_context;

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std_unsigned.all;
    use ieee.numeric_std.all;

library std;
    use std.env.all;

library xpm;
    use xpm.vcomponents.all;

    use work.FELIX_package.all;
--
entity HIFIFO_tb is
    generic(runner_cfg : string := runner_cfg_default);
end HIFIFO_tb;

architecture tb of HIFIFO_tb is

    --testbench constants
    constant FREEZEON_ERROR : integer := 0;
    constant TB_STOP_CNT    : integer := 2;
    constant TB_SEED        : integer := 20;

    constant MINE : std_logic := '1';

    constant FIFO_WRITE_DEPTH : integer := 2048;
    constant WRITE_DATA_WIDTH : integer := 32;
    constant READ_DATA_WIDTH  : integer := 256;


    signal  status    : std_logic_vector(7 downto 0);
    signal  wr_clk    : std_logic;
    signal  rd_clk    : std_logic;
    signal  reset     : std_logic;
    signal  sim_done  : std_logic;

    signal wr_en        : std_logic;
    signal din          : std_logic_vector(WRITE_DATA_WIDTH-1 downto 0);
    signal full         : std_logic;
    signal rd_en        : std_logic;
    signal dout         : std_logic_vector(READ_DATA_WIDTH-1 downto 0);
    signal empty        : std_logic;
    signal set_header   : std_logic;
    signal new_chunk    : std_logic;

    signal startReading : std_logic;

    constant wr_clk_period : time := 40 ns;
    constant rd_clk_period : time := 20 ns;
    constant FIFO_READ_DEPTH         : integer := FIFO_WRITE_DEPTH * WRITE_DATA_WIDTH / READ_DATA_WIDTH;

    signal rd_data_count: std_logic_vector(f_log2(FIFO_READ_DEPTH-1) downto 0);

begin

    -- Generation of clock
    process
    begin
        wr_clk <= '1';
        wait for wr_clk_period/2;
        wr_clk <= '0';
        wait for wr_clk_period/2;
    end process;

    process
    begin
        rd_clk <= '1';
        wait for rd_clk_period/2;
        rd_clk <= '0';
        wait for rd_clk_period/2;
    end process;

    process
    begin
        reset <= '1';
        wait for rd_clk_period * 5;
        reset <= '0';
        wait;
    end process;


    writing: process
    begin
        test_runner_setup(runner, runner_cfg);
        wr_en <= '0';
        din <= x"10101010";
        set_header <= '0';
        new_chunk <= '0';


        wait until reset = '0';
        wait for wr_clk_period * 2;
        wr_en <= '1';
        wait for wr_clk_period;
        din <= x"deadcafe";
        wait for wr_clk_period;

        -- Set header location
        new_chunk <= '1';
        din <= x"900df00d";
        wait for wr_clk_period;
        new_chunk <= '0';

        for i in 1 to 255 loop
            din <= std_logic_vector(to_unsigned(i*3, 32));
            if i = 101 or i = 110 or i = 121 then
                new_chunk <= '1';
            else
                new_chunk <= '0';
            end if;
            if i = 100 or i = 110 or i = 120  or i = 255 then
                set_header <= '1';
            else
                set_header <= '0';
            end if;

            wait for wr_clk_period;
        end loop;
        wr_en <= '0';
        wait for wr_clk_period * 1024;
        wr_en <= '1';

        -- Set header data
        din <= x"88889999";
        set_header <= '1';
        wait for wr_clk_period;
        set_header <= '0';


        din <= (others => '0');
        for i in 1 to 4000 loop
            din <= std_logic_vector(to_unsigned(i*5, 32));
            report integer'image(i);
            wait for wr_clk_period;
            if full = '1' then
                wait until full = '0';
            end if;
        end loop;
        wr_en <= '0';


        wait;
    end process;

    rd_en <= not empty and startReading;

    reading: process
    begin
        startReading <= '0';
        wait until reset = '0';
        wait until empty = '0';
        wait for rd_clk_period;


        wait for rd_clk_period * 1053.2;
        startReading <= '1';
        wait for rd_clk_period * 4;
        startReading <= '0';
        wait for rd_clk_period * 2;
        startReading <= '1';
        wait for rd_clk_period * 2;
        startReading <= '0';


        wait until full = '1';
        startReading <= '1';

        -- if dout /= x"12345604_12345603_12345602_12345601_12345600_02030405_12345678_deadbeef" then
        --     report(":( data comparison feiled") severity warning;
        -- end if;

        wait for 10 ms;
        test_runner_cleanup(runner);
        wait;
    end process;

    process
    begin
        wait for 1 ms;
        report("Test bench timed out") severity warning;
        test_runner_cleanup(runner);
        wait;
    end process;

    decideDUT: if MINE = '1' generate
        DUT: entity work.HIFIFO generic map
        (
                FIFO_WRITE_DEPTH     => FIFO_WRITE_DEPTH,
                WRITE_DATA_WIDTH     => WRITE_DATA_WIDTH,
                READ_DATA_WIDTH      => READ_DATA_WIDTH
            )
            port map(
                rst         => reset,
                wr_clk      => wr_clk,
                wr_en       => wr_en,
                din         => din,
                full        => full,
                set_header  => set_header,
                new_chunk   => new_chunk,

                rd_clk  => rd_clk,
                rd_en   => rd_en,
                dout    => dout,
                empty   => empty,
                rd_data_count => rd_data_count

            );

    else generate

        DUT: xpm_fifo_async
            generic map (
                -- Common module generics
                FIFO_MEMORY_TYPE        =>  "auto",
                FIFO_WRITE_DEPTH        =>  2048,
                CASCADE_HEIGHT          =>  0,
                RELATED_CLOCKS          =>  0,
                WRITE_DATA_WIDTH        =>  256,
                READ_MODE               =>  "std",
                FIFO_READ_LATENCY       =>  1,
                FULL_RESET_VALUE        =>  0,
                USE_ADV_FEATURES        =>  "0707",
                READ_DATA_WIDTH         =>  32,
                CDC_SYNC_STAGES         =>  2,
                WR_DATA_COUNT_WIDTH     =>  1,
                PROG_FULL_THRESH        =>  10,
                RD_DATA_COUNT_WIDTH     =>  1,
                PROG_EMPTY_THRESH       =>  10,
                DOUT_RESET_VALUE        =>  "0",
                ECC_MODE                =>  "no_ecc",
                SIM_ASSERT_CHK          =>  0,
                WAKEUP_TIME             =>  0
            )
            port map (

                sleep          => '0',-- in std_logic;
                rst            => reset,-- in std_logic;
                wr_clk         => wr_clk,-- in std_logic;
                wr_en          => wr_en,-- in std_logic;
                din            => din,-- in std_logic_vector(WRITE_DATA_WIDTH-1 downto 0);
                full           => full,-- out std_logic;
                prog_full      => open,-- out std_logic;
                wr_data_count  => open,-- out std_logic_vector(WR_DATA_COUNT_WIDTH-1 downto 0);
                overflow       => open,-- out std_logic;
                wr_rst_busy    => open,-- out std_logic;
                almost_full    => open,-- out std_logic;
                wr_ack         => open,-- out std_logic;
                rd_clk         => rd_clk,-- in std_logic;
                rd_en          => rd_en,-- in std_logic;
                dout           => dout,-- out std_logic_vector(READ_DATA_WIDTH-1 downto 0);
                empty          => empty,-- out std_logic;
                prog_empty     => open,-- out std_logic;
                rd_data_count  => open,-- out std_logic_vector(RD_DATA_COUNT_WIDTH-1 downto 0);
                underflow      => open,-- out std_logic;
                rd_rst_busy    => open,-- out std_logic;
                almost_empty   => open,-- out std_logic;
                data_valid     => open,-- out std_logic;
                injectsbiterr  => '0',-- in std_logic;
                injectdbiterr  => '0',-- in std_logic;
                sbiterr        => open,-- out std_logic;
                dbiterr        => open-- out std_logic
            );
    end generate;

end tb;-- HIFIFO_tb

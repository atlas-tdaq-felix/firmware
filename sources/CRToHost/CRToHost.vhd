--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Julia Narevicius
--!               Andrea Borga
--!               Enrico Gamberini
--!               Thei Wijnen
--!               Filiberto Bonini
--!               mtrovato
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  EDAQ WIS, Nikhef.
--! Engineer: juna, fschreud
--!
--! Create Date:    01/03/2015
--! Module Name:    CRFM
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library ieee, unisim;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    --use ieee.std_logic_unsigned.all;
    use UNISIM.VCOMPONENTS.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;
    use work.axi_stream_package.all;
library xpm;
    use xpm.vcomponents.all;

entity CRToHost is
    generic (
        NUMBER_OF_DESCRIPTORS   : integer := 3;
        NUMBER_OF_INTERRUPTS    : integer := 8;
        LINK_NUM                : integer := 1;
        LINK_CONFIG             : IntArray;
        --
        toHostTimeoutBitn       : integer := 16;
        STREAMS_TOHOST          : integer := 1;
        BLOCKSIZE               : integer := 1024;
        DATA_WIDTH              : integer := 256;
        FIRMWARE_MODE           : integer; --for clock selection
        USE_URAM                : boolean --Use Ultram in FIFOs for Versal (And Virtex Ultrascale+?)
    );
    port  (
        clk40   : in  std_logic;
        toHostFifo_wr_clk   : in std_logic;
        aclk_tohost : in std_logic;   --Clock for 32-bit AXIs interface
        aclk64_tohost : in std_logic; --Clock for 64-bit AXIs interface, 400MHz for 25Gb/s
        daq_reset : in std_logic;
        daq_fifo_flush: in std_logic;
        -----
        register_map_control               : in  register_map_control_type; --! configuration settings, 64 bit per EGROUP (7 EGROUPS total)
        register_map_xoff_monitor          : out register_map_xoff_monitor_type;
        register_map_crtohost_monitor      : out register_map_crtohost_monitor_type;
        interrupt_call                     : out std_logic_vector(NUMBER_OF_INTERRUPTS-1 downto 4);
        xoff_out                           : out std_logic_vector(LINK_NUM-1 downto 0);
        -- decoding side
        s_axis              : in  axis_32_2d_array_type(0 to LINK_NUM-1, 0 to STREAMS_TOHOST-1);
        s_axis_tready       : out axis_tready_2d_array_type(0 to LINK_NUM-1, 0 to STREAMS_TOHOST-1);
        s_axis_prog_empty   : in axis_tready_2d_array_type(0 to LINK_NUM-1, 0 to STREAMS_TOHOST-1);
        -- decoding side aux channel (BUSY and TTCToHost virtual elinks
        s_axis_aux              : in  axis_32_array_type(0 to 1);
        s_axis_aux_tready       : out axis_tready_array_type(0 to 1);
        s_axis_aux_prog_empty   : in axis_tready_array_type(0 to 1);
        -- AXI4 Stream 64b for 25G links
        s_axis64              : in  axis_64_array_type(0 to LINK_NUM-1);
        s_axis64_tready       : out axis_tready_array_type(0 to LINK_NUM-1);
        s_axis64_prog_empty   : in axis_tready_array_type(0 to LINK_NUM-1);

        -- wupper side
        dma_enable_in       : in std_logic_vector(NUMBER_OF_DESCRIPTORS-1 downto 0);
        toHostFifo_din      : out slv_array(0 to NUMBER_OF_DESCRIPTORS-2);
        toHostFifo_wr_en    : out std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
        toHostFifo_prog_full : in std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
        toHostFifo_rst      : out std_logic
    );
end CRToHost;

architecture Behavioral of CRToHost is
    --
    --signal cr_fifo_flush,cr_rst : std_logic;
    ----

    ---

    signal chFifo_dout_array : slv_array (0 to (LINK_NUM));
    signal chFifo_dvalid_array: std_logic_vector ((LINK_NUM) downto 0);
    ----
    ----

    signal chFifo_hasBlock_array: std_logic_vector((LINK_NUM) downto 0);

    signal chFifo_re_array  : std_logic_vector(LINK_NUM downto 0);


    signal thFMbusyOut_s : std_logic_vector ((LINK_NUM-1) downto 0);


    signal daq_reset_toHostFifo_wr_clk: std_logic;
    signal ch_prog_full: std_logic_vector(23 downto 0);
    signal ch_prog_full_40: std_logic_vector(23 downto 0);
    signal ch_prog_full_latched: std_logic_vector(23 downto 0);
    signal EpathIDs: array_11b(0 to LINK_NUM);
    signal Xoff_40: std_logic_vector(LINK_NUM-1 downto 0);

    function CHANNEL_FIFO_DEPTH(USE_URAM: boolean; FW_MODE: integer) return integer is
    begin
        if(USE_URAM or FW_MODE = FIRMWARE_MODE_FULL or FW_MODE = FIRMWARE_MODE_INTERLAKEN) then
            return 8*BLOCKSIZE;
        else
            return 4*BLOCKSIZE;
        end if;
    end function;

    function USE_URAM_FW_MODE(LINK_CONFIG: integer; USE_URAM: boolean) return boolean is
    begin
        if LINK_CONFIG = 1 then
            return false;
        else
            return USE_URAM;
        end if;
    end function;
begin


    ------------------------------------------------------------------------------------------
    -- Interrupts calls
    ------------------------------------------------------------------------------------------
    interrupt_call(4) <= '0'; --Reserved for ToHost4 available, added descriptor for DCS traffic.
    interrupt_call(5) <= or Xoff_40; --
    interrupt_call(6) <= '0'; --Connected in Wupper to BUSY change
    interrupt_call(7) <= or_reduce(toHostFifo_prog_full);

    ---
    --
    ------------------------------------------------------------------------------------------
    --
    -- to-Host data: Full Mode channels
    --
    ------------------------------------------------------------------------------------------
    thFMdataManagers:  for channel in 0 to (LINK_NUM-1) generate

        signal axis_ch: axis_32_array_type(0 to STREAMS_TOHOST-1);
        signal axis_tready_ch: axis_tready_array_type(0 to STREAMS_TOHOST-1);
        signal axis_prog_empty_ch: axis_tready_array_type(0 to STREAMS_TOHOST-1);


    begin

        g_axi_ch: for j in 0 to  STREAMS_TOHOST-1 generate
            axis_ch(j) <= s_axis(channel, j);
            s_axis_tready(channel,j) <= axis_tready_ch(j);
            axis_prog_empty_ch(j) <= s_axis_prog_empty(channel, j);
        end generate;

        thFMdmN: entity work.CRToHostdm
            generic map(
                FMCHid => channel,
                toHostTimeoutBitn => toHostTimeoutBitn,
                STREAMS_TOHOST => STREAMS_TOHOST,
                BLOCKSIZE => BLOCKSIZE,
                DATA_WIDTH => DATA_WIDTH,
                LINK_CONFIG => LINK_CONFIG(channel),
                USE_URAM => USE_URAM_FW_MODE(LINK_CONFIG(0), USE_URAM),
                FIFO_DEPTH => CHANNEL_FIFO_DEPTH(USE_URAM, FIRMWARE_MODE),
                ACLK_FREQ => TOHOST_AXI_STREAM_FREQ(FIRMWARE_MODE)
            )
            port map(
                toHostFifo_wr_clk => toHostFifo_wr_clk,
                clk40 => clk40,
                aclk => aclk_tohost,
                aclk64 => aclk64_tohost,
                daq_reset => daq_reset,
                daq_fifo_flush => daq_fifo_flush,
                register_map_control => register_map_control,
                s_axis => axis_ch,
                s_axis_tready => axis_tready_ch,
                s_axis_prog_empty => axis_prog_empty_ch,
                s_axis64 => s_axis64(channel),
                s_axis64_tready => s_axis64_tready(channel),
                s_axis64_prog_empty => s_axis64_prog_empty(channel),
                chFifo_re => chFifo_re_array(channel+1),
                chFifo_dout => chFifo_dout_array(channel+1)(DATA_WIDTH-1 downto 0),
                chFifo_dvalid => chFifo_dvalid_array(channel+1),
                chFifo_hasBlock => chFifo_hasBlock_array(channel+1),
                chXoffout => Xoff_40(channel),
                chHighThreshCrossed => register_map_xoff_monitor.XOFF_FM_HIGH_THRESH.CROSSED(channel),
                chHighThreshCrossedLatch => register_map_xoff_monitor.XOFF_FM_HIGH_THRESH.CROSS_LATCHED(channel+24),
                chLowThreshCrossed => register_map_xoff_monitor.XOFF_FM_LOW_THRESH_CROSSED(channel),
                AXIS_ID_out => EpathIDs(channel+1),
                ch_prog_full_out => ch_prog_full(channel)
            );

        sync_tobusyOut : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => s_axis(channel,0).tuser(2),
                dest_clk => clk40,
                dest_out => thFMbusyOut_s(channel)
            );
    end generate thFMdataManagers;


    -- XOFF monitoring
    xoff_monitoring_generate: for CH in 0 to LINK_NUM-1 generate
        signal xoff_or_soft_xoff : std_logic;
    begin

        xoff_or_soft_xoff <= (Xoff_40(CH) and register_map_control.XOFF_ENABLE(CH)) or register_map_control.XOFF_FM_SOFT_XOFF(CH); --Apply manual XOFF for monitoring, it's applied separately in the FromHost path as well for actual distribution.
        xoff_out(CH) <= xoff_or_soft_xoff;
        xoff_monitoring: entity work.XoffMonitoring
            generic map (
                DURATION_COUNTER_WIDTH => 64,
                XOFF_COUNTER_WIDTH => 64
            )
            port map (
                clk => clk40,
                reset => daq_reset,
                xoff => xoff_or_soft_xoff,
                peak_duration => register_map_xoff_monitor.XOFF_PEAK_DURATION(CH),
                total_duration => register_map_xoff_monitor.XOFF_TOTAL_DURATION(CH),
                xoff_count => register_map_xoff_monitor.XOFF_COUNT(CH)
            );
    end generate;

    -------------------------------------------------------------------------------------------------------
    -- Aux channel servicing s_axis_aux for TTCToHost (index 0) and BusyToHost (index 1) Virtual E-Links --
    -------------------------------------------------------------------------------------------------------

    thFMdmAux: entity work.CRToHostdm
        generic map(
            FMCHid => 24,
            toHostTimeoutBitn => toHostTimeoutBitn,
            STREAMS_TOHOST => 2,
            BLOCKSIZE => BLOCKSIZE,
            DATA_WIDTH => DATA_WIDTH,
            LINK_CONFIG => 0,
            USE_URAM => USE_URAM_FW_MODE(LINK_CONFIG(0), USE_URAM),
            FIFO_DEPTH => CHANNEL_FIFO_DEPTH(USE_URAM, FIRMWARE_MODE),
            ACLK_FREQ => TOHOST_AXI_STREAM_FREQ(FIRMWARE_MODE)
        )
        port map(
            toHostFifo_wr_clk => toHostFifo_wr_clk,
            clk40 => clk40,
            aclk => aclk_tohost,
            aclk64 => aclk64_tohost,
            daq_reset => daq_reset,
            daq_fifo_flush => daq_fifo_flush,
            register_map_control => register_map_control,
            s_axis => s_axis_aux,
            s_axis_tready => s_axis_aux_tready,
            s_axis_prog_empty => s_axis_aux_prog_empty,
            s_axis64 => axis_64_zero_c,
            s_axis64_tready => open,
            s_axis64_prog_empty => '1',
            chFifo_re => chFifo_re_array(0),
            chFifo_dout => chFifo_dout_array(0)(DATA_WIDTH-1 downto 0), -- @suppress "Incorrect array size in assignment: expected (<DATA_WIDTH>) but was (<256>)"
            chFifo_dvalid => chFifo_dvalid_array(0),
            chFifo_hasBlock => chFifo_hasBlock_array(0),
            chXoffout => open, -- out test purposes only, flag to a data source to stop transmitting
            chHighThreshCrossed => open,
            chHighThreshCrossedLatch => open,
            chLowThreshCrossed => open,
            AXIS_ID_out => EpathIDs(0),
            ch_prog_full_out => open
        );

    sync_cr_rst: xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 2,
            INIT => 1,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0
        )
        port map (
            src_rst => daq_reset,
            dest_clk => toHostFifo_wr_clk,
            dest_rst => daq_reset_toHostFifo_wr_clk
        );


    mux_block: block

        signal thch_sel, thMUXsel, thMUXsel_s : integer range 0 to LINK_NUM;
        signal thMUXdin_valid : std_logic := '0';
        signal mux_data_out : std_logic_vector(DATA_WIDTH-1 downto 0);
        signal mux_data_out_valid : std_logic;
        signal descriptor_select: integer range 0 to NUMBER_OF_DESCRIPTORS - 2;
        signal discard_block_s: std_logic;

    begin

        ------------------------------------------------------------
        -- writing to the cr OUT FIFO / reading from channel fifos
        ------------------------------------------------------------
        THPCIeM: entity work.CRToHostPCIeManager
            generic map(
                GBT_NUM => LINK_NUM + 1,
                DATA_WIDTH => DATA_WIDTH,
                BLOCKSIZE => BLOCKSIZE,
                NUMBER_OF_DESCRIPTORS_TOHOST => NUMBER_OF_DESCRIPTORS-1)
            port map(
                clk                  => toHostFifo_wr_clk,
                clk40                => clk40,
                rst                  => daq_reset_toHostFifo_wr_clk,   -- reset is deasserted after fifo flush!
                thch_hasBlock_array  => chFifo_hasBlock_array, -- in, 'block_ready' flags from GBTdms
                PCIe_prog_full_array => toHostFifo_prog_full,
                thch_sel             => thch_sel,      -- out, data mux select
                thch_re_array        => chFifo_re_array,-- out, enables reading from single fmchannel at a time
                output_select        => descriptor_select,
                discard_block        => discard_block_s,
                EpathIDs             => EpathIDs,
                dma_enable_in        => dma_enable_in(NUMBER_OF_DESCRIPTORS-2 downto 0),
                DISCARD_ON_DMA_DISABLE => register_map_control.DISCARD_DATA_FOR_DESCR.DMA_DISABLED(NUMBER_OF_DESCRIPTORS-2 downto 0),
                DISCARD_ON_PROG_FULL => register_map_control.DISCARD_DATA_FOR_DESCR.FIFO_FULL((NUMBER_OF_DESCRIPTORS-2)+8 downto 8),
                CRTOHOST_DMA_DESCRIPTOR_WR_EN      => register_map_control.CRTOHOST_DMA_DESCRIPTOR_1.WR_EN,
                CRTOHOST_DMA_DESCRIPTOR_DESCR      => register_map_control.CRTOHOST_DMA_DESCRIPTOR_1.DESCR,
                CRTOHOST_DMA_DESCRIPTOR_DESCR_READ => register_map_crtohost_monitor.CRTOHOST_DMA_DESCRIPTOR_2.DESCR_READ,
                CRTOHOST_DMA_DESCRIPTOR_AXIS_ID    => register_map_control.CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID
            );
        --
        process(toHostFifo_wr_clk) -- write to crOUTfifo 2 clk pipeline before data mux, aligned with data
        begin
            if rising_edge(toHostFifo_wr_clk) then
                thMUXsel_s      <= thch_sel;
                thMUXsel        <= thMUXsel_s;
            end if;
        end process;


        thMUXdin_valid <= or_reduce(chFifo_dvalid_array(LINK_NUM downto 0));

        --
        dataMUXn: entity work.MUXn (Behavioral)
            generic map(N=>LINK_NUM+1, DATA_WIDTH=>DATA_WIDTH)
            port map(
                clk              => toHostFifo_wr_clk,
                data             => chFifo_dout_array,
                data_valid       => thMUXdin_valid,
                sel              => thMUXsel,
                data_out         => mux_data_out,
                data_out_valid   => mux_data_out_valid
            );
        process(toHostFifo_wr_clk)
        begin
            if rising_edge(toHostFifo_wr_clk) then
                for desc in 0 to NUMBER_OF_DESCRIPTORS-2 loop
                    toHostFifo_din(desc)(DATA_WIDTH-1 downto 0) <= mux_data_out;
                end loop;
                toHostFifo_wr_en <= (others => '0');
                toHostFifo_wr_en(descriptor_select) <= mux_data_out_valid and not discard_block_s;
            end if;
        end process;


    end block;


    toHostFifo_rst <= daq_fifo_flush;


    busymon_proc: process(clk40, daq_reset)
    begin
        if daq_reset = '1' then
            register_map_xoff_monitor.FM_BUSY_CHANNEL_STATUS.BUSY_LATCHED <= (others => '0');
        elsif rising_edge(clk40) then
            for i in 0 to LINK_NUM-1 loop
                if(thFMbusyOut_s(i) = '1') then
                    register_map_xoff_monitor.FM_BUSY_CHANNEL_STATUS.BUSY_LATCHED(i+24) <= '1';
                end if;
                if(register_map_control.FM_BUSY_CHANNEL_STATUS.CLEAR_LATCH="1") then
                    register_map_xoff_monitor.FM_BUSY_CHANNEL_STATUS.BUSY_LATCHED(i+24) <= '0';
                end if;
                register_map_xoff_monitor.FM_BUSY_CHANNEL_STATUS.BUSY(i)             <= thFMbusyOut_s(i);
            end loop;

        end if;
    end process;





    ch_prog_full(23 downto LINK_NUM) <= (others => '0');
    register_map_crtohost_monitor.MAX_TIMEOUT(31 downto toHostTimeoutBitn)  <= (others => '0');
    register_map_crtohost_monitor.MAX_TIMEOUT(toHostTimeoutBitn-1 downto 0) <= (others => '1');
    register_map_crtohost_monitor.CRTOHOST_FIFO_STATUS.FULL <= ch_prog_full;
    register_map_crtohost_monitor.CRTOHOST_FIFO_STATUS.FULL_LATCHED      <= ch_prog_full_latched;

    sync_ch_prog_full : xpm_cdc_array_single
        generic map (
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0,
            WIDTH => LINK_NUM
        )
        port map (
            src_clk => '0',
            src_in => ch_prog_full(LINK_NUM-1 downto 0),
            dest_clk => clk40,
            dest_out => ch_prog_full_40(LINK_NUM-1 downto 0)
        );

    fifo_status_latch_proc: process(clk40)
    begin
        if rising_edge(clk40) then
            for i in 0 to 23 loop
                if ch_prog_full_40(i) = '1' then
                    ch_prog_full_latched(i) <= '1';
                end if;
            end loop;
            if register_map_control.CRTOHOST_FIFO_STATUS.CLEAR="1" or daq_reset = '1' then
                ch_prog_full_latched <= (others => '0');
            end if;
        end if;
    end process;

end Behavioral;


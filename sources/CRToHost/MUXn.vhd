--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Julia Narevicius
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.
--! Engineer: juna
--!
--! Create Date:    14/09/2016
--! Module Name:    MUXn_d256b
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library ieee;
    use ieee.std_logic_1164.ALL;
    use ieee.numeric_std.all;
    use work.pcie_package.all;

--! MUX nx1, 256 bit data
entity MUXn is
    generic (
        N           : integer := 8;
        DATA_WIDTH  : integer
    );
    port (
        clk            : in  std_logic;
        data           : in  slv_array(0 to (N-1));
        data_valid     : in  std_logic;
        sel            : in  integer range 0 to N-1;
        data_out       : out std_logic_vector(DATA_WIDTH-1 downto 0);
        data_out_valid : out std_logic
    );
end MUXn;

architecture Behavioral of MUXn is

    signal data_out_valid_s : std_logic := '0';

begin

    --------
    --------
    process(clk)
    begin
        if clk'event and clk = '1' then
            --
            if sel > (N-1) then
                data_out <= (others=>'0');
            else
                data_out <= data( sel )(DATA_WIDTH-1 downto 0); -- @suppress "Incorrect array size in assignment: expected (<DATA_WIDTH>) but was (<256>)"
            end if;
        --
        end if;
    end process;

    --------
    --------
    process(clk) -- mux output ready, alligned with data
    begin
        if clk'event and clk = '1' then
            data_out_valid_s  <= data_valid;
        end if;
    end process;
    --------
    --------
    data_out_valid <= data_out_valid_s;


end Behavioral;






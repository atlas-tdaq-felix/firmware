--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Julia Narevicius
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  EDAQ WIS., Nikhef
--! Engineer: juna, fschreud
--!
--! Create Date:    22/12/2017
--! Module Name:    ReMuxN
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity ReMuxN is
    Generic(
        N : integer
    );
    Port ( clk      : in std_logic;
        rst      : in std_logic;
        sel      : in integer range 0 to N;
        trigON   : in std_logic;
        trigOFF  : in std_logic;
        re       : out std_logic;
        reNbit   : out std_logic_vector (N-1 downto 0)
    );
end ReMuxN;

architecture Behavioral of ReMuxN is

    signal re_s : std_logic := '0';
    signal reNbit_s : std_logic_vector (N-1 downto 0) := (others=>'0');

begin

    ---
    process(clk, rst)
    begin
        if rst = '1' then
            re_s <= '0';
        elsif clk'event and clk = '1' then
            if trigOFF = '1' then
                re_s <= '0';
            elsif trigON = '1' then
                re_s <= '1';
            end if;
        end if;
    end process;
    --
    re <= re_s;
    --

    bitsel:  for I in 0 to N-1 generate
        ---
        process(clk,rst)
        begin
            if rst = '1' then
                reNbit_s(I) <= '0';
            elsif clk'event and clk = '1' then
                if trigOFF = '1' then
                    reNbit_s(I) <= '0';
                elsif trigON = '1' and (sel = I) then
                    reNbit_s(I) <= '1';
                end if;
            end if;
        end process;
    --
    end generate bitsel;
    --
    reNbit <= reNbit_s;
--


end Behavioral;

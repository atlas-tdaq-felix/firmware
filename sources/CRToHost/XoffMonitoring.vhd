--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  University of Wuppertal
--! Engineer: mwensing
--!
--! Create Date:    10/06/2020
--! Module Name:    XoffMonitoring
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

entity XoffMonitoring is
    generic (
        DURATION_COUNTER_WIDTH : integer := 64;
        XOFF_COUNTER_WIDTH : integer := 32
    );
    port (
        clk : in std_logic;
        reset : in std_logic;

        xoff : in std_logic;

        peak_duration : out std_logic_vector(DURATION_COUNTER_WIDTH-1 downto 0);
        total_duration : out std_logic_vector(DURATION_COUNTER_WIDTH-1 downto 0);
        xoff_count : out std_logic_vector(XOFF_COUNTER_WIDTH-1 downto 0)
    );
end XoffMonitoring;

architecture rtl of XoffMonitoring is
    -- synchronisation & edge detection
    constant XOFF_SYNC_STAGES : integer := 2;
    signal xoff_sr : std_logic_vector(XOFF_SYNC_STAGES-1 downto 0) := "00";
    signal xoff_assert : std_logic := '0';
    signal xoff_deassert : std_logic := '0';
    attribute ASYNC_REG : string;
    attribute ASYNC_REG of xoff_sr : signal is "true";

    -- internal registers
    signal total_duration_i : unsigned(DURATION_COUNTER_WIDTH-1 downto 0) := (others => '0');
    signal peak_duration_i : unsigned(DURATION_COUNTER_WIDTH-1 downto 0) := (others => '0');
    signal duration_count : unsigned(DURATION_COUNTER_WIDTH-1 downto 0) := (others => '0');
    signal xoff_count_i : unsigned(XOFF_COUNTER_WIDTH-1 downto 0) := (others => '0');
begin

    -- small shift register for edge-detection
    process (clk, reset) begin
        if reset = '1' then
            xoff_sr <= (others => '0');
        else
            if rising_edge(clk) then
                xoff_sr <= xoff_sr(xoff_sr'left-1 downto 0) & xoff;
            end if;
        end if;
    end process;

    -- edge detection
    xoff_assert <= not xoff_sr(xoff_sr'left) and xoff_sr(xoff_sr'left-1);
    xoff_deassert <= xoff_sr(xoff_sr'left) and not xoff_sr(xoff_sr'left-1);

    process (clk, reset) begin
        if reset = '1' then
            xoff_count_i <= (others => '0');
            duration_count <= (others => '0');
            total_duration_i <= (others => '0');
            peak_duration_i <= (others => '0');
        else
            if rising_edge(clk) then
                -- count an xoff when it is de-asserted
                if xoff_deassert = '1' then
                    total_duration_i <= total_duration_i + duration_count;
                    xoff_count_i <= xoff_count_i + 1;
                end if;

                -- update peak duration always
                if duration_count > peak_duration_i then
                    peak_duration_i <= duration_count;
                end if;

                -- reset duration counter when xoff assertion is detected
                if xoff_assert = '1' then
                    duration_count <= (DURATION_COUNTER_WIDTH-1 downto 1 => '0') & '1';      -- start counter with 1
                elsif xoff_sr(xoff_sr'left-1) = '1' then
                    duration_count <= duration_count + 1;
                end if;
            end if;
        end if;
    end process;

    -- output counters
    peak_duration <= std_logic_vector(peak_duration_i);
    total_duration <= std_logic_vector(total_duration_i);
    xoff_count <= std_logic_vector(xoff_count_i);

end architecture;

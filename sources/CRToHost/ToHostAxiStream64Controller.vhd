--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  Nikhef
--! Engineer: Frans Schreuder
--!
--! Create Date:    26/03/2021
--! Module Name:    ToHostAxiStream64Controller
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use work.axi_stream_package.all;

entity ToHostAxiStream64Controller is
    generic (
        FMCHid                  : integer := 0;
        toHostTimeoutBitn       : integer := 8;
        BLOCKSIZE           : integer := 1024
    );
    port (
        aclk        : in  std_logic;
        daq_reset   : in  std_logic;

        s_axis_in            : in axis_64_type;
        s_axis_tready_out    : out std_logic;
        s_axis_prog_empty_in : in std_logic;

        timeOutEna_i    : in  std_logic;
        timeCnt_max     : in  std_logic_vector ((toHostTimeoutBitn-1) downto 0);
        prog_full_in    : in  std_logic;-- in, downstream wm fifo is full or main , can't happen in normal operation
        ----------
        word64out         : out std_logic_vector (63 downto 0); -- to chFIFO
        word64out_valid   : out std_logic;
        end_of_block      : out std_logic;
        -- Header FIFO signals:
        new_chunk           : out std_logic;
        set_chunk_header    : out std_logic
    );
end ToHostAxiStream64Controller;
--
architecture Behavioral of ToHostAxiStream64Controller is

    signal s_axis_tready_s , s_axis_tready_toblock_s: std_logic;
    constant NUMBER_OF_WORDS_PER_BLOCK: integer := BLOCKSIZE/8;

    constant timeout_trailer    : std_logic_vector(15 downto 0) := x"A000"; -- "101"=timeout, "00"=no truncation & no cerr, '0', 10 bit length is zero;

    signal reset, rst_p1 : std_logic := '1';


    signal timeOutCnt    :  std_logic_vector ((toHostTimeoutBitn-1) downto 0); -- @ clk240 domain

    signal s_axis_timeout_cnt : std_logic_vector(1 downto 0);
    signal timeout_pulse: std_logic;

    signal start_block: std_logic;
    signal block_busy: std_logic;
    signal timeout_mechanism_running: std_logic;
    signal create_trailer: std_logic;
    signal create_zero_trailer: std_logic;

    signal block_count_s : integer range 0 to NUMBER_OF_WORDS_PER_BLOCK-1;
begin


    s_axis_tready_out <=    s_axis_tready_s;-- and not s_axis_tid_changed;

    reset_proc: process(aclk)
    begin
        if rising_edge(aclk) then
            rst_p1 <= daq_reset;
            if rst_p1 = '1' then
                reset <= '1';
            else
                reset <= '0';
            end if;
        end if;
    end process;

    --This process counts a 40MHz timeout counter, but in the 400MHz domain.
    timeout_cnt: process(aclk)
        variable cnt10 : std_logic_vector(3 downto 0);
        variable timeOutEna_i_v : std_logic;
    begin
        if rising_edge(aclk) then
            if(reset = '1') then
                cnt10 := (others => '0');
                timeOutCnt <= (others => '0');
                timeout_pulse <= '0';
                timeOutEna_i_v := '0';
            else
                timeout_pulse <= '0';
                if (cnt10 < 9) then
                    cnt10 := cnt10 + 1;
                else
                    cnt10 := (others => '0');
                    if timeOutCnt < timeCnt_max then
                        timeOutCnt <= timeOutCnt + 1;
                    else
                        timeOutCnt <= (others => '0');
                        timeout_pulse <= timeOutEna_i_v;
                    end if;
                end if;
                timeOutEna_i_v := timeOutEna_i;
            end if;
        end if;
    end process;

    timeout_pr: process(aclk)
    begin
        if rising_edge(aclk) then
            if reset = '1' then
                s_axis_timeout_cnt <= "00";
            else
                if s_axis_prog_empty_in = '1' and s_axis_in.tvalid = '1' and timeout_pulse = '1' then
                    s_axis_timeout_cnt <= s_axis_timeout_cnt(0) & '1'; --shift a '1' in, if MSB is 1, it will generate a timeout on this axi stream link
                end if;
                if s_axis_tready_s = '1' then
                    s_axis_timeout_cnt <= (others => '0');
                end if;

            end if;
        end if;
    end process;


    s_axis_tready_s <= (s_axis_tready_toblock_s and not prog_full_in) and start_block;

    start_block <= ((not s_axis_prog_empty_in) and s_axis_in.tvalid) or s_axis_timeout_cnt(1) or block_busy;

    s_axis_tready_toblock_s <= '0' when create_trailer = '1' or --Insert trailer
                                 create_zero_trailer = '1' or
                                 timeout_mechanism_running = '1' or
                                 block_busy = '0' else '1';


    toblock: process(aclk)
        variable wordOUT_v : std_logic_vector(63 downto 0) := x"0000_0000_0000_0000";
        variable wordout_valid_v : std_logic;
        variable blockCounter : integer range 0 to NUMBER_OF_WORDS_PER_BLOCK-1;  --counts the number of 32 bit words that form a block.
        variable blockSequence : std_logic_vector(4 downto 0); --counts the block sequence in the block header
        variable chunkCounter : std_logic_vector (15 downto 0);  --counts the length in the chunk, both for trailer generation and truncation

        variable create_timeout_trailer : std_logic;
        variable trunc: std_logic;
        variable trailerType : std_logic_vector(2 downto 0);
        variable first_subchunk, last_subchunk, last_subchunk_next : std_logic;
        variable CRCErrorFlag, ChunkErrorFlag, BUSYFlag: std_logic;
        variable trailer: std_logic_vector(31 downto 0);
        variable header: std_logic_vector(31 downto 0);
        variable insertHeader : std_logic := '0';
        variable insertTrailer : std_logic := '0';
    begin
        if rising_edge (aclk) then
            if(reset = '1') then
                blockCounter := 0;
                blockSequence := "00000";
                chunkCounter := x"0000";
                create_trailer <= '0';
                create_timeout_trailer := '0';
                first_subchunk := '1';
                last_subchunk := '0';
                trunc := '0';
                wordOUT_v := x"0000_0000_0000_0000";
                wordout_valid_v := '0';
                word64out <= x"0000_0000_0000_0000";
                word64out_valid <= '0';
                CRCErrorFlag := '0';
                ChunkErrorFlag := '0';
                BUSYFlag := '0';
                block_busy <= '0';
                end_of_block <= '0';
                timeout_mechanism_running <= '0';
                create_zero_trailer <= '0';
                block_count_s <= 0;
                new_chunk <= '0';
                set_chunk_header <= '0';
            else
                wordout_valid_v := '0';
                insertTrailer := '0';
                timeout_mechanism_running <= '0';

                if(s_axis_in.tvalid = '1' and s_axis_tready_s = '1') then
                    if(s_axis_in.tuser(0) = '1') then -- Check for CRC error flag in tuser
                        CRCErrorFlag := '1';
                    end if;

                    if(s_axis_in.tuser(1) = '1') then -- Check for chunk error flag in tuser
                        ChunkErrorFlag := '1';
                    end if;

                    if(s_axis_in.tuser(2) = '1') then -- Check for BUSY flag in tuser
                        BUSYFlag := '1';
                    end if;

                    if(s_axis_in.tuser(3) = '1') then -- truncation mechanism
                        trunc := '1';
                        create_trailer <= '1';
                    end if;
                end if;

                if(blockCounter > 1 and s_axis_in.tvalid = '0' and create_trailer = '0') then
                    create_timeout_trailer := '1'; --this signal will be latched and timeout (0000 or 0xA0XX) will be created
                end if;

                if(prog_full_in = '0') then    --When the fifo is full, we pause and don't do anything
                    end_of_block <= '0';
                    insertHeader := '0';
                    if(blockCounter = (NUMBER_OF_WORDS_PER_BLOCK-2)) then
                        create_trailer <= '1';
                        -- Very special case in which the second to last word is a trailer.
                        -- Adding a zero trailer (0x00000000) prevents bruh moments because it's a padding trailer with length 0.
                        if create_trailer = '1' then --last word was already a trailer, so we have to padd zero's.
                            create_zero_trailer <= '1';
                        end if;
                    end if;
                    if (s_axis_in.tvalid = '1' and s_axis_in.tlast = '1' and s_axis_tready_toblock_s = '1') then
                        create_trailer <= '1';
                        last_subchunk_next := '1';
                    end if;
                    if(blockCounter = (NUMBER_OF_WORDS_PER_BLOCK-1) and create_timeout_trailer = '0') then --end of block, always create trailer.
                        end_of_block <= '1';
                    end if;

                    if(start_block = '1') then
                        if(blockCounter = 0) then -- create block header, record incoming data

                            header :=    "11"&std_logic_vector(to_unsigned((BLOCKSIZE/1024)-1,6))&
                                      x"CF" &
                                      blockSequence &  --first 16 bits of block header are 0xXXCE for 32b trailer where XX is 2 bits "11" and 6 bits blocksize in kB.
                                      std_logic_vector(to_unsigned(FMCHid, 5))&
                                      "000000";
                            blockSequence := blockSequence + 1;
                            block_busy <= '1';
                            insertHeader := '1';
                            chunkCounter := x"0000";
                        end if;

                        if(create_trailer = '1' ) then
                            trailerType := ((not first_subchunk) and (not last_subchunk) and (not trunc)) &
                                           (last_subchunk or trunc) &
                                           first_subchunk;

                            trailer := trailerType & trunc & ChunkErrorFlag & CRCErrorFlag & BUSYFlag & "0" & x"00" & chunkCounter(15 downto 0);
                            chunkCounter := x"0000";
                            ChunkErrorFlag := '0';
                            CRCErrorFlag := '0';
                            BUSYFlag := '0';
                            trunc := '0';
                            create_trailer <= '0';
                            insertTrailer := '1';
                            create_timeout_trailer := '0'; --Timeout occurred at the moment we created a trailer, ignore it and wait for the next timeout.
                            first_subchunk := last_subchunk;
                            last_subchunk := '0';

                        elsif ( create_timeout_trailer = '1') then -- implement timout mechanism
                            timeout_mechanism_running <= '1';
                            create_trailer <= '0';
                            insertTrailer := '1';
                            if(blockCounter = (NUMBER_OF_WORDS_PER_BLOCK-1)) then --end of block, we need to stop sending 0 trailers and add the 0xA0XX timeout trailer, then go back to idle
                                trailer := timeout_trailer(15 downto 10) & "00" & x"00" & chunkCounter(15 downto 0);
                                chunkCounter := x"0000";            --chunk counter is reset here
                                create_timeout_trailer := '0';  --back to idle operation
                                first_subchunk := '0';
                                last_subchunk := '0';
                                end_of_block <= '1';
                            else
                                if  (first_subchunk = '1' or  last_subchunk = '1') and (chunkCounter /= x"0000") then
                                    last_subchunk := '0';
                                    first_subchunk := '0';
                                    trailer := timeout_trailer(15 downto 13)&"100" & "00" & x"00" & chunkCounter(15 downto 0);
                                    chunkCounter := x"0000";            --chunk counter is reset here, as we added a timeout + truncation to end an incomplete chunk

                                else  --No end of block, and no data coming in. Fill timeout frame with 0x0000
                                    trailer := x"00000000"; --Insert zero padding / zero trailer
                                    last_subchunk := '0';
                                    first_subchunk := '0';
                                    chunkCounter := chunkCounter + 8;
                                    last_subchunk := '0';
                                end if;
                            end if;
                        end if;

                        -- Reserve a space in the fifo for the chunk header if this is the first word of the chunk.
                        if chunkCounter = 0 and insertHeader = '0' and insertTrailer = '0' and create_zero_trailer = '0' then
                            new_chunk <= '1';
                        else
                            new_chunk <= '0';
                        end if;

                        set_chunk_header <= '0';
                        if insertHeader = '1' then --Trailer or header in lower 32b. Optionally padd with data.
                            wordOUT_v(31 downto 0) := header;
                            wordOUT_v(63 downto 32) := (others => '0');
                            wordout_valid_v := '1';
                        elsif insertTrailer = '1' then --Zero trailer and trailer
                            wordOUT_v(31 downto 0) := x"0000_0000";
                            wordOUT_v(63 downto 32) := trailer;
                            wordout_valid_v := '1';
                            set_chunk_header <= '1';
                        elsif create_zero_trailer = '1' then
                            create_zero_trailer <= '0';
                            wordOUT_v := (others => '0');
                            wordout_valid_v := '1';
                        else --64 bits of data
                            if(s_axis_in.tvalid = '1' and (s_axis_tready_s = '1')) then
                                wordOUT_v := s_axis_in.tdata;
                                chunkCounter := chunkCounter + 8;
                                wordout_valid_v := '1';
                            end if;
                        end if;
                    end if;
                end if;



                if(wordout_valid_v = '1') then
                    if(blockCounter = (NUMBER_OF_WORDS_PER_BLOCK-1)) then
                        blockCounter := 0;
                        block_busy <= '0';
                    else
                        blockCounter := blockCounter + 1;  --increment blockCounter as we are pushing into the fifo.
                    end if;
                end if;

                word64out <= wordOUT_v; --output data to port.
                word64out_valid <= wordout_valid_v;
                if last_subchunk_next = '1' then
                    last_subchunk := '1';
                    last_subchunk_next := '0';
                end if;

                block_count_s <= blockCounter;

            end if; --reset
        end if;
    end process;

end Behavioral;


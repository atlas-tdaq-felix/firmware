--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Julia Narevicius
--!               Enrico Gamberini
--!               Thei Wijnen
--!               mtrovato
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.
--! Engineer: juna
--!
--! Create Date:    12/09/2016
--! Module Name:    thFMdm
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library IEEE;
    use IEEE.std_logic_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use work.pcie_package.all;
    use work.axi_stream_package.all;
    use work.FELIX_package.all;

library xpm;
    use xpm.vcomponents.all;
--! to-Host centralRouter logic
entity CRToHostdm is
    generic(
        FMCHid            : integer  := 0;
        toHostTimeoutBitn : integer  := 8;
        STREAMS_TOHOST    : integer  := 1;
        BLOCKSIZE         : integer  := 1024;
        DATA_WIDTH        : integer  := 256;
        LINK_CONFIG       : integer  := 0;
        USE_URAM          : boolean;
        FIFO_DEPTH        : positive := BLOCKSIZE / 2;
        ACLK_FREQ         : integer     --Used to compensate the timeout counter to run at 40MHz (BC)
    );
    port(
        toHostFifo_wr_clk        : in  std_logic;
        clk40                    : in  std_logic;
        aclk                     : in  std_logic;
        aclk64                   : in  std_logic; --AXI clock for 64-bit interface
        daq_reset                : in  std_logic;
        daq_fifo_flush           : in  std_logic;
        register_map_control     : in  register_map_control_type; --! configuration settings, 64 bit per EGROUP (7 EGROUPS total)
        -- RX side
        s_axis                   : in  axis_32_array_type(0 to STREAMS_TOHOST - 1);
        s_axis_tready            : out axis_tready_array_type(0 to STREAMS_TOHOST - 1);
        s_axis_prog_empty        : in  axis_tready_array_type(0 to STREAMS_TOHOST - 1);

        --In case of a 25Gb/s link we use 64b axi stream
        s_axis64                 : in  axis_64_type;
        s_axis64_tready          : out std_logic;
        s_axis64_prog_empty      : in  std_logic;
        -- wupper side:
        -- 1. read from specified channel
        chFifo_re                : in  std_logic;
        chFifo_dout              : out std_logic_vector(DATA_WIDTH - 1 downto 0);
        chFifo_dvalid            : out std_logic;
        chFifo_hasBlock          : out std_logic; -- out, 'block_ready' flag
        chXoffout                : out std_logic; -- out test purposes only, flag to a data source to stop transmitting
        chHighThreshCrossed      : out std_logic;
        chHighThreshCrossedLatch : out std_logic;
        chLowThreshCrossed       : out std_logic;
        AXIS_ID_out              : out std_logic_vector(10 downto 0);
        --fmchXoffin          : in  std_logic; -- in, crOUTfifo is pfull 4060/3830 (out of 4096), stop writing flag
        --
        ch_prog_full_out         : out std_logic
    );
end CRToHostdm;

architecture Behavioral of CRToHostdm is

    --
    function AXIS_WIDTH(IS64 : integer) return integer is
    begin
        if IS64 = 1 then
            return 64;
        else
            return 32;
        end if;
    end function;

    signal aclk_s                                                                       : std_logic; --Connected to either aclk or aclk64, depending on generics.
    signal timeCnt_max                                                                  : std_logic_vector((toHostTimeoutBitn - 1) downto 0);
    signal timeCnt_ena                                                                  : std_logic;
    --
    signal ch_prog_full, chFIFO_din_valid, chFIFO_pfull_r1, chFIFO_pfull, chFIFO_pempty : std_logic;

    signal chFIFO_din    : std_logic_vector(AXIS_WIDTH(LINK_CONFIG) - 1 downto 0);
    signal chFIFO_dout_s : std_logic_vector(DATA_WIDTH - 1 downto 0);

    signal chFIFO_din_s       : std_logic_vector(AXIS_WIDTH(LINK_CONFIG) - 1 downto 0);
    signal chFIFO_din_valid_s : std_logic;

    signal chFIFO_empty : std_logic;


    --
    --signal xoff_fm_ch_fifo_thresh_low: std_logic_vector(3 downto 0);
    --signal xoff_fm_ch_fifo_thresh_high: std_logic_vector(3 downto 0);
    constant FIFO_RD_COUNT_WIDTH            : natural := f_log2(FIFO_DEPTH * AXIS_WIDTH(LINK_CONFIG) / DATA_WIDTH - 1) + 1; --f_log2((FIFO_DEPTH / (DATA_WIDTH / 32)) - 1) +1;
    constant FIFO_WR_COUNT_WIDTH            : natural := f_log2(FIFO_DEPTH - 1) + 1;
    signal wr_data_count                    : std_logic_vector(FIFO_WR_COUNT_WIDTH - 1 downto 0);
    constant PROG_EMPTY_THRESH              : natural := (BLOCKSIZE / (DATA_WIDTH / 8)) - 1;
    signal rd_data_count                    : std_logic_vector(FIFO_RD_COUNT_WIDTH - 1 downto 0);
    signal timeCnt_max_aclk                 : std_logic_vector(toHostTimeoutBitn - 1 downto 0);
    signal timeCnt_ena_aclk                 : std_logic;
    signal fmchFifo_flush_aclk              : std_logic;
    signal fmchFifo_flush_toHostFifo_wr_clk : std_logic;
    signal daq_reset_aclk                   : std_logic;
    signal wr_data_count_40                 : std_logic_vector(3 downto 0);

    signal xoff_threshold_low  : std_logic_vector(3 downto 0);
    signal xoff_threshold_high : std_logic_vector(3 downto 0);
    signal xoff_clear_latch    : std_logic;

    signal new_chunk        : std_logic;
    signal set_chunk_header : std_logic;


begin
    ------------------------------------------------------------
    -- configuration registers map
    ------------------------------------------------------------
    timeCnt_max <= register_map_control.TIMEOUT_CTRL.TIMEOUT((toHostTimeoutBitn - 1) downto 0);
    timeCnt_ena <= to_sl(register_map_control.TIMEOUT_CTRL.ENABLE);



    ------------------------------------------------------------
    --  AXI4 channel stream controller
    ------------------------------------------------------------
    g_32b : if LINK_CONFIG = 0 generate
        signal instant_timeout_enable_aclk : std_logic_vector(STREAMS_TOHOST - 1 downto 0);
    begin

        xpm_cdc_timeCnt_ena : xpm_cdc_single
            generic map(
                DEST_SYNC_FF   => 2,
                INIT_SYNC_FF   => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG  => 0
            )
            port map(
                src_clk  => '0',
                src_in   => timeCnt_ena,
                dest_clk => aclk,
                dest_out => timeCnt_ena_aclk
            );

        xpm_cdc_array_single_inst_timeCnt_max : xpm_cdc_array_single
            generic map(
                DEST_SYNC_FF   => 2,
                INIT_SYNC_FF   => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG  => 0,
                WIDTH          => toHostTimeoutBitn
            )
            port map(
                src_clk  => '0',
                src_in   => timeCnt_max,
                dest_clk => aclk,
                dest_out => timeCnt_max_aclk
            );

        g_24links : if FMCHid < 24 generate
            xpm_cdc_array_single_inst_instant_timeout_enable : xpm_cdc_array_single
                generic map(
                    DEST_SYNC_FF   => 2,
                    INIT_SYNC_FF   => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG  => 0,
                    WIDTH          => STREAMS_TOHOST
                )
                port map(
                    src_clk  => '0',
                    src_in   => register_map_control.CRTOHOST_INSTANT_TIMEOUT_ENA(FMCHid)(STREAMS_TOHOST - 1 downto 0),
                    dest_clk => aclk,
                    dest_out => instant_timeout_enable_aclk
                );
        else generate
            instant_timeout_enable_aclk <= (others => '0');
        end generate;

        sync_aresetn : xpm_cdc_sync_rst
            generic map(
                DEST_SYNC_FF   => 2,
                INIT           => 1,
                INIT_SYNC_FF   => 0,
                SIM_ASSERT_CHK => 0
            )
            port map(
                src_rst  => daq_reset,
                dest_clk => aclk,
                dest_rst => daq_reset_aclk
            );

        chStreamController : entity work.ToHostAxiStreamController
            generic map(
                FMCHid            => FMCHid,
                toHostTimeoutBitn => toHostTimeoutBitn,
                BLOCKSIZE         => BLOCKSIZE,
                STREAMS_TOHOST    => STREAMS_TOHOST,
                ACLK_FREQ         => ACLK_FREQ
            )
            port map(
                aclk                   => aclk_s,
                daq_reset              => daq_reset_aclk,
                s_axis_in              => s_axis,
                s_axis_tready_out      => s_axis_tready,
                s_axis_prog_empty_in   => s_axis_prog_empty,
                timeOutEna_i           => timeCnt_ena_aclk,
                timeCnt_max            => timeCnt_max_aclk,
                instant_timeout_enable => instant_timeout_enable_aclk,
                prog_full_in           => ch_prog_full, -- in, downstream wm fifo is full or main , can't happen in normal operation
                word32out              => chFIFO_din, -- to chFIFO
                word32out_valid        => chFIFO_din_valid, -- to chFIFO
                end_of_block           => open,
                new_chunk              => new_chunk,
                set_chunk_header       => set_chunk_header
            );
        aclk_s          <= aclk;
        s_axis64_tready <= '0';
    end generate;

    g_64b : if LINK_CONFIG = 1 generate
        aclk_s        <= aclk64;
        s_axis_tready <= (others => '0');

        xpm_cdc_timeCnt_ena : xpm_cdc_single
            generic map(
                DEST_SYNC_FF   => 2,
                INIT_SYNC_FF   => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG  => 0
            )
            port map(
                src_clk  => '0',
                src_in   => timeCnt_ena,
                dest_clk => aclk64,
                dest_out => timeCnt_ena_aclk
            );

        xpm_cdc_array_single_inst_timeCnt_max : xpm_cdc_array_single
            generic map(
                DEST_SYNC_FF   => 2,
                INIT_SYNC_FF   => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG  => 0,
                WIDTH          => toHostTimeoutBitn
            )
            port map(
                src_clk  => '0',
                src_in   => timeCnt_max,
                dest_clk => aclk64,
                dest_out => timeCnt_max_aclk
            );

        sync_aresetn : xpm_cdc_sync_rst
            generic map(
                DEST_SYNC_FF   => 2,
                INIT           => 1,
                INIT_SYNC_FF   => 0,
                SIM_ASSERT_CHK => 0
            )
            port map(
                src_rst  => daq_reset,
                dest_clk => aclk64,
                dest_rst => daq_reset_aclk
            );

        chStreamController : entity work.ToHostAxiStream64Controller
            generic map(
                FMCHid            => FMCHid,
                toHostTimeoutBitn => toHostTimeoutBitn,
                BLOCKSIZE         => BLOCKSIZE
            )
            port map(
                aclk                 => aclk_s, -- If this is aclk64 the simulation doesn't work due to dumb delta things.
                daq_reset            => daq_reset_aclk,
                s_axis_in            => s_axis64,
                s_axis_tready_out    => s_axis64_tready,
                s_axis_prog_empty_in => s_axis64_prog_empty,
                timeOutEna_i         => timeCnt_ena_aclk,
                timeCnt_max          => timeCnt_max_aclk,
                prog_full_in         => ch_prog_full, -- in, downstream wm fifo is full or main , can't happen in normal operation
                word64out            => chFIFO_din, -- to chFIFO
                word64out_valid      => chFIFO_din_valid, -- to chFIFO
                end_of_block         => open,
                new_chunk            => new_chunk,
                set_chunk_header     => set_chunk_header
            );

    end generate;


    process(aclk_s)
    begin
        if rising_edge(aclk_s) then
            chFIFO_pfull_r1 <= chFIFO_pfull;
        end if;
    end process;
    ch_prog_full <= chFIFO_pfull or chFIFO_pfull_r1; -- or fmchXoffin;

    --
    ------------------------------------------------------------
    --  full mode channel FIFO
    ------------------------------------------------------------
    chFIFO_din_s       <= chFIFO_din;
    chFIFO_din_valid_s <= chFIFO_din_valid;

    chHiFifo_block : block
        signal chFIFO_almost_empty: std_logic;
        function FIFO_MEMORY_TYPE(use_uram: boolean) return string is
        begin
            if use_uram then
                return "ultra";
            else
                return "block";
            end if;
        end function;

        signal chMatchFifo_wr_en: std_logic;
        constant INTERMEDIATE_WIDTH: integer := DATA_WIDTH/8; --64b for 512b, 128b for 1024b
        --Ultraram has a maximum output width of 64b, so for 512b we will need 8 ultraram primitives, for 1024b we need 16.
        --With the intermediate width of either 64b or 128b, a depth of 32768 will result in exactly 8 or 16 ultraram instances per "match fifo"
        impure function MATCH_FIFO_DEPTH return integer is
        begin
            if not USE_URAM then
                return 4096;
            else
                return 32768;
            end if;
        end function;

        signal chMatchFifo_din: std_logic_vector(INTERMEDIATE_WIDTH-1 downto 0);
        --constant chHIFIFO_RD_COUNT_WIDTH            : natural := f_log2(FIFO_DEPTH * AXIS_WIDTH(LINK_CONFIG) / INTERMEDIATE_WIDTH - 1) + 1; --f_log2((FIFO_DEPTH / (DATA_WIDTH / 32)) - 1) +1;
        constant chMatchFIFO_RD_COUNT_WIDTH         : natural := f_log2(MATCH_FIFO_DEPTH * INTERMEDIATE_WIDTH / DATA_WIDTH - 1) + 1;
        constant chMatchFIFO_WR_COUNT_WIDTH         : natural := f_log2(MATCH_FIFO_DEPTH - 1) + 1;
        signal chHIFIFO_empty: std_logic;
        signal chMathFifo_full: std_logic;
    begin
        chHIFIFO : entity work.HIFIFO
            generic map(
                -- Common module generics
                USE_URAM => USE_URAM,
                -- CASCADE_HEIGHT           : integer  := 0;       -- ???
                FIFO_WRITE_DEPTH => FIFO_DEPTH,
                WRITE_DATA_WIDTH => AXIS_WIDTH(LINK_CONFIG),
                READ_DATA_WIDTH  => INTERMEDIATE_WIDTH,
                PROG_FULL_THRESH => FIFO_DEPTH - 67
            )
            port map(
                rst           => fmchFifo_flush_aclk,

                wr_clk        => aclk_s,
                wr_en         => chFIFO_din_valid_s,
                din           => chFIFO_din_s,
                full          => open,
                prog_full     => chFIFO_pfull,
                set_header    => set_chunk_header,
                new_chunk     => new_chunk,
                wr_data_count => open,

                rd_en         => chMatchFifo_wr_en,
                dout          => chMatchFifo_din,
                empty         => chHIFIFO_empty,
                almost_empty  => open,
                rd_data_count => open

            );

        chMatchFifo_wr_en <= (not chHIFIFO_empty) and (not chMathFifo_full);
        --For ultraram we have to use a sync fifo, and so we must make sure that aclk_s and tohostfifo_wr_clk are the same
        g_uram: if USE_URAM generate
            chMatchingFifo: xpm_fifo_sync
                generic map( -- @suppress "Generic map uses default values. Missing optional actuals: EN_SIM_ASSERT_ERR"
                    FIFO_MEMORY_TYPE => FIFO_MEMORY_TYPE(USE_URAM),
                    FIFO_WRITE_DEPTH => MATCH_FIFO_DEPTH,
                    CASCADE_HEIGHT => 0,
                    WRITE_DATA_WIDTH => INTERMEDIATE_WIDTH,
                    READ_MODE => "fwft",
                    FIFO_READ_LATENCY => 2,
                    FULL_RESET_VALUE => 1,
                    USE_ADV_FEATURES => "0604", --9: prog_empty flag 2: wr_data_count, 10: rd_data_count
                    READ_DATA_WIDTH => DATA_WIDTH,
                    WR_DATA_COUNT_WIDTH => chMatchFIFO_WR_COUNT_WIDTH,
                    PROG_FULL_THRESH => 12,
                    RD_DATA_COUNT_WIDTH => chMatchFIFO_RD_COUNT_WIDTH,
                    PROG_EMPTY_THRESH => (BLOCKSIZE*8)/DATA_WIDTH-1,
                    DOUT_RESET_VALUE => "0",
                    ECC_MODE => "no_ecc",
                    SIM_ASSERT_CHK => 0,
                    WAKEUP_TIME => 0
                )
                port map(
                    sleep => '0',
                    rst => fmchFifo_flush_toHostFifo_wr_clk,
                    wr_clk => toHostFifo_wr_clk,
                    wr_en => chMatchFifo_wr_en,
                    din => chMatchFifo_din,
                    rd_en => chFifo_re,
                    injectsbiterr => '0',
                    injectdbiterr => '0',
                    dout => chFIFO_dout_s,
                    full => chMathFifo_full,
                    prog_full => open,
                    wr_data_count => open,
                    overflow => open,
                    wr_rst_busy => open,
                    almost_full => open,
                    wr_ack => open,
                    empty => chFIFO_empty,
                    prog_empty => chFIFO_pempty,
                    rd_data_count => open,
                    underflow => open,
                    rd_rst_busy => open,
                    almost_empty => open,
                    data_valid => open,
                    sbiterr => open,
                    dbiterr => open
                );
        else generate
            --For block ram we can use an async fifo, and so aclk_s and tohostfifo_wr_clk may be different
            chMatchingFifo: xpm_fifo_async
                generic map( -- @suppress "Generic map uses default values. Missing optional actuals: EN_SIM_ASSERT_ERR"
                    FIFO_MEMORY_TYPE => FIFO_MEMORY_TYPE(USE_URAM),
                    FIFO_WRITE_DEPTH => MATCH_FIFO_DEPTH,
                    CASCADE_HEIGHT => 0,
                    RELATED_CLOCKS => 0,
                    WRITE_DATA_WIDTH => INTERMEDIATE_WIDTH,
                    READ_MODE => "fwft",
                    FIFO_READ_LATENCY => 2,
                    FULL_RESET_VALUE => 1,
                    USE_ADV_FEATURES => "0604", --9: prog_empty flag 2: wr_data_count, 10: rd_data_count
                    READ_DATA_WIDTH => DATA_WIDTH,
                    CDC_SYNC_STAGES => 2,
                    WR_DATA_COUNT_WIDTH => chMatchFIFO_WR_COUNT_WIDTH,
                    PROG_FULL_THRESH => 12,
                    RD_DATA_COUNT_WIDTH => chMatchFIFO_RD_COUNT_WIDTH,
                    PROG_EMPTY_THRESH => (BLOCKSIZE*8)/DATA_WIDTH-1,
                    DOUT_RESET_VALUE => "0",
                    ECC_MODE => "no_ecc",
                    SIM_ASSERT_CHK => 0,
                    WAKEUP_TIME => 0
                )
                port map(
                    sleep => '0',
                    rst => fmchFifo_flush_aclk,
                    wr_clk => aclk_s,
                    wr_en => chMatchFifo_wr_en,
                    din => chMatchFifo_din,
                    rd_en => chFifo_re,
                    injectsbiterr => '0',
                    injectdbiterr => '0',
                    dout => chFIFO_dout_s,
                    full => chMathFifo_full,
                    prog_full => open,
                    wr_data_count => open,
                    overflow => open,
                    wr_rst_busy => open,
                    almost_full => open,
                    wr_ack => open,
                    empty => chFIFO_empty,
                    prog_empty => chFIFO_pempty,
                    rd_data_count => open,
                    underflow => open,
                    rd_rst_busy => open,
                    almost_empty => open,
                    data_valid => open,
                    sbiterr => open,
                    dbiterr => open,
                    rd_clk => toHostFifo_wr_clk
                );
        end generate;

        valid_proc : process(toHostFifo_wr_clk)
        begin
            if rising_edge(toHostFifo_wr_clk) then
                chFifo_dvalid <= chFifo_re and not chFIFO_empty;
                if chFifo_re = '1' then
                    chFifo_dout <= chFIFO_dout_s; --Turn FWFT into STD fifo again.
                end if;
            end if;
        end process;

        AXIS_ID_out <= chFIFO_dout_s(10 downto 0);

    end block;


    xoff_threshold_low  <= register_map_control.XOFF_FM_CH_FIFO_THRESH_LOW;
    xoff_threshold_high <= register_map_control.XOFF_FM_CH_FIFO_THRESH_HIGH;
    xoff_clear_latch    <= to_sl(register_map_control.XOFF_FM_HIGH_THRESH.CLEAR_LATCH);

    xpm_cdc_gray_wr_data_count : xpm_cdc_gray
        generic map(
            DEST_SYNC_FF          => 2,
            INIT_SYNC_FF          => 0,
            REG_OUTPUT            => 1,
            SIM_ASSERT_CHK        => 0,
            SIM_LOSSLESS_GRAY_CHK => 0,
            WIDTH                 => 4
        )
        port map(
            src_clk      => aclk_s,
            src_in_bin   => wr_data_count(FIFO_WR_COUNT_WIDTH - 1 downto FIFO_WR_COUNT_WIDTH - 4),
            dest_clk     => clk40,
            dest_out_bin => wr_data_count_40
        );


    xoff_thresholds : process(clk40)    -- fifo write clock
    begin
        if rising_edge(clk40) then
            if (wr_data_count_40 > xoff_threshold_high) then
                chXoffout                <= '1';
                chHighThreshCrossed      <= '1';
                chHighThreshCrossedLatch <= '1';
            else
                chHighThreshCrossed <= '0';
            end if;
            if (xoff_clear_latch = '1') then
                chHighThreshCrossedLatch <= '0';
            end if;
            if (wr_data_count_40 < xoff_threshold_low) then
                chXoffout          <= '0';
                chLowThreshCrossed <= '0';
            else
                chLowThreshCrossed <= '1';
            end if;
        end if;
    end process;





    --
    chFifo_hasBlock <= (not chFIFO_pempty) and (not fmchFifo_flush_toHostFifo_wr_clk); -- chFIFO_pempty is '0' when there is at least one whole block in the fifo

    sync_fmchFifo_flush1 : xpm_cdc_sync_rst
        generic map(
            DEST_SYNC_FF   => 2,
            INIT           => 0,
            INIT_SYNC_FF   => 0,
            SIM_ASSERT_CHK => 0
        )
        port map(
            src_rst  => daq_fifo_flush,
            dest_clk => aclk_s,
            dest_rst => fmchFifo_flush_aclk
        );

    sync_fmchFifo_flush2 : xpm_cdc_sync_rst
        generic map(
            DEST_SYNC_FF   => 2,
            INIT           => 0,
            INIT_SYNC_FF   => 0,
            SIM_ASSERT_CHK => 0
        )
        port map(
            src_rst  => daq_fifo_flush,
            dest_clk => toHostFifo_wr_clk,
            dest_rst => fmchFifo_flush_toHostFifo_wr_clk
        );
    --
    ch_prog_full_out <= ch_prog_full or fmchFifo_flush_aclk;
--


end Behavioral;


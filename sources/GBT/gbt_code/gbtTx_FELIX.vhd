--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               Kai Chen
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen    (kchen@bnl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2014/12/05 04:43:14 PM
-- Design Name: GBT Tx Top
-- Module Name: gbt_tx_top - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions: Vivado
-- Description:
--              The TX TOP MODULE FOR FELIX GBT
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.FELIX_gbt_package.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity gbtTx_FELIX is
    generic
    (
        channel                   : integer   := 0
    );
    Port
    (
        TX_FLAG                   : out std_logic;
        TX_RESET_I                : in  std_logic;
        TX_FRAMECLK_I           : in std_logic;
        TX_WORDCLK_I               : in std_logic;
        --Tx_latopt_scr             : in std_logic;
        --TX_LATOPT_TC              : in std_logic;
        TX_TC_METHOD              : in std_logic;
        TC_EDGE                   : in std_logic;
        DATA_MODE_CFG             : in std_logic_vector(1 downto 0);

        TX_TC_DLY_VALUE           : in std_logic_vector(2 downto 0);

        TX_HEADER_I               : in std_logic_vector(3 downto 0);
        --TX_ISDATA_SEL_I        : in  std_logic;
        TX_DATA_84b_I            : in  std_logic_vector(83 downto 0);
        TX_EXTRA_DATA_WIDEBUS_I   : in  std_logic_vector(31 downto 0);

        TX_DATA_20b_O              : out std_logic_vector(19 downto 0)
    );
end gbtTx_FELIX;

architecture Behavioral of gbtTx_FELIX is


    signal Scrambler_Enable       : std_logic;
    signal Tx_Align_Signal        : std_logic;
    --signal Tx_latopt_tc_i         : std_logic;
    signal TX_HEADER              : std_logic_vector(3 downto 0);
    signal TxExtraWidebus32b      : std_logic_vector(31 downto 0);
    signal TxCommon84b            : std_logic_vector(83 downto 0);
    signal TxFrame120b            : std_logic_vector(119 downto 0);

begin

    TX_FLAG               <= Scrambler_Enable;
    -- Scrambler, clock changed, ctrl signal added.
    --Tx_latopt_tc_i        <= '1' when TX_DLY_SW_CTRL='1' else
    --                         TX_LATOPT_TC;
    FelixScrambler: entity work.gbt_tx_scrambler_FELIX
        --generic map
        --(
        --  channel                                   => channel
        --  )
        port map (
            --CTRL
            TX_TC_METHOD => TX_TC_METHOD,
            Scrambler_Enable => Scrambler_Enable,
            Tx_Align_Signal => Tx_Align_Signal,
            --TX_LATOPT_TC => Tx_latopt_tc_i, -- Tx_latopt_tc,
            TC_EDGE => TC_EDGE,
            TX_TC_DLY_VALUE => TX_TC_DLY_VALUE,
            TX_WORDCLK_I => TX_WORDCLK_I,
            TX_RESET_I => TX_RESET_I,
            TX_FRAMECLK_I => TX_FRAMECLK_I,
            TX_HEADER_I => TX_HEADER_I,
            TX_HEADER_O => TX_HEADER,
            TX_DATA_I => TX_DATA_84b_I,
            TX_COMMON_FRAME_O => TxCommon84b,
            TX_EXTRA_DATA_WIDEBUS_I => TX_EXTRA_DATA_WIDEBUS_I,
            TX_EXTRA_FRAME_WIDEBUS_O => TxExtraWidebus32b
        );



    -- Encoder
    encoder: entity work.gbt_tx_encoder_FELIX
        port map (
            DATA_MODE_CFG => DATA_MODE_CFG,
            TX_HEADER_I => TX_HEADER,
            TX_COMMON_FRAME_I => TxCommon84b,
            TX_EXTRA_FRAME_WIDEBUS_I => TxExtraWidebus32b,
            TX_FRAME_O => TxFrame120b
        );

    ---- TxGearBox, control signal added,
    ---- The timing crossing is deleted. A new robust one is moved before scrambler.
    ---- Gearbox signal is latched, without latency added for GBT-FRAME Mode.

    FELIXTxGearbox: entity work.gbt_tx_gearbox_FELIX
        generic map
    (
            channel                                   => channel
        )
        port map (
            Scrambler_Enable_o => Scrambler_Enable,
            Tx_Align_Signal => Tx_Align_Signal,
            TX_LATOPT_SCR => '1', --TX_LATOPT_SCR,
            --TX_RESET_I => TX_RESET_I,
            TX_WORDCLK_I => TX_WORDCLK_I,
            --TX_FRAMECLK_I => TX_FRAMECLK_I,
            TX_FRAME_I => TxFrame120b,
            TX_WORD_OO => TX_DATA_20b_O
        );

end Behavioral;


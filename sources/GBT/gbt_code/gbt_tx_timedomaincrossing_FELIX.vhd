--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               Kai Chen
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen    (kchen@bnl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2014/12/05 04:43:14 PM
-- Design Name: GBT Tx Time domain crossing Top
-- Module Name: gbt_tx_timedomaincrossing_FELIX - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions: Vivado
-- Description:
--              The TX TIME DOMAIN CROSSING MODULE for GBT
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------

library ieee;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.FELIX_gbt_package.all;
library xpm;
    use xpm.vcomponents.all;

entity gbt_tx_timedomaincrossing_FELIX is
    --  generic
    --      (
    --        channel                   : integer := 0
    --      );
    port
    (
        Tx_Align_Signal           : out std_logic;
        TX_TC_METHOD              : in std_logic;
        --TX_LATOPT_TC              : in  std_logic;

        TC_EDGE                   : in std_logic;

        TX_TC_DLY_VALUE           : in std_logic_vector(2 downto 0);

        TX_WORDCLK_I              : in  std_logic;
        TX_RESET_I                : in  std_logic;
        TX_FRAMECLK_I             : in  std_logic

    --TX_ISDATA_SEL_I         : in  std_logic;

    );
end gbt_tx_timedomaincrossing_FELIX;


architecture Behavior of gbt_tx_timedomaincrossing_FELIX is

    signal TX_FRAMECLK_I_sync     : std_logic;
    --signal fsm_rst                : std_logic := '0';
    signal TX_FRAMECLK_I_4r       : std_logic;
    signal TX_FRAMECLK_I_5r       : std_logic;
    signal TX_FRAMECLK_I_2r       : std_logic;
    signal TX_FRAMECLK_I_r        : std_logic;
    signal cnt                    : std_logic_vector(2 downto 0) :="000";
    signal TX_TC_DLY_VALUE_i      : std_logic_vector(2 downto 0) :="000";
    signal TX_RESET_FLAG_clr      : std_logic;
    signal TX_RESET_FLAG          : std_logic;
    signal TX_RESET_r             : std_logic;
    signal TX_RESET_2r            : std_logic;
    signal pulse_rising           : std_logic;
    --signal pulse_rising_r         : std_logic;
    signal pulse_falling_r        : std_logic;
    signal pulse_falling          : std_logic;


begin
    xpm_cdc_single_inst : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 6,   -- 1 40 MHz clock cycle is 6 240 MHz cycles
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map (
            src_clk => '0', -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in => TX_FRAMECLK_I, -- 1-bit input: Input signal to be synchronized to dest_clk domain.
            dest_clk => TX_WORDCLK_I, -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => TX_FRAMECLK_I_sync -- 1-bit output: src_in synchronized to the destination clock domain. This output
        );
    -----------------------------------------------------------------------------
    -- Alignment signal for TX GearBox
    -----------------------------------------------------------------------------

    TX_TC_DLY_VALUE_i     <= TX_TC_DLY_VALUE when TX_DLY_SW_CTRL='1' else TX_TC_DLY_VALUE_package;

    process(TX_WORDCLK_I)
    begin
        if TX_WORDCLK_I'event and TX_WORDCLK_I='0' then
            TX_FRAMECLK_I_4r  <= TX_FRAMECLK_I_sync;
            TX_FRAMECLK_I_5r  <= TX_FRAMECLK_I_4r;
        end if;
    end process;

    process(TX_WORDCLK_I)
    begin
        if TX_WORDCLK_I'event and TX_WORDCLK_I='1' then
            TX_FRAMECLK_I_r   <= TX_FRAMECLK_I_sync;
            TX_FRAMECLK_I_2r  <= TX_FRAMECLK_I_r;
            TX_RESET_r        <= TX_RESET_I;
            TX_RESET_2r       <= TX_RESET_r;
            if TX_RESET_r='1' and TX_RESET_2r='0' then
                TX_RESET_FLAG   <='1';
            elsif  TX_RESET_FLAG_clr='1' then
                TX_RESET_FLAG   <='0';
            end if;
            pulse_rising      <= TX_FRAMECLK_I_r and (not TX_FRAMECLK_I_2r);
            pulse_falling     <= TX_FRAMECLK_I_4r and (not TX_FRAMECLK_I_5r);
            --pulse_rising_r    <= pulse_rising;
            pulse_falling_r   <= pulse_falling;
            if (TX_TC_METHOD='1' or TX_RESET_FLAG='1') then
                if TC_EDGE='0' then
                    TX_RESET_FLAG_clr     <= pulse_rising;
                else
                    TX_RESET_FLAG_clr     <= pulse_falling_r;
                end if;
            else
                TX_RESET_FLAG_clr       <= '0';
            end if;
        end if;
    end process;

    process(TX_WORDCLK_I)
    begin
        if TX_WORDCLK_I'event and TX_WORDCLK_I='1' then
            if TX_RESET_FLAG_clr='1' then
                cnt     <= TX_TC_DLY_VALUE_i;
                if TX_TC_DLY_VALUE_i = "011" then
                    Tx_Align_Signal       <= '1';
                else
                    Tx_Align_Signal       <= '0';
                end if;
            else
                case cnt is
                    when "000" =>
                        cnt                 <= "001";
                        --fsm_rst             <= '0';
                        Tx_Align_Signal     <= '0';
                    -- tx_frameclk_i_shifted <='1';
                    when "001" =>
                        cnt                 <= "010";
                        --fsm_rst             <= '0';
                        Tx_Align_Signal     <= '0';
                    -- tx_frameclk_i_shifted <='1';
                    when "010" =>
                        cnt                 <= "011";
                        --fsm_rst             <= '0';
                        Tx_Align_Signal     <= '1';
                    -- tx_frameclk_i_shifted <='0';
                    when "011" =>
                        cnt                 <= "100";
                        --fsm_rst             <= '0';
                        Tx_Align_Signal     <= '0';
                    -- tx_frameclk_i_shifted <='0';
                    when "100" =>
                        cnt                 <= "101";
                        --fsm_rst             <= '0';
                        Tx_Align_Signal     <= '0';
                    -- tx_frameclk_i_shifted <='0';
                    when "101" =>
                        cnt <="000";
                        --fsm_rst             <= '0';
                        Tx_Align_Signal     <= '0';
                    -- tx_frameclk_i_shifted <= '1';
                    when others =>
                        cnt                 <= "101";
                        --fsm_rst             <= '1';
                        Tx_Align_Signal     <= '0';
                end case;
            end if;
        end if;
    end process;




--=====================================================================================--
end Behavior;

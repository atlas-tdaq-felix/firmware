--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               Israel Grayzman
--!               RHabraken
--!               Kai Chen
--!               Shelfali Saxena
--!               mtrovato
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen    (kchen@bnl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2014/12/05 04:43:14 PM
-- Design Name: FELIX GBT Wrapper
-- Module Name: gbt_top - Behavioral
-- Project Name:
-- Target Devices: KCU / V7
-- Tool Versions: Vivado
-- Description:
--              The TOP MODULE FOR FELIX GBT & GTH
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------

LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std_unsigned.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
    use UNISIM.VComponents.all;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;
    use ieee.numeric_std.all;
library xpm;
    use xpm.vcomponents.all;

entity FELIX_gbt_wrapper is
    Generic (
        CARD_TYPE                   : integer := 709;
        GBT_NUM                     : integer := 24;
        GTHREFCLK_SEL               : std_logic; --GREFCLK   : std_logic := '1';
        --MGTREFCLK : std_logic := '0';
        PLL_SEL                     : std_logic;       -- CPLL : '0'
        -- QPLL : '1'
        GTREFCLKS                   : integer := 1
    );
    Port (
        rst_hw                      : in std_logic;

        register_map_control        : in register_map_control_type;
        register_map_link_monitor    : out register_map_link_monitor_type;

        GTREFCLK_N_IN   : in std_logic_vector(GTREFCLKS-1 downto 0);
        GTREFCLK_P_IN   : in std_logic_vector(GTREFCLKS-1 downto 0);

        clk40_in                    : in std_logic;
        -- for CentralRouter
        TX_120b_in                  : in  array_120b(0 to GBT_NUM-1);
        RX_120b_out                 : out array_120b(0 to GBT_NUM-1);
        FRAME_LOCKED_O              : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_FRAME_CLK_I              : in std_logic_vector(GBT_NUM-1 downto 0);

        -- GTH Data pins
        TX_P                        : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_N                        : out std_logic_vector(GBT_NUM-1 downto 0);
        RX_P                        : in  std_logic_vector(GBT_NUM-1 downto 0);
        RX_N                        : in  std_logic_vector(GBT_NUM-1 downto 0);
        RXUSRCLK_OUT                : out std_logic_vector(GBT_NUM-1 downto 0)
    );
end FELIX_gbt_wrapper;

architecture Behavioral of FELIX_gbt_wrapper is

    signal RxSlide_c      : STD_LOGIC_VECTOR(47 downto 0);
    signal RxSlide_i      : std_logic_vector(47 downto 0);
    signal gttx_reset     : std_logic_vector(47 downto 0);

    signal gtrx_reset     : std_logic_vector(47 downto 0);
    signal soft_reset     : std_logic_vector(47 downto 0);
    signal cpll_reset     : std_logic_vector(47 downto 0);
    signal qpll_reset     : std_logic_vector(11 downto 0);
    signal txresetdone    : std_logic_vector(47 downto 0);


    signal rxresetdone    : std_logic_vector(47 downto 0);
    signal cpllfbclklost  : std_logic_vector(47 downto 0);
    signal cplllock       : std_logic_vector(47 downto 0);
    signal rxcdrlock,RxCdrLock_int      : std_logic_vector(47 downto 0);
    signal qplllock       : std_logic_vector(11 downto 0);
    signal cdr_cnt        : std_logic_vector(19 downto 0);

    signal TX_RESET       : std_logic_vector(47 downto 0);
    signal TX_RESET_i     : std_logic_vector(47 downto 0);

    signal RX_RESET       : std_logic_vector(47 downto 0);
    signal RX_RESET_i     : std_logic_vector(47 downto 0);
    signal GT_TXUSRCLK    : std_logic_vector(47 downto 0);
    signal GT_RXUSRCLK    : std_logic_vector(47 downto 0);
    signal RX_FLAG_Oi     : std_logic_vector(47 downto 0);
    signal outsel_i       : std_logic_vector(47 downto 0);
    signal outsel_ii      : std_logic_vector(47 downto 0);
    signal outsel_o       : std_logic_vector(47 downto 0);
    signal RX_120b_out_i  : array_120b(0 to (GBT_NUM-1));
    signal RX_120b_out_ii : array_120b(0 to (GBT_NUM-1));

    signal rx_is_header   : std_logic_vector(47 downto 0);
    signal alignment_done : std_logic_vector(47 downto 0);
    --signal rx_is_data     : std_logic_vector(47 downto 0);
    signal RX_HEADER_FOUND: std_logic_vector(47 downto 0);

    signal RxSlide        : std_logic_vector(47 downto 0);

    signal GT_TX_WORD_CLK : std_logic_vector(47 downto 0);
    signal TX_TC_METHOD   : std_logic_vector(47 downto 0);
    signal TC_EDGE        : std_logic_vector(47 downto 0);

    type data20barray     is array (0 to 47) of std_logic_vector(19 downto 0);
    signal TX_DATA_20b    : data20barray := (others => ("00000000000000000000"));
    signal RX_DATA_20b    : data20barray := (others => ("00000000000000000000"));

    signal GT_RX_WORD_CLK         : std_logic_vector(47 downto 0);
    signal alignment_chk_rst_c    : std_logic_vector(47 downto 0);
    signal alignment_chk_rst_c1   : std_logic_vector(47 downto 0);
    signal alignment_chk_rst      : std_logic_vector(47 downto 0);
    signal alignment_chk_rst_f    : std_logic_vector(47 downto 0);

    signal alignment_chk_rst_i    : std_logic;
    signal DESMUX_USE_SW          : std_logic;
    SIGNAL DATA_TXFORMAT          : std_logic_vector(95 downto 0);
    signal DATA_TXFORMAT_i        : std_logic_vector(95 downto 0);
    SIGNAL DATA_RXFORMAT          : std_logic_vector(95 downto 0);
    signal DATA_RXFORMAT_i        : std_logic_vector(95 downto 0);

    signal General_ctrl           : std_logic_vector(63 downto 0);

    signal TXPMARESETDONE         : std_logic_vector(47 downto 0);
    signal RXPMARESETDONE         : std_logic_vector(47 downto 0);
    signal alignment_done_f       : std_logic_vector(47 downto 0);
    signal soft_reset_f           : std_logic_vector(47 downto 0);
    signal fifo_empty             : std_logic_vector(47 downto 0);
    signal userclk_rx_reset_in    : std_logic_vector(47 downto 0);
    signal userclk_tx_reset_in    : std_logic_vector(47 downto 0);
    signal rxcdrlock_a            : std_logic_vector(47 downto 0);

    SIGNAL Channel_disable        : std_logic_vector(63 downto 0);
    SIGNAL TX_TC_DLY_VALUE        : std_logic_vector(191 downto 0);

    signal GTH_RefClk             : std_logic_vector(47 downto 0);
    signal pulse_cnt              : std_logic_vector(29 downto 0);
    signal pulse_lg               : std_logic;

    signal GTREFCLK_IN           : std_logic_vector(GTREFCLKS-1 downto 0);

    signal alignment_done_chk_cnt : std_logic_vector(12 downto 0);
    signal alignment_done_a       : std_logic_vector(47 downto 0);
    signal fifo_rst               : std_logic_vector(47 downto 0);
    signal fifo_rden              : std_logic_vector(47 downto 0);
    signal error_orig             : std_logic_vector(47 downto 0);
    signal error_f                : std_logic_vector(47 downto 0);
    signal FSM_RST                : std_logic_vector(47 downto 0);
    signal auto_gth_rxrst         : std_logic_vector(47 downto 0);
    signal auto_gbt_rxrst         : std_logic_vector(47 downto 0);
    signal gtrx_reset_i           : std_logic_vector(47 downto 0);

    signal GT_RXOUTCLK            : std_logic_vector(47 downto 0);
    signal GT_TXOUTCLK            : std_logic_vector(47 downto 0);

    signal BITSLIP_MANUAL_r       : std_logic_vector(47 downto 0);
    signal BITSLIP_MANUAL_2r      : std_logic_vector(47 downto 0);
    type txrx80b_12ch_type        is array (11 downto 0) of std_logic_vector(79 downto 0);
    signal RX_DATA_80b            : txrx80b_12ch_type;
    signal TX_DATA_80b            : txrx80b_12ch_type;

    signal gttx_reset_merge       : std_logic_vector(11 downto 0);
    signal gtrx_reset_merge       : std_logic_vector(11 downto 0);
    signal rxcdrlock_out          : std_logic_vector(47 downto 0);

    signal RX_N_i                 : std_logic_vector(47 downto 0):=x"000000000000";
    signal RX_P_i                 : std_logic_vector(47 downto 0):=x"000000000000";
    signal TX_N_i                 : std_logic_vector(47 downto 0):=x"000000000000";
    signal TX_P_i                 : std_logic_vector(47 downto 0):=x"000000000000";

    signal drpclk_in              : std_logic_vector(0 downto 0);
    signal TXPOLARITY_TX_WORD_CLK : std_logic_vector(GBT_NUM-1 downto 0);
    signal RXPOLARITY_RX_WORD_CLK : std_logic_vector(GBT_NUM-1 downto 0);

begin

    FRAME_LOCKED_O <= alignment_done_f(GBT_NUM-1 downto 0);

    g_gtrefclks: for i in 0 to GTREFCLKS-1 generate
        g_KCU: if CARD_TYPE /= 709 and CARD_TYPE /= 710 generate
            ibufds_inst : IBUFDS_GTE3
                generic map(
                    REFCLK_EN_TX_PATH => '0',
                    REFCLK_HROW_CK_SEL => "00",
                    REFCLK_ICNTL_RX => "00"
                )
                port map(
                    O               =>    GTREFCLK_IN(i),
                    ODIV2           =>    open,
                    CEB             =>    '0',
                    I               =>    GTREFCLK_P_IN(i),
                    IB              =>    GTREFCLK_N_IN(i)
                );
        end generate;
        g_V7: if CARD_TYPE = 709 or CARD_TYPE = 710 generate
            ibufds_inst : IBUFDS_GTE2
                generic map(
                    CLKCM_CFG => true,
                    CLKRCV_TRST => true,
                    CLKSWING_CFG => "11"
                )
                port map(
                    O               =>    GTREFCLK_IN(i),
                    ODIV2           =>    open,
                    CEB             =>    '0',
                    I               =>    GTREFCLK_P_IN(i),
                    IB              =>    GTREFCLK_N_IN(i)
                );
        end generate;
    end generate;



    --FLX712 SLR0 banks: 126, 127, 128, 224, 225, 228
    --FLX712 SLR1 banks: 131, 132, 133, 231, 232, 233
    g_refclk_8ch: if (GBT_NUM <= 8) generate
        GTH_RefClk( 0)         <= GTREFCLK_IN(0);
        GTH_RefClk( 1)         <= GTREFCLK_IN(0);
        GTH_RefClk( 2)         <= GTREFCLK_IN(0);
        GTH_RefClk( 3)         <= GTREFCLK_IN(0);
        g_gt4: if GBT_NUM > 4 generate
            GTH_RefClk( 4)         <= GTREFCLK_IN(1);
            GTH_RefClk( 5)         <= GTREFCLK_IN(1);
            GTH_RefClk( 6)         <= GTREFCLK_IN(1);
            GTH_RefClk( 7)         <= GTREFCLK_IN(1);
        end generate;
    end generate g_refclk_8ch;

    g_refclk_24ch: if ((8 < GBT_NUM) and (GBT_NUM <= 24)) generate
        GTH_RefClk( 0)         <= GTREFCLK_IN(0);--bank 128 (up to  8 channels)
        GTH_RefClk( 1)         <= GTREFCLK_IN(0);--bank 128 (up to  8 channels)
        GTH_RefClk( 2)         <= GTREFCLK_IN(0);--bank 128 (up to  8 channels)
        GTH_RefClk( 3)         <= GTREFCLK_IN(0);--bank 128 (up to  8 channels)
        GTH_RefClk( 4)         <= GTREFCLK_IN(0);--bank 127 (up to 16 channels)
        GTH_RefClk( 5)         <= GTREFCLK_IN(0);--bank 127 (up to 16 channels)
        GTH_RefClk( 6)         <= GTREFCLK_IN(0);--bank 127 (up to 16 channels)
        GTH_RefClk( 7)         <= GTREFCLK_IN(0);--bank 127 (up to 16 channels)
        GTH_RefClk( 8)         <= GTREFCLK_IN(0);--bank 126 (up to 24 channels)
        GTH_RefClk( 9)         <= GTREFCLK_IN(0);--bank 126 (up to 24 channels)
        GTH_RefClk(10)         <= GTREFCLK_IN(0);--bank 126 (up to 24 channels)
        GTH_RefClk(11)         <= GTREFCLK_IN(0);--bank 126 (up to 24 channels)

        GTH_RefClk(12)         <= GTREFCLK_IN(1);--bank 133 (up to  8 channels)
        GTH_RefClk(13)         <= GTREFCLK_IN(1);--bank 133 (up to  8 channels)
        GTH_RefClk(14)         <= GTREFCLK_IN(1);--bank 133 (up to  8 channels)
        GTH_RefClk(15)         <= GTREFCLK_IN(1);--bank 133 (up to  8 channels)
        GTH_RefClk(16)         <= GTREFCLK_IN(1);--bank 132 (up to 16 channels)
        GTH_RefClk(17)         <= GTREFCLK_IN(1);--bank 132 (up to 16 channels)
        GTH_RefClk(18)         <= GTREFCLK_IN(1);--bank 132 (up to 16 channels)
        GTH_RefClk(19)         <= GTREFCLK_IN(1);--bank 132 (up to 16 channels)
        GTH_RefClk(20)         <= GTREFCLK_IN(1);--bank 131 (up to 24 channels)
        GTH_RefClk(21)         <= GTREFCLK_IN(1);--bank 131 (up to 24 channels)
        GTH_RefClk(22)         <= GTREFCLK_IN(1);--bank 131 (up to 24 channels)
        GTH_RefClk(23)         <= GTREFCLK_IN(1);--bank 131 (up to 24 channels)
    end generate g_refclk_24ch;

    g_refclk_48ch: if ((24 < GBT_NUM) and (GBT_NUM <= 48)) generate
        GTH_RefClk( 0)         <= GTREFCLK_IN(0);--bank 128 (up to  8 channels)
        GTH_RefClk( 1)         <= GTREFCLK_IN(0);--bank 128 (up to  8 channels)
        GTH_RefClk( 2)         <= GTREFCLK_IN(0);--bank 128 (up to  8 channels)
        GTH_RefClk( 3)         <= GTREFCLK_IN(0);--bank 128 (up to  8 channels)
        GTH_RefClk( 4)         <= GTREFCLK_IN(0);--bank 127 (up to 16 channels)
        GTH_RefClk( 5)         <= GTREFCLK_IN(0);--bank 127 (up to 16 channels)
        GTH_RefClk( 6)         <= GTREFCLK_IN(0);--bank 127 (up to 16 channels)
        GTH_RefClk( 7)         <= GTREFCLK_IN(0);--bank 127 (up to 16 channels)
        GTH_RefClk( 8)         <= GTREFCLK_IN(0);--bank 126 (up to 24 channels)
        GTH_RefClk( 9)         <= GTREFCLK_IN(0);--bank 126 (up to 24 channels)
        GTH_RefClk(10)         <= GTREFCLK_IN(0);--bank 126 (up to 24 channels)
        GTH_RefClk(11)         <= GTREFCLK_IN(0);--bank 126 (up to 24 channels)
        GTH_RefClk(12)         <= GTREFCLK_IN(3);--bank 228 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(13)         <= GTREFCLK_IN(3);--bank 228 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(14)         <= GTREFCLK_IN(3);--bank 228 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(15)         <= GTREFCLK_IN(3);--bank 228 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(16)         <= GTREFCLK_IN(4);--bank 224 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(17)         <= GTREFCLK_IN(4);--bank 224 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(18)         <= GTREFCLK_IN(4);--bank 224 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(19)         <= GTREFCLK_IN(4);--bank 224 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(20)         <= GTREFCLK_IN(4);--bank 225 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(21)         <= GTREFCLK_IN(4);--bank 225 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(22)         <= GTREFCLK_IN(4);--bank 225 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(23)         <= GTREFCLK_IN(4);--bank 225 (up to 48 channels) - BNL712 ONLY

        GTH_RefClk(24)         <= GTREFCLK_IN(1);--bank 133 (up to  8 channels)
        GTH_RefClk(25)         <= GTREFCLK_IN(1);--bank 133 (up to  8 channels)
        GTH_RefClk(26)         <= GTREFCLK_IN(1);--bank 133 (up to  8 channels)
        GTH_RefClk(27)         <= GTREFCLK_IN(1);--bank 133 (up to  8 channels)
        GTH_RefClk(28)         <= GTREFCLK_IN(1);--bank 132 (up to 16 channels)
        GTH_RefClk(29)         <= GTREFCLK_IN(1);--bank 132 (up to 16 channels)
        GTH_RefClk(30)         <= GTREFCLK_IN(1);--bank 132 (up to 16 channels)
        GTH_RefClk(31)         <= GTREFCLK_IN(1);--bank 132 (up to 16 channels)
        GTH_RefClk(32)         <= GTREFCLK_IN(1);--bank 131 (up to 24 channels)
        GTH_RefClk(33)         <= GTREFCLK_IN(1);--bank 131 (up to 24 channels)
        GTH_RefClk(34)         <= GTREFCLK_IN(1);--bank 131 (up to 24 channels)
        GTH_RefClk(35)         <= GTREFCLK_IN(1);--bank 131 (up to 24 channels)
        GTH_RefClk(36)         <= GTREFCLK_IN(2);--bank 231 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(37)         <= GTREFCLK_IN(2);--bank 231 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(38)         <= GTREFCLK_IN(2);--bank 231 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(39)         <= GTREFCLK_IN(2);--bank 231 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(40)         <= GTREFCLK_IN(2);--bank 232 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(41)         <= GTREFCLK_IN(2);--bank 232 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(42)         <= GTREFCLK_IN(2);--bank 232 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(43)         <= GTREFCLK_IN(2);--bank 232 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(44)         <= GTREFCLK_IN(2);--bank 233 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(45)         <= GTREFCLK_IN(2);--bank 233 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(46)         <= GTREFCLK_IN(2);--bank 233 (up to 48 channels) - BNL712 ONLY
        GTH_RefClk(47)         <= GTREFCLK_IN(2);--bank 233 (up to 48 channels) - BNL712 ONLY
    end generate g_refclk_48ch;

    General_ctrl                          <= register_map_control.GBT_GENERAL_CTRL;

    gttx_reset(47 downto 0)           <= register_map_control.GBT_GTTX_RESET(47 downto 0);
    gtrx_reset(47 downto 0)           <= register_map_control.GBT_GTRX_RESET(47 downto 0);
    soft_reset(47 downto 0)           <= register_map_control.GBT_SOFT_RESET(47 downto 0);
    cpll_reset(47 downto 0)           <= register_map_control.GBT_PLL_RESET.CPLL_RESET(47 downto 0);
    qpll_reset(11 downto 0)           <= register_map_control.GBT_PLL_RESET.QPLL_RESET(59 downto 48);


    TX_TC_DLY_VALUE(47 downto 0)  <= register_map_control.GBT_TX_TC_DLY_VALUE1;
    TX_TC_DLY_VALUE(95 downto 48) <=register_map_control.GBT_TX_TC_DLY_VALUE2;
    TX_TC_DLY_VALUE(143 downto 96)  <= register_map_control.GBT_TX_TC_DLY_VALUE3;
    TX_TC_DLY_VALUE(191 downto 144) <= register_map_control.GBT_TX_TC_DLY_VALUE4;

    DATA_TXFORMAT(47 downto 0)        <= register_map_control.GBT_DATA_TXFORMAT1(47 downto 0);
    DATA_RXFORMAT(47 downto 0)        <= register_map_control.GBT_DATA_RXFORMAT1(47 downto 0);
    DATA_TXFORMAT(95 downto 48)       <= register_map_control.GBT_DATA_TXFORMAT2(47 downto 0);
    DATA_RXFORMAT(95 downto 48)       <= register_map_control.GBT_DATA_RXFORMAT2(47 downto 0);

    TX_RESET(47 downto 0)             <= register_map_control.GBT_TX_RESET(47 downto 0);
    RX_RESET(47 downto 0)             <= register_map_control.GBT_RX_RESET(47 downto 0);
    TX_TC_METHOD(47 downto 0)         <= register_map_control.GBT_TX_TC_METHOD(47 downto 0);
    TC_EDGE(47 downto 0)              <= register_map_control.GBT_TC_EDGE(47 downto 0);
    outsel_i(47 downto 0)             <= register_map_control.GBT_OUTMUX_SEL(47 downto 0);


    g_sync_RXTXPOLARITY: for i in 0 to GBT_NUM-1 generate
        sync_TXPOLARITY : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => register_map_control.GBT_TXPOLARITY(i),
                dest_clk => GT_TX_WORD_CLK(i),
                dest_out => TXPOLARITY_TX_WORD_CLK(i)
            );
        sync_RXPOLARITY : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => register_map_control.GBT_RXPOLARITY(i),
                dest_clk => GT_RX_WORD_CLK(i),
                dest_out => RXPOLARITY_RX_WORD_CLK(i)
            );

        Channel_disable(i) <= register_map_control.GBT_CHANNEL_DISABLE(i);
    end generate;

    register_map_link_monitor.GBT_VERSION.DATE             <=  GBT_VERSION(63 downto 48);
    register_map_link_monitor.GBT_VERSION.GBT_VERSION(35 downto 32)      <=  GBT_VERSION(23 downto 20);
    register_map_link_monitor.GBT_VERSION.GTH_IP_VERSION(19 downto 16)   <=  GBT_VERSION(19 downto 16);
    register_map_link_monitor.GBT_VERSION.RESERVED         <=  GBT_VERSION(15 downto 3);
    register_map_link_monitor.GBT_VERSION.GTHREFCLK_SEL    <=  (others => GTHREFCLK_SEL);
    register_map_link_monitor.GBT_VERSION.RX_CLK_SEL       <=  GBT_VERSION(1 downto 1);
    register_map_link_monitor.GBT_VERSION.PLL_SEL          <=  GBT_VERSION(0 downto 0);


    register_map_link_monitor.GBT_TXRESET_DONE(47 downto 0)        <= txresetdone(47 downto 0);
    register_map_link_monitor.GBT_RXRESET_DONE(47 downto 0)        <= rxresetdone(47 downto 0);
    register_map_link_monitor.GBT_TXFSMRESET_DONE(47 downto 0)     <= TXPMARESETDONE(47 downto 0);
    register_map_link_monitor.GBT_RXFSMRESET_DONE(47 downto 0)     <= RXPMARESETDONE(47 downto 0);
    register_map_link_monitor.GBT_CPLL_FBCLK_LOST(47 downto 0)     <= cpllfbclklost (47 downto 0);
    register_map_link_monitor.GBT_PLL_LOCK.CPLL_LOCK(47 downto 0)  <= cplllock(47 downto 0);
    register_map_link_monitor.GBT_PLL_LOCK.QPLL_LOCK(59 downto 48) <= qplllock(11 downto 0);

    pll_lock_latch_proc: process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            if register_map_control.GBT_PLL_LOL_LATCHED.CLEAR = "1" then
                register_map_link_monitor.GBT_PLL_LOL_LATCHED.CPLL_LOL_LATCHED <= (others => '0');
                register_map_link_monitor.GBT_PLL_LOL_LATCHED.QPLL_LOL_LATCHED <= (others => '0');
            end if;
            for i in 0 to GBT_NUM-1 loop
                if cplllock(i) = '0' then
                    register_map_link_monitor.GBT_PLL_LOL_LATCHED.CPLL_LOL_LATCHED(i) <= '1';
                end if;
            end loop;
            for i in 0 to GBT_NUM/4 - 1 loop
                if qplllock(i) = '0' then
                    register_map_link_monitor.GBT_PLL_LOL_LATCHED.QPLL_LOL_LATCHED(i+48) <= '1';
                end if;
            end loop;

            if register_map_control.GBT_ALIGNMENT_LOST.CLEAR = "1" then
                register_map_link_monitor.GBT_ALIGNMENT_LOST.ALIGNMENT_LOST <= (others => '0');
            end if;
            for i in 0 to GBT_NUM-1 loop
                if alignment_done_f(i) = '0' then
                    register_map_link_monitor.GBT_ALIGNMENT_LOST.ALIGNMENT_LOST(i) <= '1';
                end if;
            end loop;
        end if;
    end process;


    register_map_link_monitor.GBT_RXCDR_LOCK(47 downto 0)          <= rxcdrlock(47 downto 0);
    register_map_link_monitor.GBT_CLK_SAMPLED(47 downto 0)         <= (others => '0'); --never written clk_sampled(47 downto 0);

    register_map_link_monitor.GBT_RX_IS_HEADER(47 downto 0)        <= rx_is_header(47 downto 0);
    register_map_link_monitor.GBT_RX_IS_DATA(47 downto 0)          <= (others => '0'); --rx_is_data(47 downto 0);
    register_map_link_monitor.GBT_RX_HEADER_FOUND(47 downto 0)     <= RX_HEADER_FOUND(47 downto 0);

    register_map_link_monitor.GBT_ALIGNMENT_DONE(47 downto 0)      <= alignment_done_f(47 downto 0);


    register_map_link_monitor.GBT_OUT_MUX_STATUS(47 downto 0)    <= outsel_o(47 downto 0);
    register_map_link_monitor.GBT_ERROR(47 downto 0)             <= error_f(47 downto 0);

    error_gen : for i in 47 downto 0 generate
        signal fec_error_cnt: std_logic_vector(31 downto 0);
    begin
        error_f(i) <= error_orig(i) and alignment_done_f(i);
        g_lt24: if i < 24 generate
            register_map_link_monitor.GT_FEC_ERR_CNT(i) <= fec_error_cnt;
        end generate;

        fecc_error_cnt_proc: process(clk40_in)
        begin
            if rising_edge(clk40_in) then
                if rst_hw = '1' then
                    fec_error_cnt <= x"0000_0000";
                else
                    if error_f(i) = '1' then
                        fec_error_cnt <= fec_error_cnt + 1;
                    end if;
                end if;
            end if;
        end process;

    end generate;

    register_map_link_monitor.GBT_GBT_TOPBOT_C(47 downto 0)      <= (others => '0');--TopBot_C(47 downto 0);


    alignment_chk_rst_i           <= General_ctrl(0);


    DESMUX_USE_SW                 <= register_map_control.GBT_MODE_CTRL.DESMUX_USE_SW(0);

    datamod_gen1 : if DYNAMIC_DATA_MODE_EN='1' generate
        DATA_TXFORMAT_i <= DATA_TXFORMAT;
        DATA_RXFORMAT_i <= DATA_RXFORMAT;
    end generate;

    datamod_gen2 : if DYNAMIC_DATA_MODE_EN='0' generate
        DATA_TXFORMAT_i <= GBT_DATA_TXFORMAT_PACKAGE;
        DATA_RXFORMAT_i <= GBT_DATA_RXFORMAT_PACKAGE;
    end generate;

    process(clk40_in)
    begin
        if clk40_in'event and clk40_in='1' then
            pulse_lg <= pulse_cnt(20);
            if pulse_cnt(20)='1' then
                pulse_cnt <=(others=>'0');
            else
                pulse_cnt <= pulse_cnt+'1';
            end if;
        end if;
    end process;

    process(clk40_in)
    begin
        if clk40_in'event and clk40_in='1' then
            alignment_done_chk_cnt <= alignment_done_chk_cnt + '1';
        end if;
    end process;

    gbtModeAutoRxReset_cnt_g: for i in 0 to GBT_NUM-1 generate
        signal RXRESET_AUTO_CNT: std_logic_vector(31 downto 0);
        signal RXRESET_AUTO_p1: std_logic;
    begin
        g_lt24: if i < 24 generate
            cnt_proc: process(clk40_in)
            begin
                if rising_edge(clk40_in) then
                    RXRESET_AUTO_p1 <= auto_gbt_rxrst(i);
                    if rst_hw = '1' or register_map_control.GT_AUTO_RX_RESET_CNT(i).CLEAR = "1" then
                        RXRESET_AUTO_CNT <= (others => '0');
                    elsif auto_gbt_rxrst(i) = '1' and RXRESET_AUTO_p1 = '0' then
                        RXRESET_AUTO_CNT <= RXRESET_AUTO_CNT + 1;
                    end if;
                end if;
            end process;

            register_map_link_monitor.GT_AUTO_RX_RESET_CNT(i).VALUE <= RXRESET_AUTO_CNT;
        end generate g_lt24;
    end generate gbtModeAutoRxReset_cnt_g;

    rxalign_auto : for i in GBT_NUM-1 downto 0 generate
        signal RxSlide_c_RX_WORD_CLOCK: std_logic;
        signal rxcdrlock_RX_WORD_CLOCK: std_logic;
        signal rxresetdone_40: std_logic;
        signal alignment_done_40: std_logic;
    begin

        process(clk40_in)
        begin
            if clk40_in'event and clk40_in='1' then
                if alignment_done_chk_cnt="0000000000000" then
                    alignment_done_a(i) <= rxcdrlock(i) and alignment_done_40;
                else
                    alignment_done_a(i) <= rxcdrlock(i) and alignment_done_40 and alignment_done_a(i);
                end if;
                if alignment_done_chk_cnt="0000000000000" then
                    alignment_done_f(i) <=  rxcdrlock(i) and alignment_done_a(i);
                end if;
            end if;
        end process;


        RX_120b_out(i) <= RX_120b_out_ii(i) when alignment_done_f(i)='1'
                            else (others =>'0');

        sync_rxresetdone: xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => rxresetdone(i),
                dest_clk => clk40_in,
                dest_out => rxresetdone_40
            );

        sync_alignment_done: xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => alignment_done(i),
                dest_clk => clk40_in,
                dest_out => alignment_done_40
            );

        auto_rxrst : entity work.FELIX_GBT_RX_AUTO_RST
            port map(
                ext_trig_realign        => open, --ext_trig_realign(i),
                FSM_CLK                 => clk40_in,
                GBT_LOCK                => alignment_done_f(i), --alignment_done(i),
                pulse_lg                => pulse_lg,
                GTHRXRESET_DONE         => rxresetdone_40,
                AUTO_GTH_RXRST          => auto_gth_rxrst(i),
                alignment_chk_rst       => alignment_chk_rst_c1(i),
                AUTO_GBT_RXRST          => auto_gbt_rxrst(i)
            );

        rafsm : entity work.FELIX_GBT_RXSLIDE_FSM
            port map(
                alignment_chk_rst       => alignment_chk_rst_c(i),
                --ext_trig_realign        => ext_trig_realign(i),
                FSM_RST                 => FSM_RST(i),
                FSM_CLK                 => clk40_in,
                GBT_LOCK                => alignment_done(i),
                RxSlide                 => RxSlide_c(i)
            );

        FSM_RST(i)          <= RX_RESET(i);-- or RX_ALIGN_SW;
        RX_RESET_i(i)       <= --RX_RESET(i) when RX_ALIGN_SW='1' else
                               (RX_RESET(i) or auto_gbt_rxrst(i));
        alignment_chk_rst(i)        <= --alignment_chk_rst_i when RX_ALIGN_SW='1' else
                                       (alignment_chk_rst_i or alignment_chk_rst_c(i) or alignment_chk_rst_c1(i));

        sync_RxSlide_c : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => RxSlide_c(i),
                dest_clk => GT_RX_WORD_CLK(i),
                dest_out => RxSlide_c_RX_WORD_CLOCK
            );
        sync_cdrlock: xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => rxcdrlock(i),
                dest_clk => GT_RX_WORD_CLK(i),
                dest_out => rxcdrlock_RX_WORD_CLOCK
            );
        RxSlide_i(i)             <= RxSlide_c_RX_WORD_CLOCK and rxcdrlock_RX_WORD_CLOCK;
        tx_reset_712_g: if CARD_TYPE /= 709 and CARD_TYPE /= 710 generate
            TX_RESET_i(i)       <= TX_RESET(i) or (not txresetdone(i));-- or (not TxFsmResetDone(i));
        end generate;
        tx_reset_709_g: if CARD_TYPE = 709 or CARD_TYPE = 710 generate
            TX_RESET_i(i)       <= TX_RESET(i) or (not txresetdone(i)) or (not TXPMARESETDONE(i));
        end generate;
    end generate;

    outsel_ii             <= outsel_o when DESMUX_USE_SW = '0' else outsel_i;

    gbtRxTx : for i in GBT_NUM-1 downto 0 generate
        signal fifo_rst_RX_WORD_CLK: std_logic;
        signal RX_RESET_i_GT_RX_WORD_CLK: std_logic;
        signal DATA_RXFORMAT_sync: std_logic_vector(1 downto 0);

        signal DATA_TXFORMAT_sync : std_logic_vector(1 downto 0);
        signal TX_TC_DLY_VALUE_sync : std_logic_vector(2 downto 0);
        signal TC_EDGE_sync: std_logic;
        signal TX_RESET_sync: std_logic;
        signal TX_TC_METHOD_sync: std_logic;
        signal TX_120b_in_wc: std_logic_vector(119 downto 0);
    begin

        sync_RXFORMAT : xpm_cdc_array_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0,
                WIDTH => 2
            )
            port map (
                src_clk => '0',
                src_in => DATA_RXFORMAT_i(2*i+1 downto 2*i),
                dest_clk => GT_RXUSRCLK(i),
                dest_out => DATA_RXFORMAT_sync
            );

        sync_TXFORMAT : xpm_cdc_array_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0,
                WIDTH => 2
            )
            port map (
                src_clk => '0',
                src_in => DATA_TXFORMAT_i(2*i+1 downto 2*i),
                dest_clk => GT_TXUSRCLK(i),
                dest_out => DATA_TXFORMAT_sync
            );

        sync_TX_TC_DLY_VALUE : xpm_cdc_array_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0,
                WIDTH => 3
            )
            port map (
                src_clk => '0',
                src_in => TX_TC_DLY_VALUE(4*i+2 downto 4*i),
                dest_clk => GT_TXUSRCLK(i),
                dest_out => TX_TC_DLY_VALUE_sync
            );

        sync_TC_EDGE : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => TC_EDGE(i),
                dest_clk => GT_TXUSRCLK(i),
                dest_out => TC_EDGE_sync
            );

        sync_TX_TC_METHOD : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => TX_TC_METHOD(i),
                dest_clk => GT_TXUSRCLK(i),
                dest_out => TX_TC_METHOD_sync
            );

        sync_TX_RESET : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => TX_RESET_i(i),
                dest_clk => GT_TXUSRCLK(i),
                dest_out => TX_RESET_sync
            );

        process(GT_RX_WORD_CLK(i))
        begin
            if GT_RX_WORD_CLK(i)'event and GT_RX_WORD_CLK(i)='1' then
                BITSLIP_MANUAL_r(i)     <= RxSlide_i(i);
                BITSLIP_MANUAL_2r(i)    <= BITSLIP_MANUAL_r(i);
                RxSlide(i)              <= BITSLIP_MANUAL_r(i) and (not BITSLIP_MANUAL_2r(i));
            end if;
        end process;


        alignment_chk_rst_f(i)      <= alignment_chk_rst(i);-- or (not RxCdrLock(i));

        sync_RX_RESET_i : xpm_cdc_sync_rst
            generic map (
                DEST_SYNC_FF => 2,
                INIT => 1,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0
            )
            port map (
                src_rst => RX_RESET_i(i),
                dest_clk => GT_RX_WORD_CLK(i),
                dest_rst => RX_RESET_i_GT_RX_WORD_CLK
            );

        sync_TX_120b_in : xpm_cdc_array_single
            generic map (
                DEST_SYNC_FF => 6, --Equivalent to 1 40 MHz cycle.
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0,
                WIDTH => 84
            )
            port map (
                src_clk => clk40_in,
                src_in => TX_120b_in(i)(115 downto 32),
                dest_clk => GT_TX_WORD_CLK(i),
                dest_out => TX_120b_in_wc(115 downto 32)
            );
        p_TX_header: process(clk40_in)
        begin
            if rising_edge(clk40_in) then
                TX_120b_in_wc(119 downto 116) <= TX_120b_in(i)(119 downto 116);
            end if;
        end process;
        TX_120b_in_wc(31 downto 0) <= x"0000_0000"; --No WideMode supported.

        gbtTxRx_inst: entity work.gbtTxRx_FELIX
            generic map(
                channel => i
            )
            port map(
                alignment_chk_rst => alignment_chk_rst_f(i),
                alignment_done_O => alignment_done(i),
                outsel_i => outsel_ii(i),
                outsel_o => outsel_o(i),
                error_o => error_orig(i),
                TX_TC_DLY_VALUE => TX_TC_DLY_VALUE_sync,
                TX_TC_METHOD => TX_TC_METHOD_sync,
                TC_EDGE => TC_EDGE_sync,
                RX_FLAG => RX_FLAG_Oi(i), --RX_FLAG_O(i),
                TX_FLAG => open,
                RX_LATOPT_DES => '1', --RX_OPT(i),
                Tx_DATA_FORMAT => DATA_TXFORMAT_sync,
                Rx_Data_Format => DATA_RXFORMAT_sync,
                RX_RESET_I => RX_RESET_i_GT_RX_WORD_CLK,
                RX_FRAME_CLK_O => open, --RX_FRAME_CLK_O(i),
                RX_HEADER_FOUND => RX_HEADER_FOUND(i),
                RX_WORD_IS_HEADER_O => rx_is_header(i),
                RX_WORDCLK_I => GT_RX_WORD_CLK(i),
                L40M => clk40_in,
                RX_DATA_20b_I => RX_DATA_20b(i),
                RX_DATA_120b_O => RX_120b_out_i(i),
                TX_RESET_I => TX_RESET_sync,
                TX_FRAMECLK_I => TX_FRAME_CLK_I(i),
                des_rxusrclk => GT_RX_WORD_CLK(i),
                TX_WORDCLK_I => GT_TX_WORD_CLK(i),
                TX_DATA_120b_I => TX_120b_in_wc,
                TX_DATA_20b_O => TX_DATA_20b(i)
            );

        fifo_rst(i) <= rst_hw or (not alignment_done_f(i)) or RX_RESET_i(i) or General_ctrl(4);
        fifo_rden(i) <= not fifo_empty(i);

        sync_fifo_rst : xpm_cdc_sync_rst
            generic map (
                DEST_SYNC_FF => 2,
                INIT => 1,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0
            )
            port map (
                src_rst => fifo_rst(i),
                dest_clk => GT_RX_WORD_CLK(i),
                dest_rst => fifo_rst_RX_WORD_CLK -- 1-bit output: src_rst synchronized to the destination clock domain. This output

            );

        fifo_inst: xpm_fifo_async
            generic map(
                FIFO_MEMORY_TYPE => "block",
                FIFO_WRITE_DEPTH => 32,
                CASCADE_HEIGHT => 0,
                RELATED_CLOCKS => 0,
                WRITE_DATA_WIDTH => 120,
                READ_MODE => "std",
                FIFO_READ_LATENCY => 1,
                FULL_RESET_VALUE => 0,
                USE_ADV_FEATURES => "0200", --only programmable empty
                READ_DATA_WIDTH => 120,
                CDC_SYNC_STAGES => 2,
                WR_DATA_COUNT_WIDTH => 5,
                PROG_FULL_THRESH => 28,
                RD_DATA_COUNT_WIDTH => 5,
                PROG_EMPTY_THRESH => 5,
                DOUT_RESET_VALUE => "0",
                ECC_MODE => "no_ecc",
                SIM_ASSERT_CHK => 0,
                WAKEUP_TIME => 0
            )
            port map(
                sleep => '0',
                rst => fifo_rst_RX_WORD_CLK, --rst_hw,
                wr_clk => GT_RX_WORD_CLK(i),
                wr_en => RX_FLAG_Oi(i),
                din => RX_120b_out_i(i),
                full => open,
                prog_full => open,
                wr_data_count => open,
                overflow => open,
                wr_rst_busy => open,
                almost_full => open,
                wr_ack => open,
                rd_clk => clk40_in, --FIFO_RD_CLK(i),
                rd_en => fifo_rden(i), --not fifo_empty(i),--'1',--FIFO_RD_EN(i),
                dout => RX_120b_out_ii(i),
                empty => open,
                prog_empty => fifo_empty(i), --FIFO_EMPTY(i)
                rd_data_count => open,
                underflow => open,
                rd_rst_busy => open,
                almost_empty => open,
                data_valid => open,
                injectsbiterr => '0',
                injectdbiterr => '0',
                sbiterr => open,
                dbiterr => open
            );


    end generate;

    g_KCU_clks: if CARD_TYPE /= 709 and CARD_TYPE /= 710 generate
        clk_generate : for i in GBT_NUM-1 downto 0 generate

            GTTXOUTCLK_BUFG: bufg_gt
                generic map(
                    SIM_DEVICE => "ULTRASCALE",
                    STARTUP_SYNC => "FALSE"
                )
                port map(
                    o       => GT_TXUSRCLK(i),
                    ce      => '1',
                    cemask  => '0',
                    clr     => '0', --userclk_tx_reset_in,--'0',
                    clrmask => '0',
                    div     => "000",
                    i       => GT_TXOUTCLK(i)
                );

            GT_TX_WORD_CLK(i) <= GT_TXUSRCLK(i);

            GTRXOUTCLK_BUFG: bufg_gt
                generic map(
                    SIM_DEVICE => "ULTRASCALE",
                    STARTUP_SYNC => "FALSE"
                )
                port map(
                    o       => GT_RXUSRCLK(i),
                    ce      => '1',
                    cemask  => '0',
                    clr     => '0', --userclk_tx_reset_in,--'0',
                    clrmask => '0',
                    div     => "000",
                    i       => GT_RXOUTCLK(i)
                );

            GT_RX_WORD_CLK(i) <= GT_RXUSRCLK(i);
            RXUSRCLK_OUT(i)   <= GT_RXUSRCLK(i);
        end generate;
    end generate g_KCU_clks;
    g_VC7_clks: if CARD_TYPE = 709 or CARD_TYPE = 710 generate
        clk_generate : for i in (GBT_NUM-1)/4 downto 0 generate

            GTTXOUTCLK_BUFG: BUFG
                port map
        (
                    O => GT_TXUSRCLK(i),
                    I => GT_TXOUTCLK(4*i)
                );

            GT_TX_WORD_CLK(4*i+0) <= GT_TXUSRCLK(i);
            GT_TX_WORD_CLK(4*i+1) <= GT_TXUSRCLK(i);
            GT_TX_WORD_CLK(4*i+2) <= GT_TXUSRCLK(i);
            GT_TX_WORD_CLK(4*i+3) <= GT_TXUSRCLK(i);

        end generate;

        RXCLK_GEN: for i in 0 to GBT_NUM-1 generate
            GTRXOUTCLK_BUFG: BUFG
                port map(
                    O => GT_RX_WORD_CLK(i),
                    I => GT_RXOUTCLK(i)
                );
            RXUSRCLK_OUT(i) <= GT_RX_WORD_CLK(i);
        end generate;

    end generate g_VC7_clks;

    drpclk_in(0) <= clk40_in;

    QPLL_GEN: if PLL_SEL = QPLL generate

        port_trans : for i in GBT_NUM-1 downto 0 generate
            RX_N_i(i)   <= RX_N(i);
            RX_P_i(i)   <= RX_P(i);
            TX_N(i)     <= TX_N_i(i);
            TX_P(i)     <= TX_P_i(i);

        end generate;

        GTH_inst : for i in (GBT_NUM-1)/4 downto 0 generate
            g_KCU: if CARD_TYPE /= 709 and CARD_TYPE /= 710 generate
                RX_DATA_20b(4*i+0) <= RX_DATA_80b(i)(19 downto 0);
                RX_DATA_20b(4*i+1) <= RX_DATA_80b(i)(39 downto 20);
                RX_DATA_20b(4*i+2) <= RX_DATA_80b(i)(59 downto 40);
                RX_DATA_20b(4*i+3) <= RX_DATA_80b(i)(79 downto 60);

                TX_DATA_80b(i) <= TX_DATA_20b(4*i+3) & TX_DATA_20b(4*i+2) & TX_DATA_20b(4*i+1) & TX_DATA_20b(4*i+0);

                GTH_TOP_INST: entity work.GTH_QPLL_Wrapper
                    Port map(
                        gt_rxusrclk_in               => GT_RX_WORD_CLK(4*i+3 downto 4*i),
                        gt_txusrclk_in               => GT_TX_WORD_CLK(4*i+3 downto 4*i),
                        gt_rxoutclk_out              => GT_RXOUTCLK(4*i+3 downto 4*i),
                        gt_txoutclk_out              => GT_TXOUTCLK(4*i+3 downto 4*i),
                        gthrxn_in                    => RX_N_i(4*i+3 downto 4*i),
                        gthrxp_in                    => RX_P_i(4*i+3 downto 4*i),
                        gthtxn_out                   => TX_N_i(4*i+3 downto 4*i),
                        gthtxp_out                   => TX_P_i(4*i+3 downto 4*i),
                        drpclk_in                    => drpclk_in, --(others=>clk40_in),
                        gtrefclk0_in                 => GTH_RefClk(4*i downto 4*i),
                        rxpolarity_in                => RXPOLARITY_RX_WORD_CLK(4*i+3 downto 4*i),
                        txpolarity_in                => TXPOLARITY_TX_WORD_CLK(4*i+3 downto 4*i),
                        loopback_in                  => register_map_control.GTH_LOOPBACK_CONTROL,
                        rxcdrhold_in                 => '0',
                        userdata_tx_in               => TX_DATA_80b(i),
                        userdata_rx_out              => RX_DATA_80b(i),
                        userclk_rx_reset_in          => userclk_rx_reset_in(i downto i), --(others=>(not rxpmaresetdone_out(i))),--locked,
                        userclk_tx_reset_in          => userclk_tx_reset_in(i downto i), --(others=>(not txpmaresetdone_out(i))),--,--locked,
                        reset_all_in                 => soft_reset_f(i downto i),
                        reset_tx_pll_and_datapath_in => qpll_reset(i downto i),
                        reset_tx_datapath_in         => gttx_reset_merge(i downto i),
                        reset_rx_pll_and_datapath_in => qpll_reset(i downto i),
                        reset_rx_datapath_in         => gtrx_reset_merge(i downto i),
                        qpll1lock_out                => qplllock(i downto i),
                        qpll1fbclklost_out           => open, --
                        qpll0lock_out                => open,
                        qpll0fbclklost_out           => open,
                        rxslide_in                   => RxSlide(4*i+3 downto 4*i),
                        txresetdone_out              => txresetdone(4*i+3 downto 4*i),
                        txpmaresetdone_out           => TXPMARESETDONE(4*i+3 downto 4*i),
                        rxresetdone_out              => rxresetdone(4*i+3 downto 4*i),
                        rxpmaresetdone_out           => RXPMARESETDONE(4*i+3 downto 4*i),
                        reset_tx_done_out            => open, --txresetdone_quad(i downto i),
                        reset_rx_done_out            => open, --rxresetdone_quad(i downto i),
                        reset_rx_cdr_stable_out      => open, --rxcdrlock_quad(i downto i),
                        rxcdrlock_out                => rxcdrlock_out(4*i+3 downto 4*i)
                    );
            end generate g_KCU;
            g_V7: if CARD_TYPE = 709 or CARD_TYPE = 710 generate
                GTH_TOP_INST: entity work.GTH_QPLL_Wrapper_V7
                    Port map
                  (
                        GTH_RefClk => GTH_RefClk(4*i),
                        DRP_CLK_IN => drpclk_in(0),
                        gt0_rxusrclk_in => GT_RX_WORD_CLK(4*i),
                        gt0_rxoutclk_out => GT_RXOUTCLK(4*i),
                        gt1_rxusrclk_in => GT_RX_WORD_CLK(4*i+1),
                        gt1_rxoutclk_out => GT_RXOUTCLK(4*i+1),
                        gt2_rxusrclk_in => GT_RX_WORD_CLK(4*i+2),
                        gt2_rxoutclk_out => GT_RXOUTCLK(4*i+2),
                        gt3_rxusrclk_in => GT_RX_WORD_CLK(4*i+3),
                        gt3_rxoutclk_out => GT_RXOUTCLK(4*i+3),
                        gt0_txusrclk_in => GT_TXUSRCLK(i), --GT_TX_WORD_CLK(4*i),
                        gt0_txoutclk_out => GT_TXOUTCLK(4*i),
                        --gt1_txusrclk_in => GT_TXUSRCLK(i), --GT_TX_WORD_CLK(4*i+1),
                        gt1_txoutclk_out => GT_TXOUTCLK(4*i+1),
                        --gt2_txusrclk_in => GT_TXUSRCLK(i), --GT_TX_WORD_CLK(4*i+2),
                        gt2_txoutclk_out => GT_TXOUTCLK(4*i+2),
                        --gt3_txusrclk_in => GT_TXUSRCLK(i), --GT_TX_WORD_CLK(4*i+3),
                        gt3_txoutclk_out => GT_TXOUTCLK(4*i+3),
                        gt_txresetdone_out => txresetdone(4*i+3 downto 4*i),
                        gt_rxresetdone_out => rxresetdone(4*i+3 downto 4*i),
                        gt_txfsmresetdone_out => TXPMARESETDONE(4*i+3 downto 4*i), --txfsmresetdone(4*i+3 downto 4*i),
                        gt_rxfsmresetdone_out => RXPMARESETDONE(4*i+3 downto 4*i), --rxfsmresetdone(4*i+3 downto 4*i),
                        gt_cpllfbclklost_out => cpllfbclklost(4*i+3 downto 4*i),
                        gt_cplllock_out => cplllock(4*i+3 downto 4*i),
                        gt_rxcdrlock_out => rxcdrlock_out(4*i+3 downto 4*i),
                        gt_qplllock_out => qplllock(i),
                        gt_rxslide_in => RxSlide(4*i+3 downto 4*i),
                        gt_txpolarity_in => TXPOLARITY_TX_WORD_CLK(4*i+3 downto 4*i),
                        gt_rxpolarity_in => RXPOLARITY_RX_WORD_CLK(4*i+3 downto 4*i),
                        gt_txuserrdy_in => "1111", --TxUsrRdy(4*i+3 downto 4*i),
                        gt_rxuserrdy_in => "1111", --RxUsrRdy(4*i+3 downto 4*i),
                        gt0_loopback_in => register_map_control.GTH_LOOPBACK_CONTROL,
                        gt0_rxcdrhold_in => '0',
                        gt1_loopback_in => register_map_control.GTH_LOOPBACK_CONTROL,
                        gt1_rxcdrhold_in => '0',
                        gt2_loopback_in => register_map_control.GTH_LOOPBACK_CONTROL,
                        gt2_rxcdrhold_in => '0',
                        gt3_loopback_in => register_map_control.GTH_LOOPBACK_CONTROL,
                        gt3_rxcdrhold_in => '0',
                        --SOFT_RESET_IN => soft_reset(i) or rst_hw,
                        GTTX_RESET_IN => gttx_reset(4*i+3 downto 4*i), -- or rst_hw,
                        GTRX_RESET_IN => gtrx_reset_i(4*i+3 downto 4*i),
                        --CPLL_RESET_IN => cpll_reset(4*i+3 downto 4*i),
                        QPLL_RESET_IN => qpll_reset(i),
                        --SOFT_TXRST_GT => SOFT_TXRST_GT(4*i+3 downto 4*i),
                        --SOFT_RXRST_GT => SOFT_RXRST_GT(4*i+3 downto 4*i),
                        SOFT_TXRST_ALL => soft_reset_f(i),
                        SOFT_RXRST_ALL => soft_reset_f(i),
                        RX_DATA_gt0_20b => RX_DATA_20b(4*i),
                        TX_DATA_gt0_20b => TX_DATA_20b(4*i),
                        RX_DATA_gt1_20b => RX_DATA_20b(4*i+1),
                        TX_DATA_gt1_20b => TX_DATA_20b(4*i+1),
                        RX_DATA_gt2_20b => RX_DATA_20b(4*i+2),
                        TX_DATA_gt2_20b => TX_DATA_20b(4*i+2),
                        RX_DATA_gt3_20b => RX_DATA_20b(4*i+3),
                        TX_DATA_gt3_20b => TX_DATA_20b(4*i+3),
                        RXN_IN => RX_N_i(4*i+3 downto 4*i),
                        RXP_IN => RX_P_i(4*i+3 downto 4*i),
                        TXN_OUT => TX_N_i(4*i+3 downto 4*i),
                        TXP_OUT => TX_P_i(4*i+3 downto 4*i)
                    );
            end generate;

            process(clk40_in)
            begin
                if clk40_in'event and clk40_in='1' then
                    if cdr_cnt ="00000000000000000000" then
                        rxcdrlock_a(4*i)     <= rxcdrlock_out(4*i);
                        rxcdrlock_a(4*i+1)   <= rxcdrlock_out(4*i+1);
                        rxcdrlock_a(4*i+2)   <= rxcdrlock_out(4*i+2);
                        rxcdrlock_a(4*i+3)   <= rxcdrlock_out(4*i+3);
                    else
                        rxcdrlock_a(4*i) <= rxcdrlock_a(4*i) and rxcdrlock_out(4*i);
                        rxcdrlock_a(4*i+1) <= rxcdrlock_a(4*i+1) and rxcdrlock_out(4*i+1);
                        rxcdrlock_a(4*i+2) <= rxcdrlock_a(4*i+2) and rxcdrlock_out(4*i+2);
                        rxcdrlock_a(4*i+3) <= rxcdrlock_a(4*i+3) and rxcdrlock_out(4*i+3);
                    end if;
                    if cdr_cnt="00000000000000000000" then
                        RxCdrLock_int(4*i) <=rxcdrlock_a(4*i);
                        RxCdrLock_int(4*i+1) <=rxcdrlock_a(4*i+1);
                        RxCdrLock_int(4*i+2) <=rxcdrlock_a(4*i+2);
                        RxCdrLock_int(4*i+3) <=rxcdrlock_a(4*i+3);
                    end if;
                end if;
            end process;
            rxcdrlock(4*i) <= (not Channel_disable(4*i)) and RxCdrLock_int(4*i);
            rxcdrlock(4*i+1) <= (not Channel_disable(4*i+1)) and RxCdrLock_int(4*i+1);
            rxcdrlock(4*i+2) <= (not Channel_disable(4*i+2)) and RxCdrLock_int(4*i+2);
            rxcdrlock(4*i+3) <= (not Channel_disable(4*i+3)) and RxCdrLock_int(4*i+3);

            soft_reset_f(i) <= soft_reset(i) or qpll_reset(i);--or rst_hw;-- or GTRX_RESET(i);

            userclk_rx_reset_in(i) <=not (RXPMARESETDONE(4*i+0) or RXPMARESETDONE(4*i+1) or RXPMARESETDONE(4*i+2) or RXPMARESETDONE(4*i+3));
            userclk_tx_reset_in(i) <=not (TXPMARESETDONE(4*i+0) or TXPMARESETDONE(4*i+1) or TXPMARESETDONE(4*i+2) or TXPMARESETDONE(4*i+3));

            gttx_reset_merge(i) <= gttx_reset(4*i) or gttx_reset(4*i+1) or gttx_reset(4*i+2) or gttx_reset(4*i+3);
            gtrx_reset_merge(i) <= (gtrx_reset(4*i) or (auto_gth_rxrst(4*i) and rxcdrlock(4*i)))
                                   or (gtrx_reset(4*i+1) or (auto_gth_rxrst(4*i+1) and rxcdrlock(4*i+1)))
                                   or (gtrx_reset(4*i+2) or (auto_gth_rxrst(4*i+2) and rxcdrlock(4*i+2)))
                                   or (gtrx_reset(4*i+3) or (auto_gth_rxrst(4*i+3) and rxcdrlock(4*i+3))) ;
        --GTRX_RESET_MERGE(i) <= GTRX_RESET(4*i) or GTRX_RESET(4*i+1) or GTRX_RESET(4*i+2) or GTRX_RESET(4*i+3);

        -- CpllLock(i) <= '1';

        end generate;
    end generate;


    process(clk40_in)
    begin
        if clk40_in'event and clk40_in='1' then
            cdr_cnt <=cdr_cnt+'1';
        end if;
    end process;


    CPLL_GEN: if  PLL_SEL = CPLL generate
        GTH_inst : for i in GBT_NUM-1 downto 0 generate
            g_KCU: if CARD_TYPE /= 709 and CARD_TYPE /= 710 generate
                GTH_TOP_INST: entity work.GTH_CPLL_Wrapper
                    Port map(
                        cpllfbclklost_out => cpllfbclklost(i downto i),
                        cplllock_out      => cplllock(i downto i),
                        gt0_rxusrclk_in   => GT_RX_WORD_CLK(i downto i),
                        gt0_txusrclk_in   => GT_TX_WORD_CLK(i downto i),
                        gt0_rxoutclk_out  => GT_RXOUTCLK(i downto i),
                        gt0_txoutclk_out  => GT_TXOUTCLK(i downto i),
                        gthrxn_in         => RX_N(i downto i),
                        gthrxp_in         => RX_P(i downto i),
                        gthtxn_out        => TX_N(i downto i),
                        gthtxp_out        => TX_P(i downto i),
                        drpclk_in         => drpclk_in, --(others=>clk40_in),
                        gtrefclk0_in      => GTH_RefClk(i downto i),
                        rxpolarity_in     => RXPOLARITY_RX_WORD_CLK(i downto i),
                        txpolarity_in     => TXPOLARITY_TX_WORD_CLK(i downto i),
                        -- for loopback: default, both signal need to be all '0'
                        -- read kcu gth manual for the details. NOTE: the TXBUFFER is disabled, so some type of loopbhack may be
                        -- not supported.
                        -- for loopback rxcdrhold needs to be set, a register needs to be added, check KCU GTH manual for details
                        -- not tested yet
                        loopback_in                  => register_map_control.GTH_LOOPBACK_CONTROL,
                        rxcdrhold_in                 => '0',
                        userdata_tx_in               => TX_DATA_20b(i),
                        userdata_rx_out              => RX_DATA_20b(i),
                        userclk_rx_reset_in          => userclk_rx_reset_in(i downto i), --(others=>(not rxpmaresetdone_out(i))),--locked,
                        userclk_tx_reset_in          => userclk_tx_reset_in(i downto i), --(others=>(not txpmaresetdone_out(i))),--,--locked,
                        reset_all_in                 => soft_reset_f(i downto i),
                        reset_tx_pll_and_datapath_in => cpll_reset(i downto i),
                        reset_tx_datapath_in         => gttx_reset(i downto i),
                        reset_rx_pll_and_datapath_in => cpll_reset(i downto i),
                        reset_rx_datapath_in         => gtrx_reset_i(i downto i), -- and RxCdrLock(i downto i),
                        rxslide_in                   => RxSlide(i downto i),
                        rxpmaresetdone_out           => RXPMARESETDONE(i downto i),
                        txpmaresetdone_out           => TXPMARESETDONE(i downto i),
                        reset_tx_done_out            => txresetdone(i downto i),
                        reset_rx_done_out            => rxresetdone(i downto i),
                        reset_rx_cdr_stable_out      => RxCdrLock_int(i downto i)

                    );
            end generate g_KCU;
            g_V7: if CARD_TYPE = 709 or CARD_TYPE = 710 generate
                GTH_TOP_INST:entity work.GTH_CPLL_Wrapper_V7
                    port map
                  (
                        GTH_RefClk => GTH_RefClk(i), --local_rx_240m_in,--gtrefclk0_in,
                        DRP_CLK_IN => drpclk_in(0),
                        gt0_loopback_in => register_map_control.GTH_LOOPBACK_CONTROL,
                        gt0_rxcdrhold_in => '0',
                        --- RX clock, for each channel
                        gt0_rxusrclk_in => GT_RX_WORD_CLK(i), --(i)
                        gt0_rxoutclk_out => GT_RXOUTCLK(i),
                        gt0_txusrclk_in => GT_TX_WORD_CLK(i),
                        gt0_txoutclk_out => GT_TXOUTCLK(i),
                        gt0_txresetdone_out => txresetdone(i), --: out  std_logic_vector(3 downto 0);
                        gt0_rxresetdone_out => rxresetdone(i), --: out  std_logic_vector(3 downto 0);
                        gt0_tx_fsm_reset_done_out => TXPMARESETDONE(i), --txfsmresetdone(i), --: out  std_logic_vector(3 downto 0);
                        gt0_rx_fsm_reset_done_out => RXPMARESETDONE(i), --rxfsmresetdone(i), --: out  std_logic_vector(3 downto 0);
                        gt0_cpllfbclklost_out => cpllfbclklost(i), --  : out  std_logic_vector(3 downto 0);
                        gt0_cplllock_out => cplllock(i), --   : out  std_logic_vector(3 downto 0);
                        gt0_rxcdrlock_out => rxcdrlock_out(i), -- : out  std_logic_vector(3 downto 0);
                        gt0_rxslide_in => RxSlide(i),
                        gt_txpolarity_in => TXPOLARITY_TX_WORD_CLK(i),
                        gt_rxpolarity_in => RXPOLARITY_RX_WORD_CLK(i),
                        gt0_txuserrdy_in => '1',
                        gt0_rxuserrdy_in => '1',
                        --SOFT_RESET_IN                           => soft_reset(i/4) or rst_hw or SOFT_TXRST_GT(i),
                        GTTX_RESET_IN => gttx_reset(i),
                        GTRX_RESET_IN => gtrx_reset(i),
                        gt0_cpllreset_in => cpll_reset(i),
                        SOFT_TXRST_GT => soft_reset_f(i),
                        SOFT_RXRST_GT => soft_reset_f(i),
                        gt0_txdata_in => TX_DATA_20b(i),
                        gt0_rxdata_out => RX_DATA_20b(i),
                        gt0_gthrxn_in => RX_N(i),
                        gt0_gthrxp_in => RX_P(i),
                        gt0_gthtxn_out => TX_N(i),
                        gt0_gthtxp_out => TX_P(i)
                    );
            end generate g_V7;
            rxcdrlock(i) <= (not Channel_disable(i)) and RxCdrLock_int(i);
            gtrx_reset_i(i) <= --GTRX_RESET(i) when RX_ALIGN_SW='1' else
                               gtrx_reset(i) or (auto_gth_rxrst(i) and rxcdrlock(i));

            soft_reset_f(i) <= soft_reset(i/4) or cpll_reset(i);--or rst_hw; -- or GTRX_RESET(i);

            userclk_rx_reset_in(i) <=not RXPMARESETDONE(i);
            userclk_tx_reset_in(i) <=not TXPMARESETDONE(i);
        --RxResetDone_f(i) <= RxResetDone(i);

        end generate;
    end generate;


end Behavioral;

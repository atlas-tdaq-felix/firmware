--!----------------------------FA------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen    (kchen@bnl.gov)
--!              Frans Schreuder (f.schreuder@nikhef.nl)
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2014/12/05 04:43:14 PM
-- Design Name: FELIX BNL-711 GBT Wrapper
-- Module Name: gbt_top - Behavioral
-- Project Name:
-- Target Devices: KCU
-- Tool Versions: Vivado
-- Description:
--              The TOP MODULE FOR FELIX GBT & GTH
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Revision N+1  - Ported from KCU to Versal
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------

LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std_unsigned.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM, xpm;
    use xpm.vcomponents.all;
    use UNISIM.VComponents.all;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;
    use ieee.numeric_std.all;

entity FELIX_gbt_wrapper_Versal is
    Generic (
        GBT_NUM                     : integer := 24
    );
    Port (
        -------------------
        ---- For debug
        -------------------
        RX_FLAG_O                   : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_FLAG_O                   : out std_logic_vector(GBT_NUM-1 downto 0);

        rst_hw                      : in std_logic;

        register_map_control        : in register_map_control_type;
        register_map_link_monitor    : out register_map_link_monitor_type;

        GTREFCLK_N_IN   : in std_logic_vector(GBT_NUM/4-1 downto 0);
        GTREFCLK_P_IN   : in std_logic_vector(GBT_NUM/4-1 downto 0);

        clk40_in                    : in std_logic;
        apb3_axi_clk                : in std_logic; --free running for reset logic

        TX_120b_in                  : in  array_120b(0 to GBT_NUM-1);
        RX_120b_out                 : out array_120b(0 to GBT_NUM-1);
        FRAME_LOCKED_O              : out std_logic_vector(GBT_NUM-1 downto 0);

        TX_FRAME_CLK_I              : in std_logic_vector(GBT_NUM-1 downto 0);

        -- GTH Data pins
        TX_P                        : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_N                        : out std_logic_vector(GBT_NUM-1 downto 0);
        RX_P                        : in  std_logic_vector(GBT_NUM-1 downto 0);
        RX_N                        : in  std_logic_vector(GBT_NUM-1 downto 0);
        RXUSRCLK_OUT                : out std_logic_vector(GBT_NUM-1 downto 0)
    );
end FELIX_gbt_wrapper_Versal;

architecture Behavioral of FELIX_gbt_wrapper_Versal is
    signal RxSlide_c      : STD_LOGIC_VECTOR(47 downto 0);
    signal RxSlide_i      : std_logic_vector(47 downto 0);
    signal gttx_reset     : std_logic_vector(47 downto 0);

    signal gtrx_reset     : std_logic_vector(47 downto 0);
    signal soft_reset     : std_logic_vector(47 downto 0);
    signal qpll_reset     : std_logic_vector(11 downto 0);
    signal txresetdone    : std_logic_vector(47 downto 0);
    signal rxresetdone    : std_logic_vector(47 downto 0);
    signal rxcdrlock,RxCdrLock_int      : std_logic_vector(47 downto 0);
    signal qplllock       : std_logic_vector(11 downto 0);
    signal cdr_cnt        : std_logic_vector(19 downto 0);
    signal TX_RESET       : std_logic_vector(47 downto 0);
    signal TX_RESET_i     : std_logic_vector(47 downto 0);

    signal RX_RESET       : std_logic_vector(47 downto 0);
    signal RX_RESET_i     : std_logic_vector(47 downto 0);
    signal GT_TXUSRCLK    : std_logic_vector(47 downto 0);
    signal GT_RXUSRCLK    : std_logic_vector(47 downto 0);
    signal RX_FLAG_Oi     : std_logic_vector(47 downto 0);
    signal outsel_i       : std_logic_vector(47 downto 0);
    signal outsel_ii      : std_logic_vector(47 downto 0);
    signal outsel_o       : std_logic_vector(47 downto 0);
    signal RX_120b_out_i  : array_120b(0 to (GBT_NUM-1));
    signal RX_120b_out_ii : array_120b(0 to (GBT_NUM-1));

    signal rx_is_header   : std_logic_vector(47 downto 0);
    signal alignment_done : std_logic_vector(47 downto 0);
    --signal rx_is_data     : std_logic_vector(47 downto 0);
    signal RX_HEADER_FOUND: std_logic_vector(47 downto 0);

    signal RxSlide        : std_logic_vector(47 downto 0);

    signal GT_TX_WORD_CLK : std_logic_vector(47 downto 0);
    signal TX_TC_METHOD   : std_logic_vector(47 downto 0);
    signal TC_EDGE        : std_logic_vector(47 downto 0);

    type data20barray     is array (0 to 47) of std_logic_vector(19 downto 0);
    signal TX_DATA_20b    : data20barray := (others => ("00000000000000000000"));
    signal RX_DATA_20b    : data20barray := (others => ("00000000000000000000"));

    signal GT_RX_WORD_CLK         : std_logic_vector(47 downto 0);
    signal alignment_chk_rst_c    : std_logic_vector(47 downto 0);
    signal alignment_chk_rst_c1   : std_logic_vector(47 downto 0);
    signal alignment_chk_rst      : std_logic_vector(47 downto 0);
    signal alignment_chk_rst_f    : std_logic_vector(47 downto 0);

    --signal rstframeclk            : std_logic;
    signal alignment_chk_rst_i    : std_logic;
    --signal rstframeclk1           : std_logic;

    signal DESMUX_USE_SW          : std_logic;

    --signal rstframeclk_3r         : std_logic;
    --signal rstframeclk_r          : std_logic;
    --signal rstframeclk_2r         : std_logic;
    --signal rstframeclk1_3r        : std_logic;
    --signal rstframeclk1_r         : std_logic;
    --signal rstframeclk1_2r        : std_logic;
    --signal cxp1_tx_pll_rst        : std_logic;
    --signal cxp2_tx_pll_rst        : std_logic;
    --signal SOFT_TXRST_GT          : std_logic_vector(47 downto 0);
    --signal TopBot                 : std_logic_vector(47 downto 0);
    --signal TopBot_C               : std_logic_vector(47 downto 0);
    --signal TopBot_i               : std_logic_vector(47 downto 0);
    --signal SOFT_RXRST_GT          : std_logic_vector(47 downto 0);
    --signal SOFT_TXRST_ALL         : std_logic_vector(11 downto 0);
    --signal SOFT_RXRST_ALL         : std_logic_vector(11 downto 0);
    --signal TX_OPT                 : std_logic_vector(95 downto 0);
    --signal RX_OPT                 : std_logic_vector(95 downto 0);
    SIGNAL DATA_TXFORMAT          : std_logic_vector(95 downto 0);
    signal DATA_TXFORMAT_i        : std_logic_vector(95 downto 0);
    SIGNAL DATA_RXFORMAT          : std_logic_vector(95 downto 0);
    signal DATA_RXFORMAT_i        : std_logic_vector(95 downto 0);

    --SIGNAL OddEven                : std_logic_vector(47 downto 0);
    --signal OddEven_i              : std_logic_vector(47 downto 0);
    --signal OddEven_c              : std_logic_vector(47 downto 0);
    --signal ext_trig_realign       : std_logic_vector(47 downto 0);


    signal General_ctrl           : std_logic_vector(63 downto 0);



    --signal GBT_RXSLIDE            : std_logic_vector(47 downto 0);
    --signal GBT_TXUSRRDY           : std_logic_vector(47 downto 0);
    --signal GBT_RXUSRRDY           : std_logic_vector(47 downto 0);
    --signal GBT_GTTX_RESET         : std_logic_vector(47 downto 0);
    --signal GBT_GTRX_RESET         : std_logic_vector(47 downto 0);
    --signal GBT_PLL_RESET          : std_logic_vector(47 downto 0);
    --signal GBT_SOFT_TX_RESET      : std_logic_vector(47 downto 0);
    --signal GBT_SOFT_RX_RESET      : std_logic_vector(47 downto 0);
    --signal GBT_ODDEVEN            : std_logic_vector(47 downto 0);
    --signal GBT_TOPBOT             : std_logic_vector(47 downto 0);
    --signal GBT_TX_TC_DLY_VALUE1   : std_logic_vector(47 downto 0);
    --signal GBT_TX_TC_DLY_VALUE2   : std_logic_vector(47 downto 0);
    --signal GBT_TX_TC_DLY_VALUE3   : std_logic_vector(47 downto 0);
    --signal GBT_TX_TC_DLY_VALUE4   : std_logic_vector(47 downto 0);
    --signal GBT_TX_OPT             : std_logic_vector(47 downto 0);
    --signal GBT_RX_OPT             : std_logic_vector(47 downto 0);
    --signal GBT_DATA_TXFORMAT      : std_logic_vector(95 downto 0);
    --signal GBT_DATA_RXFORMAT      : std_logic_vector(95 downto 0);
    --signal GBT_TX_RESET           : std_logic_vector(47 downto 0);
    --signal GBT_RX_RESET           : std_logic_vector(47 downto 0);
    --signal GBT_TX_TC_METHOD       : std_logic_vector(47 downto 0);
    --signal GBT_TC_EDGE            : std_logic_vector(47 downto 0);
    --signal GBT_OUTMUX_SEL         : std_logic_vector(47 downto 0);

    --SIGNAL GBT_TXRESET_DONE       : std_logic_vector(47 downto 0);
    --SIGNAL GBT_RXRESET_DONE       : std_logic_vector(47 downto 0);
    signal TXPMARESETDONE         : std_logic_vector(47 downto 0);
    signal RXPMARESETDONE         : std_logic_vector(47 downto 0);
    signal alignment_done_f       : std_logic_vector(47 downto 0);
    signal soft_reset_f           : std_logic_vector(47 downto 0);
    signal fifo_empty             : std_logic_vector(47 downto 0);
    signal rxcdrlock_a            : std_logic_vector(47 downto 0);

    SIGNAL Channel_disable        : std_logic_vector(63 downto 0);
    SIGNAL TX_TC_DLY_VALUE        : std_logic_vector(191 downto 0);

    signal pulse_cnt              : std_logic_vector(29 downto 0);
    signal pulse_lg               : std_logic;


    signal alignment_done_chk_cnt : std_logic_vector(12 downto 0);
    signal alignment_done_a       : std_logic_vector(47 downto 0);
    signal fifo_rst               : std_logic_vector(47 downto 0);
    signal fifo_rden              : std_logic_vector(47 downto 0);
    signal error_orig             : std_logic_vector(47 downto 0);
    signal error_f                : std_logic_vector(47 downto 0);
    signal FSM_RST                : std_logic_vector(47 downto 0);
    signal auto_gth_rxrst         : std_logic_vector(47 downto 0);
    signal auto_gbt_rxrst         : std_logic_vector(47 downto 0);


    signal BITSLIP_MANUAL_r       : std_logic_vector(47 downto 0);
    signal BITSLIP_MANUAL_2r      : std_logic_vector(47 downto 0);

    signal gttx_reset_merge       : std_logic_vector(11 downto 0);
    signal gtrx_reset_merge       : std_logic_vector(11 downto 0);
    signal rxcdrlock_out          : std_logic_vector(47 downto 0);

    signal RX_N_i                 : std_logic_vector(47 downto 0):=x"000000000000";
    signal RX_P_i                 : std_logic_vector(47 downto 0):=x"000000000000";
    signal TX_N_i                 : std_logic_vector(47 downto 0):=x"000000000000";
    signal TX_P_i                 : std_logic_vector(47 downto 0):=x"000000000000";


    component transceiver_versal_gbt_wrapper is -- @suppress "Component declaration 'transceiver_versal_gbt_wrapper' has none or multiple matching entity declarations"
        port (
            GT_REFCLK0_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
            GT_REFCLK0_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
            GT_Serial_grx_n : in STD_LOGIC_VECTOR ( 3 downto 0 );
            GT_Serial_grx_p : in STD_LOGIC_VECTOR ( 3 downto 0 );
            GT_Serial_gtx_n : out STD_LOGIC_VECTOR ( 3 downto 0 );
            GT_Serial_gtx_p : out STD_LOGIC_VECTOR ( 3 downto 0 );
            QUAD0_hsclk0_lcplllock_0 : out STD_LOGIC;
            QUAD0_hsclk1_lcplllock_0 : out STD_LOGIC;
            apb3clk_gt_bridge_ip_0 : in STD_LOGIC;
            ch0_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch0_rxbyteisaligned_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
            ch0_rxcdrlock_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
            ch0_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
            ch0_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxgearboxslip_ext_0 : in STD_LOGIC;
            ch0_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch0_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxpolarity_ext_0 : in STD_LOGIC;
            ch0_rxresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
            ch0_rxslide_ext_0 : in STD_LOGIC;
            ch0_rxvalid_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
            ch0_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
            ch0_txheader_ext_0 : in STD_LOGIC_VECTOR ( 5 downto 0 );
            ch0_txpolarity_ext_0 : in STD_LOGIC;
            ch0_txresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
            ch0_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch1_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch1_rxbyteisaligned_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
            ch1_rxcdrlock_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
            ch1_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
            ch1_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxgearboxslip_ext_0 : in STD_LOGIC;
            ch1_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch1_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxpolarity_ext_0 : in STD_LOGIC;
            ch1_rxresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
            ch1_rxslide_ext_0 : in STD_LOGIC;
            ch1_rxvalid_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
            ch1_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
            ch1_txpolarity_ext_0 : in STD_LOGIC;
            ch1_txresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
            ch1_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch2_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch2_rxbyteisaligned_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
            ch2_rxcdrlock_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
            ch2_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
            ch2_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxgearboxslip_ext_0 : in STD_LOGIC;
            ch2_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch2_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxpolarity_ext_0 : in STD_LOGIC;
            ch2_rxresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
            ch2_rxslide_ext_0 : in STD_LOGIC;
            ch2_rxvalid_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
            ch2_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
            ch2_txpolarity_ext_0 : in STD_LOGIC;
            ch2_txresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
            ch2_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch3_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch3_rxbyteisaligned_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
            ch3_rxcdrlock_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
            ch3_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
            ch3_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxgearboxslip_ext_0 : in STD_LOGIC;
            ch3_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch3_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxpolarity_ext_0 : in STD_LOGIC;
            ch3_rxresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
            ch3_rxslide_ext_0 : in STD_LOGIC;
            ch3_rxvalid_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
            ch3_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
            ch3_txpolarity_ext_0 : in STD_LOGIC;
            ch3_txresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
            ch3_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
            gt_reset_gt_bridge_ip_0 : in STD_LOGIC;
            reset_rx_datapath_in_0 : in STD_LOGIC;
            reset_rx_pll_and_datapath_in_0 : in STD_LOGIC;
            reset_tx_datapath_in_0 : in STD_LOGIC;
            reset_tx_pll_and_datapath_in_0 : in STD_LOGIC;
            rx_resetdone_out_gt_bridge_ip_0 : out STD_LOGIC;
            rxusrclk0 : out STD_LOGIC;
            rxusrclk1 : out STD_LOGIC;
            rxusrclk2 : out STD_LOGIC;
            rxusrclk3 : out STD_LOGIC;
            tx_resetdone_out_gt_bridge_ip_0 : out STD_LOGIC;
            txusrclk0 : out STD_LOGIC;
            txusrclk1 : out STD_LOGIC;
            txusrclk2 : out STD_LOGIC;
            txusrclk3 : out STD_LOGIC
        );
    end component transceiver_versal_gbt_wrapper;

    signal TXPOLARITY_TX_WORD_CLK : std_logic_vector(GBT_NUM-1 downto 0);
    signal RXPOLARITY_RX_WORD_CLK : std_logic_vector(GBT_NUM-1 downto 0);

begin

    FRAME_LOCKED_O <= alignment_done_f(GBT_NUM-1 downto 0);


    General_ctrl                          <= register_map_control.GBT_GENERAL_CTRL;

    gttx_reset(47 downto 0)           <= register_map_control.GBT_GTTX_RESET(47 downto 0);
    gtrx_reset(47 downto 0)           <= register_map_control.GBT_GTRX_RESET(47 downto 0);
    soft_reset(47 downto 0)           <= register_map_control.GBT_SOFT_RESET(47 downto 0);
    --cpll_reset(47 downto 0)           <= register_map_control.GBT_PLL_RESET.CPLL_RESET(47 downto 0);
    qpll_reset(11 downto 0)           <= register_map_control.GBT_PLL_RESET.QPLL_RESET(59 downto 48);


    TX_TC_DLY_VALUE(47 downto 0)  <= register_map_control.GBT_TX_TC_DLY_VALUE1;
    TX_TC_DLY_VALUE(95 downto 48) <=register_map_control.GBT_TX_TC_DLY_VALUE2;
    TX_TC_DLY_VALUE(143 downto 96)  <= register_map_control.GBT_TX_TC_DLY_VALUE3;
    TX_TC_DLY_VALUE(191 downto 144) <= register_map_control.GBT_TX_TC_DLY_VALUE4;


    DATA_TXFORMAT(47 downto 0)        <= register_map_control.GBT_DATA_TXFORMAT1(47 downto 0);
    DATA_RXFORMAT(47 downto 0)        <= register_map_control.GBT_DATA_RXFORMAT1(47 downto 0);
    DATA_TXFORMAT(95 downto 48)       <= register_map_control.GBT_DATA_TXFORMAT2(47 downto 0);
    DATA_RXFORMAT(95 downto 48)       <= register_map_control.GBT_DATA_RXFORMAT2(47 downto 0);

    TX_RESET(47 downto 0)             <= register_map_control.GBT_TX_RESET(47 downto 0);
    RX_RESET(47 downto 0)             <= register_map_control.GBT_RX_RESET(47 downto 0);
    TX_TC_METHOD(47 downto 0)         <= register_map_control.GBT_TX_TC_METHOD(47 downto 0);
    TC_EDGE(47 downto 0)              <= register_map_control.GBT_TC_EDGE(47 downto 0);
    outsel_i(47 downto 0)             <= register_map_control.GBT_OUTMUX_SEL(47 downto 0);


    g_sync_RXTXPOLARITY: for i in 0 to GBT_NUM-1 generate
        sync_TXPOLARITY : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => register_map_control.GBT_TXPOLARITY(i),
                dest_clk => GT_TX_WORD_CLK(i),
                dest_out => TXPOLARITY_TX_WORD_CLK(i)
            );
        sync_RXPOLARITY : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => register_map_control.GBT_RXPOLARITY(i),
                dest_clk => GT_RX_WORD_CLK(i),
                dest_out => RXPOLARITY_RX_WORD_CLK(i)
            );
        --sync_CHANNEL_DISABLE: xpm_cdc_single
        --   generic map (
        --      DEST_SYNC_FF => 2,
        --      INIT_SYNC_FF => 0,
        --      SIM_ASSERT_CHK => 0,
        --      SRC_INPUT_REG => 0
        --   )
        --   port map (
        --      dest_out => Channel_disable(i),
        --      dest_clk => GT_RX_WORD_CLK(i),
        --      src_clk => '0',
        --      src_in => register_map_control.GBT_CHANNEL_DISABLE(i)
        --   );
        Channel_disable(i) <= register_map_control.GBT_CHANNEL_DISABLE(i);
    end generate;

    register_map_link_monitor.GBT_VERSION.DATE             <=  GBT_VERSION(63 downto 48);
    register_map_link_monitor.GBT_VERSION.GBT_VERSION(35 downto 32)      <=  GBT_VERSION(23 downto 20);
    register_map_link_monitor.GBT_VERSION.GTH_IP_VERSION(19 downto 16)   <=  GBT_VERSION(19 downto 16);
    register_map_link_monitor.GBT_VERSION.RESERVED         <=  GBT_VERSION(15 downto 3);
    register_map_link_monitor.GBT_VERSION.GTHREFCLK_SEL    <=  (others => '0');
    register_map_link_monitor.GBT_VERSION.RX_CLK_SEL       <=  GBT_VERSION(1 downto 1);
    register_map_link_monitor.GBT_VERSION.PLL_SEL          <=  GBT_VERSION(0 downto 0);


    register_map_link_monitor.GBT_TXRESET_DONE(47 downto 0)        <= txresetdone(47 downto 0);
    register_map_link_monitor.GBT_RXRESET_DONE(47 downto 0)        <= rxresetdone(47 downto 0);
    register_map_link_monitor.GBT_TXFSMRESET_DONE(47 downto 0)     <= TXPMARESETDONE(47 downto 0);
    register_map_link_monitor.GBT_RXFSMRESET_DONE(47 downto 0)     <= RXPMARESETDONE(47 downto 0);
    register_map_link_monitor.GBT_CPLL_FBCLK_LOST(47 downto 0)     <= (others => '0'); --cpllfbclklost (47 downto 0);
    register_map_link_monitor.GBT_PLL_LOCK.CPLL_LOCK(47 downto 0)  <= (others => '0'); --cplllock(47 downto 0);
    register_map_link_monitor.GBT_PLL_LOCK.QPLL_LOCK(59 downto 48) <= qplllock(11 downto 0);
    pll_lock_latch_proc: process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            if register_map_control.GBT_PLL_LOL_LATCHED.CLEAR = "1" then
                register_map_link_monitor.GBT_PLL_LOL_LATCHED.CPLL_LOL_LATCHED <= (others => '0');
                register_map_link_monitor.GBT_PLL_LOL_LATCHED.QPLL_LOL_LATCHED <= (others => '0');
            end if;
            --for i in 0 to GBT_NUM-1 loop
            --    if cplllock(i) = '0' then
            --        register_map_link_monitor.GBT_PLL_LOL_LATCHED.CPLL_LOL_LATCHED(i) <= '1';
            --    end if;
            --end loop;
            for i in 0 to GBT_NUM/4 - 1 loop
                if qplllock(i) = '0' then
                    register_map_link_monitor.GBT_PLL_LOL_LATCHED.QPLL_LOL_LATCHED(i+48) <= '1';
                end if;
            end loop;

            if register_map_control.GBT_ALIGNMENT_LOST.CLEAR = "1" then
                register_map_link_monitor.GBT_ALIGNMENT_LOST.ALIGNMENT_LOST <= (others => '0');
            end if;
            for i in 0 to GBT_NUM-1 loop
                if alignment_done_f(i) = '0' then
                    register_map_link_monitor.GBT_ALIGNMENT_LOST.ALIGNMENT_LOST(i) <= '1';
                end if;
            end loop;
        end if;
    end process;
    register_map_link_monitor.GBT_RXCDR_LOCK(47 downto 0)          <= rxcdrlock(47 downto 0);
    register_map_link_monitor.GBT_CLK_SAMPLED(47 downto 0)         <= (others => '0'); --never written clk_sampled(47 downto 0);

    register_map_link_monitor.GBT_RX_IS_HEADER(47 downto 0)        <= rx_is_header(47 downto 0);
    register_map_link_monitor.GBT_RX_IS_DATA(47 downto 0)          <= (others => '0'); --rx_is_data(47 downto 0);
    register_map_link_monitor.GBT_RX_HEADER_FOUND(47 downto 0)     <= RX_HEADER_FOUND(47 downto 0);

    register_map_link_monitor.GBT_ALIGNMENT_DONE(47 downto 0)      <= alignment_done_f(47 downto 0);



    register_map_link_monitor.GBT_OUT_MUX_STATUS(47 downto 0)    <= outsel_o(47 downto 0);
    register_map_link_monitor.GBT_ERROR(47 downto 0)             <= error_f(47 downto 0);

    error_gen : for i in 47 downto 0 generate
        signal fec_error_cnt: std_logic_vector(31 downto 0);
    begin
        error_f(i) <= error_orig(i) and alignment_done_f(i);
        g_lt24: if i < 24 generate
            register_map_link_monitor.GT_FEC_ERR_CNT(i) <= fec_error_cnt;
        end generate;

        fecc_error_cnt_proc: process(clk40_in)
        begin
            if rising_edge(clk40_in) then
                if rst_hw = '1' then
                    fec_error_cnt <= x"0000_0000";
                else
                    if error_f(i) = '1' then
                        fec_error_cnt <= fec_error_cnt + 1;
                    end if;
                end if;
            end if;
        end process;

    end generate;

    register_map_link_monitor.GBT_GBT_TOPBOT_C(47 downto 0)      <= (others => '0');--TopBot_C(47 downto 0);


    ----------------------------------------
    ------ REGISTERS MAPPING
    ----------------------------------------
    alignment_chk_rst_i           <= General_ctrl(0);


    DESMUX_USE_SW                 <= register_map_control.GBT_MODE_CTRL.DESMUX_USE_SW(0);
    --RX_ALIGN_SW                   <= register_map_control.GBT_MODE_CTRL.RX_ALIGN_SW(1);
    --RX_ALIGN_TB_SW                <= register_map_control.GBT_MODE_CTRL.RX_ALIGN_TB_SW(2);






    -------

    datamod_gen1 : if DYNAMIC_DATA_MODE_EN='1' generate
        DATA_TXFORMAT_i <= DATA_TXFORMAT;
        DATA_RXFORMAT_i <= DATA_RXFORMAT;
    end generate;

    datamod_gen2 : if DYNAMIC_DATA_MODE_EN='0' generate
        DATA_TXFORMAT_i <= GBT_DATA_TXFORMAT_PACKAGE;
        DATA_RXFORMAT_i <= GBT_DATA_RXFORMAT_PACKAGE;
    end generate;

    process(clk40_in)
    begin
        if clk40_in'event and clk40_in='1' then
            pulse_lg <= pulse_cnt(20);
            if pulse_cnt(20)='1' then
                pulse_cnt <=(others=>'0');
            else
                pulse_cnt <= pulse_cnt+'1';
            end if;
        end if;
    end process;

    process(clk40_in)
    begin
        if clk40_in'event and clk40_in='1' then
            alignment_done_chk_cnt <= alignment_done_chk_cnt + '1';
        end if;
    end process;

    gbtModeAutoRxReset_cnt_g: for i in 0 to GBT_NUM-1 generate
        signal RXRESET_AUTO_CNT: std_logic_vector(31 downto 0);
        signal RXRESET_AUTO_p1: std_logic;
    begin
        cnt_proc: process(clk40_in)
        begin
            if rising_edge(clk40_in) then
                RXRESET_AUTO_p1 <= auto_gbt_rxrst(i);
                if rst_hw = '1' or register_map_control.GT_AUTO_RX_RESET_CNT(i mod 24).CLEAR = "1" then
                    RXRESET_AUTO_CNT <= (others => '0');
                elsif auto_gbt_rxrst(i) = '1' and RXRESET_AUTO_p1 = '0' then
                    RXRESET_AUTO_CNT <= RXRESET_AUTO_CNT + 1;
                end if;
            end if;
        end process;
        g_limit24: if i < 24 generate
            register_map_link_monitor.GT_AUTO_RX_RESET_CNT(i).VALUE <= RXRESET_AUTO_CNT;
        end generate;
    end generate;

    rxalign_auto : for i in GBT_NUM-1 downto 0 generate
        signal RxSlide_c_RX_WORD_CLOCK: std_logic;
        signal rxcdrlock_RX_WORD_CLOCK: std_logic;
        signal rxresetdone_40: std_logic;
        signal alignment_done_40: std_logic;
    begin

        process(clk40_in)
        begin
            if clk40_in'event and clk40_in='1' then
                if alignment_done_chk_cnt="0000000000000" then
                    alignment_done_a(i) <= alignment_done_40;
                else
                    alignment_done_a(i) <= alignment_done_40 and alignment_done_a(i);
                end if;
                if alignment_done_chk_cnt="0000000000000" then
                    alignment_done_f(i) <=  alignment_done_a(i);
                end if;
            end if;
        end process;


        RX_120b_out(i) <= RX_120b_out_ii(i) when alignment_done_f(i)='1'
                            else (others =>'0');

        sync_rxresetdone: xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => rxresetdone(i),
                dest_clk => clk40_in,
                dest_out => rxresetdone_40
            );

        sync_alignment_done: xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => alignment_done(i),
                dest_clk => clk40_in,
                dest_out => alignment_done_40
            );

        auto_rxrst : entity work.FELIX_GBT_RX_AUTO_RST
            port map(
                ext_trig_realign        => open, --ext_trig_realign(i),
                FSM_CLK                 => clk40_in,
                GBT_LOCK                => alignment_done_f(i), --alignment_done(i),
                pulse_lg                => pulse_lg,
                GTHRXRESET_DONE         => rxresetdone_40, -- and RxFsmResetDone(i),
                AUTO_GTH_RXRST          => auto_gth_rxrst(i),
                alignment_chk_rst       => alignment_chk_rst_c1(i),
                AUTO_GBT_RXRST          => auto_gbt_rxrst(i)
            );

        rafsm : entity work.FELIX_GBT_RXSLIDE_FSM
            port map(
                alignment_chk_rst       => alignment_chk_rst_c(i),
                --ext_trig_realign        => ext_trig_realign(i),
                FSM_RST                 => FSM_RST(i),
                FSM_CLK                 => clk40_in,
                GBT_LOCK                => alignment_done(i),
                RxSlide                 => RxSlide_c(i)
            );

        FSM_RST(i)          <= RX_RESET(i);-- or RX_ALIGN_SW;

        RX_RESET_i(i)       <= --RX_RESET(i) when RX_ALIGN_SW='1' else
                               (RX_RESET(i) or auto_gbt_rxrst(i));
        alignment_chk_rst(i)        <= --alignment_chk_rst_i when RX_ALIGN_SW='1' else
                                       (alignment_chk_rst_i or alignment_chk_rst_c(i) or alignment_chk_rst_c1(i));

        sync_RxSlide_c : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => RxSlide_c(i),
                dest_clk => GT_RX_WORD_CLK(i),
                dest_out => RxSlide_c_RX_WORD_CLOCK
            );
        sync_cdrlock: xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => rxcdrlock(i),
                dest_clk => GT_RX_WORD_CLK(i),
                dest_out => rxcdrlock_RX_WORD_CLOCK
            );
        RxSlide_i(i)             <= RxSlide_c_RX_WORD_CLOCK and rxcdrlock_RX_WORD_CLOCK;
        TX_RESET_i(i)       <= TX_RESET(i) or (not txresetdone(i));-- or (not TxFsmResetDone(i));
    end generate;

    outsel_ii             <= outsel_o when DESMUX_USE_SW = '0' else outsel_i;


    RX_FLAG_O             <= RX_FLAG_Oi(GBT_NUM-1 downto 0);

    gbtRxTx : for i in GBT_NUM-1 downto 0 generate
        signal fifo_rst_RX_WORD_CLK: std_logic;
        signal RX_RESET_i_GT_RX_WORD_CLK: std_logic;
        signal DATA_RXFORMAT_sync: std_logic_vector(1 downto 0);

        signal DATA_TXFORMAT_sync : std_logic_vector(1 downto 0);
        signal TX_TC_DLY_VALUE_sync : std_logic_vector(2 downto 0);
        signal TC_EDGE_sync: std_logic;
        signal TX_RESET_sync: std_logic;
        signal TX_TC_METHOD_sync: std_logic;
        signal TX_120b_in_wc: std_logic_vector(119 downto 0);
    begin

        sync_RXFORMAT : xpm_cdc_array_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0,
                WIDTH => 2
            )
            port map (
                src_clk => '0',
                src_in => DATA_RXFORMAT_i(2*i+1 downto 2*i),
                dest_clk => GT_RX_WORD_CLK(i),
                dest_out => DATA_RXFORMAT_sync
            );

        sync_TXFORMAT : xpm_cdc_array_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0,
                WIDTH => 2
            )
            port map (
                src_clk => '0',
                src_in => DATA_TXFORMAT_i(2*i+1 downto 2*i),
                dest_clk => GT_TX_WORD_CLK(i),
                dest_out => DATA_TXFORMAT_sync
            );

        sync_TX_TC_DLY_VALUE : xpm_cdc_array_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0,
                WIDTH => 3
            )
            port map (
                src_clk => '0',
                src_in => TX_TC_DLY_VALUE(4*i+2 downto 4*i),
                dest_clk => GT_TX_WORD_CLK(i),
                dest_out => TX_TC_DLY_VALUE_sync
            );

        sync_TC_EDGE : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => TC_EDGE(i),
                dest_clk => GT_TX_WORD_CLK(i),
                dest_out => TC_EDGE_sync
            );

        sync_TX_TC_METHOD : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => TX_TC_METHOD(i),
                dest_clk => GT_TX_WORD_CLK(i),
                dest_out => TX_TC_METHOD_sync
            );

        sync_TX_RESET : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => TX_RESET_i(i),
                dest_clk => GT_TX_WORD_CLK(i),
                dest_out => TX_RESET_sync
            );

        process(GT_RX_WORD_CLK(i))
        begin
            if GT_RX_WORD_CLK(i)'event and GT_RX_WORD_CLK(i)='1' then
                BITSLIP_MANUAL_r(i)     <= RxSlide_i(i);
                BITSLIP_MANUAL_2r(i)    <= BITSLIP_MANUAL_r(i);
                RxSlide(i)              <= BITSLIP_MANUAL_r(i) and (not BITSLIP_MANUAL_2r(i));
            end if;
        end process;


        alignment_chk_rst_f(i)      <= alignment_chk_rst(i);-- or (not RxCdrLock(i));

        sync_RX_RESET_i : xpm_cdc_sync_rst
            generic map (
                DEST_SYNC_FF => 2,
                INIT => 1,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0
            )
            port map (
                src_rst => RX_RESET_i(i),
                dest_clk => GT_RX_WORD_CLK(i),
                dest_rst => RX_RESET_i_GT_RX_WORD_CLK
            );

        sync_TX_120b_in : xpm_cdc_array_single
            generic map (
                DEST_SYNC_FF => 6, --Equivalent to 1 40 MHz cycle.
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0,
                WIDTH => 84
            )
            port map (
                src_clk => clk40_in,
                src_in => TX_120b_in(i)(115 downto 32),
                dest_clk => GT_TX_WORD_CLK(i),
                dest_out => TX_120b_in_wc(115 downto 32)
            );
        p_TX_header: process(clk40_in)
        begin
            if rising_edge(clk40_in) then
                TX_120b_in_wc(119 downto 116) <= TX_120b_in(i)(119 downto 116);
            end if;
        end process;
        TX_120b_in_wc(31 downto 0) <= x"0000_0000"; --No WideMode supported.

        gbtTxRx_inst: entity work.gbtTxRx_FELIX
            generic map(
                channel => i
            )
            port map(
                alignment_chk_rst => alignment_chk_rst_f(i),
                alignment_done_O => alignment_done(i),
                outsel_i => outsel_ii(i),
                outsel_o => outsel_o(i),
                error_o => error_orig(i),
                TX_TC_DLY_VALUE => TX_TC_DLY_VALUE_sync,
                TX_TC_METHOD => TX_TC_METHOD_sync,
                TC_EDGE => TC_EDGE_sync,
                RX_FLAG => RX_FLAG_Oi(i), --RX_FLAG_O(i),
                TX_FLAG => TX_FLAG_O(i),
                --Tx_latopt_scr => '1', --TX_OPT(24+i),
                --Tx_latopt_tc => '1', --TX_OPT(i),
                RX_LATOPT_DES => '1', --RX_OPT(i),
                Tx_DATA_FORMAT => DATA_TXFORMAT_sync,
                Rx_Data_Format => DATA_RXFORMAT_sync,
                RX_RESET_I => RX_RESET_i_GT_RX_WORD_CLK,
                RX_FRAME_CLK_O => open, --RX_FRAME_CLK_O(i),
                RX_HEADER_FOUND => RX_HEADER_FOUND(i),
                RX_WORD_IS_HEADER_O => rx_is_header(i),
                RX_WORDCLK_I => GT_RX_WORD_CLK(i),
                L40M => clk40_in,
                --RX_ISDATA_FLAG_O        => rx_is_data(i),
                RX_DATA_20b_I => RX_DATA_20b(i),
                RX_DATA_120b_O => RX_120b_out_i(i),
                TX_RESET_I => TX_RESET_sync,
                TX_FRAMECLK_I => TX_FRAME_CLK_I(i),
                des_rxusrclk => GT_RX_WORD_CLK(i),
                TX_WORDCLK_I => GT_TX_WORD_CLK(i),
                --TX_ISDATA_SEL_I    => TX_IS_DATA(i),
                TX_DATA_120b_I => TX_120b_in_wc,
                TX_DATA_20b_O => TX_DATA_20b(i)
            );

        fifo_rst(i) <= rst_hw or (not alignment_done_f(i)) or RX_RESET_i(i) or General_ctrl(4);
        fifo_rden(i) <= not fifo_empty(i);

        sync_fifo_rst : xpm_cdc_sync_rst
            generic map (
                DEST_SYNC_FF => 2,
                INIT => 1,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0
            )
            port map (
                src_rst => fifo_rst(i),
                -- is registered.

                dest_clk => GT_RX_WORD_CLK(i),
                dest_rst => fifo_rst_RX_WORD_CLK  -- 1-bit output: src_rst synchronized to the destination clock domain. This output
            );

        fifo_inst: xpm_fifo_async
            generic map(
                FIFO_MEMORY_TYPE => "block",
                FIFO_WRITE_DEPTH => 32,
                CASCADE_HEIGHT => 0,
                RELATED_CLOCKS => 0,
                WRITE_DATA_WIDTH => 120,
                READ_MODE => "std",
                FIFO_READ_LATENCY => 1,
                FULL_RESET_VALUE => 0,
                USE_ADV_FEATURES => "0200", --only programmable empty
                READ_DATA_WIDTH => 120,
                CDC_SYNC_STAGES => 2,
                WR_DATA_COUNT_WIDTH => 5,
                PROG_FULL_THRESH => 28,
                RD_DATA_COUNT_WIDTH => 5,
                PROG_EMPTY_THRESH => 5,
                DOUT_RESET_VALUE => "0",
                ECC_MODE => "no_ecc",
                SIM_ASSERT_CHK => 0,
                WAKEUP_TIME => 0
            )
            port map(
                sleep => '0',
                rst => fifo_rst_RX_WORD_CLK, --rst_hw,
                wr_clk => GT_RX_WORD_CLK(i),
                wr_en => RX_FLAG_Oi(i),
                din => RX_120b_out_i(i),
                full => open,
                prog_full => open,
                wr_data_count => open,
                overflow => open,
                wr_rst_busy => open,
                almost_full => open,
                wr_ack => open,
                rd_clk => clk40_in, --FIFO_RD_CLK(i),
                rd_en => fifo_rden(i), --not fifo_empty(i),--'1',--FIFO_RD_EN(i),
                dout => RX_120b_out_ii(i),
                empty => open,
                prog_empty => fifo_empty(i), --FIFO_EMPTY(i)
                rd_data_count => open,
                underflow => open,
                rd_rst_busy => open,
                almost_empty => open,
                data_valid => open,
                injectsbiterr => '0',
                injectdbiterr => '0',
                sbiterr => open,
                dbiterr => open
            );

    end generate;





    -------------------------------
    ------ GTH TOP WRAPPER
    -------------------------------

    clk_generate : for i in GBT_NUM-1 downto 0 generate



        GT_TX_WORD_CLK(i) <= GT_TXUSRCLK(i);

        GT_RX_WORD_CLK(i) <= GT_RXUSRCLK(i);
        RXUSRCLK_OUT(i)   <= GT_RX_WORD_CLK(i);
    end generate;

    port_trans : for i in GBT_NUM-1 downto 0 generate
        RX_N_i(i)   <= RX_N(i);
        RX_P_i(i)   <= RX_P(i);
        TX_N(i)     <= TX_N_i(i);
        TX_P(i)     <= TX_P_i(i);

    end generate;

    GTH_inst : for i in (GBT_NUM-1)/4 downto 0 generate
        signal GT_RX0_EXT_0_ch_rxdata: std_logic_vector(127 downto 0);
        signal GT_RX1_EXT_0_ch_rxdata: std_logic_vector(127 downto 0);
        signal GT_RX2_EXT_0_ch_rxdata: std_logic_vector(127 downto 0);
        signal GT_RX3_EXT_0_ch_rxdata: std_logic_vector(127 downto 0);
        signal GT_TX0_EXT_0_ch_txdata: std_logic_vector(127 downto 0);
        signal GT_TX1_EXT_0_ch_txdata: std_logic_vector(127 downto 0);
        signal GT_TX2_EXT_0_ch_txdata: std_logic_vector(127 downto 0);
        signal GT_TX3_EXT_0_ch_txdata: std_logic_vector(127 downto 0);
        signal rx_resetdone_out_gt_bridge_ip_0 : std_logic;
        signal tx_resetdone_out_gt_bridge_ip_0 : std_logic;
        signal rpll_lock_gt_bridge_ip_0: std_logic;
        signal lcpll_lock_gt_bridge_ip_0: std_logic_vector(1 downto 0);
    begin

        RX_DATA_20b(4*i+0) <= GT_RX0_EXT_0_ch_rxdata(19 downto 0);
        RX_DATA_20b(4*i+1) <= GT_RX1_EXT_0_ch_rxdata(19 downto 0);
        RX_DATA_20b(4*i+2) <= GT_RX2_EXT_0_ch_rxdata(19 downto 0);
        RX_DATA_20b(4*i+3) <= GT_RX3_EXT_0_ch_rxdata(19 downto 0);

        GT_TX0_EXT_0_ch_txdata(19 downto 0) <= TX_DATA_20b(4*i+0);
        GT_TX1_EXT_0_ch_txdata(19 downto 0) <= TX_DATA_20b(4*i+1);
        GT_TX2_EXT_0_ch_txdata(19 downto 0) <= TX_DATA_20b(4*i+2);
        GT_TX3_EXT_0_ch_txdata(19 downto 0) <= TX_DATA_20b(4*i+3);
        GT_TX0_EXT_0_ch_txdata(127 downto 20) <= (others => '0');
        GT_TX1_EXT_0_ch_txdata(127 downto 20) <= (others => '0');
        GT_TX2_EXT_0_ch_txdata(127 downto 20) <= (others => '0');
        GT_TX3_EXT_0_ch_txdata(127 downto 20) <= (others => '0');

        -- Channels are flipped to use the same pinout as the FLX-712
        -- Transceiver channel -> Data
        -- CH0 -> CH3
        -- CH1 -> CH2
        -- CH2 -> CH1
        -- CH3 -> CH0
        GTH_TOP_INST: transceiver_versal_gbt_wrapper
            Port map(
                GT_REFCLK0_clk_n => GTREFCLK_N_IN(i downto i),
                GT_REFCLK0_clk_p => GTREFCLK_P_IN(i downto i),
                GT_Serial_grx_n => RX_N_i(4*i+3 downto 4*i),
                GT_Serial_grx_p => RX_P_i(4*i+3 downto 4*i),
                GT_Serial_gtx_n => TX_N_i(4*i+3 downto 4*i),
                GT_Serial_gtx_p => TX_P_i(4*i+3 downto 4*i),
                apb3clk_gt_bridge_ip_0 => apb3_axi_clk,
                ch0_loopback_0 => register_map_control.GTH_LOOPBACK_CONTROL,
                ch0_rxbyteisaligned_ext_0 => open,
                ch0_rxcdrlock_ext_0(0) => rxcdrlock_out(4*i+3),
                ch0_rxctrl0_ext_0 => open,
                ch0_rxctrl1_ext_0 => open,
                ch0_rxctrl2_ext_0 => open,
                ch0_rxctrl3_ext_0 => open,
                ch0_rxdata_ext_0 => GT_RX3_EXT_0_ch_rxdata,
                ch0_rxdatavalid_ext_0 => open,
                ch0_rxgearboxslip_ext_0 => '0',
                ch0_rxheader_ext_0 => open,
                ch0_rxheadervalid_ext_0 => open,
                ch0_rxpolarity_ext_0 => RXPOLARITY_RX_WORD_CLK(4*i+3),
                ch0_rxresetdone_ext_0 => open,
                ch0_rxslide_ext_0 => RxSlide(4*i+3),
                ch0_rxvalid_ext_0 => open,
                ch0_txctrl0_ext_0 => (others => '0'),
                ch0_txctrl1_ext_0 => (others => '0'),
                ch0_txctrl2_ext_0 => (others => '0'),
                ch0_txdata_ext_0 => GT_TX3_EXT_0_ch_txdata,
                ch0_txheader_ext_0 => "000000",
                ch0_txpolarity_ext_0 => TXPOLARITY_TX_WORD_CLK(4*i+3),
                ch0_txresetdone_ext_0(0) => TXPMARESETDONE(4*i+3),
                ch0_txsequence_ext_0 => "0000000",
                ch1_loopback_0 => register_map_control.GTH_LOOPBACK_CONTROL,
                ch1_rxbyteisaligned_ext_0 => open,
                ch1_rxcdrlock_ext_0(0) => rxcdrlock_out(4*i+2),
                ch1_rxctrl0_ext_0 => open,
                ch1_rxctrl1_ext_0 => open,
                ch1_rxctrl2_ext_0 => open,
                ch1_rxctrl3_ext_0 => open,
                ch1_rxdata_ext_0 => GT_RX2_EXT_0_ch_rxdata,
                ch1_rxdatavalid_ext_0 => open,
                ch1_rxgearboxslip_ext_0 => '0',
                ch1_rxheader_ext_0 => open,
                ch1_rxheadervalid_ext_0 => open,
                ch1_rxpolarity_ext_0 => RXPOLARITY_RX_WORD_CLK(4*i+2),
                ch1_rxresetdone_ext_0 => open,
                ch1_rxslide_ext_0 => RxSlide(4*i+2),
                ch1_rxvalid_ext_0 => open,
                ch1_txctrl0_ext_0 => (others => '0'),
                ch1_txctrl1_ext_0 => (others => '0'),
                ch1_txctrl2_ext_0 => (others => '0'),
                ch1_txdata_ext_0 => GT_TX2_EXT_0_ch_txdata,
                ch1_txpolarity_ext_0 => TXPOLARITY_TX_WORD_CLK(4*i+2),
                ch1_txresetdone_ext_0(0) => TXPMARESETDONE(4*i+2),
                ch1_txsequence_ext_0 => "0000000",
                ch2_loopback_0 => register_map_control.GTH_LOOPBACK_CONTROL,
                ch2_rxbyteisaligned_ext_0 => open,
                ch2_rxcdrlock_ext_0(0) => rxcdrlock_out(4*i+1),
                ch2_rxctrl0_ext_0 => open,
                ch2_rxctrl1_ext_0 => open,
                ch2_rxctrl2_ext_0 => open,
                ch2_rxctrl3_ext_0 => open,
                ch2_rxdata_ext_0 => GT_RX1_EXT_0_ch_rxdata,
                ch2_rxdatavalid_ext_0 => open,
                ch2_rxgearboxslip_ext_0 => '0',
                ch2_rxheader_ext_0 => open,
                ch2_rxheadervalid_ext_0 => open,
                ch2_rxpolarity_ext_0 => RXPOLARITY_RX_WORD_CLK(4*i+1),
                ch2_rxresetdone_ext_0 => open,
                ch2_rxslide_ext_0 => RxSlide(4*i+1),
                ch2_rxvalid_ext_0 => open,
                ch2_txctrl0_ext_0 => (others => '0'),
                ch2_txctrl1_ext_0 => (others => '0'),
                ch2_txctrl2_ext_0 => (others => '0'),
                ch2_txdata_ext_0 => GT_TX1_EXT_0_ch_txdata,
                ch2_txpolarity_ext_0 => TXPOLARITY_TX_WORD_CLK(4*i+1),
                ch2_txresetdone_ext_0(0) => TXPMARESETDONE(4*i+1),
                ch2_txsequence_ext_0 => "0000000",
                ch3_loopback_0 => register_map_control.GTH_LOOPBACK_CONTROL,
                ch3_rxbyteisaligned_ext_0 => open,
                ch3_rxcdrlock_ext_0(0) => rxcdrlock_out(4*i+0),
                ch3_rxctrl0_ext_0 => open,
                ch3_rxctrl1_ext_0 => open,
                ch3_rxctrl2_ext_0 => open,
                ch3_rxctrl3_ext_0 => open,
                ch3_rxdata_ext_0 => GT_RX0_EXT_0_ch_rxdata,
                ch3_rxdatavalid_ext_0 => open,
                ch3_rxgearboxslip_ext_0 => '0',
                ch3_rxheader_ext_0 => open,
                ch3_rxheadervalid_ext_0 => open,
                ch3_rxpolarity_ext_0 => RXPOLARITY_RX_WORD_CLK(4*i+0),
                ch3_rxresetdone_ext_0 => open,
                ch3_rxslide_ext_0 => RxSlide(4*i+1),
                ch3_rxvalid_ext_0 => open,
                ch3_txctrl0_ext_0 => (others => '0'),
                ch3_txctrl1_ext_0 => (others => '0'),
                ch3_txctrl2_ext_0 => (others => '0'),
                ch3_txdata_ext_0 => GT_TX0_EXT_0_ch_txdata,
                ch3_txpolarity_ext_0 => TXPOLARITY_TX_WORD_CLK(4*i+0),
                ch3_txresetdone_ext_0(0) => TXPMARESETDONE(4*i+0),
                ch3_txsequence_ext_0 => "0000000",
                gt_reset_gt_bridge_ip_0 => soft_reset_f(i),
                reset_rx_datapath_in_0 => gtrx_reset_merge(i),
                reset_rx_pll_and_datapath_in_0 => qpll_reset(i),
                reset_tx_datapath_in_0 => gttx_reset_merge(i),
                reset_tx_pll_and_datapath_in_0 => qpll_reset(i),
                rx_resetdone_out_gt_bridge_ip_0 => rx_resetdone_out_gt_bridge_ip_0,
                rxusrclk0 => GT_RXUSRCLK(i*4+3),
                rxusrclk1 => GT_RXUSRCLK(i*4+2),
                rxusrclk2 => GT_RXUSRCLK(i*4+1),
                rxusrclk3 => GT_RXUSRCLK(i*4+0),

                txusrclk0 => GT_TXUSRCLK(i*4+3),
                txusrclk1 => GT_TXUSRCLK(i*4+2),
                txusrclk2 => GT_TXUSRCLK(i*4+1),
                txusrclk3 => GT_TXUSRCLK(i*4+0),
                --rxusrclk_gt_bridge_ip_0 => GT_RXUSRCLK(i),
                tx_resetdone_out_gt_bridge_ip_0 => tx_resetdone_out_gt_bridge_ip_0,
                QUAD0_hsclk0_lcplllock_0 => lcpll_lock_gt_bridge_ip_0(1),
                QUAD0_hsclk1_lcplllock_0 => lcpll_lock_gt_bridge_ip_0(0)
            --txusrclk_gt_bridge_ip_0 => GT_TXUSRCLK(i)
            --loopback_in                  => register_map_control.GTH_LOOPBACK_CONTROL,
            --userclk_rx_reset_in          => userclk_rx_reset_in(i downto i), --(others=>(not rxpmaresetdone_out(i))),--locked,
            --userclk_tx_reset_in          => userclk_tx_reset_in(i downto i), --(others=>(not txpmaresetdone_out(i))),--,--locked,


            );

        txresetdone(4*i+3 downto 4*i) <= (others => tx_resetdone_out_gt_bridge_ip_0);
        rxresetdone(4*i+3 downto 4*i) <= (others => rx_resetdone_out_gt_bridge_ip_0);
        qplllock(i) <= lcpll_lock_gt_bridge_ip_0(0) or lcpll_lock_gt_bridge_ip_0(1);


        process(clk40_in)
        begin
            if clk40_in'event and clk40_in='1' then
                if cdr_cnt ="00000000000000000000" then
                    rxcdrlock_a(4*i)     <= rxcdrlock_out(4*i);
                    rxcdrlock_a(4*i+1)   <= rxcdrlock_out(4*i+1);
                    rxcdrlock_a(4*i+2)   <= rxcdrlock_out(4*i+2);
                    rxcdrlock_a(4*i+3)   <= rxcdrlock_out(4*i+3);
                else
                    rxcdrlock_a(4*i) <= rxcdrlock_a(4*i) and rxcdrlock_out(4*i);
                    rxcdrlock_a(4*i+1) <= rxcdrlock_a(4*i+1) and rxcdrlock_out(4*i+1);
                    rxcdrlock_a(4*i+2) <= rxcdrlock_a(4*i+2) and rxcdrlock_out(4*i+2);
                    rxcdrlock_a(4*i+3) <= rxcdrlock_a(4*i+3) and rxcdrlock_out(4*i+3);
                end if;
                if cdr_cnt="00000000000000000000" then
                    RxCdrLock_int(4*i) <=rxcdrlock_a(4*i);
                    RxCdrLock_int(4*i+1) <=rxcdrlock_a(4*i+1);
                    RxCdrLock_int(4*i+2) <=rxcdrlock_a(4*i+2);
                    RxCdrLock_int(4*i+3) <=rxcdrlock_a(4*i+3);
                end if;
            end if;
        end process;
        rxcdrlock(4*i) <= (not Channel_disable(4*i)) and RxCdrLock_int(4*i);
        rxcdrlock(4*i+1) <= (not Channel_disable(4*i+1)) and RxCdrLock_int(4*i+1);
        rxcdrlock(4*i+2) <= (not Channel_disable(4*i+2)) and RxCdrLock_int(4*i+2);
        rxcdrlock(4*i+3) <= (not Channel_disable(4*i+3)) and RxCdrLock_int(4*i+3);

        soft_reset_f(i) <= soft_reset(i) or qpll_reset(i);--or rst_hw;-- or GTRX_RESET(i);

        --userclk_rx_reset_in(i) <=not (RXPMARESETDONE(4*i+0) or RXPMARESETDONE(4*i+1) or RXPMARESETDONE(4*i+2) or RXPMARESETDONE(4*i+3));
        --userclk_tx_reset_in(i) <=not (TXPMARESETDONE(4*i+0) or TXPMARESETDONE(4*i+1) or TXPMARESETDONE(4*i+2) or TXPMARESETDONE(4*i+3));

        gttx_reset_merge(i) <= gttx_reset(4*i) or gttx_reset(4*i+1) or gttx_reset(4*i+2) or gttx_reset(4*i+3);
        gtrx_reset_merge(i) <= (gtrx_reset(4*i) or (auto_gth_rxrst(4*i) and rxcdrlock(4*i)))
                               or (gtrx_reset(4*i+1) or (auto_gth_rxrst(4*i+1) and rxcdrlock(4*i+1)))
                               or (gtrx_reset(4*i+2) or (auto_gth_rxrst(4*i+2) and rxcdrlock(4*i+2)))
                               or (gtrx_reset(4*i+3) or (auto_gth_rxrst(4*i+3) and rxcdrlock(4*i+3))) ;
    --GTRX_RESET_MERGE(i) <= GTRX_RESET(4*i) or GTRX_RESET(4*i+1) or GTRX_RESET(4*i+2) or GTRX_RESET(4*i+3);

    -- CpllLock(i) <= '1';

    end generate;


    process(clk40_in)
    begin
        if clk40_in'event and clk40_in='1' then
            cdr_cnt <=cdr_cnt+'1';
        end if;
    end process;

end Behavioral;

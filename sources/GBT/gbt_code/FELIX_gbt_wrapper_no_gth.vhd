--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               RHabraken
--!               Mesfin Gebyehu
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen    (kchen@bnl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2014/12/05 04:43:14 PM
-- Design Name: GBT TxRx Top
-- Module Name: gbt_top - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions: Vivado
-- Description:
--              The TOP MODULE FOR FELIX GBT
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.FELIX_gbt_package.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
-- use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
    use UNISIM.VComponents.all;

library XPM;
    use XPM.VComponents.all;

entity FELIX_gbt_wrapper_no_gth is
    Port
    (
        alignment_chk_rst         : in std_logic;
        alignment_done_O          : out std_logic;

        outsel_i                  : in std_logic;
        outsel_o                  : out std_logic;
        error_o                   : out std_logic;
        --OddEven                   : in std_logic;
        --TopBot            : in std_logic;
        --data_sel                  : in std_logic_vector(3 downto 0);
        RX_FLAG                   : out std_logic;
        Rx_Data_Format            : IN STD_LOGIC_VECTOR(1 DOWNTO 0);

        RX_RESET_I      : in  std_logic;
        RX_FRAME_CLK_I     : in std_logic;
        RX_HEADER_FOUND     : out std_logic;
        RX_WORD_IS_HEADER_O   : out std_logic;
        RX_WORDCLK_I     : in std_logic;
        --RX_ISDATA_FLAG_O      : out std_logic;

        RX_DATA_20b_I         : in  std_logic_vector(19 downto 0);
        RX_DATA_120b_O         : out std_logic_vector(119 downto 0);

        des_rxusrclk              : in std_logic;
        reset_RX_40M_FrameClk_BUFG : out std_logic
    );

end FELIX_gbt_wrapper_no_gth;

architecture Behavioral of FELIX_gbt_wrapper_no_gth is
    --=========--

    signal alignment_done         : std_logic;
    signal alignment_done_2r      : std_logic;
    signal alignment_done_3r      : std_logic;
    signal alignment_done_4r      : std_logic;
    signal alignment_done_5r      : std_logic;
    signal alignment_done_6r      : std_logic;
    signal alignment_done_7r      : std_logic;
    signal alignment_done_r       : std_logic;
    signal header_found           : std_logic;
    signal HeaderFlag             : std_logic;
    signal outsel_gen             : std_logic;
    signal error_orig                  : std_logic;
    signal error_i                : std_logic;
    signal alignment_chk_rst_r    : std_logic;
    signal alignment_chk_rst_2r   : std_logic;
    signal RX_DATA_120b_Oi        : std_logic_vector(119 downto 0);
    signal RX_DATA_120b_O_r       : std_logic_vector(119 downto 0);
    signal header_found_L40M : std_logic;
    signal error_orig_L40M : std_logic;


--signal RX_FRAME_CLK_L40M : std_logic;

begin
    -- -- BitSlip Genaration
    --  process(RX_WORDCLK_I)
    --  begin
    --    if RX_WORDCLK_I'event and RX_WORDCLK_I='1' then
    --      BITSLIP_MANUAL_r <= BITSLIP_MANUAL;
    --      BITSLIP_MANUAL_2r <= BITSLIP_MANUAL_r;
    --      GT_RXSLIDE <= BITSLIP_MANUAL_r and (not BITSLIP_MANUAL_2r);
    --    end if;
    --  end process;

    --- FELIX RX top
    ---
    outsel_o      <= outsel_gen;

    process(RX_FRAME_CLK_I)
    begin
        if RX_FRAME_CLK_I'event and RX_FRAME_CLK_I='1' then
            if RX_RESET_I = '1' then
                outsel_gen      <= '0';
            else
                outsel_gen      <= outsel_gen;
            end if;
        end if;

    end process;


    desmux_en : if RX_DESCR_MUX_EN = '1' generate
        process(RX_FRAME_CLK_I)
        begin
            if RX_FRAME_CLK_I'event and RX_FRAME_CLK_I = '0' then
                RX_DATA_120b_O_r        <= RX_DATA_120b_Oi;
            end if;
        end process;
        RX_DATA_120b_O              <= RX_DATA_120b_Oi when outsel_i='1' else RX_DATA_120b_O_r;
    end generate;

    desmux_un : if RX_DESCR_MUX_EN = '0' generate
        RX_DATA_120b_O              <= RX_DATA_120b_Oi;
    end generate;
    --RX_FLAG <= RX_FRAME_CLK;
    --TX_FLAG <= L40M;

    gbtRx_inst: entity work.gbtRx_FMEMU
        Port Map(
            --RX_FRAME_CLK_I => RX_FRAME_CLK_I,
            RX_FLAG => RX_FLAG,
            error_o => error_orig,
            RX_RESET_I => RX_RESET_I,
            --data_sel => data_sel,
            RX_WORDCLK_I => RX_WORDCLK_I,
            des_rxusrclk => des_rxusrclk,
            --OddEven => OddEven,
            --TopBot => TopBot,
            HeaderFlag => HeaderFlag,
            header_found => header_found,
            Rx_Data_Format => Rx_Data_Format,
            --RX_ISDATA_FLAG_O => RX_ISDATA_FLAG_O,
            RX_DATA_20b_I => RX_DATA_20b_I,
            RX_HEADER_O => RX_DATA_120b_Oi(119 downto 116),
            RX_DATA_84b_O => RX_DATA_120b_Oi(115 downto 32),
            RX_EXTRA_DATA_WIDEBUS_O => RX_DATA_120b_Oi(31 downto 0),
            reset_RX_40M_FrameClk_BUFG => reset_RX_40M_FrameClk_BUFG
        );

    RX_HEADER_FOUND       <= header_found;

    RX_WORD_IS_HEADER_O   <= HeaderFlag;

    alignment_done_O      <= alignment_done;

    --  process(RX_FRAME_CLK)
    --  begin
    --  if RX_FRAME_CLK'event and RX_FRAME_CLK = '1' then
    --    if alignment_chk_rst = '1' then
    --        alignment_done        <= '1';
    --    elsif header_found = '0' then
    --        alignment_done        <= '0';
    --    else
    --        alignment_done        <= alignment_done;
    --    end if;
    --    end if;
    --  end process;
    --cdc_RX_FRAME_CLK: xpm_cdc_single generic map(
    --        DEST_SYNC_FF => 2,
    --        INIT_SYNC_FF => 0,
    --        SIM_ASSERT_CHK => 0,
    --        SRC_INPUT_REG => 0
    --    ) port map(
    --        src_clk => '0',
    --        src_in => RX_FRAME_CLK,
    --        dest_clk => L40M,
    --        dest_out => RX_FRAME_CLK_L40M
    --    );

    cdc_header_found: xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        ) port map(
            src_clk => '0',
            src_in => header_found,
            dest_clk => RX_FRAME_CLK_I,
            dest_out => header_found_L40M
        );

    cdc_error_orig: xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        ) port map(
            src_clk => '0',
            src_in => error_orig,
            dest_clk => RX_FRAME_CLK_I,
            dest_out => error_orig_L40M
        );


    process(RX_FRAME_CLK_I)
    begin
        if RX_FRAME_CLK_I'event and RX_FRAME_CLK_I = '1' then
            alignment_done_r  <= alignment_done;
            alignment_done_2r <= alignment_done_r;
            alignment_done_3r <= alignment_done_2r;
            alignment_done_4r <= alignment_done_3r;
            alignment_done_5r <= alignment_done_4r;
            alignment_done_6r <= alignment_done_5r;
            alignment_done_7r <= alignment_done_6r;
            alignment_chk_rst_r <= alignment_chk_rst;
            alignment_chk_rst_2r <= alignment_chk_rst_r;
            if alignment_chk_rst_2r = '0' and alignment_chk_rst_r = '1' then
                alignment_done  <= '1';

            elsif header_found_L40M = '0' then
                alignment_done  <= '0';
            else
                alignment_done  <= alignment_done;
            end if;

            if alignment_chk_rst = '1' or alignment_done_7r = '0' then

                error_i         <= '0';
            elsif error_orig_L40M = '1' then
                error_i         <= '1';
            else
                error_i         <= error_i;
            end if;
        end if;
    end process;
    error_o <= error_i;

end Behavioral;

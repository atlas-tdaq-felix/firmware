--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Kai Chen
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen    (kchen@bnl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2014/12/05 04:43:14 PM
-- Design Name: V7 QPLL GTH Wrapper (Tx Low Latency, RX with inside buffer)
-- Module Name: GTH_QPLL_Wrapper_V7 - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions: Vivado
-- Description:
--              The TOP MODULE: V7 QPLL GTH (for GBT)
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------


library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
library UNISIM;
    use UNISIM.VCOMPONENTS.ALL;


--***************************** Entity Declaration ****************************
entity GTH_QPLL_Wrapper_V7 is

    port
    (
        GTH_RefClk                  : in   std_logic;
        DRP_CLK_IN                        : in   std_logic;

        --- RX clock, for each channel
        gt0_rxusrclk_in                   : in   std_logic;
        gt0_rxoutclk_out                  : out  std_logic;

        gt1_rxusrclk_in                   : in   std_logic;
        gt1_rxoutclk_out                  : out  std_logic;

        gt2_rxusrclk_in                   : in   std_logic;
        gt2_rxoutclk_out                  : out  std_logic;

        gt3_rxusrclk_in                   : in   std_logic;
        gt3_rxoutclk_out                  : out  std_logic;

        --- TX clock, shared by all channels
        gt0_txusrclk_in                   : in   std_logic;
        gt0_txoutclk_out                  : out  std_logic;

        --gt1_txusrclk_in                   : in   std_logic;
        gt1_txoutclk_out                  : out  std_logic;

        --gt2_txusrclk_in                   : in   std_logic;
        gt2_txoutclk_out                  : out  std_logic;

        --gt3_txusrclk_in                   : in   std_logic;
        gt3_txoutclk_out                  : out  std_logic;
        -----------------------------------------
        ---- STATUS signals
        -----------------------------------------
        gt_txresetdone_out                : out  std_logic_vector(3 downto 0);
        gt_rxresetdone_out                : out  std_logic_vector(3 downto 0);

        gt_txfsmresetdone_out             : out  std_logic_vector(3 downto 0);
        gt_rxfsmresetdone_out             : out  std_logic_vector(3 downto 0);

        gt_cpllfbclklost_out              : out  std_logic_vector(3 downto 0);
        gt_cplllock_out                   : out  std_logic_vector(3 downto 0);

        gt_rxcdrlock_out                  : out  std_logic_vector(3 downto 0);
        gt_qplllock_out                   : out  std_logic;
        ---------------------------
        ---- CTRL signals
        ---------------------------
        gt_rxslide_in                     : in   std_logic_vector(3 downto 0);
        gt_txpolarity_in                  : in   std_logic_vector(3 downto 0);
        gt_rxpolarity_in                  : in   std_logic_vector(3 downto 0);
        gt_txuserrdy_in                   : in   std_logic_vector(3 downto 0);
        gt_rxuserrdy_in                   : in   std_logic_vector(3 downto 0);

        gt0_loopback_in                   : in std_logic_vector(2 downto 0);
        gt0_rxcdrhold_in                  : in std_logic;
        gt1_loopback_in                   : in std_logic_vector(2 downto 0);
        gt1_rxcdrhold_in                  : in std_logic;
        gt2_loopback_in                   : in std_logic_vector(2 downto 0);
        gt2_rxcdrhold_in                  : in std_logic;
        gt3_loopback_in                   : in std_logic_vector(2 downto 0);
        gt3_rxcdrhold_in                  : in std_logic;

        ----------------------------------------------------------------
        ----------RESET SIGNALs
        ----------------------------------------------------------------

        --SOFT_RESET_IN                             : in   std_logic;
        GTTX_RESET_IN                             : in   std_logic_vector(3 downto 0);
        GTRX_RESET_IN                             : in   std_logic_vector(3 downto 0);
        --CPLL_RESET_IN                             : in   std_logic_vector(3 downto 0);
        QPLL_RESET_IN                             : in   std_logic;

        --SOFT_TXRST_GT                             : in   std_logic_vector(3 downto 0);
        --SOFT_RXRST_GT                             : in   std_logic_vector(3 downto 0);
        SOFT_TXRST_ALL                            : in   std_logic;
        SOFT_RXRST_ALL                            : in   std_logic;

        -----------------------------------------------------------
        ----------- Data and TX/RX Ports
        -----------------------------------------------------------

        RX_DATA_gt0_20b                   : out std_logic_vector(19 downto 0);
        TX_DATA_gt0_20b                   : in std_logic_vector(19 downto 0);
        RX_DATA_gt1_20b                   : out std_logic_vector(19 downto 0);
        TX_DATA_gt1_20b                   : in std_logic_vector(19 downto 0);
        RX_DATA_gt2_20b                   : out std_logic_vector(19 downto 0);
        TX_DATA_gt2_20b                   : in std_logic_vector(19 downto 0);
        RX_DATA_gt3_20b                   : out std_logic_vector(19 downto 0);
        TX_DATA_gt3_20b                   : in std_logic_vector(19 downto 0);

        RXN_IN                                  : in   std_logic_vector(3 downto 0);
        RXP_IN                                  : in   std_logic_vector(3 downto 0);
        TXN_OUT                                 : out  std_logic_vector(3 downto 0);
        TXP_OUT                                 : out  std_logic_vector(3 downto 0)


    );
end GTH_QPLL_Wrapper_V7;

architecture RTL of GTH_QPLL_Wrapper_V7 is
    attribute DowngradeIPIdentifiedWarnings: string;
    attribute DowngradeIPIdentifiedWarnings of RTL : architecture is "yes";

    attribute X_CORE_INFO : string;
    attribute X_CORE_INFO of RTL : architecture is "gtwizard_qpll_4p8g_4ch,gtwizard_v3_4,{protocol_file=Start_from_scratch}";
    attribute CORE_GENERATION_INFO : string;
    attribute CORE_GENERATION_INFO of RTL : architecture is "gtwizard_qpll_4p8g_4ch,gtwizard_v3_4,{protocol_file=Start_from_scratch}";

    --**************************Component Declarations*****************************


    component gtwizard_QPLL_4p8g_V7
        port(
            SYSCLK_IN                   : in  STD_LOGIC;
            SOFT_RESET_TX_IN            : in  STD_LOGIC;
            SOFT_RESET_RX_IN            : in  STD_LOGIC;
            DONT_RESET_ON_DATA_ERROR_IN : in  STD_LOGIC;
            GT0_DRP_BUSY_OUT            : out STD_LOGIC;
            GT0_TX_FSM_RESET_DONE_OUT   : out STD_LOGIC;
            GT0_RX_FSM_RESET_DONE_OUT   : out STD_LOGIC;
            GT0_DATA_VALID_IN           : in  STD_LOGIC;
            GT1_DRP_BUSY_OUT            : out STD_LOGIC;
            GT1_TX_FSM_RESET_DONE_OUT   : out STD_LOGIC;
            GT1_RX_FSM_RESET_DONE_OUT   : out STD_LOGIC;
            GT1_DATA_VALID_IN           : in  STD_LOGIC;
            GT2_DRP_BUSY_OUT            : out STD_LOGIC;
            GT2_TX_FSM_RESET_DONE_OUT   : out STD_LOGIC;
            GT2_RX_FSM_RESET_DONE_OUT   : out STD_LOGIC;
            GT2_DATA_VALID_IN           : in  STD_LOGIC;
            GT3_DRP_BUSY_OUT            : out STD_LOGIC;
            GT3_TX_FSM_RESET_DONE_OUT   : out STD_LOGIC;
            GT3_RX_FSM_RESET_DONE_OUT   : out STD_LOGIC;
            GT3_DATA_VALID_IN           : in  STD_LOGIC;
            gt0_drpaddr_in              : in  STD_LOGIC_VECTOR(8 downto 0);
            gt0_drpclk_in               : in  STD_LOGIC;
            gt0_drpdi_in                : in  STD_LOGIC_VECTOR(15 downto 0);
            gt0_drpdo_out               : out STD_LOGIC_VECTOR(15 downto 0);
            gt0_drpen_in                : in  STD_LOGIC;
            gt0_drprdy_out              : out STD_LOGIC;
            gt0_drpwe_in                : in  STD_LOGIC;
            gt0_loopback_in             : in  STD_LOGIC_VECTOR(2 downto 0);
            gt0_eyescanreset_in         : in  STD_LOGIC;
            gt0_rxuserrdy_in            : in  STD_LOGIC;
            gt0_eyescandataerror_out    : out STD_LOGIC;
            gt0_eyescantrigger_in       : in  STD_LOGIC;
            gt0_rxcdrhold_in            : in  STD_LOGIC;
            gt0_rxslide_in              : in  STD_LOGIC;
            gt0_dmonitorout_out         : out STD_LOGIC_VECTOR(14 downto 0);
            gt0_rxusrclk_in             : in  STD_LOGIC;
            gt0_rxusrclk2_in            : in  STD_LOGIC;
            gt0_rxdata_out              : out STD_LOGIC_VECTOR(19 downto 0);
            gt0_gthrxn_in               : in  STD_LOGIC;
            gt0_rxmonitorout_out        : out STD_LOGIC_VECTOR(6 downto 0);
            gt0_rxmonitorsel_in         : in  STD_LOGIC_VECTOR(1 downto 0);
            gt0_rxoutclk_out            : out STD_LOGIC;
            gt0_rxoutclkfabric_out      : out STD_LOGIC;
            gt0_gtrxreset_in            : in  STD_LOGIC;
            gt0_rxpolarity_in           : in  STD_LOGIC;
            gt0_gthrxp_in               : in  STD_LOGIC;
            gt0_rxresetdone_out         : out STD_LOGIC;
            gt0_gttxreset_in            : in  STD_LOGIC;
            gt0_txuserrdy_in            : in  STD_LOGIC;
            gt0_txusrclk_in             : in  STD_LOGIC;
            gt0_txusrclk2_in            : in  STD_LOGIC;
            gt0_txdata_in               : in  STD_LOGIC_VECTOR(19 downto 0);
            gt0_gthtxn_out              : out STD_LOGIC;
            gt0_gthtxp_out              : out STD_LOGIC;
            gt0_txoutclk_out            : out STD_LOGIC;
            gt0_txoutclkfabric_out      : out STD_LOGIC;
            gt0_txoutclkpcs_out         : out STD_LOGIC;
            gt0_txresetdone_out         : out STD_LOGIC;
            gt0_txpolarity_in           : in  STD_LOGIC;
            gt1_drpaddr_in              : in  STD_LOGIC_VECTOR(8 downto 0);
            gt1_drpclk_in               : in  STD_LOGIC;
            gt1_drpdi_in                : in  STD_LOGIC_VECTOR(15 downto 0);
            gt1_drpdo_out               : out STD_LOGIC_VECTOR(15 downto 0);
            gt1_drpen_in                : in  STD_LOGIC;
            gt1_drprdy_out              : out STD_LOGIC;
            gt1_drpwe_in                : in  STD_LOGIC;
            gt1_loopback_in             : in  STD_LOGIC_VECTOR(2 downto 0);
            gt1_eyescanreset_in         : in  STD_LOGIC;
            gt1_rxuserrdy_in            : in  STD_LOGIC;
            gt1_eyescandataerror_out    : out STD_LOGIC;
            gt1_eyescantrigger_in       : in  STD_LOGIC;
            gt1_rxcdrhold_in            : in  STD_LOGIC;
            gt1_rxslide_in              : in  STD_LOGIC;
            gt1_dmonitorout_out         : out STD_LOGIC_VECTOR(14 downto 0);
            gt1_rxusrclk_in             : in  STD_LOGIC;
            gt1_rxusrclk2_in            : in  STD_LOGIC;
            gt1_rxdata_out              : out STD_LOGIC_VECTOR(19 downto 0);
            gt1_gthrxn_in               : in  STD_LOGIC;
            gt1_rxmonitorout_out        : out STD_LOGIC_VECTOR(6 downto 0);
            gt1_rxmonitorsel_in         : in  STD_LOGIC_VECTOR(1 downto 0);
            gt1_rxoutclk_out            : out STD_LOGIC;
            gt1_rxoutclkfabric_out      : out STD_LOGIC;
            gt1_gtrxreset_in            : in  STD_LOGIC;
            gt1_rxpolarity_in           : in  STD_LOGIC;
            gt1_gthrxp_in               : in  STD_LOGIC;
            gt1_rxresetdone_out         : out STD_LOGIC;
            gt1_gttxreset_in            : in  STD_LOGIC;
            gt1_txuserrdy_in            : in  STD_LOGIC;
            gt1_txusrclk_in             : in  STD_LOGIC;
            gt1_txusrclk2_in            : in  STD_LOGIC;
            gt1_txdata_in               : in  STD_LOGIC_VECTOR(19 downto 0);
            gt1_gthtxn_out              : out STD_LOGIC;
            gt1_gthtxp_out              : out STD_LOGIC;
            gt1_txoutclk_out            : out STD_LOGIC;
            gt1_txoutclkfabric_out      : out STD_LOGIC;
            gt1_txoutclkpcs_out         : out STD_LOGIC;
            gt1_txresetdone_out         : out STD_LOGIC;
            gt1_txpolarity_in           : in  STD_LOGIC;
            gt2_drpaddr_in              : in  STD_LOGIC_VECTOR(8 downto 0);
            gt2_drpclk_in               : in  STD_LOGIC;
            gt2_drpdi_in                : in  STD_LOGIC_VECTOR(15 downto 0);
            gt2_drpdo_out               : out STD_LOGIC_VECTOR(15 downto 0);
            gt2_drpen_in                : in  STD_LOGIC;
            gt2_drprdy_out              : out STD_LOGIC;
            gt2_drpwe_in                : in  STD_LOGIC;
            gt2_loopback_in             : in  STD_LOGIC_VECTOR(2 downto 0);
            gt2_eyescanreset_in         : in  STD_LOGIC;
            gt2_rxuserrdy_in            : in  STD_LOGIC;
            gt2_eyescandataerror_out    : out STD_LOGIC;
            gt2_eyescantrigger_in       : in  STD_LOGIC;
            gt2_rxcdrhold_in            : in  STD_LOGIC;
            gt2_rxslide_in              : in  STD_LOGIC;
            gt2_dmonitorout_out         : out STD_LOGIC_VECTOR(14 downto 0);
            gt2_rxusrclk_in             : in  STD_LOGIC;
            gt2_rxusrclk2_in            : in  STD_LOGIC;
            gt2_rxdata_out              : out STD_LOGIC_VECTOR(19 downto 0);
            gt2_gthrxn_in               : in  STD_LOGIC;
            gt2_rxmonitorout_out        : out STD_LOGIC_VECTOR(6 downto 0);
            gt2_rxmonitorsel_in         : in  STD_LOGIC_VECTOR(1 downto 0);
            gt2_rxoutclk_out            : out STD_LOGIC;
            gt2_rxoutclkfabric_out      : out STD_LOGIC;
            gt2_gtrxreset_in            : in  STD_LOGIC;
            gt2_rxpolarity_in           : in  STD_LOGIC;
            gt2_gthrxp_in               : in  STD_LOGIC;
            gt2_rxresetdone_out         : out STD_LOGIC;
            gt2_gttxreset_in            : in  STD_LOGIC;
            gt2_txuserrdy_in            : in  STD_LOGIC;
            gt2_txusrclk_in             : in  STD_LOGIC;
            gt2_txusrclk2_in            : in  STD_LOGIC;
            gt2_txdata_in               : in  STD_LOGIC_VECTOR(19 downto 0);
            gt2_gthtxn_out              : out STD_LOGIC;
            gt2_gthtxp_out              : out STD_LOGIC;
            gt2_txoutclk_out            : out STD_LOGIC;
            gt2_txoutclkfabric_out      : out STD_LOGIC;
            gt2_txoutclkpcs_out         : out STD_LOGIC;
            gt2_txresetdone_out         : out STD_LOGIC;
            gt2_txpolarity_in           : in  STD_LOGIC;
            gt3_drpaddr_in              : in  STD_LOGIC_VECTOR(8 downto 0);
            gt3_drpclk_in               : in  STD_LOGIC;
            gt3_drpdi_in                : in  STD_LOGIC_VECTOR(15 downto 0);
            gt3_drpdo_out               : out STD_LOGIC_VECTOR(15 downto 0);
            gt3_drpen_in                : in  STD_LOGIC;
            gt3_drprdy_out              : out STD_LOGIC;
            gt3_drpwe_in                : in  STD_LOGIC;
            gt3_loopback_in             : in  STD_LOGIC_VECTOR(2 downto 0);
            gt3_eyescanreset_in         : in  STD_LOGIC;
            gt3_rxuserrdy_in            : in  STD_LOGIC;
            gt3_eyescandataerror_out    : out STD_LOGIC;
            gt3_eyescantrigger_in       : in  STD_LOGIC;
            gt3_rxcdrhold_in            : in  STD_LOGIC;
            gt3_rxslide_in              : in  STD_LOGIC;
            gt3_dmonitorout_out         : out STD_LOGIC_VECTOR(14 downto 0);
            gt3_rxusrclk_in             : in  STD_LOGIC;
            gt3_rxusrclk2_in            : in  STD_LOGIC;
            gt3_rxdata_out              : out STD_LOGIC_VECTOR(19 downto 0);
            gt3_gthrxn_in               : in  STD_LOGIC;
            gt3_rxmonitorout_out        : out STD_LOGIC_VECTOR(6 downto 0);
            gt3_rxmonitorsel_in         : in  STD_LOGIC_VECTOR(1 downto 0);
            gt3_rxoutclk_out            : out STD_LOGIC;
            gt3_rxoutclkfabric_out      : out STD_LOGIC;
            gt3_gtrxreset_in            : in  STD_LOGIC;
            gt3_rxpolarity_in           : in  STD_LOGIC;
            gt3_gthrxp_in               : in  STD_LOGIC;
            gt3_rxresetdone_out         : out STD_LOGIC;
            gt3_gttxreset_in            : in  STD_LOGIC;
            gt3_txuserrdy_in            : in  STD_LOGIC;
            gt3_txusrclk_in             : in  STD_LOGIC;
            gt3_txusrclk2_in            : in  STD_LOGIC;
            gt3_txdata_in               : in  STD_LOGIC_VECTOR(19 downto 0);
            gt3_gthtxn_out              : out STD_LOGIC;
            gt3_gthtxp_out              : out STD_LOGIC;
            gt3_txoutclk_out            : out STD_LOGIC;
            gt3_txoutclkfabric_out      : out STD_LOGIC;
            gt3_txoutclkpcs_out         : out STD_LOGIC;
            gt3_txresetdone_out         : out STD_LOGIC;
            gt3_txpolarity_in           : in  STD_LOGIC;
            GT0_QPLLLOCK_IN             : in  STD_LOGIC;
            GT0_QPLLREFCLKLOST_IN       : in  STD_LOGIC;
            GT0_QPLLRESET_OUT           : out STD_LOGIC;
            GT0_QPLLOUTCLK_IN           : in  STD_LOGIC;
            GT0_QPLLOUTREFCLK_IN        : in  STD_LOGIC
        );
    end component gtwizard_QPLL_4p8g_V7;



    signal GT0_QPLLLOCK_I   : std_logic:='0';
    signal gt0_gttxreset_in : std_logic:='0';
    signal gt1_gttxreset_in : std_logic:='0';
    signal gt2_gttxreset_in : std_logic:='0';
    signal gt3_gttxreset_in : std_logic:='0';
    signal gt0_gtrxreset_in : std_logic:='0';
    signal gt1_gtrxreset_in : std_logic:='0';
    signal gt2_gtrxreset_in : std_logic:='0';
    signal gt3_gtrxreset_in : std_logic:='0';

    signal gt0_qplloutclk_i :std_logic;

    signal  tied_to_ground_i                : std_logic;
    signal  tied_to_ground_vec_i            : std_logic_vector(63 downto 0);
    signal  tied_to_vcc_i                   : std_logic;
    --signal  tied_to_vcc_vec_i               : std_logic_vector(7 downto 0);

    --signal gt0_drpaddr_in                   : std_logic_vector(8 downto 0):="000000000";
    --signal gt1_drpaddr_in                   : std_logic_vector(8 downto 0):="000000000";
    --signal gt2_drpaddr_in                   : std_logic_vector(8 downto 0):="000000000";
    --signal gt3_drpaddr_in                   : std_logic_vector(8 downto 0):="000000000";
    --signal gt0_drpdi_in                     : std_logic_vector(15 downto 0):=x"0000";
    --signal gt1_drpdi_in                     : std_logic_vector(15 downto 0):=x"0000";
    --signal gt2_drpdi_in                     : std_logic_vector(15 downto 0):=x"0000";
    --signal gt3_drpdi_in                     : std_logic_vector(15 downto 0):=x"0000";
    --signal gt0_drpdo_out                    : std_logic_vector(15 downto 0);
    --signal gt1_drpdo_out                    : std_logic_vector(15 downto 0);
    --signal gt2_drpdo_out                    : std_logic_vector(15 downto 0);
    --signal gt3_drpdo_out                    : std_logic_vector(15 downto 0);
    --signal gt0_drpen_in                     : std_logic:='0';
    --signal gt1_drpen_in                     : std_logic:='0';
    --signal gt2_drpen_in                     : std_logic:='0';
    --signal gt3_drpen_in                     : std_logic:='0';
    --signal gt0_drprdy_out                   : std_logic;
    --signal gt1_drprdy_out                   : std_logic;
    --signal gt2_drprdy_out                   : std_logic;
    --signal gt3_drprdy_out                   : std_logic;
    --signal gt0_drpwe_in                     : std_logic:='0';
    --signal gt1_drpwe_in                     : std_logic:='0';
    --signal gt2_drpwe_in                     : std_logic:='0';
    --signal gt3_drpwe_in                     : std_logic:='0';


    signal GT0_QPLLREFCLKLOST_I             : std_logic;
    signal GT0_QPLLRESET_I                  : std_logic;
    signal GT0_QPLLOUTREFCLK_I              : std_logic;


--**************************** Main Body of Code *******************************
begin

    tied_to_ground_i                             <= '0';
    tied_to_ground_vec_i                         <= x"0000000000000000";
    tied_to_vcc_i                                <= '1';

    gt_cpllfbclklost_out        <= "0000";
    gt_cplllock_out             <= "1111";

    gt_rxcdrlock_out            <= "1111";
    gt_qplllock_out             <= GT0_QPLLLOCK_I;


    gt0_gttxreset_in <= GTTX_RESET_IN(0) or (not GT0_QPLLLOCK_I);
    gt1_gttxreset_in <= GTTX_RESET_IN(1) or (not GT0_QPLLLOCK_I);
    gt2_gttxreset_in <= GTTX_RESET_IN(2) or (not GT0_QPLLLOCK_I);
    gt3_gttxreset_in <= GTTX_RESET_IN(3) or (not GT0_QPLLLOCK_I);

    gt0_gtrxreset_in <= GTRX_RESET_IN(0) or (not GT0_QPLLLOCK_I);
    gt1_gtrxreset_in <= GTRX_RESET_IN(1) or (not GT0_QPLLLOCK_I);
    gt2_gtrxreset_in <= GTRX_RESET_IN(2) or (not GT0_QPLLLOCK_I);
    gt3_gtrxreset_in <= GTRX_RESET_IN(3) or (not GT0_QPLLLOCK_I);




    gthe2_common_0_i : GTHE2_COMMON
        generic map
    (
            BIAS_CFG => (x"0000040000001050"),
            COMMON_CFG => (x"0000001C"),
            -- Simulation attributes
            IS_DRPCLK_INVERTED => '0',
            IS_GTGREFCLK_INVERTED => '0',
            IS_QPLLLOCKDETCLK_INVERTED => '0',
            QPLL_CFG => (x"04801C7"),
            QPLL_CLKOUT_CFG => ("1111"),
            QPLL_COARSE_FREQ_OVRD => ("010000"),
            QPLL_COARSE_FREQ_OVRD_EN => ('0'),
            QPLL_CP => ("0000011111"),
            QPLL_CP_MONITOR_EN => ('0'),
            QPLL_DMONITOR_SEL => ('0'),
            QPLL_FBDIV => ("0010000000"), --(QPLL_FBDIV_IN),
            QPLL_FBDIV_MONITOR_EN => ('0'),
            QPLL_FBDIV_RATIO => ('1'), --(QPLL_FBDIV_RATIO),
            QPLL_INIT_CFG => (x"000006"),
            QPLL_LOCK_CFG => (x"05E8"),
            QPLL_LPF => ("1111"),
            QPLL_REFCLK_DIV => (1),
            QPLL_RP_COMP => ('0'),
            QPLL_VTRL_RESET => ("00"),
            RCAL_CFG => ("00"),
            RSVD_ATTR0 => (x"0000"),
            RSVD_ATTR1 => (x"0000"),
            SIM_QPLLREFCLK_SEL => ("001"),
            SIM_RESET_SPEEDUP => ("FALSE"),
            SIM_VERSION => ("2.0")


        )
        port map
    (
            DRPDO => open,
            DRPRDY => open,
            PMARSVDOUT => open,
            ------------------------- Common Block -  QPLL Ports -----------------------
            QPLLDMONITOR => open,
            QPLLFBCLKLOST => open,
            QPLLLOCK => GT0_QPLLLOCK_I,
            ----------------------- Common Block - Clocking Ports ----------------------
            QPLLOUTCLK => gt0_qplloutclk_i,
            QPLLOUTREFCLK => GT0_QPLLOUTREFCLK_I,
            QPLLREFCLKLOST => GT0_QPLLREFCLKLOST_I,
            REFCLKOUTMONITOR => open,
            --------------------------------- QPLL Ports -------------------------------
            BGBYPASSB => tied_to_vcc_i,
            BGMONITORENB => tied_to_vcc_i,
            BGPDB => tied_to_vcc_i,
            BGRCALOVRD => "00000",
            ------------------------- Common Block - QPLL Ports ------------------------
            BGRCALOVRDENB => tied_to_vcc_i,
            ------------- Common Block  - Dynamic Reconfiguration Port (DRP) -----------
            DRPADDR => tied_to_ground_vec_i(7 downto 0),
            DRPCLK => tied_to_ground_i,
            DRPDI => tied_to_ground_vec_i(15 downto 0),
            DRPEN => tied_to_ground_i,
            DRPWE => tied_to_ground_i,
            ---------------------- Common Block  - Ref Clock Ports ---------------------
            GTGREFCLK => tied_to_ground_i,
            GTNORTHREFCLK0 => tied_to_ground_i,
            GTNORTHREFCLK1 => tied_to_ground_i,
            GTREFCLK0 => GTH_RefClk,
            GTREFCLK1 => tied_to_ground_i,
            GTSOUTHREFCLK0 => tied_to_ground_i,
            GTSOUTHREFCLK1 => tied_to_ground_i,
            PMARSVD => "00000000",
            QPLLLOCKDETCLK => DRP_CLK_IN,
            QPLLLOCKEN => tied_to_vcc_i,
            QPLLOUTRESET => tied_to_ground_i,
            QPLLPD => tied_to_ground_i,
            QPLLREFCLKSEL => "001",
            QPLLRESET => GT0_QPLLRESET_I or QPLL_RESET_IN,
            QPLLRSVD1 => "0000000000000000",
            QPLLRSVD2 => "11111",
            RCALENB => tied_to_vcc_i

        );




    qpll_inst: gtwizard_QPLL_4p8g_V7

        port map
(
            SYSCLK_IN => DRP_CLK_IN,
            SOFT_RESET_TX_IN => SOFT_TXRST_ALL,
            SOFT_RESET_RX_IN => SOFT_RXRST_ALL,
            DONT_RESET_ON_DATA_ERROR_IN => '1',
            GT0_DRP_BUSY_OUT => open,
            GT0_TX_FSM_RESET_DONE_OUT => gt_txfsmresetdone_out(0),
            GT0_RX_FSM_RESET_DONE_OUT => gt_rxfsmresetdone_out(0),
            GT0_DATA_VALID_IN => '1',
            GT1_DRP_BUSY_OUT => open,
            GT1_TX_FSM_RESET_DONE_OUT => gt_txfsmresetdone_out(1),
            GT1_RX_FSM_RESET_DONE_OUT => gt_rxfsmresetdone_out(1),
            GT1_DATA_VALID_IN => '1',
            GT2_DRP_BUSY_OUT => open,
            GT2_TX_FSM_RESET_DONE_OUT => gt_txfsmresetdone_out(2),
            GT2_RX_FSM_RESET_DONE_OUT => gt_rxfsmresetdone_out(2),
            GT2_DATA_VALID_IN => '1',
            GT3_DRP_BUSY_OUT => open,
            GT3_TX_FSM_RESET_DONE_OUT => gt_txfsmresetdone_out(3),
            GT3_RX_FSM_RESET_DONE_OUT => gt_rxfsmresetdone_out(3),
            GT3_DATA_VALID_IN => '1',
            gt0_drpaddr_in => (others => '0'),
            gt0_drpclk_in => DRP_CLK_IN,
            gt0_drpdi_in => (others => '0'),
            gt0_drpdo_out => open,
            gt0_drpen_in => '0',
            gt0_drprdy_out => open,
            gt0_drpwe_in => '0',
            gt0_loopback_in => gt0_loopback_in,
            gt0_eyescanreset_in => '0',
            gt0_rxuserrdy_in => gt_rxuserrdy_in(0),
            gt0_eyescandataerror_out => open,
            gt0_eyescantrigger_in => '0',
            gt0_rxcdrhold_in => gt0_rxcdrhold_in,
            gt0_rxslide_in => gt_rxslide_in(0),
            gt0_dmonitorout_out => open,
            gt0_rxusrclk_in => gt0_rxusrclk_in,
            gt0_rxusrclk2_in => gt0_rxusrclk_in,
            gt0_rxdata_out => RX_DATA_gt0_20b,
            gt0_gthrxn_in => RXN_IN(0),
            gt0_rxmonitorout_out => open,
            gt0_rxmonitorsel_in => "00",
            gt0_rxoutclk_out => gt0_rxoutclk_out,
            gt0_rxoutclkfabric_out => open,
            gt0_gtrxreset_in => gt0_gtrxreset_in,
            gt0_rxpolarity_in => gt_rxpolarity_in(0),
            gt0_gthrxp_in => RXP_IN(0),
            gt0_rxresetdone_out => gt_rxresetdone_out(0), -- gt0_rxresetdone_out,
            gt0_gttxreset_in => gt0_gttxreset_in,
            gt0_txuserrdy_in => gt_txuserrdy_in(0),
            gt0_txusrclk_in => gt0_txusrclk_in,
            gt0_txusrclk2_in => gt0_txusrclk_in,
            gt0_txdata_in => TX_DATA_gt0_20b,
            gt0_gthtxn_out => TXN_OUT(0),
            gt0_gthtxp_out => TXP_OUT(0),
            gt0_txoutclk_out => gt0_txoutclk_out,
            gt0_txoutclkfabric_out => open, --gt0_txoutclkfabric_out,
            gt0_txoutclkpcs_out => open, --gt0_txoutclkpcs_out,
            gt0_txresetdone_out => gt_txresetdone_out(0),
            gt0_txpolarity_in => gt_txpolarity_in(0),
            gt1_drpaddr_in => (others => '0'),
            gt1_drpclk_in => DRP_CLK_IN,
            gt1_drpdi_in => (others => '0'),
            gt1_drpdo_out => open,
            gt1_drpen_in => '0',
            gt1_drprdy_out => open,
            gt1_drpwe_in => '0',
            gt1_loopback_in => gt1_loopback_in,
            gt1_eyescanreset_in => '0',
            gt1_rxuserrdy_in => gt_rxuserrdy_in(1),
            gt1_eyescandataerror_out => open,
            gt1_eyescantrigger_in => '0',
            gt1_rxcdrhold_in => gt1_rxcdrhold_in,
            gt1_rxslide_in => gt_rxslide_in(1),
            gt1_dmonitorout_out => open,
            gt1_rxusrclk_in => gt1_rxusrclk_in,
            gt1_rxusrclk2_in => gt1_rxusrclk_in,
            gt1_rxdata_out => RX_DATA_gt1_20b,
            gt1_gthrxn_in => RXN_IN(1),
            --gt1_rxphmonitor_out             =>      open,
            --gt1_rxphslipmonitor_out         =>      open,
            --------------------- Receive Ports - RX Equalizer Ports -------------------
            gt1_rxmonitorout_out => open,
            gt1_rxmonitorsel_in => "00",
            --------------- Receive Ports - RX Fabric Output Control Ports -------------
            gt1_rxoutclk_out => gt1_rxoutclk_out,
            gt1_rxoutclkfabric_out => open,
            ------------- Receive Ports - RX Initialization and Reset Ports ------------
            gt1_gtrxreset_in => gt1_gtrxreset_in,
            gt1_rxpolarity_in => gt_rxpolarity_in(1),
            ------------------------ Receive Ports -RX AFE Ports -----------------------
            gt1_gthrxp_in => RXP_IN(1),
            -------------- Receive Ports -RX Initialization and Reset Ports ------------
            gt1_rxresetdone_out => gt_rxresetdone_out(1),
            --------------------- TX Initialization and Reset Ports --------------------
            gt1_gttxreset_in => gt1_gttxreset_in,
            gt1_txuserrdy_in => gt_txuserrdy_in(1),
            ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
            gt1_txusrclk_in => gt0_txusrclk_in,
            gt1_txusrclk2_in => gt0_txusrclk_in,
            ------------------ Transmit Ports - TX Data Path interface -----------------
            gt1_txdata_in => TX_DATA_gt1_20b,
            ---------------- Transmit Ports - TX Driver and OOB signaling --------------
            gt1_gthtxn_out => TXN_OUT(1),
            gt1_gthtxp_out => TXP_OUT(1),
            ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
            gt1_txoutclk_out => gt1_txoutclk_out,
            gt1_txoutclkfabric_out => open, --gt1_txoutclkfabric_out,
            gt1_txoutclkpcs_out => open, --gt1_txoutclkpcs_out,
            ------------- Transmit Ports - TX Initialization and Reset Ports -----------
            gt1_txresetdone_out => gt_txresetdone_out(1),
            gt1_txpolarity_in => gt_txpolarity_in(1),
            gt2_drpaddr_in => (others => '0'),
            gt2_drpclk_in => DRP_CLK_IN,
            gt2_drpdi_in => (others => '0'),
            gt2_drpdo_out => open,
            gt2_drpen_in => '0',
            gt2_drprdy_out => open,
            gt2_drpwe_in => '0',
            gt2_loopback_in => gt2_loopback_in,
            --------------------- RX Initialization and Reset Ports --------------------
            gt2_eyescanreset_in => '0',
            gt2_rxuserrdy_in => gt_rxuserrdy_in(2),
            -------------------------- RX Margin Analysis Ports ------------------------
            gt2_eyescandataerror_out => open,
            gt2_eyescantrigger_in => '0',
            gt2_rxcdrhold_in => gt2_rxcdrhold_in,
            --------------- Receive Ports - Comma Detection and Alignment --------------
            gt2_rxslide_in => gt_rxslide_in(2),
            ------------------- Receive Ports - Digital Monitor Ports ------------------
            gt2_dmonitorout_out => open,
            ------------------ Receive Ports - FPGA RX Interface Ports -----------------
            gt2_rxusrclk_in => gt2_rxusrclk_in,
            gt2_rxusrclk2_in => gt2_rxusrclk_in,
            ------------------ Receive Ports - FPGA RX interface Ports -----------------
            gt2_rxdata_out => RX_DATA_gt2_20b,
            ------------------------ Receive Ports - RX AFE Ports ----------------------
            gt2_gthrxn_in => RXN_IN(2),
            ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
            --gt2_rxphmonitor_out             =>      open,
            --gt2_rxphslipmonitor_out         =>      open,
            --------------------- Receive Ports - RX Equalizer Ports -------------------
            gt2_rxmonitorout_out => open,
            gt2_rxmonitorsel_in => "00",
            --------------- Receive Ports - RX Fabric Output Control Ports -------------
            gt2_rxoutclk_out => gt2_rxoutclk_out,
            gt2_rxoutclkfabric_out => open,
            ------------- Receive Ports - RX Initialization and Reset Ports ------------
            gt2_gtrxreset_in => gt2_gtrxreset_in,
            gt2_rxpolarity_in => gt_rxpolarity_in(2),
            ------------------------ Receive Ports -RX AFE Ports -----------------------
            gt2_gthrxp_in => RXP_IN(2),
            -------------- Receive Ports -RX Initialization and Reset Ports ------------
            gt2_rxresetdone_out => gt_rxresetdone_out(2),
            --------------------- TX Initialization and Reset Ports --------------------
            gt2_gttxreset_in => gt2_gttxreset_in,
            gt2_txuserrdy_in => gt_txuserrdy_in(2),
            ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
            gt2_txusrclk_in => gt0_txusrclk_in,
            gt2_txusrclk2_in => gt0_txusrclk_in,
            ------------------ Transmit Ports - TX Data Path interface -----------------
            gt2_txdata_in => TX_DATA_gt2_20b,
            ---------------- Transmit Ports - TX Driver and OOB signaling --------------
            gt2_gthtxn_out => TXN_OUT(2),
            gt2_gthtxp_out => TXP_OUT(2),
            ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
            gt2_txoutclk_out => gt2_txoutclk_out,
            gt2_txoutclkfabric_out => open, --gt2_txoutclkfabric_out,
            gt2_txoutclkpcs_out => open, --gt2_txoutclkpcs_out,
            ------------- Transmit Ports - TX Initialization and Reset Ports -----------
            gt2_txresetdone_out => gt_txresetdone_out(2),
            gt2_txpolarity_in => gt_txpolarity_in(2),
            --GT3  (X1Y7)
            --____________________________CHANNEL PORTS________________________________
            ---------------------------- Channel - DRP Ports  --------------------------
            gt3_drpaddr_in => (others => '0'),
            gt3_drpclk_in => DRP_CLK_IN,
            gt3_drpdi_in => (others => '0'),
            gt3_drpdo_out => open,
            gt3_drpen_in => '0',
            gt3_drprdy_out => open,
            gt3_drpwe_in => '0',
            gt3_loopback_in => gt3_loopback_in,
            --------------------- RX Initialization and Reset Ports --------------------
            gt3_eyescanreset_in => '0',
            gt3_rxuserrdy_in => gt_rxuserrdy_in(3),
            -------------------------- RX Margin Analysis Ports ------------------------
            gt3_eyescandataerror_out => open,
            gt3_eyescantrigger_in => '0',
            gt3_rxcdrhold_in => gt3_rxcdrhold_in,
            --------------- Receive Ports - Comma Detection and Alignment --------------
            gt3_rxslide_in => gt_rxslide_in(3),
            ------------------- Receive Ports - Digital Monitor Ports ------------------
            gt3_dmonitorout_out => open,
            ------------------ Receive Ports - FPGA RX Interface Ports -----------------
            gt3_rxusrclk_in => gt3_rxusrclk_in,
            gt3_rxusrclk2_in => gt3_rxusrclk_in,
            ------------------ Receive Ports - FPGA RX interface Ports -----------------
            gt3_rxdata_out => RX_DATA_gt3_20b,
            ------------------------ Receive Ports - RX AFE Ports ----------------------
            gt3_gthrxn_in => RXN_IN(3),
            ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
            --gt3_rxphmonitor_out             =>      open,
            --gt3_rxphslipmonitor_out         =>      open,
            --------------------- Receive Ports - RX Equalizer Ports -------------------
            gt3_rxmonitorout_out => open,
            gt3_rxmonitorsel_in => "00",
            --------------- Receive Ports - RX Fabric Output Control Ports -------------
            gt3_rxoutclk_out => gt3_rxoutclk_out,
            gt3_rxoutclkfabric_out => open,
            ------------- Receive Ports - RX Initialization and Reset Ports ------------
            gt3_gtrxreset_in => gt3_gtrxreset_in,
            gt3_rxpolarity_in => gt_rxpolarity_in(3),
            ------------------------ Receive Ports -RX AFE Ports -----------------------
            gt3_gthrxp_in => RXP_IN(3),
            -------------- Receive Ports -RX Initialization and Reset Ports ------------
            gt3_rxresetdone_out => gt_rxresetdone_out(3),
            --------------------- TX Initialization and Reset Ports --------------------
            gt3_gttxreset_in => gt3_gttxreset_in,
            gt3_txuserrdy_in => gt_txuserrdy_in(3),
            ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
            gt3_txusrclk_in => gt0_txusrclk_in,
            gt3_txusrclk2_in => gt0_txusrclk_in,
            ------------------ Transmit Ports - TX Data Path interface -----------------
            gt3_txdata_in => TX_DATA_gt3_20b,
            ---------------- Transmit Ports - TX Driver and OOB signaling --------------
            gt3_gthtxn_out => TXN_OUT(3),
            gt3_gthtxp_out => TXP_OUT(3),
            ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
            gt3_txoutclk_out => gt3_txoutclk_out,
            gt3_txoutclkfabric_out => open, --gt3_txoutclkfabric_out,
            gt3_txoutclkpcs_out => open, --gt3_txoutclkpcs_out,
            ------------- Transmit Ports - TX Initialization and Reset Ports -----------
            gt3_txresetdone_out => gt_txresetdone_out(3),
            gt3_txpolarity_in => gt_txpolarity_in(3),
            GT0_QPLLLOCK_IN => GT0_QPLLLOCK_I,
            GT0_QPLLREFCLKLOST_IN => GT0_QPLLREFCLKLOST_I,
            GT0_QPLLRESET_OUT => GT0_QPLLRESET_I,
            GT0_QPLLOUTCLK_IN => gt0_qplloutclk_i,
            GT0_QPLLOUTREFCLK_IN => GT0_QPLLOUTREFCLK_I

        );



end RTL;


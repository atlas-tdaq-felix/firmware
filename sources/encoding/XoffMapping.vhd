--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
    use ieee.std_logic_1164.all;
    use work.pcie_package.all;

--! top module for centralRouter logic
entity XoffMapping is
    generic (
        GBTid                   : integer := 0
    );
    port  (
        --reset                   : in std_logic;
        clk40                   : in  std_logic;
        --register_map_control    : in  register_map_control_type;
        toHostXoff              : in   std_logic_vector(23 downto 0); --Almost full flags from Full mode channels
        toHostXoffbits          : out std_logic_vector(39 downto 0)          --Xoff signals towards all 2b ELINKS
    );
end XoffMapping;

architecture rtl of XoffMapping is
begin


    -- In GBT 0, (full mode only) we distribute all the xoff bits to the first 24 elinks
    g_xoff0: if(GBTid = 0) generate
        process(clk40)
        begin
            if rising_edge(clk40) then
                toHostXoffbits(23 downto 0) <= toHostXoff;
                toHostXoffbits(39 downto 24) <= (others => '0');
            end if;
        end process;
    end generate;

    -- In GBT > 0, we distribute the corresponding xoff bit to the elink 0
    g_xoffN: if(GBTid > 0) generate
        process(clk40)
        begin
            if rising_edge(clk40) then
                toHostXoffbits(0) <= toHostXoff(GBTid);
                toHostXoffbits(39 downto 1) <= (others => '0');
            end if;
        end process;
    end generate;

end architecture rtl;

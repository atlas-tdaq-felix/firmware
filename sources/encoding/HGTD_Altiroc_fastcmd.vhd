--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

    use work.FELIX_package.all;
    use work.pcie_package.all;

--! From pcie_package:
--!type bitfield_hgtd_altiroc_fastcmd_w_type is record
--!  ALTIROC3_IDLE                  : std_logic_vector(14 downto 14);  -- 0 for ALTIROC2 10101100, 1 for ALTIROC3 11110000
--!  USE_CAL                        : std_logic_vector(13 downto 13);  -- When set to 1, CAL will be sent on L1A, then after TRIG_DELAY BC clocks a TRIGGER. When 0, TRIGGER will be sent on L1A.
--!  SYNCLUMI                       : std_logic_vector(12 downto 12);  -- Set to 1 to trigger a SYNCLUMI command, rising edge of this bit. Clear in software
--!  GBRST                          : std_logic_vector(11 downto 11);  -- Set to 1 to trigger a GBRST command, rising edge of this bit. Clear in software
--!  TRIG_DELAY                     : std_logic_vector(10 downto 0);   -- Number of BC clocks between CAL and TRIGGER command if USE_CAL is set to 1
--!end record;

entity HGTD_Altiroc_fastcmd is
    port (
        clk40   : in std_logic;
        TTCin   : in TTC_data_type;
        FastCmd : out std_logic_vector(7 downto 0);
        HGTD_ALTIROC_FASTCMD_CONTROL: in bitfield_hgtd_altiroc_fastcmd_w_type
    );
end HGTD_Altiroc_fastcmd;

architecture Behavioral of HGTD_Altiroc_fastcmd is
    --! From Altiroc specification https://edms.cern.ch/ui/file/2509521/1/ALTIROC_specification.pdf
    --! Table 3.1: Fast Command definition

    constant IDLEV2      : std_logic_vector(7 downto 0) := "10101100"; -- IDLE frame for ALTIROC v2
    constant IDLEV3      : std_logic_vector(7 downto 0) := "11110000"; -- IDLE frame for ALTIROC v3
    signal IDLE: std_logic_vector(7 downto 0);
    constant TRIGGER   : std_logic_vector(7 downto 0) := "10110010"; -- L0 or L1 trigger
    constant BCR       : std_logic_vector(7 downto 0) := "10011001"; -- Bunch Counter Reset
    constant TRIGBCR   : std_logic_vector(7 downto 0) := "01101001"; -- Trigger and BCR
    constant CAL       : std_logic_vector(7 downto 0) := "11010100"; -- Calibration Pulse
    constant GBRST     : std_logic_vector(7 downto 0) := "11001010"; -- Global Reset
    constant SYNCLUMI  : std_logic_vector(7 downto 0) := "01100110"; -- Synchronize luminosity stream
    constant SETTRIGID : std_logic_vector(7 downto 0) := "01010011"; -- Set Trigger ID
    constant TRIGID    : std_logic_vector(7 downto 0) := "01XXXX01"; -- Trigger ID

    signal SEND_L1A: std_logic;
    signal SEND_BCR: std_logic;
    signal SEND_ECR: std_logic;
    signal SEND_CAL: std_logic;
    signal TRIG_CAL_ACTIVE: std_logic := '0';
    signal TRIG_DELAY_CNT: unsigned(10 downto 0);
    signal SEND_GBRST: std_logic;
    signal SEND_SYNCLUMI: std_logic;
    type TrigIdStateType is (SetTrigIdCmd,TrigId0, TrigId1, TrigId2);
    signal TrigIdState: TrigIdStateType;

    signal L1Id: std_logic_vector(11 downto 0);

    --2 pipelined signals of the regmap for edge detection.
    signal HGTD_ALTIROC_FASTCMD_CONTROL_GBRST_p1 : std_logic_vector(0 downto 0);
    signal HGTD_ALTIROC_FASTCMD_CONTROL_SYNCLUMI_p1 : std_logic_vector(0 downto 0);
begin

    idle_sel_proc: process(clk40)
    begin
        if rising_edge(clk40) then
            if HGTD_ALTIROC_FASTCMD_CONTROL.ALTIROC3_IDLE = "0" then
                IDLE <= IDLEV2;
            else
                IDLE <= IDLEV3;
            end if;
        end if;
    end process;


    --Either a L1A transmits a TRIGGER immediatly, or when USE_CAL is set, it is issued after a delay set in TRIG_DELAY.
    trigger_cal_proc: process(clk40)
    begin
        if rising_edge(clk40) then
            if HGTD_ALTIROC_FASTCMD_CONTROL.USE_CAL = "0" then
                if TTCin.L1A = '1' then
                    SEND_L1A <= '1';
                elsif TrigIdState = SetTrigIdCmd then
                    SEND_L1A <= '0';
                end if;
                SEND_CAL <= '0';
            else
                SEND_L1A <= '0';
                if TTCin.L1A = '1' and TRIG_CAL_ACTIVE = '0' then
                    SEND_CAL <= '1';
                elsif SEND_L1A = '0' and  SEND_BCR = '0' and TrigIdState = SetTrigIdCmd then --These 2 signals have a higher priority, so if they are issued send them first then do a CAL, remember the state.
                    SEND_CAL <= '0';
                end if;
                if SEND_CAL = '1' and SEND_L1A = '0' and  SEND_BCR = '0' and TrigIdState = SetTrigIdCmd then --Start counting when CAL is 1, the number of IDLES set in TRIG_DELAY, then issue the L1A afterwards.
                    if HGTD_ALTIROC_FASTCMD_CONTROL.TRIG_DELAY = "00000000000" then --With 0 delay, send trigger immediately
                        SEND_L1A <= '1';
                        TRIG_CAL_ACTIVE <= '0';
                    else --Start a counter to count TRIG_DELAY IDLES
                        TRIG_DELAY_CNT <= unsigned(HGTD_ALTIROC_FASTCMD_CONTROL.TRIG_DELAY) - 1;
                        TRIG_CAL_ACTIVE <= '1';
                    end if;
                end if;
                if TRIG_CAL_ACTIVE = '1' then
                    if TRIG_DELAY_CNT = "00000000000" then
                        SEND_L1A <= '1';
                        TRIG_CAL_ACTIVE <= '0';
                    else
                        TRIG_DELAY_CNT <= TRIG_DELAY_CNT - 1;
                    end if;
                end if;
            end if;
        end if;
    end process;


    SendGBRST_SYNCLUMI_Proc: process(clk40)
    begin
        if rising_edge(clk40) then
            HGTD_ALTIROC_FASTCMD_CONTROL_GBRST_p1 <= HGTD_ALTIROC_FASTCMD_CONTROL.GBRST;
            HGTD_ALTIROC_FASTCMD_CONTROL_SYNCLUMI_p1 <= HGTD_ALTIROC_FASTCMD_CONTROL.SYNCLUMI;

            --On a rising edge of the register, send GBRST command
            if HGTD_ALTIROC_FASTCMD_CONTROL.GBRST = "1" and HGTD_ALTIROC_FASTCMD_CONTROL_GBRST_p1 = "0" then
                SEND_GBRST <= '1';
            end if;

            --Make sure it keeps it's values if any of the higher priority signals are active
            if SEND_GBRST = '1' and SEND_L1A = '0' and SEND_BCR = '0' and SEND_CAL = '0' and TrigIdState = SetTrigIdCmd then
                SEND_GBRST <= '0';
            end if;

            --On a rising edge of the register, send SYNCLUMI command
            if HGTD_ALTIROC_FASTCMD_CONTROL.SYNCLUMI = "1" and HGTD_ALTIROC_FASTCMD_CONTROL_SYNCLUMI_p1 = "0" then
                SEND_SYNCLUMI <= '1';
            end if;

            --Make sure it keeps it's values if any of the higher priority signals are active
            if SEND_SYNCLUMI = '1' and SEND_L1A = '0' and SEND_BCR = '0' and SEND_CAL = '0' and SEND_GBRST = '0' and TrigIdState = SetTrigIdCmd then
                SEND_SYNCLUMI <= '0';
            end if;
        end if;
    end process;

    FastCmd_Proc: process(clk40)
    begin
        if rising_edge(clk40) then
            SEND_ECR <= TTCin.ECR;
            SEND_BCR <= TTCin.BCR;
            FastCmd <= IDLE;
            case TrigIdState is
                when SetTrigIdCmd =>
                    if SEND_L1A = '1' and SEND_BCR = '0' then
                        FastCmd <= TRIGGER;
                    elsif SEND_L1A = '1' and SEND_BCR= '1' then
                        FastCmd <= TRIGBCR;
                    elsif SEND_BCR = '1' then
                        FastCmd <= BCR;
                    elsif SEND_CAL = '1' then
                        FastCmd <= CAL;
                    elsif SEND_GBRST = '1' then
                        FastCmd <= GBRST;
                    elsif SEND_SYNCLUMI = '1' then
                        FastCmd <= SYNCLUMI;
                    elsif SEND_ECR = '1' then
                        L1Id <= TTCin.L1Id(11 downto 0);
                        FastCmd <= SETTRIGID;
                        TrigIdState <= TrigId0;
                    end if;
                when TrigId0 =>
                    TrigIdState <= TrigId1;
                    FastCmd(7 downto 6) <= TRIGID(7 downto 6);
                    FastCmd(5 downto 2) <= L1Id(11 downto 8);
                    FastCmd(1 downto 0) <= TRIGID(1 downto 0);
                when TrigId1 =>
                    TrigIdState <= TrigId2;
                    FastCmd(7 downto 6) <= TRIGID(7 downto 6);
                    FastCmd(5 downto 2) <= L1Id(7 downto 4);
                    FastCmd(1 downto 0) <= TRIGID(1 downto 0);
                when TrigId2 =>
                    TrigIdState <= SetTrigIdCmd;
                    FastCmd(7 downto 6) <= TRIGID(7 downto 6);
                    FastCmd(5 downto 2) <= L1Id(3 downto 0);
                    FastCmd(1 downto 0) <= TRIGID(1 downto 0);
                when others =>
                    TrigIdState <= SetTrigIdCmd;
                    FastCmd <= IDLE;
            end case;
        end if;
    end process;


end Behavioral;

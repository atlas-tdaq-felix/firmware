--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Israel Grayzman
--!               Nico Giangiacomi
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.
--! Engineer: Israel Grayzman (israel.grayzman@weizmann.ac.il)
--!
--! Create Date:    21/07/2019
--! Module Name:    ExtendedTestPulse
--! Project Name:   FELIX
----------------------------------------------------------------------------------

--! Use standard library
library IEEE, UNISIM;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;

entity ExtendedTestPulse is
    port  (
        clk40           : in  std_logic;
        reset             : in  std_logic;

        TTCin_TestPulse    : in  std_logic;  -- from the TTC decoder
        ExtendedTestPulse  : out std_logic    -- towards the Central Router FromHost TTC array
    );
end ExtendedTestPulse;

architecture Behavioral of ExtendedTestPulse is

    signal intExtendedTestPulse    : std_logic;        -- NSW required a 32x 40MHz clock pulse,
    signal TestPulseCounter        : integer range 0 to 31;  -- 32 states counter

begin

    process (reset, clk40)
    begin
        if (reset = '1') then
            intExtendedTestPulse  <= '0'; -- clear the extended test pulse signal
            TestPulseCounter      <= 0;   -- hold the extended test pulse counter
        elsif rising_edge(clk40) then
            -- NSW Test Pulse bit location
            if (TTCin_TestPulse = '1') then
                intExtendedTestPulse  <= '1'; -- set the extended test pulse signal
                TestPulseCounter      <= 31;  -- initial the extended test pulse counter
            -- 32x 40MHz clocks pulse width
            elsif (TestPulseCounter = 0) then
                intExtendedTestPulse  <= '0'; -- clear the extended test pulse signal
                TestPulseCounter      <= 0;   -- hold the extended test pulse counter
            else
                intExtendedTestPulse  <= intExtendedTestPulse;       -- keep the value
                TestPulseCounter      <= TestPulseCounter - 1;    -- the extended test pulse counter count down
            end if;
        end if;
    end process;

    ExtendedTestPulse    <= intExtendedTestPulse;

end Behavioral;

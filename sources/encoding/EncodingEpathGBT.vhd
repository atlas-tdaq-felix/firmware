--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!               Frans Schreuder
--!               Nico Giangiacomi
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company: University of Bologna
-- Engineer: Nico Giangiacomi
--
-- Create Date: 02/17/2020 03:36:26 PM
-- Module Name: EncodingEpathGBT - Behavioral
-- Project Name: FELIX
-- Target Devices: FELIX
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

    use work.FELIX_package.all;
entity EncodingEpathGBT is
    generic (
        MAX_OUTPUT     : integer := 8;
        INCLUDE_8b     : std_logic := '1';
        INCLUDE_4b     : std_logic := '1';
        INCLUDE_2b     : std_logic := '1';
        INCLUDE_8b10b  : std_logic := '1';
        INCLUDE_HDLC   : std_logic := '1';
        INCLUDE_DIRECT : std_logic := '1';
        INCLUDE_TTC    : std_logic := '1';
        --BLOCKSIZE      : integer := 1024;
        GENERATE_FEI4B  : boolean := false;
        GENERATE_LCB_ENC : boolean := false;
        HDLC_IDLE_STATE : std_logic_vector(7 downto 0) := x"FF"; --IG: determine the HDLC line idle state. for EC: 0x7F, for IC: 0xFF
        USE_BUILT_IN_FIFO : std_logic := '1';
        SUPPORT_HDLC_DELAY : boolean := false;
        DISTR_RAM : boolean := false;
        INCLUDE_XOFF : boolean := false
    );
    port (
        clk40 : in std_logic; --BC clock for DataIn
        daq_reset : in std_logic; --Acitve high reset
        daq_fifo_flush : in std_logic; --Active high reset for the (built-in) FIFO
        EpathEnable : in std_logic; --From register map
        EpathEncoding : in std_logic_vector(3 downto 0); --0: direct, 1: 8b10b, 2: HDLC, 3: TTC
        ElinkWidth : in std_logic_vector(1 downto 0); --runtime configuration: 0:2, 1:4, 2:8,
        MsbFirst         : in std_logic; --Default 1, make 0 to reverse the bit order
        ReverseOutputBits : in std_logic; --Default 0, reverse the bits of the output Elink
        ElinkData : out std_logic_vector(MAX_OUTPUT-1 downto 0);
        toHostXoff : in std_logic;
        epath_almost_full : out std_logic;
        s_axis : in axis_8_type;  --FIFO read port (axi stream)
        s_axis_tready : out std_logic; --FIFO read tready (axi stream)
        s_axis_aclk : in std_logic; --FIFO read clock (axi stream)
        EnableHDLCDelay : in std_logic;
        TTCOption : in std_logic_vector(3 downto 0);
        TTCin       : in  TTC_data_type;
        FEI4Config : in std_logic_vector(5 downto 0) := (others => '0')

    );
end EncodingEpathGBT;

architecture Behavioral of EncodingEpathGBT is

    signal AxiStreamToByteDataOut : std_logic_vector(7 downto 0);
    signal AxiStreamToByteDataValidOut : std_logic;
    signal AxiStreamToByteEOPOut : std_logic;
    signal AxiStreamToByteTreadyin : std_logic;

    signal Encoder8b10bDataOut : std_logic_vector(9 downto 0);
    signal Encoder8b10bTreadyOut  : std_logic;

    signal EncoderFEI4ElinkOut : std_logic_vector(7 downto 0);
    signal EncoderFEI4readyOut : std_logic;

    signal GearBoxInputwidth : std_logic; -- 0:8b, 1:10b
    signal GearBoxDataIn : std_logic_vector(9 downto 0);
    signal GearBoxReadyOut : std_logic;
    signal GearBoxElinkData : std_logic_vector(MAX_OUTPUT-1 downto 0):= (others => '0');

    signal TTCElinkData : std_logic_vector(MAX_OUTPUT-1 downto 0);
    signal TTCEnable_i : std_logic;
    signal EncoderHDLCTreadyOut : std_logic;
    signal EncoderHDLCDataOut : std_logic_vector(1 downto 0);

    constant const_5 : std_logic_vector(15 downto 0) := x"5555";

begin

    Encoding_mux: process(GearBoxReadyOut, EpathEncoding, EpathEnable, GearBoxElinkData, AxiStreamToByteDataOut, Encoder8b10bDataOut, Encoder8b10bTreadyOut, TTCElinkData, EncoderHDLCDataOut, EncoderHDLCTreadyOut, EncoderFEI4ElinkOut, EncoderFEI4readyOut, ReverseOutputBits)
    begin
        if EpathEncoding = x"0" and INCLUDE_DIRECT = '1' and EpathEnable = '1' then
            ElinkData                   <= GearBoxElinkData;
            GearBoxDataIn               <= "00" & AxiStreamToByteDataOut;
            GearBoxInputwidth           <= '0';
            AxiStreamToByteTreadyin     <= GearBoxReadyOut;
        elsif EpathEncoding = x"1" and INCLUDE_8b10b = '1'  and EpathEnable = '1' then
            ElinkData                   <= GearBoxElinkData;
            GearBoxDataIn               <= Encoder8b10bDataOut;
            GearBoxInputwidth           <= '1';
            AxiStreamToByteTreadyin     <= Encoder8b10bTreadyOut;
        elsif EpathEncoding = x"2" and INCLUDE_HDLC = '1'  and EpathEnable = '1' then --bypassing gearbox
            if ReverseOutputBits = '1' then
                ElinkData(1 downto 0)       <= EncoderHDLCDataOut(0)&EncoderHDLCDataOut(1);
            else
                ElinkData(1 downto 0)       <= EncoderHDLCDataOut;
            end if;
            ElinkData(MAX_OUTPUT-1 downto 2) <= (others => '0');
            GearBoxDataIn               <= (others => '0');
            GearBoxInputwidth           <= '0';
            AxiStreamToByteTreadyin     <= EncoderHDLCTreadyOut;
        elsif EpathEncoding = x"3" and INCLUDE_TTC = '1'  and EpathEnable = '1' then
            ElinkData                   <= TTCElinkData(MAX_OUTPUT -1 downto 0);
            GearBoxDataIn               <= (others => '0');
            GearBoxInputwidth           <= '0';
            AxiStreamToByteTreadyin     <= '1';
        elsif EpathEncoding = x"5" and GENERATE_FEI4B and EpathEnable = '1' and INCLUDE_8b = '1' and MAX_OUTPUT = 8 then -- bypassing gearbox
            ElinkData                   <= EncoderFEI4ElinkOut(MAX_OUTPUT-1 downto 0);
            GearBoxDataIn               <= (others => '0');
            GearBoxInputwidth           <= '0';
            AxiStreamToByteTreadyin     <= EncoderFEI4readyOut;
        else
            AxiStreamToByteTreadyin         <= '1';
            ElinkData                       <= const_5(MAX_OUTPUT-1 downto 0);
            GearBoxDataIn                   <= (others => '0');
            GearBoxInputwidth               <= '1';
        end if;
    end process;

    g_includeTTC: if INCLUDE_TTC = '1' generate
        EncoderTTC0: entity work.EncoderTTC
            generic map(
                MAX_OUTPUT => MAX_OUTPUT
            )
            port Map (
                clk40      => clk40,
                enable      => TTCEnable_i,
                TTCOption    => TTCOption,
                TTCin          => TTCin,
                HGTD_Fast_CMD   => x"00", --HGTD Altiroc Fast CMD is only implemented in EncodingEpathLPGBT
                ToHostXoffIn => toHostXoff,
                ElinkOut        => TTCElinkData
            );
    end generate;

    TTCEnable_i <= '1' when (EpathEncoding = x"3") else '0';

    g_includeEpath: if (
        (MAX_OUTPUT = 2 and INCLUDE_2b = '1') or
        (MAX_OUTPUT = 4 and (INCLUDE_2b = '1' or INCLUDE_4b = '1')) or
        (MAX_OUTPUT = 8 and (INCLUDE_2b = '1' or INCLUDE_4b = '1' or INCLUDE_8b = '1'))
        ) generate


        gearbox0: entity work.EncodingGearBox
            generic map(
                MAX_OUTPUT => MAX_OUTPUT,
                MAX_INPUT => 10,
                SUPPORT_OUTPUT => INCLUDE_8b & INCLUDE_4b & INCLUDE_2b,
                SUPPORT_INPUT => INCLUDE_8b10b & INCLUDE_DIRECT
            )
            port map(
                Reset            => daq_reset,
                clk40            => clk40,

                ELinkData        => GearBoxElinkData,
                ElinkWidth       => ElinkWidth,
                MsbFirst         => MsbFirst,
                ReverseOutputBits => ReverseOutputBits,

                DataIn          => GearBoxDataIn,
                InputWidth      => GearBoxInputwidth, --: in std_logic_vector(2 downto 0); --runtime configuration: 0:8, 1:10, 2:20, 3:40, 4:66

                ReadyOut        => GearBoxReadyOut
            );

        g_include8b10b: if INCLUDE_8b10b = '1' generate
            Encoder8b10b0: entity work.Encoder8b10b
                Generic map (
                    GENERATE_FEI4B => GENERATE_FEI4B,
                    GENERATE_LCB_ENC => GENERATE_LCB_ENC,
                    INSERT_IDLES => true,
                    INCLUDE_XOFF => INCLUDE_XOFF
                )
                port map(
                    reset => daq_reset,
                    clk40 => clk40,
                    EnableIn => EpathEnable,
                    DataIn => AxiStreamToByteDataOut,
                    DataInValid => AxiStreamToByteDataValidOut,
                    EOP_in => AxiStreamToByteEOPOut,
                    toHostXoff => toHostXoff,
                    readyIn => GearBoxReadyOut,
                    HGTD_ALTIROC_ENCODING => '0',
                    readyOut => Encoder8b10bTreadyOut,
                    DataOut => Encoder8b10bDataOut
                );
        end generate; --INCLUDE_8b10b

        g_includeHDLC: if INCLUDE_HDLC = '1' generate
            EncoderHDLC0: entity work.EncoderHDLC
                generic map(
                    HDLC_IDLE_STATE => HDLC_IDLE_STATE, --: std_logic_vector(7 downto 0) := (others=>'1') --IG: determine the HDLC line idle state. for EC: 0x7F, for IC: 0xFF
                    SUPPORT_DELAY => SUPPORT_HDLC_DELAY,
                    DELAY_CYCLES => 9
                )
                port map(
                    clk40 => clk40,
                    rst => daq_reset,
                    EnableIn => EpathEnable,
                    EnableDelay => EnableHDLCDelay,
                    DataIn => AxiStreamToByteDataOut,
                    DataInValid => AxiStreamToByteDataValidOut,
                    EOP_in => AxiStreamToByteEOPOut,
                    readyOut => EncoderHDLCTreadyOut,
                    DataOut => EncoderHDLCDataOut
                );
        end generate; --INCLUDE_HDLC

        g_includeFEI4B: if GENERATE_FEI4B and INCLUDE_8b = '1' and MAX_OUTPUT = 8 generate
            EncoderFEI40: entity work.EncoderFEI4
                port map (
                    reset => daq_reset,
                    clk40 => clk40,
                    EnableIn => EpathEnable,
                    DataIn => AxiStreamToByteDataOut,
                    DataInValid => AxiStreamToByteDataValidOut,
                    EOP_in => AxiStreamToByteEOPOut,
                    readyOut => EncoderFEI4readyOut,
                    TTCin => TTCin,
                    ElinkOut => EncoderFEI4ElinkOut,
                    config => FEI4Config
                );
        end generate; --GENERATE_FEI4B


        g_includeFIFO: if  INCLUDE_8b10b = '1' or INCLUDE_HDLC = '1' or INCLUDE_DIRECT = '1' generate
            fromAxis0: entity work.AxiStreamToByte
                generic map(
                    BYTES => 1,
                    --BLOCKSIZE => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
                    DISTR_RAM => DISTR_RAM
                )
                port map(
                    clk40 => clk40,
                    reset => daq_fifo_flush,
                    EnableIn => EpathEnable,
                    s_axis => s_axis,
                    s_axis_tready => s_axis_tready,
                    s_axis_aclk => s_axis_aclk,

                    m_axis_tready => AxiStreamToByteTreadyin,
                    almost_full => epath_almost_full,

                    DataOut => AxiStreamToByteDataOut,
                    DataOutValid(0) => AxiStreamToByteDataValidOut,
                    EOP(0) => AxiStreamToByteEOPOut
                );
        end generate g_includeFIFO;
    end generate g_includeEpath;
    g_notincludeEpath: if not (
        (MAX_OUTPUT = 2 and INCLUDE_2b = '1') or
        (MAX_OUTPUT = 4 and (INCLUDE_2b = '1' or INCLUDE_4b = '1')) or
        (MAX_OUTPUT = 8 and (INCLUDE_2b = '1' or INCLUDE_4b = '1' or INCLUDE_8b = '1'))
        ) generate
        s_axis_tready <= '1'; --don't block the fifo
    --ElinkData <= (others => '0');
    end generate g_notincludeEpath;

end Behavioral;


--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Kazuki Todome
--!               Nico Giangiacomi
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  University and INFN Bologna
--! Engineer: Nico Giangiacomi
--!
--! Create Date:    02/02/2020
--! Module Name:    EncoderTTC
--! Project Name:   FELIX
--! Project description: Wrapper for 8b10b encoder
----------------------------------------------------------------------------------
--! Use standard library
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.FELIX_package.all;
    USE ieee.numeric_std.ALL;
--! a wrap for 8b10b decoder and alignment
entity EncoderTTC is
    generic (
        MAX_OUTPUT : integer := 8
    );
    port (
        clk40 : in std_logic; --BC clock
        enable : in std_logic; --Active high enable
        TTCOption : in std_logic_vector(3 downto 0);
        TTCin       : in TTC_data_type;
        HGTD_Fast_CMD     : in std_logic_vector(7 downto 0); --FastCMD for HGTD Altiroc ASIC, to be distributed as a TTC option.
        ToHostXoffIn : in std_logic;
        ElinkOut : out std_logic_vector((MAX_OUTPUT -1) downto 0) --ElinkElinkOut
    );
end EncoderTTC;

architecture Behavioral of EncoderTTC is

    signal TTCin_r  : TTC_data_type; -- use to sync the TTCin vector with the extended BCR pulse.
    signal TTCin_r2 : std_logic; -- BCR signal. Use to extend the BCR two a pulse of 2 clocks for OCR.
    signal TTCin_r9 : std_logic; -- broadcast bit#7 signal. When set, extend the BCR to 2 clocks pulse (OCR indication).
    signal ElinkOut_s: std_logic_vector(7 downto 0);
    signal new_SCA_reset_cnt: std_logic := '1'; --for NSW FLXUSER-648
    signal LFSRstate : STD_LOGIC_VECTOR(9 downto 0) := "1010101010";--for NSW FLXUSER-648

begin
    --Assign only relevant bits of TTS signals to output.
    ElinkOut(MAX_OUTPUT-1 downto 0) <= ElinkOut_s(MAX_OUTPUT-1 downto 0);

    new_SCA_reset_cnt <= LFSRstate(0);

    --Create some pseudo random data on one of the 8-bit E-Link bits in order to keep the GBTx descrambler alive
    lfsr_proc: process(clk40)
    begin
        if rising_edge(clk40) then
            if (enable = '0') then
                LFSRstate <= "1010101010";
            else
                LFSRstate <= (LFSRstate(9) xor LFSRstate(7) xor LFSRstate(6) xor LFSRstate(0)) & LFSRstate(9 downto 1);
            end if;
        end if;
    end process;

    output_select: process(clk40)
    begin
        if rising_edge(clk40) then
            -- synchronous reset
            if (enable = '0') then
                for i in 0 to 5 loop
                    ElinkOut_s     <= x"55";
                end loop;
            else
                case TTCOption is
                    when x"0" => ElinkOut_s <= "101010" & TTCin.Bchan &  TTCin.L1A;
                    when x"1" => ElinkOut_s <= x"A" & TTCin.Bchan & TTCin.ECR & TTCin.BCR & TTCin.L1A;
                    when x"2" => ElinkOut_s <= TTCin.Bchan & TTCin.Brcst(3 downto 0) & TTCin.ECR & TTCin.BCR & TTCin.L1A;
                    when x"3" => ElinkOut_s <= TTCin_r.L1A & TTCin_r.Brcst(1) & TTCin_r.ExtendedTestPulse & TTCin_r.ECR & TTCin_r2 & TTCin_r.L0A & TTCin_r.Brcst(3) & new_SCA_reset_cnt; --FLX-2069: L0A in Phase I is a delayed version of L1A
                    when x"4" => ElinkOut_s <= x"A" & (TTCin.BCR & TTCin.BCR & TTCin.BCR & TTCin.BCR); -- expend BCR signal over the entire 25nSec (4x BCR bit)
                    when x"5" => ElinkOut_s <= "000000" & (TTCin.BCR & TTCin_r.BCR);
                    when x"6" => ElinkOut_s <= TTCin_r.L1A & TTCin_r.Brcst_latched(1) & TTCin_r.ExtendedTestPulse & TTCin_r.ECR & TTCin_r2 & TTCin_r.L0A & TTCin_r.Brcst(3) & TTCin_r.ECR; --FLX-2069: L0A in Phase I is a delayed version of L1A
                    when x"7" => ElinkOut_s <= "1010" & TTCin.ECR & TTCin.BCR & ToHostXoffIn & TTCin.L1A;
                    when x"8" => ElinkOut_s <= HGTD_Fast_CMD;
                    when x"9" => ElinkOut_s <= TTCin_r.L1A & TTCin_r.Brcst(1) & TTCin_r.ExtendedTestPulse & TTCin_r.ECR & TTCin_r2 & TTCin_r.L0A & TTCin_r.Brcst(3) & TTCin_r.ECR; --FLX-2069: L0A in Phase I is a delayed version of L1A
                    when others => ElinkOut_s <= "10101010";
                end case;
            end if;
        end if;
    end process output_select;

    --
    -- sample the TTCin vector to support OCR modification
    process(clk40)
    begin
        if rising_edge(clk40) then
            -- synchronous enable
            if (enable = '0') then
                TTCin_r     <= TTC_zero;
            else
                TTCin_r     <= TTCin;
            end if;
        end if;
    end process;

    -- set the OCR indication
    process(clk40)
    begin
        if rising_edge(clk40) then
            -- synchronous enable
            if (enable = '0') then
                TTCin_r9      <= '0';
            -- OCR indication set
            elsif (TTCin.Brcst(5) = '1') then
                TTCin_r9      <= '1';
            -- clear the OCR indication when OCR indication set and BCR arrive
            elsif ((TTCin_r9 = '1') and (TTCin_r2 = '1')) then
                TTCin_r9      <= '0';
            -- keep the OCR indication value
            else
                TTCin_r9      <= TTCin_r9;
            end if;
        end if;
    end process;

    -- determine the BCR value
    process(clk40)
    begin
        if rising_edge(clk40) then
            -- synchronous enable
            if (enable = '0') then
                TTCin_r2      <= '0';
            -- BCR arrive
            elsif (TTCin.BCR = '1') then
                TTCin_r2      <= '1';
            -- OCR indication set and BCR arrive
            elsif ((TTCin_r9 = '1') and (TTCin_r2 = '1')) then
                TTCin_r2      <= '1';
            -- default value
            else
                TTCin_r2      <= '0';
            end if;
        end if;
    end process;

end Behavioral;


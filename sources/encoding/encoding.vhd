--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!               Nico Giangiacomi
--!               Elena Zhivun
--!               mtrovato
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 06/06/2019 08:38:50 AM
-- Design Name:
-- Module Name: decoding - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE, UNISIM;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use work.pcie_package.all;
    use work.strips_package.t_strips_config;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
Library xpm;
    use xpm.vcomponents.all;

    use UNISIM.VCOMPONENTS.all;
entity encoding is
    generic (
        GBT_NUM                         : integer := 4;
        FIRMWARE_MODE                   : integer := FIRMWARE_MODE_GBT;
        --BLOCKSIZE              : integer := 1024;
        STREAMS_FROMHOST                : integer := 1;
        IncludeEncodingEpath2_HDLC      : std_logic_vector(4 downto 0) := "11111";
        IncludeEncodingEpath2_8b10b     : std_logic_vector(4 downto 0) := "11111";
        IncludeEncodingEpath4_8b10b     : std_logic_vector(4 downto 0) := "11111";
        IncludeEncodingEpath8_8b10b     : std_logic_vector(4 downto 0) := "11111";
        INCLUDE_DIRECT                  : std_logic_vector(4 downto 0) := "11111";
        INCLUDE_TTC                     : std_logic_vector(4 downto 0) := "11111";
        INCLUDE_RD53                    : std_logic_vector(4 downto 0) := "11111";
        DEBUGGING_RD53                  : boolean := false;
        RD53Version                     : String := "A"; --A or B
        DISTR_RAM                       : boolean := false;
        SUPPORT_HDLC_DELAY              : boolean := false;
        USE_ULTRARAM_LCB                : boolean := false;
        INCLUDE_XOFF                    : boolean;
        USE_BUILT_IN_FIFO               : std_logic := '0'
    );
    Port (
        --TXUSERCLK            : in  std_logic_vector(GBT_NUM-1 downto 0);
        --clk250               : in  std_logic;
        clk40                : in  std_logic;
        --clk160               : in  std_logic;
        aclk                 : in std_logic;
        daq_reset            : in std_logic;
        daq_fifo_flush       : in std_logic;
        s_axis               : in axis_8_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_FROMHOST-1);
        s_axis_tready        : out  axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_FROMHOST-1);
        register_map_control : in  register_map_control_type;
        register_map_encoding_monitor : out register_map_encoding_monitor_type;
        GBT_DOWNLINK_USER_DATA : out  array_120b(0 to GBT_NUM-1);   --GBT data output
        lpGBT_DOWNLINK_USER_DATA            : out array_32b(0 to GBT_NUM-1); --TODO: Connect to E-groups, e.g. Strips LCB encoder -- @suppress "Unused port: lpGBT_DOWNLINK_USER_DATA is not used in work.encoding(Behavioral)"
        lpGBT_DOWNLINK_IC_DATA              : out array_2b(0 to GBT_NUM-1);
        lpGBT_DOWNLINK_EC_DATA              : out array_2b(0 to GBT_NUM-1);
        TTCin                               : in TTC_data_type;
        toHostXoff                          : in std_logic_vector(23 downto 0)
    );
end encoding;

architecture Behavioral of encoding is

    constant zeros16bit : std_logic_vector(15 downto 0) := (others=>'0');

begin

    g_gbtmode: if FIRMWARE_MODE = FIRMWARE_MODE_GBT or
                 FIRMWARE_MODE = FIRMWARE_MODE_FULL or
                 FIRMWARE_MODE = FIRMWARE_MODE_LTDB or
                 FIRMWARE_MODE = FIRMWARE_MODE_FEI4
                 generate

    begin

        g_GBT_Links: for link in 0 to GBT_NUM-1 generate
            signal EpathEnableEC, EpathEnableIC, EpathEnableAUX : std_logic;
            signal BitSwappingEC, BitSwappingIC, BitSwappingAUX : std_logic;
            signal MsbFirst : std_logic;
            signal EnableHDLCDelay : std_logic;
        begin

            EnableHDLCDelay <= to_sl(register_map_control.MINI_EGROUP_FROMHOST(link).ENABLE_DELAY);
            EpathEnableEC <= to_sl(register_map_control.MINI_EGROUP_FROMHOST(link).EC_ENABLE);
            BitSwappingEC <= to_sl(register_map_control.MINI_EGROUP_FROMHOST(link).EC_BIT_SWAPPING);
            EpathEnableIC <= to_sl(register_map_control.MINI_EGROUP_FROMHOST(link).IC_ENABLE);
            --! The IC bits in the GBTx ASIC are reversed with respect to the other HDLC E-Links (e.g. EC channel), See FLX-2243
            BitSwappingIC <= not to_sl(register_map_control.MINI_EGROUP_FROMHOST(link).IC_BIT_SWAPPING);
            EpathEnableAUX <= to_sl(register_map_control.MINI_EGROUP_FROMHOST(link).AUX_ENABLE);
            BitSwappingAUX <= to_sl(register_map_control.MINI_EGROUP_FROMHOST(link).AUX_BIT_SWAPPING);
            MsbFirst <= to_sl(register_map_control.ENCODING_REVERSE_10B);

            GBT_DOWNLINK_USER_DATA(link)(119 downto 116) <= "0101"; --needed for GBT alignment.

            EpathEC: entity work.EncodingEpathGBT
                generic map(
                    MAX_OUTPUT => 2,
                    INCLUDE_8b => '0',
                    INCLUDE_4b => '0',
                    INCLUDE_2b => '1',
                    INCLUDE_8b10b => '1',
                    INCLUDE_HDLC => '1',
                    INCLUDE_DIRECT => '1',
                    INCLUDE_TTC => '0',
                    --BLOCKSIZE => BLOCKSIZE,
                    GENERATE_FEI4B => false,
                    GENERATE_LCB_ENC => false,
                    HDLC_IDLE_STATE => x"FF",
                    USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
                    SUPPORT_HDLC_DELAY => SUPPORT_HDLC_DELAY,
                    DISTR_RAM => DISTR_RAM,
                    INCLUDE_XOFF => false
                )
                port map(
                    clk40 => clk40,
                    daq_reset => daq_reset,
                    daq_fifo_flush => daq_fifo_flush,
                    EpathEnable => EpathEnableEC,
                    EpathEncoding => register_map_control.MINI_EGROUP_FROMHOST(link).EC_ENCODING,
                    ElinkWidth => "00",
                    MsbFirst => MsbFirst,
                    ReverseOutputBits => BitSwappingEC,
                    ElinkData => GBT_DOWNLINK_USER_DATA(link)(113 downto 112),
                    toHostXoff => '0',
                    epath_almost_full => register_map_encoding_monitor.MINI_EGROUP_FROMHOST(link).EC_ALMOST_FULL(6),
                    s_axis => s_axis(link,STREAMS_FROMHOST-2),
                    s_axis_tready => s_axis_tready(link,STREAMS_FROMHOST-2),
                    s_axis_aclk => aclk,
                    EnableHDLCDelay => EnableHDLCDelay,
                    TTCOption => (others => '0'),
                    TTCin => TTC_zero,
                    FEI4Config => (others => '0')

                --m_axis_prog_empty => m_axis_prog_empty(link,41)
                );

            EpathIC: entity work.EncodingEpathGBT
                generic map(
                    MAX_OUTPUT => 2,
                    INCLUDE_8b => '0',
                    INCLUDE_4b => '0',
                    INCLUDE_2b => '1',
                    INCLUDE_8b10b => '0',
                    INCLUDE_HDLC => '1',
                    INCLUDE_DIRECT => '0',
                    INCLUDE_TTC => '0',
                    --BLOCKSIZE => BLOCKSIZE,
                    GENERATE_FEI4B => false,
                    GENERATE_LCB_ENC => false,
                    HDLC_IDLE_STATE => x"FF",
                    USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
                    SUPPORT_HDLC_DELAY => SUPPORT_HDLC_DELAY,
                    DISTR_RAM => DISTR_RAM,
                    INCLUDE_XOFF => false
                )
                port map(
                    clk40 => clk40,
                    daq_reset => daq_reset,
                    daq_fifo_flush => daq_fifo_flush,
                    EpathEnable => EpathEnableIC,
                    EpathEncoding => "0010", --Only support HDLC
                    ElinkWidth => "00",
                    MsbFirst => MsbFirst,
                    ReverseOutputBits => BitSwappingIC,
                    ElinkData => GBT_DOWNLINK_USER_DATA(link)(115 downto 114),
                    toHostXoff => '0',
                    epath_almost_full => register_map_encoding_monitor.MINI_EGROUP_FROMHOST(link).IC_ALMOST_FULL(9),
                    s_axis => s_axis(link,STREAMS_FROMHOST-1),
                    s_axis_tready => s_axis_tready(link,STREAMS_FROMHOST-1),
                    s_axis_aclk => aclk,
                    EnableHDLCDelay => EnableHDLCDelay,
                    TTCOption => (others => '0'),
                    TTCin => TTC_zero,
                    FEI4Config => (others => '0')
                );
            g_LTDB: if FIRMWARE_MODE = FIRMWARE_MODE_LTDB generate
                g_ltdb_tready: for epath in 0 to STREAMS_FROMHOST-4 generate
                    s_axis_tready(link,epath) <= '1';
                end generate;

                g_Egroups: for egroup in 0 to 4 generate
                    signal ElinkWidth: std_logic_vector(1 downto 0);
                    --signal PathEnable: std_logic_vector(7 downto 0);
                    --signal PathEncoding : std_logic_vector(31 downto 0);
                    --signal ReverseElinks : std_logic_vector(7 downto 0);
                    signal TTCOption : std_logic_vector(3 downto 0);
                    signal s_axis_s : axis_8_array_type(0 to 7);
                    --signal s_axis_tready_s : axis_tready_array_type(0 to 7);
                    --signal EPATH_ALMOST_FULL : std_logic_vector(7 downto 0);
                    signal EnableHDLCDelay : std_logic;
                    signal EGROUP_BITS: std_logic_vector(15 downto 0);
                    signal AUX_BITS: std_logic_vector(1 downto 0);
                begin
                    --! No AXI stream for E-groups in LTDB mode, only the AUX e-link has one.
                    g_axisindex: for i in 0 to 7 generate
                        s_axis_s(i) <= (tdata => x"00", tvalid => '0', tlast => '0');
                    end generate;
                    g_LT12: if link < 12 generate
                        register_map_encoding_monitor.ENCODING_EGROUP_CTRL(link)(egroup).EPATH_ALMOST_FULL <= x"00";
                    end generate;
                    --PathEncoding <= register_map_control.ENCODING_EGROUP_CTRL(link mod 12)(egroup).PATH_ENCODING;
                    --ReverseElinks <= register_map_control.ENCODING_EGROUP_CTRL(link mod 12)(egroup).REVERSE_ELINKS;
                    ElinkWidth <= register_map_control.ENCODING_EGROUP_CTRL(link mod 12)(egroup).EPATH_WIDTH(41 downto 40);
                    --PathEnable <= register_map_control.ENCODING_EGROUP_CTRL(link mod 12)(egroup).EPATH_ENA;
                    TTCOption <= register_map_control.ENCODING_EGROUP_CTRL(link mod 12)(egroup).TTC_OPTION;
                    EnableHDLCDelay <= to_sl(register_map_control.ENCODING_EGROUP_CTRL(link mod 12)(egroup).ENABLE_DELAY);

                    g_group4: if egroup = 4 generate --E-group 4 contains the AUX channel

                        EpathAUX: entity work.EncodingEpathGBT
                            generic map(
                                MAX_OUTPUT => 2,
                                INCLUDE_8b => '0',
                                INCLUDE_4b => '0',
                                INCLUDE_2b => '1',
                                INCLUDE_8b10b => '1',
                                INCLUDE_HDLC => '1',
                                INCLUDE_DIRECT => '1',
                                INCLUDE_TTC => '1',
                                --BLOCKSIZE => BLOCKSIZE,
                                GENERATE_FEI4B => false,
                                GENERATE_LCB_ENC => false,
                                HDLC_IDLE_STATE => x"FF",
                                USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
                                SUPPORT_HDLC_DELAY => SUPPORT_HDLC_DELAY,
                                DISTR_RAM => DISTR_RAM,
                                INCLUDE_XOFF => false
                            )
                            port map(
                                clk40 => clk40,
                                daq_reset => daq_reset,
                                daq_fifo_flush => daq_fifo_flush,
                                EpathEnable => EpathEnableAUX,
                                EpathEncoding => register_map_control.MINI_EGROUP_FROMHOST(link).AUX_ENCODING,
                                ElinkWidth => "00",
                                MsbFirst => MsbFirst,
                                ReverseOutputBits => BitSwappingAUX,
                                ElinkData => AUX_BITS,
                                toHostXoff => '0',
                                epath_almost_full => register_map_encoding_monitor.MINI_EGROUP_FROMHOST(link).AUX_ALMOST_FULL(12),
                                s_axis => s_axis(link,STREAMS_FROMHOST-3),
                                s_axis_tready => s_axis_tready(link,STREAMS_FROMHOST-3),
                                s_axis_aclk => aclk,
                                EnableHDLCDelay => EnableHDLCDelay,
                                TTCOption => TTCOption,
                                TTCin => TTCin,
                                FEI4Config => (others => '0')
                            );
                        GBT_DOWNLINK_USER_DATA(link)((2+egroup)*16+15 downto (2+egroup)*16) <= AUX_BITS & EGROUP_BITS(13 downto 0);
                    else generate
                        GBT_DOWNLINK_USER_DATA(link)((2+egroup)*16+15 downto (2+egroup)*16) <= EGROUP_BITS;
                    end generate;
                    --! Instantiate one Egroup.
                    eGroup0: entity work.EncodingEgroupGBT
                        generic map(
                            INCLUDE_8b => '1',
                            INCLUDE_4b => '1',
                            INCLUDE_2b => '1',
                            INCLUDE_8b10b => '0',
                            INCLUDE_HDLC => '0',
                            INCLUDE_DIRECT => '0',
                            INCLUDE_TTC => '1', --These e-groups contain no FIFO, only TTC distribution
                            --BLOCKSIZE => BLOCKSIZE,
                            GENERATE_FEI4B => false,
                            GENERATE_LCB_ENC => false,
                            USE_BUILT_IN_FIFO => "00000000",
                            SUPPORT_HDLC_DELAY => false,
                            DISTR_RAM => DISTR_RAM,
                            INCLUDE_XOFF => INCLUDE_XOFF
                        )
                        port map(
                            clk40 => clk40,
                            daq_reset => daq_reset,
                            daq_fifo_flush => daq_fifo_flush,
                            EpathEnable => x"FF",
                            EpathEncoding => x"33333333",
                            ElinkWidth => ElinkWidth,
                            MsbFirst         => '1',
                            ReverseOutputBits => x"00",
                            EGroupData => EGROUP_BITS,
                            toHostXoff => (others => '0'),
                            epath_almost_full => open,
                            s_axis => s_axis_s,
                            s_axis_tready => open,
                            s_axis_aclk => aclk,
                            EnableHDLCDelay => '0',
                            TTCOption => TTCOption,
                            TTCin     => TTCin,
                            FEI4Config => register_map_control.ENCODING_EGROUP_FEI4_CTRL(link mod 12)(egroup)
                        );
                end generate g_Egroups;
            end generate g_LTDB;

            g_notLTDB: if FIRMWARE_MODE /= FIRMWARE_MODE_LTDB generate
                signal ToHostXoffBits: std_logic_vector(39 downto 0);
            begin
                xoffmapping0: entity work.XoffMapping
                    generic map(
                        GBTid => link
                    )
                    port map(
                        --reset => reset,
                        clk40 => clk40,
                        --register_map_control => register_map_control,
                        toHostXoff => toHostXoff,
                        toHostXoffbits => ToHostXoffBits
                    );

                g_Egroups: for egroup in 0 to 4 generate
                    signal ElinkWidth: std_logic_vector(1 downto 0);
                    signal PathEnable: std_logic_vector(7 downto 0);
                    signal PathEncoding : std_logic_vector(31 downto 0);
                    signal ReverseElinks : std_logic_vector(7 downto 0);
                    signal TTCOption : std_logic_vector(3 downto 0);
                    signal s_axis_s : axis_8_array_type(0 to 7);
                    signal s_axis_tready_s : axis_tready_array_type(0 to 7);
                    signal EPATH_ALMOST_FULL : std_logic_vector(7 downto 0);
                    signal EnableHDLCDelay : std_logic;
                begin
                    --! Map axis 32 bus for 1 E-group to the complete 2D array.
                    g_axisindex: for i in 0 to 7 generate
                        s_axis_s(i) <= s_axis(link,egroup*8+i);
                        s_axis_tready(link,egroup*8+i) <= s_axis_tready_s(i);
                    end generate;
                    g_LT12: if link < 12 generate
                        register_map_encoding_monitor.ENCODING_EGROUP_CTRL(link)(egroup).EPATH_ALMOST_FULL <= EPATH_ALMOST_FULL;
                    end generate;
                    PathEncoding <= register_map_control.ENCODING_EGROUP_CTRL(link mod 12)(egroup).PATH_ENCODING;
                    ReverseElinks <= register_map_control.ENCODING_EGROUP_CTRL(link mod 12)(egroup).REVERSE_ELINKS;
                    ElinkWidth <= register_map_control.ENCODING_EGROUP_CTRL(link mod 12)(egroup).EPATH_WIDTH(41 downto 40);
                    PathEnable <= register_map_control.ENCODING_EGROUP_CTRL(link mod 12)(egroup).EPATH_ENA;
                    TTCOption <= register_map_control.ENCODING_EGROUP_CTRL(link mod 12)(egroup).TTC_OPTION;
                    EnableHDLCDelay <= to_sl(register_map_control.ENCODING_EGROUP_CTRL(link mod 12)(egroup).ENABLE_DELAY);
                    --! Instantiate one Egroup.
                    eGroup0: entity work.EncodingEgroupGBT
                        generic map(
                            INCLUDE_8b => IncludeEncodingEpath8_8b10b(egroup),
                            INCLUDE_4b => IncludeEncodingEpath4_8b10b(egroup),
                            INCLUDE_2b => IncludeEncodingEpath2_8b10b(egroup) or IncludeEncodingEpath2_HDLC(egroup),
                            INCLUDE_8b10b => IncludeEncodingEpath2_8b10b(egroup) or IncludeEncodingEpath4_8b10b(egroup) or IncludeEncodingEpath8_8b10b(egroup),
                            INCLUDE_HDLC => IncludeEncodingEpath2_HDLC(egroup),
                            INCLUDE_DIRECT => '1',
                            INCLUDE_TTC => '1',
                            --BLOCKSIZE => BLOCKSIZE,
                            GENERATE_FEI4B => (FIRMWARE_MODE = FIRMWARE_MODE_FEI4),
                            GENERATE_LCB_ENC => false,
                            USE_BUILT_IN_FIFO => "0"&USE_BUILT_IN_FIFO&"0"&USE_BUILT_IN_FIFO&"0"&USE_BUILT_IN_FIFO&"0"&USE_BUILT_IN_FIFO,
                            SUPPORT_HDLC_DELAY => SUPPORT_HDLC_DELAY,
                            DISTR_RAM => DISTR_RAM,
                            INCLUDE_XOFF => INCLUDE_XOFF
                        )
                        port map(
                            clk40 => clk40,
                            daq_reset => daq_reset,
                            daq_fifo_flush => daq_fifo_flush,
                            EpathEnable => PathEnable,
                            EpathEncoding => PathEncoding,
                            ElinkWidth => ElinkWidth,
                            MsbFirst         => MsbFirst,
                            ReverseOutputBits => ReverseElinks,
                            EGroupData => GBT_DOWNLINK_USER_DATA(link)((2+egroup)*16+15 downto (2+egroup)*16),
                            toHostXoff => ToHostXoffBits(7+egroup*8 downto egroup*8),
                            epath_almost_full => EPATH_ALMOST_FULL,
                            s_axis => s_axis_s,
                            s_axis_tready => s_axis_tready_s,
                            s_axis_aclk => aclk,
                            EnableHDLCDelay => EnableHDLCDelay,
                            TTCOption => TTCOption,
                            TTCin     => TTCin,
                            FEI4Config => register_map_control.ENCODING_EGROUP_FEI4_CTRL(link mod 12)(egroup)
                        );
                end generate g_Egroups;
            end generate g_notLTDB;
        end generate g_GBT_Links;



    end generate g_gbtmode;

    g_strips: if FIRMWARE_MODE = FIRMWARE_MODE_STRIP generate
        signal strips_config : t_strips_config;
        signal r3_trig : std_logic;
        signal amac_invert_polarity_out : std_logic;
        signal r3l1_invert_polarity : std_logic;
        signal lcb_invert_polarity  : std_logic;

        signal lpGBT_DOWNLINK_USER_DATA_s   : array_32b(0 to GBT_NUM-1);
        signal lpGBT_DOWNLINK_IC_DATA_s     : array_2b(0 to GBT_NUM-1);
        signal lpGBT_DOWNLINK_EC_DATA_s     : array_2b(0 to GBT_NUM-1);
    begin

        lpGBT_DOWNLINK_EC_DATA <= lpGBT_DOWNLINK_EC_DATA_s;
        lpGBT_DOWNLINK_IC_DATA <= lpGBT_DOWNLINK_IC_DATA_s;
        lpGBT_DOWNLINK_USER_DATA <= lpGBT_DOWNLINK_USER_DATA_s;

        amac_invert_polarity_out <= to_sl(register_map_control.GLOBAL_STRIPS_CONFIG.INVERT_AMAC_OUT);
        lcb_invert_polarity <= to_sl(register_map_control.GLOBAL_STRIPS_CONFIG.INVERT_LCB_OUT);
        r3l1_invert_polarity <= to_sl(register_map_control.GLOBAL_STRIPS_CONFIG.INVERT_R3L1_OUT);
        --l1_trig <= to_sl(register_map_control.STRIPS_L1_TRIGGER) or to_sl(register_map_control.STRIPS_R3L1_TRIGGER);
        r3_trig <= to_sl(register_map_control.STRIPS_R3_TRIGGER) or to_sl(register_map_control.STRIPS_R3L1_TRIGGER);

        strips_config.GLOBAL_STRIPS_CONFIG <= register_map_control.GLOBAL_STRIPS_CONFIG; -- TODO: not needed in phase2 ?
        strips_config.GLOBAL_TRICKLE_TRIGGER  <= register_map_control.GLOBAL_TRICKLE_TRIGGER; -- TODO: not needed in phase2 ?
        strips_config.STRIPS_R3_TRIGGER  <= register_map_control.STRIPS_R3_TRIGGER; -- software trigger for tests only
        strips_config.STRIPS_L1_TRIGGER  <= register_map_control.STRIPS_L1_TRIGGER; -- software trigger for tests only
        strips_config.STRIPS_R3L1_TRIGGER  <= register_map_control.STRIPS_R3L1_TRIGGER; -- software trigger for tests only

        g_links: for link in 0 to GBT_NUM-1 generate
            signal toAMACsignal : std_logic;
            signal EpathEnableIC : std_logic;
            signal amac_rst : std_logic;
            signal BitSwappingIC : std_logic;
            signal EnableHDLCDelay : std_logic;
        begin
            lpGBT_DOWNLINK_EC_DATA_s(link) <= (others => toAMACsignal); --connect both EC bits to AMAC
            Eencoding0: entity work.EndeavourEncoder
                generic map (
                    DEBUG_en => false,
                    DISTR_RAM => DISTR_RAM,
                    USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO
                )
                port map
                (
                    clk40 => clk40,
                    LinkAligned => register_map_control.MINI_EGROUP_FROMHOST(link).EC_ENABLE(0),
                    s_axis_aclk => aclk, --: in std_logic;
                    reset => amac_rst,
                    s_axis => s_axis(link, STREAMS_FROMHOST-2), --s_axis(i,0),
                    s_axis_tready => s_axis_tready(link, STREAMS_FROMHOST-2), --s_axis_tready(i,0),
                    invert_polarity => amac_invert_polarity_out,
                    amac_signal => toAMACsignal,
                    almost_full => register_map_encoding_monitor.MINI_EGROUP_FROMHOST(link).EC_ALMOST_FULL(6)
                );

            EnableHDLCDelay <= to_sl(register_map_control.MINI_EGROUP_FROMHOST(link).ENABLE_DELAY);
            EpathEnableIC <= to_sl(register_map_control.MINI_EGROUP_FROMHOST(link).IC_ENABLE);
            amac_rst <= (not to_sl(register_map_control.MINI_EGROUP_FROMHOST(link).EC_ENABLE)) or daq_reset;
            BitSwappingIC <= to_sl(register_map_control.MINI_EGROUP_FROMHOST(link).IC_BIT_SWAPPING);

            EpathIC: entity work.EncodingEpathGBT
                generic map(
                    MAX_OUTPUT => 2,
                    INCLUDE_8b => '0',
                    INCLUDE_4b => '0',
                    INCLUDE_2b => '1',
                    INCLUDE_8b10b => '0',
                    INCLUDE_HDLC => '1',
                    INCLUDE_DIRECT => '0',
                    INCLUDE_TTC => '0',
                    --BLOCKSIZE => BLOCKSIZE,
                    GENERATE_FEI4B => false,
                    GENERATE_LCB_ENC => false,
                    HDLC_IDLE_STATE => x"FF",
                    USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
                    SUPPORT_HDLC_DELAY => SUPPORT_HDLC_DELAY,
                    DISTR_RAM => DISTR_RAM,
                    INCLUDE_XOFF => false
                )
                port map(
                    clk40 => clk40,
                    daq_reset => daq_reset,
                    daq_fifo_flush => daq_fifo_flush,
                    EpathEnable => EpathEnableIC,
                    EpathEncoding => "0010", --Only support HDLC
                    ElinkWidth => "00",
                    MsbFirst => to_sl(register_map_control.ENCODING_REVERSE_10B),
                    ReverseOutputBits => BitSwappingIC,
                    ElinkData => lpGBT_DOWNLINK_IC_DATA_s(link),
                    toHostXoff => '0',
                    epath_almost_full => register_map_encoding_monitor.MINI_EGROUP_FROMHOST(link).IC_ALMOST_FULL(9),
                    s_axis => s_axis(link,STREAMS_FROMHOST-1),
                    s_axis_tready => s_axis_tready(link,STREAMS_FROMHOST-1),
                    s_axis_aclk => aclk,
                    EnableHDLCDelay => EnableHDLCDelay,
                    TTCOption => (others => '0'),
                    TTCin => TTC_zero,
                    FEI4Config => (others => '0')
                );

            -- Strips LCB encoders
            strips_links: for i in 0 to 3 generate
                signal strips_epath_almost_full : std_logic_vector(4 downto 0);
                signal strips_rst : std_logic_vector(4 downto 0);
                signal LCB_DOWNLINK_DATA, R3L1_DOWNLINK_DATA: std_logic_vector(3 downto 0);
                signal LCB_R3L1_ELINK_SWAP: std_logic;
            begin
                register_map_encoding_monitor.ENCODING_EGROUP_CTRL(link)(i).EPATH_ALMOST_FULL <= "000" & strips_epath_almost_full;
                strips_rst <= not register_map_control.ENCODING_EGROUP_CTRL(link)(i).EPATH_ENA(4 downto 0);
                LCB_R3L1_ELINK_SWAP <= register_map_control.ITKSTRIP_LCB_R3L1_ELINK_SWAP(4*link+i);

                strips_lcb_encoder: entity work.lcb_axi_encoder --@suppress
                    generic map (
                        --DEBUG_en => false,
                        USE_ULTRARAM => USE_ULTRARAM_LCB,
                        DISTR_RAM => DISTR_RAM,
                        USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO
                    )
                    port map (
                        daq_reset => daq_reset,
                        clk40 => clk40,
                        --ila_clk => clk40,
                        s_axis_aclk => aclk,
                        daq_fifo_flush => daq_fifo_flush,
                        invert_polarity => lcb_invert_polarity,
                        -- over-the-DMA configuration link
                        config_s_axis => s_axis(link, 5*i),
                        config_tready  => s_axis_tready(link, 5*i),
                        config_almost_full  => strips_epath_almost_full(0),
                        -- commands for the module
                        command_s_axis => s_axis(link, 5*i + 1),
                        command_tready => s_axis_tready(link, 5*i + 1),
                        command_almost_full  => strips_epath_almost_full(1),
                        -- trickle configuration update link
                        trickle_s_axis => s_axis(link, 5*i + 2),
                        trickle_tready => s_axis_tready(link, 5*i + 2),
                        trickle_almost_full  => strips_epath_almost_full(2),
                        -- TTC signals
                        bcr_i => TTCin.BCR,
                        l0a_i => TTCin.L0A, -- this is connected to L1A; there is no L0A signal
                        l0a_trig_i => TTCin.ITk_trig,
                        l0id_i  => TTCin.ITk_tag(6 downto 0),
                        sync_i => TTCin.ITk_sync,
                        -- configuration from global register map
                        config => strips_config,
                        frame_start_pulse_o => open,
                        regmap_o => open,
                        -- output signals
                        EdataOUT => LCB_DOWNLINK_DATA
                    );

                --
                lpGBT_DOWNLINK_USER_DATA_s(link)(i*8 + 7 downto i*8) <=
                                                                        R3L1_DOWNLINK_DATA & LCB_DOWNLINK_DATA when LCB_R3L1_ELINK_SWAP = '0' else
                                                                        LCB_DOWNLINK_DATA & R3L1_DOWNLINK_DATA;

                -- Strips R3L1 encoders
                strips_r3l1_encoder: entity work.r3l1_axi_encoder--@suppress
                    generic map (
                        --DEBUG_en => false,
                        DISTR_RAM => DISTR_RAM,
                        USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO
                    )
                    port map (
                        rst => strips_rst(3),
                        clk40 => clk40,
                        --ila_clk => clk40,
                        s_axis_aclk => aclk,
                        daq_fifo_flush => daq_fifo_flush,
                        invert_polarity => r3l1_invert_polarity,
                        -- over-the-DMA configuration link
                        config_s_axis => s_axis(link, 5*i + 3),
                        config_tready  => s_axis_tready(link, 5*i + 3),
                        config_almost_full  => strips_epath_almost_full(3),
                        -- commands for the module
                        command_s_axis => s_axis(link, 5*i + 4),
                        command_tready => s_axis_tready(link, 5*i + 4),
                        command_almost_full  => strips_epath_almost_full(4),
                        -- TTC signals
                        bcr_i => TTCin.BCR,
                        l1_trigger_i => TTCin.ITk_trig, -- TODO: does emulator support this?
                        l1_tag_i => TTCin.ITk_tag(6 downto 0), --register_map_control.GLOBAL_STRIPS_CONFIG.TEST_R3L1_TAG,
                        ITk_sync_i => TTCin.ITk_sync,
                        -- RoIE signals
                        r3_trigger_i => r3_trig, -- TODO: connect to the emulator?
                        r3_tag_i => register_map_control.GLOBAL_STRIPS_CONFIG.TEST_R3L1_TAG, -- TODO: connect to the emulator?
                        module_mask_i => register_map_control.GLOBAL_STRIPS_CONFIG.TEST_MODULE_MASK, -- TODO: connect to the emulator?
                        -- configuration from global register map
                        config => strips_config,
                        -- output signals
                        EdataOUT => R3L1_DOWNLINK_DATA
                    );
            end generate strips_links;
        end generate g_links;
    end generate g_strips;

    g_lpgbtmode: if FIRMWARE_MODE = FIRMWARE_MODE_PIXEL or
                    FIRMWARE_MODE = FIRMWARE_MODE_LPGBT generate
        --ram: A port: write
        signal CalTrigSeq_WRADDR : std_logic_vector(4 downto 0);
        signal CalTrigSeq_WRDATA : std_logic_vector(15 downto 0);
        signal CalTrigSeq_WE : std_logic_vector(0 downto 0);
        --ram: B port: read
        signal CalTrigSeq : std_logic_vector(15 downto 0) := (others => '0');
        signal ReadAddrCalTrigSeq_array : array_2d_5b(0 to GBT_NUM-1,0 to STREAMS_FROMHOST-1);
        signal HGTD_Fast_CMD: std_logic_vector(7 downto 0);
        signal RD53B_loopgen_reg : RD53_loopgen_type;
    begin


        process(clk40, daq_reset)
        begin
            if( daq_reset = '1' ) then
                CalTrigSeq_WRADDR <= (others => '0');
                CalTrigSeq_WRDATA <= (others => '0');
                CalTrigSeq_WE     <= "0";
            elsif rising_edge(clk40) then
                CalTrigSeq_WRADDR <= register_map_control.YARR_FROMHOST_CALTRIGSEQ_WRADDR(4 downto 0);
                CalTrigSeq_WRDATA <= register_map_control.YARR_FROMHOST_CALTRIGSEQ_WRDATA(15 downto 0);
                CalTrigSeq_WE     <= register_map_control.YARR_FROMHOST_CALTRIGSEQ_WE;
            end if;
        end process;
        g_hgtd_fastcmd: if FIRMWARE_MODE = FIRMWARE_MODE_LPGBT generate
            fastcmd0: entity work.HGTD_Altiroc_fastcmd
                port map(
                    clk40   => clk40,
                    TTCin   => TTCin,
                    FastCmd => HGTD_Fast_CMD,
                    HGTD_ALTIROC_FASTCMD_CONTROL => register_map_control.HGTD_ALTIROC_FASTCMD
                );
        end generate;
        --switch to block ram if tight on luts
        --switch to sdpram
        g_caltrigseq_mem: if FIRMWARE_MODE = FIRMWARE_MODE_PIXEL and RD53Version = "A" generate
            signal ReadAddrCalTrigSeq : std_logic_vector(4 downto 0);
        begin
            --mux to have addr from a single epath of LINK=0 in priority order
            ReadAddrCalTrigSeq <= ReadAddrCalTrigSeq_array(0,0)  when (register_map_control.ENCODING_EGROUP_CTRL(0)(0).EPATH_ENA(0) = '1') else
                                  ReadAddrCalTrigSeq_array(0,2)  when (register_map_control.ENCODING_EGROUP_CTRL(0)(0).EPATH_ENA(2) = '1') else
                                  ReadAddrCalTrigSeq_array(0,4)  when (register_map_control.ENCODING_EGROUP_CTRL(0)(1).EPATH_ENA(0) = '1') else
                                  ReadAddrCalTrigSeq_array(0,6)  when (register_map_control.ENCODING_EGROUP_CTRL(0)(1).EPATH_ENA(2) = '1') else
                                  ReadAddrCalTrigSeq_array(0,8)  when (register_map_control.ENCODING_EGROUP_CTRL(0)(2).EPATH_ENA(0) = '1') else
                                  ReadAddrCalTrigSeq_array(0,10) when (register_map_control.ENCODING_EGROUP_CTRL(0)(2).EPATH_ENA(2) = '1') else
                                  ReadAddrCalTrigSeq_array(0,12) when (register_map_control.ENCODING_EGROUP_CTRL(0)(3).EPATH_ENA(0) = '1') else
                                  ReadAddrCalTrigSeq_array(0,14) when (register_map_control.ENCODING_EGROUP_CTRL(0)(3).EPATH_ENA(2) = '1') else
                                  (others => '0');

            HGTD_Fast_CMD <= (others => '0');

            CalTrigSeq_mem : xpm_memory_tdpram
                generic map ( -- @suppress
                    ADDR_WIDTH_A         => f_log2(32),
                    ADDR_WIDTH_B         => f_log2(32),
                    AUTO_SLEEP_TIME      => 0,
                    BYTE_WRITE_WIDTH_A   => 16,
                    BYTE_WRITE_WIDTH_B   => 16,
                    CLOCKING_MODE        => "independent_clock",
                    ECC_MODE             => "no_ecc",
                    MEMORY_INIT_FILE     => "none", --../../../../sources/ip_cores/kintexUltrascale/CalPulseTrigSeq.mem",
                    MEMORY_INIT_PARAM    => "817e,6969,6363,a971,a66a,6969,6969,6969,6969,6969,6969,6969,6969,6969,6969,6969,6969,566a,566c,5671,5672,6969,6969,6969,6969,6969,6969,6969,6969,6969,6969,6969", --digital scan sequence
                    MEMORY_OPTIMIZATION  => "true",
                    MEMORY_PRIMITIVE     => "auto",
                    MEMORY_SIZE          => 16*32,
                    MESSAGE_CONTROL      => 1,
                    READ_DATA_WIDTH_A    => 16,
                    READ_DATA_WIDTH_B    => 16,
                    READ_LATENCY_A       => 1,
                    READ_LATENCY_B       => 1,
                    READ_RESET_VALUE_A   => "0",
                    READ_RESET_VALUE_B   => "0",
                    RST_MODE_A           => "SYNC",
                    RST_MODE_B           => "SYNC",
                    USE_EMBEDDED_CONSTRAINT => 0,
                    USE_MEM_INIT         => 1,
                    WAKEUP_TIME          => "disable_sleep",
                    WRITE_DATA_WIDTH_A   => 16,
                    WRITE_DATA_WIDTH_B   => 16,
                    WRITE_MODE_A         => "write_first",
                    WRITE_MODE_B         => "write_first"
                )
                port map (
                    sleep => '0',
                    clka => clk40,
                    rsta => daq_reset,
                    ena => '1',
                    regcea => '1',
                    wea => CalTrigSeq_WE,
                    addra => CalTrigSeq_WRADDR,
                    dina => CalTrigSeq_WRDATA,
                    injectsbiterra => '0',
                    injectdbiterra => '0',
                    douta          => open,
                    sbiterra       => open,
                    dbiterra => open,
                    clkb => clk40,
                    rstb => daq_reset,
                    enb => '1',
                    regceb => '1',
                    web => "0",
                    addrb          => ReadAddrCalTrigSeq,
                    dinb           => zeros16bit,
                    injectsbiterrb => '0',
                    injectdbiterrb => '0',
                    doutb => CalTrigSeq,
                    sbiterrb => open,
                    dbiterrb => open
                );
        end generate;

        process(clk40)
        begin
            if rising_edge(clk40) then
                RD53B_loopgen_reg.noInject          <= to_sl(register_map_control.ENCODING_ITKPIX_TRIGGER_GENERATOR.NO_INJECT);
                RD53B_loopgen_reg.edgeMode          <= to_sl(register_map_control.ENCODING_ITKPIX_TRIGGER_GENERATOR.EDGE_MODE);
                RD53B_loopgen_reg.edgeDelay         <= register_map_control.ENCODING_ITKPIX_TRIGGER_GENERATOR.EDGE_DELAY;
                RD53B_loopgen_reg.edgeDuration      <= register_map_control.ENCODING_ITKPIX_TRIGGER_GENERATOR.EDGE_DURATION;
                RD53B_loopgen_reg.trigDelay         <= register_map_control.ENCODING_ITKPIX_TRIGGER_GENERATOR.TRIG_DELAY;
                RD53B_loopgen_reg.trigMultiplier    <= register_map_control.ENCODING_ITKPIX_TRIGGER_GENERATOR.TRIG_MULTIPLIER;
            end if;
        end process;

        g_LPGBT_Links: for link in 0 to GBT_NUM-1 generate
            signal EpathEnableEC, EpathEnableIC : std_logic;
            signal BitSwappingEC, BitSwappingIC : std_logic;
            signal MsbFirst : std_logic;
            signal EnableHDLCDelay : std_logic;
            signal enAZ : std_logic;
            signal ref_dly_genCalTrig : std_logic_vector(7 downto 0);
            signal ref_cmd            : std_logic_vector(15 downto 0);
            signal cnt_cmd            : array_32b(0 to 3);
            signal cnt_trig_cmd       : array_32b(0 to 3);
            signal err_genCalTrig_dly : array_8b(0 to 3);

        begin
            --YARR
            ref_dly_genCalTrig <= register_map_control.YARR_DEBUG_ALLEGROUP_FROMHOST1(link mod 12).REF_DLY_GENCALTRIG(7 downto 0);
            ref_cmd            <= register_map_control.YARR_DEBUG_ALLEGROUP_FROMHOST2(link mod 12).REF_CMD(15 downto 0);
            --Sum over egroups
            g_lt12: if link < 12 generate
                register_map_encoding_monitor.YARR_DEBUG_ALLEGROUP_FROMHOST2(link).CNT_CMD(47 downto 16) <= std_logic_vector(unsigned(cnt_cmd(0)) + unsigned(cnt_cmd(1)) + unsigned(cnt_cmd(2)) + unsigned(cnt_cmd(3)));
                register_map_encoding_monitor.YARR_DEBUG_ALLEGROUP_FROMHOST1(link).CNT_TRIG_CMD(47 downto 16) <= std_logic_vector(unsigned(cnt_trig_cmd(0)) + unsigned(cnt_trig_cmd(1)) + unsigned(cnt_trig_cmd(2)) + unsigned(cnt_trig_cmd(3)));
                register_map_encoding_monitor.YARR_DEBUG_ALLEGROUP_FROMHOST1(link).ERR_GENCALTRIG_DLY(15 downto 8)  <= std_logic_vector(unsigned(err_genCalTrig_dly(0)) + unsigned(err_genCalTrig_dly(1)) + unsigned(err_genCalTrig_dly(2)) + unsigned(err_genCalTrig_dly(3)));
            end generate;
            --Auto-zero module for RD53A SyncFE
            enAZ      <= register_map_control.YARR_DEBUG_ALLEGROUP_FROMHOST1(link mod 12).RD53A_AZ_EN(48);
            --
            EpathEnableEC <= to_sl(register_map_control.MINI_EGROUP_FROMHOST(link mod 12).EC_ENABLE);
            BitSwappingEC <= to_sl(register_map_control.MINI_EGROUP_FROMHOST(link mod 12).EC_BIT_SWAPPING);
            EpathEnableIC <= to_sl(register_map_control.MINI_EGROUP_FROMHOST(link mod 12).IC_ENABLE);
            BitSwappingIC <= to_sl(register_map_control.MINI_EGROUP_FROMHOST(link mod 12).IC_BIT_SWAPPING);
            EnableHDLCDelay <= to_sl(register_map_control.MINI_EGROUP_FROMHOST(link mod 12).ENABLE_DELAY);
            MsbFirst <= to_sl(register_map_control.ENCODING_REVERSE_10B);

            EpathEC: entity work.EncodingEpathGBT
                generic map(
                    MAX_OUTPUT => 2,
                    INCLUDE_8b => '0',
                    INCLUDE_4b => '0',
                    INCLUDE_2b => '1',
                    INCLUDE_8b10b => '1',
                    INCLUDE_HDLC => '1',
                    INCLUDE_DIRECT => '1',
                    INCLUDE_TTC => '0',
                    --BLOCKSIZE => BLOCKSIZE,
                    GENERATE_FEI4B => false,
                    GENERATE_LCB_ENC => false,
                    HDLC_IDLE_STATE => x"FF",
                    USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
                    SUPPORT_HDLC_DELAY => SUPPORT_HDLC_DELAY,
                    DISTR_RAM => DISTR_RAM,
                    INCLUDE_XOFF => false
                )
                port map(
                    clk40 => clk40,
                    daq_reset => daq_reset,
                    daq_fifo_flush => daq_fifo_flush,
                    EpathEnable => EpathEnableEC,
                    EpathEncoding => register_map_control.MINI_EGROUP_FROMHOST(link).EC_ENCODING,
                    ElinkWidth => "00",
                    MsbFirst => MsbFirst,
                    ReverseOutputBits => BitSwappingEC,
                    ElinkData => lpGBT_DOWNLINK_EC_DATA(link),
                    toHostXoff => '0',
                    epath_almost_full => register_map_encoding_monitor.MINI_EGROUP_FROMHOST(link).EC_ALMOST_FULL(6),
                    s_axis => s_axis(link,STREAMS_FROMHOST-2),
                    s_axis_tready => s_axis_tready(link,STREAMS_FROMHOST-2),
                    s_axis_aclk => aclk,
                    EnableHDLCDelay => EnableHDLCDelay,
                    TTCOption => (others => '0'),
                    TTCin => TTC_zero,
                    FEI4Config => (others => '0')


                --m_axis_prog_empty => m_axis_prog_empty(link,41)
                );

            EpathIC: entity work.EncodingEpathGBT
                generic map(
                    MAX_OUTPUT => 2,
                    INCLUDE_8b => '0',
                    INCLUDE_4b => '0',
                    INCLUDE_2b => '1',
                    INCLUDE_8b10b => '0',
                    INCLUDE_HDLC => '1',
                    INCLUDE_DIRECT => '0',
                    INCLUDE_TTC => '0',
                    --BLOCKSIZE => BLOCKSIZE,
                    GENERATE_FEI4B => false,
                    GENERATE_LCB_ENC => false,
                    HDLC_IDLE_STATE => x"FF",
                    USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
                    SUPPORT_HDLC_DELAY => SUPPORT_HDLC_DELAY,
                    DISTR_RAM => DISTR_RAM,
                    INCLUDE_XOFF => false
                )
                port map(
                    clk40 => clk40,
                    daq_reset => daq_reset,
                    daq_fifo_flush => daq_fifo_flush,
                    EpathEnable => EpathEnableIC,
                    EpathEncoding => "0010", --Only support HDLC
                    ElinkWidth => "00",
                    MsbFirst => MsbFirst,
                    ReverseOutputBits => BitSwappingIC,
                    ElinkData => lpGBT_DOWNLINK_IC_DATA(link),
                    toHostXoff => '0',
                    epath_almost_full => register_map_encoding_monitor.MINI_EGROUP_FROMHOST(link).IC_ALMOST_FULL(9),
                    s_axis => s_axis(link,STREAMS_FROMHOST-1),
                    s_axis_tready => s_axis_tready(link,STREAMS_FROMHOST-1),
                    s_axis_aclk => aclk,
                    EnableHDLCDelay => EnableHDLCDelay,
                    TTCOption => (others => '0'),
                    TTCin => TTC_zero,
                    FEI4Config => (others => '0')
                );

            g_Egroups: for egroup in 0 to 3 generate
                signal ElinkWidth: std_logic_vector(1 downto 0);
                signal PathEnable: std_logic_vector(3 downto 0);     --4elinks
                signal PathEncoding : std_logic_vector(15 downto 0); --4bxelink
                signal ReverseElinks : std_logic_vector(3 downto 0);
                signal TTCOption : std_logic_vector(3 downto 0);
                signal s_axis_s : axis_8_array_type(0 to 3);
                signal s_axis_tready_s : axis_tready_array_type(0 to 3);
                signal EPATH_ALMOST_FULL : std_logic_vector(3 downto 0);
                signal EnableHDLCDelay : std_logic;
                signal ReadAddrCalTrigSeq_tmp : array_5b(0 to 3);
            begin
                --! Map axis 32 bus for 1 E-group to the complete 2D array.
                g_axisindex: for i in 0 to 3 generate
                    ReadAddrCalTrigSeq_array(link,egroup*4+i) <= ReadAddrCalTrigSeq_tmp(i);
                    s_axis_s(i) <= s_axis(link,egroup*4+i);
                    s_axis_tready(link,egroup*4+i) <= s_axis_tready_s(i);
                end generate;
                g_lt12_1: if link < 12 generate
                    register_map_encoding_monitor.ENCODING_EGROUP_CTRL(link)(egroup).EPATH_ALMOST_FULL <= x"0" & EPATH_ALMOST_FULL;
                end generate;
                PathEncoding <= register_map_control.ENCODING_EGROUP_CTRL(link mod 12)(egroup).PATH_ENCODING(23 downto 8);
                ReverseElinks <= register_map_control.ENCODING_EGROUP_CTRL(link mod 12)(egroup).REVERSE_ELINKS(46 downto 43);
                ElinkWidth <= register_map_control.ENCODING_EGROUP_CTRL(link mod 12)(egroup).EPATH_WIDTH(41 downto 40);
                PathEnable <= register_map_control.ENCODING_EGROUP_CTRL(link mod 12)(egroup).EPATH_ENA(3 downto 0);
                TTCOption <= register_map_control.ENCODING_EGROUP_CTRL(link mod 12)(egroup).TTC_OPTION;
                EnableHDLCDelay <= to_sl(register_map_control.ENCODING_EGROUP_CTRL(link mod 12)(egroup).ENABLE_DELAY);
                --! Instantiate one Egroup.
                eGroup0: entity work.EncodingEgroupLPGBT
                    generic map( -- @suppress "Generic map uses default values. Missing optional actuals: PCIE_ENDPOINT, LINK, EGROUP, EPATH"
                        INCLUDE_8b        => IncludeEncodingEpath8_8b10b(egroup),
                        INCLUDE_4b        => IncludeEncodingEpath4_8b10b(egroup),
                        INCLUDE_2b        => IncludeEncodingEpath2_8b10b(egroup) or IncludeEncodingEpath2_HDLC(egroup),
                        INCLUDE_8b10b     => IncludeEncodingEpath2_8b10b(egroup) or IncludeEncodingEpath4_8b10b(egroup) or IncludeEncodingEpath8_8b10b(egroup),
                        INCLUDE_HDLC      => IncludeEncodingEpath2_HDLC(egroup),
                        INCLUDE_DIRECT    => INCLUDE_DIRECT(egroup), --'1',
                        INCLUDE_TTC       => INCLUDE_TTC(egroup), --'1',
                        INCLUDE_RD53      => INCLUDE_RD53(egroup), --INCLUDE_RD53(FIRMWARE_MODE),
                        DEBUGGING_RD53    => DEBUGGING_RD53,
                        RD53Version       => RD53Version,
                        --BLOCKSIZE         => BLOCKSIZE,
                        GENERATE_FEI4B    => false,
                        GENERATE_LCB_ENC  => false,
                        SUPPORT_HDLC_DELAY => SUPPORT_HDLC_DELAY,
                        USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
                        DISTR_RAM => DISTR_RAM,
                        INCLUDE_XOFF => false
                    )
                    port map(
                        clk40             => clk40,
                        daq_reset         => daq_reset,
                        daq_fifo_flush    => daq_fifo_flush,
                        EpathEnable       => PathEnable,
                        EpathEncoding     => PathEncoding,
                        ElinkWidth        => ElinkWidth,
                        MsbFirst          => MsbFirst,
                        ReverseOutputBits => ReverseElinks,
                        EGroupData        => lpGBT_DOWNLINK_USER_DATA(link)(egroup*8+7 downto egroup*8),
                        toHostXoff        => (others => '0'),
                        epath_almost_full => EPATH_ALMOST_FULL,
                        s_axis            => s_axis_s,
                        s_axis_tready     => s_axis_tready_s,
                        s_axis_aclk       => aclk,
                        EnableHDLCDelay   => EnableHDLCDelay,
                        TTCOption         => TTCOption,
                        TTCin             => TTCin,
                        HGTD_Fast_CMD     => HGTD_Fast_CMD,
                        enAZ_in                      => enAZ,
                        CalTrigSeq_in =>   CalTrigSeq,
                        ReadAddrCalTrigSeq_out => ReadAddrCalTrigSeq_tmp,
                        RD53B_loopgen_reg   => RD53B_loopgen_reg,
                        --YARR debug info
                        ref_dly_genCalTrig_in        => ref_dly_genCalTrig,
                        ref_cmd_in                   => ref_cmd,
                        cnt_cmd_out                  => cnt_cmd(egroup),
                        cnt_trig_cmd_out             => cnt_trig_cmd(egroup),
                        err_genCalTrig_dly_out       => err_genCalTrig_dly(egroup),
                        cnt_time_firstTolastTrig_out => open

                    );
            end generate g_Egroups;
        end generate g_LPGBT_Links;


    end generate g_lpgbtmode;

    g_hgtdlumi: if FIRMWARE_MODE = FIRMWARE_MODE_HGTD_LUMI
                or FIRMWARE_MODE = FIRMWARE_MODE_BCM_PRIME
    generate
        g_links: for link in 0 to GBT_NUM-1 generate
            signal EpathEnableEC, EpathEnableIC : std_logic;
            signal BitSwappingEC, BitSwappingIC : std_logic;
            signal MsbFirst : std_logic;
            signal EnableHDLCDelay : std_logic;
        begin


            EpathEnableEC <= to_sl(register_map_control.MINI_EGROUP_FROMHOST(link).EC_ENABLE);
            BitSwappingEC <= to_sl(register_map_control.MINI_EGROUP_FROMHOST(link).EC_BIT_SWAPPING);
            EpathEnableIC <= to_sl(register_map_control.MINI_EGROUP_FROMHOST(link).IC_ENABLE);
            BitSwappingIC <= to_sl(register_map_control.MINI_EGROUP_FROMHOST(link).IC_BIT_SWAPPING);
            EnableHDLCDelay <= to_sl(register_map_control.MINI_EGROUP_FROMHOST(link).ENABLE_DELAY);
            MsbFirst <= to_sl(register_map_control.ENCODING_REVERSE_10B);

            EpathEC: entity work.EncodingEpathGBT
                generic map(
                    MAX_OUTPUT => 2,
                    INCLUDE_8b => '0',
                    INCLUDE_4b => '0',
                    INCLUDE_2b => '1',
                    INCLUDE_8b10b => '1',
                    INCLUDE_HDLC => '1',
                    INCLUDE_DIRECT => '1',
                    INCLUDE_TTC => '0',
                    --BLOCKSIZE => BLOCKSIZE,
                    GENERATE_FEI4B => false,
                    GENERATE_LCB_ENC => false,
                    HDLC_IDLE_STATE => x"FF",
                    USE_BUILT_IN_FIFO => '1',
                    SUPPORT_HDLC_DELAY => SUPPORT_HDLC_DELAY,
                    DISTR_RAM => DISTR_RAM,
                    INCLUDE_XOFF => false
                )
                port map(
                    clk40 => clk40,
                    daq_reset => daq_reset,
                    daq_fifo_flush => daq_fifo_flush,
                    EpathEnable => EpathEnableEC,
                    EpathEncoding => register_map_control.MINI_EGROUP_FROMHOST(link).EC_ENCODING,
                    ElinkWidth => "00",
                    MsbFirst => MsbFirst,
                    ReverseOutputBits => BitSwappingEC,
                    ElinkData => lpGBT_DOWNLINK_EC_DATA(link),
                    toHostXoff => '0',
                    epath_almost_full => register_map_encoding_monitor.MINI_EGROUP_FROMHOST(link).EC_ALMOST_FULL(6),
                    s_axis => s_axis(link,STREAMS_FROMHOST-2),
                    s_axis_tready => s_axis_tready(link,STREAMS_FROMHOST-2),
                    s_axis_aclk => aclk,
                    EnableHDLCDelay => EnableHDLCDelay,
                    TTCOption => (others => '0'),
                    TTCin => TTC_zero,
                    FEI4Config => (others => '0')


                --m_axis_prog_empty => m_axis_prog_empty(link,41)
                );

            EpathIC: entity work.EncodingEpathGBT
                generic map(
                    MAX_OUTPUT => 2,
                    INCLUDE_8b => '0',
                    INCLUDE_4b => '0',
                    INCLUDE_2b => '1',
                    INCLUDE_8b10b => '0',
                    INCLUDE_HDLC => '1',
                    INCLUDE_DIRECT => '1',
                    INCLUDE_TTC => '0',
                    --BLOCKSIZE => BLOCKSIZE,
                    GENERATE_FEI4B => false,
                    GENERATE_LCB_ENC => false,
                    HDLC_IDLE_STATE => x"FF",
                    USE_BUILT_IN_FIFO => '1',
                    SUPPORT_HDLC_DELAY => SUPPORT_HDLC_DELAY,
                    DISTR_RAM => DISTR_RAM,
                    INCLUDE_XOFF => false
                )
                port map(
                    clk40 => clk40,
                    daq_reset => daq_reset,
                    daq_fifo_flush => daq_fifo_flush,
                    EpathEnable => EpathEnableIC,
                    EpathEncoding => "0010", --Only support HDLC
                    ElinkWidth => "00",
                    MsbFirst => MsbFirst,
                    ReverseOutputBits => BitSwappingIC,
                    ElinkData => lpGBT_DOWNLINK_IC_DATA(link),
                    toHostXoff => '0',
                    epath_almost_full => register_map_encoding_monitor.MINI_EGROUP_FROMHOST(link).IC_ALMOST_FULL(9),
                    s_axis => s_axis(link,STREAMS_FROMHOST-1),
                    s_axis_tready => s_axis_tready(link,STREAMS_FROMHOST-1),
                    s_axis_aclk => aclk,
                    EnableHDLCDelay => EnableHDLCDelay,
                    TTCOption => (others => '0'),
                    TTCin => TTC_zero,
                    FEI4Config => (others => '0')
                );

            -- downlink unused
            lpGBT_DOWNLINK_USER_DATA(link) <= (others => '0');
        end generate g_links;
    end generate;


end Behavioral;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!               Frans Schreuder
--!               Nico Giangiacomi
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company: University of Bologna
-- Engineer: Nico Giangiacomi
--
-- Create Date: 03/02/2020 03:36:26 PM
-- Module Name: EncodingEGroupGBT - Behavioral
-- Project Name: FELIX
-- Target Devices: FELIX
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;

entity EncodingEgroupGBT is
    generic (
        INCLUDE_8b     : std_logic := '1';
        INCLUDE_4b     : std_logic := '1';
        INCLUDE_2b     : std_logic := '1';
        INCLUDE_8b10b  : std_logic := '1';
        INCLUDE_HDLC   : std_logic := '1';
        INCLUDE_DIRECT : std_logic := '1';
        INCLUDE_TTC : std_logic := '1';
        --BLOCKSIZE      : integer := 1024;
        GENERATE_FEI4B  : boolean := false;
        GENERATE_LCB_ENC : boolean := false;
        USE_BUILT_IN_FIFO : std_logic_vector(7 downto 0) := "00000000";
        SUPPORT_HDLC_DELAY : boolean := false;
        DISTR_RAM : boolean := false;
        INCLUDE_XOFF : boolean
    );
    port (
        clk40 : in std_logic; --BC clock for DataIn
        daq_reset : in std_logic; --Acitve high reset
        daq_fifo_flush : in std_logic; --Active high reset for the (built-in) fifo
        EpathEnable : in std_logic_vector(7 downto 0); --From register map
        EpathEncoding : in std_logic_vector(31 downto 0); -- 4 bits per EPath: 0: direct, 1: 8b10b, 2: HDLC 3: TTC
        ElinkWidth : in std_logic_vector(1 downto 0); --runtime configuration: 0:2, 1:4, 2:8
        MsbFirst         : in std_logic; --Default 1, make 0 to reverse the bit order
        ReverseOutputBits : in std_logic_vector(7 downto 0); --Default 0, reverse the bits of the input Elink
        EGroupData : out std_logic_vector(15 downto 0);
        toHostXoff : in std_logic_vector(7 downto 0);
        epath_almost_full : out std_logic_vector(7 downto 0);
        s_axis : in axis_8_array_type(0 to 7);  --FIFO write port (axi stream)
        s_axis_tready : out axis_tready_array_type(0 to 7); --FIFO write tready (axi stream)
        s_axis_aclk : in std_logic; --FIFO write clock (axi stream)
        EnableHDLCDelay : in std_logic;
        TTCOption   : in std_logic_vector(3 downto 0);
        TTCin        : in TTC_data_type; --IG: bit #10 is the NSW extended Test Pulse
        FEI4Config   : in bitfield_encoding_egroup_fei4_ctrl_w_type
    );
end EncodingEgroupGBT;

architecture rtl of EncodingEgroupGBT is

    type IntArray is array (0 to 7) of integer;
    constant ElinkWidths  : IntArray := (8, 2, 4, 2, 8, 2, 4, 2);

    type slv8_array is array (natural range <>) of std_logic_vector(7 downto 0);
    signal EGroupData_i : slv8_array(0 to 7);

    type slv6_array is array (natural range <>) of std_logic_vector(5 downto 0);
    signal FEI4EpathConfig : slv6_array(0 to 7);

begin

    --! If timing is an issue here, we can make this a clocked process.
    EpathDist: process(ElinkWidth, EGroupData_i)
    begin
        if ElinkWidth = "00" then --2b E-links
            EGroupData <= EGroupData_i(7)(1 downto 0) &
                          EGroupData_i(6)(1 downto 0) &
                          EGroupData_i(5)(1 downto 0) &
                          EGroupData_i(4)(1 downto 0) &
                          EGroupData_i(3)(1 downto 0) &
                          EGroupData_i(2)(1 downto 0) &
                          EGroupData_i(1)(1 downto 0) &
                          EGroupData_i(0)(1 downto 0);
        elsif ElinkWidth = "01" then --4b E-links
            EGroupData <= EGroupData_i(6)(3 downto 0) &
                          EGroupData_i(4)(3 downto 0) &
                          EGroupData_i(2)(3 downto 0) &
                          EGroupData_i(0)(3 downto 0);
        elsif ElinkWidth = "10" then --8b E-links
            EGroupData <= EGroupData_i(4)(7 downto 0) &
                          EGroupData_i(0)(7 downto 0);
        else                         --"11" is an invalid option
            EGroupData <= (others => '0');
        end if;
    end process;

    -- distribute FE-I4 encoder configuration to epaths (but only those with 8 bit width)
    FEI4ConfigDist: process (FEI4Config)
    begin
        for i in 0 to 7 loop
            if ElinkWidths(i) = 8 then
                if i > 3 then
                    FEI4EpathConfig(i) <= FEI4Config.PHASE_DELAY1 & FEI4Config.MANCHESTER_ENABLE1 & FEI4Config.AUTOMATIC_MERGE_DISABLE1 & FEI4Config.TTC_SELECT1;
                else
                    FEI4EpathConfig(i) <= FEI4Config.PHASE_DELAY0 & FEI4Config.MANCHESTER_ENABLE0 & FEI4Config.AUTOMATIC_MERGE_DISABLE0 & FEI4Config.TTC_SELECT0;
                end if;
            else
                FEI4EpathConfig(i) <= (others => '0');
            end if;
        end loop;
    end process;

    g_Epaths: for i in 0 to 7 generate
        Epath0: entity work.EncodingEpathGBT
            generic map(
                MAX_OUTPUT => ElinkWidths(i),
                INCLUDE_8b => INCLUDE_8b,
                INCLUDE_4b => INCLUDE_4b,
                INCLUDE_2b => INCLUDE_2b,
                INCLUDE_8b10b => INCLUDE_8b10b,
                INCLUDE_HDLC => INCLUDE_HDLC,
                INCLUDE_DIRECT => INCLUDE_DIRECT,
                INCLUDE_TTC => INCLUDE_TTC,
                --BLOCKSIZE => BLOCKSIZE,
                GENERATE_FEI4B => GENERATE_FEI4B,
                GENERATE_LCB_ENC => GENERATE_LCB_ENC,
                HDLC_IDLE_STATE => x"FF",
                USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO(i),
                SUPPORT_HDLC_DELAY => SUPPORT_HDLC_DELAY,
                DISTR_RAM => DISTR_RAM,
                INCLUDE_XOFF => INCLUDE_XOFF

            )
            port map(
                clk40             => clk40,
                daq_reset => daq_reset,
                daq_fifo_flush => daq_fifo_flush,
                EpathEnable       => EpathEnable(i),
                EpathEncoding     => EpathEncoding(4*i+3 downto 4*i),
                ElinkWidth        => ElinkWidth,
                MsbFirst          => MsbFirst,
                ReverseOutputBits => ReverseOutputBits(i),
                ElinkData         => EGroupData_i(i)(ElinkWidths(i)-1 downto 0),
                toHostXoff        => toHostXoff(i),
                epath_almost_full => epath_almost_full(i),
                s_axis            => s_axis(i),
                s_axis_tready     => s_axis_tready(i),
                s_axis_aclk       => s_axis_aclk,
                EnableHDLCDelay   => EnableHDLCDelay,
                TTCOption                        => TTCOption,
                TTCin                            => TTCin,
                FEI4Config                       => FEI4EpathConfig(i)
            );
    end generate;


end rtl;

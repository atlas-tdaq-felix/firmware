--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!      Nayib Boukadida
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
library IEEE, UNISIM;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use work.pcie_package.all;
    use work.strips_package.t_strips_config;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
Library xpm;
    use xpm.vcomponents.all;

entity LTI_FE_Transmitter is
    generic(
        GBT_NUM : integer := 4
    );
    Port(
        clk40                   : in  std_logic;
        daq_fifo_flush          : in  std_logic;
        TTCin                   : in  TTC_data_type;
        XoffIn                  : in  std_logic_vector(GBT_NUM-1 downto 0);
        LTI_TX_Data_Transceiver : out array_32b(0 to GBT_NUM - 1);
        LTI_TX_TX_CharIsK       : out array_4b(0 to GBT_NUM - 1);
        LTI_TXUSRCLK_in         : in  std_logic_vector(GBT_NUM - 1 downto 0)
    );
end LTI_FE_Transmitter;

architecture Behavioral of LTI_FE_Transmitter is
    --1 per quad, as they share a clock.
    signal LTI_TX_Data_Transceiver_s : array_32b(0 to GBT_NUM-1);
    signal LTI_TX_TX_CharIsK_s       : array_4b(0 to GBT_NUM-1);
    signal TTCin_p1                   :  TTC_data_type;
begin


    frame_pipeline : process(clk40) is
    begin
        if rising_edge(clk40) then
            TTCin_p1 <= TTCin;
        end if;
    end process;



    g_channels : for ch in 0 to GBT_NUM - 1 generate
        constant d16_2                                                  : std_logic_vector(7 downto 0) := x"50";
        constant k28_5                                                  : std_logic_vector(7 downto 0) := x"BC";
        signal crc_reset, crc_en                                        : std_logic;
        signal crc_out                                                  : std_logic_vector(15 downto 0);
        signal wr_en, full, wr_rst_busy, rd_en, empty, rd_rst_busy      : std_logic;
        signal LTI_Frame_240                                            : std_logic_vector(159 downto 0);
        signal frame_counter                                            : integer range 0 to 5;
        signal crc_in: std_logic_vector(31 downto 0);
        signal LTI_Frame : std_logic_vector(159 downto 0);
    begin

        wr_en <= not full and not wr_rst_busy;
        rd_en <= not empty and not rd_rst_busy;

        crc_reset <= '1' when frame_counter = 5 else '0';
        crc_en <= not crc_reset;
        crc_in <= LTI_TX_Data_Transceiver_s(ch) when frame_counter /= 5 else (others => '1');

        --Generate TTC frame per channel, as the Xoff input may be different for each channel.
        gen_frame : process(clk40) is
            variable frame_type: std_logic;
        begin
            if rising_edge(clk40) then
                --If anything relevant needs to be transmitted with frame type 0, transmit it, otherwise transmit frame type 1, including Xoff.
                frame_type := '1';
                if (
                        TTCin.SL0ID = '1' or
                        TTCin.SOrb = '1' or
                        TTCin.Sync = '1' or
                        TTCin.GRst = '1' or
                        TTCin.L0A = '1' or
                        TTCin.L0ID /= TTCin_p1.L0ID or
                        TTCin.OrbitID /= TTCin_p1.OrbitID or
                        TTCin.TriggerType /= TTCin_p1.TriggerType or
                        TTCin.LBID /= TTCin_p1.LBID
                    ) then
                    frame_type := '0';
                end if;


                if frame_type = '0' then
                    LTI_Frame <= TTCin.TriggerType & TTCin.LBID & --4
                                 TTCin.OrbitID &                  --3
                                 TTCin.L0ID(37 downto 6)  &       --2
                                 TTCin.SyncGlobalData & TTCin.TS & TTCin.ErrorFlags & TTCin.SL0ID & TTCin.SOrb & TTCin.Sync & TTCin.GRst & TTCin.L0A & TTCin.L0ID(5 downto 0) & --1
                                 '0' & TTCin.PT & TTCin.Partition & TTCin.BCID & TTCin.SyncUserData;  --0
                else
                    LTI_Frame <= x"0000_0000" &                            --4
                                 TTCin.AsyncUserData(63 downto 32) &       --3
                                 TTCin.AsyncUserData(31 downto 0)  &       --2
                                 TTCin.SyncGlobalData & TTCin.TS & TTCin.ErrorFlags & XoffIn(ch) & "0000000000" &  --1
                                 '1' & TTCin.PT & TTCin.Partition & TTCin.BCID & TTCin.SyncUserData;  --0
                end if;
            end if;
        end process gen_frame;


        fifo_0 : xpm_fifo_async
            generic map(
                FIFO_MEMORY_TYPE    => "auto",
                FIFO_WRITE_DEPTH    => 16,
                CASCADE_HEIGHT      => 0,
                RELATED_CLOCKS      => 0,
                WRITE_DATA_WIDTH    => 160,
                READ_MODE           => "fwft",
                FIFO_READ_LATENCY   => 1,
                FULL_RESET_VALUE    => 1,
                USE_ADV_FEATURES    => "0000",
                READ_DATA_WIDTH     => 160,
                CDC_SYNC_STAGES     => 2,
                WR_DATA_COUNT_WIDTH => 1,
                PROG_FULL_THRESH    => 12,
                RD_DATA_COUNT_WIDTH => 1,
                PROG_EMPTY_THRESH   => 4,
                DOUT_RESET_VALUE    => "0",
                ECC_MODE            => "no_ecc",
                SIM_ASSERT_CHK      => 0,
                WAKEUP_TIME         => 0
            )
            port map(
                sleep         => '0',
                rst           => daq_fifo_flush,
                wr_clk        => clk40,
                wr_en         => wr_en,
                din           => LTI_Frame,
                full          => full,
                prog_full     => open,
                wr_data_count => open,
                overflow      => open,
                wr_rst_busy   => wr_rst_busy,
                almost_full   => open,
                wr_ack        => open,
                rd_clk        => LTI_TXUSRCLK_in(ch),
                rd_en         => rd_en,
                dout          => LTI_Frame_240,
                empty         => empty,
                prog_empty    => open,
                rd_data_count => open,
                underflow     => open,
                rd_rst_busy   => rd_rst_busy,
                almost_empty  => open,
                data_valid    => open,
                injectsbiterr => '0',
                injectdbiterr => '0',
                sbiterr       => open,
                dbiterr       => open
            );

        CRC16_0 : entity work.crc16_lti
            port map(
                data_in => crc_in,
                crc_en  => crc_en,
                rst     => crc_reset,
                clk     => LTI_TXUSRCLK_in(ch),
                crc_out => crc_out
            );

        frame_output_proc : process(LTI_TXUSRCLK_in(ch)) is
        begin
            if rising_edge(LTI_TXUSRCLK_in(ch)) then
                if frame_counter /= 5 then
                    frame_counter <= frame_counter + 1;
                else
                    frame_counter <= 0;
                end if;
                if rd_en = '1' then
                    frame_counter <= 1;
                end if;
            end if;
        end process frame_output_proc;

        frame_output_proc_async: process(LTI_Frame_240, crc_out, frame_counter)
        begin
            LTI_TX_TX_CharIsK_s(ch) <= "0000"; --default
            case frame_counter is
                when 0 => LTI_TX_Data_Transceiver_s(ch) <= LTI_Frame_240(31 downto 0);
                when 1 => LTI_TX_Data_Transceiver_s(ch) <= LTI_Frame_240(63 downto 32);
                when 2 => LTI_TX_Data_Transceiver_s(ch) <= LTI_Frame_240(95 downto 64);
                when 3 => LTI_TX_Data_Transceiver_s(ch) <= LTI_Frame_240(127 downto 96);
                when 4 => LTI_TX_Data_Transceiver_s(ch) <= LTI_Frame_240(159 downto 128);
                when 5 => LTI_TX_Data_Transceiver_s(ch) <= crc_out & d16_2 & k28_5; LTI_TX_TX_CharIsK_s(ch) <= "0001";
                when others => LTI_TX_Data_Transceiver_s(ch) <= (others => '0');
            end case;
        end process;
        LTI_TX_Data_Transceiver(ch) <= LTI_TX_Data_Transceiver_s(ch);
        LTI_TX_TX_CharIsK(ch) <= LTI_TX_TX_CharIsK_s(ch);
    end generate g_channels;

end architecture;

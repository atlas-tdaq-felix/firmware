--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               Nico Giangiacomi
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.numeric_std.all;
    use work.axi_stream_package.all;
    use work.FELIX_package.all;

entity EncodingEgroupLPGBT is
    generic (
        INCLUDE_8b     : std_logic := '1';
        INCLUDE_4b     : std_logic := '1';
        INCLUDE_2b     : std_logic := '1';
        INCLUDE_8b10b  : std_logic := '1';
        INCLUDE_HDLC   : std_logic := '1';
        INCLUDE_DIRECT : std_logic := '1';
        INCLUDE_TTC : std_logic := '1';
        INCLUDE_RD53    : std_logic := '1';
        DEBUGGING_RD53  : boolean := false;
        RD53Version     : String := "A"; --A or B
        --BLOCKSIZE       : integer := 1024;
        GENERATE_FEI4B  : boolean := false;
        GENERATE_LCB_ENC : boolean := false;
        SUPPORT_HDLC_DELAY : boolean := false;
        USE_BUILT_IN_FIFO : std_logic := '0';
        DISTR_RAM : boolean := false;
        INCLUDE_XOFF : boolean
    );
    port (
        clk40             : in std_logic;                       --BC clock for DataIn
        daq_reset         : in std_logic;                       --Acitve high reset
        daq_fifo_flush    : in std_logic;                       --Acitve high reset for the (built-in) fifo
        EpathEnable       : in std_logic_vector(3 downto 0);    --From register map
        EpathEncoding     : in std_logic_vector(15 downto 0);   -- 4 bits per EPath: 0: direct, 1: 8b10b, 2: HDLC 3: TTC
        ElinkWidth        : in std_logic_vector(1 downto 0);    --runtime configuration: 0:2, 1:4, 2:8
        MsbFirst          : in std_logic;                       --Default 1, make 0 to reverse the bit order
        ReverseOutputBits : in std_logic_vector(3 downto 0);    --Default 0, reverse the bits of the input Elink
        EGroupData        : out std_logic_vector(7 downto 0);
        toHostXoff        : in std_logic_vector(3 downto 0);
        epath_almost_full : out std_logic_vector(3 downto 0);
        s_axis            : in axis_8_array_type(0 to 3);       --FIFO write port (axi stream)
        s_axis_tready     : out axis_tready_array_type(0 to 3); --FIFO write tready (axi stream)
        s_axis_aclk       : in std_logic;                       --FIFO write clock (axi stream)
        EnableHDLCDelay   : in std_logic;
        TTCOption         : in std_logic_vector(3 downto 0);
        TTCin             : in TTC_data_type;    --IG: bit #10 is the NSW extended Test Pulse
        HGTD_Fast_CMD     : in std_logic_vector(7 downto 0); --FastCMD for HGTD Altiroc ASIC, to be distributed as a TTC option.
        enAZ_in     : in std_logic; --enable AutoZeroing module for SyncFE

        CalTrigSeq_in       : in std_logic_vector(15 downto 0);
        ReadAddrCalTrigSeq_out : out array_5b(0 to 3);
        RD53B_loopgen_reg   : in RD53_loopgen_type;

        --debug info/from to regmap
        ref_dly_genCalTrig_in  : in std_logic_vector(7 downto 0);
        --(integral over epaths)
        ref_cmd_in            : in std_logic_vector(15 downto 0);    --
        cnt_cmd_out            : out std_logic_vector(31 downto 0);    --
        cnt_trig_cmd_out       : out std_logic_vector(31 downto 0);
        err_genCalTrig_dly_out : out std_logic_vector(7 downto 0);
        cnt_time_firstTolastTrig_out : out std_logic_vector(31 downto 0)


    );
end EncodingEgroupLPGBT;

architecture rtl of EncodingEgroupLPGBT is

    type IntArray is array (0 to 3) of integer; --use 3 as the maximum between max_num Epath in GBT and LpGBT

    constant ElinkWidths  : IntArray := (8, 2, 4, 2);

    type slv8_array is array (natural range <>) of std_logic_vector(7 downto 0);
    signal EGroupData_i : slv8_array(0 to 3);

    signal cnt_cmd_i      : array_32b(0 to 3);
    signal cnt_trig_cmd_i : array_32b(0 to 3);
    signal err_genCalTrig_dly_i : array_8b(0 to 3);
    signal cnt_time_firstTolastTrig_i : array_32b(0 to 3);

    function INCLUDE_RD53_epath (INCLUDE_RD53:std_logic; epath:integer)
        return std_logic is
    begin
        if epath = 0 or epath = 2 then
            return INCLUDE_RD53;
        else
            return '0';
        end if;
    end function;
begin

    --! If timing is an issue here, we can make this a clocked process.
    EpathDist: process(ElinkWidth, EGroupData_i)
    begin
        if ElinkWidth = "00" then --2b E-links
            EGroupData <= EGroupData_i(3)(1 downto 0) &
                          EGroupData_i(2)(1 downto 0) &
                          EGroupData_i(1)(1 downto 0) &
                          EGroupData_i(0)(1 downto 0);
        elsif ElinkWidth = "01" then --4b E-links
            EGroupData <= EGroupData_i(2)(3 downto 0) &
                          EGroupData_i(0)(3 downto 0);
        elsif ElinkWidth = "10" then --8b E-links
            EGroupData <= EGroupData_i(0)(7 downto 0);
        else                         --"11" is an invalid option
            EGroupData <= (others => '0');
        end if;
    end process;


    g_Epaths: for i in 0 to 3 generate
        Epath0: entity work.EncodingEpathLPGBT
            generic map(
                MAX_OUTPUT => ElinkWidths(i),
                INCLUDE_8b => INCLUDE_8b,
                INCLUDE_4b => INCLUDE_4b,
                INCLUDE_2b => INCLUDE_2b,
                INCLUDE_8b10b => INCLUDE_8b10b,
                INCLUDE_HDLC => INCLUDE_HDLC,
                INCLUDE_DIRECT => INCLUDE_DIRECT,
                INCLUDE_TTC => INCLUDE_TTC,
                INCLUDE_RD53 => INCLUDE_RD53_epath(INCLUDE_RD53,i),
                DEBUGGING_RD53 => DEBUGGING_RD53,
                RD53Version  => RD53Version,
                --BLOCKSIZE => BLOCKSIZE,
                GENERATE_FEI4B => GENERATE_FEI4B,
                GENERATE_LCB_ENC => GENERATE_LCB_ENC,
                HDLC_IDLE_STATE => x"FF",
                USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
                SUPPORT_HDLC_DELAY => SUPPORT_HDLC_DELAY,
                DISTR_RAM => DISTR_RAM,
                INCLUDE_XOFF => INCLUDE_XOFF

            )
            port map(
                clk40             => clk40,
                daq_reset         => daq_reset,
                daq_fifo_flush    => daq_fifo_flush,
                EpathEnable       => EpathEnable(i),
                EpathEncoding     => EpathEncoding(4*i+3 downto 4*i),
                ElinkWidth        => ElinkWidth,
                MsbFirst          => MsbFirst,
                ReverseOutputBits => ReverseOutputBits(i),
                ElinkData         => EGroupData_i(i)(ElinkWidths(i)-1 downto 0),
                toHostXoff        => toHostXoff(i),
                epath_almost_full => epath_almost_full(i),
                s_axis            => s_axis(i),
                s_axis_tready     => s_axis_tready(i),
                s_axis_aclk       => s_axis_aclk,
                EnableHDLCDelay   => EnableHDLCDelay,
                TTCOption         => TTCOption,
                TTCin             => TTCin,
                HGTD_Fast_CMD     => HGTD_Fast_CMD,
                enAZ_in                      => enAZ_in                     ,
                CalTrigSeq_in =>   CalTrigSeq_in,
                ReadAddrCalTrigSeq_out => ReadAddrCalTrigSeq_out(i),
                RD53B_loopgen_reg   => RD53B_loopgen_reg,
                --YARR debug info
                ref_dly_genCalTrig_in        => ref_dly_genCalTrig_in       ,
                ref_cmd_in                   => ref_cmd_in,
                cnt_cmd_out                  => cnt_cmd_i(i)            ,
                cnt_trig_cmd_out             => cnt_trig_cmd_i(i)            ,
                err_genCalTrig_dly_out       => err_genCalTrig_dly_i(i)      ,
                cnt_time_firstTolastTrig_out => cnt_time_firstTolastTrig_i(i)

            );
    end generate;
    --sum over epaths
    cnt_cmd_out                  <= std_logic_vector(unsigned(cnt_cmd_i(0)) + unsigned(cnt_cmd_i(1)) + unsigned(cnt_cmd_i(2)) + unsigned(cnt_cmd_i(3)));
    cnt_trig_cmd_out             <= std_logic_vector(unsigned(cnt_trig_cmd_i(0)) + unsigned(cnt_trig_cmd_i(1)) + unsigned(cnt_trig_cmd_i(2)) + unsigned(cnt_trig_cmd_i(3)));
    err_genCalTrig_dly_out       <= std_logic_vector(unsigned(err_genCalTrig_dly_i(0)) + unsigned(err_genCalTrig_dly_i(1)) + unsigned(err_genCalTrig_dly_i(2)) + unsigned(err_genCalTrig_dly_i(3)));
    cnt_time_firstTolastTrig_out <= std_logic_vector(unsigned(cnt_time_firstTolastTrig_i(0)) + unsigned(cnt_time_firstTolastTrig_i(1)) + unsigned(cnt_time_firstTolastTrig_i(2)) + unsigned(cnt_time_firstTolastTrig_i(3)));


end rtl;

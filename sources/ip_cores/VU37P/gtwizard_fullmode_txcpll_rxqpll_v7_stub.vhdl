-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
-- Date        : Mon Sep 30 16:23:29 2019
-- Host        : tarfa running 64-bit CentOS Linux release 7.6.1810 (Core)
-- Command     : write_vhdl -force -mode synth_stub
--               /localstore/et/franss/felix/firmware_vup/Projects/FLX709_FULLMODE/FLX709_FULLMODE.srcs/sources_1/ip/gtwizard_fullmode_txcpll_rxqpll_v7/gtwizard_fullmode_txcpll_rxqpll_v7_stub.vhdl
-- Design      : gtwizard_fullmode_txcpll_rxqpll_v7
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7vx690tffg1761-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity gtwizard_fullmode_txcpll_rxqpll_v7 is
  Port ( 
    SYSCLK_IN : in STD_LOGIC;                                                         --@suppress
    SOFT_RESET_TX_IN : in STD_LOGIC;                                                  --@suppress
    SOFT_RESET_RX_IN : in STD_LOGIC;                                                  --@suppress
    DONT_RESET_ON_DATA_ERROR_IN : in STD_LOGIC;                                       --@suppress
    GT0_TX_FSM_RESET_DONE_OUT : out STD_LOGIC;                                        --@suppress
    GT0_RX_FSM_RESET_DONE_OUT : out STD_LOGIC;                                        --@suppress
    GT0_DATA_VALID_IN : in STD_LOGIC;                                                 --@suppress
    GT1_TX_FSM_RESET_DONE_OUT : out STD_LOGIC;                                        --@suppress
    GT1_RX_FSM_RESET_DONE_OUT : out STD_LOGIC;                                        --@suppress
    GT1_DATA_VALID_IN : in STD_LOGIC;                                                 --@suppress
    GT2_TX_FSM_RESET_DONE_OUT : out STD_LOGIC;                                        --@suppress
    GT2_RX_FSM_RESET_DONE_OUT : out STD_LOGIC;                                        --@suppress
    GT2_DATA_VALID_IN : in STD_LOGIC;                                                 --@suppress
    GT3_TX_FSM_RESET_DONE_OUT : out STD_LOGIC;                                        --@suppress
    GT3_RX_FSM_RESET_DONE_OUT : out STD_LOGIC;                                        --@suppress
    GT3_DATA_VALID_IN : in STD_LOGIC;                                                 --@suppress
    gt0_cpllfbclklost_out : out STD_LOGIC;                                            --@suppress
    gt0_cplllock_out : out STD_LOGIC;                                                 --@suppress
    gt0_cplllockdetclk_in : in STD_LOGIC;                                             --@suppress
    gt0_cpllreset_in : in STD_LOGIC;                                                  --@suppress
    gt0_gtrefclk0_in : in STD_LOGIC;                                                  --@suppress
    gt0_gtrefclk1_in : in STD_LOGIC;                                                  --@suppress
    gt0_drpaddr_in : in STD_LOGIC_VECTOR ( 8 downto 0 );                              --@suppress
    gt0_drpclk_in : in STD_LOGIC;                                                     --@suppress
    gt0_drpdi_in : in STD_LOGIC_VECTOR ( 15 downto 0 );                               --@suppress
    gt0_drpdo_out : out STD_LOGIC_VECTOR ( 15 downto 0 );                             --@suppress
    gt0_drpen_in : in STD_LOGIC;                                                      --@suppress
    gt0_drprdy_out : out STD_LOGIC;                                                   --@suppress
    gt0_drpwe_in : in STD_LOGIC;                                                      --@suppress
    gt0_eyescanreset_in : in STD_LOGIC;                                               --@suppress
    gt0_rxuserrdy_in : in STD_LOGIC;                                                  --@suppress
    gt0_eyescandataerror_out : out STD_LOGIC;                                         --@suppress
    gt0_eyescantrigger_in : in STD_LOGIC;                                             --@suppress
    gt0_dmonitorout_out : out STD_LOGIC_VECTOR ( 14 downto 0 );                       --@suppress
    gt0_rxusrclk_in : in STD_LOGIC;                                                   --@suppress
    gt0_rxusrclk2_in : in STD_LOGIC;                                                  --@suppress
    gt0_rxdata_out : out STD_LOGIC_VECTOR ( 31 downto 0 );                            --@suppress
    gt0_rxdisperr_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                          --@suppress
    gt0_rxnotintable_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                       --@suppress
    gt0_gthrxn_in : in STD_LOGIC;                                                     --@suppress
    gt0_rxphmonitor_out : out STD_LOGIC_VECTOR ( 4 downto 0 );                        --@suppress
    gt0_rxphslipmonitor_out : out STD_LOGIC_VECTOR ( 4 downto 0 );                    --@suppress
    gt0_rxbyteisaligned_out : out STD_LOGIC;                                          --@suppress
    gt0_rxmcommaalignen_in : in STD_LOGIC;                                            --@suppress
    gt0_rxpcommaalignen_in : in STD_LOGIC;                                            --@suppress
    gt0_rxmonitorout_out : out STD_LOGIC_VECTOR ( 6 downto 0 );                       --@suppress
    gt0_rxmonitorsel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );                         --@suppress
    gt0_rxoutclk_out : out STD_LOGIC;                                                 --@suppress
    gt0_rxoutclkfabric_out : out STD_LOGIC;                                           --@suppress
    gt0_gtrxreset_in : in STD_LOGIC;                                                  --@suppress
    gt0_rxcharisk_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                          --@suppress
    gt0_gthrxp_in : in STD_LOGIC;                                                     --@suppress
    gt0_rxresetdone_out : out STD_LOGIC;                                              --@suppress
    gt0_gttxreset_in : in STD_LOGIC;                                                  --@suppress
    gt0_txuserrdy_in : in STD_LOGIC;                                                  --@suppress
    gt0_txusrclk_in : in STD_LOGIC;                                                   --@suppress
    gt0_txusrclk2_in : in STD_LOGIC;                                                  --@suppress
    gt0_txdata_in : in STD_LOGIC_VECTOR ( 19 downto 0 );                              --@suppress
    gt0_gthtxn_out : out STD_LOGIC;                                                   --@suppress
    gt0_gthtxp_out : out STD_LOGIC;                                                   --@suppress
    gt0_txoutclk_out : out STD_LOGIC;                                                 --@suppress
    gt0_txoutclkfabric_out : out STD_LOGIC;                                           --@suppress
    gt0_txoutclkpcs_out : out STD_LOGIC;                                              --@suppress
    gt0_txresetdone_out : out STD_LOGIC;                                              --@suppress
    gt1_cpllfbclklost_out : out STD_LOGIC;                                            --@suppress
    gt1_cplllock_out : out STD_LOGIC;                                                 --@suppress
    gt1_cplllockdetclk_in : in STD_LOGIC;                                             --@suppress
    gt1_cpllreset_in : in STD_LOGIC;                                                  --@suppress
    gt1_gtrefclk0_in : in STD_LOGIC;                                                  --@suppress
    gt1_gtrefclk1_in : in STD_LOGIC;                                                  --@suppress
    gt1_drpaddr_in : in STD_LOGIC_VECTOR ( 8 downto 0 );                              --@suppress
    gt1_drpclk_in : in STD_LOGIC;                                                     --@suppress
    gt1_drpdi_in : in STD_LOGIC_VECTOR ( 15 downto 0 );                               --@suppress
    gt1_drpdo_out : out STD_LOGIC_VECTOR ( 15 downto 0 );                             --@suppress
    gt1_drpen_in : in STD_LOGIC;                                                      --@suppress
    gt1_drprdy_out : out STD_LOGIC;                                                   --@suppress
    gt1_drpwe_in : in STD_LOGIC;                                                      --@suppress
    gt1_eyescanreset_in : in STD_LOGIC;                                               --@suppress
    gt1_rxuserrdy_in : in STD_LOGIC;                                                  --@suppress
    gt1_eyescandataerror_out : out STD_LOGIC;                                         --@suppress
    gt1_eyescantrigger_in : in STD_LOGIC;                                             --@suppress
    gt1_dmonitorout_out : out STD_LOGIC_VECTOR ( 14 downto 0 );                       --@suppress
    gt1_rxusrclk_in : in STD_LOGIC;                                                   --@suppress
    gt1_rxusrclk2_in : in STD_LOGIC;                                                  --@suppress
    gt1_rxdata_out : out STD_LOGIC_VECTOR ( 31 downto 0 );                            --@suppress
    gt1_rxdisperr_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                          --@suppress
    gt1_rxnotintable_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                       --@suppress
    gt1_gthrxn_in : in STD_LOGIC;                                                     --@suppress
    gt1_rxphmonitor_out : out STD_LOGIC_VECTOR ( 4 downto 0 );                        --@suppress
    gt1_rxphslipmonitor_out : out STD_LOGIC_VECTOR ( 4 downto 0 );                    --@suppress
    gt1_rxbyteisaligned_out : out STD_LOGIC;                                          --@suppress
    gt1_rxmcommaalignen_in : in STD_LOGIC;                                            --@suppress
    gt1_rxpcommaalignen_in : in STD_LOGIC;                                            --@suppress
    gt1_rxmonitorout_out : out STD_LOGIC_VECTOR ( 6 downto 0 );                       --@suppress
    gt1_rxmonitorsel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );                         --@suppress
    gt1_rxoutclk_out : out STD_LOGIC;                                                 --@suppress
    gt1_rxoutclkfabric_out : out STD_LOGIC;                                           --@suppress
    gt1_gtrxreset_in : in STD_LOGIC;                                                  --@suppress
    gt1_rxcharisk_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                          --@suppress
    gt1_gthrxp_in : in STD_LOGIC;                                                     --@suppress
    gt1_rxresetdone_out : out STD_LOGIC;                                              --@suppress
    gt1_gttxreset_in : in STD_LOGIC;                                                  --@suppress
    gt1_txuserrdy_in : in STD_LOGIC;                                                  --@suppress
    gt1_txusrclk_in : in STD_LOGIC;                                                   --@suppress
    gt1_txusrclk2_in : in STD_LOGIC;                                                  --@suppress
    gt1_txdata_in : in STD_LOGIC_VECTOR ( 19 downto 0 );                              --@suppress
    gt1_gthtxn_out : out STD_LOGIC;                                                   --@suppress
    gt1_gthtxp_out : out STD_LOGIC;                                                   --@suppress
    gt1_txoutclk_out : out STD_LOGIC;                                                 --@suppress
    gt1_txoutclkfabric_out : out STD_LOGIC;                                           --@suppress
    gt1_txoutclkpcs_out : out STD_LOGIC;                                              --@suppress
    gt1_txresetdone_out : out STD_LOGIC;                                              --@suppress
    gt2_cpllfbclklost_out : out STD_LOGIC;                                            --@suppress
    gt2_cplllock_out : out STD_LOGIC;                                                 --@suppress
    gt2_cplllockdetclk_in : in STD_LOGIC;                                             --@suppress
    gt2_cpllreset_in : in STD_LOGIC;                                                  --@suppress
    gt2_gtrefclk0_in : in STD_LOGIC;                                                  --@suppress
    gt2_gtrefclk1_in : in STD_LOGIC;                                                  --@suppress
    gt2_drpaddr_in : in STD_LOGIC_VECTOR ( 8 downto 0 );                              --@suppress
    gt2_drpclk_in : in STD_LOGIC;                                                     --@suppress
    gt2_drpdi_in : in STD_LOGIC_VECTOR ( 15 downto 0 );                               --@suppress
    gt2_drpdo_out : out STD_LOGIC_VECTOR ( 15 downto 0 );                             --@suppress
    gt2_drpen_in : in STD_LOGIC;                                                      --@suppress
    gt2_drprdy_out : out STD_LOGIC;                                                   --@suppress
    gt2_drpwe_in : in STD_LOGIC;                                                      --@suppress
    gt2_eyescanreset_in : in STD_LOGIC;                                               --@suppress
    gt2_rxuserrdy_in : in STD_LOGIC;                                                  --@suppress
    gt2_eyescandataerror_out : out STD_LOGIC;                                         --@suppress
    gt2_eyescantrigger_in : in STD_LOGIC;                                             --@suppress
    gt2_dmonitorout_out : out STD_LOGIC_VECTOR ( 14 downto 0 );                       --@suppress
    gt2_rxusrclk_in : in STD_LOGIC;                                                   --@suppress
    gt2_rxusrclk2_in : in STD_LOGIC;                                                  --@suppress
    gt2_rxdata_out : out STD_LOGIC_VECTOR ( 31 downto 0 );                            --@suppress
    gt2_rxdisperr_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                          --@suppress
    gt2_rxnotintable_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                       --@suppress
    gt2_gthrxn_in : in STD_LOGIC;                                                     --@suppress
    gt2_rxphmonitor_out : out STD_LOGIC_VECTOR ( 4 downto 0 );                        --@suppress
    gt2_rxphslipmonitor_out : out STD_LOGIC_VECTOR ( 4 downto 0 );                    --@suppress
    gt2_rxbyteisaligned_out : out STD_LOGIC;                                          --@suppress
    gt2_rxmcommaalignen_in : in STD_LOGIC;                                            --@suppress
    gt2_rxpcommaalignen_in : in STD_LOGIC;                                            --@suppress
    gt2_rxmonitorout_out : out STD_LOGIC_VECTOR ( 6 downto 0 );                       --@suppress
    gt2_rxmonitorsel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );                         --@suppress
    gt2_rxoutclk_out : out STD_LOGIC;                                                 --@suppress
    gt2_rxoutclkfabric_out : out STD_LOGIC;                                           --@suppress
    gt2_gtrxreset_in : in STD_LOGIC;                                                  --@suppress
    gt2_rxcharisk_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                          --@suppress
    gt2_gthrxp_in : in STD_LOGIC;                                                     --@suppress
    gt2_rxresetdone_out : out STD_LOGIC;                                              --@suppress
    gt2_gttxreset_in : in STD_LOGIC;                                                  --@suppress
    gt2_txuserrdy_in : in STD_LOGIC;                                                  --@suppress
    gt2_txusrclk_in : in STD_LOGIC;                                                   --@suppress
    gt2_txusrclk2_in : in STD_LOGIC;                                                  --@suppress
    gt2_txdata_in : in STD_LOGIC_VECTOR ( 19 downto 0 );                              --@suppress
    gt2_gthtxn_out : out STD_LOGIC;                                                   --@suppress
    gt2_gthtxp_out : out STD_LOGIC;                                                   --@suppress
    gt2_txoutclk_out : out STD_LOGIC;                                                 --@suppress
    gt2_txoutclkfabric_out : out STD_LOGIC;                                           --@suppress
    gt2_txoutclkpcs_out : out STD_LOGIC;                                              --@suppress
    gt2_txresetdone_out : out STD_LOGIC;                                              --@suppress
    gt3_cpllfbclklost_out : out STD_LOGIC;                                            --@suppress
    gt3_cplllock_out : out STD_LOGIC;                                                 --@suppress
    gt3_cplllockdetclk_in : in STD_LOGIC;                                             --@suppress
    gt3_cpllreset_in : in STD_LOGIC;                                                  --@suppress
    gt3_gtrefclk0_in : in STD_LOGIC;                                                  --@suppress
    gt3_gtrefclk1_in : in STD_LOGIC;                                                  --@suppress
    gt3_drpaddr_in : in STD_LOGIC_VECTOR ( 8 downto 0 );                              --@suppress
    gt3_drpclk_in : in STD_LOGIC;                                                     --@suppress
    gt3_drpdi_in : in STD_LOGIC_VECTOR ( 15 downto 0 );                               --@suppress
    gt3_drpdo_out : out STD_LOGIC_VECTOR ( 15 downto 0 );                             --@suppress
    gt3_drpen_in : in STD_LOGIC;                                                      --@suppress
    gt3_drprdy_out : out STD_LOGIC;                                                   --@suppress
    gt3_drpwe_in : in STD_LOGIC;                                                      --@suppress
    gt3_eyescanreset_in : in STD_LOGIC;                                               --@suppress
    gt3_rxuserrdy_in : in STD_LOGIC;                                                  --@suppress
    gt3_eyescandataerror_out : out STD_LOGIC;                                         --@suppress
    gt3_eyescantrigger_in : in STD_LOGIC;                                             --@suppress
    gt3_dmonitorout_out : out STD_LOGIC_VECTOR ( 14 downto 0 );                       --@suppress
    gt3_rxusrclk_in : in STD_LOGIC;                                                   --@suppress
    gt3_rxusrclk2_in : in STD_LOGIC;                                                  --@suppress
    gt3_rxdata_out : out STD_LOGIC_VECTOR ( 31 downto 0 );                            --@suppress
    gt3_rxdisperr_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                          --@suppress
    gt3_rxnotintable_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                       --@suppress
    gt3_gthrxn_in : in STD_LOGIC;                                                     --@suppress
    gt3_rxphmonitor_out : out STD_LOGIC_VECTOR ( 4 downto 0 );                        --@suppress
    gt3_rxphslipmonitor_out : out STD_LOGIC_VECTOR ( 4 downto 0 );                    --@suppress
    gt3_rxbyteisaligned_out : out STD_LOGIC;                                          --@suppress
    gt3_rxmcommaalignen_in : in STD_LOGIC;                                            --@suppress
    gt3_rxpcommaalignen_in : in STD_LOGIC;                                            --@suppress
    gt3_rxmonitorout_out : out STD_LOGIC_VECTOR ( 6 downto 0 );                       --@suppress
    gt3_rxmonitorsel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );                         --@suppress
    gt3_rxoutclk_out : out STD_LOGIC;                                                 --@suppress
    gt3_rxoutclkfabric_out : out STD_LOGIC;                                           --@suppress
    gt3_gtrxreset_in : in STD_LOGIC;                                                  --@suppress
    gt3_rxcharisk_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                          --@suppress
    gt3_gthrxp_in : in STD_LOGIC;                                                     --@suppress
    gt3_rxresetdone_out : out STD_LOGIC;                                              --@suppress
    gt3_gttxreset_in : in STD_LOGIC;                                                  --@suppress
    gt3_txuserrdy_in : in STD_LOGIC;                                                  --@suppress
    gt3_txusrclk_in : in STD_LOGIC;                                                   --@suppress
    gt3_txusrclk2_in : in STD_LOGIC;                                                  --@suppress
    gt3_txdata_in : in STD_LOGIC_VECTOR ( 19 downto 0 );                              --@suppress
    gt3_gthtxn_out : out STD_LOGIC;                                                   --@suppress
    gt3_gthtxp_out : out STD_LOGIC;                                                   --@suppress
    gt3_txoutclk_out : out STD_LOGIC;                                                 --@suppress
    gt3_txoutclkfabric_out : out STD_LOGIC;                                           --@suppress
    gt3_txoutclkpcs_out : out STD_LOGIC;                                              --@suppress
    gt3_txresetdone_out : out STD_LOGIC;                                              --@suppress
    GT0_QPLLLOCK_IN : in STD_LOGIC;                                                   --@suppress
    GT0_QPLLREFCLKLOST_IN : in STD_LOGIC;                                             --@suppress
    GT0_QPLLRESET_OUT : out STD_LOGIC;                                                --@suppress
    GT0_QPLLOUTCLK_IN : in STD_LOGIC;                                                 --@suppress
    GT0_QPLLOUTREFCLK_IN : in STD_LOGIC                                               --@suppress
  );

end gtwizard_fullmode_txcpll_rxqpll_v7;

architecture stub of gtwizard_fullmode_txcpll_rxqpll_v7 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "SYSCLK_IN,SOFT_RESET_TX_IN,SOFT_RESET_RX_IN,DONT_RESET_ON_DATA_ERROR_IN,GT0_TX_FSM_RESET_DONE_OUT,GT0_RX_FSM_RESET_DONE_OUT,GT0_DATA_VALID_IN,GT1_TX_FSM_RESET_DONE_OUT,GT1_RX_FSM_RESET_DONE_OUT,GT1_DATA_VALID_IN,GT2_TX_FSM_RESET_DONE_OUT,GT2_RX_FSM_RESET_DONE_OUT,GT2_DATA_VALID_IN,GT3_TX_FSM_RESET_DONE_OUT,GT3_RX_FSM_RESET_DONE_OUT,GT3_DATA_VALID_IN,gt0_cpllfbclklost_out,gt0_cplllock_out,gt0_cplllockdetclk_in,gt0_cpllreset_in,gt0_gtrefclk0_in,gt0_gtrefclk1_in,gt0_drpaddr_in[8:0],gt0_drpclk_in,gt0_drpdi_in[15:0],gt0_drpdo_out[15:0],gt0_drpen_in,gt0_drprdy_out,gt0_drpwe_in,gt0_eyescanreset_in,gt0_rxuserrdy_in,gt0_eyescandataerror_out,gt0_eyescantrigger_in,gt0_dmonitorout_out[14:0],gt0_rxusrclk_in,gt0_rxusrclk2_in,gt0_rxdata_out[31:0],gt0_rxdisperr_out[3:0],gt0_rxnotintable_out[3:0],gt0_gthrxn_in,gt0_rxphmonitor_out[4:0],gt0_rxphslipmonitor_out[4:0],gt0_rxbyteisaligned_out,gt0_rxmcommaalignen_in,gt0_rxpcommaalignen_in,gt0_rxmonitorout_out[6:0],gt0_rxmonitorsel_in[1:0],gt0_rxoutclk_out,gt0_rxoutclkfabric_out,gt0_gtrxreset_in,gt0_rxcharisk_out[3:0],gt0_gthrxp_in,gt0_rxresetdone_out,gt0_gttxreset_in,gt0_txuserrdy_in,gt0_txusrclk_in,gt0_txusrclk2_in,gt0_txdata_in[19:0],gt0_gthtxn_out,gt0_gthtxp_out,gt0_txoutclk_out,gt0_txoutclkfabric_out,gt0_txoutclkpcs_out,gt0_txresetdone_out,gt1_cpllfbclklost_out,gt1_cplllock_out,gt1_cplllockdetclk_in,gt1_cpllreset_in,gt1_gtrefclk0_in,gt1_gtrefclk1_in,gt1_drpaddr_in[8:0],gt1_drpclk_in,gt1_drpdi_in[15:0],gt1_drpdo_out[15:0],gt1_drpen_in,gt1_drprdy_out,gt1_drpwe_in,gt1_eyescanreset_in,gt1_rxuserrdy_in,gt1_eyescandataerror_out,gt1_eyescantrigger_in,gt1_dmonitorout_out[14:0],gt1_rxusrclk_in,gt1_rxusrclk2_in,gt1_rxdata_out[31:0],gt1_rxdisperr_out[3:0],gt1_rxnotintable_out[3:0],gt1_gthrxn_in,gt1_rxphmonitor_out[4:0],gt1_rxphslipmonitor_out[4:0],gt1_rxbyteisaligned_out,gt1_rxmcommaalignen_in,gt1_rxpcommaalignen_in,gt1_rxmonitorout_out[6:0],gt1_rxmonitorsel_in[1:0],gt1_rxoutclk_out,gt1_rxoutclkfabric_out,gt1_gtrxreset_in,gt1_rxcharisk_out[3:0],gt1_gthrxp_in,gt1_rxresetdone_out,gt1_gttxreset_in,gt1_txuserrdy_in,gt1_txusrclk_in,gt1_txusrclk2_in,gt1_txdata_in[19:0],gt1_gthtxn_out,gt1_gthtxp_out,gt1_txoutclk_out,gt1_txoutclkfabric_out,gt1_txoutclkpcs_out,gt1_txresetdone_out,gt2_cpllfbclklost_out,gt2_cplllock_out,gt2_cplllockdetclk_in,gt2_cpllreset_in,gt2_gtrefclk0_in,gt2_gtrefclk1_in,gt2_drpaddr_in[8:0],gt2_drpclk_in,gt2_drpdi_in[15:0],gt2_drpdo_out[15:0],gt2_drpen_in,gt2_drprdy_out,gt2_drpwe_in,gt2_eyescanreset_in,gt2_rxuserrdy_in,gt2_eyescandataerror_out,gt2_eyescantrigger_in,gt2_dmonitorout_out[14:0],gt2_rxusrclk_in,gt2_rxusrclk2_in,gt2_rxdata_out[31:0],gt2_rxdisperr_out[3:0],gt2_rxnotintable_out[3:0],gt2_gthrxn_in,gt2_rxphmonitor_out[4:0],gt2_rxphslipmonitor_out[4:0],gt2_rxbyteisaligned_out,gt2_rxmcommaalignen_in,gt2_rxpcommaalignen_in,gt2_rxmonitorout_out[6:0],gt2_rxmonitorsel_in[1:0],gt2_rxoutclk_out,gt2_rxoutclkfabric_out,gt2_gtrxreset_in,gt2_rxcharisk_out[3:0],gt2_gthrxp_in,gt2_rxresetdone_out,gt2_gttxreset_in,gt2_txuserrdy_in,gt2_txusrclk_in,gt2_txusrclk2_in,gt2_txdata_in[19:0],gt2_gthtxn_out,gt2_gthtxp_out,gt2_txoutclk_out,gt2_txoutclkfabric_out,gt2_txoutclkpcs_out,gt2_txresetdone_out,gt3_cpllfbclklost_out,gt3_cplllock_out,gt3_cplllockdetclk_in,gt3_cpllreset_in,gt3_gtrefclk0_in,gt3_gtrefclk1_in,gt3_drpaddr_in[8:0],gt3_drpclk_in,gt3_drpdi_in[15:0],gt3_drpdo_out[15:0],gt3_drpen_in,gt3_drprdy_out,gt3_drpwe_in,gt3_eyescanreset_in,gt3_rxuserrdy_in,gt3_eyescandataerror_out,gt3_eyescantrigger_in,gt3_dmonitorout_out[14:0],gt3_rxusrclk_in,gt3_rxusrclk2_in,gt3_rxdata_out[31:0],gt3_rxdisperr_out[3:0],gt3_rxnotintable_out[3:0],gt3_gthrxn_in,gt3_rxphmonitor_out[4:0],gt3_rxphslipmonitor_out[4:0],gt3_rxbyteisaligned_out,gt3_rxmcommaalignen_in,gt3_rxpcommaalignen_in,gt3_rxmonitorout_out[6:0],gt3_rxmonitorsel_in[1:0],gt3_rxoutclk_out,gt3_rxoutclkfabric_out,gt3_gtrxreset_in,gt3_rxcharisk_out[3:0],gt3_gthrxp_in,gt3_rxresetdone_out,gt3_gttxreset_in,gt3_txuserrdy_in,gt3_txusrclk_in,gt3_txusrclk2_in,gt3_txdata_in[19:0],gt3_gthtxn_out,gt3_gthtxp_out,gt3_txoutclk_out,gt3_txoutclkfabric_out,gt3_txoutclkpcs_out,gt3_txresetdone_out,GT0_QPLLLOCK_IN,GT0_QPLLREFCLKLOST_IN,GT0_QPLLRESET_OUT,GT0_QPLLOUTCLK_IN,GT0_QPLLOUTREFCLK_IN";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "gtwizard_fullmode_txcpll_rxqpll_v7,gtwizard_v3_6_9,{protocol_file=Start_from_scratch}";
begin
end;

`timescale 1ns / 1ps

import axi_vip_pkg::*;
import axi_vip_0_pkg::*;

module tb_fifo(

);

localparam DATA_WIDTH = 32;
localparam ADDR_WIDTH = 32;

wire aclk, aresetn;

wire [ADDR_WIDTH - 1 : 0] axi_awaddr;
wire [2 : 0] axi_awprot;
wire axi_awvalid, axi_awready;

wire [DATA_WIDTH - 1 : 0] axi_wdata;
wire [DATA_WIDTH / 8 - 1: 0] axi_wstrb;
wire axi_wvalid, axi_wready;

wire [DATA_WIDTH - 1 : 0] axi_rdata;
wire [1 : 0] axi_rresp;
wire axi_rvalid, axi_rready;

wire [ADDR_WIDTH - 1 : 0] axi_araddr;
wire [2 : 0] axi_arprot;
wire axi_arready, axi_arvalid;

wire [1 : 0] axi_bresp;
wire axi_bready, axi_bvalid;



versal_network_device_v1_0 #(
	C_S00_AXI_DATA_WIDTH(DATA_WIDTH),
	C_S00_AXI_ADDR_WIDTH(ADDR_WIDTH)
)
axi_fifo_slave (
	.s00_axi_aclk(aclk),
	.s00_axi_aresetn(aresetn),

	.s00_axi_awaddr(axi_awaddr),
	.s00_axi_awprot(axi_awprot),
	.s00_axi_awvalid(axi_awvalid),
	.s00_axi_awready(axi_awready),

	.s00_axi_wdata(axi_wdata),
	.s00_axi_wstrb(axi_wstrb),
	.s00_axi_wvalid(axi_wvalid),
	.s00_axi_wready(axi_wready),

	.s00_axi_bresp(axi_bresp),
	.s00_axi_bvalid(axi_bvalid),
	.s00_axi_bready(axi_bready),

	.s00_axi_araddr(axi_araddr),
	.s00_axi_arprot(axi_arprot),
	.s00_axi_arvalid(axi_arvalid),
	.s00_axi_arready(axi_arready),

	.s00_axi_rdata(axi_rdata),
	.s00_axi_rresp(axi_rresp),
	.s00_axi_rvalid(axi_rvalid),
	.s00_axi_rready(axi_rready)

);

axi_vip_1 AXIlitemaster (
	.aclk(aclk),
	.aresetn(aresetn),
	.m_axi_awaddr(axi_awaddr),
	.m_axi_awprot(axi_awprot),
	.m_axi_awvalid(axi_awvalid),
	.m_axi_awready(axi_awready),
	.m_axi_wdata(axi_wdata),
	.m_axi_wstrb(axi_wstrb),
	.m_axi_wvalid(axi_wvalid),
	.m_axi_wready(axi_wready),
	.m_axi_bresp(axi_bresp),
	.m_axi_bvalid(axi_bvalid),
	.m_axi_bready(axi_bready),
	.m_axi_araddr(axi_araddr),
	.m_axi_arprot(axi_arprot),
	.m_axi_arvalid(axi_arvalid),
	.m_axi_arready(axi_arready),
	.m_axi_rdata(axi_rdata),
	.m_axi_rresp(axi_rresp),
	.m_axi_rvalid(axi_rvalid),
	.m_axi_rready(axi_rready)
);

always #5 aclk = !aclk

initial begin

	rgen = new("axi_random_backpressure_wready");
	rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
	rgen.set_low_time_range(0,12);
	rgen.set_high_time_range(1,12);
	rgen.set_event_count_range(3,5);

	master_agent = new("Master VIP agent", AXIlitemaster.inst.IF);
	master_agent.set_verbosity(40);
	master_agent.wr_driver.set_wready_gen(rgen);
	master_agent.start_master();

	aresetn = 0;
	#400ns
	aresetn = 1;

	// TODO: send data

end

endmodule

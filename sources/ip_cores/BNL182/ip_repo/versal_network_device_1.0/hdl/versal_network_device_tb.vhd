library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

library uvvm_vvc_framework;
    use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

library bitvis_vip_axilite;
    context bitvis_vip_axilite.vvc_context;

library bitvis_vip_clock_generator;
    context bitvis_vip_clock_generator.vvc_context;

entity versal_network_device_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity versal_network_device_tb;

architecture sim of versal_network_device_tb is

    signal clk : std_logic;

    constant C_CLK_PERIOD   : time := 10 ns;
    constant C_CYCLES_RST   : integer := 100;
    constant C_DATA_WIDTH   : integer := 32;
    constant C_ADDR_WIDTH   : integer := 32;

    signal rst : std_logic := '0';
    signal arst : std_logic;

    signal s00_axi_awaddr  : std_logic_vector(C_ADDR_WIDTH-1 downto 0);
    signal s00_axi_awprot  : std_logic_vector(2 downto 0);
    signal s00_axi_awvalid : std_logic;
    signal s00_axi_awready : std_logic;
    signal s00_axi_wdata   : std_logic_vector(C_DATA_WIDTH-1 downto 0);
    signal s00_axi_wstrb   : std_logic_vector((C_DATA_WIDTH/8)-1 downto 0);
    signal s00_axi_wvalid  : std_logic;
    signal s00_axi_wready  : std_logic;
    signal s00_axi_bresp   : std_logic_vector(1 downto 0);
    signal s00_axi_bvalid  : std_logic;
    signal s00_axi_bready  : std_logic;
    signal s00_axi_araddr  : std_logic_vector(C_ADDR_WIDTH-1 downto 0);
    signal s00_axi_arprot  : std_logic_vector(2 downto 0);
    signal s00_axi_arvalid : std_logic;
    signal s00_axi_arready : std_logic;
    signal s00_axi_rdata   : std_logic_vector(C_DATA_WIDTH-1 downto 0);
    signal s00_axi_rresp   : std_logic_vector(1 downto 0);
    signal s00_axi_rvalid  : std_logic;
    signal s00_axi_rready  : std_logic;


    subtype t_AXI32 is t_axilite_if (
		write_address_channel(
			awaddr(C_ADDR_WIDTH-1 downto 0)
		),
		write_data_channel(
			wdata(C_DATA_WIDTH-1 downto 0),
			wstrb(C_DATA_WIDTH/8 - 1 downto 0)
		),
		read_address_channel(
			araddr(C_DATA_WIDTH-1 downto 0)
		),
		read_data_channel(
			rdata(C_DATA_WIDTH-1 downto 0)
		)
	);

    signal axi_if : t_AXI32;
    signal fromHost_wr_en_i : std_logic:= '0';
    signal fromHost_full_o : std_logic;
    signal fromHost_prog_full_o : std_logic;
    signal fromHost_din_i : std_logic_vector(C_DATA_WIDTH-1 downto 0) := (others => '0');
    signal fromHost_eop_i : std_logic := '0';
    signal toHost_rd_en_i : std_logic:= '0';
    signal toHost_empty_o : std_logic;
    signal toHost_prog_empty_o : std_logic;
    signal tohost_dout_o : std_logic_vector(C_DATA_WIDTH-1 downto 0);
    signal toHost_eop_o : std_logic;




begin

    arst <= not rst;

    i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

    i_clock_generator_vvc : entity bitvis_vip_clock_generator.clock_generator_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_INSTANCE_IDX    => 1,
            GC_CLOCK_NAME      => "Clock",
            GC_CLOCK_PERIOD    => C_CLK_PERIOD,
            GC_CLOCK_HIGH_TIME => C_CLK_PERIOD / 2
        )
        port map(
            clk => clk
        );

    i_axilite_vvc_master : entity bitvis_vip_axilite.axilite_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_AXISTREAM_BFM_CONFIG, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_ADDR_WIDTH   => C_ADDR_WIDTH,
            GC_DATA_WIDTH    => C_DATA_WIDTH,
            GC_INSTANCE_IDX  => 2)
        port map(
            clk              => clk,
            axilite_vvc_master_if => axi_if);

    DUT : entity work.VERSAL_virtual_network_v1_0
        generic map (
            C_S00_AXI_DATA_WIDTH => C_DATA_WIDTH,
            C_S00_AXI_ADDR_WIDTH => C_ADDR_WIDTH
        )
        port map (
            host_clk         => clk,
            fromHost_wr_en_i => fromHost_wr_en_i,
            fromHost_full_o  => fromHost_full_o,
            fromHost_prog_full_o => fromHost_prog_full_o,
            fromHost_din_i => fromHost_din_i,
            fromHost_eop_i => fromHost_eop_i,
            toHost_rd_en_i => toHost_rd_en_i,
            toHost_empty_o => toHost_empty_o,
            toHost_prog_empty_o => toHost_prog_empty_o,
            tohost_dout_o => tohost_dout_o,
            toHost_eop_o => toHost_eop_o,
            toHost_status_carrier_o => open,
            fromHost_set_carrier_i => '0',
            s00_axi_aclk    => clk,
            s00_axi_aresetn => arst,
            s00_axi_awaddr  => s00_axi_awaddr,
            s00_axi_awprot  => s00_axi_awprot,
            s00_axi_awvalid => s00_axi_awvalid,
            s00_axi_awready => s00_axi_awready,
            s00_axi_wdata   => s00_axi_wdata,
            s00_axi_wstrb   => s00_axi_wstrb,
            s00_axi_wvalid  => s00_axi_wvalid,
            s00_axi_wready  => s00_axi_wready,
            s00_axi_bresp   => s00_axi_bresp,
            s00_axi_bvalid  => s00_axi_bvalid,
            s00_axi_bready  => s00_axi_bready,
            s00_axi_araddr  => s00_axi_araddr,
            s00_axi_arprot  => s00_axi_arprot,
            s00_axi_arvalid => s00_axi_arvalid,
            s00_axi_arready => s00_axi_arready,
            s00_axi_rdata   => s00_axi_rdata,
            s00_axi_rresp   => s00_axi_rresp,
            s00_axi_rvalid  => s00_axi_rvalid,
            s00_axi_rready  => s00_axi_rready
        );

    s00_axi_awaddr <= axi_if.write_address_channel.awaddr;
    s00_axi_awprot <= axi_if.write_address_channel.awprot;
    s00_axi_awvalid <= axi_if.write_address_channel.awvalid;
    axi_if.write_address_channel.awready <= s00_axi_awready;

    s00_axi_wdata <= axi_if.write_data_channel.wdata;
    s00_axi_wstrb <= axi_if.write_data_channel.wstrb;
    s00_axi_wvalid <= axi_if.write_data_channel.wvalid;
    axi_if.write_data_channel.wready <= s00_axi_wready;

    s00_axi_bready <= axi_if.write_response_channel.bready;
    axi_if.write_response_channel.bresp  <= s00_axi_bresp;
    axi_if.write_response_channel.bvalid <= s00_axi_bvalid;

    s00_axi_araddr <= axi_if.read_address_channel.araddr;
    s00_axi_arvalid <= axi_if.read_address_channel.arvalid;
    s00_axi_arprot <= axi_if.read_address_channel.arprot;
    axi_if.read_address_channel.arready <= s00_axi_arready;

    s00_axi_rready <= axi_if.read_data_channel.rready;
    axi_if.read_data_channel.rdata <= s00_axi_rdata;
    axi_if.read_data_channel.rresp <= s00_axi_rresp;
    axi_if.read_data_channel.rvalid <= s00_axi_rvalid;



    proc_test: process is

        procedure fromHost_fifo_write(data : std_logic_vector(31 downto 0); eop : std_logic; message : string) is

        begin
            log(ID_LOG_HDR, "fromHost_fifo_write: " &message);
            fromHost_din_i <= data;
            fromHost_eop_i <= eop;
            fromHost_wr_en_i <= '1';
            wait until rising_edge(clk);
            fromHost_wr_en_i <= '0';
            wait until rising_edge(clk);

        end procedure fromHost_fifo_write;

        --Returns the data from the FIFO plus end-of-packet as bit 32
        procedure toHost_fifo_read(data: out std_logic_vector(31 downto 0); eop: out std_logic; message : string) is
        begin
            log(ID_LOG_HDR, "toHost_fifo_read: " &message);
            if toHost_empty_o = '1' then
                data := x"ffff_ffff";
                eop := '1';
            else

                data := tohost_dout_o;
                eop := toHost_eop_o;
                toHost_rd_en_i <= '1';
                wait until rising_edge(clk);
                toHost_rd_en_i <= '0';
                wait until rising_edge(clk);

            end if;
        end procedure toHost_fifo_read;

        variable data: std_logic_vector(31 downto 0);
        variable eop: std_logic;
    begin
        await_uvvm_initialization(VOID);
        start_clock(CLOCK_GENERATOR_VVCT, 1, "Start clock generator");--@suppress

        -- Print the configuration to the log
        --report_global_ctrl(VOID);
        --report_msg_id_panel(VOID);

        disable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        enable_log_msg(ID_SEQUENCER);
        --enable_log_msg(ID_UVVM_SEND_CMD);

        disable_log_msg(CLOCK_GENERATOR_VVCT, 1, ALL_MESSAGES); --@suppress
        enable_log_msg(CLOCK_GENERATOR_VVCT, 1, ID_BFM);--@suppress
        disable_log_msg(AXILITE_VVCT, 2, ALL_MESSAGES);--@suppress
        --enable_log_msg(AXILITE_VVCT, 2, ID_BFM);


        gen_pulse(rst, C_CYCLES_RST * C_CLK_PERIOD, "Reset DUT");

        wait_num_rising_edge(clk, 100);
        log(ID_LOG_HDR, "Verify register values in the initial state");
        axilite_check(AXILITE_VVCT, 2, x"18", x"CECABABA", "Check MAGIC"); --@suppress
        axilite_check(AXILITE_VVCT, 2, x"C", x"FFFFFFFF", "Read data from TARGET_ADDR_SEND"); --@suppress
        axilite_check(AXILITE_VVCT, 2, x"10", x"FFFFFFFF", "Read data from TARGET_ADDR_RECV"); --@suppress
        axilite_check(AXILITE_VVCT, 2, x"14", x"0C", "Read data from TARGET_ADDR_STATUS"); --@suppress
        await_completion(AXILITE_VVCT, 2, C_CLK_PERIOD * 100, "Wait for the read transactions to finish"); --@suppress

        log(ID_LOG_HDR, "Send data from host to target");
        fromHost_fifo_write(x"DE2711BB", '0', "Send data from host");
        fromHost_fifo_write(x"AD45A78C", '0', "Send data from host");
        fromHost_fifo_write(x"BE1FF25B", '0', "Send data from host");
        fromHost_fifo_write(x"EF77900D", '1', "Send data from host");
        wait_num_rising_edge(clk, 5);

        axilite_check(AXILITE_VVCT, 2, x"10", x"DE2711BB", "Check data from target"); --@suppress
        axilite_check(AXILITE_VVCT, 2, x"10", x"AD45A78C", "Check data from target"); --@suppress
        axilite_check(AXILITE_VVCT, 2, x"10", x"BE1FF25B", "Check data from target"); --@suppress
        axilite_check(AXILITE_VVCT, 2, x"10", x"EF77900D", "Check data from target"); --@suppress
        await_completion(AXILITE_VVCT, 2, C_CLK_PERIOD * 100, "Finish reading data from FromHost FIFO"); --@suppress

        log(ID_LOG_HDR, "Send data from target to host");
        axilite_write(AXILITE_VVCT, 2, x"14", x"0", "Send not eop"); --@suppress
        axilite_write(AXILITE_VVCT, 2, x"C", x"BA773AA2", "Send data from target"); --@suppress
        axilite_write(AXILITE_VVCT, 2, x"C", x"BE982FEC", "Send data from target"); --@suppress
        axilite_write(AXILITE_VVCT, 2, x"C", x"CA009CA1", "Send data from target"); --@suppress
        axilite_write(AXILITE_VVCT, 2, x"14", x"20", "Send eop"); --@suppress
        axilite_write(AXILITE_VVCT, 2, x"C", x"CEFA6700", "Send data from target"); --@suppress
        await_completion(AXILITE_VVCT, 2, C_CLK_PERIOD * 100, "Finish writing data to FromHost FIFO"); --@suppress
        wait_num_rising_edge(clk, 10);

        toHost_fifo_read(data, eop, "read data from target");
        check_value(data, x"BA773AA2", ERROR, "checking value", C_SCOPE);
        check_value(eop, '0', ERROR, "checking eop", C_SCOPE);
        toHost_fifo_read(data, eop, "read data from target");
        check_value(data, x"BE982FEC", ERROR, "checking value", C_SCOPE);
        check_value(eop, '0', ERROR, "checking eop", C_SCOPE);
        toHost_fifo_read(data, eop, "read data from target");
        check_value(data, x"CA009CA1", ERROR, "checking value", C_SCOPE);
        check_value(eop, '0', ERROR, "checking eop", C_SCOPE);
        toHost_fifo_read(data, eop, "read data from target");
        check_value(data, x"CEFA6700", ERROR, "checking value", C_SCOPE);
        check_value(eop, '1', ERROR, "checking eop", C_SCOPE);

        wait for 1000 ns;                -- to allow some time for completion
        report_alert_counters(FINAL);   -- Report final counters and print conclusion for simulation (Success/Fail)

        uvvm_completed <= '1';
        wait for 10 ns;

        stop_clock(CLOCK_GENERATOR_VVCT, 1, "Stop clock generator");--@suppress

        wait;

    end process;


end architecture sim;

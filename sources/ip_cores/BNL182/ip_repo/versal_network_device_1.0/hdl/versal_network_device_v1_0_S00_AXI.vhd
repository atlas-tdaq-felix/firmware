Library xpm;
    use xpm.vcomponents.all;

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;


entity VERSAL_virtual_network_v1_0_S00_AXI is
    generic (
        -- Users to add parameters here

        -- User parameters ends
        -- Do not modify the parameters beyond this line

        -- Width of S_AXI data bus
        C_S_AXI_DATA_WIDTH  : integer  := 32;
        -- Width of S_AXI address bus
        C_S_AXI_ADDR_WIDTH  : integer  := 5
    );
    port (
        -- Users to add ports here
        host_clk                      : in std_logic;
        fromHost_wr_en_i              : in std_logic;
        fromHost_full_o               : out std_logic;
        fromHost_prog_full_o          : out std_logic;
        fromHost_din_i                : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
        fromHost_eop_i                : in std_logic;
        toHost_rd_en_i                : in std_logic;
        toHost_empty_o                : out std_logic;
        toHost_prog_empty_o           : out std_logic;
        tohost_dout_o                 : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
        toHost_eop_o                  : out std_logic;
        toHost_status_carrier_o       : out std_logic;
        fromHost_set_carrier_i        : in std_logic;

        -- User ports ends
        -- Do not modify the ports beyond this line

        -- Global Clock Signal
        S_AXI_ACLK  : in std_logic;
        -- Global Reset Signal. This Signal is Active LOW
        S_AXI_ARESETN  : in std_logic;
        -- Write address (issued by master, acceped by Slave)
        S_AXI_AWADDR  : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
        -- Write channel Protection type. This signal indicates the
        -- privilege and security level of the transaction, and whether
        -- the transaction is a data access or an instruction access.
        S_AXI_AWPROT  : in std_logic_vector(2 downto 0);
        -- Write address valid. This signal indicates that the master signaling
        -- valid write address and control information.
        S_AXI_AWVALID  : in std_logic;
        -- Write address ready. This signal indicates that the slave is ready
        -- to accept an address and associated control signals.
        S_AXI_AWREADY  : out std_logic;
        -- Write data (issued by master, acceped by Slave)
        S_AXI_WDATA  : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
        -- Write strobes. This signal indicates which byte lanes hold
        -- valid data. There is one write strobe bit for each eight
        -- bits of the write data bus.
        S_AXI_WSTRB  : in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
        -- Write valid. This signal indicates that valid write
        -- data and strobes are available.
        S_AXI_WVALID  : in std_logic;
        -- Write ready. This signal indicates that the slave
        -- can accept the write data.
        S_AXI_WREADY  : out std_logic;
        -- Write response. This signal indicates the status
        -- of the write transaction.
        S_AXI_BRESP  : out std_logic_vector(1 downto 0);
        -- Write response valid. This signal indicates that the channel
        -- is signaling a valid write response.
        S_AXI_BVALID  : out std_logic;
        -- Response ready. This signal indicates that the master
        -- can accept a write response.
        S_AXI_BREADY  : in std_logic;
        -- Read address (issued by master, acceped by Slave)
        S_AXI_ARADDR  : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
        -- Protection type. This signal indicates the privilege
        -- and security level of the transaction, and whether the
        -- transaction is a data access or an instruction access.
        S_AXI_ARPROT  : in std_logic_vector(2 downto 0);
        -- Read address valid. This signal indicates that the channel
        -- is signaling valid read address and control information.
        S_AXI_ARVALID  : in std_logic;
        -- Read address ready. This signal indicates that the slave is
        -- ready to accept an address and associated control signals.
        S_AXI_ARREADY  : out std_logic;
        -- Read data (issued by slave)
        S_AXI_RDATA  : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
        -- Read response. This signal indicates the status of the
        -- read transfer.
        S_AXI_RRESP  : out std_logic_vector(1 downto 0);
        -- Read valid. This signal indicates that the channel is
        -- signaling the required read data.
        S_AXI_RVALID  : out std_logic;
        -- Read ready. This signal indicates that the master can
        -- accept the read data and response information.
        S_AXI_RREADY  : in std_logic
    );
end VERSAL_virtual_network_v1_0_S00_AXI;

architecture arch_imp of VERSAL_virtual_network_v1_0_S00_AXI is

    -- Example-specific design signals
    -- local parameter for addressing 32 bit / 64 bit C_S_AXI_DATA_WIDTH
    -- ADDR_LSB is used for addressing 32/64 bit registers/memories
    -- ADDR_LSB = 2 for 32 bits (n downto 2)
    -- ADDR_LSB = 3 for 64 bits (n downto 3)
    constant ADDR_LSB  : integer := (C_S_AXI_DATA_WIDTH/32) + 1;
    constant OPT_MEM_ADDR_BITS : integer := 2;

    constant FIFO_MAX_DEPTH : integer := 1024;
    constant FIFO_PROG_FULL : integer := FIFO_MAX_DEPTH - 400;

    ------------------------------------------------
    ---- Signals for user logic register space example
    --------------------------------------------------

    signal rstn, rstn_host_clk  : std_logic;
    signal fromHost_wr_en_i_p1  : std_logic;
    signal fromHost_prog_full   : std_logic;
    signal toHost_rd_en_i_p1    : std_logic;
    signal toHost_prog_full     : std_logic;

    signal fromHost_m_axis_tdata  : std_logic_vector(31 downto 0);
    signal fromHost_m_axis_tlast  : std_logic;
    signal fromHost_m_axis_tvalid : std_logic;
    signal fromHost_m_axis_tready : std_logic;

    signal fromHost_s_axis_tdata  : std_logic_vector(31 downto 0);
    signal fromHost_s_axis_tkeep  : std_logic_vector(3 downto 0);
    signal fromHost_s_axis_tid    : std_logic_vector(0 downto 0);
    signal fromHost_s_axis_tstrb  : std_logic_vector(3 downto 0);
    signal fromHost_s_axis_tuser  : std_logic_vector(0 downto 0);
    signal fromHost_s_axis_tdest  : std_logic_vector(0 downto 0);
    signal fromHost_s_axis_tlast  : std_logic;
    signal fromHost_s_axis_tvalid : std_logic;
    signal fromHost_s_axis_tready : std_logic;

    signal toHost_m_axis_tdata  : std_logic_vector(31 downto 0);
    signal toHost_m_axis_tlast  : std_logic;
    signal toHost_m_axis_tvalid : std_logic;
    signal toHost_m_axis_tready : std_logic;

    signal toHost_s_axis_tdata  : std_logic_vector(31 downto 0);
    signal toHost_s_axis_tkeep  : std_logic_vector(3 downto 0);
    signal toHost_s_axis_tid    : std_logic_vector(0 downto 0);
    signal toHost_s_axis_tstrb  : std_logic_vector(3 downto 0);
    signal toHost_s_axis_tuser  : std_logic_vector(0 downto 0);
    signal toHost_s_axis_tdest  : std_logic_vector(0 downto 0);
    signal toHost_s_axis_tlast  : std_logic;
    signal toHost_s_axis_tvalid : std_logic;
    signal toHost_s_axis_tready : std_logic;

    signal axi_rvalid, axi_awready, axi_wready : std_logic;
    signal axi_bvalid, axi_arready : std_logic;

    constant AXI_OK   :std_logic_vector(1 downto 0)    := b"00";
    constant MAGIC    : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0)    := x"CECABABA";

    type t_rd_state is (s_init, s_addr_wait, s_read, s_read_wait);
    type t_wr_state is (s_init, s_addr_wait, s_data_wait, s_write, s_write_wait, s_bresp_wait);

    signal rd_state : t_rd_state := s_init;
    signal wr_state : t_wr_state := s_init;

    signal wr_addr  : std_logic_vector(2 downto 0);
    signal rd_addr  : std_logic_vector(2 downto 0);

    signal target_set_carrier_s     : std_logic;
    signal target_status_carrier_s  : std_logic;


begin

    rstn <= S_AXI_ARESETN;

    S_AXI_AWREADY <= axi_awready;
    S_AXI_WREADY <= axi_wready;
    S_AXI_BVALID <= axi_bvalid;
    S_AXI_ARREADY <= axi_arready;
    S_AXI_RVALID <= axi_rvalid;

    -- AXI write FSM

    axi_wr_state : process (S_AXI_ACLK) is
    begin
        if rising_edge(S_AXI_ACLK) then
            if rstn = '0' then
                wr_state <= s_init;
            else
                case wr_state is
                    when s_init =>
                        wr_state <= s_addr_wait;

                    when s_addr_wait =>
                        if (S_AXI_AWVALID = '1' and axi_awready = '1') then
                            wr_state <= s_data_wait;
                        end if;

                    when s_data_wait =>
                        if (S_AXI_WVALID = '1' and axi_wready = '1') then
                            wr_state <= s_write;
                        end if;

                    when s_write =>
                        wr_state <= s_bresp_wait;

                    when s_bresp_wait =>
                        if (S_AXI_BREADY = '1' and axi_bvalid = '1') then
                            wr_state <= s_addr_wait;
                        end if;

                    when others =>
                        wr_state <= s_init;
                end case;
            end if;
        end if;
    end process;

    axi_wr_var : process (S_AXI_ACLK) is
    begin
        if rising_edge(S_AXI_ACLK) then
            if rstn = '0' then
                toHost_s_axis_tdata <= x"0000_0000";
                toHost_s_axis_tlast <= '0';
                target_set_carrier_s <= '0';
                wr_addr <= "000";
                axi_bvalid <= '0';
                axi_awready <= '0';
                axi_wready <= '0';
                S_AXI_BRESP <= AXI_OK;
                toHost_s_axis_tvalid <= '0';
            else
                case wr_state is
                    when s_init =>
                        axi_bvalid <= '0';
                        axi_awready <= '0';
                        axi_wready <= '0';
                        S_AXI_BRESP <= AXI_OK;
                        toHost_s_axis_tvalid <= '0';

                    when s_addr_wait =>
                        axi_bvalid <= '0';
                        axi_awready <= '1';
                        axi_wready <= '0';
                        toHost_s_axis_tvalid <= '0';

                        if (S_AXI_AWVALID = '1' and axi_awready= '1') then
                            wr_addr <= S_AXI_AWADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB);
                            axi_awready <= '0';
                            axi_wready <= '1';
                        end if;

                    when s_data_wait =>
                        axi_awready <= '0';
                        axi_wready <= '1';
                        axi_bvalid <= '0';
                        toHost_s_axis_tvalid <= '0';

                        if (S_AXI_WVALID = '1' and axi_wready = '1') then
                            axi_wready <= '0';
                        end if;

                    when s_write =>
                        axi_awready <= '0';
                        axi_wready <= '0';
                        axi_bvalid <= '1';
                        S_AXI_BRESP <= AXI_OK;
                        toHost_s_axis_tvalid <= '0';

                        case wr_addr is

                            when b"011" => --TARGET_ADDR_SEND
                                toHost_s_axis_tdata(C_S_AXI_DATA_WIDTH-1 downto 0) <= S_AXI_WDATA;
                                toHost_s_axis_tvalid <= '1';

                            when b"101" =>
                                target_set_carrier_s <= S_AXI_WDATA(7);
                                toHost_s_axis_tlast <= S_AXI_WDATA(5);

                            when others =>
                                -- report "read-only register written";
                                null;
                        end case;

                    when s_bresp_wait =>
                        axi_bvalid <= '1';
                        axi_awready <= '0';
                        axi_wready <= '0';
                        toHost_s_axis_tvalid <= '0';

                        if (S_AXI_BREADY = '1' and axi_bvalid = '1') then
                            axi_bvalid <= '0';
                            axi_awready <= '1';
                        end if;

                    when others =>
                        null;
                end case;
            end if;
        end if;
    end process;


    -- AXI read FSM

    axi_rd_state : process (S_AXI_ACLK) is
    begin
        if rising_edge(S_AXI_ACLK) then
            if rstn = '0' then
                rd_state <= s_init;
            else
                case rd_state is
                    when s_init =>
                        rd_state <= s_addr_wait;

                    when s_addr_wait =>
                        if (S_AXI_ARVALID = '1' and axi_arready = '1') then
                            rd_state <= s_read;
                        end if;

                    when s_read =>
                        rd_state <= s_read_wait;

                    when s_read_wait =>
                        if (S_AXI_RREADY = '1' and axi_rvalid = '1') then
                            rd_state <= s_addr_wait;
                        end if;

                    when others =>
                        rd_state <= s_init;
                end case;
            end if;
        end if;
    end process;


    axi_rd_vars : process (S_AXI_ACLK) is
    begin
        if rising_edge(S_AXI_ACLK) then
            if rstn = '0' then
                rd_addr <= "000";
                S_AXI_RRESP <= AXI_OK;
                S_AXI_RDATA <= (others => '0');
                axi_rvalid  <= '0';
                axi_arready <= '0';
                fromHost_m_axis_tready <= '0';

            else
                case rd_state is
                    when s_init =>
                        S_AXI_RRESP <= AXI_OK;
                        S_AXI_RDATA <= (others => '0');
                        axi_rvalid  <= '0';
                        axi_arready <= '0';
                        fromHost_m_axis_tready <= '0';

                    when s_addr_wait =>
                        fromHost_m_axis_tready <= '0';
                        axi_rvalid  <= '0';
                        axi_arready <= '1';

                        if (S_AXI_ARVALID = '1' and axi_arready = '1') then
                            rd_addr <= S_AXI_ARADDR(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB);
                            axi_arready <= '0';
                        end if;

                    when s_read =>
                        axi_rvalid  <= '1';
                        axi_arready <= '0';
                        fromHost_m_axis_tready  <= '0';
                        S_AXI_RRESP <= AXI_OK;

                        case rd_addr is
                            when b"100" =>  --TARGET_ADDR_RECV
                                if (fromHost_m_axis_tvalid = '0') then
                                    S_AXI_RDATA <= (others => '1');
                                else
                                    fromHost_m_axis_tready <= '1';
                                    S_AXI_RDATA <= fromHost_m_axis_tdata(C_S_AXI_DATA_WIDTH-1 downto 0);
                                end if;

                            when b"101" => --TARGET_ADDR_STATUS
                                S_AXI_RRESP    <= AXI_OK;
                                S_AXI_RDATA(C_S_AXI_DATA_WIDTH-1 downto 5) <= (others => '0');
                                S_AXI_RDATA(7) <= target_set_carrier_s;
                                S_AXI_RDATA(6) <= target_status_carrier_s;
                                S_AXI_RDATA(4) <= fromHost_m_axis_tlast;
                                S_AXI_RDATA(3) <= not toHost_prog_full;
                                S_AXI_RDATA(2) <= toHost_s_axis_tready;
                                S_AXI_RDATA(1) <= fromHost_m_axis_tvalid;
                                S_AXI_RDATA(0) <= fromHost_m_axis_tvalid;

                            when b"110" => --TARGET_MAGIC_REG
                                S_AXI_RRESP <= AXI_OK;
                                S_AXI_RDATA <= MAGIC;

                            when others =>
                                S_AXI_RRESP <= AXI_OK;
                                S_AXI_RDATA <= (others => '1');
                        end case;

                    when s_read_wait =>
                        axi_rvalid  <= '1';
                        axi_arready <= '0';
                        fromHost_m_axis_tready <= '0';
                        if (S_AXI_RREADY = '1' and axi_rvalid = '1') then
                            axi_rvalid  <= '0';
                            axi_arready <= '1';
                        end if;

                    when others =>
                        null;
                end case;
            end if;
        end if;
    end process;


    -- Add user logic here

    fromHost_s_axis_tvalid  <= fromHost_wr_en_i and not fromHost_wr_en_i_p1;
    fromHost_full_o         <= not fromHost_s_axis_tready;
    fromHost_prog_full_o    <= fromHost_prog_full;
    fromHost_s_axis_tdata(C_S_AXI_DATA_WIDTH-1 downto 0) <= fromHost_din_i;
    fromHost_s_axis_tlast   <= fromHost_eop_i;

    toHost_prog_empty_o  <= not toHost_m_axis_tvalid;
    toHost_empty_o       <= not toHost_m_axis_tvalid;
    toHost_m_axis_tready <= toHost_rd_en_i and not toHost_rd_en_i_p1;
    toHost_dout_o        <= toHost_m_axis_tdata(C_S_AXI_DATA_WIDTH-1 downto 0);
    toHost_eop_o         <= tohost_m_axis_tlast;

    pipe: process(host_clk)
    begin
        if rising_edge(host_clk) then
            fromHost_wr_en_i_p1 <= fromHost_wr_en_i;
            toHost_rd_en_i_p1   <= toHost_rd_en_i;
        end if;
    end process;

    xpm_cdc_async_rst_inst : xpm_cdc_async_rst
        generic map (
            DEST_SYNC_FF    => 2,    -- DECIMAL; range: 2-10
            INIT_SYNC_FF    => 0,    -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            RST_ACTIVE_HIGH => 0     -- DECIMAL; 0=active low reset, 1=active high reset
        )
        port map (
            dest_arst => rstn_host_clk,
            dest_clk  => host_clk,
            src_arst  => rstn
        );

    xpm_cdc_single_toHost_carrier : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 2,   -- DECIMAL; range: 2-10
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 1
        )
        port map (
            dest_out => toHost_status_carrier_o,
            dest_clk => host_clk,
            src_clk => S_AXI_ACLK,
            src_in => target_set_carrier_s
        );

    xpm_cdc_single_fromHost_carrier : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 2,   -- DECIMAL; range: 2-10
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 1
        )
        port map (
            dest_out => target_status_carrier_s,
            dest_clk => S_AXI_ACLK,
            src_clk => host_clk,
            src_in => fromHost_set_carrier_i
        );

    fromHost_s_axis_tdest <= "0";
    fromHost_s_axis_tid   <= "0";
    fromHost_s_axis_tkeep <= "1111";
    fromHost_s_axis_tstrb <= "0000";
    fromHost_s_axis_tuser <= "0";

    fromHostFifo : xpm_fifo_axis
        generic map (
            CASCADE_HEIGHT      => 0,
            CDC_SYNC_STAGES     => 2,
            CLOCKING_MODE       => "independent_clock",
            ECC_MODE            => "no_ecc",
            FIFO_DEPTH          => FIFO_MAX_DEPTH,
            FIFO_MEMORY_TYPE    => "auto",
            PACKET_FIFO         => "true",
            PROG_EMPTY_THRESH   => 10,
            PROG_FULL_THRESH    => FIFO_PROG_FULL, -- maximum size ethernet frame + delimiters
            RD_DATA_COUNT_WIDTH => 1,
            RELATED_CLOCKS      => 0,
            SIM_ASSERT_CHK      => 0,
            TDATA_WIDTH         => 32,
            TDEST_WIDTH         => 1,
            TID_WIDTH           => 1,
            TUSER_WIDTH         => 1,
            USE_ADV_FEATURES    => "0002",        -- programmable full
            WR_DATA_COUNT_WIDTH => 1
        )
        port map (
            almost_empty_axis   => open,
            almost_full_axis    => open,
            dbiterr_axis        => open,
            injectdbiterr_axis  => '0',
            injectsbiterr_axis  => '0',
            m_axis_tdata        => fromHost_m_axis_tdata,
            m_axis_tdest        => open,
            m_axis_tid          => open,
            m_axis_tkeep        => open,
            m_axis_tlast        => fromHost_m_axis_tlast,
            m_axis_tstrb        => open,
            m_axis_tuser        => open,
            m_axis_tvalid       => fromHost_m_axis_tvalid,
            m_axis_tready       => fromHost_m_axis_tready,
            prog_empty_axis     => open,
            prog_full_axis      => fromHost_prog_full,
            rd_data_count_axis  => open,
            sbiterr_axis        => open,
            wr_data_count_axis  => open,
            m_aclk              => S_AXI_ACLK,
            s_aclk              => host_clk,
            s_aresetn           => rstn_host_clk,
            s_axis_tdata        => fromHost_s_axis_tdata,
            s_axis_tdest        => fromHost_s_axis_tdest,
            s_axis_tid          => fromHost_s_axis_tid,
            s_axis_tkeep        => fromHost_s_axis_tkeep,
            s_axis_tlast        => fromHost_s_axis_tlast,
            s_axis_tstrb        => fromHost_s_axis_tstrb,
            s_axis_tuser        => fromHost_s_axis_tuser,
            s_axis_tvalid       => fromHost_s_axis_tvalid,
            s_axis_tready       => fromHost_s_axis_tready
        );


    toHost_s_axis_tdest <= "0";
    toHost_s_axis_tid   <= "0";
    toHost_s_axis_tkeep <= "1111";
    toHost_s_axis_tstrb <= "0000";
    toHost_s_axis_tuser <= "0";

    toHostFifo : xpm_fifo_axis
        generic map (
            CASCADE_HEIGHT      => 0,
            CDC_SYNC_STAGES     => 2,
            CLOCKING_MODE       => "independent_clock",
            ECC_MODE            => "no_ecc",
            FIFO_DEPTH          => FIFO_MAX_DEPTH,
            FIFO_MEMORY_TYPE    => "auto",
            PACKET_FIFO         => "true",
            PROG_EMPTY_THRESH   => 10,
            PROG_FULL_THRESH    => FIFO_PROG_FULL, -- maximum size ethernet frame + delimiters
            RD_DATA_COUNT_WIDTH => 1,
            RELATED_CLOCKS      => 0,
            SIM_ASSERT_CHK      => 0,
            TDATA_WIDTH         => 32,
            TDEST_WIDTH         => 1,
            TID_WIDTH           => 1,
            TUSER_WIDTH         => 1,
            USE_ADV_FEATURES    => "0002",        -- programmable full
            WR_DATA_COUNT_WIDTH => 1
        )
        port map (
            almost_empty_axis   => open,
            almost_full_axis    => open,
            dbiterr_axis        => open,
            injectdbiterr_axis  => '0',
            injectsbiterr_axis  => '0',
            m_axis_tdata        => toHost_m_axis_tdata,
            m_axis_tdest        => open,
            m_axis_tid          => open,
            m_axis_tkeep        => open,
            m_axis_tlast        => toHost_m_axis_tlast,
            m_axis_tstrb        => open,
            m_axis_tuser        => open,
            m_axis_tvalid       => toHost_m_axis_tvalid,
            m_axis_tready       => toHost_m_axis_tready,
            prog_empty_axis     => open,
            prog_full_axis      => toHost_prog_full,
            rd_data_count_axis  => open,
            sbiterr_axis        => open,
            wr_data_count_axis  => open,
            m_aclk              => host_clk,
            s_aclk              => S_AXI_ACLK,
            s_aresetn           => rstn,
            s_axis_tdata        => toHost_s_axis_tdata,
            s_axis_tdest        => toHost_s_axis_tdest,
            s_axis_tid          => toHost_s_axis_tid,
            s_axis_tkeep        => toHost_s_axis_tkeep,
            s_axis_tlast        => toHost_s_axis_tlast,
            s_axis_tstrb        => toHost_s_axis_tstrb,
            s_axis_tuser        => toHost_s_axis_tuser,
            s_axis_tvalid       => toHost_s_axis_tvalid,
            s_axis_tready       => toHost_s_axis_tready
        );

end arch_imp;

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
library xpm;
    use xpm.VCOMPONENTS.ALL;

entity fifo_generator_felix is
    port (
        rst : in STD_LOGIC;
        wr_clk : in STD_LOGIC;
        rd_clk : in STD_LOGIC;
        din : in STD_LOGIC_VECTOR ( 227 downto 0 );
        wr_en : in STD_LOGIC;
        rd_en : in STD_LOGIC;
        dout : out STD_LOGIC_VECTOR ( 227 downto 0 );
        full : out STD_LOGIC;
        empty : out STD_LOGIC;
        prog_full : out STD_LOGIC;
        prog_empty : out STD_LOGIC;
        wr_rst_busy : out STD_LOGIC;
        rd_rst_busy : out STD_LOGIC
    );
end fifo_generator_felix;

architecture STRUCTURE of fifo_generator_felix is
begin

    xpm_fifo_async_inst : xpm_fifo_async
        generic map (
            CASCADE_HEIGHT => 0,        -- DECIMAL
            CDC_SYNC_STAGES => 2,       -- DECIMAL
            DOUT_RESET_VALUE => "0",    -- String
            ECC_MODE => "no_ecc",       -- String
            FIFO_MEMORY_TYPE => "auto", -- String
            FIFO_READ_LATENCY => 1,     -- DECIMAL
            FIFO_WRITE_DEPTH => 64,   -- DECIMAL
            FULL_RESET_VALUE => 1,      -- DECIMAL
            PROG_EMPTY_THRESH => 10,    -- DECIMAL
            PROG_FULL_THRESH => 10,     -- DECIMAL
            RD_DATA_COUNT_WIDTH => 1,   -- DECIMAL
            READ_DATA_WIDTH => 228,      -- DECIMAL
            READ_MODE => "std",         -- String
            RELATED_CLOCKS => 0,        -- DECIMAL
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_ADV_FEATURES => "0202", -- Prog full and prog empty flag
            WAKEUP_TIME => 0,           -- DECIMAL
            WRITE_DATA_WIDTH => 228,     -- DECIMAL
            WR_DATA_COUNT_WIDTH => 1    -- DECIMAL
        )
        port map (
            almost_empty => open,
            almost_full => open,
            data_valid => open,
            dbiterr => open,
            dout => dout,
            empty => empty,
            full => full,
            overflow => open,
            prog_empty => prog_empty,
            prog_full => prog_full,
            rd_data_count => open,
            rd_rst_busy => rd_rst_busy,
            sbiterr => open,
            underflow => open,
            wr_ack => open,
            wr_data_count => open,
            wr_rst_busy => wr_rst_busy,
            din => din,
            injectdbiterr => '0',
            injectsbiterr => '0',
            rd_clk => rd_clk,
            rd_en => rd_en,
            rst => rst,
            sleep => '0',
            wr_clk => wr_clk,
            wr_en => wr_en
        );


end STRUCTURE;

-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- Copyright 2022-2024 Advanced Micro Devices, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2024.1 (lin64) Build 5076996 Wed May 22 18:36:09 MDT 2024
-- Date        : Wed Jun 12 16:30:55 2024
-- Host        : lbp001app.nikhef.nl running 64-bit unknown
-- Command     : write_vhdl -force -mode synth_stub
--               /localstore/et/franss/felix/firmware/Projects/FLX712_FMEMU/FLX712_FMEMU.gen/sources_1/ip/ila_gbt_rx_frame/ila_gbt_rx_frame_stub.vhdl
-- Design      : ila_gbt_rx_frame
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku115-flvf1924-2-e
-- --------------------------------------------------------------------------------
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

entity ila_gbt_rx_frame is
    Port (
        clk : in STD_LOGIC;
        probe0 : in STD_LOGIC_VECTOR ( 79 downto 0 )
    );

end ila_gbt_rx_frame;

architecture stub of ila_gbt_rx_frame is
    attribute syn_black_box : boolean;
    attribute black_box_pad_pin : string;
    attribute syn_black_box of stub : architecture is true;
    attribute black_box_pad_pin of stub : architecture is "clk,probe0[79:0]";
    attribute x_core_info : string;
    attribute x_core_info of stub : architecture is "ila,Vivado 2024.1";
begin
end;

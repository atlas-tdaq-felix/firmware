library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
library xpm;
    use xpm.VCOMPONENTS.ALL;

entity fifo_generator_0_ENCRRD53A is
    port (
        clk : IN STD_LOGIC;
        srst : IN STD_LOGIC;
        din : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        wr_en : IN STD_LOGIC;
        rd_en : IN STD_LOGIC;
        dout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        full : OUT STD_LOGIC;
        almost_full : OUT STD_LOGIC;
        empty : OUT STD_LOGIC;
        valid : OUT STD_LOGIC;
        wr_rst_busy : OUT STD_LOGIC;
        rd_rst_busy : OUT STD_LOGIC
    );
end fifo_generator_0_ENCRRD53A;

architecture STRUCTURE of fifo_generator_0_ENCRRD53A is
begin

    xpm_fifo_async_inst : xpm_fifo_sync
        generic map (
            CASCADE_HEIGHT => 0,        -- DECIMAL
            DOUT_RESET_VALUE => "0",    -- String
            ECC_MODE => "no_ecc",       -- String
            FIFO_MEMORY_TYPE => "auto", -- String
            FIFO_READ_LATENCY => 1,     -- DECIMAL
            FIFO_WRITE_DEPTH => 1024,   -- DECIMAL
            FULL_RESET_VALUE => 1,      -- DECIMAL
            PROG_EMPTY_THRESH => 10,    -- DECIMAL
            PROG_FULL_THRESH => 10,     -- DECIMAL
            RD_DATA_COUNT_WIDTH => 1,   -- DECIMAL
            READ_DATA_WIDTH => 16,      -- DECIMAL
            READ_MODE => "fwft",         -- String
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_ADV_FEATURES => "1008", -- almost_full and data_valid
            WAKEUP_TIME => 0,           -- DECIMAL
            WRITE_DATA_WIDTH => 16,     -- DECIMAL
            WR_DATA_COUNT_WIDTH => 1    -- DECIMAL
        )
        port map (
            almost_empty => open,
            almost_full => open,
            data_valid => valid,
            dbiterr => open,
            dout => dout,
            empty => empty,
            full => full,
            overflow => open,
            prog_empty => open,
            prog_full => open,
            rd_data_count => open,
            rd_rst_busy => rd_rst_busy,
            sbiterr => open,
            underflow => open,
            wr_ack => open,
            wr_data_count => open,
            wr_rst_busy => wr_rst_busy,
            din => din,
            injectdbiterr => '0',
            injectsbiterr => '0',
            rd_en => rd_en,
            rst => srst,
            sleep => '0',
            wr_clk => clk,
            wr_en => wr_en
        );


end STRUCTURE;

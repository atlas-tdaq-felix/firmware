--Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
--Date        : Thu Apr 13 14:28:28 2023
--Host        : PC-22-023 running 64-bit Ubuntu 22.04.2 LTS
--Command     : generate_target transceiver_versal_lpgbt_wrapper.bd
--Design      : transceiver_versal_lpgbt_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
    use UNISIM.VCOMPONENTS.ALL;
entity transceiver_versal_lpgbt_wrapper is
    port (
        APB3_INTF_0_paddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
        APB3_INTF_0_penable : in STD_LOGIC;
        APB3_INTF_0_prdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
        APB3_INTF_0_pready : out STD_LOGIC;
        APB3_INTF_0_psel : in STD_LOGIC;
        APB3_INTF_0_pslverr : out STD_LOGIC;
        APB3_INTF_0_pwdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
        APB3_INTF_0_pwrite : in STD_LOGIC;
        GT_REFCLK0_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
        GT_REFCLK0_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
        GT_REFCLK1_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
        GT_REFCLK1_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
        GT_Serial_grx_n : in STD_LOGIC_VECTOR ( 3 downto 0 );
        GT_Serial_grx_p : in STD_LOGIC_VECTOR ( 3 downto 0 );
        GT_Serial_gtx_n : out STD_LOGIC_VECTOR ( 3 downto 0 );
        GT_Serial_gtx_p : out STD_LOGIC_VECTOR ( 3 downto 0 );
        apb3clk_gt_bridge_ip_0 : in STD_LOGIC;
        apb3clk_quad : in STD_LOGIC;
        apb3presetn_0 : in STD_LOGIC;
        apb3presetn_1 : in STD_LOGIC;
        ch0_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
        ch0_rxbyteisaligned_ext_0 : out STD_LOGIC;
        ch0_rxcdrlock_ext_0 : out STD_LOGIC;
        ch0_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
        ch0_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
        ch0_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
        ch0_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
        ch0_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
        ch0_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch0_rxgearboxslip_ext_0 : in STD_LOGIC;
        ch0_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
        ch0_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch0_rxpmaresetdone_ext_0 : out STD_LOGIC;
        ch0_rxpolarity_ext_0 : in STD_LOGIC;
        ch0_rxresetdone_ext_0 : out STD_LOGIC;
        ch0_rxslide_ext_0 : in STD_LOGIC;
        ch0_rxvalid_ext_0 : out STD_LOGIC;
        ch0_txbufstatus_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch0_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
        ch0_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
        ch0_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
        ch0_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
        ch0_txheader_ext_0 : in STD_LOGIC_VECTOR ( 5 downto 0 );
        ch0_txpippmen_ext_0 : in STD_LOGIC;
        ch0_txpippmstepsize_ext_0 : in STD_LOGIC_VECTOR ( 4 downto 0 );
        ch0_txpolarity_ext_0 : in STD_LOGIC;
        ch0_txresetdone_ext_0 : out STD_LOGIC;
        ch0_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
        ch1_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
        ch1_rxbyteisaligned_ext_0 : out STD_LOGIC;
        ch1_rxcdrlock_ext_0 : out STD_LOGIC;
        ch1_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
        ch1_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
        ch1_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
        ch1_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
        ch1_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
        ch1_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch1_rxgearboxslip_ext_0 : in STD_LOGIC;
        ch1_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
        ch1_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch1_rxpmaresetdone_ext_0 : out STD_LOGIC;
        ch1_rxpolarity_ext_0 : in STD_LOGIC;
        ch1_rxresetdone_ext_0 : out STD_LOGIC;
        ch1_rxslide_ext_0 : in STD_LOGIC;
        ch1_rxvalid_ext_0 : out STD_LOGIC;
        ch1_txbufstatus_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch1_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
        ch1_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
        ch1_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
        ch1_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
        ch1_txpippmen_ext_0 : in STD_LOGIC;
        ch1_txpippmstepsize_ext_0 : in STD_LOGIC_VECTOR ( 4 downto 0 );
        ch1_txpolarity_ext_0 : in STD_LOGIC;
        ch1_txresetdone_ext_0 : out STD_LOGIC;
        ch1_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
        ch2_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
        ch2_rxbyteisaligned_ext_0 : out STD_LOGIC;
        ch2_rxcdrlock_ext_0 : out STD_LOGIC;
        ch2_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
        ch2_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
        ch2_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
        ch2_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
        ch2_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
        ch2_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch2_rxgearboxslip_ext_0 : in STD_LOGIC;
        ch2_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
        ch2_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch2_rxpmaresetdone_ext_0 : out STD_LOGIC;
        ch2_rxpolarity_ext_0 : in STD_LOGIC;
        ch2_rxresetdone_ext_0 : out STD_LOGIC;
        ch2_rxslide_ext_0 : in STD_LOGIC;
        ch2_rxvalid_ext_0 : out STD_LOGIC;
        ch2_txbufstatus_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch2_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
        ch2_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
        ch2_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
        ch2_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
        ch2_txpippmen_ext_0 : in STD_LOGIC;
        ch2_txpippmstepsize_ext_0 : in STD_LOGIC_VECTOR ( 4 downto 0 );
        ch2_txpolarity_ext_0 : in STD_LOGIC;
        ch2_txresetdone_ext_0 : out STD_LOGIC;
        ch2_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
        ch3_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
        ch3_rxbyteisaligned_ext_0 : out STD_LOGIC;
        ch3_rxcdrlock_ext_0 : out STD_LOGIC;
        ch3_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
        ch3_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
        ch3_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
        ch3_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
        ch3_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
        ch3_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch3_rxgearboxslip_ext_0 : in STD_LOGIC;
        ch3_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
        ch3_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch3_rxpmaresetdone_ext_0 : out STD_LOGIC;
        ch3_rxpolarity_ext_0 : in STD_LOGIC;
        ch3_rxresetdone_ext_0 : out STD_LOGIC;
        ch3_rxslide_ext_0 : in STD_LOGIC;
        ch3_rxvalid_ext_0 : out STD_LOGIC;
        ch3_txbufstatus_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch3_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
        ch3_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
        ch3_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
        ch3_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
        ch3_txpippmen_ext_0 : in STD_LOGIC;
        ch3_txpippmstepsize_ext_0 : in STD_LOGIC_VECTOR ( 4 downto 0 );
        ch3_txpolarity_ext_0 : in STD_LOGIC;
        ch3_txresetdone_ext_0 : out STD_LOGIC;
        ch3_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
        gt_reset_gt_bridge_ip_0 : in STD_LOGIC;
        lcpll_lock_gt_bridge_ip_0 : out STD_LOGIC;
        link_status_gt_bridge_ip_0 : out STD_LOGIC;
        rate_sel_gt_bridge_ip_0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
        reset_rx_datapath_in_0 : in STD_LOGIC;
        reset_rx_pll_and_datapath_in_0 : in STD_LOGIC;
        reset_tx_datapath_in_0 : in STD_LOGIC;
        reset_tx_pll_and_datapath_in_0 : in STD_LOGIC;
        rpll_lock_gt_bridge_ip_0 : out STD_LOGIC;
        rx_resetdone_out_gt_bridge_ip_0 : out STD_LOGIC;
        rxusrclk_gt_bridge_ip_0 : out STD_LOGIC;
        tx_resetdone_out_gt_bridge_ip_0 : out STD_LOGIC;
        txusrclk_gt_bridge_ip_0 : out STD_LOGIC
    );
end transceiver_versal_lpgbt_wrapper;

architecture STRUCTURE of transceiver_versal_lpgbt_wrapper is
    component transceiver_versal_lpgbt is
        port (
            GT_REFCLK0_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
            GT_REFCLK0_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
            GT_REFCLK1_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
            GT_REFCLK1_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
            GT_Serial_grx_n : in STD_LOGIC_VECTOR ( 3 downto 0 );
            GT_Serial_grx_p : in STD_LOGIC_VECTOR ( 3 downto 0 );
            GT_Serial_gtx_n : out STD_LOGIC_VECTOR ( 3 downto 0 );
            GT_Serial_gtx_p : out STD_LOGIC_VECTOR ( 3 downto 0 );
            APB3_INTF_0_paddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
            APB3_INTF_0_penable : in STD_LOGIC;
            APB3_INTF_0_prdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
            APB3_INTF_0_pready : out STD_LOGIC;
            APB3_INTF_0_psel : in STD_LOGIC;
            APB3_INTF_0_pslverr : out STD_LOGIC;
            APB3_INTF_0_pwdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
            APB3_INTF_0_pwrite : in STD_LOGIC;
            apb3clk_gt_bridge_ip_0 : in STD_LOGIC;
            apb3clk_quad : in STD_LOGIC;
            ch0_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch0_rxbyteisaligned_ext_0 : out STD_LOGIC;
            ch0_rxcdrlock_ext_0 : out STD_LOGIC;
            ch0_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
            ch0_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxgearboxslip_ext_0 : in STD_LOGIC;
            ch0_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch0_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxpmaresetdone_ext_0 : out STD_LOGIC;
            ch0_rxpolarity_ext_0 : in STD_LOGIC;
            ch0_rxresetdone_ext_0 : out STD_LOGIC;
            ch0_rxslide_ext_0 : in STD_LOGIC;
            ch0_rxvalid_ext_0 : out STD_LOGIC;
            ch0_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
            ch0_txheader_ext_0 : in STD_LOGIC_VECTOR ( 5 downto 0 );
            ch0_txpolarity_ext_0 : in STD_LOGIC;
            ch0_txresetdone_ext_0 : out STD_LOGIC;
            ch0_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch1_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch1_rxbyteisaligned_ext_0 : out STD_LOGIC;
            ch1_rxcdrlock_ext_0 : out STD_LOGIC;
            ch1_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
            ch1_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxgearboxslip_ext_0 : in STD_LOGIC;
            ch1_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch1_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxpmaresetdone_ext_0 : out STD_LOGIC;
            ch1_rxpolarity_ext_0 : in STD_LOGIC;
            ch1_rxresetdone_ext_0 : out STD_LOGIC;
            ch1_rxslide_ext_0 : in STD_LOGIC;
            ch1_rxvalid_ext_0 : out STD_LOGIC;
            ch1_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
            ch1_txpolarity_ext_0 : in STD_LOGIC;
            ch1_txresetdone_ext_0 : out STD_LOGIC;
            ch1_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch2_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch2_rxbyteisaligned_ext_0 : out STD_LOGIC;
            ch2_rxcdrlock_ext_0 : out STD_LOGIC;
            ch2_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
            ch2_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxgearboxslip_ext_0 : in STD_LOGIC;
            ch2_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch2_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxpmaresetdone_ext_0 : out STD_LOGIC;
            ch2_rxpolarity_ext_0 : in STD_LOGIC;
            ch2_rxresetdone_ext_0 : out STD_LOGIC;
            ch2_rxslide_ext_0 : in STD_LOGIC;
            ch2_rxvalid_ext_0 : out STD_LOGIC;
            ch2_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
            ch2_txpolarity_ext_0 : in STD_LOGIC;
            ch2_txresetdone_ext_0 : out STD_LOGIC;
            ch2_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch3_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch3_rxbyteisaligned_ext_0 : out STD_LOGIC;
            ch3_rxcdrlock_ext_0 : out STD_LOGIC;
            ch3_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
            ch3_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxgearboxslip_ext_0 : in STD_LOGIC;
            ch3_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch3_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxpmaresetdone_ext_0 : out STD_LOGIC;
            ch3_rxpolarity_ext_0 : in STD_LOGIC;
            ch3_rxresetdone_ext_0 : out STD_LOGIC;
            ch3_rxslide_ext_0 : in STD_LOGIC;
            ch3_rxvalid_ext_0 : out STD_LOGIC;
            ch3_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
            ch3_txpolarity_ext_0 : in STD_LOGIC;
            ch3_txresetdone_ext_0 : out STD_LOGIC;
            ch3_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
            gt_reset_gt_bridge_ip_0 : in STD_LOGIC;
            lcpll_lock_gt_bridge_ip_0 : out STD_LOGIC;
            link_status_gt_bridge_ip_0 : out STD_LOGIC;
            rate_sel_gt_bridge_ip_0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
            reset_rx_datapath_in_0 : in STD_LOGIC;
            reset_rx_pll_and_datapath_in_0 : in STD_LOGIC;
            reset_tx_datapath_in_0 : in STD_LOGIC;
            reset_tx_pll_and_datapath_in_0 : in STD_LOGIC;
            rpll_lock_gt_bridge_ip_0 : out STD_LOGIC;
            rx_resetdone_out_gt_bridge_ip_0 : out STD_LOGIC;
            rxusrclk_gt_bridge_ip_0 : out STD_LOGIC;
            tx_resetdone_out_gt_bridge_ip_0 : out STD_LOGIC;
            txusrclk_gt_bridge_ip_0 : out STD_LOGIC;
            ch0_txpippmen_ext_0 : in STD_LOGIC;
            ch0_txpippmstepsize_ext_0 : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_txbufstatus_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_txpippmen_ext_0 : in STD_LOGIC;
            ch1_txpippmstepsize_ext_0 : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_txbufstatus_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_txpippmen_ext_0 : in STD_LOGIC;
            ch2_txpippmstepsize_ext_0 : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_txbufstatus_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_txpippmen_ext_0 : in STD_LOGIC;
            ch3_txpippmstepsize_ext_0 : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_txbufstatus_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
            apb3presetn_0 : in STD_LOGIC;
            apb3presetn_1 : in STD_LOGIC
        );
    end component transceiver_versal_lpgbt;
begin
    transceiver_versal_lpgbt_i: component transceiver_versal_lpgbt
        port map (
            APB3_INTF_0_paddr(15 downto 0) => APB3_INTF_0_paddr(15 downto 0),
            APB3_INTF_0_penable => APB3_INTF_0_penable,
            APB3_INTF_0_prdata(31 downto 0) => APB3_INTF_0_prdata(31 downto 0),
            APB3_INTF_0_pready => APB3_INTF_0_pready,
            APB3_INTF_0_psel => APB3_INTF_0_psel,
            APB3_INTF_0_pslverr => APB3_INTF_0_pslverr,
            APB3_INTF_0_pwdata(31 downto 0) => APB3_INTF_0_pwdata(31 downto 0),
            APB3_INTF_0_pwrite => APB3_INTF_0_pwrite,
            GT_REFCLK0_clk_n(0) => GT_REFCLK0_clk_n(0),
            GT_REFCLK0_clk_p(0) => GT_REFCLK0_clk_p(0),
            GT_REFCLK1_clk_n(0) => GT_REFCLK1_clk_n(0),
            GT_REFCLK1_clk_p(0) => GT_REFCLK1_clk_p(0),
            GT_Serial_grx_n(3 downto 0) => GT_Serial_grx_n(3 downto 0),
            GT_Serial_grx_p(3 downto 0) => GT_Serial_grx_p(3 downto 0),
            GT_Serial_gtx_n(3 downto 0) => GT_Serial_gtx_n(3 downto 0),
            GT_Serial_gtx_p(3 downto 0) => GT_Serial_gtx_p(3 downto 0),
            apb3clk_gt_bridge_ip_0 => apb3clk_gt_bridge_ip_0,
            apb3clk_quad => apb3clk_quad,
            apb3presetn_0 => apb3presetn_0,
            apb3presetn_1 => apb3presetn_1,
            ch0_loopback_0(2 downto 0) => ch0_loopback_0(2 downto 0),
            ch0_rxbyteisaligned_ext_0 => ch0_rxbyteisaligned_ext_0,
            ch0_rxcdrlock_ext_0 => ch0_rxcdrlock_ext_0,
            ch0_rxctrl0_ext_0(15 downto 0) => ch0_rxctrl0_ext_0(15 downto 0),
            ch0_rxctrl1_ext_0(15 downto 0) => ch0_rxctrl1_ext_0(15 downto 0),
            ch0_rxctrl2_ext_0(7 downto 0) => ch0_rxctrl2_ext_0(7 downto 0),
            ch0_rxctrl3_ext_0(7 downto 0) => ch0_rxctrl3_ext_0(7 downto 0),
            ch0_rxdata_ext_0(127 downto 0) => ch0_rxdata_ext_0(127 downto 0),
            ch0_rxdatavalid_ext_0(1 downto 0) => ch0_rxdatavalid_ext_0(1 downto 0),
            ch0_rxgearboxslip_ext_0 => ch0_rxgearboxslip_ext_0,
            ch0_rxheader_ext_0(5 downto 0) => ch0_rxheader_ext_0(5 downto 0),
            ch0_rxheadervalid_ext_0(1 downto 0) => ch0_rxheadervalid_ext_0(1 downto 0),
            ch0_rxpmaresetdone_ext_0 => ch0_rxpmaresetdone_ext_0,
            ch0_rxpolarity_ext_0 => ch0_rxpolarity_ext_0,
            ch0_rxresetdone_ext_0 => ch0_rxresetdone_ext_0,
            ch0_rxslide_ext_0 => ch0_rxslide_ext_0,
            ch0_rxvalid_ext_0 => ch0_rxvalid_ext_0,
            ch0_txbufstatus_ext_0(1 downto 0) => ch0_txbufstatus_ext_0(1 downto 0),
            ch0_txctrl0_ext_0(15 downto 0) => ch0_txctrl0_ext_0(15 downto 0),
            ch0_txctrl1_ext_0(15 downto 0) => ch0_txctrl1_ext_0(15 downto 0),
            ch0_txctrl2_ext_0(7 downto 0) => ch0_txctrl2_ext_0(7 downto 0),
            ch0_txdata_ext_0(127 downto 0) => ch0_txdata_ext_0(127 downto 0),
            ch0_txheader_ext_0(5 downto 0) => ch0_txheader_ext_0(5 downto 0),
            ch0_txpippmen_ext_0 => ch0_txpippmen_ext_0,
            ch0_txpippmstepsize_ext_0(4 downto 0) => ch0_txpippmstepsize_ext_0(4 downto 0),
            ch0_txpolarity_ext_0 => ch0_txpolarity_ext_0,
            ch0_txresetdone_ext_0 => ch0_txresetdone_ext_0,
            ch0_txsequence_ext_0(6 downto 0) => ch0_txsequence_ext_0(6 downto 0),
            ch1_loopback_0(2 downto 0) => ch1_loopback_0(2 downto 0),
            ch1_rxbyteisaligned_ext_0 => ch1_rxbyteisaligned_ext_0,
            ch1_rxcdrlock_ext_0 => ch1_rxcdrlock_ext_0,
            ch1_rxctrl0_ext_0(15 downto 0) => ch1_rxctrl0_ext_0(15 downto 0),
            ch1_rxctrl1_ext_0(15 downto 0) => ch1_rxctrl1_ext_0(15 downto 0),
            ch1_rxctrl2_ext_0(7 downto 0) => ch1_rxctrl2_ext_0(7 downto 0),
            ch1_rxctrl3_ext_0(7 downto 0) => ch1_rxctrl3_ext_0(7 downto 0),
            ch1_rxdata_ext_0(127 downto 0) => ch1_rxdata_ext_0(127 downto 0),
            ch1_rxdatavalid_ext_0(1 downto 0) => ch1_rxdatavalid_ext_0(1 downto 0),
            ch1_rxgearboxslip_ext_0 => ch1_rxgearboxslip_ext_0,
            ch1_rxheader_ext_0(5 downto 0) => ch1_rxheader_ext_0(5 downto 0),
            ch1_rxheadervalid_ext_0(1 downto 0) => ch1_rxheadervalid_ext_0(1 downto 0),
            ch1_rxpmaresetdone_ext_0 => ch1_rxpmaresetdone_ext_0,
            ch1_rxpolarity_ext_0 => ch1_rxpolarity_ext_0,
            ch1_rxresetdone_ext_0 => ch1_rxresetdone_ext_0,
            ch1_rxslide_ext_0 => ch1_rxslide_ext_0,
            ch1_rxvalid_ext_0 => ch1_rxvalid_ext_0,
            ch1_txbufstatus_ext_0(1 downto 0) => ch1_txbufstatus_ext_0(1 downto 0),
            ch1_txctrl0_ext_0(15 downto 0) => ch1_txctrl0_ext_0(15 downto 0),
            ch1_txctrl1_ext_0(15 downto 0) => ch1_txctrl1_ext_0(15 downto 0),
            ch1_txctrl2_ext_0(7 downto 0) => ch1_txctrl2_ext_0(7 downto 0),
            ch1_txdata_ext_0(127 downto 0) => ch1_txdata_ext_0(127 downto 0),
            ch1_txpippmen_ext_0 => ch1_txpippmen_ext_0,
            ch1_txpippmstepsize_ext_0(4 downto 0) => ch1_txpippmstepsize_ext_0(4 downto 0),
            ch1_txpolarity_ext_0 => ch1_txpolarity_ext_0,
            ch1_txresetdone_ext_0 => ch1_txresetdone_ext_0,
            ch1_txsequence_ext_0(6 downto 0) => ch1_txsequence_ext_0(6 downto 0),
            ch2_loopback_0(2 downto 0) => ch2_loopback_0(2 downto 0),
            ch2_rxbyteisaligned_ext_0 => ch2_rxbyteisaligned_ext_0,
            ch2_rxcdrlock_ext_0 => ch2_rxcdrlock_ext_0,
            ch2_rxctrl0_ext_0(15 downto 0) => ch2_rxctrl0_ext_0(15 downto 0),
            ch2_rxctrl1_ext_0(15 downto 0) => ch2_rxctrl1_ext_0(15 downto 0),
            ch2_rxctrl2_ext_0(7 downto 0) => ch2_rxctrl2_ext_0(7 downto 0),
            ch2_rxctrl3_ext_0(7 downto 0) => ch2_rxctrl3_ext_0(7 downto 0),
            ch2_rxdata_ext_0(127 downto 0) => ch2_rxdata_ext_0(127 downto 0),
            ch2_rxdatavalid_ext_0(1 downto 0) => ch2_rxdatavalid_ext_0(1 downto 0),
            ch2_rxgearboxslip_ext_0 => ch2_rxgearboxslip_ext_0,
            ch2_rxheader_ext_0(5 downto 0) => ch2_rxheader_ext_0(5 downto 0),
            ch2_rxheadervalid_ext_0(1 downto 0) => ch2_rxheadervalid_ext_0(1 downto 0),
            ch2_rxpmaresetdone_ext_0 => ch2_rxpmaresetdone_ext_0,
            ch2_rxpolarity_ext_0 => ch2_rxpolarity_ext_0,
            ch2_rxresetdone_ext_0 => ch2_rxresetdone_ext_0,
            ch2_rxslide_ext_0 => ch2_rxslide_ext_0,
            ch2_rxvalid_ext_0 => ch2_rxvalid_ext_0,
            ch2_txbufstatus_ext_0(1 downto 0) => ch2_txbufstatus_ext_0(1 downto 0),
            ch2_txctrl0_ext_0(15 downto 0) => ch2_txctrl0_ext_0(15 downto 0),
            ch2_txctrl1_ext_0(15 downto 0) => ch2_txctrl1_ext_0(15 downto 0),
            ch2_txctrl2_ext_0(7 downto 0) => ch2_txctrl2_ext_0(7 downto 0),
            ch2_txdata_ext_0(127 downto 0) => ch2_txdata_ext_0(127 downto 0),
            ch2_txpippmen_ext_0 => ch2_txpippmen_ext_0,
            ch2_txpippmstepsize_ext_0(4 downto 0) => ch2_txpippmstepsize_ext_0(4 downto 0),
            ch2_txpolarity_ext_0 => ch2_txpolarity_ext_0,
            ch2_txresetdone_ext_0 => ch2_txresetdone_ext_0,
            ch2_txsequence_ext_0(6 downto 0) => ch2_txsequence_ext_0(6 downto 0),
            ch3_loopback_0(2 downto 0) => ch3_loopback_0(2 downto 0),
            ch3_rxbyteisaligned_ext_0 => ch3_rxbyteisaligned_ext_0,
            ch3_rxcdrlock_ext_0 => ch3_rxcdrlock_ext_0,
            ch3_rxctrl0_ext_0(15 downto 0) => ch3_rxctrl0_ext_0(15 downto 0),
            ch3_rxctrl1_ext_0(15 downto 0) => ch3_rxctrl1_ext_0(15 downto 0),
            ch3_rxctrl2_ext_0(7 downto 0) => ch3_rxctrl2_ext_0(7 downto 0),
            ch3_rxctrl3_ext_0(7 downto 0) => ch3_rxctrl3_ext_0(7 downto 0),
            ch3_rxdata_ext_0(127 downto 0) => ch3_rxdata_ext_0(127 downto 0),
            ch3_rxdatavalid_ext_0(1 downto 0) => ch3_rxdatavalid_ext_0(1 downto 0),
            ch3_rxgearboxslip_ext_0 => ch3_rxgearboxslip_ext_0,
            ch3_rxheader_ext_0(5 downto 0) => ch3_rxheader_ext_0(5 downto 0),
            ch3_rxheadervalid_ext_0(1 downto 0) => ch3_rxheadervalid_ext_0(1 downto 0),
            ch3_rxpmaresetdone_ext_0 => ch3_rxpmaresetdone_ext_0,
            ch3_rxpolarity_ext_0 => ch3_rxpolarity_ext_0,
            ch3_rxresetdone_ext_0 => ch3_rxresetdone_ext_0,
            ch3_rxslide_ext_0 => ch3_rxslide_ext_0,
            ch3_rxvalid_ext_0 => ch3_rxvalid_ext_0,
            ch3_txbufstatus_ext_0(1 downto 0) => ch3_txbufstatus_ext_0(1 downto 0),
            ch3_txctrl0_ext_0(15 downto 0) => ch3_txctrl0_ext_0(15 downto 0),
            ch3_txctrl1_ext_0(15 downto 0) => ch3_txctrl1_ext_0(15 downto 0),
            ch3_txctrl2_ext_0(7 downto 0) => ch3_txctrl2_ext_0(7 downto 0),
            ch3_txdata_ext_0(127 downto 0) => ch3_txdata_ext_0(127 downto 0),
            ch3_txpippmen_ext_0 => ch3_txpippmen_ext_0,
            ch3_txpippmstepsize_ext_0(4 downto 0) => ch3_txpippmstepsize_ext_0(4 downto 0),
            ch3_txpolarity_ext_0 => ch3_txpolarity_ext_0,
            ch3_txresetdone_ext_0 => ch3_txresetdone_ext_0,
            ch3_txsequence_ext_0(6 downto 0) => ch3_txsequence_ext_0(6 downto 0),
            gt_reset_gt_bridge_ip_0 => gt_reset_gt_bridge_ip_0,
            lcpll_lock_gt_bridge_ip_0 => lcpll_lock_gt_bridge_ip_0,
            link_status_gt_bridge_ip_0 => link_status_gt_bridge_ip_0,
            rate_sel_gt_bridge_ip_0(3 downto 0) => rate_sel_gt_bridge_ip_0(3 downto 0),
            reset_rx_datapath_in_0 => reset_rx_datapath_in_0,
            reset_rx_pll_and_datapath_in_0 => reset_rx_pll_and_datapath_in_0,
            reset_tx_datapath_in_0 => reset_tx_datapath_in_0,
            reset_tx_pll_and_datapath_in_0 => reset_tx_pll_and_datapath_in_0,
            rpll_lock_gt_bridge_ip_0 => rpll_lock_gt_bridge_ip_0,
            rx_resetdone_out_gt_bridge_ip_0 => rx_resetdone_out_gt_bridge_ip_0,
            rxusrclk_gt_bridge_ip_0 => rxusrclk_gt_bridge_ip_0,
            tx_resetdone_out_gt_bridge_ip_0 => tx_resetdone_out_gt_bridge_ip_0,
            txusrclk_gt_bridge_ip_0 => txusrclk_gt_bridge_ip_0
        );
end STRUCTURE;

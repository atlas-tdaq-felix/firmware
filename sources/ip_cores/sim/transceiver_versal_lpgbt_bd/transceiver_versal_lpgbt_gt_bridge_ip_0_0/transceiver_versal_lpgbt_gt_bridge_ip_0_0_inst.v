

//------------------------------------------------------------------------------
//  (c) Copyright 2017-2018 Xilinx, Inc. All rights reserved.
//
//  This file contains confidential and proprietary information
//  of Xilinx, Inc. and is protected under U.S. and
//  international copyright and other intellectual property
//  laws.
//
//  DISCLAIMER
//  This disclaimer is not a license and does not grant any
//  rights to the materials distributed herewith. Except as
//  otherwise provided in a valid license issued to you by
//  Xilinx, and to the maximum extent permitted by applicable
//  law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
//  WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
//  AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
//  BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
//  INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
//  (2) Xilinx shall not be liable (whether in contract or tort,
//  including negligence, or under any other theory of
//  liability) for any loss or damage of any kind or nature
//  related to, arising under or in connection with these
//  materials, including for any direct, or any indirect,
//  special, incidental, or consequential loss or damage
//  (including loss of data, profits, goodwill, or any type of
//  loss or damage suffered as a result of any action brought
//  by a third party) even if such damage or loss was
//  reasonably foreseeable or Xilinx had been advised of the
//  possibility of the same.
//
//  CRITICAL APPLICATIONS
//  Xilinx products are not designed or intended to be fail-
//  safe, or for use in any application requiring fail-safe
//  performance, such as life-support or safety devices or
//  systems, Class III medical devices, nuclear facilities,
//  applications related to the deployment of airbags, or any
//  other applications that could lead to death, personal
//  injury, or severe property or environmental damage
//  (individually and collectively, "Critical
//  Applications"). Customer assumes the sole risk and
//  liability of any use of Xilinx products in Critical
//  Applications, subject only to applicable laws and
//  regulations governing limitations on product liability.
//
//  THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
//  PART OF THIS FILE AT ALL TIMES.
//------------------------------------------------------------------------------

`timescale 1ns / 1ps
module transceiver_versal_lpgbt_gt_bridge_ip_0_0_inst # (
    parameter BYPASS_MODE        = 0,
    parameter EGW_IS_PARENT_IP   = 1,
    parameter IP_LR0_ENABLE      = 1,
    parameter IP_LR0_SETTINGS    = " ",
    parameter IP_LR10_ENABLE     = 0,
    parameter IP_LR10_SETTINGS   = " ",
    parameter IP_LR11_ENABLE     = 0,
    parameter IP_LR11_SETTINGS   = " ",
    parameter IP_LR12_ENABLE     = 0,
    parameter IP_LR12_SETTINGS   = " ",
    parameter IP_LR13_ENABLE     = 0,
    parameter IP_LR13_SETTINGS   = " ",
    parameter IP_LR14_ENABLE     = 0,
    parameter IP_LR14_SETTINGS   = " ",
    parameter IP_LR15_ENABLE     = 0,
    parameter IP_LR15_SETTINGS   = " ",
    parameter IP_LR1_ENABLE      = 0,
    parameter IP_LR1_SETTINGS    = " ",
    parameter IP_LR2_ENABLE      = 0,
    parameter IP_LR2_SETTINGS    = " ",
    parameter IP_LR3_ENABLE      = 0,
    parameter IP_LR3_SETTINGS    = " ",
    parameter IP_LR4_ENABLE      = 0,
    parameter IP_LR4_SETTINGS    = " ",
    parameter IP_LR5_ENABLE      = 0,
    parameter IP_LR5_SETTINGS    = " ",
    parameter IP_LR6_ENABLE      = 0,
    parameter IP_LR6_SETTINGS    = " ",
    parameter IP_LR7_ENABLE      = 0,
    parameter IP_LR7_SETTINGS    = " ",
    parameter IP_LR8_ENABLE      = 0,
    parameter IP_LR8_SETTINGS    = " ",
    parameter IP_LR9_ENABLE      = 0,
    parameter IP_LR9_SETTINGS    = " ",
    parameter IP_MLR_ENABLE      = 1,
    parameter IP_MULTI_LR        = 0,
    parameter IP_NO_OF_LANES     = 4,
    parameter IP_NO_OF_RX_LANES  = 0,
    parameter IP_NO_OF_TX_LANES  = 0,
    parameter IP_PRESET          = "start_from_scratch",                                
    parameter IP_RX_MASTERCLK_SRC  = "RX0",                                        
    parameter IP_SETTINGS          = " ",
    parameter IP_NO_OF_LR          = 0,
    parameter IP_GT_DIRECTION  = "DUPLEX",                                        
    parameter IP_TX_MASTERCLK_SRC  = "TX0"
  )

(

       output [127:0] ch0_txdata,
       output [5:0]   ch0_txheader,
       output [6:0]   ch0_txsequence,
       output         ch0_gttxreset,
       output         ch0_txprogdivreset,
       output         ch0_txuserrdy,
       output         ch0_txcominit,
       output         ch0_txcomsas,
       output         ch0_txcomwake,
       output         ch0_txdapicodeovrden,
       output         ch0_txdapicodereset,
       output         ch0_txdetectrx,
       output         ch0_txdlyalignreq,
       output         ch0_txelecidle,
       output         ch0_txinhibit,
       output         ch0_txmldchaindone,
       output         ch0_txmldchainreq,
       output         ch0_txoneszeros,
       output         ch0_txpausedelayalign,
       output         ch0_txpcsresetmask ,
       output         ch0_txphalignreq,
       output [1:0]   ch0_txphalignresetmask,
       output         ch0_txphdlypd,
       output         ch0_txphdlyreset,
       output         ch0_txphsetinitreq,
       output         ch0_txphshift180,
       output         ch0_txpicodeovrden,
       output         ch0_txpicodereset,
       output         ch0_txpippmen,
       output         ch0_txpisopd,
       output         ch0_txpolarity,
       output         ch0_txprbsforceerr,
       output         ch0_txswing,
       output         ch0_txsyncallin,
       input          ch0_tx10gstat,
       input          ch0_txcomfinish,
       input          ch0_txdccdone,
       input          ch0_txdlyalignerr,
       input          ch0_txdlyalignprog,
       input          ch0_txphaligndone,
       input          ch0_txphalignerr,
       input          ch0_txphalignoutrsvd,
       input          ch0_txphdlyresetdone,
       input          ch0_txphsetinitdone,
       input          ch0_txphshift180done,
       input          ch0_txsyncdone,
       input   [1:0]  ch0_txbufstatus,
       output  [15:0] ch0_txctrl0,
       output  [15:0] ch0_txctrl1,
       output  [1:0]  ch0_txdeemph,
       output  [1:0]  ch0_txpd,
       output  [1:0]  ch0_txresetmode,
       output         ch0_txmstreset,
       output         ch0_txmstdatapathreset,
       input          ch0_txmstresetdone,
       output  [2:0]  ch0_txmargin,
       output  [2:0]  ch0_txpmaresetmask,
       output  [3:0]  ch0_txprbssel,
       output  [4:0]  ch0_txdiffctrl,
       output  [4:0]  ch0_txpippmstepsize,
       output  [4:0]  ch0_txpostcursor,
       output  [4:0]  ch0_txprecursor,
       output  [6:0]  ch0_txmaincursor,
       output  [7:0]  ch0_txctrl2,
       output  [7:0]  ch0_txdataextendrsvd,
       output  [7:0]  ch0_txrate,
       input          ch0_txresetdone,
       input          ch0_txprogdivresetdone,
       input          ch0_txpmaresetdone,

       input [127:0]   ch0_txdata_ext,
       input [5:0]     ch0_txheader_ext,
       input [6:0]     ch0_txsequence_ext,
       input           ch0_txcominit_ext,
       input           ch0_txcomsas_ext,
       input           ch0_txcomwake_ext,
       input           ch0_txdapicodeovrden_ext,
       input           ch0_txdapicodereset_ext,
       input           ch0_txdetectrx_ext,
       input           ch0_txdlyalignreq_ext,
       input           ch0_txelecidle_ext,
       input           ch0_txinhibit_ext,
       input           ch0_txmldchaindone_ext,
       input           ch0_txmldchainreq_ext,
       input           ch0_txoneszeros_ext,
       input           ch0_txpausedelayalign_ext,
       input           ch0_txpcsresetmask_ext,
       input           ch0_txphalignreq_ext,
       input [1:0]     ch0_txphalignresetmask_ext,
       input           ch0_txphdlypd_ext,
       input           ch0_txphdlyreset_ext,
       input           ch0_txphsetinitreq_ext,
       input           ch0_txphshift180_ext,
       input           ch0_txpicodeovrden_ext,
       input           ch0_txpicodereset_ext,
       input           ch0_txpippmen_ext,
       input           ch0_txpisopd_ext,
       input           ch0_txpolarity_ext,
       input           ch0_txprbsforceerr_ext,
       input           ch0_txswing_ext,
       input           ch0_txsyncallin_ext,
       output          ch0_tx10gstat_ext,
       output          ch0_txcomfinish_ext,
       output          ch0_txdccdone_ext,
       output          ch0_txdlyalignerr_ext,
       output          ch0_txdlyalignprog_ext,
       output          ch0_txphaligndone_ext,
       output          ch0_txphalignerr_ext,
       output          ch0_txphalignoutrsvd_ext,
       output          ch0_txphdlyresetdone_ext,
       output          ch0_txphsetinitdone_ext,
       output          ch0_txphshift180done_ext,
       output          ch0_txsyncdone_ext,
       output   [1:0]  ch0_txbufstatus_ext,
       input  [15:0]   ch0_txctrl0_ext,
       input  [15:0]   ch0_txctrl1_ext,
       input  [1:0]    ch0_txdeemph_ext,
       input  [1:0]    ch0_txpd_ext,
       input  [1:0]    ch0_txresetmode_ext,
       output          ch0_txmstresetdone_ext,
       input  [2:0]    ch0_txmargin_ext,
       input  [2:0]    ch0_txpmaresetmask_ext,
       input  [3:0]    ch0_txprbssel_ext,
       input  [4:0]    ch0_txdiffctrl_ext,
       input  [4:0]    ch0_txpippmstepsize_ext,
       input  [4:0]    ch0_txpostcursor_ext,
       input  [4:0]    ch0_txprecursor_ext,
       input  [6:0]    ch0_txmaincursor_ext,
       input  [7:0]    ch0_txctrl2_ext,
       input  [7:0]    ch0_txdataextendrsvd_ext,
       output          ch0_txresetdone_ext,
       output          ch0_txprogdivresetdone_ext,
       output          ch0_txpmaresetdone_ext,

       output [127:0] ch1_txdata,
       output [5:0]   ch1_txheader,
       output [6:0]   ch1_txsequence,
       output         ch1_gttxreset,
       output         ch1_txprogdivreset,
       output         ch1_txuserrdy,
       output         ch1_txcominit,
       output         ch1_txcomsas,
       output         ch1_txcomwake,
       output         ch1_txdapicodeovrden,
       output         ch1_txdapicodereset,
       output         ch1_txdetectrx,
       output         ch1_txdlyalignreq,
       output         ch1_txelecidle,
       output         ch1_txinhibit,
       output         ch1_txmldchaindone,
       output         ch1_txmldchainreq,
       output         ch1_txoneszeros,
       output         ch1_txpausedelayalign,
       output         ch1_txpcsresetmask ,
       output         ch1_txphalignreq,
       output [1:0]   ch1_txphalignresetmask,
       output         ch1_txphdlypd,
       output         ch1_txphdlyreset,
       output         ch1_txphsetinitreq,
       output         ch1_txphshift180,
       output         ch1_txpicodeovrden,
       output         ch1_txpicodereset,
       output         ch1_txpippmen,
       output         ch1_txpisopd,
       output         ch1_txpolarity,
       output         ch1_txprbsforceerr,
       output         ch1_txswing,
       output         ch1_txsyncallin,
       input          ch1_tx10gstat,
       input          ch1_txcomfinish,
       input          ch1_txdccdone,
       input          ch1_txdlyalignerr,
       input          ch1_txdlyalignprog,
       input          ch1_txphaligndone,
       input          ch1_txphalignerr,
       input          ch1_txphalignoutrsvd,
       input          ch1_txphdlyresetdone,
       input          ch1_txphsetinitdone,
       input          ch1_txphshift180done,
       input          ch1_txsyncdone,
       input   [1:0]  ch1_txbufstatus,
       output  [15:0] ch1_txctrl0,
       output  [15:0] ch1_txctrl1,
       output  [1:0]  ch1_txdeemph,
       output  [1:0]  ch1_txpd,
       output  [1:0]  ch1_txresetmode,
       output         ch1_txmstreset,
       output         ch1_txmstdatapathreset,
       input          ch1_txmstresetdone,
       output  [2:0]  ch1_txmargin,
       output  [2:0]  ch1_txpmaresetmask,
       output  [3:0]  ch1_txprbssel,
       output  [4:0]  ch1_txdiffctrl,
       output  [4:0]  ch1_txpippmstepsize,
       output  [4:0]  ch1_txpostcursor,
       output  [4:0]  ch1_txprecursor,
       output  [6:0]  ch1_txmaincursor,
       output  [7:0]  ch1_txctrl2,
       output  [7:0]  ch1_txdataextendrsvd,
       output  [7:0]  ch1_txrate,
       input          ch1_txresetdone,
       input          ch1_txprogdivresetdone,
       input          ch1_txpmaresetdone,

       input [127:0]   ch1_txdata_ext,
       input [5:0]     ch1_txheader_ext,
       input [6:0]     ch1_txsequence_ext,
       input           ch1_txcominit_ext,
       input           ch1_txcomsas_ext,
       input           ch1_txcomwake_ext,
       input           ch1_txdapicodeovrden_ext,
       input           ch1_txdapicodereset_ext,
       input           ch1_txdetectrx_ext,
       input           ch1_txdlyalignreq_ext,
       input           ch1_txelecidle_ext,
       input           ch1_txinhibit_ext,
       input           ch1_txmldchaindone_ext,
       input           ch1_txmldchainreq_ext,
       input           ch1_txoneszeros_ext,
       input           ch1_txpausedelayalign_ext,
       input           ch1_txpcsresetmask_ext,
       input           ch1_txphalignreq_ext,
       input [1:0]     ch1_txphalignresetmask_ext,
       input           ch1_txphdlypd_ext,
       input           ch1_txphdlyreset_ext,
       input           ch1_txphsetinitreq_ext,
       input           ch1_txphshift180_ext,
       input           ch1_txpicodeovrden_ext,
       input           ch1_txpicodereset_ext,
       input           ch1_txpippmen_ext,
       input           ch1_txpisopd_ext,
       input           ch1_txpolarity_ext,
       input           ch1_txprbsforceerr_ext,
       input           ch1_txswing_ext,
       input           ch1_txsyncallin_ext,
       output          ch1_tx10gstat_ext,
       output          ch1_txcomfinish_ext,
       output          ch1_txdccdone_ext,
       output          ch1_txdlyalignerr_ext,
       output          ch1_txdlyalignprog_ext,
       output          ch1_txphaligndone_ext,
       output          ch1_txphalignerr_ext,
       output          ch1_txphalignoutrsvd_ext,
       output          ch1_txphdlyresetdone_ext,
       output          ch1_txphsetinitdone_ext,
       output          ch1_txphshift180done_ext,
       output          ch1_txsyncdone_ext,
       output   [1:0]  ch1_txbufstatus_ext,
       input  [15:0]   ch1_txctrl0_ext,
       input  [15:0]   ch1_txctrl1_ext,
       input  [1:0]    ch1_txdeemph_ext,
       input  [1:0]    ch1_txpd_ext,
       input  [1:0]    ch1_txresetmode_ext,
       output          ch1_txmstresetdone_ext,
       input  [2:0]    ch1_txmargin_ext,
       input  [2:0]    ch1_txpmaresetmask_ext,
       input  [3:0]    ch1_txprbssel_ext,
       input  [4:0]    ch1_txdiffctrl_ext,
       input  [4:0]    ch1_txpippmstepsize_ext,
       input  [4:0]    ch1_txpostcursor_ext,
       input  [4:0]    ch1_txprecursor_ext,
       input  [6:0]    ch1_txmaincursor_ext,
       input  [7:0]    ch1_txctrl2_ext,
       input  [7:0]    ch1_txdataextendrsvd_ext,
       output          ch1_txresetdone_ext,
       output          ch1_txprogdivresetdone_ext,
       output          ch1_txpmaresetdone_ext,

       output [127:0] ch2_txdata,
       output [5:0]   ch2_txheader,
       output [6:0]   ch2_txsequence,
       output         ch2_gttxreset,
       output         ch2_txprogdivreset,
       output         ch2_txuserrdy,
       output         ch2_txcominit,
       output         ch2_txcomsas,
       output         ch2_txcomwake,
       output         ch2_txdapicodeovrden,
       output         ch2_txdapicodereset,
       output         ch2_txdetectrx,
       output         ch2_txdlyalignreq,
       output         ch2_txelecidle,
       output         ch2_txinhibit,
       output         ch2_txmldchaindone,
       output         ch2_txmldchainreq,
       output         ch2_txoneszeros,
       output         ch2_txpausedelayalign,
       output         ch2_txpcsresetmask ,
       output         ch2_txphalignreq,
       output [1:0]   ch2_txphalignresetmask,
       output         ch2_txphdlypd,
       output         ch2_txphdlyreset,
       output         ch2_txphsetinitreq,
       output         ch2_txphshift180,
       output         ch2_txpicodeovrden,
       output         ch2_txpicodereset,
       output         ch2_txpippmen,
       output         ch2_txpisopd,
       output         ch2_txpolarity,
       output         ch2_txprbsforceerr,
       output         ch2_txswing,
       output         ch2_txsyncallin,
       input          ch2_tx10gstat,
       input          ch2_txcomfinish,
       input          ch2_txdccdone,
       input          ch2_txdlyalignerr,
       input          ch2_txdlyalignprog,
       input          ch2_txphaligndone,
       input          ch2_txphalignerr,
       input          ch2_txphalignoutrsvd,
       input          ch2_txphdlyresetdone,
       input          ch2_txphsetinitdone,
       input          ch2_txphshift180done,
       input          ch2_txsyncdone,
       input   [1:0]  ch2_txbufstatus,
       output  [15:0] ch2_txctrl0,
       output  [15:0] ch2_txctrl1,
       output  [1:0]  ch2_txdeemph,
       output  [1:0]  ch2_txpd,
       output  [1:0]  ch2_txresetmode,
       output         ch2_txmstreset,
       output         ch2_txmstdatapathreset,
       input          ch2_txmstresetdone,
       output  [2:0]  ch2_txmargin,
       output  [2:0]  ch2_txpmaresetmask,
       output  [3:0]  ch2_txprbssel,
       output  [4:0]  ch2_txdiffctrl,
       output  [4:0]  ch2_txpippmstepsize,
       output  [4:0]  ch2_txpostcursor,
       output  [4:0]  ch2_txprecursor,
       output  [6:0]  ch2_txmaincursor,
       output  [7:0]  ch2_txctrl2,
       output  [7:0]  ch2_txdataextendrsvd,
       output  [7:0]  ch2_txrate,
       input          ch2_txresetdone,
       input          ch2_txprogdivresetdone,
       input          ch2_txpmaresetdone,

       input [127:0]   ch2_txdata_ext,
       input [5:0]     ch2_txheader_ext,
       input [6:0]     ch2_txsequence_ext,
       input           ch2_txcominit_ext,
       input           ch2_txcomsas_ext,
       input           ch2_txcomwake_ext,
       input           ch2_txdapicodeovrden_ext,
       input           ch2_txdapicodereset_ext,
       input           ch2_txdetectrx_ext,
       input           ch2_txdlyalignreq_ext,
       input           ch2_txelecidle_ext,
       input           ch2_txinhibit_ext,
       input           ch2_txmldchaindone_ext,
       input           ch2_txmldchainreq_ext,
       input           ch2_txoneszeros_ext,
       input           ch2_txpausedelayalign_ext,
       input           ch2_txpcsresetmask_ext,
       input           ch2_txphalignreq_ext,
       input [1:0]     ch2_txphalignresetmask_ext,
       input           ch2_txphdlypd_ext,
       input           ch2_txphdlyreset_ext,
       input           ch2_txphsetinitreq_ext,
       input           ch2_txphshift180_ext,
       input           ch2_txpicodeovrden_ext,
       input           ch2_txpicodereset_ext,
       input           ch2_txpippmen_ext,
       input           ch2_txpisopd_ext,
       input           ch2_txpolarity_ext,
       input           ch2_txprbsforceerr_ext,
       input           ch2_txswing_ext,
       input           ch2_txsyncallin_ext,
       output          ch2_tx10gstat_ext,
       output          ch2_txcomfinish_ext,
       output          ch2_txdccdone_ext,
       output          ch2_txdlyalignerr_ext,
       output          ch2_txdlyalignprog_ext,
       output          ch2_txphaligndone_ext,
       output          ch2_txphalignerr_ext,
       output          ch2_txphalignoutrsvd_ext,
       output          ch2_txphdlyresetdone_ext,
       output          ch2_txphsetinitdone_ext,
       output          ch2_txphshift180done_ext,
       output          ch2_txsyncdone_ext,
       output   [1:0]  ch2_txbufstatus_ext,
       input  [15:0]   ch2_txctrl0_ext,
       input  [15:0]   ch2_txctrl1_ext,
       input  [1:0]    ch2_txdeemph_ext,
       input  [1:0]    ch2_txpd_ext,
       input  [1:0]    ch2_txresetmode_ext,
       output          ch2_txmstresetdone_ext,
       input  [2:0]    ch2_txmargin_ext,
       input  [2:0]    ch2_txpmaresetmask_ext,
       input  [3:0]    ch2_txprbssel_ext,
       input  [4:0]    ch2_txdiffctrl_ext,
       input  [4:0]    ch2_txpippmstepsize_ext,
       input  [4:0]    ch2_txpostcursor_ext,
       input  [4:0]    ch2_txprecursor_ext,
       input  [6:0]    ch2_txmaincursor_ext,
       input  [7:0]    ch2_txctrl2_ext,
       input  [7:0]    ch2_txdataextendrsvd_ext,
       output          ch2_txresetdone_ext,
       output          ch2_txprogdivresetdone_ext,
       output          ch2_txpmaresetdone_ext,

       output [127:0] ch3_txdata,
       output [5:0]   ch3_txheader,
       output [6:0]   ch3_txsequence,
       output         ch3_gttxreset,
       output         ch3_txprogdivreset,
       output         ch3_txuserrdy,
       output         ch3_txcominit,
       output         ch3_txcomsas,
       output         ch3_txcomwake,
       output         ch3_txdapicodeovrden,
       output         ch3_txdapicodereset,
       output         ch3_txdetectrx,
       output         ch3_txdlyalignreq,
       output         ch3_txelecidle,
       output         ch3_txinhibit,
       output         ch3_txmldchaindone,
       output         ch3_txmldchainreq,
       output         ch3_txoneszeros,
       output         ch3_txpausedelayalign,
       output         ch3_txpcsresetmask ,
       output         ch3_txphalignreq,
       output [1:0]   ch3_txphalignresetmask,
       output         ch3_txphdlypd,
       output         ch3_txphdlyreset,
       output         ch3_txphsetinitreq,
       output         ch3_txphshift180,
       output         ch3_txpicodeovrden,
       output         ch3_txpicodereset,
       output         ch3_txpippmen,
       output         ch3_txpisopd,
       output         ch3_txpolarity,
       output         ch3_txprbsforceerr,
       output         ch3_txswing,
       output         ch3_txsyncallin,
       input          ch3_tx10gstat,
       input          ch3_txcomfinish,
       input          ch3_txdccdone,
       input          ch3_txdlyalignerr,
       input          ch3_txdlyalignprog,
       input          ch3_txphaligndone,
       input          ch3_txphalignerr,
       input          ch3_txphalignoutrsvd,
       input          ch3_txphdlyresetdone,
       input          ch3_txphsetinitdone,
       input          ch3_txphshift180done,
       input          ch3_txsyncdone,
       input   [1:0]  ch3_txbufstatus,
       output  [15:0] ch3_txctrl0,
       output  [15:0] ch3_txctrl1,
       output  [1:0]  ch3_txdeemph,
       output  [1:0]  ch3_txpd,
       output  [1:0]  ch3_txresetmode,
       output         ch3_txmstreset,
       output         ch3_txmstdatapathreset,
       input          ch3_txmstresetdone,
       output  [2:0]  ch3_txmargin,
       output  [2:0]  ch3_txpmaresetmask,
       output  [3:0]  ch3_txprbssel,
       output  [4:0]  ch3_txdiffctrl,
       output  [4:0]  ch3_txpippmstepsize,
       output  [4:0]  ch3_txpostcursor,
       output  [4:0]  ch3_txprecursor,
       output  [6:0]  ch3_txmaincursor,
       output  [7:0]  ch3_txctrl2,
       output  [7:0]  ch3_txdataextendrsvd,
       output  [7:0]  ch3_txrate,
       input          ch3_txresetdone,
       input          ch3_txprogdivresetdone,
       input          ch3_txpmaresetdone,

       input [127:0]   ch3_txdata_ext,
       input [5:0]     ch3_txheader_ext,
       input [6:0]     ch3_txsequence_ext,
       input           ch3_txcominit_ext,
       input           ch3_txcomsas_ext,
       input           ch3_txcomwake_ext,
       input           ch3_txdapicodeovrden_ext,
       input           ch3_txdapicodereset_ext,
       input           ch3_txdetectrx_ext,
       input           ch3_txdlyalignreq_ext,
       input           ch3_txelecidle_ext,
       input           ch3_txinhibit_ext,
       input           ch3_txmldchaindone_ext,
       input           ch3_txmldchainreq_ext,
       input           ch3_txoneszeros_ext,
       input           ch3_txpausedelayalign_ext,
       input           ch3_txpcsresetmask_ext,
       input           ch3_txphalignreq_ext,
       input [1:0]     ch3_txphalignresetmask_ext,
       input           ch3_txphdlypd_ext,
       input           ch3_txphdlyreset_ext,
       input           ch3_txphsetinitreq_ext,
       input           ch3_txphshift180_ext,
       input           ch3_txpicodeovrden_ext,
       input           ch3_txpicodereset_ext,
       input           ch3_txpippmen_ext,
       input           ch3_txpisopd_ext,
       input           ch3_txpolarity_ext,
       input           ch3_txprbsforceerr_ext,
       input           ch3_txswing_ext,
       input           ch3_txsyncallin_ext,
       output          ch3_tx10gstat_ext,
       output          ch3_txcomfinish_ext,
       output          ch3_txdccdone_ext,
       output          ch3_txdlyalignerr_ext,
       output          ch3_txdlyalignprog_ext,
       output          ch3_txphaligndone_ext,
       output          ch3_txphalignerr_ext,
       output          ch3_txphalignoutrsvd_ext,
       output          ch3_txphdlyresetdone_ext,
       output          ch3_txphsetinitdone_ext,
       output          ch3_txphshift180done_ext,
       output          ch3_txsyncdone_ext,
       output   [1:0]  ch3_txbufstatus_ext,
       input  [15:0]   ch3_txctrl0_ext,
       input  [15:0]   ch3_txctrl1_ext,
       input  [1:0]    ch3_txdeemph_ext,
       input  [1:0]    ch3_txpd_ext,
       input  [1:0]    ch3_txresetmode_ext,
       output          ch3_txmstresetdone_ext,
       input  [2:0]    ch3_txmargin_ext,
       input  [2:0]    ch3_txpmaresetmask_ext,
       input  [3:0]    ch3_txprbssel_ext,
       input  [4:0]    ch3_txdiffctrl_ext,
       input  [4:0]    ch3_txpippmstepsize_ext,
       input  [4:0]    ch3_txpostcursor_ext,
       input  [4:0]    ch3_txprecursor_ext,
       input  [6:0]    ch3_txmaincursor_ext,
       input  [7:0]    ch3_txctrl2_ext,
       input  [7:0]    ch3_txdataextendrsvd_ext,
       output          ch3_txresetdone_ext,
       output          ch3_txprogdivresetdone_ext,
       output          ch3_txpmaresetdone_ext,


       input [127:0]   ch0_rxdata,
       input [1:0]     ch0_rxdatavalid,
       input [5:0]     ch0_rxheader,
       output          ch0_rxgearboxslip,
       output          ch0_gtrxreset,
       output          ch0_rxprogdivreset,
       output          ch0_rxuserrdy,
       input           ch0_rxbyteisaligned,
       input           ch0_rxbyterealign,
       input           ch0_rxcdrlock,
       input           ch0_rxcdrphdone,
       input           ch0_rxchanbondseq,
       input           ch0_rxchanisaligned,
       input           ch0_rxchanrealign,
       input           ch0_rxcominitdet,
       input           ch0_rxcommadet,
       input           ch0_rxcomsasdet,
       input           ch0_rxcomwakedet,
       input           ch0_rxdccdone,
       input           ch0_rxdlyalignerr,
       input           ch0_rxdlyalignprog,
       input           ch0_rxelecidle,
       input           ch0_rxfinealigndone,
       input           ch0_rxosintdone,
       input           ch0_rxosintstarted,
       input           ch0_rxosintstrobedone,
       input           ch0_rxosintstrobestarted,
       input           ch0_rxphaligndone,
       input           ch0_rxphalignerr,
       input           ch0_rxphdlyresetdone,
       input           ch0_rxphsetinitdone,
       input           ch0_rxphshift180done,
       input           ch0_rxprbserr,
       input           ch0_rxprbslocked,
       input           ch0_rxsliderdy,
       input           ch0_rxsyncdone,
       input           ch0_rxvalid,
       input [15:0]    ch0_rxctrl0,
       input [15:0]    ch0_rxctrl1,
       input [1:0]     ch0_rxclkcorcnt,
       input [1:0]     ch0_rxheadervalid,
       input [1:0]     ch0_rxstartofseq,
       input [2:0]     ch0_rxbufstatus,
       input [2:0]     ch0_rxstatus,
       input [4:0]     ch0_rxchbondo,
       input [7:0]     ch0_rx10gstat,
       input [7:0]     ch0_rxctrl2,
       input [7:0]     ch0_rxctrl3,
       input [7:0]     ch0_rxdataextendrsvd,
       output          ch0_rxcdrhold ,
       output          ch0_rxcdrovrden ,
       output          ch0_rxcdrreset ,
       output          ch0_rxdapicodeovrden ,
       output          ch0_rxdapicodereset ,
       output          ch0_rxdlyalignreq ,
       output          ch0_rxeqtraining ,
       output          ch0_rxlpmen ,
       output          ch0_rxmldchaindone ,
       output          ch0_rxmldchainreq ,
       output          ch0_rxmlfinealignreq ,
       output          ch0_rxoobreset ,
       output          ch0_rxphalignreq ,
       output          ch0_rxphdlypd ,
       output          ch0_rxphdlyreset ,
       output          ch0_rxphsetinitreq ,
       output          ch0_rxphshift180 ,
       output          ch0_rxpolarity ,
       output          ch0_rxprbscntreset ,
       output          ch0_rxslide ,
       output          ch0_rxsyncallin ,
       output          ch0_rxtermination ,
       output [1:0]    ch0_rxpd ,
       output [1:0]    ch0_rxphalignresetmask ,
       output [1:0]    ch0_rxresetmode ,
       output          ch0_rxmstreset,
       output          ch0_rxmstdatapathreset,
       input           ch0_rxmstresetdone,
       output [3:0]    ch0_rxprbssel ,
       output [4:0]    ch0_rxchbondi ,
       output [4:0]    ch0_rxpcsresetmask ,
       output [6:0]    ch0_rxpmaresetmask ,
       output [7:0]    ch0_rxrate ,
       input           ch0_rxprogdivresetdone,
       input           ch0_rxpmaresetdone,
       input           ch0_rxresetdone,

        output         ch0_cdrbmcdrreq,
        output         ch0_cdrfreqos, 
        output         ch0_cdrincpctrl, 
        output         ch0_cdrstepdir, 
        output         ch0_cdrstepsq, 
        output         ch0_cdrstepsx, 
        output         ch0_cfokovrdfinish, 
        output         ch0_cfokovrdpulse, 
        output         ch0_cfokovrdstart, 
        output         ch0_eyescanreset, 
        output         ch0_eyescantrigger, 
        input          ch0_eyescandataerror, 
        input          ch0_cfokovrdrdy0, 
        input          ch0_cfokovrdrdy1, 


       output [127:0]  ch0_rxdata_ext,
       output [1:0]    ch0_rxdatavalid_ext,
       output [5:0]    ch0_rxheader_ext,
       input           ch0_rxgearboxslip_ext,
       output          ch0_rxbyteisaligned_ext,
       output          ch0_rxbyterealign_ext,
       output          ch0_rxcdrlock_ext,
       output          ch0_rxcdrphdone_ext,
       output          ch0_rxchanbondseq_ext,
       output          ch0_rxchanisaligned_ext,
       output          ch0_rxchanrealign_ext,
       output          ch0_rxcominitdet_ext,
       output          ch0_rxcommadet_ext,
       output          ch0_rxcomsasdet_ext,
       output          ch0_rxcomwakedet_ext,
       output          ch0_rxdccdone_ext,
       output          ch0_rxdlyalignerr_ext,
       output          ch0_rxdlyalignprog_ext,
       output          ch0_rxelecidle_ext,
       output          ch0_rxfinealigndone_ext,
       output          ch0_rxosintdone_ext,
       output          ch0_rxosintstarted_ext,
       output          ch0_rxosintstrobedone_ext,
       output          ch0_rxosintstrobestarted_ext,
       output          ch0_rxphaligndone_ext,
       output          ch0_rxphalignerr_ext,
       output          ch0_rxphdlyresetdone_ext,
       output          ch0_rxphsetinitdone_ext,
       output          ch0_rxphshift180done_ext,
       output          ch0_rxprbserr_ext,
       output          ch0_rxprbslocked_ext,
       output          ch0_rxsliderdy_ext,
       output          ch0_rxsyncdone_ext,
       output          ch0_rxvalid_ext,
       output [15:0]   ch0_rxctrl0_ext,
       output [15:0]   ch0_rxctrl1_ext,
       output [1:0]    ch0_rxclkcorcnt_ext,
       output [1:0]    ch0_rxheadervalid_ext,
       output [1:0]    ch0_rxstartofseq_ext,
       output [2:0]    ch0_rxbufstatus_ext,
       output [2:0]    ch0_rxstatus_ext,
       output [4:0]    ch0_rxchbondo_ext,
       output [7:0]    ch0_rx10gstat_ext,
       output [7:0]    ch0_rxctrl2_ext,
       output [7:0]    ch0_rxctrl3_ext,
       output [7:0]    ch0_rxdataextendrsvd_ext,
       input           ch0_rxcdrhold_ext,
       input           ch0_rxcdrovrden_ext,
       input           ch0_rxcdrreset_ext,
       input           ch0_rxdapicodeovrden_ext,
       input           ch0_rxdapicodereset_ext,
       input           ch0_rxdlyalignreq_ext,
       input           ch0_rxeqtraining_ext,
       input           ch0_rxlpmen_ext,
       input           ch0_rxmldchaindone_ext,
       input           ch0_rxmldchainreq_ext,
       input           ch0_rxmlfinealignreq_ext,
       input           ch0_rxoobreset_ext,
       input           ch0_rxphalignreq_ext,
       input           ch0_rxphdlypd_ext,
       input           ch0_rxphdlyreset_ext,
       input           ch0_rxphsetinitreq_ext,
       input           ch0_rxphshift180_ext,
       input           ch0_rxpolarity_ext,
       input           ch0_rxprbscntreset_ext,
       input           ch0_rxslide_ext,
       input           ch0_rxsyncallin_ext,
       input           ch0_rxtermination_ext,
       input [1:0]     ch0_rxpd_ext,
       input [1:0]     ch0_rxphalignresetmask_ext,
       input [1:0]     ch0_rxresetmode_ext,
       output          ch0_rxmstresetdone_ext,
       input [3:0]     ch0_rxprbssel_ext,
       input [4:0]     ch0_rxchbondi_ext,
       input [4:0]     ch0_rxpcsresetmask_ext,
       input [6:0]     ch0_rxpmaresetmask_ext,
       output          ch0_rxprogdivresetdone_ext,
       output          ch0_rxpmaresetdone_ext,
       output          ch0_rxresetdone_ext,
        input          ch0_cdrbmcdrreq_ext,
        input          ch0_cdrfreqos_ext, 
        input          ch0_cdrincpctrl_ext, 
        input          ch0_cdrstepdir_ext, 
        input          ch0_cdrstepsq_ext, 
        input          ch0_cdrstepsx_ext, 
        input          ch0_cfokovrdfinish_ext, 
        input          ch0_cfokovrdpulse_ext, 
        input          ch0_cfokovrdstart_ext, 
        input          ch0_eyescanreset_ext, 
        input          ch0_eyescantrigger_ext, 
        output         ch0_eyescandataerror_ext, 
        output         ch0_cfokovrdrdy0_ext, 
        output         ch0_cfokovrdrdy1_ext, 


       input [127:0]   ch1_rxdata,
       input [1:0]     ch1_rxdatavalid,
       input [5:0]     ch1_rxheader,
       output          ch1_rxgearboxslip,
       output          ch1_gtrxreset,
       output          ch1_rxprogdivreset,
       output          ch1_rxuserrdy,
       input           ch1_rxbyteisaligned,
       input           ch1_rxbyterealign,
       input           ch1_rxcdrlock,
       input           ch1_rxcdrphdone,
       input           ch1_rxchanbondseq,
       input           ch1_rxchanisaligned,
       input           ch1_rxchanrealign,
       input           ch1_rxcominitdet,
       input           ch1_rxcommadet,
       input           ch1_rxcomsasdet,
       input           ch1_rxcomwakedet,
       input           ch1_rxdccdone,
       input           ch1_rxdlyalignerr,
       input           ch1_rxdlyalignprog,
       input           ch1_rxelecidle,
       input           ch1_rxfinealigndone,
       input           ch1_rxosintdone,
       input           ch1_rxosintstarted,
       input           ch1_rxosintstrobedone,
       input           ch1_rxosintstrobestarted,
       input           ch1_rxphaligndone,
       input           ch1_rxphalignerr,
       input           ch1_rxphdlyresetdone,
       input           ch1_rxphsetinitdone,
       input           ch1_rxphshift180done,
       input           ch1_rxprbserr,
       input           ch1_rxprbslocked,
       input           ch1_rxsliderdy,
       input           ch1_rxsyncdone,
       input           ch1_rxvalid,
       input [15:0]    ch1_rxctrl0,
       input [15:0]    ch1_rxctrl1,
       input [1:0]     ch1_rxclkcorcnt,
       input [1:0]     ch1_rxheadervalid,
       input [1:0]     ch1_rxstartofseq,
       input [2:0]     ch1_rxbufstatus,
       input [2:0]     ch1_rxstatus,
       input [4:0]     ch1_rxchbondo,
       input [7:0]     ch1_rx10gstat,
       input [7:0]     ch1_rxctrl2,
       input [7:0]     ch1_rxctrl3,
       input [7:0]     ch1_rxdataextendrsvd,
       output          ch1_rxcdrhold ,
       output          ch1_rxcdrovrden ,
       output          ch1_rxcdrreset ,
       output          ch1_rxdapicodeovrden ,
       output          ch1_rxdapicodereset ,
       output          ch1_rxdlyalignreq ,
       output          ch1_rxeqtraining ,
       output          ch1_rxlpmen ,
       output          ch1_rxmldchaindone ,
       output          ch1_rxmldchainreq ,
       output          ch1_rxmlfinealignreq ,
       output          ch1_rxoobreset ,
       output          ch1_rxphalignreq ,
       output          ch1_rxphdlypd ,
       output          ch1_rxphdlyreset ,
       output          ch1_rxphsetinitreq ,
       output          ch1_rxphshift180 ,
       output          ch1_rxpolarity ,
       output          ch1_rxprbscntreset ,
       output          ch1_rxslide ,
       output          ch1_rxsyncallin ,
       output          ch1_rxtermination ,
       output [1:0]    ch1_rxpd ,
       output [1:0]    ch1_rxphalignresetmask ,
       output [1:0]    ch1_rxresetmode ,
       output          ch1_rxmstreset,
       output          ch1_rxmstdatapathreset,
       input           ch1_rxmstresetdone,
       output [3:0]    ch1_rxprbssel ,
       output [4:0]    ch1_rxchbondi ,
       output [4:0]    ch1_rxpcsresetmask ,
       output [6:0]    ch1_rxpmaresetmask ,
       output [7:0]    ch1_rxrate ,
       input           ch1_rxprogdivresetdone,
       input           ch1_rxpmaresetdone,
       input           ch1_rxresetdone,

        output         ch1_cdrbmcdrreq,
        output         ch1_cdrfreqos, 
        output         ch1_cdrincpctrl, 
        output         ch1_cdrstepdir, 
        output         ch1_cdrstepsq, 
        output         ch1_cdrstepsx, 
        output         ch1_cfokovrdfinish, 
        output         ch1_cfokovrdpulse, 
        output         ch1_cfokovrdstart, 
        output         ch1_eyescanreset, 
        output         ch1_eyescantrigger, 
        input          ch1_eyescandataerror, 
        input          ch1_cfokovrdrdy0, 
        input          ch1_cfokovrdrdy1, 


       output [127:0]  ch1_rxdata_ext,
       output [1:0]    ch1_rxdatavalid_ext,
       output [5:0]    ch1_rxheader_ext,
       input           ch1_rxgearboxslip_ext,
       output          ch1_rxbyteisaligned_ext,
       output          ch1_rxbyterealign_ext,
       output          ch1_rxcdrlock_ext,
       output          ch1_rxcdrphdone_ext,
       output          ch1_rxchanbondseq_ext,
       output          ch1_rxchanisaligned_ext,
       output          ch1_rxchanrealign_ext,
       output          ch1_rxcominitdet_ext,
       output          ch1_rxcommadet_ext,
       output          ch1_rxcomsasdet_ext,
       output          ch1_rxcomwakedet_ext,
       output          ch1_rxdccdone_ext,
       output          ch1_rxdlyalignerr_ext,
       output          ch1_rxdlyalignprog_ext,
       output          ch1_rxelecidle_ext,
       output          ch1_rxfinealigndone_ext,
       output          ch1_rxosintdone_ext,
       output          ch1_rxosintstarted_ext,
       output          ch1_rxosintstrobedone_ext,
       output          ch1_rxosintstrobestarted_ext,
       output          ch1_rxphaligndone_ext,
       output          ch1_rxphalignerr_ext,
       output          ch1_rxphdlyresetdone_ext,
       output          ch1_rxphsetinitdone_ext,
       output          ch1_rxphshift180done_ext,
       output          ch1_rxprbserr_ext,
       output          ch1_rxprbslocked_ext,
       output          ch1_rxsliderdy_ext,
       output          ch1_rxsyncdone_ext,
       output          ch1_rxvalid_ext,
       output [15:0]   ch1_rxctrl0_ext,
       output [15:0]   ch1_rxctrl1_ext,
       output [1:0]    ch1_rxclkcorcnt_ext,
       output [1:0]    ch1_rxheadervalid_ext,
       output [1:0]    ch1_rxstartofseq_ext,
       output [2:0]    ch1_rxbufstatus_ext,
       output [2:0]    ch1_rxstatus_ext,
       output [4:0]    ch1_rxchbondo_ext,
       output [7:0]    ch1_rx10gstat_ext,
       output [7:0]    ch1_rxctrl2_ext,
       output [7:0]    ch1_rxctrl3_ext,
       output [7:0]    ch1_rxdataextendrsvd_ext,
       input           ch1_rxcdrhold_ext,
       input           ch1_rxcdrovrden_ext,
       input           ch1_rxcdrreset_ext,
       input           ch1_rxdapicodeovrden_ext,
       input           ch1_rxdapicodereset_ext,
       input           ch1_rxdlyalignreq_ext,
       input           ch1_rxeqtraining_ext,
       input           ch1_rxlpmen_ext,
       input           ch1_rxmldchaindone_ext,
       input           ch1_rxmldchainreq_ext,
       input           ch1_rxmlfinealignreq_ext,
       input           ch1_rxoobreset_ext,
       input           ch1_rxphalignreq_ext,
       input           ch1_rxphdlypd_ext,
       input           ch1_rxphdlyreset_ext,
       input           ch1_rxphsetinitreq_ext,
       input           ch1_rxphshift180_ext,
       input           ch1_rxpolarity_ext,
       input           ch1_rxprbscntreset_ext,
       input           ch1_rxslide_ext,
       input           ch1_rxsyncallin_ext,
       input           ch1_rxtermination_ext,
       input [1:0]     ch1_rxpd_ext,
       input [1:0]     ch1_rxphalignresetmask_ext,
       input [1:0]     ch1_rxresetmode_ext,
       output          ch1_rxmstresetdone_ext,
       input [3:0]     ch1_rxprbssel_ext,
       input [4:0]     ch1_rxchbondi_ext,
       input [4:0]     ch1_rxpcsresetmask_ext,
       input [6:0]     ch1_rxpmaresetmask_ext,
       output          ch1_rxprogdivresetdone_ext,
       output          ch1_rxpmaresetdone_ext,
       output          ch1_rxresetdone_ext,
        input          ch1_cdrbmcdrreq_ext,
        input          ch1_cdrfreqos_ext, 
        input          ch1_cdrincpctrl_ext, 
        input          ch1_cdrstepdir_ext, 
        input          ch1_cdrstepsq_ext, 
        input          ch1_cdrstepsx_ext, 
        input          ch1_cfokovrdfinish_ext, 
        input          ch1_cfokovrdpulse_ext, 
        input          ch1_cfokovrdstart_ext, 
        input          ch1_eyescanreset_ext, 
        input          ch1_eyescantrigger_ext, 
        output         ch1_eyescandataerror_ext, 
        output         ch1_cfokovrdrdy0_ext, 
        output         ch1_cfokovrdrdy1_ext, 


       input [127:0]   ch2_rxdata,
       input [1:0]     ch2_rxdatavalid,
       input [5:0]     ch2_rxheader,
       output          ch2_rxgearboxslip,
       output          ch2_gtrxreset,
       output          ch2_rxprogdivreset,
       output          ch2_rxuserrdy,
       input           ch2_rxbyteisaligned,
       input           ch2_rxbyterealign,
       input           ch2_rxcdrlock,
       input           ch2_rxcdrphdone,
       input           ch2_rxchanbondseq,
       input           ch2_rxchanisaligned,
       input           ch2_rxchanrealign,
       input           ch2_rxcominitdet,
       input           ch2_rxcommadet,
       input           ch2_rxcomsasdet,
       input           ch2_rxcomwakedet,
       input           ch2_rxdccdone,
       input           ch2_rxdlyalignerr,
       input           ch2_rxdlyalignprog,
       input           ch2_rxelecidle,
       input           ch2_rxfinealigndone,
       input           ch2_rxosintdone,
       input           ch2_rxosintstarted,
       input           ch2_rxosintstrobedone,
       input           ch2_rxosintstrobestarted,
       input           ch2_rxphaligndone,
       input           ch2_rxphalignerr,
       input           ch2_rxphdlyresetdone,
       input           ch2_rxphsetinitdone,
       input           ch2_rxphshift180done,
       input           ch2_rxprbserr,
       input           ch2_rxprbslocked,
       input           ch2_rxsliderdy,
       input           ch2_rxsyncdone,
       input           ch2_rxvalid,
       input [15:0]    ch2_rxctrl0,
       input [15:0]    ch2_rxctrl1,
       input [1:0]     ch2_rxclkcorcnt,
       input [1:0]     ch2_rxheadervalid,
       input [1:0]     ch2_rxstartofseq,
       input [2:0]     ch2_rxbufstatus,
       input [2:0]     ch2_rxstatus,
       input [4:0]     ch2_rxchbondo,
       input [7:0]     ch2_rx10gstat,
       input [7:0]     ch2_rxctrl2,
       input [7:0]     ch2_rxctrl3,
       input [7:0]     ch2_rxdataextendrsvd,
       output          ch2_rxcdrhold ,
       output          ch2_rxcdrovrden ,
       output          ch2_rxcdrreset ,
       output          ch2_rxdapicodeovrden ,
       output          ch2_rxdapicodereset ,
       output          ch2_rxdlyalignreq ,
       output          ch2_rxeqtraining ,
       output          ch2_rxlpmen ,
       output          ch2_rxmldchaindone ,
       output          ch2_rxmldchainreq ,
       output          ch2_rxmlfinealignreq ,
       output          ch2_rxoobreset ,
       output          ch2_rxphalignreq ,
       output          ch2_rxphdlypd ,
       output          ch2_rxphdlyreset ,
       output          ch2_rxphsetinitreq ,
       output          ch2_rxphshift180 ,
       output          ch2_rxpolarity ,
       output          ch2_rxprbscntreset ,
       output          ch2_rxslide ,
       output          ch2_rxsyncallin ,
       output          ch2_rxtermination ,
       output [1:0]    ch2_rxpd ,
       output [1:0]    ch2_rxphalignresetmask ,
       output [1:0]    ch2_rxresetmode ,
       output          ch2_rxmstreset,
       output          ch2_rxmstdatapathreset,
       input           ch2_rxmstresetdone,
       output [3:0]    ch2_rxprbssel ,
       output [4:0]    ch2_rxchbondi ,
       output [4:0]    ch2_rxpcsresetmask ,
       output [6:0]    ch2_rxpmaresetmask ,
       output [7:0]    ch2_rxrate ,
       input           ch2_rxprogdivresetdone,
       input           ch2_rxpmaresetdone,
       input           ch2_rxresetdone,

        output         ch2_cdrbmcdrreq,
        output         ch2_cdrfreqos, 
        output         ch2_cdrincpctrl, 
        output         ch2_cdrstepdir, 
        output         ch2_cdrstepsq, 
        output         ch2_cdrstepsx, 
        output         ch2_cfokovrdfinish, 
        output         ch2_cfokovrdpulse, 
        output         ch2_cfokovrdstart, 
        output         ch2_eyescanreset, 
        output         ch2_eyescantrigger, 
        input          ch2_eyescandataerror, 
        input          ch2_cfokovrdrdy0, 
        input          ch2_cfokovrdrdy1, 


       output [127:0]  ch2_rxdata_ext,
       output [1:0]    ch2_rxdatavalid_ext,
       output [5:0]    ch2_rxheader_ext,
       input           ch2_rxgearboxslip_ext,
       output          ch2_rxbyteisaligned_ext,
       output          ch2_rxbyterealign_ext,
       output          ch2_rxcdrlock_ext,
       output          ch2_rxcdrphdone_ext,
       output          ch2_rxchanbondseq_ext,
       output          ch2_rxchanisaligned_ext,
       output          ch2_rxchanrealign_ext,
       output          ch2_rxcominitdet_ext,
       output          ch2_rxcommadet_ext,
       output          ch2_rxcomsasdet_ext,
       output          ch2_rxcomwakedet_ext,
       output          ch2_rxdccdone_ext,
       output          ch2_rxdlyalignerr_ext,
       output          ch2_rxdlyalignprog_ext,
       output          ch2_rxelecidle_ext,
       output          ch2_rxfinealigndone_ext,
       output          ch2_rxosintdone_ext,
       output          ch2_rxosintstarted_ext,
       output          ch2_rxosintstrobedone_ext,
       output          ch2_rxosintstrobestarted_ext,
       output          ch2_rxphaligndone_ext,
       output          ch2_rxphalignerr_ext,
       output          ch2_rxphdlyresetdone_ext,
       output          ch2_rxphsetinitdone_ext,
       output          ch2_rxphshift180done_ext,
       output          ch2_rxprbserr_ext,
       output          ch2_rxprbslocked_ext,
       output          ch2_rxsliderdy_ext,
       output          ch2_rxsyncdone_ext,
       output          ch2_rxvalid_ext,
       output [15:0]   ch2_rxctrl0_ext,
       output [15:0]   ch2_rxctrl1_ext,
       output [1:0]    ch2_rxclkcorcnt_ext,
       output [1:0]    ch2_rxheadervalid_ext,
       output [1:0]    ch2_rxstartofseq_ext,
       output [2:0]    ch2_rxbufstatus_ext,
       output [2:0]    ch2_rxstatus_ext,
       output [4:0]    ch2_rxchbondo_ext,
       output [7:0]    ch2_rx10gstat_ext,
       output [7:0]    ch2_rxctrl2_ext,
       output [7:0]    ch2_rxctrl3_ext,
       output [7:0]    ch2_rxdataextendrsvd_ext,
       input           ch2_rxcdrhold_ext,
       input           ch2_rxcdrovrden_ext,
       input           ch2_rxcdrreset_ext,
       input           ch2_rxdapicodeovrden_ext,
       input           ch2_rxdapicodereset_ext,
       input           ch2_rxdlyalignreq_ext,
       input           ch2_rxeqtraining_ext,
       input           ch2_rxlpmen_ext,
       input           ch2_rxmldchaindone_ext,
       input           ch2_rxmldchainreq_ext,
       input           ch2_rxmlfinealignreq_ext,
       input           ch2_rxoobreset_ext,
       input           ch2_rxphalignreq_ext,
       input           ch2_rxphdlypd_ext,
       input           ch2_rxphdlyreset_ext,
       input           ch2_rxphsetinitreq_ext,
       input           ch2_rxphshift180_ext,
       input           ch2_rxpolarity_ext,
       input           ch2_rxprbscntreset_ext,
       input           ch2_rxslide_ext,
       input           ch2_rxsyncallin_ext,
       input           ch2_rxtermination_ext,
       input [1:0]     ch2_rxpd_ext,
       input [1:0]     ch2_rxphalignresetmask_ext,
       input [1:0]     ch2_rxresetmode_ext,
       output          ch2_rxmstresetdone_ext,
       input [3:0]     ch2_rxprbssel_ext,
       input [4:0]     ch2_rxchbondi_ext,
       input [4:0]     ch2_rxpcsresetmask_ext,
       input [6:0]     ch2_rxpmaresetmask_ext,
       output          ch2_rxprogdivresetdone_ext,
       output          ch2_rxpmaresetdone_ext,
       output          ch2_rxresetdone_ext,
        input          ch2_cdrbmcdrreq_ext,
        input          ch2_cdrfreqos_ext, 
        input          ch2_cdrincpctrl_ext, 
        input          ch2_cdrstepdir_ext, 
        input          ch2_cdrstepsq_ext, 
        input          ch2_cdrstepsx_ext, 
        input          ch2_cfokovrdfinish_ext, 
        input          ch2_cfokovrdpulse_ext, 
        input          ch2_cfokovrdstart_ext, 
        input          ch2_eyescanreset_ext, 
        input          ch2_eyescantrigger_ext, 
        output         ch2_eyescandataerror_ext, 
        output         ch2_cfokovrdrdy0_ext, 
        output         ch2_cfokovrdrdy1_ext, 


       input [127:0]   ch3_rxdata,
       input [1:0]     ch3_rxdatavalid,
       input [5:0]     ch3_rxheader,
       output          ch3_rxgearboxslip,
       output          ch3_gtrxreset,
       output          ch3_rxprogdivreset,
       output          ch3_rxuserrdy,
       input           ch3_rxbyteisaligned,
       input           ch3_rxbyterealign,
       input           ch3_rxcdrlock,
       input           ch3_rxcdrphdone,
       input           ch3_rxchanbondseq,
       input           ch3_rxchanisaligned,
       input           ch3_rxchanrealign,
       input           ch3_rxcominitdet,
       input           ch3_rxcommadet,
       input           ch3_rxcomsasdet,
       input           ch3_rxcomwakedet,
       input           ch3_rxdccdone,
       input           ch3_rxdlyalignerr,
       input           ch3_rxdlyalignprog,
       input           ch3_rxelecidle,
       input           ch3_rxfinealigndone,
       input           ch3_rxosintdone,
       input           ch3_rxosintstarted,
       input           ch3_rxosintstrobedone,
       input           ch3_rxosintstrobestarted,
       input           ch3_rxphaligndone,
       input           ch3_rxphalignerr,
       input           ch3_rxphdlyresetdone,
       input           ch3_rxphsetinitdone,
       input           ch3_rxphshift180done,
       input           ch3_rxprbserr,
       input           ch3_rxprbslocked,
       input           ch3_rxsliderdy,
       input           ch3_rxsyncdone,
       input           ch3_rxvalid,
       input [15:0]    ch3_rxctrl0,
       input [15:0]    ch3_rxctrl1,
       input [1:0]     ch3_rxclkcorcnt,
       input [1:0]     ch3_rxheadervalid,
       input [1:0]     ch3_rxstartofseq,
       input [2:0]     ch3_rxbufstatus,
       input [2:0]     ch3_rxstatus,
       input [4:0]     ch3_rxchbondo,
       input [7:0]     ch3_rx10gstat,
       input [7:0]     ch3_rxctrl2,
       input [7:0]     ch3_rxctrl3,
       input [7:0]     ch3_rxdataextendrsvd,
       output          ch3_rxcdrhold ,
       output          ch3_rxcdrovrden ,
       output          ch3_rxcdrreset ,
       output          ch3_rxdapicodeovrden ,
       output          ch3_rxdapicodereset ,
       output          ch3_rxdlyalignreq ,
       output          ch3_rxeqtraining ,
       output          ch3_rxlpmen ,
       output          ch3_rxmldchaindone ,
       output          ch3_rxmldchainreq ,
       output          ch3_rxmlfinealignreq ,
       output          ch3_rxoobreset ,
       output          ch3_rxphalignreq ,
       output          ch3_rxphdlypd ,
       output          ch3_rxphdlyreset ,
       output          ch3_rxphsetinitreq ,
       output          ch3_rxphshift180 ,
       output          ch3_rxpolarity ,
       output          ch3_rxprbscntreset ,
       output          ch3_rxslide ,
       output          ch3_rxsyncallin ,
       output          ch3_rxtermination ,
       output [1:0]    ch3_rxpd ,
       output [1:0]    ch3_rxphalignresetmask ,
       output [1:0]    ch3_rxresetmode ,
       output          ch3_rxmstreset,
       output          ch3_rxmstdatapathreset,
       input           ch3_rxmstresetdone,
       output [3:0]    ch3_rxprbssel ,
       output [4:0]    ch3_rxchbondi ,
       output [4:0]    ch3_rxpcsresetmask ,
       output [6:0]    ch3_rxpmaresetmask ,
       output [7:0]    ch3_rxrate ,
       input           ch3_rxprogdivresetdone,
       input           ch3_rxpmaresetdone,
       input           ch3_rxresetdone,

        output         ch3_cdrbmcdrreq,
        output         ch3_cdrfreqos, 
        output         ch3_cdrincpctrl, 
        output         ch3_cdrstepdir, 
        output         ch3_cdrstepsq, 
        output         ch3_cdrstepsx, 
        output         ch3_cfokovrdfinish, 
        output         ch3_cfokovrdpulse, 
        output         ch3_cfokovrdstart, 
        output         ch3_eyescanreset, 
        output         ch3_eyescantrigger, 
        input          ch3_eyescandataerror, 
        input          ch3_cfokovrdrdy0, 
        input          ch3_cfokovrdrdy1, 


       output [127:0]  ch3_rxdata_ext,
       output [1:0]    ch3_rxdatavalid_ext,
       output [5:0]    ch3_rxheader_ext,
       input           ch3_rxgearboxslip_ext,
       output          ch3_rxbyteisaligned_ext,
       output          ch3_rxbyterealign_ext,
       output          ch3_rxcdrlock_ext,
       output          ch3_rxcdrphdone_ext,
       output          ch3_rxchanbondseq_ext,
       output          ch3_rxchanisaligned_ext,
       output          ch3_rxchanrealign_ext,
       output          ch3_rxcominitdet_ext,
       output          ch3_rxcommadet_ext,
       output          ch3_rxcomsasdet_ext,
       output          ch3_rxcomwakedet_ext,
       output          ch3_rxdccdone_ext,
       output          ch3_rxdlyalignerr_ext,
       output          ch3_rxdlyalignprog_ext,
       output          ch3_rxelecidle_ext,
       output          ch3_rxfinealigndone_ext,
       output          ch3_rxosintdone_ext,
       output          ch3_rxosintstarted_ext,
       output          ch3_rxosintstrobedone_ext,
       output          ch3_rxosintstrobestarted_ext,
       output          ch3_rxphaligndone_ext,
       output          ch3_rxphalignerr_ext,
       output          ch3_rxphdlyresetdone_ext,
       output          ch3_rxphsetinitdone_ext,
       output          ch3_rxphshift180done_ext,
       output          ch3_rxprbserr_ext,
       output          ch3_rxprbslocked_ext,
       output          ch3_rxsliderdy_ext,
       output          ch3_rxsyncdone_ext,
       output          ch3_rxvalid_ext,
       output [15:0]   ch3_rxctrl0_ext,
       output [15:0]   ch3_rxctrl1_ext,
       output [1:0]    ch3_rxclkcorcnt_ext,
       output [1:0]    ch3_rxheadervalid_ext,
       output [1:0]    ch3_rxstartofseq_ext,
       output [2:0]    ch3_rxbufstatus_ext,
       output [2:0]    ch3_rxstatus_ext,
       output [4:0]    ch3_rxchbondo_ext,
       output [7:0]    ch3_rx10gstat_ext,
       output [7:0]    ch3_rxctrl2_ext,
       output [7:0]    ch3_rxctrl3_ext,
       output [7:0]    ch3_rxdataextendrsvd_ext,
       input           ch3_rxcdrhold_ext,
       input           ch3_rxcdrovrden_ext,
       input           ch3_rxcdrreset_ext,
       input           ch3_rxdapicodeovrden_ext,
       input           ch3_rxdapicodereset_ext,
       input           ch3_rxdlyalignreq_ext,
       input           ch3_rxeqtraining_ext,
       input           ch3_rxlpmen_ext,
       input           ch3_rxmldchaindone_ext,
       input           ch3_rxmldchainreq_ext,
       input           ch3_rxmlfinealignreq_ext,
       input           ch3_rxoobreset_ext,
       input           ch3_rxphalignreq_ext,
       input           ch3_rxphdlypd_ext,
       input           ch3_rxphdlyreset_ext,
       input           ch3_rxphsetinitreq_ext,
       input           ch3_rxphshift180_ext,
       input           ch3_rxpolarity_ext,
       input           ch3_rxprbscntreset_ext,
       input           ch3_rxslide_ext,
       input           ch3_rxsyncallin_ext,
       input           ch3_rxtermination_ext,
       input [1:0]     ch3_rxpd_ext,
       input [1:0]     ch3_rxphalignresetmask_ext,
       input [1:0]     ch3_rxresetmode_ext,
       output          ch3_rxmstresetdone_ext,
       input [3:0]     ch3_rxprbssel_ext,
       input [4:0]     ch3_rxchbondi_ext,
       input [4:0]     ch3_rxpcsresetmask_ext,
       input [6:0]     ch3_rxpmaresetmask_ext,
       output          ch3_rxprogdivresetdone_ext,
       output          ch3_rxpmaresetdone_ext,
       output          ch3_rxresetdone_ext,
        input          ch3_cdrbmcdrreq_ext,
        input          ch3_cdrfreqos_ext, 
        input          ch3_cdrincpctrl_ext, 
        input          ch3_cdrstepdir_ext, 
        input          ch3_cdrstepsq_ext, 
        input          ch3_cdrstepsx_ext, 
        input          ch3_cfokovrdfinish_ext, 
        input          ch3_cfokovrdpulse_ext, 
        input          ch3_cfokovrdstart_ext, 
        input          ch3_eyescanreset_ext, 
        input          ch3_eyescantrigger_ext, 
        output         ch3_eyescandataerror_ext, 
        output         ch3_cfokovrdrdy0_ext, 
        output         ch3_cfokovrdrdy1_ext, 



   input reset_tx_pll_and_datapath_in,
   input reset_tx_datapath_in,

   input reset_rx_pll_and_datapath_in,
   input reset_rx_datapath_in,


   output         txusrclk_out,
   output         rxusrclk_out,

   output         pcie_rstb,

   input   [3:0]  rate_sel,

   input          apb3clk,
   (* dont_touch = "true" *) input          gt_rxusrclk,
   (* dont_touch = "true" *) input          gt_txusrclk,
   input          gtpowergood,
   input          ilo_resetdone,
   input          gtreset_in,
   input          gt_lcpll_lock,
   input          gt_rpll_lock,

   input   [IP_NO_OF_LANES-1:0]  ch_phystatus_in,
  

   output         gt_pll_reset,
   output   [1:0] reset_mask,
   output         gt_ilo_reset,
   output         gpi_out,
   input          gpo_in,
   input          gpio_enable,
   
   output         rx_clr_out,
   output         rx_clrb_leaf_out,
   output         tx_clr_out,
   output         tx_clrb_leaf_out,
   output         link_status_out,
   output         tx_resetdone_out,
   output         rx_resetdone_out,
   output         rpll_lock_out,
   output         lcpll_lock_out

   //input  [127:0] application_intf


    );

   wire [7:0] rate_sel_delayed; 
   wire [7:0] rate_sel_apb_sync = rate_sel; 
   wire [7:0] rate_sel_post_gpio; 
    wire gt_reset_all_out;
    wire gpi_out_int;
    assign gt_reset_all_out = gtreset_in;

   assign reset_mask = 2'h3;

   wire gpo_in_sync;
   xpm_cdc_sync_rst # (
    .DEST_SYNC_FF (3),
    .INIT         (0)
  ) gpo_in_sync_inst (
    .src_rst  (gpo_in),
    .dest_rst (gpo_in_sync),
    .dest_clk (apb3clk)
  );

   assign gpi_out = gpio_enable ? gpi_out_int : 1'b0; 
   assign rate_sel_delayed = gpio_enable ? rate_sel_post_gpio : rate_sel_apb_sync;
   assign gpi_out_int = 1'b0;
   assign rate_sel_post_gpio = rate_sel_apb_sync;





wire [7:0] rate_sel_delayed_tx;
wire [7:0] rate_sel_delayed_rx;
wire [7:0] rate_sel_tx = rate_sel_delayed_tx;
wire [7:0] rate_sel_rx = rate_sel_delayed_rx;

xpm_cdc_array_single # (
  .DEST_SYNC_FF (3),
  .WIDTH(8)
) tx_rate_port_sync (
  .src_in   (rate_sel_delayed),
  .src_clk  (apb3clk),
  .dest_clk (gt_txusrclk),
  .dest_out (rate_sel_delayed_tx)
);

xpm_cdc_array_single # (
  .DEST_SYNC_FF (3),
  .WIDTH(8)
) rx_rate_port_sync (
  .src_in   (rate_sel_delayed),
  .src_clk  (apb3clk),
  .dest_clk (gt_rxusrclk),
  .dest_out (rate_sel_delayed_rx)
);

assign ch0_txrate = rate_sel_tx;
assign ch1_txrate = rate_sel_tx;
assign ch2_txrate = rate_sel_tx;
assign ch3_txrate = rate_sel_tx;
 


wire  [IP_NO_OF_TX_LANES-1:0]         tx_reset_done_int ; 
wire  [IP_NO_OF_RX_LANES-1:0]         rx_reset_done_int ; 
wire  [IP_NO_OF_TX_LANES-1:0]         tx_pma_reset_done_int ; 
wire  [IP_NO_OF_RX_LANES-1:0]         rx_pma_reset_done_int ; 
wire  [IP_NO_OF_TX_LANES-1:0]         mst_tx_resetdone_int ; 
wire  [IP_NO_OF_RX_LANES-1:0]         mst_rx_resetdone_int ; 
wire  [IP_NO_OF_TX_LANES-1:0]         tx_reset_done_master_or_seq ; 
wire  [IP_NO_OF_RX_LANES-1:0]         rx_reset_done_master_or_seq ; 

assign ch0_rxrate = rate_sel_rx;



assign ch1_rxrate = rate_sel_rx;



assign ch2_rxrate = rate_sel_rx;



assign ch3_rxrate = rate_sel_rx;






wire gtwiz_reset_clk_freerun_in = apb3clk;
wire gtwiz_reset_all_in = gt_reset_all_out;
wire gtwiz_reset_tx_pll_and_datapath_in = reset_tx_pll_and_datapath_in;
wire gtwiz_reset_tx_datapath_in = reset_tx_datapath_in;
wire gtwiz_reset_rx_pll_and_datapath_in = reset_rx_pll_and_datapath_in;
wire gtwiz_reset_rx_datapath_in = reset_rx_datapath_in;

wire gtwiz_reset_gtpowergood_int = gtpowergood;

wire gtwiz_reset_pllreset_tx_int;
wire gtwiz_reset_pllreset_rx_int;
assign gt_pll_reset = gtwiz_reset_pllreset_tx_int || gtwiz_reset_pllreset_rx_int;

 
 
 
 


assign gtwiz_reset_plllock_tx_int = (rate_sel_delayed == 8'd0) ?  gt_lcpll_lock    :
 gt_lcpll_lock  ; 


assign gtwiz_reset_plllock_rx_int = (rate_sel_delayed == 8'd0) ?  gt_lcpll_lock    :
 gt_lcpll_lock  ; 






wire gtwiz_reset_txresetdone_int = &tx_reset_done_int;
wire gtwiz_reset_rxresetdone_int = &rx_reset_done_int;
wire gtwiz_reset_msttxresetdone_int = &mst_tx_resetdone_int;
wire gtwiz_reset_mstrxresetdone_int = &mst_rx_resetdone_int;



wire gtwiz_reset_rxcdrlock_int = 1'b1;
wire ilo_reset_int;
wire ilo_reset_out_int;
 assign gt_ilo_reset = ilo_reset_int;

assign ilo_reset_int = ilo_reset_out_int;
wire gtwiz_reset_ilo_done = ilo_resetdone;
//wire gtwiz_reset_ilo_done = gt_lcpll_lock;

wire mst_tx_reset_int;
wire mst_tx_dp_reset_int;
wire gtwiz_reset_txprogdivreset_int;
wire gtwiz_reset_gttxreset_int;
wire gtwiz_reset_txuserrdy_int ;


assign ch0_txmstreset   = mst_tx_reset_int;
assign ch0_txmstdatapathreset   = mst_tx_dp_reset_int;
assign ch0_txprogdivreset = gtwiz_reset_txprogdivreset_int;
assign ch0_gttxreset = gtwiz_reset_gttxreset_int;
assign ch0_txuserrdy = gtwiz_reset_txuserrdy_int;


assign tx_reset_done_int[0]       =      ch0_txresetdone; 
assign tx_pma_reset_done_int[0]   =      ch0_txpmaresetdone; 
assign mst_tx_resetdone_int[0]    =      ch0_txmstresetdone; 

assign ch1_txmstreset   = mst_tx_reset_int;
assign ch1_txmstdatapathreset   = mst_tx_dp_reset_int;
assign ch1_txprogdivreset = gtwiz_reset_txprogdivreset_int;
assign ch1_gttxreset = gtwiz_reset_gttxreset_int;
assign ch1_txuserrdy = gtwiz_reset_txuserrdy_int;


assign tx_reset_done_int[1]       =      ch1_txresetdone; 
assign tx_pma_reset_done_int[1]   =      ch1_txpmaresetdone; 
assign mst_tx_resetdone_int[1]    =      ch1_txmstresetdone; 

assign ch2_txmstreset   = mst_tx_reset_int;
assign ch2_txmstdatapathreset   = mst_tx_dp_reset_int;
assign ch2_txprogdivreset = gtwiz_reset_txprogdivreset_int;
assign ch2_gttxreset = gtwiz_reset_gttxreset_int;
assign ch2_txuserrdy = gtwiz_reset_txuserrdy_int;


assign tx_reset_done_int[2]       =      ch2_txresetdone; 
assign tx_pma_reset_done_int[2]   =      ch2_txpmaresetdone; 
assign mst_tx_resetdone_int[2]    =      ch2_txmstresetdone; 

assign ch3_txmstreset   = mst_tx_reset_int;
assign ch3_txmstdatapathreset   = mst_tx_dp_reset_int;
assign ch3_txprogdivreset = gtwiz_reset_txprogdivreset_int;
assign ch3_gttxreset = gtwiz_reset_gttxreset_int;
assign ch3_txuserrdy = gtwiz_reset_txuserrdy_int;


assign tx_reset_done_int[3]       =      ch3_txresetdone; 
assign tx_pma_reset_done_int[3]   =      ch3_txpmaresetdone; 
assign mst_tx_resetdone_int[3]    =      ch3_txmstresetdone; 



wire mst_rx_reset_int;
wire mst_rx_dp_reset_int;
wire pcie_enable_int;

  assign pcie_enable_int = 1'b0;
wire pcie_rstb_int;
assign pcie_rstb = pcie_rstb_int;
wire gtwiz_reset_rxprogdivreset_int;
wire gtwiz_reset_gtrxreset_int;
wire gtwiz_reset_rxuserrdy_int;



assign ch0_rxmstreset   = mst_rx_reset_int;
assign ch0_rxmstdatapathreset   = mst_rx_dp_reset_int;
assign ch0_rxprogdivreset = gtwiz_reset_rxprogdivreset_int;
assign ch0_gtrxreset = gtwiz_reset_gtrxreset_int;
assign ch0_rxuserrdy = gtwiz_reset_rxuserrdy_int;


assign rx_reset_done_int[0]       =      ch0_rxresetdone; 
assign rx_pma_reset_done_int[0]   =      ch0_rxpmaresetdone; 
assign mst_rx_resetdone_int[0]    =      ch0_rxmstresetdone; 




assign ch1_rxmstreset   = mst_rx_reset_int;
assign ch1_rxmstdatapathreset   = mst_rx_dp_reset_int;
assign ch1_rxprogdivreset = gtwiz_reset_rxprogdivreset_int;
assign ch1_gtrxreset = gtwiz_reset_gtrxreset_int;
assign ch1_rxuserrdy = gtwiz_reset_rxuserrdy_int;


assign rx_reset_done_int[1]       =      ch1_rxresetdone; 
assign rx_pma_reset_done_int[1]   =      ch1_rxpmaresetdone; 
assign mst_rx_resetdone_int[1]    =      ch1_rxmstresetdone; 




assign ch2_rxmstreset   = mst_rx_reset_int;
assign ch2_rxmstdatapathreset   = mst_rx_dp_reset_int;
assign ch2_rxprogdivreset = gtwiz_reset_rxprogdivreset_int;
assign ch2_gtrxreset = gtwiz_reset_gtrxreset_int;
assign ch2_rxuserrdy = gtwiz_reset_rxuserrdy_int;


assign rx_reset_done_int[2]       =      ch2_rxresetdone; 
assign rx_pma_reset_done_int[2]   =      ch2_rxpmaresetdone; 
assign mst_rx_resetdone_int[2]    =      ch2_rxmstresetdone; 




assign ch3_rxmstreset   = mst_rx_reset_int;
assign ch3_rxmstdatapathreset   = mst_rx_dp_reset_int;
assign ch3_rxprogdivreset = gtwiz_reset_rxprogdivreset_int;
assign ch3_gtrxreset = gtwiz_reset_gtrxreset_int;
assign ch3_rxuserrdy = gtwiz_reset_rxuserrdy_int;


assign rx_reset_done_int[3]       =      ch3_rxresetdone; 
assign rx_pma_reset_done_int[3]   =      ch3_rxpmaresetdone; 
assign mst_rx_resetdone_int[3]    =      ch3_rxmstresetdone; 




wire gtwiz_reset_tx_enabled_tie_int = 1'b1;
wire gtwiz_reset_rx_enabled_tie_int = 1'b1;
wire gtwiz_reset_shared_pll_tie_int = 1'b1;

wire gtwiz_reset_userclk_tx_active_int = &tx_pma_reset_done_int; 
wire gtwiz_reset_userclk_rx_active_int = &rx_pma_reset_done_int;

assign tx_resetdone_out = gtwiz_reset_msttxresetdone_int;
assign rx_resetdone_out = gtwiz_reset_mstrxresetdone_int;

       transceiver_versal_lpgbt_gt_bridge_ip_0_0_gtreset   transceiver_versal_lpgbt_gt_bridge_ip_0_0_gtreset_inst (
          .gtwiz_reset_clk_freerun_in         (gtwiz_reset_clk_freerun_in),
          .gtwiz_reset_all_in                 (gtwiz_reset_all_in),
          .gtwiz_reset_tx_pll_and_datapath_in (gtwiz_reset_tx_pll_and_datapath_in),
          .gtwiz_reset_tx_datapath_in         (gtwiz_reset_tx_datapath_in),
          .gtwiz_reset_rx_pll_and_datapath_in (gtwiz_reset_rx_pll_and_datapath_in),
          .gtwiz_reset_rx_datapath_in         (gtwiz_reset_rx_datapath_in),
          .gtwiz_reset_rx_cdr_stable_out      (gtwiz_reset_rx_cdr_stable_out),
          .gtwiz_reset_tx_done_out            (gtwiz_reset_tx_done_out),
          .gtwiz_reset_rx_done_out            (gtwiz_reset_rx_done_out),
          .gtwiz_reset_userclk_tx_active_in   (gtwiz_reset_userclk_tx_active_int),
          .gtwiz_reset_userclk_rx_active_in   (gtwiz_reset_userclk_rx_active_int),
          .gtpowergood_in                     (gtwiz_reset_gtpowergood_int),
          .txusrclk2_in                       (gt_txusrclk),
          .ilo_reset_done                     (gtwiz_reset_ilo_done),
          .plllock_tx_in                      (gtwiz_reset_plllock_tx_int),
          .txresetdone_in                     (gtwiz_reset_txresetdone_int),
          .rxusrclk2_in                       (gt_rxusrclk),
          .plllock_rx_in                      (gtwiz_reset_plllock_rx_int),
          .rxcdrlock_in                       (gtwiz_reset_rxcdrlock_int),
          .rxresetdone_in                     (gtwiz_reset_rxresetdone_int),
          .pllreset_tx_out                    (gtwiz_reset_pllreset_tx_int),
          .txprogdivreset_out                 (gtwiz_reset_txprogdivreset_int),
          .iloreset_out                       (ilo_reset_out_int),
          //.iloreset_out                       (),
          .gttxreset_out                      (gtwiz_reset_gttxreset_int),
          .txuserrdy_out                      (gtwiz_reset_txuserrdy_int),
          .pllreset_rx_out                    (gtwiz_reset_pllreset_rx_int),
          .mst_tx_reset                       (mst_tx_reset_int),
          .mst_rx_reset                       (mst_rx_reset_int),
          .mst_tx_dp_reset                    (mst_tx_dp_reset_int),
          .mst_rx_dp_reset                    (mst_rx_dp_reset_int),
          .mst_tx_resetdone                   (gtwiz_reset_msttxresetdone_int),
          .mst_rx_resetdone                   (gtwiz_reset_mstrxresetdone_int),
          .pcie_enable                        (pcie_enable_int),
          .pcie_rstb_out                      (pcie_rstb_int),
          .rxprogdivreset_out                 (gtwiz_reset_rxprogdivreset_int),
          .gtrxreset_out                      (gtwiz_reset_gtrxreset_int),
          .rxuserrdy_out                      (gtwiz_reset_rxuserrdy_int),
          .rx_clr_out                         (rx_clr_out),
          .rx_clrb_leaf_out                   (rx_clrb_leaf_out),
          .tx_clr_out                         (tx_clr_out),
          .tx_clrb_leaf_out                   (tx_clrb_leaf_out),
          .tx_enabled_tie_in                  (gtwiz_reset_tx_enabled_tie_int),
          .rx_enabled_tie_in                  (gtwiz_reset_rx_enabled_tie_int),
          .shared_pll_tie_in                  (gtwiz_reset_shared_pll_tie_int)
        );





assign txusrclk_out = gt_txusrclk;
assign rxusrclk_out = gt_rxusrclk;
assign rpll_lock_out = gt_rpll_lock;
assign lcpll_lock_out = gt_lcpll_lock;

assign ch0_txdata                   =   ch0_txdata_ext;             
assign ch0_txheader                 =   ch0_txheader_ext;
assign ch0_txsequence               =   ch0_txsequence_ext;
assign ch0_txcominit                =   ch0_txcominit_ext;
assign ch0_txcomsas                 =   ch0_txcomsas_ext;
assign ch0_txcomwake                =   ch0_txcomwake_ext;
assign ch0_txdapicodeovrden         =   ch0_txdapicodeovrden_ext;
assign ch0_txdapicodereset          =   ch0_txdapicodereset_ext;
assign ch0_txdetectrx               =   ch0_txdetectrx_ext;
assign ch0_txdlyalignreq            =   ch0_txdlyalignreq_ext;
assign ch0_txelecidle               =   ch0_txelecidle_ext;
assign ch0_txinhibit                =   ch0_txinhibit_ext;
assign ch0_txmldchaindone           =   ch0_txmldchaindone_ext;
assign ch0_txmldchainreq            =   ch0_txmldchainreq_ext;
assign ch0_txoneszeros              =   ch0_txoneszeros_ext;
assign ch0_txpausedelayalign        =   ch0_txpausedelayalign_ext;
assign ch0_txpcsresetmask           =   ch0_txpcsresetmask_ext;
assign ch0_txphalignreq             =   ch0_txphalignreq_ext;
assign ch0_txphalignresetmask       =   ch0_txphalignresetmask_ext;
assign ch0_txphdlypd                =   ch0_txphdlypd_ext;
assign ch0_txphdlyreset             =   ch0_txphdlyreset_ext;
assign ch0_txphsetinitreq           =   ch0_txphsetinitreq_ext;
assign ch0_txphshift180             =   ch0_txphshift180_ext;
assign ch0_txpicodeovrden           =   ch0_txpicodeovrden_ext;
assign ch0_txpicodereset            =   ch0_txpicodereset_ext;
assign ch0_txpippmen                =   ch0_txpippmen_ext;
assign ch0_txpisopd                 =   ch0_txpisopd_ext;
assign ch0_txpolarity               =   ch0_txpolarity_ext;
assign ch0_txprbsforceerr           =   ch0_txprbsforceerr_ext;
assign ch0_txswing                  =   ch0_txswing_ext;
assign ch0_txsyncallin              =   ch0_txsyncallin_ext;            
assign ch0_tx10gstat_ext            =   ch0_tx10gstat;          
assign ch0_txcomfinish_ext          =   ch0_txcomfinish;       
assign ch0_txdccdone_ext            =   ch0_txdccdone;
assign ch0_txdlyalignerr_ext        =   ch0_txdlyalignerr;     
assign ch0_txdlyalignprog_ext       =   ch0_txdlyalignprog;
assign ch0_txphaligndone_ext        =   ch0_txphaligndone;     
assign ch0_txphalignerr_ext         =   ch0_txphalignerr;      
assign ch0_txphalignoutrsvd_ext     =   ch0_txphalignoutrsvd;   
assign ch0_txphdlyresetdone_ext     =   ch0_txphdlyresetdone;   
assign ch0_txphsetinitdone_ext      =   ch0_txphsetinitdone;    
assign ch0_txphshift180done_ext     =   ch0_txphshift180done;     
assign ch0_txsyncdone_ext           =   ch0_txsyncdone;        
assign ch0_txbufstatus_ext          =   ch0_txbufstatus;       
assign ch0_txctrl0                  =   ch0_txctrl0_ext;
assign ch0_txctrl1                  =   ch0_txctrl1_ext;
assign ch0_txdeemph                 =   ch0_txdeemph_ext;
assign ch0_txpd                     =   ch0_txpd_ext;
assign ch0_txresetmode              =   ch0_txresetmode_ext;
assign ch0_txmargin                 =   ch0_txmargin_ext;
assign ch0_txpmaresetmask           =   ch0_txpmaresetmask_ext;
assign ch0_txprbssel                =   ch0_txprbssel_ext;
assign ch0_txdiffctrl               =   ch0_txdiffctrl_ext;
assign ch0_txpippmstepsize          =   ch0_txpippmstepsize_ext;
assign ch0_txpostcursor             =   ch0_txpostcursor_ext;
assign ch0_txprecursor              =   ch0_txprecursor_ext;
assign ch0_txmaincursor             =   ch0_txmaincursor_ext;
assign ch0_txctrl2                  =   ch0_txctrl2_ext;
assign ch0_txdataextendrsvd         =   ch0_txdataextendrsvd_ext;
assign ch0_txresetdone_ext          =   ch0_txresetdone;         
assign ch0_txprogdivresetdone_ext   =   ch0_txprogdivresetdone;
assign ch0_txpmaresetdone_ext       =   ch0_txpmaresetdone;

assign ch0_txmstresetdone_ext       =   ch0_txmstresetdone;


assign ch1_txdata                   =   ch1_txdata_ext;             
assign ch1_txheader                 =   ch1_txheader_ext;
assign ch1_txsequence               =   ch1_txsequence_ext;
assign ch1_txcominit                =   ch1_txcominit_ext;
assign ch1_txcomsas                 =   ch1_txcomsas_ext;
assign ch1_txcomwake                =   ch1_txcomwake_ext;
assign ch1_txdapicodeovrden         =   ch1_txdapicodeovrden_ext;
assign ch1_txdapicodereset          =   ch1_txdapicodereset_ext;
assign ch1_txdetectrx               =   ch1_txdetectrx_ext;
assign ch1_txdlyalignreq            =   ch1_txdlyalignreq_ext;
assign ch1_txelecidle               =   ch1_txelecidle_ext;
assign ch1_txinhibit                =   ch1_txinhibit_ext;
assign ch1_txmldchaindone           =   ch1_txmldchaindone_ext;
assign ch1_txmldchainreq            =   ch1_txmldchainreq_ext;
assign ch1_txoneszeros              =   ch1_txoneszeros_ext;
assign ch1_txpausedelayalign        =   ch1_txpausedelayalign_ext;
assign ch1_txpcsresetmask           =   ch1_txpcsresetmask_ext;
assign ch1_txphalignreq             =   ch1_txphalignreq_ext;
assign ch1_txphalignresetmask       =   ch1_txphalignresetmask_ext;
assign ch1_txphdlypd                =   ch1_txphdlypd_ext;
assign ch1_txphdlyreset             =   ch1_txphdlyreset_ext;
assign ch1_txphsetinitreq           =   ch1_txphsetinitreq_ext;
assign ch1_txphshift180             =   ch1_txphshift180_ext;
assign ch1_txpicodeovrden           =   ch1_txpicodeovrden_ext;
assign ch1_txpicodereset            =   ch1_txpicodereset_ext;
assign ch1_txpippmen                =   ch1_txpippmen_ext;
assign ch1_txpisopd                 =   ch1_txpisopd_ext;
assign ch1_txpolarity               =   ch1_txpolarity_ext;
assign ch1_txprbsforceerr           =   ch1_txprbsforceerr_ext;
assign ch1_txswing                  =   ch1_txswing_ext;
assign ch1_txsyncallin              =   ch1_txsyncallin_ext;            
assign ch1_tx10gstat_ext            =   ch1_tx10gstat;          
assign ch1_txcomfinish_ext          =   ch1_txcomfinish;       
assign ch1_txdccdone_ext            =   ch1_txdccdone;
assign ch1_txdlyalignerr_ext        =   ch1_txdlyalignerr;     
assign ch1_txdlyalignprog_ext       =   ch1_txdlyalignprog;
assign ch1_txphaligndone_ext        =   ch1_txphaligndone;     
assign ch1_txphalignerr_ext         =   ch1_txphalignerr;      
assign ch1_txphalignoutrsvd_ext     =   ch1_txphalignoutrsvd;   
assign ch1_txphdlyresetdone_ext     =   ch1_txphdlyresetdone;   
assign ch1_txphsetinitdone_ext      =   ch1_txphsetinitdone;    
assign ch1_txphshift180done_ext     =   ch1_txphshift180done;     
assign ch1_txsyncdone_ext           =   ch1_txsyncdone;        
assign ch1_txbufstatus_ext          =   ch1_txbufstatus;       
assign ch1_txctrl0                  =   ch1_txctrl0_ext;
assign ch1_txctrl1                  =   ch1_txctrl1_ext;
assign ch1_txdeemph                 =   ch1_txdeemph_ext;
assign ch1_txpd                     =   ch1_txpd_ext;
assign ch1_txresetmode              =   ch1_txresetmode_ext;
assign ch1_txmargin                 =   ch1_txmargin_ext;
assign ch1_txpmaresetmask           =   ch1_txpmaresetmask_ext;
assign ch1_txprbssel                =   ch1_txprbssel_ext;
assign ch1_txdiffctrl               =   ch1_txdiffctrl_ext;
assign ch1_txpippmstepsize          =   ch1_txpippmstepsize_ext;
assign ch1_txpostcursor             =   ch1_txpostcursor_ext;
assign ch1_txprecursor              =   ch1_txprecursor_ext;
assign ch1_txmaincursor             =   ch1_txmaincursor_ext;
assign ch1_txctrl2                  =   ch1_txctrl2_ext;
assign ch1_txdataextendrsvd         =   ch1_txdataextendrsvd_ext;
assign ch1_txresetdone_ext          =   ch1_txresetdone;         
assign ch1_txprogdivresetdone_ext   =   ch1_txprogdivresetdone;
assign ch1_txpmaresetdone_ext       =   ch1_txpmaresetdone;

assign ch1_txmstresetdone_ext       =   ch1_txmstresetdone;


assign ch2_txdata                   =   ch2_txdata_ext;             
assign ch2_txheader                 =   ch2_txheader_ext;
assign ch2_txsequence               =   ch2_txsequence_ext;
assign ch2_txcominit                =   ch2_txcominit_ext;
assign ch2_txcomsas                 =   ch2_txcomsas_ext;
assign ch2_txcomwake                =   ch2_txcomwake_ext;
assign ch2_txdapicodeovrden         =   ch2_txdapicodeovrden_ext;
assign ch2_txdapicodereset          =   ch2_txdapicodereset_ext;
assign ch2_txdetectrx               =   ch2_txdetectrx_ext;
assign ch2_txdlyalignreq            =   ch2_txdlyalignreq_ext;
assign ch2_txelecidle               =   ch2_txelecidle_ext;
assign ch2_txinhibit                =   ch2_txinhibit_ext;
assign ch2_txmldchaindone           =   ch2_txmldchaindone_ext;
assign ch2_txmldchainreq            =   ch2_txmldchainreq_ext;
assign ch2_txoneszeros              =   ch2_txoneszeros_ext;
assign ch2_txpausedelayalign        =   ch2_txpausedelayalign_ext;
assign ch2_txpcsresetmask           =   ch2_txpcsresetmask_ext;
assign ch2_txphalignreq             =   ch2_txphalignreq_ext;
assign ch2_txphalignresetmask       =   ch2_txphalignresetmask_ext;
assign ch2_txphdlypd                =   ch2_txphdlypd_ext;
assign ch2_txphdlyreset             =   ch2_txphdlyreset_ext;
assign ch2_txphsetinitreq           =   ch2_txphsetinitreq_ext;
assign ch2_txphshift180             =   ch2_txphshift180_ext;
assign ch2_txpicodeovrden           =   ch2_txpicodeovrden_ext;
assign ch2_txpicodereset            =   ch2_txpicodereset_ext;
assign ch2_txpippmen                =   ch2_txpippmen_ext;
assign ch2_txpisopd                 =   ch2_txpisopd_ext;
assign ch2_txpolarity               =   ch2_txpolarity_ext;
assign ch2_txprbsforceerr           =   ch2_txprbsforceerr_ext;
assign ch2_txswing                  =   ch2_txswing_ext;
assign ch2_txsyncallin              =   ch2_txsyncallin_ext;            
assign ch2_tx10gstat_ext            =   ch2_tx10gstat;          
assign ch2_txcomfinish_ext          =   ch2_txcomfinish;       
assign ch2_txdccdone_ext            =   ch2_txdccdone;
assign ch2_txdlyalignerr_ext        =   ch2_txdlyalignerr;     
assign ch2_txdlyalignprog_ext       =   ch2_txdlyalignprog;
assign ch2_txphaligndone_ext        =   ch2_txphaligndone;     
assign ch2_txphalignerr_ext         =   ch2_txphalignerr;      
assign ch2_txphalignoutrsvd_ext     =   ch2_txphalignoutrsvd;   
assign ch2_txphdlyresetdone_ext     =   ch2_txphdlyresetdone;   
assign ch2_txphsetinitdone_ext      =   ch2_txphsetinitdone;    
assign ch2_txphshift180done_ext     =   ch2_txphshift180done;     
assign ch2_txsyncdone_ext           =   ch2_txsyncdone;        
assign ch2_txbufstatus_ext          =   ch2_txbufstatus;       
assign ch2_txctrl0                  =   ch2_txctrl0_ext;
assign ch2_txctrl1                  =   ch2_txctrl1_ext;
assign ch2_txdeemph                 =   ch2_txdeemph_ext;
assign ch2_txpd                     =   ch2_txpd_ext;
assign ch2_txresetmode              =   ch2_txresetmode_ext;
assign ch2_txmargin                 =   ch2_txmargin_ext;
assign ch2_txpmaresetmask           =   ch2_txpmaresetmask_ext;
assign ch2_txprbssel                =   ch2_txprbssel_ext;
assign ch2_txdiffctrl               =   ch2_txdiffctrl_ext;
assign ch2_txpippmstepsize          =   ch2_txpippmstepsize_ext;
assign ch2_txpostcursor             =   ch2_txpostcursor_ext;
assign ch2_txprecursor              =   ch2_txprecursor_ext;
assign ch2_txmaincursor             =   ch2_txmaincursor_ext;
assign ch2_txctrl2                  =   ch2_txctrl2_ext;
assign ch2_txdataextendrsvd         =   ch2_txdataextendrsvd_ext;
assign ch2_txresetdone_ext          =   ch2_txresetdone;         
assign ch2_txprogdivresetdone_ext   =   ch2_txprogdivresetdone;
assign ch2_txpmaresetdone_ext       =   ch2_txpmaresetdone;

assign ch2_txmstresetdone_ext       =   ch2_txmstresetdone;


assign ch3_txdata                   =   ch3_txdata_ext;             
assign ch3_txheader                 =   ch3_txheader_ext;
assign ch3_txsequence               =   ch3_txsequence_ext;
assign ch3_txcominit                =   ch3_txcominit_ext;
assign ch3_txcomsas                 =   ch3_txcomsas_ext;
assign ch3_txcomwake                =   ch3_txcomwake_ext;
assign ch3_txdapicodeovrden         =   ch3_txdapicodeovrden_ext;
assign ch3_txdapicodereset          =   ch3_txdapicodereset_ext;
assign ch3_txdetectrx               =   ch3_txdetectrx_ext;
assign ch3_txdlyalignreq            =   ch3_txdlyalignreq_ext;
assign ch3_txelecidle               =   ch3_txelecidle_ext;
assign ch3_txinhibit                =   ch3_txinhibit_ext;
assign ch3_txmldchaindone           =   ch3_txmldchaindone_ext;
assign ch3_txmldchainreq            =   ch3_txmldchainreq_ext;
assign ch3_txoneszeros              =   ch3_txoneszeros_ext;
assign ch3_txpausedelayalign        =   ch3_txpausedelayalign_ext;
assign ch3_txpcsresetmask           =   ch3_txpcsresetmask_ext;
assign ch3_txphalignreq             =   ch3_txphalignreq_ext;
assign ch3_txphalignresetmask       =   ch3_txphalignresetmask_ext;
assign ch3_txphdlypd                =   ch3_txphdlypd_ext;
assign ch3_txphdlyreset             =   ch3_txphdlyreset_ext;
assign ch3_txphsetinitreq           =   ch3_txphsetinitreq_ext;
assign ch3_txphshift180             =   ch3_txphshift180_ext;
assign ch3_txpicodeovrden           =   ch3_txpicodeovrden_ext;
assign ch3_txpicodereset            =   ch3_txpicodereset_ext;
assign ch3_txpippmen                =   ch3_txpippmen_ext;
assign ch3_txpisopd                 =   ch3_txpisopd_ext;
assign ch3_txpolarity               =   ch3_txpolarity_ext;
assign ch3_txprbsforceerr           =   ch3_txprbsforceerr_ext;
assign ch3_txswing                  =   ch3_txswing_ext;
assign ch3_txsyncallin              =   ch3_txsyncallin_ext;            
assign ch3_tx10gstat_ext            =   ch3_tx10gstat;          
assign ch3_txcomfinish_ext          =   ch3_txcomfinish;       
assign ch3_txdccdone_ext            =   ch3_txdccdone;
assign ch3_txdlyalignerr_ext        =   ch3_txdlyalignerr;     
assign ch3_txdlyalignprog_ext       =   ch3_txdlyalignprog;
assign ch3_txphaligndone_ext        =   ch3_txphaligndone;     
assign ch3_txphalignerr_ext         =   ch3_txphalignerr;      
assign ch3_txphalignoutrsvd_ext     =   ch3_txphalignoutrsvd;   
assign ch3_txphdlyresetdone_ext     =   ch3_txphdlyresetdone;   
assign ch3_txphsetinitdone_ext      =   ch3_txphsetinitdone;    
assign ch3_txphshift180done_ext     =   ch3_txphshift180done;     
assign ch3_txsyncdone_ext           =   ch3_txsyncdone;        
assign ch3_txbufstatus_ext          =   ch3_txbufstatus;       
assign ch3_txctrl0                  =   ch3_txctrl0_ext;
assign ch3_txctrl1                  =   ch3_txctrl1_ext;
assign ch3_txdeemph                 =   ch3_txdeemph_ext;
assign ch3_txpd                     =   ch3_txpd_ext;
assign ch3_txresetmode              =   ch3_txresetmode_ext;
assign ch3_txmargin                 =   ch3_txmargin_ext;
assign ch3_txpmaresetmask           =   ch3_txpmaresetmask_ext;
assign ch3_txprbssel                =   ch3_txprbssel_ext;
assign ch3_txdiffctrl               =   ch3_txdiffctrl_ext;
assign ch3_txpippmstepsize          =   ch3_txpippmstepsize_ext;
assign ch3_txpostcursor             =   ch3_txpostcursor_ext;
assign ch3_txprecursor              =   ch3_txprecursor_ext;
assign ch3_txmaincursor             =   ch3_txmaincursor_ext;
assign ch3_txctrl2                  =   ch3_txctrl2_ext;
assign ch3_txdataextendrsvd         =   ch3_txdataextendrsvd_ext;
assign ch3_txresetdone_ext          =   ch3_txresetdone;         
assign ch3_txprogdivresetdone_ext   =   ch3_txprogdivresetdone;
assign ch3_txpmaresetdone_ext       =   ch3_txpmaresetdone;

assign ch3_txmstresetdone_ext       =   ch3_txmstresetdone;



assign ch0_rxmstresetdone_ext       =   ch0_rxmstresetdone;

assign ch0_rxdata_ext               =   ch0_rxdata;
assign ch0_rxdatavalid_ext          =   ch0_rxdatavalid;
assign ch0_rxheader_ext             =   ch0_rxheader;           
assign ch0_rxgearboxslip            =   ch0_rxgearboxslip_ext;
assign ch0_rxbyteisaligned_ext      =   ch0_rxbyteisaligned;
assign ch0_rxbyterealign_ext        =   ch0_rxbyterealign;
assign ch0_rxcdrlock_ext            =   ch0_rxcdrlock;
assign ch0_rxcdrphdone_ext          =   ch0_rxcdrphdone;
assign ch0_rxchanbondseq_ext        =   ch0_rxchanbondseq;
assign ch0_rxchanisaligned_ext      =   ch0_rxchanisaligned;
assign ch0_rxchanrealign_ext        =   ch0_rxchanrealign;
assign ch0_rxcominitdet_ext         =   ch0_rxcominitdet;
assign ch0_rxcommadet_ext           =   ch0_rxcommadet;
assign ch0_rxcomsasdet_ext          =   ch0_rxcomsasdet;
assign ch0_rxcomwakedet_ext         =   ch0_rxcomwakedet;
assign ch0_rxdccdone_ext            =   ch0_rxdccdone;
assign ch0_rxdlyalignerr_ext        =   ch0_rxdlyalignerr;
assign ch0_rxdlyalignprog_ext       =   ch0_rxdlyalignprog;
assign ch0_rxelecidle_ext           =   ch0_rxelecidle;
assign ch0_rxfinealigndone_ext      =   ch0_rxfinealigndone;
assign ch0_rxosintdone_ext          =   ch0_rxosintdone;
assign ch0_rxosintstarted_ext       =   ch0_rxosintstarted;
assign ch0_rxosintstrobedone_ext    =   ch0_rxosintstrobedone;
assign ch0_rxosintstrobestarted_ext =   ch0_rxosintstrobestarted;
assign ch0_rxphaligndone_ext        =   ch0_rxphaligndone;
assign ch0_rxphalignerr_ext         =   ch0_rxphalignerr;
assign ch0_rxphdlyresetdone_ext     =   ch0_rxphdlyresetdone;
assign ch0_rxphsetinitdone_ext      =   ch0_rxphsetinitdone;
assign ch0_rxphshift180done_ext     =   ch0_rxphshift180done;
assign ch0_rxprbserr_ext            =   ch0_rxprbserr;
assign ch0_rxprbslocked_ext         =   ch0_rxprbslocked;
assign ch0_rxsliderdy_ext           =   ch0_rxsliderdy;
assign ch0_rxsyncdone_ext           =   ch0_rxsyncdone;
assign ch0_rxvalid_ext              =   ch0_rxvalid;
assign ch0_rxctrl0_ext              =   ch0_rxctrl0;
assign ch0_rxctrl1_ext              =   ch0_rxctrl1;
assign ch0_rxclkcorcnt_ext          =   ch0_rxclkcorcnt;
assign ch0_rxheadervalid_ext        =   ch0_rxheadervalid;
assign ch0_rxstartofseq_ext         =   ch0_rxstartofseq;
assign ch0_rxbufstatus_ext          =   ch0_rxbufstatus;
assign ch0_rxstatus_ext             =   ch0_rxstatus;
assign ch0_rxchbondo_ext            =   ch0_rxchbondo;
assign ch0_rx10gstat_ext            =   ch0_rx10gstat;
assign ch0_rxctrl2_ext              =   ch0_rxctrl2;
assign ch0_rxctrl3_ext              =   ch0_rxctrl3;
assign ch0_rxdataextendrsvd_ext     =   ch0_rxdataextendrsvd;      
assign ch0_rxcdrhold                =   ch0_rxcdrhold_ext;
assign ch0_rxcdrovrden              =   ch0_rxcdrovrden_ext;
assign ch0_rxcdrreset               =   ch0_rxcdrreset_ext;
assign ch0_rxdapicodeovrden         =   ch0_rxdapicodeovrden_ext;
assign ch0_rxdapicodereset          =   ch0_rxdapicodereset_ext;
assign ch0_rxdlyalignreq            =   ch0_rxdlyalignreq_ext;
assign ch0_rxeqtraining             =   ch0_rxeqtraining_ext;
assign ch0_rxlpmen                  =   ch0_rxlpmen_ext;
assign ch0_rxmldchaindone           =   ch0_rxmldchaindone_ext;
assign ch0_rxmldchainreq            =   ch0_rxmldchainreq_ext;
assign ch0_rxmlfinealignreq         =   ch0_rxmlfinealignreq_ext;
assign ch0_rxoobreset               =   ch0_rxoobreset_ext;
assign ch0_rxphalignreq             =   ch0_rxphalignreq_ext;
assign ch0_rxphdlypd                =   ch0_rxphdlypd_ext;
assign ch0_rxphdlyreset             =   ch0_rxphdlyreset_ext;
assign ch0_rxphsetinitreq           =   ch0_rxphsetinitreq_ext;
assign ch0_rxphshift180             =   ch0_rxphshift180_ext;
assign ch0_rxpolarity               =   ch0_rxpolarity_ext;
assign ch0_rxprbscntreset           =   ch0_rxprbscntreset_ext;
assign ch0_rxslide                  =   ch0_rxslide_ext;
assign ch0_rxsyncallin              =   ch0_rxsyncallin_ext;
assign ch0_rxtermination            =   ch0_rxtermination_ext;
assign ch0_rxpd                     =   ch0_rxpd_ext;
assign ch0_rxphalignresetmask       =   ch0_rxphalignresetmask_ext;
assign ch0_rxresetmode              =   ch0_rxresetmode_ext;
assign ch0_rxprbssel                =   ch0_rxprbssel_ext;
assign ch0_rxchbondi                =   ch0_rxchbondi_ext;
assign ch0_rxpcsresetmask           =   ch0_rxpcsresetmask_ext;
assign ch0_rxpmaresetmask           =   ch0_rxpmaresetmask_ext;
assign ch0_rxprogdivresetdone_ext   =   ch0_rxprogdivresetdone;
assign ch0_rxpmaresetdone_ext       =   ch0_rxpmaresetdone;
assign ch0_rxresetdone_ext          =   ch0_rxresetdone;

assign ch0_cdrbmcdrreq              =   ch0_cdrbmcdrreq_ext;
assign ch0_cdrfreqos                =   ch0_cdrfreqos_ext; 
assign ch0_cdrincpctrl              =   ch0_cdrincpctrl_ext; 
assign ch0_cdrstepdir               =   ch0_cdrstepdir_ext; 
assign ch0_cdrstepsq                =   ch0_cdrstepsq_ext; 
assign ch0_cdrstepsx                =   ch0_cdrstepsx_ext; 
assign ch0_cfokovrdfinish           =   ch0_cfokovrdfinish_ext; 
assign ch0_cfokovrdpulse            =   ch0_cfokovrdpulse_ext; 
assign ch0_cfokovrdstart            =   ch0_cfokovrdstart_ext; 
assign ch0_eyescanreset             =   ch0_eyescanreset_ext; 
assign ch0_eyescantrigger           =   ch0_eyescantrigger_ext; 
assign ch0_eyescandataerror_ext     =   ch0_eyescandataerror; 
assign ch0_cfokovrdrdy0_ext         =   ch0_cfokovrdrdy0;
assign ch0_cfokovrdrdy1_ext         =   ch0_cfokovrdrdy1; 


assign ch1_rxmstresetdone_ext       =   ch1_rxmstresetdone;

assign ch1_rxdata_ext               =   ch1_rxdata;
assign ch1_rxdatavalid_ext          =   ch1_rxdatavalid;
assign ch1_rxheader_ext             =   ch1_rxheader;           
assign ch1_rxgearboxslip            =   ch1_rxgearboxslip_ext;
assign ch1_rxbyteisaligned_ext      =   ch1_rxbyteisaligned;
assign ch1_rxbyterealign_ext        =   ch1_rxbyterealign;
assign ch1_rxcdrlock_ext            =   ch1_rxcdrlock;
assign ch1_rxcdrphdone_ext          =   ch1_rxcdrphdone;
assign ch1_rxchanbondseq_ext        =   ch1_rxchanbondseq;
assign ch1_rxchanisaligned_ext      =   ch1_rxchanisaligned;
assign ch1_rxchanrealign_ext        =   ch1_rxchanrealign;
assign ch1_rxcominitdet_ext         =   ch1_rxcominitdet;
assign ch1_rxcommadet_ext           =   ch1_rxcommadet;
assign ch1_rxcomsasdet_ext          =   ch1_rxcomsasdet;
assign ch1_rxcomwakedet_ext         =   ch1_rxcomwakedet;
assign ch1_rxdccdone_ext            =   ch1_rxdccdone;
assign ch1_rxdlyalignerr_ext        =   ch1_rxdlyalignerr;
assign ch1_rxdlyalignprog_ext       =   ch1_rxdlyalignprog;
assign ch1_rxelecidle_ext           =   ch1_rxelecidle;
assign ch1_rxfinealigndone_ext      =   ch1_rxfinealigndone;
assign ch1_rxosintdone_ext          =   ch1_rxosintdone;
assign ch1_rxosintstarted_ext       =   ch1_rxosintstarted;
assign ch1_rxosintstrobedone_ext    =   ch1_rxosintstrobedone;
assign ch1_rxosintstrobestarted_ext =   ch1_rxosintstrobestarted;
assign ch1_rxphaligndone_ext        =   ch1_rxphaligndone;
assign ch1_rxphalignerr_ext         =   ch1_rxphalignerr;
assign ch1_rxphdlyresetdone_ext     =   ch1_rxphdlyresetdone;
assign ch1_rxphsetinitdone_ext      =   ch1_rxphsetinitdone;
assign ch1_rxphshift180done_ext     =   ch1_rxphshift180done;
assign ch1_rxprbserr_ext            =   ch1_rxprbserr;
assign ch1_rxprbslocked_ext         =   ch1_rxprbslocked;
assign ch1_rxsliderdy_ext           =   ch1_rxsliderdy;
assign ch1_rxsyncdone_ext           =   ch1_rxsyncdone;
assign ch1_rxvalid_ext              =   ch1_rxvalid;
assign ch1_rxctrl0_ext              =   ch1_rxctrl0;
assign ch1_rxctrl1_ext              =   ch1_rxctrl1;
assign ch1_rxclkcorcnt_ext          =   ch1_rxclkcorcnt;
assign ch1_rxheadervalid_ext        =   ch1_rxheadervalid;
assign ch1_rxstartofseq_ext         =   ch1_rxstartofseq;
assign ch1_rxbufstatus_ext          =   ch1_rxbufstatus;
assign ch1_rxstatus_ext             =   ch1_rxstatus;
assign ch1_rxchbondo_ext            =   ch1_rxchbondo;
assign ch1_rx10gstat_ext            =   ch1_rx10gstat;
assign ch1_rxctrl2_ext              =   ch1_rxctrl2;
assign ch1_rxctrl3_ext              =   ch1_rxctrl3;
assign ch1_rxdataextendrsvd_ext     =   ch1_rxdataextendrsvd;      
assign ch1_rxcdrhold                =   ch1_rxcdrhold_ext;
assign ch1_rxcdrovrden              =   ch1_rxcdrovrden_ext;
assign ch1_rxcdrreset               =   ch1_rxcdrreset_ext;
assign ch1_rxdapicodeovrden         =   ch1_rxdapicodeovrden_ext;
assign ch1_rxdapicodereset          =   ch1_rxdapicodereset_ext;
assign ch1_rxdlyalignreq            =   ch1_rxdlyalignreq_ext;
assign ch1_rxeqtraining             =   ch1_rxeqtraining_ext;
assign ch1_rxlpmen                  =   ch1_rxlpmen_ext;
assign ch1_rxmldchaindone           =   ch1_rxmldchaindone_ext;
assign ch1_rxmldchainreq            =   ch1_rxmldchainreq_ext;
assign ch1_rxmlfinealignreq         =   ch1_rxmlfinealignreq_ext;
assign ch1_rxoobreset               =   ch1_rxoobreset_ext;
assign ch1_rxphalignreq             =   ch1_rxphalignreq_ext;
assign ch1_rxphdlypd                =   ch1_rxphdlypd_ext;
assign ch1_rxphdlyreset             =   ch1_rxphdlyreset_ext;
assign ch1_rxphsetinitreq           =   ch1_rxphsetinitreq_ext;
assign ch1_rxphshift180             =   ch1_rxphshift180_ext;
assign ch1_rxpolarity               =   ch1_rxpolarity_ext;
assign ch1_rxprbscntreset           =   ch1_rxprbscntreset_ext;
assign ch1_rxslide                  =   ch1_rxslide_ext;
assign ch1_rxsyncallin              =   ch1_rxsyncallin_ext;
assign ch1_rxtermination            =   ch1_rxtermination_ext;
assign ch1_rxpd                     =   ch1_rxpd_ext;
assign ch1_rxphalignresetmask       =   ch1_rxphalignresetmask_ext;
assign ch1_rxresetmode              =   ch1_rxresetmode_ext;
assign ch1_rxprbssel                =   ch1_rxprbssel_ext;
assign ch1_rxchbondi                =   ch1_rxchbondi_ext;
assign ch1_rxpcsresetmask           =   ch1_rxpcsresetmask_ext;
assign ch1_rxpmaresetmask           =   ch1_rxpmaresetmask_ext;
assign ch1_rxprogdivresetdone_ext   =   ch1_rxprogdivresetdone;
assign ch1_rxpmaresetdone_ext       =   ch1_rxpmaresetdone;
assign ch1_rxresetdone_ext          =   ch1_rxresetdone;

assign ch1_cdrbmcdrreq              =   ch1_cdrbmcdrreq_ext;
assign ch1_cdrfreqos                =   ch1_cdrfreqos_ext; 
assign ch1_cdrincpctrl              =   ch1_cdrincpctrl_ext; 
assign ch1_cdrstepdir               =   ch1_cdrstepdir_ext; 
assign ch1_cdrstepsq                =   ch1_cdrstepsq_ext; 
assign ch1_cdrstepsx                =   ch1_cdrstepsx_ext; 
assign ch1_cfokovrdfinish           =   ch1_cfokovrdfinish_ext; 
assign ch1_cfokovrdpulse            =   ch1_cfokovrdpulse_ext; 
assign ch1_cfokovrdstart            =   ch1_cfokovrdstart_ext; 
assign ch1_eyescanreset             =   ch1_eyescanreset_ext; 
assign ch1_eyescantrigger           =   ch1_eyescantrigger_ext; 
assign ch1_eyescandataerror_ext     =   ch1_eyescandataerror; 
assign ch1_cfokovrdrdy0_ext         =   ch1_cfokovrdrdy0;
assign ch1_cfokovrdrdy1_ext         =   ch1_cfokovrdrdy1; 


assign ch2_rxmstresetdone_ext       =   ch2_rxmstresetdone;

assign ch2_rxdata_ext               =   ch2_rxdata;
assign ch2_rxdatavalid_ext          =   ch2_rxdatavalid;
assign ch2_rxheader_ext             =   ch2_rxheader;           
assign ch2_rxgearboxslip            =   ch2_rxgearboxslip_ext;
assign ch2_rxbyteisaligned_ext      =   ch2_rxbyteisaligned;
assign ch2_rxbyterealign_ext        =   ch2_rxbyterealign;
assign ch2_rxcdrlock_ext            =   ch2_rxcdrlock;
assign ch2_rxcdrphdone_ext          =   ch2_rxcdrphdone;
assign ch2_rxchanbondseq_ext        =   ch2_rxchanbondseq;
assign ch2_rxchanisaligned_ext      =   ch2_rxchanisaligned;
assign ch2_rxchanrealign_ext        =   ch2_rxchanrealign;
assign ch2_rxcominitdet_ext         =   ch2_rxcominitdet;
assign ch2_rxcommadet_ext           =   ch2_rxcommadet;
assign ch2_rxcomsasdet_ext          =   ch2_rxcomsasdet;
assign ch2_rxcomwakedet_ext         =   ch2_rxcomwakedet;
assign ch2_rxdccdone_ext            =   ch2_rxdccdone;
assign ch2_rxdlyalignerr_ext        =   ch2_rxdlyalignerr;
assign ch2_rxdlyalignprog_ext       =   ch2_rxdlyalignprog;
assign ch2_rxelecidle_ext           =   ch2_rxelecidle;
assign ch2_rxfinealigndone_ext      =   ch2_rxfinealigndone;
assign ch2_rxosintdone_ext          =   ch2_rxosintdone;
assign ch2_rxosintstarted_ext       =   ch2_rxosintstarted;
assign ch2_rxosintstrobedone_ext    =   ch2_rxosintstrobedone;
assign ch2_rxosintstrobestarted_ext =   ch2_rxosintstrobestarted;
assign ch2_rxphaligndone_ext        =   ch2_rxphaligndone;
assign ch2_rxphalignerr_ext         =   ch2_rxphalignerr;
assign ch2_rxphdlyresetdone_ext     =   ch2_rxphdlyresetdone;
assign ch2_rxphsetinitdone_ext      =   ch2_rxphsetinitdone;
assign ch2_rxphshift180done_ext     =   ch2_rxphshift180done;
assign ch2_rxprbserr_ext            =   ch2_rxprbserr;
assign ch2_rxprbslocked_ext         =   ch2_rxprbslocked;
assign ch2_rxsliderdy_ext           =   ch2_rxsliderdy;
assign ch2_rxsyncdone_ext           =   ch2_rxsyncdone;
assign ch2_rxvalid_ext              =   ch2_rxvalid;
assign ch2_rxctrl0_ext              =   ch2_rxctrl0;
assign ch2_rxctrl1_ext              =   ch2_rxctrl1;
assign ch2_rxclkcorcnt_ext          =   ch2_rxclkcorcnt;
assign ch2_rxheadervalid_ext        =   ch2_rxheadervalid;
assign ch2_rxstartofseq_ext         =   ch2_rxstartofseq;
assign ch2_rxbufstatus_ext          =   ch2_rxbufstatus;
assign ch2_rxstatus_ext             =   ch2_rxstatus;
assign ch2_rxchbondo_ext            =   ch2_rxchbondo;
assign ch2_rx10gstat_ext            =   ch2_rx10gstat;
assign ch2_rxctrl2_ext              =   ch2_rxctrl2;
assign ch2_rxctrl3_ext              =   ch2_rxctrl3;
assign ch2_rxdataextendrsvd_ext     =   ch2_rxdataextendrsvd;      
assign ch2_rxcdrhold                =   ch2_rxcdrhold_ext;
assign ch2_rxcdrovrden              =   ch2_rxcdrovrden_ext;
assign ch2_rxcdrreset               =   ch2_rxcdrreset_ext;
assign ch2_rxdapicodeovrden         =   ch2_rxdapicodeovrden_ext;
assign ch2_rxdapicodereset          =   ch2_rxdapicodereset_ext;
assign ch2_rxdlyalignreq            =   ch2_rxdlyalignreq_ext;
assign ch2_rxeqtraining             =   ch2_rxeqtraining_ext;
assign ch2_rxlpmen                  =   ch2_rxlpmen_ext;
assign ch2_rxmldchaindone           =   ch2_rxmldchaindone_ext;
assign ch2_rxmldchainreq            =   ch2_rxmldchainreq_ext;
assign ch2_rxmlfinealignreq         =   ch2_rxmlfinealignreq_ext;
assign ch2_rxoobreset               =   ch2_rxoobreset_ext;
assign ch2_rxphalignreq             =   ch2_rxphalignreq_ext;
assign ch2_rxphdlypd                =   ch2_rxphdlypd_ext;
assign ch2_rxphdlyreset             =   ch2_rxphdlyreset_ext;
assign ch2_rxphsetinitreq           =   ch2_rxphsetinitreq_ext;
assign ch2_rxphshift180             =   ch2_rxphshift180_ext;
assign ch2_rxpolarity               =   ch2_rxpolarity_ext;
assign ch2_rxprbscntreset           =   ch2_rxprbscntreset_ext;
assign ch2_rxslide                  =   ch2_rxslide_ext;
assign ch2_rxsyncallin              =   ch2_rxsyncallin_ext;
assign ch2_rxtermination            =   ch2_rxtermination_ext;
assign ch2_rxpd                     =   ch2_rxpd_ext;
assign ch2_rxphalignresetmask       =   ch2_rxphalignresetmask_ext;
assign ch2_rxresetmode              =   ch2_rxresetmode_ext;
assign ch2_rxprbssel                =   ch2_rxprbssel_ext;
assign ch2_rxchbondi                =   ch2_rxchbondi_ext;
assign ch2_rxpcsresetmask           =   ch2_rxpcsresetmask_ext;
assign ch2_rxpmaresetmask           =   ch2_rxpmaresetmask_ext;
assign ch2_rxprogdivresetdone_ext   =   ch2_rxprogdivresetdone;
assign ch2_rxpmaresetdone_ext       =   ch2_rxpmaresetdone;
assign ch2_rxresetdone_ext          =   ch2_rxresetdone;

assign ch2_cdrbmcdrreq              =   ch2_cdrbmcdrreq_ext;
assign ch2_cdrfreqos                =   ch2_cdrfreqos_ext; 
assign ch2_cdrincpctrl              =   ch2_cdrincpctrl_ext; 
assign ch2_cdrstepdir               =   ch2_cdrstepdir_ext; 
assign ch2_cdrstepsq                =   ch2_cdrstepsq_ext; 
assign ch2_cdrstepsx                =   ch2_cdrstepsx_ext; 
assign ch2_cfokovrdfinish           =   ch2_cfokovrdfinish_ext; 
assign ch2_cfokovrdpulse            =   ch2_cfokovrdpulse_ext; 
assign ch2_cfokovrdstart            =   ch2_cfokovrdstart_ext; 
assign ch2_eyescanreset             =   ch2_eyescanreset_ext; 
assign ch2_eyescantrigger           =   ch2_eyescantrigger_ext; 
assign ch2_eyescandataerror_ext     =   ch2_eyescandataerror; 
assign ch2_cfokovrdrdy0_ext         =   ch2_cfokovrdrdy0;
assign ch2_cfokovrdrdy1_ext         =   ch2_cfokovrdrdy1; 


assign ch3_rxmstresetdone_ext       =   ch3_rxmstresetdone;

assign ch3_rxdata_ext               =   ch3_rxdata;
assign ch3_rxdatavalid_ext          =   ch3_rxdatavalid;
assign ch3_rxheader_ext             =   ch3_rxheader;           
assign ch3_rxgearboxslip            =   ch3_rxgearboxslip_ext;
assign ch3_rxbyteisaligned_ext      =   ch3_rxbyteisaligned;
assign ch3_rxbyterealign_ext        =   ch3_rxbyterealign;
assign ch3_rxcdrlock_ext            =   ch3_rxcdrlock;
assign ch3_rxcdrphdone_ext          =   ch3_rxcdrphdone;
assign ch3_rxchanbondseq_ext        =   ch3_rxchanbondseq;
assign ch3_rxchanisaligned_ext      =   ch3_rxchanisaligned;
assign ch3_rxchanrealign_ext        =   ch3_rxchanrealign;
assign ch3_rxcominitdet_ext         =   ch3_rxcominitdet;
assign ch3_rxcommadet_ext           =   ch3_rxcommadet;
assign ch3_rxcomsasdet_ext          =   ch3_rxcomsasdet;
assign ch3_rxcomwakedet_ext         =   ch3_rxcomwakedet;
assign ch3_rxdccdone_ext            =   ch3_rxdccdone;
assign ch3_rxdlyalignerr_ext        =   ch3_rxdlyalignerr;
assign ch3_rxdlyalignprog_ext       =   ch3_rxdlyalignprog;
assign ch3_rxelecidle_ext           =   ch3_rxelecidle;
assign ch3_rxfinealigndone_ext      =   ch3_rxfinealigndone;
assign ch3_rxosintdone_ext          =   ch3_rxosintdone;
assign ch3_rxosintstarted_ext       =   ch3_rxosintstarted;
assign ch3_rxosintstrobedone_ext    =   ch3_rxosintstrobedone;
assign ch3_rxosintstrobestarted_ext =   ch3_rxosintstrobestarted;
assign ch3_rxphaligndone_ext        =   ch3_rxphaligndone;
assign ch3_rxphalignerr_ext         =   ch3_rxphalignerr;
assign ch3_rxphdlyresetdone_ext     =   ch3_rxphdlyresetdone;
assign ch3_rxphsetinitdone_ext      =   ch3_rxphsetinitdone;
assign ch3_rxphshift180done_ext     =   ch3_rxphshift180done;
assign ch3_rxprbserr_ext            =   ch3_rxprbserr;
assign ch3_rxprbslocked_ext         =   ch3_rxprbslocked;
assign ch3_rxsliderdy_ext           =   ch3_rxsliderdy;
assign ch3_rxsyncdone_ext           =   ch3_rxsyncdone;
assign ch3_rxvalid_ext              =   ch3_rxvalid;
assign ch3_rxctrl0_ext              =   ch3_rxctrl0;
assign ch3_rxctrl1_ext              =   ch3_rxctrl1;
assign ch3_rxclkcorcnt_ext          =   ch3_rxclkcorcnt;
assign ch3_rxheadervalid_ext        =   ch3_rxheadervalid;
assign ch3_rxstartofseq_ext         =   ch3_rxstartofseq;
assign ch3_rxbufstatus_ext          =   ch3_rxbufstatus;
assign ch3_rxstatus_ext             =   ch3_rxstatus;
assign ch3_rxchbondo_ext            =   ch3_rxchbondo;
assign ch3_rx10gstat_ext            =   ch3_rx10gstat;
assign ch3_rxctrl2_ext              =   ch3_rxctrl2;
assign ch3_rxctrl3_ext              =   ch3_rxctrl3;
assign ch3_rxdataextendrsvd_ext     =   ch3_rxdataextendrsvd;      
assign ch3_rxcdrhold                =   ch3_rxcdrhold_ext;
assign ch3_rxcdrovrden              =   ch3_rxcdrovrden_ext;
assign ch3_rxcdrreset               =   ch3_rxcdrreset_ext;
assign ch3_rxdapicodeovrden         =   ch3_rxdapicodeovrden_ext;
assign ch3_rxdapicodereset          =   ch3_rxdapicodereset_ext;
assign ch3_rxdlyalignreq            =   ch3_rxdlyalignreq_ext;
assign ch3_rxeqtraining             =   ch3_rxeqtraining_ext;
assign ch3_rxlpmen                  =   ch3_rxlpmen_ext;
assign ch3_rxmldchaindone           =   ch3_rxmldchaindone_ext;
assign ch3_rxmldchainreq            =   ch3_rxmldchainreq_ext;
assign ch3_rxmlfinealignreq         =   ch3_rxmlfinealignreq_ext;
assign ch3_rxoobreset               =   ch3_rxoobreset_ext;
assign ch3_rxphalignreq             =   ch3_rxphalignreq_ext;
assign ch3_rxphdlypd                =   ch3_rxphdlypd_ext;
assign ch3_rxphdlyreset             =   ch3_rxphdlyreset_ext;
assign ch3_rxphsetinitreq           =   ch3_rxphsetinitreq_ext;
assign ch3_rxphshift180             =   ch3_rxphshift180_ext;
assign ch3_rxpolarity               =   ch3_rxpolarity_ext;
assign ch3_rxprbscntreset           =   ch3_rxprbscntreset_ext;
assign ch3_rxslide                  =   ch3_rxslide_ext;
assign ch3_rxsyncallin              =   ch3_rxsyncallin_ext;
assign ch3_rxtermination            =   ch3_rxtermination_ext;
assign ch3_rxpd                     =   ch3_rxpd_ext;
assign ch3_rxphalignresetmask       =   ch3_rxphalignresetmask_ext;
assign ch3_rxresetmode              =   ch3_rxresetmode_ext;
assign ch3_rxprbssel                =   ch3_rxprbssel_ext;
assign ch3_rxchbondi                =   ch3_rxchbondi_ext;
assign ch3_rxpcsresetmask           =   ch3_rxpcsresetmask_ext;
assign ch3_rxpmaresetmask           =   ch3_rxpmaresetmask_ext;
assign ch3_rxprogdivresetdone_ext   =   ch3_rxprogdivresetdone;
assign ch3_rxpmaresetdone_ext       =   ch3_rxpmaresetdone;
assign ch3_rxresetdone_ext          =   ch3_rxresetdone;

assign ch3_cdrbmcdrreq              =   ch3_cdrbmcdrreq_ext;
assign ch3_cdrfreqos                =   ch3_cdrfreqos_ext; 
assign ch3_cdrincpctrl              =   ch3_cdrincpctrl_ext; 
assign ch3_cdrstepdir               =   ch3_cdrstepdir_ext; 
assign ch3_cdrstepsq                =   ch3_cdrstepsq_ext; 
assign ch3_cdrstepsx                =   ch3_cdrstepsx_ext; 
assign ch3_cfokovrdfinish           =   ch3_cfokovrdfinish_ext; 
assign ch3_cfokovrdpulse            =   ch3_cfokovrdpulse_ext; 
assign ch3_cfokovrdstart            =   ch3_cfokovrdstart_ext; 
assign ch3_eyescanreset             =   ch3_eyescanreset_ext; 
assign ch3_eyescantrigger           =   ch3_eyescantrigger_ext; 
assign ch3_eyescandataerror_ext     =   ch3_eyescandataerror; 
assign ch3_cfokovrdrdy0_ext         =   ch3_cfokovrdrdy0;
assign ch3_cfokovrdrdy1_ext         =   ch3_cfokovrdrdy1; 



endmodule

//------}

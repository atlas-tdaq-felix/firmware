--Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
--Date        : Thu Apr 13 14:28:27 2023
--Host        : PC-22-023 running 64-bit Ubuntu 22.04.2 LTS
--Command     : generate_target transceiver_versal_lpgbt.bd
--Design      : transceiver_versal_lpgbt
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
    use UNISIM.VCOMPONENTS.ALL;
entity transceiver_versal_lpgbt is
    port (
        APB3_INTF_0_paddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
        APB3_INTF_0_penable : in STD_LOGIC;
        APB3_INTF_0_prdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
        APB3_INTF_0_pready : out STD_LOGIC;
        APB3_INTF_0_psel : in STD_LOGIC;
        APB3_INTF_0_pslverr : out STD_LOGIC;
        APB3_INTF_0_pwdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
        APB3_INTF_0_pwrite : in STD_LOGIC;
        GT_REFCLK0_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
        GT_REFCLK0_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
        GT_REFCLK1_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
        GT_REFCLK1_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
        GT_Serial_grx_n : in STD_LOGIC_VECTOR ( 3 downto 0 );
        GT_Serial_grx_p : in STD_LOGIC_VECTOR ( 3 downto 0 );
        GT_Serial_gtx_n : out STD_LOGIC_VECTOR ( 3 downto 0 );
        GT_Serial_gtx_p : out STD_LOGIC_VECTOR ( 3 downto 0 );
        apb3clk_gt_bridge_ip_0 : in STD_LOGIC;
        apb3clk_quad : in STD_LOGIC;
        apb3presetn_0 : in STD_LOGIC;
        apb3presetn_1 : in STD_LOGIC;
        ch0_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
        ch0_rxbyteisaligned_ext_0 : out STD_LOGIC;
        ch0_rxcdrlock_ext_0 : out STD_LOGIC;
        ch0_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
        ch0_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
        ch0_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
        ch0_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
        ch0_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
        ch0_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch0_rxgearboxslip_ext_0 : in STD_LOGIC;
        ch0_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
        ch0_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch0_rxpmaresetdone_ext_0 : out STD_LOGIC;
        ch0_rxpolarity_ext_0 : in STD_LOGIC;
        ch0_rxresetdone_ext_0 : out STD_LOGIC;
        ch0_rxslide_ext_0 : in STD_LOGIC;
        ch0_rxvalid_ext_0 : out STD_LOGIC;
        ch0_txbufstatus_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch0_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
        ch0_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
        ch0_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
        ch0_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
        ch0_txheader_ext_0 : in STD_LOGIC_VECTOR ( 5 downto 0 );
        ch0_txpippmen_ext_0 : in STD_LOGIC;
        ch0_txpippmstepsize_ext_0 : in STD_LOGIC_VECTOR ( 4 downto 0 );
        ch0_txpolarity_ext_0 : in STD_LOGIC;
        ch0_txresetdone_ext_0 : out STD_LOGIC;
        ch0_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
        ch1_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
        ch1_rxbyteisaligned_ext_0 : out STD_LOGIC;
        ch1_rxcdrlock_ext_0 : out STD_LOGIC;
        ch1_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
        ch1_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
        ch1_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
        ch1_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
        ch1_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
        ch1_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch1_rxgearboxslip_ext_0 : in STD_LOGIC;
        ch1_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
        ch1_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch1_rxpmaresetdone_ext_0 : out STD_LOGIC;
        ch1_rxpolarity_ext_0 : in STD_LOGIC;
        ch1_rxresetdone_ext_0 : out STD_LOGIC;
        ch1_rxslide_ext_0 : in STD_LOGIC;
        ch1_rxvalid_ext_0 : out STD_LOGIC;
        ch1_txbufstatus_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch1_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
        ch1_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
        ch1_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
        ch1_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
        ch1_txpippmen_ext_0 : in STD_LOGIC;
        ch1_txpippmstepsize_ext_0 : in STD_LOGIC_VECTOR ( 4 downto 0 );
        ch1_txpolarity_ext_0 : in STD_LOGIC;
        ch1_txresetdone_ext_0 : out STD_LOGIC;
        ch1_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
        ch2_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
        ch2_rxbyteisaligned_ext_0 : out STD_LOGIC;
        ch2_rxcdrlock_ext_0 : out STD_LOGIC;
        ch2_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
        ch2_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
        ch2_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
        ch2_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
        ch2_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
        ch2_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch2_rxgearboxslip_ext_0 : in STD_LOGIC;
        ch2_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
        ch2_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch2_rxpmaresetdone_ext_0 : out STD_LOGIC;
        ch2_rxpolarity_ext_0 : in STD_LOGIC;
        ch2_rxresetdone_ext_0 : out STD_LOGIC;
        ch2_rxslide_ext_0 : in STD_LOGIC;
        ch2_rxvalid_ext_0 : out STD_LOGIC;
        ch2_txbufstatus_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch2_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
        ch2_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
        ch2_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
        ch2_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
        ch2_txpippmen_ext_0 : in STD_LOGIC;
        ch2_txpippmstepsize_ext_0 : in STD_LOGIC_VECTOR ( 4 downto 0 );
        ch2_txpolarity_ext_0 : in STD_LOGIC;
        ch2_txresetdone_ext_0 : out STD_LOGIC;
        ch2_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
        ch3_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
        ch3_rxbyteisaligned_ext_0 : out STD_LOGIC;
        ch3_rxcdrlock_ext_0 : out STD_LOGIC;
        ch3_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
        ch3_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
        ch3_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
        ch3_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
        ch3_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
        ch3_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch3_rxgearboxslip_ext_0 : in STD_LOGIC;
        ch3_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
        ch3_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch3_rxpmaresetdone_ext_0 : out STD_LOGIC;
        ch3_rxpolarity_ext_0 : in STD_LOGIC;
        ch3_rxresetdone_ext_0 : out STD_LOGIC;
        ch3_rxslide_ext_0 : in STD_LOGIC;
        ch3_rxvalid_ext_0 : out STD_LOGIC;
        ch3_txbufstatus_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ch3_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
        ch3_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
        ch3_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
        ch3_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
        ch3_txpippmen_ext_0 : in STD_LOGIC;
        ch3_txpippmstepsize_ext_0 : in STD_LOGIC_VECTOR ( 4 downto 0 );
        ch3_txpolarity_ext_0 : in STD_LOGIC;
        ch3_txresetdone_ext_0 : out STD_LOGIC;
        ch3_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
        gt_reset_gt_bridge_ip_0 : in STD_LOGIC;
        lcpll_lock_gt_bridge_ip_0 : out STD_LOGIC;
        link_status_gt_bridge_ip_0 : out STD_LOGIC;
        rate_sel_gt_bridge_ip_0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
        reset_rx_datapath_in_0 : in STD_LOGIC;
        reset_rx_pll_and_datapath_in_0 : in STD_LOGIC;
        reset_tx_datapath_in_0 : in STD_LOGIC;
        reset_tx_pll_and_datapath_in_0 : in STD_LOGIC;
        rpll_lock_gt_bridge_ip_0 : out STD_LOGIC;
        rx_resetdone_out_gt_bridge_ip_0 : out STD_LOGIC;
        rxusrclk_gt_bridge_ip_0 : out STD_LOGIC;
        tx_resetdone_out_gt_bridge_ip_0 : out STD_LOGIC;
        txusrclk_gt_bridge_ip_0 : out STD_LOGIC
    );
    attribute CORE_GENERATION_INFO : string;
    attribute CORE_GENERATION_INFO of transceiver_versal_lpgbt : entity is "transceiver_versal_lpgbt,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=transceiver_versal_lpgbt,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=15,numReposBlks=15,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}";
    attribute HW_HANDOFF : string;
    attribute HW_HANDOFF of transceiver_versal_lpgbt : entity is "transceiver_versal_lpgbt.hwdef";
end transceiver_versal_lpgbt;

architecture STRUCTURE of transceiver_versal_lpgbt is
    component transceiver_versal_lpgbt_bufg_gt_0 is
        port (
            outclk : in STD_LOGIC;
            gt_bufgtce : in STD_LOGIC;
            gt_bufgtcemask : in STD_LOGIC;
            gt_bufgtclr : in STD_LOGIC;
            gt_bufgtclrmask : in STD_LOGIC;
            gt_bufgtdiv : in STD_LOGIC_VECTOR ( 2 downto 0 );
            usrclk : out STD_LOGIC
        );
    end component transceiver_versal_lpgbt_bufg_gt_0;
    component transceiver_versal_lpgbt_bufg_gt_1_0 is
        port (
            outclk : in STD_LOGIC;
            gt_bufgtce : in STD_LOGIC;
            gt_bufgtcemask : in STD_LOGIC;
            gt_bufgtclr : in STD_LOGIC;
            gt_bufgtclrmask : in STD_LOGIC;
            gt_bufgtdiv : in STD_LOGIC_VECTOR ( 2 downto 0 );
            usrclk : out STD_LOGIC
        );
    end component transceiver_versal_lpgbt_bufg_gt_1_0;
    component transceiver_versal_lpgbt_gt_bridge_ip_0_0 is
        port (
            gt_ilo_reset : out STD_LOGIC;
            gt_pll_reset : out STD_LOGIC;
            ch0_txdata : out STD_LOGIC_VECTOR ( 127 downto 0 );
            ch0_txbufstatus : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_txpmaresetmask : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch0_txpostcursor : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_txprecursor : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_txheader : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch0_txsequence : out STD_LOGIC_VECTOR ( 6 downto 0 );
            ch0_gttxreset : out STD_LOGIC;
            ch0_txprogdivreset : out STD_LOGIC;
            ch0_txuserrdy : out STD_LOGIC;
            ch0_txphalignresetmask : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_txcominit : out STD_LOGIC;
            ch0_txcomsas : out STD_LOGIC;
            ch0_txcomwake : out STD_LOGIC;
            ch0_txdapicodeovrden : out STD_LOGIC;
            ch0_txdapicodereset : out STD_LOGIC;
            ch0_txdetectrx : out STD_LOGIC;
            ch0_txdlyalignreq : out STD_LOGIC;
            ch0_txelecidle : out STD_LOGIC;
            ch0_txinhibit : out STD_LOGIC;
            ch0_txmldchaindone : out STD_LOGIC;
            ch0_txmldchainreq : out STD_LOGIC;
            ch0_txoneszeros : out STD_LOGIC;
            ch0_txpausedelayalign : out STD_LOGIC;
            ch0_txpcsresetmask : out STD_LOGIC;
            ch0_txphalignreq : out STD_LOGIC;
            ch0_txphdlypd : out STD_LOGIC;
            ch0_txphdlyreset : out STD_LOGIC;
            ch0_txphsetinitreq : out STD_LOGIC;
            ch0_txphshift180 : out STD_LOGIC;
            ch0_txpicodeovrden : out STD_LOGIC;
            ch0_txpicodereset : out STD_LOGIC;
            ch0_txpippmen : out STD_LOGIC;
            ch0_txpisopd : out STD_LOGIC;
            ch0_txpolarity : out STD_LOGIC;
            ch0_txprbsforceerr : out STD_LOGIC;
            ch0_txswing : out STD_LOGIC;
            ch0_txsyncallin : out STD_LOGIC;
            ch0_tx10gstat : in STD_LOGIC;
            ch0_txcomfinish : in STD_LOGIC;
            ch0_txdccdone : in STD_LOGIC;
            ch0_txdlyalignerr : in STD_LOGIC;
            ch0_txdlyalignprog : in STD_LOGIC;
            ch0_txphaligndone : in STD_LOGIC;
            ch0_txphalignerr : in STD_LOGIC;
            ch0_txphalignoutrsvd : in STD_LOGIC;
            ch0_txphdlyresetdone : in STD_LOGIC;
            ch0_txphsetinitdone : in STD_LOGIC;
            ch0_txphshift180done : in STD_LOGIC;
            ch0_txsyncdone : in STD_LOGIC;
            ch0_txctrl0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_txctrl1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_txdeemph : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_txpd : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_txresetmode : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_txmstreset : out STD_LOGIC;
            ch0_txmstdatapathreset : out STD_LOGIC;
            ch0_txmstresetdone : in STD_LOGIC;
            ch0_txmargin : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch0_txprbssel : out STD_LOGIC_VECTOR ( 3 downto 0 );
            ch0_txdiffctrl : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_txpippmstepsize : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_txmaincursor : out STD_LOGIC_VECTOR ( 6 downto 0 );
            ch0_txctrl2 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_txdataextendrsvd : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_txrate : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_txprogdivresetdone : in STD_LOGIC;
            ch0_txpmaresetdone : in STD_LOGIC;
            ch0_txresetdone : in STD_LOGIC;
            ch0_txdata_ext : in STD_LOGIC_VECTOR ( 127 downto 0 );
            ch0_txbufstatus_ext : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_txpmaresetmask_ext : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch0_txpostcursor_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_txprecursor_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_txheader_ext : in STD_LOGIC_VECTOR ( 5 downto 0 );
            ch0_txsequence_ext : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch0_txphalignresetmask_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_txcominit_ext : in STD_LOGIC;
            ch0_txcomsas_ext : in STD_LOGIC;
            ch0_txcomwake_ext : in STD_LOGIC;
            ch0_txdapicodeovrden_ext : in STD_LOGIC;
            ch0_txdapicodereset_ext : in STD_LOGIC;
            ch0_txdetectrx_ext : in STD_LOGIC;
            ch0_txdlyalignreq_ext : in STD_LOGIC;
            ch0_txelecidle_ext : in STD_LOGIC;
            ch0_txinhibit_ext : in STD_LOGIC;
            ch0_txmldchaindone_ext : in STD_LOGIC;
            ch0_txmldchainreq_ext : in STD_LOGIC;
            ch0_txoneszeros_ext : in STD_LOGIC;
            ch0_txpausedelayalign_ext : in STD_LOGIC;
            ch0_txpcsresetmask_ext : in STD_LOGIC;
            ch0_txphalignreq_ext : in STD_LOGIC;
            ch0_txphdlypd_ext : in STD_LOGIC;
            ch0_txphdlyreset_ext : in STD_LOGIC;
            ch0_txphsetinitreq_ext : in STD_LOGIC;
            ch0_txphshift180_ext : in STD_LOGIC;
            ch0_txpicodeovrden_ext : in STD_LOGIC;
            ch0_txpicodereset_ext : in STD_LOGIC;
            ch0_txpippmen_ext : in STD_LOGIC;
            ch0_txpisopd_ext : in STD_LOGIC;
            ch0_txpolarity_ext : in STD_LOGIC;
            ch0_txprbsforceerr_ext : in STD_LOGIC;
            ch0_txswing_ext : in STD_LOGIC;
            ch0_txsyncallin_ext : in STD_LOGIC;
            ch0_tx10gstat_ext : out STD_LOGIC;
            ch0_txcomfinish_ext : out STD_LOGIC;
            ch0_txdccdone_ext : out STD_LOGIC;
            ch0_txdlyalignerr_ext : out STD_LOGIC;
            ch0_txdlyalignprog_ext : out STD_LOGIC;
            ch0_txphaligndone_ext : out STD_LOGIC;
            ch0_txphalignerr_ext : out STD_LOGIC;
            ch0_txphalignoutrsvd_ext : out STD_LOGIC;
            ch0_txphdlyresetdone_ext : out STD_LOGIC;
            ch0_txphsetinitdone_ext : out STD_LOGIC;
            ch0_txphshift180done_ext : out STD_LOGIC;
            ch0_txsyncdone_ext : out STD_LOGIC;
            ch0_txctrl0_ext : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_txctrl1_ext : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_txdeemph_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_txpd_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_txresetmode_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_txmstresetdone_ext : out STD_LOGIC;
            ch0_txmargin_ext : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch0_txprbssel_ext : in STD_LOGIC_VECTOR ( 3 downto 0 );
            ch0_txdiffctrl_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_txpippmstepsize_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_txmaincursor_ext : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch0_txctrl2_ext : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_txdataextendrsvd_ext : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_txprogdivresetdone_ext : out STD_LOGIC;
            ch0_txpmaresetdone_ext : out STD_LOGIC;
            ch0_txresetdone_ext : out STD_LOGIC;
            ch1_txdata : out STD_LOGIC_VECTOR ( 127 downto 0 );
            ch1_txbufstatus : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_txpmaresetmask : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch1_txpostcursor : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_txprecursor : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_txheader : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch1_txsequence : out STD_LOGIC_VECTOR ( 6 downto 0 );
            ch1_gttxreset : out STD_LOGIC;
            ch1_txprogdivreset : out STD_LOGIC;
            ch1_txuserrdy : out STD_LOGIC;
            ch1_txphalignresetmask : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_txcominit : out STD_LOGIC;
            ch1_txcomsas : out STD_LOGIC;
            ch1_txcomwake : out STD_LOGIC;
            ch1_txdapicodeovrden : out STD_LOGIC;
            ch1_txdapicodereset : out STD_LOGIC;
            ch1_txdetectrx : out STD_LOGIC;
            ch1_txdlyalignreq : out STD_LOGIC;
            ch1_txelecidle : out STD_LOGIC;
            ch1_txinhibit : out STD_LOGIC;
            ch1_txmldchaindone : out STD_LOGIC;
            ch1_txmldchainreq : out STD_LOGIC;
            ch1_txoneszeros : out STD_LOGIC;
            ch1_txpausedelayalign : out STD_LOGIC;
            ch1_txpcsresetmask : out STD_LOGIC;
            ch1_txphalignreq : out STD_LOGIC;
            ch1_txphdlypd : out STD_LOGIC;
            ch1_txphdlyreset : out STD_LOGIC;
            ch1_txphsetinitreq : out STD_LOGIC;
            ch1_txphshift180 : out STD_LOGIC;
            ch1_txpicodeovrden : out STD_LOGIC;
            ch1_txpicodereset : out STD_LOGIC;
            ch1_txpippmen : out STD_LOGIC;
            ch1_txpisopd : out STD_LOGIC;
            ch1_txpolarity : out STD_LOGIC;
            ch1_txprbsforceerr : out STD_LOGIC;
            ch1_txswing : out STD_LOGIC;
            ch1_txsyncallin : out STD_LOGIC;
            ch1_tx10gstat : in STD_LOGIC;
            ch1_txcomfinish : in STD_LOGIC;
            ch1_txdccdone : in STD_LOGIC;
            ch1_txdlyalignerr : in STD_LOGIC;
            ch1_txdlyalignprog : in STD_LOGIC;
            ch1_txphaligndone : in STD_LOGIC;
            ch1_txphalignerr : in STD_LOGIC;
            ch1_txphalignoutrsvd : in STD_LOGIC;
            ch1_txphdlyresetdone : in STD_LOGIC;
            ch1_txphsetinitdone : in STD_LOGIC;
            ch1_txphshift180done : in STD_LOGIC;
            ch1_txsyncdone : in STD_LOGIC;
            ch1_txctrl0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_txctrl1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_txdeemph : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_txpd : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_txresetmode : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_txmstreset : out STD_LOGIC;
            ch1_txmstdatapathreset : out STD_LOGIC;
            ch1_txmstresetdone : in STD_LOGIC;
            ch1_txmargin : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch1_txprbssel : out STD_LOGIC_VECTOR ( 3 downto 0 );
            ch1_txdiffctrl : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_txpippmstepsize : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_txmaincursor : out STD_LOGIC_VECTOR ( 6 downto 0 );
            ch1_txctrl2 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_txdataextendrsvd : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_txrate : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_txprogdivresetdone : in STD_LOGIC;
            ch1_txpmaresetdone : in STD_LOGIC;
            ch1_txresetdone : in STD_LOGIC;
            ch1_txdata_ext : in STD_LOGIC_VECTOR ( 127 downto 0 );
            ch1_txbufstatus_ext : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_txpmaresetmask_ext : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch1_txpostcursor_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_txprecursor_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_txheader_ext : in STD_LOGIC_VECTOR ( 5 downto 0 );
            ch1_txsequence_ext : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch1_txphalignresetmask_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_txcominit_ext : in STD_LOGIC;
            ch1_txcomsas_ext : in STD_LOGIC;
            ch1_txcomwake_ext : in STD_LOGIC;
            ch1_txdapicodeovrden_ext : in STD_LOGIC;
            ch1_txdapicodereset_ext : in STD_LOGIC;
            ch1_txdetectrx_ext : in STD_LOGIC;
            ch1_txdlyalignreq_ext : in STD_LOGIC;
            ch1_txelecidle_ext : in STD_LOGIC;
            ch1_txinhibit_ext : in STD_LOGIC;
            ch1_txmldchaindone_ext : in STD_LOGIC;
            ch1_txmldchainreq_ext : in STD_LOGIC;
            ch1_txoneszeros_ext : in STD_LOGIC;
            ch1_txpausedelayalign_ext : in STD_LOGIC;
            ch1_txpcsresetmask_ext : in STD_LOGIC;
            ch1_txphalignreq_ext : in STD_LOGIC;
            ch1_txphdlypd_ext : in STD_LOGIC;
            ch1_txphdlyreset_ext : in STD_LOGIC;
            ch1_txphsetinitreq_ext : in STD_LOGIC;
            ch1_txphshift180_ext : in STD_LOGIC;
            ch1_txpicodeovrden_ext : in STD_LOGIC;
            ch1_txpicodereset_ext : in STD_LOGIC;
            ch1_txpippmen_ext : in STD_LOGIC;
            ch1_txpisopd_ext : in STD_LOGIC;
            ch1_txpolarity_ext : in STD_LOGIC;
            ch1_txprbsforceerr_ext : in STD_LOGIC;
            ch1_txswing_ext : in STD_LOGIC;
            ch1_txsyncallin_ext : in STD_LOGIC;
            ch1_tx10gstat_ext : out STD_LOGIC;
            ch1_txcomfinish_ext : out STD_LOGIC;
            ch1_txdccdone_ext : out STD_LOGIC;
            ch1_txdlyalignerr_ext : out STD_LOGIC;
            ch1_txdlyalignprog_ext : out STD_LOGIC;
            ch1_txphaligndone_ext : out STD_LOGIC;
            ch1_txphalignerr_ext : out STD_LOGIC;
            ch1_txphalignoutrsvd_ext : out STD_LOGIC;
            ch1_txphdlyresetdone_ext : out STD_LOGIC;
            ch1_txphsetinitdone_ext : out STD_LOGIC;
            ch1_txphshift180done_ext : out STD_LOGIC;
            ch1_txsyncdone_ext : out STD_LOGIC;
            ch1_txctrl0_ext : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_txctrl1_ext : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_txdeemph_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_txpd_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_txresetmode_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_txmstresetdone_ext : out STD_LOGIC;
            ch1_txmargin_ext : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch1_txprbssel_ext : in STD_LOGIC_VECTOR ( 3 downto 0 );
            ch1_txdiffctrl_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_txpippmstepsize_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_txmaincursor_ext : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch1_txctrl2_ext : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_txdataextendrsvd_ext : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_txprogdivresetdone_ext : out STD_LOGIC;
            ch1_txpmaresetdone_ext : out STD_LOGIC;
            ch1_txresetdone_ext : out STD_LOGIC;
            ch2_txdata : out STD_LOGIC_VECTOR ( 127 downto 0 );
            ch2_txbufstatus : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_txpmaresetmask : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch2_txpostcursor : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_txprecursor : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_txheader : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch2_txsequence : out STD_LOGIC_VECTOR ( 6 downto 0 );
            ch2_gttxreset : out STD_LOGIC;
            ch2_txprogdivreset : out STD_LOGIC;
            ch2_txuserrdy : out STD_LOGIC;
            ch2_txphalignresetmask : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_txcominit : out STD_LOGIC;
            ch2_txcomsas : out STD_LOGIC;
            ch2_txcomwake : out STD_LOGIC;
            ch2_txdapicodeovrden : out STD_LOGIC;
            ch2_txdapicodereset : out STD_LOGIC;
            ch2_txdetectrx : out STD_LOGIC;
            ch2_txdlyalignreq : out STD_LOGIC;
            ch2_txelecidle : out STD_LOGIC;
            ch2_txinhibit : out STD_LOGIC;
            ch2_txmldchaindone : out STD_LOGIC;
            ch2_txmldchainreq : out STD_LOGIC;
            ch2_txoneszeros : out STD_LOGIC;
            ch2_txpausedelayalign : out STD_LOGIC;
            ch2_txpcsresetmask : out STD_LOGIC;
            ch2_txphalignreq : out STD_LOGIC;
            ch2_txphdlypd : out STD_LOGIC;
            ch2_txphdlyreset : out STD_LOGIC;
            ch2_txphsetinitreq : out STD_LOGIC;
            ch2_txphshift180 : out STD_LOGIC;
            ch2_txpicodeovrden : out STD_LOGIC;
            ch2_txpicodereset : out STD_LOGIC;
            ch2_txpippmen : out STD_LOGIC;
            ch2_txpisopd : out STD_LOGIC;
            ch2_txpolarity : out STD_LOGIC;
            ch2_txprbsforceerr : out STD_LOGIC;
            ch2_txswing : out STD_LOGIC;
            ch2_txsyncallin : out STD_LOGIC;
            ch2_tx10gstat : in STD_LOGIC;
            ch2_txcomfinish : in STD_LOGIC;
            ch2_txdccdone : in STD_LOGIC;
            ch2_txdlyalignerr : in STD_LOGIC;
            ch2_txdlyalignprog : in STD_LOGIC;
            ch2_txphaligndone : in STD_LOGIC;
            ch2_txphalignerr : in STD_LOGIC;
            ch2_txphalignoutrsvd : in STD_LOGIC;
            ch2_txphdlyresetdone : in STD_LOGIC;
            ch2_txphsetinitdone : in STD_LOGIC;
            ch2_txphshift180done : in STD_LOGIC;
            ch2_txsyncdone : in STD_LOGIC;
            ch2_txctrl0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_txctrl1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_txdeemph : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_txpd : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_txresetmode : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_txmstreset : out STD_LOGIC;
            ch2_txmstdatapathreset : out STD_LOGIC;
            ch2_txmstresetdone : in STD_LOGIC;
            ch2_txmargin : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch2_txprbssel : out STD_LOGIC_VECTOR ( 3 downto 0 );
            ch2_txdiffctrl : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_txpippmstepsize : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_txmaincursor : out STD_LOGIC_VECTOR ( 6 downto 0 );
            ch2_txctrl2 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_txdataextendrsvd : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_txrate : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_txprogdivresetdone : in STD_LOGIC;
            ch2_txpmaresetdone : in STD_LOGIC;
            ch2_txresetdone : in STD_LOGIC;
            ch2_txdata_ext : in STD_LOGIC_VECTOR ( 127 downto 0 );
            ch2_txbufstatus_ext : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_txpmaresetmask_ext : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch2_txpostcursor_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_txprecursor_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_txheader_ext : in STD_LOGIC_VECTOR ( 5 downto 0 );
            ch2_txsequence_ext : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch2_txphalignresetmask_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_txcominit_ext : in STD_LOGIC;
            ch2_txcomsas_ext : in STD_LOGIC;
            ch2_txcomwake_ext : in STD_LOGIC;
            ch2_txdapicodeovrden_ext : in STD_LOGIC;
            ch2_txdapicodereset_ext : in STD_LOGIC;
            ch2_txdetectrx_ext : in STD_LOGIC;
            ch2_txdlyalignreq_ext : in STD_LOGIC;
            ch2_txelecidle_ext : in STD_LOGIC;
            ch2_txinhibit_ext : in STD_LOGIC;
            ch2_txmldchaindone_ext : in STD_LOGIC;
            ch2_txmldchainreq_ext : in STD_LOGIC;
            ch2_txoneszeros_ext : in STD_LOGIC;
            ch2_txpausedelayalign_ext : in STD_LOGIC;
            ch2_txpcsresetmask_ext : in STD_LOGIC;
            ch2_txphalignreq_ext : in STD_LOGIC;
            ch2_txphdlypd_ext : in STD_LOGIC;
            ch2_txphdlyreset_ext : in STD_LOGIC;
            ch2_txphsetinitreq_ext : in STD_LOGIC;
            ch2_txphshift180_ext : in STD_LOGIC;
            ch2_txpicodeovrden_ext : in STD_LOGIC;
            ch2_txpicodereset_ext : in STD_LOGIC;
            ch2_txpippmen_ext : in STD_LOGIC;
            ch2_txpisopd_ext : in STD_LOGIC;
            ch2_txpolarity_ext : in STD_LOGIC;
            ch2_txprbsforceerr_ext : in STD_LOGIC;
            ch2_txswing_ext : in STD_LOGIC;
            ch2_txsyncallin_ext : in STD_LOGIC;
            ch2_tx10gstat_ext : out STD_LOGIC;
            ch2_txcomfinish_ext : out STD_LOGIC;
            ch2_txdccdone_ext : out STD_LOGIC;
            ch2_txdlyalignerr_ext : out STD_LOGIC;
            ch2_txdlyalignprog_ext : out STD_LOGIC;
            ch2_txphaligndone_ext : out STD_LOGIC;
            ch2_txphalignerr_ext : out STD_LOGIC;
            ch2_txphalignoutrsvd_ext : out STD_LOGIC;
            ch2_txphdlyresetdone_ext : out STD_LOGIC;
            ch2_txphsetinitdone_ext : out STD_LOGIC;
            ch2_txphshift180done_ext : out STD_LOGIC;
            ch2_txsyncdone_ext : out STD_LOGIC;
            ch2_txctrl0_ext : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_txctrl1_ext : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_txdeemph_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_txpd_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_txresetmode_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_txmstresetdone_ext : out STD_LOGIC;
            ch2_txmargin_ext : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch2_txprbssel_ext : in STD_LOGIC_VECTOR ( 3 downto 0 );
            ch2_txdiffctrl_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_txpippmstepsize_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_txmaincursor_ext : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch2_txctrl2_ext : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_txdataextendrsvd_ext : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_txprogdivresetdone_ext : out STD_LOGIC;
            ch2_txpmaresetdone_ext : out STD_LOGIC;
            ch2_txresetdone_ext : out STD_LOGIC;
            ch3_txdata : out STD_LOGIC_VECTOR ( 127 downto 0 );
            ch3_txbufstatus : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_txpmaresetmask : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch3_txpostcursor : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_txprecursor : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_txheader : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch3_txsequence : out STD_LOGIC_VECTOR ( 6 downto 0 );
            ch3_gttxreset : out STD_LOGIC;
            ch3_txprogdivreset : out STD_LOGIC;
            ch3_txuserrdy : out STD_LOGIC;
            ch3_txphalignresetmask : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_txcominit : out STD_LOGIC;
            ch3_txcomsas : out STD_LOGIC;
            ch3_txcomwake : out STD_LOGIC;
            ch3_txdapicodeovrden : out STD_LOGIC;
            ch3_txdapicodereset : out STD_LOGIC;
            ch3_txdetectrx : out STD_LOGIC;
            ch3_txdlyalignreq : out STD_LOGIC;
            ch3_txelecidle : out STD_LOGIC;
            ch3_txinhibit : out STD_LOGIC;
            ch3_txmldchaindone : out STD_LOGIC;
            ch3_txmldchainreq : out STD_LOGIC;
            ch3_txoneszeros : out STD_LOGIC;
            ch3_txpausedelayalign : out STD_LOGIC;
            ch3_txpcsresetmask : out STD_LOGIC;
            ch3_txphalignreq : out STD_LOGIC;
            ch3_txphdlypd : out STD_LOGIC;
            ch3_txphdlyreset : out STD_LOGIC;
            ch3_txphsetinitreq : out STD_LOGIC;
            ch3_txphshift180 : out STD_LOGIC;
            ch3_txpicodeovrden : out STD_LOGIC;
            ch3_txpicodereset : out STD_LOGIC;
            ch3_txpippmen : out STD_LOGIC;
            ch3_txpisopd : out STD_LOGIC;
            ch3_txpolarity : out STD_LOGIC;
            ch3_txprbsforceerr : out STD_LOGIC;
            ch3_txswing : out STD_LOGIC;
            ch3_txsyncallin : out STD_LOGIC;
            ch3_tx10gstat : in STD_LOGIC;
            ch3_txcomfinish : in STD_LOGIC;
            ch3_txdccdone : in STD_LOGIC;
            ch3_txdlyalignerr : in STD_LOGIC;
            ch3_txdlyalignprog : in STD_LOGIC;
            ch3_txphaligndone : in STD_LOGIC;
            ch3_txphalignerr : in STD_LOGIC;
            ch3_txphalignoutrsvd : in STD_LOGIC;
            ch3_txphdlyresetdone : in STD_LOGIC;
            ch3_txphsetinitdone : in STD_LOGIC;
            ch3_txphshift180done : in STD_LOGIC;
            ch3_txsyncdone : in STD_LOGIC;
            ch3_txctrl0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_txctrl1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_txdeemph : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_txpd : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_txresetmode : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_txmstreset : out STD_LOGIC;
            ch3_txmstdatapathreset : out STD_LOGIC;
            ch3_txmstresetdone : in STD_LOGIC;
            ch3_txmargin : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch3_txprbssel : out STD_LOGIC_VECTOR ( 3 downto 0 );
            ch3_txdiffctrl : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_txpippmstepsize : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_txmaincursor : out STD_LOGIC_VECTOR ( 6 downto 0 );
            ch3_txctrl2 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_txdataextendrsvd : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_txrate : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_txprogdivresetdone : in STD_LOGIC;
            ch3_txpmaresetdone : in STD_LOGIC;
            ch3_txresetdone : in STD_LOGIC;
            ch3_txdata_ext : in STD_LOGIC_VECTOR ( 127 downto 0 );
            ch3_txbufstatus_ext : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_txpmaresetmask_ext : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch3_txpostcursor_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_txprecursor_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_txheader_ext : in STD_LOGIC_VECTOR ( 5 downto 0 );
            ch3_txsequence_ext : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch3_txphalignresetmask_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_txcominit_ext : in STD_LOGIC;
            ch3_txcomsas_ext : in STD_LOGIC;
            ch3_txcomwake_ext : in STD_LOGIC;
            ch3_txdapicodeovrden_ext : in STD_LOGIC;
            ch3_txdapicodereset_ext : in STD_LOGIC;
            ch3_txdetectrx_ext : in STD_LOGIC;
            ch3_txdlyalignreq_ext : in STD_LOGIC;
            ch3_txelecidle_ext : in STD_LOGIC;
            ch3_txinhibit_ext : in STD_LOGIC;
            ch3_txmldchaindone_ext : in STD_LOGIC;
            ch3_txmldchainreq_ext : in STD_LOGIC;
            ch3_txoneszeros_ext : in STD_LOGIC;
            ch3_txpausedelayalign_ext : in STD_LOGIC;
            ch3_txpcsresetmask_ext : in STD_LOGIC;
            ch3_txphalignreq_ext : in STD_LOGIC;
            ch3_txphdlypd_ext : in STD_LOGIC;
            ch3_txphdlyreset_ext : in STD_LOGIC;
            ch3_txphsetinitreq_ext : in STD_LOGIC;
            ch3_txphshift180_ext : in STD_LOGIC;
            ch3_txpicodeovrden_ext : in STD_LOGIC;
            ch3_txpicodereset_ext : in STD_LOGIC;
            ch3_txpippmen_ext : in STD_LOGIC;
            ch3_txpisopd_ext : in STD_LOGIC;
            ch3_txpolarity_ext : in STD_LOGIC;
            ch3_txprbsforceerr_ext : in STD_LOGIC;
            ch3_txswing_ext : in STD_LOGIC;
            ch3_txsyncallin_ext : in STD_LOGIC;
            ch3_tx10gstat_ext : out STD_LOGIC;
            ch3_txcomfinish_ext : out STD_LOGIC;
            ch3_txdccdone_ext : out STD_LOGIC;
            ch3_txdlyalignerr_ext : out STD_LOGIC;
            ch3_txdlyalignprog_ext : out STD_LOGIC;
            ch3_txphaligndone_ext : out STD_LOGIC;
            ch3_txphalignerr_ext : out STD_LOGIC;
            ch3_txphalignoutrsvd_ext : out STD_LOGIC;
            ch3_txphdlyresetdone_ext : out STD_LOGIC;
            ch3_txphsetinitdone_ext : out STD_LOGIC;
            ch3_txphshift180done_ext : out STD_LOGIC;
            ch3_txsyncdone_ext : out STD_LOGIC;
            ch3_txctrl0_ext : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_txctrl1_ext : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_txdeemph_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_txpd_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_txresetmode_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_txmstresetdone_ext : out STD_LOGIC;
            ch3_txmargin_ext : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch3_txprbssel_ext : in STD_LOGIC_VECTOR ( 3 downto 0 );
            ch3_txdiffctrl_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_txpippmstepsize_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_txmaincursor_ext : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch3_txctrl2_ext : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_txdataextendrsvd_ext : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_txprogdivresetdone_ext : out STD_LOGIC;
            ch3_txpmaresetdone_ext : out STD_LOGIC;
            ch3_txresetdone_ext : out STD_LOGIC;
            ch0_rxdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
            ch0_rxpcsresetmask : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_rxpmaresetmask : out STD_LOGIC_VECTOR ( 6 downto 0 );
            ch0_rxdatavalid : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxheader : in STD_LOGIC_VECTOR ( 5 downto 0 );
            ch0_rxgearboxslip : out STD_LOGIC;
            ch0_gtrxreset : out STD_LOGIC;
            ch0_rxprogdivreset : out STD_LOGIC;
            ch0_rxuserrdy : out STD_LOGIC;
            ch0_rxprogdivresetdone : in STD_LOGIC;
            ch0_rxpmaresetdone : in STD_LOGIC;
            ch0_rxresetdone : in STD_LOGIC;
            ch0_rx10gstat : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_rxbufstatus : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch0_rxbyteisaligned : in STD_LOGIC;
            ch0_rxbyterealign : in STD_LOGIC;
            ch0_rxcdrhold : out STD_LOGIC;
            ch0_rxcdrlock : in STD_LOGIC;
            ch0_rxcdrovrden : out STD_LOGIC;
            ch0_rxcdrphdone : in STD_LOGIC;
            ch0_rxcdrreset : out STD_LOGIC;
            ch0_rxchanbondseq : in STD_LOGIC;
            ch0_rxchanisaligned : in STD_LOGIC;
            ch0_rxchanrealign : in STD_LOGIC;
            ch0_rxchbondi : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_rxchbondo : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_rxclkcorcnt : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxcominitdet : in STD_LOGIC;
            ch0_rxcommadet : in STD_LOGIC;
            ch0_rxcomsasdet : in STD_LOGIC;
            ch0_rxcomwakedet : in STD_LOGIC;
            ch0_rxctrl0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_rxctrl1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_rxctrl2 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_rxctrl3 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_rxdapicodeovrden : out STD_LOGIC;
            ch0_rxdapicodereset : out STD_LOGIC;
            ch0_rxdataextendrsvd : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_rxdccdone : in STD_LOGIC;
            ch0_rxdlyalignerr : in STD_LOGIC;
            ch0_rxdlyalignprog : in STD_LOGIC;
            ch0_rxdlyalignreq : out STD_LOGIC;
            ch0_rxelecidle : in STD_LOGIC;
            ch0_rxeqtraining : out STD_LOGIC;
            ch0_rxfinealigndone : in STD_LOGIC;
            ch0_rxheadervalid : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxlpmen : out STD_LOGIC;
            ch0_rxmldchaindone : out STD_LOGIC;
            ch0_rxmldchainreq : out STD_LOGIC;
            ch0_rxmlfinealignreq : out STD_LOGIC;
            ch0_rxoobreset : out STD_LOGIC;
            ch0_rxosintdone : in STD_LOGIC;
            ch0_rxosintstarted : in STD_LOGIC;
            ch0_rxosintstrobedone : in STD_LOGIC;
            ch0_rxosintstrobestarted : in STD_LOGIC;
            ch0_rxpd : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxphaligndone : in STD_LOGIC;
            ch0_rxphalignerr : in STD_LOGIC;
            ch0_rxphalignreq : out STD_LOGIC;
            ch0_rxphalignresetmask : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxphdlypd : out STD_LOGIC;
            ch0_rxphdlyreset : out STD_LOGIC;
            ch0_rxphdlyresetdone : in STD_LOGIC;
            ch0_rxphsetinitdone : in STD_LOGIC;
            ch0_rxphsetinitreq : out STD_LOGIC;
            ch0_rxphshift180 : out STD_LOGIC;
            ch0_rxphshift180done : in STD_LOGIC;
            ch0_rxpolarity : out STD_LOGIC;
            ch0_rxprbscntreset : out STD_LOGIC;
            ch0_rxprbserr : in STD_LOGIC;
            ch0_rxprbslocked : in STD_LOGIC;
            ch0_rxprbssel : out STD_LOGIC_VECTOR ( 3 downto 0 );
            ch0_rxrate : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_rxresetmode : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxmstreset : out STD_LOGIC;
            ch0_rxmstdatapathreset : out STD_LOGIC;
            ch0_rxmstresetdone : in STD_LOGIC;
            ch0_rxslide : out STD_LOGIC;
            ch0_rxsliderdy : in STD_LOGIC;
            ch0_rxstartofseq : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxstatus : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch0_rxsyncallin : out STD_LOGIC;
            ch0_rxsyncdone : in STD_LOGIC;
            ch0_rxtermination : out STD_LOGIC;
            ch0_rxvalid : in STD_LOGIC;
            ch0_cdrbmcdrreq : out STD_LOGIC;
            ch0_cdrfreqos : out STD_LOGIC;
            ch0_cdrincpctrl : out STD_LOGIC;
            ch0_cdrstepdir : out STD_LOGIC;
            ch0_cdrstepsq : out STD_LOGIC;
            ch0_cdrstepsx : out STD_LOGIC;
            ch0_cfokovrdfinish : out STD_LOGIC;
            ch0_cfokovrdpulse : out STD_LOGIC;
            ch0_cfokovrdstart : out STD_LOGIC;
            ch0_eyescanreset : out STD_LOGIC;
            ch0_eyescantrigger : out STD_LOGIC;
            ch0_eyescandataerror : in STD_LOGIC;
            ch0_cfokovrdrdy0 : in STD_LOGIC;
            ch0_cfokovrdrdy1 : in STD_LOGIC;
            ch0_rxdata_ext : out STD_LOGIC_VECTOR ( 127 downto 0 );
            ch0_rxpcsresetmask_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_rxpmaresetmask_ext : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch0_rxdatavalid_ext : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxheader_ext : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch0_rxgearboxslip_ext : in STD_LOGIC;
            ch0_rxprogdivresetdone_ext : out STD_LOGIC;
            ch0_rxpmaresetdone_ext : out STD_LOGIC;
            ch0_rxresetdone_ext : out STD_LOGIC;
            ch0_rx10gstat_ext : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_rxbufstatus_ext : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch0_rxbyteisaligned_ext : out STD_LOGIC;
            ch0_rxbyterealign_ext : out STD_LOGIC;
            ch0_rxcdrhold_ext : in STD_LOGIC;
            ch0_rxcdrlock_ext : out STD_LOGIC;
            ch0_rxcdrovrden_ext : in STD_LOGIC;
            ch0_rxcdrphdone_ext : out STD_LOGIC;
            ch0_rxcdrreset_ext : in STD_LOGIC;
            ch0_rxchanbondseq_ext : out STD_LOGIC;
            ch0_rxchanisaligned_ext : out STD_LOGIC;
            ch0_rxchanrealign_ext : out STD_LOGIC;
            ch0_rxchbondi_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_rxchbondo_ext : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_rxclkcorcnt_ext : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxcominitdet_ext : out STD_LOGIC;
            ch0_rxcommadet_ext : out STD_LOGIC;
            ch0_rxcomsasdet_ext : out STD_LOGIC;
            ch0_rxcomwakedet_ext : out STD_LOGIC;
            ch0_rxctrl0_ext : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_rxctrl1_ext : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_rxctrl2_ext : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_rxctrl3_ext : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_rxdapicodeovrden_ext : in STD_LOGIC;
            ch0_rxdapicodereset_ext : in STD_LOGIC;
            ch0_rxdataextendrsvd_ext : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_rxdccdone_ext : out STD_LOGIC;
            ch0_rxdlyalignerr_ext : out STD_LOGIC;
            ch0_rxdlyalignprog_ext : out STD_LOGIC;
            ch0_rxdlyalignreq_ext : in STD_LOGIC;
            ch0_rxelecidle_ext : out STD_LOGIC;
            ch0_rxeqtraining_ext : in STD_LOGIC;
            ch0_rxfinealigndone_ext : out STD_LOGIC;
            ch0_rxheadervalid_ext : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxlpmen_ext : in STD_LOGIC;
            ch0_rxmldchaindone_ext : in STD_LOGIC;
            ch0_rxmldchainreq_ext : in STD_LOGIC;
            ch0_rxmlfinealignreq_ext : in STD_LOGIC;
            ch0_rxoobreset_ext : in STD_LOGIC;
            ch0_rxosintdone_ext : out STD_LOGIC;
            ch0_rxosintstarted_ext : out STD_LOGIC;
            ch0_rxosintstrobedone_ext : out STD_LOGIC;
            ch0_rxosintstrobestarted_ext : out STD_LOGIC;
            ch0_rxpd_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxphaligndone_ext : out STD_LOGIC;
            ch0_rxphalignerr_ext : out STD_LOGIC;
            ch0_rxphalignreq_ext : in STD_LOGIC;
            ch0_rxphalignresetmask_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxphdlypd_ext : in STD_LOGIC;
            ch0_rxphdlyreset_ext : in STD_LOGIC;
            ch0_rxphdlyresetdone_ext : out STD_LOGIC;
            ch0_rxphsetinitdone_ext : out STD_LOGIC;
            ch0_rxphsetinitreq_ext : in STD_LOGIC;
            ch0_rxphshift180_ext : in STD_LOGIC;
            ch0_rxphshift180done_ext : out STD_LOGIC;
            ch0_rxpolarity_ext : in STD_LOGIC;
            ch0_rxprbscntreset_ext : in STD_LOGIC;
            ch0_rxprbserr_ext : out STD_LOGIC;
            ch0_rxprbslocked_ext : out STD_LOGIC;
            ch0_rxprbssel_ext : in STD_LOGIC_VECTOR ( 3 downto 0 );
            ch0_rxresetmode_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxmstresetdone_ext : out STD_LOGIC;
            ch0_rxslide_ext : in STD_LOGIC;
            ch0_rxsliderdy_ext : out STD_LOGIC;
            ch0_rxstartofseq_ext : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxstatus_ext : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch0_rxsyncallin_ext : in STD_LOGIC;
            ch0_rxsyncdone_ext : out STD_LOGIC;
            ch0_rxtermination_ext : in STD_LOGIC;
            ch0_rxvalid_ext : out STD_LOGIC;
            ch0_cdrbmcdrreq_ext : in STD_LOGIC;
            ch0_cdrfreqos_ext : in STD_LOGIC;
            ch0_cdrincpctrl_ext : in STD_LOGIC;
            ch0_cdrstepdir_ext : in STD_LOGIC;
            ch0_cdrstepsq_ext : in STD_LOGIC;
            ch0_cdrstepsx_ext : in STD_LOGIC;
            ch0_cfokovrdfinish_ext : in STD_LOGIC;
            ch0_cfokovrdpulse_ext : in STD_LOGIC;
            ch0_cfokovrdstart_ext : in STD_LOGIC;
            ch0_eyescanreset_ext : in STD_LOGIC;
            ch0_eyescantrigger_ext : in STD_LOGIC;
            ch0_eyescandataerror_ext : out STD_LOGIC;
            ch0_cfokovrdrdy0_ext : out STD_LOGIC;
            ch0_cfokovrdrdy1_ext : out STD_LOGIC;
            ch1_rxdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
            ch1_rxpcsresetmask : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_rxpmaresetmask : out STD_LOGIC_VECTOR ( 6 downto 0 );
            ch1_rxdatavalid : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxheader : in STD_LOGIC_VECTOR ( 5 downto 0 );
            ch1_rxgearboxslip : out STD_LOGIC;
            ch1_gtrxreset : out STD_LOGIC;
            ch1_rxprogdivreset : out STD_LOGIC;
            ch1_rxuserrdy : out STD_LOGIC;
            ch1_rxprogdivresetdone : in STD_LOGIC;
            ch1_rxpmaresetdone : in STD_LOGIC;
            ch1_rxresetdone : in STD_LOGIC;
            ch1_rx10gstat : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_rxbufstatus : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch1_rxbyteisaligned : in STD_LOGIC;
            ch1_rxbyterealign : in STD_LOGIC;
            ch1_rxcdrhold : out STD_LOGIC;
            ch1_rxcdrlock : in STD_LOGIC;
            ch1_rxcdrovrden : out STD_LOGIC;
            ch1_rxcdrphdone : in STD_LOGIC;
            ch1_rxcdrreset : out STD_LOGIC;
            ch1_rxchanbondseq : in STD_LOGIC;
            ch1_rxchanisaligned : in STD_LOGIC;
            ch1_rxchanrealign : in STD_LOGIC;
            ch1_rxchbondi : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_rxchbondo : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_rxclkcorcnt : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxcominitdet : in STD_LOGIC;
            ch1_rxcommadet : in STD_LOGIC;
            ch1_rxcomsasdet : in STD_LOGIC;
            ch1_rxcomwakedet : in STD_LOGIC;
            ch1_rxctrl0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_rxctrl1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_rxctrl2 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_rxctrl3 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_rxdapicodeovrden : out STD_LOGIC;
            ch1_rxdapicodereset : out STD_LOGIC;
            ch1_rxdataextendrsvd : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_rxdccdone : in STD_LOGIC;
            ch1_rxdlyalignerr : in STD_LOGIC;
            ch1_rxdlyalignprog : in STD_LOGIC;
            ch1_rxdlyalignreq : out STD_LOGIC;
            ch1_rxelecidle : in STD_LOGIC;
            ch1_rxeqtraining : out STD_LOGIC;
            ch1_rxfinealigndone : in STD_LOGIC;
            ch1_rxheadervalid : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxlpmen : out STD_LOGIC;
            ch1_rxmldchaindone : out STD_LOGIC;
            ch1_rxmldchainreq : out STD_LOGIC;
            ch1_rxmlfinealignreq : out STD_LOGIC;
            ch1_rxoobreset : out STD_LOGIC;
            ch1_rxosintdone : in STD_LOGIC;
            ch1_rxosintstarted : in STD_LOGIC;
            ch1_rxosintstrobedone : in STD_LOGIC;
            ch1_rxosintstrobestarted : in STD_LOGIC;
            ch1_rxpd : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxphaligndone : in STD_LOGIC;
            ch1_rxphalignerr : in STD_LOGIC;
            ch1_rxphalignreq : out STD_LOGIC;
            ch1_rxphalignresetmask : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxphdlypd : out STD_LOGIC;
            ch1_rxphdlyreset : out STD_LOGIC;
            ch1_rxphdlyresetdone : in STD_LOGIC;
            ch1_rxphsetinitdone : in STD_LOGIC;
            ch1_rxphsetinitreq : out STD_LOGIC;
            ch1_rxphshift180 : out STD_LOGIC;
            ch1_rxphshift180done : in STD_LOGIC;
            ch1_rxpolarity : out STD_LOGIC;
            ch1_rxprbscntreset : out STD_LOGIC;
            ch1_rxprbserr : in STD_LOGIC;
            ch1_rxprbslocked : in STD_LOGIC;
            ch1_rxprbssel : out STD_LOGIC_VECTOR ( 3 downto 0 );
            ch1_rxrate : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_rxresetmode : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxmstreset : out STD_LOGIC;
            ch1_rxmstdatapathreset : out STD_LOGIC;
            ch1_rxmstresetdone : in STD_LOGIC;
            ch1_rxslide : out STD_LOGIC;
            ch1_rxsliderdy : in STD_LOGIC;
            ch1_rxstartofseq : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxstatus : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch1_rxsyncallin : out STD_LOGIC;
            ch1_rxsyncdone : in STD_LOGIC;
            ch1_rxtermination : out STD_LOGIC;
            ch1_rxvalid : in STD_LOGIC;
            ch1_cdrbmcdrreq : out STD_LOGIC;
            ch1_cdrfreqos : out STD_LOGIC;
            ch1_cdrincpctrl : out STD_LOGIC;
            ch1_cdrstepdir : out STD_LOGIC;
            ch1_cdrstepsq : out STD_LOGIC;
            ch1_cdrstepsx : out STD_LOGIC;
            ch1_cfokovrdfinish : out STD_LOGIC;
            ch1_cfokovrdpulse : out STD_LOGIC;
            ch1_cfokovrdstart : out STD_LOGIC;
            ch1_eyescanreset : out STD_LOGIC;
            ch1_eyescantrigger : out STD_LOGIC;
            ch1_eyescandataerror : in STD_LOGIC;
            ch1_cfokovrdrdy0 : in STD_LOGIC;
            ch1_cfokovrdrdy1 : in STD_LOGIC;
            ch1_rxdata_ext : out STD_LOGIC_VECTOR ( 127 downto 0 );
            ch1_rxpcsresetmask_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_rxpmaresetmask_ext : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch1_rxdatavalid_ext : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxheader_ext : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch1_rxgearboxslip_ext : in STD_LOGIC;
            ch1_rxprogdivresetdone_ext : out STD_LOGIC;
            ch1_rxpmaresetdone_ext : out STD_LOGIC;
            ch1_rxresetdone_ext : out STD_LOGIC;
            ch1_rx10gstat_ext : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_rxbufstatus_ext : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch1_rxbyteisaligned_ext : out STD_LOGIC;
            ch1_rxbyterealign_ext : out STD_LOGIC;
            ch1_rxcdrhold_ext : in STD_LOGIC;
            ch1_rxcdrlock_ext : out STD_LOGIC;
            ch1_rxcdrovrden_ext : in STD_LOGIC;
            ch1_rxcdrphdone_ext : out STD_LOGIC;
            ch1_rxcdrreset_ext : in STD_LOGIC;
            ch1_rxchanbondseq_ext : out STD_LOGIC;
            ch1_rxchanisaligned_ext : out STD_LOGIC;
            ch1_rxchanrealign_ext : out STD_LOGIC;
            ch1_rxchbondi_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_rxchbondo_ext : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_rxclkcorcnt_ext : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxcominitdet_ext : out STD_LOGIC;
            ch1_rxcommadet_ext : out STD_LOGIC;
            ch1_rxcomsasdet_ext : out STD_LOGIC;
            ch1_rxcomwakedet_ext : out STD_LOGIC;
            ch1_rxctrl0_ext : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_rxctrl1_ext : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_rxctrl2_ext : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_rxctrl3_ext : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_rxdapicodeovrden_ext : in STD_LOGIC;
            ch1_rxdapicodereset_ext : in STD_LOGIC;
            ch1_rxdataextendrsvd_ext : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_rxdccdone_ext : out STD_LOGIC;
            ch1_rxdlyalignerr_ext : out STD_LOGIC;
            ch1_rxdlyalignprog_ext : out STD_LOGIC;
            ch1_rxdlyalignreq_ext : in STD_LOGIC;
            ch1_rxelecidle_ext : out STD_LOGIC;
            ch1_rxeqtraining_ext : in STD_LOGIC;
            ch1_rxfinealigndone_ext : out STD_LOGIC;
            ch1_rxheadervalid_ext : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxlpmen_ext : in STD_LOGIC;
            ch1_rxmldchaindone_ext : in STD_LOGIC;
            ch1_rxmldchainreq_ext : in STD_LOGIC;
            ch1_rxmlfinealignreq_ext : in STD_LOGIC;
            ch1_rxoobreset_ext : in STD_LOGIC;
            ch1_rxosintdone_ext : out STD_LOGIC;
            ch1_rxosintstarted_ext : out STD_LOGIC;
            ch1_rxosintstrobedone_ext : out STD_LOGIC;
            ch1_rxosintstrobestarted_ext : out STD_LOGIC;
            ch1_rxpd_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxphaligndone_ext : out STD_LOGIC;
            ch1_rxphalignerr_ext : out STD_LOGIC;
            ch1_rxphalignreq_ext : in STD_LOGIC;
            ch1_rxphalignresetmask_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxphdlypd_ext : in STD_LOGIC;
            ch1_rxphdlyreset_ext : in STD_LOGIC;
            ch1_rxphdlyresetdone_ext : out STD_LOGIC;
            ch1_rxphsetinitdone_ext : out STD_LOGIC;
            ch1_rxphsetinitreq_ext : in STD_LOGIC;
            ch1_rxphshift180_ext : in STD_LOGIC;
            ch1_rxphshift180done_ext : out STD_LOGIC;
            ch1_rxpolarity_ext : in STD_LOGIC;
            ch1_rxprbscntreset_ext : in STD_LOGIC;
            ch1_rxprbserr_ext : out STD_LOGIC;
            ch1_rxprbslocked_ext : out STD_LOGIC;
            ch1_rxprbssel_ext : in STD_LOGIC_VECTOR ( 3 downto 0 );
            ch1_rxresetmode_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxmstresetdone_ext : out STD_LOGIC;
            ch1_rxslide_ext : in STD_LOGIC;
            ch1_rxsliderdy_ext : out STD_LOGIC;
            ch1_rxstartofseq_ext : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxstatus_ext : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch1_rxsyncallin_ext : in STD_LOGIC;
            ch1_rxsyncdone_ext : out STD_LOGIC;
            ch1_rxtermination_ext : in STD_LOGIC;
            ch1_rxvalid_ext : out STD_LOGIC;
            ch1_cdrbmcdrreq_ext : in STD_LOGIC;
            ch1_cdrfreqos_ext : in STD_LOGIC;
            ch1_cdrincpctrl_ext : in STD_LOGIC;
            ch1_cdrstepdir_ext : in STD_LOGIC;
            ch1_cdrstepsq_ext : in STD_LOGIC;
            ch1_cdrstepsx_ext : in STD_LOGIC;
            ch1_cfokovrdfinish_ext : in STD_LOGIC;
            ch1_cfokovrdpulse_ext : in STD_LOGIC;
            ch1_cfokovrdstart_ext : in STD_LOGIC;
            ch1_eyescanreset_ext : in STD_LOGIC;
            ch1_eyescantrigger_ext : in STD_LOGIC;
            ch1_eyescandataerror_ext : out STD_LOGIC;
            ch1_cfokovrdrdy0_ext : out STD_LOGIC;
            ch1_cfokovrdrdy1_ext : out STD_LOGIC;
            ch2_rxdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
            ch2_rxpcsresetmask : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_rxpmaresetmask : out STD_LOGIC_VECTOR ( 6 downto 0 );
            ch2_rxdatavalid : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxheader : in STD_LOGIC_VECTOR ( 5 downto 0 );
            ch2_rxgearboxslip : out STD_LOGIC;
            ch2_gtrxreset : out STD_LOGIC;
            ch2_rxprogdivreset : out STD_LOGIC;
            ch2_rxuserrdy : out STD_LOGIC;
            ch2_rxprogdivresetdone : in STD_LOGIC;
            ch2_rxpmaresetdone : in STD_LOGIC;
            ch2_rxresetdone : in STD_LOGIC;
            ch2_rx10gstat : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_rxbufstatus : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch2_rxbyteisaligned : in STD_LOGIC;
            ch2_rxbyterealign : in STD_LOGIC;
            ch2_rxcdrhold : out STD_LOGIC;
            ch2_rxcdrlock : in STD_LOGIC;
            ch2_rxcdrovrden : out STD_LOGIC;
            ch2_rxcdrphdone : in STD_LOGIC;
            ch2_rxcdrreset : out STD_LOGIC;
            ch2_rxchanbondseq : in STD_LOGIC;
            ch2_rxchanisaligned : in STD_LOGIC;
            ch2_rxchanrealign : in STD_LOGIC;
            ch2_rxchbondi : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_rxchbondo : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_rxclkcorcnt : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxcominitdet : in STD_LOGIC;
            ch2_rxcommadet : in STD_LOGIC;
            ch2_rxcomsasdet : in STD_LOGIC;
            ch2_rxcomwakedet : in STD_LOGIC;
            ch2_rxctrl0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_rxctrl1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_rxctrl2 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_rxctrl3 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_rxdapicodeovrden : out STD_LOGIC;
            ch2_rxdapicodereset : out STD_LOGIC;
            ch2_rxdataextendrsvd : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_rxdccdone : in STD_LOGIC;
            ch2_rxdlyalignerr : in STD_LOGIC;
            ch2_rxdlyalignprog : in STD_LOGIC;
            ch2_rxdlyalignreq : out STD_LOGIC;
            ch2_rxelecidle : in STD_LOGIC;
            ch2_rxeqtraining : out STD_LOGIC;
            ch2_rxfinealigndone : in STD_LOGIC;
            ch2_rxheadervalid : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxlpmen : out STD_LOGIC;
            ch2_rxmldchaindone : out STD_LOGIC;
            ch2_rxmldchainreq : out STD_LOGIC;
            ch2_rxmlfinealignreq : out STD_LOGIC;
            ch2_rxoobreset : out STD_LOGIC;
            ch2_rxosintdone : in STD_LOGIC;
            ch2_rxosintstarted : in STD_LOGIC;
            ch2_rxosintstrobedone : in STD_LOGIC;
            ch2_rxosintstrobestarted : in STD_LOGIC;
            ch2_rxpd : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxphaligndone : in STD_LOGIC;
            ch2_rxphalignerr : in STD_LOGIC;
            ch2_rxphalignreq : out STD_LOGIC;
            ch2_rxphalignresetmask : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxphdlypd : out STD_LOGIC;
            ch2_rxphdlyreset : out STD_LOGIC;
            ch2_rxphdlyresetdone : in STD_LOGIC;
            ch2_rxphsetinitdone : in STD_LOGIC;
            ch2_rxphsetinitreq : out STD_LOGIC;
            ch2_rxphshift180 : out STD_LOGIC;
            ch2_rxphshift180done : in STD_LOGIC;
            ch2_rxpolarity : out STD_LOGIC;
            ch2_rxprbscntreset : out STD_LOGIC;
            ch2_rxprbserr : in STD_LOGIC;
            ch2_rxprbslocked : in STD_LOGIC;
            ch2_rxprbssel : out STD_LOGIC_VECTOR ( 3 downto 0 );
            ch2_rxrate : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_rxresetmode : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxmstreset : out STD_LOGIC;
            ch2_rxmstdatapathreset : out STD_LOGIC;
            ch2_rxmstresetdone : in STD_LOGIC;
            ch2_rxslide : out STD_LOGIC;
            ch2_rxsliderdy : in STD_LOGIC;
            ch2_rxstartofseq : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxstatus : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch2_rxsyncallin : out STD_LOGIC;
            ch2_rxsyncdone : in STD_LOGIC;
            ch2_rxtermination : out STD_LOGIC;
            ch2_rxvalid : in STD_LOGIC;
            ch2_cdrbmcdrreq : out STD_LOGIC;
            ch2_cdrfreqos : out STD_LOGIC;
            ch2_cdrincpctrl : out STD_LOGIC;
            ch2_cdrstepdir : out STD_LOGIC;
            ch2_cdrstepsq : out STD_LOGIC;
            ch2_cdrstepsx : out STD_LOGIC;
            ch2_cfokovrdfinish : out STD_LOGIC;
            ch2_cfokovrdpulse : out STD_LOGIC;
            ch2_cfokovrdstart : out STD_LOGIC;
            ch2_eyescanreset : out STD_LOGIC;
            ch2_eyescantrigger : out STD_LOGIC;
            ch2_eyescandataerror : in STD_LOGIC;
            ch2_cfokovrdrdy0 : in STD_LOGIC;
            ch2_cfokovrdrdy1 : in STD_LOGIC;
            ch2_rxdata_ext : out STD_LOGIC_VECTOR ( 127 downto 0 );
            ch2_rxpcsresetmask_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_rxpmaresetmask_ext : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch2_rxdatavalid_ext : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxheader_ext : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch2_rxgearboxslip_ext : in STD_LOGIC;
            ch2_rxprogdivresetdone_ext : out STD_LOGIC;
            ch2_rxpmaresetdone_ext : out STD_LOGIC;
            ch2_rxresetdone_ext : out STD_LOGIC;
            ch2_rx10gstat_ext : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_rxbufstatus_ext : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch2_rxbyteisaligned_ext : out STD_LOGIC;
            ch2_rxbyterealign_ext : out STD_LOGIC;
            ch2_rxcdrhold_ext : in STD_LOGIC;
            ch2_rxcdrlock_ext : out STD_LOGIC;
            ch2_rxcdrovrden_ext : in STD_LOGIC;
            ch2_rxcdrphdone_ext : out STD_LOGIC;
            ch2_rxcdrreset_ext : in STD_LOGIC;
            ch2_rxchanbondseq_ext : out STD_LOGIC;
            ch2_rxchanisaligned_ext : out STD_LOGIC;
            ch2_rxchanrealign_ext : out STD_LOGIC;
            ch2_rxchbondi_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_rxchbondo_ext : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_rxclkcorcnt_ext : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxcominitdet_ext : out STD_LOGIC;
            ch2_rxcommadet_ext : out STD_LOGIC;
            ch2_rxcomsasdet_ext : out STD_LOGIC;
            ch2_rxcomwakedet_ext : out STD_LOGIC;
            ch2_rxctrl0_ext : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_rxctrl1_ext : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_rxctrl2_ext : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_rxctrl3_ext : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_rxdapicodeovrden_ext : in STD_LOGIC;
            ch2_rxdapicodereset_ext : in STD_LOGIC;
            ch2_rxdataextendrsvd_ext : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_rxdccdone_ext : out STD_LOGIC;
            ch2_rxdlyalignerr_ext : out STD_LOGIC;
            ch2_rxdlyalignprog_ext : out STD_LOGIC;
            ch2_rxdlyalignreq_ext : in STD_LOGIC;
            ch2_rxelecidle_ext : out STD_LOGIC;
            ch2_rxeqtraining_ext : in STD_LOGIC;
            ch2_rxfinealigndone_ext : out STD_LOGIC;
            ch2_rxheadervalid_ext : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxlpmen_ext : in STD_LOGIC;
            ch2_rxmldchaindone_ext : in STD_LOGIC;
            ch2_rxmldchainreq_ext : in STD_LOGIC;
            ch2_rxmlfinealignreq_ext : in STD_LOGIC;
            ch2_rxoobreset_ext : in STD_LOGIC;
            ch2_rxosintdone_ext : out STD_LOGIC;
            ch2_rxosintstarted_ext : out STD_LOGIC;
            ch2_rxosintstrobedone_ext : out STD_LOGIC;
            ch2_rxosintstrobestarted_ext : out STD_LOGIC;
            ch2_rxpd_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxphaligndone_ext : out STD_LOGIC;
            ch2_rxphalignerr_ext : out STD_LOGIC;
            ch2_rxphalignreq_ext : in STD_LOGIC;
            ch2_rxphalignresetmask_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxphdlypd_ext : in STD_LOGIC;
            ch2_rxphdlyreset_ext : in STD_LOGIC;
            ch2_rxphdlyresetdone_ext : out STD_LOGIC;
            ch2_rxphsetinitdone_ext : out STD_LOGIC;
            ch2_rxphsetinitreq_ext : in STD_LOGIC;
            ch2_rxphshift180_ext : in STD_LOGIC;
            ch2_rxphshift180done_ext : out STD_LOGIC;
            ch2_rxpolarity_ext : in STD_LOGIC;
            ch2_rxprbscntreset_ext : in STD_LOGIC;
            ch2_rxprbserr_ext : out STD_LOGIC;
            ch2_rxprbslocked_ext : out STD_LOGIC;
            ch2_rxprbssel_ext : in STD_LOGIC_VECTOR ( 3 downto 0 );
            ch2_rxresetmode_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxmstresetdone_ext : out STD_LOGIC;
            ch2_rxslide_ext : in STD_LOGIC;
            ch2_rxsliderdy_ext : out STD_LOGIC;
            ch2_rxstartofseq_ext : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxstatus_ext : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch2_rxsyncallin_ext : in STD_LOGIC;
            ch2_rxsyncdone_ext : out STD_LOGIC;
            ch2_rxtermination_ext : in STD_LOGIC;
            ch2_rxvalid_ext : out STD_LOGIC;
            ch2_cdrbmcdrreq_ext : in STD_LOGIC;
            ch2_cdrfreqos_ext : in STD_LOGIC;
            ch2_cdrincpctrl_ext : in STD_LOGIC;
            ch2_cdrstepdir_ext : in STD_LOGIC;
            ch2_cdrstepsq_ext : in STD_LOGIC;
            ch2_cdrstepsx_ext : in STD_LOGIC;
            ch2_cfokovrdfinish_ext : in STD_LOGIC;
            ch2_cfokovrdpulse_ext : in STD_LOGIC;
            ch2_cfokovrdstart_ext : in STD_LOGIC;
            ch2_eyescanreset_ext : in STD_LOGIC;
            ch2_eyescantrigger_ext : in STD_LOGIC;
            ch2_eyescandataerror_ext : out STD_LOGIC;
            ch2_cfokovrdrdy0_ext : out STD_LOGIC;
            ch2_cfokovrdrdy1_ext : out STD_LOGIC;
            ch3_rxdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
            ch3_rxpcsresetmask : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_rxpmaresetmask : out STD_LOGIC_VECTOR ( 6 downto 0 );
            ch3_rxdatavalid : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxheader : in STD_LOGIC_VECTOR ( 5 downto 0 );
            ch3_rxgearboxslip : out STD_LOGIC;
            ch3_gtrxreset : out STD_LOGIC;
            ch3_rxprogdivreset : out STD_LOGIC;
            ch3_rxuserrdy : out STD_LOGIC;
            ch3_rxprogdivresetdone : in STD_LOGIC;
            ch3_rxpmaresetdone : in STD_LOGIC;
            ch3_rxresetdone : in STD_LOGIC;
            ch3_rx10gstat : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_rxbufstatus : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch3_rxbyteisaligned : in STD_LOGIC;
            ch3_rxbyterealign : in STD_LOGIC;
            ch3_rxcdrhold : out STD_LOGIC;
            ch3_rxcdrlock : in STD_LOGIC;
            ch3_rxcdrovrden : out STD_LOGIC;
            ch3_rxcdrphdone : in STD_LOGIC;
            ch3_rxcdrreset : out STD_LOGIC;
            ch3_rxchanbondseq : in STD_LOGIC;
            ch3_rxchanisaligned : in STD_LOGIC;
            ch3_rxchanrealign : in STD_LOGIC;
            ch3_rxchbondi : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_rxchbondo : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_rxclkcorcnt : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxcominitdet : in STD_LOGIC;
            ch3_rxcommadet : in STD_LOGIC;
            ch3_rxcomsasdet : in STD_LOGIC;
            ch3_rxcomwakedet : in STD_LOGIC;
            ch3_rxctrl0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_rxctrl1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_rxctrl2 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_rxctrl3 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_rxdapicodeovrden : out STD_LOGIC;
            ch3_rxdapicodereset : out STD_LOGIC;
            ch3_rxdataextendrsvd : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_rxdccdone : in STD_LOGIC;
            ch3_rxdlyalignerr : in STD_LOGIC;
            ch3_rxdlyalignprog : in STD_LOGIC;
            ch3_rxdlyalignreq : out STD_LOGIC;
            ch3_rxelecidle : in STD_LOGIC;
            ch3_rxeqtraining : out STD_LOGIC;
            ch3_rxfinealigndone : in STD_LOGIC;
            ch3_rxheadervalid : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxlpmen : out STD_LOGIC;
            ch3_rxmldchaindone : out STD_LOGIC;
            ch3_rxmldchainreq : out STD_LOGIC;
            ch3_rxmlfinealignreq : out STD_LOGIC;
            ch3_rxoobreset : out STD_LOGIC;
            ch3_rxosintdone : in STD_LOGIC;
            ch3_rxosintstarted : in STD_LOGIC;
            ch3_rxosintstrobedone : in STD_LOGIC;
            ch3_rxosintstrobestarted : in STD_LOGIC;
            ch3_rxpd : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxphaligndone : in STD_LOGIC;
            ch3_rxphalignerr : in STD_LOGIC;
            ch3_rxphalignreq : out STD_LOGIC;
            ch3_rxphalignresetmask : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxphdlypd : out STD_LOGIC;
            ch3_rxphdlyreset : out STD_LOGIC;
            ch3_rxphdlyresetdone : in STD_LOGIC;
            ch3_rxphsetinitdone : in STD_LOGIC;
            ch3_rxphsetinitreq : out STD_LOGIC;
            ch3_rxphshift180 : out STD_LOGIC;
            ch3_rxphshift180done : in STD_LOGIC;
            ch3_rxpolarity : out STD_LOGIC;
            ch3_rxprbscntreset : out STD_LOGIC;
            ch3_rxprbserr : in STD_LOGIC;
            ch3_rxprbslocked : in STD_LOGIC;
            ch3_rxprbssel : out STD_LOGIC_VECTOR ( 3 downto 0 );
            ch3_rxrate : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_rxresetmode : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxmstreset : out STD_LOGIC;
            ch3_rxmstdatapathreset : out STD_LOGIC;
            ch3_rxmstresetdone : in STD_LOGIC;
            ch3_rxslide : out STD_LOGIC;
            ch3_rxsliderdy : in STD_LOGIC;
            ch3_rxstartofseq : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxstatus : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch3_rxsyncallin : out STD_LOGIC;
            ch3_rxsyncdone : in STD_LOGIC;
            ch3_rxtermination : out STD_LOGIC;
            ch3_rxvalid : in STD_LOGIC;
            ch3_cdrbmcdrreq : out STD_LOGIC;
            ch3_cdrfreqos : out STD_LOGIC;
            ch3_cdrincpctrl : out STD_LOGIC;
            ch3_cdrstepdir : out STD_LOGIC;
            ch3_cdrstepsq : out STD_LOGIC;
            ch3_cdrstepsx : out STD_LOGIC;
            ch3_cfokovrdfinish : out STD_LOGIC;
            ch3_cfokovrdpulse : out STD_LOGIC;
            ch3_cfokovrdstart : out STD_LOGIC;
            ch3_eyescanreset : out STD_LOGIC;
            ch3_eyescantrigger : out STD_LOGIC;
            ch3_eyescandataerror : in STD_LOGIC;
            ch3_cfokovrdrdy0 : in STD_LOGIC;
            ch3_cfokovrdrdy1 : in STD_LOGIC;
            ch3_rxdata_ext : out STD_LOGIC_VECTOR ( 127 downto 0 );
            ch3_rxpcsresetmask_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_rxpmaresetmask_ext : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch3_rxdatavalid_ext : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxheader_ext : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch3_rxgearboxslip_ext : in STD_LOGIC;
            ch3_rxprogdivresetdone_ext : out STD_LOGIC;
            ch3_rxpmaresetdone_ext : out STD_LOGIC;
            ch3_rxresetdone_ext : out STD_LOGIC;
            ch3_rx10gstat_ext : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_rxbufstatus_ext : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch3_rxbyteisaligned_ext : out STD_LOGIC;
            ch3_rxbyterealign_ext : out STD_LOGIC;
            ch3_rxcdrhold_ext : in STD_LOGIC;
            ch3_rxcdrlock_ext : out STD_LOGIC;
            ch3_rxcdrovrden_ext : in STD_LOGIC;
            ch3_rxcdrphdone_ext : out STD_LOGIC;
            ch3_rxcdrreset_ext : in STD_LOGIC;
            ch3_rxchanbondseq_ext : out STD_LOGIC;
            ch3_rxchanisaligned_ext : out STD_LOGIC;
            ch3_rxchanrealign_ext : out STD_LOGIC;
            ch3_rxchbondi_ext : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_rxchbondo_ext : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_rxclkcorcnt_ext : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxcominitdet_ext : out STD_LOGIC;
            ch3_rxcommadet_ext : out STD_LOGIC;
            ch3_rxcomsasdet_ext : out STD_LOGIC;
            ch3_rxcomwakedet_ext : out STD_LOGIC;
            ch3_rxctrl0_ext : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_rxctrl1_ext : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_rxctrl2_ext : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_rxctrl3_ext : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_rxdapicodeovrden_ext : in STD_LOGIC;
            ch3_rxdapicodereset_ext : in STD_LOGIC;
            ch3_rxdataextendrsvd_ext : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_rxdccdone_ext : out STD_LOGIC;
            ch3_rxdlyalignerr_ext : out STD_LOGIC;
            ch3_rxdlyalignprog_ext : out STD_LOGIC;
            ch3_rxdlyalignreq_ext : in STD_LOGIC;
            ch3_rxelecidle_ext : out STD_LOGIC;
            ch3_rxeqtraining_ext : in STD_LOGIC;
            ch3_rxfinealigndone_ext : out STD_LOGIC;
            ch3_rxheadervalid_ext : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxlpmen_ext : in STD_LOGIC;
            ch3_rxmldchaindone_ext : in STD_LOGIC;
            ch3_rxmldchainreq_ext : in STD_LOGIC;
            ch3_rxmlfinealignreq_ext : in STD_LOGIC;
            ch3_rxoobreset_ext : in STD_LOGIC;
            ch3_rxosintdone_ext : out STD_LOGIC;
            ch3_rxosintstarted_ext : out STD_LOGIC;
            ch3_rxosintstrobedone_ext : out STD_LOGIC;
            ch3_rxosintstrobestarted_ext : out STD_LOGIC;
            ch3_rxpd_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxphaligndone_ext : out STD_LOGIC;
            ch3_rxphalignerr_ext : out STD_LOGIC;
            ch3_rxphalignreq_ext : in STD_LOGIC;
            ch3_rxphalignresetmask_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxphdlypd_ext : in STD_LOGIC;
            ch3_rxphdlyreset_ext : in STD_LOGIC;
            ch3_rxphdlyresetdone_ext : out STD_LOGIC;
            ch3_rxphsetinitdone_ext : out STD_LOGIC;
            ch3_rxphsetinitreq_ext : in STD_LOGIC;
            ch3_rxphshift180_ext : in STD_LOGIC;
            ch3_rxphshift180done_ext : out STD_LOGIC;
            ch3_rxpolarity_ext : in STD_LOGIC;
            ch3_rxprbscntreset_ext : in STD_LOGIC;
            ch3_rxprbserr_ext : out STD_LOGIC;
            ch3_rxprbslocked_ext : out STD_LOGIC;
            ch3_rxprbssel_ext : in STD_LOGIC_VECTOR ( 3 downto 0 );
            ch3_rxresetmode_ext : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxmstresetdone_ext : out STD_LOGIC;
            ch3_rxslide_ext : in STD_LOGIC;
            ch3_rxsliderdy_ext : out STD_LOGIC;
            ch3_rxstartofseq_ext : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxstatus_ext : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch3_rxsyncallin_ext : in STD_LOGIC;
            ch3_rxsyncdone_ext : out STD_LOGIC;
            ch3_rxtermination_ext : in STD_LOGIC;
            ch3_rxvalid_ext : out STD_LOGIC;
            ch3_cdrbmcdrreq_ext : in STD_LOGIC;
            ch3_cdrfreqos_ext : in STD_LOGIC;
            ch3_cdrincpctrl_ext : in STD_LOGIC;
            ch3_cdrstepdir_ext : in STD_LOGIC;
            ch3_cdrstepsq_ext : in STD_LOGIC;
            ch3_cdrstepsx_ext : in STD_LOGIC;
            ch3_cfokovrdfinish_ext : in STD_LOGIC;
            ch3_cfokovrdpulse_ext : in STD_LOGIC;
            ch3_cfokovrdstart_ext : in STD_LOGIC;
            ch3_eyescanreset_ext : in STD_LOGIC;
            ch3_eyescantrigger_ext : in STD_LOGIC;
            ch3_eyescandataerror_ext : out STD_LOGIC;
            ch3_cfokovrdrdy0_ext : out STD_LOGIC;
            ch3_cfokovrdrdy1_ext : out STD_LOGIC;
            gt_txusrclk : in STD_LOGIC;
            gt_rxusrclk : in STD_LOGIC;
            apb3clk : in STD_LOGIC;
            gtpowergood : in STD_LOGIC;
            gt_lcpll_lock : in STD_LOGIC;
            gt_rpll_lock : in STD_LOGIC;
            ch_phystatus_in : in STD_LOGIC_VECTOR ( 3 downto 0 );
            ilo_resetdone : in STD_LOGIC;
            rx_clr_out : out STD_LOGIC;
            rx_clrb_leaf_out : out STD_LOGIC;
            tx_clr_out : out STD_LOGIC;
            tx_clrb_leaf_out : out STD_LOGIC;
            link_status_out : out STD_LOGIC;
            gpio_enable : in STD_LOGIC;
            tx_resetdone_out : out STD_LOGIC;
            rx_resetdone_out : out STD_LOGIC;
            txusrclk_out : out STD_LOGIC;
            rxusrclk_out : out STD_LOGIC;
            rpll_lock_out : out STD_LOGIC;
            lcpll_lock_out : out STD_LOGIC;
            pcie_rstb : out STD_LOGIC;
            gpi_out : out STD_LOGIC;
            gpo_in : in STD_LOGIC;
            gtreset_in : in STD_LOGIC;
            rate_sel : in STD_LOGIC_VECTOR ( 3 downto 0 );
            reset_mask : out STD_LOGIC_VECTOR ( 1 downto 0 );
            reset_tx_pll_and_datapath_in : in STD_LOGIC;
            reset_tx_datapath_in : in STD_LOGIC;
            reset_rx_pll_and_datapath_in : in STD_LOGIC;
            reset_rx_datapath_in : in STD_LOGIC
        );
    end component transceiver_versal_lpgbt_gt_bridge_ip_0_0;
    component transceiver_versal_lpgbt_gt_quad_base_0 is
        port (
            rxmarginclk : in STD_LOGIC;
            hsclk0_lcpllreset : in STD_LOGIC;
            hsclk0_rpllreset : in STD_LOGIC;
            hsclk1_lcpllreset : in STD_LOGIC;
            hsclk1_rpllreset : in STD_LOGIC;
            hsclk0_lcplllock : out STD_LOGIC;
            hsclk1_lcplllock : out STD_LOGIC;
            hsclk0_rplllock : out STD_LOGIC;
            hsclk1_rplllock : out STD_LOGIC;
            gtpowergood : out STD_LOGIC;
            ch0_pcierstb : in STD_LOGIC;
            ch1_pcierstb : in STD_LOGIC;
            ch2_pcierstb : in STD_LOGIC;
            ch3_pcierstb : in STD_LOGIC;
            pcielinkreachtarget : in STD_LOGIC;
            pcieltssm : in STD_LOGIC_VECTOR ( 5 downto 0 );
            rxmarginreqack : out STD_LOGIC;
            rxmarginrescmd : out STD_LOGIC_VECTOR ( 3 downto 0 );
            rxmarginreslanenum : out STD_LOGIC_VECTOR ( 1 downto 0 );
            rxmarginrespayld : out STD_LOGIC_VECTOR ( 7 downto 0 );
            rxmarginresreq : out STD_LOGIC;
            rxmarginreqcmd : in STD_LOGIC_VECTOR ( 3 downto 0 );
            rxmarginreqlanenum : in STD_LOGIC_VECTOR ( 1 downto 0 );
            rxmarginreqpayld : in STD_LOGIC_VECTOR ( 7 downto 0 );
            rxmarginreqreq : in STD_LOGIC;
            rxmarginresack : in STD_LOGIC;
            ch0_iloreset : in STD_LOGIC;
            ch1_iloreset : in STD_LOGIC;
            ch2_iloreset : in STD_LOGIC;
            ch3_iloreset : in STD_LOGIC;
            ch0_iloresetdone : out STD_LOGIC;
            ch1_iloresetdone : out STD_LOGIC;
            ch2_iloresetdone : out STD_LOGIC;
            ch3_iloresetdone : out STD_LOGIC;
            ch0_phystatus : out STD_LOGIC;
            ch1_phystatus : out STD_LOGIC;
            ch2_phystatus : out STD_LOGIC;
            ch3_phystatus : out STD_LOGIC;
            hsclk0_lcpllfbclklost : out STD_LOGIC;
            hsclk0_lcpllrefclklost : out STD_LOGIC;
            hsclk0_lcpllrefclkmonitor : out STD_LOGIC;
            hsclk0_rpllfbclklost : out STD_LOGIC;
            hsclk0_rpllrefclklost : out STD_LOGIC;
            hsclk0_rpllrefclkmonitor : out STD_LOGIC;
            hsclk1_lcpllfbclklost : out STD_LOGIC;
            hsclk1_lcpllrefclklost : out STD_LOGIC;
            hsclk1_lcpllrefclkmonitor : out STD_LOGIC;
            hsclk1_rpllfbclklost : out STD_LOGIC;
            hsclk1_rpllrefclklost : out STD_LOGIC;
            hsclk1_rpllrefclkmonitor : out STD_LOGIC;
            hsclk0_lcpllpd : in STD_LOGIC;
            hsclk0_rpllpd : in STD_LOGIC;
            hsclk0_lcpllresetbypassmode : in STD_LOGIC;
            hsclk0_lcpllsdmtoggle : in STD_LOGIC;
            hsclk0_rpllresetbypassmode : in STD_LOGIC;
            hsclk0_rpllsdmtoggle : in STD_LOGIC;
            hsclk1_lcpllpd : in STD_LOGIC;
            hsclk1_lcpllresetbypassmode : in STD_LOGIC;
            hsclk1_lcpllsdmtoggle : in STD_LOGIC;
            hsclk1_rpllpd : in STD_LOGIC;
            hsclk1_rpllresetbypassmode : in STD_LOGIC;
            hsclk1_rpllsdmtoggle : in STD_LOGIC;
            refclk0_gtrefclkpd : in STD_LOGIC;
            refclk1_gtrefclkpd : in STD_LOGIC;
            hsclk0_lcpllrefclksel : in STD_LOGIC_VECTOR ( 2 downto 0 );
            hsclk1_lcpllrefclksel : in STD_LOGIC_VECTOR ( 2 downto 0 );
            hsclk0_rpllrefclksel : in STD_LOGIC_VECTOR ( 2 downto 0 );
            hsclk1_rpllrefclksel : in STD_LOGIC_VECTOR ( 2 downto 0 );
            hsclk0_lcpllfbdiv : in STD_LOGIC_VECTOR ( 7 downto 0 );
            hsclk0_rpllfbdiv : in STD_LOGIC_VECTOR ( 7 downto 0 );
            hsclk1_lcpllfbdiv : in STD_LOGIC_VECTOR ( 7 downto 0 );
            hsclk1_rpllfbdiv : in STD_LOGIC_VECTOR ( 7 downto 0 );
            hsclk0_rxrecclkout0 : out STD_LOGIC;
            hsclk0_rxrecclkout1 : out STD_LOGIC;
            hsclk1_rxrecclkout0 : out STD_LOGIC;
            hsclk1_rxrecclkout1 : out STD_LOGIC;
            hsclk0_lcpllsdmdata : in STD_LOGIC_VECTOR ( 25 downto 0 );
            hsclk1_lcpllsdmdata : in STD_LOGIC_VECTOR ( 25 downto 0 );
            hsclk0_rpllsdmdata : in STD_LOGIC_VECTOR ( 25 downto 0 );
            hsclk1_rpllsdmdata : in STD_LOGIC_VECTOR ( 25 downto 0 );
            hsclk0_lcpllresetmask : in STD_LOGIC_VECTOR ( 1 downto 0 );
            hsclk1_lcpllresetmask : in STD_LOGIC_VECTOR ( 1 downto 0 );
            hsclk0_rpllresetmask : in STD_LOGIC_VECTOR ( 1 downto 0 );
            hsclk1_rpllresetmask : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_txdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
            ch0_txheader : in STD_LOGIC_VECTOR ( 5 downto 0 );
            ch0_txsequence : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch0_gttxreset : in STD_LOGIC;
            ch0_txprogdivreset : in STD_LOGIC;
            ch0_txuserrdy : in STD_LOGIC;
            ch0_txphalignresetmask : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_txcominit : in STD_LOGIC;
            ch0_txcomsas : in STD_LOGIC;
            ch0_txcomwake : in STD_LOGIC;
            ch0_txdapicodeovrden : in STD_LOGIC;
            ch0_txdapicodereset : in STD_LOGIC;
            ch0_txdetectrx : in STD_LOGIC;
            ch0_txlatclk : in STD_LOGIC;
            ch0_txphdlytstclk : in STD_LOGIC;
            ch0_txdlyalignreq : in STD_LOGIC;
            ch0_txelecidle : in STD_LOGIC;
            ch0_txinhibit : in STD_LOGIC;
            ch0_txmldchaindone : in STD_LOGIC;
            ch0_txmldchainreq : in STD_LOGIC;
            ch0_txoneszeros : in STD_LOGIC;
            ch0_txpausedelayalign : in STD_LOGIC;
            ch0_txpcsresetmask : in STD_LOGIC;
            ch0_txphalignreq : in STD_LOGIC;
            ch0_txphdlypd : in STD_LOGIC;
            ch0_txphdlyreset : in STD_LOGIC;
            ch0_txphsetinitreq : in STD_LOGIC;
            ch0_txphshift180 : in STD_LOGIC;
            ch0_txpicodeovrden : in STD_LOGIC;
            ch0_txpicodereset : in STD_LOGIC;
            ch0_txpippmen : in STD_LOGIC;
            ch0_txpisopd : in STD_LOGIC;
            ch0_txpolarity : in STD_LOGIC;
            ch0_txprbsforceerr : in STD_LOGIC;
            ch0_txswing : in STD_LOGIC;
            ch0_txsyncallin : in STD_LOGIC;
            ch0_tx10gstat : out STD_LOGIC;
            ch0_txcomfinish : out STD_LOGIC;
            ch0_txdccdone : out STD_LOGIC;
            ch0_txdlyalignerr : out STD_LOGIC;
            ch0_txdlyalignprog : out STD_LOGIC;
            ch0_txphaligndone : out STD_LOGIC;
            ch0_txphalignerr : out STD_LOGIC;
            ch0_txphalignoutrsvd : out STD_LOGIC;
            ch0_txphdlyresetdone : out STD_LOGIC;
            ch0_txphsetinitdone : out STD_LOGIC;
            ch0_txphshift180done : out STD_LOGIC;
            ch0_txsyncdone : out STD_LOGIC;
            ch0_txbufstatus : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_txctrl0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_txctrl1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_txdeemph : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_txpd : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_txresetmode : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_txmstreset : in STD_LOGIC;
            ch0_txmstdatapathreset : in STD_LOGIC;
            ch0_txmstresetdone : out STD_LOGIC;
            ch0_txmargin : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch0_txpmaresetmask : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch0_txprbssel : in STD_LOGIC_VECTOR ( 3 downto 0 );
            ch0_txdiffctrl : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_txpippmstepsize : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_txpostcursor : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_txprecursor : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_txmaincursor : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch0_txctrl2 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_txrate : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_txprogdivresetdone : out STD_LOGIC;
            ch0_txpmaresetdone : out STD_LOGIC;
            ch0_txresetdone : out STD_LOGIC;
            ch0_txdataextendrsvd : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_txoutclk : out STD_LOGIC;
            ch0_txusrclk : in STD_LOGIC;
            ch1_txdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
            ch1_txheader : in STD_LOGIC_VECTOR ( 5 downto 0 );
            ch1_txsequence : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch1_gttxreset : in STD_LOGIC;
            ch1_txprogdivreset : in STD_LOGIC;
            ch1_txuserrdy : in STD_LOGIC;
            ch1_txphalignresetmask : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_txcominit : in STD_LOGIC;
            ch1_txcomsas : in STD_LOGIC;
            ch1_txcomwake : in STD_LOGIC;
            ch1_txdapicodeovrden : in STD_LOGIC;
            ch1_txdapicodereset : in STD_LOGIC;
            ch1_txdetectrx : in STD_LOGIC;
            ch1_txlatclk : in STD_LOGIC;
            ch1_txphdlytstclk : in STD_LOGIC;
            ch1_txdlyalignreq : in STD_LOGIC;
            ch1_txelecidle : in STD_LOGIC;
            ch1_txinhibit : in STD_LOGIC;
            ch1_txmldchaindone : in STD_LOGIC;
            ch1_txmldchainreq : in STD_LOGIC;
            ch1_txoneszeros : in STD_LOGIC;
            ch1_txpausedelayalign : in STD_LOGIC;
            ch1_txpcsresetmask : in STD_LOGIC;
            ch1_txphalignreq : in STD_LOGIC;
            ch1_txphdlypd : in STD_LOGIC;
            ch1_txphdlyreset : in STD_LOGIC;
            ch1_txphsetinitreq : in STD_LOGIC;
            ch1_txphshift180 : in STD_LOGIC;
            ch1_txpicodeovrden : in STD_LOGIC;
            ch1_txpicodereset : in STD_LOGIC;
            ch1_txpippmen : in STD_LOGIC;
            ch1_txpisopd : in STD_LOGIC;
            ch1_txpolarity : in STD_LOGIC;
            ch1_txprbsforceerr : in STD_LOGIC;
            ch1_txswing : in STD_LOGIC;
            ch1_txsyncallin : in STD_LOGIC;
            ch1_tx10gstat : out STD_LOGIC;
            ch1_txcomfinish : out STD_LOGIC;
            ch1_txdccdone : out STD_LOGIC;
            ch1_txdlyalignerr : out STD_LOGIC;
            ch1_txdlyalignprog : out STD_LOGIC;
            ch1_txphaligndone : out STD_LOGIC;
            ch1_txphalignerr : out STD_LOGIC;
            ch1_txphalignoutrsvd : out STD_LOGIC;
            ch1_txphdlyresetdone : out STD_LOGIC;
            ch1_txphsetinitdone : out STD_LOGIC;
            ch1_txphshift180done : out STD_LOGIC;
            ch1_txsyncdone : out STD_LOGIC;
            ch1_txbufstatus : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_txctrl0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_txctrl1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_txdeemph : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_txpd : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_txresetmode : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_txmstreset : in STD_LOGIC;
            ch1_txmstdatapathreset : in STD_LOGIC;
            ch1_txmstresetdone : out STD_LOGIC;
            ch1_txmargin : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch1_txpmaresetmask : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch1_txprbssel : in STD_LOGIC_VECTOR ( 3 downto 0 );
            ch1_txdiffctrl : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_txpippmstepsize : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_txpostcursor : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_txprecursor : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_txmaincursor : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch1_txctrl2 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_txrate : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_txprogdivresetdone : out STD_LOGIC;
            ch1_txpmaresetdone : out STD_LOGIC;
            ch1_txresetdone : out STD_LOGIC;
            ch1_txdataextendrsvd : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_txoutclk : out STD_LOGIC;
            ch1_txusrclk : in STD_LOGIC;
            ch2_txdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
            ch2_txheader : in STD_LOGIC_VECTOR ( 5 downto 0 );
            ch2_txsequence : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch2_gttxreset : in STD_LOGIC;
            ch2_txprogdivreset : in STD_LOGIC;
            ch2_txuserrdy : in STD_LOGIC;
            ch2_txphalignresetmask : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_txcominit : in STD_LOGIC;
            ch2_txcomsas : in STD_LOGIC;
            ch2_txcomwake : in STD_LOGIC;
            ch2_txdapicodeovrden : in STD_LOGIC;
            ch2_txdapicodereset : in STD_LOGIC;
            ch2_txdetectrx : in STD_LOGIC;
            ch2_txlatclk : in STD_LOGIC;
            ch2_txphdlytstclk : in STD_LOGIC;
            ch2_txdlyalignreq : in STD_LOGIC;
            ch2_txelecidle : in STD_LOGIC;
            ch2_txinhibit : in STD_LOGIC;
            ch2_txmldchaindone : in STD_LOGIC;
            ch2_txmldchainreq : in STD_LOGIC;
            ch2_txoneszeros : in STD_LOGIC;
            ch2_txpausedelayalign : in STD_LOGIC;
            ch2_txpcsresetmask : in STD_LOGIC;
            ch2_txphalignreq : in STD_LOGIC;
            ch2_txphdlypd : in STD_LOGIC;
            ch2_txphdlyreset : in STD_LOGIC;
            ch2_txphsetinitreq : in STD_LOGIC;
            ch2_txphshift180 : in STD_LOGIC;
            ch2_txpicodeovrden : in STD_LOGIC;
            ch2_txpicodereset : in STD_LOGIC;
            ch2_txpippmen : in STD_LOGIC;
            ch2_txpisopd : in STD_LOGIC;
            ch2_txpolarity : in STD_LOGIC;
            ch2_txprbsforceerr : in STD_LOGIC;
            ch2_txswing : in STD_LOGIC;
            ch2_txsyncallin : in STD_LOGIC;
            ch2_tx10gstat : out STD_LOGIC;
            ch2_txcomfinish : out STD_LOGIC;
            ch2_txdccdone : out STD_LOGIC;
            ch2_txdlyalignerr : out STD_LOGIC;
            ch2_txdlyalignprog : out STD_LOGIC;
            ch2_txphaligndone : out STD_LOGIC;
            ch2_txphalignerr : out STD_LOGIC;
            ch2_txphalignoutrsvd : out STD_LOGIC;
            ch2_txphdlyresetdone : out STD_LOGIC;
            ch2_txphsetinitdone : out STD_LOGIC;
            ch2_txphshift180done : out STD_LOGIC;
            ch2_txsyncdone : out STD_LOGIC;
            ch2_txbufstatus : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_txctrl0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_txctrl1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_txdeemph : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_txpd : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_txresetmode : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_txmstreset : in STD_LOGIC;
            ch2_txmstdatapathreset : in STD_LOGIC;
            ch2_txmstresetdone : out STD_LOGIC;
            ch2_txmargin : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch2_txpmaresetmask : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch2_txprbssel : in STD_LOGIC_VECTOR ( 3 downto 0 );
            ch2_txdiffctrl : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_txpippmstepsize : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_txpostcursor : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_txprecursor : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_txmaincursor : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch2_txctrl2 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_txrate : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_txprogdivresetdone : out STD_LOGIC;
            ch2_txpmaresetdone : out STD_LOGIC;
            ch2_txresetdone : out STD_LOGIC;
            ch2_txdataextendrsvd : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_txoutclk : out STD_LOGIC;
            ch2_txusrclk : in STD_LOGIC;
            ch3_txdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
            ch3_txheader : in STD_LOGIC_VECTOR ( 5 downto 0 );
            ch3_txsequence : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch3_gttxreset : in STD_LOGIC;
            ch3_txprogdivreset : in STD_LOGIC;
            ch3_txuserrdy : in STD_LOGIC;
            ch3_txphalignresetmask : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_txcominit : in STD_LOGIC;
            ch3_txcomsas : in STD_LOGIC;
            ch3_txcomwake : in STD_LOGIC;
            ch3_txdapicodeovrden : in STD_LOGIC;
            ch3_txdapicodereset : in STD_LOGIC;
            ch3_txdetectrx : in STD_LOGIC;
            ch3_txlatclk : in STD_LOGIC;
            ch3_txphdlytstclk : in STD_LOGIC;
            ch3_txdlyalignreq : in STD_LOGIC;
            ch3_txelecidle : in STD_LOGIC;
            ch3_txinhibit : in STD_LOGIC;
            ch3_txmldchaindone : in STD_LOGIC;
            ch3_txmldchainreq : in STD_LOGIC;
            ch3_txoneszeros : in STD_LOGIC;
            ch3_txpausedelayalign : in STD_LOGIC;
            ch3_txpcsresetmask : in STD_LOGIC;
            ch3_txphalignreq : in STD_LOGIC;
            ch3_txphdlypd : in STD_LOGIC;
            ch3_txphdlyreset : in STD_LOGIC;
            ch3_txphsetinitreq : in STD_LOGIC;
            ch3_txphshift180 : in STD_LOGIC;
            ch3_txpicodeovrden : in STD_LOGIC;
            ch3_txpicodereset : in STD_LOGIC;
            ch3_txpippmen : in STD_LOGIC;
            ch3_txpisopd : in STD_LOGIC;
            ch3_txpolarity : in STD_LOGIC;
            ch3_txprbsforceerr : in STD_LOGIC;
            ch3_txswing : in STD_LOGIC;
            ch3_txsyncallin : in STD_LOGIC;
            ch3_tx10gstat : out STD_LOGIC;
            ch3_txcomfinish : out STD_LOGIC;
            ch3_txdccdone : out STD_LOGIC;
            ch3_txdlyalignerr : out STD_LOGIC;
            ch3_txdlyalignprog : out STD_LOGIC;
            ch3_txphaligndone : out STD_LOGIC;
            ch3_txphalignerr : out STD_LOGIC;
            ch3_txphalignoutrsvd : out STD_LOGIC;
            ch3_txphdlyresetdone : out STD_LOGIC;
            ch3_txphsetinitdone : out STD_LOGIC;
            ch3_txphshift180done : out STD_LOGIC;
            ch3_txsyncdone : out STD_LOGIC;
            ch3_txbufstatus : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_txctrl0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_txctrl1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_txdeemph : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_txpd : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_txresetmode : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_txmstreset : in STD_LOGIC;
            ch3_txmstdatapathreset : in STD_LOGIC;
            ch3_txmstresetdone : out STD_LOGIC;
            ch3_txmargin : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch3_txpmaresetmask : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch3_txprbssel : in STD_LOGIC_VECTOR ( 3 downto 0 );
            ch3_txdiffctrl : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_txpippmstepsize : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_txpostcursor : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_txprecursor : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_txmaincursor : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch3_txctrl2 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_txrate : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_txprogdivresetdone : out STD_LOGIC;
            ch3_txpmaresetdone : out STD_LOGIC;
            ch3_txresetdone : out STD_LOGIC;
            ch3_txdataextendrsvd : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_txoutclk : out STD_LOGIC;
            ch3_txusrclk : in STD_LOGIC;
            ch0_rxdata : out STD_LOGIC_VECTOR ( 127 downto 0 );
            ch0_rxdatavalid : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxheader : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch0_rxgearboxslip : in STD_LOGIC;
            ch0_rxlatclk : in STD_LOGIC;
            ch0_gtrxreset : in STD_LOGIC;
            ch0_rxprogdivreset : in STD_LOGIC;
            ch0_rxuserrdy : in STD_LOGIC;
            ch0_rxprogdivresetdone : out STD_LOGIC;
            ch0_rxpmaresetdone : out STD_LOGIC;
            ch0_rxresetdone : out STD_LOGIC;
            ch0_rx10gstat : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_rxbufstatus : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch0_rxbyteisaligned : out STD_LOGIC;
            ch0_rxbyterealign : out STD_LOGIC;
            ch0_rxcdrhold : in STD_LOGIC;
            ch0_rxcdrlock : out STD_LOGIC;
            ch0_rxcdrovrden : in STD_LOGIC;
            ch0_rxcdrphdone : out STD_LOGIC;
            ch0_rxcdrreset : in STD_LOGIC;
            ch0_rxchanbondseq : out STD_LOGIC;
            ch0_rxchanisaligned : out STD_LOGIC;
            ch0_rxchanrealign : out STD_LOGIC;
            ch0_rxchbondi : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_rxchbondo : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_rxclkcorcnt : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxcominitdet : out STD_LOGIC;
            ch0_rxcommadet : out STD_LOGIC;
            ch0_rxcomsasdet : out STD_LOGIC;
            ch0_rxcomwakedet : out STD_LOGIC;
            ch0_rxctrl0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_rxctrl1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_rxctrl2 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_rxctrl3 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_rxdapicodeovrden : in STD_LOGIC;
            ch0_rxdapicodereset : in STD_LOGIC;
            ch0_rxdlyalignerr : out STD_LOGIC;
            ch0_rxdlyalignprog : out STD_LOGIC;
            ch0_rxdlyalignreq : in STD_LOGIC;
            ch0_rxelecidle : out STD_LOGIC;
            ch0_rxeqtraining : in STD_LOGIC;
            ch0_rxfinealigndone : out STD_LOGIC;
            ch0_rxheadervalid : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxlpmen : in STD_LOGIC;
            ch0_rxmldchaindone : in STD_LOGIC;
            ch0_rxmldchainreq : in STD_LOGIC;
            ch0_rxmlfinealignreq : in STD_LOGIC;
            ch0_rxoobreset : in STD_LOGIC;
            ch0_rxosintdone : out STD_LOGIC;
            ch0_rxpcsresetmask : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch0_rxpd : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxphaligndone : out STD_LOGIC;
            ch0_rxphalignerr : out STD_LOGIC;
            ch0_rxphalignreq : in STD_LOGIC;
            ch0_rxphalignresetmask : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxphdlypd : in STD_LOGIC;
            ch0_rxphdlyreset : in STD_LOGIC;
            ch0_rxphdlyresetdone : out STD_LOGIC;
            ch0_rxphsetinitdone : out STD_LOGIC;
            ch0_rxphsetinitreq : in STD_LOGIC;
            ch0_rxphshift180 : in STD_LOGIC;
            ch0_rxphshift180done : out STD_LOGIC;
            ch0_rxpmaresetmask : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch0_rxpolarity : in STD_LOGIC;
            ch0_rxprbscntreset : in STD_LOGIC;
            ch0_rxprbserr : out STD_LOGIC;
            ch0_rxprbslocked : out STD_LOGIC;
            ch0_rxprbssel : in STD_LOGIC_VECTOR ( 3 downto 0 );
            ch0_rxrate : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_rxresetmode : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxmstreset : in STD_LOGIC;
            ch0_rxmstdatapathreset : in STD_LOGIC;
            ch0_rxmstresetdone : out STD_LOGIC;
            ch0_rxslide : in STD_LOGIC;
            ch0_rxsliderdy : out STD_LOGIC;
            ch0_rxstartofseq : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch0_rxstatus : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch0_rxsyncallin : in STD_LOGIC;
            ch0_rxsyncdone : out STD_LOGIC;
            ch0_rxtermination : in STD_LOGIC;
            ch0_rxvalid : out STD_LOGIC;
            ch0_cdrbmcdrreq : in STD_LOGIC;
            ch0_cdrfreqos : in STD_LOGIC;
            ch0_cdrincpctrl : in STD_LOGIC;
            ch0_cdrstepdir : in STD_LOGIC;
            ch0_cdrstepsq : in STD_LOGIC;
            ch0_cdrstepsx : in STD_LOGIC;
            ch0_eyescanreset : in STD_LOGIC;
            ch0_eyescantrigger : in STD_LOGIC;
            ch0_eyescandataerror : out STD_LOGIC;
            ch0_cfokovrdrdy0 : out STD_LOGIC;
            ch0_cfokovrdrdy1 : out STD_LOGIC;
            ch0_rxdataextendrsvd : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_rxdccdone : out STD_LOGIC;
            ch0_rxosintstarted : out STD_LOGIC;
            ch0_rxosintstrobedone : out STD_LOGIC;
            ch0_rxosintstrobestarted : out STD_LOGIC;
            ch0_cfokovrdfinish : in STD_LOGIC;
            ch0_cfokovrdpulse : in STD_LOGIC;
            ch0_cfokovrdstart : in STD_LOGIC;
            ch0_rxoutclk : out STD_LOGIC;
            ch0_rxusrclk : in STD_LOGIC;
            ch1_rxdata : out STD_LOGIC_VECTOR ( 127 downto 0 );
            ch1_rxdatavalid : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxheader : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch1_rxgearboxslip : in STD_LOGIC;
            ch1_rxlatclk : in STD_LOGIC;
            ch1_gtrxreset : in STD_LOGIC;
            ch1_rxprogdivreset : in STD_LOGIC;
            ch1_rxuserrdy : in STD_LOGIC;
            ch1_rxprogdivresetdone : out STD_LOGIC;
            ch1_rxpmaresetdone : out STD_LOGIC;
            ch1_rxresetdone : out STD_LOGIC;
            ch1_rx10gstat : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_rxbufstatus : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch1_rxbyteisaligned : out STD_LOGIC;
            ch1_rxbyterealign : out STD_LOGIC;
            ch1_rxcdrhold : in STD_LOGIC;
            ch1_rxcdrlock : out STD_LOGIC;
            ch1_rxcdrovrden : in STD_LOGIC;
            ch1_rxcdrphdone : out STD_LOGIC;
            ch1_rxcdrreset : in STD_LOGIC;
            ch1_rxchanbondseq : out STD_LOGIC;
            ch1_rxchanisaligned : out STD_LOGIC;
            ch1_rxchanrealign : out STD_LOGIC;
            ch1_rxchbondi : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_rxchbondo : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_rxclkcorcnt : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxcominitdet : out STD_LOGIC;
            ch1_rxcommadet : out STD_LOGIC;
            ch1_rxcomsasdet : out STD_LOGIC;
            ch1_rxcomwakedet : out STD_LOGIC;
            ch1_rxctrl0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_rxctrl1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_rxctrl2 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_rxctrl3 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_rxdapicodeovrden : in STD_LOGIC;
            ch1_rxdapicodereset : in STD_LOGIC;
            ch1_rxdlyalignerr : out STD_LOGIC;
            ch1_rxdlyalignprog : out STD_LOGIC;
            ch1_rxdlyalignreq : in STD_LOGIC;
            ch1_rxelecidle : out STD_LOGIC;
            ch1_rxeqtraining : in STD_LOGIC;
            ch1_rxfinealigndone : out STD_LOGIC;
            ch1_rxheadervalid : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxlpmen : in STD_LOGIC;
            ch1_rxmldchaindone : in STD_LOGIC;
            ch1_rxmldchainreq : in STD_LOGIC;
            ch1_rxmlfinealignreq : in STD_LOGIC;
            ch1_rxoobreset : in STD_LOGIC;
            ch1_rxosintdone : out STD_LOGIC;
            ch1_rxpcsresetmask : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch1_rxpd : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxphaligndone : out STD_LOGIC;
            ch1_rxphalignerr : out STD_LOGIC;
            ch1_rxphalignreq : in STD_LOGIC;
            ch1_rxphalignresetmask : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxphdlypd : in STD_LOGIC;
            ch1_rxphdlyreset : in STD_LOGIC;
            ch1_rxphdlyresetdone : out STD_LOGIC;
            ch1_rxphsetinitdone : out STD_LOGIC;
            ch1_rxphsetinitreq : in STD_LOGIC;
            ch1_rxphshift180 : in STD_LOGIC;
            ch1_rxphshift180done : out STD_LOGIC;
            ch1_rxpmaresetmask : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch1_rxpolarity : in STD_LOGIC;
            ch1_rxprbscntreset : in STD_LOGIC;
            ch1_rxprbserr : out STD_LOGIC;
            ch1_rxprbslocked : out STD_LOGIC;
            ch1_rxprbssel : in STD_LOGIC_VECTOR ( 3 downto 0 );
            ch1_rxrate : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_rxresetmode : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxmstreset : in STD_LOGIC;
            ch1_rxmstdatapathreset : in STD_LOGIC;
            ch1_rxmstresetdone : out STD_LOGIC;
            ch1_rxslide : in STD_LOGIC;
            ch1_rxsliderdy : out STD_LOGIC;
            ch1_rxstartofseq : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch1_rxstatus : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch1_rxsyncallin : in STD_LOGIC;
            ch1_rxsyncdone : out STD_LOGIC;
            ch1_rxtermination : in STD_LOGIC;
            ch1_rxvalid : out STD_LOGIC;
            ch1_cdrbmcdrreq : in STD_LOGIC;
            ch1_cdrfreqos : in STD_LOGIC;
            ch1_cdrincpctrl : in STD_LOGIC;
            ch1_cdrstepdir : in STD_LOGIC;
            ch1_cdrstepsq : in STD_LOGIC;
            ch1_cdrstepsx : in STD_LOGIC;
            ch1_eyescanreset : in STD_LOGIC;
            ch1_eyescantrigger : in STD_LOGIC;
            ch1_eyescandataerror : out STD_LOGIC;
            ch1_cfokovrdrdy0 : out STD_LOGIC;
            ch1_cfokovrdrdy1 : out STD_LOGIC;
            ch1_rxdataextendrsvd : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch1_rxdccdone : out STD_LOGIC;
            ch1_rxosintstarted : out STD_LOGIC;
            ch1_rxosintstrobedone : out STD_LOGIC;
            ch1_rxosintstrobestarted : out STD_LOGIC;
            ch1_cfokovrdfinish : in STD_LOGIC;
            ch1_cfokovrdpulse : in STD_LOGIC;
            ch1_cfokovrdstart : in STD_LOGIC;
            ch1_rxoutclk : out STD_LOGIC;
            ch1_rxusrclk : in STD_LOGIC;
            ch2_rxdata : out STD_LOGIC_VECTOR ( 127 downto 0 );
            ch2_rxdatavalid : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxheader : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch2_rxgearboxslip : in STD_LOGIC;
            ch2_rxlatclk : in STD_LOGIC;
            ch2_gtrxreset : in STD_LOGIC;
            ch2_rxprogdivreset : in STD_LOGIC;
            ch2_rxuserrdy : in STD_LOGIC;
            ch2_rxprogdivresetdone : out STD_LOGIC;
            ch2_rxpmaresetdone : out STD_LOGIC;
            ch2_rxresetdone : out STD_LOGIC;
            ch2_rx10gstat : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_rxbufstatus : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch2_rxbyteisaligned : out STD_LOGIC;
            ch2_rxbyterealign : out STD_LOGIC;
            ch2_rxcdrhold : in STD_LOGIC;
            ch2_rxcdrlock : out STD_LOGIC;
            ch2_rxcdrovrden : in STD_LOGIC;
            ch2_rxcdrphdone : out STD_LOGIC;
            ch2_rxcdrreset : in STD_LOGIC;
            ch2_rxchanbondseq : out STD_LOGIC;
            ch2_rxchanisaligned : out STD_LOGIC;
            ch2_rxchanrealign : out STD_LOGIC;
            ch2_rxchbondi : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_rxchbondo : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_rxclkcorcnt : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxcominitdet : out STD_LOGIC;
            ch2_rxcommadet : out STD_LOGIC;
            ch2_rxcomsasdet : out STD_LOGIC;
            ch2_rxcomwakedet : out STD_LOGIC;
            ch2_rxctrl0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_rxctrl1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_rxctrl2 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_rxctrl3 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_rxdapicodeovrden : in STD_LOGIC;
            ch2_rxdapicodereset : in STD_LOGIC;
            ch2_rxdlyalignerr : out STD_LOGIC;
            ch2_rxdlyalignprog : out STD_LOGIC;
            ch2_rxdlyalignreq : in STD_LOGIC;
            ch2_rxelecidle : out STD_LOGIC;
            ch2_rxeqtraining : in STD_LOGIC;
            ch2_rxfinealigndone : out STD_LOGIC;
            ch2_rxheadervalid : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxlpmen : in STD_LOGIC;
            ch2_rxmldchaindone : in STD_LOGIC;
            ch2_rxmldchainreq : in STD_LOGIC;
            ch2_rxmlfinealignreq : in STD_LOGIC;
            ch2_rxoobreset : in STD_LOGIC;
            ch2_rxosintdone : out STD_LOGIC;
            ch2_rxpcsresetmask : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch2_rxpd : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxphaligndone : out STD_LOGIC;
            ch2_rxphalignerr : out STD_LOGIC;
            ch2_rxphalignreq : in STD_LOGIC;
            ch2_rxphalignresetmask : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxphdlypd : in STD_LOGIC;
            ch2_rxphdlyreset : in STD_LOGIC;
            ch2_rxphdlyresetdone : out STD_LOGIC;
            ch2_rxphsetinitdone : out STD_LOGIC;
            ch2_rxphsetinitreq : in STD_LOGIC;
            ch2_rxphshift180 : in STD_LOGIC;
            ch2_rxphshift180done : out STD_LOGIC;
            ch2_rxpmaresetmask : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch2_rxpolarity : in STD_LOGIC;
            ch2_rxprbscntreset : in STD_LOGIC;
            ch2_rxprbserr : out STD_LOGIC;
            ch2_rxprbslocked : out STD_LOGIC;
            ch2_rxprbssel : in STD_LOGIC_VECTOR ( 3 downto 0 );
            ch2_rxrate : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_rxresetmode : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxmstreset : in STD_LOGIC;
            ch2_rxmstdatapathreset : in STD_LOGIC;
            ch2_rxmstresetdone : out STD_LOGIC;
            ch2_rxslide : in STD_LOGIC;
            ch2_rxsliderdy : out STD_LOGIC;
            ch2_rxstartofseq : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch2_rxstatus : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch2_rxsyncallin : in STD_LOGIC;
            ch2_rxsyncdone : out STD_LOGIC;
            ch2_rxtermination : in STD_LOGIC;
            ch2_rxvalid : out STD_LOGIC;
            ch2_cdrbmcdrreq : in STD_LOGIC;
            ch2_cdrfreqos : in STD_LOGIC;
            ch2_cdrincpctrl : in STD_LOGIC;
            ch2_cdrstepdir : in STD_LOGIC;
            ch2_cdrstepsq : in STD_LOGIC;
            ch2_cdrstepsx : in STD_LOGIC;
            ch2_eyescanreset : in STD_LOGIC;
            ch2_eyescantrigger : in STD_LOGIC;
            ch2_eyescandataerror : out STD_LOGIC;
            ch2_cfokovrdrdy0 : out STD_LOGIC;
            ch2_cfokovrdrdy1 : out STD_LOGIC;
            ch2_rxdataextendrsvd : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch2_rxdccdone : out STD_LOGIC;
            ch2_rxosintstarted : out STD_LOGIC;
            ch2_rxosintstrobedone : out STD_LOGIC;
            ch2_rxosintstrobestarted : out STD_LOGIC;
            ch2_cfokovrdfinish : in STD_LOGIC;
            ch2_cfokovrdpulse : in STD_LOGIC;
            ch2_cfokovrdstart : in STD_LOGIC;
            ch2_rxoutclk : out STD_LOGIC;
            ch2_rxusrclk : in STD_LOGIC;
            ch3_rxdata : out STD_LOGIC_VECTOR ( 127 downto 0 );
            ch3_rxdatavalid : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxheader : out STD_LOGIC_VECTOR ( 5 downto 0 );
            ch3_rxgearboxslip : in STD_LOGIC;
            ch3_rxlatclk : in STD_LOGIC;
            ch3_gtrxreset : in STD_LOGIC;
            ch3_rxprogdivreset : in STD_LOGIC;
            ch3_rxuserrdy : in STD_LOGIC;
            ch3_rxprogdivresetdone : out STD_LOGIC;
            ch3_rxpmaresetdone : out STD_LOGIC;
            ch3_rxresetdone : out STD_LOGIC;
            ch3_rx10gstat : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_rxbufstatus : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch3_rxbyteisaligned : out STD_LOGIC;
            ch3_rxbyterealign : out STD_LOGIC;
            ch3_rxcdrhold : in STD_LOGIC;
            ch3_rxcdrlock : out STD_LOGIC;
            ch3_rxcdrovrden : in STD_LOGIC;
            ch3_rxcdrphdone : out STD_LOGIC;
            ch3_rxcdrreset : in STD_LOGIC;
            ch3_rxchanbondseq : out STD_LOGIC;
            ch3_rxchanisaligned : out STD_LOGIC;
            ch3_rxchanrealign : out STD_LOGIC;
            ch3_rxchbondi : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_rxchbondo : out STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_rxclkcorcnt : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxcominitdet : out STD_LOGIC;
            ch3_rxcommadet : out STD_LOGIC;
            ch3_rxcomsasdet : out STD_LOGIC;
            ch3_rxcomwakedet : out STD_LOGIC;
            ch3_rxctrl0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_rxctrl1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_rxctrl2 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_rxctrl3 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_rxdapicodeovrden : in STD_LOGIC;
            ch3_rxdapicodereset : in STD_LOGIC;
            ch3_rxdlyalignerr : out STD_LOGIC;
            ch3_rxdlyalignprog : out STD_LOGIC;
            ch3_rxdlyalignreq : in STD_LOGIC;
            ch3_rxelecidle : out STD_LOGIC;
            ch3_rxeqtraining : in STD_LOGIC;
            ch3_rxfinealigndone : out STD_LOGIC;
            ch3_rxheadervalid : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxlpmen : in STD_LOGIC;
            ch3_rxmldchaindone : in STD_LOGIC;
            ch3_rxmldchainreq : in STD_LOGIC;
            ch3_rxmlfinealignreq : in STD_LOGIC;
            ch3_rxoobreset : in STD_LOGIC;
            ch3_rxosintdone : out STD_LOGIC;
            ch3_rxpcsresetmask : in STD_LOGIC_VECTOR ( 4 downto 0 );
            ch3_rxpd : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxphaligndone : out STD_LOGIC;
            ch3_rxphalignerr : out STD_LOGIC;
            ch3_rxphalignreq : in STD_LOGIC;
            ch3_rxphalignresetmask : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxphdlypd : in STD_LOGIC;
            ch3_rxphdlyreset : in STD_LOGIC;
            ch3_rxphdlyresetdone : out STD_LOGIC;
            ch3_rxphsetinitdone : out STD_LOGIC;
            ch3_rxphsetinitreq : in STD_LOGIC;
            ch3_rxphshift180 : in STD_LOGIC;
            ch3_rxphshift180done : out STD_LOGIC;
            ch3_rxpmaresetmask : in STD_LOGIC_VECTOR ( 6 downto 0 );
            ch3_rxpolarity : in STD_LOGIC;
            ch3_rxprbscntreset : in STD_LOGIC;
            ch3_rxprbserr : out STD_LOGIC;
            ch3_rxprbslocked : out STD_LOGIC;
            ch3_rxprbssel : in STD_LOGIC_VECTOR ( 3 downto 0 );
            ch3_rxrate : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_rxresetmode : in STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxmstreset : in STD_LOGIC;
            ch3_rxmstdatapathreset : in STD_LOGIC;
            ch3_rxmstresetdone : out STD_LOGIC;
            ch3_rxslide : in STD_LOGIC;
            ch3_rxsliderdy : out STD_LOGIC;
            ch3_rxstartofseq : out STD_LOGIC_VECTOR ( 1 downto 0 );
            ch3_rxstatus : out STD_LOGIC_VECTOR ( 2 downto 0 );
            ch3_rxsyncallin : in STD_LOGIC;
            ch3_rxsyncdone : out STD_LOGIC;
            ch3_rxtermination : in STD_LOGIC;
            ch3_rxvalid : out STD_LOGIC;
            ch3_cdrbmcdrreq : in STD_LOGIC;
            ch3_cdrfreqos : in STD_LOGIC;
            ch3_cdrincpctrl : in STD_LOGIC;
            ch3_cdrstepdir : in STD_LOGIC;
            ch3_cdrstepsq : in STD_LOGIC;
            ch3_cdrstepsx : in STD_LOGIC;
            ch3_eyescanreset : in STD_LOGIC;
            ch3_eyescantrigger : in STD_LOGIC;
            ch3_eyescandataerror : out STD_LOGIC;
            ch3_cfokovrdrdy0 : out STD_LOGIC;
            ch3_cfokovrdrdy1 : out STD_LOGIC;
            ch3_rxdataextendrsvd : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch3_rxdccdone : out STD_LOGIC;
            ch3_rxosintstarted : out STD_LOGIC;
            ch3_rxosintstrobedone : out STD_LOGIC;
            ch3_rxosintstrobestarted : out STD_LOGIC;
            ch3_cfokovrdfinish : in STD_LOGIC;
            ch3_cfokovrdpulse : in STD_LOGIC;
            ch3_cfokovrdstart : in STD_LOGIC;
            ch3_rxoutclk : out STD_LOGIC;
            ch3_rxusrclk : in STD_LOGIC;
            ch0_bufgtce : out STD_LOGIC;
            ch0_bufgtrst : out STD_LOGIC;
            ch0_bufgtcemask : out STD_LOGIC_VECTOR ( 3 downto 0 );
            ch0_bufgtrstmask : out STD_LOGIC_VECTOR ( 3 downto 0 );
            ch0_bufgtdiv : out STD_LOGIC_VECTOR ( 11 downto 0 );
            ch0_clkrsvd0 : in STD_LOGIC;
            ch0_clkrsvd1 : in STD_LOGIC;
            ch0_dmonitorclk : in STD_LOGIC;
            ch0_phyesmadaptsave : in STD_LOGIC;
            ch0_iloresetmask : in STD_LOGIC;
            ch0_loopback : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch0_dmonfiforeset : in STD_LOGIC;
            ch0_pcsrsvdin : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_gtrsvd : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_tstin : in STD_LOGIC_VECTOR ( 19 downto 0 );
            ch0_pcsrsvdout : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_pinrsvdas : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_dmonitoroutclk : out STD_LOGIC;
            ch0_resetexception : out STD_LOGIC;
            ch0_dmonitorout : out STD_LOGIC_VECTOR ( 31 downto 0 );
            ch0_phyready : out STD_LOGIC;
            ch0_hsdppcsreset : in STD_LOGIC;
            ch1_bufgtce : out STD_LOGIC;
            ch1_bufgtrst : out STD_LOGIC;
            ch1_bufgtcemask : out STD_LOGIC_VECTOR ( 3 downto 0 );
            ch1_bufgtrstmask : out STD_LOGIC_VECTOR ( 3 downto 0 );
            ch1_bufgtdiv : out STD_LOGIC_VECTOR ( 11 downto 0 );
            ch1_clkrsvd0 : in STD_LOGIC;
            ch1_clkrsvd1 : in STD_LOGIC;
            ch1_dmonitorclk : in STD_LOGIC;
            ch1_phyesmadaptsave : in STD_LOGIC;
            ch1_iloresetmask : in STD_LOGIC;
            ch1_loopback : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch1_dmonfiforeset : in STD_LOGIC;
            ch1_pcsrsvdin : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_gtrsvd : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_tstin : in STD_LOGIC_VECTOR ( 19 downto 0 );
            ch1_pcsrsvdout : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_pinrsvdas : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch1_dmonitoroutclk : out STD_LOGIC;
            ch1_resetexception : out STD_LOGIC;
            ch1_dmonitorout : out STD_LOGIC_VECTOR ( 31 downto 0 );
            ch1_phyready : out STD_LOGIC;
            ch1_hsdppcsreset : in STD_LOGIC;
            ch2_bufgtce : out STD_LOGIC;
            ch2_bufgtrst : out STD_LOGIC;
            ch2_bufgtcemask : out STD_LOGIC_VECTOR ( 3 downto 0 );
            ch2_bufgtrstmask : out STD_LOGIC_VECTOR ( 3 downto 0 );
            ch2_bufgtdiv : out STD_LOGIC_VECTOR ( 11 downto 0 );
            ch2_clkrsvd0 : in STD_LOGIC;
            ch2_clkrsvd1 : in STD_LOGIC;
            ch2_dmonitorclk : in STD_LOGIC;
            ch2_phyesmadaptsave : in STD_LOGIC;
            ch2_iloresetmask : in STD_LOGIC;
            ch2_loopback : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch2_dmonfiforeset : in STD_LOGIC;
            ch2_pcsrsvdin : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_gtrsvd : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_tstin : in STD_LOGIC_VECTOR ( 19 downto 0 );
            ch2_pcsrsvdout : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_pinrsvdas : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch2_dmonitoroutclk : out STD_LOGIC;
            ch2_resetexception : out STD_LOGIC;
            ch2_dmonitorout : out STD_LOGIC_VECTOR ( 31 downto 0 );
            ch2_phyready : out STD_LOGIC;
            ch2_hsdppcsreset : in STD_LOGIC;
            ch3_bufgtce : out STD_LOGIC;
            ch3_bufgtrst : out STD_LOGIC;
            ch3_bufgtcemask : out STD_LOGIC_VECTOR ( 3 downto 0 );
            ch3_bufgtrstmask : out STD_LOGIC_VECTOR ( 3 downto 0 );
            ch3_bufgtdiv : out STD_LOGIC_VECTOR ( 11 downto 0 );
            ch3_clkrsvd0 : in STD_LOGIC;
            ch3_clkrsvd1 : in STD_LOGIC;
            ch3_dmonitorclk : in STD_LOGIC;
            ch3_phyesmadaptsave : in STD_LOGIC;
            ch3_iloresetmask : in STD_LOGIC;
            ch3_loopback : in STD_LOGIC_VECTOR ( 2 downto 0 );
            ch3_dmonfiforeset : in STD_LOGIC;
            ch3_pcsrsvdin : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_gtrsvd : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_tstin : in STD_LOGIC_VECTOR ( 19 downto 0 );
            ch3_pcsrsvdout : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_pinrsvdas : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch3_dmonitoroutclk : out STD_LOGIC;
            ch3_resetexception : out STD_LOGIC;
            ch3_dmonitorout : out STD_LOGIC_VECTOR ( 31 downto 0 );
            ch3_phyready : out STD_LOGIC;
            ch3_hsdppcsreset : in STD_LOGIC;
            resetdone_northin : in STD_LOGIC_VECTOR ( 1 downto 0 );
            resetdone_southin : in STD_LOGIC_VECTOR ( 1 downto 0 );
            resetdone_northout : out STD_LOGIC_VECTOR ( 1 downto 0 );
            resetdone_southout : out STD_LOGIC_VECTOR ( 1 downto 0 );
            txpinorthin : in STD_LOGIC_VECTOR ( 3 downto 0 );
            rxpinorthin : in STD_LOGIC_VECTOR ( 3 downto 0 );
            txpisouthin : in STD_LOGIC_VECTOR ( 3 downto 0 );
            rxpisouthin : in STD_LOGIC_VECTOR ( 3 downto 0 );
            pipenorthin : in STD_LOGIC_VECTOR ( 5 downto 0 );
            pipesouthin : in STD_LOGIC_VECTOR ( 5 downto 0 );
            txpinorthout : out STD_LOGIC_VECTOR ( 3 downto 0 );
            txpisouthout : out STD_LOGIC_VECTOR ( 3 downto 0 );
            rxpinorthout : out STD_LOGIC_VECTOR ( 3 downto 0 );
            rxpisouthout : out STD_LOGIC_VECTOR ( 3 downto 0 );
            pipenorthout : out STD_LOGIC_VECTOR ( 5 downto 0 );
            pipesouthout : out STD_LOGIC_VECTOR ( 5 downto 0 );
            GT_REFCLK0 : in STD_LOGIC;
            bgbypassb : in STD_LOGIC;
            bgmonitorenb : in STD_LOGIC;
            bgpdb : in STD_LOGIC;
            bgrcalovrdenb : in STD_LOGIC;
            bgrcalovrd : in STD_LOGIC_VECTOR ( 4 downto 0 );
            debugtraceready : in STD_LOGIC;
            debugtraceclk : in STD_LOGIC;
            rcalenb : in STD_LOGIC;
            trigackout0 : in STD_LOGIC;
            trigin0 : in STD_LOGIC;
            ubenable : in STD_LOGIC;
            ubiolmbrst : in STD_LOGIC;
            ubmbrst : in STD_LOGIC;
            ubintr : in STD_LOGIC_VECTOR ( 11 downto 0 );
            ubrxuart : in STD_LOGIC;
            ctrlrsvdin0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ctrlrsvdin1 : in STD_LOGIC_VECTOR ( 13 downto 0 );
            gpi : in STD_LOGIC_VECTOR ( 15 downto 0 );
            refclk0_clktestsig : in STD_LOGIC;
            refclk1_clktestsig : in STD_LOGIC;
            correcterr : out STD_LOGIC;
            debugtracetvalid : out STD_LOGIC;
            debugtracetdata : out STD_LOGIC_VECTOR ( 15 downto 0 );
            refclk0_gtrefclkpdint : out STD_LOGIC;
            refclk0_clktestsigint : out STD_LOGIC;
            refclk1_gtrefclkpdint : out STD_LOGIC;
            refclk1_clktestsigint : out STD_LOGIC;
            trigackin0 : out STD_LOGIC;
            trigout0 : out STD_LOGIC;
            ubinterrupt : out STD_LOGIC;
            ubtxuart : out STD_LOGIC;
            uncorrecterr : out STD_LOGIC;
            ctrlrsvdout : out STD_LOGIC_VECTOR ( 31 downto 0 );
            gpo : out STD_LOGIC_VECTOR ( 15 downto 0 );
            hsclk0_rxrecclksel : out STD_LOGIC_VECTOR ( 1 downto 0 );
            hsclk1_rxrecclksel : out STD_LOGIC_VECTOR ( 1 downto 0 );
            altclk : in STD_LOGIC;
            hsclk0_lcpllclkrsvd0 : in STD_LOGIC;
            hsclk0_lcpllclkrsvd1 : in STD_LOGIC;
            hsclk0_rpllclkrsvd0 : in STD_LOGIC;
            hsclk0_rpllclkrsvd1 : in STD_LOGIC;
            hsclk1_lcpllclkrsvd0 : in STD_LOGIC;
            hsclk1_lcpllclkrsvd1 : in STD_LOGIC;
            hsclk1_rpllclkrsvd0 : in STD_LOGIC;
            hsclk1_rpllclkrsvd1 : in STD_LOGIC;
            hsclk0_lcpllrsvd0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            hsclk0_lcpllrsvd1 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            hsclk0_rpllrsvd0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            hsclk0_rpllrsvd1 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            hsclk1_lcpllrsvd0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            hsclk1_lcpllrsvd1 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            hsclk1_rpllrsvd0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            hsclk1_rpllrsvd1 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            hsclk0_lcpllrsvdout : out STD_LOGIC_VECTOR ( 7 downto 0 );
            hsclk1_lcpllrsvdout : out STD_LOGIC_VECTOR ( 7 downto 0 );
            hsclk0_rpllrsvdout : out STD_LOGIC_VECTOR ( 7 downto 0 );
            hsclk1_rpllrsvdout : out STD_LOGIC_VECTOR ( 7 downto 0 );
            apb3clk : in STD_LOGIC;
            apb3paddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
            apb3penable : in STD_LOGIC;
            apb3presetn : in STD_LOGIC;
            apb3prdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
            apb3psel : in STD_LOGIC;
            apb3pslverr : out STD_LOGIC;
            apb3pready : out STD_LOGIC;
            apb3pwdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
            apb3pwrite : in STD_LOGIC;
            rxp : in STD_LOGIC_VECTOR ( 3 downto 0 );
            rxn : in STD_LOGIC_VECTOR ( 3 downto 0 );
            txp : out STD_LOGIC_VECTOR ( 3 downto 0 );
            txn : out STD_LOGIC_VECTOR ( 3 downto 0 )
        );
    end component transceiver_versal_lpgbt_gt_quad_base_0;
    component transceiver_versal_lpgbt_url_gpo_0 is
        port (
            Op1 : in STD_LOGIC_VECTOR ( 3 downto 0 );
            Res : out STD_LOGIC
        );
    end component transceiver_versal_lpgbt_url_gpo_0;
    component transceiver_versal_lpgbt_urlp_0 is
        port (
            Op1 : in STD_LOGIC_VECTOR ( 0 to 0 );
            Res : out STD_LOGIC
        );
    end component transceiver_versal_lpgbt_urlp_0;
    component transceiver_versal_lpgbt_util_ds_buf1_0 is
        port (
            IBUF_DS_P : in STD_LOGIC_VECTOR ( 0 to 0 );
            IBUF_DS_N : in STD_LOGIC_VECTOR ( 0 to 0 );
            IBUF_DS_CEB : in STD_LOGIC_VECTOR ( 0 to 0 );
            IBUF_OUT : out STD_LOGIC_VECTOR ( 0 to 0 );
            IBUF_DS_ODIV2 : out STD_LOGIC_VECTOR ( 0 to 0 )
        );
    end component transceiver_versal_lpgbt_util_ds_buf1_0;
    component transceiver_versal_lpgbt_util_ds_buf_0 is
        port (
            IBUF_DS_P : in STD_LOGIC_VECTOR ( 0 to 0 );
            IBUF_DS_N : in STD_LOGIC_VECTOR ( 0 to 0 );
            IBUF_DS_CEB : in STD_LOGIC_VECTOR ( 0 to 0 );
            IBUF_OUT : out STD_LOGIC_VECTOR ( 0 to 0 );
            IBUF_DS_ODIV2 : out STD_LOGIC_VECTOR ( 0 to 0 )
        );
    end component transceiver_versal_lpgbt_util_ds_buf_0;
    component transceiver_versal_lpgbt_util_reduced_logic_0_0 is
        port (
            Op1 : in STD_LOGIC_VECTOR ( 3 downto 0 );
            Res : out STD_LOGIC
        );
    end component transceiver_versal_lpgbt_util_reduced_logic_0_0;
    component transceiver_versal_lpgbt_xlc_gpi_0 is
        port (
            In0 : in STD_LOGIC_VECTOR ( 0 to 0 );
            In1 : in STD_LOGIC_VECTOR ( 0 to 0 );
            In2 : in STD_LOGIC_VECTOR ( 0 to 0 );
            In3 : in STD_LOGIC_VECTOR ( 0 to 0 );
            In4 : in STD_LOGIC_VECTOR ( 0 to 0 );
            In5 : in STD_LOGIC_VECTOR ( 0 to 0 );
            In6 : in STD_LOGIC_VECTOR ( 0 to 0 );
            In7 : in STD_LOGIC_VECTOR ( 0 to 0 );
            In8 : in STD_LOGIC_VECTOR ( 0 to 0 );
            In9 : in STD_LOGIC_VECTOR ( 0 to 0 );
            In10 : in STD_LOGIC_VECTOR ( 0 to 0 );
            In11 : in STD_LOGIC_VECTOR ( 0 to 0 );
            In12 : in STD_LOGIC_VECTOR ( 0 to 0 );
            In13 : in STD_LOGIC_VECTOR ( 0 to 0 );
            In14 : in STD_LOGIC_VECTOR ( 0 to 0 );
            In15 : in STD_LOGIC_VECTOR ( 0 to 0 );
            dout : out STD_LOGIC_VECTOR ( 15 downto 0 )
        );
    end component transceiver_versal_lpgbt_xlc_gpi_0;
    component transceiver_versal_lpgbt_xlc_gpo_0 is
        port (
            In0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
            dout : out STD_LOGIC_VECTOR ( 3 downto 0 )
        );
    end component transceiver_versal_lpgbt_xlc_gpo_0;
    component transceiver_versal_lpgbt_xlconcat_0_0 is
        port (
            In0 : in STD_LOGIC_VECTOR ( 0 to 0 );
            In1 : in STD_LOGIC_VECTOR ( 0 to 0 );
            In2 : in STD_LOGIC_VECTOR ( 0 to 0 );
            In3 : in STD_LOGIC_VECTOR ( 0 to 0 );
            dout : out STD_LOGIC_VECTOR ( 3 downto 0 )
        );
    end component transceiver_versal_lpgbt_xlconcat_0_0;
    component transceiver_versal_lpgbt_xlconcat_1_0 is
        port (
            In0 : in STD_LOGIC_VECTOR ( 0 to 0 );
            In1 : in STD_LOGIC_VECTOR ( 0 to 0 );
            In2 : in STD_LOGIC_VECTOR ( 0 to 0 );
            In3 : in STD_LOGIC_VECTOR ( 0 to 0 );
            dout : out STD_LOGIC_VECTOR ( 3 downto 0 )
        );
    end component transceiver_versal_lpgbt_xlconcat_1_0;
    component transceiver_versal_lpgbt_xlcp_0 is
        port (
            In0 : in STD_LOGIC_VECTOR ( 0 to 0 );
            dout : out STD_LOGIC_VECTOR ( 0 to 0 )
        );
    end component transceiver_versal_lpgbt_xlcp_0;
    component transceiver_versal_lpgbt_xls_gpo_0 is
        port (
            Din : in STD_LOGIC_VECTOR ( 15 downto 0 );
            Dout : out STD_LOGIC_VECTOR ( 3 downto 0 )
        );
    end component transceiver_versal_lpgbt_xls_gpo_0;
    signal APB3_INTF_0_1_PADDR : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal APB3_INTF_0_1_PENABLE : STD_LOGIC;
    signal APB3_INTF_0_1_PRDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
    signal APB3_INTF_0_1_PREADY : STD_LOGIC;
    signal APB3_INTF_0_1_PSEL : STD_LOGIC;
    signal APB3_INTF_0_1_PSLVERR : STD_LOGIC;
    signal APB3_INTF_0_1_PWDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
    signal APB3_INTF_0_1_PWRITE : STD_LOGIC;
    signal CLK_IN_D_0_1_CLK_N : STD_LOGIC_VECTOR ( 0 to 0 );
    signal CLK_IN_D_0_1_CLK_P : STD_LOGIC_VECTOR ( 0 to 0 );
    signal apb3clk_gt_bridge_ip_0_1 : STD_LOGIC;
    signal apb3clk_quad_1 : STD_LOGIC;
    signal bufg_gt_1_usrclk : STD_LOGIC;
    signal bufg_gt_usrclk : STD_LOGIC;
    signal ch0_loopback_0_1 : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal ch0_rxgearboxslip_ext_0_1 : STD_LOGIC;
    signal ch0_rxpolarity_ext_0_1 : STD_LOGIC;
    signal ch0_rxslide_ext_0_1 : STD_LOGIC;
    signal ch0_txctrl0_ext_0_1 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal ch0_txctrl1_ext_0_1 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal ch0_txctrl2_ext_0_1 : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal ch0_txdata_ext_0_1 : STD_LOGIC_VECTOR ( 127 downto 0 );
    signal ch0_txheader_ext_0_1 : STD_LOGIC_VECTOR ( 5 downto 0 );
    signal ch0_txpippmen_ext_0_1 : STD_LOGIC;
    signal ch0_txpippmstepsize_ext_0_1 : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal ch0_txpolarity_ext_0_1 : STD_LOGIC;
    signal ch0_txsequence_ext_0_1 : STD_LOGIC_VECTOR ( 6 downto 0 );
    signal ch1_loopback_0_1 : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal ch1_rxgearboxslip_ext_0_1 : STD_LOGIC;
    signal ch1_rxpolarity_ext_0_1 : STD_LOGIC;
    signal ch1_rxslide_ext_0_1 : STD_LOGIC;
    signal ch1_txctrl0_ext_0_1 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal ch1_txctrl1_ext_0_1 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal ch1_txctrl2_ext_0_1 : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal ch1_txdata_ext_0_1 : STD_LOGIC_VECTOR ( 127 downto 0 );
    signal ch1_txpippmen_ext_0_1 : STD_LOGIC;
    signal ch1_txpippmstepsize_ext_0_1 : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal ch1_txpolarity_ext_0_1 : STD_LOGIC;
    signal ch1_txsequence_ext_0_1 : STD_LOGIC_VECTOR ( 6 downto 0 );
    signal ch2_loopback_0_1 : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal ch2_rxgearboxslip_ext_0_1 : STD_LOGIC;
    signal ch2_rxpolarity_ext_0_1 : STD_LOGIC;
    signal ch2_rxslide_ext_0_1 : STD_LOGIC;
    signal ch2_txctrl0_ext_0_1 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal ch2_txctrl1_ext_0_1 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal ch2_txctrl2_ext_0_1 : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal ch2_txdata_ext_0_1 : STD_LOGIC_VECTOR ( 127 downto 0 );
    signal ch2_txpippmen_ext_0_1 : STD_LOGIC;
    signal ch2_txpippmstepsize_ext_0_1 : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal ch2_txpolarity_ext_0_1 : STD_LOGIC;
    signal ch2_txsequence_ext_0_1 : STD_LOGIC_VECTOR ( 6 downto 0 );
    signal ch3_loopback_0_1 : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal ch3_rxgearboxslip_ext_0_1 : STD_LOGIC;
    signal ch3_rxpolarity_ext_0_1 : STD_LOGIC;
    signal ch3_rxslide_ext_0_1 : STD_LOGIC;
    signal ch3_txctrl0_ext_0_1 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal ch3_txctrl1_ext_0_1 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal ch3_txctrl2_ext_0_1 : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal ch3_txdata_ext_0_1 : STD_LOGIC_VECTOR ( 127 downto 0 );
    signal ch3_txpippmen_ext_0_1 : STD_LOGIC;
    signal ch3_txpippmstepsize_ext_0_1 : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal ch3_txpolarity_ext_0_1 : STD_LOGIC;
    signal ch3_txsequence_ext_0_1 : STD_LOGIC_VECTOR ( 6 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_cdrbmcdrreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_cdrfreqos : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_cdrincpctrl : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_cdrstepdir : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_cdrstepsq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_cdrstepsx : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_cfokovrdfinish : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_cfokovrdpulse : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_cfokovrdrdy0 : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_cfokovrdrdy1 : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_cfokovrdstart : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_eyescandataerror : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_eyescanreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_eyescantrigger : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_gtrxreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rx10gstat : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxbufstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxbyteisaligned : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxbyterealign : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxcdrhold : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxcdrlock : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxcdrovrden : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxcdrphdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxcdrreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxchanbondseq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxchanisaligned : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxchanrealign : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxchbondi : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxchbondo : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxclkcorcnt : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxcominitdet : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxcommadet : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxcomsasdet : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxcomwakedet : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxctrl3 : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxdapicodeovrden : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxdapicodereset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxdata : STD_LOGIC_VECTOR ( 127 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxdataextendrsvd : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxdatavalid : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxdccdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxdlyalignerr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxdlyalignprog : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxdlyalignreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxelecidle : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxeqtraining : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxfinealigndone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxgearboxslip : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxheader : STD_LOGIC_VECTOR ( 5 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxheadervalid : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxlpmen : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxmldchaindone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxmldchainreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxmlfinealignreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxmstdatapathreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxmstreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxmstresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxoobreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxosintdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxosintstarted : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxosintstrobedone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxosintstrobestarted : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxpcsresetmask : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxpd : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxphaligndone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxphalignerr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxphalignreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxphalignresetmask : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxphdlypd : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxphdlyreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxphdlyresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxphsetinitdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxphsetinitreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxphshift180 : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxphshift180done : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxpmaresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxpmaresetmask : STD_LOGIC_VECTOR ( 6 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxpolarity : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxprbscntreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxprbserr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxprbslocked : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxprbssel : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxprogdivreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxprogdivresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxrate : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxresetmode : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxslide : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxsliderdy : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxstartofseq : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal gt_bridge_ip_0_GT_RX0_ch_rxsyncallin : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxsyncdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxtermination : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxuserrdy : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX0_ch_rxvalid : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_cdrbmcdrreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_cdrfreqos : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_cdrincpctrl : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_cdrstepdir : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_cdrstepsq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_cdrstepsx : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_cfokovrdfinish : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_cfokovrdpulse : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_cfokovrdrdy0 : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_cfokovrdrdy1 : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_cfokovrdstart : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_eyescandataerror : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_eyescanreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_eyescantrigger : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_gtrxreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rx10gstat : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxbufstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxbyteisaligned : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxbyterealign : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxcdrhold : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxcdrlock : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxcdrovrden : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxcdrphdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxcdrreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxchanbondseq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxchanisaligned : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxchanrealign : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxchbondi : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxchbondo : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxclkcorcnt : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxcominitdet : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxcommadet : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxcomsasdet : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxcomwakedet : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxctrl3 : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxdapicodeovrden : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxdapicodereset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxdata : STD_LOGIC_VECTOR ( 127 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxdataextendrsvd : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxdatavalid : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxdccdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxdlyalignerr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxdlyalignprog : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxdlyalignreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxelecidle : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxeqtraining : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxfinealigndone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxgearboxslip : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxheader : STD_LOGIC_VECTOR ( 5 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxheadervalid : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxlpmen : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxmldchaindone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxmldchainreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxmlfinealignreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxmstdatapathreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxmstreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxmstresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxoobreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxosintdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxosintstarted : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxosintstrobedone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxosintstrobestarted : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxpcsresetmask : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxpd : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxphaligndone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxphalignerr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxphalignreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxphalignresetmask : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxphdlypd : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxphdlyreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxphdlyresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxphsetinitdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxphsetinitreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxphshift180 : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxphshift180done : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxpmaresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxpmaresetmask : STD_LOGIC_VECTOR ( 6 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxpolarity : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxprbscntreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxprbserr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxprbslocked : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxprbssel : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxprogdivreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxprogdivresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxrate : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxresetmode : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxslide : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxsliderdy : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxstartofseq : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal gt_bridge_ip_0_GT_RX1_ch_rxsyncallin : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxsyncdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxtermination : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxuserrdy : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX1_ch_rxvalid : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_cdrbmcdrreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_cdrfreqos : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_cdrincpctrl : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_cdrstepdir : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_cdrstepsq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_cdrstepsx : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_cfokovrdfinish : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_cfokovrdpulse : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_cfokovrdrdy0 : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_cfokovrdrdy1 : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_cfokovrdstart : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_eyescandataerror : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_eyescanreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_eyescantrigger : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_gtrxreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rx10gstat : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxbufstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxbyteisaligned : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxbyterealign : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxcdrhold : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxcdrlock : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxcdrovrden : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxcdrphdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxcdrreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxchanbondseq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxchanisaligned : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxchanrealign : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxchbondi : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxchbondo : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxclkcorcnt : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxcominitdet : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxcommadet : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxcomsasdet : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxcomwakedet : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxctrl3 : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxdapicodeovrden : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxdapicodereset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxdata : STD_LOGIC_VECTOR ( 127 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxdataextendrsvd : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxdatavalid : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxdccdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxdlyalignerr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxdlyalignprog : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxdlyalignreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxelecidle : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxeqtraining : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxfinealigndone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxgearboxslip : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxheader : STD_LOGIC_VECTOR ( 5 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxheadervalid : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxlpmen : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxmldchaindone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxmldchainreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxmlfinealignreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxmstdatapathreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxmstreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxmstresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxoobreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxosintdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxosintstarted : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxosintstrobedone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxosintstrobestarted : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxpcsresetmask : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxpd : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxphaligndone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxphalignerr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxphalignreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxphalignresetmask : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxphdlypd : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxphdlyreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxphdlyresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxphsetinitdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxphsetinitreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxphshift180 : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxphshift180done : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxpmaresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxpmaresetmask : STD_LOGIC_VECTOR ( 6 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxpolarity : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxprbscntreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxprbserr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxprbslocked : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxprbssel : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxprogdivreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxprogdivresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxrate : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxresetmode : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxslide : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxsliderdy : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxstartofseq : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal gt_bridge_ip_0_GT_RX2_ch_rxsyncallin : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxsyncdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxtermination : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxuserrdy : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX2_ch_rxvalid : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_cdrbmcdrreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_cdrfreqos : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_cdrincpctrl : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_cdrstepdir : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_cdrstepsq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_cdrstepsx : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_cfokovrdfinish : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_cfokovrdpulse : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_cfokovrdrdy0 : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_cfokovrdrdy1 : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_cfokovrdstart : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_eyescandataerror : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_eyescanreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_eyescantrigger : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_gtrxreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rx10gstat : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxbufstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxbyteisaligned : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxbyterealign : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxcdrhold : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxcdrlock : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxcdrovrden : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxcdrphdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxcdrreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxchanbondseq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxchanisaligned : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxchanrealign : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxchbondi : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxchbondo : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxclkcorcnt : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxcominitdet : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxcommadet : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxcomsasdet : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxcomwakedet : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxctrl3 : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxdapicodeovrden : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxdapicodereset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxdata : STD_LOGIC_VECTOR ( 127 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxdataextendrsvd : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxdatavalid : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxdccdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxdlyalignerr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxdlyalignprog : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxdlyalignreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxelecidle : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxeqtraining : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxfinealigndone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxgearboxslip : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxheader : STD_LOGIC_VECTOR ( 5 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxheadervalid : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxlpmen : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxmldchaindone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxmldchainreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxmlfinealignreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxmstdatapathreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxmstreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxmstresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxoobreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxosintdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxosintstarted : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxosintstrobedone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxosintstrobestarted : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxpcsresetmask : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxpd : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxphaligndone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxphalignerr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxphalignreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxphalignresetmask : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxphdlypd : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxphdlyreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxphdlyresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxphsetinitdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxphsetinitreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxphshift180 : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxphshift180done : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxpmaresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxpmaresetmask : STD_LOGIC_VECTOR ( 6 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxpolarity : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxprbscntreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxprbserr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxprbslocked : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxprbssel : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxprogdivreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxprogdivresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxrate : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxresetmode : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxslide : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxsliderdy : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxstartofseq : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal gt_bridge_ip_0_GT_RX3_ch_rxsyncallin : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxsyncdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxtermination : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxuserrdy : STD_LOGIC;
    signal gt_bridge_ip_0_GT_RX3_ch_rxvalid : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_gttxreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_tx10gstat : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txbufstatus : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_TX0_ch_txcomfinish : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txcominit : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txcomsas : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txcomwake : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_GT_TX0_ch_txctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_GT_TX0_ch_txctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_TX0_ch_txdapicodeovrden : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txdapicodereset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txdata : STD_LOGIC_VECTOR ( 127 downto 0 );
    signal gt_bridge_ip_0_GT_TX0_ch_txdataextendrsvd : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_TX0_ch_txdccdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txdeemph : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_TX0_ch_txdetectrx : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txdiffctrl : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_TX0_ch_txdlyalignerr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txdlyalignprog : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txdlyalignreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txelecidle : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txheader : STD_LOGIC_VECTOR ( 5 downto 0 );
    signal gt_bridge_ip_0_GT_TX0_ch_txinhibit : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txmaincursor : STD_LOGIC_VECTOR ( 6 downto 0 );
    signal gt_bridge_ip_0_GT_TX0_ch_txmargin : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal gt_bridge_ip_0_GT_TX0_ch_txmldchaindone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txmldchainreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txmstdatapathreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txmstreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txmstresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txoneszeros : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txpausedelayalign : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txpcsresetmask : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txpd : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_TX0_ch_txphaligndone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txphalignerr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txphalignoutrsvd : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txphalignreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txphalignresetmask : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_TX0_ch_txphdlypd : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txphdlyreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txphdlyresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txphsetinitdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txphsetinitreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txphshift180 : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txphshift180done : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txpicodeovrden : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txpicodereset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txpippmen : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txpippmstepsize : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_TX0_ch_txpisopd : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txpmaresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txpmaresetmask : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal gt_bridge_ip_0_GT_TX0_ch_txpolarity : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txpostcursor : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_TX0_ch_txprbsforceerr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txprbssel : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal gt_bridge_ip_0_GT_TX0_ch_txprecursor : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_TX0_ch_txprogdivreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txprogdivresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txrate : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_TX0_ch_txresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txresetmode : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_TX0_ch_txsequence : STD_LOGIC_VECTOR ( 6 downto 0 );
    signal gt_bridge_ip_0_GT_TX0_ch_txswing : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txsyncallin : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txsyncdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX0_ch_txuserrdy : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_gttxreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_tx10gstat : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txbufstatus : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_TX1_ch_txcomfinish : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txcominit : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txcomsas : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txcomwake : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_GT_TX1_ch_txctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_GT_TX1_ch_txctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_TX1_ch_txdapicodeovrden : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txdapicodereset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txdata : STD_LOGIC_VECTOR ( 127 downto 0 );
    signal gt_bridge_ip_0_GT_TX1_ch_txdataextendrsvd : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_TX1_ch_txdccdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txdeemph : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_TX1_ch_txdetectrx : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txdiffctrl : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_TX1_ch_txdlyalignerr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txdlyalignprog : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txdlyalignreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txelecidle : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txheader : STD_LOGIC_VECTOR ( 5 downto 0 );
    signal gt_bridge_ip_0_GT_TX1_ch_txinhibit : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txmaincursor : STD_LOGIC_VECTOR ( 6 downto 0 );
    signal gt_bridge_ip_0_GT_TX1_ch_txmargin : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal gt_bridge_ip_0_GT_TX1_ch_txmldchaindone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txmldchainreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txmstdatapathreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txmstreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txmstresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txoneszeros : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txpausedelayalign : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txpcsresetmask : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txpd : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_TX1_ch_txphaligndone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txphalignerr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txphalignoutrsvd : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txphalignreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txphalignresetmask : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_TX1_ch_txphdlypd : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txphdlyreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txphdlyresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txphsetinitdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txphsetinitreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txphshift180 : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txphshift180done : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txpicodeovrden : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txpicodereset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txpippmen : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txpippmstepsize : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_TX1_ch_txpisopd : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txpmaresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txpmaresetmask : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal gt_bridge_ip_0_GT_TX1_ch_txpolarity : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txpostcursor : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_TX1_ch_txprbsforceerr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txprbssel : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal gt_bridge_ip_0_GT_TX1_ch_txprecursor : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_TX1_ch_txprogdivreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txprogdivresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txrate : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_TX1_ch_txresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txresetmode : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_TX1_ch_txsequence : STD_LOGIC_VECTOR ( 6 downto 0 );
    signal gt_bridge_ip_0_GT_TX1_ch_txswing : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txsyncallin : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txsyncdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX1_ch_txuserrdy : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_gttxreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_tx10gstat : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txbufstatus : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_TX2_ch_txcomfinish : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txcominit : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txcomsas : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txcomwake : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_GT_TX2_ch_txctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_GT_TX2_ch_txctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_TX2_ch_txdapicodeovrden : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txdapicodereset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txdata : STD_LOGIC_VECTOR ( 127 downto 0 );
    signal gt_bridge_ip_0_GT_TX2_ch_txdataextendrsvd : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_TX2_ch_txdccdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txdeemph : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_TX2_ch_txdetectrx : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txdiffctrl : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_TX2_ch_txdlyalignerr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txdlyalignprog : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txdlyalignreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txelecidle : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txheader : STD_LOGIC_VECTOR ( 5 downto 0 );
    signal gt_bridge_ip_0_GT_TX2_ch_txinhibit : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txmaincursor : STD_LOGIC_VECTOR ( 6 downto 0 );
    signal gt_bridge_ip_0_GT_TX2_ch_txmargin : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal gt_bridge_ip_0_GT_TX2_ch_txmldchaindone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txmldchainreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txmstdatapathreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txmstreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txmstresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txoneszeros : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txpausedelayalign : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txpcsresetmask : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txpd : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_TX2_ch_txphaligndone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txphalignerr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txphalignoutrsvd : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txphalignreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txphalignresetmask : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_TX2_ch_txphdlypd : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txphdlyreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txphdlyresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txphsetinitdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txphsetinitreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txphshift180 : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txphshift180done : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txpicodeovrden : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txpicodereset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txpippmen : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txpippmstepsize : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_TX2_ch_txpisopd : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txpmaresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txpmaresetmask : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal gt_bridge_ip_0_GT_TX2_ch_txpolarity : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txpostcursor : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_TX2_ch_txprbsforceerr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txprbssel : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal gt_bridge_ip_0_GT_TX2_ch_txprecursor : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_TX2_ch_txprogdivreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txprogdivresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txrate : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_TX2_ch_txresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txresetmode : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_TX2_ch_txsequence : STD_LOGIC_VECTOR ( 6 downto 0 );
    signal gt_bridge_ip_0_GT_TX2_ch_txswing : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txsyncallin : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txsyncdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX2_ch_txuserrdy : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_gttxreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_tx10gstat : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txbufstatus : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_TX3_ch_txcomfinish : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txcominit : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txcomsas : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txcomwake : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_GT_TX3_ch_txctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_GT_TX3_ch_txctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_TX3_ch_txdapicodeovrden : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txdapicodereset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txdata : STD_LOGIC_VECTOR ( 127 downto 0 );
    signal gt_bridge_ip_0_GT_TX3_ch_txdataextendrsvd : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_TX3_ch_txdccdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txdeemph : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_TX3_ch_txdetectrx : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txdiffctrl : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_TX3_ch_txdlyalignerr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txdlyalignprog : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txdlyalignreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txelecidle : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txheader : STD_LOGIC_VECTOR ( 5 downto 0 );
    signal gt_bridge_ip_0_GT_TX3_ch_txinhibit : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txmaincursor : STD_LOGIC_VECTOR ( 6 downto 0 );
    signal gt_bridge_ip_0_GT_TX3_ch_txmargin : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal gt_bridge_ip_0_GT_TX3_ch_txmldchaindone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txmldchainreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txmstdatapathreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txmstreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txmstresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txoneszeros : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txpausedelayalign : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txpcsresetmask : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txpd : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_TX3_ch_txphaligndone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txphalignerr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txphalignoutrsvd : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txphalignreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txphalignresetmask : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_TX3_ch_txphdlypd : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txphdlyreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txphdlyresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txphsetinitdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txphsetinitreq : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txphshift180 : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txphshift180done : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txpicodeovrden : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txpicodereset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txpippmen : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txpippmstepsize : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_TX3_ch_txpisopd : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txpmaresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txpmaresetmask : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal gt_bridge_ip_0_GT_TX3_ch_txpolarity : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txpostcursor : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_TX3_ch_txprbsforceerr : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txprbssel : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal gt_bridge_ip_0_GT_TX3_ch_txprecursor : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal gt_bridge_ip_0_GT_TX3_ch_txprogdivreset : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txprogdivresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txrate : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_GT_TX3_ch_txresetdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txresetmode : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_GT_TX3_ch_txsequence : STD_LOGIC_VECTOR ( 6 downto 0 );
    signal gt_bridge_ip_0_GT_TX3_ch_txswing : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txsyncallin : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txsyncdone : STD_LOGIC;
    signal gt_bridge_ip_0_GT_TX3_ch_txuserrdy : STD_LOGIC;
    signal gt_bridge_ip_0_ch0_rxbyteisaligned_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch0_rxcdrlock_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch0_rxctrl0_ext : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_ch0_rxctrl1_ext : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_ch0_rxctrl2_ext : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_ch0_rxctrl3_ext : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_ch0_rxdata_ext : STD_LOGIC_VECTOR ( 127 downto 0 );
    signal gt_bridge_ip_0_ch0_rxdatavalid_ext : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_ch0_rxheader_ext : STD_LOGIC_VECTOR ( 5 downto 0 );
    signal gt_bridge_ip_0_ch0_rxheadervalid_ext : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_ch0_rxpmaresetdone_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch0_rxresetdone_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch0_rxvalid_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch0_txbufstatus_ext : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_ch0_txresetdone_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch1_rxbyteisaligned_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch1_rxcdrlock_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch1_rxctrl0_ext : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_ch1_rxctrl1_ext : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_ch1_rxctrl2_ext : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_ch1_rxctrl3_ext : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_ch1_rxdata_ext : STD_LOGIC_VECTOR ( 127 downto 0 );
    signal gt_bridge_ip_0_ch1_rxdatavalid_ext : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_ch1_rxheader_ext : STD_LOGIC_VECTOR ( 5 downto 0 );
    signal gt_bridge_ip_0_ch1_rxheadervalid_ext : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_ch1_rxpmaresetdone_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch1_rxresetdone_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch1_rxvalid_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch1_txbufstatus_ext : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_ch1_txresetdone_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch2_rxbyteisaligned_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch2_rxcdrlock_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch2_rxctrl0_ext : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_ch2_rxctrl1_ext : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_ch2_rxctrl2_ext : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_ch2_rxctrl3_ext : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_ch2_rxdata_ext : STD_LOGIC_VECTOR ( 127 downto 0 );
    signal gt_bridge_ip_0_ch2_rxdatavalid_ext : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_ch2_rxheader_ext : STD_LOGIC_VECTOR ( 5 downto 0 );
    signal gt_bridge_ip_0_ch2_rxheadervalid_ext : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_ch2_rxpmaresetdone_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch2_rxresetdone_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch2_rxvalid_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch2_txbufstatus_ext : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_ch2_txresetdone_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch3_rxbyteisaligned_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch3_rxcdrlock_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch3_rxctrl0_ext : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_ch3_rxctrl1_ext : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_bridge_ip_0_ch3_rxctrl2_ext : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_ch3_rxctrl3_ext : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal gt_bridge_ip_0_ch3_rxdata_ext : STD_LOGIC_VECTOR ( 127 downto 0 );
    signal gt_bridge_ip_0_ch3_rxdatavalid_ext : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_ch3_rxheader_ext : STD_LOGIC_VECTOR ( 5 downto 0 );
    signal gt_bridge_ip_0_ch3_rxheadervalid_ext : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_ch3_rxpmaresetdone_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch3_rxresetdone_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch3_rxvalid_ext : STD_LOGIC;
    signal gt_bridge_ip_0_ch3_txbufstatus_ext : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal gt_bridge_ip_0_ch3_txresetdone_ext : STD_LOGIC;
    signal gt_bridge_ip_0_diff_gt_ref_clock_1_CLK_N : STD_LOGIC_VECTOR ( 0 to 0 );
    signal gt_bridge_ip_0_diff_gt_ref_clock_1_CLK_P : STD_LOGIC_VECTOR ( 0 to 0 );
    signal gt_bridge_ip_0_gpi_out : STD_LOGIC;
    signal gt_bridge_ip_0_gt_ilo_reset : STD_LOGIC;
    signal gt_bridge_ip_0_gt_pll_reset : STD_LOGIC;
    signal gt_bridge_ip_0_lcpll_lock_out : STD_LOGIC;
    signal gt_bridge_ip_0_link_status_out : STD_LOGIC;
    signal gt_bridge_ip_0_rpll_lock_out : STD_LOGIC;
    signal gt_bridge_ip_0_rx_resetdone_out : STD_LOGIC;
    signal gt_bridge_ip_0_rxusrclk_out : STD_LOGIC;
    signal gt_bridge_ip_0_tx_resetdone_out : STD_LOGIC;
    signal gt_bridge_ip_0_txusrclk_out : STD_LOGIC;
    signal gt_quad_base_GT_Serial_GRX_N : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal gt_quad_base_GT_Serial_GRX_P : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal gt_quad_base_GT_Serial_GTX_N : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal gt_quad_base_GT_Serial_GTX_P : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal gt_quad_base_ch0_iloresetdone : STD_LOGIC;
    signal gt_quad_base_ch0_phystatus : STD_LOGIC;
    signal gt_quad_base_ch0_rxoutclk : STD_LOGIC;
    signal gt_quad_base_ch0_txoutclk : STD_LOGIC;
    signal gt_quad_base_ch1_iloresetdone : STD_LOGIC;
    signal gt_quad_base_ch1_phystatus : STD_LOGIC;
    signal gt_quad_base_ch2_iloresetdone : STD_LOGIC;
    signal gt_quad_base_ch2_phystatus : STD_LOGIC;
    signal gt_quad_base_ch3_iloresetdone : STD_LOGIC;
    signal gt_quad_base_ch3_phystatus : STD_LOGIC;
    signal gt_quad_base_gpo : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal gt_quad_base_gtpowergood : STD_LOGIC;
    signal gt_quad_base_hsclk0_lcplllock : STD_LOGIC;
    signal gt_quad_base_hsclk0_rplllock : STD_LOGIC;
    signal gt_reset_gt_bridge_ip_0_1 : STD_LOGIC;
    signal rate_sel_gt_bridge_ip_0_1 : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal reset_rx_datapath_in_0_1 : STD_LOGIC;
    signal reset_rx_pll_and_datapath_in_0_1 : STD_LOGIC;
    signal reset_tx_datapath_in_0_1 : STD_LOGIC;
    signal reset_tx_pll_and_datapath_in_0_1 : STD_LOGIC;
    signal url_gpo_Res : STD_LOGIC;
    signal urlp_Res : STD_LOGIC;
    signal util_ds_buf1_IBUF_OUT : STD_LOGIC_VECTOR ( 0 to 0 );
    signal util_ds_buf_IBUF_OUT : STD_LOGIC_VECTOR ( 0 to 0 );
    signal util_reduced_logic_0_Res : STD_LOGIC;
    signal xlc_gpi_dout : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal xlc_gpo_dout : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal xlconcat_0_dout : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal xlconcat_1_dout : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal xlcp_dout : STD_LOGIC_VECTOR ( 0 to 0 );
    signal xls_gpo_Dout : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal NLW_gt_bridge_ip_0_ch0_cfokovrdrdy0_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_cfokovrdrdy1_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_eyescandataerror_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxbyterealign_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxcdrphdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxchanbondseq_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxchanisaligned_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxchanrealign_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxcominitdet_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxcommadet_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxcomsasdet_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxcomwakedet_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxdccdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxdlyalignerr_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxdlyalignprog_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxelecidle_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxfinealigndone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxmstresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxosintdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxosintstarted_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxosintstrobedone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxosintstrobestarted_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxphaligndone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxphalignerr_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxphdlyresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxphsetinitdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxphshift180done_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxprbserr_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxprbslocked_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxprogdivresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxsliderdy_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rxsyncdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_tx10gstat_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_txcomfinish_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_txdccdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_txdlyalignerr_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_txdlyalignprog_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_txmstresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_txphaligndone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_txphalignerr_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_txphalignoutrsvd_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_txphdlyresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_txphsetinitdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_txphshift180done_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_txpmaresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_txprogdivresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_txsyncdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_cfokovrdrdy0_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_cfokovrdrdy1_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_eyescandataerror_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxbyterealign_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxcdrphdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxchanbondseq_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxchanisaligned_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxchanrealign_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxcominitdet_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxcommadet_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxcomsasdet_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxcomwakedet_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxdccdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxdlyalignerr_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxdlyalignprog_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxelecidle_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxfinealigndone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxmstresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxosintdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxosintstarted_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxosintstrobedone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxosintstrobestarted_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxphaligndone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxphalignerr_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxphdlyresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxphsetinitdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxphshift180done_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxprbserr_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxprbslocked_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxprogdivresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxsliderdy_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_rxsyncdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_tx10gstat_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_txcomfinish_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_txdccdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_txdlyalignerr_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_txdlyalignprog_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_txmstresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_txphaligndone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_txphalignerr_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_txphalignoutrsvd_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_txphdlyresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_txphsetinitdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_txphshift180done_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_txpmaresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_txprogdivresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch1_txsyncdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_cfokovrdrdy0_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_cfokovrdrdy1_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_eyescandataerror_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxbyterealign_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxcdrphdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxchanbondseq_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxchanisaligned_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxchanrealign_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxcominitdet_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxcommadet_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxcomsasdet_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxcomwakedet_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxdccdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxdlyalignerr_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxdlyalignprog_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxelecidle_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxfinealigndone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxmstresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxosintdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxosintstarted_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxosintstrobedone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxosintstrobestarted_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxphaligndone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxphalignerr_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxphdlyresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxphsetinitdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxphshift180done_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxprbserr_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxprbslocked_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxprogdivresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxsliderdy_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_rxsyncdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_tx10gstat_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_txcomfinish_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_txdccdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_txdlyalignerr_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_txdlyalignprog_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_txmstresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_txphaligndone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_txphalignerr_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_txphalignoutrsvd_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_txphdlyresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_txphsetinitdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_txphshift180done_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_txpmaresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_txprogdivresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch2_txsyncdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_cfokovrdrdy0_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_cfokovrdrdy1_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_eyescandataerror_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxbyterealign_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxcdrphdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxchanbondseq_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxchanisaligned_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxchanrealign_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxcominitdet_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxcommadet_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxcomsasdet_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxcomwakedet_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxdccdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxdlyalignerr_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxdlyalignprog_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxelecidle_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxfinealigndone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxmstresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxosintdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxosintstarted_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxosintstrobedone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxosintstrobestarted_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxphaligndone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxphalignerr_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxphdlyresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxphsetinitdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxphshift180done_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxprbserr_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxprbslocked_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxprogdivresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxsliderdy_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_rxsyncdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_tx10gstat_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_txcomfinish_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_txdccdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_txdlyalignerr_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_txdlyalignprog_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_txmstresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_txphaligndone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_txphalignerr_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_txphalignoutrsvd_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_txphdlyresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_txphsetinitdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_txphshift180done_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_txpmaresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_txprogdivresetdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch3_txsyncdone_ext_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_pcie_rstb_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_rx_clr_out_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_rx_clrb_leaf_out_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_tx_clr_out_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_tx_clrb_leaf_out_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_bridge_ip_0_ch0_rx10gstat_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal NLW_gt_bridge_ip_0_ch0_rxbufstatus_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal NLW_gt_bridge_ip_0_ch0_rxchbondo_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal NLW_gt_bridge_ip_0_ch0_rxclkcorcnt_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal NLW_gt_bridge_ip_0_ch0_rxdataextendrsvd_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal NLW_gt_bridge_ip_0_ch0_rxstartofseq_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal NLW_gt_bridge_ip_0_ch0_rxstatus_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal NLW_gt_bridge_ip_0_ch1_rx10gstat_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal NLW_gt_bridge_ip_0_ch1_rxbufstatus_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal NLW_gt_bridge_ip_0_ch1_rxchbondo_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal NLW_gt_bridge_ip_0_ch1_rxclkcorcnt_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal NLW_gt_bridge_ip_0_ch1_rxdataextendrsvd_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal NLW_gt_bridge_ip_0_ch1_rxstartofseq_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal NLW_gt_bridge_ip_0_ch1_rxstatus_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal NLW_gt_bridge_ip_0_ch2_rx10gstat_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal NLW_gt_bridge_ip_0_ch2_rxbufstatus_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal NLW_gt_bridge_ip_0_ch2_rxchbondo_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal NLW_gt_bridge_ip_0_ch2_rxclkcorcnt_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal NLW_gt_bridge_ip_0_ch2_rxdataextendrsvd_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal NLW_gt_bridge_ip_0_ch2_rxstartofseq_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal NLW_gt_bridge_ip_0_ch2_rxstatus_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal NLW_gt_bridge_ip_0_ch3_rx10gstat_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal NLW_gt_bridge_ip_0_ch3_rxbufstatus_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal NLW_gt_bridge_ip_0_ch3_rxchbondo_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
    signal NLW_gt_bridge_ip_0_ch3_rxclkcorcnt_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal NLW_gt_bridge_ip_0_ch3_rxdataextendrsvd_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal NLW_gt_bridge_ip_0_ch3_rxstartofseq_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal NLW_gt_bridge_ip_0_ch3_rxstatus_ext_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal NLW_gt_bridge_ip_0_reset_mask_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal NLW_gt_quad_base_ch0_bufgtce_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch0_bufgtrst_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch0_dmonitoroutclk_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch0_phyready_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch0_resetexception_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch1_bufgtce_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch1_bufgtrst_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch1_dmonitoroutclk_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch1_phyready_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch1_resetexception_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch1_rxoutclk_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch1_txoutclk_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch2_bufgtce_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch2_bufgtrst_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch2_dmonitoroutclk_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch2_phyready_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch2_resetexception_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch2_rxoutclk_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch2_txoutclk_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch3_bufgtce_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch3_bufgtrst_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch3_dmonitoroutclk_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch3_phyready_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch3_resetexception_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch3_rxoutclk_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch3_txoutclk_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_correcterr_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_debugtracetvalid_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_hsclk0_lcpllfbclklost_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_hsclk0_lcpllrefclklost_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_hsclk0_lcpllrefclkmonitor_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_hsclk0_rpllfbclklost_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_hsclk0_rpllrefclklost_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_hsclk0_rpllrefclkmonitor_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_hsclk0_rxrecclkout0_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_hsclk0_rxrecclkout1_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_hsclk1_lcpllfbclklost_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_hsclk1_lcplllock_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_hsclk1_lcpllrefclklost_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_hsclk1_lcpllrefclkmonitor_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_hsclk1_rpllfbclklost_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_hsclk1_rplllock_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_hsclk1_rpllrefclklost_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_hsclk1_rpllrefclkmonitor_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_hsclk1_rxrecclkout0_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_hsclk1_rxrecclkout1_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_refclk0_clktestsigint_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_refclk0_gtrefclkpdint_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_refclk1_clktestsigint_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_refclk1_gtrefclkpdint_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_rxmarginreqack_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_rxmarginresreq_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_trigackin0_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_trigout0_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ubinterrupt_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ubtxuart_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_uncorrecterr_UNCONNECTED : STD_LOGIC;
    signal NLW_gt_quad_base_ch0_bufgtcemask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal NLW_gt_quad_base_ch0_bufgtdiv_UNCONNECTED : STD_LOGIC_VECTOR ( 11 downto 0 );
    signal NLW_gt_quad_base_ch0_bufgtrstmask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal NLW_gt_quad_base_ch0_dmonitorout_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
    signal NLW_gt_quad_base_ch0_pcsrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal NLW_gt_quad_base_ch0_pinrsvdas_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal NLW_gt_quad_base_ch1_bufgtcemask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal NLW_gt_quad_base_ch1_bufgtdiv_UNCONNECTED : STD_LOGIC_VECTOR ( 11 downto 0 );
    signal NLW_gt_quad_base_ch1_bufgtrstmask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal NLW_gt_quad_base_ch1_dmonitorout_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
    signal NLW_gt_quad_base_ch1_pcsrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal NLW_gt_quad_base_ch1_pinrsvdas_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal NLW_gt_quad_base_ch2_bufgtcemask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal NLW_gt_quad_base_ch2_bufgtdiv_UNCONNECTED : STD_LOGIC_VECTOR ( 11 downto 0 );
    signal NLW_gt_quad_base_ch2_bufgtrstmask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal NLW_gt_quad_base_ch2_dmonitorout_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
    signal NLW_gt_quad_base_ch2_pcsrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal NLW_gt_quad_base_ch2_pinrsvdas_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal NLW_gt_quad_base_ch3_bufgtcemask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal NLW_gt_quad_base_ch3_bufgtdiv_UNCONNECTED : STD_LOGIC_VECTOR ( 11 downto 0 );
    signal NLW_gt_quad_base_ch3_bufgtrstmask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal NLW_gt_quad_base_ch3_dmonitorout_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
    signal NLW_gt_quad_base_ch3_pcsrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal NLW_gt_quad_base_ch3_pinrsvdas_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal NLW_gt_quad_base_ctrlrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
    signal NLW_gt_quad_base_debugtracetdata_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal NLW_gt_quad_base_hsclk0_lcpllrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal NLW_gt_quad_base_hsclk0_rpllrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal NLW_gt_quad_base_hsclk0_rxrecclksel_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal NLW_gt_quad_base_hsclk1_lcpllrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal NLW_gt_quad_base_hsclk1_rpllrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal NLW_gt_quad_base_hsclk1_rxrecclksel_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal NLW_gt_quad_base_pipenorthout_UNCONNECTED : STD_LOGIC_VECTOR ( 5 downto 0 );
    signal NLW_gt_quad_base_pipesouthout_UNCONNECTED : STD_LOGIC_VECTOR ( 5 downto 0 );
    signal NLW_gt_quad_base_resetdone_northout_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal NLW_gt_quad_base_resetdone_southout_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal NLW_gt_quad_base_rxmarginrescmd_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal NLW_gt_quad_base_rxmarginreslanenum_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal NLW_gt_quad_base_rxmarginrespayld_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal NLW_gt_quad_base_rxpinorthout_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal NLW_gt_quad_base_rxpisouthout_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal NLW_gt_quad_base_txpinorthout_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal NLW_gt_quad_base_txpisouthout_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal NLW_util_ds_buf_IBUF_DS_ODIV2_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
    signal NLW_util_ds_buf1_IBUF_DS_ODIV2_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
    attribute X_INTERFACE_INFO : string;
    attribute X_INTERFACE_INFO of APB3_INTF_0_penable : signal is "xilinx.com:interface:apb:1.0 APB3_INTF_0 PENABLE";
    attribute X_INTERFACE_INFO of APB3_INTF_0_pready : signal is "xilinx.com:interface:apb:1.0 APB3_INTF_0 PREADY";
    attribute X_INTERFACE_INFO of APB3_INTF_0_psel : signal is "xilinx.com:interface:apb:1.0 APB3_INTF_0 PSEL";
    attribute X_INTERFACE_INFO of APB3_INTF_0_pslverr : signal is "xilinx.com:interface:apb:1.0 APB3_INTF_0 PSLVERR";
    attribute X_INTERFACE_INFO of APB3_INTF_0_pwrite : signal is "xilinx.com:interface:apb:1.0 APB3_INTF_0 PWRITE";
    attribute X_INTERFACE_INFO of APB3_INTF_0_paddr : signal is "xilinx.com:interface:apb:1.0 APB3_INTF_0 PADDR";
    attribute X_INTERFACE_INFO of APB3_INTF_0_prdata : signal is "xilinx.com:interface:apb:1.0 APB3_INTF_0 PRDATA";
    attribute X_INTERFACE_INFO of APB3_INTF_0_pwdata : signal is "xilinx.com:interface:apb:1.0 APB3_INTF_0 PWDATA";
    attribute X_INTERFACE_INFO of GT_REFCLK0_clk_n : signal is "xilinx.com:interface:diff_clock:1.0 GT_REFCLK0 CLK_N";
    attribute X_INTERFACE_PARAMETER : string;
    attribute X_INTERFACE_PARAMETER of GT_REFCLK0_clk_n : signal is "XIL_INTERFACENAME GT_REFCLK0, CAN_DEBUG false, FREQ_HZ 240474000";
    attribute X_INTERFACE_INFO of GT_REFCLK0_clk_p : signal is "xilinx.com:interface:diff_clock:1.0 GT_REFCLK0 CLK_P";
    attribute X_INTERFACE_INFO of GT_REFCLK1_clk_n : signal is "xilinx.com:interface:diff_clock:1.0 GT_REFCLK1 CLK_N";
    attribute X_INTERFACE_PARAMETER of GT_REFCLK1_clk_n : signal is "XIL_INTERFACENAME GT_REFCLK1, CAN_DEBUG false, FREQ_HZ 240474000";
    attribute X_INTERFACE_INFO of GT_REFCLK1_clk_p : signal is "xilinx.com:interface:diff_clock:1.0 GT_REFCLK1 CLK_P";
    attribute X_INTERFACE_INFO of GT_Serial_grx_n : signal is "xilinx.com:interface:gt:1.0 GT_Serial GRX_N";
    attribute X_INTERFACE_PARAMETER of GT_Serial_grx_n : signal is "XIL_INTERFACENAME GT_Serial, CAN_DEBUG false";
    attribute X_INTERFACE_INFO of GT_Serial_grx_p : signal is "xilinx.com:interface:gt:1.0 GT_Serial GRX_P";
    attribute X_INTERFACE_INFO of GT_Serial_gtx_n : signal is "xilinx.com:interface:gt:1.0 GT_Serial GTX_N";
    attribute X_INTERFACE_INFO of GT_Serial_gtx_p : signal is "xilinx.com:interface:gt:1.0 GT_Serial GTX_P";
begin
    APB3_INTF_0_1_PADDR(15 downto 0) <= APB3_INTF_0_paddr(15 downto 0);
    APB3_INTF_0_1_PENABLE <= APB3_INTF_0_penable;
    APB3_INTF_0_1_PSEL <= APB3_INTF_0_psel;
    APB3_INTF_0_1_PWDATA(31 downto 0) <= APB3_INTF_0_pwdata(31 downto 0);
    APB3_INTF_0_1_PWRITE <= APB3_INTF_0_pwrite;
    APB3_INTF_0_prdata(31 downto 0) <= APB3_INTF_0_1_PRDATA(31 downto 0);
    APB3_INTF_0_pready <= APB3_INTF_0_1_PREADY;
    APB3_INTF_0_pslverr <= APB3_INTF_0_1_PSLVERR;
    CLK_IN_D_0_1_CLK_N(0) <= GT_REFCLK1_clk_n(0);
    CLK_IN_D_0_1_CLK_P(0) <= GT_REFCLK1_clk_p(0);
    GT_Serial_gtx_n(3 downto 0) <= gt_quad_base_GT_Serial_GTX_N(3 downto 0);
    GT_Serial_gtx_p(3 downto 0) <= gt_quad_base_GT_Serial_GTX_P(3 downto 0);
    apb3clk_gt_bridge_ip_0_1 <= apb3clk_gt_bridge_ip_0;
    apb3clk_quad_1 <= apb3clk_quad;
    ch0_loopback_0_1(2 downto 0) <= ch0_loopback_0(2 downto 0);
    ch0_rxbyteisaligned_ext_0 <= gt_bridge_ip_0_ch0_rxbyteisaligned_ext;
    ch0_rxcdrlock_ext_0 <= gt_bridge_ip_0_ch0_rxcdrlock_ext;
    ch0_rxctrl0_ext_0(15 downto 0) <= gt_bridge_ip_0_ch0_rxctrl0_ext(15 downto 0);
    ch0_rxctrl1_ext_0(15 downto 0) <= gt_bridge_ip_0_ch0_rxctrl1_ext(15 downto 0);
    ch0_rxctrl2_ext_0(7 downto 0) <= gt_bridge_ip_0_ch0_rxctrl2_ext(7 downto 0);
    ch0_rxctrl3_ext_0(7 downto 0) <= gt_bridge_ip_0_ch0_rxctrl3_ext(7 downto 0);
    ch0_rxdata_ext_0(127 downto 0) <= gt_bridge_ip_0_ch0_rxdata_ext(127 downto 0);
    ch0_rxdatavalid_ext_0(1 downto 0) <= gt_bridge_ip_0_ch0_rxdatavalid_ext(1 downto 0);
    ch0_rxgearboxslip_ext_0_1 <= ch0_rxgearboxslip_ext_0;
    ch0_rxheader_ext_0(5 downto 0) <= gt_bridge_ip_0_ch0_rxheader_ext(5 downto 0);
    ch0_rxheadervalid_ext_0(1 downto 0) <= gt_bridge_ip_0_ch0_rxheadervalid_ext(1 downto 0);
    ch0_rxpmaresetdone_ext_0 <= gt_bridge_ip_0_ch0_rxpmaresetdone_ext;
    ch0_rxpolarity_ext_0_1 <= ch0_rxpolarity_ext_0;
    ch0_rxresetdone_ext_0 <= gt_bridge_ip_0_ch0_rxresetdone_ext;
    ch0_rxslide_ext_0_1 <= ch0_rxslide_ext_0;
    ch0_rxvalid_ext_0 <= gt_bridge_ip_0_ch0_rxvalid_ext;
    ch0_txbufstatus_ext_0(1 downto 0) <= gt_bridge_ip_0_ch0_txbufstatus_ext(1 downto 0);
    ch0_txctrl0_ext_0_1(15 downto 0) <= ch0_txctrl0_ext_0(15 downto 0);
    ch0_txctrl1_ext_0_1(15 downto 0) <= ch0_txctrl1_ext_0(15 downto 0);
    ch0_txctrl2_ext_0_1(7 downto 0) <= ch0_txctrl2_ext_0(7 downto 0);
    ch0_txdata_ext_0_1(127 downto 0) <= ch0_txdata_ext_0(127 downto 0);
    ch0_txheader_ext_0_1(5 downto 0) <= ch0_txheader_ext_0(5 downto 0);
    ch0_txpippmen_ext_0_1 <= ch0_txpippmen_ext_0;
    ch0_txpippmstepsize_ext_0_1(4 downto 0) <= ch0_txpippmstepsize_ext_0(4 downto 0);
    ch0_txpolarity_ext_0_1 <= ch0_txpolarity_ext_0;
    ch0_txresetdone_ext_0 <= gt_bridge_ip_0_ch0_txresetdone_ext;
    ch0_txsequence_ext_0_1(6 downto 0) <= ch0_txsequence_ext_0(6 downto 0);
    ch1_loopback_0_1(2 downto 0) <= ch1_loopback_0(2 downto 0);
    ch1_rxbyteisaligned_ext_0 <= gt_bridge_ip_0_ch1_rxbyteisaligned_ext;
    ch1_rxcdrlock_ext_0 <= gt_bridge_ip_0_ch1_rxcdrlock_ext;
    ch1_rxctrl0_ext_0(15 downto 0) <= gt_bridge_ip_0_ch1_rxctrl0_ext(15 downto 0);
    ch1_rxctrl1_ext_0(15 downto 0) <= gt_bridge_ip_0_ch1_rxctrl1_ext(15 downto 0);
    ch1_rxctrl2_ext_0(7 downto 0) <= gt_bridge_ip_0_ch1_rxctrl2_ext(7 downto 0);
    ch1_rxctrl3_ext_0(7 downto 0) <= gt_bridge_ip_0_ch1_rxctrl3_ext(7 downto 0);
    ch1_rxdata_ext_0(127 downto 0) <= gt_bridge_ip_0_ch1_rxdata_ext(127 downto 0);
    ch1_rxdatavalid_ext_0(1 downto 0) <= gt_bridge_ip_0_ch1_rxdatavalid_ext(1 downto 0);
    ch1_rxgearboxslip_ext_0_1 <= ch1_rxgearboxslip_ext_0;
    ch1_rxheader_ext_0(5 downto 0) <= gt_bridge_ip_0_ch1_rxheader_ext(5 downto 0);
    ch1_rxheadervalid_ext_0(1 downto 0) <= gt_bridge_ip_0_ch1_rxheadervalid_ext(1 downto 0);
    ch1_rxpmaresetdone_ext_0 <= gt_bridge_ip_0_ch1_rxpmaresetdone_ext;
    ch1_rxpolarity_ext_0_1 <= ch1_rxpolarity_ext_0;
    ch1_rxresetdone_ext_0 <= gt_bridge_ip_0_ch1_rxresetdone_ext;
    ch1_rxslide_ext_0_1 <= ch1_rxslide_ext_0;
    ch1_rxvalid_ext_0 <= gt_bridge_ip_0_ch1_rxvalid_ext;
    ch1_txbufstatus_ext_0(1 downto 0) <= gt_bridge_ip_0_ch1_txbufstatus_ext(1 downto 0);
    ch1_txctrl0_ext_0_1(15 downto 0) <= ch1_txctrl0_ext_0(15 downto 0);
    ch1_txctrl1_ext_0_1(15 downto 0) <= ch1_txctrl1_ext_0(15 downto 0);
    ch1_txctrl2_ext_0_1(7 downto 0) <= ch1_txctrl2_ext_0(7 downto 0);
    ch1_txdata_ext_0_1(127 downto 0) <= ch1_txdata_ext_0(127 downto 0);
    ch1_txpippmen_ext_0_1 <= ch1_txpippmen_ext_0;
    ch1_txpippmstepsize_ext_0_1(4 downto 0) <= ch1_txpippmstepsize_ext_0(4 downto 0);
    ch1_txpolarity_ext_0_1 <= ch1_txpolarity_ext_0;
    ch1_txresetdone_ext_0 <= gt_bridge_ip_0_ch1_txresetdone_ext;
    ch1_txsequence_ext_0_1(6 downto 0) <= ch1_txsequence_ext_0(6 downto 0);
    ch2_loopback_0_1(2 downto 0) <= ch2_loopback_0(2 downto 0);
    ch2_rxbyteisaligned_ext_0 <= gt_bridge_ip_0_ch2_rxbyteisaligned_ext;
    ch2_rxcdrlock_ext_0 <= gt_bridge_ip_0_ch2_rxcdrlock_ext;
    ch2_rxctrl0_ext_0(15 downto 0) <= gt_bridge_ip_0_ch2_rxctrl0_ext(15 downto 0);
    ch2_rxctrl1_ext_0(15 downto 0) <= gt_bridge_ip_0_ch2_rxctrl1_ext(15 downto 0);
    ch2_rxctrl2_ext_0(7 downto 0) <= gt_bridge_ip_0_ch2_rxctrl2_ext(7 downto 0);
    ch2_rxctrl3_ext_0(7 downto 0) <= gt_bridge_ip_0_ch2_rxctrl3_ext(7 downto 0);
    ch2_rxdata_ext_0(127 downto 0) <= gt_bridge_ip_0_ch2_rxdata_ext(127 downto 0);
    ch2_rxdatavalid_ext_0(1 downto 0) <= gt_bridge_ip_0_ch2_rxdatavalid_ext(1 downto 0);
    ch2_rxgearboxslip_ext_0_1 <= ch2_rxgearboxslip_ext_0;
    ch2_rxheader_ext_0(5 downto 0) <= gt_bridge_ip_0_ch2_rxheader_ext(5 downto 0);
    ch2_rxheadervalid_ext_0(1 downto 0) <= gt_bridge_ip_0_ch2_rxheadervalid_ext(1 downto 0);
    ch2_rxpmaresetdone_ext_0 <= gt_bridge_ip_0_ch2_rxpmaresetdone_ext;
    ch2_rxpolarity_ext_0_1 <= ch2_rxpolarity_ext_0;
    ch2_rxresetdone_ext_0 <= gt_bridge_ip_0_ch2_rxresetdone_ext;
    ch2_rxslide_ext_0_1 <= ch2_rxslide_ext_0;
    ch2_rxvalid_ext_0 <= gt_bridge_ip_0_ch2_rxvalid_ext;
    ch2_txbufstatus_ext_0(1 downto 0) <= gt_bridge_ip_0_ch2_txbufstatus_ext(1 downto 0);
    ch2_txctrl0_ext_0_1(15 downto 0) <= ch2_txctrl0_ext_0(15 downto 0);
    ch2_txctrl1_ext_0_1(15 downto 0) <= ch2_txctrl1_ext_0(15 downto 0);
    ch2_txctrl2_ext_0_1(7 downto 0) <= ch2_txctrl2_ext_0(7 downto 0);
    ch2_txdata_ext_0_1(127 downto 0) <= ch2_txdata_ext_0(127 downto 0);
    ch2_txpippmen_ext_0_1 <= ch2_txpippmen_ext_0;
    ch2_txpippmstepsize_ext_0_1(4 downto 0) <= ch2_txpippmstepsize_ext_0(4 downto 0);
    ch2_txpolarity_ext_0_1 <= ch2_txpolarity_ext_0;
    ch2_txresetdone_ext_0 <= gt_bridge_ip_0_ch2_txresetdone_ext;
    ch2_txsequence_ext_0_1(6 downto 0) <= ch2_txsequence_ext_0(6 downto 0);
    ch3_loopback_0_1(2 downto 0) <= ch3_loopback_0(2 downto 0);
    ch3_rxbyteisaligned_ext_0 <= gt_bridge_ip_0_ch3_rxbyteisaligned_ext;
    ch3_rxcdrlock_ext_0 <= gt_bridge_ip_0_ch3_rxcdrlock_ext;
    ch3_rxctrl0_ext_0(15 downto 0) <= gt_bridge_ip_0_ch3_rxctrl0_ext(15 downto 0);
    ch3_rxctrl1_ext_0(15 downto 0) <= gt_bridge_ip_0_ch3_rxctrl1_ext(15 downto 0);
    ch3_rxctrl2_ext_0(7 downto 0) <= gt_bridge_ip_0_ch3_rxctrl2_ext(7 downto 0);
    ch3_rxctrl3_ext_0(7 downto 0) <= gt_bridge_ip_0_ch3_rxctrl3_ext(7 downto 0);
    ch3_rxdata_ext_0(127 downto 0) <= gt_bridge_ip_0_ch3_rxdata_ext(127 downto 0);
    ch3_rxdatavalid_ext_0(1 downto 0) <= gt_bridge_ip_0_ch3_rxdatavalid_ext(1 downto 0);
    ch3_rxgearboxslip_ext_0_1 <= ch3_rxgearboxslip_ext_0;
    ch3_rxheader_ext_0(5 downto 0) <= gt_bridge_ip_0_ch3_rxheader_ext(5 downto 0);
    ch3_rxheadervalid_ext_0(1 downto 0) <= gt_bridge_ip_0_ch3_rxheadervalid_ext(1 downto 0);
    ch3_rxpmaresetdone_ext_0 <= gt_bridge_ip_0_ch3_rxpmaresetdone_ext;
    ch3_rxpolarity_ext_0_1 <= ch3_rxpolarity_ext_0;
    ch3_rxresetdone_ext_0 <= gt_bridge_ip_0_ch3_rxresetdone_ext;
    ch3_rxslide_ext_0_1 <= ch3_rxslide_ext_0;
    ch3_rxvalid_ext_0 <= gt_bridge_ip_0_ch3_rxvalid_ext;
    ch3_txbufstatus_ext_0(1 downto 0) <= gt_bridge_ip_0_ch3_txbufstatus_ext(1 downto 0);
    ch3_txctrl0_ext_0_1(15 downto 0) <= ch3_txctrl0_ext_0(15 downto 0);
    ch3_txctrl1_ext_0_1(15 downto 0) <= ch3_txctrl1_ext_0(15 downto 0);
    ch3_txctrl2_ext_0_1(7 downto 0) <= ch3_txctrl2_ext_0(7 downto 0);
    ch3_txdata_ext_0_1(127 downto 0) <= ch3_txdata_ext_0(127 downto 0);
    ch3_txpippmen_ext_0_1 <= ch3_txpippmen_ext_0;
    ch3_txpippmstepsize_ext_0_1(4 downto 0) <= ch3_txpippmstepsize_ext_0(4 downto 0);
    ch3_txpolarity_ext_0_1 <= ch3_txpolarity_ext_0;
    ch3_txresetdone_ext_0 <= gt_bridge_ip_0_ch3_txresetdone_ext;
    ch3_txsequence_ext_0_1(6 downto 0) <= ch3_txsequence_ext_0(6 downto 0);
    gt_bridge_ip_0_diff_gt_ref_clock_1_CLK_N(0) <= GT_REFCLK0_clk_n(0);
    gt_bridge_ip_0_diff_gt_ref_clock_1_CLK_P(0) <= GT_REFCLK0_clk_p(0);
    gt_quad_base_GT_Serial_GRX_N(3 downto 0) <= GT_Serial_grx_n(3 downto 0);
    gt_quad_base_GT_Serial_GRX_P(3 downto 0) <= GT_Serial_grx_p(3 downto 0);
    gt_reset_gt_bridge_ip_0_1 <= gt_reset_gt_bridge_ip_0;
    lcpll_lock_gt_bridge_ip_0 <= gt_bridge_ip_0_lcpll_lock_out;
    link_status_gt_bridge_ip_0 <= gt_bridge_ip_0_link_status_out;
    rate_sel_gt_bridge_ip_0_1(3 downto 0) <= rate_sel_gt_bridge_ip_0(3 downto 0);
    reset_rx_datapath_in_0_1 <= reset_rx_datapath_in_0;
    reset_rx_pll_and_datapath_in_0_1 <= reset_rx_pll_and_datapath_in_0;
    reset_tx_datapath_in_0_1 <= reset_tx_datapath_in_0;
    reset_tx_pll_and_datapath_in_0_1 <= reset_tx_pll_and_datapath_in_0;
    rpll_lock_gt_bridge_ip_0 <= gt_bridge_ip_0_rpll_lock_out;
    rx_resetdone_out_gt_bridge_ip_0 <= gt_bridge_ip_0_rx_resetdone_out;
    rxusrclk_gt_bridge_ip_0 <= gt_bridge_ip_0_rxusrclk_out;
    tx_resetdone_out_gt_bridge_ip_0 <= gt_bridge_ip_0_tx_resetdone_out;
    txusrclk_gt_bridge_ip_0 <= gt_bridge_ip_0_txusrclk_out;
    bufg_gt: component transceiver_versal_lpgbt_bufg_gt_0
        port map (
            gt_bufgtce => '1',
            gt_bufgtcemask => '0',
            gt_bufgtclr => '0',
            gt_bufgtclrmask => '0',
            gt_bufgtdiv(2 downto 0) => B"000",
            outclk => gt_quad_base_ch0_rxoutclk,
            usrclk => bufg_gt_usrclk
        );
    bufg_gt_1: component transceiver_versal_lpgbt_bufg_gt_1_0
        port map (
            gt_bufgtce => '1',
            gt_bufgtcemask => '0',
            gt_bufgtclr => '0',
            gt_bufgtclrmask => '0',
            gt_bufgtdiv(2 downto 0) => B"000",
            outclk => gt_quad_base_ch0_txoutclk,
            usrclk => bufg_gt_1_usrclk
        );
    gt_bridge_ip_0: component transceiver_versal_lpgbt_gt_bridge_ip_0_0
        port map (
            apb3clk => apb3clk_gt_bridge_ip_0_1,
            ch0_cdrbmcdrreq => gt_bridge_ip_0_GT_RX0_ch_cdrbmcdrreq,
            ch0_cdrbmcdrreq_ext => '0',
            ch0_cdrfreqos => gt_bridge_ip_0_GT_RX0_ch_cdrfreqos,
            ch0_cdrfreqos_ext => '0',
            ch0_cdrincpctrl => gt_bridge_ip_0_GT_RX0_ch_cdrincpctrl,
            ch0_cdrincpctrl_ext => '0',
            ch0_cdrstepdir => gt_bridge_ip_0_GT_RX0_ch_cdrstepdir,
            ch0_cdrstepdir_ext => '0',
            ch0_cdrstepsq => gt_bridge_ip_0_GT_RX0_ch_cdrstepsq,
            ch0_cdrstepsq_ext => '0',
            ch0_cdrstepsx => gt_bridge_ip_0_GT_RX0_ch_cdrstepsx,
            ch0_cdrstepsx_ext => '0',
            ch0_cfokovrdfinish => gt_bridge_ip_0_GT_RX0_ch_cfokovrdfinish,
            ch0_cfokovrdfinish_ext => '0',
            ch0_cfokovrdpulse => gt_bridge_ip_0_GT_RX0_ch_cfokovrdpulse,
            ch0_cfokovrdpulse_ext => '0',
            ch0_cfokovrdrdy0 => gt_bridge_ip_0_GT_RX0_ch_cfokovrdrdy0,
            ch0_cfokovrdrdy0_ext => NLW_gt_bridge_ip_0_ch0_cfokovrdrdy0_ext_UNCONNECTED,
            ch0_cfokovrdrdy1 => gt_bridge_ip_0_GT_RX0_ch_cfokovrdrdy1,
            ch0_cfokovrdrdy1_ext => NLW_gt_bridge_ip_0_ch0_cfokovrdrdy1_ext_UNCONNECTED,
            ch0_cfokovrdstart => gt_bridge_ip_0_GT_RX0_ch_cfokovrdstart,
            ch0_cfokovrdstart_ext => '0',
            ch0_eyescandataerror => gt_bridge_ip_0_GT_RX0_ch_eyescandataerror,
            ch0_eyescandataerror_ext => NLW_gt_bridge_ip_0_ch0_eyescandataerror_ext_UNCONNECTED,
            ch0_eyescanreset => gt_bridge_ip_0_GT_RX0_ch_eyescanreset,
            ch0_eyescanreset_ext => '0',
            ch0_eyescantrigger => gt_bridge_ip_0_GT_RX0_ch_eyescantrigger,
            ch0_eyescantrigger_ext => '0',
            ch0_gtrxreset => gt_bridge_ip_0_GT_RX0_ch_gtrxreset,
            ch0_gttxreset => gt_bridge_ip_0_GT_TX0_ch_gttxreset,
            ch0_rx10gstat(7 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rx10gstat(7 downto 0),
            ch0_rx10gstat_ext(7 downto 0) => NLW_gt_bridge_ip_0_ch0_rx10gstat_ext_UNCONNECTED(7 downto 0),
            ch0_rxbufstatus(2 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxbufstatus(2 downto 0),
            ch0_rxbufstatus_ext(2 downto 0) => NLW_gt_bridge_ip_0_ch0_rxbufstatus_ext_UNCONNECTED(2 downto 0),
            ch0_rxbyteisaligned => gt_bridge_ip_0_GT_RX0_ch_rxbyteisaligned,
            ch0_rxbyteisaligned_ext => gt_bridge_ip_0_ch0_rxbyteisaligned_ext,
            ch0_rxbyterealign => gt_bridge_ip_0_GT_RX0_ch_rxbyterealign,
            ch0_rxbyterealign_ext => NLW_gt_bridge_ip_0_ch0_rxbyterealign_ext_UNCONNECTED,
            ch0_rxcdrhold => gt_bridge_ip_0_GT_RX0_ch_rxcdrhold,
            ch0_rxcdrhold_ext => '0',
            ch0_rxcdrlock => gt_bridge_ip_0_GT_RX0_ch_rxcdrlock,
            ch0_rxcdrlock_ext => gt_bridge_ip_0_ch0_rxcdrlock_ext,
            ch0_rxcdrovrden => gt_bridge_ip_0_GT_RX0_ch_rxcdrovrden,
            ch0_rxcdrovrden_ext => '0',
            ch0_rxcdrphdone => gt_bridge_ip_0_GT_RX0_ch_rxcdrphdone,
            ch0_rxcdrphdone_ext => NLW_gt_bridge_ip_0_ch0_rxcdrphdone_ext_UNCONNECTED,
            ch0_rxcdrreset => gt_bridge_ip_0_GT_RX0_ch_rxcdrreset,
            ch0_rxcdrreset_ext => '0',
            ch0_rxchanbondseq => gt_bridge_ip_0_GT_RX0_ch_rxchanbondseq,
            ch0_rxchanbondseq_ext => NLW_gt_bridge_ip_0_ch0_rxchanbondseq_ext_UNCONNECTED,
            ch0_rxchanisaligned => gt_bridge_ip_0_GT_RX0_ch_rxchanisaligned,
            ch0_rxchanisaligned_ext => NLW_gt_bridge_ip_0_ch0_rxchanisaligned_ext_UNCONNECTED,
            ch0_rxchanrealign => gt_bridge_ip_0_GT_RX0_ch_rxchanrealign,
            ch0_rxchanrealign_ext => NLW_gt_bridge_ip_0_ch0_rxchanrealign_ext_UNCONNECTED,
            ch0_rxchbondi(4 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxchbondi(4 downto 0),
            ch0_rxchbondi_ext(4 downto 0) => B"00000",
            ch0_rxchbondo(4 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxchbondo(4 downto 0),
            ch0_rxchbondo_ext(4 downto 0) => NLW_gt_bridge_ip_0_ch0_rxchbondo_ext_UNCONNECTED(4 downto 0),
            ch0_rxclkcorcnt(1 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxclkcorcnt(1 downto 0),
            ch0_rxclkcorcnt_ext(1 downto 0) => NLW_gt_bridge_ip_0_ch0_rxclkcorcnt_ext_UNCONNECTED(1 downto 0),
            ch0_rxcominitdet => gt_bridge_ip_0_GT_RX0_ch_rxcominitdet,
            ch0_rxcominitdet_ext => NLW_gt_bridge_ip_0_ch0_rxcominitdet_ext_UNCONNECTED,
            ch0_rxcommadet => gt_bridge_ip_0_GT_RX0_ch_rxcommadet,
            ch0_rxcommadet_ext => NLW_gt_bridge_ip_0_ch0_rxcommadet_ext_UNCONNECTED,
            ch0_rxcomsasdet => gt_bridge_ip_0_GT_RX0_ch_rxcomsasdet,
            ch0_rxcomsasdet_ext => NLW_gt_bridge_ip_0_ch0_rxcomsasdet_ext_UNCONNECTED,
            ch0_rxcomwakedet => gt_bridge_ip_0_GT_RX0_ch_rxcomwakedet,
            ch0_rxcomwakedet_ext => NLW_gt_bridge_ip_0_ch0_rxcomwakedet_ext_UNCONNECTED,
            ch0_rxctrl0(15 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxctrl0(15 downto 0),
            ch0_rxctrl0_ext(15 downto 0) => gt_bridge_ip_0_ch0_rxctrl0_ext(15 downto 0),
            ch0_rxctrl1(15 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxctrl1(15 downto 0),
            ch0_rxctrl1_ext(15 downto 0) => gt_bridge_ip_0_ch0_rxctrl1_ext(15 downto 0),
            ch0_rxctrl2(7 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxctrl2(7 downto 0),
            ch0_rxctrl2_ext(7 downto 0) => gt_bridge_ip_0_ch0_rxctrl2_ext(7 downto 0),
            ch0_rxctrl3(7 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxctrl3(7 downto 0),
            ch0_rxctrl3_ext(7 downto 0) => gt_bridge_ip_0_ch0_rxctrl3_ext(7 downto 0),
            ch0_rxdapicodeovrden => gt_bridge_ip_0_GT_RX0_ch_rxdapicodeovrden,
            ch0_rxdapicodeovrden_ext => '0',
            ch0_rxdapicodereset => gt_bridge_ip_0_GT_RX0_ch_rxdapicodereset,
            ch0_rxdapicodereset_ext => '0',
            ch0_rxdata(127 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxdata(127 downto 0),
            ch0_rxdata_ext(127 downto 0) => gt_bridge_ip_0_ch0_rxdata_ext(127 downto 0),
            ch0_rxdataextendrsvd(7 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxdataextendrsvd(7 downto 0),
            ch0_rxdataextendrsvd_ext(7 downto 0) => NLW_gt_bridge_ip_0_ch0_rxdataextendrsvd_ext_UNCONNECTED(7 downto 0),
            ch0_rxdatavalid(1 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxdatavalid(1 downto 0),
            ch0_rxdatavalid_ext(1 downto 0) => gt_bridge_ip_0_ch0_rxdatavalid_ext(1 downto 0),
            ch0_rxdccdone => gt_bridge_ip_0_GT_RX0_ch_rxdccdone,
            ch0_rxdccdone_ext => NLW_gt_bridge_ip_0_ch0_rxdccdone_ext_UNCONNECTED,
            ch0_rxdlyalignerr => gt_bridge_ip_0_GT_RX0_ch_rxdlyalignerr,
            ch0_rxdlyalignerr_ext => NLW_gt_bridge_ip_0_ch0_rxdlyalignerr_ext_UNCONNECTED,
            ch0_rxdlyalignprog => gt_bridge_ip_0_GT_RX0_ch_rxdlyalignprog,
            ch0_rxdlyalignprog_ext => NLW_gt_bridge_ip_0_ch0_rxdlyalignprog_ext_UNCONNECTED,
            ch0_rxdlyalignreq => gt_bridge_ip_0_GT_RX0_ch_rxdlyalignreq,
            ch0_rxdlyalignreq_ext => '0',
            ch0_rxelecidle => gt_bridge_ip_0_GT_RX0_ch_rxelecidle,
            ch0_rxelecidle_ext => NLW_gt_bridge_ip_0_ch0_rxelecidle_ext_UNCONNECTED,
            ch0_rxeqtraining => gt_bridge_ip_0_GT_RX0_ch_rxeqtraining,
            ch0_rxeqtraining_ext => '0',
            ch0_rxfinealigndone => gt_bridge_ip_0_GT_RX0_ch_rxfinealigndone,
            ch0_rxfinealigndone_ext => NLW_gt_bridge_ip_0_ch0_rxfinealigndone_ext_UNCONNECTED,
            ch0_rxgearboxslip => gt_bridge_ip_0_GT_RX0_ch_rxgearboxslip,
            ch0_rxgearboxslip_ext => ch0_rxgearboxslip_ext_0_1,
            ch0_rxheader(5 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxheader(5 downto 0),
            ch0_rxheader_ext(5 downto 0) => gt_bridge_ip_0_ch0_rxheader_ext(5 downto 0),
            ch0_rxheadervalid(1 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxheadervalid(1 downto 0),
            ch0_rxheadervalid_ext(1 downto 0) => gt_bridge_ip_0_ch0_rxheadervalid_ext(1 downto 0),
            ch0_rxlpmen => gt_bridge_ip_0_GT_RX0_ch_rxlpmen,
            ch0_rxlpmen_ext => '0',
            ch0_rxmldchaindone => gt_bridge_ip_0_GT_RX0_ch_rxmldchaindone,
            ch0_rxmldchaindone_ext => '0',
            ch0_rxmldchainreq => gt_bridge_ip_0_GT_RX0_ch_rxmldchainreq,
            ch0_rxmldchainreq_ext => '0',
            ch0_rxmlfinealignreq => gt_bridge_ip_0_GT_RX0_ch_rxmlfinealignreq,
            ch0_rxmlfinealignreq_ext => '0',
            ch0_rxmstdatapathreset => gt_bridge_ip_0_GT_RX0_ch_rxmstdatapathreset,
            ch0_rxmstreset => gt_bridge_ip_0_GT_RX0_ch_rxmstreset,
            ch0_rxmstresetdone => gt_bridge_ip_0_GT_RX0_ch_rxmstresetdone,
            ch0_rxmstresetdone_ext => NLW_gt_bridge_ip_0_ch0_rxmstresetdone_ext_UNCONNECTED,
            ch0_rxoobreset => gt_bridge_ip_0_GT_RX0_ch_rxoobreset,
            ch0_rxoobreset_ext => '0',
            ch0_rxosintdone => gt_bridge_ip_0_GT_RX0_ch_rxosintdone,
            ch0_rxosintdone_ext => NLW_gt_bridge_ip_0_ch0_rxosintdone_ext_UNCONNECTED,
            ch0_rxosintstarted => gt_bridge_ip_0_GT_RX0_ch_rxosintstarted,
            ch0_rxosintstarted_ext => NLW_gt_bridge_ip_0_ch0_rxosintstarted_ext_UNCONNECTED,
            ch0_rxosintstrobedone => gt_bridge_ip_0_GT_RX0_ch_rxosintstrobedone,
            ch0_rxosintstrobedone_ext => NLW_gt_bridge_ip_0_ch0_rxosintstrobedone_ext_UNCONNECTED,
            ch0_rxosintstrobestarted => gt_bridge_ip_0_GT_RX0_ch_rxosintstrobestarted,
            ch0_rxosintstrobestarted_ext => NLW_gt_bridge_ip_0_ch0_rxosintstrobestarted_ext_UNCONNECTED,
            ch0_rxpcsresetmask(4 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxpcsresetmask(4 downto 0),
            ch0_rxpcsresetmask_ext(4 downto 0) => B"11111",
            ch0_rxpd(1 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxpd(1 downto 0),
            ch0_rxpd_ext(1 downto 0) => B"00",
            ch0_rxphaligndone => gt_bridge_ip_0_GT_RX0_ch_rxphaligndone,
            ch0_rxphaligndone_ext => NLW_gt_bridge_ip_0_ch0_rxphaligndone_ext_UNCONNECTED,
            ch0_rxphalignerr => gt_bridge_ip_0_GT_RX0_ch_rxphalignerr,
            ch0_rxphalignerr_ext => NLW_gt_bridge_ip_0_ch0_rxphalignerr_ext_UNCONNECTED,
            ch0_rxphalignreq => gt_bridge_ip_0_GT_RX0_ch_rxphalignreq,
            ch0_rxphalignreq_ext => '0',
            ch0_rxphalignresetmask(1 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxphalignresetmask(1 downto 0),
            ch0_rxphalignresetmask_ext(1 downto 0) => B"11",
            ch0_rxphdlypd => gt_bridge_ip_0_GT_RX0_ch_rxphdlypd,
            ch0_rxphdlypd_ext => '0',
            ch0_rxphdlyreset => gt_bridge_ip_0_GT_RX0_ch_rxphdlyreset,
            ch0_rxphdlyreset_ext => '0',
            ch0_rxphdlyresetdone => gt_bridge_ip_0_GT_RX0_ch_rxphdlyresetdone,
            ch0_rxphdlyresetdone_ext => NLW_gt_bridge_ip_0_ch0_rxphdlyresetdone_ext_UNCONNECTED,
            ch0_rxphsetinitdone => gt_bridge_ip_0_GT_RX0_ch_rxphsetinitdone,
            ch0_rxphsetinitdone_ext => NLW_gt_bridge_ip_0_ch0_rxphsetinitdone_ext_UNCONNECTED,
            ch0_rxphsetinitreq => gt_bridge_ip_0_GT_RX0_ch_rxphsetinitreq,
            ch0_rxphsetinitreq_ext => '0',
            ch0_rxphshift180 => gt_bridge_ip_0_GT_RX0_ch_rxphshift180,
            ch0_rxphshift180_ext => '0',
            ch0_rxphshift180done => gt_bridge_ip_0_GT_RX0_ch_rxphshift180done,
            ch0_rxphshift180done_ext => NLW_gt_bridge_ip_0_ch0_rxphshift180done_ext_UNCONNECTED,
            ch0_rxpmaresetdone => gt_bridge_ip_0_GT_RX0_ch_rxpmaresetdone,
            ch0_rxpmaresetdone_ext => gt_bridge_ip_0_ch0_rxpmaresetdone_ext,
            ch0_rxpmaresetmask(6 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxpmaresetmask(6 downto 0),
            ch0_rxpmaresetmask_ext(6 downto 0) => B"1111111",
            ch0_rxpolarity => gt_bridge_ip_0_GT_RX0_ch_rxpolarity,
            ch0_rxpolarity_ext => ch0_rxpolarity_ext_0_1,
            ch0_rxprbscntreset => gt_bridge_ip_0_GT_RX0_ch_rxprbscntreset,
            ch0_rxprbscntreset_ext => '0',
            ch0_rxprbserr => gt_bridge_ip_0_GT_RX0_ch_rxprbserr,
            ch0_rxprbserr_ext => NLW_gt_bridge_ip_0_ch0_rxprbserr_ext_UNCONNECTED,
            ch0_rxprbslocked => gt_bridge_ip_0_GT_RX0_ch_rxprbslocked,
            ch0_rxprbslocked_ext => NLW_gt_bridge_ip_0_ch0_rxprbslocked_ext_UNCONNECTED,
            ch0_rxprbssel(3 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxprbssel(3 downto 0),
            ch0_rxprbssel_ext(3 downto 0) => B"0000",
            ch0_rxprogdivreset => gt_bridge_ip_0_GT_RX0_ch_rxprogdivreset,
            ch0_rxprogdivresetdone => gt_bridge_ip_0_GT_RX0_ch_rxprogdivresetdone,
            ch0_rxprogdivresetdone_ext => NLW_gt_bridge_ip_0_ch0_rxprogdivresetdone_ext_UNCONNECTED,
            ch0_rxrate(7 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxrate(7 downto 0),
            ch0_rxresetdone => gt_bridge_ip_0_GT_RX0_ch_rxresetdone,
            ch0_rxresetdone_ext => gt_bridge_ip_0_ch0_rxresetdone_ext,
            ch0_rxresetmode(1 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxresetmode(1 downto 0),
            ch0_rxresetmode_ext(1 downto 0) => B"00",
            ch0_rxslide => gt_bridge_ip_0_GT_RX0_ch_rxslide,
            ch0_rxslide_ext => ch0_rxslide_ext_0_1,
            ch0_rxsliderdy => gt_bridge_ip_0_GT_RX0_ch_rxsliderdy,
            ch0_rxsliderdy_ext => NLW_gt_bridge_ip_0_ch0_rxsliderdy_ext_UNCONNECTED,
            ch0_rxstartofseq(1 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxstartofseq(1 downto 0),
            ch0_rxstartofseq_ext(1 downto 0) => NLW_gt_bridge_ip_0_ch0_rxstartofseq_ext_UNCONNECTED(1 downto 0),
            ch0_rxstatus(2 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxstatus(2 downto 0),
            ch0_rxstatus_ext(2 downto 0) => NLW_gt_bridge_ip_0_ch0_rxstatus_ext_UNCONNECTED(2 downto 0),
            ch0_rxsyncallin => gt_bridge_ip_0_GT_RX0_ch_rxsyncallin,
            ch0_rxsyncallin_ext => '0',
            ch0_rxsyncdone => gt_bridge_ip_0_GT_RX0_ch_rxsyncdone,
            ch0_rxsyncdone_ext => NLW_gt_bridge_ip_0_ch0_rxsyncdone_ext_UNCONNECTED,
            ch0_rxtermination => gt_bridge_ip_0_GT_RX0_ch_rxtermination,
            ch0_rxtermination_ext => '0',
            ch0_rxuserrdy => gt_bridge_ip_0_GT_RX0_ch_rxuserrdy,
            ch0_rxvalid => gt_bridge_ip_0_GT_RX0_ch_rxvalid,
            ch0_rxvalid_ext => gt_bridge_ip_0_ch0_rxvalid_ext,
            ch0_tx10gstat => gt_bridge_ip_0_GT_TX0_ch_tx10gstat,
            ch0_tx10gstat_ext => NLW_gt_bridge_ip_0_ch0_tx10gstat_ext_UNCONNECTED,
            ch0_txbufstatus(1 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txbufstatus(1 downto 0),
            ch0_txbufstatus_ext(1 downto 0) => gt_bridge_ip_0_ch0_txbufstatus_ext(1 downto 0),
            ch0_txcomfinish => gt_bridge_ip_0_GT_TX0_ch_txcomfinish,
            ch0_txcomfinish_ext => NLW_gt_bridge_ip_0_ch0_txcomfinish_ext_UNCONNECTED,
            ch0_txcominit => gt_bridge_ip_0_GT_TX0_ch_txcominit,
            ch0_txcominit_ext => '0',
            ch0_txcomsas => gt_bridge_ip_0_GT_TX0_ch_txcomsas,
            ch0_txcomsas_ext => '0',
            ch0_txcomwake => gt_bridge_ip_0_GT_TX0_ch_txcomwake,
            ch0_txcomwake_ext => '0',
            ch0_txctrl0(15 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txctrl0(15 downto 0),
            ch0_txctrl0_ext(15 downto 0) => ch0_txctrl0_ext_0_1(15 downto 0),
            ch0_txctrl1(15 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txctrl1(15 downto 0),
            ch0_txctrl1_ext(15 downto 0) => ch0_txctrl1_ext_0_1(15 downto 0),
            ch0_txctrl2(7 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txctrl2(7 downto 0),
            ch0_txctrl2_ext(7 downto 0) => ch0_txctrl2_ext_0_1(7 downto 0),
            ch0_txdapicodeovrden => gt_bridge_ip_0_GT_TX0_ch_txdapicodeovrden,
            ch0_txdapicodeovrden_ext => '0',
            ch0_txdapicodereset => gt_bridge_ip_0_GT_TX0_ch_txdapicodereset,
            ch0_txdapicodereset_ext => '0',
            ch0_txdata(127 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txdata(127 downto 0),
            ch0_txdata_ext(127 downto 0) => ch0_txdata_ext_0_1(127 downto 0),
            ch0_txdataextendrsvd(7 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txdataextendrsvd(7 downto 0),
            ch0_txdataextendrsvd_ext(7 downto 0) => B"00000000",
            ch0_txdccdone => gt_bridge_ip_0_GT_TX0_ch_txdccdone,
            ch0_txdccdone_ext => NLW_gt_bridge_ip_0_ch0_txdccdone_ext_UNCONNECTED,
            ch0_txdeemph(1 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txdeemph(1 downto 0),
            ch0_txdeemph_ext(1 downto 0) => B"00",
            ch0_txdetectrx => gt_bridge_ip_0_GT_TX0_ch_txdetectrx,
            ch0_txdetectrx_ext => '0',
            ch0_txdiffctrl(4 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txdiffctrl(4 downto 0),
            ch0_txdiffctrl_ext(4 downto 0) => B"11001",
            ch0_txdlyalignerr => gt_bridge_ip_0_GT_TX0_ch_txdlyalignerr,
            ch0_txdlyalignerr_ext => NLW_gt_bridge_ip_0_ch0_txdlyalignerr_ext_UNCONNECTED,
            ch0_txdlyalignprog => gt_bridge_ip_0_GT_TX0_ch_txdlyalignprog,
            ch0_txdlyalignprog_ext => NLW_gt_bridge_ip_0_ch0_txdlyalignprog_ext_UNCONNECTED,
            ch0_txdlyalignreq => gt_bridge_ip_0_GT_TX0_ch_txdlyalignreq,
            ch0_txdlyalignreq_ext => '0',
            ch0_txelecidle => gt_bridge_ip_0_GT_TX0_ch_txelecidle,
            ch0_txelecidle_ext => '0',
            ch0_txheader(5 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txheader(5 downto 0),
            ch0_txheader_ext(5 downto 0) => ch0_txheader_ext_0_1(5 downto 0),
            ch0_txinhibit => gt_bridge_ip_0_GT_TX0_ch_txinhibit,
            ch0_txinhibit_ext => '0',
            ch0_txmaincursor(6 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txmaincursor(6 downto 0),
            ch0_txmaincursor_ext(6 downto 0) => B"0000000",
            ch0_txmargin(2 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txmargin(2 downto 0),
            ch0_txmargin_ext(2 downto 0) => B"000",
            ch0_txmldchaindone => gt_bridge_ip_0_GT_TX0_ch_txmldchaindone,
            ch0_txmldchaindone_ext => '0',
            ch0_txmldchainreq => gt_bridge_ip_0_GT_TX0_ch_txmldchainreq,
            ch0_txmldchainreq_ext => '0',
            ch0_txmstdatapathreset => gt_bridge_ip_0_GT_TX0_ch_txmstdatapathreset,
            ch0_txmstreset => gt_bridge_ip_0_GT_TX0_ch_txmstreset,
            ch0_txmstresetdone => gt_bridge_ip_0_GT_TX0_ch_txmstresetdone,
            ch0_txmstresetdone_ext => NLW_gt_bridge_ip_0_ch0_txmstresetdone_ext_UNCONNECTED,
            ch0_txoneszeros => gt_bridge_ip_0_GT_TX0_ch_txoneszeros,
            ch0_txoneszeros_ext => '0',
            ch0_txpausedelayalign => gt_bridge_ip_0_GT_TX0_ch_txpausedelayalign,
            ch0_txpausedelayalign_ext => '0',
            ch0_txpcsresetmask => gt_bridge_ip_0_GT_TX0_ch_txpcsresetmask,
            ch0_txpcsresetmask_ext => '1',
            ch0_txpd(1 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txpd(1 downto 0),
            ch0_txpd_ext(1 downto 0) => B"00",
            ch0_txphaligndone => gt_bridge_ip_0_GT_TX0_ch_txphaligndone,
            ch0_txphaligndone_ext => NLW_gt_bridge_ip_0_ch0_txphaligndone_ext_UNCONNECTED,
            ch0_txphalignerr => gt_bridge_ip_0_GT_TX0_ch_txphalignerr,
            ch0_txphalignerr_ext => NLW_gt_bridge_ip_0_ch0_txphalignerr_ext_UNCONNECTED,
            ch0_txphalignoutrsvd => gt_bridge_ip_0_GT_TX0_ch_txphalignoutrsvd,
            ch0_txphalignoutrsvd_ext => NLW_gt_bridge_ip_0_ch0_txphalignoutrsvd_ext_UNCONNECTED,
            ch0_txphalignreq => gt_bridge_ip_0_GT_TX0_ch_txphalignreq,
            ch0_txphalignreq_ext => '0',
            ch0_txphalignresetmask(1 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txphalignresetmask(1 downto 0),
            ch0_txphalignresetmask_ext(1 downto 0) => B"11",
            ch0_txphdlypd => gt_bridge_ip_0_GT_TX0_ch_txphdlypd,
            ch0_txphdlypd_ext => '0',
            ch0_txphdlyreset => gt_bridge_ip_0_GT_TX0_ch_txphdlyreset,
            ch0_txphdlyreset_ext => '0',
            ch0_txphdlyresetdone => gt_bridge_ip_0_GT_TX0_ch_txphdlyresetdone,
            ch0_txphdlyresetdone_ext => NLW_gt_bridge_ip_0_ch0_txphdlyresetdone_ext_UNCONNECTED,
            ch0_txphsetinitdone => gt_bridge_ip_0_GT_TX0_ch_txphsetinitdone,
            ch0_txphsetinitdone_ext => NLW_gt_bridge_ip_0_ch0_txphsetinitdone_ext_UNCONNECTED,
            ch0_txphsetinitreq => gt_bridge_ip_0_GT_TX0_ch_txphsetinitreq,
            ch0_txphsetinitreq_ext => '0',
            ch0_txphshift180 => gt_bridge_ip_0_GT_TX0_ch_txphshift180,
            ch0_txphshift180_ext => '0',
            ch0_txphshift180done => gt_bridge_ip_0_GT_TX0_ch_txphshift180done,
            ch0_txphshift180done_ext => NLW_gt_bridge_ip_0_ch0_txphshift180done_ext_UNCONNECTED,
            ch0_txpicodeovrden => gt_bridge_ip_0_GT_TX0_ch_txpicodeovrden,
            ch0_txpicodeovrden_ext => '0',
            ch0_txpicodereset => gt_bridge_ip_0_GT_TX0_ch_txpicodereset,
            ch0_txpicodereset_ext => '0',
            ch0_txpippmen => gt_bridge_ip_0_GT_TX0_ch_txpippmen,
            ch0_txpippmen_ext => ch0_txpippmen_ext_0_1,
            ch0_txpippmstepsize(4 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txpippmstepsize(4 downto 0),
            ch0_txpippmstepsize_ext(4 downto 0) => ch0_txpippmstepsize_ext_0_1(4 downto 0),
            ch0_txpisopd => gt_bridge_ip_0_GT_TX0_ch_txpisopd,
            ch0_txpisopd_ext => '0',
            ch0_txpmaresetdone => gt_bridge_ip_0_GT_TX0_ch_txpmaresetdone,
            ch0_txpmaresetdone_ext => NLW_gt_bridge_ip_0_ch0_txpmaresetdone_ext_UNCONNECTED,
            ch0_txpmaresetmask(2 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txpmaresetmask(2 downto 0),
            ch0_txpmaresetmask_ext(2 downto 0) => B"111",
            ch0_txpolarity => gt_bridge_ip_0_GT_TX0_ch_txpolarity,
            ch0_txpolarity_ext => ch0_txpolarity_ext_0_1,
            ch0_txpostcursor(4 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txpostcursor(4 downto 0),
            ch0_txpostcursor_ext(4 downto 0) => B"00000",
            ch0_txprbsforceerr => gt_bridge_ip_0_GT_TX0_ch_txprbsforceerr,
            ch0_txprbsforceerr_ext => '0',
            ch0_txprbssel(3 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txprbssel(3 downto 0),
            ch0_txprbssel_ext(3 downto 0) => B"0000",
            ch0_txprecursor(4 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txprecursor(4 downto 0),
            ch0_txprecursor_ext(4 downto 0) => B"00000",
            ch0_txprogdivreset => gt_bridge_ip_0_GT_TX0_ch_txprogdivreset,
            ch0_txprogdivresetdone => gt_bridge_ip_0_GT_TX0_ch_txprogdivresetdone,
            ch0_txprogdivresetdone_ext => NLW_gt_bridge_ip_0_ch0_txprogdivresetdone_ext_UNCONNECTED,
            ch0_txrate(7 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txrate(7 downto 0),
            ch0_txresetdone => gt_bridge_ip_0_GT_TX0_ch_txresetdone,
            ch0_txresetdone_ext => gt_bridge_ip_0_ch0_txresetdone_ext,
            ch0_txresetmode(1 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txresetmode(1 downto 0),
            ch0_txresetmode_ext(1 downto 0) => B"00",
            ch0_txsequence(6 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txsequence(6 downto 0),
            ch0_txsequence_ext(6 downto 0) => ch0_txsequence_ext_0_1(6 downto 0),
            ch0_txswing => gt_bridge_ip_0_GT_TX0_ch_txswing,
            ch0_txswing_ext => '0',
            ch0_txsyncallin => gt_bridge_ip_0_GT_TX0_ch_txsyncallin,
            ch0_txsyncallin_ext => '0',
            ch0_txsyncdone => gt_bridge_ip_0_GT_TX0_ch_txsyncdone,
            ch0_txsyncdone_ext => NLW_gt_bridge_ip_0_ch0_txsyncdone_ext_UNCONNECTED,
            ch0_txuserrdy => gt_bridge_ip_0_GT_TX0_ch_txuserrdy,
            ch1_cdrbmcdrreq => gt_bridge_ip_0_GT_RX1_ch_cdrbmcdrreq,
            ch1_cdrbmcdrreq_ext => '0',
            ch1_cdrfreqos => gt_bridge_ip_0_GT_RX1_ch_cdrfreqos,
            ch1_cdrfreqos_ext => '0',
            ch1_cdrincpctrl => gt_bridge_ip_0_GT_RX1_ch_cdrincpctrl,
            ch1_cdrincpctrl_ext => '0',
            ch1_cdrstepdir => gt_bridge_ip_0_GT_RX1_ch_cdrstepdir,
            ch1_cdrstepdir_ext => '0',
            ch1_cdrstepsq => gt_bridge_ip_0_GT_RX1_ch_cdrstepsq,
            ch1_cdrstepsq_ext => '0',
            ch1_cdrstepsx => gt_bridge_ip_0_GT_RX1_ch_cdrstepsx,
            ch1_cdrstepsx_ext => '0',
            ch1_cfokovrdfinish => gt_bridge_ip_0_GT_RX1_ch_cfokovrdfinish,
            ch1_cfokovrdfinish_ext => '0',
            ch1_cfokovrdpulse => gt_bridge_ip_0_GT_RX1_ch_cfokovrdpulse,
            ch1_cfokovrdpulse_ext => '0',
            ch1_cfokovrdrdy0 => gt_bridge_ip_0_GT_RX1_ch_cfokovrdrdy0,
            ch1_cfokovrdrdy0_ext => NLW_gt_bridge_ip_0_ch1_cfokovrdrdy0_ext_UNCONNECTED,
            ch1_cfokovrdrdy1 => gt_bridge_ip_0_GT_RX1_ch_cfokovrdrdy1,
            ch1_cfokovrdrdy1_ext => NLW_gt_bridge_ip_0_ch1_cfokovrdrdy1_ext_UNCONNECTED,
            ch1_cfokovrdstart => gt_bridge_ip_0_GT_RX1_ch_cfokovrdstart,
            ch1_cfokovrdstart_ext => '0',
            ch1_eyescandataerror => gt_bridge_ip_0_GT_RX1_ch_eyescandataerror,
            ch1_eyescandataerror_ext => NLW_gt_bridge_ip_0_ch1_eyescandataerror_ext_UNCONNECTED,
            ch1_eyescanreset => gt_bridge_ip_0_GT_RX1_ch_eyescanreset,
            ch1_eyescanreset_ext => '0',
            ch1_eyescantrigger => gt_bridge_ip_0_GT_RX1_ch_eyescantrigger,
            ch1_eyescantrigger_ext => '0',
            ch1_gtrxreset => gt_bridge_ip_0_GT_RX1_ch_gtrxreset,
            ch1_gttxreset => gt_bridge_ip_0_GT_TX1_ch_gttxreset,
            ch1_rx10gstat(7 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rx10gstat(7 downto 0),
            ch1_rx10gstat_ext(7 downto 0) => NLW_gt_bridge_ip_0_ch1_rx10gstat_ext_UNCONNECTED(7 downto 0),
            ch1_rxbufstatus(2 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxbufstatus(2 downto 0),
            ch1_rxbufstatus_ext(2 downto 0) => NLW_gt_bridge_ip_0_ch1_rxbufstatus_ext_UNCONNECTED(2 downto 0),
            ch1_rxbyteisaligned => gt_bridge_ip_0_GT_RX1_ch_rxbyteisaligned,
            ch1_rxbyteisaligned_ext => gt_bridge_ip_0_ch1_rxbyteisaligned_ext,
            ch1_rxbyterealign => gt_bridge_ip_0_GT_RX1_ch_rxbyterealign,
            ch1_rxbyterealign_ext => NLW_gt_bridge_ip_0_ch1_rxbyterealign_ext_UNCONNECTED,
            ch1_rxcdrhold => gt_bridge_ip_0_GT_RX1_ch_rxcdrhold,
            ch1_rxcdrhold_ext => '0',
            ch1_rxcdrlock => gt_bridge_ip_0_GT_RX1_ch_rxcdrlock,
            ch1_rxcdrlock_ext => gt_bridge_ip_0_ch1_rxcdrlock_ext,
            ch1_rxcdrovrden => gt_bridge_ip_0_GT_RX1_ch_rxcdrovrden,
            ch1_rxcdrovrden_ext => '0',
            ch1_rxcdrphdone => gt_bridge_ip_0_GT_RX1_ch_rxcdrphdone,
            ch1_rxcdrphdone_ext => NLW_gt_bridge_ip_0_ch1_rxcdrphdone_ext_UNCONNECTED,
            ch1_rxcdrreset => gt_bridge_ip_0_GT_RX1_ch_rxcdrreset,
            ch1_rxcdrreset_ext => '0',
            ch1_rxchanbondseq => gt_bridge_ip_0_GT_RX1_ch_rxchanbondseq,
            ch1_rxchanbondseq_ext => NLW_gt_bridge_ip_0_ch1_rxchanbondseq_ext_UNCONNECTED,
            ch1_rxchanisaligned => gt_bridge_ip_0_GT_RX1_ch_rxchanisaligned,
            ch1_rxchanisaligned_ext => NLW_gt_bridge_ip_0_ch1_rxchanisaligned_ext_UNCONNECTED,
            ch1_rxchanrealign => gt_bridge_ip_0_GT_RX1_ch_rxchanrealign,
            ch1_rxchanrealign_ext => NLW_gt_bridge_ip_0_ch1_rxchanrealign_ext_UNCONNECTED,
            ch1_rxchbondi(4 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxchbondi(4 downto 0),
            ch1_rxchbondi_ext(4 downto 0) => B"00000",
            ch1_rxchbondo(4 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxchbondo(4 downto 0),
            ch1_rxchbondo_ext(4 downto 0) => NLW_gt_bridge_ip_0_ch1_rxchbondo_ext_UNCONNECTED(4 downto 0),
            ch1_rxclkcorcnt(1 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxclkcorcnt(1 downto 0),
            ch1_rxclkcorcnt_ext(1 downto 0) => NLW_gt_bridge_ip_0_ch1_rxclkcorcnt_ext_UNCONNECTED(1 downto 0),
            ch1_rxcominitdet => gt_bridge_ip_0_GT_RX1_ch_rxcominitdet,
            ch1_rxcominitdet_ext => NLW_gt_bridge_ip_0_ch1_rxcominitdet_ext_UNCONNECTED,
            ch1_rxcommadet => gt_bridge_ip_0_GT_RX1_ch_rxcommadet,
            ch1_rxcommadet_ext => NLW_gt_bridge_ip_0_ch1_rxcommadet_ext_UNCONNECTED,
            ch1_rxcomsasdet => gt_bridge_ip_0_GT_RX1_ch_rxcomsasdet,
            ch1_rxcomsasdet_ext => NLW_gt_bridge_ip_0_ch1_rxcomsasdet_ext_UNCONNECTED,
            ch1_rxcomwakedet => gt_bridge_ip_0_GT_RX1_ch_rxcomwakedet,
            ch1_rxcomwakedet_ext => NLW_gt_bridge_ip_0_ch1_rxcomwakedet_ext_UNCONNECTED,
            ch1_rxctrl0(15 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxctrl0(15 downto 0),
            ch1_rxctrl0_ext(15 downto 0) => gt_bridge_ip_0_ch1_rxctrl0_ext(15 downto 0),
            ch1_rxctrl1(15 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxctrl1(15 downto 0),
            ch1_rxctrl1_ext(15 downto 0) => gt_bridge_ip_0_ch1_rxctrl1_ext(15 downto 0),
            ch1_rxctrl2(7 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxctrl2(7 downto 0),
            ch1_rxctrl2_ext(7 downto 0) => gt_bridge_ip_0_ch1_rxctrl2_ext(7 downto 0),
            ch1_rxctrl3(7 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxctrl3(7 downto 0),
            ch1_rxctrl3_ext(7 downto 0) => gt_bridge_ip_0_ch1_rxctrl3_ext(7 downto 0),
            ch1_rxdapicodeovrden => gt_bridge_ip_0_GT_RX1_ch_rxdapicodeovrden,
            ch1_rxdapicodeovrden_ext => '0',
            ch1_rxdapicodereset => gt_bridge_ip_0_GT_RX1_ch_rxdapicodereset,
            ch1_rxdapicodereset_ext => '0',
            ch1_rxdata(127 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxdata(127 downto 0),
            ch1_rxdata_ext(127 downto 0) => gt_bridge_ip_0_ch1_rxdata_ext(127 downto 0),
            ch1_rxdataextendrsvd(7 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxdataextendrsvd(7 downto 0),
            ch1_rxdataextendrsvd_ext(7 downto 0) => NLW_gt_bridge_ip_0_ch1_rxdataextendrsvd_ext_UNCONNECTED(7 downto 0),
            ch1_rxdatavalid(1 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxdatavalid(1 downto 0),
            ch1_rxdatavalid_ext(1 downto 0) => gt_bridge_ip_0_ch1_rxdatavalid_ext(1 downto 0),
            ch1_rxdccdone => gt_bridge_ip_0_GT_RX1_ch_rxdccdone,
            ch1_rxdccdone_ext => NLW_gt_bridge_ip_0_ch1_rxdccdone_ext_UNCONNECTED,
            ch1_rxdlyalignerr => gt_bridge_ip_0_GT_RX1_ch_rxdlyalignerr,
            ch1_rxdlyalignerr_ext => NLW_gt_bridge_ip_0_ch1_rxdlyalignerr_ext_UNCONNECTED,
            ch1_rxdlyalignprog => gt_bridge_ip_0_GT_RX1_ch_rxdlyalignprog,
            ch1_rxdlyalignprog_ext => NLW_gt_bridge_ip_0_ch1_rxdlyalignprog_ext_UNCONNECTED,
            ch1_rxdlyalignreq => gt_bridge_ip_0_GT_RX1_ch_rxdlyalignreq,
            ch1_rxdlyalignreq_ext => '0',
            ch1_rxelecidle => gt_bridge_ip_0_GT_RX1_ch_rxelecidle,
            ch1_rxelecidle_ext => NLW_gt_bridge_ip_0_ch1_rxelecidle_ext_UNCONNECTED,
            ch1_rxeqtraining => gt_bridge_ip_0_GT_RX1_ch_rxeqtraining,
            ch1_rxeqtraining_ext => '0',
            ch1_rxfinealigndone => gt_bridge_ip_0_GT_RX1_ch_rxfinealigndone,
            ch1_rxfinealigndone_ext => NLW_gt_bridge_ip_0_ch1_rxfinealigndone_ext_UNCONNECTED,
            ch1_rxgearboxslip => gt_bridge_ip_0_GT_RX1_ch_rxgearboxslip,
            ch1_rxgearboxslip_ext => ch1_rxgearboxslip_ext_0_1,
            ch1_rxheader(5 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxheader(5 downto 0),
            ch1_rxheader_ext(5 downto 0) => gt_bridge_ip_0_ch1_rxheader_ext(5 downto 0),
            ch1_rxheadervalid(1 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxheadervalid(1 downto 0),
            ch1_rxheadervalid_ext(1 downto 0) => gt_bridge_ip_0_ch1_rxheadervalid_ext(1 downto 0),
            ch1_rxlpmen => gt_bridge_ip_0_GT_RX1_ch_rxlpmen,
            ch1_rxlpmen_ext => '0',
            ch1_rxmldchaindone => gt_bridge_ip_0_GT_RX1_ch_rxmldchaindone,
            ch1_rxmldchaindone_ext => '0',
            ch1_rxmldchainreq => gt_bridge_ip_0_GT_RX1_ch_rxmldchainreq,
            ch1_rxmldchainreq_ext => '0',
            ch1_rxmlfinealignreq => gt_bridge_ip_0_GT_RX1_ch_rxmlfinealignreq,
            ch1_rxmlfinealignreq_ext => '0',
            ch1_rxmstdatapathreset => gt_bridge_ip_0_GT_RX1_ch_rxmstdatapathreset,
            ch1_rxmstreset => gt_bridge_ip_0_GT_RX1_ch_rxmstreset,
            ch1_rxmstresetdone => gt_bridge_ip_0_GT_RX1_ch_rxmstresetdone,
            ch1_rxmstresetdone_ext => NLW_gt_bridge_ip_0_ch1_rxmstresetdone_ext_UNCONNECTED,
            ch1_rxoobreset => gt_bridge_ip_0_GT_RX1_ch_rxoobreset,
            ch1_rxoobreset_ext => '0',
            ch1_rxosintdone => gt_bridge_ip_0_GT_RX1_ch_rxosintdone,
            ch1_rxosintdone_ext => NLW_gt_bridge_ip_0_ch1_rxosintdone_ext_UNCONNECTED,
            ch1_rxosintstarted => gt_bridge_ip_0_GT_RX1_ch_rxosintstarted,
            ch1_rxosintstarted_ext => NLW_gt_bridge_ip_0_ch1_rxosintstarted_ext_UNCONNECTED,
            ch1_rxosintstrobedone => gt_bridge_ip_0_GT_RX1_ch_rxosintstrobedone,
            ch1_rxosintstrobedone_ext => NLW_gt_bridge_ip_0_ch1_rxosintstrobedone_ext_UNCONNECTED,
            ch1_rxosintstrobestarted => gt_bridge_ip_0_GT_RX1_ch_rxosintstrobestarted,
            ch1_rxosintstrobestarted_ext => NLW_gt_bridge_ip_0_ch1_rxosintstrobestarted_ext_UNCONNECTED,
            ch1_rxpcsresetmask(4 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxpcsresetmask(4 downto 0),
            ch1_rxpcsresetmask_ext(4 downto 0) => B"11111",
            ch1_rxpd(1 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxpd(1 downto 0),
            ch1_rxpd_ext(1 downto 0) => B"00",
            ch1_rxphaligndone => gt_bridge_ip_0_GT_RX1_ch_rxphaligndone,
            ch1_rxphaligndone_ext => NLW_gt_bridge_ip_0_ch1_rxphaligndone_ext_UNCONNECTED,
            ch1_rxphalignerr => gt_bridge_ip_0_GT_RX1_ch_rxphalignerr,
            ch1_rxphalignerr_ext => NLW_gt_bridge_ip_0_ch1_rxphalignerr_ext_UNCONNECTED,
            ch1_rxphalignreq => gt_bridge_ip_0_GT_RX1_ch_rxphalignreq,
            ch1_rxphalignreq_ext => '0',
            ch1_rxphalignresetmask(1 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxphalignresetmask(1 downto 0),
            ch1_rxphalignresetmask_ext(1 downto 0) => B"11",
            ch1_rxphdlypd => gt_bridge_ip_0_GT_RX1_ch_rxphdlypd,
            ch1_rxphdlypd_ext => '0',
            ch1_rxphdlyreset => gt_bridge_ip_0_GT_RX1_ch_rxphdlyreset,
            ch1_rxphdlyreset_ext => '0',
            ch1_rxphdlyresetdone => gt_bridge_ip_0_GT_RX1_ch_rxphdlyresetdone,
            ch1_rxphdlyresetdone_ext => NLW_gt_bridge_ip_0_ch1_rxphdlyresetdone_ext_UNCONNECTED,
            ch1_rxphsetinitdone => gt_bridge_ip_0_GT_RX1_ch_rxphsetinitdone,
            ch1_rxphsetinitdone_ext => NLW_gt_bridge_ip_0_ch1_rxphsetinitdone_ext_UNCONNECTED,
            ch1_rxphsetinitreq => gt_bridge_ip_0_GT_RX1_ch_rxphsetinitreq,
            ch1_rxphsetinitreq_ext => '0',
            ch1_rxphshift180 => gt_bridge_ip_0_GT_RX1_ch_rxphshift180,
            ch1_rxphshift180_ext => '0',
            ch1_rxphshift180done => gt_bridge_ip_0_GT_RX1_ch_rxphshift180done,
            ch1_rxphshift180done_ext => NLW_gt_bridge_ip_0_ch1_rxphshift180done_ext_UNCONNECTED,
            ch1_rxpmaresetdone => gt_bridge_ip_0_GT_RX1_ch_rxpmaresetdone,
            ch1_rxpmaresetdone_ext => gt_bridge_ip_0_ch1_rxpmaresetdone_ext,
            ch1_rxpmaresetmask(6 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxpmaresetmask(6 downto 0),
            ch1_rxpmaresetmask_ext(6 downto 0) => B"1111111",
            ch1_rxpolarity => gt_bridge_ip_0_GT_RX1_ch_rxpolarity,
            ch1_rxpolarity_ext => ch1_rxpolarity_ext_0_1,
            ch1_rxprbscntreset => gt_bridge_ip_0_GT_RX1_ch_rxprbscntreset,
            ch1_rxprbscntreset_ext => '0',
            ch1_rxprbserr => gt_bridge_ip_0_GT_RX1_ch_rxprbserr,
            ch1_rxprbserr_ext => NLW_gt_bridge_ip_0_ch1_rxprbserr_ext_UNCONNECTED,
            ch1_rxprbslocked => gt_bridge_ip_0_GT_RX1_ch_rxprbslocked,
            ch1_rxprbslocked_ext => NLW_gt_bridge_ip_0_ch1_rxprbslocked_ext_UNCONNECTED,
            ch1_rxprbssel(3 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxprbssel(3 downto 0),
            ch1_rxprbssel_ext(3 downto 0) => B"0000",
            ch1_rxprogdivreset => gt_bridge_ip_0_GT_RX1_ch_rxprogdivreset,
            ch1_rxprogdivresetdone => gt_bridge_ip_0_GT_RX1_ch_rxprogdivresetdone,
            ch1_rxprogdivresetdone_ext => NLW_gt_bridge_ip_0_ch1_rxprogdivresetdone_ext_UNCONNECTED,
            ch1_rxrate(7 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxrate(7 downto 0),
            ch1_rxresetdone => gt_bridge_ip_0_GT_RX1_ch_rxresetdone,
            ch1_rxresetdone_ext => gt_bridge_ip_0_ch1_rxresetdone_ext,
            ch1_rxresetmode(1 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxresetmode(1 downto 0),
            ch1_rxresetmode_ext(1 downto 0) => B"00",
            ch1_rxslide => gt_bridge_ip_0_GT_RX1_ch_rxslide,
            ch1_rxslide_ext => ch1_rxslide_ext_0_1,
            ch1_rxsliderdy => gt_bridge_ip_0_GT_RX1_ch_rxsliderdy,
            ch1_rxsliderdy_ext => NLW_gt_bridge_ip_0_ch1_rxsliderdy_ext_UNCONNECTED,
            ch1_rxstartofseq(1 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxstartofseq(1 downto 0),
            ch1_rxstartofseq_ext(1 downto 0) => NLW_gt_bridge_ip_0_ch1_rxstartofseq_ext_UNCONNECTED(1 downto 0),
            ch1_rxstatus(2 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxstatus(2 downto 0),
            ch1_rxstatus_ext(2 downto 0) => NLW_gt_bridge_ip_0_ch1_rxstatus_ext_UNCONNECTED(2 downto 0),
            ch1_rxsyncallin => gt_bridge_ip_0_GT_RX1_ch_rxsyncallin,
            ch1_rxsyncallin_ext => '0',
            ch1_rxsyncdone => gt_bridge_ip_0_GT_RX1_ch_rxsyncdone,
            ch1_rxsyncdone_ext => NLW_gt_bridge_ip_0_ch1_rxsyncdone_ext_UNCONNECTED,
            ch1_rxtermination => gt_bridge_ip_0_GT_RX1_ch_rxtermination,
            ch1_rxtermination_ext => '0',
            ch1_rxuserrdy => gt_bridge_ip_0_GT_RX1_ch_rxuserrdy,
            ch1_rxvalid => gt_bridge_ip_0_GT_RX1_ch_rxvalid,
            ch1_rxvalid_ext => gt_bridge_ip_0_ch1_rxvalid_ext,
            ch1_tx10gstat => gt_bridge_ip_0_GT_TX1_ch_tx10gstat,
            ch1_tx10gstat_ext => NLW_gt_bridge_ip_0_ch1_tx10gstat_ext_UNCONNECTED,
            ch1_txbufstatus(1 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txbufstatus(1 downto 0),
            ch1_txbufstatus_ext(1 downto 0) => gt_bridge_ip_0_ch1_txbufstatus_ext(1 downto 0),
            ch1_txcomfinish => gt_bridge_ip_0_GT_TX1_ch_txcomfinish,
            ch1_txcomfinish_ext => NLW_gt_bridge_ip_0_ch1_txcomfinish_ext_UNCONNECTED,
            ch1_txcominit => gt_bridge_ip_0_GT_TX1_ch_txcominit,
            ch1_txcominit_ext => '0',
            ch1_txcomsas => gt_bridge_ip_0_GT_TX1_ch_txcomsas,
            ch1_txcomsas_ext => '0',
            ch1_txcomwake => gt_bridge_ip_0_GT_TX1_ch_txcomwake,
            ch1_txcomwake_ext => '0',
            ch1_txctrl0(15 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txctrl0(15 downto 0),
            ch1_txctrl0_ext(15 downto 0) => ch1_txctrl0_ext_0_1(15 downto 0),
            ch1_txctrl1(15 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txctrl1(15 downto 0),
            ch1_txctrl1_ext(15 downto 0) => ch1_txctrl1_ext_0_1(15 downto 0),
            ch1_txctrl2(7 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txctrl2(7 downto 0),
            ch1_txctrl2_ext(7 downto 0) => ch1_txctrl2_ext_0_1(7 downto 0),
            ch1_txdapicodeovrden => gt_bridge_ip_0_GT_TX1_ch_txdapicodeovrden,
            ch1_txdapicodeovrden_ext => '0',
            ch1_txdapicodereset => gt_bridge_ip_0_GT_TX1_ch_txdapicodereset,
            ch1_txdapicodereset_ext => '0',
            ch1_txdata(127 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txdata(127 downto 0),
            ch1_txdata_ext(127 downto 0) => ch1_txdata_ext_0_1(127 downto 0),
            ch1_txdataextendrsvd(7 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txdataextendrsvd(7 downto 0),
            ch1_txdataextendrsvd_ext(7 downto 0) => B"00000000",
            ch1_txdccdone => gt_bridge_ip_0_GT_TX1_ch_txdccdone,
            ch1_txdccdone_ext => NLW_gt_bridge_ip_0_ch1_txdccdone_ext_UNCONNECTED,
            ch1_txdeemph(1 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txdeemph(1 downto 0),
            ch1_txdeemph_ext(1 downto 0) => B"00",
            ch1_txdetectrx => gt_bridge_ip_0_GT_TX1_ch_txdetectrx,
            ch1_txdetectrx_ext => '0',
            ch1_txdiffctrl(4 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txdiffctrl(4 downto 0),
            ch1_txdiffctrl_ext(4 downto 0) => B"11001",
            ch1_txdlyalignerr => gt_bridge_ip_0_GT_TX1_ch_txdlyalignerr,
            ch1_txdlyalignerr_ext => NLW_gt_bridge_ip_0_ch1_txdlyalignerr_ext_UNCONNECTED,
            ch1_txdlyalignprog => gt_bridge_ip_0_GT_TX1_ch_txdlyalignprog,
            ch1_txdlyalignprog_ext => NLW_gt_bridge_ip_0_ch1_txdlyalignprog_ext_UNCONNECTED,
            ch1_txdlyalignreq => gt_bridge_ip_0_GT_TX1_ch_txdlyalignreq,
            ch1_txdlyalignreq_ext => '0',
            ch1_txelecidle => gt_bridge_ip_0_GT_TX1_ch_txelecidle,
            ch1_txelecidle_ext => '0',
            ch1_txheader(5 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txheader(5 downto 0),
            ch1_txheader_ext(5 downto 0) => B"000000",
            ch1_txinhibit => gt_bridge_ip_0_GT_TX1_ch_txinhibit,
            ch1_txinhibit_ext => '0',
            ch1_txmaincursor(6 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txmaincursor(6 downto 0),
            ch1_txmaincursor_ext(6 downto 0) => B"0000000",
            ch1_txmargin(2 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txmargin(2 downto 0),
            ch1_txmargin_ext(2 downto 0) => B"000",
            ch1_txmldchaindone => gt_bridge_ip_0_GT_TX1_ch_txmldchaindone,
            ch1_txmldchaindone_ext => '0',
            ch1_txmldchainreq => gt_bridge_ip_0_GT_TX1_ch_txmldchainreq,
            ch1_txmldchainreq_ext => '0',
            ch1_txmstdatapathreset => gt_bridge_ip_0_GT_TX1_ch_txmstdatapathreset,
            ch1_txmstreset => gt_bridge_ip_0_GT_TX1_ch_txmstreset,
            ch1_txmstresetdone => gt_bridge_ip_0_GT_TX1_ch_txmstresetdone,
            ch1_txmstresetdone_ext => NLW_gt_bridge_ip_0_ch1_txmstresetdone_ext_UNCONNECTED,
            ch1_txoneszeros => gt_bridge_ip_0_GT_TX1_ch_txoneszeros,
            ch1_txoneszeros_ext => '0',
            ch1_txpausedelayalign => gt_bridge_ip_0_GT_TX1_ch_txpausedelayalign,
            ch1_txpausedelayalign_ext => '0',
            ch1_txpcsresetmask => gt_bridge_ip_0_GT_TX1_ch_txpcsresetmask,
            ch1_txpcsresetmask_ext => '1',
            ch1_txpd(1 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txpd(1 downto 0),
            ch1_txpd_ext(1 downto 0) => B"00",
            ch1_txphaligndone => gt_bridge_ip_0_GT_TX1_ch_txphaligndone,
            ch1_txphaligndone_ext => NLW_gt_bridge_ip_0_ch1_txphaligndone_ext_UNCONNECTED,
            ch1_txphalignerr => gt_bridge_ip_0_GT_TX1_ch_txphalignerr,
            ch1_txphalignerr_ext => NLW_gt_bridge_ip_0_ch1_txphalignerr_ext_UNCONNECTED,
            ch1_txphalignoutrsvd => gt_bridge_ip_0_GT_TX1_ch_txphalignoutrsvd,
            ch1_txphalignoutrsvd_ext => NLW_gt_bridge_ip_0_ch1_txphalignoutrsvd_ext_UNCONNECTED,
            ch1_txphalignreq => gt_bridge_ip_0_GT_TX1_ch_txphalignreq,
            ch1_txphalignreq_ext => '0',
            ch1_txphalignresetmask(1 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txphalignresetmask(1 downto 0),
            ch1_txphalignresetmask_ext(1 downto 0) => B"11",
            ch1_txphdlypd => gt_bridge_ip_0_GT_TX1_ch_txphdlypd,
            ch1_txphdlypd_ext => '0',
            ch1_txphdlyreset => gt_bridge_ip_0_GT_TX1_ch_txphdlyreset,
            ch1_txphdlyreset_ext => '0',
            ch1_txphdlyresetdone => gt_bridge_ip_0_GT_TX1_ch_txphdlyresetdone,
            ch1_txphdlyresetdone_ext => NLW_gt_bridge_ip_0_ch1_txphdlyresetdone_ext_UNCONNECTED,
            ch1_txphsetinitdone => gt_bridge_ip_0_GT_TX1_ch_txphsetinitdone,
            ch1_txphsetinitdone_ext => NLW_gt_bridge_ip_0_ch1_txphsetinitdone_ext_UNCONNECTED,
            ch1_txphsetinitreq => gt_bridge_ip_0_GT_TX1_ch_txphsetinitreq,
            ch1_txphsetinitreq_ext => '0',
            ch1_txphshift180 => gt_bridge_ip_0_GT_TX1_ch_txphshift180,
            ch1_txphshift180_ext => '0',
            ch1_txphshift180done => gt_bridge_ip_0_GT_TX1_ch_txphshift180done,
            ch1_txphshift180done_ext => NLW_gt_bridge_ip_0_ch1_txphshift180done_ext_UNCONNECTED,
            ch1_txpicodeovrden => gt_bridge_ip_0_GT_TX1_ch_txpicodeovrden,
            ch1_txpicodeovrden_ext => '0',
            ch1_txpicodereset => gt_bridge_ip_0_GT_TX1_ch_txpicodereset,
            ch1_txpicodereset_ext => '0',
            ch1_txpippmen => gt_bridge_ip_0_GT_TX1_ch_txpippmen,
            ch1_txpippmen_ext => ch1_txpippmen_ext_0_1,
            ch1_txpippmstepsize(4 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txpippmstepsize(4 downto 0),
            ch1_txpippmstepsize_ext(4 downto 0) => ch1_txpippmstepsize_ext_0_1(4 downto 0),
            ch1_txpisopd => gt_bridge_ip_0_GT_TX1_ch_txpisopd,
            ch1_txpisopd_ext => '0',
            ch1_txpmaresetdone => gt_bridge_ip_0_GT_TX1_ch_txpmaresetdone,
            ch1_txpmaresetdone_ext => NLW_gt_bridge_ip_0_ch1_txpmaresetdone_ext_UNCONNECTED,
            ch1_txpmaresetmask(2 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txpmaresetmask(2 downto 0),
            ch1_txpmaresetmask_ext(2 downto 0) => B"111",
            ch1_txpolarity => gt_bridge_ip_0_GT_TX1_ch_txpolarity,
            ch1_txpolarity_ext => ch1_txpolarity_ext_0_1,
            ch1_txpostcursor(4 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txpostcursor(4 downto 0),
            ch1_txpostcursor_ext(4 downto 0) => B"00000",
            ch1_txprbsforceerr => gt_bridge_ip_0_GT_TX1_ch_txprbsforceerr,
            ch1_txprbsforceerr_ext => '0',
            ch1_txprbssel(3 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txprbssel(3 downto 0),
            ch1_txprbssel_ext(3 downto 0) => B"0000",
            ch1_txprecursor(4 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txprecursor(4 downto 0),
            ch1_txprecursor_ext(4 downto 0) => B"00000",
            ch1_txprogdivreset => gt_bridge_ip_0_GT_TX1_ch_txprogdivreset,
            ch1_txprogdivresetdone => gt_bridge_ip_0_GT_TX1_ch_txprogdivresetdone,
            ch1_txprogdivresetdone_ext => NLW_gt_bridge_ip_0_ch1_txprogdivresetdone_ext_UNCONNECTED,
            ch1_txrate(7 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txrate(7 downto 0),
            ch1_txresetdone => gt_bridge_ip_0_GT_TX1_ch_txresetdone,
            ch1_txresetdone_ext => gt_bridge_ip_0_ch1_txresetdone_ext,
            ch1_txresetmode(1 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txresetmode(1 downto 0),
            ch1_txresetmode_ext(1 downto 0) => B"00",
            ch1_txsequence(6 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txsequence(6 downto 0),
            ch1_txsequence_ext(6 downto 0) => ch1_txsequence_ext_0_1(6 downto 0),
            ch1_txswing => gt_bridge_ip_0_GT_TX1_ch_txswing,
            ch1_txswing_ext => '0',
            ch1_txsyncallin => gt_bridge_ip_0_GT_TX1_ch_txsyncallin,
            ch1_txsyncallin_ext => '0',
            ch1_txsyncdone => gt_bridge_ip_0_GT_TX1_ch_txsyncdone,
            ch1_txsyncdone_ext => NLW_gt_bridge_ip_0_ch1_txsyncdone_ext_UNCONNECTED,
            ch1_txuserrdy => gt_bridge_ip_0_GT_TX1_ch_txuserrdy,
            ch2_cdrbmcdrreq => gt_bridge_ip_0_GT_RX2_ch_cdrbmcdrreq,
            ch2_cdrbmcdrreq_ext => '0',
            ch2_cdrfreqos => gt_bridge_ip_0_GT_RX2_ch_cdrfreqos,
            ch2_cdrfreqos_ext => '0',
            ch2_cdrincpctrl => gt_bridge_ip_0_GT_RX2_ch_cdrincpctrl,
            ch2_cdrincpctrl_ext => '0',
            ch2_cdrstepdir => gt_bridge_ip_0_GT_RX2_ch_cdrstepdir,
            ch2_cdrstepdir_ext => '0',
            ch2_cdrstepsq => gt_bridge_ip_0_GT_RX2_ch_cdrstepsq,
            ch2_cdrstepsq_ext => '0',
            ch2_cdrstepsx => gt_bridge_ip_0_GT_RX2_ch_cdrstepsx,
            ch2_cdrstepsx_ext => '0',
            ch2_cfokovrdfinish => gt_bridge_ip_0_GT_RX2_ch_cfokovrdfinish,
            ch2_cfokovrdfinish_ext => '0',
            ch2_cfokovrdpulse => gt_bridge_ip_0_GT_RX2_ch_cfokovrdpulse,
            ch2_cfokovrdpulse_ext => '0',
            ch2_cfokovrdrdy0 => gt_bridge_ip_0_GT_RX2_ch_cfokovrdrdy0,
            ch2_cfokovrdrdy0_ext => NLW_gt_bridge_ip_0_ch2_cfokovrdrdy0_ext_UNCONNECTED,
            ch2_cfokovrdrdy1 => gt_bridge_ip_0_GT_RX2_ch_cfokovrdrdy1,
            ch2_cfokovrdrdy1_ext => NLW_gt_bridge_ip_0_ch2_cfokovrdrdy1_ext_UNCONNECTED,
            ch2_cfokovrdstart => gt_bridge_ip_0_GT_RX2_ch_cfokovrdstart,
            ch2_cfokovrdstart_ext => '0',
            ch2_eyescandataerror => gt_bridge_ip_0_GT_RX2_ch_eyescandataerror,
            ch2_eyescandataerror_ext => NLW_gt_bridge_ip_0_ch2_eyescandataerror_ext_UNCONNECTED,
            ch2_eyescanreset => gt_bridge_ip_0_GT_RX2_ch_eyescanreset,
            ch2_eyescanreset_ext => '0',
            ch2_eyescantrigger => gt_bridge_ip_0_GT_RX2_ch_eyescantrigger,
            ch2_eyescantrigger_ext => '0',
            ch2_gtrxreset => gt_bridge_ip_0_GT_RX2_ch_gtrxreset,
            ch2_gttxreset => gt_bridge_ip_0_GT_TX2_ch_gttxreset,
            ch2_rx10gstat(7 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rx10gstat(7 downto 0),
            ch2_rx10gstat_ext(7 downto 0) => NLW_gt_bridge_ip_0_ch2_rx10gstat_ext_UNCONNECTED(7 downto 0),
            ch2_rxbufstatus(2 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxbufstatus(2 downto 0),
            ch2_rxbufstatus_ext(2 downto 0) => NLW_gt_bridge_ip_0_ch2_rxbufstatus_ext_UNCONNECTED(2 downto 0),
            ch2_rxbyteisaligned => gt_bridge_ip_0_GT_RX2_ch_rxbyteisaligned,
            ch2_rxbyteisaligned_ext => gt_bridge_ip_0_ch2_rxbyteisaligned_ext,
            ch2_rxbyterealign => gt_bridge_ip_0_GT_RX2_ch_rxbyterealign,
            ch2_rxbyterealign_ext => NLW_gt_bridge_ip_0_ch2_rxbyterealign_ext_UNCONNECTED,
            ch2_rxcdrhold => gt_bridge_ip_0_GT_RX2_ch_rxcdrhold,
            ch2_rxcdrhold_ext => '0',
            ch2_rxcdrlock => gt_bridge_ip_0_GT_RX2_ch_rxcdrlock,
            ch2_rxcdrlock_ext => gt_bridge_ip_0_ch2_rxcdrlock_ext,
            ch2_rxcdrovrden => gt_bridge_ip_0_GT_RX2_ch_rxcdrovrden,
            ch2_rxcdrovrden_ext => '0',
            ch2_rxcdrphdone => gt_bridge_ip_0_GT_RX2_ch_rxcdrphdone,
            ch2_rxcdrphdone_ext => NLW_gt_bridge_ip_0_ch2_rxcdrphdone_ext_UNCONNECTED,
            ch2_rxcdrreset => gt_bridge_ip_0_GT_RX2_ch_rxcdrreset,
            ch2_rxcdrreset_ext => '0',
            ch2_rxchanbondseq => gt_bridge_ip_0_GT_RX2_ch_rxchanbondseq,
            ch2_rxchanbondseq_ext => NLW_gt_bridge_ip_0_ch2_rxchanbondseq_ext_UNCONNECTED,
            ch2_rxchanisaligned => gt_bridge_ip_0_GT_RX2_ch_rxchanisaligned,
            ch2_rxchanisaligned_ext => NLW_gt_bridge_ip_0_ch2_rxchanisaligned_ext_UNCONNECTED,
            ch2_rxchanrealign => gt_bridge_ip_0_GT_RX2_ch_rxchanrealign,
            ch2_rxchanrealign_ext => NLW_gt_bridge_ip_0_ch2_rxchanrealign_ext_UNCONNECTED,
            ch2_rxchbondi(4 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxchbondi(4 downto 0),
            ch2_rxchbondi_ext(4 downto 0) => B"00000",
            ch2_rxchbondo(4 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxchbondo(4 downto 0),
            ch2_rxchbondo_ext(4 downto 0) => NLW_gt_bridge_ip_0_ch2_rxchbondo_ext_UNCONNECTED(4 downto 0),
            ch2_rxclkcorcnt(1 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxclkcorcnt(1 downto 0),
            ch2_rxclkcorcnt_ext(1 downto 0) => NLW_gt_bridge_ip_0_ch2_rxclkcorcnt_ext_UNCONNECTED(1 downto 0),
            ch2_rxcominitdet => gt_bridge_ip_0_GT_RX2_ch_rxcominitdet,
            ch2_rxcominitdet_ext => NLW_gt_bridge_ip_0_ch2_rxcominitdet_ext_UNCONNECTED,
            ch2_rxcommadet => gt_bridge_ip_0_GT_RX2_ch_rxcommadet,
            ch2_rxcommadet_ext => NLW_gt_bridge_ip_0_ch2_rxcommadet_ext_UNCONNECTED,
            ch2_rxcomsasdet => gt_bridge_ip_0_GT_RX2_ch_rxcomsasdet,
            ch2_rxcomsasdet_ext => NLW_gt_bridge_ip_0_ch2_rxcomsasdet_ext_UNCONNECTED,
            ch2_rxcomwakedet => gt_bridge_ip_0_GT_RX2_ch_rxcomwakedet,
            ch2_rxcomwakedet_ext => NLW_gt_bridge_ip_0_ch2_rxcomwakedet_ext_UNCONNECTED,
            ch2_rxctrl0(15 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxctrl0(15 downto 0),
            ch2_rxctrl0_ext(15 downto 0) => gt_bridge_ip_0_ch2_rxctrl0_ext(15 downto 0),
            ch2_rxctrl1(15 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxctrl1(15 downto 0),
            ch2_rxctrl1_ext(15 downto 0) => gt_bridge_ip_0_ch2_rxctrl1_ext(15 downto 0),
            ch2_rxctrl2(7 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxctrl2(7 downto 0),
            ch2_rxctrl2_ext(7 downto 0) => gt_bridge_ip_0_ch2_rxctrl2_ext(7 downto 0),
            ch2_rxctrl3(7 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxctrl3(7 downto 0),
            ch2_rxctrl3_ext(7 downto 0) => gt_bridge_ip_0_ch2_rxctrl3_ext(7 downto 0),
            ch2_rxdapicodeovrden => gt_bridge_ip_0_GT_RX2_ch_rxdapicodeovrden,
            ch2_rxdapicodeovrden_ext => '0',
            ch2_rxdapicodereset => gt_bridge_ip_0_GT_RX2_ch_rxdapicodereset,
            ch2_rxdapicodereset_ext => '0',
            ch2_rxdata(127 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxdata(127 downto 0),
            ch2_rxdata_ext(127 downto 0) => gt_bridge_ip_0_ch2_rxdata_ext(127 downto 0),
            ch2_rxdataextendrsvd(7 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxdataextendrsvd(7 downto 0),
            ch2_rxdataextendrsvd_ext(7 downto 0) => NLW_gt_bridge_ip_0_ch2_rxdataextendrsvd_ext_UNCONNECTED(7 downto 0),
            ch2_rxdatavalid(1 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxdatavalid(1 downto 0),
            ch2_rxdatavalid_ext(1 downto 0) => gt_bridge_ip_0_ch2_rxdatavalid_ext(1 downto 0),
            ch2_rxdccdone => gt_bridge_ip_0_GT_RX2_ch_rxdccdone,
            ch2_rxdccdone_ext => NLW_gt_bridge_ip_0_ch2_rxdccdone_ext_UNCONNECTED,
            ch2_rxdlyalignerr => gt_bridge_ip_0_GT_RX2_ch_rxdlyalignerr,
            ch2_rxdlyalignerr_ext => NLW_gt_bridge_ip_0_ch2_rxdlyalignerr_ext_UNCONNECTED,
            ch2_rxdlyalignprog => gt_bridge_ip_0_GT_RX2_ch_rxdlyalignprog,
            ch2_rxdlyalignprog_ext => NLW_gt_bridge_ip_0_ch2_rxdlyalignprog_ext_UNCONNECTED,
            ch2_rxdlyalignreq => gt_bridge_ip_0_GT_RX2_ch_rxdlyalignreq,
            ch2_rxdlyalignreq_ext => '0',
            ch2_rxelecidle => gt_bridge_ip_0_GT_RX2_ch_rxelecidle,
            ch2_rxelecidle_ext => NLW_gt_bridge_ip_0_ch2_rxelecidle_ext_UNCONNECTED,
            ch2_rxeqtraining => gt_bridge_ip_0_GT_RX2_ch_rxeqtraining,
            ch2_rxeqtraining_ext => '0',
            ch2_rxfinealigndone => gt_bridge_ip_0_GT_RX2_ch_rxfinealigndone,
            ch2_rxfinealigndone_ext => NLW_gt_bridge_ip_0_ch2_rxfinealigndone_ext_UNCONNECTED,
            ch2_rxgearboxslip => gt_bridge_ip_0_GT_RX2_ch_rxgearboxslip,
            ch2_rxgearboxslip_ext => ch2_rxgearboxslip_ext_0_1,
            ch2_rxheader(5 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxheader(5 downto 0),
            ch2_rxheader_ext(5 downto 0) => gt_bridge_ip_0_ch2_rxheader_ext(5 downto 0),
            ch2_rxheadervalid(1 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxheadervalid(1 downto 0),
            ch2_rxheadervalid_ext(1 downto 0) => gt_bridge_ip_0_ch2_rxheadervalid_ext(1 downto 0),
            ch2_rxlpmen => gt_bridge_ip_0_GT_RX2_ch_rxlpmen,
            ch2_rxlpmen_ext => '0',
            ch2_rxmldchaindone => gt_bridge_ip_0_GT_RX2_ch_rxmldchaindone,
            ch2_rxmldchaindone_ext => '0',
            ch2_rxmldchainreq => gt_bridge_ip_0_GT_RX2_ch_rxmldchainreq,
            ch2_rxmldchainreq_ext => '0',
            ch2_rxmlfinealignreq => gt_bridge_ip_0_GT_RX2_ch_rxmlfinealignreq,
            ch2_rxmlfinealignreq_ext => '0',
            ch2_rxmstdatapathreset => gt_bridge_ip_0_GT_RX2_ch_rxmstdatapathreset,
            ch2_rxmstreset => gt_bridge_ip_0_GT_RX2_ch_rxmstreset,
            ch2_rxmstresetdone => gt_bridge_ip_0_GT_RX2_ch_rxmstresetdone,
            ch2_rxmstresetdone_ext => NLW_gt_bridge_ip_0_ch2_rxmstresetdone_ext_UNCONNECTED,
            ch2_rxoobreset => gt_bridge_ip_0_GT_RX2_ch_rxoobreset,
            ch2_rxoobreset_ext => '0',
            ch2_rxosintdone => gt_bridge_ip_0_GT_RX2_ch_rxosintdone,
            ch2_rxosintdone_ext => NLW_gt_bridge_ip_0_ch2_rxosintdone_ext_UNCONNECTED,
            ch2_rxosintstarted => gt_bridge_ip_0_GT_RX2_ch_rxosintstarted,
            ch2_rxosintstarted_ext => NLW_gt_bridge_ip_0_ch2_rxosintstarted_ext_UNCONNECTED,
            ch2_rxosintstrobedone => gt_bridge_ip_0_GT_RX2_ch_rxosintstrobedone,
            ch2_rxosintstrobedone_ext => NLW_gt_bridge_ip_0_ch2_rxosintstrobedone_ext_UNCONNECTED,
            ch2_rxosintstrobestarted => gt_bridge_ip_0_GT_RX2_ch_rxosintstrobestarted,
            ch2_rxosintstrobestarted_ext => NLW_gt_bridge_ip_0_ch2_rxosintstrobestarted_ext_UNCONNECTED,
            ch2_rxpcsresetmask(4 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxpcsresetmask(4 downto 0),
            ch2_rxpcsresetmask_ext(4 downto 0) => B"11111",
            ch2_rxpd(1 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxpd(1 downto 0),
            ch2_rxpd_ext(1 downto 0) => B"00",
            ch2_rxphaligndone => gt_bridge_ip_0_GT_RX2_ch_rxphaligndone,
            ch2_rxphaligndone_ext => NLW_gt_bridge_ip_0_ch2_rxphaligndone_ext_UNCONNECTED,
            ch2_rxphalignerr => gt_bridge_ip_0_GT_RX2_ch_rxphalignerr,
            ch2_rxphalignerr_ext => NLW_gt_bridge_ip_0_ch2_rxphalignerr_ext_UNCONNECTED,
            ch2_rxphalignreq => gt_bridge_ip_0_GT_RX2_ch_rxphalignreq,
            ch2_rxphalignreq_ext => '0',
            ch2_rxphalignresetmask(1 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxphalignresetmask(1 downto 0),
            ch2_rxphalignresetmask_ext(1 downto 0) => B"11",
            ch2_rxphdlypd => gt_bridge_ip_0_GT_RX2_ch_rxphdlypd,
            ch2_rxphdlypd_ext => '0',
            ch2_rxphdlyreset => gt_bridge_ip_0_GT_RX2_ch_rxphdlyreset,
            ch2_rxphdlyreset_ext => '0',
            ch2_rxphdlyresetdone => gt_bridge_ip_0_GT_RX2_ch_rxphdlyresetdone,
            ch2_rxphdlyresetdone_ext => NLW_gt_bridge_ip_0_ch2_rxphdlyresetdone_ext_UNCONNECTED,
            ch2_rxphsetinitdone => gt_bridge_ip_0_GT_RX2_ch_rxphsetinitdone,
            ch2_rxphsetinitdone_ext => NLW_gt_bridge_ip_0_ch2_rxphsetinitdone_ext_UNCONNECTED,
            ch2_rxphsetinitreq => gt_bridge_ip_0_GT_RX2_ch_rxphsetinitreq,
            ch2_rxphsetinitreq_ext => '0',
            ch2_rxphshift180 => gt_bridge_ip_0_GT_RX2_ch_rxphshift180,
            ch2_rxphshift180_ext => '0',
            ch2_rxphshift180done => gt_bridge_ip_0_GT_RX2_ch_rxphshift180done,
            ch2_rxphshift180done_ext => NLW_gt_bridge_ip_0_ch2_rxphshift180done_ext_UNCONNECTED,
            ch2_rxpmaresetdone => gt_bridge_ip_0_GT_RX2_ch_rxpmaresetdone,
            ch2_rxpmaresetdone_ext => gt_bridge_ip_0_ch2_rxpmaresetdone_ext,
            ch2_rxpmaresetmask(6 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxpmaresetmask(6 downto 0),
            ch2_rxpmaresetmask_ext(6 downto 0) => B"1111111",
            ch2_rxpolarity => gt_bridge_ip_0_GT_RX2_ch_rxpolarity,
            ch2_rxpolarity_ext => ch2_rxpolarity_ext_0_1,
            ch2_rxprbscntreset => gt_bridge_ip_0_GT_RX2_ch_rxprbscntreset,
            ch2_rxprbscntreset_ext => '0',
            ch2_rxprbserr => gt_bridge_ip_0_GT_RX2_ch_rxprbserr,
            ch2_rxprbserr_ext => NLW_gt_bridge_ip_0_ch2_rxprbserr_ext_UNCONNECTED,
            ch2_rxprbslocked => gt_bridge_ip_0_GT_RX2_ch_rxprbslocked,
            ch2_rxprbslocked_ext => NLW_gt_bridge_ip_0_ch2_rxprbslocked_ext_UNCONNECTED,
            ch2_rxprbssel(3 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxprbssel(3 downto 0),
            ch2_rxprbssel_ext(3 downto 0) => B"0000",
            ch2_rxprogdivreset => gt_bridge_ip_0_GT_RX2_ch_rxprogdivreset,
            ch2_rxprogdivresetdone => gt_bridge_ip_0_GT_RX2_ch_rxprogdivresetdone,
            ch2_rxprogdivresetdone_ext => NLW_gt_bridge_ip_0_ch2_rxprogdivresetdone_ext_UNCONNECTED,
            ch2_rxrate(7 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxrate(7 downto 0),
            ch2_rxresetdone => gt_bridge_ip_0_GT_RX2_ch_rxresetdone,
            ch2_rxresetdone_ext => gt_bridge_ip_0_ch2_rxresetdone_ext,
            ch2_rxresetmode(1 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxresetmode(1 downto 0),
            ch2_rxresetmode_ext(1 downto 0) => B"00",
            ch2_rxslide => gt_bridge_ip_0_GT_RX2_ch_rxslide,
            ch2_rxslide_ext => ch2_rxslide_ext_0_1,
            ch2_rxsliderdy => gt_bridge_ip_0_GT_RX2_ch_rxsliderdy,
            ch2_rxsliderdy_ext => NLW_gt_bridge_ip_0_ch2_rxsliderdy_ext_UNCONNECTED,
            ch2_rxstartofseq(1 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxstartofseq(1 downto 0),
            ch2_rxstartofseq_ext(1 downto 0) => NLW_gt_bridge_ip_0_ch2_rxstartofseq_ext_UNCONNECTED(1 downto 0),
            ch2_rxstatus(2 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxstatus(2 downto 0),
            ch2_rxstatus_ext(2 downto 0) => NLW_gt_bridge_ip_0_ch2_rxstatus_ext_UNCONNECTED(2 downto 0),
            ch2_rxsyncallin => gt_bridge_ip_0_GT_RX2_ch_rxsyncallin,
            ch2_rxsyncallin_ext => '0',
            ch2_rxsyncdone => gt_bridge_ip_0_GT_RX2_ch_rxsyncdone,
            ch2_rxsyncdone_ext => NLW_gt_bridge_ip_0_ch2_rxsyncdone_ext_UNCONNECTED,
            ch2_rxtermination => gt_bridge_ip_0_GT_RX2_ch_rxtermination,
            ch2_rxtermination_ext => '0',
            ch2_rxuserrdy => gt_bridge_ip_0_GT_RX2_ch_rxuserrdy,
            ch2_rxvalid => gt_bridge_ip_0_GT_RX2_ch_rxvalid,
            ch2_rxvalid_ext => gt_bridge_ip_0_ch2_rxvalid_ext,
            ch2_tx10gstat => gt_bridge_ip_0_GT_TX2_ch_tx10gstat,
            ch2_tx10gstat_ext => NLW_gt_bridge_ip_0_ch2_tx10gstat_ext_UNCONNECTED,
            ch2_txbufstatus(1 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txbufstatus(1 downto 0),
            ch2_txbufstatus_ext(1 downto 0) => gt_bridge_ip_0_ch2_txbufstatus_ext(1 downto 0),
            ch2_txcomfinish => gt_bridge_ip_0_GT_TX2_ch_txcomfinish,
            ch2_txcomfinish_ext => NLW_gt_bridge_ip_0_ch2_txcomfinish_ext_UNCONNECTED,
            ch2_txcominit => gt_bridge_ip_0_GT_TX2_ch_txcominit,
            ch2_txcominit_ext => '0',
            ch2_txcomsas => gt_bridge_ip_0_GT_TX2_ch_txcomsas,
            ch2_txcomsas_ext => '0',
            ch2_txcomwake => gt_bridge_ip_0_GT_TX2_ch_txcomwake,
            ch2_txcomwake_ext => '0',
            ch2_txctrl0(15 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txctrl0(15 downto 0),
            ch2_txctrl0_ext(15 downto 0) => ch2_txctrl0_ext_0_1(15 downto 0),
            ch2_txctrl1(15 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txctrl1(15 downto 0),
            ch2_txctrl1_ext(15 downto 0) => ch2_txctrl1_ext_0_1(15 downto 0),
            ch2_txctrl2(7 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txctrl2(7 downto 0),
            ch2_txctrl2_ext(7 downto 0) => ch2_txctrl2_ext_0_1(7 downto 0),
            ch2_txdapicodeovrden => gt_bridge_ip_0_GT_TX2_ch_txdapicodeovrden,
            ch2_txdapicodeovrden_ext => '0',
            ch2_txdapicodereset => gt_bridge_ip_0_GT_TX2_ch_txdapicodereset,
            ch2_txdapicodereset_ext => '0',
            ch2_txdata(127 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txdata(127 downto 0),
            ch2_txdata_ext(127 downto 0) => ch2_txdata_ext_0_1(127 downto 0),
            ch2_txdataextendrsvd(7 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txdataextendrsvd(7 downto 0),
            ch2_txdataextendrsvd_ext(7 downto 0) => B"00000000",
            ch2_txdccdone => gt_bridge_ip_0_GT_TX2_ch_txdccdone,
            ch2_txdccdone_ext => NLW_gt_bridge_ip_0_ch2_txdccdone_ext_UNCONNECTED,
            ch2_txdeemph(1 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txdeemph(1 downto 0),
            ch2_txdeemph_ext(1 downto 0) => B"00",
            ch2_txdetectrx => gt_bridge_ip_0_GT_TX2_ch_txdetectrx,
            ch2_txdetectrx_ext => '0',
            ch2_txdiffctrl(4 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txdiffctrl(4 downto 0),
            ch2_txdiffctrl_ext(4 downto 0) => B"11001",
            ch2_txdlyalignerr => gt_bridge_ip_0_GT_TX2_ch_txdlyalignerr,
            ch2_txdlyalignerr_ext => NLW_gt_bridge_ip_0_ch2_txdlyalignerr_ext_UNCONNECTED,
            ch2_txdlyalignprog => gt_bridge_ip_0_GT_TX2_ch_txdlyalignprog,
            ch2_txdlyalignprog_ext => NLW_gt_bridge_ip_0_ch2_txdlyalignprog_ext_UNCONNECTED,
            ch2_txdlyalignreq => gt_bridge_ip_0_GT_TX2_ch_txdlyalignreq,
            ch2_txdlyalignreq_ext => '0',
            ch2_txelecidle => gt_bridge_ip_0_GT_TX2_ch_txelecidle,
            ch2_txelecidle_ext => '0',
            ch2_txheader(5 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txheader(5 downto 0),
            ch2_txheader_ext(5 downto 0) => B"000000",
            ch2_txinhibit => gt_bridge_ip_0_GT_TX2_ch_txinhibit,
            ch2_txinhibit_ext => '0',
            ch2_txmaincursor(6 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txmaincursor(6 downto 0),
            ch2_txmaincursor_ext(6 downto 0) => B"0000000",
            ch2_txmargin(2 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txmargin(2 downto 0),
            ch2_txmargin_ext(2 downto 0) => B"000",
            ch2_txmldchaindone => gt_bridge_ip_0_GT_TX2_ch_txmldchaindone,
            ch2_txmldchaindone_ext => '0',
            ch2_txmldchainreq => gt_bridge_ip_0_GT_TX2_ch_txmldchainreq,
            ch2_txmldchainreq_ext => '0',
            ch2_txmstdatapathreset => gt_bridge_ip_0_GT_TX2_ch_txmstdatapathreset,
            ch2_txmstreset => gt_bridge_ip_0_GT_TX2_ch_txmstreset,
            ch2_txmstresetdone => gt_bridge_ip_0_GT_TX2_ch_txmstresetdone,
            ch2_txmstresetdone_ext => NLW_gt_bridge_ip_0_ch2_txmstresetdone_ext_UNCONNECTED,
            ch2_txoneszeros => gt_bridge_ip_0_GT_TX2_ch_txoneszeros,
            ch2_txoneszeros_ext => '0',
            ch2_txpausedelayalign => gt_bridge_ip_0_GT_TX2_ch_txpausedelayalign,
            ch2_txpausedelayalign_ext => '0',
            ch2_txpcsresetmask => gt_bridge_ip_0_GT_TX2_ch_txpcsresetmask,
            ch2_txpcsresetmask_ext => '1',
            ch2_txpd(1 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txpd(1 downto 0),
            ch2_txpd_ext(1 downto 0) => B"00",
            ch2_txphaligndone => gt_bridge_ip_0_GT_TX2_ch_txphaligndone,
            ch2_txphaligndone_ext => NLW_gt_bridge_ip_0_ch2_txphaligndone_ext_UNCONNECTED,
            ch2_txphalignerr => gt_bridge_ip_0_GT_TX2_ch_txphalignerr,
            ch2_txphalignerr_ext => NLW_gt_bridge_ip_0_ch2_txphalignerr_ext_UNCONNECTED,
            ch2_txphalignoutrsvd => gt_bridge_ip_0_GT_TX2_ch_txphalignoutrsvd,
            ch2_txphalignoutrsvd_ext => NLW_gt_bridge_ip_0_ch2_txphalignoutrsvd_ext_UNCONNECTED,
            ch2_txphalignreq => gt_bridge_ip_0_GT_TX2_ch_txphalignreq,
            ch2_txphalignreq_ext => '0',
            ch2_txphalignresetmask(1 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txphalignresetmask(1 downto 0),
            ch2_txphalignresetmask_ext(1 downto 0) => B"11",
            ch2_txphdlypd => gt_bridge_ip_0_GT_TX2_ch_txphdlypd,
            ch2_txphdlypd_ext => '0',
            ch2_txphdlyreset => gt_bridge_ip_0_GT_TX2_ch_txphdlyreset,
            ch2_txphdlyreset_ext => '0',
            ch2_txphdlyresetdone => gt_bridge_ip_0_GT_TX2_ch_txphdlyresetdone,
            ch2_txphdlyresetdone_ext => NLW_gt_bridge_ip_0_ch2_txphdlyresetdone_ext_UNCONNECTED,
            ch2_txphsetinitdone => gt_bridge_ip_0_GT_TX2_ch_txphsetinitdone,
            ch2_txphsetinitdone_ext => NLW_gt_bridge_ip_0_ch2_txphsetinitdone_ext_UNCONNECTED,
            ch2_txphsetinitreq => gt_bridge_ip_0_GT_TX2_ch_txphsetinitreq,
            ch2_txphsetinitreq_ext => '0',
            ch2_txphshift180 => gt_bridge_ip_0_GT_TX2_ch_txphshift180,
            ch2_txphshift180_ext => '0',
            ch2_txphshift180done => gt_bridge_ip_0_GT_TX2_ch_txphshift180done,
            ch2_txphshift180done_ext => NLW_gt_bridge_ip_0_ch2_txphshift180done_ext_UNCONNECTED,
            ch2_txpicodeovrden => gt_bridge_ip_0_GT_TX2_ch_txpicodeovrden,
            ch2_txpicodeovrden_ext => '0',
            ch2_txpicodereset => gt_bridge_ip_0_GT_TX2_ch_txpicodereset,
            ch2_txpicodereset_ext => '0',
            ch2_txpippmen => gt_bridge_ip_0_GT_TX2_ch_txpippmen,
            ch2_txpippmen_ext => ch2_txpippmen_ext_0_1,
            ch2_txpippmstepsize(4 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txpippmstepsize(4 downto 0),
            ch2_txpippmstepsize_ext(4 downto 0) => ch2_txpippmstepsize_ext_0_1(4 downto 0),
            ch2_txpisopd => gt_bridge_ip_0_GT_TX2_ch_txpisopd,
            ch2_txpisopd_ext => '0',
            ch2_txpmaresetdone => gt_bridge_ip_0_GT_TX2_ch_txpmaresetdone,
            ch2_txpmaresetdone_ext => NLW_gt_bridge_ip_0_ch2_txpmaresetdone_ext_UNCONNECTED,
            ch2_txpmaresetmask(2 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txpmaresetmask(2 downto 0),
            ch2_txpmaresetmask_ext(2 downto 0) => B"111",
            ch2_txpolarity => gt_bridge_ip_0_GT_TX2_ch_txpolarity,
            ch2_txpolarity_ext => ch2_txpolarity_ext_0_1,
            ch2_txpostcursor(4 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txpostcursor(4 downto 0),
            ch2_txpostcursor_ext(4 downto 0) => B"00000",
            ch2_txprbsforceerr => gt_bridge_ip_0_GT_TX2_ch_txprbsforceerr,
            ch2_txprbsforceerr_ext => '0',
            ch2_txprbssel(3 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txprbssel(3 downto 0),
            ch2_txprbssel_ext(3 downto 0) => B"0000",
            ch2_txprecursor(4 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txprecursor(4 downto 0),
            ch2_txprecursor_ext(4 downto 0) => B"00000",
            ch2_txprogdivreset => gt_bridge_ip_0_GT_TX2_ch_txprogdivreset,
            ch2_txprogdivresetdone => gt_bridge_ip_0_GT_TX2_ch_txprogdivresetdone,
            ch2_txprogdivresetdone_ext => NLW_gt_bridge_ip_0_ch2_txprogdivresetdone_ext_UNCONNECTED,
            ch2_txrate(7 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txrate(7 downto 0),
            ch2_txresetdone => gt_bridge_ip_0_GT_TX2_ch_txresetdone,
            ch2_txresetdone_ext => gt_bridge_ip_0_ch2_txresetdone_ext,
            ch2_txresetmode(1 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txresetmode(1 downto 0),
            ch2_txresetmode_ext(1 downto 0) => B"00",
            ch2_txsequence(6 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txsequence(6 downto 0),
            ch2_txsequence_ext(6 downto 0) => ch2_txsequence_ext_0_1(6 downto 0),
            ch2_txswing => gt_bridge_ip_0_GT_TX2_ch_txswing,
            ch2_txswing_ext => '0',
            ch2_txsyncallin => gt_bridge_ip_0_GT_TX2_ch_txsyncallin,
            ch2_txsyncallin_ext => '0',
            ch2_txsyncdone => gt_bridge_ip_0_GT_TX2_ch_txsyncdone,
            ch2_txsyncdone_ext => NLW_gt_bridge_ip_0_ch2_txsyncdone_ext_UNCONNECTED,
            ch2_txuserrdy => gt_bridge_ip_0_GT_TX2_ch_txuserrdy,
            ch3_cdrbmcdrreq => gt_bridge_ip_0_GT_RX3_ch_cdrbmcdrreq,
            ch3_cdrbmcdrreq_ext => '0',
            ch3_cdrfreqos => gt_bridge_ip_0_GT_RX3_ch_cdrfreqos,
            ch3_cdrfreqos_ext => '0',
            ch3_cdrincpctrl => gt_bridge_ip_0_GT_RX3_ch_cdrincpctrl,
            ch3_cdrincpctrl_ext => '0',
            ch3_cdrstepdir => gt_bridge_ip_0_GT_RX3_ch_cdrstepdir,
            ch3_cdrstepdir_ext => '0',
            ch3_cdrstepsq => gt_bridge_ip_0_GT_RX3_ch_cdrstepsq,
            ch3_cdrstepsq_ext => '0',
            ch3_cdrstepsx => gt_bridge_ip_0_GT_RX3_ch_cdrstepsx,
            ch3_cdrstepsx_ext => '0',
            ch3_cfokovrdfinish => gt_bridge_ip_0_GT_RX3_ch_cfokovrdfinish,
            ch3_cfokovrdfinish_ext => '0',
            ch3_cfokovrdpulse => gt_bridge_ip_0_GT_RX3_ch_cfokovrdpulse,
            ch3_cfokovrdpulse_ext => '0',
            ch3_cfokovrdrdy0 => gt_bridge_ip_0_GT_RX3_ch_cfokovrdrdy0,
            ch3_cfokovrdrdy0_ext => NLW_gt_bridge_ip_0_ch3_cfokovrdrdy0_ext_UNCONNECTED,
            ch3_cfokovrdrdy1 => gt_bridge_ip_0_GT_RX3_ch_cfokovrdrdy1,
            ch3_cfokovrdrdy1_ext => NLW_gt_bridge_ip_0_ch3_cfokovrdrdy1_ext_UNCONNECTED,
            ch3_cfokovrdstart => gt_bridge_ip_0_GT_RX3_ch_cfokovrdstart,
            ch3_cfokovrdstart_ext => '0',
            ch3_eyescandataerror => gt_bridge_ip_0_GT_RX3_ch_eyescandataerror,
            ch3_eyescandataerror_ext => NLW_gt_bridge_ip_0_ch3_eyescandataerror_ext_UNCONNECTED,
            ch3_eyescanreset => gt_bridge_ip_0_GT_RX3_ch_eyescanreset,
            ch3_eyescanreset_ext => '0',
            ch3_eyescantrigger => gt_bridge_ip_0_GT_RX3_ch_eyescantrigger,
            ch3_eyescantrigger_ext => '0',
            ch3_gtrxreset => gt_bridge_ip_0_GT_RX3_ch_gtrxreset,
            ch3_gttxreset => gt_bridge_ip_0_GT_TX3_ch_gttxreset,
            ch3_rx10gstat(7 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rx10gstat(7 downto 0),
            ch3_rx10gstat_ext(7 downto 0) => NLW_gt_bridge_ip_0_ch3_rx10gstat_ext_UNCONNECTED(7 downto 0),
            ch3_rxbufstatus(2 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxbufstatus(2 downto 0),
            ch3_rxbufstatus_ext(2 downto 0) => NLW_gt_bridge_ip_0_ch3_rxbufstatus_ext_UNCONNECTED(2 downto 0),
            ch3_rxbyteisaligned => gt_bridge_ip_0_GT_RX3_ch_rxbyteisaligned,
            ch3_rxbyteisaligned_ext => gt_bridge_ip_0_ch3_rxbyteisaligned_ext,
            ch3_rxbyterealign => gt_bridge_ip_0_GT_RX3_ch_rxbyterealign,
            ch3_rxbyterealign_ext => NLW_gt_bridge_ip_0_ch3_rxbyterealign_ext_UNCONNECTED,
            ch3_rxcdrhold => gt_bridge_ip_0_GT_RX3_ch_rxcdrhold,
            ch3_rxcdrhold_ext => '0',
            ch3_rxcdrlock => gt_bridge_ip_0_GT_RX3_ch_rxcdrlock,
            ch3_rxcdrlock_ext => gt_bridge_ip_0_ch3_rxcdrlock_ext,
            ch3_rxcdrovrden => gt_bridge_ip_0_GT_RX3_ch_rxcdrovrden,
            ch3_rxcdrovrden_ext => '0',
            ch3_rxcdrphdone => gt_bridge_ip_0_GT_RX3_ch_rxcdrphdone,
            ch3_rxcdrphdone_ext => NLW_gt_bridge_ip_0_ch3_rxcdrphdone_ext_UNCONNECTED,
            ch3_rxcdrreset => gt_bridge_ip_0_GT_RX3_ch_rxcdrreset,
            ch3_rxcdrreset_ext => '0',
            ch3_rxchanbondseq => gt_bridge_ip_0_GT_RX3_ch_rxchanbondseq,
            ch3_rxchanbondseq_ext => NLW_gt_bridge_ip_0_ch3_rxchanbondseq_ext_UNCONNECTED,
            ch3_rxchanisaligned => gt_bridge_ip_0_GT_RX3_ch_rxchanisaligned,
            ch3_rxchanisaligned_ext => NLW_gt_bridge_ip_0_ch3_rxchanisaligned_ext_UNCONNECTED,
            ch3_rxchanrealign => gt_bridge_ip_0_GT_RX3_ch_rxchanrealign,
            ch3_rxchanrealign_ext => NLW_gt_bridge_ip_0_ch3_rxchanrealign_ext_UNCONNECTED,
            ch3_rxchbondi(4 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxchbondi(4 downto 0),
            ch3_rxchbondi_ext(4 downto 0) => B"00000",
            ch3_rxchbondo(4 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxchbondo(4 downto 0),
            ch3_rxchbondo_ext(4 downto 0) => NLW_gt_bridge_ip_0_ch3_rxchbondo_ext_UNCONNECTED(4 downto 0),
            ch3_rxclkcorcnt(1 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxclkcorcnt(1 downto 0),
            ch3_rxclkcorcnt_ext(1 downto 0) => NLW_gt_bridge_ip_0_ch3_rxclkcorcnt_ext_UNCONNECTED(1 downto 0),
            ch3_rxcominitdet => gt_bridge_ip_0_GT_RX3_ch_rxcominitdet,
            ch3_rxcominitdet_ext => NLW_gt_bridge_ip_0_ch3_rxcominitdet_ext_UNCONNECTED,
            ch3_rxcommadet => gt_bridge_ip_0_GT_RX3_ch_rxcommadet,
            ch3_rxcommadet_ext => NLW_gt_bridge_ip_0_ch3_rxcommadet_ext_UNCONNECTED,
            ch3_rxcomsasdet => gt_bridge_ip_0_GT_RX3_ch_rxcomsasdet,
            ch3_rxcomsasdet_ext => NLW_gt_bridge_ip_0_ch3_rxcomsasdet_ext_UNCONNECTED,
            ch3_rxcomwakedet => gt_bridge_ip_0_GT_RX3_ch_rxcomwakedet,
            ch3_rxcomwakedet_ext => NLW_gt_bridge_ip_0_ch3_rxcomwakedet_ext_UNCONNECTED,
            ch3_rxctrl0(15 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxctrl0(15 downto 0),
            ch3_rxctrl0_ext(15 downto 0) => gt_bridge_ip_0_ch3_rxctrl0_ext(15 downto 0),
            ch3_rxctrl1(15 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxctrl1(15 downto 0),
            ch3_rxctrl1_ext(15 downto 0) => gt_bridge_ip_0_ch3_rxctrl1_ext(15 downto 0),
            ch3_rxctrl2(7 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxctrl2(7 downto 0),
            ch3_rxctrl2_ext(7 downto 0) => gt_bridge_ip_0_ch3_rxctrl2_ext(7 downto 0),
            ch3_rxctrl3(7 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxctrl3(7 downto 0),
            ch3_rxctrl3_ext(7 downto 0) => gt_bridge_ip_0_ch3_rxctrl3_ext(7 downto 0),
            ch3_rxdapicodeovrden => gt_bridge_ip_0_GT_RX3_ch_rxdapicodeovrden,
            ch3_rxdapicodeovrden_ext => '0',
            ch3_rxdapicodereset => gt_bridge_ip_0_GT_RX3_ch_rxdapicodereset,
            ch3_rxdapicodereset_ext => '0',
            ch3_rxdata(127 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxdata(127 downto 0),
            ch3_rxdata_ext(127 downto 0) => gt_bridge_ip_0_ch3_rxdata_ext(127 downto 0),
            ch3_rxdataextendrsvd(7 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxdataextendrsvd(7 downto 0),
            ch3_rxdataextendrsvd_ext(7 downto 0) => NLW_gt_bridge_ip_0_ch3_rxdataextendrsvd_ext_UNCONNECTED(7 downto 0),
            ch3_rxdatavalid(1 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxdatavalid(1 downto 0),
            ch3_rxdatavalid_ext(1 downto 0) => gt_bridge_ip_0_ch3_rxdatavalid_ext(1 downto 0),
            ch3_rxdccdone => gt_bridge_ip_0_GT_RX3_ch_rxdccdone,
            ch3_rxdccdone_ext => NLW_gt_bridge_ip_0_ch3_rxdccdone_ext_UNCONNECTED,
            ch3_rxdlyalignerr => gt_bridge_ip_0_GT_RX3_ch_rxdlyalignerr,
            ch3_rxdlyalignerr_ext => NLW_gt_bridge_ip_0_ch3_rxdlyalignerr_ext_UNCONNECTED,
            ch3_rxdlyalignprog => gt_bridge_ip_0_GT_RX3_ch_rxdlyalignprog,
            ch3_rxdlyalignprog_ext => NLW_gt_bridge_ip_0_ch3_rxdlyalignprog_ext_UNCONNECTED,
            ch3_rxdlyalignreq => gt_bridge_ip_0_GT_RX3_ch_rxdlyalignreq,
            ch3_rxdlyalignreq_ext => '0',
            ch3_rxelecidle => gt_bridge_ip_0_GT_RX3_ch_rxelecidle,
            ch3_rxelecidle_ext => NLW_gt_bridge_ip_0_ch3_rxelecidle_ext_UNCONNECTED,
            ch3_rxeqtraining => gt_bridge_ip_0_GT_RX3_ch_rxeqtraining,
            ch3_rxeqtraining_ext => '0',
            ch3_rxfinealigndone => gt_bridge_ip_0_GT_RX3_ch_rxfinealigndone,
            ch3_rxfinealigndone_ext => NLW_gt_bridge_ip_0_ch3_rxfinealigndone_ext_UNCONNECTED,
            ch3_rxgearboxslip => gt_bridge_ip_0_GT_RX3_ch_rxgearboxslip,
            ch3_rxgearboxslip_ext => ch3_rxgearboxslip_ext_0_1,
            ch3_rxheader(5 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxheader(5 downto 0),
            ch3_rxheader_ext(5 downto 0) => gt_bridge_ip_0_ch3_rxheader_ext(5 downto 0),
            ch3_rxheadervalid(1 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxheadervalid(1 downto 0),
            ch3_rxheadervalid_ext(1 downto 0) => gt_bridge_ip_0_ch3_rxheadervalid_ext(1 downto 0),
            ch3_rxlpmen => gt_bridge_ip_0_GT_RX3_ch_rxlpmen,
            ch3_rxlpmen_ext => '0',
            ch3_rxmldchaindone => gt_bridge_ip_0_GT_RX3_ch_rxmldchaindone,
            ch3_rxmldchaindone_ext => '0',
            ch3_rxmldchainreq => gt_bridge_ip_0_GT_RX3_ch_rxmldchainreq,
            ch3_rxmldchainreq_ext => '0',
            ch3_rxmlfinealignreq => gt_bridge_ip_0_GT_RX3_ch_rxmlfinealignreq,
            ch3_rxmlfinealignreq_ext => '0',
            ch3_rxmstdatapathreset => gt_bridge_ip_0_GT_RX3_ch_rxmstdatapathreset,
            ch3_rxmstreset => gt_bridge_ip_0_GT_RX3_ch_rxmstreset,
            ch3_rxmstresetdone => gt_bridge_ip_0_GT_RX3_ch_rxmstresetdone,
            ch3_rxmstresetdone_ext => NLW_gt_bridge_ip_0_ch3_rxmstresetdone_ext_UNCONNECTED,
            ch3_rxoobreset => gt_bridge_ip_0_GT_RX3_ch_rxoobreset,
            ch3_rxoobreset_ext => '0',
            ch3_rxosintdone => gt_bridge_ip_0_GT_RX3_ch_rxosintdone,
            ch3_rxosintdone_ext => NLW_gt_bridge_ip_0_ch3_rxosintdone_ext_UNCONNECTED,
            ch3_rxosintstarted => gt_bridge_ip_0_GT_RX3_ch_rxosintstarted,
            ch3_rxosintstarted_ext => NLW_gt_bridge_ip_0_ch3_rxosintstarted_ext_UNCONNECTED,
            ch3_rxosintstrobedone => gt_bridge_ip_0_GT_RX3_ch_rxosintstrobedone,
            ch3_rxosintstrobedone_ext => NLW_gt_bridge_ip_0_ch3_rxosintstrobedone_ext_UNCONNECTED,
            ch3_rxosintstrobestarted => gt_bridge_ip_0_GT_RX3_ch_rxosintstrobestarted,
            ch3_rxosintstrobestarted_ext => NLW_gt_bridge_ip_0_ch3_rxosintstrobestarted_ext_UNCONNECTED,
            ch3_rxpcsresetmask(4 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxpcsresetmask(4 downto 0),
            ch3_rxpcsresetmask_ext(4 downto 0) => B"11111",
            ch3_rxpd(1 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxpd(1 downto 0),
            ch3_rxpd_ext(1 downto 0) => B"00",
            ch3_rxphaligndone => gt_bridge_ip_0_GT_RX3_ch_rxphaligndone,
            ch3_rxphaligndone_ext => NLW_gt_bridge_ip_0_ch3_rxphaligndone_ext_UNCONNECTED,
            ch3_rxphalignerr => gt_bridge_ip_0_GT_RX3_ch_rxphalignerr,
            ch3_rxphalignerr_ext => NLW_gt_bridge_ip_0_ch3_rxphalignerr_ext_UNCONNECTED,
            ch3_rxphalignreq => gt_bridge_ip_0_GT_RX3_ch_rxphalignreq,
            ch3_rxphalignreq_ext => '0',
            ch3_rxphalignresetmask(1 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxphalignresetmask(1 downto 0),
            ch3_rxphalignresetmask_ext(1 downto 0) => B"11",
            ch3_rxphdlypd => gt_bridge_ip_0_GT_RX3_ch_rxphdlypd,
            ch3_rxphdlypd_ext => '0',
            ch3_rxphdlyreset => gt_bridge_ip_0_GT_RX3_ch_rxphdlyreset,
            ch3_rxphdlyreset_ext => '0',
            ch3_rxphdlyresetdone => gt_bridge_ip_0_GT_RX3_ch_rxphdlyresetdone,
            ch3_rxphdlyresetdone_ext => NLW_gt_bridge_ip_0_ch3_rxphdlyresetdone_ext_UNCONNECTED,
            ch3_rxphsetinitdone => gt_bridge_ip_0_GT_RX3_ch_rxphsetinitdone,
            ch3_rxphsetinitdone_ext => NLW_gt_bridge_ip_0_ch3_rxphsetinitdone_ext_UNCONNECTED,
            ch3_rxphsetinitreq => gt_bridge_ip_0_GT_RX3_ch_rxphsetinitreq,
            ch3_rxphsetinitreq_ext => '0',
            ch3_rxphshift180 => gt_bridge_ip_0_GT_RX3_ch_rxphshift180,
            ch3_rxphshift180_ext => '0',
            ch3_rxphshift180done => gt_bridge_ip_0_GT_RX3_ch_rxphshift180done,
            ch3_rxphshift180done_ext => NLW_gt_bridge_ip_0_ch3_rxphshift180done_ext_UNCONNECTED,
            ch3_rxpmaresetdone => gt_bridge_ip_0_GT_RX3_ch_rxpmaresetdone,
            ch3_rxpmaresetdone_ext => gt_bridge_ip_0_ch3_rxpmaresetdone_ext,
            ch3_rxpmaresetmask(6 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxpmaresetmask(6 downto 0),
            ch3_rxpmaresetmask_ext(6 downto 0) => B"1111111",
            ch3_rxpolarity => gt_bridge_ip_0_GT_RX3_ch_rxpolarity,
            ch3_rxpolarity_ext => ch3_rxpolarity_ext_0_1,
            ch3_rxprbscntreset => gt_bridge_ip_0_GT_RX3_ch_rxprbscntreset,
            ch3_rxprbscntreset_ext => '0',
            ch3_rxprbserr => gt_bridge_ip_0_GT_RX3_ch_rxprbserr,
            ch3_rxprbserr_ext => NLW_gt_bridge_ip_0_ch3_rxprbserr_ext_UNCONNECTED,
            ch3_rxprbslocked => gt_bridge_ip_0_GT_RX3_ch_rxprbslocked,
            ch3_rxprbslocked_ext => NLW_gt_bridge_ip_0_ch3_rxprbslocked_ext_UNCONNECTED,
            ch3_rxprbssel(3 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxprbssel(3 downto 0),
            ch3_rxprbssel_ext(3 downto 0) => B"0000",
            ch3_rxprogdivreset => gt_bridge_ip_0_GT_RX3_ch_rxprogdivreset,
            ch3_rxprogdivresetdone => gt_bridge_ip_0_GT_RX3_ch_rxprogdivresetdone,
            ch3_rxprogdivresetdone_ext => NLW_gt_bridge_ip_0_ch3_rxprogdivresetdone_ext_UNCONNECTED,
            ch3_rxrate(7 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxrate(7 downto 0),
            ch3_rxresetdone => gt_bridge_ip_0_GT_RX3_ch_rxresetdone,
            ch3_rxresetdone_ext => gt_bridge_ip_0_ch3_rxresetdone_ext,
            ch3_rxresetmode(1 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxresetmode(1 downto 0),
            ch3_rxresetmode_ext(1 downto 0) => B"00",
            ch3_rxslide => gt_bridge_ip_0_GT_RX3_ch_rxslide,
            ch3_rxslide_ext => ch3_rxslide_ext_0_1,
            ch3_rxsliderdy => gt_bridge_ip_0_GT_RX3_ch_rxsliderdy,
            ch3_rxsliderdy_ext => NLW_gt_bridge_ip_0_ch3_rxsliderdy_ext_UNCONNECTED,
            ch3_rxstartofseq(1 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxstartofseq(1 downto 0),
            ch3_rxstartofseq_ext(1 downto 0) => NLW_gt_bridge_ip_0_ch3_rxstartofseq_ext_UNCONNECTED(1 downto 0),
            ch3_rxstatus(2 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxstatus(2 downto 0),
            ch3_rxstatus_ext(2 downto 0) => NLW_gt_bridge_ip_0_ch3_rxstatus_ext_UNCONNECTED(2 downto 0),
            ch3_rxsyncallin => gt_bridge_ip_0_GT_RX3_ch_rxsyncallin,
            ch3_rxsyncallin_ext => '0',
            ch3_rxsyncdone => gt_bridge_ip_0_GT_RX3_ch_rxsyncdone,
            ch3_rxsyncdone_ext => NLW_gt_bridge_ip_0_ch3_rxsyncdone_ext_UNCONNECTED,
            ch3_rxtermination => gt_bridge_ip_0_GT_RX3_ch_rxtermination,
            ch3_rxtermination_ext => '0',
            ch3_rxuserrdy => gt_bridge_ip_0_GT_RX3_ch_rxuserrdy,
            ch3_rxvalid => gt_bridge_ip_0_GT_RX3_ch_rxvalid,
            ch3_rxvalid_ext => gt_bridge_ip_0_ch3_rxvalid_ext,
            ch3_tx10gstat => gt_bridge_ip_0_GT_TX3_ch_tx10gstat,
            ch3_tx10gstat_ext => NLW_gt_bridge_ip_0_ch3_tx10gstat_ext_UNCONNECTED,
            ch3_txbufstatus(1 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txbufstatus(1 downto 0),
            ch3_txbufstatus_ext(1 downto 0) => gt_bridge_ip_0_ch3_txbufstatus_ext(1 downto 0),
            ch3_txcomfinish => gt_bridge_ip_0_GT_TX3_ch_txcomfinish,
            ch3_txcomfinish_ext => NLW_gt_bridge_ip_0_ch3_txcomfinish_ext_UNCONNECTED,
            ch3_txcominit => gt_bridge_ip_0_GT_TX3_ch_txcominit,
            ch3_txcominit_ext => '0',
            ch3_txcomsas => gt_bridge_ip_0_GT_TX3_ch_txcomsas,
            ch3_txcomsas_ext => '0',
            ch3_txcomwake => gt_bridge_ip_0_GT_TX3_ch_txcomwake,
            ch3_txcomwake_ext => '0',
            ch3_txctrl0(15 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txctrl0(15 downto 0),
            ch3_txctrl0_ext(15 downto 0) => ch3_txctrl0_ext_0_1(15 downto 0),
            ch3_txctrl1(15 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txctrl1(15 downto 0),
            ch3_txctrl1_ext(15 downto 0) => ch3_txctrl1_ext_0_1(15 downto 0),
            ch3_txctrl2(7 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txctrl2(7 downto 0),
            ch3_txctrl2_ext(7 downto 0) => ch3_txctrl2_ext_0_1(7 downto 0),
            ch3_txdapicodeovrden => gt_bridge_ip_0_GT_TX3_ch_txdapicodeovrden,
            ch3_txdapicodeovrden_ext => '0',
            ch3_txdapicodereset => gt_bridge_ip_0_GT_TX3_ch_txdapicodereset,
            ch3_txdapicodereset_ext => '0',
            ch3_txdata(127 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txdata(127 downto 0),
            ch3_txdata_ext(127 downto 0) => ch3_txdata_ext_0_1(127 downto 0),
            ch3_txdataextendrsvd(7 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txdataextendrsvd(7 downto 0),
            ch3_txdataextendrsvd_ext(7 downto 0) => B"00000000",
            ch3_txdccdone => gt_bridge_ip_0_GT_TX3_ch_txdccdone,
            ch3_txdccdone_ext => NLW_gt_bridge_ip_0_ch3_txdccdone_ext_UNCONNECTED,
            ch3_txdeemph(1 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txdeemph(1 downto 0),
            ch3_txdeemph_ext(1 downto 0) => B"00",
            ch3_txdetectrx => gt_bridge_ip_0_GT_TX3_ch_txdetectrx,
            ch3_txdetectrx_ext => '0',
            ch3_txdiffctrl(4 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txdiffctrl(4 downto 0),
            ch3_txdiffctrl_ext(4 downto 0) => B"11001",
            ch3_txdlyalignerr => gt_bridge_ip_0_GT_TX3_ch_txdlyalignerr,
            ch3_txdlyalignerr_ext => NLW_gt_bridge_ip_0_ch3_txdlyalignerr_ext_UNCONNECTED,
            ch3_txdlyalignprog => gt_bridge_ip_0_GT_TX3_ch_txdlyalignprog,
            ch3_txdlyalignprog_ext => NLW_gt_bridge_ip_0_ch3_txdlyalignprog_ext_UNCONNECTED,
            ch3_txdlyalignreq => gt_bridge_ip_0_GT_TX3_ch_txdlyalignreq,
            ch3_txdlyalignreq_ext => '0',
            ch3_txelecidle => gt_bridge_ip_0_GT_TX3_ch_txelecidle,
            ch3_txelecidle_ext => '0',
            ch3_txheader(5 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txheader(5 downto 0),
            ch3_txheader_ext(5 downto 0) => B"000000",
            ch3_txinhibit => gt_bridge_ip_0_GT_TX3_ch_txinhibit,
            ch3_txinhibit_ext => '0',
            ch3_txmaincursor(6 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txmaincursor(6 downto 0),
            ch3_txmaincursor_ext(6 downto 0) => B"0000000",
            ch3_txmargin(2 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txmargin(2 downto 0),
            ch3_txmargin_ext(2 downto 0) => B"000",
            ch3_txmldchaindone => gt_bridge_ip_0_GT_TX3_ch_txmldchaindone,
            ch3_txmldchaindone_ext => '0',
            ch3_txmldchainreq => gt_bridge_ip_0_GT_TX3_ch_txmldchainreq,
            ch3_txmldchainreq_ext => '0',
            ch3_txmstdatapathreset => gt_bridge_ip_0_GT_TX3_ch_txmstdatapathreset,
            ch3_txmstreset => gt_bridge_ip_0_GT_TX3_ch_txmstreset,
            ch3_txmstresetdone => gt_bridge_ip_0_GT_TX3_ch_txmstresetdone,
            ch3_txmstresetdone_ext => NLW_gt_bridge_ip_0_ch3_txmstresetdone_ext_UNCONNECTED,
            ch3_txoneszeros => gt_bridge_ip_0_GT_TX3_ch_txoneszeros,
            ch3_txoneszeros_ext => '0',
            ch3_txpausedelayalign => gt_bridge_ip_0_GT_TX3_ch_txpausedelayalign,
            ch3_txpausedelayalign_ext => '0',
            ch3_txpcsresetmask => gt_bridge_ip_0_GT_TX3_ch_txpcsresetmask,
            ch3_txpcsresetmask_ext => '1',
            ch3_txpd(1 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txpd(1 downto 0),
            ch3_txpd_ext(1 downto 0) => B"00",
            ch3_txphaligndone => gt_bridge_ip_0_GT_TX3_ch_txphaligndone,
            ch3_txphaligndone_ext => NLW_gt_bridge_ip_0_ch3_txphaligndone_ext_UNCONNECTED,
            ch3_txphalignerr => gt_bridge_ip_0_GT_TX3_ch_txphalignerr,
            ch3_txphalignerr_ext => NLW_gt_bridge_ip_0_ch3_txphalignerr_ext_UNCONNECTED,
            ch3_txphalignoutrsvd => gt_bridge_ip_0_GT_TX3_ch_txphalignoutrsvd,
            ch3_txphalignoutrsvd_ext => NLW_gt_bridge_ip_0_ch3_txphalignoutrsvd_ext_UNCONNECTED,
            ch3_txphalignreq => gt_bridge_ip_0_GT_TX3_ch_txphalignreq,
            ch3_txphalignreq_ext => '0',
            ch3_txphalignresetmask(1 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txphalignresetmask(1 downto 0),
            ch3_txphalignresetmask_ext(1 downto 0) => B"11",
            ch3_txphdlypd => gt_bridge_ip_0_GT_TX3_ch_txphdlypd,
            ch3_txphdlypd_ext => '0',
            ch3_txphdlyreset => gt_bridge_ip_0_GT_TX3_ch_txphdlyreset,
            ch3_txphdlyreset_ext => '0',
            ch3_txphdlyresetdone => gt_bridge_ip_0_GT_TX3_ch_txphdlyresetdone,
            ch3_txphdlyresetdone_ext => NLW_gt_bridge_ip_0_ch3_txphdlyresetdone_ext_UNCONNECTED,
            ch3_txphsetinitdone => gt_bridge_ip_0_GT_TX3_ch_txphsetinitdone,
            ch3_txphsetinitdone_ext => NLW_gt_bridge_ip_0_ch3_txphsetinitdone_ext_UNCONNECTED,
            ch3_txphsetinitreq => gt_bridge_ip_0_GT_TX3_ch_txphsetinitreq,
            ch3_txphsetinitreq_ext => '0',
            ch3_txphshift180 => gt_bridge_ip_0_GT_TX3_ch_txphshift180,
            ch3_txphshift180_ext => '0',
            ch3_txphshift180done => gt_bridge_ip_0_GT_TX3_ch_txphshift180done,
            ch3_txphshift180done_ext => NLW_gt_bridge_ip_0_ch3_txphshift180done_ext_UNCONNECTED,
            ch3_txpicodeovrden => gt_bridge_ip_0_GT_TX3_ch_txpicodeovrden,
            ch3_txpicodeovrden_ext => '0',
            ch3_txpicodereset => gt_bridge_ip_0_GT_TX3_ch_txpicodereset,
            ch3_txpicodereset_ext => '0',
            ch3_txpippmen => gt_bridge_ip_0_GT_TX3_ch_txpippmen,
            ch3_txpippmen_ext => ch3_txpippmen_ext_0_1,
            ch3_txpippmstepsize(4 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txpippmstepsize(4 downto 0),
            ch3_txpippmstepsize_ext(4 downto 0) => ch3_txpippmstepsize_ext_0_1(4 downto 0),
            ch3_txpisopd => gt_bridge_ip_0_GT_TX3_ch_txpisopd,
            ch3_txpisopd_ext => '0',
            ch3_txpmaresetdone => gt_bridge_ip_0_GT_TX3_ch_txpmaresetdone,
            ch3_txpmaresetdone_ext => NLW_gt_bridge_ip_0_ch3_txpmaresetdone_ext_UNCONNECTED,
            ch3_txpmaresetmask(2 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txpmaresetmask(2 downto 0),
            ch3_txpmaresetmask_ext(2 downto 0) => B"111",
            ch3_txpolarity => gt_bridge_ip_0_GT_TX3_ch_txpolarity,
            ch3_txpolarity_ext => ch3_txpolarity_ext_0_1,
            ch3_txpostcursor(4 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txpostcursor(4 downto 0),
            ch3_txpostcursor_ext(4 downto 0) => B"00000",
            ch3_txprbsforceerr => gt_bridge_ip_0_GT_TX3_ch_txprbsforceerr,
            ch3_txprbsforceerr_ext => '0',
            ch3_txprbssel(3 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txprbssel(3 downto 0),
            ch3_txprbssel_ext(3 downto 0) => B"0000",
            ch3_txprecursor(4 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txprecursor(4 downto 0),
            ch3_txprecursor_ext(4 downto 0) => B"00000",
            ch3_txprogdivreset => gt_bridge_ip_0_GT_TX3_ch_txprogdivreset,
            ch3_txprogdivresetdone => gt_bridge_ip_0_GT_TX3_ch_txprogdivresetdone,
            ch3_txprogdivresetdone_ext => NLW_gt_bridge_ip_0_ch3_txprogdivresetdone_ext_UNCONNECTED,
            ch3_txrate(7 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txrate(7 downto 0),
            ch3_txresetdone => gt_bridge_ip_0_GT_TX3_ch_txresetdone,
            ch3_txresetdone_ext => gt_bridge_ip_0_ch3_txresetdone_ext,
            ch3_txresetmode(1 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txresetmode(1 downto 0),
            ch3_txresetmode_ext(1 downto 0) => B"00",
            ch3_txsequence(6 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txsequence(6 downto 0),
            ch3_txsequence_ext(6 downto 0) => ch3_txsequence_ext_0_1(6 downto 0),
            ch3_txswing => gt_bridge_ip_0_GT_TX3_ch_txswing,
            ch3_txswing_ext => '0',
            ch3_txsyncallin => gt_bridge_ip_0_GT_TX3_ch_txsyncallin,
            ch3_txsyncallin_ext => '0',
            ch3_txsyncdone => gt_bridge_ip_0_GT_TX3_ch_txsyncdone,
            ch3_txsyncdone_ext => NLW_gt_bridge_ip_0_ch3_txsyncdone_ext_UNCONNECTED,
            ch3_txuserrdy => gt_bridge_ip_0_GT_TX3_ch_txuserrdy,
            ch_phystatus_in(3 downto 0) => xlconcat_0_dout(3 downto 0),
            gpi_out => gt_bridge_ip_0_gpi_out,
            gpio_enable => '0',
            gpo_in => url_gpo_Res,
            gt_ilo_reset => gt_bridge_ip_0_gt_ilo_reset,
            gt_lcpll_lock => gt_quad_base_hsclk0_lcplllock,
            gt_pll_reset => gt_bridge_ip_0_gt_pll_reset,
            gt_rpll_lock => gt_quad_base_hsclk0_rplllock,
            gt_rxusrclk => bufg_gt_usrclk,
            gt_txusrclk => bufg_gt_1_usrclk,
            gtpowergood => urlp_Res,
            gtreset_in => gt_reset_gt_bridge_ip_0_1,
            ilo_resetdone => util_reduced_logic_0_Res,
            lcpll_lock_out => gt_bridge_ip_0_lcpll_lock_out,
            link_status_out => gt_bridge_ip_0_link_status_out,
            pcie_rstb => NLW_gt_bridge_ip_0_pcie_rstb_UNCONNECTED,
            rate_sel(3 downto 0) => rate_sel_gt_bridge_ip_0_1(3 downto 0),
            reset_mask(1 downto 0) => NLW_gt_bridge_ip_0_reset_mask_UNCONNECTED(1 downto 0),
            reset_rx_datapath_in => reset_rx_datapath_in_0_1,
            reset_rx_pll_and_datapath_in => reset_rx_pll_and_datapath_in_0_1,
            reset_tx_datapath_in => reset_tx_datapath_in_0_1,
            reset_tx_pll_and_datapath_in => reset_tx_pll_and_datapath_in_0_1,
            rpll_lock_out => gt_bridge_ip_0_rpll_lock_out,
            rx_clr_out => NLW_gt_bridge_ip_0_rx_clr_out_UNCONNECTED,
            rx_clrb_leaf_out => NLW_gt_bridge_ip_0_rx_clrb_leaf_out_UNCONNECTED,
            rx_resetdone_out => gt_bridge_ip_0_rx_resetdone_out,
            rxusrclk_out => gt_bridge_ip_0_rxusrclk_out,
            tx_clr_out => NLW_gt_bridge_ip_0_tx_clr_out_UNCONNECTED,
            tx_clrb_leaf_out => NLW_gt_bridge_ip_0_tx_clrb_leaf_out_UNCONNECTED,
            tx_resetdone_out => gt_bridge_ip_0_tx_resetdone_out,
            txusrclk_out => gt_bridge_ip_0_txusrclk_out
        );
    gt_quad_base: component transceiver_versal_lpgbt_gt_quad_base_0
        port map (
            GT_REFCLK0 => util_ds_buf_IBUF_OUT(0),
            altclk => '0',
            apb3clk => apb3clk_quad_1,
            apb3paddr(15 downto 0) => APB3_INTF_0_1_PADDR(15 downto 0),
            apb3penable => APB3_INTF_0_1_PENABLE,
            apb3prdata(31 downto 0) => APB3_INTF_0_1_PRDATA(31 downto 0),
            apb3pready => APB3_INTF_0_1_PREADY,
            apb3presetn => '1',
            apb3psel => APB3_INTF_0_1_PSEL,
            apb3pslverr => APB3_INTF_0_1_PSLVERR,
            apb3pwdata(31 downto 0) => APB3_INTF_0_1_PWDATA(31 downto 0),
            apb3pwrite => APB3_INTF_0_1_PWRITE,
            bgbypassb => '0',
            bgmonitorenb => '0',
            bgpdb => '0',
            bgrcalovrd(4 downto 0) => B"00000",
            bgrcalovrdenb => '0',
            ch0_bufgtce => NLW_gt_quad_base_ch0_bufgtce_UNCONNECTED,
            ch0_bufgtcemask(3 downto 0) => NLW_gt_quad_base_ch0_bufgtcemask_UNCONNECTED(3 downto 0),
            ch0_bufgtdiv(11 downto 0) => NLW_gt_quad_base_ch0_bufgtdiv_UNCONNECTED(11 downto 0),
            ch0_bufgtrst => NLW_gt_quad_base_ch0_bufgtrst_UNCONNECTED,
            ch0_bufgtrstmask(3 downto 0) => NLW_gt_quad_base_ch0_bufgtrstmask_UNCONNECTED(3 downto 0),
            ch0_cdrbmcdrreq => gt_bridge_ip_0_GT_RX0_ch_cdrbmcdrreq,
            ch0_cdrfreqos => gt_bridge_ip_0_GT_RX0_ch_cdrfreqos,
            ch0_cdrincpctrl => gt_bridge_ip_0_GT_RX0_ch_cdrincpctrl,
            ch0_cdrstepdir => gt_bridge_ip_0_GT_RX0_ch_cdrstepdir,
            ch0_cdrstepsq => gt_bridge_ip_0_GT_RX0_ch_cdrstepsq,
            ch0_cdrstepsx => gt_bridge_ip_0_GT_RX0_ch_cdrstepsx,
            ch0_cfokovrdfinish => gt_bridge_ip_0_GT_RX0_ch_cfokovrdfinish,
            ch0_cfokovrdpulse => gt_bridge_ip_0_GT_RX0_ch_cfokovrdpulse,
            ch0_cfokovrdrdy0 => gt_bridge_ip_0_GT_RX0_ch_cfokovrdrdy0,
            ch0_cfokovrdrdy1 => gt_bridge_ip_0_GT_RX0_ch_cfokovrdrdy1,
            ch0_cfokovrdstart => gt_bridge_ip_0_GT_RX0_ch_cfokovrdstart,
            ch0_clkrsvd0 => '0',
            ch0_clkrsvd1 => '0',
            ch0_dmonfiforeset => '0',
            ch0_dmonitorclk => '0',
            ch0_dmonitorout(31 downto 0) => NLW_gt_quad_base_ch0_dmonitorout_UNCONNECTED(31 downto 0),
            ch0_dmonitoroutclk => NLW_gt_quad_base_ch0_dmonitoroutclk_UNCONNECTED,
            ch0_eyescandataerror => gt_bridge_ip_0_GT_RX0_ch_eyescandataerror,
            ch0_eyescanreset => gt_bridge_ip_0_GT_RX0_ch_eyescanreset,
            ch0_eyescantrigger => gt_bridge_ip_0_GT_RX0_ch_eyescantrigger,
            ch0_gtrsvd(15 downto 0) => B"0000000000000000",
            ch0_gtrxreset => gt_bridge_ip_0_GT_RX0_ch_gtrxreset,
            ch0_gttxreset => gt_bridge_ip_0_GT_TX0_ch_gttxreset,
            ch0_hsdppcsreset => '0',
            ch0_iloreset => gt_bridge_ip_0_gt_ilo_reset,
            ch0_iloresetdone => gt_quad_base_ch0_iloresetdone,
            ch0_iloresetmask => '1',
            ch0_loopback(2 downto 0) => ch0_loopback_0_1(2 downto 0),
            ch0_pcierstb => '1',
            ch0_pcsrsvdin(15 downto 0) => B"0000001001000000",
            ch0_pcsrsvdout(15 downto 0) => NLW_gt_quad_base_ch0_pcsrsvdout_UNCONNECTED(15 downto 0),
            ch0_phyesmadaptsave => '0',
            ch0_phyready => NLW_gt_quad_base_ch0_phyready_UNCONNECTED,
            ch0_phystatus => gt_quad_base_ch0_phystatus,
            ch0_pinrsvdas(15 downto 0) => NLW_gt_quad_base_ch0_pinrsvdas_UNCONNECTED(15 downto 0),
            ch0_resetexception => NLW_gt_quad_base_ch0_resetexception_UNCONNECTED,
            ch0_rx10gstat(7 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rx10gstat(7 downto 0),
            ch0_rxbufstatus(2 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxbufstatus(2 downto 0),
            ch0_rxbyteisaligned => gt_bridge_ip_0_GT_RX0_ch_rxbyteisaligned,
            ch0_rxbyterealign => gt_bridge_ip_0_GT_RX0_ch_rxbyterealign,
            ch0_rxcdrhold => gt_bridge_ip_0_GT_RX0_ch_rxcdrhold,
            ch0_rxcdrlock => gt_bridge_ip_0_GT_RX0_ch_rxcdrlock,
            ch0_rxcdrovrden => gt_bridge_ip_0_GT_RX0_ch_rxcdrovrden,
            ch0_rxcdrphdone => gt_bridge_ip_0_GT_RX0_ch_rxcdrphdone,
            ch0_rxcdrreset => gt_bridge_ip_0_GT_RX0_ch_rxcdrreset,
            ch0_rxchanbondseq => gt_bridge_ip_0_GT_RX0_ch_rxchanbondseq,
            ch0_rxchanisaligned => gt_bridge_ip_0_GT_RX0_ch_rxchanisaligned,
            ch0_rxchanrealign => gt_bridge_ip_0_GT_RX0_ch_rxchanrealign,
            ch0_rxchbondi(4 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxchbondi(4 downto 0),
            ch0_rxchbondo(4 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxchbondo(4 downto 0),
            ch0_rxclkcorcnt(1 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxclkcorcnt(1 downto 0),
            ch0_rxcominitdet => gt_bridge_ip_0_GT_RX0_ch_rxcominitdet,
            ch0_rxcommadet => gt_bridge_ip_0_GT_RX0_ch_rxcommadet,
            ch0_rxcomsasdet => gt_bridge_ip_0_GT_RX0_ch_rxcomsasdet,
            ch0_rxcomwakedet => gt_bridge_ip_0_GT_RX0_ch_rxcomwakedet,
            ch0_rxctrl0(15 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxctrl0(15 downto 0),
            ch0_rxctrl1(15 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxctrl1(15 downto 0),
            ch0_rxctrl2(7 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxctrl2(7 downto 0),
            ch0_rxctrl3(7 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxctrl3(7 downto 0),
            ch0_rxdapicodeovrden => gt_bridge_ip_0_GT_RX0_ch_rxdapicodeovrden,
            ch0_rxdapicodereset => gt_bridge_ip_0_GT_RX0_ch_rxdapicodereset,
            ch0_rxdata(127 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxdata(127 downto 0),
            ch0_rxdataextendrsvd(7 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxdataextendrsvd(7 downto 0),
            ch0_rxdatavalid(1 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxdatavalid(1 downto 0),
            ch0_rxdccdone => gt_bridge_ip_0_GT_RX0_ch_rxdccdone,
            ch0_rxdlyalignerr => gt_bridge_ip_0_GT_RX0_ch_rxdlyalignerr,
            ch0_rxdlyalignprog => gt_bridge_ip_0_GT_RX0_ch_rxdlyalignprog,
            ch0_rxdlyalignreq => gt_bridge_ip_0_GT_RX0_ch_rxdlyalignreq,
            ch0_rxelecidle => gt_bridge_ip_0_GT_RX0_ch_rxelecidle,
            ch0_rxeqtraining => gt_bridge_ip_0_GT_RX0_ch_rxeqtraining,
            ch0_rxfinealigndone => gt_bridge_ip_0_GT_RX0_ch_rxfinealigndone,
            ch0_rxgearboxslip => gt_bridge_ip_0_GT_RX0_ch_rxgearboxslip,
            ch0_rxheader(5 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxheader(5 downto 0),
            ch0_rxheadervalid(1 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxheadervalid(1 downto 0),
            ch0_rxlatclk => '0',
            ch0_rxlpmen => gt_bridge_ip_0_GT_RX0_ch_rxlpmen,
            ch0_rxmldchaindone => gt_bridge_ip_0_GT_RX0_ch_rxmldchaindone,
            ch0_rxmldchainreq => gt_bridge_ip_0_GT_RX0_ch_rxmldchainreq,
            ch0_rxmlfinealignreq => gt_bridge_ip_0_GT_RX0_ch_rxmlfinealignreq,
            ch0_rxmstdatapathreset => gt_bridge_ip_0_GT_RX0_ch_rxmstdatapathreset,
            ch0_rxmstreset => gt_bridge_ip_0_GT_RX0_ch_rxmstreset,
            ch0_rxmstresetdone => gt_bridge_ip_0_GT_RX0_ch_rxmstresetdone,
            ch0_rxoobreset => gt_bridge_ip_0_GT_RX0_ch_rxoobreset,
            ch0_rxosintdone => gt_bridge_ip_0_GT_RX0_ch_rxosintdone,
            ch0_rxosintstarted => gt_bridge_ip_0_GT_RX0_ch_rxosintstarted,
            ch0_rxosintstrobedone => gt_bridge_ip_0_GT_RX0_ch_rxosintstrobedone,
            ch0_rxosintstrobestarted => gt_bridge_ip_0_GT_RX0_ch_rxosintstrobestarted,
            ch0_rxoutclk => gt_quad_base_ch0_rxoutclk,
            ch0_rxpcsresetmask(4 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxpcsresetmask(4 downto 0),
            ch0_rxpd(1 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxpd(1 downto 0),
            ch0_rxphaligndone => gt_bridge_ip_0_GT_RX0_ch_rxphaligndone,
            ch0_rxphalignerr => gt_bridge_ip_0_GT_RX0_ch_rxphalignerr,
            ch0_rxphalignreq => gt_bridge_ip_0_GT_RX0_ch_rxphalignreq,
            ch0_rxphalignresetmask(1 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxphalignresetmask(1 downto 0),
            ch0_rxphdlypd => gt_bridge_ip_0_GT_RX0_ch_rxphdlypd,
            ch0_rxphdlyreset => gt_bridge_ip_0_GT_RX0_ch_rxphdlyreset,
            ch0_rxphdlyresetdone => gt_bridge_ip_0_GT_RX0_ch_rxphdlyresetdone,
            ch0_rxphsetinitdone => gt_bridge_ip_0_GT_RX0_ch_rxphsetinitdone,
            ch0_rxphsetinitreq => gt_bridge_ip_0_GT_RX0_ch_rxphsetinitreq,
            ch0_rxphshift180 => gt_bridge_ip_0_GT_RX0_ch_rxphshift180,
            ch0_rxphshift180done => gt_bridge_ip_0_GT_RX0_ch_rxphshift180done,
            ch0_rxpmaresetdone => gt_bridge_ip_0_GT_RX0_ch_rxpmaresetdone,
            ch0_rxpmaresetmask(6 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxpmaresetmask(6 downto 0),
            ch0_rxpolarity => gt_bridge_ip_0_GT_RX0_ch_rxpolarity,
            ch0_rxprbscntreset => gt_bridge_ip_0_GT_RX0_ch_rxprbscntreset,
            ch0_rxprbserr => gt_bridge_ip_0_GT_RX0_ch_rxprbserr,
            ch0_rxprbslocked => gt_bridge_ip_0_GT_RX0_ch_rxprbslocked,
            ch0_rxprbssel(3 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxprbssel(3 downto 0),
            ch0_rxprogdivreset => gt_bridge_ip_0_GT_RX0_ch_rxprogdivreset,
            ch0_rxprogdivresetdone => gt_bridge_ip_0_GT_RX0_ch_rxprogdivresetdone,
            ch0_rxrate(7 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxrate(7 downto 0),
            ch0_rxresetdone => gt_bridge_ip_0_GT_RX0_ch_rxresetdone,
            ch0_rxresetmode(1 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxresetmode(1 downto 0),
            ch0_rxslide => gt_bridge_ip_0_GT_RX0_ch_rxslide,
            ch0_rxsliderdy => gt_bridge_ip_0_GT_RX0_ch_rxsliderdy,
            ch0_rxstartofseq(1 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxstartofseq(1 downto 0),
            ch0_rxstatus(2 downto 0) => gt_bridge_ip_0_GT_RX0_ch_rxstatus(2 downto 0),
            ch0_rxsyncallin => gt_bridge_ip_0_GT_RX0_ch_rxsyncallin,
            ch0_rxsyncdone => gt_bridge_ip_0_GT_RX0_ch_rxsyncdone,
            ch0_rxtermination => gt_bridge_ip_0_GT_RX0_ch_rxtermination,
            ch0_rxuserrdy => gt_bridge_ip_0_GT_RX0_ch_rxuserrdy,
            ch0_rxusrclk => bufg_gt_usrclk,
            ch0_rxvalid => gt_bridge_ip_0_GT_RX0_ch_rxvalid,
            ch0_tstin(19 downto 0) => B"00000000000000000000",
            ch0_tx10gstat => gt_bridge_ip_0_GT_TX0_ch_tx10gstat,
            ch0_txbufstatus(1 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txbufstatus(1 downto 0),
            ch0_txcomfinish => gt_bridge_ip_0_GT_TX0_ch_txcomfinish,
            ch0_txcominit => gt_bridge_ip_0_GT_TX0_ch_txcominit,
            ch0_txcomsas => gt_bridge_ip_0_GT_TX0_ch_txcomsas,
            ch0_txcomwake => gt_bridge_ip_0_GT_TX0_ch_txcomwake,
            ch0_txctrl0(15 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txctrl0(15 downto 0),
            ch0_txctrl1(15 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txctrl1(15 downto 0),
            ch0_txctrl2(7 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txctrl2(7 downto 0),
            ch0_txdapicodeovrden => gt_bridge_ip_0_GT_TX0_ch_txdapicodeovrden,
            ch0_txdapicodereset => gt_bridge_ip_0_GT_TX0_ch_txdapicodereset,
            ch0_txdata(127 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txdata(127 downto 0),
            ch0_txdataextendrsvd(7 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txdataextendrsvd(7 downto 0),
            ch0_txdccdone => gt_bridge_ip_0_GT_TX0_ch_txdccdone,
            ch0_txdeemph(1 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txdeemph(1 downto 0),
            ch0_txdetectrx => gt_bridge_ip_0_GT_TX0_ch_txdetectrx,
            ch0_txdiffctrl(4 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txdiffctrl(4 downto 0),
            ch0_txdlyalignerr => gt_bridge_ip_0_GT_TX0_ch_txdlyalignerr,
            ch0_txdlyalignprog => gt_bridge_ip_0_GT_TX0_ch_txdlyalignprog,
            ch0_txdlyalignreq => gt_bridge_ip_0_GT_TX0_ch_txdlyalignreq,
            ch0_txelecidle => gt_bridge_ip_0_GT_TX0_ch_txelecidle,
            ch0_txheader(5 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txheader(5 downto 0),
            ch0_txinhibit => gt_bridge_ip_0_GT_TX0_ch_txinhibit,
            ch0_txlatclk => '0',
            ch0_txmaincursor(6 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txmaincursor(6 downto 0),
            ch0_txmargin(2 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txmargin(2 downto 0),
            ch0_txmldchaindone => gt_bridge_ip_0_GT_TX0_ch_txmldchaindone,
            ch0_txmldchainreq => gt_bridge_ip_0_GT_TX0_ch_txmldchainreq,
            ch0_txmstdatapathreset => gt_bridge_ip_0_GT_TX0_ch_txmstdatapathreset,
            ch0_txmstreset => gt_bridge_ip_0_GT_TX0_ch_txmstreset,
            ch0_txmstresetdone => gt_bridge_ip_0_GT_TX0_ch_txmstresetdone,
            ch0_txoneszeros => gt_bridge_ip_0_GT_TX0_ch_txoneszeros,
            ch0_txoutclk => gt_quad_base_ch0_txoutclk,
            ch0_txpausedelayalign => gt_bridge_ip_0_GT_TX0_ch_txpausedelayalign,
            ch0_txpcsresetmask => gt_bridge_ip_0_GT_TX0_ch_txpcsresetmask,
            ch0_txpd(1 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txpd(1 downto 0),
            ch0_txphaligndone => gt_bridge_ip_0_GT_TX0_ch_txphaligndone,
            ch0_txphalignerr => gt_bridge_ip_0_GT_TX0_ch_txphalignerr,
            ch0_txphalignoutrsvd => gt_bridge_ip_0_GT_TX0_ch_txphalignoutrsvd,
            ch0_txphalignreq => gt_bridge_ip_0_GT_TX0_ch_txphalignreq,
            ch0_txphalignresetmask(1 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txphalignresetmask(1 downto 0),
            ch0_txphdlypd => gt_bridge_ip_0_GT_TX0_ch_txphdlypd,
            ch0_txphdlyreset => gt_bridge_ip_0_GT_TX0_ch_txphdlyreset,
            ch0_txphdlyresetdone => gt_bridge_ip_0_GT_TX0_ch_txphdlyresetdone,
            ch0_txphdlytstclk => '0',
            ch0_txphsetinitdone => gt_bridge_ip_0_GT_TX0_ch_txphsetinitdone,
            ch0_txphsetinitreq => gt_bridge_ip_0_GT_TX0_ch_txphsetinitreq,
            ch0_txphshift180 => gt_bridge_ip_0_GT_TX0_ch_txphshift180,
            ch0_txphshift180done => gt_bridge_ip_0_GT_TX0_ch_txphshift180done,
            ch0_txpicodeovrden => gt_bridge_ip_0_GT_TX0_ch_txpicodeovrden,
            ch0_txpicodereset => gt_bridge_ip_0_GT_TX0_ch_txpicodereset,
            ch0_txpippmen => gt_bridge_ip_0_GT_TX0_ch_txpippmen,
            ch0_txpippmstepsize(4 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txpippmstepsize(4 downto 0),
            ch0_txpisopd => gt_bridge_ip_0_GT_TX0_ch_txpisopd,
            ch0_txpmaresetdone => gt_bridge_ip_0_GT_TX0_ch_txpmaresetdone,
            ch0_txpmaresetmask(2 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txpmaresetmask(2 downto 0),
            ch0_txpolarity => gt_bridge_ip_0_GT_TX0_ch_txpolarity,
            ch0_txpostcursor(4 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txpostcursor(4 downto 0),
            ch0_txprbsforceerr => gt_bridge_ip_0_GT_TX0_ch_txprbsforceerr,
            ch0_txprbssel(3 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txprbssel(3 downto 0),
            ch0_txprecursor(4 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txprecursor(4 downto 0),
            ch0_txprogdivreset => gt_bridge_ip_0_GT_TX0_ch_txprogdivreset,
            ch0_txprogdivresetdone => gt_bridge_ip_0_GT_TX0_ch_txprogdivresetdone,
            ch0_txrate(7 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txrate(7 downto 0),
            ch0_txresetdone => gt_bridge_ip_0_GT_TX0_ch_txresetdone,
            ch0_txresetmode(1 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txresetmode(1 downto 0),
            ch0_txsequence(6 downto 0) => gt_bridge_ip_0_GT_TX0_ch_txsequence(6 downto 0),
            ch0_txswing => gt_bridge_ip_0_GT_TX0_ch_txswing,
            ch0_txsyncallin => gt_bridge_ip_0_GT_TX0_ch_txsyncallin,
            ch0_txsyncdone => gt_bridge_ip_0_GT_TX0_ch_txsyncdone,
            ch0_txuserrdy => gt_bridge_ip_0_GT_TX0_ch_txuserrdy,
            ch0_txusrclk => bufg_gt_1_usrclk,
            ch1_bufgtce => NLW_gt_quad_base_ch1_bufgtce_UNCONNECTED,
            ch1_bufgtcemask(3 downto 0) => NLW_gt_quad_base_ch1_bufgtcemask_UNCONNECTED(3 downto 0),
            ch1_bufgtdiv(11 downto 0) => NLW_gt_quad_base_ch1_bufgtdiv_UNCONNECTED(11 downto 0),
            ch1_bufgtrst => NLW_gt_quad_base_ch1_bufgtrst_UNCONNECTED,
            ch1_bufgtrstmask(3 downto 0) => NLW_gt_quad_base_ch1_bufgtrstmask_UNCONNECTED(3 downto 0),
            ch1_cdrbmcdrreq => gt_bridge_ip_0_GT_RX1_ch_cdrbmcdrreq,
            ch1_cdrfreqos => gt_bridge_ip_0_GT_RX1_ch_cdrfreqos,
            ch1_cdrincpctrl => gt_bridge_ip_0_GT_RX1_ch_cdrincpctrl,
            ch1_cdrstepdir => gt_bridge_ip_0_GT_RX1_ch_cdrstepdir,
            ch1_cdrstepsq => gt_bridge_ip_0_GT_RX1_ch_cdrstepsq,
            ch1_cdrstepsx => gt_bridge_ip_0_GT_RX1_ch_cdrstepsx,
            ch1_cfokovrdfinish => gt_bridge_ip_0_GT_RX1_ch_cfokovrdfinish,
            ch1_cfokovrdpulse => gt_bridge_ip_0_GT_RX1_ch_cfokovrdpulse,
            ch1_cfokovrdrdy0 => gt_bridge_ip_0_GT_RX1_ch_cfokovrdrdy0,
            ch1_cfokovrdrdy1 => gt_bridge_ip_0_GT_RX1_ch_cfokovrdrdy1,
            ch1_cfokovrdstart => gt_bridge_ip_0_GT_RX1_ch_cfokovrdstart,
            ch1_clkrsvd0 => '0',
            ch1_clkrsvd1 => '0',
            ch1_dmonfiforeset => '0',
            ch1_dmonitorclk => '0',
            ch1_dmonitorout(31 downto 0) => NLW_gt_quad_base_ch1_dmonitorout_UNCONNECTED(31 downto 0),
            ch1_dmonitoroutclk => NLW_gt_quad_base_ch1_dmonitoroutclk_UNCONNECTED,
            ch1_eyescandataerror => gt_bridge_ip_0_GT_RX1_ch_eyescandataerror,
            ch1_eyescanreset => gt_bridge_ip_0_GT_RX1_ch_eyescanreset,
            ch1_eyescantrigger => gt_bridge_ip_0_GT_RX1_ch_eyescantrigger,
            ch1_gtrsvd(15 downto 0) => B"0000000000000000",
            ch1_gtrxreset => gt_bridge_ip_0_GT_RX1_ch_gtrxreset,
            ch1_gttxreset => gt_bridge_ip_0_GT_TX1_ch_gttxreset,
            ch1_hsdppcsreset => '0',
            ch1_iloreset => gt_bridge_ip_0_gt_ilo_reset,
            ch1_iloresetdone => gt_quad_base_ch1_iloresetdone,
            ch1_iloresetmask => '1',
            ch1_loopback(2 downto 0) => ch1_loopback_0_1(2 downto 0),
            ch1_pcierstb => '1',
            ch1_pcsrsvdin(15 downto 0) => B"0000001001000000",
            ch1_pcsrsvdout(15 downto 0) => NLW_gt_quad_base_ch1_pcsrsvdout_UNCONNECTED(15 downto 0),
            ch1_phyesmadaptsave => '0',
            ch1_phyready => NLW_gt_quad_base_ch1_phyready_UNCONNECTED,
            ch1_phystatus => gt_quad_base_ch1_phystatus,
            ch1_pinrsvdas(15 downto 0) => NLW_gt_quad_base_ch1_pinrsvdas_UNCONNECTED(15 downto 0),
            ch1_resetexception => NLW_gt_quad_base_ch1_resetexception_UNCONNECTED,
            ch1_rx10gstat(7 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rx10gstat(7 downto 0),
            ch1_rxbufstatus(2 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxbufstatus(2 downto 0),
            ch1_rxbyteisaligned => gt_bridge_ip_0_GT_RX1_ch_rxbyteisaligned,
            ch1_rxbyterealign => gt_bridge_ip_0_GT_RX1_ch_rxbyterealign,
            ch1_rxcdrhold => gt_bridge_ip_0_GT_RX1_ch_rxcdrhold,
            ch1_rxcdrlock => gt_bridge_ip_0_GT_RX1_ch_rxcdrlock,
            ch1_rxcdrovrden => gt_bridge_ip_0_GT_RX1_ch_rxcdrovrden,
            ch1_rxcdrphdone => gt_bridge_ip_0_GT_RX1_ch_rxcdrphdone,
            ch1_rxcdrreset => gt_bridge_ip_0_GT_RX1_ch_rxcdrreset,
            ch1_rxchanbondseq => gt_bridge_ip_0_GT_RX1_ch_rxchanbondseq,
            ch1_rxchanisaligned => gt_bridge_ip_0_GT_RX1_ch_rxchanisaligned,
            ch1_rxchanrealign => gt_bridge_ip_0_GT_RX1_ch_rxchanrealign,
            ch1_rxchbondi(4 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxchbondi(4 downto 0),
            ch1_rxchbondo(4 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxchbondo(4 downto 0),
            ch1_rxclkcorcnt(1 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxclkcorcnt(1 downto 0),
            ch1_rxcominitdet => gt_bridge_ip_0_GT_RX1_ch_rxcominitdet,
            ch1_rxcommadet => gt_bridge_ip_0_GT_RX1_ch_rxcommadet,
            ch1_rxcomsasdet => gt_bridge_ip_0_GT_RX1_ch_rxcomsasdet,
            ch1_rxcomwakedet => gt_bridge_ip_0_GT_RX1_ch_rxcomwakedet,
            ch1_rxctrl0(15 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxctrl0(15 downto 0),
            ch1_rxctrl1(15 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxctrl1(15 downto 0),
            ch1_rxctrl2(7 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxctrl2(7 downto 0),
            ch1_rxctrl3(7 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxctrl3(7 downto 0),
            ch1_rxdapicodeovrden => gt_bridge_ip_0_GT_RX1_ch_rxdapicodeovrden,
            ch1_rxdapicodereset => gt_bridge_ip_0_GT_RX1_ch_rxdapicodereset,
            ch1_rxdata(127 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxdata(127 downto 0),
            ch1_rxdataextendrsvd(7 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxdataextendrsvd(7 downto 0),
            ch1_rxdatavalid(1 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxdatavalid(1 downto 0),
            ch1_rxdccdone => gt_bridge_ip_0_GT_RX1_ch_rxdccdone,
            ch1_rxdlyalignerr => gt_bridge_ip_0_GT_RX1_ch_rxdlyalignerr,
            ch1_rxdlyalignprog => gt_bridge_ip_0_GT_RX1_ch_rxdlyalignprog,
            ch1_rxdlyalignreq => gt_bridge_ip_0_GT_RX1_ch_rxdlyalignreq,
            ch1_rxelecidle => gt_bridge_ip_0_GT_RX1_ch_rxelecidle,
            ch1_rxeqtraining => gt_bridge_ip_0_GT_RX1_ch_rxeqtraining,
            ch1_rxfinealigndone => gt_bridge_ip_0_GT_RX1_ch_rxfinealigndone,
            ch1_rxgearboxslip => gt_bridge_ip_0_GT_RX1_ch_rxgearboxslip,
            ch1_rxheader(5 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxheader(5 downto 0),
            ch1_rxheadervalid(1 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxheadervalid(1 downto 0),
            ch1_rxlatclk => '0',
            ch1_rxlpmen => gt_bridge_ip_0_GT_RX1_ch_rxlpmen,
            ch1_rxmldchaindone => gt_bridge_ip_0_GT_RX1_ch_rxmldchaindone,
            ch1_rxmldchainreq => gt_bridge_ip_0_GT_RX1_ch_rxmldchainreq,
            ch1_rxmlfinealignreq => gt_bridge_ip_0_GT_RX1_ch_rxmlfinealignreq,
            ch1_rxmstdatapathreset => gt_bridge_ip_0_GT_RX1_ch_rxmstdatapathreset,
            ch1_rxmstreset => gt_bridge_ip_0_GT_RX1_ch_rxmstreset,
            ch1_rxmstresetdone => gt_bridge_ip_0_GT_RX1_ch_rxmstresetdone,
            ch1_rxoobreset => gt_bridge_ip_0_GT_RX1_ch_rxoobreset,
            ch1_rxosintdone => gt_bridge_ip_0_GT_RX1_ch_rxosintdone,
            ch1_rxosintstarted => gt_bridge_ip_0_GT_RX1_ch_rxosintstarted,
            ch1_rxosintstrobedone => gt_bridge_ip_0_GT_RX1_ch_rxosintstrobedone,
            ch1_rxosintstrobestarted => gt_bridge_ip_0_GT_RX1_ch_rxosintstrobestarted,
            ch1_rxoutclk => NLW_gt_quad_base_ch1_rxoutclk_UNCONNECTED,
            ch1_rxpcsresetmask(4 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxpcsresetmask(4 downto 0),
            ch1_rxpd(1 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxpd(1 downto 0),
            ch1_rxphaligndone => gt_bridge_ip_0_GT_RX1_ch_rxphaligndone,
            ch1_rxphalignerr => gt_bridge_ip_0_GT_RX1_ch_rxphalignerr,
            ch1_rxphalignreq => gt_bridge_ip_0_GT_RX1_ch_rxphalignreq,
            ch1_rxphalignresetmask(1 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxphalignresetmask(1 downto 0),
            ch1_rxphdlypd => gt_bridge_ip_0_GT_RX1_ch_rxphdlypd,
            ch1_rxphdlyreset => gt_bridge_ip_0_GT_RX1_ch_rxphdlyreset,
            ch1_rxphdlyresetdone => gt_bridge_ip_0_GT_RX1_ch_rxphdlyresetdone,
            ch1_rxphsetinitdone => gt_bridge_ip_0_GT_RX1_ch_rxphsetinitdone,
            ch1_rxphsetinitreq => gt_bridge_ip_0_GT_RX1_ch_rxphsetinitreq,
            ch1_rxphshift180 => gt_bridge_ip_0_GT_RX1_ch_rxphshift180,
            ch1_rxphshift180done => gt_bridge_ip_0_GT_RX1_ch_rxphshift180done,
            ch1_rxpmaresetdone => gt_bridge_ip_0_GT_RX1_ch_rxpmaresetdone,
            ch1_rxpmaresetmask(6 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxpmaresetmask(6 downto 0),
            ch1_rxpolarity => gt_bridge_ip_0_GT_RX1_ch_rxpolarity,
            ch1_rxprbscntreset => gt_bridge_ip_0_GT_RX1_ch_rxprbscntreset,
            ch1_rxprbserr => gt_bridge_ip_0_GT_RX1_ch_rxprbserr,
            ch1_rxprbslocked => gt_bridge_ip_0_GT_RX1_ch_rxprbslocked,
            ch1_rxprbssel(3 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxprbssel(3 downto 0),
            ch1_rxprogdivreset => gt_bridge_ip_0_GT_RX1_ch_rxprogdivreset,
            ch1_rxprogdivresetdone => gt_bridge_ip_0_GT_RX1_ch_rxprogdivresetdone,
            ch1_rxrate(7 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxrate(7 downto 0),
            ch1_rxresetdone => gt_bridge_ip_0_GT_RX1_ch_rxresetdone,
            ch1_rxresetmode(1 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxresetmode(1 downto 0),
            ch1_rxslide => gt_bridge_ip_0_GT_RX1_ch_rxslide,
            ch1_rxsliderdy => gt_bridge_ip_0_GT_RX1_ch_rxsliderdy,
            ch1_rxstartofseq(1 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxstartofseq(1 downto 0),
            ch1_rxstatus(2 downto 0) => gt_bridge_ip_0_GT_RX1_ch_rxstatus(2 downto 0),
            ch1_rxsyncallin => gt_bridge_ip_0_GT_RX1_ch_rxsyncallin,
            ch1_rxsyncdone => gt_bridge_ip_0_GT_RX1_ch_rxsyncdone,
            ch1_rxtermination => gt_bridge_ip_0_GT_RX1_ch_rxtermination,
            ch1_rxuserrdy => gt_bridge_ip_0_GT_RX1_ch_rxuserrdy,
            ch1_rxusrclk => bufg_gt_usrclk,
            ch1_rxvalid => gt_bridge_ip_0_GT_RX1_ch_rxvalid,
            ch1_tstin(19 downto 0) => B"00000000000000000000",
            ch1_tx10gstat => gt_bridge_ip_0_GT_TX1_ch_tx10gstat,
            ch1_txbufstatus(1 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txbufstatus(1 downto 0),
            ch1_txcomfinish => gt_bridge_ip_0_GT_TX1_ch_txcomfinish,
            ch1_txcominit => gt_bridge_ip_0_GT_TX1_ch_txcominit,
            ch1_txcomsas => gt_bridge_ip_0_GT_TX1_ch_txcomsas,
            ch1_txcomwake => gt_bridge_ip_0_GT_TX1_ch_txcomwake,
            ch1_txctrl0(15 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txctrl0(15 downto 0),
            ch1_txctrl1(15 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txctrl1(15 downto 0),
            ch1_txctrl2(7 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txctrl2(7 downto 0),
            ch1_txdapicodeovrden => gt_bridge_ip_0_GT_TX1_ch_txdapicodeovrden,
            ch1_txdapicodereset => gt_bridge_ip_0_GT_TX1_ch_txdapicodereset,
            ch1_txdata(127 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txdata(127 downto 0),
            ch1_txdataextendrsvd(7 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txdataextendrsvd(7 downto 0),
            ch1_txdccdone => gt_bridge_ip_0_GT_TX1_ch_txdccdone,
            ch1_txdeemph(1 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txdeemph(1 downto 0),
            ch1_txdetectrx => gt_bridge_ip_0_GT_TX1_ch_txdetectrx,
            ch1_txdiffctrl(4 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txdiffctrl(4 downto 0),
            ch1_txdlyalignerr => gt_bridge_ip_0_GT_TX1_ch_txdlyalignerr,
            ch1_txdlyalignprog => gt_bridge_ip_0_GT_TX1_ch_txdlyalignprog,
            ch1_txdlyalignreq => gt_bridge_ip_0_GT_TX1_ch_txdlyalignreq,
            ch1_txelecidle => gt_bridge_ip_0_GT_TX1_ch_txelecidle,
            ch1_txheader(5 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txheader(5 downto 0),
            ch1_txinhibit => gt_bridge_ip_0_GT_TX1_ch_txinhibit,
            ch1_txlatclk => '0',
            ch1_txmaincursor(6 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txmaincursor(6 downto 0),
            ch1_txmargin(2 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txmargin(2 downto 0),
            ch1_txmldchaindone => gt_bridge_ip_0_GT_TX1_ch_txmldchaindone,
            ch1_txmldchainreq => gt_bridge_ip_0_GT_TX1_ch_txmldchainreq,
            ch1_txmstdatapathreset => gt_bridge_ip_0_GT_TX1_ch_txmstdatapathreset,
            ch1_txmstreset => gt_bridge_ip_0_GT_TX1_ch_txmstreset,
            ch1_txmstresetdone => gt_bridge_ip_0_GT_TX1_ch_txmstresetdone,
            ch1_txoneszeros => gt_bridge_ip_0_GT_TX1_ch_txoneszeros,
            ch1_txoutclk => NLW_gt_quad_base_ch1_txoutclk_UNCONNECTED,
            ch1_txpausedelayalign => gt_bridge_ip_0_GT_TX1_ch_txpausedelayalign,
            ch1_txpcsresetmask => gt_bridge_ip_0_GT_TX1_ch_txpcsresetmask,
            ch1_txpd(1 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txpd(1 downto 0),
            ch1_txphaligndone => gt_bridge_ip_0_GT_TX1_ch_txphaligndone,
            ch1_txphalignerr => gt_bridge_ip_0_GT_TX1_ch_txphalignerr,
            ch1_txphalignoutrsvd => gt_bridge_ip_0_GT_TX1_ch_txphalignoutrsvd,
            ch1_txphalignreq => gt_bridge_ip_0_GT_TX1_ch_txphalignreq,
            ch1_txphalignresetmask(1 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txphalignresetmask(1 downto 0),
            ch1_txphdlypd => gt_bridge_ip_0_GT_TX1_ch_txphdlypd,
            ch1_txphdlyreset => gt_bridge_ip_0_GT_TX1_ch_txphdlyreset,
            ch1_txphdlyresetdone => gt_bridge_ip_0_GT_TX1_ch_txphdlyresetdone,
            ch1_txphdlytstclk => '0',
            ch1_txphsetinitdone => gt_bridge_ip_0_GT_TX1_ch_txphsetinitdone,
            ch1_txphsetinitreq => gt_bridge_ip_0_GT_TX1_ch_txphsetinitreq,
            ch1_txphshift180 => gt_bridge_ip_0_GT_TX1_ch_txphshift180,
            ch1_txphshift180done => gt_bridge_ip_0_GT_TX1_ch_txphshift180done,
            ch1_txpicodeovrden => gt_bridge_ip_0_GT_TX1_ch_txpicodeovrden,
            ch1_txpicodereset => gt_bridge_ip_0_GT_TX1_ch_txpicodereset,
            ch1_txpippmen => gt_bridge_ip_0_GT_TX1_ch_txpippmen,
            ch1_txpippmstepsize(4 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txpippmstepsize(4 downto 0),
            ch1_txpisopd => gt_bridge_ip_0_GT_TX1_ch_txpisopd,
            ch1_txpmaresetdone => gt_bridge_ip_0_GT_TX1_ch_txpmaresetdone,
            ch1_txpmaresetmask(2 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txpmaresetmask(2 downto 0),
            ch1_txpolarity => gt_bridge_ip_0_GT_TX1_ch_txpolarity,
            ch1_txpostcursor(4 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txpostcursor(4 downto 0),
            ch1_txprbsforceerr => gt_bridge_ip_0_GT_TX1_ch_txprbsforceerr,
            ch1_txprbssel(3 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txprbssel(3 downto 0),
            ch1_txprecursor(4 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txprecursor(4 downto 0),
            ch1_txprogdivreset => gt_bridge_ip_0_GT_TX1_ch_txprogdivreset,
            ch1_txprogdivresetdone => gt_bridge_ip_0_GT_TX1_ch_txprogdivresetdone,
            ch1_txrate(7 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txrate(7 downto 0),
            ch1_txresetdone => gt_bridge_ip_0_GT_TX1_ch_txresetdone,
            ch1_txresetmode(1 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txresetmode(1 downto 0),
            ch1_txsequence(6 downto 0) => gt_bridge_ip_0_GT_TX1_ch_txsequence(6 downto 0),
            ch1_txswing => gt_bridge_ip_0_GT_TX1_ch_txswing,
            ch1_txsyncallin => gt_bridge_ip_0_GT_TX1_ch_txsyncallin,
            ch1_txsyncdone => gt_bridge_ip_0_GT_TX1_ch_txsyncdone,
            ch1_txuserrdy => gt_bridge_ip_0_GT_TX1_ch_txuserrdy,
            ch1_txusrclk => bufg_gt_1_usrclk,
            ch2_bufgtce => NLW_gt_quad_base_ch2_bufgtce_UNCONNECTED,
            ch2_bufgtcemask(3 downto 0) => NLW_gt_quad_base_ch2_bufgtcemask_UNCONNECTED(3 downto 0),
            ch2_bufgtdiv(11 downto 0) => NLW_gt_quad_base_ch2_bufgtdiv_UNCONNECTED(11 downto 0),
            ch2_bufgtrst => NLW_gt_quad_base_ch2_bufgtrst_UNCONNECTED,
            ch2_bufgtrstmask(3 downto 0) => NLW_gt_quad_base_ch2_bufgtrstmask_UNCONNECTED(3 downto 0),
            ch2_cdrbmcdrreq => gt_bridge_ip_0_GT_RX2_ch_cdrbmcdrreq,
            ch2_cdrfreqos => gt_bridge_ip_0_GT_RX2_ch_cdrfreqos,
            ch2_cdrincpctrl => gt_bridge_ip_0_GT_RX2_ch_cdrincpctrl,
            ch2_cdrstepdir => gt_bridge_ip_0_GT_RX2_ch_cdrstepdir,
            ch2_cdrstepsq => gt_bridge_ip_0_GT_RX2_ch_cdrstepsq,
            ch2_cdrstepsx => gt_bridge_ip_0_GT_RX2_ch_cdrstepsx,
            ch2_cfokovrdfinish => gt_bridge_ip_0_GT_RX2_ch_cfokovrdfinish,
            ch2_cfokovrdpulse => gt_bridge_ip_0_GT_RX2_ch_cfokovrdpulse,
            ch2_cfokovrdrdy0 => gt_bridge_ip_0_GT_RX2_ch_cfokovrdrdy0,
            ch2_cfokovrdrdy1 => gt_bridge_ip_0_GT_RX2_ch_cfokovrdrdy1,
            ch2_cfokovrdstart => gt_bridge_ip_0_GT_RX2_ch_cfokovrdstart,
            ch2_clkrsvd0 => '0',
            ch2_clkrsvd1 => '0',
            ch2_dmonfiforeset => '0',
            ch2_dmonitorclk => '0',
            ch2_dmonitorout(31 downto 0) => NLW_gt_quad_base_ch2_dmonitorout_UNCONNECTED(31 downto 0),
            ch2_dmonitoroutclk => NLW_gt_quad_base_ch2_dmonitoroutclk_UNCONNECTED,
            ch2_eyescandataerror => gt_bridge_ip_0_GT_RX2_ch_eyescandataerror,
            ch2_eyescanreset => gt_bridge_ip_0_GT_RX2_ch_eyescanreset,
            ch2_eyescantrigger => gt_bridge_ip_0_GT_RX2_ch_eyescantrigger,
            ch2_gtrsvd(15 downto 0) => B"0000000000000000",
            ch2_gtrxreset => gt_bridge_ip_0_GT_RX2_ch_gtrxreset,
            ch2_gttxreset => gt_bridge_ip_0_GT_TX2_ch_gttxreset,
            ch2_hsdppcsreset => '0',
            ch2_iloreset => gt_bridge_ip_0_gt_ilo_reset,
            ch2_iloresetdone => gt_quad_base_ch2_iloresetdone,
            ch2_iloresetmask => '1',
            ch2_loopback(2 downto 0) => ch2_loopback_0_1(2 downto 0),
            ch2_pcierstb => '1',
            ch2_pcsrsvdin(15 downto 0) => B"0000001001000000",
            ch2_pcsrsvdout(15 downto 0) => NLW_gt_quad_base_ch2_pcsrsvdout_UNCONNECTED(15 downto 0),
            ch2_phyesmadaptsave => '0',
            ch2_phyready => NLW_gt_quad_base_ch2_phyready_UNCONNECTED,
            ch2_phystatus => gt_quad_base_ch2_phystatus,
            ch2_pinrsvdas(15 downto 0) => NLW_gt_quad_base_ch2_pinrsvdas_UNCONNECTED(15 downto 0),
            ch2_resetexception => NLW_gt_quad_base_ch2_resetexception_UNCONNECTED,
            ch2_rx10gstat(7 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rx10gstat(7 downto 0),
            ch2_rxbufstatus(2 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxbufstatus(2 downto 0),
            ch2_rxbyteisaligned => gt_bridge_ip_0_GT_RX2_ch_rxbyteisaligned,
            ch2_rxbyterealign => gt_bridge_ip_0_GT_RX2_ch_rxbyterealign,
            ch2_rxcdrhold => gt_bridge_ip_0_GT_RX2_ch_rxcdrhold,
            ch2_rxcdrlock => gt_bridge_ip_0_GT_RX2_ch_rxcdrlock,
            ch2_rxcdrovrden => gt_bridge_ip_0_GT_RX2_ch_rxcdrovrden,
            ch2_rxcdrphdone => gt_bridge_ip_0_GT_RX2_ch_rxcdrphdone,
            ch2_rxcdrreset => gt_bridge_ip_0_GT_RX2_ch_rxcdrreset,
            ch2_rxchanbondseq => gt_bridge_ip_0_GT_RX2_ch_rxchanbondseq,
            ch2_rxchanisaligned => gt_bridge_ip_0_GT_RX2_ch_rxchanisaligned,
            ch2_rxchanrealign => gt_bridge_ip_0_GT_RX2_ch_rxchanrealign,
            ch2_rxchbondi(4 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxchbondi(4 downto 0),
            ch2_rxchbondo(4 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxchbondo(4 downto 0),
            ch2_rxclkcorcnt(1 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxclkcorcnt(1 downto 0),
            ch2_rxcominitdet => gt_bridge_ip_0_GT_RX2_ch_rxcominitdet,
            ch2_rxcommadet => gt_bridge_ip_0_GT_RX2_ch_rxcommadet,
            ch2_rxcomsasdet => gt_bridge_ip_0_GT_RX2_ch_rxcomsasdet,
            ch2_rxcomwakedet => gt_bridge_ip_0_GT_RX2_ch_rxcomwakedet,
            ch2_rxctrl0(15 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxctrl0(15 downto 0),
            ch2_rxctrl1(15 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxctrl1(15 downto 0),
            ch2_rxctrl2(7 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxctrl2(7 downto 0),
            ch2_rxctrl3(7 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxctrl3(7 downto 0),
            ch2_rxdapicodeovrden => gt_bridge_ip_0_GT_RX2_ch_rxdapicodeovrden,
            ch2_rxdapicodereset => gt_bridge_ip_0_GT_RX2_ch_rxdapicodereset,
            ch2_rxdata(127 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxdata(127 downto 0),
            ch2_rxdataextendrsvd(7 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxdataextendrsvd(7 downto 0),
            ch2_rxdatavalid(1 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxdatavalid(1 downto 0),
            ch2_rxdccdone => gt_bridge_ip_0_GT_RX2_ch_rxdccdone,
            ch2_rxdlyalignerr => gt_bridge_ip_0_GT_RX2_ch_rxdlyalignerr,
            ch2_rxdlyalignprog => gt_bridge_ip_0_GT_RX2_ch_rxdlyalignprog,
            ch2_rxdlyalignreq => gt_bridge_ip_0_GT_RX2_ch_rxdlyalignreq,
            ch2_rxelecidle => gt_bridge_ip_0_GT_RX2_ch_rxelecidle,
            ch2_rxeqtraining => gt_bridge_ip_0_GT_RX2_ch_rxeqtraining,
            ch2_rxfinealigndone => gt_bridge_ip_0_GT_RX2_ch_rxfinealigndone,
            ch2_rxgearboxslip => gt_bridge_ip_0_GT_RX2_ch_rxgearboxslip,
            ch2_rxheader(5 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxheader(5 downto 0),
            ch2_rxheadervalid(1 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxheadervalid(1 downto 0),
            ch2_rxlatclk => '0',
            ch2_rxlpmen => gt_bridge_ip_0_GT_RX2_ch_rxlpmen,
            ch2_rxmldchaindone => gt_bridge_ip_0_GT_RX2_ch_rxmldchaindone,
            ch2_rxmldchainreq => gt_bridge_ip_0_GT_RX2_ch_rxmldchainreq,
            ch2_rxmlfinealignreq => gt_bridge_ip_0_GT_RX2_ch_rxmlfinealignreq,
            ch2_rxmstdatapathreset => gt_bridge_ip_0_GT_RX2_ch_rxmstdatapathreset,
            ch2_rxmstreset => gt_bridge_ip_0_GT_RX2_ch_rxmstreset,
            ch2_rxmstresetdone => gt_bridge_ip_0_GT_RX2_ch_rxmstresetdone,
            ch2_rxoobreset => gt_bridge_ip_0_GT_RX2_ch_rxoobreset,
            ch2_rxosintdone => gt_bridge_ip_0_GT_RX2_ch_rxosintdone,
            ch2_rxosintstarted => gt_bridge_ip_0_GT_RX2_ch_rxosintstarted,
            ch2_rxosintstrobedone => gt_bridge_ip_0_GT_RX2_ch_rxosintstrobedone,
            ch2_rxosintstrobestarted => gt_bridge_ip_0_GT_RX2_ch_rxosintstrobestarted,
            ch2_rxoutclk => NLW_gt_quad_base_ch2_rxoutclk_UNCONNECTED,
            ch2_rxpcsresetmask(4 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxpcsresetmask(4 downto 0),
            ch2_rxpd(1 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxpd(1 downto 0),
            ch2_rxphaligndone => gt_bridge_ip_0_GT_RX2_ch_rxphaligndone,
            ch2_rxphalignerr => gt_bridge_ip_0_GT_RX2_ch_rxphalignerr,
            ch2_rxphalignreq => gt_bridge_ip_0_GT_RX2_ch_rxphalignreq,
            ch2_rxphalignresetmask(1 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxphalignresetmask(1 downto 0),
            ch2_rxphdlypd => gt_bridge_ip_0_GT_RX2_ch_rxphdlypd,
            ch2_rxphdlyreset => gt_bridge_ip_0_GT_RX2_ch_rxphdlyreset,
            ch2_rxphdlyresetdone => gt_bridge_ip_0_GT_RX2_ch_rxphdlyresetdone,
            ch2_rxphsetinitdone => gt_bridge_ip_0_GT_RX2_ch_rxphsetinitdone,
            ch2_rxphsetinitreq => gt_bridge_ip_0_GT_RX2_ch_rxphsetinitreq,
            ch2_rxphshift180 => gt_bridge_ip_0_GT_RX2_ch_rxphshift180,
            ch2_rxphshift180done => gt_bridge_ip_0_GT_RX2_ch_rxphshift180done,
            ch2_rxpmaresetdone => gt_bridge_ip_0_GT_RX2_ch_rxpmaresetdone,
            ch2_rxpmaresetmask(6 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxpmaresetmask(6 downto 0),
            ch2_rxpolarity => gt_bridge_ip_0_GT_RX2_ch_rxpolarity,
            ch2_rxprbscntreset => gt_bridge_ip_0_GT_RX2_ch_rxprbscntreset,
            ch2_rxprbserr => gt_bridge_ip_0_GT_RX2_ch_rxprbserr,
            ch2_rxprbslocked => gt_bridge_ip_0_GT_RX2_ch_rxprbslocked,
            ch2_rxprbssel(3 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxprbssel(3 downto 0),
            ch2_rxprogdivreset => gt_bridge_ip_0_GT_RX2_ch_rxprogdivreset,
            ch2_rxprogdivresetdone => gt_bridge_ip_0_GT_RX2_ch_rxprogdivresetdone,
            ch2_rxrate(7 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxrate(7 downto 0),
            ch2_rxresetdone => gt_bridge_ip_0_GT_RX2_ch_rxresetdone,
            ch2_rxresetmode(1 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxresetmode(1 downto 0),
            ch2_rxslide => gt_bridge_ip_0_GT_RX2_ch_rxslide,
            ch2_rxsliderdy => gt_bridge_ip_0_GT_RX2_ch_rxsliderdy,
            ch2_rxstartofseq(1 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxstartofseq(1 downto 0),
            ch2_rxstatus(2 downto 0) => gt_bridge_ip_0_GT_RX2_ch_rxstatus(2 downto 0),
            ch2_rxsyncallin => gt_bridge_ip_0_GT_RX2_ch_rxsyncallin,
            ch2_rxsyncdone => gt_bridge_ip_0_GT_RX2_ch_rxsyncdone,
            ch2_rxtermination => gt_bridge_ip_0_GT_RX2_ch_rxtermination,
            ch2_rxuserrdy => gt_bridge_ip_0_GT_RX2_ch_rxuserrdy,
            ch2_rxusrclk => bufg_gt_usrclk,
            ch2_rxvalid => gt_bridge_ip_0_GT_RX2_ch_rxvalid,
            ch2_tstin(19 downto 0) => B"00000000000000000000",
            ch2_tx10gstat => gt_bridge_ip_0_GT_TX2_ch_tx10gstat,
            ch2_txbufstatus(1 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txbufstatus(1 downto 0),
            ch2_txcomfinish => gt_bridge_ip_0_GT_TX2_ch_txcomfinish,
            ch2_txcominit => gt_bridge_ip_0_GT_TX2_ch_txcominit,
            ch2_txcomsas => gt_bridge_ip_0_GT_TX2_ch_txcomsas,
            ch2_txcomwake => gt_bridge_ip_0_GT_TX2_ch_txcomwake,
            ch2_txctrl0(15 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txctrl0(15 downto 0),
            ch2_txctrl1(15 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txctrl1(15 downto 0),
            ch2_txctrl2(7 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txctrl2(7 downto 0),
            ch2_txdapicodeovrden => gt_bridge_ip_0_GT_TX2_ch_txdapicodeovrden,
            ch2_txdapicodereset => gt_bridge_ip_0_GT_TX2_ch_txdapicodereset,
            ch2_txdata(127 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txdata(127 downto 0),
            ch2_txdataextendrsvd(7 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txdataextendrsvd(7 downto 0),
            ch2_txdccdone => gt_bridge_ip_0_GT_TX2_ch_txdccdone,
            ch2_txdeemph(1 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txdeemph(1 downto 0),
            ch2_txdetectrx => gt_bridge_ip_0_GT_TX2_ch_txdetectrx,
            ch2_txdiffctrl(4 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txdiffctrl(4 downto 0),
            ch2_txdlyalignerr => gt_bridge_ip_0_GT_TX2_ch_txdlyalignerr,
            ch2_txdlyalignprog => gt_bridge_ip_0_GT_TX2_ch_txdlyalignprog,
            ch2_txdlyalignreq => gt_bridge_ip_0_GT_TX2_ch_txdlyalignreq,
            ch2_txelecidle => gt_bridge_ip_0_GT_TX2_ch_txelecidle,
            ch2_txheader(5 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txheader(5 downto 0),
            ch2_txinhibit => gt_bridge_ip_0_GT_TX2_ch_txinhibit,
            ch2_txlatclk => '0',
            ch2_txmaincursor(6 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txmaincursor(6 downto 0),
            ch2_txmargin(2 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txmargin(2 downto 0),
            ch2_txmldchaindone => gt_bridge_ip_0_GT_TX2_ch_txmldchaindone,
            ch2_txmldchainreq => gt_bridge_ip_0_GT_TX2_ch_txmldchainreq,
            ch2_txmstdatapathreset => gt_bridge_ip_0_GT_TX2_ch_txmstdatapathreset,
            ch2_txmstreset => gt_bridge_ip_0_GT_TX2_ch_txmstreset,
            ch2_txmstresetdone => gt_bridge_ip_0_GT_TX2_ch_txmstresetdone,
            ch2_txoneszeros => gt_bridge_ip_0_GT_TX2_ch_txoneszeros,
            ch2_txoutclk => NLW_gt_quad_base_ch2_txoutclk_UNCONNECTED,
            ch2_txpausedelayalign => gt_bridge_ip_0_GT_TX2_ch_txpausedelayalign,
            ch2_txpcsresetmask => gt_bridge_ip_0_GT_TX2_ch_txpcsresetmask,
            ch2_txpd(1 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txpd(1 downto 0),
            ch2_txphaligndone => gt_bridge_ip_0_GT_TX2_ch_txphaligndone,
            ch2_txphalignerr => gt_bridge_ip_0_GT_TX2_ch_txphalignerr,
            ch2_txphalignoutrsvd => gt_bridge_ip_0_GT_TX2_ch_txphalignoutrsvd,
            ch2_txphalignreq => gt_bridge_ip_0_GT_TX2_ch_txphalignreq,
            ch2_txphalignresetmask(1 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txphalignresetmask(1 downto 0),
            ch2_txphdlypd => gt_bridge_ip_0_GT_TX2_ch_txphdlypd,
            ch2_txphdlyreset => gt_bridge_ip_0_GT_TX2_ch_txphdlyreset,
            ch2_txphdlyresetdone => gt_bridge_ip_0_GT_TX2_ch_txphdlyresetdone,
            ch2_txphdlytstclk => '0',
            ch2_txphsetinitdone => gt_bridge_ip_0_GT_TX2_ch_txphsetinitdone,
            ch2_txphsetinitreq => gt_bridge_ip_0_GT_TX2_ch_txphsetinitreq,
            ch2_txphshift180 => gt_bridge_ip_0_GT_TX2_ch_txphshift180,
            ch2_txphshift180done => gt_bridge_ip_0_GT_TX2_ch_txphshift180done,
            ch2_txpicodeovrden => gt_bridge_ip_0_GT_TX2_ch_txpicodeovrden,
            ch2_txpicodereset => gt_bridge_ip_0_GT_TX2_ch_txpicodereset,
            ch2_txpippmen => gt_bridge_ip_0_GT_TX2_ch_txpippmen,
            ch2_txpippmstepsize(4 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txpippmstepsize(4 downto 0),
            ch2_txpisopd => gt_bridge_ip_0_GT_TX2_ch_txpisopd,
            ch2_txpmaresetdone => gt_bridge_ip_0_GT_TX2_ch_txpmaresetdone,
            ch2_txpmaresetmask(2 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txpmaresetmask(2 downto 0),
            ch2_txpolarity => gt_bridge_ip_0_GT_TX2_ch_txpolarity,
            ch2_txpostcursor(4 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txpostcursor(4 downto 0),
            ch2_txprbsforceerr => gt_bridge_ip_0_GT_TX2_ch_txprbsforceerr,
            ch2_txprbssel(3 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txprbssel(3 downto 0),
            ch2_txprecursor(4 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txprecursor(4 downto 0),
            ch2_txprogdivreset => gt_bridge_ip_0_GT_TX2_ch_txprogdivreset,
            ch2_txprogdivresetdone => gt_bridge_ip_0_GT_TX2_ch_txprogdivresetdone,
            ch2_txrate(7 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txrate(7 downto 0),
            ch2_txresetdone => gt_bridge_ip_0_GT_TX2_ch_txresetdone,
            ch2_txresetmode(1 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txresetmode(1 downto 0),
            ch2_txsequence(6 downto 0) => gt_bridge_ip_0_GT_TX2_ch_txsequence(6 downto 0),
            ch2_txswing => gt_bridge_ip_0_GT_TX2_ch_txswing,
            ch2_txsyncallin => gt_bridge_ip_0_GT_TX2_ch_txsyncallin,
            ch2_txsyncdone => gt_bridge_ip_0_GT_TX2_ch_txsyncdone,
            ch2_txuserrdy => gt_bridge_ip_0_GT_TX2_ch_txuserrdy,
            ch2_txusrclk => bufg_gt_1_usrclk,
            ch3_bufgtce => NLW_gt_quad_base_ch3_bufgtce_UNCONNECTED,
            ch3_bufgtcemask(3 downto 0) => NLW_gt_quad_base_ch3_bufgtcemask_UNCONNECTED(3 downto 0),
            ch3_bufgtdiv(11 downto 0) => NLW_gt_quad_base_ch3_bufgtdiv_UNCONNECTED(11 downto 0),
            ch3_bufgtrst => NLW_gt_quad_base_ch3_bufgtrst_UNCONNECTED,
            ch3_bufgtrstmask(3 downto 0) => NLW_gt_quad_base_ch3_bufgtrstmask_UNCONNECTED(3 downto 0),
            ch3_cdrbmcdrreq => gt_bridge_ip_0_GT_RX3_ch_cdrbmcdrreq,
            ch3_cdrfreqos => gt_bridge_ip_0_GT_RX3_ch_cdrfreqos,
            ch3_cdrincpctrl => gt_bridge_ip_0_GT_RX3_ch_cdrincpctrl,
            ch3_cdrstepdir => gt_bridge_ip_0_GT_RX3_ch_cdrstepdir,
            ch3_cdrstepsq => gt_bridge_ip_0_GT_RX3_ch_cdrstepsq,
            ch3_cdrstepsx => gt_bridge_ip_0_GT_RX3_ch_cdrstepsx,
            ch3_cfokovrdfinish => gt_bridge_ip_0_GT_RX3_ch_cfokovrdfinish,
            ch3_cfokovrdpulse => gt_bridge_ip_0_GT_RX3_ch_cfokovrdpulse,
            ch3_cfokovrdrdy0 => gt_bridge_ip_0_GT_RX3_ch_cfokovrdrdy0,
            ch3_cfokovrdrdy1 => gt_bridge_ip_0_GT_RX3_ch_cfokovrdrdy1,
            ch3_cfokovrdstart => gt_bridge_ip_0_GT_RX3_ch_cfokovrdstart,
            ch3_clkrsvd0 => '0',
            ch3_clkrsvd1 => '0',
            ch3_dmonfiforeset => '0',
            ch3_dmonitorclk => '0',
            ch3_dmonitorout(31 downto 0) => NLW_gt_quad_base_ch3_dmonitorout_UNCONNECTED(31 downto 0),
            ch3_dmonitoroutclk => NLW_gt_quad_base_ch3_dmonitoroutclk_UNCONNECTED,
            ch3_eyescandataerror => gt_bridge_ip_0_GT_RX3_ch_eyescandataerror,
            ch3_eyescanreset => gt_bridge_ip_0_GT_RX3_ch_eyescanreset,
            ch3_eyescantrigger => gt_bridge_ip_0_GT_RX3_ch_eyescantrigger,
            ch3_gtrsvd(15 downto 0) => B"0000000000000000",
            ch3_gtrxreset => gt_bridge_ip_0_GT_RX3_ch_gtrxreset,
            ch3_gttxreset => gt_bridge_ip_0_GT_TX3_ch_gttxreset,
            ch3_hsdppcsreset => '0',
            ch3_iloreset => gt_bridge_ip_0_gt_ilo_reset,
            ch3_iloresetdone => gt_quad_base_ch3_iloresetdone,
            ch3_iloresetmask => '1',
            ch3_loopback(2 downto 0) => ch3_loopback_0_1(2 downto 0),
            ch3_pcierstb => '1',
            ch3_pcsrsvdin(15 downto 0) => B"0000001001000000",
            ch3_pcsrsvdout(15 downto 0) => NLW_gt_quad_base_ch3_pcsrsvdout_UNCONNECTED(15 downto 0),
            ch3_phyesmadaptsave => '0',
            ch3_phyready => NLW_gt_quad_base_ch3_phyready_UNCONNECTED,
            ch3_phystatus => gt_quad_base_ch3_phystatus,
            ch3_pinrsvdas(15 downto 0) => NLW_gt_quad_base_ch3_pinrsvdas_UNCONNECTED(15 downto 0),
            ch3_resetexception => NLW_gt_quad_base_ch3_resetexception_UNCONNECTED,
            ch3_rx10gstat(7 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rx10gstat(7 downto 0),
            ch3_rxbufstatus(2 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxbufstatus(2 downto 0),
            ch3_rxbyteisaligned => gt_bridge_ip_0_GT_RX3_ch_rxbyteisaligned,
            ch3_rxbyterealign => gt_bridge_ip_0_GT_RX3_ch_rxbyterealign,
            ch3_rxcdrhold => gt_bridge_ip_0_GT_RX3_ch_rxcdrhold,
            ch3_rxcdrlock => gt_bridge_ip_0_GT_RX3_ch_rxcdrlock,
            ch3_rxcdrovrden => gt_bridge_ip_0_GT_RX3_ch_rxcdrovrden,
            ch3_rxcdrphdone => gt_bridge_ip_0_GT_RX3_ch_rxcdrphdone,
            ch3_rxcdrreset => gt_bridge_ip_0_GT_RX3_ch_rxcdrreset,
            ch3_rxchanbondseq => gt_bridge_ip_0_GT_RX3_ch_rxchanbondseq,
            ch3_rxchanisaligned => gt_bridge_ip_0_GT_RX3_ch_rxchanisaligned,
            ch3_rxchanrealign => gt_bridge_ip_0_GT_RX3_ch_rxchanrealign,
            ch3_rxchbondi(4 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxchbondi(4 downto 0),
            ch3_rxchbondo(4 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxchbondo(4 downto 0),
            ch3_rxclkcorcnt(1 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxclkcorcnt(1 downto 0),
            ch3_rxcominitdet => gt_bridge_ip_0_GT_RX3_ch_rxcominitdet,
            ch3_rxcommadet => gt_bridge_ip_0_GT_RX3_ch_rxcommadet,
            ch3_rxcomsasdet => gt_bridge_ip_0_GT_RX3_ch_rxcomsasdet,
            ch3_rxcomwakedet => gt_bridge_ip_0_GT_RX3_ch_rxcomwakedet,
            ch3_rxctrl0(15 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxctrl0(15 downto 0),
            ch3_rxctrl1(15 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxctrl1(15 downto 0),
            ch3_rxctrl2(7 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxctrl2(7 downto 0),
            ch3_rxctrl3(7 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxctrl3(7 downto 0),
            ch3_rxdapicodeovrden => gt_bridge_ip_0_GT_RX3_ch_rxdapicodeovrden,
            ch3_rxdapicodereset => gt_bridge_ip_0_GT_RX3_ch_rxdapicodereset,
            ch3_rxdata(127 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxdata(127 downto 0),
            ch3_rxdataextendrsvd(7 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxdataextendrsvd(7 downto 0),
            ch3_rxdatavalid(1 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxdatavalid(1 downto 0),
            ch3_rxdccdone => gt_bridge_ip_0_GT_RX3_ch_rxdccdone,
            ch3_rxdlyalignerr => gt_bridge_ip_0_GT_RX3_ch_rxdlyalignerr,
            ch3_rxdlyalignprog => gt_bridge_ip_0_GT_RX3_ch_rxdlyalignprog,
            ch3_rxdlyalignreq => gt_bridge_ip_0_GT_RX3_ch_rxdlyalignreq,
            ch3_rxelecidle => gt_bridge_ip_0_GT_RX3_ch_rxelecidle,
            ch3_rxeqtraining => gt_bridge_ip_0_GT_RX3_ch_rxeqtraining,
            ch3_rxfinealigndone => gt_bridge_ip_0_GT_RX3_ch_rxfinealigndone,
            ch3_rxgearboxslip => gt_bridge_ip_0_GT_RX3_ch_rxgearboxslip,
            ch3_rxheader(5 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxheader(5 downto 0),
            ch3_rxheadervalid(1 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxheadervalid(1 downto 0),
            ch3_rxlatclk => '0',
            ch3_rxlpmen => gt_bridge_ip_0_GT_RX3_ch_rxlpmen,
            ch3_rxmldchaindone => gt_bridge_ip_0_GT_RX3_ch_rxmldchaindone,
            ch3_rxmldchainreq => gt_bridge_ip_0_GT_RX3_ch_rxmldchainreq,
            ch3_rxmlfinealignreq => gt_bridge_ip_0_GT_RX3_ch_rxmlfinealignreq,
            ch3_rxmstdatapathreset => gt_bridge_ip_0_GT_RX3_ch_rxmstdatapathreset,
            ch3_rxmstreset => gt_bridge_ip_0_GT_RX3_ch_rxmstreset,
            ch3_rxmstresetdone => gt_bridge_ip_0_GT_RX3_ch_rxmstresetdone,
            ch3_rxoobreset => gt_bridge_ip_0_GT_RX3_ch_rxoobreset,
            ch3_rxosintdone => gt_bridge_ip_0_GT_RX3_ch_rxosintdone,
            ch3_rxosintstarted => gt_bridge_ip_0_GT_RX3_ch_rxosintstarted,
            ch3_rxosintstrobedone => gt_bridge_ip_0_GT_RX3_ch_rxosintstrobedone,
            ch3_rxosintstrobestarted => gt_bridge_ip_0_GT_RX3_ch_rxosintstrobestarted,
            ch3_rxoutclk => NLW_gt_quad_base_ch3_rxoutclk_UNCONNECTED,
            ch3_rxpcsresetmask(4 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxpcsresetmask(4 downto 0),
            ch3_rxpd(1 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxpd(1 downto 0),
            ch3_rxphaligndone => gt_bridge_ip_0_GT_RX3_ch_rxphaligndone,
            ch3_rxphalignerr => gt_bridge_ip_0_GT_RX3_ch_rxphalignerr,
            ch3_rxphalignreq => gt_bridge_ip_0_GT_RX3_ch_rxphalignreq,
            ch3_rxphalignresetmask(1 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxphalignresetmask(1 downto 0),
            ch3_rxphdlypd => gt_bridge_ip_0_GT_RX3_ch_rxphdlypd,
            ch3_rxphdlyreset => gt_bridge_ip_0_GT_RX3_ch_rxphdlyreset,
            ch3_rxphdlyresetdone => gt_bridge_ip_0_GT_RX3_ch_rxphdlyresetdone,
            ch3_rxphsetinitdone => gt_bridge_ip_0_GT_RX3_ch_rxphsetinitdone,
            ch3_rxphsetinitreq => gt_bridge_ip_0_GT_RX3_ch_rxphsetinitreq,
            ch3_rxphshift180 => gt_bridge_ip_0_GT_RX3_ch_rxphshift180,
            ch3_rxphshift180done => gt_bridge_ip_0_GT_RX3_ch_rxphshift180done,
            ch3_rxpmaresetdone => gt_bridge_ip_0_GT_RX3_ch_rxpmaresetdone,
            ch3_rxpmaresetmask(6 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxpmaresetmask(6 downto 0),
            ch3_rxpolarity => gt_bridge_ip_0_GT_RX3_ch_rxpolarity,
            ch3_rxprbscntreset => gt_bridge_ip_0_GT_RX3_ch_rxprbscntreset,
            ch3_rxprbserr => gt_bridge_ip_0_GT_RX3_ch_rxprbserr,
            ch3_rxprbslocked => gt_bridge_ip_0_GT_RX3_ch_rxprbslocked,
            ch3_rxprbssel(3 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxprbssel(3 downto 0),
            ch3_rxprogdivreset => gt_bridge_ip_0_GT_RX3_ch_rxprogdivreset,
            ch3_rxprogdivresetdone => gt_bridge_ip_0_GT_RX3_ch_rxprogdivresetdone,
            ch3_rxrate(7 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxrate(7 downto 0),
            ch3_rxresetdone => gt_bridge_ip_0_GT_RX3_ch_rxresetdone,
            ch3_rxresetmode(1 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxresetmode(1 downto 0),
            ch3_rxslide => gt_bridge_ip_0_GT_RX3_ch_rxslide,
            ch3_rxsliderdy => gt_bridge_ip_0_GT_RX3_ch_rxsliderdy,
            ch3_rxstartofseq(1 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxstartofseq(1 downto 0),
            ch3_rxstatus(2 downto 0) => gt_bridge_ip_0_GT_RX3_ch_rxstatus(2 downto 0),
            ch3_rxsyncallin => gt_bridge_ip_0_GT_RX3_ch_rxsyncallin,
            ch3_rxsyncdone => gt_bridge_ip_0_GT_RX3_ch_rxsyncdone,
            ch3_rxtermination => gt_bridge_ip_0_GT_RX3_ch_rxtermination,
            ch3_rxuserrdy => gt_bridge_ip_0_GT_RX3_ch_rxuserrdy,
            ch3_rxusrclk => bufg_gt_usrclk,
            ch3_rxvalid => gt_bridge_ip_0_GT_RX3_ch_rxvalid,
            ch3_tstin(19 downto 0) => B"00000000000000000000",
            ch3_tx10gstat => gt_bridge_ip_0_GT_TX3_ch_tx10gstat,
            ch3_txbufstatus(1 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txbufstatus(1 downto 0),
            ch3_txcomfinish => gt_bridge_ip_0_GT_TX3_ch_txcomfinish,
            ch3_txcominit => gt_bridge_ip_0_GT_TX3_ch_txcominit,
            ch3_txcomsas => gt_bridge_ip_0_GT_TX3_ch_txcomsas,
            ch3_txcomwake => gt_bridge_ip_0_GT_TX3_ch_txcomwake,
            ch3_txctrl0(15 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txctrl0(15 downto 0),
            ch3_txctrl1(15 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txctrl1(15 downto 0),
            ch3_txctrl2(7 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txctrl2(7 downto 0),
            ch3_txdapicodeovrden => gt_bridge_ip_0_GT_TX3_ch_txdapicodeovrden,
            ch3_txdapicodereset => gt_bridge_ip_0_GT_TX3_ch_txdapicodereset,
            ch3_txdata(127 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txdata(127 downto 0),
            ch3_txdataextendrsvd(7 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txdataextendrsvd(7 downto 0),
            ch3_txdccdone => gt_bridge_ip_0_GT_TX3_ch_txdccdone,
            ch3_txdeemph(1 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txdeemph(1 downto 0),
            ch3_txdetectrx => gt_bridge_ip_0_GT_TX3_ch_txdetectrx,
            ch3_txdiffctrl(4 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txdiffctrl(4 downto 0),
            ch3_txdlyalignerr => gt_bridge_ip_0_GT_TX3_ch_txdlyalignerr,
            ch3_txdlyalignprog => gt_bridge_ip_0_GT_TX3_ch_txdlyalignprog,
            ch3_txdlyalignreq => gt_bridge_ip_0_GT_TX3_ch_txdlyalignreq,
            ch3_txelecidle => gt_bridge_ip_0_GT_TX3_ch_txelecidle,
            ch3_txheader(5 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txheader(5 downto 0),
            ch3_txinhibit => gt_bridge_ip_0_GT_TX3_ch_txinhibit,
            ch3_txlatclk => '0',
            ch3_txmaincursor(6 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txmaincursor(6 downto 0),
            ch3_txmargin(2 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txmargin(2 downto 0),
            ch3_txmldchaindone => gt_bridge_ip_0_GT_TX3_ch_txmldchaindone,
            ch3_txmldchainreq => gt_bridge_ip_0_GT_TX3_ch_txmldchainreq,
            ch3_txmstdatapathreset => gt_bridge_ip_0_GT_TX3_ch_txmstdatapathreset,
            ch3_txmstreset => gt_bridge_ip_0_GT_TX3_ch_txmstreset,
            ch3_txmstresetdone => gt_bridge_ip_0_GT_TX3_ch_txmstresetdone,
            ch3_txoneszeros => gt_bridge_ip_0_GT_TX3_ch_txoneszeros,
            ch3_txoutclk => NLW_gt_quad_base_ch3_txoutclk_UNCONNECTED,
            ch3_txpausedelayalign => gt_bridge_ip_0_GT_TX3_ch_txpausedelayalign,
            ch3_txpcsresetmask => gt_bridge_ip_0_GT_TX3_ch_txpcsresetmask,
            ch3_txpd(1 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txpd(1 downto 0),
            ch3_txphaligndone => gt_bridge_ip_0_GT_TX3_ch_txphaligndone,
            ch3_txphalignerr => gt_bridge_ip_0_GT_TX3_ch_txphalignerr,
            ch3_txphalignoutrsvd => gt_bridge_ip_0_GT_TX3_ch_txphalignoutrsvd,
            ch3_txphalignreq => gt_bridge_ip_0_GT_TX3_ch_txphalignreq,
            ch3_txphalignresetmask(1 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txphalignresetmask(1 downto 0),
            ch3_txphdlypd => gt_bridge_ip_0_GT_TX3_ch_txphdlypd,
            ch3_txphdlyreset => gt_bridge_ip_0_GT_TX3_ch_txphdlyreset,
            ch3_txphdlyresetdone => gt_bridge_ip_0_GT_TX3_ch_txphdlyresetdone,
            ch3_txphdlytstclk => '0',
            ch3_txphsetinitdone => gt_bridge_ip_0_GT_TX3_ch_txphsetinitdone,
            ch3_txphsetinitreq => gt_bridge_ip_0_GT_TX3_ch_txphsetinitreq,
            ch3_txphshift180 => gt_bridge_ip_0_GT_TX3_ch_txphshift180,
            ch3_txphshift180done => gt_bridge_ip_0_GT_TX3_ch_txphshift180done,
            ch3_txpicodeovrden => gt_bridge_ip_0_GT_TX3_ch_txpicodeovrden,
            ch3_txpicodereset => gt_bridge_ip_0_GT_TX3_ch_txpicodereset,
            ch3_txpippmen => gt_bridge_ip_0_GT_TX3_ch_txpippmen,
            ch3_txpippmstepsize(4 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txpippmstepsize(4 downto 0),
            ch3_txpisopd => gt_bridge_ip_0_GT_TX3_ch_txpisopd,
            ch3_txpmaresetdone => gt_bridge_ip_0_GT_TX3_ch_txpmaresetdone,
            ch3_txpmaresetmask(2 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txpmaresetmask(2 downto 0),
            ch3_txpolarity => gt_bridge_ip_0_GT_TX3_ch_txpolarity,
            ch3_txpostcursor(4 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txpostcursor(4 downto 0),
            ch3_txprbsforceerr => gt_bridge_ip_0_GT_TX3_ch_txprbsforceerr,
            ch3_txprbssel(3 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txprbssel(3 downto 0),
            ch3_txprecursor(4 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txprecursor(4 downto 0),
            ch3_txprogdivreset => gt_bridge_ip_0_GT_TX3_ch_txprogdivreset,
            ch3_txprogdivresetdone => gt_bridge_ip_0_GT_TX3_ch_txprogdivresetdone,
            ch3_txrate(7 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txrate(7 downto 0),
            ch3_txresetdone => gt_bridge_ip_0_GT_TX3_ch_txresetdone,
            ch3_txresetmode(1 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txresetmode(1 downto 0),
            ch3_txsequence(6 downto 0) => gt_bridge_ip_0_GT_TX3_ch_txsequence(6 downto 0),
            ch3_txswing => gt_bridge_ip_0_GT_TX3_ch_txswing,
            ch3_txsyncallin => gt_bridge_ip_0_GT_TX3_ch_txsyncallin,
            ch3_txsyncdone => gt_bridge_ip_0_GT_TX3_ch_txsyncdone,
            ch3_txuserrdy => gt_bridge_ip_0_GT_TX3_ch_txuserrdy,
            ch3_txusrclk => bufg_gt_1_usrclk,
            correcterr => NLW_gt_quad_base_correcterr_UNCONNECTED,
            ctrlrsvdin0(15 downto 0) => B"0000000000000000",
            ctrlrsvdin1(13 downto 0) => B"00000000000000",
            ctrlrsvdout(31 downto 0) => NLW_gt_quad_base_ctrlrsvdout_UNCONNECTED(31 downto 0),
            debugtraceclk => '0',
            debugtraceready => '0',
            debugtracetdata(15 downto 0) => NLW_gt_quad_base_debugtracetdata_UNCONNECTED(15 downto 0),
            debugtracetvalid => NLW_gt_quad_base_debugtracetvalid_UNCONNECTED,
            gpi(15 downto 0) => xlc_gpi_dout(15 downto 0),
            gpo(15 downto 0) => gt_quad_base_gpo(15 downto 0),
            gtpowergood => gt_quad_base_gtpowergood,
            hsclk0_lcpllclkrsvd0 => '0',
            hsclk0_lcpllclkrsvd1 => '0',
            hsclk0_lcpllfbclklost => NLW_gt_quad_base_hsclk0_lcpllfbclklost_UNCONNECTED,
            hsclk0_lcpllfbdiv(7 downto 0) => B"00000000",
            hsclk0_lcplllock => gt_quad_base_hsclk0_lcplllock,
            hsclk0_lcpllpd => '0',
            hsclk0_lcpllrefclklost => NLW_gt_quad_base_hsclk0_lcpllrefclklost_UNCONNECTED,
            hsclk0_lcpllrefclkmonitor => NLW_gt_quad_base_hsclk0_lcpllrefclkmonitor_UNCONNECTED,
            hsclk0_lcpllrefclksel(2 downto 0) => B"001",
            hsclk0_lcpllreset => gt_bridge_ip_0_gt_pll_reset,
            hsclk0_lcpllresetbypassmode => '0',
            hsclk0_lcpllresetmask(1 downto 0) => B"11",
            hsclk0_lcpllrsvd0(7 downto 0) => B"00000000",
            hsclk0_lcpllrsvd1(7 downto 0) => B"00000000",
            hsclk0_lcpllrsvdout(7 downto 0) => NLW_gt_quad_base_hsclk0_lcpllrsvdout_UNCONNECTED(7 downto 0),
            hsclk0_lcpllsdmdata(25 downto 0) => B"01000111011100101111000001",
            hsclk0_lcpllsdmtoggle => '0',
            hsclk0_rpllclkrsvd0 => '0',
            hsclk0_rpllclkrsvd1 => '0',
            hsclk0_rpllfbclklost => NLW_gt_quad_base_hsclk0_rpllfbclklost_UNCONNECTED,
            hsclk0_rpllfbdiv(7 downto 0) => B"00000000",
            hsclk0_rplllock => gt_quad_base_hsclk0_rplllock,
            hsclk0_rpllpd => '0',
            hsclk0_rpllrefclklost => NLW_gt_quad_base_hsclk0_rpllrefclklost_UNCONNECTED,
            hsclk0_rpllrefclkmonitor => NLW_gt_quad_base_hsclk0_rpllrefclkmonitor_UNCONNECTED,
            hsclk0_rpllrefclksel(2 downto 0) => B"001",
            hsclk0_rpllreset => gt_bridge_ip_0_gt_pll_reset,
            hsclk0_rpllresetbypassmode => '0',
            hsclk0_rpllresetmask(1 downto 0) => B"11",
            hsclk0_rpllrsvd0(7 downto 0) => B"00000000",
            hsclk0_rpllrsvd1(7 downto 0) => B"00000000",
            hsclk0_rpllrsvdout(7 downto 0) => NLW_gt_quad_base_hsclk0_rpllrsvdout_UNCONNECTED(7 downto 0),
            hsclk0_rpllsdmdata(25 downto 0) => B"00010001010011010001000000",
            hsclk0_rpllsdmtoggle => '0',
            hsclk0_rxrecclkout0 => NLW_gt_quad_base_hsclk0_rxrecclkout0_UNCONNECTED,
            hsclk0_rxrecclkout1 => NLW_gt_quad_base_hsclk0_rxrecclkout1_UNCONNECTED,
            hsclk0_rxrecclksel(1 downto 0) => NLW_gt_quad_base_hsclk0_rxrecclksel_UNCONNECTED(1 downto 0),
            hsclk1_lcpllclkrsvd0 => '0',
            hsclk1_lcpllclkrsvd1 => '0',
            hsclk1_lcpllfbclklost => NLW_gt_quad_base_hsclk1_lcpllfbclklost_UNCONNECTED,
            hsclk1_lcpllfbdiv(7 downto 0) => B"00000000",
            hsclk1_lcplllock => NLW_gt_quad_base_hsclk1_lcplllock_UNCONNECTED,
            hsclk1_lcpllpd => '0',
            hsclk1_lcpllrefclklost => NLW_gt_quad_base_hsclk1_lcpllrefclklost_UNCONNECTED,
            hsclk1_lcpllrefclkmonitor => NLW_gt_quad_base_hsclk1_lcpllrefclkmonitor_UNCONNECTED,
            hsclk1_lcpllrefclksel(2 downto 0) => B"001",
            hsclk1_lcpllreset => '0',
            hsclk1_lcpllresetbypassmode => '0',
            hsclk1_lcpllresetmask(1 downto 0) => B"11",
            hsclk1_lcpllrsvd0(7 downto 0) => B"00000000",
            hsclk1_lcpllrsvd1(7 downto 0) => B"00000000",
            hsclk1_lcpllrsvdout(7 downto 0) => NLW_gt_quad_base_hsclk1_lcpllrsvdout_UNCONNECTED(7 downto 0),
            hsclk1_lcpllsdmdata(25 downto 0) => B"01000111011100101111000001",
            hsclk1_lcpllsdmtoggle => '0',
            hsclk1_rpllclkrsvd0 => '0',
            hsclk1_rpllclkrsvd1 => '0',
            hsclk1_rpllfbclklost => NLW_gt_quad_base_hsclk1_rpllfbclklost_UNCONNECTED,
            hsclk1_rpllfbdiv(7 downto 0) => B"00000000",
            hsclk1_rplllock => NLW_gt_quad_base_hsclk1_rplllock_UNCONNECTED,
            hsclk1_rpllpd => '0',
            hsclk1_rpllrefclklost => NLW_gt_quad_base_hsclk1_rpllrefclklost_UNCONNECTED,
            hsclk1_rpllrefclkmonitor => NLW_gt_quad_base_hsclk1_rpllrefclkmonitor_UNCONNECTED,
            hsclk1_rpllrefclksel(2 downto 0) => B"001",
            hsclk1_rpllreset => '0',
            hsclk1_rpllresetbypassmode => '0',
            hsclk1_rpllresetmask(1 downto 0) => B"11",
            hsclk1_rpllrsvd0(7 downto 0) => B"00000000",
            hsclk1_rpllrsvd1(7 downto 0) => B"00000000",
            hsclk1_rpllrsvdout(7 downto 0) => NLW_gt_quad_base_hsclk1_rpllrsvdout_UNCONNECTED(7 downto 0),
            hsclk1_rpllsdmdata(25 downto 0) => B"00010001010011010001000000",
            hsclk1_rpllsdmtoggle => '0',
            hsclk1_rxrecclkout0 => NLW_gt_quad_base_hsclk1_rxrecclkout0_UNCONNECTED,
            hsclk1_rxrecclkout1 => NLW_gt_quad_base_hsclk1_rxrecclkout1_UNCONNECTED,
            hsclk1_rxrecclksel(1 downto 0) => NLW_gt_quad_base_hsclk1_rxrecclksel_UNCONNECTED(1 downto 0),
            pcielinkreachtarget => '0',
            pcieltssm(5 downto 0) => B"000000",
            pipenorthin(5 downto 0) => B"000000",
            pipenorthout(5 downto 0) => NLW_gt_quad_base_pipenorthout_UNCONNECTED(5 downto 0),
            pipesouthin(5 downto 0) => B"000000",
            pipesouthout(5 downto 0) => NLW_gt_quad_base_pipesouthout_UNCONNECTED(5 downto 0),
            rcalenb => '0',
            refclk0_clktestsig => '0',
            refclk0_clktestsigint => NLW_gt_quad_base_refclk0_clktestsigint_UNCONNECTED,
            refclk0_gtrefclkpd => '0',
            refclk0_gtrefclkpdint => NLW_gt_quad_base_refclk0_gtrefclkpdint_UNCONNECTED,
            refclk1_clktestsig => '0',
            refclk1_clktestsigint => NLW_gt_quad_base_refclk1_clktestsigint_UNCONNECTED,
            refclk1_gtrefclkpd => '0',
            refclk1_gtrefclkpdint => NLW_gt_quad_base_refclk1_gtrefclkpdint_UNCONNECTED,
            resetdone_northin(1 downto 0) => B"00",
            resetdone_northout(1 downto 0) => NLW_gt_quad_base_resetdone_northout_UNCONNECTED(1 downto 0),
            resetdone_southin(1 downto 0) => B"00",
            resetdone_southout(1 downto 0) => NLW_gt_quad_base_resetdone_southout_UNCONNECTED(1 downto 0),
            rxmarginclk => '0',
            rxmarginreqack => NLW_gt_quad_base_rxmarginreqack_UNCONNECTED,
            rxmarginreqcmd(3 downto 0) => B"0000",
            rxmarginreqlanenum(1 downto 0) => B"00",
            rxmarginreqpayld(7 downto 0) => B"00000000",
            rxmarginreqreq => '0',
            rxmarginresack => '0',
            rxmarginrescmd(3 downto 0) => NLW_gt_quad_base_rxmarginrescmd_UNCONNECTED(3 downto 0),
            rxmarginreslanenum(1 downto 0) => NLW_gt_quad_base_rxmarginreslanenum_UNCONNECTED(1 downto 0),
            rxmarginrespayld(7 downto 0) => NLW_gt_quad_base_rxmarginrespayld_UNCONNECTED(7 downto 0),
            rxmarginresreq => NLW_gt_quad_base_rxmarginresreq_UNCONNECTED,
            rxn(3 downto 0) => gt_quad_base_GT_Serial_GRX_N(3 downto 0),
            rxp(3 downto 0) => gt_quad_base_GT_Serial_GRX_P(3 downto 0),
            rxpinorthin(3 downto 0) => B"0000",
            rxpinorthout(3 downto 0) => NLW_gt_quad_base_rxpinorthout_UNCONNECTED(3 downto 0),
            rxpisouthin(3 downto 0) => B"0000",
            rxpisouthout(3 downto 0) => NLW_gt_quad_base_rxpisouthout_UNCONNECTED(3 downto 0),
            trigackin0 => NLW_gt_quad_base_trigackin0_UNCONNECTED,
            trigackout0 => '0',
            trigin0 => '0',
            trigout0 => NLW_gt_quad_base_trigout0_UNCONNECTED,
            txn(3 downto 0) => gt_quad_base_GT_Serial_GTX_N(3 downto 0),
            txp(3 downto 0) => gt_quad_base_GT_Serial_GTX_P(3 downto 0),
            txpinorthin(3 downto 0) => B"0000",
            txpinorthout(3 downto 0) => NLW_gt_quad_base_txpinorthout_UNCONNECTED(3 downto 0),
            txpisouthin(3 downto 0) => B"0000",
            txpisouthout(3 downto 0) => NLW_gt_quad_base_txpisouthout_UNCONNECTED(3 downto 0),
            ubenable => '1',
            ubinterrupt => NLW_gt_quad_base_ubinterrupt_UNCONNECTED,
            ubintr(11 downto 0) => B"000000000000",
            ubiolmbrst => '0',
            ubmbrst => '0',
            ubrxuart => '0',
            ubtxuart => NLW_gt_quad_base_ubtxuart_UNCONNECTED,
            uncorrecterr => NLW_gt_quad_base_uncorrecterr_UNCONNECTED
        );
    url_gpo: component transceiver_versal_lpgbt_url_gpo_0
        port map (
            Op1(3 downto 0) => xlc_gpo_dout(3 downto 0),
            Res => url_gpo_Res
        );
    urlp: component transceiver_versal_lpgbt_urlp_0
        port map (
            Op1(0) => xlcp_dout(0),
            Res => urlp_Res
        );
    util_ds_buf: component transceiver_versal_lpgbt_util_ds_buf_0
        port map (
            IBUF_DS_CEB(0) => '0',
            IBUF_DS_N(0) => gt_bridge_ip_0_diff_gt_ref_clock_1_CLK_N(0),
            IBUF_DS_ODIV2(0) => NLW_util_ds_buf_IBUF_DS_ODIV2_UNCONNECTED(0),
            IBUF_DS_P(0) => gt_bridge_ip_0_diff_gt_ref_clock_1_CLK_P(0),
            IBUF_OUT(0) => util_ds_buf_IBUF_OUT(0)
        );
    util_ds_buf1: component transceiver_versal_lpgbt_util_ds_buf1_0
        port map (
            IBUF_DS_CEB(0) => '0',
            IBUF_DS_N(0) => CLK_IN_D_0_1_CLK_N(0),
            IBUF_DS_ODIV2(0) => NLW_util_ds_buf1_IBUF_DS_ODIV2_UNCONNECTED(0),
            IBUF_DS_P(0) => CLK_IN_D_0_1_CLK_P(0),
            IBUF_OUT(0) => util_ds_buf1_IBUF_OUT(0)
        );
    util_reduced_logic_0: component transceiver_versal_lpgbt_util_reduced_logic_0_0
        port map (
            Op1(3 downto 0) => xlconcat_1_dout(3 downto 0),
            Res => util_reduced_logic_0_Res
        );
    xlc_gpi: component transceiver_versal_lpgbt_xlc_gpi_0
        port map (
            In0(0) => gt_bridge_ip_0_gpi_out,
            In1(0) => gt_bridge_ip_0_gpi_out,
            In10(0) => '0',
            In11(0) => '0',
            In12(0) => '0',
            In13(0) => '0',
            In14(0) => '0',
            In15(0) => '0',
            In2(0) => gt_bridge_ip_0_gpi_out,
            In3(0) => gt_bridge_ip_0_gpi_out,
            In4(0) => '0',
            In5(0) => '0',
            In6(0) => '0',
            In7(0) => '0',
            In8(0) => '0',
            In9(0) => '0',
            dout(15 downto 0) => xlc_gpi_dout(15 downto 0)
        );
    xlc_gpo: component transceiver_versal_lpgbt_xlc_gpo_0
        port map (
            In0(3 downto 0) => xls_gpo_Dout(3 downto 0),
            dout(3 downto 0) => xlc_gpo_dout(3 downto 0)
        );
    xlconcat_0: component transceiver_versal_lpgbt_xlconcat_0_0
        port map (
            In0(0) => gt_quad_base_ch0_phystatus,
            In1(0) => gt_quad_base_ch1_phystatus,
            In2(0) => gt_quad_base_ch2_phystatus,
            In3(0) => gt_quad_base_ch3_phystatus,
            dout(3 downto 0) => xlconcat_0_dout(3 downto 0)
        );
    xlconcat_1: component transceiver_versal_lpgbt_xlconcat_1_0
        port map (
            In0(0) => gt_quad_base_ch0_iloresetdone,
            In1(0) => gt_quad_base_ch1_iloresetdone,
            In2(0) => gt_quad_base_ch2_iloresetdone,
            In3(0) => gt_quad_base_ch3_iloresetdone,
            dout(3 downto 0) => xlconcat_1_dout(3 downto 0)
        );
    xlcp: component transceiver_versal_lpgbt_xlcp_0
        port map (
            In0(0) => gt_quad_base_gtpowergood,
            dout(0) => xlcp_dout(0)
        );
    xls_gpo: component transceiver_versal_lpgbt_xls_gpo_0
        port map (
            Din(15 downto 0) => gt_quad_base_gpo(15 downto 0),
            Dout(3 downto 0) => xls_gpo_Dout(3 downto 0)
        );
end STRUCTURE;

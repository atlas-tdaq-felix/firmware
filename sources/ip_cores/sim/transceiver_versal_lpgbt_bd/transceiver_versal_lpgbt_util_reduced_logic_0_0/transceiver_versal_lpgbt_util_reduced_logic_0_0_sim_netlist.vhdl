-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Thu Apr 13 14:41:06 2023
-- Host        : PC-22-023 running 64-bit Ubuntu 22.04.2 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/melvinl/felix/firmware/Projects/FLX182_FELIX/FLX182_FELIX.gen/sources_1/bd/BNL182/ip/transceiver_versal_lpgbt_util_reduced_logic_0_0/transceiver_versal_lpgbt_util_reduced_logic_0_0_sim_netlist.vhdl
-- Design      : transceiver_versal_lpgbt_util_reduced_logic_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcvm1802-vsva2197-1MP-e-S
-- --------------------------------------------------------------------------------
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
    use UNISIM.VCOMPONENTS.ALL;
entity transceiver_versal_lpgbt_util_reduced_logic_0_0_util_reduced_logic_v2_0_4_util_reduced_logic is
    port (
        Res : out STD_LOGIC;
        Op1 : in STD_LOGIC_VECTOR ( 3 downto 0 )
    );
    attribute ORIG_REF_NAME : string;
    attribute ORIG_REF_NAME of transceiver_versal_lpgbt_util_reduced_logic_0_0_util_reduced_logic_v2_0_4_util_reduced_logic : entity is "util_reduced_logic_v2_0_4_util_reduced_logic";
end transceiver_versal_lpgbt_util_reduced_logic_0_0_util_reduced_logic_v2_0_4_util_reduced_logic;

architecture STRUCTURE of transceiver_versal_lpgbt_util_reduced_logic_0_0_util_reduced_logic_v2_0_4_util_reduced_logic is
begin
    Res0: unisim.vcomponents.LUT4
        generic map(
            INIT => X"8000"
        )
        port map (
            I0 => Op1(1),
            I1 => Op1(0),
            I2 => Op1(3),
            I3 => Op1(2),
            O => Res
        );
end STRUCTURE;
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
    use UNISIM.VCOMPONENTS.ALL;
entity transceiver_versal_lpgbt_util_reduced_logic_0_0 is
    port (
        Op1 : in STD_LOGIC_VECTOR ( 3 downto 0 );
        Res : out STD_LOGIC
    );
    attribute NotValidForBitStream : boolean;
    attribute NotValidForBitStream of transceiver_versal_lpgbt_util_reduced_logic_0_0 : entity is true;
    attribute CHECK_LICENSE_TYPE : string;
    attribute CHECK_LICENSE_TYPE of transceiver_versal_lpgbt_util_reduced_logic_0_0 : entity is "transceiver_versal_lpgbt_util_reduced_logic_0_0,util_reduced_logic_v2_0_4_util_reduced_logic,{}";
    attribute DowngradeIPIdentifiedWarnings : string;
    attribute DowngradeIPIdentifiedWarnings of transceiver_versal_lpgbt_util_reduced_logic_0_0 : entity is "yes";
    attribute X_CORE_INFO : string;
    attribute X_CORE_INFO of transceiver_versal_lpgbt_util_reduced_logic_0_0 : entity is "util_reduced_logic_v2_0_4_util_reduced_logic,Vivado 2022.2";
end transceiver_versal_lpgbt_util_reduced_logic_0_0;

architecture STRUCTURE of transceiver_versal_lpgbt_util_reduced_logic_0_0 is
begin
    inst: entity work.transceiver_versal_lpgbt_util_reduced_logic_0_0_util_reduced_logic_v2_0_4_util_reduced_logic
        port map (
            Op1(3 downto 0) => Op1(3 downto 0),
            Res => Res
        );
end STRUCTURE;

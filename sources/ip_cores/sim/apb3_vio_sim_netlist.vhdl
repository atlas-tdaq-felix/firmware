-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Fri Apr 19 09:49:55 2024
-- Host        : PC-22-023 running 64-bit Ubuntu 22.04.4 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/melvinl/firmware_temp/Projects/FLX182_FELIX/FLX182_FELIX.gen/sources_1/ip/apb3_vio/apb3_vio_stub.vhdl
-- Design      : apb3_vio
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcvm1802-vsva2197-1MP-e-S
-- --------------------------------------------------------------------------------
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

entity apb3_vio is
    Port (
        probe_in0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
        probe_out0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
        probe_out1 : out STD_LOGIC_VECTOR ( 31 downto 0 );
        probe_out2 : out STD_LOGIC_VECTOR ( 0 to 0 );
        probe_out3 : out STD_LOGIC_VECTOR ( 0 to 0 );
        probe_out4 : out STD_LOGIC_VECTOR ( 0 to 0 );
        clk : in STD_LOGIC
    );

end apb3_vio;


architecture stub of apb3_vio is
begin
    probe_out0 <= (others => '0');
    probe_out1 <= (others => '0');
    probe_out2 <= (others => '0');
    probe_out3 <= (others => '0');
    probe_out4 <= (others => '0');
end;

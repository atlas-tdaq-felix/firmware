
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

entity dsp_counter is
    port (
        CLK : in STD_LOGIC;
        CE : in STD_LOGIC;
        SCLR : in STD_LOGIC;
        UP : in STD_LOGIC;
        LOAD : in STD_LOGIC;
        L : in STD_LOGIC_VECTOR ( 47 downto 0 );
        Q : out STD_LOGIC_VECTOR ( 47 downto 0 )
    );
end dsp_counter;

architecture STRUCTURE of dsp_counter is
    signal CNT: std_logic_vector(47 downto 0);
begin

    dsp_counter_proc: process(CLK, SCLR)
    begin
        if SCLR = '1' then
            CNT <= (others => '0');
        elsif rising_edge(CLK) then
            if CE = '1' then
                if UP = '1' then
                    CNT <= CNT + 1;
                else
                    CNT <= CNT - 1;
                end if;
                if LOAD = '1' then
                    CNT <= L;
                end if;
            end if;
        end if;
    end process;
    Q <= CNT;
end STRUCTURE;

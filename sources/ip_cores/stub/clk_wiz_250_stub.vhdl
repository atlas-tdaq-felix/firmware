-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Mon Sep 28 15:30:00 2020
-- Host        : tarfa running 64-bit CentOS Linux release 7.6.1810 (Core)
-- Command     : write_vhdl -force -mode synth_stub
--               /localstore/et/franss/felix/firmware2/Projects/FLX712_FELIX/FLX712_FELIX.srcs/sources_1/ip/clk_wiz_250/clk_wiz_250_stub.vhdl
-- Design      : clk_wiz_250
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku115-flvf1924-2-e
-- --------------------------------------------------------------------------------
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

entity clk_wiz_250 is
    Port (
        clk_in40_2 : in STD_LOGIC; -- @suppress "Unused port: clk_in40_2 is not used in work.clk_wiz_250(stub)"
        clk_in_sel : in STD_LOGIC; -- @suppress "Unused port: clk_in_sel is not used in work.clk_wiz_250(stub)"
        clk250 : out STD_LOGIC; -- @suppress "Unused port: clk250 is not used in work.clk_wiz_250(stub)"
        clk365 : out STD_LOGIC; -- @suppress "Unused port: clk365 is not used in work.clk_wiz_250(stub)"
        reset : in STD_LOGIC; -- @suppress "Unused port: reset is not used in work.clk_wiz_250(stub)"
        locked : out STD_LOGIC; -- @suppress "Unused port: locked is not used in work.clk_wiz_250(stub)"
        clk_in40_1 : in STD_LOGIC -- @suppress "Unused port: clk_in40_1 is not used in work.clk_wiz_250(stub)"
    );

end clk_wiz_250;

architecture stub of clk_wiz_250 is
    attribute syn_black_box : boolean;
    attribute black_box_pad_pin : string;
    attribute syn_black_box of stub : architecture is true;
    attribute black_box_pad_pin of stub : architecture is "clk_in40_2,clk_in_sel,clk250,clk365,reset,locked,clk_in40_1";
begin
end;

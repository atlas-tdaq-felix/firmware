-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Mon Sep 28 16:48:22 2020
-- Host        : tarfa running 64-bit CentOS Linux release 7.6.1810 (Core)
-- Command     : write_vhdl -force -mode synth_stub
--               /localstore/et/franss/felix/firmware2/Projects/FLX712_FELIX/FLX712_FELIX.srcs/sources_1/ip/system_management_wiz_0/system_management_wiz_0_stub.vhdl
-- Design      : system_management_wiz_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku115-flvf1924-2-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity system_management_wiz_0 is
  Port ( 
    daddr_in : in STD_LOGIC_VECTOR ( 7 downto 0 );            --@suppress
    den_in : in STD_LOGIC;                                    --@suppress
    di_in : in STD_LOGIC_VECTOR ( 15 downto 0 );              --@suppress
    dwe_in : in STD_LOGIC;                                    --@suppress
    do_out : out STD_LOGIC_VECTOR ( 15 downto 0 );            --@suppress
    drdy_out : out STD_LOGIC;                                 --@suppress
    dclk_in : in STD_LOGIC;                                   --@suppress
    reset_in : in STD_LOGIC;                                  --@suppress
    busy_out : out STD_LOGIC;                                 --@suppress
    channel_out : out STD_LOGIC_VECTOR ( 5 downto 0 );        --@suppress
    eoc_out : out STD_LOGIC;                                  --@suppress
    eos_out : out STD_LOGIC;                                  --@suppress
    alarm_out : out STD_LOGIC                                 --@suppress
  );

end system_management_wiz_0;

architecture stub of system_management_wiz_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "daddr_in[7:0],den_in,di_in[15:0],dwe_in,do_out[15:0],drdy_out,dclk_in,reset_in,busy_out,channel_out[5:0],eoc_out,eos_out,alarm_out";
begin
end;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity BUFGMUX is
    generic (
      CLK_SEL_TYPE : string := "SYNC");--@suppress
    port (                             --@suppress
      O  : out std_ulogic := '0';      --@suppress
      I0 : in  std_ulogic := '0';      --@suppress
      I1 : in  std_ulogic := '0';      --@suppress
      S  : in  std_ulogic := '0');     --@suppress
end BUFGMUX;

architecture stub of BUFGMUX is
begin
end;

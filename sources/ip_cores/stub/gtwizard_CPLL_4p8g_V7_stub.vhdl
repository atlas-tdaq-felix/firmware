-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
-- Date        : Mon Mar  9 16:45:10 2020
-- Host        : tarfa running 64-bit CentOS Linux release 7.6.1810 (Core)
-- Command     : write_vhdl -force -mode synth_stub
--               /localstore/et/franss/felix/firmware2/Projects/FLX709_FELIX/FLX709_FELIX.srcs/sources_1/ip/gtwizard_CPLL_4p8g_V7/gtwizard_CPLL_4p8g_V7_stub.vhdl
-- Design      : gtwizard_CPLL_4p8g_V7
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7vx690tffg1761-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity gtwizard_CPLL_4p8g_V7 is
  Port ( 
    SYSCLK_IN : in STD_LOGIC;                                         --@suppress
    SOFT_RESET_TX_IN : in STD_LOGIC;                                  --@suppress
    SOFT_RESET_RX_IN : in STD_LOGIC;                                  --@suppress
    DONT_RESET_ON_DATA_ERROR_IN : in STD_LOGIC;                       --@suppress
    GT0_TX_FSM_RESET_DONE_OUT : out STD_LOGIC;                        --@suppress
    GT0_RX_FSM_RESET_DONE_OUT : out STD_LOGIC;                        --@suppress
    GT0_DATA_VALID_IN : in STD_LOGIC;                                 --@suppress
    gt0_cpllfbclklost_out : out STD_LOGIC;                            --@suppress
    gt0_cplllock_out : out STD_LOGIC;                                 --@suppress
    gt0_cplllockdetclk_in : in STD_LOGIC;                             --@suppress
    gt0_cpllreset_in : in STD_LOGIC;                                  --@suppress
    gt0_gtrefclk0_in : in STD_LOGIC;                                  --@suppress
    gt0_gtrefclk1_in : in STD_LOGIC;                                  --@suppress
    gt0_drpaddr_in : in STD_LOGIC_VECTOR ( 8 downto 0 );              --@suppress
    gt0_drpclk_in : in STD_LOGIC;                                     --@suppress
    gt0_drpdi_in : in STD_LOGIC_VECTOR ( 15 downto 0 );               --@suppress
    gt0_drpdo_out : out STD_LOGIC_VECTOR ( 15 downto 0 );             --@suppress
    gt0_drpen_in : in STD_LOGIC;                                      --@suppress
    gt0_drprdy_out : out STD_LOGIC;                                   --@suppress
    gt0_drpwe_in : in STD_LOGIC;                                      --@suppress
    gt0_loopback_in : in STD_LOGIC_VECTOR ( 2 downto 0 );             --@suppress
    gt0_eyescanreset_in : in STD_LOGIC;                               --@suppress
    gt0_rxuserrdy_in : in STD_LOGIC;                                  --@suppress
    gt0_eyescandataerror_out : out STD_LOGIC;                         --@suppress
    gt0_eyescantrigger_in : in STD_LOGIC;                             --@suppress
    gt0_rxcdrhold_in : in STD_LOGIC;                                  --@suppress
    gt0_rxslide_in : in STD_LOGIC;                                    --@suppress
    gt0_dmonitorout_out : out STD_LOGIC_VECTOR ( 14 downto 0 );       --@suppress
    gt0_rxusrclk_in : in STD_LOGIC;                                   --@suppress
    gt0_rxusrclk2_in : in STD_LOGIC;                                  --@suppress
    gt0_rxdata_out : out STD_LOGIC_VECTOR ( 19 downto 0 );            --@suppress
    gt0_gthrxn_in : in STD_LOGIC;                                     --@suppress
    gt0_rxmonitorout_out : out STD_LOGIC_VECTOR ( 6 downto 0 );       --@suppress
    gt0_rxmonitorsel_in : in STD_LOGIC_VECTOR ( 1 downto 0 );         --@suppress
    gt0_rxoutclk_out : out STD_LOGIC;                                 --@suppress
    gt0_rxoutclkfabric_out : out STD_LOGIC;                           --@suppress
    gt0_gtrxreset_in : in STD_LOGIC;                                  --@suppress
    gt0_rxpolarity_in : in STD_LOGIC;                                 --@suppress
    gt0_gthrxp_in : in STD_LOGIC;                                     --@suppress
    gt0_rxresetdone_out : out STD_LOGIC;                              --@suppress
    gt0_gttxreset_in : in STD_LOGIC;                                  --@suppress
    gt0_txuserrdy_in : in STD_LOGIC;                                  --@suppress
    gt0_txusrclk_in : in STD_LOGIC;                                   --@suppress
    gt0_txusrclk2_in : in STD_LOGIC;                                  --@suppress
    gt0_txdata_in : in STD_LOGIC_VECTOR ( 19 downto 0 );              --@suppress
    gt0_gthtxn_out : out STD_LOGIC;                                   --@suppress
    gt0_gthtxp_out : out STD_LOGIC;                                   --@suppress
    gt0_txoutclk_out : out STD_LOGIC;                                 --@suppress
    gt0_txoutclkfabric_out : out STD_LOGIC;                           --@suppress
    gt0_txoutclkpcs_out : out STD_LOGIC;                              --@suppress
    gt0_txresetdone_out : out STD_LOGIC;                              --@suppress
    gt0_txpolarity_in : in STD_LOGIC;                                 --@suppress
    GT0_QPLLOUTCLK_IN : in STD_LOGIC;                                 --@suppress
    GT0_QPLLOUTREFCLK_IN : in STD_LOGIC                               --@suppress
  );

end gtwizard_CPLL_4p8g_V7;

architecture stub of gtwizard_CPLL_4p8g_V7 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "SYSCLK_IN,SOFT_RESET_TX_IN,SOFT_RESET_RX_IN,DONT_RESET_ON_DATA_ERROR_IN,GT0_TX_FSM_RESET_DONE_OUT,GT0_RX_FSM_RESET_DONE_OUT,GT0_DATA_VALID_IN,gt0_cpllfbclklost_out,gt0_cplllock_out,gt0_cplllockdetclk_in,gt0_cpllreset_in,gt0_gtrefclk0_in,gt0_gtrefclk1_in,gt0_drpaddr_in[8:0],gt0_drpclk_in,gt0_drpdi_in[15:0],gt0_drpdo_out[15:0],gt0_drpen_in,gt0_drprdy_out,gt0_drpwe_in,gt0_loopback_in[2:0],gt0_eyescanreset_in,gt0_rxuserrdy_in,gt0_eyescandataerror_out,gt0_eyescantrigger_in,gt0_rxcdrhold_in,gt0_rxslide_in,gt0_dmonitorout_out[14:0],gt0_rxusrclk_in,gt0_rxusrclk2_in,gt0_rxdata_out[19:0],gt0_gthrxn_in,gt0_rxmonitorout_out[6:0],gt0_rxmonitorsel_in[1:0],gt0_rxoutclk_out,gt0_rxoutclkfabric_out,gt0_gtrxreset_in,gt0_rxpolarity_in,gt0_gthrxp_in,gt0_rxresetdone_out,gt0_gttxreset_in,gt0_txuserrdy_in,gt0_txusrclk_in,gt0_txusrclk2_in,gt0_txdata_in[19:0],gt0_gthtxn_out,gt0_gthtxp_out,gt0_txoutclk_out,gt0_txoutclkfabric_out,gt0_txoutclkpcs_out,gt0_txresetdone_out,gt0_txpolarity_in,GT0_QPLLOUTCLK_IN,GT0_QPLLOUTREFCLK_IN";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "gtwizard_CPLL_4p8g_V7,gtwizard_v3_6_9,{protocol_file=Start_from_scratch}";
begin
end;

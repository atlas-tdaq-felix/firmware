-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Mon Sep 28 16:44:46 2020
-- Host        : tarfa running 64-bit CentOS Linux release 7.6.1810 (Core)
-- Command     : write_vhdl -force -mode synth_stub
--               /localstore/et/franss/felix/firmware2/Projects/FLX712_FELIX/FLX712_FELIX.srcs/sources_1/ip/clk_wiz_regmap/clk_wiz_regmap_stub.vhdl
-- Design      : clk_wiz_regmap
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku115-flvf1924-2-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity clk_wiz_regmap is
  Port ( 
    clk_out25 : out STD_LOGIC;--@suppress
    reset : in STD_LOGIC;     --@suppress
    locked : out STD_LOGIC;   --@suppress
    clk_in1 : in STD_LOGIC    --@suppress
  );

end clk_wiz_regmap;

architecture stub of clk_wiz_regmap is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk_out25,reset,locked,clk_in1";
begin
end;

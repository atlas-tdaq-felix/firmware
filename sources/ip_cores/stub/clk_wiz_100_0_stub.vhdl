-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Mon Sep 28 15:29:57 2020
-- Host        : tarfa running 64-bit CentOS Linux release 7.6.1810 (Core)
-- Command     : write_vhdl -force -mode synth_stub
--               /localstore/et/franss/felix/firmware2/Projects/FLX712_FELIX/FLX712_FELIX.srcs/sources_1/ip/clk_wiz_100_0/clk_wiz_100_0_stub.vhdl
-- Design      : clk_wiz_100_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku115-flvf1924-2-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity clk_wiz_100_0 is
  Port ( 
    clk40 : out STD_LOGIC; -- @suppress "Unused port: clk40 is not used in work.clk_wiz_100_0(stub)"
    clk10 : out STD_LOGIC; -- @suppress "Unused port: clk10 is not used in work.clk_wiz_100_0(stub)"
    reset : in STD_LOGIC; -- @suppress "Unused port: reset is not used in work.clk_wiz_100_0(stub)"
    locked : out STD_LOGIC; -- @suppress "Unused port: locked is not used in work.clk_wiz_100_0(stub)"
    clk_in1_p : in STD_LOGIC; -- @suppress "Unused port: clk_in1_p is not used in work.clk_wiz_100_0(stub)"
    clk_in1_n : in STD_LOGIC -- @suppress "Unused port: clk_in1_n is not used in work.clk_wiz_100_0(stub)"
  );

end clk_wiz_100_0;

architecture stub of clk_wiz_100_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk40,clk10,reset,locked,clk_in1_p,clk_in1_n";
begin
end;

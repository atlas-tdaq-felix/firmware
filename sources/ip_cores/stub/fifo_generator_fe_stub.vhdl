library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity  fifo_generator_fe IS
  PORT (
    rst : IN STD_LOGIC;                       --@suppress
    wr_clk : IN STD_LOGIC;                    --@suppress
    rd_clk : IN STD_LOGIC;                    --@suppress
    din : IN STD_LOGIC_VECTOR(35 DOWNTO 0);   --@suppress
    wr_en : IN STD_LOGIC;                     --@suppress
    rd_en : IN STD_LOGIC;                     --@suppress
    dout : OUT STD_LOGIC_VECTOR(35 DOWNTO 0); --@suppress
    full : OUT STD_LOGIC;                     --@suppress
    empty : OUT STD_LOGIC;                    --@suppress
    prog_full : OUT STD_LOGIC;                --@suppress
    prog_empty : OUT STD_LOGIC;               --@suppress
    wr_rst_busy : OUT STD_LOGIC;              --@suppress
    rd_rst_busy : OUT STD_LOGIC               --@suppress
  );
END fifo_generator_fe;

architecture stub of fifo_generator_fe is
begin
end;

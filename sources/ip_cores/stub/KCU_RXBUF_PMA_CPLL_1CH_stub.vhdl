-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Mon Sep 28 16:41:45 2020
-- Host        : tarfa running 64-bit CentOS Linux release 7.6.1810 (Core)
-- Command     : write_vhdl -force -mode synth_stub
--               /localstore/et/franss/felix/firmware2/Projects/FLX712_FELIX/FLX712_FELIX.srcs/sources_1/ip/KCU_RXBUF_PMA_CPLL_1CH/KCU_RXBUF_PMA_CPLL_1CH_stub.vhdl
-- Design      : KCU_RXBUF_PMA_CPLL_1CH
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku115-flvf1924-2-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity KCU_RXBUF_PMA_CPLL_1CH is
  Port ( 
    gtwiz_userclk_tx_active_in : in STD_LOGIC_VECTOR ( 0 to 0 );             --@suppress
    gtwiz_userclk_rx_active_in : in STD_LOGIC_VECTOR ( 0 to 0 );             --@suppress
    gtwiz_buffbypass_tx_reset_in : in STD_LOGIC_VECTOR ( 0 to 0 );           --@suppress
    gtwiz_buffbypass_tx_start_user_in : in STD_LOGIC_VECTOR ( 0 to 0 );      --@suppress
    gtwiz_buffbypass_tx_done_out : out STD_LOGIC_VECTOR ( 0 to 0 );          --@suppress
    gtwiz_buffbypass_tx_error_out : out STD_LOGIC_VECTOR ( 0 to 0 );         --@suppress
    gtwiz_reset_clk_freerun_in : in STD_LOGIC_VECTOR ( 0 to 0 );             --@suppress
    gtwiz_reset_all_in : in STD_LOGIC_VECTOR ( 0 to 0 );                     --@suppress
    gtwiz_reset_tx_pll_and_datapath_in : in STD_LOGIC_VECTOR ( 0 to 0 );     --@suppress
    gtwiz_reset_tx_datapath_in : in STD_LOGIC_VECTOR ( 0 to 0 );             --@suppress
    gtwiz_reset_rx_pll_and_datapath_in : in STD_LOGIC_VECTOR ( 0 to 0 );     --@suppress
    gtwiz_reset_rx_datapath_in : in STD_LOGIC_VECTOR ( 0 to 0 );             --@suppress
    gtwiz_reset_rx_cdr_stable_out : out STD_LOGIC_VECTOR ( 0 to 0 );         --@suppress
    gtwiz_reset_tx_done_out : out STD_LOGIC_VECTOR ( 0 to 0 );               --@suppress
    gtwiz_reset_rx_done_out : out STD_LOGIC_VECTOR ( 0 to 0 );               --@suppress
    gtwiz_userdata_tx_in : in STD_LOGIC_VECTOR ( 19 downto 0 );              --@suppress
    gtwiz_userdata_rx_out : out STD_LOGIC_VECTOR ( 19 downto 0 );            --@suppress
    cplllockdetclk_in : in STD_LOGIC_VECTOR ( 0 to 0 );                      --@suppress
    cpllreset_in : in STD_LOGIC_VECTOR ( 0 to 0 );                           --@suppress
    drpclk_in : in STD_LOGIC_VECTOR ( 0 to 0 );                              --@suppress
    gtgrefclk_in : in STD_LOGIC_VECTOR ( 0 to 0 );                           --@suppress
    gthrxn_in : in STD_LOGIC_VECTOR ( 0 to 0 );                              --@suppress
    gthrxp_in : in STD_LOGIC_VECTOR ( 0 to 0 );                              --@suppress
    gtrefclk0_in : in STD_LOGIC_VECTOR ( 0 to 0 );                           --@suppress
    loopback_in : in STD_LOGIC_VECTOR ( 2 downto 0 );                        --@suppress
    rxcdrhold_in : in STD_LOGIC_VECTOR ( 0 to 0 );                           --@suppress
    rxcdrreset_in : in STD_LOGIC_VECTOR ( 0 to 0 );                          --@suppress
    rxcommadeten_in : in STD_LOGIC_VECTOR ( 0 to 0 );                        --@suppress
    rxmcommaalignen_in : in STD_LOGIC_VECTOR ( 0 to 0 );                     --@suppress
    rxpcommaalignen_in : in STD_LOGIC_VECTOR ( 0 to 0 );                     --@suppress
    rxpolarity_in : in STD_LOGIC_VECTOR ( 0 to 0 );                          --@suppress
    rxslide_in : in STD_LOGIC_VECTOR ( 0 to 0 );                             --@suppress
    rxusrclk_in : in STD_LOGIC_VECTOR ( 0 to 0 );                            --@suppress
    rxusrclk2_in : in STD_LOGIC_VECTOR ( 0 to 0 );                           --@suppress
    txpolarity_in : in STD_LOGIC_VECTOR ( 0 to 0 );                          --@suppress
    txusrclk_in : in STD_LOGIC_VECTOR ( 0 to 0 );                            --@suppress
    txusrclk2_in : in STD_LOGIC_VECTOR ( 0 to 0 );                           --@suppress
    cpllfbclklost_out : out STD_LOGIC_VECTOR ( 0 to 0 );                     --@suppress
    cplllock_out : out STD_LOGIC_VECTOR ( 0 to 0 );                          --@suppress
    gthtxn_out : out STD_LOGIC_VECTOR ( 0 to 0 );                            --@suppress
    gthtxp_out : out STD_LOGIC_VECTOR ( 0 to 0 );                            --@suppress
    gtpowergood_out : out STD_LOGIC_VECTOR ( 0 to 0 );                       --@suppress
    rxbyteisaligned_out : out STD_LOGIC_VECTOR ( 0 to 0 );                   --@suppress
    rxbyterealign_out : out STD_LOGIC_VECTOR ( 0 to 0 );                     --@suppress
    rxcdrlock_out : out STD_LOGIC_VECTOR ( 0 to 0 );                         --@suppress
    rxcommadet_out : out STD_LOGIC_VECTOR ( 0 to 0 );                        --@suppress
    rxdata_out : out STD_LOGIC_VECTOR ( 127 downto 0 );                      --@suppress
    rxoutclk_out : out STD_LOGIC_VECTOR ( 0 to 0 );                          --@suppress
    rxpmaresetdone_out : out STD_LOGIC_VECTOR ( 0 to 0 );                    --@suppress
    rxresetdone_out : out STD_LOGIC_VECTOR ( 0 to 0 );                       --@suppress
    txoutclk_out : out STD_LOGIC_VECTOR ( 0 to 0 );                          --@suppress
    txpmaresetdone_out : out STD_LOGIC_VECTOR ( 0 to 0 );                    --@suppress
    txresetdone_out : out STD_LOGIC_VECTOR ( 0 to 0 )                        --@suppress
  );

end KCU_RXBUF_PMA_CPLL_1CH;

architecture stub of KCU_RXBUF_PMA_CPLL_1CH is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "gtwiz_userclk_tx_active_in[0:0],gtwiz_userclk_rx_active_in[0:0],gtwiz_buffbypass_tx_reset_in[0:0],gtwiz_buffbypass_tx_start_user_in[0:0],gtwiz_buffbypass_tx_done_out[0:0],gtwiz_buffbypass_tx_error_out[0:0],gtwiz_reset_clk_freerun_in[0:0],gtwiz_reset_all_in[0:0],gtwiz_reset_tx_pll_and_datapath_in[0:0],gtwiz_reset_tx_datapath_in[0:0],gtwiz_reset_rx_pll_and_datapath_in[0:0],gtwiz_reset_rx_datapath_in[0:0],gtwiz_reset_rx_cdr_stable_out[0:0],gtwiz_reset_tx_done_out[0:0],gtwiz_reset_rx_done_out[0:0],gtwiz_userdata_tx_in[19:0],gtwiz_userdata_rx_out[19:0],cplllockdetclk_in[0:0],cpllreset_in[0:0],drpclk_in[0:0],gtgrefclk_in[0:0],gthrxn_in[0:0],gthrxp_in[0:0],gtrefclk0_in[0:0],loopback_in[2:0],rxcdrhold_in[0:0],rxcdrreset_in[0:0],rxcommadeten_in[0:0],rxmcommaalignen_in[0:0],rxpcommaalignen_in[0:0],rxpolarity_in[0:0],rxslide_in[0:0],rxusrclk_in[0:0],rxusrclk2_in[0:0],txpolarity_in[0:0],txusrclk_in[0:0],txusrclk2_in[0:0],cpllfbclklost_out[0:0],cplllock_out[0:0],gthtxn_out[0:0],gthtxp_out[0:0],gtpowergood_out[0:0],rxbyteisaligned_out[0:0],rxbyterealign_out[0:0],rxcdrlock_out[0:0],rxcommadet_out[0:0],rxdata_out[127:0],rxoutclk_out[0:0],rxpmaresetdone_out[0:0],rxresetdone_out[0:0],txoutclk_out[0:0],txpmaresetdone_out[0:0],txresetdone_out[0:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "KCU_RXBUF_PMA_CPLL_1CH_gtwizard_top,Vivado 2020.1";
begin
end;

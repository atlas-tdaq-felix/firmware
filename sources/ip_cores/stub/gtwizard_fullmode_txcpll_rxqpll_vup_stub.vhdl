-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Mon Sep 28 16:44:51 2020
-- Host        : tarfa running 64-bit CentOS Linux release 7.6.1810 (Core)
-- Command     : write_vhdl -force -mode synth_stub
--               /localstore/et/franss/felix/firmware2/Projects/FLX712_FELIX/FLX712_FELIX.srcs/sources_1/ip/gtwizard_fullmode_txcpll_rxqpll_ku/gtwizard_fullmode_txcpll_rxqpll_ku_stub.vhdl
-- Design      : gtwizard_fullmode_txcpll_rxqpll_ku
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku115-flvf1924-2-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity gtwizard_fullmode_txcpll_rxqpll_vup is
  PORT (
    gtwiz_userclk_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);                --@suppress
    gtwiz_userclk_tx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);               --@suppress
    gtwiz_userclk_rx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);                --@suppress
    gtwiz_userclk_rx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);               --@suppress
    gtwiz_buffbypass_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);             --@suppress
    gtwiz_buffbypass_tx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);        --@suppress
    gtwiz_buffbypass_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);            --@suppress
    gtwiz_buffbypass_tx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);           --@suppress
    gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);               --@suppress
    gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);                       --@suppress
    gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);       --@suppress
    gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);               --@suppress
    gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);       --@suppress
    gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);               --@suppress
    gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);           --@suppress
    gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);                 --@suppress
    gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);                 --@suppress
    gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(79 DOWNTO 0);                    --@suppress
    gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);                 --@suppress
    gtrefclk01_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);                            --@suppress
    qpll1lock_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);                           --@suppress
    qpll1outclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);                         --@suppress
    qpll1outrefclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);                      --@suppress
    cplllockdetclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);                        --@suppress
    cpllreset_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);                             --@suppress
    drpclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);                                --@suppress
    gtrefclk0_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);                             --@suppress
    gtyrxn_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);                                --@suppress
    gtyrxp_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);                                --@suppress
    rx8b10ben_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);                             --@suppress
    rxcommadeten_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);                          --@suppress
    rxmcommaalignen_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);                       --@suppress
    rxpcommaalignen_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);                       --@suppress
    rxusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);                              --@suppress
    rxusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);                             --@suppress
    txusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);                              --@suppress
    txusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);                             --@suppress
    cpllfbclklost_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);                       --@suppress
    cplllock_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);                            --@suppress
    gtpowergood_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);                         --@suppress
    gtytxn_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);                              --@suppress
    gtytxp_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);                              --@suppress
    rxbyteisaligned_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);                     --@suppress
    rxbyterealign_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);                       --@suppress
    rxcdrlock_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);                           --@suppress
    rxcommadet_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);                          --@suppress
    rxctrl0_out : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);                            --@suppress
    rxctrl1_out : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);                            --@suppress
    rxctrl2_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);                            --@suppress
    rxctrl3_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);                            --@suppress
    rxoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);                            --@suppress
    rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);                      --@suppress
    rxresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);                         --@suppress
    txoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);                            --@suppress
    txpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);                      --@suppress
    txresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)                          --@suppress
  );

end gtwizard_fullmode_txcpll_rxqpll_vup;

architecture stub of gtwizard_fullmode_txcpll_rxqpll_vup is

begin
end;

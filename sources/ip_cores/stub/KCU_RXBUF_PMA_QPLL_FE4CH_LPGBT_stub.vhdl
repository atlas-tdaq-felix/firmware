-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Mon Sep 28 15:30:21 2020
-- Host        : tarfa running 64-bit CentOS Linux release 7.6.1810 (Core)
-- Command     : write_vhdl -force -mode synth_stub
--               /localstore/et/franss/felix/firmware2/Projects/FLX712_FELIX/FLX712_FELIX.srcs/sources_1/ip/KCU_RXBUF_PMA_QPLL_FE4CH_LPGBT/KCU_RXBUF_PMA_QPLL_FE4CH_LPGBT_stub.vhdl
-- Design      : KCU_RXBUF_PMA_QPLL_FE4CH_LPGBT
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku115-flvf1924-2-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity KCU_RXBUF_PMA_QPLL_FE4CH_LPGBT is
  Port ( 
    gtwiz_userclk_tx_active_in : in STD_LOGIC_VECTOR ( 0 to 0 );                 --@suppress
    gtwiz_userclk_rx_active_in : in STD_LOGIC_VECTOR ( 0 to 0 );                 --@suppress
    gtwiz_buffbypass_tx_reset_in : in STD_LOGIC_VECTOR ( 0 to 0 );               --@suppress
    gtwiz_buffbypass_tx_start_user_in : in STD_LOGIC_VECTOR ( 0 to 0 );          --@suppress
    gtwiz_buffbypass_tx_done_out : out STD_LOGIC_VECTOR ( 0 to 0 );              --@suppress
    gtwiz_buffbypass_tx_error_out : out STD_LOGIC_VECTOR ( 0 to 0 );             --@suppress
    gtwiz_reset_clk_freerun_in : in STD_LOGIC_VECTOR ( 0 to 0 );                 --@suppress
    gtwiz_reset_all_in : in STD_LOGIC_VECTOR ( 0 to 0 );                         --@suppress
    gtwiz_reset_tx_pll_and_datapath_in : in STD_LOGIC_VECTOR ( 0 to 0 );         --@suppress
    gtwiz_reset_tx_datapath_in : in STD_LOGIC_VECTOR ( 0 to 0 );                 --@suppress
    gtwiz_reset_rx_pll_and_datapath_in : in STD_LOGIC_VECTOR ( 0 to 0 );         --@suppress
    gtwiz_reset_rx_datapath_in : in STD_LOGIC_VECTOR ( 0 to 0 );                 --@suppress
    gtwiz_reset_rx_cdr_stable_out : out STD_LOGIC_VECTOR ( 0 to 0 );             --@suppress
    gtwiz_reset_tx_done_out : out STD_LOGIC_VECTOR ( 0 to 0 );                   --@suppress
    gtwiz_reset_rx_done_out : out STD_LOGIC_VECTOR ( 0 to 0 );                   --@suppress
    gtwiz_userdata_tx_in : in STD_LOGIC_VECTOR ( 127 downto 0 );                 --@suppress
    gtwiz_userdata_rx_out : out STD_LOGIC_VECTOR ( 63 downto 0 );                --@suppress
    gtrefclk01_in : in STD_LOGIC_VECTOR ( 0 to 0 );                              --@suppress
    qpll0fbclklost_out : out STD_LOGIC_VECTOR ( 0 to 0 );                        --@suppress
    qpll0lock_out : out STD_LOGIC_VECTOR ( 0 to 0 );                             --@suppress
    qpll1fbclklost_out : out STD_LOGIC_VECTOR ( 0 to 0 );                        --@suppress
    qpll1lock_out : out STD_LOGIC_VECTOR ( 0 to 0 );                             --@suppress
    qpll1outclk_out : out STD_LOGIC_VECTOR ( 0 to 0 );                           --@suppress
    qpll1outrefclk_out : out STD_LOGIC_VECTOR ( 0 to 0 );                        --@suppress
    drpclk_in : in STD_LOGIC_VECTOR ( 3 downto 0 );                              --@suppress
    gthrxn_in : in STD_LOGIC_VECTOR ( 3 downto 0 );                              --@suppress
    gthrxp_in : in STD_LOGIC_VECTOR ( 3 downto 0 );                              --@suppress
    gtrefclk0_in : in STD_LOGIC_VECTOR ( 3 downto 0 );                           --@suppress
    loopback_in : in STD_LOGIC_VECTOR ( 11 downto 0 );                           --@suppress
    rxcdrhold_in : in STD_LOGIC_VECTOR ( 3 downto 0 );                           --@suppress
    rxpolarity_in : in STD_LOGIC_VECTOR ( 3 downto 0 );                          --@suppress
    rxslide_in : in STD_LOGIC_VECTOR ( 3 downto 0 );                             --@suppress
    rxusrclk_in : in STD_LOGIC_VECTOR ( 3 downto 0 );                            --@suppress
    rxusrclk2_in : in STD_LOGIC_VECTOR ( 3 downto 0 );                           --@suppress
    txpolarity_in : in STD_LOGIC_VECTOR ( 3 downto 0 );                          --@suppress
    txusrclk_in : in STD_LOGIC_VECTOR ( 3 downto 0 );                            --@suppress
    txusrclk2_in : in STD_LOGIC_VECTOR ( 3 downto 0 );                           --@suppress
    cplllock_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                          --@suppress
    gthtxn_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                            --@suppress
    gthtxp_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                            --@suppress
    gtpowergood_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                       --@suppress
    rxcdrlock_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                         --@suppress
    rxoutclk_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                          --@suppress
    rxpmaresetdone_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                    --@suppress
    rxresetdone_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                       --@suppress
    txoutclk_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                          --@suppress
    txpmaresetdone_out : out STD_LOGIC_VECTOR ( 3 downto 0 );                    --@suppress
    txresetdone_out : out STD_LOGIC_VECTOR ( 3 downto 0 )                        --@suppress
  );

end KCU_RXBUF_PMA_QPLL_FE4CH_LPGBT;

architecture stub of KCU_RXBUF_PMA_QPLL_FE4CH_LPGBT is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "gtwiz_userclk_tx_active_in[0:0],gtwiz_userclk_rx_active_in[0:0],gtwiz_buffbypass_tx_reset_in[0:0],gtwiz_buffbypass_tx_start_user_in[0:0],gtwiz_buffbypass_tx_done_out[0:0],gtwiz_buffbypass_tx_error_out[0:0],gtwiz_reset_clk_freerun_in[0:0],gtwiz_reset_all_in[0:0],gtwiz_reset_tx_pll_and_datapath_in[0:0],gtwiz_reset_tx_datapath_in[0:0],gtwiz_reset_rx_pll_and_datapath_in[0:0],gtwiz_reset_rx_datapath_in[0:0],gtwiz_reset_rx_cdr_stable_out[0:0],gtwiz_reset_tx_done_out[0:0],gtwiz_reset_rx_done_out[0:0],gtwiz_userdata_tx_in[127:0],gtwiz_userdata_rx_out[63:0],gtrefclk01_in[0:0],qpll0fbclklost_out[0:0],qpll0lock_out[0:0],qpll1fbclklost_out[0:0],qpll1lock_out[0:0],qpll1outclk_out[0:0],qpll1outrefclk_out[0:0],drpclk_in[3:0],gthrxn_in[3:0],gthrxp_in[3:0],gtrefclk0_in[3:0],loopback_in[11:0],rxcdrhold_in[3:0],rxpolarity_in[3:0],rxslide_in[3:0],rxusrclk_in[3:0],rxusrclk2_in[3:0],txpolarity_in[3:0],txusrclk_in[3:0],txusrclk2_in[3:0],cplllock_out[3:0],gthtxn_out[3:0],gthtxp_out[3:0],gtpowergood_out[3:0],rxcdrlock_out[3:0],rxoutclk_out[3:0],rxpmaresetdone_out[3:0],rxresetdone_out[3:0],txoutclk_out[3:0],txpmaresetdone_out[3:0],txresetdone_out[3:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "KCU_RXBUF_PMA_QPLL_FE4CH_LPGBT_gtwizard_top,Vivado 2020.1";
begin
end;

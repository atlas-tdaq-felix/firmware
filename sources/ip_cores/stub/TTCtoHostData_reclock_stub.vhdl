-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Mon Sep 28 15:35:50 2020
-- Host        : tarfa running 64-bit CentOS Linux release 7.6.1810 (Core)
-- Command     : write_vhdl -force -mode synth_stub
--               /localstore/et/franss/felix/firmware2/Projects/FLX712_FELIX/FLX712_FELIX.srcs/sources_1/ip/TTCtoHostData_reclock/TTCtoHostData_reclock_stub.vhdl
-- Design      : TTCtoHostData_reclock
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku115-flvf1924-2-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TTCtoHostData_reclock is
  Port ( 
    rst : in STD_LOGIC;                                     --@suppress
    wr_clk : in STD_LOGIC;                                  --@suppress
    rd_clk : in STD_LOGIC;                                  --@suppress
    din : in STD_LOGIC_VECTOR ( 159 downto 0 );             --@suppress
    wr_en : in STD_LOGIC;                                   --@suppress
    rd_en : in STD_LOGIC;                                   --@suppress
    dout : out STD_LOGIC_VECTOR ( 159 downto 0 );           --@suppress
    full : out STD_LOGIC;                                   --@suppress
    empty : out STD_LOGIC;                                  --@suppress
    valid : out STD_LOGIC                                   --@suppress
  );

end TTCtoHostData_reclock;

architecture stub of TTCtoHostData_reclock is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "rst,wr_clk,rd_clk,din[159:0],wr_en,rd_en,dout[159:0],full,empty,valid";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "fifo_generator_v13_2_5,Vivado 2020.1";
begin
end;

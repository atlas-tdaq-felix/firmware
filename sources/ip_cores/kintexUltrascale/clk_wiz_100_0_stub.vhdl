-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.4 (lin64) Build 1733598 Wed Dec 14 22:35:42 MST 2016
-- Date        : Tue Jan 23 16:23:42 2018
-- Host        : piedra running 64-bit Scientific Linux CERN SLC release 6.9 (Carbon)
-- Command     : write_vhdl -force -mode synth_stub
--               /data/et/rhabrake/FELIX/firmware/Projects/fmemu_top_BNL711/fmemu_top_BNL711.srcs/sources_1/ip/clk_wiz_100_0/clk_wiz_100_0_stub.vhdl
-- Design      : clk_wiz_100_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku115-flvf1924-2-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity clk_wiz_100_0 is
  Port ( 
    clk40 : out STD_LOGIC;
    clk10 : out STD_LOGIC;
    reset : in STD_LOGIC;
    locked : out STD_LOGIC;
    clk_100_in_p : in STD_LOGIC;
    clk_100_in_n : in STD_LOGIC
  );

end clk_wiz_100_0;

architecture stub of clk_wiz_100_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk40,clk10,reset,locked,clk_100_in_p,clk_100_in_n";
begin
end;

-- Copyright 1986-2015 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2015.4 (lin64) Build 1412921 Wed Nov 18 09:44:32 MST 2015
-- Date        : Thu Jul 21 11:23:17 2016
-- Host        : avel running 64-bit Ubuntu 16.04 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/franss/cernFelix/trunk/firmware/Projects/felix_top/felix_top.srcs/sources_1/ip/xadc_wiz_0/xadc_wiz_0_stub.vhdl
-- Design      : xadc_wiz_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7vx690tffg1761-2
-- --------------------------------------------------------------------------------
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

entity xadc_wiz_0 is
    Port (
        daddr_in : in STD_LOGIC_VECTOR ( 6 downto 0 ); -- @suppress "Unused port: daddr_in is not used in work.xadc_wiz_0(stub)"
        den_in : in STD_LOGIC; -- @suppress "Unused port: den_in is not used in work.xadc_wiz_0(stub)"
        di_in : in STD_LOGIC_VECTOR ( 15 downto 0 ); -- @suppress "Unused port: di_in is not used in work.xadc_wiz_0(stub)"
        dwe_in : in STD_LOGIC; -- @suppress "Unused port: dwe_in is not used in work.xadc_wiz_0(stub)"
        do_out : out STD_LOGIC_VECTOR ( 15 downto 0 ); -- @suppress "Unused port: do_out is not used in work.xadc_wiz_0(stub)"
        drdy_out : out STD_LOGIC; -- @suppress "Unused port: drdy_out is not used in work.xadc_wiz_0(stub)"
        dclk_in : in STD_LOGIC; -- @suppress "Unused port: dclk_in is not used in work.xadc_wiz_0(stub)"
        reset_in : in STD_LOGIC; -- @suppress "Unused port: reset_in is not used in work.xadc_wiz_0(stub)"
        busy_out : out STD_LOGIC; -- @suppress "Unused port: busy_out is not used in work.xadc_wiz_0(stub)"
        channel_out : out STD_LOGIC_VECTOR ( 4 downto 0 ); -- @suppress "Unused port: channel_out is not used in work.xadc_wiz_0(stub)"
        eoc_out : out STD_LOGIC; -- @suppress "Unused port: eoc_out is not used in work.xadc_wiz_0(stub)"
        eos_out : out STD_LOGIC; -- @suppress "Unused port: eos_out is not used in work.xadc_wiz_0(stub)"
        alarm_out : out STD_LOGIC; -- @suppress "Unused port: alarm_out is not used in work.xadc_wiz_0(stub)"
        vp_in : in STD_LOGIC; -- @suppress "Unused port: vp_in is not used in work.xadc_wiz_0(stub)"
        vn_in : in STD_LOGIC -- @suppress "Unused port: vn_in is not used in work.xadc_wiz_0(stub)"
    );

end xadc_wiz_0;

architecture stub of xadc_wiz_0 is
    attribute syn_black_box : boolean;
    attribute black_box_pad_pin : string;
    attribute syn_black_box of stub : architecture is true;
    attribute black_box_pad_pin of stub : architecture is "daddr_in[6:0],den_in,di_in[15:0],dwe_in,do_out[15:0],drdy_out,dclk_in,reset_in,busy_out,channel_out[4:0],eoc_out,eos_out,alarm_out,vp_in,vn_in";
begin
end;

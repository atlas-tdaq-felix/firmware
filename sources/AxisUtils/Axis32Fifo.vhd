--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
library UNISIM;
    use UNISIM.VCOMPONENTS.ALL;
library XPM;
    use XPM.VCOMPONENTS.ALL;

    use work.axi_stream_package.ALL;

entity Axis32Fifo is
    generic (
        DEPTH : integer := 512;
        --CLOCKING_MODE : string := "independent_clock";
        --RELATED_CLOCKS : integer range 0 to 1 := 0;
        --FIFO_MEMORY_TYPE : string := "auto";
        --PACKET_FIFO : string := "false";
        USE_BUILT_IN_FIFO : std_logic := '0';
        VERSAL : boolean := true;
        BLOCKSIZE : integer := 1024;
        ISPIXEL : boolean := false
    );
    port (
        -- axi stream slave
        s_axis_aresetn : in std_logic;
        s_axis_aclk : in std_logic;
        s_axis : in axis_32_type;
        s_axis_tready : out std_logic;

        -- axi stream master
        m_axis_aclk : in std_logic;
        m_axis : out axis_32_type;
        m_axis_tready : in std_logic;

        --Indication that the FIFO contains a block of data (for MUX).
        m_axis_prog_empty : out std_logic
    );
end Axis32Fifo;

architecture rtl of Axis32Fifo is

    COMPONENT axis32_fifo_bif
        PORT (
            m_aclk : IN STD_LOGIC;
            s_aclk : IN STD_LOGIC;
            s_aresetn : IN STD_LOGIC;
            s_axis_tvalid : IN STD_LOGIC;
            s_axis_tready : OUT STD_LOGIC;
            s_axis_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
            s_axis_tlast : IN STD_LOGIC;
            s_axis_tuser : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
            m_axis_tvalid : OUT STD_LOGIC;
            m_axis_tready : IN STD_LOGIC;
            m_axis_tdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
            m_axis_tlast : OUT STD_LOGIC;
            m_axis_tuser : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
            axis_prog_empty : OUT STD_LOGIC
        );
    END COMPONENT;

    signal busy_sync: std_logic;
    signal trunc_sync: std_logic;
    signal framing_error_sync: std_logic;
    signal s_axis_tready_s : std_logic;
    signal s_axis_tuser: std_logic_vector(2 downto 0);
    signal s_axis_tvalid : std_logic;
    signal s_axis_tdata : std_logic_vector(31 downto 0);
    signal s_axis_tlast : std_logic;
    signal m_axis_tuser: std_logic_vector(2 downto 0);
    signal m_axis_tvalid : std_logic;
    signal m_axis_tdata : std_logic_vector(31 downto 0);
    signal m_axis_tlast : std_logic;
    signal s_axis_tuser_2, s_axis_tuser_3, s_axis_tuser_1: std_logic; --for xpm cdc sync
    signal m_axis_aresetn: std_logic;
    signal m_axis_s : axis_32_type;
    signal m_axis_tready_s: std_logic;
begin

    --For GBT mode we can't use more than 0.5 BRAM per FIFO, 2KB means we can use 36 FIFO bits, 3 for TUSER + 1 for tlast.
    -- TKEEP is encoded in TUSER bits 1:0, only the error bit is used in TUSER, BUSY goes around the FIFO.
    g_tkeepenc_notpixel: if ISPIXEL = false generate
        process(s_axis_aclk)
        begin
            if rising_edge(s_axis_aclk) then
                if s_axis_tready_s = '1' then
                    case s_axis.tkeep is
                        when "0001" => s_axis_tuser(1 downto 0) <= "01";
                        when "0011" => s_axis_tuser(1 downto 0) <= "10";
                        when "0111" => s_axis_tuser(1 downto 0) <= "11";
                        when "1111" => s_axis_tuser(1 downto 0) <= "00";
                        when others => s_axis_tuser(1 downto 0) <= "00";
                    end case;
                end if;
            end if;
        end process;
    end generate;

    g_tkeepenc_pixel: if ISPIXEL generate
        process(s_axis_aclk)
        begin
            if rising_edge(s_axis_aclk) then
                if s_axis_tready_s = '1' then
                    --will keep all bytes or none
                    s_axis_tuser(0) <= s_axis.tkeep(0) or s_axis.tkeep(1) or s_axis.tkeep(2) or s_axis.tkeep(3);
                    s_axis_tuser(1) <= '0';
                end if;
            end if;
        end process;
    end generate;

    process(s_axis_aclk)
    begin
        if rising_edge(s_axis_aclk) then
            if s_axis_tready_s = '1' then
                s_axis_tuser(2) <= s_axis.tuser(1) or s_axis.tuser(3); --Error bit goes through FIFO, CRC ignored in GBT mode, BUSY goes through syncronozer, combine Error and truncation, extract it again after FIFO.
                s_axis_tvalid <= s_axis.tvalid;
                s_axis_tdata <= s_axis.tdata;
                s_axis_tlast <= s_axis.tlast;
            end if;
        end if;
    end process;

    s_axis_tready <= s_axis_tready_s;

    xpm_cdc_trunc : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 2, -- DECIMAL; range: 2-10
            INIT_SYNC_FF => 0, -- DECIMAL; integer; 0=disable simulation init values, 1=enable simulation init
            -- values
            SIM_ASSERT_CHK => 0, -- DECIMAL; integer; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG => 1   -- DECIMAL; integer; 0=do not register input, 1=register input
        )
        port map (
            src_clk => s_axis_aclk, -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in => s_axis.tuser(3), -- 1-bit input: Input signal to be synchronized to dest_clk domain.
            dest_clk => m_axis_aclk, -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => s_axis_tuser_3 -- 1-bit output: src_in synchronized to the destination clock domain. This output is registered.
        );

    xpm_cdc_busy : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 2, -- DECIMAL; range: 2-10
            INIT_SYNC_FF => 0, -- DECIMAL; integer; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0, -- DECIMAL; integer; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG => 1   -- DECIMAL; integer; 0=do not register input, 1=register input
        )
        port map (
            src_clk => s_axis_aclk, -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in => s_axis.tuser(2), -- 1-bit input: Input signal to be synchronized to dest_clk domain.
            dest_clk => m_axis_aclk, -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => s_axis_tuser_2 -- 1-bit output: src_in synchronized to the destination clock domain. This output is registered.
        );

    xpm_cdc_err : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 2, -- DECIMAL; range: 2-10
            INIT_SYNC_FF => 0, -- DECIMAL; integer; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0, -- DECIMAL; integer; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG => 1   -- DECIMAL; integer; 0=do not register input, 1=register input
        )
        port map (
            src_clk => s_axis_aclk, -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in => s_axis.tuser(1), -- 1-bit input: Input signal to be synchronized to dest_clk domain.
            dest_clk => m_axis_aclk, -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => s_axis_tuser_1 -- 1-bit output: src_in synchronized to the destination clock domain. This output is registered.
        );

    sync_s_axis_aresetn : xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 2,
            INIT => 0,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0
        )
        port map (
            src_rst => s_axis_aresetn,
            dest_clk => m_axis_aclk,
            dest_rst => m_axis_aresetn
        );

    process(m_axis_aclk, m_axis_aresetn)
        variable busy_sync_v: std_logic;
        variable trunc_sync_v: std_logic;
        variable trunc_sync_p1: std_logic;
        variable framing_error_sync_v: std_logic;
        variable framing_error_sync_p1: std_logic;
        constant max_trunc_cnt: integer := 15;
        variable trunc_cnt: integer range 0 to 15; --count truncations in FIFO
    begin
        if m_axis_aresetn = '0' then
            trunc_cnt := 0;
            busy_sync <= '0';
            trunc_sync <= '0';
            trunc_sync_p1 := '0';
            trunc_sync_v := '0';
            framing_error_sync <= '0';
            framing_error_sync_p1 := '0';
            framing_error_sync_v := '0';
            busy_sync_v := '0';
        elsif rising_edge(m_axis_aclk) then
            busy_sync <= busy_sync_v;
            busy_sync_v := s_axis_tuser_2;
            --Truncate flag does not go through FIFO to save resources, use error flag instead and replace if after fifo.
            trunc_sync_v := s_axis_tuser_3;
            framing_error_sync_v := s_axis_tuser_3 and s_axis_tuser_1; --Both T and E were set at the same time
            if trunc_sync_v = '1' and trunc_sync_p1 = '0' then --Before FIFO truncation was asserted, count up to maximum max_trunc_cnt
                trunc_sync <= '1';
                if trunc_cnt /= max_trunc_cnt then
                    trunc_cnt := trunc_cnt + 1;
                end if;
            end if;
            if framing_error_sync_v = '1' and framing_error_sync_p1 = '0' then --Before FIFO truncation was asserted, count counter is already done with trunc_sync
                framing_error_sync <= '1';
            end if;
            if m_axis_tuser(2) = '1' and m_axis_tready_s = '1' and m_axis_s.tlast = '1' then  --Truncation handled by m_axis, decrement counter
                if trunc_cnt /= 0 then
                    trunc_cnt := trunc_cnt - 1;
                end if;
            end if;
            if trunc_cnt = 0 then --Counter not zero, tell m_axis_tuser to set trunc flag instead of chunk error.
                trunc_sync <= '0';
                framing_error_sync <= '0';
            end if;
            trunc_sync_p1 := trunc_sync_v; --pipeline.
            framing_error_sync_p1 := framing_error_sync_v; --pipeline.
        end if;
    end process;

    g_bif: if USE_BUILT_IN_FIFO = '1' and VERSAL = false generate
        fifo0 : axis32_fifo_bif
            PORT MAP (
                m_aclk => m_axis_aclk,
                s_aclk => s_axis_aclk,
                s_aresetn => s_axis_aresetn,
                s_axis_tvalid => s_axis_tvalid,
                s_axis_tready => s_axis_tready_s,
                s_axis_tdata => s_axis_tdata,
                s_axis_tlast => s_axis_tlast,
                s_axis_tuser => s_axis_tuser,
                m_axis_tvalid => m_axis_tvalid,
                m_axis_tready => m_axis_tready_s,
                m_axis_tdata => m_axis_tdata,
                m_axis_tlast => m_axis_tlast,
                m_axis_tuser => m_axis_tuser,
                axis_prog_empty => m_axis_prog_empty
            );
    end generate;
    g_XPM: if USE_BUILT_IN_FIFO = '0' or VERSAL generate
        signal wr_en, full, rd_en, empty, rd_rst_busy, wr_rst_busy, rst: std_logic;
        signal din,dout: std_logic_vector(35 downto 0);
    begin
        rst <= not s_axis_aresetn;
        wr_en <= s_axis_tvalid and (not full) and (not wr_rst_busy);
        s_axis_tready_s <= (not full) and (not wr_rst_busy);
        din <= s_axis_tuser & s_axis_tlast & s_axis_tdata;
        rd_en <= (not rd_rst_busy) and m_axis_tready_s and (not empty);
        m_axis_tvalid <= (not empty) and (not rd_rst_busy);
        m_axis_tdata <= dout(31 downto 0);
        m_axis_tlast <= dout(32);
        m_axis_tuser <= dout(35 downto 33);
        xpm_fifo_async_inst : xpm_fifo_async
            generic map (
                FIFO_MEMORY_TYPE => "auto",
                FIFO_WRITE_DEPTH => DEPTH,
                CASCADE_HEIGHT => 0,
                RELATED_CLOCKS => 0,
                WRITE_DATA_WIDTH => 36,
                READ_MODE => "fwft",
                FIFO_READ_LATENCY => 1,
                FULL_RESET_VALUE => 1,
                USE_ADV_FEATURES => "0200",
                READ_DATA_WIDTH => 36, --Tdata: 32b, tlast: 1b, tuser/tkeep combi: 3b
                CDC_SYNC_STAGES => 2,
                WR_DATA_COUNT_WIDTH => 1,
                PROG_FULL_THRESH => DEPTH-10,
                RD_DATA_COUNT_WIDTH => 1,
                PROG_EMPTY_THRESH => (BLOCKSIZE/4)-1,
                DOUT_RESET_VALUE => "0",
                ECC_MODE => "no_ecc",
                SIM_ASSERT_CHK => 0,
                WAKEUP_TIME => 0
            )
            port map (
                sleep => '0',
                rst => rst,
                wr_clk => s_axis_aclk,
                wr_en => wr_en,
                din => din,
                full => full,
                prog_full => open,
                wr_data_count => open,
                overflow => open,
                wr_rst_busy => wr_rst_busy,
                almost_full => open,
                wr_ack => open,
                rd_clk => m_axis_aclk,
                rd_en => rd_en,
                dout => dout,
                empty => empty,
                prog_empty => m_axis_prog_empty,
                rd_data_count => open,
                underflow => open,
                rd_rst_busy => rd_rst_busy,
                almost_empty => open,
                data_valid => open,
                injectsbiterr => '0',
                injectdbiterr => '0',
                sbiterr => open,
                dbiterr => open
            );
    end generate;


    --Add pipeline to m_axis_s to ease timing towards CRToHost
    process(m_axis_aclk)
        variable tkeep_bits: std_logic_vector(1 downto 0);
    begin
        if rising_edge(m_axis_aclk) then
            if m_axis_aresetn = '0' then
                m_axis_s.tvalid <= '0';
                m_axis_s.tdata <= (others => '0');
                m_axis_s.tkeep <= (others => '0');
                m_axis_s.tid <= (others => '0');
                m_axis_s.tlast <= '0';
                m_axis_s.tuser <= (others => '0');
            else
                if m_axis_tready_s = '1' then --If there is no valid data in the pipeline, we can safely enable the CE
                    if ISPIXEL = false then  --For GBT/lpGBT/Strips, tkeep is encoded in 2 bits, for pixel only one bit isused, adding the possibility to have "0000"
                        tkeep_bits := m_axis_tuser(1 downto 0);
                        case tkeep_bits is
                            when "00" => m_axis_s.tkeep <= "1111";
                            when "01" => m_axis_s.tkeep <= "0001";
                            when "10" => m_axis_s.tkeep <= "0011";
                            when "11" => m_axis_s.tkeep <= "0111";
                            when others => m_axis_s.tkeep <= "1111";
                        end case;
                    else
                        m_axis_s.tkeep <= m_axis_tuser(0) & m_axis_tuser(0) & m_axis_tuser(0) & m_axis_tuser(0);
                    end if;
                    m_axis_s.tuser(3) <= m_axis_tuser(2) and trunc_sync; --Error bit used as truncation bit
                    m_axis_s.tuser(2) <= busy_sync;
                    m_axis_s.tuser(1) <= m_axis_tuser(2) and ((not trunc_sync) or framing_error_sync); --Error bit, in case of Framing error, we set T+E flags
                    m_axis_s.tuser(0) <= '0'; --Unused in GBT mode where we use the builtin fifo.
                    m_axis_s.tvalid <= m_axis_tvalid;
                    m_axis_s.tdata  <= m_axis_tdata;
                    m_axis_s.tlast  <= m_axis_tlast;
                end if;
            end if;
        end if;
    end process;

    m_axis_tready_s <= m_axis_tready or not m_axis_s.tvalid;

    m_axis <= m_axis_s;

end architecture;


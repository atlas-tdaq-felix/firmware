--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!               Nico Giangiacomi
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

library XPM;
    use XPM.VCOMPONENTS.ALL;

    use work.axi_stream_package.ALL;

entity Axis8Fifo is
    generic (
        --DEPTH : integer;
        --CLOCKING_MODE : string := "independent_clock";
        --RELATED_CLOCKS : integer range 0 to 1 := 0;
        --FIFO_MEMORY_TYPE : string := "auto";
        --PACKET_FIFO : string := "false";
        USE_BUILT_IN_FIFO : std_logic := '1';
        DISTR_RAM : boolean := false
    );
    port (
        -- axi stream slave
        s_axis_aresetn : in std_logic;
        s_axis_aclk : in std_logic;
        s_axis : in axis_8_type;
        s_axis_tready : out std_logic;

        -- axi stream master
        m_axis_aclk : in std_logic;
        m_axis : out axis_8_type;
        m_axis_tready : in std_logic;
        almost_full : out std_logic
    );
end Axis8Fifo;

architecture rtl of Axis8Fifo is


    COMPONENT axi8_fifo_bif
        PORT (
            wr_rst_busy : OUT STD_LOGIC;
            rd_rst_busy : OUT STD_LOGIC;
            m_aclk : IN STD_LOGIC;
            s_aclk : IN STD_LOGIC;
            s_aresetn : IN STD_LOGIC;
            s_axis_tvalid : IN STD_LOGIC;
            s_axis_tready : OUT STD_LOGIC;
            s_axis_tdata : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
            s_axis_tlast : IN STD_LOGIC;
            m_axis_tvalid : OUT STD_LOGIC;
            m_axis_tready : IN STD_LOGIC;
            m_axis_tdata : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            m_axis_tlast : OUT STD_LOGIC;
            axis_prog_full : OUT STD_LOGIC
        );
    END COMPONENT;


begin
    xpm_fifo_gen: if USE_BUILT_IN_FIFO = '0' or DISTR_RAM generate
        function memory_type(t: std_logic) return string is
        begin
            if t = '1' or DISTR_RAM then
                return "distributed";
            else
                return "block";
            end if;

        end function;
        function fifo_depth(t: std_logic) return integer is
        begin
            if t = '1' or DISTR_RAM then
                return 16;   --For distributed ram we use a shallow FIFO.
            else
                return 1024; --For block ram based fifo's we can fit 1 kB + tlast in one RAMB18
            end if;
        end function;
        signal din,dout: std_logic_vector(8 downto 0);
        signal rst: std_logic;
        signal full,empty: std_logic;
        signal rd_en, wr_en: std_logic;
        signal rd_rst_busy, wr_rst_busy: std_logic;
    begin
        --xpm_fifo_async fits better in a BRAM18 dan xpm_fifo_axis because you can't shut down all axi stream sideband signals that we don't use.
        --A simple FWFT fifo will do just as well.
        --If built-in FIFO is set to false we use this FIFO, for Versal we always use XPM, but when USE_BUILT_IN_FIFO is '1' we use distributed ram instead
        --g_versal: if VERSAL generate
        --srl_fifo_inst: entity work.SRL16_FIFO
        --        generic map (
        --      DATA_WIDTH        => 9,--: integer := 9;
        --      FWFT              => true--: boolean := true
        --        )
        --        port map (
        --      rst    => rst,--: in  std_logic;
        --      wr_clk => s_axis_aclk,--: in  std_logic;
        --      rd_clk => m_axis_aclk,--: in  std_logic;
        --      din    => din,--: in  std_logic_vector(DATA_WIDTH - 1 downto 0);
        --      dout   => dout,--: out std_logic_vector(DATA_WIDTH - 1 downto 0);
        --      wr_en  => wr_en,--: in  std_logic;
        --      rd_en  => rd_en,--: in  std_logic;
        --      full   => full,--: out std_logic;
        --      empty  => empty --: out std_logic
        --        );
        --      almost_full <= full;
        --      rd_rst_busy <= '0';
        --      wr_rst_busy <= '0';
        --
        --end generate;
        --g_noVersal: if not Versal generate
        xpm_fifo_async_inst : xpm_fifo_async
            generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT, SIM_ASSERT_CHK"
                CDC_SYNC_STAGES => 2,
                DOUT_RESET_VALUE => "0",
                ECC_MODE => "no_ecc",
                FIFO_MEMORY_TYPE => memory_type(USE_BUILT_IN_FIFO),
                FIFO_READ_LATENCY => 1,
                FIFO_WRITE_DEPTH => fifo_depth(USE_BUILT_IN_FIFO),
                FULL_RESET_VALUE => 0,
                PROG_EMPTY_THRESH => 6,
                PROG_FULL_THRESH => fifo_depth(USE_BUILT_IN_FIFO)-6,
                RD_DATA_COUNT_WIDTH => 1,
                READ_DATA_WIDTH => 9,
                READ_MODE => "fwft",
                RELATED_CLOCKS => 0,
                USE_ADV_FEATURES => "0008", --only almost_full flag
                WAKEUP_TIME => 0,
                WRITE_DATA_WIDTH => 9,
                WR_DATA_COUNT_WIDTH => 1
            )
            port map (
                sleep => '0',
                rst => rst,
                wr_clk => s_axis_aclk,
                wr_en => wr_en,
                din => din,
                full => full,
                prog_full => open,
                wr_data_count => open,
                overflow => open,
                wr_rst_busy => wr_rst_busy,
                almost_full => almost_full,
                wr_ack => open,
                rd_clk => m_axis_aclk,
                rd_en => rd_en,
                dout => dout,
                empty => empty,
                prog_empty => open,
                rd_data_count => open,
                underflow => open,
                rd_rst_busy => rd_rst_busy,
                almost_empty => open,
                data_valid => open,
                injectsbiterr => '0',
                injectdbiterr => '0',
                sbiterr => open,
                dbiterr => open
            );
        --end generate;
        rst <= not s_axis_aresetn;
        wr_en <= s_axis.tvalid and not full and not wr_rst_busy;
        s_axis_tready <= not full;
        din(7 downto 0) <= s_axis.tdata;
        din(8) <= s_axis.tlast;
        m_axis.tvalid <= not empty;
        rd_en <= m_axis_tready and (not empty) and (not rd_rst_busy);
        m_axis.tdata <= dout(7 downto 0);
        m_axis.tlast <= dout(8);
    end generate;

    builtin_fifo_gen: if USE_BUILT_IN_FIFO = '1' and DISTR_RAM = false generate
        axi8_fifo_inst: axi8_fifo_bif
            PORT Map(
                wr_rst_busy         => open,
                rd_rst_busy         => open,
                m_aclk              => m_axis_aclk,
                s_aclk              => s_axis_aclk,
                s_aresetn           => s_axis_aresetn,
                s_axis_tvalid       => s_axis.tvalid,
                s_axis_tready       => s_axis_tready,
                s_axis_tdata        => s_axis.tdata,
                s_axis_tlast        => s_axis.tlast,
                m_axis_tvalid       => m_axis.tvalid,
                m_axis_tready       => m_axis_tready,
                m_axis_tdata        => m_axis.tdata,
                m_axis_tlast        => m_axis.tlast,
                axis_prog_full      => almost_full
            );
    end generate;
end architecture;

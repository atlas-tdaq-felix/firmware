--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
library UNISIM;
    use UNISIM.VCOMPONENTS.ALL;
library XPM;
    use XPM.VCOMPONENTS.ALL;

    use work.axi_stream_package.ALL;

entity Axis64Fifo is
    generic (
        DEPTH : integer
    );
    port (
        -- axi stream slave
        s_axis_aresetn : in std_logic;
        s_axis_aclk : in std_logic;
        s_axis : in axis_64_type;
        s_axis_tready : out std_logic;

        -- axi stream master
        m_axis_aclk : in std_logic;
        m_axis : out axis_64_type;
        m_axis_tready : in std_logic;

        --Indication that the FIFO contains a block of data (for MUX).
        m_axis_prog_empty : out std_logic
    );
end Axis64Fifo;

architecture rtl of Axis64Fifo is



begin

    fifo: xpm_fifo_axis
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT"
            CLOCKING_MODE => "independent_clock",
            FIFO_MEMORY_TYPE => "auto",
            PACKET_FIFO => "false",
            FIFO_DEPTH => DEPTH,
            TDATA_WIDTH => 64,
            TID_WIDTH => 8,
            TDEST_WIDTH => 1,
            TUSER_WIDTH => 4,
            ECC_MODE => "no_ecc",
            RELATED_CLOCKS => 0,
            USE_ADV_FEATURES => "1200", --"1200" to enable prog_empty
            WR_DATA_COUNT_WIDTH => 10,
            RD_DATA_COUNT_WIDTH => 10,
            PROG_FULL_THRESH => 7,
            PROG_EMPTY_THRESH => (DEPTH/2)-2,
            SIM_ASSERT_CHK => 0,
            CDC_SYNC_STAGES => 2
        )
        port map (
            s_aresetn => s_axis_aresetn,
            m_aclk => m_axis_aclk,
            s_aclk => s_axis_aclk,
            s_axis_tvalid => s_axis.tvalid,
            s_axis_tready => s_axis_tready,
            s_axis_tdata => s_axis.tdata,
            s_axis_tstrb => (others => '1'),
            s_axis_tkeep => s_axis.tkeep,
            s_axis_tlast => s_axis.tlast,
            s_axis_tid => s_axis.tid,
            s_axis_tdest => (others => '0'),
            s_axis_tuser => s_axis.tuser,
            m_axis_tvalid => m_axis.tvalid,
            m_axis_tready => m_axis_tready,
            m_axis_tdata => m_axis.tdata,
            m_axis_tstrb => open,
            m_axis_tkeep => m_axis.tkeep,
            m_axis_tlast => m_axis.tlast,
            m_axis_tid => m_axis.tid,
            m_axis_tdest => open,
            m_axis_tuser => m_axis.tuser,
            prog_full_axis => open,
            wr_data_count_axis => open,
            almost_full_axis => open,
            prog_empty_axis => m_axis_prog_empty,
            rd_data_count_axis => open,
            almost_empty_axis => open,
            injectsbiterr_axis => '0',
            injectdbiterr_axis => '0',
            sbiterr_axis => open,
            dbiterr_axis => open
        );



end architecture;

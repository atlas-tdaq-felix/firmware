--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

    use IEEE.NUMERIC_STD.ALL;

library UNISIM;
    use UNISIM.VComponents.all;
library XPM;
    use XPM.vcomponents.all;


entity SRL16_FIFO is
    generic (
        DATA_WIDTH        : integer := 9;
        FWFT              : boolean := true
    );
    port (
        rst     : in  std_logic;
        wr_clk  : in  std_logic;
        rd_clk  : in  std_logic;
        din     : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
        dout    : out std_logic_vector(DATA_WIDTH - 1 downto 0);
        wr_en   : in  std_logic;
        rd_en   : in  std_logic;
        full    : out std_logic;
        empty   : out std_logic;
        pfull   : out std_logic
    );

end entity SRL16_FIFO;

architecture rtl of SRL16_FIFO is

    signal wr_en_s       : std_logic;
    signal full_s   : std_logic;
    signal pfull_s   : std_logic;
    signal rd_en_s      : std_logic;
    signal empty_s      : std_logic;

    signal counter_diff: unsigned(3 downto 0);
    --signal counter_diff_wr_clk: unsigned(3 downto 0);
    signal wr_counter: unsigned(3 downto 0);
    --signal wr_counter_p1: unsigned(3 downto 0);
    signal rd_counter: unsigned(3 downto 0);
    signal din_s: std_logic_vector(DATA_WIDTH-1 downto 0);
    signal dout_s: std_logic_vector(DATA_WIDTH-1 downto 0);
    signal reset_done: std_logic;

begin  -- architecture fifo_srl_uni_r


    wr_en_s            <= wr_en and (not full_s);
    rd_en_s            <= rd_en and (not empty_s);

    g_FWFT: if FWFT generate
        full_s        <= '1' when counter_diff = "1101" or reset_done = '0' else '0';
        empty_s <= '1' when (counter_diff) = "1111"     or reset_done = '0' else '0';
        pfull_s <= '1' when (counter_diff) = "1011" or (counter_diff) = "1100" or counter_diff = "1101" or reset_done = '0' else '0';
    end generate;
    g_notFWFT: if not FWFT generate
        full_s        <= '1' when counter_diff = "1110" else '0';
        empty_s <= '1' when (counter_diff) = "0000"             else '0';
        pfull_s <= '1' when (counter_diff) = "1100" or (counter_diff) = "1101" or counter_diff = "1110" or reset_done = '0' else '0';
    end generate;


    full <= full_s;
    pfull <= pfull_s;
    din_s <= din;

    g_databits: for i in 0 to DATA_WIDTH - 1 generate

        SRLC16_inst : SRLC16E
            generic map(
                INIT => x"0000",
                IS_CLK_INVERTED => '0'
            )
            port map
            (
                Q => dout_s(i), -- SRL data output
                Q15 => open, -- Carry output (connect to next SRL)
                A0 => counter_diff(0), -- Select[0] input
                A1 => counter_diff(1), -- Select[1] input
                A2 => counter_diff(2), -- Select[2] input
                A3 => counter_diff(3), -- Select[3] input
                CE => wr_en_s, -- Clock enable input
                CLK => wr_clk, -- Clock input
                D => din_s(i) -- SRL data input
            );

    end generate g_databits;

    wr_cnt_proc: process (wr_clk, rst) is
    begin  -- process P0
        if rst = '1' then
            wr_counter <= (others => '0');
            reset_done <= '0';
        elsif rising_edge(wr_clk) then
            reset_done <= '1';
            --wr_counter_p1 <= wr_counter;
            if (wr_en_s = '1') then
                wr_counter <= wr_counter + 1;
            end if;

        end if;

    end process;

    rd_cnt_proc: process (rd_clk, rst) is
    begin  -- process P0
        if rst = '1' then
            if FWFT then
                rd_counter <= "0001";
            else
                rd_counter <= "0000";
            end if;
        --dout <= (others => '0');
        --empty <= '1';
        elsif rising_edge(rd_clk) then
            if (rd_en_s = '1') then
                rd_counter <= rd_counter + 1;
            end if;

        end if;

    end process;
    dout <= dout_s;
    empty <= empty_s;

    counter_diff <= wr_counter - rd_counter;
--counter_diff_wr_clk <= wr_counter - rd_counter;



end architecture rtl;

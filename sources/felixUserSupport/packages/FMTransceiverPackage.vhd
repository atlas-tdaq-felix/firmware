--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               Mesfin Gebyehu
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

-------------------------------------------
-- Frans Schreuder (Nikhef)
-- June 2017
-- package file for FMUserExample
--
-------------------------------------------

library ieee;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use ieee.numeric_std.all;

package FMTransceiverPackage is
    type slv20_array       is array (natural range <>) of std_logic_vector(19 downto 0);
    --    type slv32_array       is array (natural range <>) of std_logic_vector(31 downto 0);
    type slv4_array       is array (natural range <>) of std_logic_vector(3 downto 0);
end FMTransceiverPackage;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Weihao Wu
--!               Frans Schreuder
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:  BNL
-- Engineer: Weihao Wu
-- 
-- Create Date: 10/03/2016 03:50:38 PM
-- Design Name: 
-- Module Name: gth_cpll_wrapper_proca - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description:   This is the user example for FELIX FULL mode.
--                It is the GTH wrapper used in the gFEX processor FPGA A (Virtex UltraScale FPGA).
--                It connects the TX data source from FMchannelTXctrl and FullModeUserLogic.
--                It has been tested with FELIX in the FULL mode.
--                The GTH is configured: TX FULL mode (9.6 Gb/s)  RX GBT mode (4.8 Gb/s).
--                A external FPGA-GBT firmware is needed, if the RX link is also tested.
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity gth_qpll_wrapper_proca is
  Port (
  gtrefclk0_in       	  : in   std_logic_vector(0 downto 0); 
  DRP_CLK_IN          : in   std_logic_vector(0 downto 0);
  
  --- RXTX clock, for each channel
  rxusrclk_in     : in   std_logic_vector(3 downto 0);
  rxoutclk_out    : out  std_logic_vector(3 downto 0);
  txusrclk_in     : in   std_logic_vector(3 downto 0);
  txoutclk_out    : out  std_logic_vector(3 downto 0);
   
-----------------------------------------
---- STATUS signals
-----------------------------------------
  
  reset_tx_done_out  : out  std_logic_vector(0 downto 0);
  reset_rx_done_out  : out  std_logic_vector(0 downto 0);
  
  gtrxreset_in       : in std_logic_vector(3 downto 0);
  gttxreset_in       : in std_logic_vector(3 downto 0);
  cdr_stable          : out  std_logic_vector(3 downto 0);
  --gt_qplllock_out             : out  std_logic;

---------------------------
---- CTRL signals
---------------------------
  rxslide_in             : in   std_logic_vector(3 downto 0);
  
----------------------------------------------------------------
----------RESET SIGNALs
----------------------------------------------------------------     
  
  reset_all              : in   std_logic_vector(0 downto 0); 
 
  reset_tx_pll_and_datapath      : in   std_logic_vector(0 downto 0);
  reset_rx_pll_and_datapath      : in   std_logic_vector(0 downto 0);

  reset_tx_datapath              : in   std_logic_vector(0 downto 0);
  reset_rx_datapath              : in   std_logic_vector(0 downto 0);
-----------------------------------------------------------
----------- Data and TX/RX Ports
-----------------------------------------------------------
  
  --gt0_txdata_in : in std_logic_vector(19 downto 0);
  userdata_rx_out : out std_logic_vector(79 downto 0);
 
  gthrxn_in                                  : in   std_logic_vector(3 downto 0);
  gthrxp_in                                  : in   std_logic_vector(3 downto 0);
  gthtxn_out                                 : out  std_logic_vector(3 downto 0);
  gthtxp_out                                 : out  std_logic_vector(3 downto 0) 
   );
end gth_qpll_wrapper_proca;
 
architecture Behavioral of gth_qpll_wrapper_proca is

    attribute DowngradeIPIdentifiedWarnings: string;
    attribute DowngradeIPIdentifiedWarnings of Behavioral : architecture is "yes";

    attribute X_CORE_INFO : string;
    attribute X_CORE_INFO of Behavioral : architecture is "gtwizard_qpll_4p8g_4ch,gtwizard_v3_4,{protocol_file=Start_from_scratch}";
    attribute CORE_GENERATION_INFO : string;
    attribute CORE_GENERATION_INFO of Behavioral : architecture is "gtwizard_qpll_4p8g_4ch,gtwizard_v3_4,{protocol_file=Start_from_scratch}";

--**************************Component Declarations*****************************
component gth_tx_fullmode_rx_gbtmode_qpll_ultrascale 
port (
  gtwiz_userclk_tx_active_in           : in std_logic_vector(0 downto 0);
  gtwiz_userclk_rx_active_in           : in std_logic_vector(0 downto 0);   
  gtwiz_buffbypass_rx_reset_in         : in std_logic_vector(0 downto 0); 
  gtwiz_buffbypass_rx_start_user_in    : in std_logic_vector(0 downto 0); 
  gtwiz_buffbypass_rx_done_out         : out std_logic_vector(0 downto 0);
  gtwiz_buffbypass_rx_error_out        : out std_logic_vector(0 downto 0);
  gtwiz_reset_clk_freerun_in           : in std_logic_vector(0 downto 0);
  gtwiz_reset_all_in                   : in std_logic_vector(0 downto 0);
  gtwiz_reset_tx_pll_and_datapath_in   : in std_logic_vector(0 downto 0);
  gtwiz_reset_tx_datapath_in           : in std_logic_vector(0 downto 0);
  gtwiz_reset_rx_pll_and_datapath_in   : in std_logic_vector(0 downto 0);
  gtwiz_reset_rx_datapath_in           : in std_logic_vector(0 downto 0);
  gtwiz_reset_rx_cdr_stable_out        : out std_logic_vector(0 downto 0);
  gtwiz_reset_tx_done_out              : out std_logic_vector(0 downto 0);
  gtwiz_reset_rx_done_out              : out std_logic_vector(0 downto 0);
  gtwiz_userdata_tx_in                 : in std_logic_vector(127 downto 0);
  gtwiz_userdata_rx_out                : out std_logic_vector(79 downto 0); 
  gtrefclk01_in                        : in std_logic_vector(0 downto 0);
  qpll1outclk_out                      : out std_logic_vector(0 downto 0);
  qpll1outrefclk_out                   : out std_logic_vector(0 downto 0);
  cpllreset_in                         : in std_logic_vector(3 downto 0);
  drpclk_in                            : in std_logic_vector(3 downto 0);
  gthrxn_in                            : in std_logic_vector(3 downto 0);
  gthrxp_in                            : in std_logic_vector(3 downto 0);
  gtrxreset_in                         : in std_logic_vector(3 downto 0);
  gttxreset_in                         : in std_logic_vector(3 downto 0);
  rxcommadeten_in                      : in std_logic_vector(3 downto 0);
  rxmcommaalignen_in                   : in std_logic_vector(3 downto 0);
  rxpcommaalignen_in                   : in std_logic_vector(3 downto 0);
  rxslide_in                           : in std_logic_vector(3 downto 0);
  rxusrclk_in                          : in std_logic_vector(3 downto 0);
  rxusrclk2_in                         : in std_logic_vector(3 downto 0);
  tx8b10ben_in                         : in std_logic_vector(3 downto 0);
  txctrl0_in                           : in std_logic_vector(63 downto 0);
  txctrl1_in                           : in std_logic_vector(63 downto 0);
  txctrl2_in                           : in std_logic_vector(31 downto 0);
  txusrclk_in                          : in std_logic_vector(3 downto 0);
  txusrclk2_in                         : in std_logic_vector(3 downto 0);
  gthtxn_out                           : out std_logic_vector(3 downto 0);
  gthtxp_out                           : out std_logic_vector(3 downto 0);
  rxbyteisaligned_out                  : out std_logic_vector(3 downto 0);
  rxbyterealign_out                    : out std_logic_vector(3 downto 0);
  rxcdrlock_out                        : out std_logic_vector(3 downto 0);
  rxcommadet_out                       : out std_logic_vector(3 downto 0);
  rxoutclk_out                         : out std_logic_vector(3 downto 0);
  rxpmaresetdone_out                   : out std_logic_vector(3 downto 0);
  txoutclk_out                         : out std_logic_vector(3 downto 0);
  txpmaresetdone_out                   : out std_logic_vector(3 downto 0)
);
end component;

signal  tied_to_ground_i           : std_logic_vector(0 downto 0);
signal  tied_to_ground_i_vec       : std_logic_vector(3 downto 0);
signal  gt0_cplllock_i             : std_logic_vector(0 downto 0);
signal  tied_to_ground_vec_i       : std_logic_vector(63 downto 0);
signal  tied_to_vcc_i              : std_logic_vector(0 downto 0);
signal  tied_to_vcc_vec_i          : std_logic_vector(7 downto 0);
signal  tied_to_vcc_i_vec          : std_logic_vector(3 downto 0);
 

signal comma_type                  : std_logic_vector(1 downto 0);
signal comma_en_p                  : std_logic;
signal prbscnt                     : std_logic_vector(9 downto 0);
signal txcharisk_in_p              : std_logic_vector(7 downto 0);
signal txcharisk_in                : std_logic_vector(7 downto 0);
signal txcharisk_in_32b            : std_logic_vector(31 downto 0);
signal userdata_tx_in              : std_logic_vector(31 downto 0);
signal userdata_tx_in_128b         : std_logic_vector(127 downto 0);

signal userclk_tx_active_out       : std_logic_vector(0 downto 0);
signal userclk_tx_active_out_p     : std_logic_vector(0 downto 0);
signal userclk_rx_active_out       : std_logic_vector(0 downto 0);
signal userclk_rx_active_out_p     : std_logic_vector(0 downto 0);
signal txpmaresetdone_out          : std_logic_vector(3 downto 0);
signal rxpmaresetdone_out          : std_logic_vector(3 downto 0);

signal user_busy                   : std_logic;
signal fm_link_rdy                 : std_logic;
signal fm_user_fifo_rclk           : std_logic;
signal fm_user_fifo_re             : std_logic;
signal fm_user_fifo_dout           : std_logic_vector(31 downto 0);
signal fm_user_fifo_dtype          : std_logic_vector(1 downto 0);
signal fm_user_fifo_empty          : std_logic;
signal fm_user_data_32b            : std_logic_vector(31 downto 0);
signal fm_user_comma_4b            : std_logic_vector(3 downto 0);

signal drpclk_in                   : std_logic_vector(3 downto 0);

begin

    tied_to_ground_i(0)        <= '0';
    tied_to_ground_vec_i       <= x"0000000000000000";
    tied_to_ground_i_vec       <= x"0";
    tied_to_vcc_i(0)           <= '1';
    tied_to_vcc_vec_i          <= x"ff";
    tied_to_vcc_i_vec          <= x"f";
    
    drpclk_in                   <= DRP_CLK_IN &  DRP_CLK_IN &  DRP_CLK_IN &  DRP_CLK_IN;
    

  process(txpmaresetdone_out, txusrclk_in(0))
  begin
    if txpmaresetdone_out /= x"f" then
      userclk_tx_active_out(0) <= '0';
    elsif txusrclk_in(0)'event and txusrclk_in(0) = '1' then
      userclk_tx_active_out_p(0) <= '1';
      userclk_tx_active_out <= userclk_tx_active_out_p;
    end if;
  end process;
  
  process(rxpmaresetdone_out, rxusrclk_in(0))
  begin
    if rxpmaresetdone_out /= x"f" then
      userclk_rx_active_out(0) <= '0';
    elsif rxusrclk_in(0)'event and rxusrclk_in(0) = '1' then
      userclk_rx_active_out_p(0) <= '1';
      userclk_rx_active_out <= userclk_rx_active_out_p;
    end if;
  end process;
  
  
-----------------------------------------------
gth_tx_fullmode_rx_gbtmode_qpll_ultrascale_inst: gth_tx_fullmode_rx_gbtmode_qpll_ultrascale 
port map(
  gtwiz_userclk_tx_active_in              => userclk_tx_active_out, 
  gtwiz_userclk_rx_active_in              => userclk_rx_active_out, 
  gtwiz_buffbypass_rx_reset_in            => tied_to_ground_i, 
  gtwiz_buffbypass_rx_start_user_in       => tied_to_ground_i,   
  gtwiz_buffbypass_rx_done_out            => open, 
  gtwiz_buffbypass_rx_error_out           => open,  
  gtwiz_reset_clk_freerun_in              => DRP_CLK_IN,        
  gtwiz_reset_all_in                      => reset_all,    
  gtwiz_reset_tx_pll_and_datapath_in      => reset_tx_pll_and_datapath,
  gtwiz_reset_tx_datapath_in              => reset_tx_datapath,
  gtwiz_reset_rx_pll_and_datapath_in      => reset_rx_pll_and_datapath,
  gtwiz_reset_rx_datapath_in              => reset_rx_datapath,
  gtwiz_reset_rx_cdr_stable_out           => open,  
  gtwiz_reset_tx_done_out                 => reset_tx_done_out,
  gtwiz_reset_rx_done_out                 => reset_rx_done_out,
  gtwiz_userdata_tx_in                    => userdata_tx_in_128b,
  gtwiz_userdata_rx_out                   => userdata_rx_out,

  gtrefclk01_in                           => gtrefclk0_in,
  qpll1outclk_out                         => open,
  qpll1outrefclk_out                      => open,
  cpllreset_in                            => tied_to_ground_i_vec,

  drpclk_in                               => drpclk_in,  
  gthrxn_in                               => gthrxn_in,
  gthrxp_in                               => gthrxp_in,
  gtrxreset_in                            => gtrxreset_in,
  gttxreset_in                            => gttxreset_in,
  rxcommadeten_in                         => tied_to_ground_i_vec, 
  rxmcommaalignen_in                      => tied_to_ground_i_vec,
  rxpcommaalignen_in                      => tied_to_ground_i_vec,
  rxslide_in                              => rxslide_in,
  rxusrclk_in                             => rxusrclk_in,
  rxusrclk2_in                            => rxusrclk_in, 
  tx8b10ben_in                            => tied_to_vcc_i_vec,
  txctrl0_in                              => (others => '0'),
  txctrl1_in                              => (others => '0'),
  txctrl2_in                              => txcharisk_in_32b,
  txusrclk_in                             => txusrclk_in,
  txusrclk2_in                            => txusrclk_in,
  gthtxn_out                              => gthtxn_out,
  gthtxp_out                              => gthtxp_out,
  rxbyteisaligned_out                     => open,
  rxbyterealign_out                       => open,
  rxcdrlock_out                           => cdr_stable,
  rxcommadet_out                          => open,
  rxoutclk_out                            => rxoutclk_out,
  rxpmaresetdone_out                      => rxpmaresetdone_out,
  txoutclk_out                            => txoutclk_out,
  txpmaresetdone_out                      => txpmaresetdone_out
);

process(txusrclk_in(0))
begin
if rising_edge(txusrclk_in(0)) then
  userdata_tx_in_128b <= userdata_tx_in & userdata_tx_in & userdata_tx_in & userdata_tx_in;
  txcharisk_in_32b <= txcharisk_in & txcharisk_in & txcharisk_in & txcharisk_in;
end if;
end process;


FM_TXctrl_inst: entity work.FMchannelTXctrl
port map(
    clk240      => txusrclk_in(0),
    rst         => reset_all(0),
    --
    busy        => user_busy,
    link_rdy_o  => fm_link_rdy,
    fifo_rclk   => fm_user_fifo_rclk,
    fifo_re     => fm_user_fifo_re,
    fifo_dout   => fm_user_fifo_dout,
    fifo_dtype  => fm_user_fifo_dtype,
    fifo_empty  => fm_user_fifo_empty,
    --
    dout        => fm_user_data_32b,
    kout        => fm_user_comma_4b
    );

userdata_tx_in <= fm_user_data_32b(31 downto 0);
txcharisk_in <= "0000" & fm_user_comma_4b;

FM_user_inst: entity work.FullModeUserLogic
port map(
    clk         => txusrclk_in(0),
    rst         => reset_all(0),
    --
    busy        => user_busy,
    link_rdy_i  => fm_link_rdy,
    fifo_rclk   => fm_user_fifo_rclk,
    fifo_re     => fm_user_fifo_re,
    fifo_dout   => fm_user_fifo_dout,
    fifo_dtype  => fm_user_fifo_dtype,
    fifo_empty  => fm_user_fifo_empty
    );

end Behavioral;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               RHabraken
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.





library ieee, UNISIM, work;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use work.I2C.all;
use work.FMTransceiverPackage.all;
use work.FELIX_gbt_package.all;

entity FM_UserExample is
  generic(
    GTX_TRANSCEIVERS        : boolean := false; -- true for GTX, false for GTH
    NUM_LINKS               : integer := 4;
    RECOVER_CLK_FROM_RX_GBT : boolean := false);
  port (
    GTREFCLK0_N_IN       : in     std_logic;
    GTREFCLK0_P_IN       : in     std_logic;
    RESET_BUTTON         : in     std_logic;
    SCL                  : inout  std_logic;
    SDA                  : inout  std_logic;
    SFP_TX_ENABLE        : out    std_logic;
    SmaOut_x3            : out    std_logic;
    SmaOut_x4            : out    std_logic;
    app_clk_in_n         : in     std_logic;
    app_clk_in_p         : in     std_logic;
    clk_si5324_240_out_n : out    std_logic;
    clk_si5324_240_out_p : out    std_logic;
    emcclk               : in     std_logic;
    emcclk_out           : out    std_logic;
    gtrxn_in             : in     std_logic_vector(NUM_LINKS-1 downto 0);
    gtrxp_in             : in     std_logic_vector(NUM_LINKS-1 downto 0);
    gttxn_out            : out    std_logic_vector(NUM_LINKS-1 downto 0);
    gttxp_out            : out    std_logic_vector(NUM_LINKS-1 downto 0);
    i2cmux_rst           : out    std_logic;
    leds                 : out    std_logic_vector(7 downto 0);
    opto_inhibit         : out    std_logic_vector(3 downto 0);
    si5324_resetn        : out    std_logic);
end entity FM_UserExample;


architecture structure of FM_UserExample is

  signal tx_din            : std_logic_vector(31 downto 0);
  signal fifo_dout         : std_logic_vector(31 downto 0);
  signal fifo_dtype        : std_logic_vector(1 downto 0);
  signal fifo_empty        : std_logic;
  signal fifo_re           : std_logic;
  signal fifo_rclk         : std_logic;
  signal rst               : std_logic;
  signal clk40             : std_logic;
  signal fifo34_din        : std_logic_vector(31 downto 0);
  signal fifo34_full       : std_logic;
  signal FIFO34b_WE        : std_logic;
  signal EMU32b_EN         : std_logic;
  signal EMU32b_dout       : std_logic_vector(31 downto 0);
  signal EMU32b_dvalid     : std_logic;
  signal rst_hw            : std_logic;
  signal nReset            : std_logic;
  signal clk               : std_logic;
  signal cmd_ack           : std_logic;
  signal ack_out           : std_logic;
  signal Dout              : std_logic_vector(7 downto 0);
  signal Din               : std_logic_vector(7 downto 0);
  signal ack_in            : std_logic;
  signal write             : std_logic;
  signal read              : std_logic;
  signal stop              : std_logic;
  signal start             : std_logic;
  signal ena               : std_logic;
  signal TXUSRCLK_240      : std_logic;
  signal i2c_reset         : std_logic;
  signal fifo34_dtype      : std_logic_vector(1 downto 0);
  signal LOS_READ          : std_logic_vector(7 downto 0);
  signal tx_kin            : std_logic_vector(3 downto 0);
  signal vio_soft_reset    : std_logic_vector(0 to 0);
  signal vio_i2c_reset     : std_logic_vector(0 to 0);
  signal sys_reset_n       : std_logic;
  signal LOL_READ          : std_logic_vector(7 downto 0);
  signal RXUSRCLK_OUT      : std_logic;
  signal clk240            : std_logic;
  signal rxdata_out        : slv20_array(0 to NUM_LINKS-1);
  signal txprbs_sel        : std_logic_vector(2 downto 0);
  signal tx_din_array      : slv32_array(0 to NUM_LINKS-1);
  signal tx_kin_array      : slv4_array(0 to NUM_LINKS-1);
  signal RXOUTCLK          : std_logic_vector(NUM_LINKS-1 downto 0);
  signal RX_CDRLOCKED      : std_logic_vector(NUM_LINKS-1 downto 0);
  signal RXSLIDE           : std_logic_vector(NUM_LINKS-1 downto 0);
  signal GT_RXRESET        : std_logic_vector(NUM_LINKS-1 downto 0);
  signal FRAME_LOCKED_SLV  : std_logic_vector(0 downto 0);
  signal GBT_RXDATA0       : std_logic_vector(119 downto 0);
  signal RX_120b           : txrx120b_type(NUM_LINKS-1 downto 0);
  signal FRAME_LOCKED      : std_logic_vector(NUM_LINKS-1 downto 0);
  signal RX_FSM_RESET_DONE : std_logic_vector(NUM_LINKS-1 downto 0);

  component FMchannelTXctrl
    port (
      clk240     : in     std_logic;
      rst        : in     std_logic;
      busy       : in     std_logic;
      fifo_rclk  : out    std_logic;
      fifo_re    : out    std_logic;
      fifo_dout  : in     std_logic_vector(31 downto 0);
      fifo_dtype : in     std_logic_vector(1 downto 0);
      fifo_empty : in     std_logic;
      dout       : out    std_logic_vector(31 downto 0);
      kout       : out    std_logic_vector(3 downto 0));
  end component FMchannelTXctrl;

  component FIFO34to34b
    port (
      FIFO34b_WE   : in     std_logic;
      clk240       : in     std_logic;
      fifo34_din   : in     std_logic_vector(31 downto 0);
      fifo34_dout  : out    std_logic_vector(31 downto 0);
      fifo34_dtype : in     std_logic_vector(1 downto 0);
      fifo34_empty : out    std_logic;
      fifo34_full  : out    std_logic;
      fifo34_rclk  : in     std_logic;
      fifo34_re    : in     std_logic;
      fifo_dtype   : out    std_logic_vector(1 downto 0);
      rst          : in     std_logic);
  end component FIFO34to34b;

  component FM_example_clocking
    port (
      RXUSRCLK_OUT         : in     std_logic;
      app_clk_in_n         : in     std_logic;
      app_clk_in_p         : in     std_logic;
      clk240               : out    std_logic;
      clk40                : out    std_logic;
      clk_si5324_240_out_n : out    std_logic;
      clk_si5324_240_out_p : out    std_logic;
      reset_out            : out    std_logic; --! Active high reset out (synchronous to clk40)
      sys_reset_n          : in     std_logic);
  end component FM_example_clocking;

  component FM_example_emuram
    port (
      EMU32b_EN     : in     std_logic;
      EMU32b_dout   : out    std_logic_vector(31 downto 0);
      EMU32b_dvalid : out    std_logic;
      clk240        : in     std_logic;
      rst           : in     std_logic);
  end component FM_example_emuram;

  component FM_example_FIFOctrl
    port (
      EMU32b_EN     : out    std_logic;
      EMU32b_dout   : in     std_logic_vector(31 downto 0);
      EMU32b_dvalid : in     std_logic;
      FIFO34b_WE    : out    std_logic;
      clk240        : in     std_logic;
      fifo34_din    : out    std_logic_vector(31 downto 0);
      fifo34_dtype  : out    std_logic_vector(1 downto 0);
      fifo34_full   : in     std_logic;
      rst           : in     std_logic);
  end component FM_example_FIFOctrl;

  component si5324_init
    port (
      Din      : out    std_logic_vector(7 downto 0);
      Dout     : in     std_logic_vector(7 downto 0);
      LOL_READ : out    std_logic_vector(7 downto 0);
      LOS_READ : out    std_logic_vector(7 downto 0);
      RST      : in     std_logic;
      ack_in   : out    std_logic;
      ack_out  : in     std_logic;
      clk      : out    std_logic;
      clk40    : in     std_logic;
      cmd_ack  : in     std_logic;
      ena      : out    std_logic;
      nReset   : out    std_logic;
      read     : out    std_logic;
      start    : out    std_logic;
      stop     : out    std_logic;
      write    : out    std_logic);
  end component si5324_init;

  component ila_fullmode_rx_tx
    port (
      clk    : in     std_logic;
      probe0 : in     std_logic_vector(31 downto 0);
      probe1 : in     std_logic_vector(3 downto 0));
  end component ila_fullmode_rx_tx;

  component vio_si5324_init
    port (
      clk        : in     std_logic;
      probe_in0  : in     std_logic_vector(7 downto 0);
      probe_in1  : in     std_logic_vector(7 downto 0);
      probe_out0 : out    std_logic_vector(0 to 0);
      probe_out1 : out    std_logic_vector(0 to 0);
      probe_out2 : out    std_logic_vector(2 downto 0));
  end component vio_si5324_init;

  component FullModeTransceiver
    generic(
      NUM_LINKS               : integer := 4;
      GTX_TRANSCEIVERS        : boolean := false;
      RECOVER_CLK_FROM_RX_GBT : boolean := false);
    port (
      RESET             : in     std_logic;
      GTREFCLK0_0_N_IN  : in     std_logic;
      GTREFCLK0_0_P_IN  : in     std_logic;
      gtrxp_in          : in     std_logic_vector(NUM_LINKS-1 downto 0);
      gtrxn_in          : in     std_logic_vector(NUM_LINKS-1 downto 0);
      gttxn_out         : out    std_logic_vector(NUM_LINKS-1 downto 0);
      gttxp_out         : out    std_logic_vector(NUM_LINKS-1 downto 0);
      TXUSRCLK_OUT      : out    std_logic;
      RXUSRCLK_OUT      : out    std_logic;
      rxdata_out        : out    slv20_array(0 to NUM_LINKS-1);
      txdata_in         : in     slv32_array(0 to NUM_LINKS-1);
      txcharisk_in      : in     slv4_array(0 to NUM_LINKS-1);
      txprbssel_in      : in     std_logic_vector(2 downto 0);
      sysclk_in         : in     std_logic;
      RX_SLIDE_IN       : in     std_logic_vector(NUM_LINKS-1 downto 0);
      RX_FSM_RESET_DONE : out    std_logic_vector(NUM_LINKS-1 downto 0);
      RX_CDRLOCKED_OUT  : out    std_logic_vector(NUM_LINKS-1 downto 0);
      GT_RXRESET        : in     std_logic_vector(NUM_LINKS-1 downto 0);
      GTREFCLK0_1_N_IN  : in     std_logic;
      GTREFCLK0_1_P_IN  : in     std_logic;
      GTREFCLK0_2_N_IN  : in     std_logic;
      GTREFCLK0_2_P_IN  : in     std_logic;
      GTREFCLK0_3_N_IN  : in     std_logic;
      GTREFCLK0_3_P_IN  : in     std_logic);
  end component FullModeTransceiver;

  component FELIX_gbt_rx_wrapper
    generic(
      GBT_NUM : integer := 1);
    port (
      rst_hw            : in     std_logic;
      clk40_in          : in     std_logic;
      RX_120b_out       : out    txrx120b_type(GBT_NUM-1 downto 0);
      FRAME_LOCKED_O    : out    std_logic_vector(GBT_NUM-1 downto 0);
      RXSLIDE_out       : out    std_logic_vector(GBT_NUM-1 downto 0);
      RXOUTCLK_IN       : in     std_logic_vector(GBT_NUM-1 downto 0);
      RX_CDRLOCKED_IN   : in     std_logic_vector(GBT_NUM-1 downto 0);
      RX_DATA_20b_in    : in     slv20_array(0 to GBT_NUM-1);
      GT_RXRESET        : out    std_logic_vector(GBT_NUM-1 downto 0);
      RX_FSM_RESET_DONE : in     std_logic_vector(GBT_NUM-1 downto 0));
  end component FELIX_gbt_rx_wrapper;

  component ila_gbt_rx
    port (
      clk    : in     std_logic;
      probe0 : in     std_logic_vector(0 downto 0);
      probe1 : in     std_logic_vector(119 downto 0));
  end component ila_gbt_rx;

begin
  opto_inhibit <= (OTHERS => '0');
  SFP_TX_ENABLE <= '1';
  emcclk_out <= emcclk;
  SmaOut_x4 <= TXUSRCLK_240;
  leds <= LOL_READ;
  SmaOut_x3 <= RXUSRCLK_OUT;

  u1: for i in 0 to NUM_LINKS-1 generate
  begin
      tx_din_array(i) <= tx_din;
      tx_kin_array(i) <= tx_kin;
  end generate u1;

  u5: FMchannelTXctrl
    port map(
      clk240     => TXUSRCLK_240,
      rst        => rst,
      busy       => '0',
      fifo_rclk  => fifo_rclk,
      fifo_re    => fifo_re,
      fifo_dout  => fifo_dout,
      fifo_dtype => fifo_dtype,
      fifo_empty => fifo_empty,
      dout       => tx_din,
      kout       => tx_kin);

  u7: FIFO34to34b
    port map(
      FIFO34b_WE   => FIFO34b_WE,
      clk240       => TXUSRCLK_240,
      fifo34_din   => fifo34_din,
      fifo34_dout  => fifo_dout,
      fifo34_dtype => fifo34_dtype,
      fifo34_empty => fifo_empty,
      fifo34_full  => fifo34_full,
      fifo34_rclk  => fifo_rclk,
      fifo34_re    => fifo_re,
      fifo_dtype   => fifo_dtype,
      rst          => rst);

  i2c0: simple_i2c
    port map(
      clk     => clk,
      ena     => ena,
      nReset  => nReset,
      clk_cnt => "11111111",
      start   => start,
      stop    => stop,
      read    => read,
      write   => write,
      ack_in  => ack_in,
      Din     => Din,
      cmd_ack => cmd_ack,
      ack_out => ack_out,
      Dout    => Dout,
      SCL     => SCL,
      SDA     => SDA);


  --!------------------------------------------------------------------------------
  --!                                                             
  --!           NIKHEF - National Institute for Subatomic Physics 
  --!
  --!                       Electronics Department                
  --!                                                             
  --!-----------------------------------------------------------------------------
  --! @class FM_example_clocking
  --! 
  --!
  --! @author      Frans Schreuder (frans.schreuder@nikhef.nl)
  --!
  --!
  --! @date        08/05/2017    created
  --!
  --! @version     1.0
  --!
  --! @brief 
  --! 
  --! Inputs 156.25MHz clock (Si570 on VC709) and outputs 40.08MHz, also provides
  --! 160MHz reference clock for Si5324.
  --! 
  --! @detail
  --!
  --!-----------------------------------------------------------------------------
  --! @TODO
  --!  
  --!
  --! ------------------------------------------------------------------------------
  -- 
  --! @brief ieee
  clk1: FM_example_clocking
    port map(
      RXUSRCLK_OUT         => RXUSRCLK_OUT,
      app_clk_in_n         => app_clk_in_n,
      app_clk_in_p         => app_clk_in_p,
      clk240               => clk240,
      clk40                => clk40,
      clk_si5324_240_out_n => clk_si5324_240_out_n,
      clk_si5324_240_out_p => clk_si5324_240_out_p,
      reset_out            => rst_hw,
      sys_reset_n          => sys_reset_n);


  --!------------------------------------------------------------------------------
  --!                                                             
  --!           NIKHEF - National Institute for Subatomic Physics 
  --!
  --!                       Electronics Department                
  --!                                                             
  --!-----------------------------------------------------------------------------
  --! @class FM_example_emuram
  --! 
  --!
  --! @author      Rene Habraken(r.habraken@science.ru.nl)<br>
  --!              Frans Schreuder (frans.schreuder@nikhef.nl)
  --!
  --!
  --! @date        08/05/2017    created
  --!
  --! @version     1.0
  --!
  --! @brief 
  --! 
  --! Fullmode emulator ram block with Read enable and data valid
  --! 
  --! 
  --! @detail
  --!
  --!-----------------------------------------------------------------------------
  --! @TODO
  --!  
  --!
  --! ------------------------------------------------------------------------------
  -- 
  --! @brief ieee
  ram0: FM_example_emuram
    port map(
      EMU32b_EN     => EMU32b_EN,
      EMU32b_dout   => EMU32b_dout,
      EMU32b_dvalid => EMU32b_dvalid,
      clk240        => TXUSRCLK_240,
      rst           => rst);


  --!------------------------------------------------------------------------------
  --!                                                             
  --!           NIKHEF - National Institute for Subatomic Physics 
  --!
  --!                       Electronics Department                
  --!                                                             
  --!-----------------------------------------------------------------------------
  --! @class FM_example_FIFOctrl
  --! 
  --!
  --! @author      Rene Habraken(r.habraken@science.ru.nl)<br>
  --!              Frans Schreuder (frans.schreuder@nikhef.nl)
  --!
  --!
  --! @date        08/05/2017    created
  --!
  --! @version     1.0
  --!
  --! @brief 
  --! 
  --! Reads the emulator ram, adds a data type and controls the 34bto34b fifo
  --! 
  --! 
  --! @detail
  --!
  --!-----------------------------------------------------------------------------
  --! @TODO
  --!  
  --!
  --! ------------------------------------------------------------------------------
  -- 
  --! @brief ieee
  ctl0: FM_example_FIFOctrl
    port map(
      EMU32b_EN     => EMU32b_EN,
      EMU32b_dout   => EMU32b_dout,
      EMU32b_dvalid => EMU32b_dvalid,
      FIFO34b_WE    => FIFO34b_WE,
      clk240        => TXUSRCLK_240,
      fifo34_din    => fifo34_din,
      fifo34_dtype  => fifo34_dtype,
      fifo34_full   => fifo34_full,
      rst           => rst);


  --!------------------------------------------------------------------------------
  --!                                                             
  --!           NIKHEF - National Institute for Subatomic Physics 
  --!
  --!                       Electronics Department                
  --!                                                             
  --!-----------------------------------------------------------------------------
  --! @class si5324_init
  --! 
  --!
  --! @author      Frans Schreuder (frans.schreuder@nikhef.nl)
  --!
  --!
  --! @date        08/05/2017    created
  --!
  --! @version     1.0
  --!
  --! @brief 
  --! 
  --! Standalone routine to initialize the Si5324 (VC709) to input 160MHz and 
  --! output 240MHz
  --! 
  --! @detail
  --!
  --!-----------------------------------------------------------------------------
  --! @TODO
  --!  
  --!
  --! ------------------------------------------------------------------------------
  -- 
  --! @brief ieee
  init0: si5324_init
    port map(
      Din      => Din,
      Dout     => Dout,
      LOL_READ => LOL_READ,
      LOS_READ => LOS_READ,
      RST      => i2c_reset,
      ack_in   => ack_in,
      ack_out  => ack_out,
      clk      => clk,
      clk40    => clk40,
      cmd_ack  => cmd_ack,
      ena      => ena,
      nReset   => nReset,
      read     => read,
      start    => start,
      stop     => stop,
      write    => write);

  ila0: ila_fullmode_rx_tx
    port map(
      clk    => TXUSRCLK_240,
      probe0 => tx_din,
      probe1 => tx_kin);

  vio0: vio_si5324_init
    port map(
      clk        => clk40,
      probe_in0  => LOL_READ,
      probe_in1  => LOS_READ,
      probe_out0 => vio_soft_reset,
      probe_out1 => vio_i2c_reset,
      probe_out2 => txprbs_sel);

  u0: FullModeTransceiver
    generic map(
      NUM_LINKS               => NUM_LINKS,
      GTX_TRANSCEIVERS        => GTX_TRANSCEIVERS,
      RECOVER_CLK_FROM_RX_GBT => RECOVER_CLK_FROM_RX_GBT)
    port map(
      RESET             => rst,
      GTREFCLK0_0_N_IN  => GTREFCLK0_N_IN,
      GTREFCLK0_0_P_IN  => GTREFCLK0_P_IN,
      gtrxp_in          => gtrxp_in,
      gtrxn_in          => gtrxn_in,
      gttxn_out         => gttxn_out,
      gttxp_out         => gttxp_out,
      TXUSRCLK_OUT      => TXUSRCLK_240,
      RXUSRCLK_OUT      => RXUSRCLK_OUT,
      rxdata_out        => rxdata_out,
      txdata_in         => tx_din_array,
      txcharisk_in      => tx_kin_array,
      txprbssel_in      => txprbs_sel,
      sysclk_in         => clk40,
      RX_SLIDE_IN       => RXSLIDE,
      RX_FSM_RESET_DONE => RX_FSM_RESET_DONE,
      RX_CDRLOCKED_OUT  => RX_CDRLOCKED,
      GT_RXRESET        => GT_RXRESET,
      GTREFCLK0_1_N_IN  => '0',
      GTREFCLK0_1_P_IN  => '0',
      GTREFCLK0_2_N_IN  => '0',
      GTREFCLK0_2_P_IN  => '0',
      GTREFCLK0_3_N_IN  => '0',
      GTREFCLK0_3_P_IN  => '0');

  gbt_rx0: FELIX_gbt_rx_wrapper
    generic map(
      GBT_NUM => NUM_LINKS)
    port map(
      rst_hw            => rst,
      clk40_in          => clk40,
      RX_120b_out       => RX_120b,
      FRAME_LOCKED_O    => FRAME_LOCKED,
      RXSLIDE_out       => RXSLIDE,
      RXOUTCLK_IN       => RXOUTCLK,
      RX_CDRLOCKED_IN   => RX_CDRLOCKED,
      RX_DATA_20b_in    => rxdata_out,
      GT_RXRESET        => GT_RXRESET,
      RX_FSM_RESET_DONE => RX_FSM_RESET_DONE);

  ila1: ila_gbt_rx
    port map(
      clk    => clk40,
      probe0 => FRAME_LOCKED_SLV,
      probe1 => GBT_RXDATA0);
  rst <= rst_hw or vio_soft_reset(0);
  i2c_reset <= rst or vio_i2c_reset(0);

  si5324_resetn <= not rst;
  i2cmux_rst <= not rst;
  sys_reset_n <= not RESET_BUTTON;

  RXOUTCLK <= (others => RXUSRCLK_OUT);

  GBT_RXDATA0 <= RX_120b(0);
  FRAME_LOCKED_SLV(0) <= FRAME_LOCKED(0);
end architecture structure ; -- of FM_UserExample


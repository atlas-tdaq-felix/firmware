--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               Mesfin Gebyehu
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.



library ieee;
use ieee.std_logic_1164.all;
--use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
library work;
use work.FMTransceiverPackage.all;
use work.pcie_package.all; 

entity FullModeTransceiver is
generic
(
    NUM_LINKS                           : integer := 12
);
port
(
    RESET                               : in   std_logic;
    GT_RXRESET                          : in   std_logic_vector(NUM_LINKS-1 downto 0);
    RX_SLIDE_IN                         : in   std_logic_vector(NUM_LINKS-1 downto 0);
    FRAME_LOCKED_i                      : in   std_logic_vector(NUM_LINKS-1 downto 0);
    RX_FSM_RESET_DONE                   : out  std_logic_vector(NUM_LINKS-1 downto 0);
	TX_RESET_DONE						: out  std_logic_vector(NUM_LINKS-1 downto 0);
    RX_CDRLOCKED_OUT                    : out  std_logic_vector(NUM_LINKS-1 downto 0);
    GTREFCLK0_0_N_IN                      : in   std_logic;
    GTREFCLK0_0_P_IN                      : in   std_logic;
    GTREFCLK0_1_N_IN                      : in   std_logic;
    GTREFCLK0_1_P_IN                      : in   std_logic;
    GTREFCLK0_2_N_IN                      : in   std_logic;
    GTREFCLK0_2_P_IN                      : in   std_logic;
    GTREFCLK0_3_N_IN                      : in   std_logic;
    GTREFCLK0_3_P_IN                      : in   std_logic;
--    GTGREFCLK_IN                        : in   std_logic;
    gtrxp_in                            : in   std_logic_vector(NUM_LINKS-1 downto 0);
    gtrxn_in                            : in   std_logic_vector(NUM_LINKS-1 downto 0);
    gttxn_out                           : out  std_logic_vector(NUM_LINKS-1 downto 0);
    gttxp_out                           : out  std_logic_vector(NUM_LINKS-1 downto 0);

    TXUSRCLK_OUT                        : out  std_logic;
    RXUSRCLK_OUT                        : out  std_logic;
    
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    rxdata_out                          : out  slv20_array(0 to NUM_LINKS-1);
    ------------------ Transmit Ports - TX Data Path interface -----------------
    txdata_in                           : in   slv32_array(0 to NUM_LINKS-1);
    txcharisk_in                        : in   slv4_array(0 to NUM_LINKS-1);
    ------------------ Transmit Ports - pattern Generator Ports ----------------
    txprbssel_in                        : in   std_logic_vector(2 downto 0);
	register_map_control				: in register_map_control_type;
   -- 40 MHz system (DRP) clk
    sysclk_in        : in std_logic

);

end FullModeTransceiver;
    
architecture RTL of FullModeTransceiver is

COMPONENT fullmodetransceiver_core
  PORT (
    gtwiz_userclk_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_tx_srcclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_tx_usrclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_tx_usrclk2_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_tx_active_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_srcclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_usrclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_usrclk2_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_active_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(383 DOWNTO 0);
    gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(239 DOWNTO 0);
    gtrefclk01_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    qpll1lock_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    qpll1outclk_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    qpll1outrefclk_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    drpclk_in : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    gthrxn_in : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    gthrxp_in : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    gtrefclk0_in : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    rxslide_in : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    tx8b10ben_in : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    txctrl0_in : IN STD_LOGIC_VECTOR(191 DOWNTO 0);
    txctrl1_in : IN STD_LOGIC_VECTOR(191 DOWNTO 0);
    txctrl2_in : IN STD_LOGIC_VECTOR(95 DOWNTO 0);
    cplllock_out : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    gthtxn_out : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    gthtxp_out : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    gtpowergood_out : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    rxcdrlock_out : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    rxresetdone_out : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    txpmaresetdone_out : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    txresetdone_out : OUT STD_LOGIC_VECTOR(11 DOWNTO 0)
  );
END COMPONENT;

signal RX_FSM_RESET_DONE_S: std_logic; --_vector(NUM_LINKS/4 -1 downto 0);
signal TX_RESET_DONE_S: std_logic; --_vector(NUM_LINKS/4 -1 downto 0);

signal RXSLIDE_S: std_logic_vector(NUM_LINKS - 1 downto 0);
signal gtrefclk0_i : std_logic;
signal gtrefclk1_i : std_logic;
signal gtrefclk2_i : std_logic;
signal gtrefclk0_i_multi : std_logic_vector(NUM_LINKS/4 - 1 downto 0);
signal rxusrclk : std_logic; --_vector(NUM_LINKS/4 -1 downto 0);
signal txusrclk : std_logic; --_vector(NUM_LINKS/4 -1 downto 0);
signal rxdata_out_s : std_logic_vector( (NUM_LINKS*20) -1 downto 0);
signal txdata_in_s : std_logic_vector( (NUM_LINKS*32) -1 downto 0);
signal gtrxp_in_s, gtrxn_in_s, gttxp_out_s, gttxn_out_s : std_logic_vector(NUM_LINKS - 1 downto 0);
signal txctrl2_in_s: std_logic_vector((8*(NUM_LINKS)-1) downto 0);
signal zero_bit: std_logic := '0';
signal ones_NUM_LINKS: std_logic_vector(NUM_LINKS - 1 downto 0) := (others => '1');
signal drpclk_in, gtrefclk0_in : std_logic_vector(NUM_LINKS - 1 downto 0);
signal tx8b10ben_in_ones : std_logic_vector(NUM_LINKS - 1 downto 0):= (others => '1');
signal txctrl0_in_zeros : STD_LOGIC_VECTOR((64*NUM_LINKS/4)-1 downto 0):= (others => '0');-- ( 191 downto 0 );
signal txctrl1_in_zeros  : STD_LOGIC_VECTOR((64*NUM_LINKS/4)-1 downto 0):= (others => '0');
signal alignment_done_chk_cnt : std_logic_vector(12 downto 0);
signal alignment_done_a       : std_logic_vector(NUM_LINKS - 1 downto 0);
signal GT_RXByteisAligned     : std_logic_vector(NUM_LINKS - 1 downto 0);
signal    rxresetdone  : STD_LOGIC_VECTOR ( NUM_LINKS - 1 downto 0 ); 
signal    txresetdone  : STD_LOGIC_VECTOR ( NUM_LINKS - 1 downto 0 ); 
signal    cplllock  :  STD_LOGIC_VECTOR ( NUM_LINKS - 1 downto 0 );
signal SOFT_RESET_reg :  STD_LOGIC_VECTOR ( NUM_LINKS - 1 downto 0 );
signal TX_RESET_reg :   STD_LOGIC_VECTOR ( NUM_LINKS - 1 downto 0 );
signal RX_RESET_reg :   STD_LOGIC_VECTOR ( NUM_LINKS - 1 downto 0 );
signal SOFT_RESET :  STD_LOGIC_VECTOR ( NUM_LINKS - 1 downto 0 );
signal TX_RESET :   STD_LOGIC_VECTOR ( NUM_LINKS - 1 downto 0 );
signal RX_RESET :   STD_LOGIC_VECTOR ( NUM_LINKS - 1 downto 0 );
signal rxcdrlock_out:   STD_LOGIC_VECTOR ( NUM_LINKS - 1 downto 0 );
signal RX_CDRLOCKED_OUT_t                    : std_logic_vector(NUM_LINKS-1 downto 0);

signal rxdata_out_t : slv20_array(0 to NUM_LINKS-1);
signal txdata_in_t  : slv32_array(0 to NUM_LINKS-1);
signal txcharisk_in_t  : slv4_array(0 to NUM_LINKS-1);
signal qpll1lock : STD_LOGIC_VECTOR(2 DOWNTO 0);
signal gtwiz_buffbypass_rx_reset : STD_LOGIC_VECTOR(0 DOWNTO 0);
signal drpclk : std_logic_vector(11 downto 0);

begin
	
	bufbypass_reset: process(rxusrclk)
		variable do_reset : integer range 0 to 15;
	begin
		if rising_edge(rxusrclk) then
			if qpll1lock /= "111" then
				gtwiz_buffbypass_rx_reset <= "0";
				do_reset := 15;
			else
				if do_reset /= 0 then
					gtwiz_buffbypass_rx_reset <= "1";
					do_reset := do_reset - 1;
				else
					gtwiz_buffbypass_rx_reset <= "0";
				end if;
			end if;
		end if;
	end process;
		

RX_CDRLOCKED_OUT <= RX_CDRLOCKED_OUT_t;
drpclk_in <= (others => sysclk_in); 
gtrefclk0_in(NUM_LINKS - 1 downto 0) <= (others => gtrefclk0_i);
tx8b10ben_in_ones <= (others => '1'); 
txctrl0_in_zeros <= (others => '0');
txctrl1_in_zeros <= (others => '0');
gtrefclk0_i_multi <= (others => gtrefclk0_i);    
 
TXUSRCLK_OUT <= txusrclk;
RXUSRCLK_OUT <= rxusrclk;

g_NUM_LINKS: for i in 0 to NUM_LINKS - 1 generate
  gttxp_out(i)  <= gttxp_out_s(i);
  gttxn_out(i)  <= gttxn_out_s(i);
  gtrxp_in_s(i) <= gtrxp_in(i);
  gtrxn_in_s(i) <= gtrxn_in(i);

  rxdata_out_t(i) 					    <= rxdata_out_s( i*20+19 downto i*20);
  txdata_in_s(i*32+31 downto i*32) 	<= txdata_in_t(i);
  txctrl2_in_s(i*8+3 downto i*8) 		<= txcharisk_in_t(i);
end generate;

  
  RX_FSM_RESET_DONE <= (others => RX_FSM_RESET_DONE_S);
  TX_RESET_DONE 	<= ( others => TX_RESET_DONE_S);

-- swap the channels to adapt to the 24 channel fetch box configuration.
g_cha_swap: for i in 0 to NUM_LINKS - 1 generate
--  RX_FSM_RESET_DONE(i) 						<= rxresetdone(cha_swap(i));
  RXSLIDE_S(i) 				<= RX_SLIDE_IN(i);
  rxdata_out(i) 			<= rxdata_out_t(i);
  txdata_in_t(i)  			<= txdata_in(i);
  txcharisk_in_t(i)  		<= txcharisk_in(i);
  RX_CDRLOCKED_OUT_t(i)  	<= rxcdrlock_out(i);
  SOFT_RESET(i)  			<= SOFT_RESET_reg(i)   OR RESET;
  TX_RESET(i)   			<= TX_RESET_reg(i)  OR RESET;
  RX_RESET(i)   			<= RX_RESET_reg(i)  OR RESET;
end generate;

IBUFDS_GTE3_inst0 : IBUFDS_GTE3
generic map (
REFCLK_EN_TX_PATH => '0', -- Refer to Transceiver User Guide
REFCLK_HROW_CK_SEL => "00", -- Refer to Transceiver User Guide
REFCLK_ICNTL_RX => "00" -- Refer to Transceiver User Guide
)
port map (
O => gtrefclk0_i, -- 1-bit output: Refer to Transceiver User Guide
ODIV2 => open, -- 1-bit output: Refer to Transceiver User Guide
CEB => '0', -- 1-bit input: Refer to Transceiver User Guide
I => GTREFCLK0_0_P_IN, -- 1-bit input: Refer to Transceiver User Guide
IB => GTREFCLK0_0_N_IN -- 1-bit input: Refer to Transceiver User Guide
);

drpclk <= (others => sysclk_in);

u0 : fullmodetransceiver_core
  PORT MAP (
    drpclk_in => drpclk,
    gtrefclk0_in => gtrefclk0_in,
    gtwiz_buffbypass_rx_reset_in => gtwiz_buffbypass_rx_reset,
    gtwiz_buffbypass_rx_start_user_in => "0",
    gtwiz_buffbypass_rx_done_out => open,
    gtwiz_buffbypass_rx_error_out => open,
    qpll1lock_out => qpll1lock,
    gtwiz_userclk_tx_reset_in(0) => TX_RESET(0), --RESET, 
    gtwiz_userclk_tx_srcclk_out => open,
    gtwiz_userclk_tx_usrclk_out(0) => txusrclk,
    gtwiz_userclk_tx_usrclk2_out => open,
    gtwiz_userclk_tx_active_out => open,
    gtwiz_userclk_rx_reset_in(0) => RX_RESET(0), --RESET,
    gtwiz_userclk_rx_srcclk_out => open,
    gtwiz_userclk_rx_usrclk_out(0) => rxusrclk,
    gtwiz_userclk_rx_usrclk2_out => open,
    gtwiz_userclk_rx_active_out => open,
    gtwiz_reset_clk_freerun_in(0) => sysclk_in,
    gtwiz_reset_all_in(0) => SOFT_RESET(0), --RESET,
    gtwiz_reset_tx_pll_and_datapath_in(0) => RESET,
    gtwiz_reset_tx_datapath_in(0) => RESET,
    gtwiz_reset_rx_pll_and_datapath_in(0) => RESET,
    gtwiz_reset_rx_datapath_in(0) => RESET,
    gtwiz_reset_rx_cdr_stable_out => open,
    gtwiz_reset_tx_done_out(0) => TX_RESET_DONE_S, --open,
    gtwiz_reset_rx_done_out(0) => RX_FSM_RESET_DONE_S,
    gtwiz_userdata_tx_in => txdata_in_s, --(128*NUM_LINKS/4-1 downto 0),
    gtwiz_userdata_rx_out => rxdata_out_s, --(80*NUM_LINKS/4-1 downto 0),
    gtrefclk01_in => gtrefclk0_i_multi,
    qpll1outclk_out => open,
    qpll1outrefclk_out => open,
    gthrxn_in => gtrxn_in_s, --(NUM_LINKS - 1 downto 0),
    gthrxp_in => gtrxp_in_s, --(NUM_LINKS - 1 downto 0),
    rxslide_in => RXSLIDE_S, --(NUM_LINKS - 1 downto 0),
    tx8b10ben_in => tx8b10ben_in_ones,
    txctrl0_in => txctrl0_in_zeros,
    txctrl1_in => txctrl1_in_zeros,
    txctrl2_in => txctrl2_in_s, --(32*NUM_LINKS/4 - 1 downto 0),
    gthtxn_out => gttxn_out_s, --(NUM_LINKS - 1 downto 0),
    gthtxp_out => gttxp_out_s, --(NUM_LINKS - 1 downto 0),
	gtpowergood_out => open,
    rxpmaresetdone_out => open,
    rxresetdone_out => rxresetdone, 
    txresetdone_out => txresetdone, 
    cplllock_out => cplllock,
    rxcdrlock_out => rxcdrlock_out,
    txpmaresetdone_out => open
  );


  SOFT_RESET_reg(11 downto 0)         <= register_map_control.GBT_SOFT_RESET(11 downto 0);
  TX_RESET_reg(11 downto 0)           <= register_map_control.GBT_TX_RESET(11 downto 0);
  RX_RESET_reg(11 downto 0)           <= register_map_control.GBT_RX_RESET(11 downto 0);


  
end RTL;

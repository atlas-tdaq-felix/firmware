--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Rene Habraken
--!               Frans Schreuder
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
library work;
use work.FMTransceiverPackage.all;

entity FullModeTransceiver is
generic
(
    NUM_LINKS                           : integer := 4;     --
    GTX_TRANSCEIVERS                    : boolean := false; --set to true for GTX, false for GTH
    RECOVER_CLK_FROM_RX_GBT             : boolean := false  --set to true to connect GREFCLK_IN to the CPLL for the RX channel
);
port
(
    RESET                               : in   std_logic;
    GT_RXRESET                          : in   std_logic_vector(NUM_LINKS-1 downto 0);
    RX_SLIDE_IN                         : in   std_logic_vector(NUM_LINKS-1 downto 0);
    RX_FSM_RESET_DONE                   : out  std_logic_vector(NUM_LINKS-1 downto 0);
    RX_CDRLOCKED_OUT                    : out  std_logic_vector(NUM_LINKS-1 downto 0);
    GTREFCLK0_0_N_IN                      : in   std_logic;
    GTREFCLK0_0_P_IN                      : in   std_logic;
    GTREFCLK0_1_N_IN                      : in   std_logic;
    GTREFCLK0_1_P_IN                      : in   std_logic;
    GTREFCLK0_2_N_IN                      : in   std_logic;
    GTREFCLK0_2_P_IN                      : in   std_logic;
    GTREFCLK0_3_N_IN                      : in   std_logic;
    GTREFCLK0_3_P_IN                      : in   std_logic;
    gtrxp_in                            : in   std_logic_vector(NUM_LINKS-1 downto 0);
    gtrxn_in                            : in   std_logic_vector(NUM_LINKS-1 downto 0);
    gttxn_out                           : out  std_logic_vector(NUM_LINKS-1 downto 0);
    gttxp_out                           : out  std_logic_vector(NUM_LINKS-1 downto 0);

    TXUSRCLK_OUT                        : out  std_logic;
    RXUSRCLK_OUT                        : out  std_logic;
    
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    rxdata_out                          : out  slv20_array(0 to NUM_LINKS-1);
    ------------------ Transmit Ports - TX Data Path interface -----------------
    txdata_in                           : in   slv32_array(0 to NUM_LINKS-1);
    txcharisk_in                        : in   slv4_array(0 to NUM_LINKS-1);
    ------------------ Transmit Ports - pattern Generator Ports ----------------
    txprbssel_in                        : in   std_logic_vector(2 downto 0);
    -- 40 MHz system (DRP) clk
    sysclk_in        : in std_logic

);

end FullModeTransceiver;
    
architecture RTL of FullModeTransceiver is

component fullmodetransceiver_core 
port
(
    SYSCLK_IN                               : in   std_logic;
    SOFT_RESET_TX_IN                        : in   std_logic;
    SOFT_RESET_RX_IN                        : in   std_logic;
    DONT_RESET_ON_DATA_ERROR_IN             : in   std_logic;
    GT0_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_DATA_VALID_IN                       : in   std_logic;
    GT1_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT1_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT1_DATA_VALID_IN                       : in   std_logic;
    GT2_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT2_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT2_DATA_VALID_IN                       : in   std_logic;
    GT3_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT3_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT3_DATA_VALID_IN                       : in   std_logic;

    --_________________________________________________________________________
    --GT0  (X0Y4)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
    gt0_cpllfbclklost_out                   : out  std_logic;
    gt0_cplllock_out                        : out  std_logic;
    gt0_cplllockdetclk_in                   : in   std_logic;
    gt0_cpllreset_in                        : in   std_logic;
    -------------------------- Channel - Clocking Ports ------------------------
    gt0_gtrefclk0_in                        : in   std_logic;
    gt0_gtrefclk1_in                        : in   std_logic;
    ---------------------------- Channel - DRP Ports  --------------------------
    gt0_drpclk_in                           : in   std_logic;
    --------------------- RX Initialization and Reset Ports --------------------
    gt0_eyescanreset_in                     : in   std_logic;
    gt0_rxuserrdy_in                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    gt0_eyescandataerror_out                : out  std_logic;
    gt0_eyescantrigger_in                   : in   std_logic;
    --------------- Receive Ports - Comma Detection and Alignment --------------
    gt0_rxslide_in                          : in   std_logic;
    ------------------- Receive Ports - Digital Monitor Ports ------------------
    gt0_dmonitorout_out                     : out  std_logic_vector(14 downto 0);
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
    gt0_rxusrclk_in                         : in   std_logic;
    gt0_rxusrclk2_in                        : in   std_logic;
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    gt0_rxdata_out                          : out  std_logic_vector(19 downto 0);
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    gt0_gthrxn_in                           : in   std_logic;
    --------------------- Receive Ports - RX Equalizer Ports -------------------
    gt0_rxmonitorout_out                    : out  std_logic_vector(6 downto 0);
    gt0_rxmonitorsel_in                     : in   std_logic_vector(1 downto 0);
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
    gt0_rxoutclk_out                        : out  std_logic;
    gt0_rxoutclkfabric_out                  : out  std_logic;
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    gt0_gtrxreset_in                        : in   std_logic;
    ------------------------ Receive Ports -RX AFE Ports -----------------------
    gt0_gthrxp_in                           : in   std_logic;
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    gt0_rxresetdone_out                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    gt0_gttxreset_in                        : in   std_logic;
    gt0_txuserrdy_in                        : in   std_logic;
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
    gt0_txusrclk_in                         : in   std_logic;
    gt0_txusrclk2_in                        : in   std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    gt0_txdata_in                           : in   std_logic_vector(31 downto 0);
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    gt0_gthtxn_out                          : out  std_logic;
    gt0_gthtxp_out                          : out  std_logic;
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    gt0_txoutclk_out                        : out  std_logic;
    gt0_txoutclkfabric_out                  : out  std_logic;
    gt0_txoutclkpcs_out                     : out  std_logic;
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    gt0_txresetdone_out                     : out  std_logic;
    ------------------ Transmit Ports - pattern Generator Ports ----------------
    gt0_txprbssel_in                        : in   std_logic_vector(2 downto 0);
    ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
    gt0_txcharisk_in                        : in   std_logic_vector(3 downto 0);

    --GT1  (X0Y5)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
    gt1_cpllfbclklost_out                   : out  std_logic;
    gt1_cplllock_out                        : out  std_logic;
    gt1_cplllockdetclk_in                   : in   std_logic;
    gt1_cpllreset_in                        : in   std_logic;
    -------------------------- Channel - Clocking Ports ------------------------
    gt1_gtrefclk0_in                        : in   std_logic;
    gt1_gtrefclk1_in                        : in   std_logic;
    ---------------------------- Channel - DRP Ports  --------------------------
    gt1_drpclk_in                           : in   std_logic;
    --------------------- RX Initialization and Reset Ports --------------------
    gt1_eyescanreset_in                     : in   std_logic;
    gt1_rxuserrdy_in                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    gt1_eyescandataerror_out                : out  std_logic;
    gt1_eyescantrigger_in                   : in   std_logic;
    --------------- Receive Ports - Comma Detection and Alignment --------------
    gt1_rxslide_in                          : in   std_logic;
    ------------------- Receive Ports - Digital Monitor Ports ------------------
    gt1_dmonitorout_out                     : out  std_logic_vector(14 downto 0);
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
    gt1_rxusrclk_in                         : in   std_logic;
    gt1_rxusrclk2_in                        : in   std_logic;
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    gt1_rxdata_out                          : out  std_logic_vector(19 downto 0);
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    gt1_gthrxn_in                           : in   std_logic;
    --------------------- Receive Ports - RX Equalizer Ports -------------------
    gt1_rxmonitorout_out                    : out  std_logic_vector(6 downto 0);
    gt1_rxmonitorsel_in                     : in   std_logic_vector(1 downto 0);
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
    gt1_rxoutclk_out                        : out  std_logic;
    gt1_rxoutclkfabric_out                  : out  std_logic;
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    gt1_gtrxreset_in                        : in   std_logic;
    ------------------------ Receive Ports -RX AFE Ports -----------------------
    gt1_gthrxp_in                           : in   std_logic;
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    gt1_rxresetdone_out                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    gt1_gttxreset_in                        : in   std_logic;
    gt1_txuserrdy_in                        : in   std_logic;
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
    gt1_txusrclk_in                         : in   std_logic;
    gt1_txusrclk2_in                        : in   std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    gt1_txdata_in                           : in   std_logic_vector(31 downto 0);
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    gt1_gthtxn_out                          : out  std_logic;
    gt1_gthtxp_out                          : out  std_logic;
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    gt1_txoutclk_out                        : out  std_logic;
    gt1_txoutclkfabric_out                  : out  std_logic;
    gt1_txoutclkpcs_out                     : out  std_logic;
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    gt1_txresetdone_out                     : out  std_logic;
    ------------------ Transmit Ports - pattern Generator Ports ----------------
    gt1_txprbssel_in                        : in   std_logic_vector(2 downto 0);
    ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
    gt1_txcharisk_in                        : in   std_logic_vector(3 downto 0);

    --GT2  (X0Y6)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
    gt2_cpllfbclklost_out                   : out  std_logic;
    gt2_cplllock_out                        : out  std_logic;
    gt2_cplllockdetclk_in                   : in   std_logic;
    gt2_cpllreset_in                        : in   std_logic;
    -------------------------- Channel - Clocking Ports ------------------------
    gt2_gtrefclk0_in                        : in   std_logic;
    gt2_gtrefclk1_in                        : in   std_logic;
    ---------------------------- Channel - DRP Ports  --------------------------
    gt2_drpclk_in                           : in   std_logic;
    --------------------- RX Initialization and Reset Ports --------------------
    gt2_eyescanreset_in                     : in   std_logic;
    gt2_rxuserrdy_in                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    gt2_eyescandataerror_out                : out  std_logic;
    gt2_eyescantrigger_in                   : in   std_logic;
    --------------- Receive Ports - Comma Detection and Alignment --------------
    gt2_rxslide_in                          : in   std_logic;
    ------------------- Receive Ports - Digital Monitor Ports ------------------
    gt2_dmonitorout_out                     : out  std_logic_vector(14 downto 0);
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
    gt2_rxusrclk_in                         : in   std_logic;
    gt2_rxusrclk2_in                        : in   std_logic;
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    gt2_rxdata_out                          : out  std_logic_vector(19 downto 0);
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    gt2_gthrxn_in                           : in   std_logic;
    --------------------- Receive Ports - RX Equalizer Ports -------------------
    gt2_rxmonitorout_out                    : out  std_logic_vector(6 downto 0);
    gt2_rxmonitorsel_in                     : in   std_logic_vector(1 downto 0);
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
    gt2_rxoutclk_out                        : out  std_logic;
    gt2_rxoutclkfabric_out                  : out  std_logic;
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    gt2_gtrxreset_in                        : in   std_logic;
    ------------------------ Receive Ports -RX AFE Ports -----------------------
    gt2_gthrxp_in                           : in   std_logic;
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    gt2_rxresetdone_out                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    gt2_gttxreset_in                        : in   std_logic;
    gt2_txuserrdy_in                        : in   std_logic;
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
    gt2_txusrclk_in                         : in   std_logic;
    gt2_txusrclk2_in                        : in   std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    gt2_txdata_in                           : in   std_logic_vector(31 downto 0);
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    gt2_gthtxn_out                          : out  std_logic;
    gt2_gthtxp_out                          : out  std_logic;
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    gt2_txoutclk_out                        : out  std_logic;
    gt2_txoutclkfabric_out                  : out  std_logic;
    gt2_txoutclkpcs_out                     : out  std_logic;
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    gt2_txresetdone_out                     : out  std_logic;
    ------------------ Transmit Ports - pattern Generator Ports ----------------
    gt2_txprbssel_in                        : in   std_logic_vector(2 downto 0);
    ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
    gt2_txcharisk_in                        : in   std_logic_vector(3 downto 0);

    --GT3  (X0Y7)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
    gt3_cpllfbclklost_out                   : out  std_logic;
    gt3_cplllock_out                        : out  std_logic;
    gt3_cplllockdetclk_in                   : in   std_logic;
    gt3_cpllreset_in                        : in   std_logic;
    -------------------------- Channel - Clocking Ports ------------------------
    gt3_gtrefclk0_in                        : in   std_logic;
    gt3_gtrefclk1_in                        : in   std_logic;
    ---------------------------- Channel - DRP Ports  --------------------------
    gt3_drpclk_in                           : in   std_logic;
    --------------------- RX Initialization and Reset Ports --------------------
    gt3_eyescanreset_in                     : in   std_logic;
    gt3_rxuserrdy_in                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    gt3_eyescandataerror_out                : out  std_logic;
    gt3_eyescantrigger_in                   : in   std_logic;
    --------------- Receive Ports - Comma Detection and Alignment --------------
    gt3_rxslide_in                          : in   std_logic;
    ------------------- Receive Ports - Digital Monitor Ports ------------------
    gt3_dmonitorout_out                     : out  std_logic_vector(14 downto 0);
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
    gt3_rxusrclk_in                         : in   std_logic;
    gt3_rxusrclk2_in                        : in   std_logic;
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    gt3_rxdata_out                          : out  std_logic_vector(19 downto 0);
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    gt3_gthrxn_in                           : in   std_logic;
    --------------------- Receive Ports - RX Equalizer Ports -------------------
    gt3_rxmonitorout_out                    : out  std_logic_vector(6 downto 0);
    gt3_rxmonitorsel_in                     : in   std_logic_vector(1 downto 0);
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
    gt3_rxoutclk_out                        : out  std_logic;
    gt3_rxoutclkfabric_out                  : out  std_logic;
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    gt3_gtrxreset_in                        : in   std_logic;
    ------------------------ Receive Ports -RX AFE Ports -----------------------
    gt3_gthrxp_in                           : in   std_logic;
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    gt3_rxresetdone_out                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    gt3_gttxreset_in                        : in   std_logic;
    gt3_txuserrdy_in                        : in   std_logic;
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
    gt3_txusrclk_in                         : in   std_logic;
    gt3_txusrclk2_in                        : in   std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    gt3_txdata_in                           : in   std_logic_vector(31 downto 0);
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    gt3_gthtxn_out                          : out  std_logic;
    gt3_gthtxp_out                          : out  std_logic;
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    gt3_txoutclk_out                        : out  std_logic;
    gt3_txoutclkfabric_out                  : out  std_logic;
    gt3_txoutclkpcs_out                     : out  std_logic;
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    gt3_txresetdone_out                     : out  std_logic;
    ------------------ Transmit Ports - pattern Generator Ports ----------------
    gt3_txprbssel_in                        : in   std_logic_vector(2 downto 0);
    ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
    gt3_txcharisk_in                        : in   std_logic_vector(3 downto 0);


    --____________________________COMMON PORTS________________________________
    GT0_QPLLLOCK_IN : in std_logic;
    GT0_QPLLREFCLKLOST_IN  : in std_logic;
    GT0_QPLLRESET_OUT  : out std_logic;
     GT0_QPLLOUTCLK_IN  : in std_logic;
     GT0_QPLLOUTREFCLK_IN : in std_logic

);
end component;

signal RX_FSM_RESET_DONE_S: std_logic_vector(3 downto 0);
signal RXSLIDE_S: std_logic_vector(3 downto 0);
signal gtrefclk0_i : std_logic;
signal rxusrclk : std_logic;
signal rxoutclk0 : std_logic;
signal txusrclk : std_logic;
signal txoutclk0 : std_logic;
signal rxdata_out_s : slv20_array(0 to 3);
signal txdata_in_s : slv32_array(0 to 3);
signal txcharisk_in_s : slv4_array(0 to 3);
signal gtrxp_in_s, gtrxn_in_s, gttxp_out_s, gttxn_out_s : std_logic_vector(3 downto 0);

signal qpll_reset_from_core, QPLLRESET, QPLLREFCLKLOST, QPLLLOCK, QPLLOUTREFCLK, QPLLOUTCLK : std_logic;
constant STABLE_CLOCK_PERIOD            : integer   := 25;
constant STARTUP_DELAY        : integer := 500;--AR43482: Transceiver needs to wait for 500 ns after configuration
constant WAIT_CYCLES          : integer := STARTUP_DELAY / STABLE_CLOCK_PERIOD; -- Number of Clock-Cycles to wait after configuration
constant WAIT_MAX             : integer := WAIT_CYCLES + 10;                    -- 500 ns plus some additional margin
signal init_wait_count  : std_logic_vector(7 downto 0) :=(others => '0');
signal init_wait_done   : std_logic :='0';
signal common_reset_asserted   : std_logic :='0';
signal COMMON_RESET   : std_logic ;
type rst_type is(
    INIT, ASSERT_COMMON_RESET);
    
signal state : rst_type := INIT;

begin

g_NUM_LINKS: for i in 0 to NUM_LINKS - 1 generate
  RX_FSM_RESET_DONE(i) <= RX_FSM_RESET_DONE_S(i);
  RXSLIDE_S(i) <= RX_SLIDE_IN(i);
  rxdata_out(i) <= rxdata_out_s(i);
  txdata_in_s(i) <= txdata_in(i);
  txcharisk_in_s(i) <= txcharisk_in(i);
  
  gttxp_out(i)  <= gttxp_out_s(i);
  gttxn_out(i)  <= gttxn_out_s(i);
  gtrxp_in_s(i) <= gtrxp_in(i);
  gtrxn_in_s(i) <= gtrxn_in(i);
  
  RX_CDRLOCKED_OUT(i) <= '1';
  
end generate;

refclk_ibuf: IBUFDS_GTE2
port map(
      O               =>     gtrefclk0_i,
      ODIV2           =>    open,
      CEB             =>     '0',
      I               =>     GTREFCLK0_0_P_IN,
      IB              =>     GTREFCLK0_0_N_IN
);

rxusrclk_bufg : BUFG
port map(
    I => rxoutclk0,
    O => rxusrclk
    );
    
RXUSRCLK_OUT <= rxusrclk;

g_recover_false: if RECOVER_CLK_FROM_RX_GBT = false generate
    TXUSRCLK_OUT <= txusrclk;

    txusrclk_bufg : BUFG
    port map(
        I => txoutclk0,
        O => txusrclk
        );
end generate;

g_recover_true: if RECOVER_CLK_FROM_RX_GBT = true generate
    TXUSRCLK_OUT <= rxusrclk;
end generate;


fullmodetransceiver_core_i : fullmodetransceiver_core
port map
(
        SYSCLK_IN                       =>      sysclk_in,
        SOFT_RESET_TX_IN                =>      RESET,
        SOFT_RESET_RX_IN                =>      RESET,
        DONT_RESET_ON_DATA_ERROR_IN     =>      '1',
        GT0_TX_FSM_RESET_DONE_OUT => open,
        GT0_RX_FSM_RESET_DONE_OUT => RX_FSM_RESET_DONE_S(0),
        GT0_DATA_VALID_IN => '1',
        GT1_TX_FSM_RESET_DONE_OUT => open,
        GT1_RX_FSM_RESET_DONE_OUT => RX_FSM_RESET_DONE_S(1),
        GT1_DATA_VALID_IN => '1',
        GT2_TX_FSM_RESET_DONE_OUT => open,
        GT2_RX_FSM_RESET_DONE_OUT => RX_FSM_RESET_DONE_S(2),
        GT2_DATA_VALID_IN => '1',
        GT3_TX_FSM_RESET_DONE_OUT => open,
        GT3_RX_FSM_RESET_DONE_OUT => RX_FSM_RESET_DONE_S(3),
        GT3_DATA_VALID_IN => '1',

    --_________________________________________________________________________
    --GT0  (X0Y4)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
        gt0_cpllfbclklost_out           =>      open,
        gt0_cplllock_out                =>      open,
        gt0_cplllockdetclk_in           =>      sysclk_in,
        gt0_cpllreset_in                =>      RESET,
    -------------------------- Channel - Clocking Ports ------------------------
        gt0_gtrefclk0_in                =>      gtrefclk0_i,
        gt0_gtrefclk1_in                =>      '0',
    ---------------------------- Channel - DRP Ports  --------------------------
        gt0_drpclk_in                   =>      sysclk_in,
    --------------------- RX Initialization and Reset Ports --------------------
        gt0_eyescanreset_in             =>      '0',
        gt0_rxuserrdy_in                =>      '1',
    -------------------------- RX Margin Analysis Ports ------------------------
        gt0_eyescandataerror_out        =>      open,
        gt0_eyescantrigger_in           =>      '0',
    --------------- Receive Ports - Comma Detection and Alignment --------------
        gt0_rxslide_in                  =>      rxslide_s(0),
    ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt0_dmonitorout_out             =>      open,
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt0_rxusrclk_in                 =>      rxusrclk,
        gt0_rxusrclk2_in                =>      rxusrclk,
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt0_rxdata_out                  =>      rxdata_out_s(0),
    ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt0_gthrxn_in                   =>      gtrxn_in_s(0),
    --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt0_rxmonitorout_out            =>      open,
        gt0_rxmonitorsel_in             =>      "00",
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt0_rxoutclk_out                =>      rxoutclk0,
        gt0_rxoutclkfabric_out          =>      open,
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt0_gtrxreset_in                =>      reset,
    ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt0_gthrxp_in                   =>      gtrxp_in_s(0),
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt0_rxresetdone_out             =>      open,
    --------------------- TX Initialization and Reset Ports --------------------
        gt0_gttxreset_in                =>      reset,
        gt0_txuserrdy_in                =>      '1',
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        gt0_txusrclk_in                 =>      txusrclk,
        gt0_txusrclk2_in                =>      txusrclk,
    ------------------ Transmit Ports - TX Data Path interface -----------------
        gt0_txdata_in                   =>      txdata_in_s(0),
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        gt0_gthtxn_out                  =>      gttxn_out_s(0),
        gt0_gthtxp_out                  =>      gttxp_out_s(0),
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        gt0_txoutclk_out                =>      txoutclk0,
        gt0_txoutclkfabric_out          =>      open,
        gt0_txoutclkpcs_out             =>      open,
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        gt0_txresetdone_out             =>      open,
    ------------------ Transmit Ports - pattern Generator Ports ----------------
        gt0_txprbssel_in                =>      txprbssel_in,
    ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
        gt0_txcharisk_in                =>      txcharisk_in(0),                

    --GT1  (X0Y5)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
        gt1_cpllfbclklost_out           =>      open,
        gt1_cplllock_out                =>      open,
        gt1_cplllockdetclk_in           =>      sysclk_in,
        gt1_cpllreset_in                =>      RESET,
    -------------------------- Channel - Clocking Ports ------------------------
        gt1_gtrefclk0_in                =>      gtrefclk0_i,
        gt1_gtrefclk1_in                =>      '0',
    ---------------------------- Channel - DRP Ports  --------------------------
        gt1_drpclk_in                   =>      sysclk_in,
    --------------------- RX Initialization and Reset Ports --------------------
        gt1_eyescanreset_in             =>      '0',
        gt1_rxuserrdy_in                =>      '1',
    -------------------------- RX Margin Analysis Ports ------------------------
        gt1_eyescandataerror_out        =>      open,
        gt1_eyescantrigger_in           =>      '0',
    --------------- Receive Ports - Comma Detection and Alignment --------------
        gt1_rxslide_in                  =>      rxslide_s(1),
    ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt1_dmonitorout_out             =>      open,
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt1_rxusrclk_in                 =>      rxusrclk,
        gt1_rxusrclk2_in                =>      rxusrclk,
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt1_rxdata_out                  =>      rxdata_out_s(1),
    ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt1_gthrxn_in                   =>      gtrxn_in_s(1),
    --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt1_rxmonitorout_out            =>      open,
        gt1_rxmonitorsel_in             =>      "00",
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt1_rxoutclk_out                =>      open,
        gt1_rxoutclkfabric_out          =>      open,
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt1_gtrxreset_in                =>      reset,
    ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt1_gthrxp_in                   =>      gtrxp_in_s(1),
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt1_rxresetdone_out             =>      open,
    --------------------- TX Initialization and Reset Ports --------------------
        gt1_gttxreset_in                =>      reset,
        gt1_txuserrdy_in                =>      '1',
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        gt1_txusrclk_in                 =>      txusrclk,
        gt1_txusrclk2_in                =>      txusrclk,
    ------------------ Transmit Ports - TX Data Path interface -----------------
        gt1_txdata_in                   =>      txdata_in_s(1),
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        gt1_gthtxn_out                  =>      gttxn_out_s(1),
        gt1_gthtxp_out                  =>      gttxp_out_s(1),
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        gt1_txoutclk_out                =>      open,
        gt1_txoutclkfabric_out          =>      open,
        gt1_txoutclkpcs_out             =>      open,
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        gt1_txresetdone_out             =>      open,
    ------------------ Transmit Ports - pattern Generator Ports ----------------
        gt1_txprbssel_in                =>      txprbssel_in,
    ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
        gt1_txcharisk_in                =>      txcharisk_in(1),                

    --GT2  (X0Y6)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
        gt2_cpllfbclklost_out           =>      open,
        gt2_cplllock_out                =>      open,
        gt2_cplllockdetclk_in           =>      sysclk_in,
        gt2_cpllreset_in                =>      RESET,
    -------------------------- Channel - Clocking Ports ------------------------
        gt2_gtrefclk0_in                =>      gtrefclk0_i,
        gt2_gtrefclk1_in                =>      '0',
    ---------------------------- Channel - DRP Ports  --------------------------
        gt2_drpclk_in                   =>      sysclk_in,
    --------------------- RX Initialization and Reset Ports --------------------
        gt2_eyescanreset_in             =>      '0',
        gt2_rxuserrdy_in                =>      '1',
    -------------------------- RX Margin Analysis Ports ------------------------
        gt2_eyescandataerror_out        =>      open,
        gt2_eyescantrigger_in           =>      '0',
    --------------- Receive Ports - Comma Detection and Alignment --------------
        gt2_rxslide_in                  =>      rxslide_s(2),
    ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt2_dmonitorout_out             =>      open,
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt2_rxusrclk_in                 =>      rxusrclk,
        gt2_rxusrclk2_in                =>      rxusrclk,
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt2_rxdata_out                  =>      rxdata_out_s(2),
    ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt2_gthrxn_in                   =>      gtrxn_in_s(2),
    --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt2_rxmonitorout_out            =>      open,
        gt2_rxmonitorsel_in             =>      "00",
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt2_rxoutclk_out                =>      open,
        gt2_rxoutclkfabric_out          =>      open,
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt2_gtrxreset_in                =>      reset,
    ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt2_gthrxp_in                   =>      gtrxp_in_s(2),
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt2_rxresetdone_out             =>      open,
    --------------------- TX Initialization and Reset Ports --------------------
        gt2_gttxreset_in                =>      reset,
        gt2_txuserrdy_in                =>      '1',
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        gt2_txusrclk_in                 =>      txusrclk,
        gt2_txusrclk2_in                =>      txusrclk,
    ------------------ Transmit Ports - TX Data Path interface -----------------
        gt2_txdata_in                   =>      txdata_in_s(2),
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        gt2_gthtxn_out                  =>      gttxn_out_s(2),
        gt2_gthtxp_out                  =>      gttxp_out_s(2),
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        gt2_txoutclk_out                =>      open,
        gt2_txoutclkfabric_out          =>      open,
        gt2_txoutclkpcs_out             =>      open,
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        gt2_txresetdone_out             =>      open,
    ------------------ Transmit Ports - pattern Generator Ports ----------------
        gt2_txprbssel_in                =>      txprbssel_in,
    ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
        gt2_txcharisk_in                =>      txcharisk_in(2),                

    --GT3  (X0Y7)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
        gt3_cpllfbclklost_out           =>      open,
        gt3_cplllock_out                =>      open,
        gt3_cplllockdetclk_in           =>      sysclk_in,
        gt3_cpllreset_in                =>      RESET,
    -------------------------- Channel - Clocking Ports ------------------------
        gt3_gtrefclk0_in                =>      gtrefclk0_i,
        gt3_gtrefclk1_in                =>      '0',
    ---------------------------- Channel - DRP Ports  --------------------------
        gt3_drpclk_in                   =>      sysclk_in,
    --------------------- RX Initialization and Reset Ports --------------------
        gt3_eyescanreset_in             =>      '0',
        gt3_rxuserrdy_in                =>      '1',
    -------------------------- RX Margin Analysis Ports ------------------------
        gt3_eyescandataerror_out        =>      open,
        gt3_eyescantrigger_in           =>      '0',
    --------------- Receive Ports - Comma Detection and Alignment --------------
        gt3_rxslide_in                  =>      rxslide_s(3),
    ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt3_dmonitorout_out             =>      open,
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt3_rxusrclk_in                 =>      rxusrclk,
        gt3_rxusrclk2_in                =>      rxusrclk,
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt3_rxdata_out                  =>      rxdata_out_s(3),
    ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt3_gthrxn_in                   =>      gtrxn_in_s(3),
    --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt3_rxmonitorout_out            =>      open,
        gt3_rxmonitorsel_in             =>      "00",
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt3_rxoutclk_out                =>      open,
        gt3_rxoutclkfabric_out          =>      open,
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt3_gtrxreset_in                =>      reset,
    ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt3_gthrxp_in                   =>      gtrxp_in_s(3),
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt3_rxresetdone_out             =>      open,
    --------------------- TX Initialization and Reset Ports --------------------
        gt3_gttxreset_in                =>      reset,
        gt3_txuserrdy_in                =>      '1',
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        gt3_txusrclk_in                 =>      txusrclk,
        gt3_txusrclk2_in                =>      txusrclk,
    ------------------ Transmit Ports - TX Data Path interface -----------------
        gt3_txdata_in                   =>      txdata_in_s(3),
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        gt3_gthtxn_out                  =>      gttxn_out_s(3),
        gt3_gthtxp_out                  =>      gttxp_out_s(3),
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        gt3_txoutclk_out                =>      open,
        gt3_txoutclkfabric_out          =>      open,
        gt3_txoutclkpcs_out             =>      open,
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        gt3_txresetdone_out             =>      open,
    ------------------ Transmit Ports - pattern Generator Ports ----------------
        gt3_txprbssel_in                =>      txprbssel_in,
    ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
        gt3_txcharisk_in                =>      txcharisk_in(3),                


    --____________________________COMMON PORTS________________________________
        GT0_QPLLLOCK_IN => QPLLLOCK, 
        GT0_QPLLREFCLKLOST_IN => QPLLREFCLKLOST, 
        GT0_QPLLRESET_OUT => qpll_reset_from_core, 
        GT0_QPLLOUTCLK_IN  => QPLLOUTCLK,
        GT0_QPLLOUTREFCLK_IN => QPLLOUTREFCLK 

);


  process(sysclk_in)
  begin
    if rising_edge(sysclk_in) then
      -- The counter starts running when configuration has finished and 
      -- the clock is stable. When its maximum count-value has been reached,
      -- the 500 ns from Answer Record 43482 have been passed.
      if init_wait_count = WAIT_MAX then
        init_wait_done <= '1';
      else
        init_wait_count <= init_wait_count + 1;
      end if;
    end if;
  end process;

  process(sysclk_in)
  begin
    if rising_edge(sysclk_in) then
      if(reset = '1') then
        state                <= INIT;
        common_reset_asserted   <= '0';
        COMMON_RESET   <= '0';
      else
        
        case state is
          when INIT => 
            if init_wait_done = '1' then
              state        <= ASSERT_COMMON_RESET;
            end if;
            
          when ASSERT_COMMON_RESET =>
             if common_reset_asserted = '0' then
                COMMON_RESET          <= '1';
                common_reset_asserted  <= '1';
              else
                COMMON_RESET          <= '0';
              end if;
           when OTHERS =>
            state   <= INIT;
         end case;
       end if;
    end if;
  end process;

QPLLRESET <= COMMON_RESET or qpll_reset_from_core;

gthe2_common_i : GTHE2_COMMON
    generic map
    (
            -- Simulation attributes
            SIM_RESET_SPEEDUP    => "TRUE",
            SIM_QPLLREFCLK_SEL   => "001",
            SIM_VERSION          => "2.0",


       ------------------COMMON BLOCK Attributes---------------
        BIAS_CFG                                =>     (x"0000040000001050"),
        COMMON_CFG                              =>     (x"0000005C"),
        QPLL_CFG                                =>     (x"04801C7"),
        QPLL_CLKOUT_CFG                         =>     ("1111"),
        QPLL_COARSE_FREQ_OVRD                   =>     ("010000"),
        QPLL_COARSE_FREQ_OVRD_EN                =>     ('0'),
        QPLL_CP                                 =>     ("0000011111"),
        QPLL_CP_MONITOR_EN                      =>     ('0'),
        QPLL_DMONITOR_SEL                       =>     ('0'),
        QPLL_FBDIV                              =>     ("0010000000"),
        QPLL_FBDIV_MONITOR_EN                   =>     ('0'),
        QPLL_FBDIV_RATIO                        =>     ('1'),
        QPLL_INIT_CFG                           =>     (x"000006"),
        QPLL_LOCK_CFG                           =>     (x"05E8"),
        QPLL_LPF                                =>     ("1111"),
        QPLL_REFCLK_DIV                         =>     (1),
        RSVD_ATTR0                              =>     (x"0000"),
        RSVD_ATTR1                              =>     (x"0000"),
        QPLL_RP_COMP                            =>     ('0'),
        QPLL_VTRL_RESET                         =>     ("00"),
        RCAL_CFG                                =>     ("00")

        
    )
    port map
    (
        ------------- Common Block  - Dynamic Reconfiguration Port (DRP) -----------
        DRPADDR                         =>      (others => '0'),
        DRPCLK                          =>      '0',
        DRPDI                           =>      x"0000",
        DRPDO                           =>      open,
        DRPEN                           =>      '0',
        DRPRDY                          =>      open,
        DRPWE                           =>      '0',
        ---------------------- Common Block  - Ref Clock Ports ---------------------
        GTGREFCLK                       =>      '0',
        GTNORTHREFCLK0                  =>      '0',
        GTNORTHREFCLK1                  =>      '0',
        GTREFCLK0                       =>      gtrefclk0_i,
        GTREFCLK1                       =>      '0',
        GTSOUTHREFCLK0                  =>      '0',
        GTSOUTHREFCLK1                  =>      '0',
        ------------------------- Common Block -  QPLL Ports -----------------------
        QPLLDMONITOR                    =>      open,
        ----------------------- Common Block - Clocking Ports ----------------------
        QPLLOUTCLK                      =>      QPLLOUTCLK,
        QPLLOUTREFCLK                   =>      QPLLOUTREFCLK,
        REFCLKOUTMONITOR                =>      open,
        ------------------------- Common Block - QPLL Ports ------------------------
        BGRCALOVRDENB                   =>      '1',
        PMARSVDOUT                      =>      open,
        QPLLFBCLKLOST                   =>      open,
        QPLLLOCK                        =>      QPLLLOCK,
        QPLLLOCKDETCLK                  =>      sysclk_in,
        QPLLLOCKEN                      =>      '1',
        QPLLOUTRESET                    =>      '0',
        QPLLPD                          =>      '0',
        QPLLREFCLKLOST                  =>      QPLLREFCLKLOST,
        QPLLREFCLKSEL                   =>      "001",
        QPLLRESET                       =>      QPLLRESET,
        QPLLRSVD1                       =>      "0000000000000000",
        QPLLRSVD2                       =>      "11111",
        --------------------------------- QPLL Ports -------------------------------
        BGBYPASSB                       =>      '1',
        BGMONITORENB                    =>      '1',
        BGPDB                           =>      '1',
        BGRCALOVRD                      =>      "11111",
        PMARSVD                         =>      "00000000",
        RCALENB                         =>      '1'

    );
    
    
    


end RTL;









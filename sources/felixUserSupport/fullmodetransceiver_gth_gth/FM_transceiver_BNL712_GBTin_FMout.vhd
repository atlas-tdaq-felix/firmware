--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               RHabraken
--!               Mesfin Gebyehu
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.



library ieee;
use ieee.std_logic_1164.all;
--use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use work.centralRouter_package.all;
use work.FMTransceiverPackage.all;

entity FullModeTransceiver is
generic
(
    NUM_LINKS                           : integer := 12
);
port
(
    RESET                               : in   std_logic;
    reset_rx_all						: in   std_logic_vector(NUM_LINKS/4 -1 downto 0);
	TXCLK_RESET_in						: in   std_logic_vector(NUM_LINKS-1 downto 0);					
    RX_SLIDE_IN                         : in   std_logic_vector(NUM_LINKS-1 downto 0);
    RX_FSM_RESET_DONE                   : out  std_logic_vector(NUM_LINKS-1 downto 0);
    TX_FSM_RESET_DONE                   : out  std_logic_vector(NUM_LINKS-1 downto 0);
    RX_CDRLOCKED_OUT                    : out  std_logic_vector(NUM_LINKS-1 downto 0);
    GTREFCLK0_0_N_IN                      : in   std_logic;
    GTREFCLK0_0_P_IN                      : in   std_logic;
    GTREFCLK0_1_N_IN                      : in   std_logic;
    GTREFCLK0_1_P_IN                      : in   std_logic;
    GTREFCLK0_2_N_IN                      : in   std_logic;
    GTREFCLK0_2_P_IN                      : in   std_logic;
    GTREFCLK0_3_N_IN                      : in   std_logic;
    GTREFCLK0_3_P_IN                      : in   std_logic;
--    GTGREFCLK_IN                        : in   std_logic;
    gtrxp_in                            : in   std_logic_vector(NUM_LINKS-1 downto 0);
    gtrxn_in                            : in   std_logic_vector(NUM_LINKS-1 downto 0);
    gttxn_out                           : out  std_logic_vector(NUM_LINKS-1 downto 0);
    gttxp_out                           : out  std_logic_vector(NUM_LINKS-1 downto 0);

    TXUSRCLK_OUT                        : out  std_logic_vector(NUM_LINKS-1 downto 0);
    RXUSRCLK_OUT                        : out  std_logic_vector(NUM_LINKS-1 downto 0);
    
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    rxdata_out                          : out  slv20_array(0 to NUM_LINKS-1);
    ------------------ Transmit Ports - TX Data Path interface -----------------
    txdata_in                           : in   slv32_array(0 to NUM_LINKS-1);
    txcharisk_in                        : in   slv4_array(0 to NUM_LINKS-1);
    ------------------ Transmit Ports - pattern Generator Ports ----------------
    txprbssel_in                        : in   std_logic_vector(2 downto 0);
    -- 40 MHz system (DRP) clk
    sysclk_in        : in std_logic

);

end FullModeTransceiver;
    
architecture RTL of FullModeTransceiver is
COMPONENT fullmodetransceiver_core
  PORT (
    gtwiz_userclk_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_tx_srcclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_tx_usrclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_tx_usrclk2_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_tx_active_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(79 DOWNTO 0);
    gtrefclk01_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1outclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1outrefclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    drpclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gthrxn_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gthrxp_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtrefclk0_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxslide_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    tx8b10ben_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txctrl0_in : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    txctrl1_in : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    txctrl2_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    gthtxn_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gthtxp_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtpowergood_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxcdrlock_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
  );
END COMPONENT;
COMPONENT vio_0
  PORT (
    clk : IN STD_LOGIC;
    probe_in0 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
    probe_in1 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
    probe_in2 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
    probe_in3 : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    probe_in4 : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    probe_in5 : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    probe_in6 : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    probe_out0 : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    probe_out1 : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    probe_out2 : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    probe_out3 : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    probe_out4 : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    probe_out5 : OUT STD_LOGIC_VECTOR(5 DOWNTO 0)
  );
END COMPONENT;


signal RX_FSM_RESET_DONE_S		: std_logic_vector(NUM_LINKS/4-1 downto 0);
signal TX_FSM_RESET_DONE_S		: std_logic_vector(NUM_LINKS/4-1 downto 0);
signal RXSLIDE_S				: std_logic_vector(NUM_LINKS - 1 downto 0);
signal rxcdrlock_out			: std_logic_vector(NUM_LINKS - 1 downto 0);
signal gtrefclk0_i 				: std_logic;
signal gtrefclk1_i 				: std_logic;

signal rxusrclk 				: std_logic_vector(NUM_LINKS-1 downto 0);
signal rxoutclk 				: std_logic_vector(NUM_LINKS-1 downto 0);

signal txusrclk 				: std_logic_vector(NUM_LINKS/4-1 downto 0);

signal rxdata_out_s 			: std_logic_vector( (NUM_LINKS*20) -1 downto 0);
signal txdata_in_s 				: std_logic_vector( (NUM_LINKS*32) -1 downto 0);
signal gtrxp_in_s 				: std_logic_vector(NUM_LINKS - 1 downto 0);
signal gtrxn_in_s 				: std_logic_vector(NUM_LINKS - 1 downto 0);
signal gttxp_out_s 				: std_logic_vector(NUM_LINKS - 1 downto 0);
signal gttxn_out_s 				: std_logic_vector(NUM_LINKS - 1 downto 0);
signal txctrl2_in_s				: std_logic_vector((8*(NUM_LINKS)-1) downto 0);
--Monitor signals (connect to VIO)
signal rxpmaresetdone_out		: std_logic_vector(NUM_LINKS-1 downto 0);
signal txpmaresetdone_out		: std_logic_vector(NUM_LINKS-1 downto 0);
signal gtpowergood_out			: std_logic_vector(NUM_LINKS-1 downto 0);
signal gtwiz_reset_rx_done_out	: std_logic_vector(NUM_LINKS/4-1 downto 0);
signal gtwiz_reset_tx_done_out	: std_logic_vector(NUM_LINKS/4-1 downto 0);
signal gtwiz_reset_rx_cdr_stable_out: std_logic_vector(NUM_LINKS/4-1 downto 0);
--Resets, connected to VIO
signal gtwiz_reset_rx_datapath_in 				: std_logic_vector(NUM_LINKS/4-1 downto 0);
signal gtwiz_reset_rx_datapath_in_s				: std_logic_vector(NUM_LINKS/4-1 downto 0);
signal gtwiz_reset_rx_pll_and_datapath_in 		: std_logic_vector(NUM_LINKS/4-1 downto 0);
signal gtwiz_reset_rx_pll_and_datapath_in_s		: std_logic_vector(NUM_LINKS/4-1 downto 0);
signal gtwiz_reset_all_in, gtwiz_reset_all_in_s	: std_logic_vector(NUM_LINKS/4-1 downto 0);
signal gtwiz_reset_tx_pll_and_datapath_in 		: std_logic_vector(NUM_LINKS/4-1 downto 0);
signal gtwiz_reset_tx_pll_and_datapath_in_s		: std_logic_vector(NUM_LINKS/4-1 downto 0);
signal gtwiz_reset_tx_datapath_in 				: std_logic_vector(NUM_LINKS/4-1 downto 0);
signal gtwiz_reset_tx_datapath_in_s 			: std_logic_vector(NUM_LINKS/4-1 downto 0);
signal gtwiz_userclk_tx_reset_in 				: std_logic_vector(NUM_LINKS/4-1 downto 0);
signal gtwiz_userclk_tx_reset_in_s				: std_logic_vector(NUM_LINKS/4-1 downto 0);
signal gtwiz_userclk_rx_active_in 				: std_logic_vector(NUM_LINKS/4-1 downto 0);
signal gtwiz_userclk_tx_active_out 				: std_logic_vector(NUM_LINKS/4-1 downto 0) := (others => '1');
type drpclk_in_type is array (NUM_LINKS/4-1 downto 0) of std_logic_vector(3 downto 0);
signal drpclk_in, gtrefclk0_in: drpclk_in_type;

signal TXCLK_RESET_in_or			: std_logic_vector(NUM_LINKS/4-1 downto 0);
constant tx8b10ben_in 				: std_logic_vector(3 downto 0) := "1111";
constant txctrl0_in 				: std_logic_vector(63 downto 0) := (others => '0');
constant txctrl1_in 				: std_logic_vector(63 downto 0) := (others => '0');


begin


transceiver_vio0: vio_0
  PORT MAP (
    clk => sysclk_in,
    probe_in0 => rxpmaresetdone_out,
    probe_in1 => txpmaresetdone_out,
    probe_in2 => gtpowergood_out,
    probe_in3 => gtwiz_reset_rx_done_out,
    probe_in4 => gtwiz_reset_tx_done_out,
    probe_in5 => gtwiz_reset_rx_cdr_stable_out,
    probe_in6 => gtwiz_userclk_rx_active_in,
    probe_out0 => gtwiz_reset_rx_datapath_in_s,
    probe_out1 => gtwiz_reset_rx_pll_and_datapath_in_s,
    probe_out2 => gtwiz_reset_all_in_s,
    probe_out3 => gtwiz_reset_tx_pll_and_datapath_in_s,
    probe_out4 => gtwiz_reset_tx_datapath_in_s,
    probe_out5 => gtwiz_userclk_tx_reset_in_s
  );

g_txusrclk: for i in 0 to NUM_LINKS/4 - 1 generate
  TXUSRCLK_OUT(i*4+3 downto i*4) <= (others => txusrclk(i));
  TXCLK_RESET_in_or(i) <= TXCLK_RESET_in(i*4) or TXCLK_RESET_in(i*4+1) or TXCLK_RESET_in(i*4+2) or TXCLK_RESET_in(i*4+3);
end generate;

RXUSRCLK_OUT <= rxusrclk;

g_NUM_LINKS: for i in 0 to NUM_LINKS - 1 generate
  RX_FSM_RESET_DONE(i) 				<= RX_FSM_RESET_DONE_S(i/4);
  TX_FSM_RESET_DONE(i) 				<= TX_FSM_RESET_DONE_S(i/4);
  RXSLIDE_S(i) 						<= RX_SLIDE_IN(i);
  rxdata_out(i) 					<= rxdata_out_s(i*20+19 downto i*20);
  txdata_in_s(i*32+31 downto i*32) 	<= txdata_in(i);
  txctrl2_in_s(i*8+3 downto i*8) 	<= txcharisk_in(i);
  txctrl2_in_s(i*8+7 downto i*8+4) 	<= (others => '0');
  
  gttxp_out(i)  <= gttxp_out_s(i);
  gttxn_out(i)  <= gttxn_out_s(i);
  gtrxp_in_s(i) <= gtrxp_in(i);
  gtrxn_in_s(i) <= gtrxn_in(i);
  
  RX_CDRLOCKED_OUT(i) <= rxcdrlock_out(i); --'1';
  
  BUFG_GT_inst_rx: BUFG_GT port map(
  	O 		=> rxusrclk(i),
  	CE 		=> '1',
  	CEMASK 	=> '0',
  	CLR 	=> '0',
  	CLRMASK => '0',
  	DIV 	=> "000",
  	I 		=> rxoutclk(i)
  );

  
end generate;
--  BUFG_GT_inst_tx0: BUFG_GT port map(
--  	O 		=> txoutclk1_gt,
--  	CE 		=> '1', --CESYNC(i),
--  	CEMASK 	=> '1',
--  	CLR 	=> '0', --CLRSYNC(i),
--  	CLRMASK => '1',
--  	DIV 	=> "000",
--  	I 		=> txoutclk1
--  );
--  BUFG_GT_inst_tx1: BUFG_GT port map(
--  	O 		=> txoutclk0_gt,
--  	CE 		=> '1', --CESYNC(i),
--  	CEMASK 	=> '1',
--  	CLR 	=> '0', --CLRSYNC(i),
--  	CLRMASK => '1',
--  	DIV 	=> "000",
--  	I 		=> txoutclk0(0)
--  );
--txoutclk0 <= txoutclk(0);  
--txusrclk <= (others => txoutclk0_gt);

IBUFDS_GTE3_inst0 : IBUFDS_GTE3
generic map (
REFCLK_EN_TX_PATH 	=> '0', -- Refer to Transceiver User Guide
REFCLK_HROW_CK_SEL 	=> "00", -- Refer to Transceiver User Guide
REFCLK_ICNTL_RX 	=> "00" -- Refer to Transceiver User Guide
)
port map (
O 			=> gtrefclk0_i, -- 1-bit output: Refer to Transceiver User Guide
ODIV2 		=> open, --txoutclk0, --open, -- 1-bit output: Refer to Transceiver User Guide
CEB 		=> '0', -- 1-bit input: Refer to Transceiver User Guide
I 			=> GTREFCLK0_0_P_IN, -- 1-bit input: Refer to Transceiver User Guide
IB 			=> GTREFCLK0_0_N_IN -- 1-bit input: Refer to Transceiver User Guide
);

IBUFDS_GTE3_inst1 : IBUFDS_GTE3
generic map (
REFCLK_EN_TX_PATH 	=> '0', -- Refer to Transceiver User Guide
REFCLK_HROW_CK_SEL 	=> "00", -- Refer to Transceiver User Guide
REFCLK_ICNTL_RX 	=> "00" -- Refer to Transceiver User Guide
)
port map (
O 		=> gtrefclk1_i, -- 1-bit output: Refer to Transceiver User Guide
ODIV2 	=> open, --txoutclk1, --open, -- 1-bit output: Refer to Transceiver User Guide
CEB 	=> '0', -- 1-bit input: Refer to Transceiver User Guide
I 		=> GTREFCLK0_1_P_IN, -- 1-bit input: Refer to Transceiver User Guide
IB 		=> GTREFCLK0_1_N_IN -- 1-bit input: Refer to Transceiver User Guide
);

--txoutclk(23 downto 0) <= (others => txoutclk0_gt);
--txoutclk(23 downto 12) <= (others => txoutclk1_gt);
--IBUFDS_GTE3_inst2 : IBUFDS_GTE3
--generic map (
--REFCLK_EN_TX_PATH 	=> '0', -- Refer to Transceiver User Guide
--REFCLK_HROW_CK_SEL 	=> "00", -- Refer to Transceiver User Guide
--REFCLK_ICNTL_RX 	=> "00" -- Refer to Transceiver User Guide
--)
--port map (
--O 		=> gtrefclk2_i, -- 1-bit output: Refer to Transceiver User Guide
--ODIV2 	=> open, -- 1-bit output: Refer to Transceiver User Guide
--CEB 	=> '0', -- 1-bit input: Refer to Transceiver User Guide
--I 		=> GTREFCLK0_2_P_IN, -- 1-bit input: Refer to Transceiver User Guide
--IB 		=> GTREFCLK0_2_N_IN -- 1-bit input: Refer to Transceiver User Guide
--);


GEN_FOR12CH: if NUM_LINKS <= 12 generate

	g_quads: for i in 0 to (NUM_LINKS/4)-1 generate
		
		begin
		
		--##################### for 12 channel
		gtwiz_reset_rx_done_out(i) <= RX_FSM_RESET_DONE_S(i);
		gtwiz_reset_tx_done_out(i) <= TX_FSM_RESET_DONE_S(i);
		drpclk_in(i) 	<=  (others => sysclk_in);
		gtrefclk0_in(i) <= (others => gtrefclk0_i);
		
		gtwiz_reset_rx_datapath_in(i) 			<= gtwiz_reset_rx_datapath_in_s(i) ;
		gtwiz_reset_rx_pll_and_datapath_in(i) 	<= gtwiz_reset_rx_pll_and_datapath_in_s(i) ;
		gtwiz_reset_all_in(i) 					<=  gtwiz_reset_all_in_s(i) or reset_rx_all(i);
		gtwiz_reset_tx_pll_and_datapath_in(i) 	<= gtwiz_reset_tx_pll_and_datapath_in_s(i) ;
		gtwiz_reset_tx_datapath_in(i) 			<= gtwiz_reset_tx_datapath_in_s(i) ;
		gtwiz_userclk_tx_reset_in(i) 			<= gtwiz_userclk_tx_reset_in_s(i) or TXCLK_RESET_in_or(i);
		gtwiz_userclk_rx_active_in(i) 			<= gtwiz_userclk_tx_active_out(i);
		
		
		u0 : fullmodetransceiver_core
		PORT MAP (
			gtwiz_userclk_rx_active_in 			=> gtwiz_userclk_rx_active_in(i downto i),
			rxusrclk_in 						=> rxusrclk(i*4+3 downto i*4),
			rxusrclk2_in 						=> rxusrclk(i*4+3 downto i*4),
			gtpowergood_out 					=> gtpowergood_out(i*4+3 downto i*4),
			rxoutclk_out 						=> rxoutclk(i*4+3 downto i*4),
			gtwiz_userclk_tx_reset_in 			=> gtwiz_userclk_tx_reset_in(i downto i),
			gtwiz_buffbypass_tx_reset_in 		=> (others => '0'), --reset_rx_all(i downto i),
			gtwiz_buffbypass_tx_start_user_in 	=> (others => '0'), --reset_rx_all(i downto i),
			gtwiz_buffbypass_tx_done_out 		=> open,
			gtwiz_buffbypass_tx_error_out 		=> open,
			gtwiz_userclk_tx_srcclk_out 		=> open,
			gtwiz_userclk_tx_usrclk_out 		=> txusrclk(i downto i),
			gtwiz_userclk_tx_usrclk2_out 		=> open,
			gtwiz_userclk_tx_active_out 		=> gtwiz_userclk_tx_active_out(i downto i),
			gtwiz_reset_clk_freerun_in(0) 		=> sysclk_in,
			gtwiz_reset_all_in 					=> gtwiz_reset_all_in(i downto i),
			gtwiz_reset_tx_pll_and_datapath_in 	=> gtwiz_reset_tx_pll_and_datapath_in(i downto i),
			gtwiz_reset_tx_datapath_in 			=> gtwiz_reset_tx_datapath_in(i downto i),
			gtwiz_reset_rx_pll_and_datapath_in 	=> gtwiz_reset_rx_pll_and_datapath_in(i downto i),
			gtwiz_reset_rx_datapath_in 			=> gtwiz_reset_rx_datapath_in(i downto i),
			gtwiz_reset_rx_cdr_stable_out 		=> gtwiz_reset_rx_cdr_stable_out(i downto i),
			gtwiz_reset_tx_done_out 			=> TX_FSM_RESET_DONE_S(i downto i),
			gtwiz_reset_rx_done_out 			=> RX_FSM_RESET_DONE_S(i downto i),
			gtwiz_userdata_tx_in 				=> txdata_in_s((128*(i+1))-1 downto (128 * (i))),
			gtwiz_userdata_rx_out 				=> rxdata_out_s((80*(i+1))-1 downto (80 * (i))),
			gtrefclk01_in(0) 					=> gtrefclk0_i,
			qpll1outclk_out 					=> open,
			qpll1outrefclk_out 					=> open,
			drpclk_in 							=> drpclk_in(i),
			gthrxn_in 							=> gtrxn_in_s((4*(i+1))-1 downto 4*(i)),
			gthrxp_in 							=> gtrxp_in_s((4*(i+1))-1 downto 4*(i)),
			gtrefclk0_in 						=> gtrefclk0_in(i),
			rxslide_in 							=> RXSLIDE_S((4*(i+1))-1 downto 4*(i)),
			tx8b10ben_in 						=> tx8b10ben_in,
			txctrl0_in 							=> txctrl0_in,
			txctrl1_in 							=> txctrl1_in,
			txctrl2_in 							=> txctrl2_in_s((32*(i+1))-1 downto 32*(i)),
			gthtxn_out 							=> gttxn_out_s((4*(i+1))-1 downto 4*(i)),
			gthtxp_out 							=> gttxp_out_s((4*(i+1))-1 downto 4*(i)),
			rxpmaresetdone_out 					=> rxpmaresetdone_out((4*(i+1))-1 downto 4*(i)),
			txpmaresetdone_out 					=> txpmaresetdone_out((4*(i+1))-1 downto 4*(i)),
			rxcdrlock_out 						=> rxcdrlock_out((4*(i+1))-1 downto 4*(i))
		);
	end generate;	
end generate GEN_FOR12CH;


GEN_FOR24CH: if NUM_LINKS > 12 generate 

	g_quads0: for i in 0 to 2 generate
		
		begin
		
		--##################### for 12 channel
		gtwiz_reset_rx_done_out(i) <= RX_FSM_RESET_DONE_S(i);
		gtwiz_reset_tx_done_out(i) <= TX_FSM_RESET_DONE_S(i);
		
		drpclk_in(i) 	<=  (others => sysclk_in);
		gtrefclk0_in(i) <= (others => gtrefclk0_i);
		
		gtwiz_reset_rx_datapath_in(i) 			<= gtwiz_reset_rx_datapath_in_s(i) ;
		gtwiz_reset_rx_pll_and_datapath_in(i) 	<= gtwiz_reset_rx_pll_and_datapath_in_s(i) ;
		gtwiz_reset_all_in(i) 					<=  gtwiz_reset_all_in_s(i) or reset_rx_all(i);
		gtwiz_reset_tx_pll_and_datapath_in(i) 	<= gtwiz_reset_tx_pll_and_datapath_in_s(i) ;
		gtwiz_reset_tx_datapath_in(i) 			<= gtwiz_reset_tx_datapath_in_s(i) ;
		gtwiz_userclk_tx_reset_in(i) 			<= gtwiz_userclk_tx_reset_in_s(i) ;
		gtwiz_userclk_rx_active_in(i) 			<= gtwiz_userclk_tx_active_out(i);
		
		
		u0 : fullmodetransceiver_core
		PORT MAP (
			gtwiz_userclk_rx_active_in 			=> gtwiz_userclk_rx_active_in(i downto i),
			rxusrclk_in 						=> rxusrclk(i*4+3 downto i*4),
			rxusrclk2_in 						=> rxusrclk(i*4+3 downto i*4),
			gtpowergood_out 					=> gtpowergood_out(i*4+3 downto i*4),
			rxoutclk_out 						=> rxoutclk(i*4+3 downto i*4),
			gtwiz_userclk_tx_reset_in 			=> gtwiz_userclk_tx_reset_in(i downto i),
			gtwiz_buffbypass_tx_reset_in 		=> (others => '0'), --reset_rx_all(i downto i),
			gtwiz_buffbypass_tx_start_user_in 	=> (others => '0'), --reset_rx_all(i downto i),
			gtwiz_buffbypass_tx_done_out 		=> open,
			gtwiz_buffbypass_tx_error_out 		=> open,
			gtwiz_userclk_tx_srcclk_out 		=> open,
			gtwiz_userclk_tx_usrclk_out 		=> txusrclk(i downto i),
			gtwiz_userclk_tx_usrclk2_out 		=> open,
			gtwiz_userclk_tx_active_out 		=> gtwiz_userclk_tx_active_out(i downto i),
			gtwiz_reset_clk_freerun_in(0) 		=> sysclk_in,
			gtwiz_reset_all_in 					=> gtwiz_reset_all_in(i downto i),
			gtwiz_reset_tx_pll_and_datapath_in 	=> gtwiz_reset_tx_pll_and_datapath_in(i downto i),
			gtwiz_reset_tx_datapath_in 			=> gtwiz_reset_tx_datapath_in(i downto i),
			gtwiz_reset_rx_pll_and_datapath_in 	=> gtwiz_reset_rx_pll_and_datapath_in(i downto i),
			gtwiz_reset_rx_datapath_in 			=> gtwiz_reset_rx_datapath_in(i downto i),
			gtwiz_reset_rx_cdr_stable_out 		=> gtwiz_reset_rx_cdr_stable_out(i downto i),
			gtwiz_reset_tx_done_out 			=> TX_FSM_RESET_DONE_S(i downto i),
			gtwiz_reset_rx_done_out 			=> RX_FSM_RESET_DONE_S(i downto i),
			gtwiz_userdata_tx_in 				=> txdata_in_s((128*(i+1))-1 downto (128 * (i))),
			gtwiz_userdata_rx_out 				=> rxdata_out_s((80*(i+1))-1 downto (80 * (i))),
			gtrefclk01_in(0) 					=> gtrefclk0_i,
			qpll1outclk_out 					=> open,
			qpll1outrefclk_out 					=> open,
			drpclk_in 							=> drpclk_in(i),
			gthrxn_in 							=> gtrxn_in_s((4*(i+1))-1 downto 4*(i)),
			gthrxp_in 							=> gtrxp_in_s((4*(i+1))-1 downto 4*(i)),
			gtrefclk0_in 						=> gtrefclk0_in(i),
			rxslide_in 							=> RXSLIDE_S((4*(i+1))-1 downto 4*(i)),
			tx8b10ben_in 						=> tx8b10ben_in,
			txctrl0_in 							=> txctrl0_in,
			txctrl1_in 							=> txctrl1_in,
			txctrl2_in 							=> txctrl2_in_s((32*(i+1))-1 downto 32*(i)),
			gthtxn_out 							=> gttxn_out_s((4*(i+1))-1 downto 4*(i)),
			gthtxp_out 							=> gttxp_out_s((4*(i+1))-1 downto 4*(i)),
			rxpmaresetdone_out 					=> rxpmaresetdone_out((4*(i+1))-1 downto 4*(i)),
			txpmaresetdone_out 					=> txpmaresetdone_out((4*(i+1))-1 downto 4*(i)),
			rxcdrlock_out 						=> rxcdrlock_out((4*(i+1))-1 downto 4*(i))
		);
	end generate;

--##################### for 12 to 24 channel
	g_quads2: for i in 3 to 5 generate
		
		begin

		gtwiz_reset_rx_done_out(i) <= RX_FSM_RESET_DONE_S(i);
		gtwiz_reset_tx_done_out(i) <= TX_FSM_RESET_DONE_S(i);
		
		drpclk_in(i) 	<=  (others => sysclk_in);
		gtrefclk0_in(i) <= (others => gtrefclk1_i);
		
		gtwiz_reset_rx_datapath_in(i) 			<= gtwiz_reset_rx_datapath_in_s(i) ;
		gtwiz_reset_rx_pll_and_datapath_in(i) 	<= gtwiz_reset_rx_pll_and_datapath_in_s(i) ;
		gtwiz_reset_all_in(i) 					<=  gtwiz_reset_all_in_s(i) or reset_rx_all(i);
		gtwiz_reset_tx_pll_and_datapath_in(i) 	<= gtwiz_reset_tx_pll_and_datapath_in_s(i) ;
		gtwiz_reset_tx_datapath_in(i) 			<= gtwiz_reset_tx_datapath_in_s(i) ;
		gtwiz_userclk_tx_reset_in(i) 			<= gtwiz_userclk_tx_reset_in_s(i) ;
		gtwiz_userclk_rx_active_in(i) 			<= gtwiz_userclk_tx_active_out(i);
		
		
		u0 : fullmodetransceiver_core
		PORT MAP (
			gtwiz_userclk_rx_active_in 			=> gtwiz_userclk_rx_active_in(i downto i),
			rxusrclk_in 						=> rxusrclk(i*4+3 downto i*4),
			rxusrclk2_in 						=> rxusrclk(i*4+3 downto i*4),
			gtpowergood_out 					=> gtpowergood_out(i*4+3 downto i*4),
			rxoutclk_out 						=> rxoutclk(i*4+3 downto i*4),
			gtwiz_userclk_tx_reset_in 			=> gtwiz_userclk_tx_reset_in(i downto i),
			gtwiz_buffbypass_tx_reset_in 		=> (others => '0'), --reset_rx_all(i downto i),
			gtwiz_buffbypass_tx_start_user_in 	=> (others => '0'), --reset_rx_all(i downto i),
			gtwiz_buffbypass_tx_done_out 		=> open,
			gtwiz_buffbypass_tx_error_out 		=> open,
			gtwiz_userclk_tx_srcclk_out 		=> open,
			gtwiz_userclk_tx_usrclk_out 		=> txusrclk(i downto i),
			gtwiz_userclk_tx_usrclk2_out 		=> open,
			gtwiz_userclk_tx_active_out 		=> gtwiz_userclk_tx_active_out(i downto i),
			gtwiz_reset_clk_freerun_in(0) 		=> sysclk_in,
			gtwiz_reset_all_in 					=> gtwiz_reset_all_in(i downto i),
			gtwiz_reset_tx_pll_and_datapath_in 	=> gtwiz_reset_tx_pll_and_datapath_in(i downto i),
			gtwiz_reset_tx_datapath_in 			=> gtwiz_reset_tx_datapath_in(i downto i),
			gtwiz_reset_rx_pll_and_datapath_in 	=> gtwiz_reset_rx_pll_and_datapath_in(i downto i),
			gtwiz_reset_rx_datapath_in 			=> gtwiz_reset_rx_datapath_in(i downto i),
			gtwiz_reset_rx_cdr_stable_out 		=> gtwiz_reset_rx_cdr_stable_out(i downto i),
			gtwiz_reset_tx_done_out 			=> TX_FSM_RESET_DONE_S(i downto i),
			gtwiz_reset_rx_done_out 			=> RX_FSM_RESET_DONE_S(i downto i),
			gtwiz_userdata_tx_in 				=> txdata_in_s((128*(i+1))-1 downto (128 * (i))),
			gtwiz_userdata_rx_out 				=> rxdata_out_s((80*(i+1))-1 downto (80 * (i))),
			gtrefclk01_in(0) 					=> gtrefclk1_i,
			qpll1outclk_out 					=> open,
			qpll1outrefclk_out 					=> open,
			drpclk_in 							=> drpclk_in(i),
			gthrxn_in 							=> gtrxn_in_s((4*(i+1))-1 downto 4*(i)),
			gthrxp_in 							=> gtrxp_in_s((4*(i+1))-1 downto 4*(i)),
			gtrefclk0_in 						=> gtrefclk0_in(i),
			rxslide_in 							=> RXSLIDE_S((4*(i+1))-1 downto 4*(i)),
			tx8b10ben_in 						=> tx8b10ben_in,
			txctrl0_in 							=> txctrl0_in,
			txctrl1_in 							=> txctrl1_in,
			txctrl2_in 							=> txctrl2_in_s((32*(i+1))-1 downto 32*(i)),
			gthtxn_out 							=> gttxn_out_s((4*(i+1))-1 downto 4*(i)),
			gthtxp_out 							=> gttxp_out_s((4*(i+1))-1 downto 4*(i)),
			rxpmaresetdone_out 					=> rxpmaresetdone_out((4*(i+1))-1 downto 4*(i)),
			txpmaresetdone_out 					=> txpmaresetdone_out((4*(i+1))-1 downto 4*(i)),
			rxcdrlock_out 						=> rxcdrlock_out((4*(i+1))-1 downto 4*(i))
		);
	end generate;

end generate GEN_FOR24CH;
  
end RTL;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               Mesfin Gebyehu
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company: Nikhef
-- Engineer: Frans Schreuder
--
-- Create Date: 01/05/2017 04:24:14 PM
-- Design Name: InputShifterNb
-- Module Name: InputShifterNb - Behavioral
-- Project Name: FELIX
-- Target Devices: Virtex7, Kintex Ultrascale
-- Tool Versions:
-- Description: Universal input shift register, takes 2, 4, or 8 bit Elink data
--              and shifts it into 10b data (target direct data or 8b10b decoder)
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.std_logic_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use work.FELIX_package.all;

entity InputShifterNb is
    Port ( DIN : in std_logic_vector(1 downto 0);
        DOUT : out std_logic_vector (9 downto 0);
        DValid : out std_logic;
        Clk : in std_logic;
        Reset: in std_logic
    );
end InputShifterNb;

architecture Behavioral of InputShifterNb is

    --signal S_Cnt: integer range 0 to 7;
    --signal S_ShiftReg: std_logic_vector(9 downto 0);
    signal LOL_cnt : integer range 0 to 65535;
    signal LOCK : std_logic;
begin

    shift: process(Clk, Reset)
        Variable V_Cnt: integer range 0 to 7;
        variable ShiftReg: std_logic_vector(9 downto 0);
    begin
        if(Reset = '1') then
            V_Cnt := 0;
            ShiftReg := "0000000000";
            DValid <= '0';
            DOUT <= "0000000000";
            LOL_cnt <= 65535;
            LOCK <= '0';
        elsif rising_edge(Clk) then
            DValid <= '0';
            ShiftReg := ShiftReg(7 downto 0) & DIN(1 downto 0);
            if(V_Cnt < 4) then
                V_Cnt := V_Cnt + 1;
            else
                V_Cnt := 0;
            end if;
            if ShiftReg = COMMAp or ShiftReg = COMMAn then
                V_Cnt := 4;
                LOCK <= '1';
            else
                if V_Cnt = 4 then
                    if LOL_cnt < 65535 then
                        LOL_cnt <= LOL_cnt + 1;
                    else
                        LOCK <= '0';
                    end if;
                end if;
            end if;

            if(V_Cnt = 4) then
                if LOCK = '1' then
                    DOUT <= ShiftReg(9 downto 0);
                else
                    DOUT <= (others => '0');
                end if;
                DValid <= LOCK;
            end if;
        --S_Cnt <= V_Cnt;
        --S_ShiftReg <= ShiftReg;




        end if;
    end process;

end Behavioral;

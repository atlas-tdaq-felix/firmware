--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Mesfin Gebyehu
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company: Nikhef
-- Engineer: Frans Schreuder
--
-- Create Date: 01/05/2017 04:24:14 PM
-- Design Name: InputShifterNb
-- Module Name: InputShifterNb - Behavioral
-- Project Name: FELIX
-- Target Devices: Virtex7, Kintex Ultrascale
-- Tool Versions:
-- Description: Universal input shift register, takes 2, 4, or 8 bit Elink data
--              and shifts it into 10b data (target direct data or 8b10b decoder)
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.std_logic_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

entity Xoff_decoder_top is
    generic (
        DinWidth : integer range 0 to 16 := 16 --maximum width of the elink
    );
    Port (
        DIN : in std_logic_vector(DinWidth-1 downto 0);
        Clk : in std_logic;
        Reset: in std_logic;
        --InputWidth: in integer range 0 to 16;
        --MODE8b : in std_logic; --for direct mode set to '1', for 8b10b mode set to '0'
        DATA_OUT : out std_logic_vector (9 downto 0);
        DATA_RDY : out std_logic;
        Xoff_nXon  : out std_logic
    );
end Xoff_decoder_top;

architecture Behavioral of Xoff_decoder_top is

    signal DOUT : std_logic_vector (9 downto 0) := (others => '0');
    signal DValid : std_logic := '0';
    signal Xoff_nXon_t : std_logic := '0';
    signal DIN_s : std_logic_vector(1 downto 0);
    signal count : unsigned(7 downto 0);
    constant count_max : unsigned(7 downto 0):= (others => '1');

begin

    DIN_s <= DIN(1 downto 0);

    InputShifterNb_inst : entity work.InputShifterNb
        port map (
            DIN    => DIN_s,
            DOUT   => DOUT,
            DValid => DValid,
            Clk    => Clk,
            Reset  => Reset
        );

    process(Clk)
    begin
        if rising_edge(Clk) then
            DATA_RDY <= DValid;
        end if;
    end process;
    process(Clk)
    begin
        if rising_edge(Clk) then
            if(Reset = '1') then
                Xoff_nXon <= '0';
                count <= (others => '0');
            else
                if(count /= count_max) then
                    count <= count + 1;
                    Xoff_nXon <= '0';
                else
                    Xoff_nXon <= Xoff_nXon_t;
                end if;
            end if;
        end if;
    end process;

    dec_8b10: entity work.dec_8b10_wrap_fmemu
        port map(
            RESET         => Reset,
            RBYTECLK      => Clk,
            ABCDEIFGHJ_IN => DOUT,
            HGFEDCBA      => DATA_OUT(7 downto 0),
            ISK           => DATA_OUT(9 downto 8),
            BUSY          => Xoff_nXon_t
        );

end Behavioral;

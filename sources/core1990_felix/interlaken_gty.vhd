library ieee, xpm;
    use xpm.vcomponents.all;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.interlaken_package.all;
    use work.axi_stream_package.all;
    use work.FELIX_package.all;

library unisim;
    use unisim.vcomponents.all;

entity interlaken_gty is
    generic(
        Lanes             : positive := 4; -- Number of Lanes (Transmission channels)
        BondNumberOfLanes : positive := 1; -- @suppress "Unused generic: BondNumberOfLanes is not used in work.interlaken_gty(Behavioral)"
        CARD_TYPE         : integer  := 128;
        GTREFCLKS         : integer  := 1;
        GTREFCLK1S        : integer  := 1
    );
    Port(
        reset                : in  std_logic;
        rst_tx_pll_and_datapath_in : in std_logic_vector(Lanes/4 -1 downto 0);
        rst_tx_datapath_in   : in std_logic_vector(Lanes/4 -1 downto 0);
        rst_rx_pll_and_datapath_in : in std_logic_vector(Lanes/4 -1 downto 0);
        rst_rx_datapath_in   : in std_logic_vector(Lanes/4 -1 downto 0);
        rst_txusr_403M_s     : out std_logic;
        rst_rxusr_403M_s     : out std_logic;
        --------- 125 MHz input, to transceiver (QSFP4 clock)------------
        GTREFCLK_IN_P        : in  std_logic_vector(GTREFCLKS - 1 downto 0);
        GTREFCLK_IN_N        : in  std_logic_vector(GTREFCLKS - 1 downto 0);
        --------- 125 MHz input, to transceiver (QSFP4 clock)------------
        GTREFCLK1_IN_P       : in  std_logic_vector(GTREFCLK1S - 1 downto 0);
        GTREFCLK1_IN_N       : in  std_logic_vector(GTREFCLK1S - 1 downto 0);
        -------- 100 MHz input, Free Running CLK (QDR4 clock) -----------
        clk100               : in  std_logic;
        ------------------- GT data in/out ------------------------------
        TX_Out_P             : out std_logic_vector(Lanes - 1 downto 0);
        TX_Out_N             : out std_logic_vector(Lanes - 1 downto 0);
        RX_In_P              : in  std_logic_vector(Lanes - 1 downto 0);
        RX_In_N              : in  std_logic_vector(Lanes - 1 downto 0);
        TX_User_Clock_s      : out std_logic_vector(Lanes - 1 downto 0);
        RX_User_Clock_s      : out std_logic_vector(Lanes - 1 downto 0);
        loopback_in          : in  std_logic_vector(2 downto 0);
        Data_Transceiver_In  : in  array_32b(0 to Lanes - 1);
        Interlaken_Data_Transceiver_Out : out slv_67_array(0 to Lanes - 1);
        RX_Datavalid_Out     : out std_logic_vector(Lanes - 1 downto 0);
        RX_Gearboxslip_In    : in  std_logic_vector(Lanes - 1 downto 0); -- @suppress "Unused port: RX_Gearboxslip_In is not used in work.interlaken_gty(Behavioral)"
        TX_CharIsK_in        : in  array_4b(0 to Lanes - 1);
        rxresetdone_out      : out std_logic_vector(Lanes/4-1 downto 0);
        txresetdone_out      : out std_logic_vector(Lanes/4-1 downto 0);
        lcplllock_out        : out std_logic_vector(Lanes/4-1 downto 0);
        rplllock_out         : out std_logic_vector(Lanes/4-1 downto 0);
        quad0_gtrefclk1_out  : out std_logic
    );
end interlaken_gty;

architecture Behavioral of interlaken_gty is
    --signal TX_User_Clock, RX_User_Clock : std_logic;
    signal TX_User_Clock, RX_User_Clock : std_logic_vector(Lanes - 1 downto 0);


    signal RX_Header_s          : array_3b(0 to Lanes - 1);
    signal RX_Headervalid_s     : std_logic_vector(Lanes - 1 downto 0);
    signal RX_Gearboxslip_s     : std_logic_vector(Lanes - 1 downto 0);
    signal not_RX_Resetdone_Out : std_logic_vector(Lanes - 1 downto 0); --Todo use as status bit -- @suppress "signal RX_Resetdone_Out is never read"

    signal not_TX_Resetdone_Out : std_logic_vector(Lanes - 1 downto 0); --Todo use as status bit -- @suppress "signal TX_Resetdone_Out is never read"



    signal GTREFCLK  : std_logic_vector((Lanes / 4) - 1 downto 0);
    signal GTREFCLK1 : std_logic_vector((Lanes / 4) - 1 downto 0);

    ---- GTY added signals -------
    signal txoutclk_out, rxoutclk_out                             : std_logic_vector(Lanes - 1 downto 0);
    --     signal tx_active_sync, tx_active_meta : std_logic;
    --     signal rx_active_sync, rx_active_meta : std_logic;
    signal gtwiz_userclk_rx_active_in, gtwiz_userclk_tx_active_in : std_logic_vector((Lanes / 4) - 1 downto 0);
    signal rx_gearbox_reset                     : std_logic_vector((Lanes / 4) - 1 downto 0);
    signal rst_txusr_403M, rst_rxusr_403M                         : std_logic;
    signal gtwiz_reset_rx_done_out                                : std_logic_vector((Lanes / 4) - 1 downto 0);
    signal gtwiz_reset_tx_done_out                                : std_logic_vector((Lanes / 4) - 1 downto 0);

    signal GTREFCLK_VERSAL_BUF0, GTREFCLK_VERSAL_SEL0 : std_logic_vector(Lanes / 4 - 1 downto 0);
    signal GTREFCLK_VERSAL_BUF1, GTREFCLK_VERSAL_SEL1 : std_logic_vector(Lanes / 4 - 1 downto 0);
    --signal txsequence_in : std_logic_vector(6 downto 0);
    signal lcplllock, rplllock: std_logic_vector(Lanes/4 -1 downto 0);


begin
    rxresetdone_out <= gtwiz_reset_rx_done_out;
    txresetdone_out <= gtwiz_reset_tx_done_out;
    lcplllock_out <= lcplllock;
    rplllock_out <= rplllock;
    --    gtwiz_userclk_tx_active_in <= (others => tx_active_sync);
    --    gtwiz_userclk_rx_active_in <= (others => rx_active_sync);

    TX_User_Clock_s      <= TX_User_Clock;
    RX_User_Clock_s      <= RX_User_Clock;
    rst_txusr_403M_s     <= rst_txusr_403M;
    rst_rxusr_403M_s     <= rst_rxusr_403M;

    quad0_gtrefclk1_out <= GTREFCLK_VERSAL_SEL1(0);

    INST_RST_TXUSR : xpm_cdc_async_rst
        generic map(
            DEST_SYNC_FF    => 2,       -- DECIMAL; range: 2-10
            INIT_SYNC_FF    => 1,       -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            RST_ACTIVE_HIGH => 1        -- DECIMAL; 0=active low reset, 1=active high reset
        )
        port map(
            src_arst  => reset,         -- 1-bit input: Source asynchronous reset signal.
            dest_clk  => TX_User_Clock(0), -- 1-bit input: Destination clock.
            dest_arst => rst_txusr_403M -- 1-bit output: src_arst asynchronous reset signal synchronized to destination clock domain.
        );
    INST_RST_RXUSR : xpm_cdc_async_rst
        generic map(
            DEST_SYNC_FF    => 2,       -- DECIMAL; range: 2-10
            INIT_SYNC_FF    => 1,       -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            RST_ACTIVE_HIGH => 1        -- DECIMAL; 0=active low reset, 1=active high reset
        )
        port map(
            src_arst  => reset,         -- 1-bit input: Source asynchronous reset signal.
            dest_clk  => RX_User_Clock(0), -- 1-bit input: Destination clock.
            dest_arst => rst_rxusr_403M -- 1-bit output: src_arst asynchronous reset signal synchronized to destination clock domain.
        );

    g_quads : for quad in 0 to Lanes / 4 - 1 generate
        signal gtwiz_userdata_tx_in   : std_logic_vector(127 downto 0);
        signal gtwiz_userdata_rx_out  : std_logic_vector(255 downto 0);
        signal gtyrxn_in, gtyrxp_in   : std_logic_vector(3 downto 0);
        signal gtytxn_out, gtytxp_out : std_logic_vector(3 downto 0);
        signal rxgearboxslip_in       : std_logic_vector(3 downto 0);

        signal ch0_txdata_ext_0, ch1_txdata_ext_0, ch2_txdata_ext_0, ch3_txdata_ext_0 : std_logic_vector(127 downto 0);

        signal txpmaresetdone_out, rxpmaresetdone_out : std_logic_vector(3 downto 0);
        signal gtwiz_reset_all_in                     : std_logic_vector(0 downto 0);
        signal rxdatavalid_out, rxheadervalid_out     : std_logic_vector(7 downto 0);
        signal rxusrclk_in, rxusrclk2_in              : std_logic_vector(3 downto 0);
        signal txusrclk_in, txusrclk2_in              : std_logic_vector(3 downto 0);
        signal gtwiz_reset_clk_freerun_in             : std_logic_vector(0 downto 0);
        --signal gt_rxsequence_o : std_logic_vector(7 downto 0);
        signal rxheader_out              : std_logic_vector(23 downto 0);
        signal loopback                               : std_logic_vector(11 downto 0);

        signal tx_active_sync, tx_active_meta                                         : std_logic;
        signal rx_active_sync, rx_active_meta                                         : std_logic;
        signal rx_gearbox_reset_i                                                     : std_logic;
        signal not_TX_Resetdone_Out_tx_User_clock, not_RX_Resetdone_Out_rx_User_clock : std_logic;


    begin

        GTREFCLK_VERSAL_SEL0(quad) <= GTREFCLK_VERSAL_BUF0(quad);
        GTREFCLK_VERSAL_SEL1(quad) <= GTREFCLK_VERSAL_BUF1(quad);

        gtwiz_userclk_tx_active_in(quad downto quad) <= (others => tx_active_sync);
        gtwiz_userclk_rx_active_in(quad downto quad) <= (others => rx_active_sync);

        gtwiz_userdata_tx_in(31 downto 0)   <= Data_Transceiver_In(quad * 4 + 0);
        gtwiz_userdata_tx_in(63 downto 32)  <= Data_Transceiver_In(quad * 4 + 1);
        gtwiz_userdata_tx_in(95 downto 64)  <= Data_Transceiver_In(quad * 4 + 2);
        gtwiz_userdata_tx_in(127 downto 96) <= Data_Transceiver_In(quad * 4 + 3);

        Interlaken_Data_Transceiver_Out(quad * 4 + 0)(63 downto 0) <= gtwiz_userdata_rx_out(63 downto 0);
        Interlaken_Data_Transceiver_Out(quad * 4 + 1)(63 downto 0) <= gtwiz_userdata_rx_out(127 downto 64);
        Interlaken_Data_Transceiver_Out(quad * 4 + 2)(63 downto 0) <= gtwiz_userdata_rx_out(191 downto 128);
        Interlaken_Data_Transceiver_Out(quad * 4 + 3)(63 downto 0) <= gtwiz_userdata_rx_out(255 downto 192);

        Interlaken_Data_Transceiver_Out(quad * 4 + 0)(66 downto 64) <= RX_Header_s(quad *4 + 0);
        Interlaken_Data_Transceiver_Out(quad * 4 + 1)(66 downto 64) <= RX_Header_s(quad *4 + 1);
        Interlaken_Data_Transceiver_Out(quad * 4 + 2)(66 downto 64) <= RX_Header_s(quad *4 + 2);
        Interlaken_Data_Transceiver_Out(quad * 4 + 3)(66 downto 64) <= RX_Header_s(quad *4 + 3);

        RX_Header_s(quad *4 + 0) <= rxheader_out(2 downto 0);
        RX_Header_s(quad *4 + 1) <= rxheader_out(8 downto 6);
        RX_Header_s(quad *4 + 2) <= rxheader_out(14 downto 12);
        RX_Header_s(quad *4 + 3) <= rxheader_out(20 downto 18);


        gtyrxn_in                              <= RX_In_N(quad * 4 + 3 downto quad * 4);
        gtyrxp_in                              <= RX_In_P(quad * 4 + 3 downto quad * 4);
        TX_Out_N(quad * 4 + 3 downto quad * 4) <= gtytxn_out;
        TX_Out_P(quad * 4 + 3 downto quad * 4) <= gtytxp_out;

        rxgearboxslip_in <= RX_Gearboxslip_s(quad * 4 + 3 downto quad * 4);
        txusrclk_in      <= TX_User_Clock(quad*4+3 downto quad*4);
        txusrclk2_in     <= txusrclk_in; --Datapath 64b and intw 2; ug578 p105
        rxusrclk_in      <= RX_User_Clock(quad*4+3 downto quad*4);
        rxusrclk2_in     <= rxusrclk_in; --Datapath 64b and intw 2; ug578 p105



        not_TX_Resetdone_Out(quad * 4 + 3 downto quad * 4) <= not txpmaresetdone_out;
        not_RX_Resetdone_Out(quad * 4 + 3 downto quad * 4) <= not rxpmaresetdone_out;

        gtwiz_reset_all_in             <= (others => reset);
        RX_Datavalid_Out(quad * 4 + 0) <= rxdatavalid_out(0);
        RX_Datavalid_Out(quad * 4 + 1) <= rxdatavalid_out(2);
        RX_Datavalid_Out(quad * 4 + 2) <= rxdatavalid_out(4);
        RX_Datavalid_Out(quad * 4 + 3) <= rxdatavalid_out(6);

        RX_Headervalid_s(quad * 4 + 0) <= rxheadervalid_out(0);
        RX_Headervalid_s(quad * 4 + 1) <= rxheadervalid_out(2);
        RX_Headervalid_s(quad * 4 + 2) <= rxheadervalid_out(4);
        RX_Headervalid_s(quad * 4 + 3) <= rxheadervalid_out(6);

        gtwiz_reset_clk_freerun_in(0) <= clk100;

        --txsequence_in               <= (others => '0');
        --When using a 32-bit (4-byte) TXDATA interface to interconnect logic, toggle TXSEQUENCE[0] every TXUSRCLK cycle.

        loopback <= loopback_in & loopback_in & loopback_in & loopback_in;

        g_ultrascale : if CARD_TYPE = 128 generate
            -------------------------- Include Transceiver -----------------------------
            COMPONENT gtwizard_ultrascale_0 -- @suppress "Component declaration 'gtwizard_ultrascale_0' has none or multiple matching entity declarations"
                PORT(
                    gtwiz_userclk_tx_active_in         : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
                    gtwiz_userclk_rx_active_in         : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
                    gtwiz_reset_clk_freerun_in         : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
                    gtwiz_reset_all_in                 : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
                    gtwiz_reset_tx_pll_and_datapath_in : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
                    gtwiz_reset_tx_datapath_in         : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
                    gtwiz_reset_rx_pll_and_datapath_in : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
                    gtwiz_reset_rx_datapath_in         : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
                    gtwiz_reset_rx_cdr_stable_out      : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
                    gtwiz_reset_tx_done_out            : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
                    gtwiz_reset_rx_done_out            : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
                    gtwiz_userdata_tx_in               : IN  STD_LOGIC_VECTOR(127 DOWNTO 0);
                    gtwiz_userdata_rx_out              : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
                    gtrefclk00_in                      : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
                    gtrefclk01_in                      : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
                    qpll0outclk_out                    : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
                    qpll0outrefclk_out                 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
                    qpll1outclk_out                    : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
                    qpll1outrefclk_out                 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
                    gtyrxn_in                          : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
                    gtyrxp_in                          : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
                    loopback_in                        : IN  STD_LOGIC_VECTOR(11 DOWNTO 0);
                    rxgearboxslip_in                   : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
                    rxusrclk_in                        : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
                    rxusrclk2_in                       : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
                    tx8b10ben_in                       : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
                    txctrl0_in                         : IN  STD_LOGIC_VECTOR(63 DOWNTO 0);
                    txctrl1_in                         : IN  STD_LOGIC_VECTOR(63 DOWNTO 0);
                    txctrl2_in                         : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
                    txusrclk_in                        : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
                    txusrclk2_in                       : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
                    gtpowergood_out                    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                    gtytxn_out                         : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                    gtytxp_out                         : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                    rxdatavalid_out                    : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
                    rxheader_out                       : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
                    rxheadervalid_out                  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
                    rxoutclk_out                       : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                    rxpmaresetdone_out                 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                    rxstartofseq_out                   : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
                    txoutclk_out                       : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                    txpmaresetdone_out                 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
                );
            END COMPONENT;

            -- 8B/10B encoding
            signal tx8b10ben_in : std_logic_vector(3 DOWNTO 0);
            signal txctrl0_in   : std_logic_vector(63 DOWNTO 0);
            signal txctrl1_in   : std_logic_vector(63 DOWNTO 0);
            signal txctrl2_in   : std_logic_vector(31 DOWNTO 0);

        begin

            tx8b10ben_in(0) <= '1';
            txctrl0_in      <= (others => '0'); --Set Low to use normal running disparity.
            txctrl1_in      <= (others => '0'); --Set Low to use normal running disparity.
            txctrl2_in      <= x"0" & TX_CharIsK_in(4*quad+3) &
                               x"0" & TX_CharIsK_in(4*quad+2) &
                               x"0" & TX_CharIsK_in(4*quad+1) &
                               x"0" & TX_CharIsK_in(4*quad+0);
            --When High, indicates the corresponding data byte on TXDATA is a valid K character.

            gtwizard_ultrascale_0_i : gtwizard_ultrascale_0
                PORT MAP(
                    gtwiz_userclk_tx_active_in         => gtwiz_userclk_tx_active_in(quad downto quad),
                    gtwiz_userclk_rx_active_in         => gtwiz_userclk_rx_active_in(quad downto quad),
                    gtwiz_reset_clk_freerun_in         => gtwiz_reset_clk_freerun_in,
                    gtwiz_reset_all_in                 => gtwiz_reset_all_in,
                    gtwiz_reset_tx_pll_and_datapath_in => rst_tx_pll_and_datapath_in(quad downto quad),
                    gtwiz_reset_tx_datapath_in         => rst_tx_datapath_in(quad downto quad),
                    gtwiz_reset_rx_pll_and_datapath_in => rst_rx_pll_and_datapath_in(quad downto quad),
                    gtwiz_reset_rx_datapath_in         => rst_rx_datapath_in(quad downto quad),
                    gtwiz_reset_rx_cdr_stable_out      => open,
                    gtwiz_reset_tx_done_out            => gtwiz_reset_tx_done_out(quad downto quad),
                    gtwiz_reset_rx_done_out            => gtwiz_reset_rx_done_out(quad downto quad),
                    gtwiz_userdata_tx_in               => gtwiz_userdata_tx_in, --Data_Transceiver_In(0),
                    gtwiz_userdata_rx_out              => gtwiz_userdata_rx_out, --Data_Transceiver_Out(0),
                    gtrefclk00_in                      => GTREFCLK(quad downto quad),
                    gtrefclk01_in                      => GTREFCLK1(quad downto quad),
                    qpll0outclk_out                    => open,
                    qpll0outrefclk_out                 => open,
                    qpll1outclk_out                    => open,
                    qpll1outrefclk_out                 => open,
                    gtyrxn_in                          => gtyrxn_in,
                    gtyrxp_in                          => gtyrxp_in,
                    loopback_in                        => loopback,
                    rxgearboxslip_in                   => rxgearboxslip_in,
                    rxusrclk_in                        => rxusrclk_in,
                    rxusrclk2_in                       => rxusrclk2_in,
                    tx8b10ben_in                       => tx8b10ben_in,
                    txctrl0_in                         => txctrl0_in,
                    txctrl1_in                         => txctrl1_in,
                    txctrl2_in                         => txctrl2_in,
                    txusrclk_in                        => txusrclk_in,
                    txusrclk2_in                       => txusrclk2_in,
                    gtpowergood_out                    => open,
                    gtytxn_out                         => gtytxn_out,
                    gtytxp_out                         => gtytxp_out,
                    rxdatavalid_out                    => rxdatavalid_out,
                    rxheader_out                       => rxheader_out,
                    rxheadervalid_out                  => rxheadervalid_out,
                    rxoutclk_out                       => rxoutclk_out(quad * 4 + 3 downto quad * 4),
                    rxpmaresetdone_out                 => rxpmaresetdone_out,
                    rxstartofseq_out                   => open, --gt_rxsequence_o,
                    txoutclk_out                       => txoutclk_out(quad * 4 + 3 downto quad * 4),
                    txpmaresetdone_out                 => txpmaresetdone_out
                );
            --end generate;
            ------------------------------- Buffering tx/rx out clock signals --------------------------------
            g_clockbuffers: for i in 0 to 4 generate
                BUFG_GT_TXclk : BUFG_GT     -- @suppress "Generic map uses default values. Missing optional actuals: SIM_DEVICE, STARTUP_SYNC"
                    port map(
                        O       => TX_User_Clock(quad*4+i),
                        CE      => '1',
                        CEMASK  => '0',
                        CLR     => not_TX_Resetdone_Out(quad * 4+i), --NOT_TX_Resetdone_Out
                        CLRMASK => '0',
                        DIV     => "000",
                        I       => txoutclk_out(quad * 4+i)
                    );

                BUFG_GT_RXclk : BUFG_GT     -- @suppress "Generic map uses default values. Missing optional actuals: SIM_DEVICE, STARTUP_SYNC"
                    port map(
                        O       => RX_User_Clock(quad*4+i),
                        CE      => '1',
                        CEMASK  => '0',
                        CLR     => not_RX_Resetdone_Out(quad * 4+i),
                        CLRMASK => '0',
                        DIV     => "000",
                        I       => rxoutclk_out(quad * 4+i)
                    );
            end generate;

            -------------------------------- Buffering QSFP GT clock -------------------------------------
            IBUFDS_GTE4_GT0 : IBUFDS_GTE4
                generic map(
                    REFCLK_EN_TX_PATH  => '0',
                    REFCLK_HROW_CK_SEL => "00",
                    REFCLK_ICNTL_RX    => "00"
                )
                port map(
                    O     => GTREFCLK(quad),
                    ODIV2 => open,
                    CEB   => '0',
                    I     => GTREFCLK_IN_P(quad),
                    IB    => GTREFCLK_IN_N(quad)
                );

            IBUFDS_GTE4_GT1 : IBUFDS_GTE4
                generic map(
                    REFCLK_EN_TX_PATH  => '0',
                    REFCLK_HROW_CK_SEL => "00",
                    REFCLK_ICNTL_RX    => "00"
                )
                port map(
                    O     => GTREFCLK1(quad),
                    ODIV2 => open,
                    CEB   => '0',
                    I     => GTREFCLK1_IN_P(quad),
                    IB    => GTREFCLK1_IN_N(quad)
                );
        end generate g_ultrascale;

        g_versalprime : if CARD_TYPE = 180 or CARD_TYPE = 181 or CARD_TYPE = 182 or CARD_TYPE = 155 generate


            signal ch0_rxdata_ext_0  : std_logic_vector(127 downto 0);
            signal ch1_rxdata_ext_0  : std_logic_vector(127 downto 0);
            signal ch2_rxdata_ext_0  : std_logic_vector(127 downto 0);
            signal ch3_rxdata_ext_0  : std_logic_vector(127 downto 0);

            signal ch0_txctrl0_ext_0 : STD_LOGIC_VECTOR(15 downto 0);
            signal ch0_txctrl1_ext_0 : STD_LOGIC_VECTOR(15 downto 0);
            signal ch1_txctrl0_ext_0 : STD_LOGIC_VECTOR(15 downto 0);
            signal ch1_txctrl1_ext_0 : STD_LOGIC_VECTOR(15 downto 0);
            signal ch2_txctrl0_ext_0 : STD_LOGIC_VECTOR(15 downto 0);
            signal ch2_txctrl1_ext_0 : STD_LOGIC_VECTOR(15 downto 0);
            signal ch3_txctrl0_ext_0 : STD_LOGIC_VECTOR(15 downto 0);
            signal ch3_txctrl1_ext_0 : STD_LOGIC_VECTOR(15 downto 0);

            signal ch0_txctrl2_ext_0 : std_logic_vector(7 downto 0);
            signal ch1_txctrl2_ext_0 : std_logic_vector(7 downto 0);
            signal ch2_txctrl2_ext_0 : std_logic_vector(7 downto 0);
            signal ch3_txctrl2_ext_0 : std_logic_vector(7 downto 0);

            signal rxgearbox_data_in : std_logic_vector(255 downto 0);
            signal rxoutclk, txoutclk, rxusrclk, txusrclk: std_logic_vector(3 downto 0);
            signal txresetdone, rxresetdone: std_logic;
            signal lcplllock0, lcplllock1, rplllock0, rplllock1: std_logic;

            COMPONENT transceiver_versal_interlaken -- @suppress "Component declaration 'transceiver_versal_interlaken' has none or multiple matching entity declarations"
                PORT (
                    gtpowergood : OUT STD_LOGIC;
                    gtwiz_freerun_clk : IN STD_LOGIC;
                    QUAD0_GTREFCLK0 : IN STD_LOGIC;
                    QUAD0_GTREFCLK1 : IN STD_LOGIC;
                    QUAD0_TX0_outclk : OUT STD_LOGIC;
                    QUAD0_TX1_outclk : OUT STD_LOGIC;
                    QUAD0_TX2_outclk : OUT STD_LOGIC;
                    QUAD0_TX3_outclk : OUT STD_LOGIC;
                    QUAD0_RX0_outclk : OUT STD_LOGIC;
                    QUAD0_RX1_outclk : OUT STD_LOGIC;
                    QUAD0_RX2_outclk : OUT STD_LOGIC;
                    QUAD0_RX3_outclk : OUT STD_LOGIC;
                    QUAD0_rxp : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                    QUAD0_rxn : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
                    QUAD0_txp : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                    QUAD0_txn : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
                    QUAD0_hsclk0_lcplllock : OUT STD_LOGIC;
                    QUAD0_hsclk1_lcplllock : OUT STD_LOGIC;
                    QUAD0_hsclk0_rplllock : OUT STD_LOGIC;
                    QUAD0_hsclk1_rplllock : OUT STD_LOGIC;
                    QUAD0_TX0_usrclk : IN STD_LOGIC;
                    QUAD0_TX1_usrclk : IN STD_LOGIC;
                    QUAD0_TX2_usrclk : IN STD_LOGIC;
                    QUAD0_TX3_usrclk : IN STD_LOGIC;
                    QUAD0_RX0_usrclk : IN STD_LOGIC;
                    QUAD0_RX1_usrclk : IN STD_LOGIC;
                    QUAD0_RX2_usrclk : IN STD_LOGIC;
                    QUAD0_RX3_usrclk : IN STD_LOGIC;
                    INTF0_TX0_ch_txdata : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
                    INTF0_TX0_ch_txrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
                    INTF0_TX0_ch_txctrl0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
                    INTF0_TX0_ch_txctrl1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
                    INTF0_TX0_ch_txctrl2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
                    INTF0_TX1_ch_txdata : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
                    INTF0_TX1_ch_txrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
                    INTF0_TX1_ch_txctrl0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
                    INTF0_TX1_ch_txctrl1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
                    INTF0_TX1_ch_txctrl2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
                    INTF0_TX2_ch_txdata : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
                    INTF0_TX2_ch_txrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
                    INTF0_TX2_ch_txctrl0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
                    INTF0_TX2_ch_txctrl1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
                    INTF0_TX2_ch_txctrl2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
                    INTF0_TX3_ch_txdata : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
                    INTF0_TX3_ch_txrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
                    INTF0_TX3_ch_txctrl0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
                    INTF0_TX3_ch_txctrl1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
                    INTF0_TX3_ch_txctrl2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
                    INTF0_RX0_ch_rxrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
                    INTF0_RX0_ch_rxdata : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
                    INTF0_RX1_ch_rxrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
                    INTF0_RX1_ch_rxdata : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
                    INTF0_RX2_ch_rxrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
                    INTF0_RX2_ch_rxdata : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
                    INTF0_RX3_ch_rxrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
                    INTF0_RX3_ch_rxdata : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
                    INTF0_TX_clr_out : OUT STD_LOGIC;
                    INTF0_TX_clrb_leaf_out : OUT STD_LOGIC;
                    INTF0_RX_clr_out : OUT STD_LOGIC;
                    INTF0_RX_clrb_leaf_out : OUT STD_LOGIC;
                    INTF0_rst_all_in : IN STD_LOGIC;
                    INTF0_rst_tx_pll_and_datapath_in : IN STD_LOGIC;
                    INTF0_rst_tx_datapath_in : IN STD_LOGIC;
                    INTF0_rst_tx_done_out : OUT STD_LOGIC;
                    INTF0_rst_rx_pll_and_datapath_in : IN STD_LOGIC;
                    INTF0_rst_rx_datapath_in : IN STD_LOGIC;
                    INTF0_rst_rx_done_out : OUT STD_LOGIC
                );
            END COMPONENT;

        begin

            ch0_txctrl0_ext_0 <= (others => '0');
            ch0_txctrl1_ext_0 <= (others => '0');
            ch1_txctrl0_ext_0 <= (others => '0');
            ch1_txctrl1_ext_0 <= (others => '0');
            ch2_txctrl0_ext_0 <= (others => '0');
            ch2_txctrl1_ext_0 <= (others => '0');
            ch3_txctrl0_ext_0 <= (others => '0');
            ch3_txctrl1_ext_0 <= (others => '0');

            ch0_txctrl2_ext_0 <= x"0" & TX_CharIsK_in(4 * quad + 0);
            ch1_txctrl2_ext_0 <= x"0" & TX_CharIsK_in(4 * quad + 1);
            ch2_txctrl2_ext_0 <= x"0" & TX_CharIsK_in(4 * quad + 2);
            ch3_txctrl2_ext_0 <= x"0" & TX_CharIsK_in(4 * quad + 3);

            IBUFDS_GTE5_REF0 : IBUFDS_GTE5
                generic map(
                    REFCLK_EN_TX_PATH  => '0',
                    REFCLK_HROW_CK_SEL => 0,
                    REFCLK_ICNTL_RX    => 0
                )
                port map(
                    O     => GTREFCLK_VERSAL_BUF0(quad),
                    ODIV2 => open,
                    CEB   => '0',
                    I     => GTREFCLK_IN_P(quad),
                    IB    => GTREFCLK_IN_N(quad)
                );

            IBUFDS_GTE5_REF1 : IBUFDS_GTE5
                generic map(
                    REFCLK_EN_TX_PATH  => '0',
                    REFCLK_HROW_CK_SEL => 0,
                    REFCLK_ICNTL_RX    => 0
                )
                port map(
                    O     => GTREFCLK_VERSAL_BUF1(quad),
                    ODIV2 => open,
                    CEB   => '0',
                    I     => GTREFCLK1_IN_P(quad),
                    IB    => GTREFCLK1_IN_N(quad)
                );

            ch0_txdata_ext_0 <= x"0000_0000_0000_0000_0000_0000" & gtwiz_userdata_tx_in(31 downto 0);
            ch1_txdata_ext_0 <= x"0000_0000_0000_0000_0000_0000" & gtwiz_userdata_tx_in(63 downto 32);
            ch2_txdata_ext_0 <= x"0000_0000_0000_0000_0000_0000" & gtwiz_userdata_tx_in(95 downto 64);
            ch3_txdata_ext_0 <= x"0000_0000_0000_0000_0000_0000" & gtwiz_userdata_tx_in(127 downto 96);

            g_usrclk: for i in 0 to 3 generate
                bufg_rxusrclk: BUFG_GT generic map(
                        SIM_DEVICE => SIM_DEVICE(CARD_TYPE),
                        STARTUP_SYNC => "FALSE"
                    )
                    port map(
                        O => rxusrclk(i),
                        CE => '1',
                        CEMASK => '0',
                        CLR => '0',
                        CLRMASK => '0',
                        DIV => "000",
                        I => rxoutclk(i)
                    );
                RX_User_Clock(quad*4+i) <= rxusrclk(i);
                bufg_txusrclk: BUFG_GT generic map(
                        SIM_DEVICE => SIM_DEVICE(CARD_TYPE),
                        STARTUP_SYNC => "FALSE"
                    )
                    port map(
                        O => txusrclk(i),
                        CE => '1',
                        CEMASK => '0',
                        CLR => '0',
                        CLRMASK => '0',
                        DIV => "000",
                        I => txoutclk(i)
                    );
                TX_User_Clock(quad*4+i) <= txusrclk(i);
            end generate g_usrclk;

            txpmaresetdone_out(3 downto 0) <= (others => txresetdone);
            rxpmaresetdone_out(3 downto 0) <= (others => rxresetdone);
            lcplllock(quad) <= lcplllock0 and lcplllock1;
            rplllock(quad) <= rplllock0 and rplllock1;
            gtwiz_reset_rx_done_out(quad) <= rxresetdone;
            gtwiz_reset_tx_done_out(quad) <= txresetdone;

            -- Channels are flipped to use the same pinout as the FLX-712
            -- Transceiver channel -> Data
            -- CH0 -> CH3
            -- CH1 -> CH2
            -- CH2 -> CH1
            -- CH3 -> CH0
            transceiver_versal_interlaken_raw_i : transceiver_versal_interlaken
                port map(
                    QUAD0_GTREFCLK0                      => GTREFCLK_VERSAL_SEL0(quad), -- Interlaken 25 Gbps
                    QUAD0_GTREFCLK1                      => GTREFCLK_VERSAL_SEL1(quad), -- TTC LTI 9.6 Gbps
                    QUAD0_rxn                 => gtyrxn_in, --: in STD_LOGIC_VECTOR ( 3 downto 0 );
                    QUAD0_rxp                 => gtyrxp_in, --: in STD_LOGIC_VECTOR ( 3 downto 0 );
                    QUAD0_txn                 => gtytxn_out, --: out STD_LOGIC_VECTOR ( 3 downto 0 );
                    QUAD0_txp                 => gtytxp_out, --: out STD_LOGIC_VECTOR ( 3 downto 0 );
                    gtwiz_freerun_clk          => clk100, --: in STD_LOGIC;

                    gtpowergood => open,
                    QUAD0_TX0_outclk => txoutclk(3),
                    QUAD0_TX1_outclk => txoutclk(2),
                    QUAD0_TX2_outclk => txoutclk(1),
                    QUAD0_TX3_outclk => txoutclk(0),
                    QUAD0_RX0_outclk => rxoutclk(3),
                    QUAD0_RX1_outclk => rxoutclk(2),
                    QUAD0_RX2_outclk => rxoutclk(1),
                    QUAD0_RX3_outclk => rxoutclk(0),
                    INTF0_RX0_ch_rxdata => ch3_rxdata_ext_0,
                    INTF0_RX1_ch_rxdata => ch2_rxdata_ext_0,
                    INTF0_RX2_ch_rxdata => ch1_rxdata_ext_0,
                    INTF0_RX3_ch_rxdata => ch0_rxdata_ext_0,
                    INTF0_TX_clr_out => open,
                    INTF0_TX_clrb_leaf_out => open,
                    INTF0_RX_clr_out => open,
                    INTF0_RX_clrb_leaf_out => open,
                    INTF0_rst_tx_done_out => txresetdone,
                    INTF0_rst_rx_done_out => rxresetdone,
                    QUAD0_TX0_usrclk => txusrclk(3),
                    QUAD0_TX1_usrclk => txusrclk(2),
                    QUAD0_TX2_usrclk => txusrclk(1),
                    QUAD0_TX3_usrclk => txusrclk(0),
                    QUAD0_RX0_usrclk => rxusrclk(3),
                    QUAD0_RX1_usrclk => rxusrclk(2),
                    QUAD0_RX2_usrclk => rxusrclk(1),
                    QUAD0_RX3_usrclk => rxusrclk(0),
                    INTF0_TX0_ch_txdata => ch3_txdata_ext_0,
                    INTF0_TX0_ch_txrate => x"00",
                    INTF0_TX0_ch_txctrl0 => ch3_txctrl0_ext_0,
                    INTF0_TX0_ch_txctrl1 => ch3_txctrl1_ext_0,
                    INTF0_TX0_ch_txctrl2 => ch3_txctrl2_ext_0,
                    INTF0_TX1_ch_txdata => ch2_txdata_ext_0,
                    INTF0_TX1_ch_txrate => x"00",
                    INTF0_TX1_ch_txctrl0 => ch2_txctrl0_ext_0,
                    INTF0_TX1_ch_txctrl1 => ch2_txctrl1_ext_0,
                    INTF0_TX1_ch_txctrl2 => ch2_txctrl2_ext_0,
                    INTF0_TX2_ch_txdata => ch1_txdata_ext_0,
                    INTF0_TX2_ch_txrate => x"00",
                    INTF0_TX2_ch_txctrl0 => ch1_txctrl0_ext_0,
                    INTF0_TX2_ch_txctrl1 => ch1_txctrl1_ext_0,
                    INTF0_TX2_ch_txctrl2 => ch1_txctrl2_ext_0,
                    INTF0_TX3_ch_txdata => ch0_txdata_ext_0,
                    INTF0_TX3_ch_txrate => x"00",
                    INTF0_TX3_ch_txctrl0 => ch0_txctrl0_ext_0,
                    INTF0_TX3_ch_txctrl1 => ch0_txctrl1_ext_0,
                    INTF0_TX3_ch_txctrl2 => ch0_txctrl2_ext_0,
                    INTF0_RX0_ch_rxrate => x"00",
                    INTF0_RX1_ch_rxrate => x"00",
                    INTF0_RX2_ch_rxrate => x"00",
                    INTF0_RX3_ch_rxrate => x"00",
                    INTF0_rst_all_in => gtwiz_reset_all_in(0),
                    INTF0_rst_tx_pll_and_datapath_in => rst_tx_pll_and_datapath_in(quad),
                    INTF0_rst_tx_datapath_in => rst_tx_datapath_in(quad),
                    INTF0_rst_rx_pll_and_datapath_in => rst_rx_pll_and_datapath_in(quad),
                    INTF0_rst_rx_datapath_in => rst_rx_datapath_in(quad),
                    QUAD0_hsclk0_lcplllock => lcplllock0,
                    QUAD0_hsclk1_lcplllock => lcplllock1,
                    QUAD0_hsclk0_rplllock => rplllock0,
                    QUAD0_hsclk1_rplllock => rplllock1

                --
                --gt_reset_gt_bridge_ip_0         => gtwiz_reset_all_in(0), --: in STD_LOGIC;
                --lcpll_lock_gt_bridge_ip_0       => lcpll_lock, --: out STD_LOGIC;
                --rpll_lock_gt_bridge_ip_0        => rpll_lock, --: out STD_LOGIC;
                --rx_resetdone_out_gt_bridge_ip_0 => gtwiz_reset_rx_done_out(quad) --: out STD_LOGIC;

                );




            rxgearbox_data_in <= ch3_rxdata_ext_0(63 downto 0) & ch2_rxdata_ext_0(63 downto 0) & ch1_rxdata_ext_0(63 downto 0) & ch0_rxdata_ext_0(63 downto 0);

            g_rxgearbox: for channel in 0 to 3 generate
                --                    type array_69b is array (natural range <>) of std_logic_vector(68 downto 0);
                signal rxgearbox_data_out : std_logic_vector(66 downto 0);
                signal rxgearbox_data_valid_out : std_logic;
                signal userdata_rx_reversed: std_logic_vector(63 downto 0);
                signal gtwiz_userdata_rx_out_s : std_logic_vector(63 downto 0);
            begin
                rxgearbox_inst : entity work.rxgearbox_64b67b
                    Port Map(
                        clk            => RX_User_Clock(quad*4+channel),
                        data_in        => userdata_rx_reversed, --gtwiz_userdata_rx_out(0 to 63), -- gtwiz_userdata_rx_out(63 downto 0),
                        data_out       => rxgearbox_data_out,
                        data_valid_out => rxgearbox_data_valid_out,
                        BitSlip        => RX_Gearboxslip_s(4*quad+channel) --rxgearboxslip_in(j) --RX_Gearboxslip_In_s -- Slipping done by Transceiver_10g_64b67b_BLOCK_SYNC_SM.
                    ); -- Slipping can also be performed by the Interlaken RX lane

                gtwiz_userdata_rx_out_s <= rxgearbox_data_in(63+(64*channel) downto 64*channel);
                userdata_inverse : for i in 0 to 63 generate
                    userdata_rx_reversed(i) <= gtwiz_userdata_rx_out_s(63 - i);
                end generate;

                rxdatavalid_out(2 * channel)                           <= rxgearbox_data_valid_out;
                rxheadervalid_out(2 * channel)                         <= rxgearbox_data_valid_out;
                rxheader_out(2 + (6 * channel) downto 6 * channel)           <= rxgearbox_data_out(66 downto 64);
                gtwiz_userdata_rx_out(63 + 64 * channel downto 64 * channel) <= rxgearbox_data_out(63 downto 0);

            end generate;


        end generate g_versalprime;

        xpm_cdc_sync_rst_inst_not_tx_resetdone_out : xpm_cdc_sync_rst
            generic map(
                DEST_SYNC_FF   => 2,
                INIT           => 1,
                INIT_SYNC_FF   => 0,
                SIM_ASSERT_CHK => 0
            )
            port map(
                src_rst  => not_TX_Resetdone_Out(quad),
                dest_clk => TX_User_Clock(quad*4),
                dest_rst => not_TX_Resetdone_Out_tx_User_clock
            );
        xpm_cdc_sync_rst_inst_not_rx_resetdone_out : xpm_cdc_sync_rst
            generic map(
                DEST_SYNC_FF   => 2,
                INIT           => 1,
                INIT_SYNC_FF   => 0,
                SIM_ASSERT_CHK => 0
            )
            port map(
                src_rst  => not_RX_Resetdone_Out(quad),
                dest_clk => RX_User_Clock(quad*4),
                dest_rst => not_RX_Resetdone_Out_rx_User_clock
            );
        ------------------------------ Set GTY active signals ---------------------------------------
        --! FS: TX_Resetdone_Out and RX_Resetdone_out prepended with not_ because they are inverted
        --! FS: Syncronized the signals with xpm_cdc_sync_rst to RX/TX user_clock to avoid timing violations
        tx_active : process(TX_User_Clock, not_TX_Resetdone_Out_tx_User_clock)
        begin
            if not_TX_Resetdone_Out_tx_User_clock = '1' then
                tx_active_meta <= '0';
                tx_active_sync <= '0';
            elsif rising_edge(TX_User_Clock(quad*4)) then
                tx_active_meta <= '1';
                tx_active_sync <= tx_active_meta;
            end if;
        end process;

        rx_active : process(RX_User_Clock, not_RX_Resetdone_Out_rx_User_clock)
        begin
            if not_RX_Resetdone_Out_rx_User_clock = '1' then
                rx_active_meta <= '0';
                rx_active_sync <= '0';
            elsif rising_edge(RX_User_Clock(quad*4)) then

                rx_active_meta <= '1';
                rx_active_sync <= rx_active_meta;

            end if;
        end process;

        ------------------------------- Gearbox reset -------------------------------------
        --        rx_gearbox_reset(quad) <= rst_rxusr_403M or not gtwiz_reset_rx_done_out(quad);
        rx_gearbox_reset_i <= rst_rxusr_403M or not gtwiz_reset_rx_done_out(quad);


        rx_gearbox_reset_sync_inst : xpm_cdc_single
            generic map(
                DEST_SYNC_FF   => 2,
                INIT_SYNC_FF   => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG  => 0
            )
            port map(
                src_clk  => clk100,
                src_in   => rx_gearbox_reset_i,
                dest_clk => RX_User_Clock(quad*4),
                dest_out => rx_gearbox_reset(quad)
            );

    end generate g_quads;

    ------------------------------- Gearbox logic -------------------------------------
    g_block_sync_sm : for i in 0 to Lanes - 1 generate

        ------------------------------- RX Gearbox bitslip -- -------------------------------------
        block_sync_i  :  entity work.word_boundary_sync
            generic map
        (
                GOOD_WORDS_TO_LOCK      => 64,
                SYNC_WORD_COUNT_PERIOD  => 64,
                SYNC_ERRORS_TO_UNLOCK   => 16,
                BITSLIP_PULSE_LENGTH    => 5,
                BITSLIP_WAIT_DELAY      => 32
            )
            port map
        (
                -- User Interface
                lock_o       =>    open,
                bitslip_o    =>    RX_Gearboxslip_s(i),
                header_i     =>    RX_Header_s(i),
                valid_i      =>    RX_Headervalid_s(i),

                -- System Interface
                clk          =>    RX_User_Clock(i),
                rst          =>    rx_gearbox_reset(i / 4)
            );
    end generate;

end Behavioral;

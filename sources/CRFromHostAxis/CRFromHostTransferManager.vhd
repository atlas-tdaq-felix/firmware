--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2024 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               Marius Wensing
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

library XPM;
    use XPM.VCOMPONENTS.ALL;

    use work.axi_stream_package.all;
    use work.FELIX_package.all;

entity CRFromHostTransferManager is
    generic (
        NUM_STREAMS : integer := 1;
        CH_OFFSET : integer := 0
    );
    port (
        clk : in std_logic;
        daq_reset : in std_logic;
        daq_fifo_flush : in std_logic;
        fhAxis32 : in axis_32_type;
        fhAxis32_tready : out std_logic;
        stream_out : out axis_8_array_type(0 to NUM_STREAMS-1);
        stream_out_tready : in axis_tready_array_type(0 to NUM_STREAMS-1);
        broadcastEnable : in std_logic_vector(NUM_STREAMS-1 downto 0) := (others => '0')
    );
end CRFromHostTransferManager;

architecture rtl of CRFromHostTransferManager is
    signal fifo_din : std_logic_vector(44 downto 0);
    signal fifo_dout : std_logic_vector(44 downto 0);
    signal fifo_wren : std_logic;
    signal fifo_rden : std_logic;
    signal fifo_full : std_logic;
    signal fifo_empty : std_logic;
    -- signal fifo_m_axis_tdata : std_logic_vector(31 downto 0);
    -- signal fifo_m_axis_tkeep : std_logic_vector(3 downto 0);
    -- signal fifo_m_axis_tlast : std_logic;
    -- signal fifo_m_axis_tvalid : std_logic;
    -- signal fifo_m_axis_tid : std_logic_vector(7 downto 0);
    -- signal fifo_m_axis_tready : std_logic;
    -- signal fifo_s_axis_tdata : std_logic_vector(31 downto 0);
    -- signal fifo_s_axis_tkeep : std_logic_vector(3 downto 0);
    -- signal fifo_s_axis_tlast : std_logic;
    -- signal fifo_s_axis_tvalid : std_logic;
    -- signal fifo_s_axis_tid : std_logic_vector(7 downto 0);
    -- signal fifo_s_axis_tready : std_logic;
    signal fhAxis32_fifo : axis_32_type;
    signal fhAxis32_fifo_tready : std_logic;

    signal data_sr : std_logic_vector(23 downto 0);
    signal valid_sr : std_logic_vector(2 downto 0);
    signal last_sr : std_logic_vector(2 downto 0);
    signal data_cnt : integer range 0 to 3 := 0;
    signal stream_sel : std_logic_vector(0 to NUM_STREAMS-1);
    signal stream_ack : std_logic_vector(0 to NUM_STREAMS-1);
    signal stream_out_tready_combined : std_logic;
    signal tready : std_logic;
begin

    -- fifo_s_axis_tdata <= fhAxis32.tdata;
    -- fifo_s_axis_tkeep <= fhAxis32.tkeep;
    -- fifo_s_axis_tid <= fhAxis32.tid;
    -- fifo_s_axis_tvalid <= fhAxis32.tvalid;
    -- fifo_s_axis_tlast <= fhAxis32.tlast;
    -- fhAxis32_tready <= fifo_s_axis_tready;

    fifo_din(31 downto 0) <= fhAxis32.tdata;
    fifo_din(35 downto 32) <= fhAxis32.tkeep;
    fifo_din(43 downto 36) <= fhAxis32.tid;
    fifo_din(44) <= fhAxis32.tlast;
    fifo_wren <= fhAxis32.tvalid and not fifo_full;
    fhAxis32_tready <= not fifo_full;

    fifo: xpm_fifo_sync
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT"
            FIFO_MEMORY_TYPE => "auto",
            FIFO_WRITE_DEPTH => 64,
            WRITE_DATA_WIDTH => 45,
            READ_MODE => "fwft",
            FIFO_READ_LATENCY => 0,
            FULL_RESET_VALUE => 0,
            USE_ADV_FEATURES => "0002",
            READ_DATA_WIDTH => 45,
            WR_DATA_COUNT_WIDTH => 1,
            PROG_FULL_THRESH => 50,
            RD_DATA_COUNT_WIDTH => 1,
            PROG_EMPTY_THRESH => 10,
            DOUT_RESET_VALUE => "0",
            ECC_MODE => "no_ecc",
            SIM_ASSERT_CHK => 0,
            WAKEUP_TIME => 0
        )
        port map (
            sleep => '0',
            rst => daq_fifo_flush,
            wr_clk => clk,
            wr_en => fifo_wren,
            din => fifo_din,
            full => fifo_full,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => open,
            almost_full => open,
            wr_ack => open,
            rd_en => fifo_rden,
            dout => fifo_dout,
            empty => fifo_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => open,
            almost_empty => open,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );

    fhAxis32_fifo.tdata <= fifo_dout(31 downto 0);
    fhAxis32_fifo.tuser <= (others => '0');
    fhAxis32_fifo.tid <= fifo_dout(43 downto 36);
    fhAxis32_fifo.tkeep <= fifo_dout(35 downto 32);
    fhAxis32_fifo.tvalid <= not fifo_empty;
    fhAxis32_fifo.tlast <= fifo_dout(44);
    fifo_rden <= not fifo_empty and fhAxis32_fifo_tready;

    --fifo: XPM_FIFO_AXIS
    --    generic map (
    --        CASCADE_HEIGHT => 0,
    --        CDC_SYNC_STAGES => 2,
    --        CLOCKING_MODE => "common_clock",
    --        ECC_MODE => "no_ecc",
    --        FIFO_DEPTH => 64,
    --        FIFO_MEMORY_TYPE => "auto",
    --        PACKET_FIFO => "false",
    --        PROG_EMPTY_THRESH => 10,
    --        PROG_FULL_THRESH => 10,
    --        RD_DATA_COUNT_WIDTH => 1,
    --        RELATED_CLOCKS => 0,
    --        SIM_ASSERT_CHK => 1,
    --        TDATA_WIDTH => 32,
    --        TDEST_WIDTH => 1,
    --        TID_WIDTH => 8,
    --        TUSER_WIDTH => 1,
    --        USE_ADV_FEATURES => "1000",
    --        WR_DATA_COUNT_WIDTH => 1
    --    )
    --    port map (
    --        almost_empty_axis => open,
    --        almost_full_axis => open,
    --        dbiterr_axis => open,
    --        m_axis_tdata => fifo_m_axis_tdata,
    --        m_axis_tdest => open,
    --        m_axis_tid => fifo_m_axis_tid,
    --        m_axis_tkeep => fifo_m_axis_tkeep,
    --        m_axis_tlast => fifo_m_axis_tlast,
    --        m_axis_tstrb => open,
    --        m_axis_tuser => open,
    --        m_axis_tvalid => fifo_m_axis_tvalid,
    --        prog_empty_axis => open,
    --        prog_full_axis => open,
    --        rd_data_count_axis => open,
    --        s_axis_tready => fifo_s_axis_tready,
    --        sbiterr_axis => open,
    --        wr_data_count_axis => open,
    --        injectdbiterr_axis => '0',
    --        injectsbiterr_axis => '0',
    --        m_aclk => clk,
    --        m_axis_tready => fifo_m_axis_tready,
    --        s_aclk => clk,
    --        s_aresetn => aresetn,
    --        s_axis_tdata => fifo_s_axis_tdata,
    --        s_axis_tdest => "0",
    --        s_axis_tid => fifo_s_axis_tid,
    --        s_axis_tkeep => fifo_s_axis_tkeep,
    --        s_axis_tlast => fifo_s_axis_tlast,
    --        s_axis_tstrb => "1111",
    --        s_axis_tuser => "0",
    --        s_axis_tvalid => fifo_s_axis_tvalid
    --    );

    -- fhAxis32_fifo.tdata <= fifo_m_axis_tdata;
    -- fhAxis32_fifo.tuser <= (others => '0');
    -- fhAxis32_fifo.tid <= fifo_m_axis_tid;
    -- fhAxis32_fifo.tkeep <= fifo_m_axis_tkeep;
    -- fhAxis32_fifo.tvalid <= fifo_m_axis_tvalid;
    -- fhAxis32_fifo.tlast <= fifo_m_axis_tlast;
    -- fifo_m_axis_tready <= fhAxis32_fifo_tready;

    -- select stream based on tid field
    process (fhAxis32_fifo, broadcastEnable) begin
        if fhAxis32_fifo.tid = "11111111" then
            -- simply copy broadcast enable
            for I in 0 to NUM_STREAMS-1 loop
                stream_sel(I) <= broadcastEnable(I);
            end loop;
        elsif (to_integer(unsigned(fhAxis32_fifo.tid)) >= CH_OFFSET) and (to_integer(unsigned(fhAxis32_fifo.tid)) < CH_OFFSET + NUM_STREAMS) then
            stream_sel <= (others => '0');
            stream_sel(to_integer(unsigned(fhAxis32_fifo.tid))-CH_OFFSET) <= '1';
        else
            stream_sel <= (others => '0');
        end if;
    end process;

    -- simple 32 -> 8 shift register
    tready <= stream_out_tready_combined when (data_cnt = 3) else '0';
    fhAxis32_fifo_tready <= tready;

    -- combined tready
    process (stream_out_tready, stream_ack, stream_sel) is
        variable ready : std_logic;
    begin
        ready := '1';
        for I in 0 to NUM_STREAMS-1 loop
            if stream_sel(I) = '1' then
                if (stream_ack(I) = '0') and (stream_out_tready(I) = '0') then
                    ready := '0';
                end if;
            end if;
        end loop;
        stream_out_tready_combined <= ready;
    end process;

    -- keep track of broadcasts
    process (clk)
        variable all_acked : boolean;
    begin
        if rising_edge(clk) then
            if daq_reset = '1' then
                stream_ack <= (others => '0');
            else
                -- loop through all transfer managers
                all_acked := true;
                for ST in 0 to NUM_STREAMS-1 loop
                    if stream_sel(ST) = '1' then
                        if (stream_out_tready(ST) = '0') then
                            all_acked := false;
                        else
                            stream_ack(ST) <= '1';
                        end if;
                    end if;
                end loop;

                -- if all selected acknowledged the transfer we can proceed
                if all_acked then
                    stream_ack <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    -- fanout streams
    stream_fanout_mux: process (data_cnt, data_sr, valid_sr, last_sr, fhAxis32_fifo, stream_sel, stream_ack)
    begin
        for ST in 0 to NUM_STREAMS-1 loop
            stream_out(ST).tdata <= data_sr(23 downto 16) when (data_cnt = 3) else
                                    data_sr(15 downto 8) when (data_cnt = 2) else
                                    data_sr(7 downto 0) when (data_cnt = 1) else
                                    fhAxis32_fifo.tdata(7 downto 0);
            stream_out(ST).tvalid <= valid_sr(2) and stream_sel(ST) and not stream_ack(ST) when data_cnt = 3 else
                                     valid_sr(1) and stream_sel(ST) and not stream_ack(ST) when data_cnt = 2 else
                                     valid_sr(0) and stream_sel(ST) and not stream_ack(ST)  when data_cnt = 1 else
                                     (fhAxis32_fifo.tkeep(0) and fhAxis32_fifo.tvalid and stream_sel(ST) and not stream_ack(ST));
            stream_out(ST).tlast <= '1' when (data_cnt = 0) and fhAxis32_fifo.tkeep = "0001" else
                                    last_sr(2) when data_cnt = 3 else
                                    last_sr(1) when data_cnt = 2 else
                                    last_sr(0) when data_cnt = 1 else
                                    '0';
        end loop;
    end process;

    process (clk) begin
        if rising_edge(clk) then
            if daq_reset = '1' then
                data_cnt <= 0;
                data_sr <= (others => '0');
                last_sr <= (others => '0');
                valid_sr <= (others => '0');
            else
                if data_cnt = 0 then
                    if (fhAxis32_fifo.tvalid = '1') and (stream_out_tready_combined = '1') then
                        data_sr <= fhAxis32_fifo.tdata(31 downto 8);
                        valid_sr <= fhAxis32_fifo.tkeep(3 downto 1);
                        case fhAxis32_fifo.tkeep is
                            when "1111" => last_sr <= fhAxis32_fifo.tlast & "00";
                            when "0111" => last_sr <= "0" & fhAxis32_fifo.tlast & "0";
                            when "0011" => last_sr <= "00" & fhAxis32_fifo.tlast;
                            when others => last_sr <= "000";
                        end case;
                        data_cnt <= 1;
                    end if;
                else
                    if stream_out_tready_combined = '1' then
                        if data_cnt = 3 then
                            data_cnt <= 0;
                        else
                            data_cnt <= data_cnt + 1;
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process;


end architecture;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2024 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

library XPM;
    use XPM.VCOMPONENTS.ALL;

    use work.axi_stream_package.ALL;

entity CRFromHostLinkManager is
    generic (
        DATA_WIDTH : integer := 256;
        NUM_TRANSFER_MANAGERS : integer := 1;
        STREAMS_PER_LINK_FROMHOST : integer range 1 to 64 := 1
    );
    port (
        clk : in std_logic;
        daq_reset : in std_logic;
        daq_fifo_flush : in std_logic;

        linkManager_data : in std_logic_vector(DATA_WIDTH-1 downto 0);
        linkManager_valid : in std_logic;
        linkManager_ready : out std_logic;

        fhAxis32             : out axis_32_array_type(0 to NUM_TRANSFER_MANAGERS-1);
        fhAxis32_tready      : in  std_logic_vector(0 to NUM_TRANSFER_MANAGERS-1)
    );
end CRFromHostLinkManager;

architecture rtl of CRFromHostLinkManager is
    function CALC_READ_DATA_WIDTH(DATA_WIDTH : integer) return integer is
    begin
        if DATA_WIDTH >= 256 then
            return DATA_WIDTH / 8;
        else
            return 32;
        end if;
    end function;

    constant NUM_FIFO_PHASES : integer := CALC_READ_DATA_WIDTH(DATA_WIDTH) / 32;

    signal fifo_din : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal fifo_dout : std_logic_vector(CALC_READ_DATA_WIDTH(DATA_WIDTH)-1 downto 0);
    signal fifo_dout_phase : std_logic_vector(31 downto 0);
    signal fifo_rden_phase : std_logic;
    signal fifo_wren : std_logic;
    signal fifo_rden : std_logic;
    signal fifo_full : std_logic; -- @suppress "signal fifo_full is never read"
    signal fifo_pfull : std_logic;
    signal fifo_empty : std_logic;
    signal fifo_phase : integer range 0 to NUM_FIFO_PHASES-1;

    signal state : std_logic_vector((DATA_WIDTH/32)-1 downto 0);
    signal active : std_logic;
    signal mlen : integer range 0 to 124;
    signal lastpkt : std_logic;
    signal channel : std_logic_vector(7 downto 0);
    signal tm_sel : std_logic_vector(0 to NUM_TRANSFER_MANAGERS-1);
    signal tm_ack : std_logic_vector(0 to NUM_TRANSFER_MANAGERS-1);
    signal fhAxis32_tready_combined : std_logic;
begin
    -- we can accept data when there is space in the FIFO
    linkManager_ready <= not fifo_pfull;

    -- we have to reverse the data to the FIFO
    process (linkManager_data) begin
        for I in 0 to DATA_WIDTH/32-1 loop
            fifo_din(32*I+31 downto 32*I) <= linkManager_data(DATA_WIDTH-32*I-1 downto DATA_WIDTH-32*I-32);
        end loop;
    end process;

    -- write new data when valid
    fifo_wren <= linkManager_valid;

    -- read enable and phase
    fifo_rden_phase <= not fifo_empty when (active = '1') and (fhAxis32_tready_combined = '1' or mlen = 0) else '0';
    fifo_rden <= fifo_rden_phase when fifo_phase = NUM_FIFO_PHASES-1 else '0';
    -- fifo_dout_phase <= fifo_dout(63 downto 32) when fifo_phase = '1' else fifo_dout(31 downto 0);
    fifo_dout_phase <= fifo_dout(32*fifo_phase+31 downto 32*fifo_phase);

    process (clk) begin
        if rising_edge(clk) then
            if (daq_reset = '1') or (daq_fifo_flush = '1') then
                fifo_phase <= 0;
            else
                if fifo_rden_phase = '1' then
                    if fifo_phase = NUM_FIFO_PHASES - 1 then
                        fifo_phase <= 0;
                    else
                        fifo_phase <= fifo_phase + 1;
                    end if;
                end if;
            end if;
        end if;
    end process;

    fifo: xpm_fifo_sync
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT"
            FIFO_MEMORY_TYPE => "block",
            FIFO_WRITE_DEPTH => 512,
            WRITE_DATA_WIDTH => DATA_WIDTH,
            READ_MODE => "fwft",
            FIFO_READ_LATENCY => 0,
            FULL_RESET_VALUE => 0,
            USE_ADV_FEATURES => "0002",
            READ_DATA_WIDTH => CALC_READ_DATA_WIDTH(DATA_WIDTH),
            WR_DATA_COUNT_WIDTH => 1,
            PROG_FULL_THRESH => 502,
            RD_DATA_COUNT_WIDTH => 1,
            PROG_EMPTY_THRESH => 10,
            DOUT_RESET_VALUE => "0",
            ECC_MODE => "no_ecc",
            SIM_ASSERT_CHK => 0,
            WAKEUP_TIME => 0
        )
        port map (
            sleep => '0',
            rst => daq_fifo_flush,
            wr_clk => clk,
            wr_en => fifo_wren,
            din => fifo_din,
            full => fifo_full,
            prog_full => fifo_pfull,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => open,
            almost_full => open,
            wr_ack => open,
            rd_en => fifo_rden,
            dout => fifo_dout,
            empty => fifo_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => open,
            almost_empty => open,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );

    -- active state
    active <= or (state);

    -- link manager control
    process (clk) begin
        if rising_edge(clk) then
            if daq_reset = '1' then
                mlen <= 0;
                lastpkt <= '0';
                state <= (others => '0');
                channel <= (others => '0');
            else
                if active = '0' then
                    -- leave IDLE state when data is in the FIFO
                    if fifo_empty = '0' then
                        state(0) <= '1';
                        state(state'left downto 1) <= (others => '0');
                    end if;
                else
                    if state(0) = '1' then
                        if fifo_empty = '1' then
                            state <= (others => '0');
                        else
                            -- in this state the FIFO has the packet header
                            channel <= fifo_dout_phase(15 downto 8);
                            if fifo_dout_phase(7 downto 0) = "11111111" then
                                mlen <= (DATA_WIDTH-32)/8;
                                lastpkt <= '0';
                            else
                                mlen <= to_integer(unsigned(fifo_dout_phase(7 downto 0)));
                                lastpkt <= '1';
                            end if;

                            state(state'left downto 1) <= state(state'left-1 downto 0);
                            state(0) <= state(state'left);
                        end if;
                    else
                        if mlen > 0 then
                            -- if we still have data available we have to respect tready
                            if fhAxis32_tready_combined = '1' then
                                -- leave active state when no data is in the FIFO in the last state
                                state(state'left downto 1) <= state(state'left-1 downto 0);
                                state(0) <= state(state'left);

                                -- count mlen
                                if mlen >= 4 then
                                    mlen <= mlen - 4;
                                else
                                    mlen <= 0;
                                end if;
                            end if;
                        else
                            -- just shift
                            -- leave active state when no data is in the FIFO in the last state
                            state <= state(state'left-1 downto 0) & (state(state'left) and not fifo_empty);
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process;

    -- select transfer manager
    process (channel) begin
        if channel = "11111111" then
            tm_sel <= (others => '1');
        elsif to_integer(unsigned(channel)) < STREAMS_PER_LINK_FROMHOST then
            tm_sel <= (others => '0');
            tm_sel(to_integer(unsigned(channel)) * NUM_TRANSFER_MANAGERS / STREAMS_PER_LINK_FROMHOST) <= '1';
        else
            tm_sel <= (others => '0');
        end if;
    end process;

    -- maintain acknowledge for broadcasts
    process (clk)
        variable all_acked : boolean;
    begin
        if rising_edge(clk) then
            if daq_reset = '1' then
                tm_ack <= (others => '0');
            else
                -- loop through all transfer managers
                all_acked := true;
                for TM in 0 to NUM_TRANSFER_MANAGERS-1 loop
                    if tm_sel(TM) = '1' then
                        if (fhAxis32_tready(TM) = '0') then
                            all_acked := false;
                        else
                            tm_ack(TM) <= '1';
                        end if;
                    end if;
                end loop;

                -- if all selected acknowledged the transfer we can proceed
                if all_acked then
                    tm_ack <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    -- generate combined tready signal
    -- fhAxis32_tready_combined <= or(fhAxis32_tready or tm_ack or (not tm_sel));
    process (tm_ack, tm_sel, fhAxis32_tready) is
        variable ready : std_logic;
    begin
        ready := '1';
        for I in 0 to NUM_TRANSFER_MANAGERS-1 loop
            if tm_sel(I) = '1' then
                if (tm_ack(I) = '0') and (fhAxis32_tready(I) = '0') then
                    ready := '0';
                end if;
            end if;
        end loop;
        fhAxis32_tready_combined <= ready;
    end process;

    -- fanout data
    process (fifo_dout_phase, mlen, state, lastpkt, channel, tm_ack, tm_sel) begin
        for TM in 0 to NUM_TRANSFER_MANAGERS-1 loop
            fhAxis32(TM).tdata <= fifo_dout_phase(7 downto 0) & fifo_dout_phase(15 downto 8) & fifo_dout_phase(23 downto 16) & fifo_dout_phase(31 downto 24);
            fhAxis32(TM).tvalid <= '1' when (mlen > 0 and state(0) /= '1' and tm_ack(TM) = '0' and tm_sel(TM) = '1') else '0';
            fhAxis32(TM).tkeep <= "0000" when mlen = 0 else
                                  "0001" when mlen = 1 else
                                  "0011" when mlen = 2 else
                                  "0111" when mlen = 3 else
                                  "1111";
            fhAxis32(TM).tlast <= '1' when (mlen <= 4) and (lastpkt = '1') else '0';
            fhAxis32(TM).tid <= channel;
            fhAxis32(TM).tuser <= "0000";
        end loop;
    end process;


end architecture;

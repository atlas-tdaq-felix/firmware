library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

library XPM;
    use XPM.VCOMPONENTS.ALL;

    use work.axi_stream_package.all;

entity CRFromHostDataManagerAxis64 is
    generic (
        DATA_WIDTH          : integer := 256
    );
    port (
        -- reset
        clk : in std_logic;
        daq_reset : in std_logic;
        daq_fifo_flush : in std_logic;

        linkManager_data : in std_logic_vector(DATA_WIDTH-1 downto 0);
        linkManager_valid : in std_logic;
        linkManager_ready : out std_logic;

        fhAxis              : out axis_64_type;
        fhAxis_tready       : in  std_logic
    );
end CRFromHostDataManagerAxis64;

architecture rtl of CRFromHostDataManagerAxis64 is
    signal linkFifo_din : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal linkFifo_dout : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal linkFifo_wren : std_logic;
    signal linkFifo_rden : std_logic;
    signal linkFifo_empty : std_logic;
    signal linkFifo_full : std_logic;
    signal linkFifo_dout_rev : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal linkFifo_dout_rev_r : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal linkFifo_bytevalid_r : std_logic_vector((((DATA_WIDTH-32)/8)-1) downto 0);
    signal linkFifo_lastbyte_r : std_logic_vector((((DATA_WIDTH-32)/8)-1) downto 0);
    signal linkFifo_r_rden : std_logic;
    signal linkFifo_r_wouldread : std_logic;
    signal linkFifo_r_empty : std_logic;
    signal buf : std_logic_vector(31 downto 0);            -- we have to buffer up to 32 bits
    signal buf_bytevalid : std_logic_vector(3 downto 0);
    signal buf_lastbyte : std_logic_vector(3 downto 0);
    signal tdata : std_logic_vector(63 downto 0);
    signal tvalid : std_logic;
    signal tlast : std_logic;
    signal tkeep : std_logic_vector(7 downto 0);
    signal tid : std_logic_vector(7 downto 0);
    signal tready : std_logic;
    signal active : std_logic;
    signal leave_active_next : std_logic;
    signal cycle : integer range 0 to 30 := 0;
    signal ch : std_logic_vector(7 downto 0);
    signal ch_prev : std_logic_vector(7 downto 0);
    signal fhAxis_s : axis_64_type;
begin
    linkFifo_din <= linkManager_data;
    linkFifo_wren <= linkManager_valid;
    linkManager_ready <= not linkFifo_full;

    -- FIFO
    fifo: xpm_fifo_sync
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT"
            FIFO_MEMORY_TYPE => "block",
            FIFO_WRITE_DEPTH => 512,
            WRITE_DATA_WIDTH => DATA_WIDTH,
            READ_MODE => "std",
            FIFO_READ_LATENCY => 1,
            FULL_RESET_VALUE => 0,
            USE_ADV_FEATURES => "0002",
            READ_DATA_WIDTH => DATA_WIDTH,
            WR_DATA_COUNT_WIDTH => 1,
            PROG_FULL_THRESH => 502,
            RD_DATA_COUNT_WIDTH => 1,
            PROG_EMPTY_THRESH => 10,
            DOUT_RESET_VALUE => "0",
            ECC_MODE => "no_ecc",
            SIM_ASSERT_CHK => 0,
            WAKEUP_TIME => 0
        )
        port map (
            sleep => '0',
            rst => daq_fifo_flush,
            wr_clk => clk,
            wr_en => linkFifo_wren,
            din => linkFifo_din,
            full => linkFifo_full,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => open,
            almost_full => open,
            wr_ack => open,
            rd_en => linkFifo_rden,
            dout => linkFifo_dout,
            empty => linkFifo_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => open,
            almost_empty => open,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );

    -- reverse payload bytes from link FIFO
    linkFifo_dout_rev(DATA_WIDTH-1 downto DATA_WIDTH-32) <= linkFifo_dout(DATA_WIDTH-1 downto DATA_WIDTH-32);            -- do not reverse header
    linkFifo_dout_rev_generate: for I in 0 to (((DATA_WIDTH-32)/8)-1) generate
        linkFifo_dout_rev(8*I+7 downto 8*I) <= linkFifo_dout(DATA_WIDTH-40-8*I+7 downto DATA_WIDTH-40-8*I);
    end generate;
    ch <= linkFifo_dout_rev_r(DATA_WIDTH-32+15 downto DATA_WIDTH-32+8);        -- extract channel number (will be tid field)

    -- add another stage after the link FIFO to calculate a byte valid mask
    process (clk) is
        variable mlen : integer range 0 to 255;
    begin
        if rising_edge(clk) then
            if daq_reset = '1' or daq_fifo_flush = '1' then
                linkFifo_dout_rev_r <= (others => '0');
                linkFifo_bytevalid_r <= (others => '0');
                linkFifo_lastbyte_r <= (others => '0');
                linkFifo_r_empty <= '1';
            else
                if linkFifo_r_empty = '1' and linkFifo_empty = '0' then
                    linkFifo_r_empty <= '0';        -- we will have a valid in linkFifo_dout in next cycle
                elsif linkFifo_r_rden = '1' and linkFifo_empty = '1' then
                    linkFifo_r_empty <= '1';
                end if;

                if linkFifo_r_rden = '1' then
                    -- just register data
                    linkFifo_dout_rev_r <= linkFifo_dout_rev;

                    -- calculate last block info and bytevalid mask from mlen field
                    mlen := to_integer(unsigned(linkFifo_dout_rev(DATA_WIDTH-32+7 downto DATA_WIDTH-32)));
                    if ((mlen >= 127) and (DATA_WIDTH = 1024)) or ((mlen >= 63) and (DATA_WIDTH = 512)) or ((mlen >= 31) and (DATA_WIDTH = 256)) then
                        linkFifo_bytevalid_r <= (others => '1');
                        linkFifo_lastbyte_r <= (others => '0');
                    else
                        for I in 0 to ((DATA_WIDTH-32)/8)-1 loop
                            if I = mlen-1 then
                                linkFifo_lastbyte_r(I) <= '1';
                            else
                                linkFifo_lastbyte_r(I) <= '0';
                            end if;

                            if I <= mlen-1 then
                                linkFifo_bytevalid_r(I) <= '1';
                            else
                                linkFifo_bytevalid_r(I) <= '0';
                            end if;
                        end loop;
                    end if;
                end if;
            end if;
        end if;
    end process;
    linkFifo_rden <= not linkFifo_empty and (linkFifo_r_rden or linkFifo_r_empty);

    -- output to AXI stream record
    fhAxis_s.tdata <= tdata;
    fhAxis_s.tvalid <= tvalid;
    fhAxis_s.tid <= tid;
    fhAxis_s.tlast <= tlast;
    fhAxis_s.tkeep <= tkeep;
    fhAxis_s.tuser <= (others => '0');
    tready <= fhAxis_tready;
    tvalid <= active;

    pipe_axis: process(clk)
    begin
        if rising_edge(clk) then
            if tready = '1' then
                fhAxis <= fhAxis_s;
            end if;
        end if;
    end process;

    -- generate tdata, tkeep, tlast, tid
    process (cycle, linkFifo_dout_rev_r, linkFifo_bytevalid_r, linkFifo_lastbyte_r, buf_lastbyte, buf, buf_bytevalid) is
    begin
        if DATA_WIDTH = 256 then
            case cycle is
                when 0  =>
                    tdata <= linkFifo_dout_rev_r(63 downto 0);
                    tkeep <= linkFifo_bytevalid_r(7 downto 0);
                    tlast <= or (linkFifo_lastbyte_r(7 downto 0));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(7 downto 0));
                when 1 =>
                    tdata <= linkFifo_dout_rev_r(127 downto 64);
                    tkeep <= linkFifo_bytevalid_r(15 downto 8);
                    tlast <= or (linkFifo_lastbyte_r(15 downto 8));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(15 downto 8));
                when 2 =>
                    tdata <= linkFifo_dout_rev_r(191 downto 128);
                    tkeep <= linkFifo_bytevalid_r(23 downto 16);
                    tlast <= or (linkFifo_lastbyte_r(23 downto 16));
                    linkFifo_r_wouldread <= '1';
                when 3 =>
                    tdata <= linkFifo_dout_rev_r(31 downto 0) & buf;
                    tkeep(3 downto 0) <= buf_bytevalid;
                    if or (buf_lastbyte) = '1' then
                        tkeep(7 downto 4) <= "0000";
                        tlast <= '1';
                        linkFifo_r_wouldread <= '0';
                    else
                        tkeep(7 downto 4) <= linkFifo_bytevalid_r(3 downto 0);
                        tlast <= or (linkFifo_lastbyte_r(3 downto 0));
                        linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(3 downto 0));
                    end if;
                when 4 =>
                    tdata <= linkFifo_dout_rev_r(95 downto 32);
                    tkeep <= linkFifo_bytevalid_r(11 downto 4);
                    tlast <= or (linkFifo_lastbyte_r(11 downto 4));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(11 downto 4));
                when 5 =>
                    tdata <= linkFifo_dout_rev_r(159 downto 96);
                    tkeep <= linkFifo_bytevalid_r(19 downto 12);
                    tlast <= or (linkFifo_lastbyte_r(19 downto 12));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(19 downto 12));
                when 6 =>
                    tdata <= linkFifo_dout_rev_r(223 downto 160);
                    tkeep <= linkFifo_bytevalid_r(27 downto 20);
                    tlast <= or (linkFifo_lastbyte_r(27 downto 20));
                    linkFifo_r_wouldread <= '1';
                when others =>
                    tdata <= (others => '0');
                    tkeep <= (others => '0');
                    tlast <= '0';
                    linkFifo_r_wouldread <= '0';
            end case;
        elsif DATA_WIDTH = 512 then
            case cycle is
                when 0  =>
                    tdata <= linkFifo_dout_rev_r(63 downto 0);
                    tkeep <= linkFifo_bytevalid_r(7 downto 0);
                    tlast <= or (linkFifo_lastbyte_r(7 downto 0));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(7 downto 0));
                when 1 =>
                    tdata <= linkFifo_dout_rev_r(127 downto 64);
                    tkeep <= linkFifo_bytevalid_r(15 downto 8);
                    tlast <= or (linkFifo_lastbyte_r(15 downto 8));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(15 downto 8));
                when 2 =>
                    tdata <= linkFifo_dout_rev_r(191 downto 128);
                    tkeep <= linkFifo_bytevalid_r(23 downto 16);
                    tlast <= or (linkFifo_lastbyte_r(23 downto 16));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(23 downto 16));
                when 3 =>
                    tdata <= linkFifo_dout_rev_r(255 downto 192);
                    tkeep <= linkFifo_bytevalid_r(31 downto 24);
                    tlast <= or (linkFifo_lastbyte_r(31 downto 24));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(31 downto 24));
                when 4 =>
                    tdata <= linkFifo_dout_rev_r(319 downto 256);
                    tkeep <= linkFifo_bytevalid_r(39 downto 32);
                    tlast <= or (linkFifo_lastbyte_r(39 downto 32));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(39 downto 32));
                when 5 =>
                    tdata <= linkFifo_dout_rev_r(383 downto 320);
                    tkeep <= linkFifo_bytevalid_r(47 downto 40);
                    tlast <= or (linkFifo_lastbyte_r(47 downto 40));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(47 downto 40));
                when 6 =>
                    tdata <= linkFifo_dout_rev_r(447 downto 384);
                    tkeep <= linkFifo_bytevalid_r(55 downto 48);
                    tlast <= or (linkFifo_lastbyte_r(55 downto 48));
                    linkFifo_r_wouldread <= '1';
                when 7 =>
                    tdata <= linkFifo_dout_rev_r(31 downto 0) & buf;
                    tkeep(3 downto 0) <= buf_bytevalid;
                    if or (buf_lastbyte) = '1' then
                        tkeep(7 downto 4) <= "0000";
                        tlast <= '1';
                        linkFifo_r_wouldread <= '0';
                    else
                        tkeep(7 downto 4) <= linkFifo_bytevalid_r(3 downto 0);
                        tlast <= or (linkFifo_lastbyte_r(3 downto 0));
                        linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(3 downto 0));
                    end if;
                when 8 =>
                    tdata <= linkFifo_dout_rev_r(95 downto 32);
                    tkeep <= linkFifo_bytevalid_r(11 downto 4);
                    tlast <= or (linkFifo_lastbyte_r(11 downto 4));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(11 downto 4));
                when 9 =>
                    tdata <= linkFifo_dout_rev_r(159 downto 96);
                    tkeep <= linkFifo_bytevalid_r(19 downto 12);
                    tlast <= or (linkFifo_lastbyte_r(19 downto 12));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(19 downto 12));
                when 10 =>
                    tdata <= linkFifo_dout_rev_r(223 downto 160);
                    tkeep <= linkFifo_bytevalid_r(27 downto 20);
                    tlast <= or (linkFifo_lastbyte_r(27 downto 20));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(27 downto 20));
                when 11 =>
                    tdata <= linkFifo_dout_rev_r(287 downto 224);
                    tkeep <= linkFifo_bytevalid_r(35 downto 28);
                    tlast <= or (linkFifo_lastbyte_r(35 downto 28));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(35 downto 28));
                when 12 =>
                    tdata <= linkFifo_dout_rev_r(351 downto 288);
                    tkeep <= linkFifo_bytevalid_r(43 downto 36);
                    tlast <= or (linkFifo_lastbyte_r(43 downto 36));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(43 downto 36));
                when 13 =>
                    tdata <= linkFifo_dout_rev_r(415 downto 352);
                    tkeep <= linkFifo_bytevalid_r(51 downto 44);
                    tlast <= or (linkFifo_lastbyte_r(51 downto 44));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(51 downto 44));
                when 14 =>
                    tdata <= linkFifo_dout_rev_r(479 downto 416);
                    tkeep <= linkFifo_bytevalid_r(59 downto 52);
                    tlast <= or (linkFifo_lastbyte_r(59 downto 52));
                    linkFifo_r_wouldread <= '1';
                when others =>
                    tdata <= (others => '0');
                    tkeep <= (others => '0');
                    tlast <= '0';
                    linkFifo_r_wouldread <= '0';
            end case;
        else --DATA_WIDTH = 1024
            --@TODO: Ask Marius Wensing to implement this for 1024 bit.
            case cycle is
                when 0  =>
                    tdata <= linkFifo_dout_rev_r(63 downto 0);
                    tkeep <= linkFifo_bytevalid_r(7 downto 0);
                    tlast <= or (linkFifo_lastbyte_r(7 downto 0));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(7 downto 0));
                when 1 =>
                    tdata <= linkFifo_dout_rev_r(127 downto 64);
                    tkeep <= linkFifo_bytevalid_r(15 downto 8);
                    tlast <= or (linkFifo_lastbyte_r(15 downto 8));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(15 downto 8));
                when 2 =>
                    tdata <= linkFifo_dout_rev_r(191 downto 128);
                    tkeep <= linkFifo_bytevalid_r(23 downto 16);
                    tlast <= or (linkFifo_lastbyte_r(23 downto 16));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(23 downto 16));
                when 3 =>
                    tdata <= linkFifo_dout_rev_r(255 downto 192);
                    tkeep <= linkFifo_bytevalid_r(31 downto 24);
                    tlast <= or (linkFifo_lastbyte_r(31 downto 24));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(31 downto 24));
                when 4 =>
                    tdata <= linkFifo_dout_rev_r(319 downto 256);
                    tkeep <= linkFifo_bytevalid_r(39 downto 32);
                    tlast <= or (linkFifo_lastbyte_r(39 downto 32));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(39 downto 32));
                when 5 =>
                    tdata <= linkFifo_dout_rev_r(383 downto 320);
                    tkeep <= linkFifo_bytevalid_r(47 downto 40);
                    tlast <= or (linkFifo_lastbyte_r(47 downto 40));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(47 downto 40));
                when 6 =>
                    tdata <= linkFifo_dout_rev_r(447 downto 384);
                    tkeep <= linkFifo_bytevalid_r(55 downto 48);
                    tlast <= or (linkFifo_lastbyte_r(55 downto 48));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(55 downto 48));
                when 7 =>
                    tdata <= linkFifo_dout_rev_r(511 downto 448);
                    tkeep <= linkFifo_bytevalid_r(63 downto 56);
                    tlast <= or (linkFifo_lastbyte_r(63 downto 56));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(63 downto 56));
                when 8 =>
                    tdata <= linkFifo_dout_rev_r(575 downto 512);
                    tkeep <= linkFifo_bytevalid_r(71 downto 64);
                    tlast <= or (linkFifo_lastbyte_r(71 downto 64));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(71 downto 64));
                when 9 =>
                    tdata <= linkFifo_dout_rev_r(639 downto 576);
                    tkeep <= linkFifo_bytevalid_r(79 downto 72);
                    tlast <= or (linkFifo_lastbyte_r(79 downto 72));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(79 downto 72));
                when 10 =>
                    tdata <= linkFifo_dout_rev_r(703 downto 640);
                    tkeep <= linkFifo_bytevalid_r(87 downto 80);
                    tlast <= or (linkFifo_lastbyte_r(87 downto 80));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(87 downto 80));
                when 11 =>
                    tdata <= linkFifo_dout_rev_r(767 downto 704);
                    tkeep <= linkFifo_bytevalid_r(95 downto 88);
                    tlast <= or (linkFifo_lastbyte_r(95 downto 88));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(95 downto 88));
                when 12 =>
                    tdata <= linkFifo_dout_rev_r(831 downto 768);
                    tkeep <= linkFifo_bytevalid_r(103 downto 96);
                    tlast <= or (linkFifo_lastbyte_r(103 downto 96));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(103 downto 96));
                when 13 =>
                    tdata <= linkFifo_dout_rev_r(895 downto 832);
                    tkeep <= linkFifo_bytevalid_r(111 downto 104);
                    tlast <= or (linkFifo_lastbyte_r(111 downto 104));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(111 downto 104));
                when 14 =>
                    tdata <= linkFifo_dout_rev_r(959 downto 896);
                    tkeep <= linkFifo_bytevalid_r(119 downto 112);
                    tlast <= or (linkFifo_lastbyte_r(119 downto 112));
                    linkFifo_r_wouldread <= '1';
                when 15 =>
                    tdata <= linkFifo_dout_rev_r(31 downto 0) & buf;
                    tkeep(3 downto 0) <= buf_bytevalid;
                    if or (buf_lastbyte) = '1' then
                        tkeep(7 downto 4) <= "0000";
                        tlast <= '1';
                        linkFifo_r_wouldread <= '0';
                    else
                        tkeep(7 downto 4) <= linkFifo_bytevalid_r(3 downto 0);
                        tlast <= or (linkFifo_lastbyte_r(3 downto 0));
                        linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(3 downto 0));
                    end if;
                when 16 =>
                    tdata <= linkFifo_dout_rev_r(95 downto 32);
                    tkeep <= linkFifo_bytevalid_r(11 downto 4);
                    tlast <= or (linkFifo_lastbyte_r(11 downto 4));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(11 downto 4));
                when 17 =>
                    tdata <= linkFifo_dout_rev_r(159 downto 96);
                    tkeep <= linkFifo_bytevalid_r(19 downto 12);
                    tlast <= or (linkFifo_lastbyte_r(19 downto 12));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(19 downto 12));
                when 18 =>
                    tdata <= linkFifo_dout_rev_r(223 downto 160);
                    tkeep <= linkFifo_bytevalid_r(27 downto 20);
                    tlast <= or (linkFifo_lastbyte_r(27 downto 20));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(27 downto 20));
                when 19 =>
                    tdata <= linkFifo_dout_rev_r(287 downto 224);
                    tkeep <= linkFifo_bytevalid_r(35 downto 28);
                    tlast <= or (linkFifo_lastbyte_r(35 downto 28));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(35 downto 28));
                when 20 =>
                    tdata <= linkFifo_dout_rev_r(351 downto 288);
                    tkeep <= linkFifo_bytevalid_r(43 downto 36);
                    tlast <= or (linkFifo_lastbyte_r(43 downto 36));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(43 downto 36));
                when 21 =>
                    tdata <= linkFifo_dout_rev_r(415 downto 352);
                    tkeep <= linkFifo_bytevalid_r(51 downto 44);
                    tlast <= or (linkFifo_lastbyte_r(51 downto 44));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(51 downto 44));
                when 22 =>
                    tdata <= linkFifo_dout_rev_r(479 downto 416);
                    tkeep <= linkFifo_bytevalid_r(59 downto 52);
                    tlast <= or (linkFifo_lastbyte_r(59 downto 52));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(59 downto 52));
                when 23 =>
                    tdata <= linkFifo_dout_rev_r(543 downto 480);
                    tkeep <= linkFifo_bytevalid_r(67 downto 60);
                    tlast <= or (linkFifo_lastbyte_r(67 downto 60));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(67 downto 60));
                when 24 =>
                    tdata <= linkFifo_dout_rev_r(607 downto 544);
                    tkeep <= linkFifo_bytevalid_r(75 downto 68);
                    tlast <= or (linkFifo_lastbyte_r(75 downto 68));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(75 downto 68));
                when 25 =>
                    tdata <= linkFifo_dout_rev_r(671 downto 608);
                    tkeep <= linkFifo_bytevalid_r(83 downto 76);
                    tlast <= or (linkFifo_lastbyte_r(83 downto 76));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(83 downto 76));
                when 26 =>
                    tdata <= linkFifo_dout_rev_r(735 downto 672);
                    tkeep <= linkFifo_bytevalid_r(91 downto 84);
                    tlast <= or (linkFifo_lastbyte_r(91 downto 84));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(91 downto 84));
                when 27 =>
                    tdata <= linkFifo_dout_rev_r(799 downto 736);
                    tkeep <= linkFifo_bytevalid_r(99 downto 92);
                    tlast <= or (linkFifo_lastbyte_r(99 downto 92));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(99 downto 92));
                when 28 =>
                    tdata <= linkFifo_dout_rev_r(863 downto 800);
                    tkeep <= linkFifo_bytevalid_r(107 downto 100);
                    tlast <= or (linkFifo_lastbyte_r(107 downto 100));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(107 downto 100));
                when 29 =>
                    tdata <= linkFifo_dout_rev_r(927 downto 864);
                    tkeep <= linkFifo_bytevalid_r(115 downto 108);
                    tlast <= or (linkFifo_lastbyte_r(115 downto 108));
                    linkFifo_r_wouldread <= or (linkFifo_lastbyte_r(115 downto 108));
                when 30 =>
                    tdata <= linkFifo_dout_rev_r(991 downto 928);
                    tkeep <= linkFifo_bytevalid_r(123 downto 116);
                    tlast <= or (linkFifo_lastbyte_r(123 downto 116));
                    linkFifo_r_wouldread <= '1';
                when others =>
                    tdata <= (others => '0');
                    tkeep <= (others => '0');
                    tlast <= '0';
                    linkFifo_r_wouldread <= '0';
            end case;
        end if;
    end process;

    -- select tid
    tid <= ch_prev when ((cycle = 3) and (DATA_WIDTH = 256)) or ((cycle = 7) and (DATA_WIDTH = 512)) or ((cycle = 15) and (DATA_WIDTH = 1024)) else ch;

    -- reading of the link FIFO
    process (active, linkFifo_r_empty, linkFifo_r_wouldread, tready) begin
        if active = '0' then
            -- read the link FIFO when there is something available
            linkFifo_r_rden <= not linkFifo_r_empty;
        else
            linkFifo_r_rden <= not linkFifo_r_empty and linkFifo_r_wouldread and tready;
        end if;
    end process;

    -- state machine
    process (clk) begin
        if rising_edge(clk) then
            if daq_reset = '1' then
                active <= '0';
                cycle <= 0;
                ch_prev <= "00000000";
                buf <= (others => '0');
                buf_bytevalid <= (others => '0');
                buf_lastbyte <= (others => '0');
                leave_active_next <= '0';
            else
                if active = '0' then
                    -- go into active state when there is new data available on the FIFO
                    if linkFifo_r_empty = '0' then
                        active <= '1';
                    end if;
                else
                    -- move cycle ahead
                    if tready = '1' then
                        -- leave active state when there is no new data to read from the FIFO when we would need it
                        if (linkFifo_r_empty = '1') and (linkFifo_r_wouldread = '1') then
                            if (DATA_WIDTH = 256) and (cycle = 2) and (linkFifo_bytevalid_r(27 downto 24) /= "0000") then
                                leave_active_next <= '1';
                            elsif (DATA_WIDTH = 512) and (cycle = 6) and (linkFifo_bytevalid_r(59 downto 56) /= "0000") then
                                leave_active_next <= '1';
                            elsif (DATA_WIDTH = 1024) and (cycle = 14) and (linkFifo_bytevalid_r(123 downto 120) /= "0000") then
                                leave_active_next <= '1';
                            else
                                active <= '0';
                            end if;
                        end if;
                        if leave_active_next = '1' then
                            leave_active_next <= '0';
                            active <= '0';
                        end if;

                        -- save data for next step
                        if (DATA_WIDTH = 256) and (cycle = 2) then
                            buf <= linkFifo_dout_rev_r(223 downto 192);
                            buf_bytevalid <= linkFifo_bytevalid_r(27 downto 24);
                            buf_lastbyte <= linkFifo_lastbyte_r(27 downto 24);
                        elsif (DATA_WIDTH = 512) and (cycle = 6) then
                            buf <= linkFifo_dout_rev_r(479 downto 448);
                            buf_bytevalid <= linkFifo_bytevalid_r(59 downto 56);
                            buf_lastbyte <= linkFifo_lastbyte_r(59 downto 56);
                        elsif (DATA_WIDTH = 1024) and (cycle = 14) then
                            buf <= linkFifo_dout_rev_r(991 downto 960);
                            buf_bytevalid <= linkFifo_bytevalid_r(123 downto 120);
                            buf_lastbyte <= linkFifo_lastbyte_r(123 downto 120);
                        end if;

                        -- save mlen for next step
                        if ((cycle = 3) and (DATA_WIDTH = 256)) or ((cycle = 7) and (DATA_WIDTH = 512)) or ((cycle = 15) and (DATA_WIDTH = 1024)) then
                            ch_prev <= ch;
                        end if;

                        if tlast = '1' then
                            -- if it is the last word in the transmission start again with cycle 0
                            cycle <= 0;
                        else
                            -- otherwise just count up with wrap around after 6/14/30
                            if ((DATA_WIDTH = 256) and (cycle = 6)) or ((DATA_WIDTH = 512) and (cycle = 14)) or ((DATA_WIDTH = 1024) and (cycle = 30)) then
                                cycle <= 0;
                            else
                                cycle <= cycle + 1;
                            end if;
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process;

end architecture;

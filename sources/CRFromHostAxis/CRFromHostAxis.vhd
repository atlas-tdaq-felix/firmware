--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use IEEE.MATH_REAL.CEIL;

    use work.axi_stream_package.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;
library xpm;
    use xpm.vcomponents.all;

entity CRFromHostAxis is
    generic (
        LINK_NUM                        : integer range 1 to 32 := 1;
        LINK_CONFIG                     : IntArray;
        STREAMS_PER_LINK_FROMHOST       : integer range 1 to 64 := 1;
        NUM_TRANSFER_MANAGERS           : integer := 2;
        --GROUP_CONFIG                    : IntArray(0 to MAX_GROUPS_PER_STREAM_FROMHOST-1) := (0 => 1, others => 0);
        DATA_WIDTH                      : integer := 256
    --SUPPORT_DELAY                   : boolean := false                -- boolean to support delay packet generation (FLX-1862)
    );
    port (
        -- reset
        daq_reset            : in std_logic;
        daq_fifo_flush       : in std_logic;

        -- FromHost FIFO interface
        fromHostFifo_clk     : in  std_logic;
        fromHostFifo_dout    : in  std_logic_vector(DATA_WIDTH-1 downto 0);
        fromHostFifo_rd_en   : out std_logic;
        fromHostFifo_empty   : in  std_logic;
        fromHostFifo_rst     : out std_logic;

        -- AXI streaming
        -- fhAxis_aclk          : in  std_logic;
        fhAxis               : out axis_8_2d_array_type(0 to LINK_NUM-1, 0 to STREAMS_PER_LINK_FROMHOST-1);
        fhAxis_tready        : in  axis_tready_2d_array_type(0 to LINK_NUM-1, 0 to STREAMS_PER_LINK_FROMHOST-1);
        -- fhAxis64_aclk        : in  std_logic;
        fhAxis64             : out axis_64_array_type(0 to LINK_NUM-1);
        fhAxis64_tready      : in  axis_tready_array_type(0 to LINK_NUM-1);

        -- configuration & status (should be synchronous to fromHostFifo_clk!)
        fifo_monitoring      : out bitfield_crfromhost_fifo_status_r_type;
        register_map_control : in  register_map_control_type
    );
end CRFromHostAxis;

architecture rtl of CRFromHostAxis is
    signal daq_reset_sync : std_logic;
    signal daq_fifo_flush_sync : std_logic;

    signal linkManager_ready : std_logic_vector(LINK_NUM-1 downto 0);
    signal linkManager_valid : std_logic_vector(LINK_NUM-1 downto 0);
    signal linkTarget : integer range 0 to 255;

    --signal packet_empty : std_logic;
    signal CLEAR_LATCH_sync : std_logic;
begin
    -- check data width (can be only 256, 512 or 1024)
    assert ((DATA_WIDTH = 256) or (DATA_WIDTH = 512) or (DATA_WIDTH = 1024))
        report "DATA_WIDTH can be either 256, 512 or 1024"
        severity failure;

    -- check length of LINK_CONFIG array
    assert LINK_CONFIG'length = LINK_NUM
        report "Every link must have a corresponding LINK_CONFIG, please check the LINK_CONFIG array length!"
        severity failure;

    xpm_sync_daq_reset: xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 2,
            INIT => 0,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0
        )
        port map (
            src_rst => daq_reset,
            dest_clk => fromHostFifo_clk,
            dest_rst => daq_reset_sync
        );

    xpm_sync_daq_fifo_flush: xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 2,
            INIT => 0,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0
        )
        port map (
            src_rst => daq_fifo_flush,
            dest_clk => fromHostFifo_clk,
            dest_rst => daq_fifo_flush_sync
        );

    -- flush also PCIe FIFO
    fromHostFifo_rst <= daq_fifo_flush_sync;
    fromHostFifo_rd_en <= or(linkManager_valid);

    linkTarget <= to_integer(unsigned(fromHostFifo_dout(DATA_WIDTH-32+23 downto DATA_WIDTH-32+16)));

    process (linkTarget, linkManager_ready, fromHostFifo_empty) begin
        if fromHostFifo_empty = '1' then
            linkManager_valid <= (others => '0');
        else
            if linkTarget = 255 then
                if linkManager_ready = (linkManager_ready'range => '1') then
                    linkManager_valid <= (others => '1');
                else
                    linkManager_valid <= (others => '0');
                end if;
            else
                if linkManager_ready(linkTarget) = '1' then
                    linkManager_valid <= (others => '0');
                    linkManager_valid(linkTarget) <= '1';
                else
                    linkManager_valid <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    -- generate all links
    links: for LINK in 0 to LINK_NUM-1 generate
        signal channelFhAxis : axis_8_array_type(0 to STREAMS_PER_LINK_FROMHOST-1);
        signal channelFhAxis_tready : axis_tready_array_type(0 to STREAMS_PER_LINK_FROMHOST-1);
    begin
        -- check proper link configuration
        assert ((LINK_CONFIG(LINK) = 0) or (LINK_CONFIG(LINK) = 1))
            report "Invalid LINK_CONFIG for link " & integer'image(LINK)
            severity failure;

        dataManager_gbt: if LINK_CONFIG(LINK) = 0 generate
            -- use AXIS-64 clock
            signal BROADCAST_ENABLE_fromHostFifo_clk: std_logic_vector(STREAMS_PER_LINK_FROMHOST-1 downto 0);
            signal fhAxis32 : axis_32_array_type(0 to NUM_TRANSFER_MANAGERS-1);
            signal fhAxis32_tready : std_logic_vector(0 to NUM_TRANSFER_MANAGERS-1);
        begin
            chAssign: for STREAM in 0 to STREAMS_PER_LINK_FROMHOST-1 generate
                fhAxis(LINK, STREAM) <= channelFhAxis(STREAM);
                channelFhAxis_tready(STREAM) <= fhAxis_tready(LINK, STREAM);
            end generate;

            xpm_cdc_array_single_inst_BROADCAST_ENABLE : xpm_cdc_array_single
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0,
                    WIDTH => STREAMS_PER_LINK_FROMHOST
                )
                port map (
                    src_clk => '0',
                    src_in => register_map_control.BROADCAST_ENABLE(LINK)(STREAMS_PER_LINK_FROMHOST-1 downto 0),
                    dest_clk => fromHostFifo_clk,
                    dest_out => BROADCAST_ENABLE_fromHostFifo_clk
                );

            linkManager_I: entity work.CRFromHostLinkManager
                generic map (
                    DATA_WIDTH => DATA_WIDTH,
                    NUM_TRANSFER_MANAGERS => NUM_TRANSFER_MANAGERS,
                    STREAMS_PER_LINK_FROMHOST => STREAMS_PER_LINK_FROMHOST
                )
                port map (
                    clk => fromHostFifo_clk,
                    daq_reset => daq_reset_sync,
                    daq_fifo_flush => daq_fifo_flush_sync,
                    linkManager_data => fromHostFifo_dout,
                    linkManager_valid => linkManager_valid(LINK),
                    linkManager_ready => linkManager_ready(LINK),
                    fhAxis32 => fhAxis32,
                    fhAxis32_tready => fhAxis32_tready
                );

            transfer_managers: for TM in 0 to NUM_TRANSFER_MANAGERS-1 generate
                constant CH_LOW : integer := integer(ceil(real(TM) * real(STREAMS_PER_LINK_FROMHOST) / real(NUM_TRANSFER_MANAGERS)));
                constant CH_HIGH : integer := integer(ceil(real(TM+1) * real(STREAMS_PER_LINK_FROMHOST) / real(NUM_TRANSFER_MANAGERS)))-1;
                constant CH_NUM : integer := CH_HIGH - CH_LOW + 1;
            begin
                transferManager_I: entity work.CRFromHostTransferManager
                    generic map (
                        NUM_STREAMS => CH_NUM,
                        CH_OFFSET => CH_LOW
                    )
                    port map (
                        clk => fromHostFifo_clk,
                        daq_reset => daq_reset_sync,
                        daq_fifo_flush => daq_fifo_flush_sync,
                        fhAxis32 => fhAxis32(TM),
                        fhAxis32_tready => fhAxis32_tready(TM),
                        stream_out => channelFhAxis(CH_LOW to CH_HIGH),
                        stream_out_tready => channelFhAxis_tready(CH_LOW to CH_HIGH),
                        broadcastEnable => BROADCAST_ENABLE_fromHostFifo_clk(CH_HIGH downto CH_LOW)
                    );
            end generate;

            -- for GBT links we don't use the fhAxis64 port
            fhAxis64(LINK).tdata <= (others => '0');
            fhAxis64(LINK).tkeep <= (others => '0');
            fhAxis64(LINK).tvalid <= '0';
            fhAxis64(LINK).tlast <= '0';
            fhAxis64(LINK).tid <= (others => '0');
            fhAxis64(LINK).tuser <= (others => '0');
        end generate;

        dataManager_25g: if LINK_CONFIG(LINK) = 1 generate
            -- we don't use the fhAxis port for 25G links
            chAssign: for STREAM in 0 to STREAMS_PER_LINK_FROMHOST-1 generate
                fhAxis(LINK, STREAM).tdata <= (others => '0');
                fhAxis(LINK, STREAM).tvalid <= '0';
                fhAxis(LINK, STREAM).tlast <= '0';
            end generate;

            dataManager_I: entity work.CRFromHostDataManagerAxis64
                generic map (
                    DATA_WIDTH => DATA_WIDTH
                )
                port map (
                    clk => fromHostFifo_clk,
                    daq_reset => daq_reset_sync,
                    daq_fifo_flush => daq_fifo_flush_sync,
                    linkManager_data => fromHostFifo_dout,
                    linkManager_valid => linkManager_valid(LINK),
                    linkManager_ready => linkManager_ready(LINK),
                    fhAxis => fhAxis64(LINK),
                    fhAxis_tready => fhAxis64_tready(LINK)
                );
        end generate;
    end generate;
    sync_CLEAR_LATCH : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map (
            src_clk => '0',
            src_in => register_map_control.CRFROMHOST_FIFO_STATUS.CLEAR(64),
            dest_clk => fromHostFifo_clk,
            dest_out => CLEAR_LATCH_sync
        );

    -- latching FIFO full status
    process (fromHostFifo_clk) begin
        if rising_edge(fromHostFifo_clk) then
            if daq_reset_sync = '1' then
                fifo_monitoring.FULL_LATCHED <= (others => '0');
            else
                -- clear latch flags
                if CLEAR_LATCH_sync = '1' then
                    fifo_monitoring.FULL_LATCHED <= (others => '0');
                end if;

                for I in 0 to LINK_NUM-1 loop
                    if linkManager_ready(I) = '0' then          -- should we use pfull here???
                        fifo_monitoring.FULL_LATCHED(I) <= '1';
                    end if;
                end loop;
            end if;
        end if;
    end process;

    -- fifo full current status
    fifo_full_status: for I in 0 to 23 generate
        no_link_present: if I >= LINK_NUM generate
            fifo_monitoring.FULL(24+I) <= '0';
        end generate;

        link_present: if I < LINK_NUM generate
            fifo_monitoring.FULL(24+I) <= not linkManager_ready(I);      -- should we use pfull here???
        end generate;
    end generate;


end architecture;

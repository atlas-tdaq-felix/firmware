--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               RHabraken
--!               Mesfin Gebyehu
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.
--! Engineer: juna, fschreud
--!
--! Create Date:    09/10/2016
--! Module Name:    FMchannelTXctrl
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library ieee;
    use ieee.std_logic_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.std_logic_unsigned.all; -- @suppress "Deprecated package"
--use work.centralRouter_package.all;

--! Full Mode channel transmitter controller
entity FMchannelTXctrl is
    port (
        clk240      : in  std_logic;
        rst         : in  std_logic;
        --
        busy        : in  std_logic;
        fifo_dout   : in  std_logic_vector(31 downto 0);
        fifo_dtype  : in  std_logic_vector(2 downto 0);
        --
        dout        : out std_logic_vector(31 downto 0); -- data out @ clk240
        kout        : out std_logic_vector(3 downto 0)
    );
end FMchannelTXctrl;


architecture Behavioral of FMchannelTXctrl is

    constant comma32word    : std_logic_vector (31 downto 0) := "00000000000000000000000010111100"; -- K28.5 bc
    constant eop32word      : std_logic_vector (31 downto 0) := "00000000000000000000000011011100"; -- K28.6 dc
    constant sop32word      : std_logic_vector (31 downto 0) := "00000000000000000000000000111100"; -- K28.1 3c

    constant sob32word      : std_logic_vector (31 downto 0) := "00000000000000000000000001011100"; -- K28.2 5c
    constant eob32word      : std_logic_vector (31 downto 0) := "00000000000000000000000001111100"; -- K28.3 7c

    signal sop_detect,eop_detect,err_detect : std_logic;
    signal sob_detect,eob_detect : std_logic;

    signal crc_calc, crc_start, crc_add, crc_add_r1 : std_logic;
    signal crc_din, crc_din_r1: std_logic_vector(31 downto 0);
    signal crc_kin, crc_kin_r1 : std_logic_vector(3 downto 0);
    signal crc_out : std_logic_vector(19 downto 0);
    signal tx_state : integer :=0;
--signal fifo_dout_r1        : std_logic_vector(31 downto 0);

--signal fifo_dtype_r1        : std_logic_vector(2 downto 0);

--signal err_detect_r1        : std_logic;
--signal eop_detect_r1        : std_logic;
--signal eop_detect_r2        : std_logic;

--signal sop_detect_r1        : std_logic;
--signal sop_detect_r2        : std_logic;

--signal sob_detect_r1        : std_logic;
--signal eob_detect_r1        : std_logic;
--signal sob_detect_r2        : std_logic;
--signal eob_detect_r2        : std_logic;


--signal dout_r1        : std_logic_vector(31 downto 0);
--signal kout_r1        : std_logic_vector(3 downto 0);

begin

    ---------------------------------------------------------------------------------------
    -- data types
    ---------------------------------------------------------------------------------------
    sop_detect <= '1' when (fifo_dtype = "001") else '0'; -- start-of-packet
    eop_detect <= '1' when (fifo_dtype = "010") else '0'; -- end-of-packet
    err_detect <= '1' when (fifo_dtype = "011") else '0'; -- data to be ignored
    sob_detect <= '1' when (fifo_dtype = "100") else '0'; -- start-of-busy
    eob_detect <= '1' when (fifo_dtype = "101") else '0'; -- end-of-busy

    ---------------------------------------------------------------------------------------
    -- reading from user's fifo
    ---------------------------------------------------------------------------------------

    --tx_state_output: process(clk240, rst)
    --begin
    --if (rst = '1') then
    --fifo_dout_r1 <=  (others => '0');
    --fifo_dtype_r1 <= (others => '0');
    --err_detect_r1 <= '0';
    --eop_detect_r1 <= '0';
    --sop_detect_r1 <= '0';
    --eop_detect_r2 <= '0';
    --sop_detect_r2 <= '0';
    --sob_detect_r1 <= '0';
    --eob_detect_r1 <= '0';
    --sob_detect_r2 <= '0';
    --eob_detect_r2 <= '0';
    --elsif rising_edge(clk240) then
    --fifo_dout_r1 <= fifo_dout;
    --fifo_dtype_r1 <= fifo_dtype;
    --err_detect_r1 <= err_detect;
    --eop_detect_r1 <= eop_detect;
    --sop_detect_r1 <= sop_detect;
    --eop_detect_r2 <= eop_detect_r1;
    --sop_detect_r2 <= sop_detect_r1;
    --sob_detect_r1 <= sob_detect;
    --eob_detect_r1 <= eob_detect;
    --sob_detect_r2 <= sob_detect_r1;
    --eob_detect_r2 <= eob_detect_r1;
    --end if;
    --end process;

    tx_detect_output: process(clk240, rst)
    begin
        if (rst = '1') then
            crc_start <= '0';
            crc_add <= '0';
            crc_calc <= '0';
            crc_kin <= "0001";
            crc_din <= comma32word;
        elsif rising_edge(clk240) then
            --    crc_start <= '0';
            --    crc_add <= '0';
            --    crc_calc <= '0';
            case tx_state is
                when 0 =>
                    crc_kin <= "0001";
                    crc_calc <= '0';
                    crc_add <= '0';
                    if(sop_detect = '1') then -- start-of-packet
                        crc_start <= '1';
                        crc_din <= sop32word;
                        tx_state <= tx_state + 1;
                    elsif(sob_detect = '1') then
                        crc_start <= '0';
                        crc_din <= sob32word;
                    elsif(eob_detect = '1') then
                        crc_start <= '0';
                        crc_din <= eob32word;
                    else
                        crc_start <= '0';
                        crc_din <= comma32word;
                    end if;
                when 1 =>
                    crc_start <= '0';
                    if(sob_detect = '1') then
                        crc_calc <= '0';
                        crc_kin <= "0001";
                        crc_din <= sob32word;
                        crc_add <= '0';
                    elsif(eob_detect = '1') then
                        crc_calc <= '0';
                        crc_kin <= "0001";
                        crc_din <= eob32word;
                        crc_add <= '0';
                    elsif(eop_detect = '1') then -- end-of-packet delay 2clk (sop/eop insertion)
                        crc_calc <= '0';
                        crc_add <= '1';
                        crc_din <= eop32word;
                        crc_kin <= "0001";
                        tx_state <= 0;
                    elsif(err_detect = '1') then
                        crc_add <= '0';
                        crc_calc <= '0';
                        crc_din <= comma32word;
                        crc_kin <= "0001";
                    else
                        crc_add <= '0';
                        crc_calc <= '1';
                        crc_din <= fifo_dout;
                        crc_kin <= "0000";
                    end if;
                when others => NULL;
            end case;
        end if;
    end process;


    crc20_0: entity work.CRC
        --generic map(
        --Nbits     => 32--,
        --CRC_Width => 20,
        --G_Poly    => x"8359f", --x"c1acf",
        --G_InitVal => x"fffff"
        --  )
        port map(
            CRC   => crc_out,
            Calc  => crc_calc,
            Clk   => clk240,
            Din   => crc_din,
            Reset => crc_start);

    --Pipe crc_din and crc_kin one clock, then add CRC to EOP and drive output ports dout and kout
    crc_to_output: process(clk240)
    begin
        if rising_edge(clk240) then
            --pipeline
            crc_add_r1 <= crc_add;
            crc_din_r1 <= crc_din;
            crc_kin_r1 <= crc_kin;

            if(crc_add_r1 = '1') then
                dout <= "000" & (busy) & crc_out & eop32word(7 downto 0);
                kout <= "0001";
            else
                kout <= crc_kin_r1;
                dout <= crc_din_r1;
            end if;
        end if;
    end process;

end Behavioral;


--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Kai Chen
--!               mtrovato
--!               Elena Zhivun
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen      (kchen@bnl.gov)
--!              Marco Trovato (mtrovato@anl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2019/05/21 04:43:14 PM
-- Design Name: LpGBT BE Top
-- Module Name: LpGBT BE Top - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions: Vivado
-- Description:
--              The LpGBT Back-End Top
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------



LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.numeric_std.ALL;
    use IEEE.numeric_std_unsigned.ALL;

library XPM;
    use xpm.vcomponents.all;



    use work.lpgbtfpga_package.all;


entity FLX_LpGBT_BE is
    Port
    (
        downlinkUserData_i                      : in std_logic_vector(31 downto 0);
        downlinkEcData_i                        : in std_logic_vector(1 downto 0);
        downlinkIcData_i                        : in std_logic_vector(1 downto 0);
        TXCLK40                                 : in std_logic;
        --RXCLK40                                 : in std_logic;
        TXCLK320                                : in std_logic;
        RXCLK320m                               : in std_logic;
        uplinkSelectFEC_i                       : in std_logic;
        data_rdy                                : out std_logic;
        Tx_scrambler_bypass                     : in std_logic;
        Tx_Interleaver_bypass                   : in std_logic;
        --phase_sel                               : in std_logic_vector(2 downto 0);
        Tx_FEC_bypass                           : in std_logic;
        TxData_Out                              : out std_logic_vector(15 downto 0);
        rxdatain                                : in std_logic_vector(31 downto 0);
        GBT_TX_RST                              : in std_logic;
        GBT_RX_RST                              : in std_logic;
        --rst_patternsearch                       : in std_logic;
        --        uplinkSelectDataRate_i                  : in std_logic; --MT now hard coded to 1024Gbps -- EZ add a cdc synchronizer if uncommented
        uplinkBypassInterleaver_i               : in std_logic;
        uplinkBypassFECEncoder_i                : in std_logic;
        uplinkBypassScrambler_i                 : in std_logic;
        uplinkMulticycleDelay_i                 : in std_logic_vector(2 downto 0);


        sta_headerFecLocked_o                     : out std_logic;
        ctr_clkSlip_o                           : out std_logic;

        uplinkReady_o                           : out std_logic;
        uplinkUserData_o                        : out std_logic_vector(229 downto 0);
        uplinkEcData_o                          : out std_logic_vector(1 downto 0);
        uplinkIcData_o                          : out std_logic_vector(1 downto 0);
        fec_error_o                             : out std_logic;
        fec_err_cnt_o                           : out std_logic_vector(31 downto 0);
        TX_aligned_i                            : in std_logic
    );
end FLX_LpGBT_BE;

architecture Behavioral of FLX_LpGBT_BE is


    signal TxData_scrambled                 : std_logic_vector(35 downto 0);
    signal TxData_FEC                       : std_logic_vector(23 downto 0);
    signal cnt                              : std_logic_vector(2 downto 0);
    signal TxData_Interleaved               : std_logic_vector(63 downto 0);
    signal TxData_Interleaved_inv           : std_logic_vector(63 downto 0);
    signal TxData_Interleaved_latched       : std_logic_vector(63 downto 0);
    --signal ctr_clkSlip_5g12_s               : std_logic;
    --signal ctr_clkSlip_10g24_s              : std_logic;
    --signal sta_headeLocked_10g24_s          : std_logic;
    --signal sta_headeLocked_5g12_s           : std_logic;
    --signal sta_headerFlag_5g12_s            : std_logic;
    --signal sta_headerFlag_10g24_s           : std_logic;
    --signal clk_dataFlag_rxgearbox_5g12_s    : std_logic;
    --signal rst_rxgearbox_10g24_s            : std_logic;
    --signal rst_rxgearbox_5g12_s             : std_logic;
    --signal clk_dataFlag_rxgearbox_10g24_s   : std_logic;
    --signal uplinkFrame_from_rxgb_s          : std_logic_vector(255 downto 0);
    --signal uplinkFrame_from_5g12_rxgb_s     : std_logic_vector(255 downto 0);
    --signal uplinkFrame_from_10g24_rxgb_s    : std_logic_vector(255 downto 0);
    --signal dat_upLinkWord_fromMgt_s256_1024 : std_logic_vector(255 downto 0);
    --signal dat_upLinkWord_fromGb_s1024_buf  : std_logic_vector(255 downto 0);
    --signal sta_rxGbRdy_5g12_s               : std_logic;
    --signal sta_rxGbRdy_10g24_s              : std_logic;
    --signal clk_dataFlag_rxgearbox_s         : std_logic;
    --signal uplinkUserData_s                 : std_logic_vector(229 downto 0);
    --signal uplinkEcData_s                   : std_logic_vector(1 downto 0);
    --signal uplinkIcData_s                   : std_logic_vector(1 downto 0);
    --signal uplinkReady_s                    : std_logic;
    signal TXCLK40_r                        : std_logic;
    --signal TXCLK40_rr                       : std_logic;
    signal data_rdy_or                      : std_logic;
    signal data_rdy_o                       : std_logic;
    signal downLinkData_s                   : std_logic_vector(35 downto 0);
    --signal TxData_Out_i                     : std_logic_vector(31 downto 0);
    signal dat_upLinkWord_fromMgt_s32_1024  : std_logic_vector(31 downto 0);
    --signal dat_upLinkWord_fromMgt_s128_512  : std_logic_vector(127 downto 0);
    --signal dat_upLinkWord_fromGb_s512_buf   : std_logic_vector(127 downto 0);
    --signal count_rx                         : std_logic_vector(2 downto 0);
    --signal count_rx2                        : std_logic_vector(2 downto 0);
    signal TxData_Out_8                     : std_logic_vector(7 downto 0);
    --signal dat_upLinkWord_fromMgt_s16_512   : std_logic_vector(15 downto 0);
    --signal sta_headerFlag_5g12_s_vec        : std_logic_vector(9 downto 0);
    --signal sta_headerFlag_10g24_s_vec       : std_logic_vector(9 downto 0);
    --signal sta_headerFlag_s512              : std_logic;
    --signal RXCLK40_r                        : std_logic;
    --signal sel                              : std_logic;
    --signal sel2                             : std_logic;
    --signal sta_headerFlag_s512_r1           : std_logic;
    --signal sta_headerFlag_s1024             : std_logic;
    --signal sta_headerFlag_s512_r2           : std_logic;
    --signal sta_headerFlag_s512_r3           : std_logic;
    --signal sta_headerFlag_s512_r4           : std_logic;
    --signal sta_headerFlag_s1024_r4          : std_logic;
    --signal sta_headerFlag_s1024_r3          : std_logic;
    --signal sta_headerFlag_s1024_r2          : std_logic;
    --signal sta_headerFlag_s1024_r1          : std_logic;

    signal uplinkUserData_fec12_s           : std_logic_vector(229 downto 0);
    signal uplinkEcData_fec12_s             : std_logic_vector(1 downto 0);
    signal uplinkIcData_fec12_s             : std_logic_vector(1 downto 0);
    signal uplinkUserData_fec5_s            : std_logic_vector(229 downto 0);
    signal uplinkEcData_fec5_s              : std_logic_vector(1 downto 0);
    signal uplinkIcData_fec5_s              : std_logic_vector(1 downto 0);
    signal fec12_error_s                    : std_logic;
    signal fec5_error_s                     : std_logic;
    signal fec12_error_cnt_s, fec5_error_cnt_s : std_logic_vector(31 downto 0);


    signal GBT_RX_RST_N : std_logic; --inverted reset

    -- control register synchronized to RXCLK320m
    signal uplinkSelectFEC_sync            : std_logic := '0';
    signal uplinkBypassInterleaver_sync    : std_logic := '0';
    signal uplinkBypassFECEncoder_sync     : std_logic := '0';
    signal uplinkBypassScrambler_sync      : std_logic := '0';
    --signal uplinkSelectDataRate_sync       : std_logic;

    -- control register updated only when header is aligned
    signal uplinkSelectFEC_multicycle            : std_logic := '0';
    signal uplinkBypassInterleaver_multicycle    : std_logic := '0';
    signal uplinkBypassFECEncoder_multicycle     : std_logic := '0';
    signal uplinkBypassScrambler_multicycle      : std_logic := '0';
    --signal uplinkSelectDataRate_multicycle       : std_logic;

    signal sta_headerFlag_fec12_s                 : std_logic;
    signal sta_headerFlag_fec5_s                  : std_logic;
    signal TXCLK40_320: std_logic; --CDC synchronized version of TXCLK40 in 8 320MHz clocks
    signal GBT_TX_RST_40: std_logic; --Some parts of the scrambler use this reset in 40 MHz domain.
    signal sta_headerFecLocked_fec12_s : std_logic;
    signal sta_headerFecLocked_fec5_s : std_logic;
    signal data_rdy_fec12_s : std_logic;
    signal data_rdy_fec5_s : std_logic;
    signal uplinkReady_fec12_s : std_logic;
    signal uplinkReady_fec5_s : std_logic;
    signal ctr_clkSlip_fec12_s : std_logic;
    signal ctr_clkSlip_fec5_s : std_logic;

begin


    -- downlink
    downLinkData_s(31 downto 0)    <= downlinkUserData_i;
    downLinkData_s(33 downto 32)   <= downlinkEcData_i;
    downLinkData_s(35 downto 34)   <= downlinkIcData_i;

    TxData_Out  <=  TxData_Out_8(7) & TxData_Out_8(7) &
                   TxData_Out_8(6) & TxData_Out_8(6) &
                   TxData_Out_8(5) & TxData_Out_8(5) &
                   TxData_Out_8(4) & TxData_Out_8(4) &
                   TxData_Out_8(3) & TxData_Out_8(3) &
                   TxData_Out_8(2) & TxData_Out_8(2) &
                   TxData_Out_8(1) & TxData_Out_8(1) &
                   TxData_Out_8(0) & TxData_Out_8(0);

    RX_RESET_sync_inst : xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 2,
            INIT => 1,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0
        )
        port map (
            src_rst => GBT_TX_RST,
            dest_clk => TXCLK40,
            dest_rst => GBT_TX_RST_40
        );

    lpgbtfpga_scrambler_inst: entity work.lpgbtfpga_scrambler
        generic map(
            INIT_SEED => x"1fba847af"
        )
        port map
        (
            clk_i                => TXCLK40,
            clkEn_i              => '1',
            reset_i              => GBT_TX_RST_40,
            data_i               => downLinkData_s ,
            data_o               => TxData_scrambled,
            bypass               => Tx_scrambler_bypass
        );

    lpgbtfpga_encoder_inst: entity work.lpgbtfpga_encoder
        port map
        (
            data_i               => TxData_scrambled,
            FEC_o                => TxData_FEC,
            bypass               => Tx_FEC_bypass
        );

    lpgbtfpga_interleaver_inst: entity work.lpgbtfpga_interleaver
        generic map
        (
            HEADER_c             => "1001"
        )
        port map
        (
            data_i               => TxData_scrambled,
            FEC_i                => TxData_FEC,
            data_o               => TxData_Interleaved,
            bypass               => Tx_Interleaver_bypass
        );

    txinv : for i in 64-1 downto 0 generate
        TxData_Interleaved_inv(i) <= TxData_Interleaved(63-i);
    end generate;

    xpm_cdc_single_inst : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 8, --equivalent to 1 40 MHz clock cycle at 320 MHz
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0 --Cannot sync a clock to itself
        )
        port map (
            src_clk => '0',
            src_in => TXCLK40,
            dest_clk => TXCLK320,
            dest_out => TXCLK40_320
        );
    process(TXCLK320)
    begin
        if TXCLK320'event and TXCLK320='1' then
            TXCLK40_r <= TXCLK40_320; --comment if using xpmcdc
            if TX_aligned_i= '1' THEN
                --if TXCLK40_r='0' and TXCLK40_rr='1' then--MT Nov 10
                if TXCLK40_320='0' and TXCLK40_r='1' then
                    cnt <="000";
                else
                    cnt <=cnt+ '1';
                end if;
            else
                cnt <=cnt+ '1';
            end if;

            if cnt="010" then
                TxData_Interleaved_latched <= TxData_Interleaved_inv;
            end if;

            case cnt is
                when "011" =>
                    TxData_Out_8 <= TxData_Interleaved_latched(7 downto 0) ;
                when "100" =>
                    TxData_Out_8 <= TxData_Interleaved_latched(15 downto 8) ;
                when "101" =>
                    TxData_Out_8 <= TxData_Interleaved_latched(23 downto 16) ;
                when "110" =>
                    TxData_Out_8 <= TxData_Interleaved_latched(31 downto 24) ;
                when "111" =>
                    TxData_Out_8 <= TxData_Interleaved_latched(39 downto 32) ;
                when "000" =>
                    TxData_Out_8 <= TxData_Interleaved_latched(47 downto 40) ;
                when "001" =>
                    TxData_Out_8 <= TxData_Interleaved_latched(55 downto 48) ;
                when others =>
                    TxData_Out_8 <= TxData_Interleaved_latched(63 downto 56) ;
            end case;
        end if;
    end process;


    --------------------------------------------------------------------------------------
    -- Add replicated cdc synchronizers for these decoder controls to ease the timing -- E.Z.
    -- no src_clk => no setup constraint => each synchronizer can be placed close to own decoder

    -- uplinkSelectFEC_i
    -- uplinkBypassInterleaver_i
    -- uplinkBypassFECEncoder_i
    -- uplinkBypassScrambler_i
    -- uplinkSelectDataRate_i

    xpm_cdc_uplinkSelectFEC : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 2, -- DECIMAL; range: 2-10
            INIT_SYNC_FF => 0, -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG => 0 -- DECIMAL; 0=do not register input, 1=register input
        )
        port map (
            src_clk => '0', -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in => uplinkSelectFEC_i, -- 1-bit input: Input signal to be synchronized to dest_clk domain.
            dest_clk => RXCLK320m, -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => uplinkSelectFEC_sync -- 1-bit output: src_in synchronized to the destination clock domain. This output is registered.
        );

    xpm_cdc_uplinkBypassInterleaver : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 2, -- DECIMAL; range: 2-10
            INIT_SYNC_FF => 0, -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG => 0 -- DECIMAL; 0=do not register input, 1=register input
        )
        port map (
            src_clk => '0', -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in => uplinkBypassInterleaver_i, -- 1-bit input: Input signal to be synchronized to dest_clk domain.
            dest_clk => RXCLK320m, -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => uplinkBypassInterleaver_sync -- 1-bit output: src_in synchronized to the destination clock domain. This output is registered.
        );

    xpm_cdc_uplinkBypassFECEncoder : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 2, -- DECIMAL; range: 2-10
            INIT_SYNC_FF => 0, -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG => 0 -- DECIMAL; 0=do not register input, 1=register input
        )
        port map (
            src_clk => '0', -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in => uplinkBypassFECEncoder_i, -- 1-bit input: Input signal to be synchronized to dest_clk domain.
            dest_clk => RXCLK320m, -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => uplinkBypassFECEncoder_sync -- 1-bit output: src_in synchronized to the destination clock domain. This output is registered.
        );

    xpm_cdc_uplinkBypassScrambler : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 2, -- DECIMAL; range: 2-10
            INIT_SYNC_FF => 0, -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG => 0 -- DECIMAL; 0=do not register input, 1=register input
        )
        port map (
            src_clk => '0', -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in => uplinkBypassScrambler_i, -- 1-bit input: Input signal to be synchronized to dest_clk domain.
            dest_clk => RXCLK320m, -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => uplinkBypassScrambler_sync -- 1-bit output: src_in synchronized to the destination clock domain. This output is registered.
        );

    --xpm_cdc_uplinkSelectDataRate : xpm_cdc_single
    --generic map (
    --    DEST_SYNC_FF => 2, -- DECIMAL; range: 2-10
    --    INIT_SYNC_FF => 0, -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
    --    SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
    --    SRC_INPUT_REG => 0 -- DECIMAL; 0=do not register input, 1=register input
    --)
    --port map (
    --    dest_out => uplinkSelectDataRate_sync, -- 1-bit output: src_in synchronized to the destination clock domain. This output is registered.
    --    dest_clk => RXCLK320m, -- 1-bit input: Clock signal for the destination clock domain.
    --    src_clk => '0', -- 1-bit input: optional; required when SRC_INPUT_REG = 1
    --    src_in => uplinkSelectDataRate_i  -- 1-bit input: Input signal to be synchronized to dest_clk domain.
    --);

    multicycle_ctrl_update : process(RXCLK320m) is
    begin
        if rising_edge(RXCLK320m) then
            if (sta_headerFlag_fec5_s = '1' and uplinkSelectFEC_sync = '0') or (sta_headerFlag_fec12_s = '1' and uplinkSelectFEC_sync = '1') then -- allow controls update only once per frame
                uplinkSelectFEC_multicycle <= uplinkSelectFEC_sync;
                uplinkBypassScrambler_multicycle <= uplinkBypassScrambler_sync;
                uplinkBypassInterleaver_multicycle <= uplinkBypassInterleaver_sync;
                uplinkBypassFECEncoder_multicycle <= uplinkBypassFECEncoder_sync;
                uplinkBypassScrambler_multicycle <= uplinkBypassScrambler_sync;
            --uplinkSelectDataRate_multicycle <= uplinkSelectDataRate_multicycle;
            end if;
        end if;
    end process;


    --------------------------------------------------------------------------------------

    -- uplink
    --  dat_upLinkWord_fromMgt_s16_512 <=   rxdatain(31) & rxdatain(29) & rxdatain(27) & rxdatain(25) & rxdatain(23) & rxdatain(21) & rxdatain(19) & rxdatain(17) &
    --                                        rxdatain(15) & rxdatain(13) & rxdatain(11) &-- rxdatain(9) & rxdatain(7) & rxdatain(5) & rxdatain(3) & rxdatain(1);

    dat_upLinkWord_fromMgt_s32_1024 <=  rxdatain(31 downto 0);

    process(RXCLK320m)
    begin
        if RXCLK320m'event and RXCLK320m='1' then
            data_rdy_or <= data_rdy_o;
            data_rdy    <= data_rdy_or;
        end if;
    end process;


    GBT_RX_RST_N <= not GBT_RX_RST;

    lpgbtfpga_uplink_fec12_inst: entity work.lpgbtfpga_uplink
        generic map
    (--convert to vector first
            DATARATE                        => DATARATE_10G24,
            FEC                             => FEC12,
            -- Expert parameters
            c_clockRatio                    => 8,
            c_mgtWordWidth                  => 32,
            c_allowedFalseHeader            => 38, --32,
            c_allowedFalseHeaderOverN       => 40,
            c_requiredTrueHeader            => 30,
            c_MaxFecErrors                  => 5,
            c_bitslip_mindly                => 1, --no wait
            c_bitslip_waitdly               => 40


        )
        port map
    (
            uplinkClk_i                     => RXCLK320m,
            clk40_i                         => TXCLK40,
            uplinkClkOutEn_o                => data_rdy_fec12_s,
            uplinkRst_n_i                   => GBT_RX_RST_N,

            mgt_word_i                      => dat_upLinkWord_fromMgt_s32_1024,
            -- Data
            userData_o                      => uplinkUserData_fec12_s,
            EcData_o                        => uplinkEcData_fec12_s,
            IcData_o                        => uplinkIcData_fec12_s,

            -- Control
            bypassInterleaver_i             => uplinkBypassInterleaver_multicycle,
            bypassFECEncoder_i              => uplinkBypassFECEncoder_multicycle,
            bypassScrambler_i               => uplinkBypassScrambler_multicycle,
            multicyleDelay_i                => uplinkMulticycleDelay_i,

            -- Transceiver control
            mgt_bitslipCtrl_o               => ctr_clkSlip_fec12_s,

            -- Status
            dataCorrected_o                 => open,
            IcCorrected_o                   => open,
            EcCorrected_o                   => open,
            fec_error_o                     => fec12_error_s,
            fec_err_cnt_o                   => fec12_error_cnt_s,
            rdy_o                           => uplinkReady_fec12_s,
            frameAlignerEven_o              => open,
            sta_headerFecLocked_out         => sta_headerFecLocked_fec12_s,
            sta_headerFlag_out              => sta_headerFlag_fec12_s
        );

    lpgbtfpga_uplink_fec5_inst: entity work.lpgbtfpga_uplink
        generic map
    (
            DATARATE                        => DATARATE_10G24,
            FEC                             => FEC5,
            -- Expert parameters
            --c_multicyleDelay                => 3,
            c_clockRatio                    => 8,
            c_mgtWordWidth                  => 32,
            c_allowedFalseHeader            => 32,
            c_allowedFalseHeaderOverN       => 40,
            c_requiredTrueHeader            => 30,
            c_MaxFecErrors                  => 5,
            c_bitslip_mindly                => 1, --no wait
            c_bitslip_waitdly               => 40


        )
        port map
    (
            uplinkClk_i                     => RXCLK320m,
            clk40_i                         => TXCLK40,
            uplinkClkOutEn_o                => data_rdy_fec5_s,
            uplinkRst_n_i                   => GBT_RX_RST_N,

            mgt_word_i                      => dat_upLinkWord_fromMgt_s32_1024,
            -- Data
            userData_o                      => uplinkUserData_fec5_s,
            EcData_o                        => uplinkEcData_fec5_s,
            IcData_o                        => uplinkIcData_fec5_s,

            -- Control
            bypassInterleaver_i             => uplinkBypassInterleaver_multicycle,
            bypassFECEncoder_i              => uplinkBypassFECEncoder_multicycle,
            bypassScrambler_i               => uplinkBypassScrambler_multicycle,
            multicyleDelay_i                => uplinkMulticycleDelay_i,

            -- Transceiver control
            mgt_bitslipCtrl_o               => ctr_clkSlip_fec5_s,

            -- Status
            dataCorrected_o                 => open,
            IcCorrected_o                   => open,
            EcCorrected_o                   => open,
            fec_error_o                     => fec5_error_s,
            fec_err_cnt_o                   => fec5_error_cnt_s,
            rdy_o                           => uplinkReady_fec5_s,
            frameAlignerEven_o              => open,
            sta_headerFecLocked_out         => sta_headerFecLocked_fec5_s,
            --sta_headerLocked_out            => open,
            sta_headerFlag_out              => sta_headerFlag_fec5_s
        );

    data_rdy_o          <= data_rdy_fec12_s       when uplinkSelectFEC_multicycle = '1' else
                           data_rdy_fec5_s;
    ctr_clkSlip_o       <= ctr_clkSlip_fec12_s       when uplinkSelectFEC_multicycle = '1' else
                           ctr_clkSlip_fec5_s;
    uplinkReady_o       <= uplinkReady_fec12_s       when uplinkSelectFEC_multicycle = '1' else
                           uplinkReady_fec5_s;
    uplinkUserData_o <= uplinkUserData_fec12_s when uplinkSelectFEC_multicycle = '1' else
                        uplinkUserData_fec5_s;
    uplinkEcData_o   <= uplinkEcData_fec12_s when uplinkSelectFEC_multicycle = '1' else
                        uplinkEcData_fec5_s;
    uplinkIcData_o   <= uplinkIcData_fec12_s when uplinkSelectFEC_multicycle = '1' else
                        uplinkIcData_fec5_s;
    fec_error_o      <= fec12_error_s when uplinkSelectFEC_multicycle = '1' else
                        fec5_error_s;
    fec_err_cnt_o    <= fec12_error_cnt_s when uplinkSelectFEC_multicycle = '1' else
                        fec5_error_cnt_s;

    sta_headerFecLocked_o               <= sta_headerFecLocked_fec12_s                when uplinkSelectFEC_multicycle = '1' else sta_headerFecLocked_fec5_s;


end Behavioral;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               dmatakia
--!               Kai Chen
--!               Elena Zhivun
--!               mtrovato
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen      (kchen@bnl.gov)
--! modified by: Marco Trovato (mtrovato@anl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2019/05/21 04:23:03 PM
-- Design Name: FLX_LpGBT_BE_Wrapper
-- Module Name: FLX_LpGBT_BE_Wrapper - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions: Vivado
-- Description:
--              The FELIX Back End Wrapper
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------

LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std_unsigned.all;

library UNISIM;
    use UNISIM.VComponents.all;
    use work.lpgbtfpga_package.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;

Library xpm;
    use xpm.vcomponents.all;

entity FLX_LpGBT_BE_Wrapper is
    Generic (
        CARD_TYPE                   : integer := 712;
        GBT_NUM                     : integer   := 24;
        KCU_LOWER_LATENCY : integer := 0;
        GTREFCLKS : integer
    );
    Port (

        FELIX_DOWNLINK_USER_DATA    : in array_32b(0 to GBT_NUM-1);
        FELIX_DOWNLINK_EC_DATA      : in array_2b(0 to GBT_NUM-1);
        FELIX_DOWNLINK_IC_DATA      : in array_2b(0 to GBT_NUM-1);

        FELIX_UPLINK_USER_DATA      : out array_224b(0 to GBT_NUM-1);
        FELIX_UPLINK_EC_DATA        : out array_2b(0 to GBT_NUM-1);
        FELIX_UPLINK_IC_DATA        : out array_2b(0 to GBT_NUM-1);

        clk40_in                    : in std_logic;
        clk100_in                   : in std_logic;
        rst_hw                      : in std_logic;
        --        FELIX_SIDE_RX40MCLK         : out std_logic;

        RX_P                        : in std_logic_vector(GBT_NUM-1 downto 0);
        RX_N                        : in std_logic_vector(GBT_NUM-1 downto 0);
        TX_P                        : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_N                        : out std_logic_vector(GBT_NUM-1 downto 0);

        GTHREFCLK                   : in std_logic_vector(GBT_NUM-1 downto 0);
        GTREFCLK_p                  : in std_logic_vector(GTREFCLKS-1 downto 0); --Only used for Versal, the clock buffers reside in the block design.
        GTREFCLK_n                  : in std_logic_vector(GTREFCLKS-1 downto 0); --Only used for Versal, the clock buffers reside in the block design.

        CTRL_SOFT_RESET             : in std_logic_vector(GBT_NUM/4-1 downto 0);
        CTRL_TXPLL_DATAPATH_RESET   : in std_logic_vector(GBT_NUM/4-1 downto 0);
        CTRL_RXPLL_DATAPATH_RESET   : in std_logic_vector(GBT_NUM/4-1 downto 0);
        CTRL_TX_DATAPATH_RESET      : in std_logic_vector(GBT_NUM/4-1 downto 0);
        CTRL_RX_DATAPATH_RESET      : in std_logic_vector(GBT_NUM/4-1 downto 0);
        CTRL_TXPOLARITY             : in std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_RXPOLARITY             : in std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_GBTTXRST               : in std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_GBTRXRST               : in std_logic_vector(GBT_NUM-1 downto 0);
        SOFT_TXRST_ALL              : in std_logic_vector(11 downto 0);--vc709
        SOFT_RXRST_ALL              : in std_logic_vector(11 downto 0);
        QPLL_RESET                  : in std_logic_vector(GBT_NUM/4-1 downto 0); --VC709
        --        CTRL_DATARATE               : in std_logic_vector(GBT_NUM-1 downto
        --        0); --MT now hard coded to 1024Gbps
        CTRL_FECMODE                : in std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_CHANNEL_DISABLE        : in std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_GBT_General_ctrl       : in std_logic_vector(63 downto 0);
        CTRL_MULTICYCLE_DELAY       : in std_logic_vector(2 downto 0);



        MON_RXRSTDONE               : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_TXRSTDONE               : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_RXRSTDONE_QUAD          : out std_logic_vector(GBT_NUM/4-1 downto 0);
        MON_TXRSTDONE_QUAD          : out std_logic_vector(GBT_NUM/4-1 downto 0);
        MON_RXPMARSTDONE            : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_TXPMARSTDONE            : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_TXFSMRESETDONE          : out std_logic_vector(GBT_NUM-1 downto 0);
        --VC709 only
        MON_RXFSMRESETDONE          : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_RXCDR_LCK               : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_RXCDR_LCK_QUAD          : out std_logic_vector(GBT_NUM/4-1 downto 0);
        MON_QPLL_LCK                : out std_logic_vector(GBT_NUM/4-1 downto 0);
        MON_CPLL_LCK                : out std_logic_vector(GBT_NUM-1 downto 0);

        MON_ALIGNMENT_DONE          : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_FEC_ERROR               : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_FEC_ERR_CNT             : out array_32b(0 to GBT_NUM-1);
        MON_AUTO_RX_RESET_CNT       : out array_32b(0 to GBT_NUM-1);
        CTRL_AUTO_RX_RESET_CNT_CLEAR: in  std_logic_vector(GBT_NUM-1 downto 0);
        RXUSRCLK_OUT                : out std_logic_vector(GBT_NUM-1 downto 0);
        TXUSRCLK_OUT                : out std_logic_vector(GBT_NUM-1 downto 0);
        GT_REFCLK_OUT               : out std_logic_vector(GBT_NUM-1 downto 0);
        --CTRL_TCLINK_PS_INC_NDEC     : in std_logic_vector(GBT_NUM-1 downto 0);
        --CTRL_TCLINK_PS_STROBE       : in std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_TCLINK_TX_FINE_REALIGN        : in  std_logic_vector(GBT_NUM - 1 downto 0);
        CTRL_TCLINK_TX_UI_ALIGN_CALIB      : in  std_logic_vector(GBT_NUM - 1 downto 0);
        CTRL_TCLINK_TX_PI_PHASE_CALIB      : in  array_7b(0 to GBT_NUM - 1);
        CTRL_TCLINK_TX_CLOSE_LOOP          : in  std_logic_vector(GBT_NUM - 1 downto 0);
        CTRL_TCLINK_OFFSET_ERROR           : in  array_48b(0 to GBT_NUM - 1);
        CTRL_TCLINK_DEBUG_TESTER_ADDR_READ : in  array_10b(0 to GBT_NUM - 1);
        MON_TCLINK_ERROR_CONTROLLER        : out array_48b(0 to GBT_NUM - 1);
        MON_TCLINK_LOOP_CLOSED             : out std_logic_vector(GBT_NUM - 1 downto 0);
        MON_TCLINK_TX_ALIGNED              : out std_logic_vector(GBT_NUM - 1 downto 0);
        MON_TCLINK_PS_DONE                 : out std_logic_vector(GBT_NUM - 1 downto 0);
        MON_TCLINK_MASTER_MGT_READY        : in  std_logic_vector(GBT_NUM - 1 downto 0);
        MON_TCLINK_PHASE_DETECTOR          : out array_32b(0 to GBT_NUM - 1);
        MON_TCLINK_TX_FIFO_FILL_PD         : out array_32b(0 to GBT_NUM - 1);
        MON_TCLINK_LOOP_NOT_CLOSED_REASON  : out array_5b(0 to GBT_NUM - 1);
        MON_TCLINK_PHASE_ACC               : out array_16b(0 to GBT_NUM - 1);
        MON_TCLINK_OPERATION_ERROR         : out std_logic_vector(GBT_NUM - 1 downto 0);
        MON_TCLINK_DEBUG_TESTER_DATA_READ  : out array_16b(0 to GBT_NUM - 1);
        MON_TCLINK_PS_PHASE_STEP           : out std_logic_vector(GBT_NUM - 1 downto 0);
        MON_TCLINK_TX_PI_PHASE             : out array_7b(0 to GBT_NUM - 1)
    );
end FLX_LpGBT_BE_Wrapper;


architecture Behavioral of FLX_LpGBT_BE_Wrapper is

    signal pulse_lg             : std_logic;
    signal alignment_done_f     : std_logic_vector(GBT_NUM-1 downto 0);
    signal alignment_done_f_clk40     : std_logic_vector(GBT_NUM-1 downto 0);
    signal alignment_done_f_latched     : std_logic_vector(GBT_NUM-1 downto 0);
    signal RX_RESET_i           : std_logic_vector(GBT_NUM-1 downto 0);
    signal TX_RESET_i           : std_logic_vector(GBT_NUM-1 downto 0);
    signal TxResetDone          : std_logic_vector(GBT_NUM-1 downto 0);
    signal TxResetDone_clk40          : std_logic_vector(GBT_NUM-1 downto 0);
    signal GT_TX_WORD_CLK       : std_logic_vector(GBT_NUM-1 downto 0);
    signal GT_RX_WORD_CLK       : std_logic_vector(GBT_NUM-1 downto 0);
    signal rxresetdone          : std_logic_vector(GBT_NUM-1 downto 0);
    signal rxresetdone_clk40          : std_logic_vector(GBT_NUM-1 downto 0);
    signal data_rdy             : std_logic_vector(GBT_NUM-1 downto 0);
    signal RxSlide              : std_logic_vector(GBT_NUM-1 downto 0);
    --signal UplinkRdy            : std_logic_vector(GBT_NUM-1 downto 0);
    signal fifo_rst             : std_logic_vector(GBT_NUM-1 downto 0);
    signal fifo_empty           : std_logic_vector(GBT_NUM-1 downto 0);
    signal fifo_rden            : std_logic_vector(GBT_NUM-1 downto 0);
    signal GT_TXOUTCLK          : std_logic_vector(GBT_NUM-1 downto 0);
    signal GT_RXOUTCLK          : std_logic_vector(GBT_NUM-1 downto 0);
    signal GT_TXUSRCLK          : std_logic_vector(GBT_NUM-1 downto 0); --Per quad for Virtex7, Per channel for KU.
    signal GT_RXUSRCLK          : std_logic_vector(GBT_NUM-1 downto 0);
    signal RX_N_i               : std_logic_vector(GBT_NUM-1 downto 0);
    signal RX_P_i               : std_logic_vector(GBT_NUM-1 downto 0);
    signal TX_N_i               : std_logic_vector(GBT_NUM-1 downto 0);
    signal TX_P_i               : std_logic_vector(GBT_NUM-1 downto 0);
    signal rxpmaresetdone       : std_logic_vector(GBT_NUM-1 downto 0);
    signal txpmaresetdone       : std_logic_vector(GBT_NUM-1 downto 0);
    signal RxCdrLock            : std_logic_vector(GBT_NUM-1 downto 0);
    signal RxCdrLock_int        : std_logic_vector(GBT_NUM-1 downto 0);
    signal RxCdrLock_a          : std_logic_vector(GBT_NUM-1 downto 0);
    signal rxcdrlock_out        : std_logic_vector(GBT_NUM-1 downto 0);
    signal auto_gth_rxrst       : std_logic_vector(GBT_NUM-1 downto 0);
    signal drpclk_vec           : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal rxresetdone_quad     : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal rxresetdone_quad_clk40     : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal txresetdone_quad     : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal txresetdone_quad_clk40     : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal RxCdrLock_quad       : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal userclk_rx_reset_in  : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal userclk_tx_reset_in  : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal RX_DATAPATH_RESET_FINL   : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal RX_DATAPATH_RESET_CHANNEL : std_logic_vector(GBT_NUM-1 downto 0);
    signal GBT_General_ctrl     : std_logic_vector(63 downto 0);
    signal downlinkUserData_i   : array_32b(GBT_NUM-1 downto 0);
    signal downlinkEcData_i     : array_2b(GBT_NUM-1 downto 0);
    signal downlinkIcData_i     : array_2b(GBT_NUM-1 downto 0);
    type data16barray     is array (0 to GBT_NUM-1) of std_logic_vector(15 downto 0);
    type data32barray     is array (0 to GBT_NUM-1) of std_logic_vector(31 downto 0);
    signal TX_DATA_16b    : data16barray := (others => ("0000000000000000"));
    signal RX_DATA_32b    : data32barray := (others => ("00000000000000000000000000000000"));
    type txrx234b_48ch_type        is array (GBT_NUM-1 downto 0) of std_logic_vector(233 downto 0);
    signal uplinkData_i :txrx234b_48ch_type;

    type txrx228b_48ch_type        is array (GBT_NUM-1  downto 0) of std_logic_vector(227 downto 0);
    signal uplinkData_o :txrx228b_48ch_type;
    type txrx128b_12ch_type        is array (GBT_NUM/4-1  downto 0) of std_logic_vector(127 downto 0);
    signal RX_DATA_128b            : txrx128b_12ch_type;
    type txrx64b_12ch_type        is array (GBT_NUM/4-1  downto 0) of std_logic_vector(63 downto 0);
    signal TX_DATA_64b            : txrx64b_12ch_type;

    signal cdr_cnt        : std_logic_vector(19 downto 0);
    signal pulse_cnt      : std_logic_vector(29 downto 0);

    signal MON_CPLL_LCK_reg : std_logic_vector(GBT_NUM-1 downto 0);
    signal MON_QPLL_LCK_reg : std_logic_vector(GBT_NUM/4-1 downto 0);

    signal fec_error_i : std_logic_vector(GBT_NUM-1 downto 0);
    signal fec_err_cnt_i : array_32b(0 to GBT_NUM-1);


    --VC709 only
    signal txfsmresetdone : std_logic_vector(GBT_NUM-1 downto 0);
    signal rxfsmresetdone : std_logic_vector(GBT_NUM-1 downto 0);
    --signal rxfsmresetdone_clk40 : std_logic_vector(GBT_NUM-1 downto 0);
    --signal txfsmresetdone_clk40 : std_logic_vector(GBT_NUM-1 downto 0);
    signal gttx_reset_i  : std_logic_vector(GBT_NUM-1 downto 0);
    signal gtrx_reset_i  : std_logic_vector(GBT_NUM-1 downto 0);

    signal  tied_to_ground_i                : std_logic;
    signal  tied_to_ground_vec_i            : std_logic_vector(2 downto 0);

    signal drp_clk: std_logic;

    COMPONENT mmcm_tclink_offset_clock -- @suppress "Component declaration 'mmcm_tclink_offset_clock' has none or multiple matching entity declarations"
        PORT(
            reset    : IN  STD_LOGIC;
            locked   : OUT STD_LOGIC;
            clk_in1  : IN  STD_LOGIC;
            clk_out1 : OUT STD_LOGIC
        );
    END COMPONENT;

    --signal txusrclk_quad : std_logic;
    signal tclink_clk_offset : std_logic;

    signal txpippmen       : std_logic_vector(GBT_NUM-1 downto 0);
    --signal txpippmovrden   : std_logic_vector(GBT_NUM-1 downto 0);
    --signal txpippmsel      : std_logic_vector(GBT_NUM-1 downto 0);
    --signal txpippmpd       : std_logic_vector(GBT_NUM-1 downto 0);
    signal txpippmstepsize : array_5b(0 to GBT_NUM-1);
    signal txpippmstepsize_txuserclk_enable : array_5b(0 to GBT_NUM-1);
    signal txpippmstepsize_txuserclk : array_5b(0 to GBT_NUM-1);

    signal txpippmen_ext       : std_logic_vector(GBT_NUM-1 downto 0);

    signal txbufstatus : array_2b (0 to GBT_NUM-1);
    signal txusrclk : std_logic_vector(GBT_NUM-1 downto 0);

    signal mmcm_lock :std_logic;
    signal mmcm_lock_100 : std_logic;

    signal inc_ndec : std_logic_vector(GBT_NUM-1 downto 0);
    signal phase_step: std_logic_vector(GBT_NUM-1 downto 0);
    signal ps_strobe: std_logic_vector(GBT_NUM-1 downto 0);

    signal tx_phasealigner_reset : std_logic_vector(GBT_NUM-1 downto 0);

    signal TCLINK_TX_ALIGNED_tx_word_clk: std_logic_vector(GBT_NUM - 1 downto 0);
    signal refclk_320 : std_logic_vector(GBT_NUM/4-1 downto 0);


begin

    g_182: if CARD_TYPE = 182 or CARD_TYPE = 181 or CARD_TYPE = 180 generate
        drp_clk <= clk100_in;
    else generate
        drp_clk <= clk40_in;
    end generate;

    tied_to_ground_i                             <= '0';
    tied_to_ground_vec_i                         <= "000";


    MON_ALIGNMENT_DONE <= alignment_done_f_clk40 when GBT_General_ctrl(42)='0' else alignment_done_f_latched;
    GBT_General_ctrl <= CTRL_GBT_General_ctrl;
    MON_CPLL_LCK <= MON_CPLL_LCK_reg;
    MON_QPLL_LCK <= MON_QPLL_LCK_reg;

    process(clk40_in)
    begin
        if clk40_in'event and clk40_in='1' then
            pulse_lg <= pulse_cnt(20);
            if pulse_cnt(20)='1' then
                pulse_cnt <=(others=>'0');
            else
                pulse_cnt <= pulse_cnt+'1';
            end if;
        end if;
    end process;

    lpgbtModeAutoRxReset_cnt_g: for i in 0 to GBT_NUM-1 generate
        signal RXRESET_AUTO_CNT: std_logic_vector(31 downto 0);
        signal RXRESET_AUTO_p1: std_logic;
    begin
        cnt_proc: process(clk40_in)
        begin
            if rising_edge(clk40_in) then
                RXRESET_AUTO_p1 <= auto_gth_rxrst(i);
                if rst_hw = '1' or CTRL_AUTO_RX_RESET_CNT_CLEAR(i) = '1' then
                    RXRESET_AUTO_CNT <= (others => '0');
                elsif auto_gth_rxrst(i) = '1' and RXRESET_AUTO_p1 = '0' then
                    RXRESET_AUTO_CNT <= RXRESET_AUTO_CNT + 1;
                end if;
            end if;
        end process;

        MON_AUTO_RX_RESET_CNT(i) <= RXRESET_AUTO_CNT;
    end generate;

    rxreset: for i in 0 to GBT_NUM-1 generate

        process(clk40_in)
        begin
            if clk40_in'event and clk40_in='1' then
                if GBT_General_ctrl(44)='1' then
                    alignment_done_f_latched(i) <='1';
                else
                    alignment_done_f_latched(i) <= alignment_done_f_latched(i) and alignment_done_f_clk40(i);
                end if;
                if pulse_lg = '1' then
                    --handles both KCU and VC709
                    --           if alignment_done_f_clk40(i)='0' and (rxresetdone_quad_clk40(i/4)='1' or rxfsmresetdone_clk40(i)='1') and rxresetdone_clk40(i)='1' then
                    if alignment_done_f_clk40(i)='0' and (rxresetdone_quad_clk40(i/4)='1' or rxfsmresetdone(i)='1') and rxresetdone_clk40(i)='1' then
                        auto_gth_rxrst(i) <='1';
                    else
                        auto_gth_rxrst(i) <='0';
                    end if;
                else
                    auto_gth_rxrst(i) <='0';
                end if;
            end if;
        end process;
    end generate;


    rst_loop: for i in 0 to GBT_NUM-1 generate
        rst_712: if CARD_TYPE = 712 generate
            RX_RESET_i(i)       <= CTRL_GBTRXRST(i);
            TX_RESET_i(i)       <= CTRL_GBTTXRST(i) or (not TxResetDone(i));
        end generate; --rst_712
        rst_709: if CARD_TYPE = 709 generate
            RX_RESET_i(i)       <= CTRL_GBTRXRST(i) and (not rxfsmresetdone(i));
            TX_RESET_i(i)       <= CTRL_GBTTXRST(i) or (not TxResetDone(i)) or (not txfsmresetdone(i));
        end generate; --rst_709
    end generate; --rst_loop


    gbtRxTx : for i in 0 to GBT_NUM-1 generate
        signal fifo_inst_din : std_logic_vector(227 downto 0);
        signal RX_RESET_rxusrclk: std_logic;
        signal TX_RESET_txusrclk: std_logic;
        signal fifo_rst_sync: std_logic;
        signal uplinkMulticycleDelay : std_logic_vector(2 downto 0);
    begin
        downlinkUserData_i(i)   <= FELIX_DOWNLINK_USER_DATA(i);
        downlinkEcData_i(i)     <= FELIX_DOWNLINK_EC_DATA(i);
        downlinkIcData_i(i)     <= FELIX_DOWNLINK_IC_DATA(i);

        RX_RESET_sync_inst : xpm_cdc_sync_rst
            generic map (
                DEST_SYNC_FF => 2,
                INIT => 1,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0
            )
            port map (
                src_rst => RX_RESET_i(i),
                dest_clk => GT_RX_WORD_CLK(i),
                dest_rst => RX_RESET_rxusrclk
            );

        TX_RESET_sync_inst : xpm_cdc_sync_rst
            generic map (
                DEST_SYNC_FF => 2,
                INIT => 1,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0
            )
            port map (
                src_rst => TX_RESET_i(i),
                dest_clk => GT_TX_WORD_CLK(i),
                dest_rst => TX_RESET_txusrclk
            );

        fifo_rst_sync_inst : xpm_cdc_sync_rst
            generic map (
                DEST_SYNC_FF => 2,
                INIT => 1,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0
            )
            port map (
                src_rst => fifo_rst(i),
                dest_clk => GT_RX_WORD_CLK(i),
                dest_rst => fifo_rst_sync
            );

        xpm_cdc_MULTICYCLE_DELAY : xpm_cdc_array_single
            generic map (
                DEST_SYNC_FF => 4,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 1,
                WIDTH => 3
            )
            port map (
                src_clk => clk40_in,
                src_in => CTRL_MULTICYCLE_DELAY,
                dest_clk =>  GT_RX_WORD_CLK(i),
                dest_out => uplinkMulticycleDelay
            );

        lpgbt_inst: entity work.FLX_LpGBT_BE
            Port map (
                downlinkUserData_i => downlinkUserData_i(i),
                downlinkEcData_i => downlinkEcData_i(i),
                downlinkIcData_i => downlinkIcData_i(i),
                TXCLK40 => clk40_in,
                TXCLK320 => GT_TX_WORD_CLK(i),
                RXCLK320m => GT_RX_WORD_CLK(i),
                uplinkSelectFEC_i => CTRL_FECMODE(i),
                data_rdy => data_rdy(i),
                Tx_scrambler_bypass => '0',
                Tx_Interleaver_bypass => '0',
                Tx_FEC_bypass => '0',
                TxData_Out => TX_DATA_16b(i),
                rxdatain => RX_DATA_32b(i),
                TX_aligned_i => TCLINK_TX_ALIGNED_tx_word_clk(i),
                GBT_TX_RST => TX_RESET_txusrclk,
                GBT_RX_RST => RX_RESET_rxusrclk,
                uplinkBypassInterleaver_i => GBT_General_ctrl(35),
                uplinkBypassFECEncoder_i => GBT_General_ctrl(36),
                uplinkBypassScrambler_i => GBT_General_ctrl(37),
                uplinkMulticycleDelay_i => uplinkMulticycleDelay,
                sta_headerFecLocked_o => alignment_done_f(i),
                ctr_clkSlip_o => RxSlide(i),
                --sta_rxGbRdy_o                           => open,
                uplinkReady_o => open, --UplinkRdy(i),
                uplinkUserData_o => uplinkData_i(i)(229 downto 0),
                uplinkEcData_o => uplinkData_i(i)(231 downto 230),
                uplinkIcData_o => uplinkData_i(i)(233 downto 232),
                fec_error_o    => fec_error_i(i),
                fec_err_cnt_o  => fec_err_cnt_i(i)
            );
        FELIX_UPLINK_USER_DATA(i)(223 downto 0) <= uplinkData_o(i)(223 downto 0);


        fifo_rst(i) <= (not alignment_done_f(i)) or rst_hw or GBT_General_ctrl(4) ;
        fifo_rden(i) <= not fifo_empty(i);


        FELIX_UPLINK_EC_DATA(i) <= uplinkData_o(i)(225 downto 224);
        FELIX_UPLINK_IC_DATA(i) <= uplinkData_o(i)(227 downto 226);

        fifo_inst_din <= uplinkData_i(i)(233 downto 232) & uplinkData_i(i)(231 downto 230) & uplinkData_i(i)(223 downto 0);

        fifo_inst : xpm_fifo_async
            generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT, SIM_ASSERT_CHK"
                CDC_SYNC_STAGES => 2,
                DOUT_RESET_VALUE => "0",
                ECC_MODE => "no_ecc",
                FIFO_MEMORY_TYPE => "auto",
                FIFO_READ_LATENCY => 1,
                FIFO_WRITE_DEPTH => 64,
                FULL_RESET_VALUE => 1,
                PROG_EMPTY_THRESH => 10,
                PROG_FULL_THRESH => 40,
                RD_DATA_COUNT_WIDTH => 1,
                READ_DATA_WIDTH => 228,
                READ_MODE => "std",
                RELATED_CLOCKS => 0,
                USE_ADV_FEATURES => "0000",
                WAKEUP_TIME => 0,
                WRITE_DATA_WIDTH => 228,
                WR_DATA_COUNT_WIDTH => 1
            )
            port map (
                sleep => '0',
                rst => fifo_rst_sync,
                wr_clk => GT_RX_WORD_CLK(i),
                wr_en => data_rdy(i),
                din => fifo_inst_din,
                full => open,
                prog_full => open,
                wr_data_count => open,
                overflow => open,
                wr_rst_busy => open,
                almost_full => open,
                wr_ack => open,
                rd_clk => clk40_in,
                rd_en => fifo_rden(i),
                dout => uplinkData_o(i),
                empty => fifo_empty(i),
                prog_empty => open,
                rd_data_count => open,
                underflow => open,
                rd_rst_busy => open,
                almost_empty => open,
                data_valid => open,
                injectsbiterr => '0',
                injectdbiterr => '0',
                sbiterr => open,
                dbiterr => open
            );

    end generate;


    clk_709_clk: if CARD_TYPE = 709 generate
        clk_generate_txoutclk: for i in 0 to GBT_NUM/4-1 generate

            GTTXOUTCLK_BUFG: BUFG
                port map(
                    o => GT_TXUSRCLK(i),
                    i => GT_TXOUTCLK(4*i)
                );
            GT_TX_WORD_CLK(i)   <= GT_TXUSRCLK(i);
            GT_TX_WORD_CLK(i+1) <= GT_TXUSRCLK(i);
            GT_TX_WORD_CLK(i+2) <= GT_TXUSRCLK(i);
            GT_TX_WORD_CLK(i+3) <= GT_TXUSRCLK(i);

        end generate; --clk_generate_txoutclk

        clk_generate_rxoutclk : for i in 0 to GBT_NUM-1 generate

            GTRXOUTCLK_BUFG: BUFG
                port map(
                    o => GT_RXUSRCLK(i),
                    i => GT_RXOUTCLK(i)
                );
            GT_RX_WORD_CLK(i) <= GT_RXUSRCLK(i);

        end generate; --clk_generate_rxoutclk
    end generate; --clk_709_clk

    clk_712: if CARD_TYPE = 712 generate
        clk_generate : for i in 0 to GBT_NUM-1 generate

            GTTXOUTCLK_BUFG: bufg_gt -- @suppress "Generic map uses default values. Missing optional actuals: SIM_DEVICE, STARTUP_SYNC"
                port map(
                    o => GT_TXUSRCLK(i),
                    ce => '1',
                    cemask => '0',
                    clr => '0',
                    clrmask => '0',
                    div => "000",
                    i => GT_TXOUTCLK(i)
                );

            GT_TX_WORD_CLK(i) <= GT_TXUSRCLK(i);

            GTRXOUTCLK_BUFG: bufg_gt -- @suppress "Generic map uses default values. Missing optional actuals: SIM_DEVICE, STARTUP_SYNC"
                port map(
                    o => GT_RXUSRCLK(i),
                    ce => '1',
                    cemask => '0',
                    clr => '0',
                    clrmask => '0',
                    div => "000",
                    i => GT_RXOUTCLK(i)
                );

            GT_RX_WORD_CLK(i) <= GT_RXUSRCLK(i);
        end generate; --clk_generate
    end generate; --clk_712

    RXUSRCLK_OUT   <= GT_RXUSRCLK;
    TXUSRCLK_OUT   <= GT_TXUSRCLK;
    port_trans : for i in GBT_NUM-1 downto 0 generate
        RX_N_i(i)   <= RX_N(i);
        RX_P_i(i)   <= RX_P(i);
        TX_N(i)     <= TX_N_i(i);
        TX_P(i)     <= TX_P_i(i);
    end generate;

    x_712: if CARD_TYPE = 712 generate
        quad_cdc: for i in 0 to GBT_NUM/4-1 generate
            -- Ease the timing constraints on auto_gth_rxrst path
            -- by adding CDC for input signals from GT_RXUSRCLK to clk40 MHz

            xpm_cdc_rxresetdone_quad : xpm_cdc_single
                generic map (
                    DEST_SYNC_FF => 4,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 1
                )
                port map (
                    src_clk => GT_RX_WORD_CLK(4*i),
                    src_in => rxresetdone_quad(i),
                    dest_clk => clk40_in,
                    dest_out => rxresetdone_quad_clk40(i)
                );

            xpm_cdc_txresetdone_quad : xpm_cdc_single
                generic map (
                    DEST_SYNC_FF => 4,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 1
                )
                port map (
                    src_clk => GT_TX_WORD_CLK(4*i),
                    src_in => txresetdone_quad(i),
                    dest_clk => clk40_in,
                    dest_out => txresetdone_quad_clk40(i)
                );
        end generate; --quad_cdc
    end generate; --  x_712


    channel_cdc: for i in 0 to GBT_NUM-1 generate
        -- Ease the timing constraints on auto_gth_rxrst path
        -- by adding CDC for input signals from GT_RXUSRCLK to clk40 MHz
        xpm_cdc_rxresetdone : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => rxresetdone(i),
                dest_clk => clk40_in,
                dest_out => rxresetdone_clk40(i)
            );

        xpm_cdc_txresetdone : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => TxResetDone(i),
                dest_clk => clk40_in,
                dest_out => TxResetDone_clk40(i)
            );

        xpm_cdc_alignment_done_f : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 4,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 1
            )
            port map (
                src_clk => GT_RX_WORD_CLK(i),
                src_in => alignment_done_f(i),
                dest_clk => clk40_in,
                dest_out => alignment_done_f_clk40(i)
            );

        xpm_cdc_fec_error : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 4,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 1
            )
            port map (
                src_clk => GT_RX_WORD_CLK(i),
                src_in => fec_error_i(i),
                dest_clk => clk40_in,
                dest_out => MON_FEC_ERROR(i)
            );


        --xpm_cdc_fec_err_cnt : xpm_cdc_gray
        --    generic map (
        --        DEST_SYNC_FF => 2,
        --        INIT_SYNC_FF => 0,
        --        REG_OUTPUT => 1,            -- DECIMAL; 0=disable registered output, 1=enable registered output
        --        SIM_ASSERT_CHK => 0,
        --        SIM_LOSSLESS_GRAY_CHK => 0, -- DECIMAL; 0=disable lossless check, 1=enable lossless check
        --        WIDTH => 32                 -- DECIMAL; range: 2-32
        --    )
        --    port map (
        --        src_clk => GT_RX_WORD_CLK(i),
        --        src_in_bin => fec_err_cnt_i(i),
        --        dest_clk => clk40_in,
        --        dest_out_bin => MON_FEC_ERR_CNT(i)
        --    );
        MON_FEC_ERR_CNT(i) <= fec_err_cnt_i(i); --FEC Error count is now registered at 40 MHz, so no synchronization required anymore

    end generate; --channel_cdc

    g_tclink_versal : if CARD_TYPE = 181 or CARD_TYPE = 182 or CARD_TYPE = 155 generate
        cmp_mmcme_master_tclink : mmcm_tclink_offset_clock
            PORT MAP(
                reset    => RX_DATAPATH_RESET_FINL(0),
                locked   => mmcm_lock,
                clk_in1  => refclk_320(0),
                clk_out1 => tclink_clk_offset
            );

        xpm_cdc_async_mmcm_lock : xpm_cdc_async_rst
            generic map (
                DEST_SYNC_FF => 4,
                INIT_SYNC_FF => 0,
                RST_ACTIVE_HIGH => 0
            )
            port map (
                src_arst => mmcm_lock,
                dest_clk => drp_clk,
                dest_arst => mmcm_lock_100
            );



        TClink_inst : for i in 0 to GBT_NUM-1 generate


            signal ps_done: std_logic;

            signal hptd_ps_phase_step: std_logic_vector(3 downto 0);
            signal hptd_ps_phase_step_p1: std_logic_vector(3 downto 0);

            --! Synchronization of register map registers to 100MHz clock drp_clk
            signal CTRL_TCLINK_OFFSET_ERROR_100: std_logic_vector(47 downto 0);
            signal CTRL_TCLINK_TX_FINE_REALIGN_100 :  std_logic;
            signal CTRL_TCLINK_TX_UI_ALIGN_CALIB_100:  std_logic;
            signal CTRL_TCLINK_TX_PI_PHASE_CALIB_100:  std_logic_vector(6 downto 0);
            signal CTRL_TCLINK_TX_CLOSE_LOOP_100:  std_logic;
            signal CTRL_TCLINK_DEBUG_TESTER_ADDR_READ_100: std_logic_vector(9 downto 0);
            signal MON_TCLINK_MASTER_MGT_READY_100: std_logic;

            signal tx_ready : std_logic;
            signal rx_ready : std_logic;
            signal mgt_rx_ready:std_logic;

        begin


            xpm_cdc_array_OFFSET_ERROR : xpm_cdc_array_single
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0,
                    WIDTH => 48
                )
                port map ( -- @suppress "The order of the associations is different from the declaration order"
                    dest_out => CTRL_TCLINK_OFFSET_ERROR_100,
                    dest_clk => drp_clk,
                    src_clk => '0',
                    src_in => CTRL_TCLINK_OFFSET_ERROR(i)
                );
            xpm_cdc_array_PHASE_CALIB : xpm_cdc_array_single
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0,
                    WIDTH => 7
                )
                port map ( -- @suppress "The order of the associations is different from the declaration order"
                    dest_out => CTRL_TCLINK_TX_PI_PHASE_CALIB_100,
                    dest_clk => drp_clk,
                    src_clk => '0',
                    src_in => CTRL_TCLINK_TX_PI_PHASE_CALIB(i)
                );
            xpm_cdc_array_DEBUG_TESTER_ADDR : xpm_cdc_array_single
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0,
                    WIDTH => 10
                )
                port map ( -- @suppress "The order of the associations is different from the declaration order"
                    dest_out => CTRL_TCLINK_DEBUG_TESTER_ADDR_READ_100,
                    dest_clk => drp_clk,
                    src_clk => '0',
                    src_in => CTRL_TCLINK_DEBUG_TESTER_ADDR_READ(i)
                );
            xpm_cdc_TX_FINE_REALIGN : xpm_cdc_single
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0
                )
                port map ( -- @suppress "The order of the associations is different from the declaration order"
                    dest_out => CTRL_TCLINK_TX_FINE_REALIGN_100,
                    dest_clk => drp_clk,
                    src_clk => '0',
                    src_in => CTRL_TCLINK_TX_FINE_REALIGN(i)
                );
            xpm_cdc_TX_UI_ALIGN_CALIB : xpm_cdc_single
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0
                )
                port map ( -- @suppress "The order of the associations is different from the declaration order"
                    dest_out => CTRL_TCLINK_TX_UI_ALIGN_CALIB_100,
                    dest_clk => drp_clk,
                    src_clk => '0',
                    src_in => CTRL_TCLINK_TX_UI_ALIGN_CALIB(i)
                );
            xpm_cdc_TX_CLOSE_LOOP : xpm_cdc_single
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0
                )
                port map ( -- @suppress "The order of the associations is different from the declaration order"
                    dest_out => CTRL_TCLINK_TX_CLOSE_LOOP_100,
                    dest_clk => drp_clk,
                    src_clk => '0',
                    src_in => CTRL_TCLINK_TX_CLOSE_LOOP(i)
                );
            xpm_cdc_MASTER_MGT_READY : xpm_cdc_single
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0
                )
                port map ( -- @suppress "The order of the associations is different from the declaration order"
                    dest_out => MON_TCLINK_MASTER_MGT_READY_100,
                    dest_clk => drp_clk,
                    src_clk => '0',
                    src_in => MON_TCLINK_MASTER_MGT_READY(i)
                );

            xpm_cdc_array_txpippmstepsize : xpm_cdc_array_single
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 1,
                    WIDTH => 5
                )
                port map ( -- @suppress "The order of the associations is different from the declaration order"
                    dest_out => txpippmstepsize_txuserclk(i),
                    dest_clk => GT_TX_WORD_CLK(i),
                    src_clk => drp_clk,
                    src_in => txpippmstepsize(i)
                );

            tx_ready <= TxResetDone(i) and mmcm_lock_100;
            rx_ready <= rxresetdone(i) and mmcm_lock_100;
            mgt_rx_ready <= MON_TCLINK_MASTER_MGT_READY_100 and mmcm_lock_100;
            tx_phasealigner_reset(i) <= (not TxResetDone(i));

            -- Values are calculated with: model_transfer_function.py
            tclink_inst : entity work.tclink
                generic map(
                    g_ENABLE_TESTER_IMPLEMENTATION => false,
                    g_MASTER_RX_MGT_WORD_WIDTH     => 32,
                    g_ENABLE_PHASE_LIMIT           => true,
                    g_PHASE_LIMIT_PI               => 500
                )
                port map( -- @suppress "The order of the associations is different from the declaration order"
                    clk_sys_i                       => drp_clk,
                    tx_ready_i                      => tx_ready,
                    rx_ready_i                      => rx_ready,
                    clk_tx_i                        => GT_TX_WORD_CLK(i),
                    clk_rx_i                        => GT_RX_WORD_CLK(i),
                    clk_offset_i                    => tclink_clk_offset,
                    metastability_deglitch_i        => x"0051",
                    phase_detector_navg_i           => x"040",
                    phase_detector_o                => MON_TCLINK_PHASE_DETECTOR(i),
                    modulo_carrier_period_i         => x"00007e951034",
                    offset_error_i                  => CTRL_TCLINK_OFFSET_ERROR_100,
                    error_controller_o              => MON_TCLINK_ERROR_CONTROLLER(i),
                    master_rx_slide_mode_i          => '1',
                    master_rx_ui_period_i           => x"000003f4a882",
                    master_rx_slide_clk_i           => GT_RX_WORD_CLK(i),
                    master_mgt_rx_ready_i           => mgt_rx_ready,--master_mgt_rx_ready_i,
                    master_rx_slide_i               => '0',--master_rx_slide_i,
                    close_loop_i                    => CTRL_TCLINK_TX_CLOSE_LOOP_100,
                    loop_closed_o                   => MON_TCLINK_LOOP_CLOSED(i),
                    loop_not_closed_reason_o        => MON_TCLINK_LOOP_NOT_CLOSED_REASON(i),
                    Aie_i                           => x"0",
                    Aie_enable_i                    => '0',
                    Ape_i                           => x"e",
                    sigma_delta_clk_div_i           => x"0141",
                    enable_mirror_i                 => '1',
                    Adco_i                          => x"0000000fd2a2",
                    phase_acc_o                     => MON_TCLINK_PHASE_ACC(i),-- Used for debugging
                    operation_error_o               => MON_TCLINK_OPERATION_ERROR(i),-- Used for debugging
                    strobe_o                        => ps_strobe(i),
                    inc_ndec_o                      => inc_ndec(i),
                    phase_step_o                    => phase_step(i),
                    done_i                          => ps_done,
                    debug_tester_enable_stimulis_i  => '0', -- Used for testing
                    debug_tester_fcw_i              => "00" & x"02", -- Used for testing
                    debug_tester_nco_scale_i        => '1' & x"4", -- Value from python script is :0x14
                    debug_tester_enable_stock_out_i => '0', -- Used for testing
                    debug_tester_addr_read_i        => CTRL_TCLINK_DEBUG_TESTER_ADDR_READ_100, -- Used for testing
                    debug_tester_data_read_o        => MON_TCLINK_DEBUG_TESTER_DATA_READ(i)  -- Used for testing
                );

            MON_TCLINK_PS_PHASE_STEP(i) <= phase_step(0);
            MON_TCLINK_PS_DONE(i) <= ps_done;
            hptd_ps_phase_step <= "000"&'0' when CTRL_TCLINK_TX_CLOSE_LOOP_100='1' else hptd_ps_phase_step_p1;

            phase_step_pipe_proc: process(drp_clk)
            begin
                if rising_edge(drp_clk) then
                    hptd_ps_phase_step_p1 <= hptd_ps_phase_step;
                end if;
            end process;
            tx_phase_aligner_inst : entity work.tx_phase_aligner
                generic map(
                    g_DRP_NPORT_CTRL => false,
                    g_DRP_ADDR_TXPI_PPM_CFG => ("010011010")
                )
                port map(
                    clk_sys_i             => drp_clk,
                    reset_i               => tx_phasealigner_reset(i),
                    tx_aligned_o          => MON_TCLINK_TX_ALIGNED(i),
                    tx_pi_phase_calib_i   => CTRL_TCLINK_TX_PI_PHASE_CALIB_100,
                    tx_ui_align_calib_i   => CTRL_TCLINK_TX_UI_ALIGN_CALIB_100,
                    tx_fifo_fill_pd_max_i => x"00040000", -- Recommended by: refnote
                    tx_fine_realign_i     => CTRL_TCLINK_TX_FINE_REALIGN_100,
                    ps_strobe_i           => ps_strobe(i),
                    ps_inc_ndec_i         => inc_ndec(i),
                    ps_phase_step_i       => hptd_ps_phase_step,
                    ps_done_o             => ps_done,
                    tx_pi_phase_o         => MON_TCLINK_TX_PI_PHASE(i), -- Status
                    tx_fifo_fill_pd_o     => MON_TCLINK_TX_FIFO_FILL_PD(i), -- Status
                    clk_txusr_i           => GT_TX_WORD_CLK(i),
                    tx_fifo_fill_level_i  => txbufstatus(i)(0),
                    txpippmen_o           => txpippmen(i),
                    txpippmovrden_o       => open, --txpippmovrden(i),
                    txpippmsel_o          => open, --txpippmsel(i),
                    txpippmpd_o           => open, --txpippmpd(i),
                    txpippmstepsize_o     => txpippmstepsize(i),
                    drpaddr_o             => open, --drpaddr(i),
                    drpen_o               => open, --drpen(i),
                    drpdi_o               => open, --drpdi(i),
                    drprdy_i              => '0',
                    drpdo_i               => x"0000",
                    drpwe_o               => open
                );

            TCLINK_TX_ALIGNED_cdc : xpm_cdc_single
                generic map(
                    DEST_SYNC_FF   => 2,
                    INIT_SYNC_FF   => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG  => 0
                )
                port map(
                    src_clk  => '0',
                    src_in   => MON_TCLINK_TX_ALIGNED(i),
                    dest_clk => GT_TX_WORD_CLK(i),
                    dest_out => TCLINK_TX_ALIGNED_tx_word_clk(i)
                );

            txpippmsetpize_update : process(GT_TX_WORD_CLK)
            begin
                if rising_edge(GT_TX_WORD_CLK(i)) then
                    if txpippmen(i) = '1' then
                        txpippmstepsize_txuserclk_enable(i) <= txpippmstepsize_txuserclk(i);
                    else
                        txpippmstepsize_txuserclk_enable(i) <= (others => '0');
                    end if;
                end if;
            end process;
            txpippmen_ext(i) <= '1';

        end generate TClink_inst;
    else generate
        g_number_of_resets: for i in 0 to GBT_NUM-1 generate
            signal TX_RESET_txusrclk: std_logic;
        begin
            TX_RESET_sync_inst : xpm_cdc_sync_rst
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT => 1,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0
                )
                port map (
                    src_rst => TX_RESET_i(i),
                    dest_clk => GT_TX_WORD_CLK(i),
                    dest_rst => TX_RESET_txusrclk
                );

            TCLINK_TX_ALIGNED_tx_word_clk(i) <= TX_RESET_txusrclk;
        end generate g_number_of_resets;
    end generate g_tclink_versal;

    GTH_inst : for i in 0 to (GBT_NUM-1)/4 generate
        signal CTRL_RXPOLARITY_rxusrclk: std_logic_vector(3 downto 0);
        signal CTRL_TXPOLARITY_txusrclk: std_logic_vector(3 downto 0);
    begin
        drpclk_vec(i)       <= clk40_in;
        RX_DATA_32b(4*i+0)  <= RX_DATA_128b(i)(31 downto 0);
        RX_DATA_32b(4*i+1)  <= RX_DATA_128b(i)(63 downto 32);
        RX_DATA_32b(4*i+2)  <= RX_DATA_128b(i)(95 downto 64);
        RX_DATA_32b(4*i+3)  <= RX_DATA_128b(i)(127 downto 96);
        TX_DATA_64b(i)      <= TX_DATA_16b(4*i+3) & TX_DATA_16b(4*i+2) & TX_DATA_16b(4*i+1) & TX_DATA_16b(4*i+0);

        g_sync_POLARITY: for pol in 0 to 3 generate
            xpm_cdc_RXPOLARITY: xpm_cdc_single
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0
                )
                port map (
                    src_clk => '0',
                    src_in => CTRL_RXPOLARITY(4*i+pol),
                    dest_clk => GT_RX_WORD_CLK(4*i+pol),
                    dest_out => CTRL_RXPOLARITY_rxusrclk(pol)
                );
            xpm_cdc_TXPOLARITY: xpm_cdc_single
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0
                )
                port map (
                    src_clk => '0',
                    src_in => CTRL_TXPOLARITY(4*i+pol),
                    dest_clk => GT_TX_WORD_CLK(4*i+pol),
                    dest_out => CTRL_TXPOLARITY_txusrclk(pol)
                );
        end generate;

        g_712: if CARD_TYPE = 712 or CARD_TYPE = 128 or CARD_TYPE = 800 generate
            signal RxCdrLock_quad_s: std_logic_vector(3 downto 0);
            signal txresetdone_quad_s: std_logic_vector(3 downto 0);
            signal rxresetdone_quad_s: std_logic_vector(3 downto 0);
        begin
            GTH_TOP712_INST: entity work.FLX_LpGBT_GTH_BE
                generic map(
                    KCU_LOWER_LATENCY => KCU_LOWER_LATENCY,
                    CARD_TYPE => CARD_TYPE
                )
                Port map
        (
                    gt_rxusrclk_in => GT_RX_WORD_CLK(4*i+3 downto 4*i),
                    gt_txusrclk_in => GT_TX_WORD_CLK(4*i+3 downto 4*i),
                    gt_rxoutclk_out => GT_RXOUTCLK(4*i+3 downto 4*i),
                    gt_txoutclk_out => GT_TXOUTCLK(4*i+3 downto 4*i),
                    gthrxn_in => RX_N_i(4*i+3 downto 4*i),
                    gthrxp_in => RX_P_i(4*i+3 downto 4*i),
                    gthtxn_out => TX_N_i(4*i+3 downto 4*i),
                    gthtxp_out => TX_P_i(4*i+3 downto 4*i),
                    drpclk_in => drpclk_vec(i downto i),
                    gtrefclk0_in => GTHREFCLK(4*i downto 4*i),

                    rxpolarity_in => CTRL_RXPOLARITY_rxusrclk,
                    txpolarity_in => CTRL_TXPOLARITY_txusrclk,
                    loopback_in => tied_to_ground_vec_i,
                    rxcdrhold_in => tied_to_ground_i,
                    userdata_tx_in => TX_DATA_64b(i),
                    userdata_rx_out => RX_DATA_128b(i),

                    userclk_rx_reset_in => userclk_rx_reset_in(i downto i),
                    userclk_tx_reset_in => userclk_tx_reset_in(i downto i),
                    reset_all_in => CTRL_SOFT_RESET(i downto i),
                    reset_tx_pll_and_datapath_in => CTRL_TXPLL_DATAPATH_RESET(i downto i),
                    reset_tx_datapath_in => CTRL_TX_DATAPATH_RESET(i downto i),
                    reset_rx_pll_and_datapath_in => CTRL_RXPLL_DATAPATH_RESET(i downto i),
                    reset_rx_datapath_in => RX_DATAPATH_RESET_CHANNEL(4*i+3 downto 4*i),
                    qpll1lock_out => MON_QPLL_LCK_reg(i downto i),
                    qpll1fbclklost_out => open, --
                    qpll0lock_out => open,
                    qpll0fbclklost_out => open,
                    rxslide_in => RxSlide(4*i+3 downto 4*i),
                    txresetdone_out => TxResetDone(4*i+3 downto 4*i),
                    txpmaresetdone_out => txpmaresetdone(4*i+3 downto 4*i),
                    rxresetdone_out => rxresetdone(4*i+3 downto 4*i),
                    rxpmaresetdone_out => rxpmaresetdone(4*i+3 downto 4*i),
                    reset_tx_done_out => txresetdone_quad_s,
                    reset_rx_done_out => rxresetdone_quad_s,
                    reset_rx_cdr_stable_out => RxCdrLock_quad_s,
                    rxcdrlock_out => rxcdrlock_out(4*i+3 downto 4*i),
                    cplllock_out => MON_CPLL_LCK_reg(4*i+3 downto 4*i)
                );
            --zeroing to avoid glitches for auto_gth_rxrst
            rxfsmresetdone(4*i+3 downto 4*i) <= "0000";

            MON_RXRSTDONE_QUAD(i downto i)              <= rxresetdone_quad_clk40(i downto i);
            MON_TXRSTDONE_QUAD(i downto i)              <= txresetdone_quad_clk40(i downto i);
            MON_RXPMARSTDONE(4*i+3 downto 4*i)          <= rxpmaresetdone(4*i+3 downto 4*i);
            MON_TXPMARSTDONE(4*i+3 downto 4*i)          <= txpmaresetdone(4*i+3 downto 4*i);
            MON_RXCDR_LCK_QUAD(i downto i)              <= RxCdrLock_quad(i downto i);

            RxCdrLock_quad(i) <= RxCdrLock_quad_s(3) and RxCdrLock_quad_s(2) and  RxCdrLock_quad_s(1) and RxCdrLock_quad_s(0);
            txresetdone_quad(i) <= txresetdone_quad_s(3) and txresetdone_quad_s(2) and txresetdone_quad_s(1) and txresetdone_quad_s(0);
            rxresetdone_quad(i) <= rxresetdone_quad_s(3) and rxresetdone_quad_s(2) and rxresetdone_quad_s(1) and rxresetdone_quad_s(0);

        end generate; --g_712: if CARD_TYPE = 712 generate

        --to DO: implement CPLL
        g_709: if CARD_TYPE = 709 generate

            GTH_TOP709_INST: entity work.FLX_LpGBT_GTH_BE_VC7
                Port map
      (
                    GTH_RefClk => GTHREFCLK(4*i),
                    drpclk_in => drpclk_vec(i),
                    gt_rxusrclk_in => GT_RX_WORD_CLK(4*i+3 downto 4*i),
                    gt_txusrclk_in => GT_TXUSRCLK(4*i downto 4*i),
                    gt_rxoutclk_out => GT_RXOUTCLK(4*i+3 downto 4*i),
                    gt_txoutclk_out => GT_TXOUTCLK(4*i+3 downto 4*i),
                    gthrxn_in => RX_N_i(4*i+3 downto 4*i),
                    gthrxp_in => RX_P_i(4*i+3 downto 4*i),
                    gthtxn_out => TX_N_i(4*i+3 downto 4*i),
                    gthtxp_out => TX_P_i(4*i+3 downto 4*i),
                    rxpolarity_in => CTRL_RXPOLARITY_rxusrclk,
                    txpolarity_in => CTRL_TXPOLARITY_txusrclk,
                    userdata_tx_in => TX_DATA_64b(i),
                    userdata_rx_out => RX_DATA_128b(i),
                    gt_txresetdone_out => TxResetDone(4*i+3 downto 4*i),
                    gt_rxresetdone_out => rxresetdone(4*i+3 downto 4*i),
                    gt_txfsmresetdone_out => txfsmresetdone(4*i+3 downto 4*i),
                    gt_rxfsmresetdone_out => rxfsmresetdone(4*i+3 downto 4*i),
                    gt_cplllock_out => MON_CPLL_LCK_reg(4*i+3 downto 4*i),
                    gt_rxcdrlock_out => rxcdrlock_out(4*i+3 downto 4*i),
                    gt_rxcdrhold_in => "0000",
                    gt_qplllock_out => MON_QPLL_LCK_reg(i downto i),
                    gt_rxslide_in => RxSlide(4*i+3 downto 4*i),
                    gt_txuserrdy_in => "1111", --TxUsrRdy(4*i+3 downto 4*i),
                    --gt_rxuserrdy_in => "1111", --RxUsrRdy(4*i+3 downto 4*i),
                    --SOFT_RESET_IN => soft_reset(i) or rst_hw,
                    GTTX_RESET_IN => gttx_reset_i(4*i+3 downto 4*i), -- or rst_hw,
                    GTRX_RESET_IN => gtrx_reset_i(4*i+3 downto 4*i),
                    --CPLL_RESET_IN => cpll_reset(4*i+3 downto 4*i),
                    QPLL_RESET_IN => QPLL_RESET(i),
                    SOFT_TXRST_ALL => SOFT_TXRST_ALL(i) or rst_hw,
                    SOFT_RXRST_ALL => SOFT_RXRST_ALL(i) or rst_hw
                );
            --zeroing to avoid glitches for auto_gth_rxrst
            rxresetdone_quad(i) <= '0';
            MON_TXFSMRESETDONE(i) <= txfsmresetdone(i); --_clk40(i);
            MON_RXFSMRESETDONE(i) <= rxfsmresetdone(i); --_clk40(i);

        end generate; --g_709: if CARD_TYPE = 709 generate

        g_182: if CARD_TYPE = 180 or CARD_TYPE = 181 or CARD_TYPE = 182 or CARD_TYPE = 155 generate
            signal ch0_rxdata: std_logic_vector(127 downto 0);
            signal ch1_rxdata: std_logic_vector(127 downto 0);
            signal ch2_rxdata: std_logic_vector(127 downto 0);
            signal ch3_rxdata: std_logic_vector(127 downto 0);
            signal ch0_txdata: std_logic_vector(127 downto 0);
            signal ch1_txdata: std_logic_vector(127 downto 0);
            signal ch2_txdata: std_logic_vector(127 downto 0);
            signal ch3_txdata: std_logic_vector(127 downto 0);
            signal lcpll_lock: std_logic_vector(1 downto 0);
            --signal rxusrclk_quad : std_logic;

            signal txusrclk0 : std_logic;
            signal txusrclk1 : std_logic;
            signal txusrclk2 : std_logic;
            signal txusrclk3 : std_logic;

            component transceiver_versal_lpgbt_wrapper is -- @suppress "Component declaration 'transceiver_versal_lpgbt_wrapper' has none or multiple matching entity declarations"
                port (
                    APB3_INTF_0_paddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    APB3_INTF_0_penable : in STD_LOGIC;
                    APB3_INTF_0_prdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
                    APB3_INTF_0_pready : out STD_LOGIC;
                    APB3_INTF_0_psel : in STD_LOGIC;
                    APB3_INTF_0_pslverr : out STD_LOGIC;
                    APB3_INTF_0_pwdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
                    APB3_INTF_0_pwrite : in STD_LOGIC;
                    GT_REFCLK0_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
                    GT_REFCLK0_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
                    GT_Serial_grx_n : in STD_LOGIC_VECTOR ( 3 downto 0 );
                    GT_Serial_grx_p : in STD_LOGIC_VECTOR ( 3 downto 0 );
                    GT_Serial_gtx_n : out STD_LOGIC_VECTOR ( 3 downto 0 );
                    GT_Serial_gtx_p : out STD_LOGIC_VECTOR ( 3 downto 0 );
                    QUAD0_hsclk0_lcplllock_0 : out STD_LOGIC;
                    QUAD0_hsclk1_lcplllock_0 : out STD_LOGIC;
                    apb3presetn_0 : in STD_LOGIC;
                    ch0_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
                    ch0_rxcdrlock_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch0_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch0_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch0_rxgearboxslip_ext_0 : in STD_LOGIC;
                    ch0_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
                    ch0_rxpolarity_ext_0 : in STD_LOGIC;
                    ch0_rxresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch0_rxslide_ext_0 : in STD_LOGIC;
                    ch0_txbufstatus_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch0_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch0_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch0_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch0_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch0_txheader_ext_0 : in STD_LOGIC_VECTOR ( 5 downto 0 );
                    ch0_txpippmen_ext_0 : in STD_LOGIC;
                    ch0_txpippmstepsize_ext_0 : in STD_LOGIC_VECTOR ( 4 downto 0 );
                    ch0_txpolarity_ext_0 : in STD_LOGIC;
                    ch0_txresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch0_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
                    ch1_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
                    ch1_rxcdrlock_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch1_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch1_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch1_rxgearboxslip_ext_0 : in STD_LOGIC;
                    ch1_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
                    ch1_rxpolarity_ext_0 : in STD_LOGIC;
                    ch1_rxresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch1_rxslide_ext_0 : in STD_LOGIC;
                    ch1_txbufstatus_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch1_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch1_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch1_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch1_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch1_txpippmen_ext_0 : in STD_LOGIC;
                    ch1_txpippmstepsize_ext_0 : in STD_LOGIC_VECTOR ( 4 downto 0 );
                    ch1_txpolarity_ext_0 : in STD_LOGIC;
                    ch1_txresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch1_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
                    ch2_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
                    ch2_rxcdrlock_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch2_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch2_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch2_rxgearboxslip_ext_0 : in STD_LOGIC;
                    ch2_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
                    ch2_rxpolarity_ext_0 : in STD_LOGIC;
                    ch2_rxresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch2_rxslide_ext_0 : in STD_LOGIC;
                    ch2_txbufstatus_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch2_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch2_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch2_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch2_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch2_txpippmen_ext_0 : in STD_LOGIC;
                    ch2_txpippmstepsize_ext_0 : in STD_LOGIC_VECTOR ( 4 downto 0 );
                    ch2_txpolarity_ext_0 : in STD_LOGIC;
                    ch2_txresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch2_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
                    ch3_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
                    ch3_rxcdrlock_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch3_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch3_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch3_rxgearboxslip_ext_0 : in STD_LOGIC;
                    ch3_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
                    ch3_rxpolarity_ext_0 : in STD_LOGIC;
                    ch3_rxresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch3_rxslide_ext_0 : in STD_LOGIC;
                    ch3_txbufstatus_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch3_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch3_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch3_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch3_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch3_txpippmen_ext_0 : in STD_LOGIC;
                    ch3_txpippmstepsize_ext_0 : in STD_LOGIC_VECTOR ( 4 downto 0 );
                    ch3_txpolarity_ext_0 : in STD_LOGIC;
                    ch3_txresetdone_ext_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
                    ch3_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
                    gt_reset_all_in_0 : in STD_LOGIC;
                    gtwiz_freerun_clk_0 : in STD_LOGIC;
                    refclk_320_out : out STD_LOGIC;
                    reset_rx_datapath_in_0 : in STD_LOGIC;
                    reset_rx_pll_and_datapath_in_0 : in STD_LOGIC;
                    reset_tx_datapath_in_0 : in STD_LOGIC;
                    reset_tx_pll_and_datapath_in_0 : in STD_LOGIC;
                    rx_resetdone_out_gt_bridge_ip_0 : out STD_LOGIC;
                    rxusrclk0 : out STD_LOGIC;
                    rxusrclk1 : out STD_LOGIC;
                    rxusrclk2 : out STD_LOGIC;
                    rxusrclk3 : out STD_LOGIC;
                    tx_resetdone_out_gt_bridge_ip_0 : out STD_LOGIC;
                    txusrclk0 : out STD_LOGIC;
                    txusrclk1 : out STD_LOGIC;
                    txusrclk2 : out STD_LOGIC;
                    txusrclk3 : out STD_LOGIC
                );
            end component transceiver_versal_lpgbt_wrapper;
        begin

            RX_DATA_128b(i) <= ch3_rxdata(31 downto 0) &
                               ch2_rxdata(31 downto 0) &
                               ch1_rxdata(31 downto 0) &
                               ch0_rxdata(31 downto 0);

            ch0_txdata(15 downto 0) <= TX_DATA_64b(i)(15 downto 0);
            ch1_txdata(15 downto 0) <= TX_DATA_64b(i)(31 downto 16);
            ch2_txdata(15 downto 0) <= TX_DATA_64b(i)(47 downto 32);
            ch3_txdata(15 downto 0) <= TX_DATA_64b(i)(63 downto 48);

            MON_CPLL_LCK_reg(4*i+3 downto 4*i) <= lcpll_lock & lcpll_lock;

            -- Channels are flipped to use the same pinout as the FLX-712
            -- Transceiver channel -> Data
            -- CH0 -> CH3
            -- CH1 -> CH2
            -- CH2 -> CH1
            -- CH3 -> CH0
            GTH_TOP182_INST: transceiver_versal_lpgbt_wrapper
                port map(
                    APB3_INTF_0_paddr   => x"0000",
                    APB3_INTF_0_penable => '0',
                    APB3_INTF_0_prdata  => open,
                    APB3_INTF_0_pready  => open,
                    APB3_INTF_0_psel    => '0',
                    APB3_INTF_0_pslverr => open,
                    APB3_INTF_0_pwdata  => x"00000000",
                    APB3_INTF_0_pwrite  => '0',
                    GT_REFCLK0_clk_n => GTREFCLK_n(i downto i),
                    GT_REFCLK0_clk_p => GTREFCLK_p(i downto i),
                    GT_Serial_grx_n => RX_N_i(4*i+3 downto 4*i),
                    GT_Serial_grx_p => RX_P_i(4*i+3 downto 4*i),
                    GT_Serial_gtx_n => TX_N_i(4*i+3 downto 4*i),
                    GT_Serial_gtx_p => TX_P_i(4*i+3 downto 4*i),
                    apb3presetn_0 => RX_DATAPATH_RESET_FINL(i),
                    ch0_loopback_0 => "000",
                    ch0_rxcdrlock_ext_0 => rxcdrlock_out(4*i+3 downto 4*i+3),
                    ch0_rxdata_ext_0 => ch3_rxdata,
                    ch0_rxdatavalid_ext_0 => open,
                    ch0_rxgearboxslip_ext_0 => '0',
                    ch0_rxheader_ext_0 => open,
                    -- ch0_rxpmaresetdone_ext_0 => rxpmaresetdone(4*i+0),
                    ch0_rxpolarity_ext_0 => CTRL_RXPOLARITY_rxusrclk(3),
                    ch0_rxresetdone_ext_0 => rxresetdone(4*i+3 downto 4*i+3),
                    ch0_rxslide_ext_0 => RxSlide(4*i+3),
                    ch0_txbufstatus_ext_0 => txbufstatus(4*i+3),
                    ch0_txctrl0_ext_0 => x"0000",
                    ch0_txctrl1_ext_0 => x"0000",
                    ch0_txctrl2_ext_0 => x"00",
                    ch0_txdata_ext_0 => ch3_txdata,
                    ch0_txheader_ext_0 => "000000",
                    ch0_txpippmen_ext_0 => txpippmen_ext(4*i+3),
                    ch0_txpippmstepsize_ext_0 => txpippmstepsize_txuserclk_enable(4*i+3),
                    ch0_txpolarity_ext_0 => CTRL_TXPOLARITY_txusrclk(3),
                    ch0_txresetdone_ext_0 => TxResetDone(4*i+3 downto 4*i+3),
                    ch0_txsequence_ext_0 => "0000000",
                    ch1_loopback_0 => "000",
                    ch1_rxcdrlock_ext_0 => rxcdrlock_out(4*i+2 downto 4*i+2),
                    ch1_rxdata_ext_0 => ch2_rxdata,
                    ch1_rxdatavalid_ext_0 => open,
                    ch1_rxgearboxslip_ext_0 => '0',
                    ch1_rxheader_ext_0 => open,
                    -- ch1_rxpmaresetdone_ext_0 => rxpmaresetdone(4*i+1),
                    ch1_rxpolarity_ext_0 => CTRL_RXPOLARITY_rxusrclk(2),
                    ch1_rxresetdone_ext_0 => rxresetdone(4*i+2 downto 4*i+2),
                    ch1_rxslide_ext_0 => RxSlide(4*i+2),
                    ch1_txbufstatus_ext_0           => txbufstatus(4*i+2),
                    ch1_txctrl0_ext_0 => x"0000",
                    ch1_txctrl1_ext_0 => x"0000",
                    ch1_txctrl2_ext_0 => x"00",
                    ch1_txdata_ext_0 => ch2_txdata,
                    ch1_txpippmen_ext_0             => txpippmen_ext(4*i+2),
                    ch1_txpippmstepsize_ext_0       => txpippmstepsize_txuserclk_enable(4*i+2),
                    ch1_txpolarity_ext_0 => CTRL_TXPOLARITY_txusrclk(1),
                    ch1_txresetdone_ext_0 => TxResetDone(4*i+2 downto 4*i+2),
                    ch1_txsequence_ext_0 => "0000000",
                    ch2_loopback_0 => "000",
                    ch2_rxcdrlock_ext_0 => rxcdrlock_out(4*i+1 downto 4*i+1),
                    ch2_rxdata_ext_0 => ch1_rxdata,
                    ch2_rxdatavalid_ext_0 => open,
                    ch2_rxgearboxslip_ext_0 => '0',
                    ch2_rxheader_ext_0 => open,
                    -- ch2_rxpmaresetdone_ext_0 => rxpmaresetdone(4*i+2),
                    ch2_rxpolarity_ext_0 => CTRL_RXPOLARITY_rxusrclk(1),
                    ch2_rxresetdone_ext_0 => rxresetdone(4*i+1 downto 4*i+1),
                    ch2_rxslide_ext_0 => RxSlide(4*i+1),
                    ch2_txbufstatus_ext_0 => txbufstatus(4*i+1),
                    ch2_txctrl0_ext_0 => x"0000",
                    ch2_txctrl1_ext_0 => x"0000",
                    ch2_txctrl2_ext_0 => x"00",
                    ch2_txdata_ext_0 => ch1_txdata,
                    ch2_txpippmen_ext_0 => txpippmen_ext(4*i+1),
                    ch2_txpippmstepsize_ext_0 => txpippmstepsize_txuserclk_enable(4*i+1),
                    ch2_txpolarity_ext_0 => CTRL_TXPOLARITY_txusrclk(1),
                    ch2_txresetdone_ext_0 => TxResetDone(4*i+1 downto 4*i+1),
                    ch2_txsequence_ext_0 => "0000000",
                    ch3_loopback_0 => "000",
                    ch3_rxcdrlock_ext_0 => rxcdrlock_out(4*i+0 downto 4*i+0),
                    ch3_rxdata_ext_0 => ch0_rxdata,
                    ch3_rxdatavalid_ext_0 => open,
                    ch3_rxgearboxslip_ext_0 => '0',
                    ch3_rxheader_ext_0 => open,
                    -- ch3_rxpmaresetdone_ext_0 => rxpmaresetdone(4*i+3),
                    ch3_rxpolarity_ext_0 => CTRL_RXPOLARITY_rxusrclk(0),
                    ch3_rxresetdone_ext_0 => rxresetdone(4*i+0 downto 4*i+0),
                    ch3_rxslide_ext_0 => RxSlide(4*i+0),
                    ch3_txbufstatus_ext_0 => txbufstatus(4*i+0),
                    ch3_txctrl0_ext_0 => x"0000",
                    ch3_txctrl1_ext_0 => x"0000",
                    ch3_txctrl2_ext_0 => x"00",
                    ch3_txdata_ext_0 => ch0_txdata,
                    ch3_txpippmen_ext_0 => txpippmen_ext(4*i+0),
                    ch3_txpippmstepsize_ext_0 => txpippmstepsize_txuserclk_enable(4*i+0),
                    ch3_txpolarity_ext_0 => CTRL_TXPOLARITY_txusrclk(0),
                    ch3_txresetdone_ext_0 => TxResetDone(4*i+0 downto 4*i+0),
                    ch3_txsequence_ext_0 => "0000000",
                    refclk_320_out => refclk_320(i),
                    reset_rx_datapath_in_0 => RX_DATAPATH_RESET_FINL(i),
                    reset_rx_pll_and_datapath_in_0 => CTRL_RXPLL_DATAPATH_RESET(i),
                    reset_tx_datapath_in_0 => CTRL_TX_DATAPATH_RESET(i),
                    reset_tx_pll_and_datapath_in_0 => CTRL_TXPLL_DATAPATH_RESET(i),
                    rx_resetdone_out_gt_bridge_ip_0 => rxresetdone_quad(i),
                    tx_resetdone_out_gt_bridge_ip_0 => txresetdone_quad(i),
                    rxusrclk0 => GT_RX_WORD_CLK(4*i+3),
                    rxusrclk1 => GT_RX_WORD_CLK(4*i+2),
                    rxusrclk2 => GT_RX_WORD_CLK(4*i+1),
                    rxusrclk3 => GT_RX_WORD_CLK(4*i+0),
                    txusrclk0 => txusrclk3,
                    txusrclk1 => txusrclk2,
                    txusrclk2 => txusrclk1,
                    txusrclk3 => txusrclk0,
                    gtwiz_freerun_clk_0 => drp_clk,
                    gt_reset_all_in_0 => CTRL_SOFT_RESET(i),
                    QUAD0_hsclk0_lcplllock_0 => lcpll_lock(0),
                    QUAD0_hsclk1_lcplllock_0 => lcpll_lock(1)
                );

            GT_REFCLK_OUT(4*i+3 downto 4*i) <= (others => refclk_320(i));
            GT_TX_WORD_CLK(4*i+3 downto 4*i) <= (others => refclk_320(i));
            GT_RXUSRCLK(4*i+3 downto 4*i) <= GT_RX_WORD_CLK(4*i +3 downto 4 * i);
            GT_TXUSRCLK(4*i+3 downto 4*i) <= txusrclk(4*i +3 downto 4 * i);
            txusrclk(4*i+0) <= txusrclk0;
            txusrclk(4*i+1) <= txusrclk1;
            txusrclk(4*i+2) <= txusrclk2;
            txusrclk(4*i+3) <= txusrclk3;

            --zeroing to avoid glitches for auto_gth_rxrst
            rxfsmresetdone(4*i+3 downto 4*i) <= "0000";

            MON_RXRSTDONE_QUAD(i downto i)              <= rxresetdone_quad_clk40(i downto i);
            MON_TXRSTDONE_QUAD(i downto i)              <= txresetdone_quad_clk40(i downto i);
            MON_RXPMARSTDONE(4*i+3 downto 4*i)          <= rxpmaresetdone(4*i+3 downto 4*i);
            MON_TXPMARSTDONE(4*i+3 downto 4*i)          <= txpmaresetdone(4*i+3 downto 4*i);
            MON_RXCDR_LCK_QUAD(i downto i)              <= RxCdrLock_quad(i downto i);

        end generate; --g_182: if CARD_TYPE = 180 or CARD_TYPE = 181 or CARD_TYPE = 182 generate

        process(clk40_in)
        begin
            if clk40_in'event and clk40_in='1' then
                if cdr_cnt ="00000000000000000000" then
                    RxCdrLock_a(4*i)     <= rxcdrlock_out(4*i);
                    RxCdrLock_a(4*i+1)   <= rxcdrlock_out(4*i+1);
                    RxCdrLock_a(4*i+2)   <= rxcdrlock_out(4*i+2);
                    RxCdrLock_a(4*i+3)   <= rxcdrlock_out(4*i+3);
                else
                    RxCdrLock_a(4*i) <= RxCdrLock_a(4*i) and rxcdrlock_out(4*i);
                    RxCdrLock_a(4*i+1) <= RxCdrLock_a(4*i+1) and rxcdrlock_out(4*i+1);
                    RxCdrLock_a(4*i+2) <= RxCdrLock_a(4*i+2) and rxcdrlock_out(4*i+2);
                    RxCdrLock_a(4*i+3) <= RxCdrLock_a(4*i+3) and rxcdrlock_out(4*i+3);
                end if;
                if cdr_cnt="00000000000000000000" then
                    RxCdrLock_int(4*i) <=RxCdrLock_a(4*i);
                    RxCdrLock_int(4*i+1) <=RxCdrLock_a(4*i+1);
                    RxCdrLock_int(4*i+2) <=RxCdrLock_a(4*i+2);
                    RxCdrLock_int(4*i+3) <=RxCdrLock_a(4*i+3);
                end if;
            end if;
        end process;

        RxCdrLock(4*i)      <= (not CTRL_CHANNEL_DISABLE(4*i)) and RxCdrLock_int(4*i);
        RxCdrLock(4*i+1)    <= (not CTRL_CHANNEL_DISABLE(4*i+1)) and RxCdrLock_int(4*i+1);
        RxCdrLock(4*i+2)    <= (not CTRL_CHANNEL_DISABLE(4*i+2)) and RxCdrLock_int(4*i+2);
        RxCdrLock(4*i+3)    <= (not CTRL_CHANNEL_DISABLE(4*i+3)) and RxCdrLock_int(4*i+3);

        MON_RXRSTDONE(4*i+3 downto 4*i)             <= rxresetdone_clk40(4*i+3 downto 4*i);
        MON_TXRSTDONE(4*i+3 downto 4*i)             <= TxResetDone_clk40(4*i+3 downto 4*i);
        MON_RXCDR_LCK(4*i+3 downto 4*i)             <= RxCdrLock(4*i+3 downto 4*i);

    end generate; --GTH_inst

    process(clk40_in)
    begin
        if clk40_in'event and clk40_in='1' then
            cdr_cnt <=cdr_cnt+'1';
        end if;
    end process;


    rst2_712: if CARD_TYPE = 712 or CARD_TYPE=182 or CARD_TYPE=181 or CARD_TYPE = 180 generate
        GTH_inst2 : for i in 0 to (GBT_NUM-1)/4 generate
            userclk_rx_reset_in(i) <=not (rxpmaresetdone(4*i+0) or rxpmaresetdone(4*i+1) or rxpmaresetdone(4*i+2) or rxpmaresetdone(4*i+3));
            userclk_tx_reset_in(i) <=not (txpmaresetdone(4*i+0) or txpmaresetdone(4*i+1) or txpmaresetdone(4*i+2) or txpmaresetdone(4*i+3));

            RX_DATAPATH_RESET_FINL(i) <= CTRL_RX_DATAPATH_RESET(i) or (auto_gth_rxrst(4*i) and RxCdrLock(4*i))
                                         or (auto_gth_rxrst(4*i+1) and RxCdrLock(4*i+1))
                                         or (auto_gth_rxrst(4*i+2) and RxCdrLock(4*i+2))
                                         or (auto_gth_rxrst(4*i+3) and RxCdrLock(4*i+3)) ;

            RX_DATAPATH_RESET_CHANNEL(4*i+3 downto 4*i) <= (CTRL_RX_DATAPATH_RESET(i) or (auto_gth_rxrst(4*i+3) and RxCdrLock(4*i+3))) &
                                                           (CTRL_RX_DATAPATH_RESET(i) or (auto_gth_rxrst(4*i+2) and RxCdrLock(4*i+2))) &
                                                           (CTRL_RX_DATAPATH_RESET(i) or (auto_gth_rxrst(4*i+1) and RxCdrLock(4*i+1))) &
                                                           (CTRL_RX_DATAPATH_RESET(i) or (auto_gth_rxrst(4*i+0) and RxCdrLock(4*i+0)));
        end generate; --GTH_inst2
    end generate; --rst2_712

    rx_rst_709: if CARD_TYPE = 709 generate
        rx_reset_l: for i in 0 to GBT_NUM-1 generate
            gtrx_reset_i(i) <= CTRL_GBTRXRST(i) or auto_gth_rxrst(i);
            gttx_reset_i(i) <= CTRL_GBTTXRST(i);
        end generate;
    end generate; --rx_rst_709

end Behavioral;

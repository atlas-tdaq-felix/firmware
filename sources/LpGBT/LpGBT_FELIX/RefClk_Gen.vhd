--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Kai Chen
--!               Frans Schreuder
--!               Elena Zhivun
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen      (kchen@bnl.gov)
--! modified by: Marco Trovato (mtrovato@anl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2019/05/21 04:43:14 PM
-- Design Name: RefClk_Gen
-- Module Name: RefClk_Gen - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions: Vivado
-- Description:
--              The GTH Reference Clock Selection
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------

LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.numeric_std.ALL;
    use IEEE.numeric_std_unsigned.ALL;

library UNISIM;
    use UNISIM.VComponents.all;
    use work.lpgbtfpga_package.all;
    use work.pcie_package.all;

entity RefClk_Gen is
    Generic (
        GBT_NUM                     : integer   := 24;
        GTREFCLKS                   : integer   := 6;
        GTHREFCLK_SEL               : std_logic := '0';
        -- GREFCLK              : std_logic := '1';
        -- MGTREFCLK            : std_logic := '0';
        CARD_TYPE                   : integer   := 712;
        CLK_CHIP_SEL                : integer   := 1
    -- SI53XX               : integer   := 0;
    -- LMK03200             : integer   := 1

    );
    Port (


        --        GREFCLK_IN                  : in std_logic;
        --SI5345 FLX712, SI5324 VC709
        SI53XX_0_P                  : in std_logic;
        SI53XX_0_N                  : in std_logic;
        SI53XX_2_P                  : in std_logic;
        SI53XX_2_N                  : in std_logic;
        SI53XX_4_P                  : in std_logic;
        SI53XX_4_N                  : in std_logic;
        SI53XX_5_P                  : in std_logic;
        SI53XX_5_N                  : in std_logic;
        SI53XX_8_P                  : in std_logic;
        SI53XX_8_N                  : in std_logic;
        LMK0_P                      : in std_logic;
        LMK0_N                      : in std_logic;
        LMK1_P                      : in std_logic;
        LMK1_N                      : in std_logic;
        LMK2_P                      : in std_logic;
        LMK2_N                      : in std_logic;
        LMK3_P                      : in std_logic;
        LMK3_N                      : in std_logic;
        LMK4_P                      : in std_logic;
        LMK4_N                      : in std_logic;
        LMK5_P                      : in std_logic;
        LMK5_N                      : in std_logic;
        LMK6_P                      : in std_logic; -- @suppress "Unused port: LMK6_P is not used in work.RefClk_Gen(Behavioral)"
        LMK6_N                      : in std_logic; -- @suppress "Unused port: LMK6_N is not used in work.RefClk_Gen(Behavioral)"
        LMK7_P                      : in std_logic; -- @suppress "Unused port: LMK7_P is not used in work.RefClk_Gen(Behavioral)"
        LMK7_N                      : in std_logic; -- @suppress "Unused port: LMK7_N is not used in work.RefClk_Gen(Behavioral)"
        GTREFCLK_P_IN               : in std_logic_vector(GTREFCLKS-1 downto 0); --Only for VCU128, BNL181 and XUPP3R-VU9P
        GTREFCLK_N_IN               : in std_logic_vector(GTREFCLKS-1 downto 0); --Only for VCU128, BNL181 and XUPP3R-VU9P
        GTH_REFCLK_OUT              : OUT std_logic_vector(GBT_NUM-1 downto 0)

    );
end RefClk_Gen;


architecture Behavioral of RefClk_Gen is

    signal LMK0_REFCLK : std_logic;
    signal LMK1_REFCLK : std_logic;
    signal LMK2_REFCLK : std_logic;
    signal LMK3_REFCLK : std_logic;
    signal LMK4_REFCLK : std_logic;
    signal LMK5_REFCLK : std_logic;
    signal SI0_REFCLK  : std_logic;
    signal SI2_REFCLK  : std_logic;
    signal SI4_REFCLK  : std_logic;
    signal SI5_REFCLK  : std_logic;
    signal SI8_REFCLK  : std_logic;
    signal GTH_RefClk  : std_logic_vector(47 downto 0);
    signal CXP1_GTH_RefClk        : std_logic;

begin
    clk_709: if CARD_TYPE = 709 generate
        GTHREFCLK_1 : if GTHREFCLK_SEL = '0' generate
            ibufds_instq2_clk0 : IBUFDS_GTE2
                generic map(
                    CLKCM_CFG => true,
                    CLKRCV_TRST => true,
                    CLKSWING_CFG => "11"
                )
                port map
      (
                    O               =>   CXP1_GTH_RefClk,
                    ODIV2           =>    open,
                    CEB             =>   '0',
                    I               =>   SI53XX_0_P,
                    IB              =>   SI53XX_0_N
                );
            GTH_RefClk(0) <= CXP1_GTH_RefClk;
            GTH_RefClk(1) <= CXP1_GTH_RefClk;
            GTH_RefClk(2) <= CXP1_GTH_RefClk;
            GTH_RefClk(3) <= CXP1_GTH_RefClk;
        end generate;

        --DO I need this? In GBT design it seems to be available
        --GTHREFCLK_2 : if GTHREFCLK_SEL = '1' generate
        --  GTH_RefClk(0)  <= GREFCLK_IN;
        --  GTH_RefClk(1)  <= GREFCLK_IN;
        --  GTH_RefClk(2)  <= GREFCLK_IN;
        --  GTH_RefClk(3)  <= GREFCLK_IN;
        --end generate;

        g_refclk_vc709: if (GBT_NUM = 4) generate
            GTH_REFCLK_OUT(GBT_NUM-1 downto 0) <= GTH_RefClk(GBT_NUM-1 downto 0);
        end generate;


    end generate; --clk_709

    clk_712: if CARD_TYPE = 712 generate              --
        lmk0_gen : IBUFDS_GTE3
            generic map(
                REFCLK_EN_TX_PATH => '0',
                REFCLK_HROW_CK_SEL => "00",
                REFCLK_ICNTL_RX => "00"
            )
            port map
    (
                O               =>       LMK0_REFCLK,
                ODIV2           =>      open,
                CEB             =>       '0',
                I               =>       LMK0_P,
                IB              =>       LMK0_N
            );

        lmk1_gen : IBUFDS_GTE3
            generic map(
                REFCLK_EN_TX_PATH => '0',
                REFCLK_HROW_CK_SEL => "00",
                REFCLK_ICNTL_RX => "00"
            )
            port map
      (
                O               =>      LMK1_REFCLK,
                ODIV2           =>      open,
                CEB             =>      '0',
                I               =>      LMK1_P,
                IB              =>      LMK1_N
            );

        lmk2_gen : IBUFDS_GTE3
            generic map(
                REFCLK_EN_TX_PATH => '0',
                REFCLK_HROW_CK_SEL => "00",
                REFCLK_ICNTL_RX => "00"
            )
            port map
    (
                O               =>      LMK2_REFCLK,
                ODIV2           =>      open,
                CEB             =>      '0',
                I               =>      LMK2_P,
                IB              =>      LMK2_N
            );

        lmk3_gen : IBUFDS_GTE3
            generic map(
                REFCLK_EN_TX_PATH => '0',
                REFCLK_HROW_CK_SEL => "00",
                REFCLK_ICNTL_RX => "00"
            )
            port map
    (
                O               =>      LMK3_REFCLK,
                ODIV2           =>      open,
                CEB             =>      '0',
                I               =>      LMK3_P,
                IB              =>      LMK3_N
            );

        lmk4_gen : IBUFDS_GTE3
            generic map(
                REFCLK_EN_TX_PATH => '0',
                REFCLK_HROW_CK_SEL => "00",
                REFCLK_ICNTL_RX => "00"
            )
            port map
    (
                O               =>      LMK4_REFCLK,
                ODIV2           =>      open,
                CEB             =>      '0',
                I               =>      LMK4_P,
                IB              =>      LMK4_N
            );

        lmk5_gen : IBUFDS_GTE3
            generic map(
                REFCLK_EN_TX_PATH => '0',
                REFCLK_HROW_CK_SEL => "00",
                REFCLK_ICNTL_RX => "00"
            )
            port map
    (
                O               =>      LMK5_REFCLK,
                ODIV2           =>      open,
                CEB             =>      '0',
                I               =>      LMK5_P,
                IB              =>      LMK5_N
            );

        --        lmk6_gen : IBUFDS_GTE3
        --            generic map(
        --                REFCLK_EN_TX_PATH => '0',
        --                REFCLK_HROW_CK_SEL => "00",
        --                REFCLK_ICNTL_RX => "00"
        --            )
        --            port map
        --    (
        --                O               =>      LMK6_REFCLK,
        --                ODIV2           =>      open,
        --                CEB             =>      '0',
        --                I               =>      LMK6_P,
        --                IB              =>      LMK6_N
        --            );
        --
        --        lmk7_gen : IBUFDS_GTE3
        --            generic map(
        --                REFCLK_EN_TX_PATH => '0',
        --                REFCLK_HROW_CK_SEL => "00",
        --                REFCLK_ICNTL_RX => "00"
        --            )
        --            port map
        --    (
        --                O               =>      LMK7_REFCLK,
        --                ODIV2           =>      open,
        --                CEB             =>      '0',
        --                I               =>      LMK7_P,
        --                IB              =>      LMK7_N
        --            );


        si0_gen : IBUFDS_GTE3
            generic map(
                REFCLK_EN_TX_PATH => '0',
                REFCLK_HROW_CK_SEL => "00",
                REFCLK_ICNTL_RX => "00"
            )
            port map
    (
                O               =>      SI0_REFCLK,
                ODIV2           =>      open,
                CEB             =>      '0',
                I               =>      SI53XX_0_P,
                IB              =>      SI53XX_0_N
            );

        si2_gen : IBUFDS_GTE3
            generic map(
                REFCLK_EN_TX_PATH => '0',
                REFCLK_HROW_CK_SEL => "00",
                REFCLK_ICNTL_RX => "00"
            )
            port map
    (
                O               =>      SI2_REFCLK,
                ODIV2           =>      open,
                CEB             =>      '0',
                I               =>      SI53XX_2_P,
                IB              =>      SI53XX_2_N
            );

        si4_gen : IBUFDS_GTE3
            generic map(
                REFCLK_EN_TX_PATH => '0',
                REFCLK_HROW_CK_SEL => "00",
                REFCLK_ICNTL_RX => "00"
            )
            port map
    (
                O               =>      SI4_REFCLK,
                ODIV2           =>      open,
                CEB             =>      '0',
                I               =>      SI53XX_4_P,
                IB              =>      SI53XX_4_N
            );

        si5_gen : IBUFDS_GTE3
            generic map(
                REFCLK_EN_TX_PATH => '0',
                REFCLK_HROW_CK_SEL => "00",
                REFCLK_ICNTL_RX => "00"
            )
            port map
    (
                O               =>      SI5_REFCLK,
                ODIV2           =>      open,
                CEB             =>      '0',
                I               =>      SI53XX_5_P,
                IB              =>      SI53XX_5_N
            );

        si8_gen : IBUFDS_GTE3
            generic map(
                REFCLK_EN_TX_PATH => '0',
                REFCLK_HROW_CK_SEL => "00",
                REFCLK_ICNTL_RX => "00"
            )
            port map
    (
                O               =>      SI8_REFCLK,
                ODIV2           =>      open,
                CEB             =>      '0',
                I               =>      SI53XX_8_P,
                IB              =>      SI53XX_8_N
            );

        g_refclk_48ch: if ((24 < GBT_NUM) and (GBT_NUM <= 48) and (CLK_CHIP_SEL = 1)) generate

            GTH_RefClk( 0)         <= LMK1_REFCLK;
            GTH_RefClk( 1)         <= LMK1_REFCLK;
            GTH_RefClk( 2)         <= LMK1_REFCLK;
            GTH_RefClk( 3)         <= LMK1_REFCLK;
            GTH_RefClk( 4)         <= LMK1_REFCLK;
            GTH_RefClk( 5)         <= LMK1_REFCLK;
            GTH_RefClk( 6)         <= LMK1_REFCLK;
            GTH_RefClk( 7)         <= LMK1_REFCLK;
            GTH_RefClk( 8)         <= LMK0_REFCLK;
            GTH_RefClk( 9)         <= LMK0_REFCLK;
            GTH_RefClk(10)         <= LMK0_REFCLK;
            GTH_RefClk(11)         <= LMK0_REFCLK;
            GTH_RefClk(12)         <= LMK4_REFCLK;
            GTH_RefClk(13)         <= LMK4_REFCLK;
            GTH_RefClk(14)         <= LMK4_REFCLK;
            GTH_RefClk(15)         <= LMK4_REFCLK;
            GTH_RefClk(16)         <= LMK5_REFCLK;
            GTH_RefClk(17)         <= LMK5_REFCLK;
            GTH_RefClk(18)         <= LMK5_REFCLK;
            GTH_RefClk(19)         <= LMK5_REFCLK;
            GTH_RefClk(20)         <= LMK5_REFCLK;
            GTH_RefClk(21)         <= LMK5_REFCLK;
            GTH_RefClk(22)         <= LMK5_REFCLK;
            GTH_RefClk(23)         <= LMK5_REFCLK;

            GTH_RefClk(24)         <= LMK3_REFCLK;
            GTH_RefClk(25)         <= LMK3_REFCLK;
            GTH_RefClk(26)         <= LMK3_REFCLK;
            GTH_RefClk(27)         <= LMK3_REFCLK;
            GTH_RefClk(28)         <= LMK3_REFCLK;
            GTH_RefClk(29)         <= LMK3_REFCLK;
            GTH_RefClk(30)         <= LMK3_REFCLK;
            GTH_RefClk(31)         <= LMK3_REFCLK;
            GTH_RefClk(32)         <= LMK2_REFCLK;
            GTH_RefClk(33)         <= LMK2_REFCLK;
            GTH_RefClk(34)         <= LMK2_REFCLK;
            GTH_RefClk(35)         <= LMK2_REFCLK;
        --GTH_RefClk(36)         <= LMK6_REFCLK;
        --GTH_RefClk(37)         <= LMK6_REFCLK;
        --GTH_RefClk(38)         <= LMK6_REFCLK;
        --GTH_RefClk(39)         <= LMK6_REFCLK;
        --GTH_RefClk(40)         <= LMK6_REFCLK;
        --GTH_RefClk(41)         <= LMK6_REFCLK;
        --GTH_RefClk(42)         <= LMK6_REFCLK;
        --GTH_RefClk(43)         <= LMK6_REFCLK;
        --GTH_RefClk(44)         <= LMK7_REFCLK;
        --GTH_RefClk(45)         <= LMK7_REFCLK;
        --GTH_RefClk(46)         <= LMK7_REFCLK;
        --GTH_RefClk(47)         <= LMK7_REFCLK;

        end generate g_refclk_48ch;

        g_refclk_24ch: if ( (8 < GBT_NUM) and (GBT_NUM <= 24) and (CLK_CHIP_SEL = 1) ) generate

            GTH_RefClk( 0)         <= LMK1_REFCLK;
            GTH_RefClk( 1)         <= LMK1_REFCLK;
            GTH_RefClk( 2)         <= LMK1_REFCLK;
            GTH_RefClk( 3)         <= LMK1_REFCLK;
            GTH_RefClk( 4)         <= LMK1_REFCLK;
            GTH_RefClk( 5)         <= LMK1_REFCLK;
            GTH_RefClk( 6)         <= LMK1_REFCLK;
            GTH_RefClk( 7)         <= LMK1_REFCLK;
            GTH_RefClk( 8)         <= LMK0_REFCLK;
            GTH_RefClk( 9)         <= LMK0_REFCLK;
            GTH_RefClk(10)         <= LMK0_REFCLK;
            GTH_RefClk(11)         <= LMK0_REFCLK;

            GTH_RefClk(12)         <= LMK3_REFCLK;
            GTH_RefClk(13)         <= LMK3_REFCLK;
            GTH_RefClk(14)         <= LMK3_REFCLK;
            GTH_RefClk(15)         <= LMK3_REFCLK;
            GTH_RefClk(16)         <= LMK3_REFCLK;
            GTH_RefClk(17)         <= LMK3_REFCLK;
            GTH_RefClk(18)         <= LMK3_REFCLK;
            GTH_RefClk(19)         <= LMK3_REFCLK;
            GTH_RefClk(20)         <= LMK2_REFCLK;
            GTH_RefClk(21)         <= LMK2_REFCLK;
            GTH_RefClk(22)         <= LMK2_REFCLK;
            GTH_RefClk(23)         <= LMK2_REFCLK;
        end generate g_refclk_24ch;

        g_refclk_8ch: if ( (GBT_NUM <= 8) and (CLK_CHIP_SEL = 1) ) generate

            GTH_RefClk( 0)         <= LMK1_REFCLK;
            GTH_RefClk( 1)         <= LMK1_REFCLK;
            GTH_RefClk( 2)         <= LMK1_REFCLK;
            GTH_RefClk( 3)         <= LMK1_REFCLK;

            GTH_RefClk( 4)         <= LMK3_REFCLK;
            GTH_RefClk( 5)         <= LMK3_REFCLK;
            GTH_RefClk( 6)         <= LMK3_REFCLK;
            GTH_RefClk( 7)         <= LMK3_REFCLK;

        end generate g_refclk_8ch;

        g_refclk_48ch_SI: if ((24 < GBT_NUM) and (GBT_NUM <= 48) and (CLK_CHIP_SEL = 0)) generate

            GTH_RefClk( 0)         <= SI0_REFCLK;
            GTH_RefClk( 1)         <= SI0_REFCLK;
            GTH_RefClk( 2)         <= SI0_REFCLK;
            GTH_RefClk( 3)         <= SI0_REFCLK;
            GTH_RefClk( 4)         <= SI0_REFCLK;
            GTH_RefClk( 5)         <= SI0_REFCLK;
            GTH_RefClk( 6)         <= SI0_REFCLK;
            GTH_RefClk( 7)         <= SI0_REFCLK;
            GTH_RefClk( 8)         <= SI0_REFCLK;
            GTH_RefClk( 9)         <= SI0_REFCLK;
            GTH_RefClk(10)         <= SI0_REFCLK;
            GTH_RefClk(11)         <= SI0_REFCLK;
            GTH_RefClk(12)         <= SI4_REFCLK;
            GTH_RefClk(13)         <= SI4_REFCLK;
            GTH_RefClk(14)         <= SI4_REFCLK;
            GTH_RefClk(15)         <= SI4_REFCLK;
            GTH_RefClk(16)         <= SI8_REFCLK;
            GTH_RefClk(17)         <= SI8_REFCLK;
            GTH_RefClk(18)         <= SI8_REFCLK;
            GTH_RefClk(19)         <= SI8_REFCLK;
            GTH_RefClk(20)         <= SI8_REFCLK;
            GTH_RefClk(21)         <= SI8_REFCLK;
            GTH_RefClk(22)         <= SI8_REFCLK;
            GTH_RefClk(23)         <= SI8_REFCLK;

            GTH_RefClk(24)         <= SI2_REFCLK;
            GTH_RefClk(25)         <= SI2_REFCLK;
            GTH_RefClk(26)         <= SI2_REFCLK;
            GTH_RefClk(27)         <= SI2_REFCLK;
            GTH_RefClk(28)         <= SI2_REFCLK;
            GTH_RefClk(29)         <= SI2_REFCLK;
            GTH_RefClk(30)         <= SI2_REFCLK;
            GTH_RefClk(31)         <= SI2_REFCLK;
            GTH_RefClk(32)         <= SI2_REFCLK;
            GTH_RefClk(33)         <= SI2_REFCLK;
            GTH_RefClk(34)         <= SI2_REFCLK;
            GTH_RefClk(35)         <= SI2_REFCLK;
            GTH_RefClk(36)         <= SI5_REFCLK;
            GTH_RefClk(37)         <= SI5_REFCLK;
            GTH_RefClk(38)         <= SI5_REFCLK;
            GTH_RefClk(39)         <= SI5_REFCLK;
            GTH_RefClk(40)         <= SI5_REFCLK;
            GTH_RefClk(41)         <= SI5_REFCLK;
            GTH_RefClk(42)         <= SI5_REFCLK;
            GTH_RefClk(43)         <= SI5_REFCLK;
            GTH_RefClk(44)         <= SI5_REFCLK;
            GTH_RefClk(45)         <= SI5_REFCLK;
            GTH_RefClk(46)         <= SI5_REFCLK;
            GTH_RefClk(47)         <= SI5_REFCLK;

        end generate g_refclk_48ch_SI;

        g_refclk_24ch_SI: if ((8 < GBT_NUM) and (GBT_NUM <= 24) and (CLK_CHIP_SEL = 0)) generate

            GTH_RefClk( 0)         <= SI0_REFCLK;
            GTH_RefClk( 1)         <= SI0_REFCLK;
            GTH_RefClk( 2)         <= SI0_REFCLK;
            GTH_RefClk( 3)         <= SI0_REFCLK;
            GTH_RefClk( 4)         <= SI0_REFCLK;
            GTH_RefClk( 5)         <= SI0_REFCLK;
            GTH_RefClk( 6)         <= SI0_REFCLK;
            GTH_RefClk( 7)         <= SI0_REFCLK;
            GTH_RefClk( 8)         <= SI0_REFCLK;
            GTH_RefClk( 9)         <= SI0_REFCLK;
            GTH_RefClk(10)         <= SI0_REFCLK;
            GTH_RefClk(11)         <= SI0_REFCLK;

            GTH_RefClk(12)         <= SI2_REFCLK;
            GTH_RefClk(13)         <= SI2_REFCLK;
            GTH_RefClk(14)         <= SI2_REFCLK;
            GTH_RefClk(15)         <= SI2_REFCLK;
            GTH_RefClk(16)         <= SI2_REFCLK;
            GTH_RefClk(17)         <= SI2_REFCLK;
            GTH_RefClk(18)         <= SI2_REFCLK;
            GTH_RefClk(19)         <= SI2_REFCLK;
            GTH_RefClk(20)         <= SI2_REFCLK;
            GTH_RefClk(21)         <= SI2_REFCLK;
            GTH_RefClk(22)         <= SI2_REFCLK;
            GTH_RefClk(23)         <= SI2_REFCLK;
        end generate g_refclk_24ch_SI;

        g_refclk_8ch_SI: if ( (GBT_NUM <= 8) and (CLK_CHIP_SEL = 0)) generate

            GTH_RefClk( 0)         <= SI0_REFCLK;
            GTH_RefClk( 1)         <= SI0_REFCLK;
            GTH_RefClk( 2)         <= SI0_REFCLK;
            GTH_RefClk( 3)         <= SI0_REFCLK;
            GTH_RefClk( 4)         <= SI2_REFCLK;
            GTH_RefClk( 5)         <= SI2_REFCLK;
            GTH_RefClk( 6)         <= SI2_REFCLK;
            GTH_RefClk( 7)         <= SI2_REFCLK;

        end generate g_refclk_8ch_SI;

        g_refclk: if (GBT_NUM <= 48) generate
            GTH_REFCLK_OUT(GBT_NUM-1 downto 0) <= GTH_RefClk(GBT_NUM-1 downto 0);
        end generate;

    end generate; --clk_712
    g_VUP: if CARD_TYPE = 128 or CARD_TYPE = 800 generate
        signal GTREFCLK: std_logic_vector(GBT_NUM/4-1 downto 0);
    begin
        g_IBUFDS_GTE4_i: for i in 0 to GBT_NUM/4-1 generate
            gtrefclk0_clk0: IBUFDS_GTE4
                generic map (
                    REFCLK_EN_TX_PATH => '0',   -- Refer to Transceiver User Guide
                    REFCLK_HROW_CK_SEL => "00", -- Refer to Transceiver User Guide
                    REFCLK_ICNTL_RX => "00"     -- Refer to Transceiver User Guide
                )
                port map (
                    O     => GTREFCLK(i),
                    ODIV2 => open,
                    CEB   => '0',
                    I               =>   GTREFCLK_P_IN(i),
                    IB              =>   GTREFCLK_N_IN(i)
                );
            GTH_RefClk(i*4+0) <= GTREFCLK(i);
            GTH_RefClk(i*4+1) <= GTREFCLK(i);
            GTH_RefClk(i*4+2) <= GTREFCLK(i);
            GTH_RefClk(i*4+3) <= GTREFCLK(i);
        end generate;
    end generate;


end Behavioral;

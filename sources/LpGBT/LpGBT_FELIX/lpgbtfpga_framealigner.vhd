-------------------------------------------------------
--! @file
--! @author Julian Mendez <julian.mendez@cern.ch> (CERN - EP-ESE-BE)
--! @version 2.0
--! @brief MGT word aligner (Pattern search)
-------------------------------------------------------

-- This VHDL code represents the implementation of a frame aligner module (lpgbtfpga_framealigner) for MGT word alignment.
-- It includes logic for pattern search, bitslip control, header detection, and state management to ensure proper alignment and synchronization of incoming data frames.

--! Include the IEEE VHDL standard LIBRARY
LIBRARY ieee;
    USE ieee.std_logic_1164.all;
    USE ieee.numeric_std.all;

--! Include the lpGBT-FPGA specific package
    USE work.lpgbtfpga_package.all;

--! @brief lpgbtfpga_framealigner - MGT word aligner (Pattern search)
--! @details
--! The lpgbtfpga_framealigner module ask for clock shift to align the
--! frame header.
ENTITY lpgbtfpga_framealigner IS
    GENERIC (
        c_wordRatio                      : integer;             --! Word ration: frameclock / mgt_wordclock
        c_wordSize                       : integer;             --! Size of the mgt word (32 b)
        c_headerPattern                  : std_logic_vector;    --! Header pattern specified by the standard
        c_allowedFalseHeader             : integer;             --! Number of false header allowed to avoid unlock on frame error // 32
        c_allowedFalseHeaderOverN        : integer;             --! Number of header checked to know wether the lock is lost or not
        c_requiredTrueHeader             : integer;             --! Number of true header required to go in locked state // 30
        c_MaxFecErrors                   : integer;             --! Max FEC errors before to go in locked state // 5

        c_bitslip_mindly                 : integer := 1;        --! Number of clock cycle required WHEN asserting the bitslip SIGNAL
        c_bitslip_waitdly                : integer := 40        --! Number of clock cycle required before being back in a stable state
    );
    PORT (
        -- Clock(s)

        clk_pcsRx_i                      : in  std_logic;       --! MGT Wordclock (320 MHz)
        -- Reset(s)
        rst_pattsearch_i                 : in  std_logic;       --! Rst the pattern search state machines

        -- Control
        cmd_bitslipCtrl_o                : out std_logic;       --! Bitslip SIGNAL to shift the parrallel word

        -- Status
        sta_headerFecLocked_o            : out std_logic;    --! Status: header is FEC are locked
        sta_headerLocked_o               : out std_logic;       --! Status: header is locked
        sta_headerFlag_o                 : out std_logic;       --! Status: header flag (1 pulse over c_wordRatio)
        sta_bitSlipEven_o                : out std_logic;       --!  Status: number of bit slips is even

        -- Data
        dat_word_i                       : in  std_logic_vector(c_headerPattern'length-1 downto 0);  --! Header bits from the MGT word (compared with c_headerPattern);

        -- FEC error
        fec_error_i                        : in std_logic
    );
END lpgbtfpga_framealigner;

--! @brief lpgbtfpga_framealigner - MGT word aligner (Pattern search)
--! @details
--! Check IF the header set on dat_word_i is equal to the c_headerPattern everytime a full
--! loop has been executed (c_wordRatio clock cycles). Manage the bitslip SIGNAL to shift
--! the mgt parallel word until the header is aligned.


ARCHITECTURE behavioral OF lpgbtfpga_framealigner IS

    --================================ Signal Declarations ================================--
    TYPE machine IS (UNLOCKED, CHECK_HEADER, LOCKED, GOING_UNLOCK); --GOING_LOCK
    SIGNAL state                 : machine;

    SIGNAL psAddress             : integer RANGE 0 TO c_wordRatio; -- Declares a signal psAddress to keep track of the pattern search address within a range determined by c_wordRatio.
    SIGNAL shiftPsAddr           : std_logic;
    SIGNAL bitSlipCmd_s          : std_logic;
    SIGNAL headerFlag_s          : std_logic;
    SIGNAL sta_headerLocked_s    : std_logic;
    SIGNAL sta_headerFecLocked_s    : std_logic;
    SIGNAL bitSlipCounter_s      : integer RANGE 0 TO c_wordSize; -- changed c_wordSize+1 to c_wordSize

    SIGNAL cmd_bitslipDone_s     : std_logic;

    SIGNAL sta_bitSlipEven_s     : std_logic;

    TYPE rxBitSlipCtrlStateLatOpt_T IS (e0_idle, e4_doBitslip, e5_waitNcycles);
    SIGNAL stateBitSlip          : rxBitSlipCtrlStateLatOpt_T;

    SIGNAL dat_word_s            :  std_logic_vector(c_headerPattern'length-1 downto 0);

    signal num_fec_errors        : integer range 0 to c_MaxFecErrors;
    signal consec_false_headers  : integer range 0 to c_allowedFalseHeader;
    signal consec_correct_headers: integer range 0 to c_requiredTrueHeader;


--=================================================================================================--
BEGIN                 --========####   Architecture Body   ####========--
    --=================================================================================================--

    --==================================== User Logic =====================================--

    -- Process for handling data word pipeline logic, which updates dat_word_s based on clock and reset signals.

    rxWordPipeline_proc: PROCESS(rst_pattsearch_i, clk_pcsRx_i)
    BEGIN
        IF rst_pattsearch_i = '1' THEN
            dat_word_s <= (OTHERS => '0');
        ELSIF rising_edge(clk_pcsRx_i) THEN
            dat_word_s <= dat_word_i;
        END IF;
    END PROCESS;

    --! MGT: Bitslip controller

    -- Process for managing the bitslip control logic, including state transitions and timing.

    clkSlipProcess: PROCESS(rst_pattsearch_i, clk_pcsRx_i)
        VARIABLE timer                            : integer RANGE 0 TO (c_bitslip_waitdly+c_bitslip_mindly);
    BEGIN

        IF rst_pattsearch_i = '1' THEN
            stateBitSlip        <= e0_idle;
            cmd_bitslipCtrl_o   <= '0';
            cmd_bitslipDone_s   <= '0';
            timer := 0;

        ELSIF rising_edge(clk_pcsRx_i) THEN
            CASE stateBitSlip is
                WHEN e0_idle =>         cmd_bitslipDone_s <= '1';

                    IF bitSlipCmd_s = '1' THEN
                        stateBitSlip       <= e4_doBitslip;
                        timer              := 0;
                        cmd_bitslipDone_s  <= '0';
                    END IF;

                WHEN e4_doBitslip =>    cmd_bitslipCtrl_o <= '1';
                    IF timer = c_bitslip_mindly-1 THEN
                        stateBitSlip <= e5_waitNcycles;
                        timer := 0;
                    ELSE
                        timer := timer + 1;
                    END IF;

                WHEN e5_waitNcycles =>  cmd_bitslipCtrl_o <= '0';
                    IF timer = c_bitslip_waitdly-1 THEN
                        stateBitSlip <= e0_idle;
                    ELSE
                        timer := timer + 1;
                    END IF;
            END CASE;
        END IF;

    END PROCESS;

    sta_bitSlipEven_o  <= sta_bitSlipEven_s ;

    sta_headerLocked_o <= sta_headerLocked_s;
    sta_headerFecLocked_o <= sta_headerFecLocked_s;

    --! Pattern searcher: check the header and ask for bitslip

    -- Process for performing pattern search and generating bitslip commands based on the received data word.

    patternSearch_proc: PROCESS(rst_pattsearch_i, clk_pcsRx_i)
    BEGIN

        IF rst_pattsearch_i = '1' THEN
            bitSlipCmd_s       <= '0';
            sta_bitSlipEven_s  <= '1';
            bitSlipCounter_s   <= 0;
            shiftPsAddr        <= '0';

        ELSIF rising_edge(clk_pcsRx_i) THEN
            bitSlipCmd_s <= '0';
            shiftPsAddr  <= '0';

            IF state = UNLOCKED and psAddress = 0 and cmd_bitslipDone_s = '1' THEN

                IF (dat_word_s(c_headerPattern'length-1 downto 0) /= c_headerPattern) THEN

                    if bitSlipCounter_s < c_wordSize then -- it should be bitSlipCounter_s < c_wordSize
                        sta_bitSlipEven_s  <= not(sta_bitSlipEven_s); -- Why we need this?
                        bitSlipCmd_s       <= '1';
                        bitSlipCounter_s   <= bitSlipCounter_s + 1;
                    else
                        shiftPsAddr      <= '1'; -- MGT word jump is done only in PCS mode
                        bitSlipCounter_s <= 0;
                    end if;

                END IF;

            END IF;
        END IF;
    END PROCESS;

    --! Pattern search address controller

    -- Process for controlling the pattern search address and updating psAddress.

    patternSearchAddr_proc: PROCESS(rst_pattsearch_i, clk_pcsRx_i)
    BEGIN

        IF rst_pattsearch_i = '1' THEN
            psAddress         <= 0;
            headerFlag_s      <= '0';

        ELSIF rising_edge(clk_pcsRx_i) THEN
            headerFlag_s     <= '0';

            IF psAddress = 0 THEN
                headerFlag_s <= '1';
            END IF;

            IF shiftPsAddr = '0' THEN
                psAddress <= psAddress + 1;

                IF psAddress = c_wordRatio-1 THEN
                    psAddress <= 0;
                END IF;

            END IF;
        END IF;
    END PROCESS;


    --! Header locked state machine

    -- Process implementing the header locked state machine logic, determining the state based on received data word and FEC errors.

    lockFSM_proc: PROCESS(rst_pattsearch_i, clk_pcsRx_i)
        VARIABLE consecFalseHeaders        : integer RANGE 0 TO c_allowedFalseHeader;
        VARIABLE consecCorrectHeaders      : integer RANGE 0 TO c_requiredTrueHeader;
        VARIABLE nbCheckedHeaders          : integer RANGE 0 TO c_allowedFalseHeaderOverN;
        VARIABLE numFecErrors              : integer RANGE 0 TO c_MaxFecErrors;
    BEGIN
        IF rst_pattsearch_i = '1' THEN
            state               <= UNLOCKED;
            consecFalseHeaders  := 0;
            consecCorrectHeaders:= 0;
            nbCheckedHeaders    := 0;
            numFecErrors        := 0;
            consecCorrectHeaders := 0;
            consecFalseHeaders := 0;
            nbCheckedHeaders := 0;
        ELSIF rising_edge(clk_pcsRx_i) THEN

            -- Added the following signals for ILA
            num_fec_errors <= numFecErrors;
            consec_false_headers <= consecFalseHeaders;
            consec_correct_headers <= consecCorrectHeaders;

            IF psAddress = 0 THEN

                --            IF (fec_error_i = '1' AND numFecErrors < c_MaxFecErrors) THEN
                --                numFecErrors := numFecErrors + 1;
                --            END IF;

                CASE state IS
                    WHEN UNLOCKED =>
                        IF (dat_word_s(c_headerPattern'length-1 DOWNTO 0) = c_headerPattern AND fec_error_i = '0') THEN
                            state <= CHECK_HEADER;
                            consecCorrectHeaders := 0;

                        END IF;

                    WHEN CHECK_HEADER =>
                        IF (dat_word_s(c_headerPattern'length-1 DOWNTO 0) /= c_headerPattern OR fec_error_i = '1') THEN
                            state <= UNLOCKED;
                        ELSE
                            consecCorrectHeaders := consecCorrectHeaders + 1;
                            IF (consecCorrectHeaders = c_requiredTrueHeader) THEN
                                state <= LOCKED; -- GOING_LOCK;
                            END IF;
                        END IF;

                    --                WHEN GOING_LOCK =>
                    --                    IF (numFecErrors = c_MaxFecErrors) THEN  -- It was numFecErrors = c_MaxFecErrors
                    --                        state <= UNLOCKED;
                    --                    ELSE
                    --                        state <= LOCKED;
                    --                    END IF;

                    WHEN LOCKED =>
                        --                    IF (numFecErrors = c_MaxFecErrors) THEN
                        --                        state <= UNLOCKED;
                        --                        numFecErrors := 0;  -- Reset numFecErrors when transitioning to UNLOCKED
                        --                    ELS
                        IF (dat_word_s(c_headerPattern'length-1 DOWNTO 0) /= c_headerPattern OR fec_error_i = '1') THEN
                            state <= GOING_UNLOCK;
                            consecFalseHeaders := 0;  -- Reset after transition
                            nbCheckedHeaders   := 0;

                        END IF;

                    WHEN GOING_UNLOCK =>
                        --                    IF (numFecErrors >= c_MaxFecErrors) THEN
                        --                        state <= UNLOCKED;
                        --                    ELS

                        IF (dat_word_s(c_headerPattern'length-1 DOWNTO 0) = c_headerPattern AND fec_error_i = '0') THEN

                            IF nbCheckedHeaders = c_allowedFalseHeaderOverN THEN
                                state <= LOCKED;
                            ELSE
                                nbCheckedHeaders := nbCheckedHeaders + 1;
                            END IF;
                        ELSE
                            consecFalseHeaders := consecFalseHeaders + 1;
                            IF consecFalseHeaders = c_allowedFalseHeader THEN
                                state <= UNLOCKED;
                            END IF;
                        END IF;

                END CASE;
            END IF;
        END IF;
    END PROCESS;

    -- Process for synchronizing and updating the header locked status based on the current state.

    headerLocked_sync: PROCESS(rst_pattsearch_i, clk_pcsRx_i)
    BEGIN
        IF rst_pattsearch_i = '1' THEN
            sta_headerLocked_s <= '0';
            sta_headerFecLocked_s <= '0';
        ELSIF rising_edge(clk_pcsRx_i) THEN
            IF psAddress = 0 THEN
                IF state = LOCKED or state = GOING_UNLOCK THEN
                    sta_headerFecLocked_s <= '1';
                ELSE
                    sta_headerFecLocked_s <= '0';
                END IF;
                IF state /= UNLOCKED THEN
                    sta_headerLocked_s <= '1';
                ELSE
                    sta_headerLocked_s <= '0';
                END IF;
            END IF;
        END IF;
    END PROCESS;

    sta_headerFlag_o        <= '1' when psAddress = 7 else '0'; -- headerFlag_s WHEN (state /= UNLOCKED) ELSE '0';
--=====================================================================================--


END behavioral;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

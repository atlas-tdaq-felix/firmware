--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Kai Chen
--!               mtrovato
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen    (kchen@bnl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2019/05/21 04:43:14 PM
-- Design Name: Regs_RW
-- Module Name: Regs_RW - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions: Vivado
-- Description:
--              The Registers R/W
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------

LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.numeric_std.ALL;
    use IEEE.numeric_std_unsigned.ALL;

library UNISIM;
    use UNISIM.VComponents.all;
    use work.lpgbtfpga_package.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;

entity Regs_RW is
    Generic (
        GBT_NUM                     : integer   := 24;
        CARD_TYPE                   : integer   := 712
    );
    Port (
        --VC709
        SOFT_TXRST_ALL                : out std_logic_vector(11 downto 0);
        SOFT_RXRST_ALL                : out std_logic_vector(11 downto 0);
        QPLL_RESET                    : out std_logic_vector(GBT_NUM/4-1 downto 0);
        --
        CTRL_SOFT_RESET             : out std_logic_vector(GBT_NUM/4-1 downto 0);
        CTRL_TXPLL_DATAPATH_RESET   : out std_logic_vector(GBT_NUM/4-1 downto 0);
        CTRL_RXPLL_DATAPATH_RESET   : out std_logic_vector(GBT_NUM/4-1 downto 0);
        CTRL_TX_DATAPATH_RESET      : out std_logic_vector(GBT_NUM/4-1 downto 0);
        CTRL_RX_DATAPATH_RESET      : out std_logic_vector(GBT_NUM/4-1 downto 0);
        CTRL_TXPOLARITY             : out std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_RXPOLARITY             : out std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_GBTTXRST               : out std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_GBTRXRST               : out std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_CHANNEL_DISABLE        : out std_logic_vector(GBT_NUM-1 downto 0);
        --        CTRL_DATARATE               : out std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_FECMODE                : out std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_GBT_General_ctrl       : out std_logic_vector(63 downto 0);
        CTRL_MULTICYCLE_DELAY       : out std_logic_vector(2 downto 0);



        MON_RXRSTDONE               : in std_logic_vector(GBT_NUM-1 downto 0);
        MON_TXRSTDONE               : in std_logic_vector(GBT_NUM-1 downto 0);
        MON_TXFSMRESETDONE          : in std_logic_vector(GBT_NUM-1 downto 0);
        MON_RXFSMRESETDONE          : in std_logic_vector(GBT_NUM-1 downto 0);
        MON_RXPMARSTDONE            : in std_logic_vector(GBT_NUM-1 downto 0);
        MON_TXPMARSTDONE            : in std_logic_vector(GBT_NUM-1 downto 0);
        MON_RXCDR_LCK               : in std_logic_vector(GBT_NUM-1 downto 0);
        MON_QPLL_LCK                : in std_logic_vector(GBT_NUM/4-1 downto 0);
        MON_CPLL_LCK                : in std_logic_vector(GBT_NUM-1 downto 0);
        MON_ALIGNMENT_DONE          : in std_logic_vector(GBT_NUM-1 downto 0);
        MON_FEC_ERROR               : in std_logic_vector(GBT_NUM-1 downto 0);
        MON_FEC_ERR_CNT             : in array_32b(0 to GBT_NUM-1);
        MON_AUTO_RX_RESET_CNT       : in array_32b(0 to GBT_NUM-1);
        CTRL_AUTO_RX_RESET_CNT_CLEAR: out  std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_TCLINK_PS_INC_NDEC     : out std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_TCLINK_PS_STROBE       : out std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_TCLINK_TX_FINE_REALIGN : out std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_TCLINK_TX_UI_ALIGN_CALIB:out std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_TCLINK_TX_PI_PHASE_CALIB: out array_7b(0 to GBT_NUM-1);
        CTRL_TCLINK_TX_CLOSE_LOOP: out std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_TCLINK_OFFSET_ERROR : out array_48b(0 to GBT_NUM-1);
        CTRL_TCLINK_DEBUG_TESTER_ADDR_READ: out array_10b(0 to GBT_NUM-1);
        MON_TCLINK_ERROR_CONTROLLER: in array_48b(0 to GBT_NUM-1);
        MON_TCLINK_LOOP_CLOSED: in std_logic_vector(GBT_NUM-1 downto 0);
        MON_TCLINK_TX_ALIGNED: in std_logic_vector(GBT_NUM-1 downto 0);
        MON_TCLINK_PS_DONE: in std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_TCLINK_MASTER_MGT_READY: out std_logic_vector(GBT_NUM-1 downto 0);
        MON_TCLINK_PHASE_DETECTOR: in array_32b(0 to GBT_NUM-1);
        MON_TCLINK_TX_FIFO_FILL_PD: in array_32b(0 to GBT_NUM-1);
        MON_TCLINK_LOOP_NOT_CLOSED_REASON: in array_5b(0 to GBT_NUM-1);
        MON_TCLINK_PHASE_ACC: in array_16b(0 to GBT_NUM-1);
        MON_TCLINK_OPERATION_ERROR: in std_logic_vector(GBT_NUM-1 downto 0);
        MON_TCLINK_DEBUG_TESTER_DATA_READ: in array_16b(0 to GBT_NUM-1);
        MON_TCLINK_PS_PHASE_STEP: in std_logic_vector(GBT_NUM-1 downto 0);
        MON_TCLINK_TX_PI_PHASE: in array_7b(0 to GBT_NUM-1);
        clk40_in                    : in std_logic;
        register_map_control        : in register_map_control_type;
        register_map_link_monitor    : out register_map_link_monitor_type

    );
end Regs_RW;


architecture Behavioral of Regs_RW is
begin

    --CTRL

    --VC709 only
    SOFT_TXRST_ALL    <= register_map_control.GBT_SOFT_TX_RESET.RESET_ALL(59 downto 48);
    SOFT_RXRST_ALL    <= register_map_control.GBT_SOFT_RX_RESET.RESET_ALL(59 downto 48);
    QPLL_RESET        <= register_map_control.GBT_PLL_RESET.QPLL_RESET(GBT_NUM/4-1+48 downto 48);

    CTRL_TXPLL_DATAPATH_RESET   <= register_map_control.GBT_PLL_RESET.CPLL_RESET(GBT_NUM/4-1 downto 0);
    CTRL_RXPLL_DATAPATH_RESET   <= register_map_control.GBT_PLL_RESET.QPLL_RESET(GBT_NUM/4+47 downto 48);

    --CTRL_DATARATE               <= register_map_control.LPGBT_DATARATE(GBT_NUM-1 downto 0);

    CTRL_FECMODE               <= register_map_control.LPGBT_FEC(GBT_NUM-1 downto 0);

    CTRL_SOFT_RESET             <= register_map_control.GBT_SOFT_RESET(GBT_NUM/4-1 downto 0);
    CTRL_TX_DATAPATH_RESET      <= register_map_control.GBT_GTTX_RESET(GBT_NUM/4-1 downto 0);
    CTRL_RX_DATAPATH_RESET      <= register_map_control.GBT_GTRX_RESET(GBT_NUM/4-1 downto 0);
    CTRL_TXPOLARITY             <= register_map_control.GBT_TXPOLARITY(GBT_NUM-1 downto 0);
    CTRL_RXPOLARITY             <= register_map_control.GBT_RXPOLARITY(GBT_NUM-1 downto 0);
    CTRL_GBTTXRST               <= register_map_control.GBT_TX_RESET(GBT_NUM-1 downto 0);
    CTRL_GBTRXRST               <= register_map_control.GBT_RX_RESET(GBT_NUM-1 downto 0);
    CTRL_CHANNEL_DISABLE        <= register_map_control.GBT_CHANNEL_DISABLE(GBT_NUM-1 downto 0);
    CTRL_GBT_General_ctrl       <= register_map_control.GBT_GENERAL_CTRL;


    --MON
    txrxrst_709: if CARD_TYPE = 709 generate
        register_map_link_monitor.GBT_TXFSMRESET_DONE(GBT_NUM-1 downto 0) <= MON_TXFSMRESETDONE;
        register_map_link_monitor.GBT_RXFSMRESET_DONE(GBT_NUM-1 downto 0) <= MON_RXFSMRESETDONE;
    end generate; --txrxrst_712
    txrxrst_712: if CARD_TYPE = 712 generate
        register_map_link_monitor.GBT_TXFSMRESET_DONE(GBT_NUM-1 downto 0) <= MON_TXPMARSTDONE;
        register_map_link_monitor.GBT_RXFSMRESET_DONE(GBT_NUM-1 downto 0) <= MON_RXPMARSTDONE;
    end generate; --txrxrst_712
    register_map_link_monitor.GBT_TXRESET_DONE(GBT_NUM-1 downto 0) <= MON_TXRSTDONE;
    register_map_link_monitor.GBT_RXRESET_DONE(GBT_NUM-1 downto 0) <= MON_RXRSTDONE;

    register_map_link_monitor.GBT_RXCDR_LOCK(GBT_NUM-1 downto 0) <= MON_RXCDR_LCK;
    register_map_link_monitor.GBT_PLL_LOCK.CPLL_LOCK(GBT_NUM-1 downto 0) <= MON_CPLL_LCK;
    register_map_link_monitor.GBT_PLL_LOCK.QPLL_LOCK(GBT_NUM/4+47 downto 48) <= MON_QPLL_LCK;
    register_map_link_monitor.GBT_ALIGNMENT_DONE(GBT_NUM-1 downto 0) <= MON_ALIGNMENT_DONE;
    register_map_link_monitor.GBT_ERROR(GBT_NUM-1 downto 0) <= MON_FEC_ERROR;

    g_FEC_channels: for i in 0 to GBT_NUM-1 generate
        g_limit24: if i < 24 generate
            register_map_link_monitor.GT_FEC_ERR_CNT(i) <= MON_FEC_ERR_CNT(i);
            register_map_link_monitor.GT_AUTO_RX_RESET_CNT(i).VALUE <= MON_AUTO_RX_RESET_CNT(i);
        end generate;
        CTRL_AUTO_RX_RESET_CNT_CLEAR(i) <= to_sl(register_map_control.GT_AUTO_RX_RESET_CNT(i mod 24).CLEAR);
    end generate;

    CTRL_MULTICYCLE_DELAY <= register_map_control.GBT_TX_TC_DLY_VALUE1(2 downto 0);

    g_TCLINK: for i in 0 to GBT_NUM-1 generate
        CTRL_TCLINK_PS_INC_NDEC(i) <= to_sl(register_map_control.TCLINK_CONTROL(i).PS_INC_NDEC);
        CTRL_TCLINK_PS_STROBE(i) <= to_sl(register_map_control.TCLINK_CONTROL(i).PS_STROBE);
        CTRL_TCLINK_TX_FINE_REALIGN(i) <= to_sl(register_map_control.TCLINK_CONTROL(i).PS_INC_NDEC);
        CTRL_TCLINK_TX_UI_ALIGN_CALIB(i) <= to_sl(register_map_control.TCLINK_CONTROL(i).PS_INC_NDEC);
        CTRL_TCLINK_TX_PI_PHASE_CALIB(i) <= register_map_control.TCLINK_CONTROL(i).TX_PI_PHASE_CALIB;
        CTRL_TCLINK_TX_CLOSE_LOOP(i) <= to_sl(register_map_control.TCLINK_CONTROL(i).CLOSE_LOOP);
        CTRL_TCLINK_OFFSET_ERROR(i) <= register_map_control.TCLINK_CONTROL(i).OFFSET_ERROR;
        CTRL_TCLINK_DEBUG_TESTER_ADDR_READ(i) <= register_map_control.TCLINK_MONITOR_3(i).DEBUG_TESTER_ADDR_READ;
        CTRL_TCLINK_MASTER_MGT_READY(i)<= to_sl(register_map_control.TCLINK_CONTROL(i).MASTER_MGT_RX_READY);

        register_map_link_monitor.TCLINK_MONITOR_1(i).ERROR_CONTROLLER <= MON_TCLINK_ERROR_CONTROLLER(i);
        register_map_link_monitor.TCLINK_MONITOR_1(i).LOOP_CLOSED(14) <= MON_TCLINK_LOOP_CLOSED(i);
        register_map_link_monitor.TCLINK_MONITOR_1(i).TX_ALIGNED(13) <= MON_TCLINK_TX_ALIGNED(i);
        register_map_link_monitor.TCLINK_MONITOR_1(i).PS_DONE(12) <= MON_TCLINK_PS_DONE(i);
        register_map_link_monitor.TCLINK_MONITOR_1(i).TX_PI_PHASE <= MON_TCLINK_TX_PI_PHASE(i);
        register_map_link_monitor.TCLINK_MONITOR_2(i).PHASE_DETECTOR <= MON_TCLINK_PHASE_DETECTOR(i);
        register_map_link_monitor.TCLINK_MONITOR_2(i).TX_FIFO_FILL_PD <= MON_TCLINK_TX_FIFO_FILL_PD(i);
        register_map_link_monitor.TCLINK_MONITOR_3(i).LOOP_NOT_CLOSED_REASON <= MON_TCLINK_LOOP_NOT_CLOSED_REASON(i);
        register_map_link_monitor.TCLINK_MONITOR_3(i).PHASE_ACC <= MON_TCLINK_PHASE_ACC(i);
        register_map_link_monitor.TCLINK_MONITOR_3(i).OPERATION_ERROR(37) <= MON_TCLINK_OPERATION_ERROR(i);
        register_map_link_monitor.TCLINK_MONITOR_3(i).DEBUG_TESTER_DATA_READ <= MON_TCLINK_DEBUG_TESTER_DATA_READ(i);
        register_map_link_monitor.TCLINK_MONITOR_3(i).PS_PHASE_STEP(7) <= MON_TCLINK_PS_PHASE_STEP(i);



    end generate;

    pll_lock_latch_proc: process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            if register_map_control.GBT_PLL_LOL_LATCHED.CLEAR = "1" then
                register_map_link_monitor.GBT_PLL_LOL_LATCHED.CPLL_LOL_LATCHED <= (others => '0');
                register_map_link_monitor.GBT_PLL_LOL_LATCHED.QPLL_LOL_LATCHED <= (others => '0');
            end if;
            for i in 0 to GBT_NUM-1 loop
                if MON_CPLL_LCK(i) = '0' then
                    register_map_link_monitor.GBT_PLL_LOL_LATCHED.CPLL_LOL_LATCHED(i) <= '1';
                end if;
            end loop;
            for i in 0 to GBT_NUM/4 - 1 loop
                if MON_QPLL_LCK(i) = '0' then
                    register_map_link_monitor.GBT_PLL_LOL_LATCHED.QPLL_LOL_LATCHED(i+48) <= '1';
                end if;
            end loop;

            if register_map_control.GBT_ALIGNMENT_LOST.CLEAR = "1" then
                register_map_link_monitor.GBT_ALIGNMENT_LOST.ALIGNMENT_LOST <= (others => '0');
            end if;
            for i in 0 to GBT_NUM-1 loop
                if MON_ALIGNMENT_DONE(i) = '0' then
                    register_map_link_monitor.GBT_ALIGNMENT_LOST.ALIGNMENT_LOST(i) <= '1';
                end if;
            end loop;
        end if;
    end process;

end Behavioral;

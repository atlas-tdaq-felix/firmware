--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Ricardo Luz
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

-- v2 by Ricardo Luz.
-- Based on elink_printer.vhd initially written by Michael Oberlingand and later modified by Marco Trovato.
-- Complies with 32-b width and MSB first for all widths.


LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

    use work.type_lib.ALL;
    use work.ip_lib.ALL;

library UNISIM;
    use UNISIM.VComponents.all;

Library xpm;
    use xpm.vcomponents.all;

entity gbt_word_printer_v2 is
    generic (
        NUMELINKmax     : integer := 112;
        NUMEGROUPmax    : integer := 7;
        PROTOCOL        : std_logic := '0'
    );
    port (
        clk                 : in  std_logic;
        elink_control       : in  lane_elink_control_array(0 to NUMELINKmax-1);
        elink_sync_reg      : in  std_logic;
        elink_data          : in  array_of_slv_9_0(0 to NUMELINKmax-1);
        tx_flag             : in  std_logic;
        l1a_trigger         : in  std_logic;
        --std_logic_vector(2 downto 0);
        MSBfirst            : in  std_logic;
        elink_read_enable   : out std_logic_vector(0 to NUMELINKmax-1);
        gbt_payload         : out std_logic_vector(223 downto 0);
        aurora_en           : in  std_logic_vector(0 to NUMELINKmax-1);
        data_ready          : out std_logic;
        or_out_of_sync      : in  std_logic;
        flag_sync           : in  std_logic_vector(0 to NUMEGROUPmax-1)
    );
end gbt_word_printer_v2;

architecture Behavioral of gbt_word_printer_v2 is
    signal gbt_shift_count                  : std_logic_vector(3 downto 0) := (others => '0');
    signal bit_stream_en                    : std_logic_vector(3 downto 0) := (others => '0');
    signal bit_stream_sync                  : std_logic := '0';
    signal elink_sync_lat                   : std_logic := '0';
    signal gbt_word_latch                   : std_logic := '0';
    signal bit_stream                       : array_of_slv_31_0(0 to NUMELINKmax-1);
    signal gbt_bit_stream                   : std_logic_vector(223 downto 0) := (others => '0');
    signal gbt_payload_i                    : std_logic_vector(223 downto 0) := (others => '0');
    signal elink_read_enable_i              : std_logic_vector(0 to NUMELINKmax-1);
    --signal lpgbt                            : std_logic := '0';
    signal elink_control_output_width_array : array_of_slv_2_0(0 to NUMELINKmax-1);

    type STATEM  is (st_idle, st_sync, st_generate) ;
    signal state                            : STATEM := st_idle;
    signal elink_sync                       :  std_logic;
    signal start_gen                        : std_logic;
    signal gen_done                         : std_logic_vector(0 to NUMEGROUPmax-1) := (others => '0');
    constant gen_done_1                     : std_logic_vector(0 to NUMEGROUPmax-1) := (others => '1');
    constant gen_done_0                     : std_logic_vector(0 to NUMEGROUPmax-1) := (others => '0');

    signal count_sync                       : std_logic_vector(0 to 5) := "000000";
    signal sync_read_enable                 : std_logic :='0';


    signal will_sync                        : std_logic := '0';
    signal flag_will_sync                   : std_logic := '0';
begin

    gbt_payload <= gbt_payload_i;
    elink_read_enable <= elink_read_enable_i;
    --lpgbt <= PROTOCOL;--LINKSconfig(2); --0 is gbt, 1 is lpgbt

    gen_elink_printers : for i in 0 to NUMELINKmax-1 generate
        signal elink_control_enable         : std_logic;
        signal elink_control_endian_mode    : std_logic;
        signal elink_control_output_width   : std_logic_vector(2 downto 0);
        signal elink_control_input_width    : std_logic;
        signal MSBfirst_d       : std_logic;
    begin

        xpm_enable : xpm_cdc_single generic map(
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map(
                src_clk => '0',
                src_in => elink_control(i).enable,
                dest_clk => clk,
                dest_out => elink_control_enable
            );

        xpm_endian_mode : xpm_cdc_single generic map(
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map(
                src_clk => '0',
                src_in => elink_control(i).endian_mode,
                dest_clk => clk,
                dest_out => elink_control_endian_mode
            );

        xpm_output_width : xpm_cdc_array_single generic map(
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0,
                WIDTH => 3
            )
            port map(
                src_clk => '0',
                src_in => elink_control(i).output_width,
                dest_clk => clk,
                dest_out => elink_control_output_width
            );

        xpm_input_width : xpm_cdc_single generic map(
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map(
                src_clk => '0',
                src_in => elink_control(i).input_width,
                dest_clk => clk,
                dest_out => elink_control_input_width
            );
        elink_control_output_width_array(i) <= elink_control_output_width;

        reg_input : process(clk)
        begin
            if clk'event and clk ='1' then
                MSBfirst_d <= MSBfirst; --register so it can be delayed to avoid high fanout
            end if;
        end process;
        elink_bit_feeder : entity work.elink_printer_bit_feeder_v2
            generic map (
                PROTOCOL            => PROTOCOL
            )
            port map (
                clk                 => clk,
                enable              => elink_control_enable,
                elink_endian_mode   => elink_control_endian_mode,
                elink_output_width  => elink_control_output_width,
                elink_input_width   => elink_control_input_width,
                word_in             => elink_data(i),
                bit_stream_en       => bit_stream_en,
                bit_stream_sync     => bit_stream_sync,
                --protocol            => protocol,
                gbt_word_latch      => gbt_word_latch,
                MSBfirst            => MSBfirst_d,
                aurora              => aurora_en(i),
                read_enable         => elink_read_enable_i(i),
                bit_stream_out      => bit_stream(i)
            );
    end generate gen_elink_printers;

    g_GBT: if PROTOCOL = '0' generate

        reg_input : process(clk)
        begin
            if clk'event and clk ='1' then
                if (tx_flag = '1' ) then
                    gbt_shift_count <= (others => '0');
                    if gbt_shift_count /= "0101" then
                        elink_sync_lat <= '1';
                    end if;
                elsif (gbt_shift_count /= "1111") then
                    gbt_shift_count <= gbt_shift_count + 1;
                end if;
                --------
                if (gbt_word_latch = '1') then
                    gbt_payload_i <= gbt_bit_stream;
                    data_ready    <= '1';
                else
                    data_ready    <= '0';
                end if;
                --------
                case gbt_shift_count is
                    when "0000" =>
                        bit_stream_en <= "1000";
                        bit_stream_sync <= '0';
                        gbt_word_latch <= '0';
                        if (elink_sync = '1') then
                            elink_sync_lat <= '1';
                        end if;
                    when "0001" =>
                        bit_stream_en <= "1000";
                        bit_stream_sync <= '0';
                        gbt_word_latch <= '0';
                        if (elink_sync = '1') then
                            elink_sync_lat <= '1';
                        end if;
                    when "0010" =>
                        bit_stream_en <= "1100";
                        bit_stream_sync <= elink_sync_lat;
                        gbt_word_latch <= '0';
                        elink_sync_lat <= elink_sync;
                    when "0011" =>
                        bit_stream_en <= "1111";
                        bit_stream_sync <= '0';
                        gbt_word_latch <= '1';
                        if (elink_sync = '1') then
                            elink_sync_lat <= '1';
                        end if;
                    when "0100" =>
                        bit_stream_en <= "1110";
                        bit_stream_sync <= '0';
                        gbt_word_latch <= '0';
                        if (elink_sync = '1') then
                            elink_sync_lat <= '1';
                        end if;
                    when "0101" =>
                        bit_stream_en <= "1100";
                        bit_stream_sync <= '0';
                        gbt_word_latch <= '0';
                        if (elink_sync = '1') then
                            elink_sync_lat <= '1';
                        end if;
                    when "0110" =>
                        bit_stream_en <= "1000";
                        bit_stream_sync <= '0';
                        gbt_word_latch <= '0';
                        if (elink_sync = '1') then
                            elink_sync_lat <= '1';
                        end if;
                    when "0111" =>
                        bit_stream_en <= "1000";
                        bit_stream_sync <= '0';
                        gbt_word_latch <= '0';
                        if (elink_sync = '1') then
                            elink_sync_lat <= '1';
                        end if;
                    when others =>
                        bit_stream_en <= "0000";
                        bit_stream_sync <= '0';
                        gbt_word_latch <= '0';
                        elink_sync_lat <= '1';
                end case;
            end if;
        end process reg_input;

        gen_gbt_stream : for i in 0 to 6 generate
            signal output_width : std_logic_vector(2 downto 0);
        begin
            output_width <= elink_control_output_width_array(i*16);--elink_control(i*16).output_width;
            gbt_bit_stream(32*(i+1)-1 downto 32*i)
                       <= bit_stream(i*16+15)( 1 downto 0) &
                          bit_stream(i*16+14)( 1 downto 0) &
                          bit_stream(i*16+13)( 1 downto 0) &
                          bit_stream(i*16+12)( 1 downto 0) &
                          bit_stream(i*16+11)( 1 downto 0) &
                          bit_stream(i*16+10)( 1 downto 0) &
                          bit_stream(i*16+ 9)( 1 downto 0) &
                          bit_stream(i*16+ 8)( 1 downto 0) &
                          bit_stream(i*16+ 7)( 1 downto 0) &
                          bit_stream(i*16+ 6)( 1 downto 0) &
                          bit_stream(i*16+ 5)( 1 downto 0) &
                          bit_stream(i*16+ 4)( 1 downto 0) &
                          bit_stream(i*16+ 3)( 1 downto 0) &
                          bit_stream(i*16+ 2)( 1 downto 0) &
                          bit_stream(i*16+ 1)( 1 downto 0) &
                          bit_stream(i*16+ 0)( 1 downto 0) when output_width="000" else
                          bit_stream(i*16+14)( 3 downto 0) &
                          bit_stream(i*16+12)( 3 downto 0) &
                          bit_stream(i*16+10)( 3 downto 0) &
                          bit_stream(i*16+ 8)( 3 downto 0) &
                          bit_stream(i*16+ 6)( 3 downto 0) &
                          bit_stream(i*16+ 4)( 3 downto 0) &
                          bit_stream(i*16+ 2)( 3 downto 0) &
                          bit_stream(i*16+ 0)( 3 downto 0) when output_width="001" else
                          bit_stream(i*16+12)( 7 downto 0) &
                          bit_stream(i*16+ 8)( 7 downto 0) &
                          bit_stream(i*16+ 4)( 7 downto 0) &
                          bit_stream(i*16+ 0)( 7 downto 0) when output_width="010" else
                          bit_stream(i*16+ 8)(15 downto 0) &
                          bit_stream(i*16+ 0)(15 downto 0) when output_width="011" else
                          (others => '0');
        end generate gen_gbt_stream;
    end generate;

    g_lpGBT: if PROTOCOL = '1' generate

        reg_input : process(clk)
        begin
            if clk'event and clk ='1' then
                if (tx_flag = '1' ) then
                    gbt_shift_count <= (others => '0');
                    if gbt_shift_count /= "0111" then
                        elink_sync_lat <= '1';
                    end if;
                elsif (gbt_shift_count /= "1111") then
                    gbt_shift_count <= gbt_shift_count + 1;
                end if;
                --------
                if (gbt_word_latch = '1') then
                    gbt_payload_i <= gbt_bit_stream;
                    data_ready    <= '1';
                else
                    data_ready    <= '0';
                end if;
                --------
                case gbt_shift_count is
                    when "0000" =>
                        bit_stream_en <= "1000";
                        bit_stream_sync <= '0';
                        gbt_word_latch <= '0';
                        if (elink_sync = '1') then
                            elink_sync_lat <= '1';
                        end if;
                    when "0001" =>
                        bit_stream_en <= "1000";
                        bit_stream_sync <= '0';
                        gbt_word_latch <= '0';
                        if (elink_sync = '1') then
                            elink_sync_lat <= '1';
                        end if;
                    when "0010" =>
                        bit_stream_en <= "1100";
                        bit_stream_sync <= elink_sync_lat;
                        gbt_word_latch <= '0';
                        elink_sync_lat <= elink_sync;
                    when "0011" =>
                        bit_stream_en <= "1111";
                        bit_stream_sync <= '0';
                        gbt_word_latch <= '0';
                        if (elink_sync = '1') then
                            elink_sync_lat <= '1';
                        end if;
                    when "0100" =>
                        bit_stream_en <= "1110";
                        bit_stream_sync <= '0';
                        gbt_word_latch <= '0';
                        if (elink_sync = '1') then
                            elink_sync_lat <= '1';
                        end if;
                    when "0101" =>
                        bit_stream_en <= "1100";
                        bit_stream_sync <= '0';
                        gbt_word_latch <= '0';
                        if (elink_sync = '1') then
                            elink_sync_lat <= '1';
                        end if;
                    when "0110" =>
                        bit_stream_en <= "1000";
                        bit_stream_sync <= '0';
                        gbt_word_latch <= '0';
                        if (elink_sync = '1') then
                            elink_sync_lat <= '1';
                        end if;
                    when "0111" =>
                        bit_stream_en <= "1000";
                        bit_stream_sync <= '0';
                        gbt_word_latch <= '1';
                        if (elink_sync = '1') then
                            elink_sync_lat <= '1';
                        end if;
                    when others =>
                        bit_stream_en <= "0000";
                        bit_stream_sync <= '0';
                        gbt_word_latch <= '0';
                        elink_sync_lat <= '1';
                end case;
            end if;
        end process reg_input;

        gen_gbt_stream : for i in 0 to 6 generate
            signal output_width : std_logic_vector(2 downto 0);
        begin
            output_width <= elink_control_output_width_array(i*16);--elink_control(i*16).output_width;
            gbt_bit_stream(32*(i+1)-1 downto 32*i)
                       <= bit_stream(i*16+12)( 7 downto 0) &
                          bit_stream(i*16+ 8)( 7 downto 0) &
                          bit_stream(i*16+ 4)( 7 downto 0) &
                          bit_stream(i*16+ 0)( 7 downto 0) when output_width="010" else
                          bit_stream(i*16+ 8)(15 downto 0) &
                          bit_stream(i*16+ 0)(15 downto 0) when output_width="011" else
                          bit_stream(i*16+ 0)(31 downto 0) when output_width="100" else
                          (others => '0');
        end generate gen_gbt_stream;
    end generate;

    --state machine to control elink_sync.
    start_gen <= elink_sync_reg;
    sync_readenable : process(clk)
    begin
        if clk'event and clk ='1' then
            sm_sync: case state is
                when st_idle =>
                    sync_read_enable <= '0';
                    count_sync<="000000";
                    if l1a_trigger = '1' then
                        state <= st_generate;
                    elsif start_gen = '1' then
                        state <= st_sync;
                    else
                        state <= st_idle;
                    end if;
                when st_sync =>
                    gen_done <= gen_done_0;
                    if(count_sync = "111111") then
                        if(will_sync = '1') then
                            sync_read_enable <= '1';--'1';
                            will_sync <= '0';
                        end if;
                        count_sync<="000000";
                    else
                        sync_read_enable <= '0';
                        count_sync <= count_sync + '1';
                    end if;
                    if l1a_trigger = '1' then
                        state <= st_generate;
                        count_sync<="000000";
                    else
                        state <= st_sync;
                    end if;
                when st_generate =>
                    sync_read_enable <= '0';
                    for i in 0 to NUMEGROUPmax-1 loop
                        if(flag_sync(i) = '1') then
                            gen_done(i) <= '1';
                        end if;
                    end loop;
                    if gen_done = gen_done_1 or start_gen = '1' then
                        state <= st_sync;
                    else
                        state <= st_generate;
                    end if;
            end case sm_sync;
            if ( elink_sync_reg = '1' or or_out_of_sync = '1' ) and flag_will_sync = '0' then
                will_sync <= '1';
                flag_will_sync <= '1';
            elsif elink_sync_reg = '0' and or_out_of_sync = '0' and will_sync = '0' then
                flag_will_sync <= '0';
            end if;
        end if;
    end process sync_readenable;

    elink_sync <= sync_read_enable;
end Behavioral;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Shelfali Saxena
--!               mtrovato
--!               Ricardo Luz
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.
--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:  Michael Oberling, Marco Trovato
--
-- Design Name:  gbt_word_printer
-- Version:    1.0
-- Date:    9/13/2017
--
-- Description:  generate payload of 80b (no FEC, HDR, IC, EC)
--
-- Change Log:  V1.0 -
--         V1.1 - added dynamic bit-wise endian control.
--
--=============================================================================

LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

    use work.type_lib.ALL;
    use work.ip_lib.ALL;

--DATARATE (Gbps)  FEC #egroups #elinks/group #elinks elink_widths (b)
--   4.8                   5            8         40    2,4,8,16
--   5.12            5     7            4         28      4,8,16
--   5.12           12     6            4         24      4,8,16
--   10.24           5     7            4         28        8,16,32
--   10.24          12     6            4         24        8,16,32


library UNISIM;
    use UNISIM.VComponents.all;

entity gbt_word_printer is
    generic (
        enable_endian_control        : std_logic;  -- if '0', then operation is always in little endian.
        DATARATE                      : integer := 0;
        FEC                           : integer := 0;
        --NUMELINK                      : integer := 40;
        NUMELINKmax                   : integer := 112
    );
    port (
        --MT  clk240        : in  std_logic;
        clk        : in  std_logic;
        --For Phase2 DATARATES elink_control(24 to 39)="0..0" (FEC12) or
        --elink_control(27 to 39)="0..0" (FEC5)
        elink_control      : in  lane_elink_control_array(0 to NUMELINKmax-1);--RL
        --  elink_read_enable    : out std_logic_vector(0 to 39); --ph1
        elink_read_enable    : out std_logic_vector(0 to NUMELINKmax-1); --MT RL
        --ph2
        elink_sync      : in  std_logic;
        --  elink_data      : in  array_of_slv_9_0(0 to 39);--ph1
        elink_data      : in  array_of_slv_9_0(0 to NUMELINKmax-1);--MT ph2 RL
        --  gbt_payload      : out std_logic_vector( 79 downto 0);
        gbt_payload      : out std_logic_vector( 223 downto 0);
        tx_flag        : in  std_logic;
        ila_gbt_word_gen_state          : out std_logic_vector(  3 downto 0);
        LINKSconfig                   : in std_logic_vector(2 downto 0)
    );
end gbt_word_printer;

architecture Behavioral of gbt_word_printer is
    --ph1
    --  signal elink_read_enable_i  :  std_logic_vector(0 to 39);
    --MT ph2
    signal elink_read_enable_i  :  std_logic_vector(0 to NUMELINKmax-1);
    signal gbt_payload_i      : std_logic_vector(223 downto 0) := (others => '0'); --( 79 downto 0);
    --  signal gbt_bit_stream    : std_logic_vector(107 downto 0)  := (others =>
    --  '0'); --MT for DATARATE=0 why 108b and not 96b (5groups x 16b=80b + 1x16b allocated for
    --  downshifting)
    --for DATARATE=1 7groups*16b+ extra 1x16b =128 for downshifting
    --for DATARATE=2 7groups*32b+ extra 1x32b =256 for downshifting
    signal gbt_bit_stream    : std_logic_vector(255 downto 0)  := (others => '0');


    --MT SIMU+
    --signal gbt_bit_stream_extra    : std_logic_vector(239 downto 0)  := (others => '0');
    --
    --40+ extra 1 for downshifting
    --ph1
    --  signal bit_stream_1_we  : std_logic_vector( 40 downto 0)  := (others => '0');
    --ph2
    signal bit_stream_1_we  : std_logic_vector( 112 downto 0)  := (others => '0');
    --MT for DATARATE=0 why 92 downto 0 rather than 39*2+3 downto 0 ? (see
    --elink_printer_bit_feeder)
    --ph1
    --  signal bit_stream_1_0    : std_logic_vector( 92 downto 0)  := (others => '0');
    --MT ph2
    signal bit_stream_1    : std_logic_vector( 225 downto 0)  := (others => '0');
    --ph1
    --  signal bit_stream_2_we  : std_logic_vector( 40 downto 0)  := (others => '0');
    --ph2
    signal bit_stream_2_we  : std_logic_vector( 112 downto 0)  := (others => '0');
    --MT same as above
    --ph1
    --  signal bit_stream_3_2    : std_logic_vector( 92 downto 0)  := (others => '0');
    --ph2
    signal bit_stream_2    : std_logic_vector( 225 downto 0)  := (others => '0');
    --  signal gbt_shift_count    : std_logic_vector(  2 downto 0)  := (others => '0');
    --MT for DARATE>0 LANETXCLK=WORDCLK=320 MHZ / 40 MHZ = 8
    signal gbt_shift_count    : std_logic_vector(  3 downto 0)  := (others => '0');
    --MT increased width to allow 32b elink (DATARATE>0)
    signal bit_stream_en    : std_logic_vector(  3 downto 0)  := (others => '0');
    signal bit_stream_sync    : std_logic            := '0';
    signal elink_sync_lat    : std_logic            := '0';
    signal gbt_word_latch    : std_logic            := '0';



begin


    ila_gbt_word_gen_state  <= gbt_shift_count;

    --  gbt_bit_stream(gbt_bit_stream'high downto gbt_bit_stream'high-3) <= (others =>
    --  '0'); --MT I don't understand why this is needed
    gbt_payload <= gbt_payload_i;
    elink_read_enable <= elink_read_enable_i;



    --  gen_elink_printers : for i in 0 to 39 generate
    gen_elink_printers : for i in 0 to NUMELINKmax-1 generate
        --MT this handles the data flow from generated elink_data/10b
        --to bit_stream_1_0/2b and bit_stream_2_0/2b according to the elink
        --configuration (enable, elink_input/output_width, bit_stream_en,
        --sync)

        --MT added ph2: il keeps track of the real elink, i of the virtual elinks
        --with 2b width

        --   begin                                        --
        --il := i   when (DATARATE=0) else
        --      i/2 when (DATARATE=1) else
        --      i/4 when (DATARATE=3);


        --    elink_bit_feeder_DATARATE0 : if (DATARATE=0) generate
        elink_bit_feeder : entity work.elink_printer_bit_feeder
            generic map (
                enable_endian_control => enable_endian_control,
                DATARATE              => DATARATE
            )
            port map (
                --MT      clk240      => clk240,
                clk                => clk,
                enable            => elink_control(i).enable,
                elink_endian_mode    => elink_control(i).endian_mode,
                elink_output_width    => elink_control(i).output_width,
                elink_input_width    => elink_control(i).input_width,
                word_in            => elink_data           (i),
                bit_stream_1      => bit_stream_1         (i*2 + 3 downto i*2 + 2),
                bit_stream_1_we        => bit_stream_1_we     (i + 1),
                bit_stream_2      => bit_stream_2         (i*2 + 3 downto i*2 + 2),
                bit_stream_2_we        => bit_stream_2_we     (i + 1),
                read_enable          => elink_read_enable_i   (i),
                bit_stream_en      => bit_stream_en,
                bit_stream_sync      => bit_stream_sync
            );
        --    end generate elink_bit_feeder_DATARATE0;

        --MT ph2
        --    elink_bit_feeder_DATARATE1 : if (DATARATE=1) generate
        --    elink_bit_feeder : entity work.elink_printer_bit_feeder
        --      generic map (
        --        enable_endian_control => enable_endian_control,
        --        DATARATE              => DATARATE
        --        )
        --      port map (
        ----MT      clk240      => clk240,
        --        clk      => clk,
        --        enable      => elink_control(i/2).enable, --TO DO:
        --        elink_endian_mode  => elink_control(i/2).endian_mode,
        --        elink_output_width  => elink_control(i/2).output_width,
        --        elink_input_width  => elink_control(i/2).input_width,
        --        word_in      => elink_data    (i),
        --        bit_stream_1    => bit_stream_1  (i*2 + 3 downto i*2 + 2),
        --        bit_stream_1_we  => bit_stream_1_we  (i + 1),
        --        bit_stream_2    => bit_stream_2  (i*2 + 3 downto i*2 + 2),
        --        bit_stream_2_we  => bit_stream_2_we  (i + 1),
        --        read_enable    => elink_read_enable_i  (i),
        --        bit_stream_en    => bit_stream_en,
        --        bit_stream_sync    => bit_stream_sync
        --        );
        --    end generate elink_bit_feeder_DATARATE1;

        --    elink_bit_feeder_DATARATE2 : if (DATARATE=2) generate
        --    elink_bit_feeder : entity work.elink_printer_bit_feeder
        --      generic map (
        --        enable_endian_control => enable_endian_control,
        --        DATARATE              => DATARATE
        --        )
        --      port map (
        ----MT      clk240      => clk240,
        --        clk      => clk,
        --        enable      => elink_control(i/4).enable, --TO DO : 4
        --                                                              --virtual elinks
        --                                                              --corresponds to
        --                                                              --one real elink
        --                                                              --
        --        elink_endian_mode  => elink_control(i/4).endian_mode,
        --        elink_output_width  => elink_control(i/4).output_width,
        --        elink_input_width  => elink_control(i/4).input_width,
        --        word_in      => elink_data    (i),
        --        bit_stream_1    => bit_stream_1  (i*2 + 3 downto i*2 + 2),
        --        bit_stream_1_we  => bit_stream_1_we  (i + 1),
        --        bit_stream_2    => bit_stream_2  (i*2 + 3 downto i*2 + 2),
        --        bit_stream_2_we  => bit_stream_2_we  (i + 1),
        --        read_enable    => elink_read_enable_i  (i),
        --        bit_stream_en    => bit_stream_en,
        --        bit_stream_sync    => bit_stream_sync
        --        );
        --    end generate elink_bit_feeder_DATARATE2;
        --
        --MT either shift down by 2b (if We=0) or write bit_stream_1
        --or 2_0 into out. Doesn't know anything about elink configurations
        --NB: DATARATE=0 bit_stream_2_we  is indexed  (i) while bit_stream_1_we is indexed
        --(i+1) since I can only copy 2b at the time into gbt_bit_stream(i*2 + 13 downto i*2 +
        --12). Given that two contiguous 4b are not allowed, this trick to write 2b
        --out 4b with (i) and the other 2b with (i+1) works. For example If i=0 is 4b
        --bit_stream_1_we = 1 for i=0; bit_stream_2_we for i=1
        elink_printhead : entity work.elink_printer_printhead

            port map (
                clk                      => clk,
                --min elink width=2
                bit_stream_1                  => bit_stream_1  (i*2 + 3 downto i*2 + 2),
                bit_stream_1_we                  => bit_stream_1_we  (i + 1),
                bit_stream_2                  => bit_stream_2  (i*2 + 1 downto i*2 + 0),
                bit_stream_2_we                  => bit_stream_2_we  (i),
                --MT ph1 commented
                --        gbt_bit_stream_in  => gbt_bit_stream  (i*2 + 17 downto i*2 + 16),
                --        gbt_bit_stream_out  => gbt_bit_stream  (i*2 + 13 downto i*2 + 12)
                --MT ph2 because gbt_bit_tray has been extended to 32b
                gbt_bit_stream_in  => gbt_bit_stream  (i*2 + 33 downto i*2 + 32),
                gbt_bit_stream_out  => gbt_bit_stream  (i*2 + 29 downto i*2 + 28)
            );
    end generate gen_elink_printers;
    --MT shift down by 4b the remaining 32b (it was 16b). This may need a
    --resynchronization with tx_flag

    --MT  gbt_bit_tray : process(clk240)
    gbt_bit_tray : process(clk)        --can gbt_bit_tray be consolidated wtih
    --elink_printhead by changing 17-> 5, 16
    ---> 4; 13->1; 12->0 ? This is just
    ---ensuring that generated data gbt_bit_stream(95 downto
    ---16) will be available at the second
    ---6x240MHZ_clks (or tx_flag) rather
    ---than the first one. Need to synchronize
    ---with it
    begin
        --MT    if clk240'event and clk240 ='1' then
        if clk'event and clk ='1' then
            --ph2 added

            gbt_bit_stream(27 downto 26) <= gbt_bit_stream(31 downto 30);
            gbt_bit_stream(25 downto 24) <= gbt_bit_stream(29 downto 28);

            gbt_bit_stream(23 downto 22) <= gbt_bit_stream(27 downto 26);
            gbt_bit_stream(21 downto 20) <= gbt_bit_stream(25 downto 24);

            gbt_bit_stream(19 downto 18) <= gbt_bit_stream(23 downto 22);
            gbt_bit_stream(17 downto 16) <= gbt_bit_stream(21 downto 20);

            gbt_bit_stream(15 downto 14) <= gbt_bit_stream(19 downto 18);
            gbt_bit_stream(13 downto 12) <= gbt_bit_stream(17 downto 16);
            --
            gbt_bit_stream(11 downto 10) <= gbt_bit_stream(15 downto 14);
            gbt_bit_stream( 9 downto  8) <= gbt_bit_stream(13 downto 12);

            gbt_bit_stream( 7 downto  6) <= gbt_bit_stream(11 downto 10);
            gbt_bit_stream( 5 downto  4) <= gbt_bit_stream( 9 downto  8);

            gbt_bit_stream( 3 downto  2) <= gbt_bit_stream( 7 downto  6);
            gbt_bit_stream( 1 downto  0) <= gbt_bit_stream( 5 downto  4);
        end if;
    end process gbt_bit_tray;


    --John/Mike (MT something doesn't make sense here. Rewriting below)
    -- TX FLAG  0    1    0    0    0    0    0    1
    -- COUNT  100    101    000    001    010    011    100    101
    -- ENABLE  000    111    110    100    100    000    000
    -- payload  XXXX          XXXX          XXXA          XXBA          XCBA          DCBA          DCBA          0000
    -- latch_en  0    0    0    0    0    1    0
    -- output  XXXX          XXXX          XXXX          XXXX          XXXX          XXXX          DCBA          DCBA

    --MT: waveform per egroup (16b)
    -- CLK240               0               1               2               3               4               5               6               7
    -- TX FLAG          0    1    0    0    0    0    0    1
    -- gbt_shift_count  100    101    000    001    010    011    100    101
    -- bit_stream_en  111    110    100    100    000          000    111    110
    -- gbt_bit_stream  XXXX          XXXX          AXXX          BAXX          CBAX          DCBA          DCBA          0000
    -- gbt_bit_stream'  XXXX          XXXX          XXXX            AXXX          BAXX          CBAX          DCBA          NCBA
    -- gbt_word_latch  0    0    0    0    0    0    1    0
    -- gbt_payload_i  XXXX          XXXX          XXXX          XXXX          XXXX          XXXX          XXXX          DCBA

    --N.B1: gbt_payload_i need to be synchronized with TX_FLAG
    --(GBT_WRAPPER_KC serialize payload when TX_FLAG=1)
    --N.B2: gbt_bit_stream shift down by 4 bits at each clk
    --N.B3: bitstream_en: (2,16b elink) is eq 1 4/6 times => 4x4=16b in 40 MHZ;
    --(1,8b elink) is eq 1 2/6 times => 2x4 = 8b; (0,4b or 2b elink) is eq 1/6 times => 4x1 = 4b (4b elink), 2x1 = 2b
    --elink
    --Q.: why at clk=6 gbt_bit_stream doesn't change? The shifting happens
    --regardless of the bit_stream_en. Guess gbt_bit_stream' instead of gbt_bit+stream


    --MT  reg_input : process(clk240)
    reg_input : process(clk)
    begin
        --    if clk240'event and clk240 ='1' then
        if clk'event and clk ='1' then
            --expecting
            --DATARATE=0  tx_flag=1 for 1 clk every 6
            --DATARATE!=0 tx_flag=1 for 1 clk every 8

            if (tx_flag = '1' ) then
                gbt_shift_count <= (others => '0');
            elsif (gbt_shift_count /= "1111") then
                gbt_shift_count <= gbt_shift_count + 1;
            end if;

            --same as ph1
            --if (gbt_shift_count(2 downto 0) /= "101") then
            --  gbt_shift_count <= gbt_shift_count + 1;
            --else
            --  gbt_shift_count <= (others => '0');
            --end if;
            --

            if (gbt_word_latch = '1') then
                gbt_payload_i <= gbt_bit_stream(gbt_payload'range);
            end if;
            --bit_stream_en (N=6 DATARATE=0, 8 DATARATE=1)
            --32b    4 out of N clks   (N.B: 8b shifted at the time)
            --16b    4 out of N clks   (N.B: 4b shifted at the time)
            --8b     2 out of N clks   (N.B: 4b shifted at the time)
            --4b/2b  1 out of N clks   (N.B: 4b shifted at the time)            --
            --NB bit_stream_en = 1 per elink have to be contiguous
            case gbt_shift_count is
                when "0000" =>
                    bit_stream_en <= "1000"; --ph1 "1100";
                    bit_stream_sync <= '0';
                    gbt_word_latch <= '0';
                    if (elink_sync = '1') then
                        elink_sync_lat <= '1';
                    end if;
                when "0001" =>
                    bit_stream_en <= "1000";
                    bit_stream_sync <= '0';
                    -- if(LINKSconfig = "110") then
                    --  gbt_word_latch <= '1';
                    --else
                    gbt_word_latch <= '0';
                    --end if;
                    if (elink_sync = '1') then
                        elink_sync_lat <= '1';
                    end if;
                when "0010" =>
                    bit_stream_en <= "1100"; --ph1 "1000";
                    bit_stream_sync <= elink_sync_lat;
                    gbt_word_latch <= '0';
                    elink_sync_lat <= elink_sync;
                when "0011" =>
                    bit_stream_en <= "1111";
                    bit_stream_sync <= '0';
                    if(LINKSconfig(2) = '0') then --or LINKSconfig = "110") then
                        gbt_word_latch <= '1';
                    else
                        gbt_word_latch <= '0';
                    end if;
                    if (elink_sync = '1') then
                        elink_sync_lat <= '1';
                    end if;
                when "0100" =>
                    bit_stream_en <= "1110";
                    bit_stream_sync <= '0';
                    gbt_word_latch <= '0';
                    if (elink_sync = '1') then
                        elink_sync_lat <= '1';
                    end if;
                when "0101" =>
                    bit_stream_en <= "1100";
                    bit_stream_sync <= '0';
                    if(LINKSconfig(2) = '1') then-- downto 1) = "10" or LINKSconfig = "111") then
                        gbt_word_latch <= '1';
                    else
                        gbt_word_latch <= '0';
                    end if;
                    if (elink_sync = '1') then
                        elink_sync_lat <= '1';
                    end if;
                when "0110" =>
                    bit_stream_en <= "1000";
                    bit_stream_sync <= '0';
                    gbt_word_latch <= '0';
                    if (elink_sync = '1') then
                        elink_sync_lat <= '1';
                    end if;
                when "0111" =>
                    bit_stream_en <= "1000";
                    bit_stream_sync <= '0';
                    gbt_word_latch <= '0';
                    if (elink_sync = '1') then
                        elink_sync_lat <= '1';
                    end if;
                when others =>
                    bit_stream_en <= "0000";
                    bit_stream_sync <= '0';
                    gbt_word_latch <= '0';
                    elink_sync_lat <= '1';
            end case;
        end if;
    end process reg_input;
end Behavioral;

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:  Soo Ryu
-- Modified By:  Michael Oberling
-- Converted to NSW packet generator: Radu Coliban, Transilvania Univ., Brasov
--
-- Design Name:  nsw_packet_generator
-- Version:    1.0
-- Date:    9/13/2017
--
-- Description:  Coming soon.
--
-- Change Log:  V1.0 -
--
--==============================================================================

LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
--use ieee.numeric_std.all;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.FELIX_package.all;
    use work.ip_lib.ALL;

entity nsw_packet_generator is
    generic (
        epath  : std_logic_vector (4 downto 0);
        egroup  : std_logic_vector (2 downto 0)
    );
    Port (
        clk_i : in STD_LOGIC;
        rst_i : in STD_LOGIC;
        --MT  ewidth : in STD_LOGIC_VECTOR (1 downto 0);
        ewidth : in STD_LOGIC_VECTOR (2 downto 0);
        pattern_sel_i : in STD_LOGIC_VECTOR (1 downto 0);
        userdata_i : in STD_LOGIC_VECTOR (15 downto 0);
        chunk_length_i : in STD_LOGIC_VECTOR (11 downto 0);
        l1trigger_i : in STD_LOGIC;
        l1a_id : in STD_LOGIC_VECTOR (15 downto 0);
        sw_busy_i : in std_logic;
        elinkdata_o : out STD_LOGIC_VECTOR (17 downto 0);
        elinkdata_rdy_o : out STD_LOGIC;
        --Frans 2
        chunk_length_trig_o : out STD_LOGIC; --ask for a new chunk length if 1 (could be random).
        --NSW
        even_parity_i : in STD_LOGIC

    );
end nsw_packet_generator;

architecture Behavioral of nsw_packet_generator is
    --
    constant sof_code : std_logic_vector (1 downto 0) := "10";    --[comma][sof]
    constant eof_code : std_logic_vector (1 downto 0) := "01";    --[eof][comma]
    constant data_code : std_logic_vector (1 downto 0) := "00";    --[data][data]
    constant comma_code : std_logic_vector (1 downto 0) := "11";    --[comma][comma]

    type ChunkFSM   is (idl, soc, h1, h2, h3, payload1, payload2, t1, t2, eoc) ;
    type BusyFSM    is (idl, sob, eob) ;

    signal busy_data : std_logic_vector(17 downto 0);
    signal data16b, prbs16b, din, userdata  : std_logic_vector(15 downto 0);
    signal pattern_sel   : std_logic_vector(1 downto 0);
    signal clk           : std_logic;
    signal hold, rst     : std_logic;
    -- signal l1trig        : std_logic; -- RL: commented because it is never used
    signal prbs16b_rdy   : std_logic;
    signal chunk_length  : std_logic_vector(11 downto 0) := x"0A0";    -- chunk length 12 bits in byte
    signal state : ChunkFSM;
    signal busy_state : BusyFSM;
    signal dataCode : std_logic_vector(1 downto 0);
    signal count_length : std_logic_vector(11 downto 0) := x"000";
    signal count_chunk  : std_logic_vector(11 downto 0) := x"000";
    signal l1a_pipe : std_logic := '0';
    signal is_header : std_logic := '0';
    signal elinkdata_rdy : std_logic := '0';

    signal q_unused : std_logic_vector(47 downto count_length'high);

    --- Signals for L1A trigger FIFO ----
    signal l1a_id_Fifo   : std_logic_vector(15 downto 0);

    signal data_in  : std_logic_vector(15 downto 0);
    --signal write_en : std_logic ; RL: commented because it is never used
    signal read_en  : std_logic ;
    signal valid_out  : std_logic;
    signal data_out : std_logic_vector(15 downto 0);
    signal Fifo_full    : std_logic;
    signal Fifo_almost_full : std_logic;
    signal Fifo_empty  : std_logic;
    signal Fifo_almost_empty    : std_logic;
    signal State_counter : std_logic_vector(4 downto 0);
    signal Fifo_Data_Count : std_logic_vector(10 downto 0);

    --- NSW signals ---

    signal adc : std_logic_vector(9 downto 0);
    signal adc_out : std_logic_vector(9 downto 0); --output saturated to X3FE
    signal tdc : std_logic_vector(7 downto 0);
    signal n_rel_bcid : std_logic_vector(3 downto 0);
    signal vmm_id : std_logic_vector(2 downto 0);
    signal chan : std_logic_vector(5 downto 0);
    signal parity_bit : std_logic;
    signal vmm_missing : std_logic_vector(7 downto 0);
    signal hit_count : std_logic_vector(9 downto 0); --number of hits in packet
    signal checksum : std_logic_vector(10 downto 0); --includes carry
    signal interm_checksum : std_logic_vector(7 downto 0); --last sum + carry
    signal final_checksum : std_logic_vector(7 downto 0); --inverted


    signal chunk_counter_l_val : std_logic_vector(count_length'high-1 downto 0);
    signal enable_lfsr : std_logic;
    signal reset_flags : std_logic;
    ---
    --FIFO for the triggers
    COMPONENT L1A_Fifo
        PORT (
            clk : IN STD_LOGIC;
            srst : IN STD_LOGIC;
            din : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
            wr_en : IN STD_LOGIC;
            rd_en : IN STD_LOGIC;
            dout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
            full : OUT STD_LOGIC;
            almost_full : OUT STD_LOGIC;
            empty : OUT STD_LOGIC;
            almost_empty : OUT STD_LOGIC;
            valid : OUT STD_LOGIC;
            data_count : OUT STD_LOGIC_VECTOR(10 DOWNTO 0);
            wr_rst_busy : OUT STD_LOGIC;
            rd_rst_busy : OUT STD_LOGIC
        ----------------------------------------------------------------------------


        );
    END COMPONENT;

begin

    clk          <= clk_i;
    rst          <= rst_i;
    pattern_sel  <= pattern_sel_i;
    chunk_length <= chunk_length_i;
    userdata     <= userdata_i;

    elinkdata_o <= dataCode & data16b when sw_busy_i = '0' else
                   busy_data; -- 18bit

    -- MBO: commented to save on slices.  This uses 55slices x 24 channels = 1,320 (about 1%) x [# emulators / channel].
    --prbs_gen: entity work.prbs_16bit
    --port map (
    --  clk_i    => clk,
    --  rst_i    => rst,
    --  hold_i   => hold,
    --  seed_i   => userdata,
    --  data_rdy => prbs16b_rdy,
    --  data_o   => prbs16b
    --);

    din <= x"0000" when pattern_sel = "11" else
           prbs16b  when pattern_sel = "10" else
           userdata when pattern_sel = "01" else
           x"0" & count_length when pattern_sel = "00";

    -- l1trig <= l1trigger_i; -- RL: commented because it is never used
    elinkdata_rdy_o <= elinkdata_rdy;

    -- MBO: slice off a few more slices.
    -- mbo: Changed to count down, forced even.
    -- For NSW: forced multiple of 4 in load value
    chunk_counter_l_val <= chunk_length(count_length'high downto 2) & '0';
    chunk_counter : dsp_counter
        PORT MAP (
            CLK                => clk,
            CE                => elinkdata_rdy,
            SCLR              => rst,
            UP                => '0',
            LOAD              => is_header,
            L(47 downto count_length'high)  => (others => '0'),
            L(count_length'high-1 downto 0)  => chunk_counter_l_val,
            Q(47 downto count_length'high)  => q_unused,
            Q(count_length'high-1 downto 0)  => count_length(count_length'high downto 1)
        );
    count_length(0) <= '0';

    -- L1A trigger Instantiation ----
    -- SS: added FIFO for randon L1A trigger--
    data_in <=  l1a_id;--(15 downto 0);
    L1A_Trigger_FIFO_Inst : L1A_Fifo
        PORT MAP (
            clk => clk,
            srst => rst,
            din => data_in,
            wr_en => l1trigger_i,
            rd_en => read_en,
            valid => valid_out,
            dout => data_out,
            full => Fifo_full,
            almost_full => Fifo_almost_full,
            empty => Fifo_empty,
            almost_empty => Fifo_almost_empty,
            data_count => Fifo_Data_Count,
            wr_rst_busy => open,
            rd_rst_busy => open
        );
    -- End L1A trigger Instantiation ----

    chunk_generator: process (clk, rst)
    begin
        if rst = '1' then
            state      <= idl;
            dataCode    <= "11";  -- comma
            count_chunk  <= (others => '0');
            busy_state    <= sob;
        elsif clk'event and clk='1' then
            --Frans 1
            --chunk_length_trig_o <= '0'; --ask for a new chunk length if 1 (could be random).
            -- RL commented to clean up code
            if sw_busy_i = '1' then
                chunk_length_trig_o <= '0'; --ask for a new chunk length if 1 (could be random).
        busy_mode: case busy_state is
                    when sob =>
                        busy_data <= "00" & "00000000" & Kchar_sob;
                        elinkdata_rdy <= '1';
                        busy_state <= idl;
                    when idl =>
                        elinkdata_rdy <= '0';
                        if (chunk_length < count_length) then
                            busy_state <= eob;
                        end if;
                    when eob =>
                        busy_data <= "00" & "00000000" & Kchar_eob;
                        elinkdata_rdy <= '1';
                end case busy_mode;
            else
                busy_state <= sob;
        normal_mode: case state is
                    when idl =>
                        data16b  <= (others => '0');
                        is_header <= '1';
                        chunk_length_trig_o <= '0'; --ask for a new chunk length if 1 (could be random).
                        if  valid_out = '1' then
                            state    <= soc;
                            dataCode <= sof_code;
                            elinkdata_rdy <= '1';     -- MBO: added line to assert elinkdata_rdy_o
                            read_en <= '0'; -- SS making read_en 0 after getting the trigger
                            l1a_id_Fifo   <= data_out;
                        else
                            read_en <= '1'; -- RL read_en value was unknown until the first state change
                            dataCode <= comma_code;  -- MBO: don't care as long as elinkdata_rdy_o is '0'
                            elinkdata_rdy <= '0';     -- MBO: added line to release elinkdata_rdy_o
                        end if;
                    when soc =>
                        state <= h1;
                        dataCode <= data_code;
                        --mbo: 0xAA,CL_MSB (or 0x00)?
                        data16b(15 downto 8) <= X"AA";
                        data16b( 7 downto 0) <= X"0" & chunk_length(11 downto 8);
                    when h1 =>
                        state <= h2;
                        dataCode <= data_code;
                        --mbo: CL_LSB,I1A_ID_LSB
                        data16b(15 downto 8) <= chunk_length(7 downto 2) & "00";-- NSW: Forced multiple of 4.
                        data16b( 7 downto 0) <= l1a_id_Fifo(15 downto 8);
                    when h2 =>
                        state <= h3;
                        dataCode <= data_code;
                        is_header <= '0';    -- MBO: enable counter at this point to allow for DSP pipeline delay (1 clock)
                        --mbo: I1A_ID_MSB, 0xBB
                        data16b(15 downto 8) <= l1a_id_Fifo(7 downto 0);
                        data16b( 7 downto 0) <= X"BB";
                    when h3 =>
                        dataCode <= data_code;
                        --mbo: 0xAA,Fixed Value?
                        data16b(15 downto 8) <= X"AA";
                        if (ewidth = "000") then   data16b( 7 downto 0) <= X"02";
                        elsif (ewidth = "001") then    data16b( 7 downto 0) <= X"04";
                        elsif (ewidth = "010") then    data16b( 7 downto 0) <= X"08";
                        elsif (ewidth = "011") then    data16b( 7 downto 0) <= X"10";
                        --MT
                        elsif (ewidth = "100") then    data16b( 7 downto 0) <= X"20";
                        else           data16b( 7 downto 0) <= X"00";
                        end if;
                        if count_length = 0 then
                            state <= eoc;
                        elsif count_length = 4 then
                            state <= t1;
                        else
                            state <= payload1;
                        end if;
                    when payload1 =>
                        dataCode <= data_code;
                        data16b <= parity_bit & n_rel_bcid & vmm_id & chan & adc_out(9 downto 8);
                        state <= payload2;
                    when payload2 =>
                        dataCode <= data_code;
                        data16b <= adc_out(7 downto 0) & tdc;
                        if count_length = 4 then
                            state <= t1;
                        else
                            state <= payload1;
                        end if;
                    when t1 =>
                        dataCode <= data_code;
                        data16b <= "00" & vmm_missing & "0000" & hit_count(9 downto 8); --timeout bit set to 0
                        state <= t2;
                    when t2 =>
                        dataCode <= data_code;
                        data16b <= hit_count(7 downto 0) & final_checksum;
                        state <= eoc;
                    when eoc =>
                        state    <= idl;
                        dataCode <= eof_code;
                        data16b  <= (others => '0');  -- MBO: don't care.
                        count_chunk <= count_chunk + 1;
                        read_en <= '1';
                        --Frans 1
                        chunk_length_trig_o <= '1'; --ask for a new chunk length if 1 (could be random).
                end case normal_mode;
            end if;
        end if;
    end process;


    enable_lfsr <= '1' when state = payload2 else
                   '0';
    reset_flags <= '1' when state = soc else
                   '0';


    --ADC counter (10b)
    --Poly: x**10 + x**7 + 1
    adc_reg: process (clk, rst)
    begin
        if rst = '1' then
            --    adc <= "0000000010";
            adc <= "0110111100";
        elsif clk'event and clk = '1' then
            if enable_lfsr = '1' then
                adc <= adc(8 downto 0) & (adc(9) xor adc(6));
            end if;
        end if;
    end process;

    adc_out <= adc - 1; --max value of ADC is X3FE (dummy hits excluded)

    --TDC counter (8b)
    --Poly: x**8 + x**6 + x**5 + x**4 + 1
    --Transition through 0: X23 -> 0 -> X47
    tdc_reg: process (clk, rst)
    begin
        if rst = '1' then
            --    tdc <= "00000001";
            tdc <= "10111110";
        elsif clk'event and clk = '1' then
            if enable_lfsr = '1' then
                if tdc = X"23" then
                    tdc <= X"00";
                elsif tdc = X"00" then
                    tdc <= X"47";
                else
                    tdc <= tdc(6 downto 0) & (tdc(7) xor tdc(5) xor tdc(4) xor tdc(3));
                end if;
            end if;
        end if;
    end process;

    --N + rel BCID counter (4b)
    --Poly: x**4 + x**3 + 1
    --Transition through 0: X4 -> 0 -> X9
    n_rel_bcid_reg: process (clk, rst)
    begin
        if rst = '1' then
            --    n_rel_bcid <= "0001";
            n_rel_bcid <= "1001";
        elsif clk'event and clk = '1' then
            if enable_lfsr = '1' then
                if n_rel_bcid = X"4" then
                    n_rel_bcid <= X"0";
                elsif n_rel_bcid = X"0" then
                    n_rel_bcid <= X"9";
                else
                    n_rel_bcid <= n_rel_bcid(2 downto 0) & (n_rel_bcid(3) xor n_rel_bcid(2));
                end if;
            end if;
        end if;
    end process;

    --VMM id counter (3b)
    --Poly: x**3 + x**2 + 1
    --Transition through 0: X1 -> 0 -> X2
    vmm_id_reg: process (clk, rst)
    begin
        if rst = '1' then
            --    vmm_id <= "001";
            vmm_id <= "011";
        elsif clk'event and clk = '1' then
            if enable_lfsr = '1' then
                if vmm_id = "001" then
                    vmm_id <= "000";
                elsif vmm_id = "000" then
                    vmm_id <= "010";
                else
                    vmm_id <= vmm_id(1 downto 0) & (vmm_id(2) xor vmm_id(1));
                end if;
            end if;
        end if;
    end process;

    --chan counter (6b)
    --Poly: x**6 + x**5 + 1
    --Transition through 0: X31 -> 0 -> X22
    chan_reg: process (clk, rst)
    begin
        if rst = '1' then
            --    chan <= "000001";
            chan <= "011110";
        elsif clk'event and clk = '1' then
            if enable_lfsr = '1' then
                if chan = "110001" then
                    chan <= "000000";
                elsif chan = "000000" then
                    chan <= "100010";
                else
                    chan <= chan(4 downto 0) & (chan(5) xor chan(4));
                end if;
            end if;
        end if;
    end process;

    --hit counter (10b)
    hit_counter: process (clk, rst)
    begin
        if rst = '1' then
            hit_count <= (others => '0');
        elsif clk'event and clk = '1' then
            if reset_flags = '1' then
                hit_count <= (others => '0');
            elsif enable_lfsr = '1' then
                hit_count <= hit_count + 1;
            end if;
        end if;
    end process;


    --The parity bit doesn't cover the VMMid field
    parity_bit <= (not(even_parity_i)) xor n_rel_bcid(3) xor n_rel_bcid(2) xor n_rel_bcid(1) xor n_rel_bcid(0)
                  xor chan(5) xor chan(4) xor chan(3) xor chan(2) xor chan(1) xor chan(0)
                  xor adc_out(9) xor adc_out(8) xor adc_out(7) xor adc_out(6) xor adc_out(5) xor adc_out(4) xor adc_out(3) xor adc_out(2) xor adc_out(1) xor adc_out(0)
                  xor tdc(7) xor tdc(6) xor tdc(5) xor tdc(4) xor tdc(3) xor tdc(2) xor tdc(1) xor tdc(0);

    vmm_missing_reg: process (clk, rst)
    begin
        if rst = '1' then
            vmm_missing <= X"FF";
        elsif clk'event and clk = '1' then
            if reset_flags = '1' then
                vmm_missing <= X"FF";
            elsif enable_lfsr = '1' then
                case vmm_id is
                    when "000" => vmm_missing(0) <= '0';
                    when "001" => vmm_missing(1) <= '0';
                    when "010" => vmm_missing(2) <= '0';
                    when "011" => vmm_missing(3) <= '0';
                    when "100" => vmm_missing(4) <= '0';
                    when "101" => vmm_missing(5) <= '0';
                    when "110" => vmm_missing(6) <= '0';
                    when others => vmm_missing(7) <= '0';
                end case;
            end if;
        end if;
    end process;


    checksum_reg : process (clk, rst)
    begin
        if rst = '1' then
            checksum <= (others => '0');
        elsif clk'event and clk = '1' then
            if reset_flags = '1' then
                checksum <= (others => '0');
            elsif state = h1 or state = h2 or state = h3 or state = payload1 or state = payload2 then
                checksum <= ("00000000" & checksum(10 downto 8)) + ("000" & checksum(7 downto 0)) + ("000" & data16b(15 downto 8)) + ("000" & data16b(7 downto 0));

            elsif state = t1 then
                checksum <= ("00000000" & checksum(10 downto 8)) + ("000" & checksum(7 downto 0)) + ("000" & data16b(15 downto 8)) + ("000" & data16b(7 downto 0))
                            + ("00000" & vmm_missing(7 downto 2)) + ("000" & vmm_missing(1 downto 0) & "0000" & hit_count(9 downto 8))
                            + ("000" & hit_count(7 downto 0));
            end if;
        end if;
    end process;

    interm_checksum <= ("00000" & checksum(10 downto 8)) + checksum(7 downto 0);
    final_checksum <= not(interm_checksum(7)) & not(interm_checksum(6)) & not(interm_checksum(5)) & not(interm_checksum(4))
                      & not(interm_checksum(3)) & not(interm_checksum(2)) & not(interm_checksum(1)) & not(interm_checksum(0));

end Behavioral;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.
--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:  Michael Oberling
--
-- Design Name  elink_printer_printhead.c
-- Version:    1.0
-- Date:    9/13/2017
--
-- Description:  Coming soon.
--
-- Change Log:  V1.0 -
--
--==============================================================================

LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
    use UNISIM.VComponents.all;

entity elink_printer_printhead is
    port (
        --MT  clk240      : in  std_logic;
        clk      : in  std_logic;
        bit_stream_1    : in  std_logic_vector(1 downto 0);
        bit_stream_1_we          : in  std_logic;
        bit_stream_2    : in  std_logic_vector(1 downto 0);
        bit_stream_2_we          : in  std_logic;
        gbt_bit_stream_in  : in  std_logic_vector(1 downto 0);
        gbt_bit_stream_out  : out std_logic_vector(1 downto 0)

    );
end elink_printer_printhead;

architecture Behavioral of elink_printer_printhead is
begin
    --MT  reg_input : process(clk240)
    reg_input : process(clk)
    begin
        --MT    if clk240'event and clk240 ='1' then
        if clk'event and clk ='1' then
            if (bit_stream_1_we = '1') then
                gbt_bit_stream_out <= bit_stream_1;
            elsif (bit_stream_2_we = '1') then
                gbt_bit_stream_out <= bit_stream_2;
            else
                gbt_bit_stream_out <= gbt_bit_stream_in;
            end if;
        end if;
    end process reg_input;
end Behavioral;

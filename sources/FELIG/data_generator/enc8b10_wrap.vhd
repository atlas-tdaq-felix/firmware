--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               Israel Grayzman
--!               Ricardo Luz
--!               Ohad Shaked
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.
--! Engineer: juna
--!
--! Create Date:    06/19/2014
--! Module Name:    enc_8b10_wrap_NEW
--! Project Name:   FELIX
--!
--! Modified by Ohad Shaked
--!  Date: 09/11//2020
--! Modifed to support new enc_8b10
--!
----------------------------------------------------------------------------------
--! Use standard library
library IEEE, work;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.FELIX_package.all;
    use work.all;

-- Wrapper for 8b10b Encoder:
-- =========================

entity enc8b10_wrap is
    port (
        clk            : in  std_logic;
        rst            : in  std_logic;
        dataCode       : in  std_logic_vector (1 downto 0); -- 00"data, 01"eop, 10"sop, 11"comma
        dataIN         : in  std_logic_vector (7 downto 0);
        dataINrdy      : in  std_logic;
        Xoff_Set_Rdy   : in  std_logic := '0';
        Xoff_Clear_Rdy : in  std_logic := '0';
        encDataOut     : out  std_logic_vector (9 downto 0);
        encDataOutrdy  : out  std_logic
    );
end enc8b10_wrap;

architecture Behavioral of enc8b10_wrap is

    ----------------------------------
    ----------------------------------
    component MUX4_Nbit
        generic (N : integer := 1);
        port (
            data0    : in  std_logic_vector((N-1) downto 0);
            data1    : in  std_logic_vector((N-1) downto 0);
            data2    : in  std_logic_vector((N-1) downto 0);
            data3    : in  std_logic_vector((N-1) downto 0);
            sel      : in  std_logic_vector(1 downto 0);
            data_out : out std_logic_vector((N-1) downto 0)
        );
    end component MUX4_Nbit;

    ----------------------------------
    ----------------------------------
    component enc_8b10b
        port(
            reset   : in std_logic;
            clk   : in std_logic ;
            ena   : in std_logic ;
            KI   : in std_logic ;            -- Control (K) input(active high)
            datain : in std_logic_vector(7 downto 0);
            dataout: out std_logic_vector(9 downto 0)
        );
    end component enc_8b10b;
    ----------------------------------
    ----------------------------------



    signal isk : std_logic := '1';
    signal encoder_rst, enc_ena_s : std_logic;
    signal enc_ena, encoder_rst_delayed, encoder_rst_clk1 : std_logic := '1';
    signal dataINrdy_s : std_logic;

    signal rst_state : std_logic := '1';
    signal dataIN_s, byte : std_logic_vector(7 downto 0);
    signal dataCode_s : std_logic_vector(1 downto 0) := (others => '1');
    signal encDataOut_int: std_logic_vector(9 downto 0);

begin

    --IG dataINrdy_s <= dataINrdy and (not encoder_rst);
    dataINrdy_s <= (dataINrdy or Xoff_Set_Rdy or Xoff_Clear_Rdy) and (not encoder_rst);

    -------------------------------------------------------------------------------------------
    -- input registers
    -------------------------------------------------------------------------------------------
    process(clk)
    begin
        if clk'event and clk = '1' then
            rst_state <= rst;
        end if;
    end process;
    --
    process(clk)
    begin
        if clk'event and clk = '1' then
            if dataINrdy_s = '1' then
                dataIN_s    <= dataIN;
                dataCode_s  <= dataCode;
            --IG      isk         <= dataCode(1) or dataCode(0);
            --rst_state   <= '0';
            --    else
            --      dataIN_s    <= Kchar_comma;
            --          dataCode_s  <= "11";
            end if;
        end if;
    end process;
    --
    encoder_rst <= rst_state or rst;
    --

    -------------------------------------------------------------------------------------------
    -- data code cases
    -- 00"data, 01"eop, 10"sop, 11"comma
    -------------------------------------------------------------------------------------------
    --IG inmux: MUX4_Nbit
    --IG generic map (N=>8)
    --IG port map (
    --IG   data0    => dataIN_s,
    --IG   data1    => Kchar_eop,
    --IG   data2    => Kchar_sop,
    --IG   data3    => Kchar_comma,
    --IG   sel      => dataCode_s,
    --IG   data_out => byte
    --IG   );

    process(rst, clk)
    begin
        if (rst = '1') then
            byte        <= (others => '0');
            isk         <= '0';
        elsif rising_edge(clk) then
            if    (rst_state = '1') then
                byte        <= (others => '0');
                isk         <= '0';
            elsif (Xoff_Set_Rdy = '1') then
                byte        <= Kchar_FM_XOFF;
                isk         <= '1';
            elsif (Xoff_Clear_Rdy = '1') then
                byte        <= Kchar_FM_XON;
                isk         <= '1';
            elsif (dataCode = "00") then
                byte        <= dataIN;
                isk         <= '0';
            elsif (dataCode = "01") then
                byte        <= Kchar_eop;
                isk         <= '1';
            elsif (dataCode = "10") then
                byte        <= Kchar_sop;
                isk         <= '1';
            else--(dataCode = "11")
                byte        <= Kchar_comma;
                isk         <= '1';
            end if;
        end if;
    end process;
    --

    -------------------------------------------------------------------------------------------
    -- 8b10b encoder
    -------------------------------------------------------------------------------------------
    process(clk)
    begin
        -- Orig:  if clk'event and clk = '0' then
        if clk'event and clk = '1' then     -- Rising edge!!
            enc_ena <= dataINrdy_s or encoder_rst;
            encoder_rst_clk1 <= encoder_rst;
            encoder_rst_delayed <= encoder_rst_clk1;
        end if;
    end process;
    --
    enc_ena_s <= enc_ena or encoder_rst_delayed;
    --
    enc_8b10bx: enc_8b10b
        port map(
            reset   => encoder_rst,  -- Global asynchronous reset (active high)
            clk   => clk,
            ena    => enc_ena_s,
            KI      => isk,      -- Control (K) input(active high)
            datain  => byte,      -- Unencoded input data
            dataout => encDataOut_int    -- Encoded out
        );

    -- Orig.
    -- encDataOut <= encDataOut_int;

    -- OS:   Reverse the New encoder Data out LSB <-> MSB in order to maintain compatibility with the original 8b10b encoder bit order.
    --     AO -> Dout(0) (LSB)
    encDataOut <= encDataOut_int(0) & encDataOut_int(1) & encDataOut_int(2) & encDataOut_int(3) & encDataOut_int(4) & encDataOut_int(5) & encDataOut_int(6) & encDataOut_int(7) & encDataOut_int(8) & encDataOut_int(9); -- Assign encoded out AO to DataOut(0), BO to DataOut(1), etc.


    process(clk)
    begin
        if clk'event and clk = '1' then
            encDataOutrdy <= dataINrdy_s and (not encoder_rst);
        end if;
    end process;
--

end Behavioral;


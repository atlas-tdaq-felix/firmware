--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!               Ricardo Luz
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:  Michael Oberling
--
-- Design Name:  elink_printer_bit_feeder
-- Version:    1.0
-- Date:    9/13/2017
--
-- Description:  Coming soon.
--
-- Change Log:  V1.0 -
--         V1.1 - added dynamic bit-wise endian control.
--
--=============================================================================

LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

--TO DO: implemenent 32b elink

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
    use UNISIM.VComponents.all;

entity elink_printer_bit_feeder is
    generic (
        enable_endian_control  : std_logic;                      -- if '0', then operation is always in little endian.
        DATARATE                : integer := 0                      --0=4.8 Gbps (GBT, Phase1),
    --1=5.12 Gbps (LPGBT, Phase2)
    --2=10.24 Gbps(LPGBT, Phase2)

    );
    port (
        --MT  clk240      : in  std_logic;
        clk      : in  std_logic;
        enable      : in  std_logic;                    --enable output, read_enable
        elink_endian_mode  : in  std_logic;                    --bitwise endianness: '0' little endian (8b10b), '1' big endian.
        elink_output_width  : in  std_logic_vector(2 downto 0); --MT (1 downto
        --0) --
        --"000","001","010","011", "100" = 2,4,8,16,32-bit elink width
        elink_input_width  : in  std_logic;        -- '0' = 8-bit input, '1' = 10 bit-input
        word_in      : in  std_logic_vector(9 downto 0); --from upstreamEpathFifoWrap
        bit_stream_1    : out std_logic_vector(1 downto 0); --main output : 2,4,8,16,32 elinks
        bit_stream_1_we          : out std_logic;                    --main valid
        bit_stream_2    : out std_logic_vector(1 downto 0); --secondary output: -2 elink only
        bit_stream_2_we          : out std_logic;                    --secondary valid
        read_enable    : out std_logic;                    --rd_en of upstreamEpathFifoWrap
        --  bit_stream_en    : in  std_logic_vector(2 downto 0);
        bit_stream_en    : in  std_logic_vector(3 downto 0); -- enable for (0): 2b/4b (1): 8b (2): 16b (3): 32b                                                                          --
        bit_stream_sync    : in  std_logic                     --sync by zeroing the counter
    );
end elink_printer_bit_feeder;

--MT (?) not sure I understand this table
-- bitwise endianness: '0' little endian (8b10b), '1' big endian.
-- words are always divided and shifted form lsb to msb.
-- e.g.:
--
-- X = Don't care
-- N = Next input word
-- ... = Pattern continues
--
-- 8 bit input (Direct-Mode), little endian:
-- Input (9 downto 0) : XX76543210
--          GBT Word# : |        1st        |        2nd        |
-- Output 16-bit elink: | NNNNNNNN76543210  | ...               |
--          GBT Word# : |   1st   |   2nd   |   3rd   |   4th   |
-- Output 8-bit elink : |76543210 | ...     | ...     | ...     |
--          GBT Word# : |1st |2nd |3rd |4th |5th |6th |7th |8th |
-- Output 4-bit elink : |3210|7654|... |... |... |... |... |... |
-- Output 2-bit elink : |  10|  43|  54|  76|... |... |... |... |
--
-- 8 bit input (Direct-Mode), big endian:
-- Input (9 downto 0) : XX76543210
--          GBT Word# : |        1st        |        2nd        |
-- Output 16-bit elink: | NNNNNNNN01234567  | ...               |
--          GBT Word# : |   1st   |   2nd   |   3rd   |   4th   |
-- Output 8-bit elink : |01234567 |NNNNNNNN | ...     | ...     |
--          GBT Word# : |1st |2nd |3rd |4th |5th |6th |7th |8th |
-- Output 4-bit elink : |0123|4567|NNNN|NNNN|... |... |... |... |
-- Output 2-bit elink : |  01|  34|  45|  67|... |... |... |... |
--
-- 10 bit input (8b10b-Mode), little endian:
-- Input (9 downto 0) : ABCDEIFGHJ
--          GBT Word# : |        1st        |        2nd        |
-- Output 16-bit elink: | NNNNNNABCDEIFGHJ  | ............NNNN  |
--          GBT Word# : |   1st   |   2nd   |   3rd   |   4th   |
-- Output 8-bit elink : |CDEIFGHJ | NNNNNNAB| ....NNNN|...      |
--          GBT Word# : |1st |2nd |3rd |4th |5th |6th |7th |8th |
-- Output 4-bit elink : |FGHJ|CDEI|NNAB|NNNN|NNNN|... |... |... |
-- Output 2-bit elink : |  HJ|  FG|  EI|  CD|  AB|... |... |... |
--Q. for 16b elink every 40Mhz cycle 4b left behind (see NNNN in 2nd): does
--this mean that at the 4th cycle rd_en will go up only one time instead of
--two? If not I will loose a word
--
-- 10 bit input (8b10b-Mode), big endian:
-- Input (9 downto 0) : ABCDEIFGHJ
-- Output 16-bit elink: not supported
-- Output 8-bit elink : not supported
--          GBT Word# : |1st |2nd |3rd |4th |5th |6th |7th |8th |
-- Output 4-bit elink : |JHGF|IEDC|BANN|NNNN|NNNN|... |... |... |
-- Output 2-bit elink : |  JH|  GF|  IE|  DC|  BA|... |... |... |

--        |  shift_op pattern          |  pattern_length                  |  shift cycles / GBT word(MT: ??)
--  8  into 2:  0-000  |  0,2,2,2,      |  4        |  1
--  10 into 2:  1-000  |  0,2,2,2,2,      |  5        |  1
--  8  into 4:  0-001  |  0,3,        |  2        |  1
--  10 into 4:  1-001  |  0,3,1,3,3,      |  5        |  1
--  8  into 8:  0-010  |  0,3,        |  2        |  2
--  10 into 8:  1-010  |  0,3,1,3,3,      |  5        |  2
--  8  into 16:  0-011  |  0,3,        |  2        |  4
--  10 into 16:  1-011  |  0,3,1,3,3,      |  5        |  4
--  8  into 32:  0-100  |  0,3,        |  2        |  4
--  10 into 32:  1-100  |  0,3,1,3,3,      |  5        |  4
--MT N.B: given the same input, output-4,8,16 has same sequence (3 to shift 4b, 0
--to load a new word, and in the case of input=10 a 1 to shift 2b)

architecture Behavioral of elink_printer_bit_feeder is
    signal shift_en_q      : std_logic            := '0';
    signal shift_en_d      : std_logic            := '0';
    signal read_enable_d    : std_logic            := '0';
    --MT Jun9
    signal read_enable_dd    : std_logic            := '0';
    signal read_enable_ddd    : std_logic            := '0';
    signal read_enable_dddd    : std_logic            := '0';
    signal bit_stream_1_we_d  : std_logic            := '0';
    signal bit_stream_2_we_d  : std_logic            := '0';
    signal shift_mode      : std_logic            := '0';
    signal input_width      : std_logic            := '0';
    --DATARATE=0    10b word + 2b for shifting = 12b
    --DATARATE=3or4 10b word + 8b for shifting = 18b
    signal reg_d        : std_logic_vector(11 downto 0)  := (others => '0');
    signal reg_q        : std_logic_vector(11 downto 0)  := (others => '0');
    --DATARATE=0
    --  signal shift_op        : std_logic_vector(1 downto 0)  := (others => '0');
    --DATARATE=2 or 3 need an extra option: shift by 8
    signal shift_op        : std_logic_vector(2 downto 0)  := (others => '0');
    --allowing for ELINKWIDTH=32 need an extra bit
    --  signal output_width      : std_logic_vector(1 downto 0)  := (others => '0');
    signal output_width      : std_logic_vector(2 downto 0)  := (others => '0');
    signal cycle_count_q            : std_logic_vector(2 downto 0)  := (others => '0');
    signal cycle_count_d            : std_logic_vector(2 downto 0)  := (others => '0');

    --MT SIMU+
    signal word_lat_ext          : std_logic_vector(word_in'range)  := (others => '0');

--
begin
    -- Complier optimization may yield different LUT usage/configuration,
    -- than is described in the VHDL, and associated comments. However, regardless of
    -- the details of implementation, the resource utilization and levels of logic
    -- should only be less than or equal to what is described below.

    gen_static_endian : if (enable_endian_control = '0') generate
        bit_stream_1  <=  reg_q (1 downto 0);
        bit_stream_2    <=  reg_q (3 downto 2);

        -- LUT6 implementing a MUX4 per FF (x10), some optimization will occur.
        reg_d  <=     "00" & word_in      when (shift_op = "00") else  -- Mux input 0: latch next word, aligned output
                  word_in & reg_q(5 downto 4)    when (shift_op = "01") else  -- Mux input 1: shift by 2(?) and latch next word, offset
                  "00" & reg_q(11 downto 2)          when (shift_op = "10") else  -- Mux input 2: shift by 2
                  "0000" & reg_q(11 downto 4);         -- when (shift_op = "11");  -- Mux input 3: shift by 4
    end generate gen_static_endian;

    gen_dynamic_endian : if (enable_endian_control = '1') generate
        -- one clock of added latency when endian control is enabled.
        -- ned to check effect on 16-bit elink mode.
        signal word_lat          : std_logic_vector(word_in'range)  := (others => '0');
        signal reverse_2bit        : std_logic              := '0';
        signal reverse_4bit        : std_logic              := '0';
        signal bit_manipulation_mode  : std_logic_vector(2 downto 0)    := (others => '0');
    begin
        --MT SIMU+
        word_lat_ext <= word_lat;
        --

        -- additional LUT5 on output bits.

        --or reverse_2bit =1 (elink_endian_mode=1)
        bit_stream_1  <=  reg_q (0) & reg_q (1) when (reverse_2bit = '1' and reverse_4bit = '0') else
                         reg_q (2) & reg_q (3) when (reverse_2bit = '0' and reverse_4bit = '1') else
                         reg_q (1 downto 0);
        bit_stream_2  <=  reg_q (2) & reg_q (3) when (reverse_2bit = '1' and reverse_4bit = '0') else
                         reg_q (0) & reg_q (1) when (reverse_2bit = '0' and reverse_4bit = '1') else
                         reg_q (3 downto 2);



        -- LUT6 implementing a MUX4 per FF (x10), some optimization will occur.
        reg_d  <=  "00"        & word_lat       when (shift_op = "000") else            -- Mux input 0: latch next word, aligned output
                  word_lat & reg_q(5 downto 4) when (shift_op = "001") else            -- Mux input 1: shift by 2 and latch next word, offset
                  "00"        & reg_q(11 downto 2)           when (shift_op = "010") else            -- Mux input 2: shift by 2
                  "0000"      & reg_q(11 downto 4)           when (shift_op = "011");               -- Mux input 3: shift by 4

        --                word_lat <= word_in; --ph2
        bit_manipulation_mode <= elink_endian_mode & input_width & output_width(1);

        --MT    user_configurable_bit_mangling : process(clk240)
        user_configurable_bit_mangling : process(clk)
        begin
            --MT      if clk240'event and clk240 ='1' then
            if clk'event and clk ='1' then
                case (bit_manipulation_mode) is
                    when '1' & '0' & '0' =>  -- big-endian, 8 in, 2 or 4 out
                        reverse_2bit  <= output_width(0);
                        reverse_4bit  <= not output_width(0);
                        word_lat    <= word_in;
                    when '1' & '1' & '0' =>  -- big-endian, 10 in, 2 or 4 out
                        reverse_2bit  <= output_width(0);
                        reverse_4bit  <= not output_width(0);
                        word_lat    <= word_in;
                    when '1' & '0' & '1' =>  -- big-endian, 8 in, 8 or 16 out
                        reverse_2bit  <= '0';
                        reverse_4bit  <= '1';
                        word_lat    <= word_in(9 downto 8) & word_in(3 downto 0) & word_in(7 downto 4); -- upper 2 two = don't care
                    when others =>  -- little-endian, all modes
                        -- big-endian with 10 bit input and 8 or 16 bit output is note supported and also falls to the case
                        reverse_2bit  <= '0';
                        reverse_4bit  <= '0';
                        word_lat    <= word_in;
                end case;
            end if;
        end process user_configurable_bit_mangling;
    end generate gen_dynamic_endian;

    -- LUT5 implementing a MUX4(3)
    shift_en_d  <=  bit_stream_en(0) when (output_width = "000") else  -- Mux input 0: 2 bit output, use 1 clock wide enable
                   bit_stream_en(0) when (output_width = "001") else  -- Mux input 1: 4 bit output, use 1 clock wide enable
                   bit_stream_en(1) when (output_width = "010") else  -- Mux input 2: 8 bit output, use 2 clock wide enable
                   bit_stream_en(2) when (output_width = "011") else  -- Mux input 3: 16 bit output, use 4 clock wide enable
                   bit_stream_en(3) when (output_width = "100") else  -- Mux input 4: 32 bit output, use 4 clock wide enable
                   '0';

    --1clk ph1 delay works for 2,4,6 elink
    --MT  read_enable_d    <= '1' when ((enable = '1' ) and (shift_en_q = '1') and (shift_op(1) = '0')) else
    --  '0';
    --MT ph1 16belink 2-clk delay
    read_enable_dd    <= '1' when ((enable = '1' ) and (shift_en_q = '1') and (shift_op(1) = '0')) else '0';
    --MT ph1 16belink 4-clk delay (have to compensate for gbt_count going up to 7
    --rather than 5 as for ph1)
    --read_enable_dddd    <= '1' when ((enable = '1' ) and (shift_en_q = '1') and (shift_op(1) = '0')) else '0';        --
    bit_stream_1_we_d  <= '1' when ((enable = '1' ) and (shift_en_q = '1')) else '0';
    bit_stream_2_we_d  <= '1' when ((enable = '1' ) and (shift_en_q = '1') and (shift_mode = '1')) else '0';

    -- LUT5 x 3
    cycle_count_d  <=  "001" when (shift_mode = '0' and input_width = '0' and cycle_count_q = "000") else  -- 2 bit direct output, 8-bit input
                      "010" when (shift_mode = '0' and input_width = '0' and cycle_count_q = "001") else  -- 2 bit direct output, 8-bit input
                      "011" when (shift_mode = '0' and input_width = '0' and cycle_count_q = "010") else  -- 2 bit direct output, 8-bit input
                      
                      "001" when (shift_mode = '0' and input_width = '1' and cycle_count_q = "000") else  -- 2 bit direct output, 10-bit input
                      "010" when (shift_mode = '0' and input_width = '1' and cycle_count_q = "001") else  -- 2 bit direct output, 10-bit input
                      "011" when (shift_mode = '0' and input_width = '1' and cycle_count_q = "010") else  -- 2 bit direct output, 10-bit input
                      "111" when (shift_mode = '0' and input_width = '1' and cycle_count_q = "011") else  -- 2 bit direct output, 10-bit input
                      
                      "001" when (shift_mode = '1' and input_width = '0' and cycle_count_q = "000") else  -- 4,8,16,32 bit direct output, 8-bit input
                      "001" when (shift_mode = '1' and input_width = '1' and cycle_count_q = "000") else  -- 4,8,16,32 bit direct output, 10-bit input
                      "010" when (shift_mode = '1' and input_width = '1' and cycle_count_q = "001") else  -- 4,8,16,32 bit direct output, 10-bit input
                      "011" when (shift_mode = '1' and input_width = '1' and cycle_count_q = "010") else  -- 4,8,16,32 bit direct output, 10-bit input
                      "111" when (shift_mode = '1' and input_width = '1' and cycle_count_q = "011") else  -- 4,8,16,32 bit direct output, 10-bit input
                      
                      "000";

    -- LUT3 x2
    shift_op  <=  "010" when (shift_mode = '0' and cycle_count_q(1 downto 0) = "01") else
                 "010" when (shift_mode = '0' and cycle_count_q(1 downto 0) = "10") else
                 "010" when (shift_mode = '0' and cycle_count_q(1 downto 0) = "11") else
                 
                 "011" when (shift_mode = '1' and cycle_count_q(1 downto 0) = "01") else
                 "001" when (shift_mode = '1' and cycle_count_q(1 downto 0) = "10") else
                 "011" when (shift_mode = '1' and cycle_count_q(1 downto 0) = "11") else
                 
                 "000";

    --MT  reg_input : process(clk240)
    reg_input : process(clk)
    begin
        --    if clk240'event and clk240 ='1' then
        if clk'event and clk ='1' then
            read_enable      <= read_enable_d;
            --MT Jun 9
            read_enable_d      <= read_enable_dd;
            --read_enable_dd      <= read_enable_ddd;
            --read_enable_ddd      <= read_enable_dddd;

            bit_stream_1_we  <= bit_stream_1_we_d;
            bit_stream_2_we  <= bit_stream_2_we_d;
            shift_en_q    <= shift_en_d;

            if (shift_en_q = '1') then
                reg_q <= reg_d;
            end if;

            if (bit_stream_sync = '1') then
                cycle_count_q <= (others => '0');
            elsif (shift_en_q = '1') then
                cycle_count_q <= cycle_count_d;
            end if;
        end if;
    end process reg_input;

    --  mode_reg : process(clk240)
    mode_reg : process(clk)
    begin
        --MT    if clk240'event and clk240 ='1' then
        if clk'event and clk ='1' then
            output_width  <= elink_output_width;
            input_width    <= elink_input_width;
            --MT      if (elink_output_width =  "00") then
            if (elink_output_width =  "000") then
                shift_mode <= '0';
            else
                shift_mode <= '1';
            end if;
        end if;
    end process mode_reg;
end Behavioral;

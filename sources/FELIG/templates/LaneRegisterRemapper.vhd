--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!               Ricardo Luz
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:  Michael Oberling
--
-- Design Name:  Lane Register Remapper
-- Version:    1.0
-- Date:    5/18/2018
--
-- Description:   Remaps registers into per lane controls and monitors
--
-- Change Log:  V1.0 -
--
--==============================================================================
LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
--use ieee.numeric_std.all;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.type_lib.ALL;
    use work.ip_lib.ALL;
--use work.function_lib.ALL;

    use work.pcie_package.all;
    use work.FELIX_package.all;

entity LaneRegisterRemapper is
    generic
(
        GBT_NUM                     : integer := 24;
        FIRMWARE_MODE               : integer := FIRMWARE_MODE_FELIG_GBT
    );
    port(
        register_map_monitor        : out  register_map_monitor_type;
        register_map_control        : in  register_map_control_type;
        register_map_hk_monitor_in  : in register_map_hk_monitor_type;
        register_map_hk_monitor_out : out register_map_hk_monitor_type;
        LMK_LOCKED                  : in std_logic;
        LinkAligned                 : in std_logic_vector(GBT_NUM-1 downto 0);
        lane_control                : out  array_of_lane_control_type(GBT_NUM-1 downto 0);
        lane_monitor                : in  array_of_lane_monitor_type(GBT_NUM-1 downto 0)
    );
end entity LaneRegisterRemapper;

architecture RTL of LaneRegisterRemapper is
    signal felig_data_gen_config            : bitfield_felig_data_gen_config_w_type_array(lane_control'range);
    signal felig_elink_config               : bitfield_felig_elink_config_w_type_array(lane_control'range);
    signal felig_data_gen_config_userdata   : array_of_slv_15_0(lane_control'range);
    signal felig_elink_enable_orig          : array_of_slv_39_0(lane_control'range);
    signal felig_elink_enable               : array_of_slv_111_0(lane_control'range);
    signal felig_elink_endian_mode          : array_of_slv_111_0(lane_control'range);
    signal felig_elink_input_width          : array_of_slv_111_0(lane_control'range);
    signal felig_elink_output_width         : array_of_array_0_111_slv_2_0(lane_control'range);
    signal felig_lane_config                : bitfield_felig_lane_config_w_type_array(lane_control'range);
begin

    --loop over links
    GEN_LANE_CONTROL_MAP : for i in lane_control'range generate
        lane_control(i).global.FEC                          <= register_map_control.LPGBT_FEC(i);  --FEC5 0 FEC12 1
        lane_control(i).global.DATARATE                     <= '1'; -- not register_map_control.LPGBT_DATARATE(0) --data rate 5.12 not implemented and not needed --5.12 0 10.24 1 (register needs to be negated)
        lane_control(i).global.aligned                      <= LinkAligned(i);
        felig_data_gen_config(i)                            <= register_map_control.FELIG_DATA_GEN_CONFIG(i)                ;
        felig_data_gen_config_userdata(i)                   <= register_map_control.FELIG_DATA_GEN_CONFIG_USERDATA(i)       ;
        felig_elink_config(i)                               <= register_map_control.FELIG_ELINK_CONFIG(i)                   ;
        felig_elink_enable_orig(i)                          <= register_map_control.FELIG_ELINK_ENABLE(i)                   ;
        felig_lane_config(i)                                <= register_map_control.FELIG_LANE_CONFIG(i)                    ;
        lane_control(i).global.lane_reset                   <= register_map_control.FELIG_RESET.LANE            (i+register_map_control.FELIG_RESET.LANE'low);
        lane_control(i).global.framegen_reset               <= register_map_control.FELIG_RESET.FRAMEGEN          (i+register_map_control.FELIG_RESET.FRAMEGEN'low);
        lane_control(i).global.elink_sync                   <= felig_lane_config(i).ELINK_SYNC                (felig_lane_config(i).ELINK_SYNC'low);
        lane_control(i).global.framegen_data_select         <= felig_lane_config(i).FG_SOURCE                (felig_lane_config(i).FG_SOURCE'low);
        lane_control(i).global.emu_data_select              <= felig_lane_config(i).GBT_EMU_SOURCE              (felig_lane_config(i).GBT_EMU_SOURCE'low);
        lane_control(i).global.l1a_source                   <= felig_lane_config(i).L1A_SOURCE                (felig_lane_config(i).L1A_SOURCE'low);
        lane_control(i).global.loopback_fifo_delay          <= felig_lane_config(i).LB_FIFO_DELAY              ;
        lane_control(i).global.loopback_fifo_reset          <= register_map_control.FELIG_RESET.LB_FIFO            (register_map_control.FELIG_RESET.LB_FIFO'low);
        lane_control(i).global.a_ch_bit_sel                 <= felig_lane_config(i).A_CH_BIT_SEL              ;
        lane_control(i).global.b_ch_bit_sel                 <= felig_lane_config(i).B_CH_BIT_SEL(48 downto 42)              ;
        lane_control(i).global.l1a_counter_reset            <= register_map_control.FELIG_L1ID_RESET                        (register_map_control.FELIG_L1ID_RESET'low);
        lane_control(i).global.MSB                          <= register_map_control.ENCODING_REVERSE_10B                    (register_map_control.ENCODING_REVERSE_10B'low);
        lane_control(i).global.l1a_max_count                <= register_map_control.FELIG_GLOBAL_CONTROL.FAKE_L1A_RATE;
        lane_control(i).fmemu_random.FMEMU_RANDOM_RAM_ADDR  <= register_map_control.FMEMU_RANDOM_RAM_ADDR;
        lane_control(i).fmemu_random.FMEMU_RANDOM_RAM       <= register_map_control.FMEMU_RANDOM_RAM;
        lane_control(i).fmemu_random.FMEMU_RANDOM_CONTROL   <= register_map_control.FMEMU_RANDOM_CONTROL;

        --loop over egroups
        GEN_EGROUPS: for j in lane_control(0).emulator'range generate
            signal felig_output_width       : std_logic_vector(2 downto 0);
            signal felig_output_width_array : array_of_slv_2_0(15 downto 0);
            signal felig_input_width        : std_logic;
            signal felig_endian_mode        : std_logic;
        begin
            lane_control(i).emulator(j).pattern_select(0)   <= felig_data_gen_config(i).PATTERN_SEL(j+felig_data_gen_config(i).PATTERN_SEL'low);
            lane_control(i).emulator(j).pattern_select(1)   <= '0';
            lane_control(i).emulator(j).data_format(0)      <= felig_data_gen_config(i).DATA_FORMAT(2*j+felig_data_gen_config(i).DATA_FORMAT'low);
            lane_control(i).emulator(j).data_format(1)      <= felig_data_gen_config(i).DATA_FORMAT(2*j+1+felig_data_gen_config(i).DATA_FORMAT'low);
            lane_control(i).emulator(j).sw_busy             <= felig_data_gen_config(i).SW_BUSY(j+felig_data_gen_config(i).SW_BUSY'low);
            lane_control(i).emulator(j).reset               <= felig_data_gen_config(i).RESET(j+felig_data_gen_config(i).RESET'low);
            lane_control(i).emulator(j).chunk_length        <= felig_data_gen_config(i).CHUNK_LENGTH ;
            lane_control(i).emulator(j).userdata            <= felig_data_gen_config_userdata(i);

            felig_output_width <= felig_elink_config(i).OUTPUT_WIDTH(j*3+2 downto j*3);
            felig_input_width  <= felig_elink_config(i).INPUT_WIDTH(j+felig_elink_config(i).INPUT_WIDTH'low);
            felig_endian_mode  <= felig_elink_config(i).ENDIAN_MOD(j+felig_elink_config(i).ENDIAN_MOD'low);

            GEN_ENABLES_GBT : if FIRMWARE_MODE = FIRMWARE_MODE_FELIG_GBT generate
                less_that_five : if j<5 generate
                    felig_elink_enable(i)(j*16+7 downto j*16)       <= felig_elink_enable_orig(i)(j*8+7 downto j*8);
                    felig_elink_enable(i)(j*16+15 downto j*16+8)    <= X"00";
                end generate less_that_five;
                more_that_five : if j>4 generate
                    felig_elink_enable(i)(j*16+15 downto j*16)      <= (others=>'0');
                end generate more_that_five;
            end generate GEN_ENABLES_GBT;

            GEN_ENABLES_LPGBT : if FIRMWARE_MODE = FIRMWARE_MODE_FELIG_LPGBT generate
                felig_elink_enable(i)(j*16+15 downto j*16)  <= "000" & felig_elink_enable_orig(i)(4*j+3) &
                                                               "000" & felig_elink_enable_orig(i)(4*j+2) &
                                                               "000" & felig_elink_enable_orig(i)(4*j+1) &
                                                               "000" & felig_elink_enable_orig(i)(4*j);
            end generate GEN_ENABLES_LPGBT;

            felig_output_width_array(0)                 <= felig_output_width;
            felig_output_width_array(1)                 <= "000";
            felig_output_width_array(2)                 <= "00" & felig_output_width(0);
            felig_output_width_array(3)                 <= "000";
            felig_output_width_array(4)                 <= '0' & felig_output_width(1 downto 0);
            felig_output_width_array(5)                 <= "000";
            felig_output_width_array(6)                 <= "00" & felig_output_width(0);
            felig_output_width_array(7)                 <= "000";
            felig_output_width_array(8)                 <= '0' & felig_output_width(1 downto 0);
            felig_output_width_array(9)                 <= "000";
            felig_output_width_array(10)                <= "00" & felig_output_width(0);
            felig_output_width_array(11)                <= "000";
            felig_output_width_array(12)                <= '0' & felig_output_width(1 downto 0);
            felig_output_width_array(13)                <= "000";
            felig_output_width_array(14)                <= "00" & felig_output_width(0);
            felig_output_width_array(15)                <= "000";

            lane_control(i).emulator(j).output_width    <= felig_output_width;

            GEN_WIDTHS : for h in 0 to 15 generate
                felig_elink_output_width(i)(j*16+h)     <= felig_output_width_array(h);
                felig_elink_input_width(i)(j*16+h)      <= felig_input_width;
                felig_elink_endian_mode(i)(j*16+h)      <= felig_endian_mode;
            end generate GEN_WIDTHS;
        end generate GEN_EGROUPS;
        --loop over epaths
        GEN_LANE_ELINK_CONTROL_MAP : for j in lane_control(0).elink'range generate --RL:changed so it compiles. sizes need to match 0 to 39 generate --
            lane_control(i).elink(j).output_width       <= felig_elink_output_width(i)(j);
            lane_control(i).elink(j).input_width        <= felig_elink_input_width(i)(j);
            lane_control(i).elink(j).endian_mode        <= felig_elink_endian_mode(i)(j);
            lane_control(i).elink(j).enable             <= felig_elink_enable(i)(j);
        end generate GEN_LANE_ELINK_CONTROL_MAP;
    end generate GEN_LANE_CONTROL_MAP;

    GEN_LANE_MONITOR_MAP : for i in lane_monitor'range generate
        register_map_monitor.register_map_link_monitor.GBT_ALIGNMENT_DONE(i)                <= lane_monitor(i).gbt.frame_locked;
        register_map_monitor.register_map_link_monitor.GBT_RX_IS_HEADER(i)                  <= lane_monitor(i).gbt.rx_is_header;
        register_map_monitor.register_map_link_monitor.GBT_RX_HEADER_FOUND(i)               <= lane_monitor(i).gbt.rx_header_found;
        register_map_monitor.register_map_link_monitor.GBT_ERROR(i)                         <= lane_monitor(i).gbt.error;
        register_map_monitor.register_map_link_monitor.GBT_TXRESET_DONE(i)                  <= lane_monitor(i).gth.txreset_done;
        register_map_monitor.register_map_link_monitor.GBT_RXRESET_DONE(i)                  <= lane_monitor(i).gth.rxreset_done;
        register_map_monitor.register_map_link_monitor.GBT_TXFSMRESET_DONE(i)               <= lane_monitor(i).gth.txfsmreset_done;
        register_map_monitor.register_map_link_monitor.GBT_RXFSMRESET_DONE(i)               <= lane_monitor(i).gth.rxfsmreset_done;
        register_map_monitor.register_map_generators.FELIG_MON_COUNTERS(i).SLIDE_COUNT      <= lane_monitor(i).gbt.rxslide_count;
        register_map_monitor.register_map_generators.FELIG_MON_COUNTERS(i).FC_ERROR_COUNT   <= lane_monitor(i).global.fc_error_count;
        register_map_monitor.register_map_generators.FELIG_MON_FREQ(i).RX                   <= lane_monitor(i).clock.freq_rx_clk;
        register_map_monitor.register_map_generators.FELIG_MON_FREQ(i).TX                   <= lane_monitor(i).clock.freq_tx_clk;
        register_map_monitor.register_map_generators.FELIG_MON_PICXO(i).VLOT                <= lane_monitor(i).clock.picxo_volt;
        register_map_monitor.register_map_generators.FELIG_MON_PICXO(i).ERROR               <= lane_monitor(i).clock.picxo_error;
        register_map_monitor.register_map_generators.FELIG_MON_L1A_ID(i)                    <= lane_monitor(i).global.l1a_id;
        register_map_monitor.register_map_generators.FELIG_MON_TTC_0(i).FMT                 <= x"04";
        register_map_monitor.register_map_generators.FELIG_MON_TTC_0(i).LEN                 <= x"18";
        register_map_monitor.register_map_generators.FELIG_MON_TTC_0(i).reserved0           <= (others => '0');
        register_map_monitor.register_map_generators.FELIG_MON_TTC_0(i).BCID                <= lane_monitor(i).global.ttc_mon.BCID;
        register_map_monitor.register_map_generators.FELIG_MON_TTC_0(i).XL1ID               <= lane_monitor(i).global.ttc_mon.XL1ID;
        register_map_monitor.register_map_generators.FELIG_MON_TTC_0(i).L1ID                <= lane_monitor(i).global.ttc_mon.L1ID;
        register_map_monitor.register_map_generators.FELIG_MON_TTC_1(i).orbit               <= lane_monitor(i).global.ttc_mon.OrbitId;
        register_map_monitor.register_map_generators.FELIG_MON_TTC_1(i).trigger_type        <= lane_monitor(i).global.ttc_mon.TriggerType;
        register_map_monitor.register_map_generators.FELIG_MON_TTC_1(i).reserved1           <= (others => '0');
    end generate GEN_LANE_MONITOR_MAP;


    register_map_hk_monitor_out.LMK_LOCKED(0)                                <= LMK_LOCKED;
    register_map_hk_monitor_out.I2C_RD                                       <= register_map_hk_monitor_in.I2C_RD;
    register_map_hk_monitor_out.I2C_WR                                       <= register_map_hk_monitor_in.I2C_WR;
    register_map_hk_monitor_out.FPGA_CORE_TEMP                               <= register_map_hk_monitor_in.FPGA_CORE_TEMP;
    register_map_hk_monitor_out.FPGA_CORE_VCCINT                             <= register_map_hk_monitor_in.FPGA_CORE_VCCINT;
    register_map_hk_monitor_out.FPGA_CORE_VCCAUX                             <= register_map_hk_monitor_in.FPGA_CORE_VCCAUX;
    register_map_hk_monitor_out.FPGA_CORE_VCCBRAM                            <= register_map_hk_monitor_in.FPGA_CORE_VCCBRAM;
    register_map_hk_monitor_out.FPGA_DNA                                     <= register_map_hk_monitor_in.FPGA_DNA;
    register_map_hk_monitor_out.CONFIG_FLASH_RD                              <= register_map_hk_monitor_in.CONFIG_FLASH_RD;
    register_map_hk_monitor_out.RXUSRCLK_FREQ.VAL                            <= register_map_hk_monitor_in.RXUSRCLK_FREQ.VAL;
    register_map_hk_monitor_out.RXUSRCLK_FREQ.VALID                          <= register_map_hk_monitor_in.RXUSRCLK_FREQ.VALID;
    register_map_hk_monitor_out.HK_CTRL_FMC.SI5345_INTR_B                    <= register_map_hk_monitor_in.HK_CTRL_FMC.SI5345_INTR_B;
    register_map_hk_monitor_out.HK_CTRL_FMC.SI5345_LOL                       <= register_map_hk_monitor_in.HK_CTRL_FMC.SI5345_LOL;
    register_map_hk_monitor_out.HK_CTRL_FMC.SI5345_INTR_B                    <= register_map_hk_monitor_in.HK_CTRL_FMC.SI5345_INTR_B;
    register_map_hk_monitor_out.HK_CTRL_FMC.SI5345_LOL_LATCHED               <= register_map_hk_monitor_in.HK_CTRL_FMC.SI5345_LOL_LATCHED;
    register_map_hk_monitor_out.MMCM_MAIN.MAIN_INPUT                         <= register_map_hk_monitor_in.MMCM_MAIN.MAIN_INPUT;
    register_map_hk_monitor_out.MMCM_MAIN.PLL_LOCK                           <= register_map_hk_monitor_in.MMCM_MAIN.PLL_LOCK;
    register_map_hk_monitor_out.MMCM_MAIN.LOL_LATCHED                        <= register_map_hk_monitor_in.MMCM_MAIN.LOL_LATCHED;
    register_map_hk_monitor_out.TACH_CNT                                     <= register_map_hk_monitor_in.TACH_CNT;

end RTL;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:  (modified by) Michael Oberling
--
-- Design Name:  ttc_wrapper
-- Version:    1.0
-- Date:    5/4/2018
--
-- Description:  Modification of ttc_wrapper for FELIG
--
-- Change Log:  V1.0 -
--
--==============================================================================

-- Original File Header --
-- (none)
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use IEEE.NUMERIC_STD.ALL;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.FELIX_package.all;
    use work.pcie_package.all;

--=================================================================================================--
--======================================= Module Body =============================================--
--=================================================================================================--
entity ttc_wrapper_felig is
    port (
        --== ttc fmc interface ==--
        a_data_in          : in   std_logic;
        b_data_in          : in   std_logic;
        ttc_strobe          : in   std_logic;

        --== to Central Router ==---
        TTC_out                     : out std_logic_vector(9 downto 0);

        clk240                       : in std_logic; --clock source to synchronize TTC_out to the rest of the logic

        BUSY                        : out std_logic;

        -- for ILA --
        --rst_TTCtoHost_i              : in std_logic;

        --    ToHostData_full_o           : out std_logic;
        --    ToHostData_empty_o          : out std_logic;
        --    ToHostData_count_o          : out std_logic_vector(9 downto 0);
        --    ToHostData_flush_o          : out std_logic;

        --    L1ID_err_o                  : out std_logic;
        --    L1ID_err_cnt_o              : out std_logic_vector(23 downto 0);

        --    L1ID_cnt_o                  : out std_logic_vector(23 downto 0);
        --    L1ID_prev_o                 : out std_logic_vector(23 downto 0);

        --    TT_cnt_o                    : out std_logic_vector(31 downto 0);
        --    L1a_TT_cnt_o                : out std_logic_vector(31 downto 0);

        --    L1a_excl_time_o             : out std_logic_vector(31 downto 0);
        --    L1a_excl_o                  : out std_logic;
        L1ID_Bch                    : out std_logic_vector(23 downto 0);
        TTC_ToHost_Data_out         : out TTC_data_type
    );

end ttc_wrapper_felig;



architecture top of ttc_wrapper_felig is
    --signal RESET      : std_logic;
    --========================= Signal Declarations ==========================--
    --signal cdrbad      : std_logic;
    --signal pll_clk     : std_logic;
    --signal pll_locked    : std_logic;
    --====================--
    -- ttc wrapper control
    --====================--
    signal rst_TTCtoHost          : std_logic;
    signal master_BUSY            : std_logic;  -- to throtlle the L1A
    signal L1A_throttle           : std_logic;

    --==============--
    -- ttc decoder
    --==============--
    signal l1a                    : std_logic;
    signal l1a_dec                : std_logic;
    signal channelB               : std_logic;
    signal brc_b                  : std_logic;
    signal brc_e                  : std_logic;
    signal brc_t2                 : std_logic_vector(1 downto 0);
    signal brc_d4                 : std_logic_vector(3 downto 0);
    signal single_bit_error      : std_logic;
    signal double_bit_error       : std_logic;
    signal communication_error    : std_logic;
    signal brc_strobe        : std_logic;
    signal add_strobe             : std_logic;
    signal add_a14             : std_logic_vector(13 downto 0);
    signal add_e                  : std_logic;
    signal add_s8                 : std_logic_vector(7 downto 0);
    signal add_d8                 : std_logic_vector(7 downto 0);

    signal busy_unsync            : std_logic;
    signal busy_sync              : std_logic;
    signal TTC_Out_unsync         : std_logic_vector(9 downto 0);
    signal TTC_Out_sync           : std_logic_vector(9 downto 0);

    --======================--
    --- TTC-To-Host Design ---
    --======================--
    -- "TT" stands for trigger type

    -- To-Host control registers --
    signal XL1ID_RST_FROM_PCIE    : std_logic;  -- Reset XL1ID
    signal TT_Bch_En              : std_logic;  -- Enabling this will make use of trigger type information from TTC B channel

    -- To-Host data format and initial values --
    signal FMT          : std_logic_vector(7 downto 0) := x"01";  --byte0, represents version
    signal LEN          : std_logic_vector(7 downto 0) := x"14";  --byte1, length of packet, always 20
    signal reserved0    : std_logic_vector(3 downto 0) := x"0";  --byte2, set to all 0
    signal BCID         : std_logic_vector(11 downto 0):= x"000"; --byte2,3
    signal XL1ID        : std_logic_vector(7 downto 0) := x"00";  --byte4
    signal L1ID         : std_logic_vector(23 downto 0):= x"000000"; --byte 5,6,7
    signal orbit        : std_logic_vector(31 downto 0):= x"00000000"; --byte 8,9,10,11
    signal trigger_type : std_logic_vector(15 downto 0):= x"0000"; --byte 12,13
    signal reserved1    : std_logic_vector(15 downto 0):= x"0000"; --byte 14,15
    signal L0ID         : std_logic_vector(31 downto 0):= x"00000000"; --byte 16,17,18,19

    signal ToHostData     : std_logic_vector(143 downto 0);   -- 144 bit = 20 bytes of ToHostData
    signal ToHostData_in  : std_logic_vector(144 downto 0);   -- 1 bit for indicating an ignored L1 trigger + ToHostData
    signal ToHostData_out : std_logic_vector(144 downto 0);   -- same as just above

    --- To-Host Fifo control ---
    signal ToHostData_count       : std_logic_vector(9 downto 0);
    signal ToHostData_full        : std_logic;
    signal ToHostData_empty       : std_logic;
    signal wr_en_ToHostData       : std_logic;
    signal rd_en_ToHostData       : std_logic;
    signal rd_en_ToHostData_align : std_logic;
    signal flush_ToHostData       : std_logic;
    signal rst_ToHostData         : std_logic;

    signal L1a_latch     : std_logic;
    signal TT_cnt        : integer;    -- "TT" stands for trigger type, this counts the number of trigger type messages asserted by TTCvi
    signal L1a_TT_cnt    : integer;    -- It counts the number of L1 associated with B-channel message. This should NOT be equal to all number of L1.

    -- To-Host Monitoring --
    signal L1ID_prev              : std_logic_vector(23 downto 0);
    signal L1ID_err_cnt           : std_logic_vector(23 downto 0);
    signal L1ID_cnt               : std_logic_vector(23 downto 0);
    signal L1ID_err               : std_logic;
    signal L1ID_from_Bch          : std_logic_vector(23 downto 0);

    signal brc_b_cnt      : std_logic_vector(1 downto 0) := "00";     -- count the number of BC
    signal brc_b_interval : std_logic_vector(7 downto 0) := X"00";   -- interval between two BCR

    component TTCtoHostData IS
        PORT (
            clk : IN STD_LOGIC;
            rst : IN STD_LOGIC;
            din : IN STD_LOGIC_VECTOR(144 DOWNTO 0);
            wr_en : IN STD_LOGIC;
            rd_en : IN STD_LOGIC;
            dout : OUT STD_LOGIC_VECTOR(144 DOWNTO 0);
            full : OUT STD_LOGIC;
            empty : OUT STD_LOGIC;
            data_count : OUT STD_LOGIC_VECTOR(9 DOWNTO 0)
        );
    END component;

begin
    L1ID_Bch <= L1ID_from_Bch;
    --=====================--
    -- ttc wrapper control --
    --=====================--
    --TT_Bch_En <= to_sl(register_map_control.xxx);
    --XL1ID_RST_FROM_PCIE <= to_sl(register_map_control.xxx);
    --TT_Bch_En <= '1';
    L1aThrottle: process(clk240)
    begin
        if (clk240'event and clk240='1') then
            if (ttc_strobe = '1') then
                if (master_BUSY='1') then
                    L1A_throttle <= '1';
                else
                    L1A_throttle <= '0';
                end if;
            end if;
        end if;
    end process;

    --  rst_TTCtoHost <= rst_TTCtoHost_i;
    rst_TTCtoHost <= '0';

    --==============--
    -- To-Host Data --
    --==============--
    ToHostData <=
                  L0ID & reserved1 & orbit & L1ID & XL1ID & BCID & reserved0 & LEN & FMT;
    --   32       16        32      24      8      12       4         8     8   =  144 bits = 20 bytes

    BCID_gen: process (clk240,rst_TTCtoHost)
    begin
        if (rst_TTCtoHost='1') then
            BCID <= (others=>'0');
        elsif (clk240'event and clk240='1') then
            if (ttc_strobe = '1') then
                if (brc_b = '1') then
                    BCID <= (others=>'0');
                else
                    BCID <= BCID + 1;
                end if;
            end if;
        end if;
    end process;

    Orbit_gen: process(clk240,rst_TTCtoHost)
    begin
        if (rst_TTCtoHost='1') then
            brc_b_cnt <= (others=>'0');
            brc_b_interval <= (others=>'0');
        elsif (clk240'event and clk240='1') then
            if (ttc_strobe = '1') then

                if (brc_b_cnt = "00") then
                    if (brc_b = '1') then
                        brc_b_cnt <= "01";
                        orbit <= orbit + 1;
                    end if;
                    brc_b_interval <= (others=>'0');
                elsif (brc_b_cnt = "01") then
                    if (brc_b = '1') then
                        brc_b_cnt <= "10";
                        orbit <= orbit + 1;
                    end if;
                    if (brc_b_interval /= X"FF") then
                        brc_b_interval <= brc_b_interval + 1;
                    end if;
                else
                    if ( brc_b_interval < 100 ) then  -- 25ns x 100 = 2.5 us
                        orbit <= (others=>'0');
                        brc_b_cnt <= "00";
                    else
                        brc_b_cnt <= "01";
                        brc_b_interval <= (others=>'0');
                    end if;
                end if;
            end if;
        end if;
    end process;

    L1ID_gen: process (rst_TTCtoHost, clk240, L1a, L1a_latch, XL1ID_RST_FROM_PCIE, TT_Bch_En)
        variable Bch_timeout_cnt      : integer := 0;
        variable L1a_excl_time        : integer := 0;
        variable L1a_excl_status      : std_logic := '0';
        variable TT_timeout_status    : std_logic := '0';
    begin
        if (rst_TTCtoHost='1') then
            L1ID <= (others=>'0');
            XL1ID <= x"00";
            wr_en_ToHostData <= '0';
            rst_ToHostData <= '1';
            TT_Bch_En <= '1';
            L1a_TT_cnt <= 0;
            TT_cnt <= 0;
            flush_ToHostData <= '0';
        elsif (clk240'event and clk240='1') then
            if (ttc_strobe = '1') then
                L1a_latch   <= L1a;
                rst_ToHostData    <= '0';

                if (brc_e = '1') then
                    L1ID <= (others=>'0');
                    XL1ID <= XL1ID + 1;
                elsif (L1a = '1') then
                    L1ID    <= L1ID + 1;
                end if;

                if (L1a = '1' and L1a_excl_status = '0') then
                    wr_en_ToHostData <= '1';
                    ToHostData_in <= '1' & ToHostData;
                    L1a_TT_cnt <= L1a_TT_cnt + 1;
                elsif (L1a = '1' and L1a_excl_status = '1') then
                    wr_en_ToHostData <= '1';
                    ToHostData_in  <= '0' & ToHostData;
                else
                    wr_en_ToHostData <= '0';
                end if;

       L1aExclTimeMon: case L1a_excl_status is
                    when '0' =>
                        if (L1a='1') then
                            L1a_excl_time := L1a_excl_time + 1;
                            L1a_excl_status := '1';
                        end if;
                    when '1' =>
                        L1a_excl_time := L1a_excl_time + 1;
                        if (L1a_excl_time = 6) then
                            L1a_excl_status := '0';
                            L1a_excl_time := 0;
                        end if;
                    when others =>
                end case;
                --L1a_excl_o <= L1a_excl_status;
                --L1a_excl_time_o <= std_logic_vector(to_unsigned(L1a_excl_time,32));

                rd_en_ToHostData_align <= rd_en_ToHostData;

                if (TT_Bch_En='0') then
                    if (L1a_latch='1') then
                        rd_en_ToHostData <= '1';
                    else
                        rd_en_ToHostData <= '0';
                    end if;
                end if;

                if (TT_Bch_En='1') then
          ToHostData_flusing : case flush_ToHostData is
                        when '0' =>
                            if ( add_strobe = '1' and add_e = '1' and add_s8 = x"00") then
                                trigger_type(7 downto 0) <= add_d8;
                                TT_cnt <= TT_cnt + 1;
                                flush_ToHostData <= '1';
                                rd_en_ToHostData <= '1';
                            end if;
                        when '1' =>
                            if (ToHostData_out(144) = '1') then
                                flush_ToHostData <= '0';
                                rd_en_ToHostData <= '0';
                            else
                                rd_en_ToHostData <= '1';
                            end if;
                        when others => flush_ToHostData <= flush_ToHostData;
                    end case;

                    --Event ID, not used but just in case--
                    if ( add_strobe = '1' and add_e = '1' and add_s8 = x"01") then  -- event/orbit counter
                        L1ID_from_Bch(23 downto 16) <= add_d8;
                    end if;
                    if ( add_strobe = '1' and add_e = '1' and add_s8 = x"02") then  -- event/orbit counter
                        L1ID_from_Bch(15 downto 8)  <= add_d8;
                    end if;
                    if ( add_strobe = '1' and add_e = '1' and add_s8 = x"03") then  -- event/orbit counter
                        L1ID_from_Bch(7 downto 0)   <= add_d8;
                    end if;

                end if;

                -- PCIE registers --
                if (XL1ID_RST_FROM_PCIE='1') then
                    XL1ID <= (others=>'0');
                end if;

                -- Monitering Trigger Type Time out --
       TT_TimeoutMon: case TT_timeout_status is
                    when '0' =>
                        if (L1a = '1') then
                            TT_timeout_status := '1';
                        end if;
                    when '1' =>
                        Bch_timeout_cnt := Bch_timeout_cnt + 1;
                        if (add_strobe = '1' and add_e = '1' and add_s8 = x"00") then
                            Bch_timeout_cnt := 0;
                            TT_timeout_status := '0';
                        end if;
                        if Bch_timeout_cnt = 1000 then
                            TT_Bch_En <= '0';
                            Bch_timeout_cnt := 0;
                            TT_timeout_status := '0';
                        end if;
                    when others =>
                end case;
            else
                rd_en_ToHostData <= '0';
                wr_en_ToHostData <= '0';
            end if;
        end if;
    end process;

    L0ID <= XL1ID & L1ID;

    --L1a_TT_cnt_o <= std_logic_vector(to_unsigned(L1a_TT_cnt,32));
    --TT_cnt_o     <= std_logic_vector(to_unsigned(TT_cnt,32));

    Fifo_ToHostData: TTCtoHostData
        port map (
            rst    => rst_ToHostData,
            clk    => clk240,
            din    => ToHostData_in,
            wr_en  => wr_en_ToHostData,
            rd_en  => rd_en_ToHostData,
            dout   => ToHostData_out,
            full   => ToHostData_full,
            empty  => ToHostData_empty,
            data_count => ToHostData_count
        );
    --ToHostData_count_o <= ToHostData_count;
    --ToHostData_empty_o <= ToHostData_empty;
    --ToHostData_full_o  <= ToHostData_full;
    --ToHostData_flush_o <= rd_en_ToHostData;

    TTC_ToHost_Data_out.BCID               <= ToHostData_out(31 downto 20);
    TTC_ToHost_Data_out.XL1ID              <= ToHostData_out(39 downto 32);
    TTC_ToHost_Data_out.L1ID               <= ToHostData_out(63 downto 40);
    TTC_ToHost_Data_out.OrbitId            <= ToHostData_out(95 downto 64);
    TTC_ToHost_Data_out.TriggerType        <= trigger_type;
    TTC_ToHost_Data_out.L0ID(31 downto 0)  <= ToHostData_out(143 downto 112);
    TTC_ToHost_Data_out.L0A                <= rd_en_ToHostData_align;

    L1ID_check: process (clk240,rst_TTCtoHost,rd_en_ToHostData_align,L1ID_prev)
    begin
        if(rst_TTCtoHost='1') then
            L1ID_cnt     <= x"000000";
            L1ID_prev    <= x"000000";
            L1ID_err_cnt <= x"000000";
            L1ID_err     <= '0';
        elsif (clk240'event and clk240='1') then
            if (ttc_strobe = '1') then
                L1ID_prev <= ToHostData_out(63 downto 40);
                L1ID_err  <= '0';
                if(rd_en_ToHostData_align = '1' and L1ID_prev+1 /= ToHostData_out(63 downto 40)) then
                    L1ID_err_cnt <= L1ID_err_cnt + 1;
                    L1ID_err <= '1';
                end if;
                if(rd_en_ToHostData_align = '1' and L1ID_prev+1 = ToHostData_out(63 downto 40)) then
                    L1ID_cnt <= L1ID_cnt + 1;
                end if;
            end if;
        end if;
    end process;

    --L1ID_err_cnt_o <= L1ID_err_cnt;
    --L1ID_cnt_o     <= L1ID_cnt;
    --L1ID_prev_o    <= L1ID_prev;
    --L1ID_err_o     <= L1ID_err;


    --=====================================--
    -- ttc decoder --
    --=====================================--
    l1a <= l1a_dec and not L1A_throttle;

    TTC_out_unsync(0)        <= l1a;
    TTC_out_unsync(1)        <= channelB;
    TTC_out_unsync(2)        <= brc_b;
    TTC_out_unsync(3)        <= brc_e;
    TTC_out_unsync(4)        <= brc_d4(0);
    TTC_out_unsync(5)        <= brc_d4(1);
    TTC_out_unsync(6)        <= brc_d4(2);
    TTC_out_unsync(7)        <= brc_d4(3);
    TTC_out_unsync(8)        <= brc_t2(0);
    TTC_out_unsync(9)        <= brc_t2(1);

    busy_unsync <= single_bit_error or double_bit_error or communication_error;

    sync: process(clk240)
    begin
        if rising_edge(clk240) then
            if (ttc_strobe = '1') then
                TTC_out_sync <= TTC_out_unsync;
                busy_sync <= busy_unsync;
            end if;
        end if;
    end process;

    busy    <= busy_sync;
    TTC_out <= TTC_out_sync;


    --=====================================--
    ttc_dec: entity work.ttc_decoder_core_felig
        --=====================================--
        port map
(
            --== cdr interface ==--
            cdrclk_in            => clk240,
            a_data_in            => a_data_in,
            b_data_in            => b_data_in,
            ttc_strobe            => ttc_strobe,
            --== ttc decoder output ==--
            single_bit_error          => single_bit_error,
            double_bit_error          => double_bit_error,
            communication_error       => communication_error,
            l1a                => l1a_dec,
            brc_strobe            => brc_strobe,
            add_strobe            => add_strobe,
            --TTDDDDDEB
            brc_t2              => brc_t2,
            brc_d4              => brc_d4,
            brc_e                => brc_e,
            brc_b              => brc_b,
            --AAAAAAAAAAAAAAE1SSSSSSSSDDDDDDDD
            add_a14              => add_a14,
            add_e              => add_e,
            add_s8              => add_s8,
            add_d8              => add_d8,
            --== ttc decoder aux flags ==--
            channelB_o                      => channelB
        );


end top;



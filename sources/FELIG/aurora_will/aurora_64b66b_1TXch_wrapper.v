 ///////////////////////////////////////////////////////////////////////////////
 //
 // Project:  Aurora 64B/66B
 // Company:  Xilinx
 //
 //
 //
 // (c) Copyright 2008 - 2014 Xilinx, Inc. All rights reserved.
 //
 // This file contains confidential and proprietary information
 // of Xilinx, Inc. and is protected under U.S. and
 // international copyright and other intellectual property
 // laws.
 //
 // DISCLAIMER
 // This disclaimer is not a license and does not grant any
 // rights to the materials distributed herewith. Except as
 // otherwise provided in a valid license issued to you by
 // Xilinx, and to the maximum extent permitted by applicable
 // law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
 // WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
 // AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
 // BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
 // INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
 // (2) Xilinx shall not be liable (whether in contract or tort,
 // including negligence, or under any other theory of
 // liability) for any loss or damage of any kind or nature
 // related to, arising under or in connection with these
 // materials, including for any direct, or any indirect,
 // special, incidental, or consequential loss or damage
 // (including loss of data, profits, goodwill, or any type of
 // loss or damage suffered as a result of any action brought
 // by a third party) even if such damage or loss was
 // reasonably foreseeable or Xilinx had been advised of the
 // possibility of the same.
 //
 // CRITICAL APPLICATIONS
 // Xilinx products are not designed or intended to be fail-
 // safe, or for use in any application requiring fail-safe
 // performance, such as life-support or safety devices or
 // systems, Class III medical devices, nuclear facilities,
 // applications related to the deployment of airbags, or any
 // other applications that could lead to death, personal
 // injury, or severe property or environmental damage
 // (individually and collectively, "Critical
 // Applications"). Customer assumes the sole risk and
 // liability of any use of Xilinx products in Critical
 // Applications, subject only to applicable laws and
 // regulations governing limitations on product liability.
 //
 // THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
 // PART OF THIS FILE AT ALL TIMES.

 //
 //
 ////////////////////////////////////////////////////////////////////////////////
 // Design Name: aurora_64b66b_1TXch_WRAPPER
 //
 // Module aurora_64b66b_1TXch_WRAPPER

 // This is V8/K8 wrapper

 `timescale 1ns / 1ps
   (* core_generation_info = "aurora_64b66b_1TXch,aurora_64b66b_v11_2_4,{c_aurora_lanes=1,c_column_used=left,c_gt_clock_1=GTHQ0,c_gt_clock_2=None,c_gt_loc_1=1,c_gt_loc_10=X,c_gt_loc_11=X,c_gt_loc_12=X,c_gt_loc_13=X,c_gt_loc_14=X,c_gt_loc_15=X,c_gt_loc_16=X,c_gt_loc_17=X,c_gt_loc_18=X,c_gt_loc_19=X,c_gt_loc_2=X,c_gt_loc_20=X,c_gt_loc_21=X,c_gt_loc_22=X,c_gt_loc_23=X,c_gt_loc_24=X,c_gt_loc_25=X,c_gt_loc_26=X,c_gt_loc_27=X,c_gt_loc_28=X,c_gt_loc_29=X,c_gt_loc_3=X,c_gt_loc_30=X,c_gt_loc_31=X,c_gt_loc_32=X,c_gt_loc_33=X,c_gt_loc_34=X,c_gt_loc_35=X,c_gt_loc_36=X,c_gt_loc_37=X,c_gt_loc_38=X,c_gt_loc_39=X,c_gt_loc_4=X,c_gt_loc_40=X,c_gt_loc_41=X,c_gt_loc_42=X,c_gt_loc_43=X,c_gt_loc_44=X,c_gt_loc_45=X,c_gt_loc_46=X,c_gt_loc_47=X,c_gt_loc_48=X,c_gt_loc_5=X,c_gt_loc_6=X,c_gt_loc_7=X,c_gt_loc_8=X,c_gt_loc_9=X,c_lane_width=4,c_line_rate=10.24,c_gt_type=GTHE3,c_qpll=true,c_nfc=false,c_nfc_mode=IMM,c_refclk_frequency=160.0,c_simplex=true,c_simplex_mode=TX,c_stream=false,c_ufc=false,c_user_k=false,flow_mode=None,interface_mode=Framing,dataflow_config=TX-only_Simplex}" *)
(* DowngradeIPIdentifiedWarnings="yes" *)
 module aurora_64b66b_1TXch_WRAPPER #
 (
      parameter INTER_CB_GAP  = 5'd9,
      parameter SEQ_COUNT  = 4,
    parameter wait_for_fifo_wr_rst_busy_value = 6'd32,
      parameter TRAVELLING_STAGES = 3'd2,
      parameter BACKWARD_COMP_MODE1 = 1'b0, //disable check for interCB gap
      parameter BACKWARD_COMP_MODE2 = 1'b0, //reduce RXCDR lock time, Block Sync SH max count, disable CDR FSM in wrapper
      parameter BACKWARD_COMP_MODE3 = 1'b0, //clear hot-plug counter with any valid btf detected
     // Channel bond MASTER/SLAVE connection
 // Simulation attributes
     parameter   EXAMPLE_SIMULATION   =   0            // Set to 1 to speed up sim reset
 )
 `define DLY #1
 (

 
    //----------------- Receive Ports - Channel Bonding Ports -----------------

 //------------------- Shared Ports - Tile and PLL Ports --------------------

       
       RESET,
       

       //WB FELIG PLLLKDET_OUT,

       //WB remove u_pd_syncPOWERDOWN_IN,

     //-------------- Transmit Ports - 64b66b TX Header Ports --------------
       TXHEADER_IN,
       //---------------- Transmit Ports - TX Data Path interface -----------------
       TXDATA_IN,
       DataOut64brdy, //WB gearbox
       //WBTXRESET_IN,

       //WB FELIG TXUSRCLK_IN,//
       TXUSRCLK2_IN,//
       //txusrclk_out,
       //txusrclk2_out,

        //WB FELIG TXBUFERR_OUT,
       //--------------Data Valid Signals for Local Link
       TXDATAVALID_OUT,
       
       TXDATAVALID_SYMGEN_OUT,

     //------------- Transmit Ports - TX Driver and OOB signalling --------------
    //---------------------- Loopback Port ----------------------
//---{
       //WB FELIG gt_qplllock_quad1_in,
       //WB FELIG gt_qpllrefclklost_quad1,





       //gt_qplllock_in,
       //gt_qpllrefclklost_in,
//---}

       //---------------------- GTXE2 COMMON DRP Ports ----------------------



       // From GT in
       //WB FELIG gttx_fsm_resetdone_in,
       //WBgtrx_fsm_resetdone_in,
       //WBfabric_pcs_reset_in,
//       userclk_rx_active_in,
//       //WBrxusrclk_in,
//       pre_rxdata_from_gtx_in,
//       pre_rxheader_from_gtx_in,
//       pre_rxdatavalid_in,
//       pre_rxheadervalid_in,


       // To GT out
        //WB FELIG txsequence_out,
        tx_hdr_out,
       scrambled_data_out,



       //TXCLK_LOCK,
       //WB FELIG INIT_CLK,
       USER_CLK
       //WB remove u_rst_done_syncFSM_RESETDONE,
       //WB remove u_link_rst_syncLINK_RESET_OUTf
 );
 //***************************** Port Declarations *****************************


     //---------------------- Loopback and Powerdown Ports ----------------------
     //----------------- Receive Ports - Channel Bonding Ports -----------------
     //------------------- Shared Ports - Tile and PLL Ports --------------------
        //WB FELIG input               GTXRESET_IN;
       //WB FELIG output            PLLLKDET_OUT;
       //WB remove u_pd_syncinput               POWERDOWN_IN;
       input               RESET;
     //-------------- Transmit Ports - TX Header Control Port ----------------
       input    [1:0]    TXHEADER_IN;
     //---------------- Transmit Ports - TX Data Path interface -----------------
       input    [63:0]   TXDATA_IN;
       input DataOut64brdy; //WB gearbox
      
       //WBinput             TXRESET_IN;
       //WB FELIG  output            TXBUFERR_OUT;
    //WB FELIG  input               TXUSRCLK_IN;//
     input               TXUSRCLK2_IN;//
     //output              txusrclk_out;//
     //output              txusrclk2_out;//
     //------------- Transmit Ports - TX Driver and OOB signalling --------------
     output              TXDATAVALID_OUT;
     
     output              TXDATAVALID_SYMGEN_OUT;
    //---------------------- GTXE2 CHANNEL DRP Ports ----------------------
//---{
    //WB FELIG input                     gt_qplllock_quad1_in   ;
    //WB FELIG input                     gt_qpllrefclklost_quad1;


    //input                     gt_qplllock_in;
    //input                     gt_qpllrefclklost_in;

//---}


       // From GT in
       //WB FELIG input                  gttx_fsm_resetdone_in;
       //WBinput                  gtrx_fsm_resetdone_in;
       //WBinput                  fabric_pcs_reset_in;
//       input                  userclk_rx_active_in;
//       //WBinput                  rxusrclk_in;
//       input [31:0] pre_rxdata_from_gtx_in;
//       input  [1:0]                          pre_rxheader_from_gtx_in;
//       input                                 pre_rxdatavalid_in;
//       input                                 pre_rxheadervalid_in;


    // To GT out
        //WB FELIG output [6:0]                                    txsequence_out;
       output  [1:0]                          tx_hdr_out;
       output  [63:0]                         scrambled_data_out;


     //input             TXCLK_LOCK;
     //WB FELIG input             INIT_CLK;
	 input             USER_CLK;
     

 //***************************** FIFO Watermark settings ************************
     localparam LOW_WATER_MARK_SLAVE  = 13'd450;
     localparam LOW_WATER_MARK_MASTER = 13'd450;

     localparam HIGH_WATER_MARK_SLAVE  = 13'd8;
     localparam HIGH_WATER_MARK_MASTER = 13'd14;

     localparam SH_CNT_MAX   = EXAMPLE_SIMULATION ? 16'd64 : (BACKWARD_COMP_MODE2) ? 16'd64 : 16'd60000;

     localparam SH_INVALID_CNT_MAX = 16'd16;
 //***************************** Wire Declarations *****************************
    

     reg   [6:0]             txseq_counter_i;
    
     reg   [6:0]             datavalid_counter_i;
     
       wire  [63:0]            scrambled_data_i;

     wire                    stableclk_gtx_reset_comb;
     wire                    gtx_reset_comb;
       
       reg   [1:0]             tx_hdr_r;
       wire     TXDATAVALID_OUT_o;

     
//------------------------------------------------------------------------------
assign stableclk_gtx_reset_comb = RESET;//0;
aurora_64b66b_1TXch_rst_sync u_rst_sync_gtx_reset_comb
    (
      .prmry_in         (stableclk_gtx_reset_comb),
      .scndry_aclk      (TXUSRCLK2_IN),
      .scndry_out       (gtx_reset_comb)
    );

//------------------------------------------------------------------------------
     always @ (posedge TXUSRCLK2_IN)//always @ (posedge TXUSRCLK2_IN)
     begin
           tx_hdr_r   <= `DLY TXHEADER_IN;
     end

     always @(posedge TXUSRCLK2_IN)
     begin
        if(gtx_reset_comb)
          txseq_counter_i <=  `DLY  7'd0;
         else if(txseq_counter_i == 32)
          txseq_counter_i <=  `DLY  7'd0;
         else
           txseq_counter_i <=  `DLY txseq_counter_i + 7'd1;
     end

     //Assign the Data Valid signal
     assign TXDATAVALID_OUT           = (txseq_counter_i != 28);
     //Assign TXDATAVALID to sym gen by accounting for the latency
     assign TXDATAVALID_SYMGEN_OUT    = (txseq_counter_i != 30); 

     //_______________________________ Data Valid Signal ____ ________________________
     assign data_valid_i = (txseq_counter_i != 31);
     assign txsequence_i = txseq_counter_i; 

  





    //#########################scrambler instantiation########################
aurora_64b66b_1TXch_SCRAMBLER_64B66B #
     (
        .TX_DATA_WIDTH(64)
     )
       scrambler_64b66b_gtx0_i
     (
       // User Interface
 
        .UNSCRAMBLED_DATA_IN    (TXDATA_IN),
        .SCRAMBLED_DATA_OUT     (scrambled_data_i),
        .DATA_VALID_IN          (DataOut64brdy),//WBdata_valid_i),
        .DATA_VALID_OUT         (TXDATAVALID_OUT_o),
        // System Interface
        .USER_CLK               (TXUSRCLK2_IN), // (TXUSRCLK2_IN),
        .SYSTEM_RESET           (gtx_reset_comb)
     );



//aurora_64b66b_0_DESCRAMBLER_64B66B AA
// (
//     // User Interface
//     .SCRAMBLED_DATA_IN (scrambled_data_i),
//     .UNSCRAMBLED_DATA_OUT (),
//     .DATA_VALID_IN (TXDATAVALID_OUT_o),
//     // System Interface
//     .CLK40 (TXUSRCLK2_IN),
//     .SYSTEM_RESET (gtx_reset_comb)
// );
 



// From GT subcore assignment

    
//    assign gtx_rx_pcsreset_comb   = userclk_rx_active_in;
    
//    assign pre_rxdata_from_gtx_i      = pre_rxdata_from_gtx_in;  
//    assign pre_rxheader_from_gtx_i    = pre_rxheader_from_gtx_in;
//    assign pre_rxdatavalid_i          = pre_rxdatavalid_in;      
//    assign pre_rxheadervalid_i        = pre_rxheadervalid_in;    


         assign  tx_hdr_out              = tx_hdr_r;
         assign  scrambled_data_out              = scrambled_data_i;
 
 
 endmodule

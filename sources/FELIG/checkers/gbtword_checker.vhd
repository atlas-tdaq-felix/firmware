--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!               Shelfali Saxena
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 01/05/2019 10:33:43 AM
-- Design Name:
-- Module Name: gbtword_checker - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity gbtword_checker is
    port (
        lane_txclk_240    : in std_logic;
        gbt_tx_flag             : in std_logic;
        tx_120b_from_mach       : in std_logic_vector(119 downto 0);

        ila_phlck_gwchk         : out std_logic;
        ila_en_gwchk            : out std_logic;
        ila_count_gwchk         : out std_logic_vector(2 downto 0);
        ila_count_start_gwchk   : out std_logic_vector(1 downto 0);
        ila_testpass_gwchk      : out std_logic_vector(3 downto 0);
        ila_dist_gwchk          : out std_logic_vector(3 downto 0);
        ila_word10b_gwchk       : out std_logic_vector(9 downto 0);
        ila_count_pyld          : out std_logic_vector(8 downto 0)

    );
end gbtword_checker;


architecture Behavioral of gbtword_checker is

    --MT  checker gbt word
    signal count_gwchk : std_logic_vector(2 downto 0) := "000";
    signal count_phlck_gwchk : std_logic_vector(2 downto 0) := "000";  --up to 5 is
    --what I nee
    signal phlck_gwchk : std_logic := '0';  --decide the 2b phase in the 10b word
    signal en_gwchk : std_logic := '0';
    signal word10b_phlck_gwchk : std_logic_vector(9 downto 0) := (others => '0');
    signal word10b_gwchk : std_logic_vector(9 downto 0) := (others => '0');
    signal count_start_gwchk : std_logic_vector(1 downto 0) := "00";
    signal dist_gwchk : std_logic_vector(3 downto 0) := "0000";
    signal count_pyld : std_logic_vector(8 downto 0) := (others => '0');
    type STCHK  is (st_idl, st_start, st_count, st_countpyld) ;
    signal state_gwchk : STCHK := st_idl;

    type STPHLCKCHK  is (st_chk, st_lck) ;
    signal state_phlck_gwchk : STPHLCKCHK := st_chk;

    signal count_wait_gwchk : std_logic_vector(1 downto 0) := "00";

    --MSB indicates whether is idle (0) or the test is ongoing
    --2nd indicates whether the test is passed or not when it's not idle
    --3rd and 4th gives more details about the test
    --"0000": idle;
    --"1001": failed, starting sequence (ie: xaa XX YY with XX!=x00 or
    --Y!=x3c)
    --"1010": failed, after starting sequence I get KK, LL, MM, NN where
    --MM!=xaa or NN!=xbb
    --"1111": test passed
    signal testpass_gwchk : std_logic_vector(3 downto 0) := "0000";


    signal tx_120b_from_mach_pipe1, tx_120b_from_mach_pipe2 : std_logic_vector(119 downto  0)  := (others => '0');

--



begin


    --as in the elink_packet_generator
    --8b10b enc
    --xaa -> x15a

    --accumulate 10b word here
    --HAD TO COMME IEEE.STD_LOGIC_ARITH.ALL and uncomment
    --ieee.numeric_std.all to be able to use to_integer -> understand
    --what's the equivalent of to unteger in logic arithm
    checker_phaselock: process (lane_txclk_240)
    begin
        if lane_txclk_240'event and lane_txclk_240='1' then
            if gbt_tx_flag = '1' then
                --word10b_phlck_gwchk((2*conv_integer(unsigned(count_phlck_gwchk))+1) downto (2*conv_integer(unsigned(count_phlck_gwchk)))) <=  tx_120b_from_mach(49 downto
                --48); --requires more memory than the implementation below
                --if count_phlck_gwchk /= "100" then
                --  count_phlck_gwchk <= count_phlck_gwchk + '1';
                --else
                --  count_phlck_gwchk <= (others => '0');
                --end if;

                word10b_phlck_gwchk(1 downto 0) <= word10b_phlck_gwchk(3 downto 2);
                word10b_phlck_gwchk(3 downto 2) <= word10b_phlck_gwchk(5 downto 4);
                word10b_phlck_gwchk(5 downto 4) <= word10b_phlck_gwchk(7 downto 6);
                word10b_phlck_gwchk(7 downto 6) <= word10b_phlck_gwchk(9 downto 8);
                word10b_phlck_gwchk(9 downto 8) <= tx_120b_from_mach(49 downto 48);

              checker_phaselock_sm: case state_phlck_gwchk is
                    when st_chk =>
                        if word10b_phlck_gwchk = X"15A" then
                            phlck_gwchk <= '0';
                            state_phlck_gwchk <= st_lck;
                        else
                            phlck_gwchk <= '0';
                            state_phlck_gwchk <= st_chk;
                        end if;
                    when st_lck =>
                        state_phlck_gwchk <= st_lck;
                        phlck_gwchk <= '1';
                end case checker_phaselock_sm;
            end if;
        end if;
    end process;

    --delay tx_120b_from_mach to synchronize with phlck
    checker_delayinputs: process (lane_txclk_240)
    begin
        if lane_txclk_240'event and lane_txclk_240='1' then
            if gbt_tx_flag = '1' then
                tx_120b_from_mach_pipe2     <= tx_120b_from_mach_pipe1;
                tx_120b_from_mach_pipe1     <= tx_120b_from_mach;
            end if;
        end if;
    end process;



    checker_gbtwordforming: process (lane_txclk_240)
    begin
        if lane_txclk_240'event and lane_txclk_240='1' then
            if phlck_gwchk = '1' then  --will loose the first goog block
                --since I didn't delat word10b but ok
                if gbt_tx_flag = '1' then

                    --word10b_gwchk((2*conv_integer(unsigned(count_gwchk))+1) downto (2*conv_integer(unsigned(count_gwchk)))) <=  tx_120b_from_mach(49 downto 48);
                    word10b_gwchk(1 downto 0) <= word10b_gwchk(3 downto 2);
                    word10b_gwchk(3 downto 2) <= word10b_gwchk(5 downto 4);
                    word10b_gwchk(5 downto 4) <= word10b_gwchk(7 downto 6);
                    word10b_gwchk(7 downto 6) <= word10b_gwchk(9 downto 8);
                    word10b_gwchk(9 downto 8) <= tx_120b_from_mach_pipe2(49 downto 48);



                    if count_gwchk /= "100" then
                        count_gwchk <= count_gwchk + 1;
                        en_gwchk <= '0';
                    else
                        count_gwchk <= "000";
                        en_gwchk <= '1';
                    end if;
                elsif en_gwchk = '1' then
                    en_gwchk <= '0';
                end if;
            end if;
        end if;
    end process;

    checker_gbtword: process (lane_txclk_240)
    begin
        if lane_txclk_240'event and lane_txclk_240='1' then
            if en_gwchk = '1' then
              checker_gbtword_sm: case state_gwchk is
                    when st_idl =>
                        count_start_gwchk <= (others => '0');
                        dist_gwchk <= (others => '0');
                        testpass_gwchk <= "0000";
                        count_pyld <= (others => '0');
                        --xaa encoded = x15a
                        if word10b_gwchk = X"15A" then
                            state_gwchk <= st_start;
                            count_start_gwchk <= count_start_gwchk + 1;
                        end if;
                    when st_start =>
                        --0x00 encoded is x274 or x18B
                        if (word10b_gwchk = X"274" or word10b_gwchk = X"18B") and count_start_gwchk = "01" then
                            state_gwchk <= st_start;
                            count_start_gwchk <= count_start_gwchk + 1;
                        --03C chunk lenght encodeded is x0E9
                        elsif word10b_gwchk = X"0E9" and count_start_gwchk = "10" then
                            state_gwchk <= st_count;
                            count_start_gwchk <=  count_start_gwchk + 1;
                        else
                            --starting sequence is different than xaa, x00,
                            --xchunklenght(7 downto 0)
                            state_gwchk <= st_idl;
                            count_start_gwchk <= "00";
                            testpass_gwchk <= "1001";
                        end if;
                    when st_count =>
                        count_start_gwchk <= "00";  --remove this since when it goes
                        --to idle will be zeroed
                        --xbb encoded = X36a or x09a
                        if (word10b_gwchk = X"36A" or word10b_gwchk = X"09A" ) and dist_gwchk = "0010" then
                            dist_gwchk <= dist_gwchk + 1;
                            state_gwchk <= st_count;
                            testpass_gwchk <= testpass_gwchk;
                        elsif word10b_gwchk = X"15A" and dist_gwchk = "0011" then
                            --    dist_gwchk <= "00";
                            state_gwchk <= st_countpyld; --st_idl;  --done test passed
                            testpass_gwchk <= "1111";
                        --allow for L1ID (2 bytes). N.B: i'm not checking the value
                        elsif dist_gwchk = "0000" or dist_gwchk = "0001" then
                            dist_gwchk <= dist_gwchk + 1;
                            state_gwchk <= st_count;
                            testpass_gwchk <= testpass_gwchk;
                        else
                            dist_gwchk <= "0000";
                            state_gwchk <= st_idl;  --done test failed
                            testpass_gwchk <= "1010";                                            --
                        end if;
                    when st_countpyld =>
                        if word10b_gwchk = X"0F6" or word10b_gwchk = X"309" then  --EOP
                            state_gwchk <= st_idl;
                        else
                            count_pyld <= count_pyld + 1;
                            state_gwchk <= st_countpyld;
                        end if;
                end case checker_gbtword_sm;
            end if;
        end if;
    end process;

    ila_phlck_gwchk <= phlck_gwchk;
    ila_en_gwchk <= en_gwchk;
    ila_count_gwchk <= count_gwchk;
    ila_count_start_gwchk <= count_start_gwchk;
    ila_testpass_gwchk <=  testpass_gwchk;
    ila_dist_gwchk <= dist_gwchk;
    ila_word10b_gwchk <= word10b_gwchk;
    ila_count_pyld <= count_pyld;




end Behavioral;

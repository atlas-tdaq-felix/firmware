--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen      (kchen@bnl.gov)
--! modified:    Marco Trovato (mtrovato@anl.gov)
--!              Ricardo Luz   (rluz@anl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2019/05/21 04:43:14 PM
-- Design Name: FELIX LpGBT Wrapper
-- Module Name: FELIX LpGBT Wrapper - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions: Vivado
-- Description:
--              The TOP MODULE FOR FELIX LpGBT adapted for FELIG . Left only FE
--              Wrapper
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------

LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

library UNISIM;
    use UNISIM.VComponents.all;
    use work.lpgbtfpga_package.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;

Library xpm;
    use xpm.vcomponents.all;

entity FELIX_LpGBT_Wrapper_FELIG is
    Generic (
        STABLE_CLOCK_PERIOD         : integer   := 24;
        GBT_NUM                     : integer   := 24;
        PRBS_TEST_EN                : integer := 1;
        GTHREFCLK_SEL               : std_logic := '0';
        CARD_TYPE                   : integer   := 712;
        CLK_CHIP_SEL                : integer   := 1;
        PLL_SEL                     : std_logic := '1';
        sim_emulator            : boolean       := false
    );
    Port (
        rst_hw                      : in std_logic;
        rxrecclk40m_out             : out std_logic;
        register_map_control        : in register_map_control_type;
        register_map_link_monitor    : out register_map_link_monitor_type;
        CLK40_IN                    : in std_logic;
        clk320_in                   : in std_logic;
        GREFCLK_IN                  : in std_logic;
        GTREFCLK_P_s                : in std_logic_vector(4 downto 0);
        GTREFCLK_N_s                : in std_logic_vector(4 downto 0);
        LMK_P                       : in std_logic_vector(7 downto 0);
        LMK_N                       : in std_logic_vector(7 downto 0);
        RX_LINK_LCK                 : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_2b_IC_in                 : in array_2b(0 to GBT_NUM-1);
        TX_2b_EC_in                 : in array_2b(0 to GBT_NUM-1);
        TX_224b_DATA_in             : in array_224b(0 to GBT_NUM-1);
        TX_ready_in                 : in std_logic_vector(0 to GBT_NUM-1);
        RX_2b_IC_out                : out array_2b(0 to GBT_NUM-1);
        RX_2b_EC_out                : out array_2b(0 to GBT_NUM-1);
        RX_32b_DATA_out             : out array_32b(0 to GBT_NUM-1);
        GT_TXUSRCLK_OUT             : out std_logic_vector(GBT_NUM-1 downto 0);
        GT_RXUSRCLK_OUT             : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_P                        : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_N                        : out std_logic_vector(GBT_NUM-1 downto 0);
        RX_P                        : in std_logic_vector(GBT_NUM-1 downto 0);
        RX_N                        : in std_logic_vector(GBT_NUM-1 downto 0);
        sta_headerFlag_out          : out std_logic_vector(GBT_NUM-1 downto 0);
        tx_flag_out                 : out std_logic_vector(GBT_NUM-1 downto 0);
        alignment_done_out          : out std_logic_vector(GBT_NUM-1 downto 0);
        LMK_LD                      : in  std_logic;
        GT_RXCLK40                  : out std_logic;
        alignment_done_to_LMK       : out std_logic;
        RxCdrLock_to_LMK            : out std_logic
    );
end FELIX_LpGBT_Wrapper_FELIG;


architecture Behavioral of FELIX_LpGBT_Wrapper_FELIG is

    signal GTH_REFCLK_LMK_OUT           : std_logic_vector(GBT_NUM-1 downto 0);
    signal GTH_REFCLK_OUT           : std_logic_vector(GBT_NUM-1 downto 0);

    signal CTRL_TXPOLARITY              : std_logic_vector(GBT_NUM-1 downto 0);
    signal CTRL_RXPOLARITY              : std_logic_vector(GBT_NUM-1 downto 0);
    signal CTRL_GBTTXRST                : std_logic_vector(GBT_NUM-1 downto 0);
    signal CTRL_GBTRXRST                : std_logic_vector(GBT_NUM-1 downto 0);
    --signal CTRL_DATARATE             : std_logic_vector(GBT_NUM-1 downto 0);
    signal CTRL_FECMODE                 : std_logic_vector(GBT_NUM-1 downto 0);
    signal CTRL_CHANNEL_DISABLE         : std_logic_vector(GBT_NUM-1 downto 0);
    signal MON_RXRSTDONE                : std_logic_vector(GBT_NUM-1 downto 0);
    signal MON_TXRSTDONE                : std_logic_vector(GBT_NUM-1 downto 0);
    signal MON_RXPMARSTDONE             : std_logic_vector(GBT_NUM-1 downto 0);
    signal MON_TXPMARSTDONE              : std_logic_vector(GBT_NUM-1 downto 0);
    signal MON_RXCDR_LCK                : std_logic_vector(GBT_NUM-1 downto 0);
    signal MON_CPLL_LCK                 : std_logic_vector(GBT_NUM-1 downto 0);
    signal MON_ALIGNMENT_DONE           : std_logic_vector(GBT_NUM-1 downto 0);
    signal MON_LPGBT_ERRFLG             : std_logic_vector(GBT_NUM-1 downto 0);
    --signal MON_RXRSTDONE_QUAD        : std_logic_vector(GBT_NUM/4-1 downto 0);
    --signal MON_TXRSTDONE_QUAD        : std_logic_vector(GBT_NUM/4-1 downto 0);
    --signal MON_RXCDR_LCK_QUAD        : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal MON_QPLL_LCK                 : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal CTRL_SOFT_RESET              : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal CTRL_TXPLL_DATAPATH_RESET    : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal CTRL_RXPLL_DATAPATH_RESET    : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal CTRL_TX_DATAPATH_RESET       : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal CTRL_RX_DATAPATH_RESET       : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal CTRL_GBT_General_ctrl    : std_logic_vector(63 downto 0);
    signal MON_FEC_ERROR            : std_logic_vector(GBT_NUM-1 downto 0);
    signal MON_FEC_ERR_CNT              : array_32b(0 to GBT_NUM-1) := (others => (others => '0'));
    signal MON_AUTO_RX_RESET_CNT        : array_32b(0 to GBT_NUM-1) := (others => (others => '0'));
    signal CTRL_AUTO_RX_RESET_CNT_CLEAR :  std_logic_vector(GBT_NUM-1 downto 0);

    signal FE_DOWNLINK_USER_DATA        : array_32b(0 to GBT_NUM-1);
    signal FE_DOWNLINK_IC_DATA          : array_2b(0 to GBT_NUM-1);
    signal FE_DOWNLINK_EC_DATA          : array_2b(0 to GBT_NUM-1);
    signal FE_UPLINK_USER_DATA          : array_224b(0 to GBT_NUM-1);
    signal FE_UPLINK_EC_DATA            : array_2b(0 to GBT_NUM-1);
    signal FE_UPLINK_IC_DATA            : array_2b(0 to GBT_NUM-1);

    signal sta_headerFlag_i : std_logic_vector(GBT_NUM-1 downto 0);


    signal DATARATE                     : std_logic_vector(GBT_NUM-1 downto 0);
    signal FECMODE                      : std_logic_vector(GBT_NUM-1 downto 0);

    signal RxCdrLock_o          : std_logic_vector(GBT_NUM-1 downto 0);
    signal alignment_done_o     : std_logic_vector(GBT_NUM-1 downto 0);

    signal alignment_done_o_40rec   : std_logic;
    signal sta_headerFlag_i_40rec   : std_logic;
    signal RxCdrLock_o_40rec        : std_logic;
begin

    refclk_notsim : if sim_emulator = false generate
        --SI5345
        Reference_Clk_Gen_SI5345 : entity work.RefClk_Gen
            Generic Map(
                GBT_NUM => GBT_NUM,
                CARD_TYPE                   => CARD_TYPE,
                CLK_CHIP_SEL                => 0
            )
            Port Map(
                SI53XX_0_P                  => GTREFCLK_P_s(0),
                SI53XX_0_N                  => GTREFCLK_N_s(0),
                SI53XX_2_P                  => GTREFCLK_P_s(1),
                SI53XX_2_N                  => GTREFCLK_N_s(1),
                SI53XX_4_P                  => GTREFCLK_P_s(3),
                SI53XX_4_N                  => GTREFCLK_N_s(3),
                SI53XX_5_P                  => GTREFCLK_P_s(2),
                SI53XX_5_N                  => GTREFCLK_N_s(2),
                SI53XX_8_P                  => GTREFCLK_P_s(4),
                SI53XX_8_N                  => GTREFCLK_N_s(4),

                LMK0_P                      => LMK_P(0),
                LMK0_N                      => LMK_N(0),
                LMK1_P                      => LMK_P(1),
                LMK1_N                      => LMK_N(1),
                LMK2_P                      => LMK_P(2),
                LMK2_N                      => LMK_N(2),
                LMK3_P                      => LMK_P(3),
                LMK3_N                      => LMK_N(3),
                LMK4_P                      => LMK_P(4),
                LMK4_N                      => LMK_N(4),
                LMK5_P                      => LMK_P(5),
                LMK5_N                      => LMK_N(5),
                LMK6_P                      => LMK_P(6),
                LMK6_N                      => LMK_N(6),
                LMK7_P                      => LMK_P(7),
                LMK7_N                      => LMK_N(7),

                GTREFCLK_P_IN               => (others => '0'),
                GTREFCLK_N_IN               => (others => '0'),

                GTH_REFCLK_OUT              => GTH_REFCLK_OUT
            );
        -- LMK03200
        Reference_Clk_Gen_LMK03200 : entity work.RefClk_Gen
            Generic Map(
                GBT_NUM => GBT_NUM,
                CARD_TYPE                   => CARD_TYPE,
                CLK_CHIP_SEL                => 1
            )
            Port Map(
                SI53XX_0_P                  => GTREFCLK_P_s(0),
                SI53XX_0_N                  => GTREFCLK_N_s(0),
                SI53XX_2_P                  => GTREFCLK_P_s(1),
                SI53XX_2_N                  => GTREFCLK_N_s(1),
                SI53XX_4_P                  => GTREFCLK_P_s(3),
                SI53XX_4_N                  => GTREFCLK_N_s(3),
                SI53XX_5_P                  => GTREFCLK_P_s(2),
                SI53XX_5_N                  => GTREFCLK_N_s(2),
                SI53XX_8_P                  => GTREFCLK_P_s(4),
                SI53XX_8_N                  => GTREFCLK_N_s(4),

                LMK0_P                      => LMK_P(0),
                LMK0_N                      => LMK_N(0),
                LMK1_P                      => LMK_P(1),
                LMK1_N                      => LMK_N(1),
                LMK2_P                      => LMK_P(2),
                LMK2_N                      => LMK_N(2),
                LMK3_P                      => LMK_P(3),
                LMK3_N                      => LMK_N(3),
                LMK4_P                      => LMK_P(4),
                LMK4_N                      => LMK_N(4),
                LMK5_P                      => LMK_P(5),
                LMK5_N                      => LMK_N(5),
                LMK6_P                      => LMK_P(6),
                LMK6_N                      => LMK_N(6),
                LMK7_P                      => LMK_P(7),
                LMK7_N                      => LMK_N(7),

                GTREFCLK_P_IN               => (others => '0'),
                GTREFCLK_N_IN               => (others => '0'),

                GTH_REFCLK_OUT              => GTH_REFCLK_LMK_OUT
            );
    end generate; --refclk_notsim
    --
    REGS_INTERFACE : entity work.Regs_RW
        Generic map(
            GBT_NUM                     => GBT_NUM,
            CARD_TYPE                   => CARD_TYPE
        )
        Port map(

            SOFT_TXRST_ALL => open,
            SOFT_RXRST_ALL => open,
            QPLL_RESET => open,
            CTRL_SOFT_RESET             => CTRL_SOFT_RESET,
            CTRL_TXPLL_DATAPATH_RESET   => CTRL_TXPLL_DATAPATH_RESET,
            CTRL_RXPLL_DATAPATH_RESET   => CTRL_RXPLL_DATAPATH_RESET,
            CTRL_TX_DATAPATH_RESET      => CTRL_TX_DATAPATH_RESET,
            CTRL_RX_DATAPATH_RESET      => CTRL_RX_DATAPATH_RESET,
            CTRL_TXPOLARITY             => CTRL_TXPOLARITY,
            CTRL_RXPOLARITY             => CTRL_RXPOLARITY,
            CTRL_GBTTXRST               => CTRL_GBTTXRST,
            CTRL_GBTRXRST               => CTRL_GBTRXRST,
            CTRL_CHANNEL_DISABLE        => CTRL_CHANNEL_DISABLE,
            CTRL_FECMODE                => CTRL_FECMODE,
            CTRL_GBT_General_ctrl       => CTRL_GBT_General_ctrl,
            CTRL_MULTICYCLE_DELAY => open,
            MON_RXRSTDONE               => MON_RXRSTDONE,
            MON_TXRSTDONE               => MON_TXRSTDONE,
            MON_TXFSMRESETDONE          => (others => '0'),
            MON_RXFSMRESETDONE          => (others => '0'),
            MON_RXPMARSTDONE            => MON_RXPMARSTDONE,
            MON_TXPMARSTDONE            => MON_TXPMARSTDONE,
            MON_RXCDR_LCK               => MON_RXCDR_LCK,
            MON_QPLL_LCK                => MON_QPLL_LCK,
            MON_CPLL_LCK                => MON_CPLL_LCK,
            MON_ALIGNMENT_DONE          => MON_ALIGNMENT_DONE,
            MON_FEC_ERROR               =>  MON_FEC_ERROR,
            MON_FEC_ERR_CNT                 => MON_FEC_ERR_CNT,
            MON_AUTO_RX_RESET_CNT           => MON_AUTO_RX_RESET_CNT,
            CTRL_AUTO_RX_RESET_CNT_CLEAR    => CTRL_AUTO_RX_RESET_CNT_CLEAR,

            CTRL_TCLINK_PS_INC_NDEC => open,
            CTRL_TCLINK_PS_STROBE => open,
            CTRL_TCLINK_TX_FINE_REALIGN => open,
            CTRL_TCLINK_TX_UI_ALIGN_CALIB => open,
            CTRL_TCLINK_TX_PI_PHASE_CALIB => open,
            CTRL_TCLINK_TX_CLOSE_LOOP => open,
            CTRL_TCLINK_OFFSET_ERROR => open,
            CTRL_TCLINK_DEBUG_TESTER_ADDR_READ => open,
            MON_TCLINK_ERROR_CONTROLLER => (others => (others=>'0')),
            MON_TCLINK_LOOP_CLOSED => (others => '0'),
            MON_TCLINK_TX_ALIGNED => (others => '0'),
            MON_TCLINK_PS_DONE => (others => '0'),
            CTRL_TCLINK_MASTER_MGT_READY => open,
            MON_TCLINK_PHASE_DETECTOR => (others => (others=>'0')),
            MON_TCLINK_TX_FIFO_FILL_PD => (others => (others=>'0')),
            MON_TCLINK_LOOP_NOT_CLOSED_REASON => (others => (others=>'0')),
            MON_TCLINK_PHASE_ACC => (others => (others=>'0')),
            MON_TCLINK_OPERATION_ERROR => (others => '0'),
            MON_TCLINK_DEBUG_TESTER_DATA_READ => (others => (others=>'0')),
            MON_TCLINK_PS_PHASE_STEP => (others => '0'),
            MON_TCLINK_TX_PI_PHASE => (others => (others=>'0')),
            clk40_in                        => CLK40_IN,
            register_map_control        => register_map_control,
            register_map_link_monitor    => register_map_link_monitor

        );

    RX_DOWNLINK_inst: for I in 0 to GBT_NUM - 1 generate
        RX_2b_IC_out(I) <= FE_DOWNLINK_IC_DATA(I);
        RX_2b_EC_out(I) <= FE_DOWNLINK_EC_DATA(I);
        RX_32b_DATA_out(I) <= FE_DOWNLINK_USER_DATA(I);
    end generate RX_DOWNLINK_inst;

    TX_UPLINK_inst: for I in 0 to GBT_NUM - 1 generate
        FE_UPLINK_IC_DATA(I)   <= TX_2b_IC_in(I);
        FE_UPLINK_EC_DATA(I)   <= TX_2b_EC_in(I);
        FE_UPLINK_USER_DATA(I) <= TX_224b_DATA_in(I);
    end generate TX_UPLINK_inst;

    DATARATE <= (others => '1'); --10.24
    FECMODE  <= CTRL_FECMODE;

    FLX_LpGBT_FE_INST: entity work.FLX_LpGBT_FE_Wrapper_FELIG
        Generic map(
            GBT_NUM                     => GBT_NUM,
            sim_emulator                => sim_emulator
        )
        Port map(
            clk40_in                    => clk40_in,
            clk320_in                   => clk320_in,
            rst_hw                      => rst_hw,

            GTHREFCLK0                   => GTH_REFCLK_OUT,
            GTHREFCLK1                   => GTH_REFCLK_LMK_OUT,

            GT_TXUSRCLK_OUT           => GT_TXUSRCLK_OUT,
            GT_RXUSRCLK_OUT           => GT_RXUSRCLK_OUT,
            GT_RXCLK40_OUT              => GT_RXCLK40,

            RX_P                        => RX_P(GBT_NUM-1 downto 0),
            RX_N                        => RX_N(GBT_NUM-1 downto 0),
            TX_P                        => TX_P(GBT_NUM-1 downto 0),
            TX_N                        => TX_N(GBT_NUM-1 downto 0),

            FE_DOWNLINK_USER_DATA       => FE_DOWNLINK_USER_DATA,
            FE_DOWNLINK_EC_DATA         => FE_DOWNLINK_EC_DATA,
            FE_DOWNLINK_IC_DATA         => FE_DOWNLINK_IC_DATA,

            FE_UPLINK_USER_DATA         => FE_UPLINK_USER_DATA,
            FE_UPLINK_EC_DATA           => FE_UPLINK_EC_DATA,
            FE_UPLINK_IC_DATA           => FE_UPLINK_IC_DATA,
            FE_UPLINK_READY             => TX_ready_in,

            CTRL_SOFT_RESET             => CTRL_SOFT_RESET(GBT_NUM/4-1 downto 0),
            CTRL_TXPLL_DATAPATH_RESET   => CTRL_TXPLL_DATAPATH_RESET(GBT_NUM/4-1 downto 0),
            CTRL_RXPLL_DATAPATH_RESET   => CTRL_RXPLL_DATAPATH_RESET(GBT_NUM/4-1 downto 0),
            CTRL_TX_DATAPATH_RESET      => CTRL_TX_DATAPATH_RESET(GBT_NUM/4-1 downto 0),
            CTRL_RX_DATAPATH_RESET      => CTRL_RX_DATAPATH_RESET(GBT_NUM/4-1 downto 0),
            CTRL_TXPOLARITY             => CTRL_TXPOLARITY(GBT_NUM-1 downto 0),
            CTRL_RXPOLARITY             => CTRL_TXPOLARITY(GBT_NUM-1 downto 0),
            CTRL_GBTTXRST               => CTRL_GBTTXRST(GBT_NUM-1 downto 0),
            CTRL_GBTRXRST               => CTRL_GBTRXRST(GBT_NUM-1 downto 0),
            CTRL_DATARATE               => DATARATE,
            CTRL_FECMODE                => FECMODE,
            CTRL_CHANNEL_DISABLE        => CTRL_CHANNEL_DISABLE(GBT_NUM-1 downto 0),
            CTRL_GBT_General_ctrl       => CTRL_GBT_General_ctrl,
            MON_RXRSTDONE               => MON_RXRSTDONE(GBT_NUM-1 downto 0),
            MON_TXRSTDONE               => MON_TXRSTDONE(GBT_NUM-1 downto 0),
            MON_RXRSTDONE_QUAD          => open,
            MON_TXRSTDONE_QUAD          => open,
            MON_RXPMARSTDONE            => MON_RXPMARSTDONE(GBT_NUM-1 downto 0),
            MON_TXPMARSTDONE            => MON_TXPMARSTDONE(GBT_NUM-1 downto 0),
            MON_RXCDR_LCK               => MON_RXCDR_LCK(GBT_NUM-1 downto 0),
            MON_RXCDR_LCK_QUAD          => open,
            MON_QPLL_LCK                => MON_QPLL_LCK(GBT_NUM/4-1 downto 0),
            MON_CPLL_LCK                => MON_CPLL_LCK(GBT_NUM-1 downto 0),
            MON_ALIGNMENT_DONE          => MON_ALIGNMENT_DONE(GBT_NUM-1 downto 0),
            MON_LPGBT_ERRFLG            => MON_LPGBT_ERRFLG(GBT_NUM-1 downto 0),

            sta_headerFlag_out         => sta_headerFlag_i,
            tx_flag_out                => tx_flag_out,
            LMK_LD                     => LMK_LD,
            RxCdrLock_o                => RxCdrLock_o,
            alignment_done_o            => alignment_done_o,
            MON_AUTO_RX_RESET_CNT       => MON_AUTO_RX_RESET_CNT,
            CTRL_AUTO_RX_RESET_CNT_CLEAR=> CTRL_AUTO_RX_RESET_CNT_CLEAR,
            MON_FEC_ERROR               => MON_FEC_ERROR,
            MON_FEC_ERR_CNT             => MON_FEC_ERR_CNT
        );
    sta_headerFlag_out      <= sta_headerFlag_i;
    alignment_done_out      <= alignment_done_o;
    RxCdrLock_to_LMK        <= RxCdrLock_o(0);
    alignment_done_to_LMK   <= MON_ALIGNMENT_DONE(0);

end Behavioral;

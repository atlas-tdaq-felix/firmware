--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:                Kai Chen    (kchen@bnl.gov)
--! adapted for FELIG by:  Marco Trovato (mtrovato@anl.gov)
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2014/12/05 04:43:14 PM
-- Design Name: FELIX BNL-711 GBT Wrapper
-- Module Name: gbt_top - Behavioral
-- Project Name:
-- Target Devices: KCU
-- Tool Versions: Vivado
-- Description:
--              The TOP MODULE FOR FELIX GBT & GTH
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------

LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
    use UNISIM.VComponents.all;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;
    use ieee.numeric_std.all;
    use work.sim_lib.all; --MT SIMU+
library XPM;
    use XPM.VComponents.all;
entity FELIX_gbt_wrapper_FELIGKCU is
    Generic (
        GBT_NUM                     : integer := 24;
        GTHREFCLK_SEL               : std_logic := '1'; --GREFCLK        : std_logic := '1';
        CARD_TYPE                   : integer := 712;
        PLL_SEL                     : std_logic := '0';  -- CPLL : '0' -- QPLL : '1'
        sim_emulator                : boolean := false
    );
    Port (
        rst_hw                      : in std_logic;
        register_map_control        : in register_map_control_type;
        register_map_gbt_monitor    : out register_map_link_monitor_type;
        Q2_CLK0_GTREFCLK_PAD_N_IN   : in std_logic;
        Q2_CLK0_GTREFCLK_PAD_P_IN   : in std_logic;
        Q8_CLK0_GTREFCLK_PAD_N_IN   : in std_logic;
        Q8_CLK0_GTREFCLK_PAD_P_IN   : in std_logic;
        Q4_CLK0_GTREFCLK_PAD_N_IN   : in std_logic;
        Q4_CLK0_GTREFCLK_PAD_P_IN   : in std_logic;
        Q5_CLK0_GTREFCLK_PAD_N_IN   : in std_logic;
        Q5_CLK0_GTREFCLK_PAD_P_IN   : in std_logic;
        Q6_CLK0_GTREFCLK_PAD_N_IN   : in std_logic;
        Q6_CLK0_GTREFCLK_PAD_P_IN   : in std_logic;
        GREFCLK_IN                  : in std_logic;
        LMK_GTH_REFCLK1_P           : in std_logic;
        LMK_GTH_REFCLK1_N           : in std_logic;
        LMK_GTH_REFCLK3_P           : in std_logic;
        LMK_GTH_REFCLK3_N           : in std_logic;
        clk40_in                    : in std_logic;
        clk240_in                   : in std_logic;
        TX_120b_in                  : in array_120b(0 to GBT_NUM-1);
        RX_120b_out                 : out array_120b(0 to GBT_NUM-1);
        FRAME_LOCKED_O              : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_FRAME_CLK_I              : in std_logic_vector(GBT_NUM-1 downto 0);
        GT_TXUSRCLK_OUT             : out std_logic_vector(GBT_NUM-1 downto 0);
        GT_RXUSRCLK_OUT             : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_P                        : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_N                        : out std_logic_vector(GBT_NUM-1 downto 0);
        RX_P                        : in std_logic_vector(GBT_NUM-1 downto 0);
        RX_N                        : in std_logic_vector(GBT_NUM-1 downto 0);
        RX_FLAG_O                   : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_FLAG_O                   : out std_logic_vector(GBT_NUM-1 downto 0);
        LMK_LD                      : in std_logic;
        GT_RXCLK40                  : out std_logic;
        alignment_done_to_LMK       : out std_logic;
        RxCdrLock_to_LMK            : out std_logic
    );
end FELIX_gbt_wrapper_FELIGKCU;

architecture Behavioral of FELIX_gbt_wrapper_FELIGKCU is
    signal RxSlide_Manual           : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal RxSlide_c                : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal RxSlide_i                : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal RxSlide_Sel              : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal TXUSRRDY                 : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal RXUSRRDY                 : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal GTTX_RESET               : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal GTTX_RESET_MERGE         : std_logic_vector(GBT_NUM/4-1 downto 0) := (others=>'0');
    signal GTRX_RESET               : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal GTRX_RESET_MERGE         : std_logic_vector(GBT_NUM/4-1 downto 0) := (others=>'0');
    signal SOFT_RESET               : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal SOFT_RESET_f             : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal CPLL_RESET               : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal QPLL_RESET               : std_logic_vector(GBT_NUM/4-1 downto 0) := (others=>'0');
    signal txresetdone              : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'1');
    signal txresetdone_clk40        : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'1');
    signal rxresetdone              : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'1');
    signal rxresetdone_clk40        : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'1');
    signal cplllock                 : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal qplllock                 : std_logic_vector(GBT_NUM/4-1 downto 0) := (others=>'0');
    signal qplllock_inv             : std_logic_vector(GBT_NUM/4-1 downto 0) := (others=>'0');
    signal rxcdrlock                : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'1');
    signal rxcdrlock_int            : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'1');
    signal rxcdrlock_a              : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal rxcdrlock_quad           : std_logic_vector(GBT_NUM/4-1 downto 0) := (others=>'0');
    signal rxcdrlock_out            : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal cdr_cnt                  : std_logic_vector(19 downto 0) := (others=>'0');
    signal TX_RESET                 : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal TX_RESET_i               : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal RX_RESET                 : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal RX_RESET_i               : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal GT_TXUSRCLK              : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal GT_RXUSRCLK              : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal RX_FLAG_Oi               : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal outsel_i                 : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal outsel_ii                : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal outsel_o                 : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal RX_120b_out_i            : array_120b(0 to (GBT_NUM-1));
    signal RX_IS_HEADER             : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal RX_HEADER_FOUND          : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal alignment_done           : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal alignment_done_f         : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal alignment_done_a         : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal alignment_done_chk_cnt   : std_logic_vector(12 downto 0) := (others=>'0');
    signal alignment_chk_rst_c      : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal alignment_chk_rst_c1     : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal alignment_chk_rst        : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal alignment_chk_rst_f      : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal alignment_chk_rst_i      : std_logic := '0';
    signal RxSlide                  : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal GT_TX_WORD_CLK           : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal GT_RX_WORD_CLK           : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal TX_TC_METHOD             : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal TC_EDGE                  : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    type data20barray               is array (0 to GBT_NUM-1) of std_logic_vector(19 downto 0);
    signal TX_DATA_20b              : data20barray := (others => (others => '0'));
    signal RX_DATA_20b              : data20barray := (others => (others => '0'));
    signal DESMUX_USE_SW            : std_logic := '0';
    signal DATA_TXFORMAT            : std_logic_vector(95 downto 0) := (others=>'0');
    signal DATA_TXFORMAT_i          : std_logic_vector(95 downto 0) := (others=>'0');
    signal DATA_RXFORMAT            : std_logic_vector(95 downto 0) := (others=>'0');
    signal DATA_RXFORMAT_i          : std_logic_vector(95 downto 0) := (others=>'0');
    signal General_ctrl             : std_logic_vector(63 downto 0) := (others=>'0');
    SIGNAL GBT_TXRESET_DONE         : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    SIGNAL GBT_RXRESET_DONE         : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal txpmaresetdone           : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal rxpmaresetdone           : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal userclk_rx_reset_in      : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal userclk_tx_reset_in      : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal Channel_disable          : std_logic_vector(63 downto 0) := (others=>'0');
    signal TX_TC_DLY_VALUE          : std_logic_vector(191 downto 0) := (others=>'0');
    signal GTH_RefClk               : std_logic_vector(47 downto 0) := (others=>'0');
    signal GTH_RefClk_LMK           : std_logic_vector(23 downto 0) := (others=>'0');
    signal pulse_cnt                : std_logic_vector(29 downto 0) := (others=>'0');
    signal pulse_lg                 : std_logic := '0';
    signal CXP1_GTH_RefClk          : std_logic := '0';
    signal CXP2_GTH_RefClk          : std_logic := '0';
    signal CXP4_GTH_RefClk          : std_logic := '0';
    signal CXP3_GTH_RefClk          : std_logic := '0';
    signal CXP5_GTH_RefClk          : std_logic := '0';
    signal CXP1_GTH_RefClk_LMK      : std_logic := '0';
    signal CXP2_GTH_RefClk_LMK      : std_logic := '0';
    signal error_orig               : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal error_f                  : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal FSM_RST                  : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal auto_gth_rxrst           : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal auto_gbt_rxrst           : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal GT_RXOUTCLK              : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal GT_TXOUTCLK              : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal BITSLIP_MANUAL_r         : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal BITSLIP_MANUAL_2r        : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    signal BITSLIP_MANUAL_3r        : std_logic_vector(GBT_NUM-1 downto 0) := (others=>'0');
    type txrx80b_type               is array (GBT_NUM/4-1 downto 0) of std_logic_vector(79 downto 0);
    signal RX_DATA_80b              : txrx80b_type;
    signal TX_DATA_80b              : txrx80b_type;
    signal RX_N_i                   : std_logic_vector(GBT_NUM-1 downto 0):= (others => '0');
    signal RX_P_i                   : std_logic_vector(GBT_NUM-1 downto 0):= (others => '0');
    signal TX_N_i                   : std_logic_vector(GBT_NUM-1 downto 0):= (others => '0');
    signal TX_P_i                   : std_logic_vector(GBT_NUM-1 downto 0):= (others => '0');
    signal drpclk_in                : std_logic_vector(0 downto 0) := (others=>'0');
    signal GT_RXUSRCLK_40MHz        : std_logic;
    signal QPLL_RESET_LMK           : std_logic_vector(GBT_NUM/4-1 downto 0) := (others=>'0');
    signal QPLL_PIPE                : std_logic_vector(3 downto 0) := (others=>'0');
    signal TXPOLARITY               : std_logic_vector(GBT_NUM-1 downto 0);
    signal RXPOLARITY               : std_logic_vector(GBT_NUM-1 downto 0);
begin

    GT_TXUSRCLK_OUT             <= GT_TXUSRCLK(GBT_NUM-1 downto 0);
    GT_RXUSRCLK_OUT             <= GT_RXUSRCLK(GBT_NUM-1 downto 0);
    FRAME_LOCKED_O              <= alignment_done_f(GBT_NUM-1 downto 0);
    GT_RXCLK40                  <= GT_RXUSRCLK_40MHz;
    alignment_done_to_LMK       <= alignment_done_f(0);
    RxCdrLock_to_LMK            <= rxcdrlock(0);

    --Reference_Clk_Gen

    outclk_sim : if sim_emulator = true generate
        GT_RXOUTCLK <= (others => clk240_in);
        GT_TXOUTCLK <= (others => clk240_in);
    end generate;

    refclk_notsim : if sim_emulator = false generate
        --bank 126, 127, 128 use clk from bank 127
        ibufds_instq2_clk0 : IBUFDS_GTE3
            port map(
                O => CXP1_GTH_RefClk,
                ODIV2 => open,
                CEB => '0',
                I => Q2_CLK0_GTREFCLK_PAD_P_IN,
                IB => Q2_CLK0_GTREFCLK_PAD_N_IN
            );

        --bank 131, 132, 133 use clk from bank 132
        ibufds_instq8_clk0 : IBUFDS_GTE3
            port map
    (
                O => CXP2_GTH_RefClk,
                ODIV2 => open,
                CEB => '0',
                I => Q8_CLK0_GTREFCLK_PAD_P_IN,
                IB => Q8_CLK0_GTREFCLK_PAD_N_IN
            );

        --bank 231, 232, 233,use clk from bank 232
        ibufds_instq4_clk0 : IBUFDS_GTE3
            port map(
                O => CXP3_GTH_RefClk,
                ODIV2 => open,
                CEB => '0',
                I => Q4_CLK0_GTREFCLK_PAD_P_IN,
                IB => Q4_CLK0_GTREFCLK_PAD_N_IN
            );

        --bank 228 use clk from bank 228
        ibufds_instq5_clk0 : IBUFDS_GTE3
            port map(
                O => CXP4_GTH_RefClk,
                ODIV2 => open,
                CEB => '0',
                I => Q5_CLK0_GTREFCLK_PAD_P_IN,
                IB => Q5_CLK0_GTREFCLK_PAD_N_IN
            );

        --bank 224, 225 use clk from bank 225
        CXP5: if (CARD_TYPE = 712) generate
            ibufds_instq6_clk0 : IBUFDS_GTE3
                port map(
                    O => CXP5_GTH_RefClk,
                    ODIV2 => open,
                    CEB => '0',
                    I => Q6_CLK0_GTREFCLK_PAD_P_IN,
                    IB => Q6_CLK0_GTREFCLK_PAD_N_IN
                );
        end generate CXP5;
    end generate; --refclk_notsim

    refclk_notsim2 : if sim_emulator = false generate
        -- GTHREFCLK_1 : if GTHREFCLK_SEL = '0' generate -- Jitter cleaner LMK03200
        -- IBUFDS_GTE2
        --bank 126, 127, 128 use clk from bank 127 -- Jitter cleaner LMK03200
        ibufds_LMK1 : IBUFDS_GTE3
            port map(
                O => CXP1_GTH_RefClk_LMK,
                ODIV2 => open,
                CEB => '0',
                I => LMK_GTH_REFCLK1_P,
                IB => LMK_GTH_REFCLK1_N
            );

        --bank 131, 132, 133 use clk from bank 132 -- Jitter cleaner LMK03200
        ibufds_LMK2 : IBUFDS_GTE3
            port map(
                O => CXP2_GTH_RefClk_LMK,
                ODIV2 => open,
                CEB => '0',
                I => LMK_GTH_REFCLK3_P,
                IB => LMK_GTH_REFCLK3_N
            );
        --================================================================-----
        ----- End Jitter cleaner LMK03200 GTH REF clks-------------------------
        --================================================================------

        --SLR0 banks: 126, 127, 128, 224, 225, 228
        --SLR1 banks: 131, 132, 133, 231, 232, 233
        g_refclk_8ch: if (GBT_NUM <= 8) generate
            GTH_RefClk( 0)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 1)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 2)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 3)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)

            GTH_RefClk( 4)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk( 5)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk( 6)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk( 7)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
        end generate g_refclk_8ch;

        g_refclk_16ch: if ((8 < GBT_NUM) and (GBT_NUM <= 16)) generate
            GTH_RefClk( 0)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 1)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 2)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 3)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 4)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 5)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 6)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 7)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)

            GTH_RefClk( 8)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk( 9)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(10)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(11)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(12)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(13)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(14)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(15)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
        end generate g_refclk_16ch;

        g_refclk_24ch: if ((16 < GBT_NUM) and (GBT_NUM <= 24)) generate
            GTH_RefClk( 0)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 1)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 2)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 3)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 4)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 5)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 6)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 7)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 8)         <= CXP1_GTH_RefClk;--bank 126 (up to 24 channels)
            GTH_RefClk( 9)         <= CXP1_GTH_RefClk;--bank 126 (up to 24 channels)
            GTH_RefClk(10)         <= CXP1_GTH_RefClk;--bank 126 (up to 24 channels)
            GTH_RefClk(11)         <= CXP1_GTH_RefClk;--bank 126 (up to 24 channels)

            GTH_RefClk(12)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(13)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(14)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(15)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(16)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(17)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(18)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(19)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(20)         <= CXP2_GTH_RefClk;--bank 131 (up to 24 channels)
            GTH_RefClk(21)         <= CXP2_GTH_RefClk;--bank 131 (up to 24 channels)
            GTH_RefClk(22)         <= CXP2_GTH_RefClk;--bank 131 (up to 24 channels)
            GTH_RefClk(23)         <= CXP2_GTH_RefClk;--bank 131 (up to 24 channels)
        end generate g_refclk_24ch;

        g_refclk_48ch: if ((24 < GBT_NUM) and (GBT_NUM <= 48)) generate
            GTH_RefClk( 0)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 1)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 2)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 3)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 4)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 5)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 6)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 7)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 8)         <= CXP1_GTH_RefClk;--bank 126 (up to 24 channels)
            GTH_RefClk( 9)         <= CXP1_GTH_RefClk;--bank 126 (up to 24 channels)
            GTH_RefClk(10)         <= CXP1_GTH_RefClk;--bank 126 (up to 24 channels)
            GTH_RefClk(11)         <= CXP1_GTH_RefClk;--bank 126 (up to 24 channels)
            GTH_RefClk(12)         <= CXP4_GTH_RefClk;--bank 228 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(13)         <= CXP4_GTH_RefClk;--bank 228 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(14)         <= CXP4_GTH_RefClk;--bank 228 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(15)         <= CXP4_GTH_RefClk;--bank 228 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(16)         <= CXP5_GTH_RefClk;--bank 224 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(17)         <= CXP5_GTH_RefClk;--bank 224 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(18)         <= CXP5_GTH_RefClk;--bank 224 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(19)         <= CXP5_GTH_RefClk;--bank 224 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(20)         <= CXP5_GTH_RefClk;--bank 225 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(21)         <= CXP5_GTH_RefClk;--bank 225 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(22)         <= CXP5_GTH_RefClk;--bank 225 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(23)         <= CXP5_GTH_RefClk;--bank 225 (up to 48 channels) - BNL712 ONLY

            GTH_RefClk(24)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(25)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(26)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(27)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(28)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(29)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(30)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(31)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(32)         <= CXP2_GTH_RefClk;--bank 131 (up to 24 channels)
            GTH_RefClk(33)         <= CXP2_GTH_RefClk;--bank 131 (up to 24 channels)
            GTH_RefClk(34)         <= CXP2_GTH_RefClk;--bank 131 (up to 24 channels)
            GTH_RefClk(35)         <= CXP2_GTH_RefClk;--bank 131 (up to 24 channels)
            GTH_RefClk(36)         <= CXP3_GTH_RefClk;--bank 231 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(37)         <= CXP3_GTH_RefClk;--bank 231 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(38)         <= CXP3_GTH_RefClk;--bank 231 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(39)         <= CXP3_GTH_RefClk;--bank 231 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(40)         <= CXP3_GTH_RefClk;--bank 232 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(41)         <= CXP3_GTH_RefClk;--bank 232 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(42)         <= CXP3_GTH_RefClk;--bank 232 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(43)         <= CXP3_GTH_RefClk;--bank 232 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(44)         <= CXP3_GTH_RefClk;--bank 233 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(45)         <= CXP3_GTH_RefClk;--bank 233 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(46)         <= CXP3_GTH_RefClk;--bank 233 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(47)         <= CXP3_GTH_RefClk;--bank 233 (up to 48 channels) - BNL712 ONLY
        end generate g_refclk_48ch;

        --  GTH Ref clock from LMK03200 ---  added by RL
        g_refclk_8ch_LMK: if (GBT_NUM <= 8) generate
            GTH_RefClk_LMK( 0)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 1)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 2)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 3)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)

            GTH_RefClk_LMK( 4)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK( 5)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK( 6)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK( 7)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
        end generate g_refclk_8ch_LMK;

        --  GTH Ref clock from LMK03200 ---  added by RL
        g_refclk_16ch_LMK: if ((8 < GBT_NUM) and (GBT_NUM <= 16)) generate
            GTH_RefClk_LMK( 0)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 1)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 2)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 3)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 4)         <= CXP1_GTH_RefClk_LMK;--bank 127 (up to 16 channels)
            GTH_RefClk_LMK( 5)         <= CXP1_GTH_RefClk_LMK;--bank 127 (up to 16 channels)
            GTH_RefClk_LMK( 6)         <= CXP1_GTH_RefClk_LMK;--bank 127 (up to 16 channels)
            GTH_RefClk_LMK( 7)         <= CXP1_GTH_RefClk_LMK;--bank 127 (up to 16 channels)

            GTH_RefClk_LMK( 8)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK( 9)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK(10)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK(11)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK(12)         <= CXP2_GTH_RefClk_LMK;--bank 132 (up to 16 channels)
            GTH_RefClk_LMK(13)         <= CXP2_GTH_RefClk_LMK;--bank 132 (up to 16 channels)
            GTH_RefClk_LMK(14)         <= CXP2_GTH_RefClk_LMK;--bank 132 (up to 16 channels)
            GTH_RefClk_LMK(15)         <= CXP2_GTH_RefClk_LMK;--bank 132 (up to 16 channels)
        end generate g_refclk_16ch_LMK;

        --  GTH Ref clock from LMK03200 ---  added by MT/SS
        g_refclk_24ch_LMK: if ((16 < GBT_NUM) and (GBT_NUM <= 24)) generate
            GTH_RefClk_LMK( 0)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 1)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 2)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 3)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 4)         <= CXP1_GTH_RefClk_LMK;--bank 127 (up to 16 channels)
            GTH_RefClk_LMK( 5)         <= CXP1_GTH_RefClk_LMK;--bank 127 (up to 16 channels)
            GTH_RefClk_LMK( 6)         <= CXP1_GTH_RefClk_LMK;--bank 127 (up to 16 channels)
            GTH_RefClk_LMK( 7)         <= CXP1_GTH_RefClk_LMK;--bank 127 (up to 16 channels)
            GTH_RefClk_LMK( 8)         <= CXP1_GTH_RefClk_LMK;--bank 126 (up to 24 channels)
            GTH_RefClk_LMK( 9)         <= CXP1_GTH_RefClk_LMK;--bank 126 (up to 24 channels)
            GTH_RefClk_LMK(10)         <= CXP1_GTH_RefClk_LMK;--bank 126 (up to 24 channels)
            GTH_RefClk_LMK(11)         <= CXP1_GTH_RefClk_LMK;--bank 126 (up to 24 channels)

            GTH_RefClk_LMK(12)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK(13)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK(14)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK(15)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK(16)         <= CXP2_GTH_RefClk_LMK;--bank 132 (up to 16 channels)
            GTH_RefClk_LMK(17)         <= CXP2_GTH_RefClk_LMK;--bank 132 (up to 16 channels)
            GTH_RefClk_LMK(18)         <= CXP2_GTH_RefClk_LMK;--bank 132 (up to 16 channels)
            GTH_RefClk_LMK(19)         <= CXP2_GTH_RefClk_LMK;--bank 132 (up to 16 channels)
            GTH_RefClk_LMK(20)         <= CXP2_GTH_RefClk_LMK;--bank 131 (up to 24 channels)
            GTH_RefClk_LMK(21)         <= CXP2_GTH_RefClk_LMK;--bank 131 (up to 24 channels)
            GTH_RefClk_LMK(22)         <= CXP2_GTH_RefClk_LMK;--bank 131 (up to 24 channels)
            GTH_RefClk_LMK(23)         <= CXP2_GTH_RefClk_LMK;--bank 131 (up to 24 channels)
        end generate g_refclk_24ch_LMK;
    end generate; -- refclk_notsim2;

    --Registers mapping

    Channel_disable(GBT_NUM-1 downto 0)     <= register_map_control.GBT_CHANNEL_DISABLE(GBT_NUM-1 downto 0);
    General_ctrl                            <= register_map_control.GBT_GENERAL_CTRL;

    RxSlide_Manual                          <= register_map_control.GBT_RXSLIDE_MANUAL(GBT_NUM-1 downto 0);
    RxSlide_Sel                             <= register_map_control.GBT_RXSLIDE_SELECT(GBT_NUM-1 downto 0);
    TXUSRRDY                                <= register_map_control.GBT_TXUSRRDY(GBT_NUM-1 downto 0);
    RXUSRRDY                                <= register_map_control.GBT_RXUSRRDY(GBT_NUM-1 downto 0);
    GTTX_RESET                              <= register_map_control.GBT_GTTX_RESET(GBT_NUM-1 downto 0);
    GTRX_RESET                              <= register_map_control.GBT_GTRX_RESET(GBT_NUM-1 downto 0);
    SOFT_RESET                              <= register_map_control.GBT_SOFT_RESET(GBT_NUM-1 downto 0);
    CPLL_RESET                              <= register_map_control.GBT_PLL_RESET.CPLL_RESET(GBT_NUM-1 downto 0);
    QPLL_RESET                              <= register_map_control.GBT_PLL_RESET.QPLL_RESET(GBT_NUM/4-1+48 downto 48) or QPLL_RESET_LMK;

    TX_TC_DLY_VALUE(47 downto 0)            <= register_map_control.GBT_TX_TC_DLY_VALUE1;
    TX_TC_DLY_VALUE(95 downto 48)           <=register_map_control.GBT_TX_TC_DLY_VALUE2;
    TX_TC_DLY_VALUE(143 downto 96)          <= register_map_control.GBT_TX_TC_DLY_VALUE3;
    TX_TC_DLY_VALUE(191 downto 144)         <= register_map_control.GBT_TX_TC_DLY_VALUE4;

    DATA_TXFORMAT(47 downto 0)      <= register_map_control.GBT_DATA_TXFORMAT1(47 downto 0);
    DATA_RXFORMAT(47 downto 0)      <= register_map_control.GBT_DATA_RXFORMAT1(47 downto 0);
    DATA_TXFORMAT(95 downto 48)     <= register_map_control.GBT_DATA_TXFORMAT2(47 downto 0);
    DATA_RXFORMAT(95 downto 48)     <= register_map_control.GBT_DATA_RXFORMAT2(47 downto 0);

    TX_RESET                                <= register_map_control.GBT_TX_RESET(GBT_NUM-1 downto 0);
    RX_RESET                                <= register_map_control.GBT_RX_RESET(GBT_NUM-1 downto 0);
    TX_TC_METHOD                            <= register_map_control.GBT_TX_TC_METHOD(GBT_NUM-1 downto 0);
    TC_EDGE                                 <= register_map_control.GBT_TC_EDGE(GBT_NUM-1 downto 0);
    outsel_i                                <= register_map_control.GBT_OUTMUX_SEL(GBT_NUM-1 downto 0);
    DESMUX_USE_SW                           <= register_map_control.GBT_MODE_CTRL.DESMUX_USE_SW(0);

    alignment_chk_rst_i                     <= General_ctrl(0);

    register_map_gbt_monitor.GBT_VERSION.DATE                                   <=  GBT_VERSION(63 downto 48);
    register_map_gbt_monitor.GBT_VERSION.GBT_VERSION(35 downto 32)              <=  GBT_VERSION(23 downto 20);
    register_map_gbt_monitor.GBT_VERSION.GTH_IP_VERSION(19 downto 16)           <=  GBT_VERSION(19 downto 16);
    register_map_gbt_monitor.GBT_VERSION.RESERVED                               <=  GBT_VERSION(15 downto 3);
    register_map_gbt_monitor.GBT_VERSION.GTHREFCLK_SEL                          <=  (others => GTHREFCLK_SEL);
    register_map_gbt_monitor.GBT_VERSION.RX_CLK_SEL                             <=  GBT_VERSION(1 downto 1);
    register_map_gbt_monitor.GBT_VERSION.PLL_SEL                                <=  GBT_VERSION(0 downto 0);
    register_map_gbt_monitor.GBT_TXRESET_DONE(GBT_NUM-1 downto 0)               <= txresetdone_clk40;
    register_map_gbt_monitor.GBT_RXRESET_DONE(GBT_NUM-1 downto 0)               <= rxresetdone_clk40;
    register_map_gbt_monitor.GBT_TXFSMRESET_DONE(GBT_NUM-1 downto 0)            <= txpmaresetdone;
    register_map_gbt_monitor.GBT_RXFSMRESET_DONE(GBT_NUM-1 downto 0)            <= rxpmaresetdone;
    register_map_gbt_monitor.GBT_PLL_LOCK.CPLL_LOCK(GBT_NUM-1 downto 0)         <= cplllock;
    register_map_gbt_monitor.GBT_PLL_LOCK.QPLL_LOCK(GBT_NUM/4-1+48 downto 48)   <= qplllock;
    register_map_gbt_monitor.GBT_RXCDR_LOCK(GBT_NUM-1 downto 0)                 <= rxcdrlock;
    register_map_gbt_monitor.GBT_RX_IS_HEADER(GBT_NUM-1 downto 0)               <= RX_IS_HEADER;
    register_map_gbt_monitor.GBT_RX_HEADER_FOUND(GBT_NUM-1 downto 0)            <= RX_HEADER_FOUND;
    register_map_gbt_monitor.GBT_ALIGNMENT_DONE(GBT_NUM-1 downto 0)             <= alignment_done_f;
    register_map_gbt_monitor.GBT_OUT_MUX_STATUS(GBT_NUM-1 downto 0)             <= outsel_o;
    register_map_gbt_monitor.GBT_ERROR(GBT_NUM-1 downto 0)                      <= error_f;

    error_gen : for i in GBT_NUM-1 downto 0 generate
        error_f(i) <= error_orig(i) and alignment_done_f(i);
    end generate;

    --GBT FE

    datamod_gen1 : if DYNAMIC_DATA_MODE_EN='1' generate
        DATA_TXFORMAT_i <= DATA_TXFORMAT;
        DATA_RXFORMAT_i <= DATA_RXFORMAT;
    end generate;

    datamod_gen2 : if DYNAMIC_DATA_MODE_EN='0' generate
        DATA_TXFORMAT_i <= GBT_DATA_TXFORMAT_PACKAGE;
        DATA_RXFORMAT_i <= GBT_DATA_RXFORMAT_PACKAGE;
    end generate;

    process(clk40_in)
    begin
        if clk40_in'event and clk40_in='1' then
            pulse_lg <= pulse_cnt(20);
            if pulse_cnt(20)='1' then
                pulse_cnt <=(others=>'0');
            else
                pulse_cnt <= pulse_cnt+'1';
            end if;
        end if;
    end process;

    process(clk40_in)
    begin
        if clk40_in'event and clk40_in='1' then
            alignment_done_chk_cnt <= alignment_done_chk_cnt + '1';
        end if;
    end process;

    rxalign_auto : for i in GBT_NUM-1 downto 0 generate
        signal TX_RESET40   : std_logic;
        signal RxSlide_c_RX_WORD_CLOCK: std_logic;
        signal rxcdrlock_RX_WORD_CLOCK: std_logic;
        --signal rxresetdone_40: std_logic;
        --signal alignment_done_40: std_logic;
        signal alignment_done_f_RXCLK: std_logic;
    begin
        process(clk40_in)
        begin
            if clk40_in'event and clk40_in='1' then
                if alignment_done_chk_cnt="0000000000000" then
                    alignment_done_a(i) <= rxcdrlock(i) and alignment_done(i);
                else
                    alignment_done_a(i) <= rxcdrlock(i) and alignment_done(i) and alignment_done_a(i);
                end if;
                if alignment_done_chk_cnt="0000000000000" then
                    alignment_done_f(i) <=  rxcdrlock(i) and alignment_done_a(i);
                end if;
            end if;
        end process;

        sync_alignment_done: xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 4,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 1
            )
            port map (
                src_clk => clk40_in,
                src_in => alignment_done_f(i),
                dest_clk => GT_RX_WORD_CLK(i),
                dest_out => alignment_done_f_RXCLK
            );

        RX_120b_out(i) <= RX_120b_out_i(i) when alignment_done_f_RXCLK='1' --in FELIG RX_120b stays in GT_RX_WORD_CLK
                            else (others =>'0');

        --not needed since aligment_done is all in the 40MHz clk
        --sync_alignment_done: xpm_cdc_single
        --    generic map (
        --        DEST_SYNC_FF => 2,
        --        INIT_SYNC_FF => 0,
        --        SIM_ASSERT_CHK => 0,
        --        SRC_INPUT_REG => 0
        --    )
        --    port map (
        --        src_clk => '0',
        --        src_in => alignment_done(i),
        --        dest_clk => clk40_in,
        --        dest_out => alignment_done_40
        --    );

        xpm_cdc_rxresetdone : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => rxresetdone(i),
                dest_clk => clk40_in,
                dest_out => rxresetdone_clk40(i)
            );

        xpm_cdc_TX_RESET : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 4,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 1
            )
            port map (
                src_clk => clk40_in,
                src_in => TX_RESET40,
                dest_clk => GT_TX_WORD_CLK(i),
                dest_out => TX_RESET_i(i)
            );

        xpm_cdc_TXPOLARITY : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => register_map_control.GBT_TXPOLARITY(i),
                dest_clk => GT_TX_WORD_CLK(i),
                dest_out => TXPOLARITY(i)
            );

        xpm_cdc_RXPOLARITY : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => register_map_control.GBT_RXPOLARITY(i),
                dest_clk => GT_RX_WORD_CLK(i),
                dest_out => RXPOLARITY(i)
            );

        auto_rxrst : entity work.FELIX_GBT_RX_AUTO_RST
            port map(
                FSM_CLK                 => clk40_in,
                pulse_lg                => pulse_lg,
                GTHRXRESET_DONE         => rxresetdone_clk40(i),
                alignment_chk_rst       => alignment_chk_rst_c1(i),
                GBT_LOCK                => alignment_done_f(i),
                AUTO_GTH_RXRST          => auto_gth_rxrst(i),
                ext_trig_realign        => open,
                AUTO_GBT_RXRST          => auto_gbt_rxrst(i)
            );

        rafsm : entity work.FELIX_GBT_RXSLIDE_FSM
            port map(
                FSM_RST                 => FSM_RST(i),
                FSM_CLK                 => clk40_in,
                GBT_LOCK                => alignment_done(i),
                RxSlide                 => RxSlide_c(i),
                alignment_chk_rst       => alignment_chk_rst_c(i)
            );

        FSM_RST(i)              <= RX_RESET(i);
        RX_RESET_i(i)           <= (RX_RESET(i) or auto_gbt_rxrst(i));
        alignment_chk_rst(i)    <= (alignment_chk_rst_i or alignment_chk_rst_c(i) or alignment_chk_rst_c1(i));

        sync_RxSlide_c : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => RxSlide_c(i),
                dest_clk => GT_RX_WORD_CLK(i),
                dest_out => RxSlide_c_RX_WORD_CLOCK
            );
        sync_cdrlock: xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => rxcdrlock(i),
                dest_clk => GT_RX_WORD_CLK(i),
                dest_out => rxcdrlock_RX_WORD_CLOCK
            );

        RxSlide_i(i)            <= RxSlide_c_RX_WORD_CLOCK and rxcdrlock_RX_WORD_CLOCK;--<= RxSlide_c(i) and rxcdrlock(i);
        TX_RESET40              <= TX_RESET(i) or (not txresetdone_clk40(i));-- or (not TxFsmResetDone(i));

    end generate;

    outsel_ii             <= outsel_o when DESMUX_USE_SW = '0' else
                             outsel_i;

    RX_FLAG_O             <= RX_FLAG_Oi(GBT_NUM-1 downto 0);

    gbtRxTx : for i in GBT_NUM-1 downto 0 generate
        signal TXFORMAT : std_logic_vector(1 downto 0);
        signal RXFORMAT : std_logic_vector(1 downto 0);
        signal RX_RESET_i_GT_RX_WORD_CLK: std_logic;
        signal TX_TC_DLY_VALUE_sync : std_logic_vector(2 downto 0);
        signal TC_EDGE_sync: std_logic;
        signal TX_RESET_sync: std_logic;
        signal TX_TC_METHOD_sync: std_logic;
    begin
        process(GT_RX_WORD_CLK(i))
        begin
            if GT_RX_WORD_CLK(i)'event and GT_RX_WORD_CLK(i)='1' then
                BITSLIP_MANUAL_r(i)     <= RxSlide_i(i);
                BITSLIP_MANUAL_2r(i)    <= BITSLIP_MANUAL_r(i);
                BITSLIP_MANUAL_3r(i)    <= BITSLIP_MANUAL_2r(i);
                RxSlide(i)              <= BITSLIP_MANUAL_r(i) and (not BITSLIP_MANUAL_2r(i));
            end if;
        end process;

        alignment_chk_rst_f(i)      <= alignment_chk_rst(i);-- or (not RxCdrLock(i));

        xpm_DATA_TXFORMAT : xpm_cdc_array_single generic map(
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0,
                WIDTH => 2
            )
            port map(
                src_clk => '0',
                src_in => DATA_TXFORMAT_i(2*i+1 downto 2*i),
                dest_clk => GT_TX_WORD_CLK(i),
                dest_out => TXFORMAT
            );

        xpm_DATA_RXFORMATl : xpm_cdc_array_single generic map(
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0,
                WIDTH => 2
            )
            port map(
                src_clk => '0',
                src_in => DATA_RXFORMAT_i(2*i+1 downto 2*i),
                dest_clk => GT_RX_WORD_CLK(i),
                dest_out => RXFORMAT
            );

        sync_RX_RESET_i : xpm_cdc_sync_rst
            generic map (
                DEST_SYNC_FF => 2,
                INIT => 1,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0
            )
            port map (
                src_rst => RX_RESET_i(i),
                dest_clk => GT_RX_WORD_CLK(i),
                dest_rst => RX_RESET_i_GT_RX_WORD_CLK
            );

        sync_TX_TC_DLY_VALUE : xpm_cdc_array_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0,
                WIDTH => 3
            )
            port map (
                src_clk => '0',
                src_in => TX_TC_DLY_VALUE(4*i+2 downto 4*i),
                dest_clk => GT_TXUSRCLK(i),
                dest_out => TX_TC_DLY_VALUE_sync
            );

        sync_TC_EDGE : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => TC_EDGE(i),
                dest_clk => GT_TXUSRCLK(i),
                dest_out => TC_EDGE_sync
            );

        sync_TX_TC_METHOD : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => TX_TC_METHOD(i),
                dest_clk => GT_TXUSRCLK(i),
                dest_out => TX_TC_METHOD_sync
            );

        sync_TX_RESET : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => TX_RESET_i(i),
                dest_clk => GT_TXUSRCLK(i),
                dest_out => TX_RESET_sync
            );

        gbtTxRx_inst: entity work.gbtTxRx_FELIX
            generic map(
                channel => i)
            port map(
                error_o                 => error_orig(i),
                RX_FLAG                 => RX_FLAG_Oi(i),
                TX_FLAG                 => TX_FLAG_O(i),

                Tx_DATA_FORMAT          => TXFORMAT,
                Rx_DATA_FORMAT          => RXFORMAT,

                RX_LATOPT_DES           => '1',

                TX_TC_METHOD            => TX_TC_METHOD_sync,
                TC_EDGE                 => TC_EDGE_sync,
                TX_TC_DLY_VALUE         => TX_TC_DLY_VALUE_sync,

                alignment_chk_rst       => alignment_chk_rst_f(i),
                alignment_done_O        => alignment_done(i),
                L40M                    => clk40_in,
                outsel_i                => outsel_ii(i),
                outsel_o                => outsel_o(i),

                TX_RESET_I              => TX_RESET_sync,
                TX_FRAMECLK_I           => TX_FRAME_CLK_I(i),
                TX_WORDCLK_I            => GT_TX_WORD_CLK(i),
                TX_DATA_120b_I          => TX_120b_in(i),
                TX_DATA_20b_O           => TX_DATA_20b(i),

                RX_RESET_I              => RX_RESET_i_GT_RX_WORD_CLK,
                RX_FRAME_CLK_O          => open,--RX_FRAME_CLK_O(i),
                RX_WORD_IS_HEADER_O     => RX_IS_HEADER(i),
                RX_HEADER_FOUND         => RX_HEADER_FOUND(i),
                RX_DATA_20b_I           => RX_DATA_20b(i),
                RX_DATA_120b_O          => RX_120b_out_i(i),
                des_rxusrclk            => GT_RX_WORD_CLK(i),
                RX_WORDCLK_I            => GT_RX_WORD_CLK(i)
            );


    end generate;

    clk_generate : for i in GBT_NUM-1 downto 0 generate

        GTTXOUTCLK_BUFG: bufg_gt
            port map(
                i       => GT_TXOUTCLK(i),
                div     => "000",
                clr     => '0',
                cemask  => '0',
                clrmask => '0',
                ce      => '1',
                o       => GT_TX_WORD_CLK(i)
            );

        GT_TXUSRCLK(i) <= GT_TX_WORD_CLK(i);

        GTRXOUTCLK_BUFG: bufg_gt
            port map(
                i       => GT_RXOUTCLK(i),
                div     => "000",
                clr     => '0',
                cemask  => '0',
                clrmask => '0',
                ce      => '1',
                o       => GT_RX_WORD_CLK(i) --changed MT/SS
            );


        GT_RXUSRCLK(i) <= GT_RX_WORD_CLK(i); -- changed MT/SS
    end generate;

    rxcdrlock_sim : if sim_emulator = true generate
        rxcdrlock_gen : for i in GBT_NUM-1 downto 0 generate
            rxcdrlock(i) <= '1'; --hard coding to 1 since MGT has been commented out
        end generate;
    end generate;

    qpllreset_notsim : if sim_emulator = false generate
        qplllock_inv <= not qplllock;

        process(clk40_in)
        begin
            if clk40_in'event and clk40_in='1' then
                QPLL_PIPE(3 downto 1) <= QPLL_PIPE(2 downto 0);
                QPLL_PIPE(0) <= LMK_LD;
                if ((QPLL_PIPE(3) = '0') AND (QPLL_PIPE(0) = '1')) then --rising edge
                    QPLL_RESET_LMK  <= qplllock_inv;
                else
                    QPLL_RESET_LMK  <= (others =>'0');
                end if;
            end if;
        end process;
    end generate; --qpllreset_notsim

    qpllgen_sim : if sim_emulator = true generate
        RX_DATA_20b <= TX_DATA_20b; --data looped back
    end generate; --qpllgen_sim

    qpllgen_notsim : if sim_emulator = false generate
        drpclk_in(0) <= clk40_in;

        port_trans : for i in GBT_NUM-1 downto 0 generate
            RX_N_i(i)   <= RX_N(i);
            RX_P_i(i)   <= RX_P(i);
            TX_N(i)     <= TX_N_i(i);
            TX_P(i)     <= TX_P_i(i);

        end generate;

        GTH_inst : for i in (GBT_NUM-1)/4 downto 0 generate

            RX_DATA_20b(4*i+0) <= RX_DATA_80b(i)(19 downto 0);
            RX_DATA_20b(4*i+1) <= RX_DATA_80b(i)(39 downto 20);
            RX_DATA_20b(4*i+2) <= RX_DATA_80b(i)(59 downto 40);
            RX_DATA_20b(4*i+3) <= RX_DATA_80b(i)(79 downto 60);


            TX_DATA_80b(i) <= TX_DATA_20b(4*i+3) & TX_DATA_20b(4*i+2) & TX_DATA_20b(4*i+1) & TX_DATA_20b(4*i+0);

            GTH_TOP_INST: entity work.GTH_QPLL_Wrapper_FELIG
                Port map(
                    gthrxn_in                       => RX_N_i(4*i+3 downto 4*i),
                    gthrxp_in                       => RX_P_i(4*i+3 downto 4*i),
                    gthtxn_out                      => TX_N_i(4*i+3 downto 4*i),
                    gthtxp_out                      => TX_P_i(4*i+3 downto 4*i),

                    drpclk_in                       => drpclk_in,
                    gtrefclk0_in                    => GTH_RefClk(4*i downto 4*i),
                    gtrefclk1_in                    => GTH_RefClk_LMK(4*i downto 4*i),
                    gt_rxusrclk_in                  => GT_RX_WORD_CLK(4*i+3 downto 4*i),
                    gt_rxoutclk_out                 => GT_RXOUTCLK(4*i+3 downto 4*i),
                    gt_txusrclk_in                  => GT_TX_WORD_CLK(4*i+3 downto 4*i),
                    gt_txoutclk_out                 => GT_TXOUTCLK(4*i+3 downto 4*i),

                    userdata_tx_in                  => TX_DATA_80b(i),
                    userdata_rx_out                 => RX_DATA_80b(i),
                    rxpolarity_in                   => RXPOLARITY(4*i+3 downto 4*i),
                    txpolarity_in                   => TXPOLARITY(4*i+3 downto 4*i),

                    loopback_in                     => register_map_control.GTH_LOOPBACK_CONTROL,
                    rxcdrhold_in                    => '0',

                    userclk_rx_reset_in             => userclk_rx_reset_in(i downto i),
                    userclk_tx_reset_in             => userclk_tx_reset_in(i downto i),

                    reset_all_in                    => SOFT_RESET_f(i downto i),
                    reset_tx_pll_and_datapath_in    => QPLL_RESET(i downto i),
                    reset_tx_datapath_in            => GTTX_RESET_MERGE(i downto i),
                    reset_rx_pll_and_datapath_in    => CPLL_RESET(i downto i),
                    reset_rx_datapath_in            => GTRX_RESET_MERGE(i downto i),

                    qpll0lock_out                   => open,
                    qpll1lock_out                   => qplllock(i downto i),
                    qpll1fbclklost_out              => open,--
                    qpll0fbclklost_out              => open,
                    rxslide_in                      => RxSlide(4*i+3 downto 4*i),

                    cplllock_out                    => cplllock(4*i+3 downto 4*i),

                    rxresetdone_out                 => rxresetdone(4*i+3 downto 4*i),
                    txresetdone_out                 => txresetdone(4*i+3 downto 4*i),
                    rxpmaresetdone_out              => rxpmaresetdone(4*i+3 downto 4*i),
                    txpmaresetdone_out              => txpmaresetdone(4*i+3 downto 4*i),
                    reset_tx_done_out               => open,--txresetdone_quad(i downto i),
                    reset_rx_done_out               => open,--rxresetdone_quad(i downto i),
                    reset_rx_cdr_stable_out         => rxcdrlock_quad(i downto i),
                    rxcdrlock_out                   => rxcdrlock_out(4*i+3 downto 4*i)
                );

            process(clk40_in)
            begin
                if clk40_in'event and clk40_in='1' then
                    if cdr_cnt ="00000000000000000000" then
                        rxcdrlock_a(4*i)     <= rxcdrlock_out(4*i);
                        rxcdrlock_a(4*i+1)   <= rxcdrlock_out(4*i+1);
                        rxcdrlock_a(4*i+2)   <= rxcdrlock_out(4*i+2);
                        rxcdrlock_a(4*i+3)   <= rxcdrlock_out(4*i+3);
                    else
                        rxcdrlock_a(4*i) <= rxcdrlock_a(4*i) and rxcdrlock_out(4*i);
                        rxcdrlock_a(4*i+1) <= rxcdrlock_a(4*i+1) and rxcdrlock_out(4*i+1);
                        rxcdrlock_a(4*i+2) <= rxcdrlock_a(4*i+2) and rxcdrlock_out(4*i+2);
                        rxcdrlock_a(4*i+3) <= rxcdrlock_a(4*i+3) and rxcdrlock_out(4*i+3);
                    end if;
                    if cdr_cnt="00000000000000000000" then
                        rxcdrlock_int(4*i) <=rxcdrlock_a(4*i);
                        rxcdrlock_int(4*i+1) <=rxcdrlock_a(4*i+1);
                        rxcdrlock_int(4*i+2) <=rxcdrlock_a(4*i+2);
                        rxcdrlock_int(4*i+3) <=rxcdrlock_a(4*i+3);
                    end if;
                end if;
            end process;
            rxcdrlock(4*i) <= (not Channel_disable(4*i)) and rxcdrlock_int(4*i);
            rxcdrlock(4*i+1) <= (not Channel_disable(4*i+1)) and rxcdrlock_int(4*i+1);
            rxcdrlock(4*i+2) <= (not Channel_disable(4*i+2)) and rxcdrlock_int(4*i+2);
            rxcdrlock(4*i+3) <= (not Channel_disable(4*i+3)) and rxcdrlock_int(4*i+3);

            SOFT_RESET_f(i) <= SOFT_RESET(i) or QPLL_RESET(i);--or rst_hw;-- or GTRX_RESET(i);

            userclk_rx_reset_in(i) <=not (rxpmaresetdone(4*i+0) or rxpmaresetdone(4*i+1) or rxpmaresetdone(4*i+2) or rxpmaresetdone(4*i+3));
            userclk_tx_reset_in(i) <=not (txpmaresetdone(4*i+0) or txpmaresetdone(4*i+1) or txpmaresetdone(4*i+2) or txpmaresetdone(4*i+3));

            GTTX_RESET_MERGE(i) <= GTTX_RESET(4*i) or GTTX_RESET(4*i+1) or GTTX_RESET(4*i+2) or GTTX_RESET(4*i+3);
            GTRX_RESET_MERGE(i) <= (GTRX_RESET(4*i) or (auto_gth_rxrst(4*i) and rxcdrlock(4*i)))
                                   or (GTRX_RESET(4*i+1) or (auto_gth_rxrst(4*i+1) and rxcdrlock(4*i+1)))
                                   or (GTRX_RESET(4*i+2) or (auto_gth_rxrst(4*i+2) and rxcdrlock(4*i+2)))
                                   or (GTRX_RESET(4*i+3) or (auto_gth_rxrst(4*i+3) and rxcdrlock(4*i+3))) ;

        end generate;
    end generate;

    process(clk40_in)
    begin
        if clk40_in'event and clk40_in='1' then
            cdr_cnt <=cdr_cnt+'1';
        end if;
    end process;


    bufgceobuf_notsim : if sim_emulator = false generate
        BUFGCE_DIV_inst : BUFGCE_DIV
            generic map (
                BUFGCE_DIVIDE => 6, -- 1-8 -divide by 6 to get 40 MHz
                -- Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
                IS_CE_INVERTED => '0', -- Optional inversion for CE
                IS_CLR_INVERTED => '0', -- Optional inversion for CLR
                IS_I_INVERTED => '0' -- Optional inversion for I
            )
            port map (
                O => GT_RXUSRCLK_40MHz, -- 1-bit output: Buffer
                CE => '1', -- 1-bit input: Buffer enable
                CLR => '0', -- 1-bit input: Asynchronous clear
                I => GT_RX_WORD_CLK(0) -- 1-bit input: Buffer 240 MHz RXUSER_CLK after BUFG_GT
            );

    end generate;

end Behavioral;

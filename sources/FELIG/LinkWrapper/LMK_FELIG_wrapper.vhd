--ricardo luz, argonne

library ieee;
    use ieee.std_logic_1164.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;

library UNISIM;
    use UNISIM.VComponents.all;

library xpm;
    use xpm.vcomponents.all;

entity LMK_FELIG_wrapper is
    Port (
        clk40_in                : in std_logic;
        clk10_xtal              : in std_logic;
        GT_RXCLK40              : in std_logic;
        rst_hw                  : in std_logic;
        LMK_DATA                : out std_logic;
        LMK_CLK                 : out std_logic;
        LMK_LE                  : out std_logic;
        LMK_GOE                 : out std_logic;
        LMK_LD                  : in std_logic;
        LMK_SYNCn               : out std_logic;
        LMK_LOCKED              : out std_logic;
        RxCdrLock               : in std_logic;
        alignment_done          : in std_logic;
        CLK40_FPGA2LMK_out_P    : out std_logic;
        CLK40_FPGA2LMK_out_N    : out std_logic
    );
end LMK_FELIG_wrapper;

architecture Behavioral of LMK_FELIG_wrapper is
    signal LMK_PIPE                     : std_logic_vector(3 downto 0);
    signal LMK_RESET                    : std_logic;
    type STATEM  is (st_idle, st_wait_for_aligment, st_LMK_reseted);
    signal state                        : STATEM := st_idle;
    signal LMK_RESET_b                  : std_logic := '0';
    signal RESET_TO_LMK                 : std_logic;
    signal RESET_TO_LMK_CLK10           : std_logic;
begin

    lmk_init : entity work.LMK03200_wrapper
        generic map(
            freq => 240)
        port map(
            rst_lmk => RESET_TO_LMK_CLK10,--'0',
            hw_rst => rst_hw,
            LMK_locked => LMK_LOCKED,
            clk40m_in => '0',--clk40_rxusrclk,
            clk10m_in => clk10_xtal,
            CLK40_FPGA2LMK_P => open,
            CLK40_FPGA2LMK_N => open,
            LMK_DATA => LMK_DATA,
            LMK_CLK => LMK_CLK,
            LMK_LE => LMK_LE,
            LMK_GOE => LMK_GOE,
            LMK_LD => LMK_LD,
            LMK_SYNCn => LMK_SYNCn);

    xpm_cdc_RESET_LMK : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 1
        )
        port map (
            src_clk => clk40_in,
            src_in => RESET_TO_LMK,
            dest_clk => clk10_xtal,
            dest_out => RESET_TO_LMK_CLK10
        );

    OBUF240_LMK03200: OBUFDS
        generic map (
            IOSTANDARD => "LVDS",
            SLEW       => "FAST")
        port map(
            I => GT_RXCLK40,
            O => CLK40_FPGA2LMK_out_P,
            OB => CLK40_FPGA2LMK_out_N);


    process(clk40_in)
    begin
        if (clk40_in'event and clk40_in='1') then
            LMK_PIPE(3 downto 1) <= LMK_PIPE(2 downto 0);
            LMK_PIPE(0) <= LMK_RESET_b;
        end if;
    end process;
    LMK_RESET <= LMK_PIPE(0) or LMK_PIPE(1) or LMK_PIPE(2) or LMK_PIPE(3); --this way the pulse is 4 clks long. the lti clock is 10MHz
    RESET_TO_LMK <= LMK_RESET;


    process(clk40_in)
    begin
        if (clk40_in'event and clk40_in='1') then
       sm_lmk_reset: case state is
                when st_idle =>
                    LMK_RESET_b <= '0';
                    if RxCdrLock = '1' then
                        state <= st_wait_for_aligment;
                    else
                        state <= st_idle;
                    end if;
                when st_wait_for_aligment =>
                    if alignment_done = '1'then
                        LMK_RESET_b <= '1';
                        state <= st_LMK_reseted;
                    elsif RxCdrLock = '0' then
                        LMK_RESET_b <= '0';
                        state <= st_idle;
                    else
                        LMK_RESET_b <= '0';
                        state <= st_wait_for_aligment;
                    end if;
                when st_LMK_reseted =>
                    LMK_RESET_b <= '0';
                    if RxCdrLock = '0' and LMK_RESET_b <= '0'then
                        state <= st_idle;
                    else
                        state <= st_LMK_reseted;
                    end if;
                when others =>
                    state <= st_idle;
            end case sm_lmk_reset;
        end if;
    end process;
end Behavioral;

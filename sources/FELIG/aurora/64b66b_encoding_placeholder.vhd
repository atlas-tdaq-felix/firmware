library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.type_lib.ALL;
    use work.pcie_package.all;

entity encoder64b66b_placeholder is
    port (
        clk       : in  std_logic;
        data_in   : in  std_logic_vector (63 downto 0);
        ready_in  : in  std_logic;
        count     : in  std_logic_vector (2 downto 0);
        data_out  : out std_logic_vector (65 downto 0);
        ready_out : out std_logic
    );
end encoder64b66b_placeholder;
architecture Behavioral of encoder64b66b_placeholder is
    signal flag    : std_logic := '0';
    signal data_out_b : std_logic_vector (65 downto 0) := (others => '0');
    signal data_zero  : std_logic_vector (63 downto 0) := (others => '0');

begin
    proc : process (clk, ready_in)
    begin
        if ready_in = '1' then
            flag <= '1';
        elsif clk'event and clk='1' then
            if flag = '1' and count = "000" then
                ready_out <= '1';
                if data_in = data_zero then
                    data_out_b  <= "10" & data_in;--data_in;
                else
                    data_out_b  <= "01" & data_in;
                end if;
                flag      <= '0';
            else
                ready_out <= '0';
            end if;
        end if;
    end process proc;
    data_out  <= data_out_b;
end Behavioral;

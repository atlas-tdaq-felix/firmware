----------------------------------------------------------------------------------
-- Company: Argonne National Laboratory
-- Engineer: Initially written by Will Brunner. Adapted by Ricardo Luz (rluz@anl.gov)
-- Create Date: 06/30/2021 03:01:26 PM
-- Description: 64b66b encoder. Encoder and scrambler
-- Revision 0.01 - File Created
----------------------------------------------------------------------------------

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.type_lib.ALL;
    use work.pcie_package.all;

entity encoder64b66b is
    port (
        clk             : in   std_logic;
        rst             : in   std_logic;
        data_in         : in   std_logic_vector(0 to 63);
        data_in_rdy     : in   std_logic;
        data_in_rdy_idl : in   std_logic;
        data_in_valid   : in   std_logic_vector(2 downto 0);
        data_in_code    : in   std_logic_vector(1 downto 0);
        data_in_eof     : in   std_logic;
        count_66to10    : in  std_logic_vector (2 downto 0);
        data_out        : out  std_logic_vector(65 downto 0);
        data_out_rdy    : out  std_logic
    );
end encoder64b66b;

architecture Behavioral of encoder64b66b is
    --aurora_64b66b_1TXch_SIMPLEX_TX_AURORA_LANE
    signal DataOut64b_i        : std_logic_vector(0 to 63)  := (others => '0');            --input from 8to64 gearbox
    signal DataOut64brdy_i      : std_logic := '0';                                        --input from 8to64 gearbox
    signal gen_sep7_FELIG_i      : std_logic := '0';                                        --wire process gen_sep
    signal gen_sep_FELIG_i      : std_logic := '0';                                        --wire process gen_sep
    signal ValidOct_i        : std_logic_vector(2 downto 0)  := (others => '0');        --input from 8to64 gearbox
    signal tx_channel_up_i      : std_logic := '1';                                        --wire probably can be left to always '1'. check later. from aurora_64b66b_1TXch_SIMPLEX_TX_GLOBAL_LOGIC
    signal gen_periodic_cb_i    : std_logic := '0';                                        --wire probably can be left to always '0'. check later. from aurora_64b66b_1TXch_SIMPLEX_TX_LL
    signal tx_header_1_i      : std_logic := '0';                                        --wire encoder output
    signal tx_header_0_i      : std_logic := '0';                                        --wire encoder output
    signal tx_data_i        : std_logic_vector(0 to 63)  := (others => '0');            --wire encoder output
    signal gen_na_idles_i      : std_logic := '0';                                        --wire probably can be left to always '0'. it's always 0 after channel up is 1. check later. from aurora_64b66b_1TXch_SIMPLEX_TX_GLOBAL_LOGIC
    signal gen_ch_bond_i      : std_logic := '0';                                        --wire probably can be left to always '0'. it's always 0 after channel up is 1. check later. from aurora_64b66b_1TXch_SIMPLEX_TX_GLOBAL_LOGIC
    signal tx_lane_up_i        : std_logic := '0';                                        --wire encoder output
    signal tx_hard_err_i      : std_logic := '0';                                        --wire encoder output
    signal tx_soft_err_i      : std_logic := '0';                                        --wire encoder output
    signal user_clk          : std_logic := '0';                                        --input clock
    signal reset_lanes_i      : std_logic := '0';                                        --wire probably can be left to always '0'. it's always 0 after channel up is 1. check later. from aurora_64b66b_1TXch_SIMPLEX_TX_GLOBAL_LOGIC
    signal DataOut64brdy_symgen    : std_logic := '0';                                        --wire process gen_idle
    signal reset          : std_logic := '0';                                        --wire seems to be always '0'. recheck later
    signal GEN_CC                   : std_logic := '0';
    signal TX_BUF_ERR               : std_logic := '0';
    --aurora_64b66b_1TXch_WRAPPER
    signal tx_header_i        : std_logic_vector(1 downto 0)  := (others => '0');        --reg encoder output
    signal tx_hdr_out        : std_logic_vector(1 downto 0)  := (others => '0');        --output scrambler header output
    signal scrambled_data_out    : std_logic_vector(63 downto 0)  := (others => '0');        --output scrambler data output
    signal DataOut64brdy_scrambler  : std_logic := '0';                                        --wire process gen_idle
    signal txdatavalid_i      : std_logic := '0';                                        --wire output scrambler data output. not used?
    --signal txdatavalid_symgen_i      : std_logic := '0';

    --others
    signal DataOut64beof_i                 : std_logic := '0';                                        --input from 8to64 gearbox
    signal DataIn8bCode                  : std_logic_vector(1 downto 0)  := (others => '0');       --input from data generator
    signal IdleTimer                     : std_logic_vector(3 downto 0)  := (others => '0');       --wire process gen_idle
    signal IdleOut                       : std_logic := '0';                                        --wire process gen_idle
    signal DataOut64brdy_fifo_i            : std_logic := '0';                                        --wire process gen_idle
    signal DataOut64brdy_scrambler_i       : std_logic := '0';                                        --wire process gen_idle
    signal DataOut64brdy_scrambler_ii      : std_logic := '0';                                        --wire process gen_idle
    signal DataOut64brdy_fifo              : std_logic := '0';                                        --wire process gen_idle
    signal DataOut64brdy_fifo_ii           : std_logic := '0';                                        --wire process gen_idle
    signal flag                            : std_logic := '0';
    signal data_out_b                      : std_logic_vector(65 downto 0)  := (others => '0');
    signal data_out_rdy_b                  : std_logic := '0';
--signal tx_channel_up_i               : std_logic := '0';
begin

    --inputs
    user_clk        <= clk;
    DataOut64b_i    <= data_in;
    DataOut64brdy_i <= data_in_rdy;
    ValidOct_i      <= data_in_valid;
    DataOut64beof_i <= data_in_eof;
    DataIn8bCode    <= data_in_code;
    reset           <= rst;
    tx_channel_up_i <= not reset;

    gen_sep : process (DataOut64beof_i)
    begin
        if DataOut64beof_i = '1' then
            if ValidOct_i = "111" then --provavelmente preciso de mudar este valor
                gen_sep_FELIG_i  <= '0';
                gen_sep7_FELIG_i <= '1';
            else
                gen_sep_FELIG_i  <= '1';
                gen_sep7_FELIG_i <= '0';
            end if;
        else
            gen_sep_FELIG_i  <= '0';
            gen_sep7_FELIG_i <= '0';
        end if;
    end process gen_sep;

    DataOut64brdy_symgen    <= DataOut64brdy_i or IdleOut;
    DataOut64brdy_scrambler <= DataOut64brdy_scrambler_i or DataOut64brdy_scrambler_ii;
    DataOut64brdy_fifo      <= DataOut64brdy_fifo_i or DataOut64brdy_fifo_ii;

    IdleOut <= data_in_rdy_idl xor data_in_rdy;--when DataIn8bCode = "11" else '0';

    gen_idl : process (user_clk)
    begin
        if user_clk'event and user_clk='1' then
            --    if DataIn8bCode = "11" and IdleTimer /= "1001" then
            --      IdleTimer <= IdleTimer + "0001";
            --      IdleOut   <= '0';
            --    elsif DataIn8bCode = "11" and IdleTimer = "1001" then
            --      IdleTimer <= "0000";
            --      IdleOut   <= '1';
            --    else
            --      IdleTimer <= "0000";
            --      --IdleOut   <= '0';
            --    end if;
            DataOut64brdy_scrambler_i <= IdleOut;
            DataOut64brdy_fifo_i <= (DataOut64brdy_scrambler_i);
            DataOut64brdy_scrambler_ii <= (DataOut64brdy_i);
            DataOut64brdy_fifo_ii <= (DataOut64brdy_scrambler_ii);
        end if;
    end process gen_idl;

    --RL: encoder
    encode : entity work.aurora_64b66b_1TXch_SIMPLEX_TX_AURORA_LANE
        generic map(
            EXAMPLE_SIMULATION   => 0
        )
        port map(
            TX_PE_DATA              => DataOut64b_i,            --input   --WB gearbox s_axi_tx_tdata),
            TX_PE_DATA_V            => DataOut64brdy_i,         --input   --WB gearbox !tx_dst_rdy_n_i),
            GEN_SEP7                => gen_sep7_FELIG_i,        --input   --WB gen_sep7_i),
            GEN_SEP                 => gen_sep_FELIG_i,         --input   --WB gen_sep_i),
            SEP_NB                  => ValidOct_i,              --input   --WB sep_nb_i[0:2]),
            TX_CHANNEL_UP           => tx_channel_up_i,         --input
            GEN_CC                  => GEN_CC,                     --input   --WB add this manually later tfor clock correction signal
            GEN_PERIODIC_CB         => gen_periodic_cb_i,       --input
            TX_BUF_ERR              => TX_BUF_ERR,                     --input   --WB FELIG |tx_buf_err_i,
            TX_HEADER_1             => tx_header_1_i,           --output
            TX_HEADER_0             => tx_header_0_i,           --output
            TX_DATA                 => tx_data_i(0 to 63),      --output
            TX_ENA_COMMA_ALIGN      => open,                    --output  --WBtx_ena_comma_align_i),
            GEN_NA_IDLE             => gen_na_idles_i,          --input
            GEN_CH_BOND             => gen_ch_bond_i,           --input
            TX_LANE_UP              => tx_lane_up_i,            --output
            TX_HARD_ERR             => tx_hard_err_i,           --output
            TX_SOFT_ERR             => tx_soft_err_i,           --output
            USER_CLK                => user_clk,                --input
            RESET_LANES             => reset_lanes_i,           --input
            TXDATAVALID_SYMGEN_IN   => DataOut64brdy_symgen,    --input   --WB txdatavalid_symgen_i),
            RESET                   => reset                    --input
        );

    tx_header_i <= tx_header_1_i & tx_header_0_i;

    --RL scrambler
    scramble : entity work.aurora_64b66b_1TXch_WRAPPER
        --generic map(
        --     --check generics
        --    INTER_CB_GAP                     => "00101",
        --    wait_for_fifo_wr_rst_busy_value  => "010",
        --    BACKWARD_COMP_MODE1              => "0",
        --    BACKWARD_COMP_MODE2              => "0",
        --    BACKWARD_COMP_MODE3              => "0",
        --    EXAMPLE_SIMULATION               => 0
        --)
        port map(
            tx_hdr_out              => tx_hdr_out,              --output
            scrambled_data_out      => scrambled_data_out,      --output
            TXDATA_IN               => tx_data_i(0 to 63),      --input
            DataOut64brdy           => DataOut64brdy_scrambler, --input
            TXHEADER_IN             => tx_header_i,             --input
            TXDATAVALID_OUT         => txdatavalid_i,           --output
            TXDATAVALID_SYMGEN_OUT  => open,                    --output --not used in Will's code txdatavalid_symgen_i
            TXUSRCLK2_IN            => user_clk,                --input
            USER_CLK                => user_clk,                --input
            RESET                   => reset                    --input
        );

    proc : process (clk, DataOut64brdy_fifo)
    begin
        if DataOut64brdy_fifo = '1' then
            if count_66to10 = "000" and data_out_b  /= tx_hdr_out & scrambled_data_out then
                data_out_rdy_b <= '1';
                data_out_b  <= tx_hdr_out & scrambled_data_out;
                flag <= '1';
            else
                flag <= '1';
            --data_out_rdy_b <= '0';
            end if;
        elsif clk'event and clk='1' then
            if flag = '1' and count_66to10 = "000" and data_out_rdy_b <= '0' then
                data_out_rdy_b <= '1';
                data_out_b  <= tx_hdr_out & scrambled_data_out;
                flag      <= '0';
            else
                data_out_rdy_b <= '0';
            --flag      <= '0';
            end if;
        end if;
    end process proc;

    --outputs
    data_out        <= data_out_b;
    data_out_rdy    <= data_out_rdy_b;

end Behavioral;

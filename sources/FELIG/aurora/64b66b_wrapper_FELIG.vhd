----------------------------------------------------------------------------------
-- Company: Argonne National Laboratory
-- Engineer: Ricardo Luz (rluz@anl.gov)
-- Create Date: 05/19/2021 09:56:53 AM
-- Description: 64b66b module for FELIG data emulator
-- Revision 0.01 - File Created
----------------------------------------------------------------------------------

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    --use IEEE.STD_LOGIC_ARITH.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.type_lib.ALL;
    use work.pcie_package.all;

entity aurora_wrapper_FELIG is
    port (
        clk           : in std_logic;
        rst           : in std_logic;
        read_en_in    : in std_logic;
        count_fifochk : in std_logic_vector(15 downto 0);
        data_in       : in std_logic_vector(9 downto 0);
        --output_width  : in std_logic_vector(2 downto 0);
        read_en_out   : out std_logic;
        data_out      : out std_logic_vector(9 downto 0)
    );
end aurora_wrapper_FELIG;

architecture Behavioral of aurora_wrapper_FELIG is
    signal flag_start          : std_logic := '1';
    signal counter_aurora      : std_logic_vector(3 downto 0) := (others => '0');
    signal read_en_out_b       : std_logic := '0';
    signal get_data            : std_logic := '0';
    signal data_64             : std_logic_vector(63 downto 0) := (others => '0');
    signal data_64_buf         : std_logic_vector(63 downto 0) := (others => '0');
    signal data_66             : std_logic_vector(65 downto 0) := (others => '0');
    signal data_66_real        : std_logic_vector(65 downto 0) := (others => '0');
    signal data_66_placeolder  : std_logic_vector(65 downto 0) := (others => '0');
    signal data_66_buf         : std_logic_vector(73 downto 0) := (others => '0');
    signal ready_64            : std_logic := '0';
    signal ready_66            : std_logic := '0';
    signal ready_66_placeolder : std_logic := '0';
    signal ready_66_real       : std_logic := '0';
    signal flag_66to10         : std_logic := '0';
    signal count5_66to10       : std_logic_vector(2 downto 0) := (others => '0');
    signal count6_66to10       : std_logic_vector(2 downto 0) := (others => '0');
    signal count6_maxis5       : std_logic := '0';
    signal data8b_code         : std_logic_vector(1 downto 0) := (others => '0');
    signal flag_code           : std_logic := '0';
    signal flag_code_buf       : std_logic := '0';
    signal flag_code_buf_buf   : std_logic := '0';
    signal caurora_st          : std_logic_vector(3 downto 0) := (others => '0');
    signal caurora_st_b        : std_logic_vector(3 downto 0) := (others => '0');
    signal caurora_fi          : std_logic_vector(3 downto 0) := (others => '0');
    signal caurora_fi_b        : std_logic_vector(3 downto 0) := (others => '0');
    signal data_64_new         : std_logic_vector(63 downto 0) := (others => '0');
    signal data_eof            : std_logic := '0';
    signal ready_64_new        : std_logic := '0';
    signal ready_66_xor        : std_logic := '0';
    signal data_out_b          : std_logic_vector(9 downto 0) := (others => '0');


--signal caurora_dif         : std_logic_vector(3 downto 0) := (others => '0');
begin
    read_en_out <= read_en_out_b;
    data8b_code <= data_in(9 downto 8);
    caurora_st_b <= caurora_st - "0011" when caurora_st > "0010" else "0000";
    caurora_fi_b <= caurora_fi - "0011" when caurora_fi > "0010" else "0000";

    --getting 8 bytes
    proc_8to64 : process (clk, rst)
    begin
        if rst = '1' then
            flag_start        <= '1';
            counter_aurora    <= (others => '0');
            read_en_out_b     <= '0';
            data_64           <= (others => '0');
            data_64_new       <= (others => '0');
            data_64_buf       <= (others => '0');
            ready_64          <= '0';
            ready_64_new      <= '0';
            flag_code_buf     <= '0';
            flag_code_buf_buf <= '0';
            data_eof          <= '0';
            flag_code         <= '0';
            caurora_st        <= (others => '0');
            caurora_fi        <= (others => '0');
        elsif clk'event and clk='1' then
            if counter_aurora /= "0000" then
                if counter_aurora = "1011"  then
                    counter_aurora <= "0000";
                    data_64 <= data_64_buf;
                    if flag_code_buf = '0' then
                        flag_code_buf_buf <= '0';
                    end if;
                    if flag_code_buf_buf = '0' then
                        data_64_new <= (others => '0');
                    else
                        if caurora_st_b /= "0000" then
                            data_64_new(63 downto 64-8*(8-to_integer(unsigned(caurora_st_b)))) <= data_64(8*(8-to_integer(unsigned(caurora_st_b)))-1 downto 0);
                            data_64_new(63-8*(8-to_integer(unsigned(caurora_st_b))) downto 0) <= data_64_buf(63 downto 8*(8-to_integer(unsigned(caurora_st_b))));
                            if flag_code_buf = '0' then
                                data_eof <= '1';
                            else
                                data_eof <= '0';
                            end if;
                        else
                            data_64_new <= data_64;
                        end if;
                        ready_64_new <= '1';
                    end if;
                    ready_64 <= '1';
                else
                    data_eof <= '0';
                    counter_aurora <= counter_aurora + "0001";
                    if flag_code = '0' and data8b_code = "00" then
                        flag_code <= '1';
                        caurora_st <= counter_aurora;
                    elsif flag_code = '1' and data8b_code /= "00" then
                        flag_code <= '0';
                        --caurora_fi <= counter_aurora;
                        if counter_aurora > caurora_st then
                            caurora_fi <= counter_aurora - caurora_st;
                        elsif counter_aurora < caurora_st then
                            caurora_fi <= caurora_st - counter_aurora;
                        else
                            caurora_fi <= "0000";
                        end if;
                    end if;
                    if flag_code = '1' and flag_code_buf = '0' and  counter_aurora = caurora_st then
                        flag_code_buf <= '1';
                        flag_code_buf_buf <= '1';
                    elsif flag_code = '0' and flag_code_buf = '1' and counter_aurora = caurora_fi then
                        flag_code_buf <= '0';
                    end if;
                    if counter_aurora = "0011"  then    --3
                        data_64_buf(63 downto 56) <= data_in(7 downto 0);
                    elsif counter_aurora = "0100"  then --4
                        data_64_buf(55 downto 48) <= data_in(7 downto 0);
                    elsif counter_aurora = "0101"  then --5
                        data_64_buf(47 downto 40) <= data_in(7 downto 0);
                    elsif counter_aurora = "0110"  then --6
                        data_64_buf(39 downto 32) <= data_in(7 downto 0);
                    elsif counter_aurora = "0111"  then --7
                        data_64_buf(31 downto 24) <= data_in(7 downto 0);
                    elsif counter_aurora = "1000"  then --8
                        data_64_buf(23 downto 16) <= data_in(7 downto 0);
                        read_en_out_b <='0';
                    elsif counter_aurora = "1001"  then --9
                        data_64_buf(15 downto  8) <= data_in(7 downto 0);
                    elsif counter_aurora = "1010"  then --10
                        data_64_buf( 7 downto  0) <= data_in(7 downto 0);
                    --          elsif  counter_aurora = "0001"  then
                    --            flag_code_buf <= flag_code;
                    end if;
                end if;
            else
                ready_64 <= '0';
                ready_64_new <= '0';
                data_eof <= '0';
                --flag_code
                if (read_en_in = '1' and get_data = '1') or flag_start = '1' then
                    read_en_out_b <='1';
                    counter_aurora <= counter_aurora + "0001";
                    flag_start <= '0';
                end if;
            end if;
        end if;
    end process proc_8to64;

    --6466b enconder
    encoder64b66b_placeholder: entity work.encoder64b66b_placeholder
        port map (
            clk       => clk,
            data_in   => data_64_new,
            ready_in  => ready_64,
            count     => count6_66to10,
            data_out  => data_66_placeolder,
            ready_out => ready_66_placeolder
        );

    encoder64b66b: entity work.encoder64b66b
        port map (
            clk               => clk,
            rst               => rst,
            data_in           => data_64_new,
            data_in_rdy       => ready_64_new,
            data_in_rdy_idl   => ready_64,
            data_in_valid     => caurora_fi(2 downto 0),
            data_in_code      => data8b_code,
            data_in_eof       => data_eof,
            count_66to10      => count6_66to10,
            data_out          => data_66_real,
            data_out_rdy      => ready_66_real
        );

    data_66 <= data_66_real;
    ready_66 <= ready_66_real;
    --    data_66 <= data_66_placeolder;
    --    ready_66 <= ready_66_placeolder;
    ready_66_xor <= ready_66_placeolder xor ready_66;

    --66 bits to 10 bits output
    --66 56 46 36 26 16     --6 left
    --72 62 52 42 32 22 12  --2 left
    --68 58 48 38 28 18     --8 left
    --74 64 54 44 34 24 14  --4 left
    --70 60 50 40 30 20 10  --0 left
    --repeat
    proc_66to10 : process (clk, rst, ready_66)
    begin
        if rst = '1' then
            get_data      <= '0';
            flag_66to10   <= '0';
            data_66_buf   <= (others => '0');
            count5_66to10 <= (others => '0');
            count6_66to10 <= (others => '0');
            count6_maxis5 <= '0';
            data_out_b    <= (others => '0');
        else
            if ready_66'event and ready_66='1' then
                flag_66to10 <= '1';
                if count5_66to10 = "100" then --4
                    count5_66to10 <= "000";
                    data_66_buf(73 downto 70) <= data_66_buf( 3 downto 0);
                    data_66_buf(69 downto  4) <= data_66;
                    count6_maxis5 <= '0';
                else
                    count5_66to10 <= count5_66to10 + "001";
                    if count5_66to10 = "000" then --0
                        data_66_buf(73 downto 8) <= data_66; --73 8
                        count6_maxis5 <= '1';
                    elsif count5_66to10 = "001" then --1
                        data_66_buf(73 downto 68) <= data_66_buf(13 downto 8);
                        data_66_buf(67 downto  2) <= data_66;
                        count6_maxis5 <= '0';
                    elsif count5_66to10 = "010" then --2
                        data_66_buf(73 downto 72) <= data_66_buf( 3 downto 2);
                        data_66_buf(71 downto  6) <= data_66;
                        count6_maxis5 <= '1';
                    elsif count5_66to10 = "011" then --3
                        data_66_buf(73 downto 66) <= data_66_buf(13 downto 6);
                        data_66_buf(65 downto  0) <= data_66;
                        count6_maxis5 <= '0';
                    end if;
                end if;
            end if;
            if clk'event and clk='1' then
                if flag_66to10 = '1' and read_en_in = '1' then
                    if count6_66to10 = "000" then --0
                        data_out_b <= data_66_buf(73 downto 64);
                        count6_66to10 <= count6_66to10 + "001";
                        get_data <= '0';
                    elsif count6_66to10 = "001" then --1
                        data_out_b <= data_66_buf(63 downto 54);
                        count6_66to10 <= count6_66to10 + "001";
                        get_data <= '0';
                    elsif count6_66to10 = "010" then  --2
                        data_out_b <= data_66_buf(53 downto 44);
                        count6_66to10 <= count6_66to10 + "001";
                        get_data <= '0';
                    elsif count6_66to10 = "011" then  --3
                        data_out_b <= data_66_buf(43 downto 34);
                        count6_66to10 <= count6_66to10 + "001";
                        get_data <= '0';
                    elsif count6_66to10 = "100" then  --4
                        data_out_b <= data_66_buf(33 downto 24);
                        count6_66to10 <= count6_66to10 + "001";
                        get_data <= '0';
                    elsif count6_66to10 = "101" then  --5
                        data_out_b <= data_66_buf(23 downto 14);
                        if count6_maxis5 = '1' then
                            count6_66to10 <= "000";
                            get_data <= '1';
                        else
                            count6_66to10 <= count6_66to10 + "001";
                            get_data <= '0';
                        end if;
                    elsif count6_66to10 = "110" then  --6
                        data_out_b <= data_66_buf(13 downto 4);
                        count6_66to10 <= "000";
                        get_data <= '1';
                    end if;
                end if;
            end if;
        end if;
    end process proc_66to10;

    data_out <= data_out_b;
end Behavioral;

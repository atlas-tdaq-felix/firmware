--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:  John Anderson, Michael Oberling, Soo Ryu
--
-- Design Name:  ip_lib
-- Version:    1.0
-- Date:    9/13/2017
--
-- Description:  Coming soon.
--
-- Change Log:  V1.0 -
--
--==============================================================================

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use ieee.math_real.all;

package ip_lib is
    component clk_wiz_100_0
        port
	(-- Clock in ports
            clk_100_xtal_in_p         : in     std_logic;
            clk_100_xtal_in_n         : in     std_logic;
            -- Clock out ports
            clk40_xtal          : out    std_logic;
            clk_100_xtal_out          : out    std_logic;
            -- Status and control signals
            locked            : out    std_logic
        );
    end component;

    COMPONENT lane_ila

        PORT (
            clk : IN STD_LOGIC;



            probe0 : IN STD_LOGIC_VECTOR(119 DOWNTO 0);
            probe1 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
            probe2 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
            probe3 : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
            probe4 : IN STD_LOGIC_VECTOR(119 DOWNTO 0);
            probe5 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
            probe6 : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
            probe7 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
            probe8 : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
            probe9 : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
            probe10 : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
            probe11 : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
            probe12 : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
            probe13 : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
            probe14 : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
            probe15 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            probe16 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            probe17 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            probe18 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            probe19 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            probe20 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
            probe21 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            probe22 : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
            probe23 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
            probe24 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
            probe25 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
            probe26 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
            probe27 : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
            probe28 : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
            probe29 : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
            probe30 : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
            probe31 : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
            probe32 : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
            probe33 : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
            probe34 : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
            probe35 : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
            probe36 : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
            probe37 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
            probe38 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
            probe39 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
            probe40 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
            probe41 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
            probe42 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
            probe43 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
            probe44 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)

        );
    END COMPONENT  ;


    COMPONENT epath_fifo
        PORT (
            clk : IN STD_LOGIC;
            --MT asynch rst not supported for ultrascale
            --    rst : IN STD_LOGIC;
            srst : IN STD_LOGIC;
            din : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
            wr_en : IN STD_LOGIC;
            rd_en : IN STD_LOGIC;
            dout : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
            full : OUT STD_LOGIC;
            empty : OUT STD_LOGIC;
            valid : OUT STD_LOGIC;
            prog_full : OUT STD_LOGIC
        );
    END COMPONENT;

    COMPONENT gbt_word_domain_cross_fifo
        PORT (
            rst : IN STD_LOGIC;
            wr_clk : IN STD_LOGIC;
            rd_clk : IN STD_LOGIC;
            din : IN STD_LOGIC_VECTOR(119 DOWNTO 0);
            wr_en : IN STD_LOGIC;
            rd_en : IN STD_LOGIC;
            prog_empty_thresh : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
            dout : OUT STD_LOGIC_VECTOR(119 DOWNTO 0);
            full : OUT STD_LOGIC;
            empty : OUT STD_LOGIC;
            almost_empty : OUT STD_LOGIC;
            prog_empty : OUT STD_LOGIC
        );
    END COMPONENT;

    COMPONENT gth_word_domain_cross_fifo is
        PORT (
            rst : IN STD_LOGIC;
            wr_clk : IN STD_LOGIC;
            rd_clk : IN STD_LOGIC;
            din : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
            wr_en : IN STD_LOGIC;
            rd_en : IN STD_LOGIC;
            prog_empty_thresh : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
            dout : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
            full : OUT STD_LOGIC;
            empty : OUT STD_LOGIC;
            almost_empty : OUT STD_LOGIC;
            prog_empty : OUT STD_LOGIC
        );
    END COMPONENT gth_word_domain_cross_fifo;

    COMPONENT dsp_counter
        PORT (
            CLK : IN STD_LOGIC;
            CE : IN STD_LOGIC;
            SCLR : IN STD_LOGIC;
            UP : IN STD_LOGIC;
            LOAD : IN STD_LOGIC;
            L : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
            Q : OUT STD_LOGIC_VECTOR(47 DOWNTO 0)
        );
    END COMPONENT;
end package ip_lib;

package body ip_lib is
end package body ip_lib;

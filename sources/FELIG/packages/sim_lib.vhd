--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:  Michael Oberling
--
-- Design Name:  sim_lib
-- Version:    1.0
-- Date:    10/1/2018
--
-- Description:  Package file for all simulation only items.
--
-- Change Log:  V1.0 -
--
--==============================================================================

library ieee;
    use IEEE.STD_LOGIC_1164.ALL;

    use work.FELIX_gbt_package.all;
    use work.pcie_package.all;
    use work.FELIX_package.all; --MT added

package sim_lib is
    -- orginaly defined in FELIX_gbt_wrapper_KCU.vhd
    type data20barray is array (0 to 47) of std_logic_vector(19 downto 0);

    component SIM_FELIX_gbt_wrapper_KCU is
        generic (
            GBT_NUM                     : integer := 24
        );
        port (
            RX_FLAG_O                   : out std_logic_vector(GBT_NUM-1 downto 0);
            TX_FLAG_O                   : out std_logic_vector(GBT_NUM-1 downto 0);
            rst_hw                      : in std_logic;
            register_map_control        : in register_map_control_type;
            register_map_gbt_monitor    : out register_map_link_monitor_type;
            clk40_in                    : in std_logic;
            clk240_in                   : in std_logic;
            TX_120b_in                  : in  array_120b(0 to GBT_NUM-1);
            RX_120b_out                 : out array_120b(0 to GBT_NUM-1);
            FRAME_LOCKED_O              : out std_logic_vector(GBT_NUM-1 downto 0);
            TX_FRAME_CLK_I              : in std_logic_vector(GBT_NUM-1 downto 0);
            TX_DATA_20b    : out data20barray := (others => ("00000000000000000000"));--*SIM
            RX_DATA_20b    : in  data20barray := (others => ("00000000000000000000"))--*SIM
        );
    end component SIM_FELIX_gbt_wrapper_KCU;

    type sim_register_map_control_type is record
        GBT_GENERAL_CTRL               : std_logic_vector(63 downto 0);   -- Alignment chk reset (not self clearing)
        GBT_GTTX_RESET                 : std_logic_vector(47 downto 0);   -- GTTX_RESET [47:0]
        GBT_GTRX_RESET                 : std_logic_vector(47 downto 0);   -- GTRX_RESET [47:0]
        GBT_PLL_RESET                  : bitfield_gbt_pll_reset_w_type;
        GBT_TX_TC_DLY_VALUE1           : std_logic_vector(47 downto 0);   -- TX_TC_DLY_VALUE [47:0]
        GBT_TX_TC_DLY_VALUE2           : std_logic_vector(47 downto 0);   -- TX_TC_DLY_VALUE [95:48]
        GBT_TX_TC_DLY_VALUE3           : std_logic_vector(47 downto 0);   -- TX_TC_DLY_VALUE [143:96]
        GBT_TX_TC_DLY_VALUE4           : std_logic_vector(47 downto 0);   -- TX_TC_DLY_VALUE [191:144]
        GBT_DATA_TXFORMAT1             : std_logic_vector(47 downto 0);   -- DATA_TXFORMAT [47:0]
        GBT_DATA_TXFORMAT2             : std_logic_vector(47 downto 0);   -- DATA_TXFORMAT [95:48]
        GBT_DATA_RXFORMAT1             : std_logic_vector(47 downto 0);   -- DATA_RXFORMAT [47:0]
        GBT_DATA_RXFORMAT2             : std_logic_vector(47 downto 0);   -- DATA_RXFORMAT [95:0]
        GBT_TX_RESET                   : std_logic_vector(47 downto 0);   -- TX Logic reset [47:0]
        GBT_RX_RESET                   : std_logic_vector(47 downto 0);   -- RX Logic reset [47:0]
        GBT_TX_TC_METHOD               : std_logic_vector(47 downto 0);   -- TX time domain crossing method [47:0]
        GBT_OUTMUX_SEL                 : std_logic_vector(47 downto 0);   -- Descrambler output MUX selection [47:0]
        GBT_TC_EDGE                    : std_logic_vector(47 downto 0);   -- Sampling edge selection for TX domain crossing [47:0]
        GBT_MODE_CTRL                  : bitfield_gbt_mode_ctrl_w_type;
    end record;


end sim_lib;


package body sim_lib is

end sim_lib;

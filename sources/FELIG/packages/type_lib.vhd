--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!               Ricardo Luz
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:  John Anderson, Michael Oberling, Soo Ryu
--
-- Design Name:  type_lib
-- Version:    1.0
-- Date:    9/13/2017
--
-- Description:  Coming soon.
--
-- Change Log:  V1.0 -
--
--==============================================================================

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use ieee.math_real.all;

    use work.FELIX_package.all;
    use work.pcie_package.all;

package type_lib is
    type t_REGISTER_CONFIG is record
        address        : std_logic_vector(11 downto 0);
        reset_value      : std_logic_vector(31 downto 0);
        read_mask      : std_logic_vector(31 downto 0);
        write_mask      : std_logic_vector(31 downto 0);
        fan_in_group    : integer;
    end record;

    type t_REGISTER_DESIGN_CONFIG is record
        use_shadow_registers  : boolean;
        use_write_masking    : boolean;
        use_address_remap    : boolean;
        max_int_address_width  : integer;
        fan_in_sel_width    : integer;
        address_width      : integer;
        address_step      : integer;
    end record;

    type array_of_slv_1_0  is array (integer range <>) of std_logic_vector( 1 downto 0);
    type array_of_slv_2_0  is array (integer range <>) of std_logic_vector( 2 downto 0);
    type array_of_slv_3_0  is array (integer range <>) of std_logic_vector( 3 downto 0);
    type array_of_slv_4_0  is array (integer range <>) of std_logic_vector( 4 downto 0);
    type array_of_slv_6_0  is array (integer range <>) of std_logic_vector( 6 downto 0);
    type array_of_slv_7_0   is array (integer range <>) of std_logic_vector( 7 downto 0);
    type array_of_slv_8_0   is array (integer range <>) of std_logic_vector( 8 downto 0);
    type array_of_slv_9_0   is array (integer range <>) of std_logic_vector( 9 downto 0);
    type array_of_slv_11_0   is array (integer range <>) of std_logic_vector(11 downto 0);
    type array_of_slv_13_0   is array (integer range <>) of std_logic_vector(13 downto 0);
    type array_of_slv_14_0  is array (integer range <>) of std_logic_vector(14 downto 0);
    type array_of_slv_15_0   is array (integer range <>) of std_logic_vector(15 downto 0);
    type array_of_slv_17_0   is array (integer range <>) of std_logic_vector(17 downto 0);
    --MT
    type array_of_slv_10_0_MT  is array (integer range <>) of std_logic_vector(10 downto 0);
    type array_of_slv_11_0_MT  is array (integer range <>) of std_logic_vector(11 downto 0);
    --
    type array_of_slv_19_0   is array (integer range <>) of std_logic_vector(19 downto 0);
    type array_of_slv_16_0  is array (integer range <>) of std_logic_vector(16 downto 0);
    type array_of_slv_20_0  is array (integer range <>) of std_logic_vector(20 downto 0);
    type array_of_slv_21_0  is array (integer range <>) of std_logic_vector(21 downto 0);
    type array_of_slv_23_0  is array (integer range <>) of std_logic_vector(23 downto 0);
    type array_of_slv_31_0   is array (integer range <>) of std_logic_vector(31 downto 0);
    type array_of_slv_35_0   is array (integer range <>) of std_logic_vector(35 downto 0);
    type array_of_slv_39_0   is array (integer range <>) of std_logic_vector(39 downto 0);
    type array_of_slv_47_0  is array (integer range <>) of std_logic_vector(47 downto 0);
    type array_of_slv_79_0   is array (integer range <>) of std_logic_vector(79 downto 0);
    type array_of_slv_87_0   is array (integer range <>) of std_logic_vector(87 downto 0);
    type array_of_slv_111_0  is array (integer range <>) of std_logic_vector(111 downto 0);
    type array_of_slv_119_0  is array (integer range <>) of std_logic_vector(119 downto 0);
    type array_of_slv_224_0  is array (integer range <>) of std_logic_vector(224 downto 0);
    type array_of_slv_223_0  is array (integer range <>) of std_logic_vector(223 downto 0);

    type array_of_array_0_2_slv_9_0 is array (integer range <>) of array_of_slv_9_0( 0 to 2);
    type array_of_array_0_39_slv_1_0 is array (integer range <>) of array_of_slv_1_0(0 to 39);
    --type array_of_array_0_7_slv_2_0 is array (integer range <>) of array_of_slv_2_0(0 to 7);
    type array_of_array_0_39_slv_2_0 is array (integer range <>) of array_of_slv_2_0(0 to 39);
    type array_of_array_0_111_slv_2_0 is array (integer range <>) of array_of_slv_2_0(0 to 111);

    type array_of_array_0_3_slv_31_0 is array (integer range <>) of array_of_slv_31_0(0 to 3);

    type bitfield_felig_data_gen_config_w_type_array  is array (integer range <>) of bitfield_felig_data_gen_config_w_type;
    type bitfield_felig_elink_config_w_type_array    is array (integer range <>) of bitfield_felig_elink_config_w_type;
    type bitfield_felig_lane_config_w_type_array    is array (integer range <>) of bitfield_felig_lane_config_w_type;

    type lane_gbt_monitor is record
        frame_locked  : std_logic;
        rx_is_header  : std_logic;
        rx_header_found  : std_logic;
        error      : std_logic;
        rxslide_count  : std_logic_vector(31 downto 0);
    end record lane_gbt_monitor;

    type lane_gth_monitor is record
        txreset_done  : std_logic;
        rxreset_done  : std_logic;
        txfsmreset_done  : std_logic;
        rxfsmreset_done  : std_logic;
    end record lane_gth_monitor;

    type lane_emulator_control is record
        pattern_select  : std_logic_vector(1 downto 0);
        data_format    : std_logic_vector(1 downto 0);
        sw_busy      : std_logic;
        reset      : std_logic;
        output_width  : std_logic_vector(2 downto 0);
        chunk_length  : std_logic_vector(15 downto 0);
        userdata    : std_logic_vector(15 downto 0);
        pack_gen_select  : std_logic;
        nsw_even_parity  : std_logic;
    end record lane_emulator_control;

    type lane_emulator_control_array  is array (integer range <>) of lane_emulator_control;

    type lane_elink_control is record
        output_width  : std_logic_vector(2 downto 0);    -- bit per e-link
        input_width    : std_logic;            -- bit per e-link
        endian_mode    : std_logic;            -- bit per e-link
        enable      : std_logic;            -- bit per e-link
    end record lane_elink_control;

    type lane_elink_control_array  is array (integer range <>) of lane_elink_control;

    type lane_global_control is record
        lane_reset      : std_logic;
        framegen_reset    : std_logic;
        elink_sync      : std_logic;
        framegen_data_select: std_logic;
        emu_data_select    : std_logic;
        l1a_source      : std_logic;
        loopback_fifo_delay  : std_logic_vector(4 downto 0);
        loopback_fifo_reset  : std_logic;
        a_ch_bit_sel    : std_logic_vector(6 downto 0);
        b_ch_bit_sel    : std_logic_vector(6 downto 0);
        l1a_counter_reset   : std_logic;
        MSB                 : std_logic;
        l1a_max_count       : std_logic_vector(27 downto 0);
        aligned             : std_logic;
        --PROTOCOL            : std_logic;
        FEC                 : std_logic;
        DATARATE            : std_logic;
    end record lane_global_control;

    type lane_emu_random_control is record
        FMEMU_RANDOM_RAM_ADDR   : std_logic_vector(9 downto 0);    -- Controls the address of the ramblock for the random number generator
        FMEMU_RANDOM_RAM        : bitfield_fmemu_random_ram_t_type;
        FMEMU_RANDOM_CONTROL    : bitfield_fmemu_random_control_w_type;
    end record lane_emu_random_control;

    type lane_global_monitor is record
        ttc_mon        : TTC_data_type;
        l1a_id        : std_logic_vector(31 downto 0);
        fc_error_count    : std_logic_vector(31 downto 0);
    end record lane_global_monitor;

    type lane_clock_monitor is record
        freq_rx_clk  : std_logic_vector(31 downto 0);
        freq_tx_clk  : std_logic_vector(31 downto 0);
        picxo_error  : std_logic_vector(20 downto 0);
        picxo_volt  : std_logic_vector(21 downto 0);
    end record lane_clock_monitor;

    type lane_control_type is record
        global    : lane_global_control;
        emulator  : lane_emulator_control_array(0 to 6);
        elink    : lane_elink_control_array(0 to 111);
        fmemu_random : lane_emu_random_control;
    end record lane_control_type;

    type lane_monitor_type is record
        global  : lane_global_monitor;
        clock  : lane_clock_monitor;
        gbt    : lane_gbt_monitor;
        gth    : lane_gth_monitor;
    end record lane_monitor_type;

    type array_of_lane_control_type  is array (integer range <>) of lane_control_type;
    type array_of_lane_monitor_type  is array (integer range <>) of lane_monitor_type;

end package type_lib;

package body type_lib is
end package body type_lib;

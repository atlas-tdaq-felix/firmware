--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:  Michael Oberling
--
-- Design Name:  clock_frequncy_counter
-- Version:    1.0
-- Date:    9/13/2017
--
-- Description:  Coming soon.
--
-- Change Log:  V1.0 -
--
--==============================================================================

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use ieee.math_real.all;

entity clock_frequncy_counter is
    generic (
        -- Units: Hz
        clk_timebase_frequency    : real := 10000000.0;

        -- Units: Seconds
        accumulation_period      : real := 1.0;
        -- clk_meas_prescale divides down the clk_meas frequency
        -- (clk_meas freq) / (2^clk_meas_prescale_factor)  must be less than (clk_timebase freq) / 2
        -- values other than 1 will limit resolution.
        -- Minimum value = 1
        clk_meas_prescale_factor  : integer := 1
    );
    -- Port list
    port (
        -- stable local clock input
        -- must be at least twice as fast at the clk_meas / clk_meas_prescale
        clk_timebase      : in  std_logic;

        -- clk to measure
        clk_meas        : in  std_logic;

        -- output count
        frequency        : out std_logic_vector(31 downto 0);

        div_2_clock_signal    : out std_logic;
        prescaled_clock_signal  : out std_logic
    );
end entity clock_frequncy_counter;

architecture RTL of clock_frequncy_counter is
    constant  meas_period          : real := accumulation_period * clk_timebase_frequency;
    constant  meas_period_counter_width  : integer := integer(floor(log2(meas_period))) + 1;
    constant  meas_period_reset_count    : std_logic_vector(meas_period_counter_width - 1 downto 0) := std_logic_vector(to_unsigned(integer(meas_period), meas_period_counter_width));

    signal clk_meas_toggle          : std_logic              := '0';
    signal clk_meas_toggle_detection_pipe  : std_logic_vector(1 downto 0)    := (others => '0');
    signal count              : std_logic_vector(frequency'range)  := (others => '0');
    signal count_enable            : std_logic              := '0';
    signal count_latch            : std_logic              := '0';
    signal meas_period_count        : std_logic_vector(meas_period_counter_width - 1 downto 0) := (others => '0');
    signal prescale_count          : std_logic_vector(clk_meas_prescale_factor-1 downto 0) := (others => '0');

begin
    -- input clock divider
    clk_meas_prescaler : process (clk_meas)
    begin
        if (clk_meas'event and clk_meas = '1') then
            prescale_count <= prescale_count + 1;
            div_2_clock_signal <= prescale_count(0);
            if (prescale_count = 0) then
                prescaled_clock_signal <= not clk_meas_toggle;
                clk_meas_toggle <= not clk_meas_toggle;
            end if;
        end if;
    end process clk_meas_prescaler;

    -- domain cross between clk_meas and clk_timebase occurs here
    clk_meas_toggle_detector : process (clk_timebase)
    begin
        if (clk_timebase'event and clk_timebase = '1') then
            clk_meas_toggle_detection_pipe(0) <= clk_meas_toggle;
            clk_meas_toggle_detection_pipe(1) <= clk_meas_toggle_detection_pipe(0);

            if (clk_meas_toggle_detection_pipe(0) /= clk_meas_toggle_detection_pipe(1)) then
                count_enable <= '1';
            else
                count_enable <= '0';
            end if;
        end if;
    end process clk_meas_toggle_detector;

    -- counter process
    frequency_counter : process (clk_timebase)
    begin
        if (clk_timebase'event and clk_timebase = '1') then
            if (count_latch = '1') then
                frequency <= count;

                if (count_enable = '1') then
                    count(0) <= '1';
                    count(count'high downto 1) <= (others => '0');
                else
                    count <= (others => '0');
                end if;
            elsif (count_enable = '1') then
                count <= count + 1;
            end if;
        end if;
    end process frequency_counter;

    meas_period_counter : process(clk_timebase)
    begin
        if (clk_timebase'event and clk_timebase = '1') then
            if(meas_period_count = 0) then
                count_latch <= '1';                -- Latch and
                meas_period_count <= meas_period_reset_count;  -- reset counter
            else
                count_latch <= '0';
                meas_period_count <= meas_period_count - 1;    -- Decrement
            end if;
        end if;
    end process meas_period_counter;
end architecture RTL;

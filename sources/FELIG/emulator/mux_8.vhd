--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:  Michael Oberling
--
-- Design Name:  mux_8
-- Version:    1.0
-- Date:    9/13/2017
--
-- Description:  Coming soon.
--
-- Change Log:  V1.0 -
--
--==============================================================================

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use ieee.math_real.all;

library UNISIM;
    use UNISIM.Vcomponents.ALL;

--Library XilinxCoreLib;

entity mux_8 is
    port (
        bit_input      : in  std_logic_vector( 7 downto 0);
        bit_select      : in  std_logic_vector( 2 downto 0);

        bit_output      : out  std_logic;
        bit_output_local  : out  std_logic
    );
end entity mux_8;

architecture rtl of mux_8 is
    signal mux_4_output  : std_logic_vector( 1 downto 0)  := (others => '0');
begin
    gen_mux_4 : for i in mux_4_output'range generate
        -- For each of 4 LUTs with in a slice create a 4:1 MUX.
        mux_4_output(i) <=  bit_input(i*4+0) when (bit_select(1 downto 0) = "00") else
                           bit_input(i*4+1) when (bit_select(1 downto 0) = "01") else
                           bit_input(i*4+2) when (bit_select(1 downto 0) = "10") else
                           bit_input(i*4+3) when (bit_select(1 downto 0) = "11") else
                           '0';
    end generate gen_mux_4;

    MUXF7_comp : MUXF7_D --@suppress
        port map (
            LO  => bit_output_local,  -- Ouptut of MUX to local routing
            O  => bit_output,      -- Output of MUX to general routing
            I0  => mux_4_output(0),    -- Input (tie to LUT6 O6 pin)
            I1  => mux_4_output(1),    -- Input (tie to LUT6 O6 pin)
            S  => bit_select(2)    -- Input select to MUX
        );
end rtl;

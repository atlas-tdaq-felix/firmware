--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Shelfali Saxena
--!               mtrovato
--!               Ricardo Luz
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineeer: Initially created as FELIG_lane_wrapper for HG710 by John Anderson, Michael Oberling, Soo Ryu
-- modified for FLX-712 by Marco Trovato
--
-- Design Name:  Emulator
-- Version:    1.0
-- Date:    9/13/2017
--
-- Description:  Coming soon.
--
-- Change Log:  V1.0 -
--
--==============================================================================

--==============
LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.type_lib.ALL;
    use work.ip_lib.ALL;
--use work.function_lib.ALL;

    use work.pcie_package.all;
    use work.FELIX_package.all;

Library xpm;
    use xpm.vcomponents.all;

entity Emulator is
    generic (
        useGBTdataEmulator      : boolean  := false;
        sim_emulator            : boolean  := false;
        LANE_ID                 : integer   := 0;
        NUMELINKmax             : integer := 112;
        NUMEGROUPmax            : integer := 7;
        PROTOCOL                : std_logic := '0'
    );
    port (
        clk40                   : in std_logic;
        lane_rxclk              : in std_logic;
        lane_txclk              : in std_logic;
        l1a_int_trigger         : in std_logic;
        l1a_int_int_id          : in std_logic_vector(15 downto 0);
        l1a_trigger_out         : out std_logic;
        gbt_tx_data_228b_out    : out std_logic_vector(227 downto 0);
        data_ready_tx_out       : out std_logic; --to link_wrapper_fifo
        gbt_rx_data_120b_in     : in std_logic_vector(119 downto 0);
        gbt_tx_flag_in          : in std_logic;
        gbt_rx_flag_in          : in std_logic;
        lane_control            : in lane_control_type;
        lane_monitor            : out lane_monitor_type

    );
end entity Emulator;

architecture Behavioral of Emulator is
    constant epath                          : std_logic_vector(4 downto 0) := (others => '0');

    signal ext_l1a_id                       : std_logic_vector( 15 downto 0)  := (others => '0');
    signal ext_l1a_select                   : std_logic              := '0';
    signal ext_l1a_trigger                  : std_logic              := '0';
    signal gbt_extracted_bchan              : std_logic              := '0';
    signal gbt_extractedl1a                 : std_logic              := '0';
    signal l1a_int_trigger_clktx            : std_logic              := '0';
    signal l1a_int_trigger_clktx_onecycle   : std_logic              := '0';
    signal l1a_int_trigger_clktx_delay      : std_logic              := '0';
    signal link_aligned                     : std_logic              := '0';
    signal gbt_frame_locked_a               : std_logic_vector( 47 downto 0)  := (others => '0');
    signal gbt_frame_locked_c               : std_logic              := '0';
    signal gbt_rx_data_120b                 : std_logic_vector(119 downto 0)  := (others => '0');
    signal gbt_rx_flag                      : std_logic              := '0';
    signal gbt_tx_flag                      : std_logic              := '0';
    signal gbtbitsel                        : std_logic_vector(  6 downto 0)  := (others => '0');
    signal b_ch_bit_sel                     : std_logic_vector(  6 downto 0)  := (others => '0');
    signal l1a_id                           : std_logic_vector( 15 downto 0)  := (others => '0');
    signal l1a_trigger                      : std_logic              := '0';
    signal l1id                             : std_logic_vector( 15 downto 0)  := (others => '0');
    signal l1a_ext_int_id                   : std_logic_vector( 15 downto 0)  := (others => '0'); --MT
    signal l1a_ext_int_id_extra             : std_logic_vector( 31 downto 0)  := (others => '0'); --MT
    signal l1a_int_int_id_tx_clk            : std_logic_vector( 15 downto 0)  := (others => '0');
    signal elink_sync                       : std_logic              := '0';
    signal elink_sync_reg                   : std_logic;
    signal emu_data_re                      : std_logic_vector(0 to NUMEGROUPmax-1); --(0 to  4);
    signal emu_data_out                     : array_of_slv_9_0(0 to NUMEGROUPmax-1); --(0 to  4);
    signal elink_data_in                    : array_of_slv_9_0(0 to NUMELINKmax-1);
    signal elink_data_re                    : std_logic_vector(0 to NUMELINKmax-1) ;
    signal elink_control                    : lane_elink_control_array(0 to NUMELINKmax-1);
    signal emu_control                      : lane_emulator_control_array(0 to NUMEGROUPmax-1); --(4 downto 0);
    signal fmemu_random                     : lane_emu_random_control;
    signal TTC_out                          : std_logic_vector(9 downto 0) := (others => '0');
    signal payload_225b_ph2                 : std_logic_vector(224 downto  0)  := (others => '0');
    signal payload_225b_pipe_ph2            : array_of_slv_224_0(0 to 1);
    signal HDR_4b                           : std_logic_vector(3 downto 0);
    signal HDR_4b_pipe                      : array_of_slv_3_0(0 to 1);
    signal IC_2b                            : std_logic_vector(1 downto 0);
    signal IC_2b_pipe                       : array_of_slv_1_0(0 to 1);
    signal EC_2b                            : std_logic_vector(1 downto 0);
    signal EC_2b_pipe                       : array_of_slv_1_0(0 to 1);
    signal FEC_48b                          : std_logic_vector(47 downto 0);
    signal FEC_48b_pipe                     : array_of_slv_47_0(0 to 1);
    signal MSBfirst                         : std_logic := '0';
    signal l1a_counter_reset                : std_logic;
    --signal FEC                              : std_logic;
    signal DATARATE                         : std_logic;
    signal l1a_ext_counter_reset            : std_logic := '0';
    signal flag_ext_counter_reset           : std_logic := '0';
    signal ext_counter_l1a_trigger          : std_logic := '0';
    signal ECR                              : std_logic := '0';
    signal ttc_fifo_rst                     : std_logic := '1';
    signal ttc_fifo_wr_en                   : std_logic := '0';
    signal ttc_fifo_rd_en                   : std_logic := '0';
    signal ttc_fifo_full                    : std_logic := '0';
    signal ttc_fifo_empty                   : std_logic := '0';
    signal ttc_fifo_din, ttc_fifo_dout      : std_logic_vector(9 downto 0) := (others => '0');
    signal TTC_out_fifo                     : std_logic_vector(9 downto 0) := (others => '0');
    signal TTC_out_fifo_dly                 : std_logic_vector(9 downto 0) := (others => '0');
    signal lane_reset                       : std_logic := '0';
    signal egroup_enable                    : std_logic_vector(0 to NUMEGROUPmax-1);
    signal flag_data_gen                    : std_logic_vector(0 to NUMEGROUPmax-1);
    signal out_of_sync                      : std_logic_vector(NUMEGROUPmax-1 downto 0) := (others => '0');
    signal or_out_of_sync                   : std_logic;
    signal flag_sync                        : std_logic_vector(0 to NUMEGROUPmax-1);
    signal start_dsp_flag                   : std_logic := '0'; -- so dsp works in sim
    signal start_dsp                        : std_logic := '0'; -- so dsp works in sim

--RL to be implemented
--signal aurora_en                    : std_logic_vector(0 to NUMELINKmax-1);
--signal aurora_en_EGROUP             : std_logic_vector(0 to NUMEGROUPmax-1);
begin

    lane_monitor.global.l1a_id      <= X"0000" & l1a_id;
    lane_monitor.gbt.frame_locked   <= gbt_frame_locked_c;

    l1a_trigger_out                 <= l1a_trigger;

    gbt_rx_data_120b                <= gbt_rx_data_120b_in;
    gbt_rx_flag                     <= gbt_rx_flag_in;  --from gbtTxRx_FELIX
    gbt_tx_flag                     <= gbt_tx_flag_in;  --from gbtTxRx_FELIX

    emu_control                     <= lane_control.emulator;
    elink_control                   <= lane_control.elink;
    fmemu_random                    <= lane_control.fmemu_random;
    link_aligned                    <= lane_control.global.aligned; --(not anymore) clock is correct, GT_RX_WORD_CLK. from alignment_done_f in sources/FELIG/LinkWrapper/FLX_LpGBT_FE_Wrapper_FELIG.vhd
    --FEC                             <= lane_control.global.FEC; -- not used
    DATARATE                        <= lane_control.global.DATARATE; -- XPM not needed, no clocks involved

    xpm_lane_reset : xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => '0',
            src_in => lane_control.global.lane_reset,
            dest_clk => lane_rxclk,
            dest_out => lane_reset
        );

    xpm_ext_l1a_select : xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => '0',
            src_in => lane_control.global.l1a_source,
            dest_clk => lane_txclk,
            dest_out => ext_l1a_select
        );

    xpm_gbtbitsel : xpm_cdc_array_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0,
            WIDTH => 7
        )
        port map(
            src_clk => '0',
            src_in => lane_control.global.a_ch_bit_sel,
            dest_clk => lane_rxclk,
            dest_out => gbtbitsel
        );

    xpm_b_ch_bit_sel : xpm_cdc_array_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0,
            WIDTH => 7
        )
        port map(
            src_clk => '0',
            src_in => lane_control.global.b_ch_bit_sel,
            dest_clk => lane_rxclk,
            dest_out => b_ch_bit_sel
        );

    xpm_MSBfirst : xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => '0',
            src_in => lane_control.global.MSB,
            dest_clk => lane_txclk,
            dest_out => MSBfirst
        );

    xpm_elink_sync : xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => '0',
            src_in => lane_control.global.elink_sync,
            dest_clk => lane_txclk,
            dest_out => elink_sync_reg
        );

    xpm_l1a_counter_reset : xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => '0',
            src_in => lane_control.global.l1a_counter_reset,
            dest_clk => lane_txclk,
            dest_out => l1a_counter_reset
        );

    xpm_l1a_int_trigger : xpm_cdc_single generic map(
            DEST_SYNC_FF => 4,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 1
        )
        port map(
            src_clk => clk40,
            src_in => l1a_int_trigger,
            dest_clk => lane_txclk,
            dest_out => l1a_int_trigger_clktx
        );

    xpm_l1a_int_int_id : xpm_cdc_array_single generic map(
            DEST_SYNC_FF => 4,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 1,
            WIDTH => 16
        )
        port map(
            src_clk => clk40,
            src_in => l1a_int_int_id,
            dest_clk => lane_txclk,
            dest_out => l1a_int_int_id_tx_clk
        );

    --================================================================
    -- Frame Locked monitoring
    --================================================================

    u0_mon: process(lane_rxclk)
    begin
        if rising_edge(lane_rxclk) then
            if (gbt_rx_flag = '1') then
                gbt_frame_locked_a(47 downto 1) <= gbt_frame_locked_a(46 downto 0);
                gbt_frame_locked_a(0) <= link_aligned;
                if gbt_frame_locked_a = x"FFFFFFFFFFFF" then
                    gbt_frame_locked_c <= '1';
                else
                    gbt_frame_locked_c <= '0';
                end if;
            end if;
        end if;
    end process u0_mon;

    --================================================================
    -- L1A Extraction
    --================================================================
    -- External trigger

    --a channel select
    l1a_mux: entity work.mux_128_sync
        port map (
            clk              => lane_rxclk,
            bit_input(127 downto 121)  => "0000000" ,
            bit_input(120)        => '1',
            bit_input(119 downto 0)    => gbt_rx_data_120b,
            bit_select          => gbtbitsel,
            bit_output_sync        => gbt_extractedl1a,
            bit_output_aync        => open
        );

    --b channel select
    bchan_mux: entity work.mux_128_sync
        port map (
            clk              => lane_rxclk,
            bit_input(127 downto 121)  => "0000000" ,
            bit_input(120)        => '1',
            bit_input(119 downto 0)    => gbt_rx_data_120b,
            bit_select          => b_ch_bit_sel,
            bit_output_sync        => gbt_extracted_bchan,
            bit_output_aync        => open
        );

    --ttc decoder
    ttc_wrapper_comp : entity work.ttc_wrapper_felig
        port map (
            a_data_in      => gbt_extractedl1a,
            b_data_in      => gbt_extracted_bchan,
            ttc_strobe      => gbt_rx_flag,
            TTC_out        => TTC_out,
            clk240        => lane_rxclk,
            BUSY        => open,
            L1ID_Bch      => open,
            TTC_ToHost_Data_out  => open
        );

    --clock domain crossing
    ttc_fifo_rst    <= (not link_aligned) or lane_reset;
    ttc_fifo_wr_en  <= '1';
    ttc_fifo_rd_en  <= not ttc_fifo_empty;
    ttc_fifo_din    <= TTC_out;
    TTC_out_fifo    <= ttc_fifo_dout;

    ttc_fifo: xpm_fifo_async
        generic map
    (
            FIFO_MEMORY_TYPE => "auto",
            FIFO_WRITE_DEPTH => 16,
            RELATED_CLOCKS => 0,
            WRITE_DATA_WIDTH => 10,
            READ_MODE => "fwft",
            FIFO_READ_LATENCY => 0,
            FULL_RESET_VALUE => 1,
            USE_ADV_FEATURES => "0000",
            READ_DATA_WIDTH => 10,
            CDC_SYNC_STAGES => 2,
            WR_DATA_COUNT_WIDTH => 1,
            PROG_FULL_THRESH => 10,
            RD_DATA_COUNT_WIDTH => 1,
            PROG_EMPTY_THRESH => 6,
            DOUT_RESET_VALUE => "0",
            ECC_MODE => "no_ecc"
        )
        port map(
            sleep => '0',
            rst => ttc_fifo_rst,
            wr_clk => lane_rxclk,
            wr_en => ttc_fifo_wr_en,
            din => ttc_fifo_din,
            full => ttc_fifo_full,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => open,
            almost_full => open,
            wr_ack => open,
            rd_clk => lane_txclk,
            rd_en => ttc_fifo_rd_en,
            dout => ttc_fifo_dout,
            empty => ttc_fifo_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => open,
            almost_empty => open,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );

    --generates external l1a and final for counting
    ext_l1a_generator: process(lane_txclk)
    begin
        if lane_txclk'event and lane_txclk='1' then
            TTC_out_fifo_dly <= TTC_out_fifo;
            if TTC_out_fifo_dly(0) = '1' and TTC_out_fifo(0) = '0' then
                ext_l1a_trigger         <= '1';
                ext_counter_l1a_trigger <= '0';
                ext_l1a_id              <= l1a_ext_int_id;
            elsif TTC_out_fifo_dly(0) = '0' and TTC_out_fifo(0) = '1' then
                ext_l1a_trigger         <= '0';
                ext_counter_l1a_trigger <= '1';
            else
                ext_l1a_trigger         <= '0';
                ext_counter_l1a_trigger <= '0';
            end if;
        end if;
    end process;

    ECR <= TTC_out_fifo(3);

    --generates load signal for counter
    process(lane_txclk)
    begin
        if lane_txclk'event and lane_txclk='1' then
            if (l1a_counter_reset = '1' or ECR = '1') and flag_ext_counter_reset = '0' then
                l1a_ext_counter_reset <= '1';
                flag_ext_counter_reset <= '1';
            elsif l1a_ext_int_id  = X"0000" then
                l1a_ext_counter_reset <= '0';
                if l1a_counter_reset = '0' and ECR = '0' then
                    flag_ext_counter_reset <= '0';
                end if;
            end if;
            if start_dsp_flag = '0' then
                start_dsp_flag  <= '1';
                start_dsp       <= '1';
            else
                start_dsp       <= '0';
            end if;        end if;
    end process;

    --l1a id counter
    l1a_ext_int_id_counter : dsp_counter
        PORT MAP (
            CLK                => lane_txclk,
            CE                => ext_counter_l1a_trigger or start_dsp,
            SCLR              => '0',
            UP              => '1',
            LOAD              => l1a_ext_counter_reset or start_dsp,
            L                 => X"000000000000",
            Q(47 downto 16)   => l1a_ext_int_id_extra,
            Q(15 downto  0)   => l1a_ext_int_id
        );

    -- Internal trigger
    l1a_generator: process(lane_txclk)
    begin
        if lane_txclk'event and lane_txclk='1' then
            l1a_int_trigger_clktx_delay <= l1a_int_trigger_clktx;
            if l1a_int_trigger_clktx_delay = '0' and l1a_int_trigger_clktx = '1' then
                l1a_int_trigger_clktx_onecycle <= '1';
                l1id <= l1a_int_int_id_tx_clk;
            else
                l1a_int_trigger_clktx_onecycle <= '0';
            end if;
        end if;
    end process;

    -- Trigger select mux

    l1a_ext_mux: process(lane_txclk)
    begin
        --MT    if lane_txclk_240'event and lane_txclk_240='1' then
        if lane_txclk'event and lane_txclk='1' then
            if (ext_l1a_select = '0') then
                l1a_trigger  <= l1a_int_trigger_clktx_onecycle;
                l1a_id    <= l1id;
            else
                l1a_trigger  <= ext_l1a_trigger;
                l1a_id    <= ext_l1a_id;
            end if;
        end if;
    end process;

    --================================================================
    -- Data Emulator
    --================================================================

    --PROTOCOL, DATARATE, FEC. 0.X,X = 4.8 Gbps    (GBT   Phase1),
    --                         1,0.0 = 5.12 FEC5   (lpGBT Phase2),
    --                         1,0.1 = 5.12 FEC12  (lpGBT Phase2),
    --                         1.1,0 = 10.24 FEC5  (lpGBT Phase2),
    --                         1.1,1 = 10.24 FEC12 (lpGBT Phase2)

    gbt_tx_data_228b_out <= x"000000000000000000000000000"       &
                            HDR_4b_pipe(1)                       &
                            IC_2b_pipe(1)                        &
                            EC_2b_pipe(1)                        &
                            payload_225b_pipe_ph2(1)(143 downto 128) &
                            payload_225b_pipe_ph2(1)(111 downto  96) &
                            payload_225b_pipe_ph2(1)( 79 downto  64) &
                            payload_225b_pipe_ph2(1)( 47 downto  32) &
                            payload_225b_pipe_ph2(1)( 15 downto  0) &
                            FEC_48b_pipe(1)(31 downto 0)         when PROTOCOL = '0' else
                            IC_2b_pipe(1)                        &
                            EC_2b_pipe(1)                        &
                            payload_225b_pipe_ph2(1)(223 downto 0); --when lpgbt
    data_ready_tx_out  <= payload_225b_pipe_ph2(1)(224);

    --HEADER IC EC and FEC are hardcoded
    HDR_4b <= "0101"      when PROTOCOL = '0' else "00" & "01";
    IC_2b  <= "11";
    EC_2b  <= "11";
    FEC_48b <= (others=>'0');

    --delaying data
    EMU_DATA_MUX: process(lane_txclk)
    begin
        if lane_txclk'event and lane_txclk='1' then
            payload_225b_pipe_ph2(1) <= payload_225b_pipe_ph2(0);
            payload_225b_pipe_ph2(0) <= payload_225b_ph2;
            HDR_4b_pipe(1)       <= HDR_4b_pipe(0);
            HDR_4b_pipe(0)       <= HDR_4b;
            IC_2b_pipe(1)        <= IC_2b_pipe(0);
            IC_2b_pipe(0)        <= IC_2b;
            EC_2b_pipe(1)        <= EC_2b_pipe(0);
            EC_2b_pipe(0)        <= EC_2b;
            FEC_48b_pipe(1)      <= FEC_48b_pipe(0);
            FEC_48b_pipe(0)      <= FEC_48b;
        end if;
    end process;

    --gbt/lpgbt printer
    elink_muxer_v2:   entity work.gbt_word_printer_v2
        generic map (
            NUMELINKmax         => NUMELINKmax,
            NUMEGROUPmax        => NUMEGROUPmax,
            PROTOCOL            => PROTOCOL
        )
        port map (
            clk                 => lane_txclk,
            elink_sync_reg      => elink_sync_reg,
            elink_control       => elink_control,
            elink_data          => elink_data_in,
            tx_flag             => gbt_tx_flag,
            l1a_trigger         => l1a_trigger,
            MSBfirst            => MSBfirst,
            gbt_payload         => payload_225b_ph2(223 downto 0),
            elink_read_enable   => elink_data_re,
            aurora_en           => (others => '0'),
            data_ready          => payload_225b_ph2(224),
            or_out_of_sync      => or_out_of_sync,
            flag_sync           => flag_sync
        );

    or_out_of_sync <= or_reduce(out_of_sync);

    dgen_group: for i in 0 to NUMEGROUPmax-1 generate
        signal elink_control_enable_array       : std_logic_vector(15 downto 0);
        signal elink_data_re_array              : std_logic_vector(15 downto 0);
        signal elink_data_re_array_0            : std_logic_vector(15 downto 0) := (others => '0');
    begin
        emu_data_re(i)      <= or_reduce(elink_data_re_array);
        egroup_enable(i)    <= or_reduce(elink_control_enable_array);

        generate_arrays : for j in 0 to 15 generate
            xpm_enable_array : xpm_cdc_single generic map(
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0
                )
                port map(
                    src_clk => '0',
                    src_in => elink_control(i*16+j).enable,
                    dest_clk => lane_txclk,
                    dest_out => elink_control_enable_array(j)
                );
            elink_data_re_array(j)            <=  elink_data_re(i*16+j);
        --aurora_en(i*16+j) <= aurora_en_EGROUP(i);  -- Check LATER
        end generate generate_arrays;

        dmap : for j in 0 to 7 generate
            elink_data_in(i*16+j)   <= emu_data_out(i) when egroup_enable(i) = '1' else "0000000000";
            elink_data_in(i*16+j+8) <= emu_data_out(i) when (PROTOCOL = '1' and DATARATE = '1' and egroup_enable(i) = '1') else "0000000000";
        end generate dmap;

        flag_sync(i) <= (not egroup_enable(i)) or flag_data_gen(i);

        --read enable should always be either equal to 0 or the enable vector. If not they are desynced.
        check_read_enables : process(lane_txclk)
        begin
            if lane_txclk'event and lane_txclk ='1' then
                if elink_data_re_array = elink_data_re_array_0 then
                    out_of_sync(i) <= '0';
                elsif elink_data_re_array = elink_control_enable_array then
                    out_of_sync(i) <= '0';
                else
                    out_of_sync(i) <= '1';
                end if;
            end if;
        end process check_read_enables;


        emu_0: entity work.elink_data_emulator
            generic map (
                epath       => epath,
                egroup      => to_std_logic_vector(i,3),
                LANE_ID     => LANE_ID,
                PROTOCOL    => PROTOCOL
            )
            port map (
                clk40               => clk40,  --Frans1
                clk                 => lane_txclk,
                emu_control         => emu_control(i),
                elink_data_out      => emu_data_out(i),
                elink_data_re       => emu_data_re(i),
                l1a_trig            => l1a_trigger,
                l1a_id              => l1a_id,
                flag_data_gen       => flag_data_gen(i),
                fmemu_random        => fmemu_random,
                chunk_length_out    => open
            );

    end generate dgen_group;

end architecture Behavioral;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:  Michael Oberling
--
-- Design Name:  mux_128_sync
-- Version:    1.0
-- Date:    9/13/2017
--
-- Description:  Coming soon.
--
-- Change Log:  V1.0 -
--
--==============================================================================

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use ieee.math_real.all;

library UNISIM;
    use UNISIM.Vcomponents.ALL;

--Library XilinxCoreLib;

entity mux_128_sync is
    port (
        clk          : in  std_logic;
        bit_input      : in  std_logic_vector(127 downto 0);
        bit_select      : in  std_logic_vector(  6 downto 0);

        bit_output_sync    : out  std_logic;
        bit_output_aync    : out  std_logic
    );
end entity mux_128_sync;

architecture rtl of mux_128_sync is
    signal mux_16_output    : std_logic_vector( 7 downto 0)  := (others => '0');
    signal mux_16_output_local  : std_logic_vector( 7 downto 0)  := (others => '0');
    signal mux_16_output_sync  : std_logic_vector( 7 downto 0)  := (others => '0');

    signal mux_128_output    : std_logic            := '0';
    signal mux_128_output_local  : std_logic            := '0';
    signal mux_128_output_sync  : std_logic            := '0';
begin
    -- RANK 1
    -- Each of the 16 blocks below are routed within one slice.
    gen_mux_16 : for i in mux_16_output'range generate
        comp_mux_16 : entity work.mux_16
            port map (
                bit_input      => bit_input((i+1)*16-1 downto i*16),
                bit_select      => bit_select(3 downto 0),
                bit_output      => mux_16_output(i),
                bit_output_local  => mux_16_output_local(i)
            );

        comp_mux_16_latch : process (clk)
        begin
            if (clk'event and clk = '1') then
                -- use local signal to place FF with same slice as F8MUX.
                mux_16_output_sync(i) <= mux_16_output_local(i);
            end if;
        end process comp_mux_16_latch;
    end generate gen_mux_16;

    -- RANK 2
    -- Mux the outputs of the 16 slices defined above.
    comp_mux_128 : entity work.mux_8
        port map (
            bit_input      => mux_16_output_sync,
            bit_select      => bit_select(6 downto 4),
            bit_output      => mux_128_output,
            bit_output_local  => mux_128_output_local
        );

    bit_output_aync <= mux_128_output;
    comp_mux_16_latch : process (clk)
    begin
        if (clk'event and clk = '1') then
            -- use local signal to place FF with same slice as F8MUX.
            bit_output_sync <= mux_128_output_local;
        end if;
    end process comp_mux_16_latch;
end rtl;

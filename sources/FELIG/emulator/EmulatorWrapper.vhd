--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!               Shelfali Saxena
--!               Ricardo Luz
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer: Initially created for HG710 as gt_core_exdes by Michael Oberling
-- modified for FLX-712 by Marco Trovato

-- Design Name:  EmulatorWrapper
-- Version:    1.0
-- Date:    9/13/2017
--
-- Description:  Coming soon.
--
-- Change Log:  V1.0 -
--
LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
--use ieee.numeric_std.all;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.type_lib.ALL;
    use work.ip_lib.ALL;
    use work.FELIX_package.all;
    use work.pcie_package.all;


entity EmulatorWrapper is
    generic(
        STABLE_CLOCK_PERIOD         : integer := 10;
        GBT_NUM                     : integer := 24;
        NUMELINKmax                 : integer := 112;
        NUMEGROUPmax                : integer := 7;
        FIRMWARE_MODE               : integer := 0
    );
    port(
        clk40                       : in std_logic;
        gt_txusrclk_in              : in std_logic_vector(GBT_NUM-1 downto 0);
        gt_rxusrclk_in              : in std_logic_vector(GBT_NUM-1 downto 0);
        link_tx_data_228b_array_out : out array_228b(0 to GBT_NUM-1);
        data_ready_tx_out           : out std_logic_vector(0 to GBT_NUM-1);
        link_rx_data_120b_array_in  : in  array_120b(0 to GBT_NUM-1);
        link_tx_flag_in             : in std_logic_vector(GBT_NUM-1 downto 0);
        link_rx_flag_in             : in std_logic_vector(GBT_NUM-1 downto 0);
        l1a_int_trigger_out         : out std_logic;
        lane_control                : in  array_of_lane_control_type(GBT_NUM-1 downto 0);
        lane_monitor                : out array_of_lane_monitor_type(GBT_NUM-1 downto 0)
    );


end EmulatorWrapper;

architecture RTL of EmulatorWrapper is
    function FM_TO_PROTOCOL(FIRMWARE_MODE:integer)
        return std_logic is
    begin
        if FIRMWARE_MODE = FIRMWARE_MODE_FELIG_GBT then
            return '0';
        elsif FIRMWARE_MODE = FIRMWARE_MODE_FELIG_LPGBT then
            return '1';
        else
            return '0';
        end if;
    end function;
    signal l1a_int_trigger                  : std_logic := '0';
    signal l1a_int_int_id                   : std_logic_vector(15 downto 0) := (others=>'0');
    signal l1a_int_int_id_extra             : std_logic_vector(31 downto 0) := (others=>'0');
    signal l1a_int_count, l1a_int_max_count : std_logic_vector(31 downto 0) := (others=>'0');
    signal l1a_int_max_count_extra          : std_logic_vector(15 downto 0) := (others=>'0');
    signal l1a_int_count_load               : std_logic := '0';
    signal l1a_int_int_id_sclr              : std_logic := '0';
    signal l1a_int_counter_reset            : std_logic := '0';
    signal flag_int_counter_reset           : std_logic := '0';
    signal counter_reset                    : std_logic := '0';
    signal start_dsp_flag                   : std_logic := '0'; -- so dsp works in sim
    signal start_dsp                        : std_logic := '0'; -- so dsp works in sim
begin

    l1a_int_max_count   <= "0000" & lane_control(0).global.l1a_max_count;--"0000" & register_map_control_40xtal.FELIG_GLOBAL_CONTROL.FAKE_L1A_RATE;
    counter_reset       <= lane_control(0).global.l1a_counter_reset;
    l1a_int_trigger_out <= l1a_int_trigger;
    l1a_int_counter : dsp_counter
        PORT MAP (
            CLK    => clk40,
            CE    => '1',
            SCLR  => '0',
            UP    => '0',
            LOAD  => l1a_int_count_load or start_dsp,
            L(47 downto 32)    => X"0000",
            L(31 downto  0)    => l1a_int_max_count,
            Q(47 downto 32)    => l1a_int_max_count_extra, --MT SIMU+
            Q(31 downto  0)    => l1a_int_count
        );

    l1a_int_int_id_counter : dsp_counter
        PORT MAP (
            CLK    => clk40,
            CE    => l1a_int_trigger or start_dsp,
            SCLR  => l1a_int_int_id_sclr,
            UP    => '1',
            LOAD  => l1a_int_counter_reset or start_dsp, --RL
            L    => X"000000000000",
            Q(47 downto 16)    => l1a_int_int_id_extra, --MT SIMU+ open,
            Q(15 downto  0)    => l1a_int_int_id
        );

    process(clk40)
    begin
        if clk40'event and clk40='1' then
            if counter_reset = '1' and flag_int_counter_reset = '0' then
                l1a_int_counter_reset <= '1';
                flag_int_counter_reset <= '1';
            elsif l1a_int_int_id  = X"0000" then
                l1a_int_counter_reset <= '0';
                if counter_reset = '0' then
                    flag_int_counter_reset <= '0';
                end if;
            end if;
        end if;
    end process;

    process(clk40)
    begin
        if clk40'event and clk40 = '1' then
            if l1a_int_max_count = 0 then
                l1a_int_trigger <= '0';
                l1a_int_int_id_sclr <= '1';
                l1a_int_count_load <= '1';
            elsif l1a_int_count = 0 then
                l1a_int_trigger <= '1';
                l1a_int_int_id_sclr <= '0';
                l1a_int_count_load <= '1';
            else
                l1a_int_trigger <= '0';
                l1a_int_int_id_sclr <= '0';
                l1a_int_count_load <= '0';
            end if;
            if start_dsp_flag = '0' then
                start_dsp_flag  <= '1';
                start_dsp       <= '1';
            else
                start_dsp       <= '0';
            end if;
        end if;
    end process;

    emulator_inst : for link in GBT_NUM-1 downto 0 generate
        gbt : entity work.emulator
            generic map (
                useGBTdataEmulator       => false,
                sim_emulator             => false,
                LANE_ID                  => link,
                PROTOCOL                 => FM_TO_PROTOCOL(FIRMWARE_MODE)
            )
            port map (
                clk40                   => clk40,
                lane_rxclk              => gt_rxusrclk_in(link),
                lane_txclk              => gt_txusrclk_in(link),
                l1a_int_trigger         => l1a_int_trigger,
                l1a_int_int_id          => l1a_int_int_id,
                l1a_trigger_out         => open,--l1a_trigger_out(link),
                gbt_tx_data_228b_out    => link_tx_data_228b_array_out(link),
                data_ready_tx_out       => data_ready_tx_out(link),-- out std_logic;
                gbt_rx_data_120b_in     => link_rx_data_120b_array_in(link),
                gbt_tx_flag_in          => link_tx_flag_in(link),
                gbt_rx_flag_in          => link_rx_flag_in(link),
                lane_control            => lane_control(link),
                lane_monitor            => lane_monitor(link)--,
            );
    end generate;

end RTL;

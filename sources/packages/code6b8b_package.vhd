--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

package code6b8b_package is
    function encode_6b8b(data_in : in std_logic_vector(5 downto 0); data_isk : in std_logic) return std_logic_vector;
    function decode_6b8b(data_in : in std_logic_vector(7 downto 0)) return std_logic_vector;

    constant K_7_encoded  : std_logic_vector(7 downto 0) := "01000111";    -- 0x47
    constant K_21_encoded : std_logic_vector(7 downto 0) := "01010101";    -- 0x55
    constant K_42_encoded : std_logic_vector(7 downto 0) := "01101010";    -- 0x6A
    constant K_56_encoded : std_logic_vector(7 downto 0) := "01111000";    -- 0x78
end package;

package body code6b8b_package is
    function encode_6b8b(data_in : in std_logic_vector(5 downto 0); data_isk : in std_logic) return std_logic_vector is
        variable disp : integer range -6 to 6;
        variable ret : std_logic_vector(7 downto 0);
    begin
        if data_isk = '1' then
            case data_in is
                when "000111" => ret := K_7_encoded;
                when "010101" => ret := K_21_encoded;
                when "101010" => ret := K_42_encoded;
                when "111000" => ret := K_56_encoded;
                when others   => ret := "00000000";
            end case;
        else
            -- calculate disparity of input word
            disp := 0;
            for i in data_in'range loop
                if data_in(i) = '1' then
                    disp := disp + 1;
                else
                    disp := disp - 1;
                end if;
            end loop;

            if disp = 0 then
                ret := "10" & data_in;
            elsif disp = 2 then
                if data_in = "001111" then
                    ret := "01001011";
                else
                    ret := "00" & data_in;
                end if;
            elsif disp = -2 then
                if data_in = "110000" then
                    ret := "01110100";
                else
                    ret := "11" & data_in;
                end if;
            else
                case data_in is
                    when "000000" => ret := "01011001";
                    when "111111" => ret := "01100110";
                    when "000001" => ret := "01110001";
                    when "000010" => ret := "01110010";
                    when "000100" => ret := "01100101";
                    when "001000" => ret := "01101001";
                    when "010000" => ret := "01010011";
                    when "100000" => ret := "01100011";
                    when "111110" => ret := "01001110";
                    when "111101" => ret := "01001101";
                    when "111011" => ret := "01011010";
                    when "110111" => ret := "01010110";
                    when "101111" => ret := "01101100";
                    when "011111" => ret := "01011100";
                    when others   => ret := "00000000";
                end case;
            end if;
        end if;

        return ret;
    end function;

    function decode_6b8b(data_in : in std_logic_vector(7 downto 0)) return std_logic_vector is
        type decode_6b8b_LUT_t is array (0 to 255) of std_logic_vector(7 downto 0);
        constant decode_6b8b_LUT : decode_6b8b_LUT_t := (
                                                          0 => "10000000",
                                                          1 => "10000000",
                                                          2 => "10000010",
                                                          3 => "10000011",
                                                          4 => "10000100",
                                                          5 => "10000100",
                                                          6 => "10000110",
                                                          7 => "10000011",
                                                          8 => "10000000",
                                                          9 => "10000000",
                                                          10 => "10001010",
                                                          11 => "10000000",
                                                          12 => "10001100",
                                                          13 => "10000000",
                                                          14 => "10000110",
                                                          15 => "00001111",
                                                          16 => "10000000",
                                                          17 => "10000000",
                                                          18 => "10000010",
                                                          19 => "10000000",
                                                          20 => "10010100",
                                                          21 => "10000000",
                                                          22 => "10000010",
                                                          23 => "00010111",
                                                          24 => "10000000",
                                                          25 => "10000000",
                                                          26 => "10000000",
                                                          27 => "00011011",
                                                          28 => "10000000",
                                                          29 => "00011101",
                                                          30 => "00011110",
                                                          31 => "10000000",
                                                          32 => "10000001",
                                                          33 => "10000001",
                                                          34 => "10000010",
                                                          35 => "10000001",
                                                          36 => "10000100",
                                                          37 => "10000001",
                                                          38 => "10000010",
                                                          39 => "00100111",
                                                          40 => "10001000",
                                                          41 => "10000000",
                                                          42 => "10000010",
                                                          43 => "00101011",
                                                          44 => "10000100",
                                                          45 => "00101101",
                                                          46 => "00101110",
                                                          47 => "10000100",
                                                          48 => "10000001",
                                                          49 => "10000000",
                                                          50 => "10000001",
                                                          51 => "00110011",
                                                          52 => "10000001",
                                                          53 => "00110101",
                                                          54 => "00110110",
                                                          55 => "10000001",
                                                          56 => "10000000",
                                                          57 => "00111001",
                                                          58 => "00111010",
                                                          59 => "10000000",
                                                          60 => "00111100",
                                                          61 => "10000000",
                                                          62 => "10000010",
                                                          63 => "10001111",
                                                          64 => "10000000",
                                                          65 => "10000000",
                                                          66 => "10000010",
                                                          67 => "10000000",
                                                          68 => "10000100",
                                                          69 => "10000000",
                                                          70 => "10000010",
                                                          71 => "01000111",
                                                          72 => "10000000",
                                                          73 => "10000000",
                                                          74 => "10000000",
                                                          75 => "00001111",
                                                          76 => "10000000",
                                                          77 => "00111101",
                                                          78 => "00111110",
                                                          79 => "10000000",
                                                          80 => "10000000",
                                                          81 => "10000000",
                                                          82 => "10000000",
                                                          83 => "00010000",
                                                          84 => "10000000",
                                                          85 => "01010101",
                                                          86 => "00110111",
                                                          87 => "10000000",
                                                          88 => "10000000",
                                                          89 => "00000000",
                                                          90 => "00111011",
                                                          91 => "10000000",
                                                          92 => "00011111",
                                                          93 => "10000000",
                                                          94 => "10000000",
                                                          95 => "10000000",
                                                          96 => "10000001",
                                                          97 => "10000000",
                                                          98 => "10000001",
                                                          99 => "00100000",
                                                          100 => "10000001",
                                                          101 => "00000100",
                                                          102 => "00111111",
                                                          103 => "10000001",
                                                          104 => "10000000",
                                                          105 => "00001000",
                                                          106 => "01101010",
                                                          107 => "10000000",
                                                          108 => "00101111",
                                                          109 => "10000000",
                                                          110 => "10000010",
                                                          111 => "10000100",
                                                          112 => "10000000",
                                                          113 => "00000001",
                                                          114 => "00000010",
                                                          115 => "10000000",
                                                          116 => "00110000",
                                                          117 => "10000000",
                                                          118 => "10000001",
                                                          119 => "10000001",
                                                          120 => "01111000",
                                                          121 => "10000000",
                                                          122 => "10000000",
                                                          123 => "10000000",
                                                          124 => "10000000",
                                                          125 => "10000000",
                                                          126 => "10000010",
                                                          127 => "10000000",
                                                          128 => "10000011",
                                                          129 => "10000011",
                                                          130 => "10000011",
                                                          131 => "10000011",
                                                          132 => "10000101",
                                                          133 => "10000011",
                                                          134 => "10000011",
                                                          135 => "00000111",
                                                          136 => "10001001",
                                                          137 => "10000000",
                                                          138 => "10000011",
                                                          139 => "00001011",
                                                          140 => "10000101",
                                                          141 => "00001101",
                                                          142 => "00001110",
                                                          143 => "10000011",
                                                          144 => "10010001",
                                                          145 => "10000000",
                                                          146 => "10000010",
                                                          147 => "00010011",
                                                          148 => "10000101",
                                                          149 => "00010101",
                                                          150 => "00010110",
                                                          151 => "10000011",
                                                          152 => "10000000",
                                                          153 => "00011001",
                                                          154 => "00011010",
                                                          155 => "10000000",
                                                          156 => "00011100",
                                                          157 => "10000000",
                                                          158 => "10000110",
                                                          159 => "10000111",
                                                          160 => "10100001",
                                                          161 => "10000001",
                                                          162 => "10000010",
                                                          163 => "00100011",
                                                          164 => "10000100",
                                                          165 => "00100101",
                                                          166 => "00100110",
                                                          167 => "10000011",
                                                          168 => "10001000",
                                                          169 => "00101001",
                                                          170 => "00101010",
                                                          171 => "10000011",
                                                          172 => "00101100",
                                                          173 => "10000100",
                                                          174 => "10000110",
                                                          175 => "10000111",
                                                          176 => "10000001",
                                                          177 => "00110001",
                                                          178 => "00110010",
                                                          179 => "10000001",
                                                          180 => "00110100",
                                                          181 => "10000001",
                                                          182 => "10000010",
                                                          183 => "10000111",
                                                          184 => "00111000",
                                                          185 => "10000000",
                                                          186 => "10000010",
                                                          187 => "10001011",
                                                          188 => "10001100",
                                                          189 => "10001101",
                                                          190 => "10001110",
                                                          191 => "10000111",
                                                          192 => "10000011",
                                                          193 => "10000000",
                                                          194 => "10000010",
                                                          195 => "00000011",
                                                          196 => "10000011",
                                                          197 => "00000101",
                                                          198 => "00000110",
                                                          199 => "10000011",
                                                          200 => "10000000",
                                                          201 => "00001001",
                                                          202 => "00001010",
                                                          203 => "10000000",
                                                          204 => "00001100",
                                                          205 => "10000000",
                                                          206 => "10000011",
                                                          207 => "10000011",
                                                          208 => "10000000",
                                                          209 => "00010001",
                                                          210 => "00010010",
                                                          211 => "10000000",
                                                          212 => "00010100",
                                                          213 => "10000000",
                                                          214 => "10000010",
                                                          215 => "10000011",
                                                          216 => "00011000",
                                                          217 => "10000000",
                                                          218 => "10000000",
                                                          219 => "10000000",
                                                          220 => "10000000",
                                                          221 => "10000000",
                                                          222 => "10000110",
                                                          223 => "10000000",
                                                          224 => "10000001",
                                                          225 => "00100001",
                                                          226 => "00100010",
                                                          227 => "10000001",
                                                          228 => "00100100",
                                                          229 => "10000001",
                                                          230 => "10000010",
                                                          231 => "10000011",
                                                          232 => "00101000",
                                                          233 => "10000000",
                                                          234 => "10000010",
                                                          235 => "10000011",
                                                          236 => "10000100",
                                                          237 => "10000100",
                                                          238 => "10000110",
                                                          239 => "10000011",
                                                          240 => "00110000",
                                                          241 => "10000000",
                                                          242 => "10000001",
                                                          243 => "10000001",
                                                          244 => "10000001",
                                                          245 => "10000001",
                                                          246 => "10000010",
                                                          247 => "10000001",
                                                          248 => "10000000",
                                                          249 => "10000000",
                                                          250 => "10000010",
                                                          251 => "10000000",
                                                          252 => "10001100",
                                                          253 => "10000000",
                                                          254 => "10000010",
                                                          255 => "10000000"
                                                        );
        variable idx : integer range 0 to 255;
    begin
        idx := to_integer(unsigned(data_in));
        return decode_6b8b_LUT(idx);
    end function;
end package body;

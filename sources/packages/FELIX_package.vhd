--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Alessandra Camplani
--!               Frans Schreuder
--!               Thei Wijnen
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
--use work.pcie_package.all;

package FELIX_package is

    type array_228b is array (natural range <>) of std_logic_vector(227 downto 0);
    type array_224b is array (natural range <>) of std_logic_vector(223 downto 0);
    type array_120b is array (natural range <>) of std_logic_vector(119 downto 0);
    type array_69b is array (natural range <>) of std_logic_vector(68 downto 0);
    type array_66b is array (natural range <>) of std_logic_vector(65 downto 0);
    type array_65b is array (natural range <>) of std_logic_vector(64 downto 0);
    type array_64b is array (natural range <>) of std_logic_vector(63 downto 0);
    type array_57b  is array (natural range <>) of std_logic_vector(56 downto 0);
    type array_48b is array (natural range <>) of std_logic_vector(47 downto 0);
    type array_36b is array (natural range <>) of std_logic_vector(35 downto 0);
    type array_34b is array (natural range <>) of std_logic_vector(33 downto 0);
    type array_33b  is array (natural range <>) of std_logic_vector(32 downto 0);
    type array_32b is array (natural range <>) of std_logic_vector(31 downto 0);
    type array_20b is array (natural range <>) of std_logic_vector(19 downto 0);
    type array_16b is array (natural range <>) of std_logic_vector(15 downto 0);
    type array_11b is array (natural range <>) of std_logic_vector(10 downto 0);
    type array_10b is array (natural range <>) of std_logic_vector(9 downto 0);
    type array_9b  is array (natural range <>) of std_logic_vector(8 downto 0);
    type array_8b  is array (natural range <>) of std_logic_vector(7 downto 0);
    type array_7b  is array (natural range <>) of std_logic_vector(6 downto 0);
    type array_5b  is array (natural range <>) of std_logic_vector(4 downto 0);
    type array_4b  is array (natural range <>) of std_logic_vector(3 downto 0);
    type array_3b  is array (natural range <>) of std_logic_vector(2 downto 0);
    type array_2b  is array (natural range <>) of std_logic_vector(1 downto 0);

    type array_2d_32b           is array (natural range <>, natural range <>) of std_logic_vector(31 downto 0); --pixel decoding
    type array_2d_5b            is array (natural range <>, natural range <>) of std_logic_vector(4 downto 0);
    type array_2d_4b            is array (natural range <>, natural range <>) of std_logic_vector(3 downto 0);--pixel decoding
    type array_2d_3b            is array (natural range <>, natural range <>) of std_logic_vector(2 downto 0);--pixel decoding
    type array_inv_6b           is array (natural range <>) of std_logic_vector(0 to 5); --pixel decoding
    --

    -- GTH PLL selection
    -- When using GREFCLK, QPLL should be used
    -- use CPLL for VC-709 and BNL-711
    -- use QPLL for HTG-710 (cannot use dedicated clock pin) <- this has WORSE jitter performance
    constant CPLL                                         : std_logic := '0';
    constant QPLL                                         : std_logic := '1';

    constant FIRMWARE_MODE_GBT         : integer := 0;-- 0: GBT mode
    constant FIRMWARE_MODE_FULL        : integer := 1;-- 1: FULL mode with selectable GBT/LTI-FE downlink
    constant FIRMWARE_MODE_LTDB        : integer := 2;-- 2: LTDB mode (GBT mode with only IC/EC/Aux)
    constant FIRMWARE_MODE_FEI4        : integer := 3;-- 3: FEI4 / RD53A
    constant FIRMWARE_MODE_PIXEL       : integer := 4;-- 4: ITK Pixel (RD53B)
    constant FIRMWARE_MODE_STRIP       : integer := 5;-- 5: ITK Strip
    constant FIRMWARE_MODE_FELIG_GBT   : integer := 6;-- 6: FELIG_GBT
    constant FIRMWARE_MODE_FMEMU       : integer := 7;-- 7: FULL mode emulator
    constant FIRMWARE_MODE_MROD        : integer := 8; --8: FELIX mrod (2Gb/s S-links) mode.
    constant FIRMWARE_MODE_LPGBT       : integer := 9; --9: LPGBT mode
    constant FIRMWARE_MODE_INTERLAKEN  : integer := 10; --10: 25Gb/s interlaken links
    constant FIRMWARE_MODE_FELIG_LPGBT : integer := 11;-- 11: FELIG_LPGBT
    constant FIRMWARE_MODE_HGTD_LUMI   : integer := 12; --12: HGTD ALTIROC Lumi
    constant FIRMWARE_MODE_BCM_PRIME   : integer := 13; --13: BCM' readout
    constant FIRMWARE_MODE_FELIG_PIXEL : integer := 14; --14: FELIG pixel / 64b66b
    constant FIRMWARE_MODE_FELIG_STRIP : integer := 15; --15: FELIG strip
    constant FIRMWARE_MODE_WUPPER      : integer := 16; --16: Wupper only for PCIe performance test

    type IntArray is array (natural range<>) of integer;
    constant MAX_GROUPS_PER_STREAM_FROMHOST : integer := 8;
    type IntArray2D is array (natural range<>) of IntArray(0 to MAX_GROUPS_PER_STREAM_FROMHOST-1);
    type IntArray2D_24 is array (natural range<>) of IntArray(0 to 23);
    constant STREAMS_TOHOST_MODE : IntArray(0 to 16) :=
    (
      42, --GBT mode: 40 EPaths + IC + EC + TTCToHost + BusyXoff
      1,  --FULL mode: + TTCToHost + BusyXoff
      42,  --LTDB mode:
      42, --FEI4 (tbd)
      26,  --ITK Pixel 24 (6 eg * 4) EPaths + IC + EC
      30,  --ITK Strip - IC/EC + 7 egroups * 4 channels (320 Mbps 8b10b)
      42,  --FELIG GBT
      42,  --FMEmu
      42,  --FELIX mrod
      30,   --LPGBT mode: 28 EPaths + IC + EC
      1,   -- Interlaken mode
      1,  -- FELIG LPGBT
      30,  -- HGTD luminosity (14 lpGBT e-links splitted into aggregated and per-event lumi) + IC + EC
      26,   --BCMPRIME
      1,   --FELIG pixel
      1,   --FELIG strip
      1    --Wupper standalone
    );
    constant STREAMS_FROMHOST_MODE : IntArray2D(0 to 16) :=
    (
      (8,8,8,8,8,2,0,0), --GBT mode: 40 EPaths + IC + EC divided into 6 groups (5x8 + 1x2)
      (8,8,8,8,8,2,0,0), --FULL mode: 40 EPaths + IC + EC
      (8,8,8,8,8,2,0,0), --LTDB mode:  ??
      (8,8,8,8,8,2,0,0), -- FEI4: 40 EPaths + IC + EC
      (4,4,4,4,2,0,0,0), -- ITK Pixel: 16 EPATH = 4x4 + IC + EC.
      (5,5,5,5,2,0,0,0), -- ITK Strip: lpGBT mode + IC/EC
      (8,8,8,8,8,2,0,0), -- FELIG GBT
      (8,8,8,8,8,2,0,0), -- FMEmu
      (8,8,8,8,8,2,0,0), -- FELIX mrod
      (4,4,4,4,2,0,0,0), -- LPGBT mode  16 EPATH = 4x4 + IC + EC
      (1,0,0,0,0,0,0,0), -- Interlaken mode
      (1,0,0,0,0,0,0,0),  -- FELIG LPGBT
      (2,0,0,0,0,0,0,0),  -- HGTD luminosity readout (only IC/EC)
      (2,0,0,0,0,0,0,0),  -- ITK BCM: (only IC/EC)
      (1,0,0,0,0,0,0,0),   --FELIG pixel
      (1,0,0,0,0,0,0,0),   --FELIG strip
      (1,0,0,0,0,0,0,0)    --Wupper standalone
    );

    --Determines whether axis32/axis8 or axis64 will be used by CRToHost / CRFromHost for each link.
    constant LINK_CONFIG_MODE : IntArray2D_24(0 to 16) :=
    (
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), --GBT mode: 40 EPaths + IC + EC divided into 6 groups (5x8 + 1x2)
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), --FULL mode: 40 EPaths + IC + EC
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), --LTDB mode:  ??
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), -- FEI4: 40 EPaths + IC + EC
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), -- ITK Pixel: 16 EPATH = 4x4 + IC + EC.
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), -- ITK Strip: lpGBT mode + IC/EC
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), -- FELIG
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), -- FMEmu
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), -- FELIX mrod
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), -- LPGBT mode  16 EPATH = 4x4 + IC + EC
      (1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1),  -- Interlaken mode
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), -- FELIG LPGBT
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), -- HGTD_LUMI
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), -- BCMPRIME
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), -- FELIG pixel
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0), -- FELIG strip
      (0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0)  -- Wupper standalone
    );


    ---------------------------------------------------------------------
    ---- 8b10b encoding / decoding parameters
    ---------------------------------------------------------------------
    ---- 1. 10-bit values
    ----- comma / idle character
    constant COMMAp     : std_logic_vector (9 downto 0) := "0011111010"; -- -K.28.5
    constant COMMAn     : std_logic_vector (9 downto 0) := "1100000101"; -- +K.28.5
    constant FEI4B_COMMAp  : std_logic_vector (9 downto 0) := "1100000110"; -- +K.28.1
    constant FEI4B_COMMAn  : std_logic_vector (9 downto 0) := "0011111001"; -- -K.28.1
    --constant LCB_ENC_COMMAp    : std_logic_vector (9 downto 0) := "1100000110"; -- +K.28.1
    --constant LCB_ENC_COMMAn    : std_logic_vector (9 downto 0) := "0011111001"; -- -K.28.1
    ----these need to be changed
    constant LCB_COMMAp   : std_logic_vector (9 downto 0) := "1100000110"; -- +K.28.1
    constant LCB_COMMAn   : std_logic_vector (9 downto 0) := "0011111001"; -- -K.28.1
    --
    ----- start-of-chunk and end-of-chunk characters
    constant EOCp       : std_logic_vector (9 downto 0) := "0011110110"; -- -K.28.6
    constant EOCn       : std_logic_vector (9 downto 0) := "1100001001"; -- +K.28.6
    constant SOCp       : std_logic_vector (9 downto 0) := "0011111001"; -- -K.28.1
    constant SOCn       : std_logic_vector (9 downto 0) := "1100000110"; -- +K.28.1
    --constant FEI4B_EOCp    : std_logic_vector (9 downto 0) := "1100000101"; -- +K.28.5
    --constant FEI4B_EOCn    : std_logic_vector (9 downto 0) := "0011111010"; -- -K.28.5
    constant FEI4B_SOCp    : std_logic_vector (9 downto 0) := "1100000111"; -- +K.28.7
    constant FEI4B_SOCn    : std_logic_vector (9 downto 0) := "0011111000"; -- -K.28.7
    --constant LCB_ENC_EOCp        : std_logic_vector (9 downto 0) := "1100000101"; -- +K.28.5
    --constant LCB_ENC_EOCn        : std_logic_vector (9 downto 0) := "0011111010"; -- -K.28.5
    --constant LCB_ENC_SOCp        : std_logic_vector (9 downto 0) := "1100000111"; -- +K.28.7
    --constant LCB_ENC_SOCn        : std_logic_vector (9 downto 0) := "0011111000"; -- -K.28.7
    ----these need to be changed:
    --constant LCB_EOCp     : std_logic_vector (9 downto 0) := "1100000101"; -- +K.28.5
    --constant LCB_EOCn     : std_logic_vector (9 downto 0) := "0011111010"; -- -K.28.5
    --constant LCB_SOCp     : std_logic_vector (9 downto 0) := "1100000111"; -- +K.28.7
    --constant LCB_SOCn     : std_logic_vector (9 downto 0) := "0011111000"; -- -K.28.7
    --
    --
    ----- start-of-busy and end-of-busy characters
    --constant SOBp   : std_logic_vector (9 downto 0) := "0011110101"; -- -K.28.2
    --constant SOBn   : std_logic_vector (9 downto 0) := "1100001010"; -- +K.28.2
    --constant EOBp   : std_logic_vector (9 downto 0) := "0011110011"; -- -K.28.3
    --constant EOBn   : std_logic_vector (9 downto 0) := "1100001100"; -- +K.28.3
    --
    ---- 2. 8-bit values
    constant Kchar_comma  : std_logic_vector (7 downto 0) := "10111100"; -- K28.5 0xBC
    constant Kchar_eop    : std_logic_vector (7 downto 0) := "11011100"; -- K28.6 0xDC
    constant Kchar_sop    : std_logic_vector (7 downto 0) := "00111100"; -- K28.1 0x3C
    constant Kchar_sob    : std_logic_vector (7 downto 0) := "01011100"; -- K28.2 0x5C
    constant Kchar_eob    : std_logic_vector (7 downto 0) := "01111100"; -- K28.3 0x7C
    --constant Kchar_sot    : std_logic_vector (7 downto 0) := "10011100"; -- K28.4 --start truncation
    --
    constant FEI4B_Kchar_comma  : std_logic_vector (7 downto 0) := "00111100"; -- K28.1
    constant FEI4B_Kchar_eop    : std_logic_vector (7 downto 0) := "10111100"; -- K28.5
    constant FEI4B_Kchar_sop    : std_logic_vector (7 downto 0) := "11111100"; -- K28.7
    --
    constant LCB_Kchar_comma  : std_logic_vector (7 downto 0) := "00111100"; -- K28.1
    constant LCB_Kchar_eop    : std_logic_vector (7 downto 0) := "10111100"; -- K28.5
    constant LCB_Kchar_sop    : std_logic_vector (7 downto 0) := "11111100"; -- K28.7
    --
    --
    constant HGTD_Kchar_comma  : std_logic_vector (7 downto 0) := "11111100"; -- K28.7
    constant HGTD_Kchar_eop    : std_logic_vector (7 downto 0) := "10111100"; -- K28.5
    constant HGTD_Kchar_sop    : std_logic_vector (7 downto 0) := "00000000"; -- there is no start of packet character
    --
    --K.28.5 188 BC 1011 1100 10bN 00 1111 1010 10bP 11 0000 0101
    --K.28.7 252 FC 1111 1100 10bN 00 1111 1000 10bP 11 0000 0111
    constant HGTD_EOPp          : std_logic_vector (9 downto 0) := "1100000101"; -- +K.28.5
    constant HGTD_EOPn          : std_logic_vector (9 downto 0) := "0011111010"; -- -K.28.5
    constant HGTD_COMMAp        : std_logic_vector (9 downto 0) := "1100000111"; -- +K.28.7
    constant HGTD_COMMAn        : std_logic_vector (9 downto 0) := "0011111000"; -- -K.28.7
    --
    --
    constant Kchar_FM_XOFF: std_logic_vector (7 downto 0) := "01011100"; -- K28.2
    constant Kchar_FM_XON : std_logic_vector (7 downto 0) := "01111100"; -- K28.3
    --
    ---------------------------------------------------------------------
    ---- 8b10b encoding / decoding parameters Phase-II (FE-I4B chip)
    ---------------------------------------------------------------------
    ------ 1. 10-bit values
    ------- comma / idle character
    ----constant COMMAp : std_logic_vector (9 downto 0) := "0011111001"; -- -K.28.1
    ----constant COMMAn : std_logic_vector (9 downto 0) := "1100000110"; -- +K.28.1
    ------- start-of-chunk and end-of-chunk characters
    ----constant EOCp   : std_logic_vector (9 downto 0) := "0011111010"; -- -K.28.5
    ----constant EOCn   : std_logic_vector (9 downto 0) := "1100000101"; -- +K.28.5
    ----constant SOCp   : std_logic_vector (9 downto 0) := "0011111000"; -- -K.28.7
    ----constant SOCn   : std_logic_vector (9 downto 0) := "1100000111"; -- +K.28.7
    ------- start-of-busy and end-of-busy characters
    ----constant SOBp   : std_logic_vector (9 downto 0) := "0011110101"; -- -K.28.2
    ----constant SOBn   : std_logic_vector (9 downto 0) := "1100001010"; -- +K.28.2
    ----constant EOBp   : std_logic_vector (9 downto 0) := "0011110011"; -- -K.28.3
    ----constant EOBn   : std_logic_vector (9 downto 0) := "1100001100"; -- +K.28.3
    ----
    ------ 2. 8-bit values
    ----constant Kchar_comma  : std_logic_vector (7 downto 0) := "00111100"; -- K28.1
    ----constant Kchar_eop    : std_logic_vector (7 downto 0) := "10111100"; -- K28.5
    ----constant Kchar_sop    : std_logic_vector (7 downto 0) := "11111100"; -- K28.7
    ----constant Kchar_sob    : std_logic_vector (7 downto 0) := "01011100"; -- K28.2
    ----constant Kchar_eob    : std_logic_vector (7 downto 0) := "01111100"; -- K28.3
    --
    ---------------------------------------------------------------------
    ---- HDLC encoding / decoding parameters
    ---------------------------------------------------------------------
    --constant HDLC_flag : std_logic_vector(7 downto 0) := "01111110";
    --

    --sum up all numbers in an IntArray
    function sum(constant x : IntArray) return integer;

    --To go from nenory depth to number of address bits
    function f_log2 (constant x : positive) return natural;
    function div_ceil(a : natural; b : positive) return natural;

    --Some clock buffers require a SIM_DEVICE generic that is depending on the hardware
    function SIM_DEVICE(brd: integer) return string;

    type TTC_data_type is record
        --***Phase2***
        -- Header
        --MT                  : std_logic;
        PT                  : std_logic;
        Partition           : std_logic_vector(1 downto 0);
        BCID                : std_logic_vector(11 downto 0);
        SyncUserData        : std_logic_vector(15 downto 0);
        SyncGlobalData      : std_logic_vector(15 downto 0);
        TS                  : std_logic;
        ErrorFlags          : std_logic_vector(3 downto 0);

        -- TTC Message
        SL0ID               : std_logic;
        SOrb                : std_logic;
        Sync                : std_logic;
        GRst                : std_logic;
        L0A                 : std_logic;
        L0ID                : std_logic_vector(37 downto 0);
        OrbitID             : std_logic_vector(31 downto 0);
        TriggerType         : std_logic_vector(15 downto 0);
        LBID                : std_logic_vector(15 downto 0);

        --User message
        AsyncUserData       : std_logic_vector(63 downto 0);
        XOFF                : std_logic;

        --! FS: No need to distribute LTI trailer info through the firmware
        -- Trailer
        CRC                 : std_logic_vector(15 downto 0);
        D16_2               : std_logic_vector(7 downto 0);
        --Comma               : std_logic_vector(7 downto 0);
        LTI_decoder_aligned   : std_logic;
        LTI_CRC_valid         : std_logic;

        --***Phase1***
        -- TTC Message
        L1A                 : std_logic; --bit 0
        Bchan               : std_logic; --bit 1
        BCR                 : std_logic; --bit 2
        ECR                 : std_logic; -- bit 3
        Brcst               : std_logic_vector(5 downto 0); --[7...2] bits 4-9
        Brcst_latched       : std_logic_vector(5 downto 0); -- [7..2] bits 10-15
        ExtendedTestPulse   : std_logic; -- bit 16
        XL1Id               : std_logic_vector(7 downto 0);
        L1Id                : std_logic_vector(23 downto 0);
        ITk_sync            : std_logic;
        ITk_tag             : std_logic_vector(7 downto 0);
        ITk_trig            : std_logic_vector(3 downto 0);
    --L0A                 : std_logic; --For Phase I TTC: Delayed version of L1A

    end record;

    type TTC_data_array_type is array(natural range <>) of TTC_data_type;

    constant TTC_zero : TTC_data_type := (
                                           -- Header
                                           --MT                  => '0',
                                           PT                  => '0',
                                           Partition           => (others => '0'),
                                           BCID                => (others => '0'),
                                           SyncUserData        => (others => '0'),
                                           SyncGlobalData      => (others => '0'),
                                           TS                  => '0',
                                           ErrorFlags          => (others => '0'),
                                           -- TTC Message
                                           SL0ID               => '0',
                                           SOrb                => '0',
                                           Sync                => '0',
                                           GRst                => '0',
                                           L0A                 => '0',
                                           L0ID                => (others => '0'),
                                           OrbitID             => (others => '0'),
                                           TriggerType         => (others => '0'),
                                           LBID                => (others => '0'),
                                           --User message
                                           AsyncUserData       => (others => '0'),
                                           XOFF                => '0',
                                           -- Trailer
                                           CRC                 => (others => '0'),
                                           D16_2               => (others => '0'),
                                           --Comma               => (others => '0'),
                                           LTI_decoder_aligned   => '1',
                                           LTI_CRC_valid         => '1',
                                           L1A => '0',
                                           Bchan => '0',
                                           BCR => '0',
                                           ECR => '0',
                                           Brcst => (others => '0'),
                                           Brcst_latched => (others => '0'),
                                           ExtendedTestPulse => '0',
                                           L1Id => (others => '0'),
                                           XL1ID => x"00",
                                           ITk_sync => '0',
                                           ITk_tag => (others => '0'),
                                           ITk_trig => "0000"
                                         );

    --type TTC_ToHost_data_type is record
    --    FMT          : std_logic_vector(7 downto 0);  --byte0
    --    LEN          : std_logic_vector(7 downto 0);  --byte1
    --    reserved0    : std_logic_vector(3 downto 0);  --byte2
    --    BCID         : std_logic_vector(11 downto 0); --byte2,3
    --    XL1ID        : std_logic_vector(7 downto 0);  --byte4
    --    L1ID         : std_logic_vector(23 downto 0); --byte 5,6,7
    --    orbit        : std_logic_vector(31 downto 0); --byte 8,9,10,11
    --    trigger_type : std_logic_vector(15 downto 0); --byte 12,13
    --    reserved1    : std_logic_vector(15 downto 0); --byte 14,15
    --    L0ID         : std_logic_vector(31 downto 0); --byte 16,17,18,19
    --    TAG          : std_logic_vector(7 downto 0);  --byte 20
    --    data_rdy     : std_logic;
    --end record;

    --constant TTC_ToHost_data_c: TTC_ToHost_data_type := (
    --                                                      FMT          => x"00",
    --                                                      LEN          => x"00",
    --                                                      reserved0    => x"0",
    --                                                      BCID         => x"000",
    --                                                      XL1ID        => x"00",
    --                                                      L1ID         => x"000000",
    --                                                      orbit        => x"00000000",
    --                                                      trigger_type => x"0000",
    --                                                      reserved1    => x"0000",
    --                                                      L0ID         => x"00000000",
    --                                                      TAG          => x"00",
    --                                                      data_rdy     => '0'
    --                                                    );

    type axi_miso_type is record
        AXI_LITE_0_arready :  STD_LOGIC;
        AXI_LITE_0_awready :  STD_LOGIC;
        AXI_LITE_0_bresp :  STD_LOGIC_VECTOR ( 1 downto 0 );
        AXI_LITE_0_bvalid : std_logic;
        AXI_LITE_0_rdata : std_logic_vector (31 downto 0);
        AXI_LITE_0_rresp : std_logic_vector (1 downto 0);
        AXI_LITE_0_rvalid: std_logic;
        AXI_LITE_0_wready: std_logic;

    end record;

    type axi_mosi_type is record
        AXI_LITE_0_araddr :  STD_LOGIC_VECTOR ( 15 downto 0 );
        AXI_LITE_0_arvalid :  STD_LOGIC;
        AXI_LITE_0_awaddr :  STD_LOGIC_VECTOR ( 15 downto 0 );
        AXI_LITE_0_awvalid :  STD_LOGIC;
        AXI_LITE_0_bready :  STD_LOGIC;
        AXI_LITE_0_rready : std_logic;
        AXI_LITE_0_wdata : std_logic_vector(31 downto 0);
        AXI_LITE_0_wvalid : std_logic;
        s_axi_lite_resetn_0 : std_logic;

    end record;


    type DDR_out_type is record
        act_n   :  STD_LOGIC_VECTOR ( 0 to 0 );
        adr     :  STD_LOGIC_VECTOR ( 16 downto 0 );
        ba      :  STD_LOGIC_VECTOR ( 1 downto 0 );
        bg      :  STD_LOGIC_VECTOR ( 1 downto 0 );
        ck_c    :  STD_LOGIC_VECTOR ( 1 downto 0 );
        ck_t    :  STD_LOGIC_VECTOR ( 1 downto 0 );
        cke     :  STD_LOGIC_VECTOR ( 1 downto 0 );
        cs_n    :  STD_LOGIC_VECTOR ( 1 downto 0 );
        odt     :  STD_LOGIC_VECTOR ( 1 downto 0 );
        reset_n :  STD_LOGIC_VECTOR ( 0 to 0 );
    end record;
    type DDR_out_array_type is array(natural range <>) of DDR_out_type;
    type DDR_inout_type is record
        dm_n  : STD_LOGIC_VECTOR ( 8 downto 0 );
        dq    : STD_LOGIC_VECTOR ( 71 downto 0 );
        dqs_c : STD_LOGIC_VECTOR ( 8 downto 0 );
        dqs_t : STD_LOGIC_VECTOR ( 8 downto 0 );
    end record;
    type DDR_inout_array_type is array(natural range <>) of DDR_inout_type;
    type DDR_in_type is record
        sys_clk_n : STD_LOGIC_VECTOR ( 0 to 0 );
        sys_clk_p : STD_LOGIC_VECTOR ( 0 to 0 );
    end record;
    type DDR_in_array_type is array(natural range <>) of DDR_in_type;






    type LPDDR_out_type is record
        ca_a    : STD_LOGIC_VECTOR ( 5 downto 0 );
        ca_b    : STD_LOGIC_VECTOR ( 5 downto 0 );
        ck_c_a  : STD_LOGIC_VECTOR ( 0 to 0 );
        ck_c_b  : STD_LOGIC_VECTOR ( 0 to 0 );
        ck_t_a  : STD_LOGIC_VECTOR ( 0 to 0 );
        ck_t_b  : STD_LOGIC_VECTOR ( 0 to 0 );
        cke_a   : STD_LOGIC_VECTOR ( 0 to 0 );
        cke_b   : STD_LOGIC_VECTOR ( 0 to 0 );
        cs_a    : STD_LOGIC_VECTOR ( 0 to 0 );
        cs_b    : STD_LOGIC_VECTOR ( 0 to 0 );
        reset_n : STD_LOGIC_VECTOR ( 0 to 0 );
    end record;
    type LPDDR_out_array_type is array(natural range <>) of LPDDR_out_type;
    type LPDDR_inout_type is record
        dmi_a   : STD_LOGIC_VECTOR ( 1 downto 0 );
        dmi_b   : STD_LOGIC_VECTOR ( 1 downto 0 );
        dq_a    : STD_LOGIC_VECTOR ( 15 downto 0 );
        dq_b    : STD_LOGIC_VECTOR ( 15 downto 0 );
        dqs_c_a : STD_LOGIC_VECTOR ( 1 downto 0 );
        dqs_c_b : STD_LOGIC_VECTOR ( 1 downto 0 );
        dqs_t_a : STD_LOGIC_VECTOR ( 1 downto 0 );
        dqs_t_b : STD_LOGIC_VECTOR ( 1 downto 0 );
    end record;
    type LPDDR_inout_array_type is array(natural range <>) of LPDDR_inout_type;
    type LPDDR_in_type is record
        sys_clk_n : STD_LOGIC_VECTOR ( 0 to 0 );
        sys_clk_p : STD_LOGIC_VECTOR ( 0 to 0 );
    end record;
    type LPDDR_in_array_type is array(natural range <>) of LPDDR_in_type;


    --Calculate ToHost AXI Stream clock frequency based on FIRMWARE_MODE.
    function TOHOST_AXI_STREAM_FREQ(FIRMWARE_MODE: integer) return integer;

    -- Calculate the number of CRFromHost transfer managers based on FIRMWARE_MODE
    function NUM_TRANSFER_MANAGERS_FROMHOST(FIRMWARE_MODE : integer) return integer;

    function USE_ULTRARAM_WUPPER(brd: integer) return boolean ;
    function USE_ULTRARAM_LCB(brd: integer) return boolean ;
    function USE_ULTRARAM_CRTOHOST(brd: integer; mode: integer) return boolean ;
    function USE_ENCODING_DISTR_RAM(brd: integer) return boolean;
    function IS_VERSAL(brd: integer) return boolean ;
    function SUPPORT_BUILT_IN_FIFO(brd: integer) return std_logic;
    function get_trigtag_mode(FIRMWARE_MODE: integer; RD53Version: string) return integer;

    --! Function to limit toplevel ports
    function NUM_BUSY_OUTPUTS(CARD_TYPE: integer) return integer;
    function NUM_NT_PORTSEL(CARD_TYPE: integer) return integer;
    function NUM_LMK(CARD_TYPE: integer) return integer;
    function NUM_PEX(CARD_TYPE: integer) return integer;
    function NUM_BIFURCATION_SELECT(CARD_TYPE: integer) return integer;
    function NUM_ADN(CARD_TYPE: integer) return integer;
    function NUM_TTC_INPUTS(CARD_TYPE: integer) return integer;
    function NUM_LTITTC_INPUTS(CARD_TYPE: integer; TTC_SYS_SEL: std_logic) return integer;
    function NUM_LTITTC_GTREFCLK1(CARD_TYPE: integer; TTC_SYS_SEL: std_logic) return integer;
    function LMKFREQ(TTC_SYS_SEL: std_logic) return integer;
    function NUM_TB_TRIGGERS(CARD_TYPE: integer) return integer;
    function NUM_SI5324(CARD_TYPE: integer) return integer;
    function NUM_BPI_FLASH(CARD_TYPE: integer) return integer;
    function NUM_SI5345(CARD_TYPE: integer) return integer;
    function NUM_STN0_PORTCFG(CARD_TYPE: integer) return integer;
    function NUM_STN1_PORTCFG(CARD_TYPE: integer) return integer;
    function NUM_SMA(CARD_TYPE: integer) return integer;
    function NUM_TACH(CARD_TYPE: integer) return integer;
    function NUM_TESTMODE(CARD_TYPE: integer) return integer;
    function NUM_UPSTREAM_PORTSEL(CARD_TYPE: integer) return integer;
    function NUM_EMCCLK(CARD_TYPE: integer) return integer;
    function NUM_LEDS(CARD_TYPE: integer) return integer;
    function NUM_UC_RESET_N(CARD_TYPE: integer) return integer;
    function NUM_OPTO_LOS(CARD_TYPE: integer) return integer;
    function NUM_I2C_MUXES(CARD_TYPE: integer) return integer;
    function NUM_GTREFCLK1S(GTREFCLKS: integer;FIRMWARE_MODE: integer;CARD_TYPE: integer) return integer;
    function NUM_DDR(CARD_TYPE: integer) return integer;
    function NUM_LPDDR(CARD_TYPE: integer) return integer;
    function NUM_FAN_FAIL(CARD_TYPE: integer) return integer;
    function NUM_FF3_PRSTN(CARD_TYPE: integer) return integer;
    function NUM_IOEXP(CARD_TYPE: integer) return integer;
    function NUM_PCIE_PWRBRK(CARD_TYPE: integer) return integer;
    function NUM_QSPI_RST(CARD_TYPE: integer) return integer;
    function NUM_FAN_PWM(CARD_TYPE: integer) return integer;

    type RD53_loopgen_type is record
        noInject        : std_logic;
        edgeMode        : std_logic;
        edgeDelay       : std_logic_vector(4 downto 0);
        edgeDuration    : std_logic_vector(7 downto 0);
        trigDelay       : std_logic_vector(7 downto 0);
        trigMultiplier  : std_logic_vector(5 downto 0);
    end record;

end FELIX_package;

package body FELIX_package is
    function sum(constant x : IntArray) return integer is
        variable xsum : integer;
    begin
        xsum := 0;
        for i in x'range loop
            xsum := xsum + x(i);
        end loop;
        return xsum;
    end function;

    function f_log2 (constant x : positive) return natural is
        variable i : natural;
    begin
        i := 0;
        while (2**i < x) and i < 31 loop
            i := i + 1;
        end loop;
        return i;
    end function;

    --! integer division; always round-up
    --! calculates: ceil(a / b)
    function div_ceil(a : natural; b : positive) return natural is
    begin
        return (a + (b - 1)) / b;
    end function;

    function SIM_DEVICE(brd: integer) return string is
    begin
        if brd = 155 then
            return "VERSAL_PREMIUM";
        elsif brd = 182 or brd = 181 or brd = 180 then
            return "VERSAL_PRIME";
        elsif brd = 711 or brd = 712 then
            return "ULTRASCALE";
        else
            return "7SERIES";
        end if;
    end function;

    --Calculate ToHost AXI Stream clock frequency based on FIRMWARE_MODE.
    function TOHOST_AXI_STREAM_FREQ(FIRMWARE_MODE: integer) return integer is
    begin
        case FIRMWARE_MODE is
            when FIRMWARE_MODE_GBT         => return 160;
            when FIRMWARE_MODE_FULL        => return 250;
            when FIRMWARE_MODE_LTDB        => return 160;
            when FIRMWARE_MODE_FEI4        => return 160;
            when FIRMWARE_MODE_PIXEL       => return 240;
            when FIRMWARE_MODE_STRIP       => return 240;
            when FIRMWARE_MODE_FELIG_GBT   => return 0;
            when FIRMWARE_MODE_FMEMU       => return 0;
            when FIRMWARE_MODE_MROD        => return 160;
            when FIRMWARE_MODE_LPGBT       => return 240;
            when FIRMWARE_MODE_INTERLAKEN  => return 250; --Using AXI-Stream 64b for Interlaken, this frequency is used for AUX E-Links.
            when FIRMWARE_MODE_FELIG_LPGBT => return 0;
            when FIRMWARE_MODE_HGTD_LUMI   => return 160; -- reducing clock from 240 to 160 for better timing in CRToHost, should be changed back with better timing
            when FIRMWARE_MODE_BCM_PRIME   => return 160;
            when FIRMWARE_MODE_FELIG_PIXEL => return 0;
            when FIRMWARE_MODE_FELIG_STRIP => return 160;
            when FIRMWARE_MODE_WUPPER      => return 250;

            when others => report "Unknown FIRMWARE_MODE given, don't know how to calculate ToHost AXI Stream clock frequency" severity error;
                return 0;

        end case;

    end function;

    function NUM_TRANSFER_MANAGERS_FROMHOST(FIRMWARE_MODE : integer) return integer is
    begin
        case FIRMWARE_MODE is
            when FIRMWARE_MODE_GBT         => return 4;
            when FIRMWARE_MODE_FULL        => return 4;
            when FIRMWARE_MODE_LTDB        => return 1;
            when FIRMWARE_MODE_FEI4        => return 1;
            when FIRMWARE_MODE_PIXEL       => return 1;
            when FIRMWARE_MODE_STRIP       => return 1;
            when FIRMWARE_MODE_FELIG_GBT   => return 1;
            when FIRMWARE_MODE_FMEMU       => return 0;
            when FIRMWARE_MODE_MROD        => return 1;
            when FIRMWARE_MODE_LPGBT       => return 1;
            when FIRMWARE_MODE_INTERLAKEN  => return 1;
            when FIRMWARE_MODE_FELIG_LPGBT => return 1;
            when FIRMWARE_MODE_HGTD_LUMI   => return 1;
            when FIRMWARE_MODE_BCM_PRIME   => return 1;
            when FIRMWARE_MODE_FELIG_PIXEL => return 1;
            when FIRMWARE_MODE_FELIG_STRIP => return 1;
            when FIRMWARE_MODE_WUPPER      => return 1;
            when others => report "Unknown FIRMWARE_MODE given, don't know how to calculate number of CRFromHost transfer managers. Use 1 as default." severity warning;
                return 1;
        end case;
    end function;

    function USE_ULTRARAM_WUPPER(brd: integer) return boolean is
    begin
        if brd = 180 or brd = 181 or brd = 182 or brd = 128 or brd = 155 then
            return true;
        else
            return false;
        end if;
    end function;

    function USE_ULTRARAM_LCB(brd: integer) return boolean is
    begin
        if brd = 180 or brd = 181 or brd = 182 or brd = 128 or brd = 155 then
            return true;
        else
            return false;
        end if;
    end function;

    function USE_ULTRARAM_CRTOHOST(brd: integer; mode: integer) return boolean is
    begin
        if brd = 180 or brd = 181 or brd = 182 or brd = 128 or brd = 155 then
            if mode /= FIRMWARE_MODE_INTERLAKEN then
                return true;
            else
                return false;
            end if;
        else
            return false;
        end if;
    end function;

    function USE_ENCODING_DISTR_RAM(brd: integer) return boolean is
    begin
        return false;
    end function;

    function IS_VERSAL(brd: integer) return boolean is
    begin
        if brd = 180 or brd = 181 or brd = 182 or brd = 155 then
            return true;
        else
            return false;
        end if;
    end function;

    function SUPPORT_BUILT_IN_FIFO(brd: integer) return std_logic is
    begin
        if IS_VERSAL(brd) then
            return '0';
        else
            return '1';
        end if;
    end function;

    function get_trigtag_mode(FIRMWARE_MODE: integer; RD53Version: string) return integer is
    begin
        if FIRMWARE_MODE = FIRMWARE_MODE_STRIP then
            return 2;           -- ABC*
        elsif FIRMWARE_MODE = FIRMWARE_MODE_PIXEL then
            if RD53Version = "A" then
                return 0;       -- RD53A
            else
                return 1;       -- ITkPixV1 (RD53B) and newer
            end if;
        else
            return -1;
        end if;
    end function;

    --! Function to limit toplevel ports
    function NUM_BUSY_OUTPUTS(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 709 or CARD_TYPE = 710 or CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_NT_PORTSEL(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 3;
        else
            return 0;
        end if;
    end function;

    --function NUM_MGMT_PORT_EN(CARD_TYPE: integer) return integer is
    --begin
    --  if CARD_TYPE = 711 or CARD_TYPE = 712 then
    --      return 3;
    --  else
    --      return 0;
    --  end if;
    --end function;

    function NUM_LMK(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_PEX(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 1;
        else
            return 0;
        end if;
    end function;


    function NUM_BIFURCATION_SELECT(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 712 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_ADN(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 709 or CARD_TYPE = 710 or CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 1;
        else
            return 0;
        end if;
    end function;


    function NUM_TTC_INPUTS(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 709 or CARD_TYPE = 710 or CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_LTITTC_INPUTS(CARD_TYPE: integer; TTC_SYS_SEL: std_logic) return integer is
    begin
        if (CARD_TYPE = 711 or CARD_TYPE = 712 or CARD_TYPE = 182 or CARD_TYPE = 155) and TTC_SYS_SEL = '1' then
            return 1;
        else
            return 0;
        end if;
    end function;

    --!Same as NUM_LTITTC_INPUTS, but for the FLX182 we share the LTI clock with the first transceiver quad, so no secondary refclk
    function NUM_LTITTC_GTREFCLK1(CARD_TYPE: integer; TTC_SYS_SEL: std_logic) return integer is
    begin
        if (CARD_TYPE = 711 or CARD_TYPE = 712 or CARD_TYPE = 155) and TTC_SYS_SEL = '1' then
            return 1;
        else
            return 0;
        end if;
    end function;


    function LMKFREQ(TTC_SYS_SEL: std_logic) return integer is
    begin
        if TTC_SYS_SEL = '1' then
            return 240;
        else
            return 320;
        end if;
    end function;

    function NUM_TB_TRIGGERS(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 712 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_SI5324(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 709 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_BPI_FLASH(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 709 or CARD_TYPE = 710 or CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 1;
        else
            return 0;
        end if;
    end function;

    --function NUM_I2C_SMB_CFG(CARD_TYPE: integer) return integer is
    --begin
    --  if CARD_TYPE = 711 or CARD_TYPE = 712 then
    --      return 1;
    --  else
    --      return 0;
    --  end if;
    --end function;
    --
    --function NUM_PCIE_PERSTN_OUT(CARD_TYPE: integer) return integer is
    --begin
    --  if CARD_TYPE = 711 or CARD_TYPE = 712 then
    --      return 2;
    --  else
    --      return 0;
    --  end if;
    --end function;
    --
    --function NUM_SHPC_INT(CARD_TYPE: integer) return integer is
    --begin
    --  if CARD_TYPE = 711 or CARD_TYPE = 712 then
    --      return 1;
    --  else
    --      return 0;
    --  end if;
    --end function;

    function NUM_SI5345(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 709 or CARD_TYPE = 710 or CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 1;
        elsif CARD_TYPE = 181 then
            return 4;
        elsif CARD_TYPE = 128 or CARD_TYPE = 182 or CARD_TYPE = 155 then
            return 2;
        else
            return 0;
        end if;
    end function;

    function NUM_STN0_PORTCFG(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 2;
        else
            return 0;
        end if;
    end function;

    function NUM_STN1_PORTCFG(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 2;
        else
            return 0;
        end if;
    end function;

    function NUM_SMA(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 4;
        elsif CARD_TYPE = 709 or CARD_TYPE = 181 then
            return 4;
        elsif CARD_TYPE = 182 then
            return 8;
        else
            return 0;
        end if;
    end function;


    function NUM_TACH(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 or CARD_TYPE = 709 or CARD_TYPE = 181 then
            return 1;
        else
            return 0; --FLX182 has a TACH pin, but the resistor is not connected. Another pin is connected to LPD MIO22, which we can use through the PL.
        end if;
    end function;

    function NUM_TESTMODE(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 3;
        else
            return 0;
        end if;
    end function;

    function NUM_UPSTREAM_PORTSEL(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 3;
        else
            return 0;
        end if;
    end function;

    function NUM_EMCCLK(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 or CARD_TYPE = 709 or CARD_TYPE = 710 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_LEDS(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 709 or CARD_TYPE = 710 then
            return 8;
        elsif CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 7;
        elsif CARD_TYPE = 800 or CARD_TYPE = 181 or CARD_TYPE = 182 or CARD_TYPE = 155 or CARD_TYPE = 120 then
            return 4;
        else
            return 8;
        end if;
    end function;

    function NUM_UC_RESET_N(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 711 or CARD_TYPE = 712 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_OPTO_LOS(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 709 or CARD_TYPE = 711 or CARD_TYPE = 712 or CARD_TYPE = 181 then
            return 4;
        elsif CARD_TYPE = 710 then
            return 2;
        elsif CARD_TYPE = 182 or CARD_TYPE = 155 then --For FLX182, the firefly modules are monitored through I2C GPIO
            return 0;
        else
            return 0;
        end if;
    end function;

    function NUM_I2C_MUXES(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 181 or CARD_TYPE = 128 then
            return 2;
        else
            return 1;
        end if;
    end function;

    --Return the number of secondary gtrefclks, for Interlaken we have the TTC/LTI transmitter which uses the primary one.
    function NUM_GTREFCLK1S(GTREFCLKS: integer;FIRMWARE_MODE: integer;CARD_TYPE:integer) return integer is
    begin
        if FIRMWARE_MODE = FIRMWARE_MODE_INTERLAKEN then
            return GTREFCLKS;
        else
            if CARD_TYPE = 182 or CARD_TYPE = 181 then --The GTREFCLK1 is used for the LTI wrapper in other modes than Interlaken
                return 1;
            else

                return 0;
            end if;
        end if;
    end function;

    function NUM_DDR(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 182 or CARD_TYPE = 155 then --For now only implement DDR controller for FLX182 and FLX155. FLX181 has 3 but we don't use them for now.
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_LPDDR(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 120 then
            return 4;
        else
            return 0;
        end if;
    end function;

    function NUM_FAN_FAIL(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 182 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_FF3_PRSTN(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 182 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_IOEXP(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 182 or CARD_TYPE = 155 then
            return 2;
        else
            return 0;
        end if;
    end function;

    function NUM_PCIE_PWRBRK(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 182 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_QSPI_RST(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 182 then
            return 3;
        elsif CARD_TYPE = 155 then
            return 1;
        else
            return 0;
        end if;
    end function;

    function NUM_FAN_PWM(CARD_TYPE: integer) return integer is
    begin
        if CARD_TYPE = 182 then
            return 1;
        else
            return 0;
        end if;
    end function;

end FELIX_package;

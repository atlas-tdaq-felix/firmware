--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Soo Ryu
--!               Israel Grayzman
--!               Kai Chen
--!               Ricardo Luz
--!               Thei Wijnen
--!               Alessandra Camplani
--!               Ohad Shaked
--!               Alexander Paramonov
--!               Ali Skaf
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std_unsigned.all;
    use ieee.numeric_std.all;

library UNISIM;
    use UNISIM.VComponents.all;
library xpm;
    use xpm.vcomponents.all;

    use work.pcie_package.all;
    use work.FELIX_package.all;
--=================================================================================================--
--======================================= Module Body =============================================--
--=================================================================================================--
entity ttc_wrapper is
    generic(
        CARD_TYPE     : integer;
        ISTESTBEAM    : boolean := true;
        ITK_TRIGTAG_MODE : integer := -1;    -- -1: not implemented, 0: RD53A, 1: ITkPixV1, 2: ABC*
        FIRMWARE_MODE : integer := 0;
        GBT_NUM : integer := 24
    );
    port (
        --== ttc fmc interface ==--
        CLK_TTC_P                   : in   std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);           -- ADN2812 CDR 160MHz clock output
        CLK_TTC_N                   : in   std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);           -- ADN2812 CDR 160MHz clock output
        DATA_TTC_P                  : in   std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);           -- ADN2812 CDR Serial Data output
        DATA_TTC_N                  : in   std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);           -- ADN2812 CDR Serial Data output
        TB_trigger                  : in   std_logic_vector(NUM_TB_TRIGGERS(CARD_TYPE)-1 downto 0);
        LOL_ADN                     : in   std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);           -- ADN2812 CDR Loss of Lock output
        LOS_ADN                     : in   std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);           -- ADN2812 CDR Loss of Signal output
        --RESET_N                     : in   std_logic;           -- if low, stop PLL inside wrapper
        register_map_control        : in   register_map_control_type;
        register_map_ttc_monitor    : out  register_map_ttc_monitor_type;
        register_map_control_appreg_clk : in register_map_control_type; --For writing from RM into FIFO for AsyncUserData
        appreg_clk                  : in std_logic;                     --For writing from RM into FIFO for AsyncUserData
        --== to Central Router ==---
        TTC_out                     : out TTC_data_type;

        clk_adn_160                 : out std_logic;
        clk_ttc_40                  : out std_logic;            --40Mhz clock rcovered from the TTC signal

        clk40                       : in std_logic;             --clock source to synchronize TTC_out to the rest of the logic

        BUSY                        : out std_logic;            --TTC decode errors
        cdrlocked_out               : out std_logic;

        TTC_ToHost_Data_out         : out TTC_data_type;
        TTC_BUSY_mon_array          : in array_57b(0 to 23);
        BUSY_IN                     : in std_logic;
        toHostXoff                  : in std_logic_vector(GBT_NUM-1 downto 0);
        LTI_FE_TXUSRCLK_in                 : in  std_logic_vector(GBT_NUM - 1 downto 0);

        LTI_FE_Data_Transceiver_out     : out array_32b(0 to GBT_NUM - 1);
        LTI_FE_CharIsK_out           : out array_4b(0 to GBT_NUM - 1)

    );

end ttc_wrapper;


architecture top of ttc_wrapper is

    signal TTCEmu_TTCout          : std_logic_vector(9 downto 0);
    signal TTCEmu_add_s8          : std_logic_vector(7 downto 0);
    signal TTCEmu_add_strobe      : std_logic;
    signal TTCEmu_add_e           : std_logic;
    signal TTCEMu_add_d8          : std_logic_vector(7 downto 0);


    --signal RESET            : std_logic;
    --========================= Signal Declarations ==========================--
    --signal cdrbad         : std_logic;
    --signal pll_clk        : std_logic;
    --signal pll_locked     : std_logic;
    --====================--
    -- ttc wrapper control
    --====================--

    signal rst_TTCtoHost                : std_logic;
    signal rst_TTCtoHost_40             : std_logic; -- in the TTC clock domain (40 MHz)
    --signal MMCM_MAIN_LCLK_SEL           : std_logic;
    --signal master_BUSY                  : std_logic; -- to throtlle the L1A
    --signal L1A_throttle                 : std_logic;
    signal TTC_BIT_ERR_REG              : std_logic_vector(2 downto 0);

    --==============--
    -- ttc decoder
    --==============--
    signal cdrclk_in                    : std_logic;
    signal cdrdata_in                   : std_logic;
    signal xcdrdata_in                  : std_logic;
    signal l1a                          : std_logic;
    signal l1a_40                       : std_logic;
    --signal l1a_dec                      : std_logic;
    signal l1a_dec_ttcdec               : std_logic;
    signal channelB                     : std_logic;
    signal channelB_ttcdec              : std_logic;
    signal brc_ei, brc_e_40      : std_logic;
    signal brc_ei_ttcdec                : std_logic;
    signal brc_b, brc_bi, brc_b_40      : std_logic;
    signal brc_bi_ttcdec                : std_logic;
    signal brc_t2                       : std_logic_vector(1 downto 0);
    signal brc_t2_ttcdec                : std_logic_vector(1 downto 0);
    signal brc_d4                       : std_logic_vector(3 downto 0);
    signal brc_d4_ttcdec                : std_logic_vector(3 downto 0);
    signal single_bit_error             : std_logic;
    signal single_bit_error_ttcdec      : std_logic;
    signal double_bit_error             : std_logic;
    signal double_bit_error_ttcdec      : std_logic;
    signal communication_error          : std_logic;
    signal communication_error_ttcdec   : std_logic;
    --signal brc_strobe_ttcdec            : std_logic;
    signal add_strobe                   : std_logic;
    signal add_strobe_ttcdec            : std_logic;
    signal add_strobe_40                : std_logic;
    --signal add_a14                      : std_logic_vector(13 downto 0);
    --signal add_a14_ttcdec               : std_logic_vector(13 downto 0);
    signal add_e                        : std_logic;
    signal add_e_ttcdec                 : std_logic;
    signal add_e_40                     : std_logic;
    signal add_s8                       : std_logic_vector(7 downto 0);
    signal add_s8_ttcdec                : std_logic_vector(7 downto 0);
    signal add_s8_40                    : std_logic_vector(7 downto 0);
    signal add_d8                       : std_logic_vector(7 downto 0);
    signal add_d8_ttcdec                : std_logic_vector(7 downto 0);
    signal add_d8_40                    : std_logic_vector(7 downto 0);
    signal ready                        : std_logic;        --- indicates PLL Locked
    signal cdrlocked                    : std_logic; --ready signal synchronized to clk40
    signal ttc_clk_gated                : std_logic;        -- gated 40MHz clock, for comparison only
    signal cdrclk_in_locked             : std_logic;
    signal div_nrst                     : std_logic;
    signal div_nrst_ttcdec              : std_logic;

    signal busy_unsync                  : std_logic;
    signal busy_sync                    : std_logic;

    --======================--
    --- Trigger Tag for ITk
    --======================--
    signal itk_sync : std_logic;
    signal itk_trig : std_logic_vector(3 downto 0);
    signal itk_tag : std_logic_vector(6 downto 0);
    signal itk_ttc2h_tag    : std_logic_vector(7 downto 0);
    signal itk_ttc2h_tag_d  : std_logic_vector(7 downto 0);

    --======================--
    --- TTC-To-Host Design ---
    --======================--
    -- "TT" stands for trigger type

    -- To-Host control registers --
    signal TT_Bch_En_Reg                : std_logic;  -- Enabling this will make use of trigger type information from TTC B channel

    -- To-Host data format and initial values --
    signal BCID                         : std_logic_vector(11 downto 0):= x"000"; --byte2,3
    signal XL1ID                        : std_logic_vector(7 downto 0) := x"00";  --byte4
    signal L1ID                         : std_logic_vector(23 downto 0):= x"000000"; --byte 5,6,7
    signal orbit                        : std_logic_vector(31 downto 0):= x"00000000"; --byte 8,9,10,11
    signal trigger_type                 : std_logic_vector(15 downto 0):= x"0000"; --byte 12,13
    --signal trigger_type_align           : std_logic_vector(15 downto 0); --byte 12,13
    signal L0ID                         : std_logic_vector(37 downto 0):= (others => '0');


    --signal ToHostData           : std_logic_vector(143 downto 0);   -- 144 bit = 20 bytes of ToHostData
    signal ToHostData_in                : std_logic_vector(81 downto 0);   -- 1 bit for indicating an ignored L1 trigger + ToHostData
    signal ToHostData_in_d1             : std_logic_vector(81 downto 0);   -- 1 bit for indicating an ignored L1 trigger + ToHostData
    signal ToHostData_in_d2             : std_logic_vector(81 downto 0);   -- 1 bit for indicating an ignored L1 trigger + ToHostData
    signal ToHostData_in_d3             : std_logic_vector(81 downto 0);   -- 1 bit for indicating an ignored L1 trigger + ToHostData
    signal ToHostData_in_d4             : std_logic_vector(81 downto 0);   -- 1 bit for indicating an ignored L1 trigger + ToHostData
    signal ToHostData_in_d5             : std_logic_vector(81 downto 0);   -- 1 bit for indicating an ignored L1 trigger + ToHostData
    signal ToHostData_in_fifo           : std_logic_vector(89 downto 0);   -- same as above + 8-bit trigger tag
    signal ToHostData_out               : std_logic_vector(89 downto 0);   -- same as just above
    signal ToHostData_tmp               : std_logic_vector(106 downto 0);   -- same as just above
    signal ToHostData_fifo              : std_logic_vector(106 downto 0);   -- same as just above


    --- To-Host Fifo control ---
    signal ToHostData_count             : std_logic_vector(10 downto 0);
    signal ToHostData_full              : std_logic;
    signal ToHostData_empty             : std_logic;
    signal wr_en_ToHostData             : std_logic;
    signal wr_en_ToHostData_d1          : std_logic;
    signal wr_en_ToHostData_d2          : std_logic;
    signal wr_en_ToHostData_d3          : std_logic;
    signal wr_en_ToHostData_d4          : std_logic;
    signal wr_en_ToHostData_d5          : std_logic;
    signal ToHostData_ready             : std_logic;
    signal ToHostData_ready_d1          : std_logic;
    signal ToHostData_ready_d2          : std_logic;
    signal ToHostData_ready_d3          : std_logic;
    signal ToHostData_ready_d4          : std_logic;
    signal ToHostData_ready_d5          : std_logic;
    signal rd_en_ToHostData             : std_logic;

    -- To-Host Monitoring --
    signal TH_FF_EMPTY            : std_logic_vector(0 downto 0);
    signal TH_FF_FULL             : std_logic_vector(0 downto 0);
    signal TH_FF_COUNT            : std_logic_vector(10 downto 0);


    --signal cdrclk_en   : std_logic; --reduced the 160 MHz clock to the TTC-synchronous 40 MHz --not used

    signal Bch_timeout_cnt      : integer range 0 to 511 := 0;
    --signal local_ttc_clk        : std_logic; -- local TTC clock
    --signal TTC_EMU_SEL          : std_logic; --clk domain

    --monitoring counters
    signal ECR_counter      :  std_logic_vector(31 downto 0) := x"00000000";
    signal TTYPE_counter        :  std_logic_vector(31 downto 0) := x"00000000";
    signal BCR_MISMATCH_counter :  std_logic_vector(31 downto 0) := x"00000000";

    signal ECR_cntr_reset_0, ECR_cntr_reset_1 : std_logic;
    signal TTYPE_cntr_reset_0, TTYPE_cntr_reset_1 : std_logic;
    signal  BCR_MISMATCH_cntr_reset_0, BCR_MISMATCH_cntr_reset_1 : std_logic;
    signal BCR_period                         : std_logic_vector(11 downto 0):= x"000";

    signal TTC_BCR_COUNTER :  std_logic_vector(31 downto 0) := x"00000000"; --counts BCR pulses
    signal TTC_BCR_COUNTER_reset :  std_logic := '0'; --retimed reset from the register. rising edge is used to reset the counter
    signal TTC_BCR_COUNTER_reset_d :  std_logic := '0';




    attribute IOB : string;
    attribute IOB of xcdrdata_in: signal is "TRUE";
    attribute ASYNC_REG : string;
    attribute ASYNC_REG of busy_sync: signal is "TRUE";

    signal div_nrst_ttcdec_ttc_clk_gated: std_logic;
    signal single_bit_error_ttcdec_ttc_clk_gated : std_logic;
    signal double_bit_error_ttcdec_ttc_clk_gated : std_logic;
    signal communication_error_ttcdec_ttc_clk_gated : std_logic;
    signal ready_ttc_clk_gated : std_logic;

    --Signals connected to cdc_fifo_din
    signal l1a_dec_ttcdec_ttc_clk_gated : std_logic;
    signal channelB_ttcdec_ttc_clk_gated : std_logic;
    signal add_strobe_ttcdec_ttc_clk_gated : std_logic;
    signal brc_t2_ttcdec_ttc_clk_gated : std_logic_vector(1 downto 0);
    signal brc_d4_ttcdec_ttc_clk_gated : std_logic_vector(3 downto 0);
    signal brc_ei_ttcdec_ttc_clk_gated : std_logic;
    signal brc_bi_ttcdec_ttc_clk_gated : std_logic;
    signal add_e_ttcdec_ttc_clk_gated : std_logic;
    signal add_s8_ttcdec_ttc_clk_gated : std_logic_vector(7 downto 0);
    signal add_d8_ttcdec_ttc_clk_gated : std_logic_vector(7 downto 0);

    signal cdc_fifo_din, cdc_fifo_dout: std_logic_vector(27 downto 0);

    signal Fifo_ToHostData_reclock_rd_en : std_logic;
    signal Fifo_ToHostData_reclock_empty : std_logic;

    signal testbeam_trigger_i : std_logic := '0';
    signal tb_trigger_sync_i : std_logic := '0';    --trigger pulse registered at clk40
    signal old_tb_trigger_sync_i : std_logic := '0';    --trigger pulse registered at
    --clk40, with one clk40
    --delay
    signal count_clk40 : std_logic_vector(3 downto 0) := (others => '0');



    type tb_state_type is (idle, stretch);


    signal tb_state: tb_state_type := idle;--better to have initial value
    signal TTC_out_i : TTC_data_type;



begin

    --=====================--
    -- ttc wrapper control --
    --=====================--

    rst_TTCtoHost   <= to_sl(register_map_control.TTC_DEC_CTRL.TOHOST_RST) or (not div_nrst);


    register_map_ttc_monitor.TTC_DEC_MON.TTC_BIT_ERR <= TTC_BIT_ERR_REG;

    register_map_ttc_monitor.TTC_DEC_MON.TH_FF_FULL  <= TH_FF_FULL;
    register_map_ttc_monitor.TTC_DEC_MON.TH_FF_EMPTY <= TH_FF_EMPTY;
    register_map_ttc_monitor.TTC_DEC_MON.TH_FF_COUNT <= TH_FF_COUNT;

    g_links: for i in 0 to 23 generate
        register_map_ttc_monitor.TTC_BUSY_ACCEPTED(i) <= TTC_BUSY_mon_array(i);
    end generate;
    register_map_ttc_monitor.TTC_DEC_CTRL.BUSY_OUTPUT_STATUS <= (others => BUSY_IN);

    --L1A, ECR and TType, and a check if the BCR period is exactly 3564 BC.
    --
    --   L1A counter can be the value of L1ID + XL1ID as sent to the TTC ToHost.
    --   ECR can be a 32b counter
    --   TType can be a 32b counter (was implemented before but seems commented out)
    --
    --The BCR periodicity should be a check whether a BCR matches a period of 3564 BC, every time there
    --is a mismatch, a second counter should be incremented by one and added to a 32b monitor register.
    register_map_ttc_monitor.TTC_L1ID_MONITOR <= XL1ID & L1ID; --32b
    register_map_ttc_monitor.TTC_ECR_MONITOR.VALUE <= ECR_counter;  --32b  Counts the number of ECRs received from the TTC system
    register_map_ttc_monitor.TTC_TTYPE_MONITOR.VALUE <= TTYPE_counter; --32b Counts the number of TType received from the TTC system
    register_map_ttc_monitor.TTC_BCR_PERIODICITY_MONITOR.VALUE <= BCR_MISMATCH_counter; --32b Counts the number of times the BCR period does not match 3564
    register_map_ttc_monitor.TTC_BCR_COUNTER.VALUE <= TTC_BCR_COUNTER; --32b counter for BCRs from TTC

    ToHostFFMon: process (clk40)
    begin
        if (clk40'event and clk40='1') then

            if (register_map_control.TTC_DEC_CTRL.TOHOST_RST = "1") then --synchronous reset. comes with clk40
                TH_FF_FULL(0) <= '0';
                TH_FF_EMPTY(0) <= '1';
                TH_FF_COUNT <= (others=>'0');
            else
                if (ToHostData_full = '1') then
                    TH_FF_FULL(0) <= '1';
                end if;
                TH_FF_COUNT    <= ToHostData_count;
                TH_FF_EMPTY(0) <= ToHostData_empty;
            end if;

        end if;
    end process;


    --ECR counter for monitoring
    ECR_CNT: process (clk40)
    begin
        if (rising_edge(clk40)) then --40 MHz clock
            ECR_cntr_reset_0 <= to_sl(register_map_control.TTC_ECR_MONITOR.CLEAR);
            ECR_cntr_reset_1 <= ECR_cntr_reset_0;

            if (ECR_cntr_reset_0 = '1' and ECR_cntr_reset_1 = '0') then
                ECR_counter <= (others => '0');
            elsif (brc_e_40 = '1') then
                ECR_counter <= ECR_counter + 1;
            end if;
        end if;
    end process;

    --Trigger TYPE counter for monitoring
    TTYPE_CNT: process (clk40)
    begin
        if (rising_edge(clk40)) then --40 MHz clock
            TTYPE_cntr_reset_0 <= to_sl(register_map_control.TTC_TTYPE_MONITOR.CLEAR);
            TTYPE_cntr_reset_1 <= TTYPE_cntr_reset_0;

            if (TTYPE_cntr_reset_0 = '1' and TTYPE_cntr_reset_1 = '0') then
                TTYPE_counter <= (others => '0');
            elsif ( add_strobe_40 = '1' and add_e_40 = '1' and add_s8_40 = x"00") then --data from long b-channel commands
                TTYPE_counter <= TTYPE_counter + 1;
            end if;
        end if;
    end process;


    --BCR PERIODICITY MONITOR
    -- Counts the number of times the BCR period does not match 3564
    BCR_ERR_CNT: process (clk40)
    begin
        if (rising_edge(clk40)) then --40 MHz clock
            BCR_MISMATCH_cntr_reset_0 <= to_sl(register_map_control.TTC_BCR_PERIODICITY_MONITOR.CLEAR);
            BCR_MISMATCH_cntr_reset_1 <= BCR_MISMATCH_cntr_reset_0;


            if (BCR_MISMATCH_cntr_reset_1 = '0' and BCR_MISMATCH_cntr_reset_0 = '1') then
                BCR_MISMATCH_counter <= (others=>'0');

                --need to update the period counter
                if(brc_b_40 = '1') then
                    BCR_period <= (others=>'0');
                elsif (BCR_period /= "111111111111") then --Do not roll over the couter
                    BCR_period <= BCR_period + 1;
                end if;

            elsif (brc_b_40 = '1') then --check if BCR is arriving on time
                BCR_period <= (others=>'0');
                if (BCR_period /= "110111101011") then  --3564 is the number of buckets in an LHC orbit.
                    BCR_MISMATCH_counter <= BCR_MISMATCH_counter + 1;
                end if;
            elsif (BCR_period /= "111111111111") then --Do not roll over the couter
                BCR_period <= BCR_period + 1;
            else --BCR is not arriving at ALL
                if(BCR_MISMATCH_counter /= x"FFFFFFFF") then
                    BCR_MISMATCH_counter <= BCR_MISMATCH_counter + 1;
                end if;
            end if;


        end if;
    end process;




    --BCR  MONITOR
    -- Counts the number of BCR pulses from TTC
    BCR_CNT: process (clk40)
    begin
        if (rising_edge(clk40)) then --40 MHz clock
            TTC_BCR_COUNTER_reset <= to_sl(register_map_control.TTC_BCR_COUNTER.CLEAR);
            TTC_BCR_COUNTER_reset_d <= TTC_BCR_COUNTER_reset;

            if (TTC_BCR_COUNTER_reset_d = '0' and TTC_BCR_COUNTER_reset = '1') then -- the counter is reset on the rising edge of the register
                TTC_BCR_COUNTER <= (others=>'0');
            elsif (brc_b_40 = '1') then --check if BCR is arriving on time
                TTC_BCR_COUNTER <= TTC_BCR_COUNTER + 1; --the resigter will overflow.
            end if;

        end if; --clock
    end process;



    --L1A throttle is in sync with TTC
    retiming: process(clk40)
    begin
        if (rising_edge(clk40)) then
            rst_TTCtoHost_40 <= rst_TTCtoHost;
            TT_Bch_En_Reg   <= to_sl(register_map_control.TTC_DEC_CTRL.TT_BCH_EN); -- The trigger ID commands are now also supported by the emulator
        end if;
    end process;

    --======================--
    --- Trigger Tag for ITk
    --======================--
    tag_gen_enable: if ITK_TRIGTAG_MODE >= 0 generate
        tag_gen: entity work.itk_trigtag_generator
            generic map (
                MODE => ITK_TRIGTAG_MODE
            )
            port map (
                clk => clk40,
                rst => rst_TTCtoHost_40,
                enc_trig => itk_trig,
                enc_sync => itk_sync,
                enc_tag => itk_tag,
                ttc2h_tag => itk_ttc2h_tag,
                ttc2h_tag_valid => open,
                ttc_l0a =>  l1a or testbeam_trigger_i, -- l1a_40,
                ttc_bcr =>  brc_b--, -- brc_b_40,
            --ttc_phase => "00"           -- TODO: connect to something meaningful in the regbank
            );
    end generate;

    tag_gen_disable: if ITK_TRIGTAG_MODE < 0 generate
        itk_trig <= "0000";
        itk_sync <= '0';
        itk_tag <= (others => '0');
        itk_ttc2h_tag <= (others => '0');
    end generate;

    --==============--
    -- To-Host Data --
    --==============--
    ToHostData_in <= orbit & L0ID & BCID;




    -- used when TT_Bch_En_Reg = '0'
    ToHostData_ready <= l1a_40 when (rst_TTCtoHost_40 = '0') else '0';

    --We are writing into the FIFO on every L1A when TT_Bch_En_Reg='1')
    wr_en_ToHostData <= l1a_40 when TT_Bch_En_Reg='1' else '0';

    -- we have to delay the ToHostData and write-enables by four cycles to synchronize with the output of the trigger tag
    ToHostData_in_d1 <= ToHostData_in when rising_edge(clk40);
    ToHostData_in_d2 <= ToHostData_in_d1 when rising_edge(clk40);
    ToHostData_in_d3 <= ToHostData_in_d2 when rising_edge(clk40);
    ToHostData_in_d4 <= ToHostData_in_d3 when rising_edge(clk40);
    ToHostData_in_d5 <= ToHostData_in_d4 when rising_edge(clk40);
    ToHostData_ready_d1 <= ToHostData_ready when rising_edge(clk40);
    ToHostData_ready_d2 <= ToHostData_ready_d1 when rising_edge(clk40);
    ToHostData_ready_d3 <= ToHostData_ready_d2 when rising_edge(clk40);
    ToHostData_ready_d4 <= ToHostData_ready_d3 when rising_edge(clk40);
    ToHostData_ready_d5 <= ToHostData_ready_d4 when rising_edge(clk40);
    wr_en_ToHostData_d1 <= wr_en_ToHostData when rising_edge(clk40);
    wr_en_ToHostData_d2 <= wr_en_ToHostData_d1 when rising_edge(clk40);
    wr_en_ToHostData_d3 <= wr_en_ToHostData_d2 when rising_edge(clk40);
    wr_en_ToHostData_d4 <= wr_en_ToHostData_d3 when rising_edge(clk40);
    wr_en_ToHostData_d5 <= wr_en_ToHostData_d4 when rising_edge(clk40);




    --The L1ID counter is in the TTC clock domain
    L1ID_gen: process (clk40)
    begin
        if (rising_edge(clk40)) then
            --if (cdrclk_en = '1') then --enable



            if (rst_TTCtoHost_40='1') then --synchronous reset
                rd_en_ToHostData <= '0';
                Bch_timeout_cnt <= 0;
                trigger_type <= (others => '0');

            else


                --From the TTCvi documentation:
                --After each L1A is transmitted, the contents of the 24-bit event/orbit counter isn the TTCvi is broadcast together
                --with an 8-bit trigger type parameter, which is received from the Central Trigger Processor via a front panel connection.
                --This broadcast, which is intended for check purposes, is made asynchronously and takes about 4.4 us if the B Channel is free.
                --The sub-address 0 is used for the 8-bit trigger type.


                --I am using a FWFT fifo
                --rd_en_ToHostData_align <= rd_en_ToHostData; --data from FIFO shows up on the next clock cycle
                --trigger_type_align <= trigger_type;

                if (TT_Bch_En_Reg='1') then  --Outputing the long b-channel trigger type together with the trigger and the counters

                    -- We read from the FIFO on timeout or when we get trigger type from the B-channel (long command)
                    if (ToHostData_empty = '0') then  --I am using a FWFT fifo
                        if ( add_strobe_40 = '1' and add_e_40 = '1' and add_s8_40 = x"00") then --data from long b-channel commands
                            Bch_timeout_cnt <= 0;
                            trigger_type(7 downto 0) <= add_d8_40;
                            rd_en_ToHostData <= '1';
                        elsif (Bch_timeout_cnt = 200) then --timeout
                            Bch_timeout_cnt <= 0;
                            trigger_type(7 downto 0) <= x"00"; --trigger type is not assigned
                            rd_en_ToHostData <= '1';
                        else --counting timeout when the fifo is not empty
                            Bch_timeout_cnt <= Bch_timeout_cnt +1;
                            rd_en_ToHostData <= '0';
                        end if;
                    else
                        rd_en_ToHostData <= '0';
                        Bch_timeout_cnt <= 0;
                    end if;

                end if;

            end if; --reset
        end if; --clock
    end process;

    ToHostData_in_fifo <= itk_ttc2h_tag_d & ToHostData_in_d5;

    itk_ttc2h_tag_d <= itk_ttc2h_tag when rising_edge(clk40);

    Fifo_ToHostData : xpm_fifo_sync
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT"
            DOUT_RESET_VALUE => "0",
            ECC_MODE => "no_ecc",
            FIFO_MEMORY_TYPE => "auto",
            FIFO_READ_LATENCY => 1,
            FIFO_WRITE_DEPTH => 1024,
            FULL_RESET_VALUE => 1,
            PROG_EMPTY_THRESH => 10,
            PROG_FULL_THRESH => 1019,
            RD_DATA_COUNT_WIDTH => 11,
            READ_DATA_WIDTH => 90,
            READ_MODE => "fwft",
            SIM_ASSERT_CHK => 0,
            USE_ADV_FEATURES => "0707",
            WAKEUP_TIME => 0,
            WRITE_DATA_WIDTH => 90,
            WR_DATA_COUNT_WIDTH => 11
        )
        port map (
            sleep => '0',
            rst => rst_TTCtoHost_40,
            wr_clk => clk40,
            wr_en => wr_en_ToHostData_d5,
            din => ToHostData_in_fifo,
            full => ToHostData_full,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => open,
            almost_full => open,
            wr_ack => open,
            rd_en => rd_en_ToHostData,
            dout => ToHostData_out,
            empty => ToHostData_empty,
            prog_empty => open,
            rd_data_count => ToHostData_count,
            underflow => open,
            rd_rst_busy => open,
            almost_empty => open,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );


    --output TTC_ToHost_Data_out is retimed to the local 40 MHz clock domain
    data_mux: process (TT_Bch_En_Reg, ToHostData_out, trigger_type, rd_en_ToHostData, ToHostData_ready_d5, itk_ttc2h_tag_d, ToHostData_in_d5)
    begin
        if(TT_Bch_En_Reg = '1') then -- the data is taken from a FIFO if we are using B-channel to received the trigger type
            ToHostData_tmp(89 downto 0)       <= ToHostData_out(89 downto 0);
            ToHostData_tmp(105 downto 90)     <= trigger_type;
            ToHostData_tmp(106)               <= rd_en_ToHostData;
        else  --the fifo is bypassed if the B-channel is not used
            ToHostData_tmp(81 downto 0)       <= ToHostData_in_d5(81 downto 0);
            ToHostData_tmp(89 downto 82)      <= itk_ttc2h_tag_d;
            ToHostData_tmp(105 downto 90)     <= x"0000";
            ToHostData_tmp(106)               <= ToHostData_ready_d5; --wr_en_ToHostData;
        end if;
    end process;


    --A FIFO to pass TTC data to decoding runing from the local oscillator clocl

    Fifo_ToHostData_reclock : xpm_fifo_async
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT"
            CDC_SYNC_STAGES => 2,
            DOUT_RESET_VALUE => "0",
            ECC_MODE => "no_ecc",
            FIFO_MEMORY_TYPE => "auto",
            FIFO_READ_LATENCY => 1,
            FIFO_WRITE_DEPTH => 16,
            FULL_RESET_VALUE => 0,
            PROG_EMPTY_THRESH => 6,
            PROG_FULL_THRESH => 10,
            RD_DATA_COUNT_WIDTH => 1,
            READ_DATA_WIDTH => 106,
            READ_MODE => "fwft",
            RELATED_CLOCKS => 0,
            SIM_ASSERT_CHK => 0,
            USE_ADV_FEATURES => "1000",
            WAKEUP_TIME => 0,
            WRITE_DATA_WIDTH => 106,
            WR_DATA_COUNT_WIDTH => 1
        )
        port map (
            sleep => '0',
            rst => rst_TTCtoHost,
            wr_clk => clk40,
            wr_en => ToHostData_tmp(106),
            din => ToHostData_tmp(105 downto 0),
            full => open,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => open,
            almost_full => open,
            wr_ack => open,
            rd_clk => clk40,
            rd_en => Fifo_ToHostData_reclock_rd_en,
            dout => ToHostData_fifo(105 downto 0),
            empty => Fifo_ToHostData_reclock_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => open,
            almost_empty => open,
            data_valid => ToHostData_fifo(106),
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );

    Fifo_ToHostData_reclock_rd_en <= not Fifo_ToHostData_reclock_empty;


    TTC_ToHost_Data_out.BCID              <= ToHostData_fifo(11 downto 0);
    TTC_ToHost_Data_out.L0ID              <= ToHostData_fifo(49 downto 12);
    TTC_ToHost_Data_out.OrbitId           <= ToHostData_fifo(81 downto 50);
    TTC_ToHost_Data_out.Itk_Tag           <= ToHostData_fifo(89 downto 82);
    TTC_ToHost_Data_out.TriggerType       <= ToHostData_fifo(105 downto 90);
    TTC_ToHost_Data_out.Itk_trig          <= "0000";
    TTC_ToHost_Data_out.L0A               <= ToHostData_fifo(106);


    TTC_ToHost_Data_out.XL1ID             <= x"00";
    TTC_ToHost_Data_out.L1ID              <= x"000000";

    TTC_ToHost_Data_out.PT                  <= '0';
    TTC_ToHost_Data_out.Partition           <= "00";--: std_logic_vector(1 downto 0);
    TTC_ToHost_Data_out.SyncUserData        <= x"0000";--: std_logic_vector(15 downto 0);
    TTC_ToHost_Data_out.SyncGlobalData      <= x"0000";--: std_logic_vector(15 downto 0);
    TTC_ToHost_Data_out.TS                  <= '0';--: std_logic;
    TTC_ToHost_Data_out.ErrorFlags          <= "0000";--: std_logic_vector(3 downto 0);

    TTC_ToHost_Data_out.SL0ID               <= brc_e_40;--: std_logic;
    TTC_ToHost_Data_out.SOrb                <= brc_b_40;--: std_logic;
    TTC_ToHost_Data_out.Sync                <= '0';--: std_logic;
    TTC_ToHost_Data_out.GRst                <= '0';--: std_logic;
    TTC_ToHost_Data_out.LBID                <= x"0000";--: std_logic_vector(15 downto 0);
    TTC_ToHost_Data_out.AsyncUserData       <= register_map_control.TTC_ASYNCUSERDATA.DATA;--: std_logic_vector(63 downto 0);
    TTC_ToHost_Data_out.CRC                 <= x"0000";--: std_logic_vector(15 downto 0);
    TTC_ToHost_Data_out.D16_2               <= x"50";--: std_logic_vector(7 downto 0);
    TTC_ToHost_Data_out.LTI_decoder_aligned <= '1';--: std_logic;
    TTC_ToHost_Data_out.LTI_CRC_valid       <= '1';--: std_logic;


    --  --this is counting ECRs as errors
    --  L1ID_check: process (clk40)
    --      begin

    --        if (clk40'event and clk40='1') then

    --          if(rst_TTCtoHost='1') then
    --                 L1ID_cnt     <= x"000000";
    --                 L1ID_prev    <= x"000000";
    --                 L1ID_err_cnt <= x"000000";
    --                 L1ID_err     <= '0';
    --           else
    --                 L1ID_prev <= ToHostData_out(63 downto 40);
    --                 L1ID_err  <= '0';
    --                 if(rd_en_ToHostData_align = '1' and L1ID_prev+1 /= ToHostData_out(63 downto 40)) then --This will not work then L1ID is cleared with an ECR
    --                      L1ID_err_cnt <= L1ID_err_cnt + 1;
    --                      L1ID_err <= '1';
    --                 end if;
    --                 if(rd_en_ToHostData_align = '1' and L1ID_prev+1 = ToHostData_out(63 downto 40)) then
    --                      L1ID_cnt <= L1ID_cnt + 1;
    --                 end if;
    --            end if; --reset
    --        end if; --clock
    --      end process;

    --L1ID_err_cnt_o <= L1ID_err_cnt;
    --L1ID_cnt_o     <= L1ID_cnt;
    --L1ID_prev_o    <= L1ID_prev;
    --L1ID_err_o     <= L1ID_err;


    --=====================================--
    -- ttc decoder --
    --=====================================--
    g_NUM_TTC_INPUTS: for i in 0 to NUM_TTC_INPUTS(CARD_TYPE)-1 generate
        signal cdrclk_buf: std_logic;
    begin
        ibuf_ttc_clk : ibufds  -- @suppress "Generic map uses default values"
            port map (I=> CLK_TTC_P(i), IB=> CLK_TTC_N(i), O=> cdrclk_buf); -- @suppress "The order of the associations is different from the declaration order"

        bufg_ttc_clk : bufg
            port map (I=> cdrclk_buf, O => cdrclk_in); -- @suppress "The order of the associations is different from the declaration order"

        -- ttc data
        ibuf_ttc_data : ibufds  -- @suppress
            port map (
                O  => xcdrdata_in,
                I  => DATA_TTC_P(i),
                IB => DATA_TTC_N(i)
            );

        cdrclk_in_locked <= not (LOS_ADN(i) or LOL_ADN(i));

        adn2814_monitor_proc: process(clk40)
        begin
            if rising_edge(clk40) then
                if register_map_control.TTC_CDRLOCK_MONITOR.CLEAR = "1" then
                    register_map_ttc_monitor.TTC_CDRLOCK_MONITOR.ADN_LOL_LATCHED <= "0";
                    register_map_ttc_monitor.TTC_CDRLOCK_MONITOR.ADN_LOS_LATCHED <= "0";
                    register_map_ttc_monitor.TTC_CDRLOCK_MONITOR.CDRLOCK_LOST <= "0";
                end if;
                if LOL_ADN(0) = '1' then
                    register_map_ttc_monitor.TTC_CDRLOCK_MONITOR.ADN_LOL_LATCHED <= "1";
                end if;
                if LOS_ADN(0) = '1' then
                    register_map_ttc_monitor.TTC_CDRLOCK_MONITOR.ADN_LOS_LATCHED <= "1";
                end if;
                if cdrlocked = '0' then
                    register_map_ttc_monitor.TTC_CDRLOCK_MONITOR.CDRLOCK_LOST <= "1";
                end if;
                register_map_ttc_monitor.TTC_CDRLOCK_MONITOR.CDRLOCKED <= (others => cdrlocked);
                register_map_ttc_monitor.TTC_CDRLOCK_MONITOR.ADN_LOL <= (others => LOL_ADN(0));
                register_map_ttc_monitor.TTC_CDRLOCK_MONITOR.ADN_LOS <= (others => LOS_ADN(0));
            end if;
        end process;

    end generate;

    g_TB_TRIGGER_SYNC: if NUM_TB_TRIGGERS(CARD_TYPE) = 1 generate
        process(clk40)
        begin
            if rising_edge(clk40) then
                tb_trigger_sync_i <= TB_trigger(0);
                old_tb_trigger_sync_i <= tb_trigger_sync_i;
            end if;
        end process;
    end generate;

    --SM to stretch trigger for 16 clk40 cycles. Assumption: separation between
    --trigger pulse > 16 clks

    g_bmcmod_testbeam_flag: if ISTESTBEAM  and CARD_TYPE = 712 and FIRMWARE_MODE = FIRMWARE_MODE_BCM_PRIME  generate
        process(clk40)
        begin
            if rising_edge(clk40) then
                if old_tb_trigger_sync_i='1' and tb_trigger_sync_i='0' and testbeam_trigger_i <= '0' then
                    testbeam_trigger_i <= '1';
                else
                    testbeam_trigger_i <= '0';
                end if;
            end if;
        end process;
    end generate;

    testbeam_trig: if ISTESTBEAM  and CARD_TYPE = 712 and FIRMWARE_MODE /= FIRMWARE_MODE_BCM_PRIME generate

        process(clk40)
        begin
            if rising_edge(clk40) then
                case tb_state is
                    when idle =>
                        if tb_trigger_sync_i = '1' then
                            tb_state <= stretch;
                            --            count_clk40 <= count_clk40 + '1';
                            count_clk40 <= (others => '0');
                            testbeam_trigger_i <= '1';
                        else
                            tb_state <= idle;
                            count_clk40 <= (others => '0');
                            testbeam_trigger_i <= '0';
                        end if;
                    --stretch by 15 more clks
                    when stretch =>
                        if (tb_trigger_sync_i = '1') then
                            tb_state <= stretch;
                            count_clk40 <= (others => '0'); --reset counter if another trigger comes before the counter is maxed
                            testbeam_trigger_i <= '1';
                        elsif count_clk40 = X"F" then     --end stretching since no other trigger and counter is maxed
                            tb_state <= idle;
                            count_clk40 <= (others => '0');
                            testbeam_trigger_i <= '0';
                        else                              --keep stretching unntil counter is maxed
                            tb_state <= stretch;
                            count_clk40 <= count_clk40 + 1;
                            testbeam_trigger_i <= '1';
                        end if;
                end case; --the state machine
            end if; --clock
        end process;
    end generate;


    CDR_IN_LATCH : process (cdrclk_in)
    begin
        if (cdrclk_in'event AND (cdrclk_in='1')) then
            cdrdata_in <= xcdrdata_in;
        end if;
    end process;

    clk_adn_160 <= cdrclk_in;
    clk_ttc_40  <= ttc_clk_gated;





    busy_unsync <= single_bit_error or double_bit_error or communication_error;



    BUSY    <= busy_sync;

    sync: process(clk40)
    begin
        if rising_edge(clk40) then
            TTC_BIT_ERR_REG <= double_bit_error & single_bit_error & communication_error;
            busy_sync <= busy_unsync;
        end if;
    end process;

    --=====================================--
    ttc_dec: entity work.ttc_decoder_core
        --=====================================--
        port map
(
            --RESET_N                         => RESET_N_s,
            --== cdr interface ==--
            cdrclk_in_locked => cdrclk_in_locked,
            cdrclk_in => cdrclk_in,
            cdrdata_in => cdrdata_in,
            --== ttc decoder output ==--
            single_bit_error => single_bit_error_ttcdec_ttc_clk_gated,
            double_bit_error => double_bit_error_ttcdec_ttc_clk_gated,
            communication_error => communication_error_ttcdec_ttc_clk_gated,
            l1a => l1a_dec_ttcdec_ttc_clk_gated,
            channelB_o => channelB_ttcdec_ttc_clk_gated,
            brc_strobe => open, --brc_strobe_ttcdec, --not used
            add_strobe => add_strobe_ttcdec_ttc_clk_gated, --used for extracting the trigger ID from the long b-channel commands
            --TTDDDDDEB
            brc_t2 => brc_t2_ttcdec_ttc_clk_gated, -- 2 bit
            brc_d4 => brc_d4_ttcdec_ttc_clk_gated, -- 4 bit
            brc_e => brc_ei_ttcdec_ttc_clk_gated, --ECR
            brc_b => brc_bi_ttcdec_ttc_clk_gated, --BCR
            --AAAAAAAAAAAAAAE1SSSSSSSSDDDDDDDD
            add_a14 => open, --add_a14_ttcdec, --not used
            add_e => add_e_ttcdec_ttc_clk_gated, --used for extracting the trigger ID from the long b-channel commands
            add_s8 => add_s8_ttcdec_ttc_clk_gated, --8 bit used for extracting the trigger ID from the long b-channel commands
            add_d8 => add_d8_ttcdec_ttc_clk_gated, --8 bit used for extracting the trigger ID from the long b-channel commands
            --== ttc decoder aux flags ==--
            ready => ready_ttc_clk_gated, -- the ttc_clk_gated is present
            div_nrst => div_nrst_ttcdec_ttc_clk_gated, --1 when the TTC bistream is aligned and recognied. the TTC decoder core outputs garbage before that happens
            ttc_clk_gated => ttc_clk_gated --,
        --cdrclk_en                       => cdrclk_en
        );

    --Status and reset signals are synchronized using XPM_CDC
    xpm_cdc_single_bit_error: xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        ) port map(
            src_clk => ttc_clk_gated,
            src_in => single_bit_error_ttcdec_ttc_clk_gated,
            dest_clk => clk40,
            dest_out => single_bit_error_ttcdec
        );

    xpm_cdc_double_bit_error: xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        ) port map(
            src_clk => ttc_clk_gated,
            src_in => double_bit_error_ttcdec_ttc_clk_gated,
            dest_clk => clk40,
            dest_out => double_bit_error_ttcdec
        );

    xpm_cdc_communication_error: xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        ) port map(
            src_clk => ttc_clk_gated,
            src_in => communication_error_ttcdec_ttc_clk_gated,
            dest_clk => clk40,
            dest_out => communication_error_ttcdec
        );

    xpm_cdc_ready: xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        ) port map(
            src_clk => ttc_clk_gated,
            src_in => ready_ttc_clk_gated,
            dest_clk => clk40,
            dest_out => ready
        );

    xpm_cdc_div_nrst: xpm_cdc_sync_rst     generic map(
            DEST_SYNC_FF => 2,
            INIT => 0,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0
        )
        port map(
            src_rst => div_nrst_ttcdec_ttc_clk_gated,
            dest_clk => clk40,
            dest_rst => div_nrst_ttcdec
        );

    cdc_fifo_din <= l1a_dec_ttcdec_ttc_clk_gated & --27
                    channelB_ttcdec_ttc_clk_gated & --26
                    add_strobe_ttcdec_ttc_clk_gated & --25
                    brc_t2_ttcdec_ttc_clk_gated & --24..23
                    brc_d4_ttcdec_ttc_clk_gated & --22..19
                    brc_ei_ttcdec_ttc_clk_gated & --18
                    brc_bi_ttcdec_ttc_clk_gated & --17
                    add_e_ttcdec_ttc_clk_gated & --16
                    add_s8_ttcdec_ttc_clk_gated & -- 15 .. 8
                    add_d8_ttcdec_ttc_clk_gated; --7 .. 0


    l1a_dec_ttcdec    <= cdc_fifo_dout(27);
    channelB_ttcdec   <= cdc_fifo_dout(26);
    add_strobe_ttcdec <= cdc_fifo_dout(25);
    brc_t2_ttcdec     <= cdc_fifo_dout(24 downto 23);
    brc_d4_ttcdec     <= cdc_fifo_dout(22 downto 19);
    brc_ei_ttcdec     <= cdc_fifo_dout(18);
    brc_bi_ttcdec     <= cdc_fifo_dout(17);
    add_e_ttcdec      <= cdc_fifo_dout(16);
    add_s8_ttcdec     <= cdc_fifo_dout(15 downto 8);
    add_d8_ttcdec     <= cdc_fifo_dout(7 downto 0);

    xpm_cdc_array_single_inst : xpm_cdc_array_single
        generic map (
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0,
            WIDTH => 28
        )
        port map (
            src_clk => ttc_clk_gated,
            src_in => cdc_fifo_din,
            dest_clk => clk40,
            dest_out => cdc_fifo_dout
        );

    cdrlocked_out <= ready;


    xmp_cdrlocked: xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map (
            dest_out => cdrlocked,
            dest_clk => clk40,
            src_clk => '0',
            src_in => ready
        );

    --TTC emulator
    TTCEmu: entity work.TTC_Emulator
        port map(
            Clock => clk40,
            Reset => rst_TTCtoHost,
            register_map_control => register_map_control,
            add_s8 => TTCEmu_add_s8,
            add_d8 => TTCEMu_add_d8,
            add_strobe => TTCEmu_add_strobe,
            add_e => TTCEmu_add_e,
            TTCout => TTCEmu_TTCout, --      : out std_logic_vector(9 downto 0)
            BUSYIn => BUSY_IN
        );


    --switch between the emulated and TTC data
    ttc_mux: process(register_map_control.TTC_EMU.SEL, single_bit_error_ttcdec, double_bit_error_ttcdec, communication_error_ttcdec, l1a_dec_ttcdec, add_strobe_ttcdec,brc_t2_ttcdec, brc_d4_ttcdec, brc_ei_ttcdec, brc_bi_ttcdec, add_e_ttcdec, add_s8_ttcdec, add_d8_ttcdec, div_nrst_ttcdec, channelB_ttcdec, TTCEmu_TTCout, TTCEmu_add_s8, TTCEmu_add_strobe, TTCEmu_add_e, TTCEMu_add_d8)
    begin
        if (register_map_control.TTC_EMU.SEL = "0") then --signals from the TTC decoder
            single_bit_error <= single_bit_error_ttcdec;
            double_bit_error <= double_bit_error_ttcdec;
            communication_error <= communication_error_ttcdec;
            l1a <= l1a_dec_ttcdec;
            --l1a_dec <= l1a_dec_ttcdec;
            add_strobe <= add_strobe_ttcdec; --used for extracting the trigger ID from the long b-channel commands
            --TTDDDDDEB
            brc_t2 <= brc_t2_ttcdec;
            brc_d4 <= brc_d4_ttcdec;
            brc_ei <= brc_ei_ttcdec; --ECR
            brc_bi <= brc_bi_ttcdec; --BCR
            --AAAAAAAAAAAAAAE1SSSSSSSSDDDDDDDD
            --add_a14 <= (others => '0'); --add_a14_ttcdec; --not used
            add_e <= add_e_ttcdec;  --used for extracting the trigger ID from the long b-channel commands
            add_s8 <= add_s8_ttcdec; --used for extracting the trigger ID from the long b-channel commands
            add_d8 <= add_d8_ttcdec; --used for extracting the trigger ID from the long b-channel commands
            --== ttc decoder aux flags ==--
            --ready <= ready_ttcdec;
            div_nrst <= div_nrst_ttcdec; --1 when the TTC bistream is aligned and recognied. the TTC decoder core outputs garbage before that happens
            channelB <= channelB_ttcdec;

        else --emulated signals ---------------------------------------------------------------
            single_bit_error <= '0';
            double_bit_error <= '0';
            communication_error <= '0';
            l1a <= TTCEmu_TTCout(0);
            --l1a_dec <= TTCEmu_TTCout(0);
            add_strobe <= TTCEmu_add_strobe; --used for extracting the trigger ID from the long b-channel commands
            --TTDDDDDEB
            brc_t2 <= TTCEmu_TTCout(9 downto 8);
            brc_d4 <= TTCEmu_TTCout(7 downto 4);
            brc_ei <= TTCEmu_TTCout(3); --ECR
            brc_bi <= TTCEmu_TTCout(2); --BCR
            --AAAAAAAAAAAAAAE1SSSSSSSSDDDDDDDD
            --add_a14 <= "00000000000000"; --not used
            add_e   <= TTCEmu_add_e;  --used for extracting the trigger ID from the long b-channel commands
            add_s8  <= TTCEmu_add_s8; --used for extracting the trigger ID from the long b-channel commands
            add_d8  <= TTCEMu_add_d8;  --used for extracting the trigger ID from the long b-channel commands
            --== ttc decoder aux flags ==--
            --ready <='1';
            div_nrst <= '1'; --1 when the TTC bistream is aligned and recognized.
            channelB <= TTCEmu_TTCout(1);
        end if;
    end process;




    to_ttc_type0: entity work.ttc10b_to_ttc_type port map(
            register_map_control_appreg_clk => register_map_control_appreg_clk,
            register_map_control => register_map_control,
            appreg_clk => appreg_clk,
            rst_TTCtoHost_40 => rst_TTCtoHost_40,
            clk40 => clk40,
            TTC_out => TTC_out_i,
            testbeam_trigger_i => testbeam_trigger_i,
            l1a => l1a,
            channelB => channelB,
            brc_bi => brc_bi,
            brc_ei => brc_ei,
            brc_d4 => brc_d4,
            brc_t2 => brc_t2,
            add_strobe => add_strobe,
            add_e => add_e,
            add_s8 => add_s8,
            add_d8 => add_d8,
            itk_sync => itk_sync,
            itk_trig => itk_trig,
            itk_tag => itk_tag,
            BCID => BCID,
            L0ID => L0ID,
            L1ID => L1ID,
            XL1ID => XL1ID,
            brc_b_40 => brc_b_40,
            brc_b => brc_b,
            brc_e_40 => brc_e_40,
            orbit => orbit,
            l1a_40 => l1a_40,
            add_strobe_40 => add_strobe_40,
            add_e_40 => add_e_40,
            add_s8_40 => add_s8_40,
            add_d8_40 => add_d8_40
        );

    TTC_out <= TTC_out_i;
    g_TTC_LTI: if FIRMWARE_MODE = FIRMWARE_MODE_INTERLAKEN or
                FIRMWARE_MODE = FIRMWARE_MODE_FULL generate
        signal daq_fifo_flush: std_logic;
    begin
        daq_fifo_flush <= register_map_control.TTC_EMU_RESET(64) or register_map_control.CRFROMHOST_RESET(64);

        TTC_LTI_0: entity work.LTI_FE_Transmitter generic map(
                GBT_NUM => GBT_NUM
            )
            port map(
                clk40 => clk40,
                daq_fifo_flush => daq_fifo_flush,
                TTCin => TTC_out_i,
                XoffIn => toHostXoff(GBT_NUM-1 downto 0),
                LTI_TX_Data_Transceiver => LTI_FE_Data_Transceiver_out,
                LTI_TX_TX_CharIsK => LTI_FE_CharIsK_out,
                LTI_TXUSRCLK_in => LTI_FE_TXUSRCLK_in
            );

    end generate g_TTC_LTI;


end top;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Soo Ryu
--!               Israel Grayzman
--!               Kai Chen
--!               Ricardo Luz
--!               Thei Wijnen
--!               Alessandra Camplani
--!               Ohad Shaked
--!               Alexander Paramonov
--!               Ali Skaf
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std_unsigned.all;
    use ieee.numeric_std.all;

library UNISIM;
    use UNISIM.VComponents.all;
library xpm;
    use xpm.vcomponents.all;

    use work.pcie_package.all;
    use work.FELIX_package.all;
--=================================================================================================--
--======================================= Module Body =============================================--
--=================================================================================================--
entity ttc10b_to_ttc_type is
    port (
        register_map_control_appreg_clk : in register_map_control_type; --For writing from RM into FIFO for AsyncUserData
        register_map_control : in register_map_control_type; --synchronized to clk40
        appreg_clk                  : in std_logic;                     --For writing from RM into FIFO for AsyncUserData
        rst_TTCtoHost_40            : in std_logic;
        clk40                       : in std_logic;             --clock source to synchronize TTC_out to the rest of the logic
        TTC_out                     : out TTC_data_type;
        testbeam_trigger_i : in std_logic;
        l1a : in std_logic;
        channelB : in std_logic;
        brc_bi : in std_logic;
        brc_ei : in std_logic;
        brc_d4: in std_logic_vector(3 downto 0);
        brc_t2: in std_logic_vector(1 downto 0);
        add_strobe : in std_logic;
        add_e : in std_logic;
        add_s8 : in std_logic_vector(7 downto 0);
        add_d8 : in std_logic_vector(7 downto 0);
        itk_sync : in std_logic;
        itk_trig : in std_logic_vector(3 downto 0);
        itk_tag : in std_logic_vector(6 downto 0);
        BCID : out std_logic_vector(11 downto 0);
        L0ID : out std_logic_vector(37 downto 0);
        L1ID : out std_logic_vector(23 downto 0);
        XL1ID : out std_logic_vector(7 downto 0);
        brc_b_40 : out std_logic;
        brc_b    : out std_logic;
        brc_e_40 : out std_logic;
        orbit : out std_logic_vector(31 downto 0);
        l1a_40 : out std_logic;
        add_strobe_40 : out std_logic;
        add_e_40 : out std_logic;
        add_s8_40 : out std_logic_vector(7 downto 0);
        add_d8_40 : out std_logic_vector(7 downto 0)

    );

end ttc10b_to_ttc_type;

architecture rtl of ttc10b_to_ttc_type is
    signal brc_e, brc_b_s: std_logic;
    signal AsyncUserData : std_logic_vector(63 downto 0);
    signal AsyncUserData_wr_en : std_logic;
    signal AsyncUserData_full : std_logic;
    signal TTC_ASYNCUSERDATA_WR_EN_p1 : std_logic;
    signal AsyncUserData_rd_en : std_logic;
    signal AsyncUserData_empty : std_logic;
    signal AsyncUserData_dout : std_logic_vector(63 downto 0);
    signal TTC_out_p1 : TTC_data_type;
    signal xTTCin: std_logic_vector(28 downto 0); --Original TTC signals extended with "extended test pulse"
    signal xTTCin_delayed: std_logic_vector(28 downto 0); --Delayed (Using SRL16E) version of xTTCin
    signal l0a_shiftreg: std_logic_vector(63 downto 0);

    signal TTC_DELAY  : std_logic_vector(3 downto 0);
    signal ExtendedTestPulse : std_logic;
    signal TTC_Out_unsync               : std_logic_vector(27 downto 0);
    signal TTC_Out_sync                 : std_logic_vector(27 downto 0);
    signal TTC_out_delayed              : std_logic_vector(27 downto 0);
    attribute ASYNC_REG : string;
    attribute ASYNC_REG of TTC_Out_sync: signal is "TRUE";
    signal Brcst_latched            : std_logic_vector(5 downto 0) := "000000"; -- needed for the NSW. Controlled with the long commands

    signal BCID_s : std_logic_vector(11 downto 0);

    signal brcst7_40                    : std_logic;
    signal brc_b_40_s                   : std_logic;
    signal brc_e_40_s                   : std_logic;
    signal XL1ID_RST                    : std_logic;  -- Reset XL1ID
    signal XL1ID_RST_latch              : std_logic;  -- delayed Reset XL1ID
    signal XL1ID_SW                     : std_logic_vector(7 downto 0);  --XL1ID is set to this value when reset
    signal BCID_reg                     : std_logic_vector(11 downto 0):= x"000"; -- BCID is set to this when reset

    --needed for LAr; TODO: this needs to be done inside the central router to avoid confusing all the other pieces of the design that use TTC
    signal ECR_BCR_SWAP           : std_logic;
    signal dbw_bcr                      : std_logic;
    signal orbit_reset_latch            : std_logic;

    signal XL1ID_s                        : std_logic_vector(7 downto 0) := x"00";  --byte4
    signal L1ID_s                         : std_logic_vector(37 downto 0):= (others => '0'); --byte 5,6,7)
    signal orbit_s                        : std_logic_vector(31 downto 0):= x"00000000"; --byte 8,9,10,11
    signal l1a_40_s                       : std_logic;
    --signal brc_t2_d4_40                 : std_logic_vector(5 downto 0);
    signal add_d8_40_s                    : std_logic_vector(7 downto 0);
    signal add_e_40_s : std_logic;
    signal add_s8_40_s                    : std_logic_vector(7 downto 0);


    signal add_strobe_40_s : std_logic;
    signal trigger_type : std_logic_vector(7 downto 0) := (others => '0');
    signal sorb: std_logic;


begin

    TTC_Out_unsync(0)        <= testbeam_trigger_i or l1a;
    TTC_Out_unsync(1)        <= channelB;
    TTC_Out_unsync(2)        <= brc_b_s; --BCR
    TTC_Out_unsync(3)        <= brc_e; --ECR
    TTC_Out_unsync(4)        <= brc_d4(0);
    TTC_Out_unsync(5)        <= brc_d4(1);
    TTC_Out_unsync(6)        <= brc_d4(2);
    TTC_Out_unsync(7)        <= brc_d4(3);
    TTC_Out_unsync(8)        <= brc_t2(0);
    TTC_Out_unsync(9)        <= brc_t2(1);

    TTC_Out_unsync(15 downto 10) <= Brcst_latched;

    TTC_Out_unsync(16)           <= itk_sync;
    TTC_Out_unsync(20 downto 17) <= itk_trig;
    TTC_Out_unsync(27 downto 21) <= itk_tag;

    sync: process(clk40)
    begin
        if rising_edge(clk40) then
            --! Implement the behaviour of the TTC data switch. TTCToHost data still needs to be derived from the right source
            TTC_Out_sync(1 downto 0) <= TTC_Out_unsync(1 downto 0); --l1a and b-channel have constant delay
            TTC_Out_sync(27 downto 16) <= TTC_Out_unsync(27 downto 16); -- ITk signals also with constant delay
        end if;
    end process;

    GEN_SHIFT:  for I in 2 to 15 generate
        SRL16E_2 : SRL16E
            generic map (
                INIT => X"0000",        -- Initial contents of shift register
                IS_CLK_INVERTED => '0'  -- Optional inversion for CLK
            )
            port map (
                Q => TTC_out_delayed(I), -- 1-bit output: SRL Data
                -- Depth Selection inputs: A0-A3 select SRL depth
                --the minimal length of the shift register is 1 clock  cycle
                A0 => register_map_control.TTC_DEC_CTRL.B_CHAN_DELAY(27),
                A1 => register_map_control.TTC_DEC_CTRL.B_CHAN_DELAY(28),
                A2 => register_map_control.TTC_DEC_CTRL.B_CHAN_DELAY(29),
                A3 => register_map_control.TTC_DEC_CTRL.B_CHAN_DELAY(30),
                CE => '1', -- 1-bit input: Clock enable
                CLK => clk40, -- 1-bit input: Clock
                D => TTC_Out_unsync(I) -- 1-bit input: SRL Data

            );
    end generate GEN_SHIFT;

    ttcn: process(clk40)
    begin
        if clk40'event and clk40 = '1' then
            xTTCin(28 downto 17) <= TTC_Out_sync(27 downto 16);
            xTTCin(15 downto 2) <= TTC_out_delayed(15 downto 2);
            xTTCin(1 downto 0) <= TTC_Out_sync(1 downto 0);
        end if;
    end process;

    ExtendingTestPulse: entity work.ExtendedTestPulse (Behavioral)
        port map(
            clk40                   => clk40,
            reset                 => rst_TTCtoHost_40,

            TTCin_TestPulse        => TTC_out_delayed(4),
            ExtendedTestPulse    => ExtendedTestPulse
        );

    xTTCin(16)    <= ExtendedTestPulse; -- the ExtendedTestPulse already set in one clock delay, like in the "ttcn" process above

    -- If you want to generate a per-GBT (per front end) delay, that
    --  can be done here, but only in steps of the 40MHz clock.
    --
    --  If used connect DlydTTCin_array to upstream instead of TTCin_array.
    TTC_DELAY <= register_map_control.TTC_DELAY;



    --IG ttcFanDly : for J in 0 to 9 generate
    ttcFanDly : for J in 0 to 28 generate --IG: add another bit to support the extended Test Pulse
        signal ttc_delay_shiftreg: std_logic_vector(14 downto 0);
    begin
        shifreg_proc: process(clk40)
        begin
            if rising_edge(clk40) then
                ttc_delay_shiftreg(0) <= xTTCin(J);
                for pipe in 1 to 14 loop
                    ttc_delay_shiftreg(pipe) <= ttc_delay_shiftreg(pipe-1);
                end loop;
            end if;
        end process;

        with TTC_DELAY select xTTCin_delayed(J) <= xTTCin(J) when x"0",
                                           ttc_delay_shiftreg(0) when x"1",
                                           ttc_delay_shiftreg(1) when x"2",
                                           ttc_delay_shiftreg(2) when x"3",
                                           ttc_delay_shiftreg(3) when x"4",
                                           ttc_delay_shiftreg(4) when x"5",
                                           ttc_delay_shiftreg(5) when x"6",
                                           ttc_delay_shiftreg(6) when x"7",
                                           ttc_delay_shiftreg(7) when x"8",
                                           ttc_delay_shiftreg(8) when x"9",
                                           ttc_delay_shiftreg(9) when x"A",
                                           ttc_delay_shiftreg(10) when x"B",
                                           ttc_delay_shiftreg(11) when x"C",
                                           ttc_delay_shiftreg(12) when x"D",
                                           ttc_delay_shiftreg(13) when x"E",
                                           ttc_delay_shiftreg(14) when x"F",
                                           xTTCin(J) when others;

    end generate ttcFanDly;


    l0a_shift_proc: process(clk40)
    begin
        if rising_edge(clk40) then
            l0a_shiftreg(63 downto 1) <= l0a_shiftreg(62 downto 0); --FLX-2069: Make a shift register with a delayed version of L1A to feed into TTC_out(16)
        end if;
    end process;

    l0a_shiftreg(0) <= xTTCin_delayed(0);


    BCID_reg        <= register_map_control.TTC_DEC_CTRL.BCID_ONBCR; -- BCID is set to this when reset
    XL1ID_SW        <= register_map_control.TTC_DEC_CTRL.XL1ID_SW;  --XL1ID is set to this value when reset


    --ECR and BCR are swapped in the LAr TTC system
    ECR_BCR_SWAP <= to_sl(register_map_control.TTC_DEC_CTRL.ECR_BCR_SWAP);
    brc_e <= brc_ei when (ECR_BCR_SWAP = '0') else brc_bi; --ECR by default
    brc_b_s <= brc_bi when (ECR_BCR_SWAP = '0') else brc_ei; --BCR by default

    --The counter is synchronous with the TTC clock
    BCID_gen: process (clk40)
    begin
        if (rising_edge(clk40)) then
            brc_b_40_s <= brc_b_s;

            if (rst_TTCtoHost_40 = '1') then
                BCID_s <= (others=>'0');
            else
                if (BCID_s = "110111101011" and brc_b_40_s = '0') then  --3564 is the number of buckets in an LHC orbit.
                    BCID_s <= (others=>'0');    --reset the orbit counter if needed
                elsif (brc_b_40_s = '1') then
                    BCID_s <= BCID_reg;
                else
                    BCID_s <= BCID_s + 1;
                end if;
            end if;

        end if;
    end process;

    BCID <= BCID_s;
    brc_b <= brc_b_s;
    brc_b_40 <= brc_b_40_s;


    --Orbit counter is reset whel BCID rolls through 0. The orgibt counter is also incremented when BCID rolls through 0.
    --The counter is synchronous with the TTC clock
    --"OCR means reset the orbit counter on the next rollover of the BC counter"
    Orbit_gen: process (clk40)
    begin
        if (rising_edge(clk40)) then

            brcst7_40 <= brc_t2(1);

            --"A  double-width BCR is scheduled by sensing Brcst7 to indicate that the next BCR should be double-width."
            if(brcst7_40 = '1' and brc_b_40 = '1') then
                dbw_bcr <= '0';
                orbit_reset_latch <= '1';
            elsif (brcst7_40 = '1' and brc_b_40 = '0') then
                dbw_bcr <= '1';
            elsif (dbw_bcr = '1' and brc_b_40 = '1') then
                dbw_bcr <= '0';
                orbit_reset_latch <= '1';
            end if;
            sorb <= '0';

            if (rst_TTCtoHost_40 = '1') then
                orbit_s <= (others=>'0');
            else
                if (BCID_s = "110111101011") then -- 3564 is the number of buckets in an LHC orbit.
                    --reset the orbit counter if needed or increment

                    if(orbit_reset_latch = '1') then
                        orbit_s <= (others=>'0');
                        orbit_reset_latch <= '0';
                        sorb <= '1';
                    else
                        orbit_s <= orbit_s + 1;
                    end if;

                end if;
            end if;

        end if;
    end process;

    orbit <= orbit_s;

    XL1ID_RST       <= to_sl(register_map_control.TTC_DEC_CTRL.XL1ID_RST);
    --All the counters are using the TTC clock
    --XL1ID incermented in ECR and cleared with a PCIe register
    --ECR and BCR are transmitted in short broadcast commands.
    XL1ID_gen: process (clk40)
    begin
        if (rising_edge(clk40)) then
            XL1ID_RST_latch <= XL1ID_RST;
            brc_e_40_s <= brc_e;

            if (rst_TTCtoHost_40 = '1') then -- synchronous reset
                XL1ID_s <= XL1ID_SW;
            else

                if (XL1ID_RST = '1' and XL1ID_RST_latch = '0' ) then  -- XL1ID reset
                    XL1ID_s     <= XL1ID_SW;
                elsif (brc_e_40_s = '1') then --XL1ID incermented in ECR and cleared with a PCIe register
                    XL1ID_s <= XL1ID_s+ 1;
                end if;

            end if;
        end if;
    end process;

    brc_e_40 <= brc_e_40_s;

    XL1ID <= XL1ID_s;



    --The L1ID counter is in the TTC clock domain
    L1ID_gen: process (clk40)
    begin
        if (rising_edge(clk40)) then
            --if (cdrclk_en = '1') then --enable

            l1a_40_s <= l1a or testbeam_trigger_i;

            --brc_t2_d4_40 <= brc_t2 & brc_d4;
            --brc_d4_40 <= brc_d4;

            add_strobe_40_s <= add_strobe;
            add_e_40_s <= add_e;
            add_s8_40_s <= add_s8;
            add_d8_40_s <= add_d8;

            if (rst_TTCtoHost_40='1') then --synchronous reset
                L1ID_s <= (others => '0');
                Brcst_latched <= "000000";

            else

                if (brc_e_40_s = '1') then --L1ID is cleared on ECRs and incremented on L1ID
                    L1ID_s  <= (others=>'0');
                elsif (l1a_40_s = '1') then --L1ID counter
                    L1ID_s  <= L1ID_s + 1;
                end if;

                --setting the latched bits for the NSW
                --the address is required to be 0 and the sub-address is  0x04
                --E=0 accesses TTCrx internal registers. E=1 is for access to external subaddresses in the associated front-end electronics
                if ( add_strobe_40_s = '1' and add_e_40_s = '1' and add_s8_40_s = x"04") then
                    if(add_d8_40_s(6) = '1') then
                        Brcst_latched <= Brcst_latched or add_d8_40_s(5 downto 0);
                    else
                        Brcst_latched <= Brcst_latched and (not add_d8_40_s(5 downto 0));
                    end if;
                end if;

                if ( add_strobe_40 = '1' and add_e_40 = '1' and add_s8_40 = x"00") then --data from long b-channel commands
                    trigger_type(7 downto 0) <= add_d8_40;
                end if;

            end if; --reset
        end if; --clock
    end process;

    l1a_40  <= l1a_40_s;
    add_e_40 <= add_e_40_s;
    add_strobe_40 <= add_strobe_40_s;
    add_s8_40 <= add_s8_40_s;
    add_d8_40 <= add_d8_40_s;
    L1ID <= L1ID_s(23 downto 0);
    L0ID <= L1ID_s;

    TTC_out.L0A <= xTTCin_delayed(0);
    TTC_out.Bchan <= xTTCin_delayed(1);
    TTC_out.BCR <= xTTCin_delayed(2);
    TTC_out.ECR <= xTTCin_delayed(3);
    TTC_out.Brcst <= xTTCin_delayed(9 downto 4);
    TTC_out.Brcst_latched <= xTTCin_delayed(15 downto 10);
    TTC_out.ExtendedTestPulse <= xTTCin_delayed(16);
    TTC_out.L1Id <= L1ID;
    TTC_out.ITk_sync <= xTTCin_delayed(17);
    TTC_out.ITk_trig <= xTTCin_delayed(21 downto 18);
    TTC_out.ITk_tag <= '0'&xTTCin_delayed(28 downto 22);
    TTC_out.L1A <= l0a_shiftreg(to_integer(unsigned(register_map_control.TTC_L1A_DELAY)));

    TTC_out.BCID <= BCID;
    TTC_out.TriggerType <= x"00" & trigger_type;
    TTC_out.LBID <= (others => '0');
    TTC_out.OrbitID <= orbit;
    TTC_out.L0ID(37 downto 0) <= L1ID_s;
    TTC_out.SyncGlobalData <= (others => '0');
    TTC_out.TS <= xTTCin_delayed(2);
    TTC_out.ErrorFlags   <= (others => '0');
    TTC_out.SL0ID        <= xTTCin_delayed(3);
    TTC_out.SOrb         <= sorb;
    TTC_out.Sync         <= '0';
    TTC_out.GRst         <= '0';
    TTC_out.PT           <= '0';
    TTC_out.Partition    <= (others => '0');
    TTC_out.SyncUserData <= (others => '0');
    TTC_out.AsyncUserData <= AsyncUserData;

    TTC_out.XOFF <= '0';
    TTC_out.CRC <= x"0000"; --should be recalculated on redistribution
    TTC_out.D16_2 <= x"50";
    TTC_out.LTI_CRC_valid <= '1';
    TTC_out.LTI_decoder_aligned <= '1';



    --Single shot write enable into fifo on the register map clock
    process(appreg_clk)
    begin
        if rising_edge(appreg_clk) then
            TTC_ASYNCUSERDATA_WR_EN_p1 <= register_map_control_appreg_clk.TTC_ASYNCUSERDATA.WR_EN(64);
        end if;
    end process;

    AsyncUserData_wr_en <= not AsyncUserData_full and (register_map_control_appreg_clk.TTC_ASYNCUSERDATA.WR_EN(64) and (not TTC_ASYNCUSERDATA_WR_EN_p1));

    fifo_AsyncUserData: xpm_fifo_async generic map( -- @suppress "Generic map uses default values. Missing optional actuals: EN_SIM_ASSERT_ERR"
            FIFO_MEMORY_TYPE => "auto",
            FIFO_WRITE_DEPTH => 16,
            CASCADE_HEIGHT => 0,
            RELATED_CLOCKS => 0,
            WRITE_DATA_WIDTH => 64,
            READ_MODE => "fwft",
            FIFO_READ_LATENCY => 0,
            FULL_RESET_VALUE => 1,
            USE_ADV_FEATURES => "0000",
            READ_DATA_WIDTH => 64,
            CDC_SYNC_STAGES => 2,
            WR_DATA_COUNT_WIDTH => 4,
            PROG_FULL_THRESH => 12,
            RD_DATA_COUNT_WIDTH => 4,
            PROG_EMPTY_THRESH => 2,
            DOUT_RESET_VALUE => "0",
            ECC_MODE => "no_ecc",
            SIM_ASSERT_CHK => 0,
            WAKEUP_TIME => 0
        )
        port map(
            sleep => '0',
            rst => rst_TTCtoHost_40,
            wr_clk => appreg_clk,
            wr_en => AsyncUserData_wr_en,
            din => register_map_control_appreg_clk.TTC_ASYNCUSERDATA.DATA,
            full => AsyncUserData_full,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => open,
            almost_full => open,
            wr_ack => open,
            rd_clk => clk40,
            rd_en => AsyncUserData_rd_en,
            dout => AsyncUserData_dout,
            empty => AsyncUserData_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => open,
            almost_empty => open,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );

    AsyncUserData_proc: process(AsyncUserData_empty, AsyncUserData_dout, TTC_out, TTC_out_p1)
        variable frame_type: std_logic;
    begin
        if (
            TTC_out.SL0ID = '1' or
            TTC_out.SOrb = '1' or
            TTC_out.Sync = '1' or
            TTC_out.GRst = '1' or
            TTC_out.L0A = '1' or
            TTC_out.L0ID /= TTC_out_p1.L0ID or
            TTC_out.OrbitID /= TTC_out_p1.OrbitID or
            TTC_out.TriggerType /= TTC_out_p1.TriggerType or
            TTC_out.LBID /= TTC_out_p1.LBID
        ) then
            frame_type := '0';
        else
            frame_type := '1';
        end if;
        if frame_type = '1' and AsyncUserData_empty = '0' then
            AsyncUserData <= AsyncUserData_dout;
            AsyncUserData_rd_en <= '1';
        else
            AsyncUserData <= x"0000_0000_0000_0000";
            AsyncUserData_rd_en <= '0';
        end if;
    end process;

    AsyncUserData_pipeline: process(clk40)
    begin
        if rising_edge(clk40) then
            TTC_out_p1 <= TTC_out;
        end if;
    end process;

end architecture rtl;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Soo Ryu
--!               Alexander Paramonov
--!               Frans Schreuder
--!               Thei Wijnen
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

-------------------------------------------------------------------------------
--  Argonne National Laboratory
-------------------------------------------------------------------------------
--  Filename: FELIX_busy_OR.vhd
--  Title: Collector for BUSYs in FELIX, adapted from Digital Gammasphere

--
--  This file implements some number of copies of FELIX_busy_OR plus one
--  copy of FELIX_BUSY_LIMIT_TIMER.  The ACCEPTED_BUSY signals from all the copies
--  of FELIX_busy_OR are ORed together to make ANY_BUSY.  A digital one-shot
--  ensures a minimum assertion time for ANY_BUSY.
--
--  Per email received 20160511 from Julia Narevicius, the BUSY inputs from the
--  central router of FELIX are structured as follows:

--  for each GBT channel we have 57-bit busy status vector:
--  busyOut <=
--          EPATH_BUSY_array(7)(7) &  (JTA: a single bit)
--          EPATH_BUSY_array(6) &    (JTA: eight-bit vector)
--          EPATH_BUSY_array(5) &
--          EPATH_BUSY_array(4) &
--          EPATH_BUSY_array(3) &
--          EPATH_BUSY_array(2) &
--          EPATH_BUSY_array(1) &
--          EPATH_BUSY_array(0);
--
--  where
--  EPATH_BUSY_array(7)(7) is a busy status of EC e-link
--  EPATH_BUSY_array(6) is an 8-bit vector, each bit represents a busy status of a corresponding Epath for Egroup 6 �
--  EPATH_BUSY_array(0) is an 8-bit vector, each bit represents a busy status of a corresponding Epath for Egroup 0

--
-------------------------------------------------------------------------------
-- Author:   John Anderson
-- revised by A. Paramonov <alexander.paramonov@cern.ch> January 2018
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  Library Declaration
-------------------------------------------------------------------------------

library ieee, UNISIM;
    --    use work.lpgbtfpga_package.all;
    use ieee.numeric_std.all;
    use UNISIM.VCOMPONENTS.all;
    use ieee.numeric_std_unsigned.all;
    use ieee.std_logic_1164.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;

-------------------------------------------------------------------------------
--  PORT DECLARATION
-------------------------------------------------------------------------------

entity ttc_busy is
    generic (    GBT_NUM  : integer := 1 );
    port (
        mclk  :  in std_logic;  --master clock; 40 MHz
        mrst  :  in std_logic;  --global reset; registered with the 40 MHz clock in register_map_synch.
        -- control comes from the wupper register map
        -- register_map_control.TTC_BUSY_TIMING_CTRL
        -- has been defined as of 20161025.  This register has the sub-fields
        --    .PRESCALE   (20 bit std logic vector)
        --    .BUSY_WIDTH  (16 bit std logic vector)
        --    .LIMIT_TIME (16 bit std logic vector)
        register_map_control            : in     register_map_control_type;
        -- actual busy requests from all E-links in central router
        BUSY_REQUESTs : in array_57b (0 to (GBT_NUM-1));  --active high busy request of unknown duration
        --BUSY_TTC : in std_logic;
        --stretched OR of all collected BUSYs
        ANY_BUSY_REQ_OUT   : out std_logic;
        TTC_BUSY_mon_array : out array_57b(0 to GBT_NUM-1);
        DMA_BUSY_in        : in std_logic;
        FIFO_BUSY_in       : in std_logic;
        BUSY_INTERRUPT_out : out std_logic
    );
end ttc_busy;

architecture rtl of ttc_busy is

    -- controls for clock prescaling logic
    --signal PRESCALE_VALUE : std_logic_vector(19 downto 0);
    -- controls for limiter logic
    signal TEST_MODEs : std_logic_vector(GBT_NUM downto 1);    --if set, can manually drive busy low or high for testing
    signal TEST_VALUEs : std_logic_vector(GBT_NUM downto 1);    --the value BUSY should have in test mode
    signal BUSY_LIMIT_TIME : std_logic_vector(15 downto 0);        --sets time that BUSY must continuously be asserted before accepted
    --this is a multiple of the period of PRESCALED_CLOCK_ENABLE.
    signal ANY_BUSY_WIDTH_REG : std_logic_vector(15 downto 0);    --how wide to make the final output pulse (minimum)


    --signal PRESCALED_CLOCK_ENABLE : std_logic;    --connects output of limit timer to all limiter machines
    signal ACCEPTED_BUSY_OUTs : array_57b (0 to (GBT_NUM-1));  --BUSYs from each limited if passed by limit logic

    --signal LIMITER_STATE_MON_ARR : busyOut_SM_array_type (0 to (GBT_NUM-1));

    signal ANY_BUSY_COUNT : std_logic_vector(15 downto 0);      -- counter for ANY BUSY output
    --signal ANY_BUSY_REQUEST : std_logic;

    signal GBT_Busy : std_logic_vector((GBT_NUM-1) downto 0);  --first rank OR, per GBT
    signal ANY_GBT_BUSY : std_logic;
    signal BUSY_CLEAR : std_logic;

    constant VAR_SIZE_ZERO : std_logic_vector((GBT_NUM-1) downto 0) := (others => '0');
    signal SW_BUSY : std_logic := '0';

    signal internal_BUSY_mon_array : array_57b(0 to (GBT_NUM-1)) := (others => ('0' & x"00000000000000"));

    signal ACCEPTED_BUSY_OUTs_FIFO, ACCEPTED_BUSY_OUTs_DMA: std_logic;

    signal elink_busy_enable : array_57b (0 to 47);
    signal dma_busy_enable: std_logic;
    signal ANY_BUSY_REQ_OUT_s: std_logic;

    --signal ILA_PROBE0 : std_logic_vector(113 downto 0);
    --signal ILA_PROBE1 : std_logic_vector(113 downto 0);
    --signal ILA_PROBE2 : std_logic_vector(113 downto 0);
    --signal ILA_PROBE3 : std_logic_vector(113 downto 0);

    -------------------------------------------------------------------------------
    -- SUBDESIGN COMPONENT DECLARATIONS
    -------------------------------------------------------------------------------
    --component busy_limit_timer is
    --port (
    --    mclk  :  in std_logic;  --master clock (presumably 40-ish MHz in FELIX)
    --    mrst  :  in std_logic;  --global reset
    --    PRESCALE_VALUE : in std_logic_vector(19 downto 0);
    --    PRESCALED_CLOCK_ENABLE : out std_logic  --clock enable from external process (so can be shared by multiple copies of this)
    --                        --one tick of mclk wide at lesser rate, used to sample BUSY_REQ.
    --   );
    --end component busy_limit_timer;

    component busy_limiter is
        port (
            mclk  :  in std_logic;  --master clock (presumably 40-ish MHz in FELIX)
            mrst  :  in std_logic;  --global reset
            BUSY_REQUEST : in std_logic;  --active high busy request of unknown duration
            TEST_MODE : in std_logic;  --if set, can manually drive busy low or high for testing
            TEST_VALUE : in std_logic;  --the value BUSY should have in test mode
            BUSY_LIMIT_TIME : in std_logic_vector(7 downto 0);    --sets time that BUSY must continuously be asserted before accepted
            --this is a multiple of the period of PRESCALED_CLOCK_ENABLE.
            PRESCALED_CLOCK_ENABLE : in std_logic;  --clock enable from external process (so can be shared by multiple copies of this)
            --one tick of mclk wide at lesser rate, used to sample BUSY_REQ.
            --    LIMITER_STATE_MON : out std_logic_vector(1 downto 0);
            ACCEPTED_BUSY_OUT : out std_logic  --BUSY, if it was asserted long enough to be accepted.
        );
    end component busy_limiter;

--component Busy_limit_ila
--  PORT (
--      clk : IN STD_LOGIC;
--      probe0 : IN STD_LOGIC_VECTOR(113 DOWNTO 0);   --2 bit limit SM monitor for each of 57 elinks, for one GBT
--      probe1 : IN STD_LOGIC_VECTOR(113 DOWNTO 0);   --2 bit limit SM monitor for each of 57 elinks, for one GBT
--      probe2 : IN STD_LOGIC_VECTOR(113 DOWNTO 0);   --2 bit limit SM monitor for each of 57 elinks, for one GBT
--      probe3 : IN STD_LOGIC_VECTOR(113 DOWNTO 0);   --2 bit limit SM monitor for each of 57 elinks, for one GBT
--      probe4 : IN STD_LOGIC_VECTOR(56 DOWNTO 0);    --
--      probe5 : IN STD_LOGIC_VECTOR(19 DOWNTO 0)     --catch-all for counter, final or, etc.
--  );
--  END component Busy_limit_ila;


begin

    g_links: for i in 0 to 23 generate
        elink_busy_enable(i) <= register_map_control.ELINK_BUSY_ENABLE(i);
        elink_busy_enable(i+24) <= register_map_control.ELINK_BUSY_ENABLE(i);
    end generate;

    dma_busy_enable <= to_sl(register_map_control.DMA_BUSY_STATUS.ENABLE);
    -- controls for clock prescaling logic
    --PRESCALE_VALUE <= register_map_control.TTC_BUSY_TIMING_CTRL.PRESCALE; --controls for limiter logic
    TEST_MODEs <= (others => '0');    --if set, can manually drive busy low or high for testing
    TEST_VALUEs <= (others => '0');  --the value BUSY should have in test mode

    --Sasha: we use only 8 bits out of 16
    BUSY_LIMIT_TIME <= register_map_control.TTC_BUSY_TIMING_CTRL.LIMIT_TIME;        --sets time that BUSY must continuously be asserted before accepted
    --this is a multiple of the period of PRESCALED_CLOCK_ENABLE.
    ANY_BUSY_WIDTH_REG  <= register_map_control.TTC_BUSY_TIMING_CTRL.BUSY_WIDTH;    --how wide to make the final output pulse (minimum)
    BUSY_CLEAR <= to_sl(register_map_control.TTC_BUSY_CLEAR);
    SW_BUSY <= to_sl(register_map_control.TTC_DEC_CTRL.MASTER_BUSY);

    -------------------------------------------------------------------------------
    -- INSTANTIATION OF SUBDESIGNS
    -------------------------------------------------------------------------------

    --
    --  The COMMON_PRESCALER takes the 40-ish MHz clock of FELIX (mclk) and generates
    --  a PRESCALED_CLOCK_ENABLE that is used by the busy_limiter design for the sampling
    --  of the BUSY signals from each GBT link.
    --
    --COMMON_PRESCALER : busy_limit_timer
    --port map (
    --    mclk  => mclk,  --master clock (presumably 40-ish MHz in FELIX)
    --    mrst  => mrst,  --global reset
    --    PRESCALE_VALUE => PRESCALE_VALUE,
    --    PRESCALED_CLOCK_ENABLE => PRESCALED_CLOCK_ENABLE  --clock enable from external process (so can be shared by multiple copies of this)
    --                        --one tick of mclk wide at lesser rate, used to sample BUSY_REQ.
    --   );


    --
    --  A large number of busy_limiter implementations are generated in the BUSY_FILTERS
    --  nested for...generate loops.  Each GBT link can have up to 57 busy bits, and each
    --  of those must be individually filtered.  Then that inner loop is multiplied by the
    --  number of GBT links in operation.
    --

    BUSY_FILTERS_DMA : busy_limiter
        port map (
            mclk  => mclk,  --master clock (presumably 40-ish MHz in FELIX)
            mrst  => mrst,  --global reset
            BUSY_REQUEST => DMA_BUSY_in,  --active high busy request of unknown duration
            TEST_MODE => '0',  --if set, can manually drive busy low or high for testing
            TEST_VALUE => '0',  --the value BUSY should have in test mode
            BUSY_LIMIT_TIME => BUSY_LIMIT_TIME(7 downto 0),    --sets time that BUSY must continuously be asserted before accepted
            --this is a multiple of the period of PRESCALED_CLOCK_ENABLE.
            PRESCALED_CLOCK_ENABLE => '1', --PRESCALED_CLOCK_ENABLE,  --clock enable from external process (so can be shared by multiple copies of this)
            --one tick of mclk wide at lesser rate, used to sample BUSY_REQ.
            --            LIMITER_STATE_MON => LIMITER_STATE_MON_ARR(GBTnum)(i),
            ACCEPTED_BUSY_OUT => ACCEPTED_BUSY_OUTs_DMA --BUSY, if it was asserted long enough to be accepted.
        );

    BUSY_FILTERS_FIFO : busy_limiter
        port map (
            mclk  => mclk,  --master clock (presumably 40-ish MHz in FELIX)
            mrst  => mrst,  --global reset
            BUSY_REQUEST => FIFO_BUSY_in,  --active high busy request of unknown duration
            TEST_MODE => '0',  --if set, can manually drive busy low or high for testing
            TEST_VALUE => '0',  --the value BUSY should have in test mode
            BUSY_LIMIT_TIME => BUSY_LIMIT_TIME(7 downto 0),    --sets time that BUSY must continuously be asserted before accepted
            --this is a multiple of the period of PRESCALED_CLOCK_ENABLE.
            PRESCALED_CLOCK_ENABLE => '1', --PRESCALED_CLOCK_ENABLE,  --clock enable from external process (so can be shared by multiple copies of this)
            --one tick of mclk wide at lesser rate, used to sample BUSY_REQ.
            --            LIMITER_STATE_MON => LIMITER_STATE_MON_ARR(GBTnum)(i),
            ACCEPTED_BUSY_OUT => ACCEPTED_BUSY_OUTs_FIFO --BUSY, if it was asserted long enough to be accepted.
        );

    FILTER_OUTER_LOOP : for GBTnum in 0 to (GBT_NUM-1) generate
        FILTER_INNER_LOOP: for i in 0 to 56 generate
            BUSY_FILTERS : busy_limiter
                port map (
                    mclk  => mclk,  --master clock (presumably 40-ish MHz in FELIX)
                    mrst  => mrst,  --global reset
                    BUSY_REQUEST => BUSY_REQUESTs(GBTnum)(i),  --active high busy request of unknown duration
                    TEST_MODE => TEST_MODEs(GBTnum+1),  --if set, can manually drive busy low or high for testing
                    TEST_VALUE => TEST_VALUEs(GBTnum+1),  --the value BUSY should have in test mode
                    BUSY_LIMIT_TIME => BUSY_LIMIT_TIME(7 downto 0),    --sets time that BUSY must continuously be asserted before accepted
                    --this is a multiple of the period of PRESCALED_CLOCK_ENABLE.
                    PRESCALED_CLOCK_ENABLE => '1', --PRESCALED_CLOCK_ENABLE,  --clock enable from external process (so can be shared by multiple copies of this)
                    --one tick of mclk wide at lesser rate, used to sample BUSY_REQ.
                    --            LIMITER_STATE_MON => LIMITER_STATE_MON_ARR(GBTnum)(i),
                    ACCEPTED_BUSY_OUT => ACCEPTED_BUSY_OUTs(GBTnum)(i)  --BUSY, if it was asserted long enough to be accepted.
                );

            --Monitoring resisters (latch on BUSY, release on reset).
            BUSY_LATCH : process(mclk)
            begin
                if (mclk'event AND (mclk = '1')) then
                    if (BUSY_CLEAR = '1') then --synchronous reset of clear
                        internal_BUSY_mon_array(GBTnum)(i) <='0';
                    else
                        if (ACCEPTED_BUSY_OUTs(GBTnum)(i) = '1') then        --single plus 16 hex digits is 1+56, or 57 bits
                            internal_BUSY_mon_array(GBTnum)(i) <= '1';
                        else
                            internal_BUSY_mon_array(GBTnum)(i) <= internal_BUSY_mon_array(GBTnum)(i);
                        end if;
                    end if;
                end if;
            end process BUSY_LATCH;

        end generate FILTER_INNER_LOOP;  --closes inner loop

        FIRST_RANK_OR : process(mclk)
        begin
            if (mclk'event AND (mclk = '1')) then
                if (mrst = '1') then --synchronous reset
                    GBT_Busy(GBTnum) <= '0';
                else
                    GBT_Busy(GBTnum) <= '0';
                    for i in 0 to 56 loop
                        if ACCEPTED_BUSY_OUTs(GBTnum)(i) = '1' and  elink_busy_enable(GBTnum)(i) = '1' then
                            GBT_Busy(GBTnum) <= '1';
                        end if;
                    end loop;
                end if;
            end if;
        end process;


    end generate FILTER_OUTER_LOOP;  --closes outer loop

    TTC_BUSY_mon_array <= internal_BUSY_mon_array;

    SECOND_RANK_OR: process(mclk)
    begin
        if (mclk'event AND (mclk = '1')) then
            if (GBT_Busy /= VAR_SIZE_ZERO or ((ACCEPTED_BUSY_OUTs_DMA = '1' and dma_busy_enable = '1') or ACCEPTED_BUSY_OUTs_FIFO = '1')) then
                ANY_GBT_BUSY <= '1';
            else
                ANY_GBT_BUSY <= '0';
            end if;
        end if;
    end process;

    --LIMITER_STATE_MON is indexed (gbt)(elink)(1:0) as bit-pairs
    --CONVERT_TYPES: for i in 0 to 56 generate
    --    ILA_PROBE0((i*2)+1 downto (i*2)) <= LIMITER_STATE_MON_ARR(0)(i);
    --    ILA_PROBE1((i*2)+1 downto (i*2)) <= LIMITER_STATE_MON_ARR(1)(i);
    --    ILA_PROBE2((i*2)+1 downto (i*2)) <= LIMITER_STATE_MON_ARR(2)(i);
    --    ILA_PROBE3((i*2)+1 downto (i*2)) <= LIMITER_STATE_MON_ARR(3)(i);
    --end generate;

    --
    --  retriggerable one-shot for ANY_BUSY.
    --
    ANY_BUSY_COUNT_PROC : process(mclk)
    begin
        if (mclk'event and (mclk = '1')) then
            if (mrst = '1') then
                ANY_BUSY_COUNT <= X"0000";
            else
                if (ANY_GBT_BUSY = '1') then --Sasha: This is not exactly a one-shot! It is widening the pulse with the counter length.
                    ANY_BUSY_COUNT <= ANY_BUSY_WIDTH_REG;
                elsif (ANY_BUSY_COUNT /= X"0000") then     --if counter non-zero, count down
                    ANY_BUSY_COUNT <= ANY_BUSY_COUNT - 1;
                end if;
            end if;
        end if;
    end process;

    ANY_BUSY_OUTPUT_PROC : process(mclk)
        variable ANY_BUSY_REQ_OUT_last_v: std_logic;
        variable BUSY_INTERRUPT_CNT: integer range 0 to 7;
    begin
        if (mclk'event and (mclk = '1')) then
            --     if (TEST_MODEs(1) = '1') then
            --         ANY_BUSY_REQ_OUT <= TEST_VALUEs(1);
            --    elsif (ANY_GBT_BUSY = '1') then             --set output upon receipt of input
            if (mrst = '1') then
                BUSY_INTERRUPT_CNT := 0;
                ANY_BUSY_REQ_OUT_s <= '0';
                BUSY_INTERRUPT_out <= '0';
                ANY_BUSY_REQ_OUT_last_v := '0';
            else
                if ((ANY_GBT_BUSY = '1' OR SW_BUSY = '1' OR ANY_BUSY_COUNT /= X"0000") AND register_map_control.TTC_DEC_CTRL.BUSY_OUTPUT_INHIBIT = "0") then             --set the LEMO output upon receipt GBT BUSY or SW BUSY or non-zero COUNTER
                    ANY_BUSY_REQ_OUT_s <= '1';
                else                  --if input not set and counter is zero, clear output
                    ANY_BUSY_REQ_OUT_s <= '0';
                end if;
                BUSY_INTERRUPT_out <= '0';
                if(ANY_BUSY_REQ_OUT_s /= ANY_BUSY_REQ_OUT_last_v) then
                    BUSY_INTERRUPT_CNT := 4;
                end if;
                if (BUSY_INTERRUPT_CNT > 0) then
                    BUSY_INTERRUPT_out <= '1';
                    BUSY_INTERRUPT_CNT := BUSY_INTERRUPT_CNT -1;
                end if;
                ANY_BUSY_REQ_OUT_last_v := ANY_BUSY_REQ_OUT_s;
            end if;
        end if;
    end process;

    ANY_BUSY_REQ_OUT <= ANY_BUSY_REQ_OUT_s;


--
--LIMITER_ILA :Busy_limit_ila
--  PORT MAP(
--      clk =>  mclk,
--      probe0 => ILA_PROBE0,   --2 bit limit SM monitor for each of 57 elinks, for one GBT
--      probe1 => ILA_PROBE1,   --2 bit limit SM monitor for each of 57 elinks, for one GBT
--      probe2 => ILA_PROBE2,   --2 bit limit SM monitor for each of 57 elinks, for one GBT
--      probe3 => ILA_PROBE3,   --2 bit limit SM monitor for each of 57 elinks, for one GBT
--      probe4 => (others => '0'),
--      probe5(19) => GBT_BUSY(1),
--      probe5(18) => GBT_BUSY(2),
--      probe5(17) => GBT_BUSY(3),
--      probe5(16) => GBT_BUSY(4),
--      probe5(15 downto 0) => ANY_BUSY_COUNT
--
--  );



end rtl;

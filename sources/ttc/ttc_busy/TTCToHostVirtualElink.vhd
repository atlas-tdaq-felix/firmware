--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.


library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use work.FELIX_package.all;
    use work.axi_stream_package.all;
Library xpm;
    use xpm.vcomponents.all;

entity TTCToHostVirtualElink is
    generic (
        BLOCKSIZE : integer;     --Determines the m_axis_prog_empty threshold
        VERSAL : boolean         --For the axis32 fifo, to determine memory types
    );
    port (
        clk40 : in std_logic;                   -- 40 MHz BC clock
        daq_reset : in std_logic;               -- Active high reset
        daq_fifo_flush : in std_logic;          --Active high reset for the FIFO
        TTC_ToHost_Data_in  : in TTC_data_type; --Contains TTC information
        Enable : in std_logic;                  --Virtual e-link enable.
        IncludeAsyncUserData : in std_logic;    --Include AsyncUserdata, SyncUserdata and SyncGlobaldata
        IncludeSyncUserData : in std_logic;     --Include SyncUserdata and SyncGlobaldata
        IncludeSyncGlobalData : in std_logic;   --Include SyncGlobaldata
        m_axis : out axis_32_type;              --AXI4 Stream towards CRToHost aux
        m_axis_prog_empty : out std_logic;      --Towards CRToHost aux
        m_axis_tready : in std_logic;           --AXI4 Stream handshake from CRToHost aux
        m_axis_aclk : in std_logic              --R AXI4 Stream clock
    );
end TTCToHostVirtualElink;

architecture rtl of TTCToHostVirtualElink is

    signal s_axis: axis_32_type;
    signal s_axis_tready : std_logic;
    signal xpmfifo0_dout, xpmfifo0_din : std_logic_vector(255 downto 0);
    signal xpmfifo0_empty : std_logic;
    signal xpmfifo0_full : std_logic;
    signal xpmfifo0_rd_en : std_logic;
    signal xpmfifo0_wr_en : std_logic;
    signal m_axis_s : axis_32_type;
    signal m_axis_prog_empty_s : std_logic;
    signal Enable_aclk : std_logic;

    signal trunc, trunc_aclk: std_logic;

    signal daq_reset_m_axis_aclk : std_logic;
    signal daq_fifo_flush_m_axis_aclk : std_logic;
    signal daq_fifo_flush_n_m_axis_aclk : std_logic;
    signal IncludeAsyncUserData_aclk: std_logic;
    signal IncludeSyncUserData_aclk : std_logic;
    signal IncludeSyncGlobalData_aclk : std_logic;

begin

    cdc_IncludeAsyncUserData: xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => '0',
            src_in => IncludeAsyncUserData,
            dest_clk => m_axis_aclk,
            dest_out => IncludeAsyncUserData_aclk
        );

    cdc_IncludeSyncUserData: xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => '0',
            src_in => IncludeSyncUserData,
            dest_clk => m_axis_aclk,
            dest_out => IncludeSyncUserData_aclk
        );

    cdc_IncludeSyncGlobalData: xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => '0',
            src_in => IncludeSyncGlobalData,
            dest_clk => m_axis_aclk,
            dest_out => IncludeSyncGlobalData_aclk
        );


    pipeline: process(clk40, daq_reset)
        --variable SL0ID_v , SOrb_v , Sync_v , GRst_v, TS_v: std_logic;
        variable length_v: std_logic_vector(7 downto 0);
    begin
        if daq_reset = '1' then
            xpmfifo0_din <= (others => '0');
            xpmfifo0_wr_en <= '0';
            trunc <= '0';
        --SL0ID_v := '0';
        --SOrb_v := '0';
        --Sync_v := '0';
        --GRst_v := '0';
        --TS_v := '0';
        elsif rising_edge(clk40) then
            --if TTC_ToHost_Data_in.L0A = '1' then
            --    SL0ID_v := '0';
            --    SOrb_v := '0';
            --    Sync_v := '0';
            --    GRst_v := '0';
            --    TS_v := '0';
            --end if;

            --if TTC_ToHost_data_in.SL0ID = '1' then
            --    SL0ID_v := '1';
            --end if;
            --if TTC_ToHost_data_in.SOrb = '1' then
            --    SOrb_v := '1';
            --end if;
            --if TTC_ToHost_data_in.Sync = '1' then
            --    Sync_v := '1';
            --end if;
            --if TTC_ToHost_data_in.GRst = '1' then
            --    GRst_v := '1';
            --end if;
            --if TTC_ToHost_data_in.TS = '1' then
            --    TS_v := '1';
            --end if;

            length_v := x"14";
            if IncludeSyncGlobalData = '1' then
                length_v := x"16";
            end if;
            if IncludeSyncUserData = '1' then --SyncGlobalData will also be included
                length_v := x"18";
            end if;
            if IncludeAsyncUserData = '1' then --SyncUserData and SyncGlobaldata will also be included
                length_v := x"20";
            end if;

            xpmfifo0_din <=
            --Word 7 and 6
                            TTC_ToHost_data_in.AsyncUserData&
            --Word 5
                            TTC_ToHost_data_in.SyncUserData & TTC_ToHost_data_in.SyncGlobalData&
            --Word 4
                            TTC_ToHost_data_in.OrbitID&
            --Word 3
                            TTC_ToHost_data_in.LBID&TTC_ToHost_data_in.ITk_tag & "00" & TTC_ToHost_data_in.L0ID(37 downto 32) &
            --Word 2
                            TTC_ToHost_data_in.L0ID(31 downto 0)&
            --Word 1
                            TTC_ToHost_data_in.TriggerType& "000000000"&TTC_ToHost_data_in.PT&TTC_ToHost_data_in.PARTITION &TTC_ToHost_data_in.ITk_trig &
            --Word 0
                            TTC_ToHost_data_in.ErrorFlags&TTC_ToHost_data_in.BCID &length_v & x"04"; --FMT (4); -- 8 bit;

            --TS_v&
            --SL0ID_v & SOrb_v & Sync_v & GRst_v & '1' &

            xpmfifo0_wr_en <= TTC_ToHost_Data_in.L0A and not xpmfifo0_full and Enable;
            trunc <= '0';
            if TTC_ToHost_Data_in.L0A = '1' and xpmfifo0_full = '1' then
                trunc <= '1';
            end if;
        end if;
    end process;

    xpmfifo0 : xpm_fifo_async
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT, SIM_ASSERT_CHK"
            FIFO_MEMORY_TYPE => "auto",
            FIFO_WRITE_DEPTH => 16,
            RELATED_CLOCKS => 0,
            WRITE_DATA_WIDTH => 256,
            READ_MODE => "std",
            FIFO_READ_LATENCY => 1,
            FULL_RESET_VALUE => 1,
            USE_ADV_FEATURES => "0000",
            READ_DATA_WIDTH => 256,
            CDC_SYNC_STAGES => 2,
            WR_DATA_COUNT_WIDTH => 1,
            PROG_FULL_THRESH => 10,
            RD_DATA_COUNT_WIDTH => 1,
            PROG_EMPTY_THRESH => 10,
            DOUT_RESET_VALUE => "0",
            ECC_MODE => "no_ecc",
            WAKEUP_TIME => 0
        )
        port map (
            sleep => '0',
            rst => daq_fifo_flush,
            wr_clk => clk40,
            wr_en => xpmfifo0_wr_en,
            din => xpmfifo0_din,
            full => xpmfifo0_full,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => open,
            almost_full => open,
            wr_ack => open,
            rd_clk => m_axis_aclk,
            rd_en => xpmfifo0_rd_en,
            dout => xpmfifo0_dout,
            empty => xpmfifo0_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => open,
            almost_empty => open,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );


    sync_reset : xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 2,
            INIT => 1,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0
        )
        port map (
            src_rst => daq_reset,
            dest_clk => m_axis_aclk,
            dest_rst => daq_reset_m_axis_aclk
        );

    sync_flush : xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 2,
            INIT => 0,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0
        )
        port map (
            src_rst => daq_fifo_flush,
            dest_clk => m_axis_aclk,
            dest_rst => daq_fifo_flush_m_axis_aclk
        );

    daq_fifo_flush_n_m_axis_aclk <= not daq_fifo_flush_m_axis_aclk;



    toaxis: process(m_axis_aclk, daq_reset_m_axis_aclk)
        variable PacketCnt : integer range 0 to 7;
        variable Trig: std_logic;
        variable trunc_v : std_logic;
        variable fifo_data_valid: std_logic;
    begin
        if daq_reset_m_axis_aclk = '1' then
            Trig := '0';
            s_axis.tdata <= (others => '0');
            s_axis.tuser <= (others => '0');
            s_axis.tkeep <= "0000";
            s_axis.tvalid <= '0';
            s_axis.tlast <= '0';
            xpmfifo0_rd_en <= '0';
            trunc_v := '0';
            PacketCnt := 0;
            fifo_data_valid := '0';
        elsif rising_edge(m_axis_aclk) then
            s_axis.tdata <= (others => '0');
            s_axis.tuser <= (others => '0');
            s_axis.tkeep <= "0000";
            s_axis.tvalid <= '0';
            s_axis.tlast <= '0';
            xpmfifo0_rd_en <= '0';

            if trunc_aclk = '1' then
                trunc_v := '1';
            end if;
            if Trig = '0' then
                if xpmfifo0_empty = '0' then
                    xpmfifo0_rd_en <= '1';
                    Trig := '1';
                end if;
                PacketCnt := 0;
            else

                if fifo_data_valid = '1' then
                    if s_axis_tready = '1' then
                        s_axis.tdata <= xpmfifo0_dout(PacketCnt*32+31 downto PacketCnt*32);

                        s_axis.tvalid <= '1';

                        if (PacketCnt /= 7 and IncludeAsyncUserData_aclk = '1') or   --Include everything
                           (PacketCnt /= 5 and IncludeAsyncUserData_aclk = '0' and (IncludeSyncUserData_aclk = '1' or IncludeSyncGlobalData_aclk = '1')) or  --Include SyncUserdata and SyncGlobalData, or only SyncGlobalData, determined by tkeep
                           (PacketCnt /= 4 and IncludeAsyncUserData_aclk = '0' and (IncludeSyncUserData_aclk = '0' and IncludeSyncGlobalData_aclk = '0')) then  --Include none of them.
                            s_axis.tkeep <= "1111";
                            s_axis.tuser <= (others => '0');
                            s_axis.tlast <= '0';
                            PacketCnt := PacketCnt + 1;
                        else
                            Trig := '0';
                            fifo_data_valid := '0';
                            s_axis.tuser <= trunc_v & "000";
                            trunc_v := '0';
                            s_axis.tlast <= '1';
                            PacketCnt := 0;
                            if IncludeSyncGlobalData_aclk = '1' and IncludeSyncUserData_aclk = '0' and IncludeAsyncUserData_aclk = '0' then
                                s_axis.tkeep <= "0011";
                            else
                                s_axis.tkeep <= "1111";
                            end if;

                        end if;
                    end if;
                end if;
                fifo_data_valid := Trig;
            end if;
        end if;
    end process;

    axis32fifo0: entity work.Axis32Fifo
        generic map(
            DEPTH => BLOCKSIZE/2,
            --CLOCKING_MODE => "common_clock",
            --RELATED_CLOCKS => 0,
            --FIFO_MEMORY_TYPE => "auto",
            --PACKET_FIFO => "false",
            USE_BUILT_IN_FIFO => '0',
            VERSAL => VERSAL,
            BLOCKSIZE => BLOCKSIZE,
            ISPIXEL => false
        )
        port map(
            -- axi stream slave
            s_axis_aresetn => daq_fifo_flush_n_m_axis_aclk,
            s_axis_aclk => m_axis_aclk,
            s_axis => s_axis,
            s_axis_tready => s_axis_tready,

            -- axi stream master
            m_axis_aclk => m_axis_aclk,
            m_axis => m_axis_s,
            m_axis_tready => m_axis_tready,

            --Indication that the FIFO contains a block of data (for MUX).
            m_axis_prog_empty => m_axis_prog_empty_s
        );

    sync_enable: xpm_cdc_single
        generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 1,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => clk40,
            src_in => Enable,
            dest_clk => m_axis_aclk,
            dest_out => Enable_aclk
        );

    sync_trunc: xpm_cdc_single
        generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 1,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => clk40,
            src_in => trunc,
            dest_clk => m_axis_aclk,
            dest_out => trunc_aclk
        );

    process(m_axis_s , m_axis_prog_empty_s, Enable_aclk)
    begin
        m_axis <= m_axis_s;
        m_axis_prog_empty <= m_axis_prog_empty_s;
        if Enable_aclk = '0' then
            m_axis.tvalid <= '0';
            m_axis_prog_empty <= '1';
        end if;
    end process;



end rtl;

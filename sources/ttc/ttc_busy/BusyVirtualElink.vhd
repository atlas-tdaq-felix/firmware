--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.


library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.axi_stream_package.all;
    use work.FELIX_package.all;
Library xpm;
    use xpm.vcomponents.all;

entity BusyVirtualElink is
    generic (
        GBT_NUM : integer;
        BLOCKSIZE : integer
    );
    port (
        clk : in std_logic;
        daq_reset : in std_logic;
        daq_fifo_flush : in std_logic;
        Orbit : in std_logic_vector(31 downto 0);
        BCID : in std_logic_vector(11 downto 0);
        ElinkBusyIn : in array_57b(0 to GBT_NUM-1); --First busy source: SOB/EOB received through E-links
        SoftBusyIn : in std_logic; --Fourth busy source: Triggered by register map
        DmaBusyIn : in std_logic; --Second busy source: Host memory occupation
        FifoBusyIn : in std_logic; --Third busy source: Wupper ToHostFifo prog_full
        BusySumIn : in std_logic;  --Status of the board LEMO output
        Enable : in std_logic; --Virtual e-link enable.
        m_axis : out axis_32_type;
        m_axis_prog_empty : out std_logic;
        m_axis_tready : in std_logic;
        m_axis_aclk : in std_logic
    );
end BusyVirtualElink;

architecture rtl of BusyVirtualElink is

    signal ElinkBusy_p1 : array_57b(0 to GBT_NUM-1);
    signal DmaBusy_p1 : std_logic;
    signal FifoBusy_p1 : std_logic;
    signal SoftBusy_p1 : std_logic;
    signal ElinkBusy_p2 : array_57b(0 to GBT_NUM-1);
    signal DmaBusy_p2 : std_logic;
    signal FifoBusy_p2 : std_logic;
    signal SoftBusy_p2 : std_logic;
    signal BCID_p1 : std_logic_vector(11 downto 0);
    signal orbit_p1 : std_logic_vector(31 downto 0);

    signal s_axis: axis_32_type;
    signal s_axis_tready : std_logic;
    signal xpmfifo0_dout, xpmfifo0_din : std_logic_vector(63 downto 0);
    signal xpmfifo0_empty : std_logic;
    signal xpmfifo0_full : std_logic;
    signal xpmfifo0_rd_en : std_logic;
    signal xpmfifo0_wr_en : std_logic;
    signal m_axis_s : axis_32_type;
    signal m_axis_prog_empty_s : std_logic;
    signal Enable_aclk : std_logic;

    signal trunc, trunc_aclk: std_logic;
    signal daq_reset_m_axis_aclk : std_logic;
    signal daq_fifo_flush_m_axis_aclk : std_logic;
    signal daq_fifo_flush_n_m_axis_aclk : std_logic;
begin

    pipeline: process(clk, daq_reset)
        variable Trig : std_logic;
        variable TrigVal : std_logic;
        variable TrigLink : std_logic_vector(4 downto 0);
        variable TrigElink : std_logic_vector(5 downto 0);
        variable ElinkBusySum : std_logic;
    begin
        if daq_reset = '1' then
            Trig := '0';
            ElinkBusy_p1 <= (others => (others => '0'));
            DmaBusy_p1 <= '0';
            FifoBusy_p1 <= '0';
            SoftBusy_p1 <= '0';
            ElinkBusy_p2 <= (others => (others => '0'));
            DmaBusy_p2 <= '0';
            FifoBusy_p2 <= '0';
            SoftBusy_p2 <= '0';
            xpmfifo0_din <= (others => '0');
            xpmfifo0_wr_en <= '0';
            trunc <= '0';
            TrigVal := '0';
            TrigLink := (others => '0');
            BCID_p1 <= (others => '0');
            orbit_p1 <= (others => '0');
            TrigElink := (others => '0');
        elsif rising_edge(clk) then
            Trig := '0';
            ElinkBusy_p1 <= ElinkBusyIn;
            DmaBusy_p1 <= DmaBusyIn;
            FifoBusy_p1 <= FifoBusyIn;
            SoftBusy_p1 <= SoftBusyIn;
            ElinkBusy_p2 <= ElinkBusy_p1;
            DmaBusy_p2 <= DmaBusy_p1;
            FifoBusy_p2 <= FifoBusy_p1;
            SoftBusy_p2 <= SoftBusy_p1;
            BCID_p1 <= BCID;
            orbit_p1 <= Orbit;
            ElinkBusySum := '0';

            for link in 0 to GBT_NUM-1 loop
                for elink in 0 to 39 loop
                    if(ElinkBusy_p1(link)(elink) = '1' ) then
                        ElinkBusySum := '1'; --Indication if any of the E-links triggered busy.
                    end if;
                end loop;
            end loop;

            for link in 0 to GBT_NUM-1 loop
                for elink in 0 to 39 loop
                    if(ElinkBusy_p2(link)(elink) /= ElinkBusy_p1(link)(elink)) then
                        Trig := '1';
                        TrigLink := std_logic_vector(to_unsigned(link, 5));
                        TrigElink := std_logic_vector(to_unsigned(elink, 6));
                        TrigVal := ElinkBusy_p1(link)(elink);
                        exit;
                    end if;
                end loop;
                if Trig = '1' then
                    exit;
                end if;
            end loop;
            if DmaBusy_p2 /= DmaBusy_p1 then
                Trig := '1';
                TrigLink := (others => '1');
                TrigElink := "000000";
                TrigVal := DmaBusy_p1;
            end if;
            if FifoBusy_p2 /= FifoBusy_p1 then
                Trig := '1';
                TrigLink := (others => '1');
                TrigElink := "000001";
                TrigVal := FifoBusy_p1;
            end if;
            if SoftBusy_p2 /= SoftBusy_p1 then
                Trig := '1';
                TrigLink := (others => '1');
                TrigElink := "000010";
                TrigVal := SoftBusy_p1;
            end if;

            xpmfifo0_din(xpmfifo0_din'high downto xpmfifo0_din'high-31) <= orbit_p1;
            xpmfifo0_din(xpmfifo0_din'high-32 downto xpmfifo0_din'high-63) <=
                                                                              BCID_p1 &        --31..20
                                                                              TrigLink&     --19..15
                                                                              TrigElink&    --14..9
                                                                              TrigVal&      --8
                                                                              "000"&        --7..5
                                                                              ElinkBusySum& --4
                                                                              SoftBusy_p1&   --3
                                                                              DmaBusy_p1&    --2
                                                                              FifoBusy_p1&   --1
                                                                              BusySumIn;    --0

            xpmfifo0_wr_en <= Trig and not xpmfifo0_full and Enable;
            trunc <= '0';
            if Trig = '1' and xpmfifo0_full = '1' then
                trunc <= '1';
            end if;
        end if;
    end process;

    xpmfifo0 : xpm_fifo_async
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT, SIM_ASSERT_CHK"
            FIFO_MEMORY_TYPE => "distributed",
            FIFO_WRITE_DEPTH => 16,
            RELATED_CLOCKS => 0,
            WRITE_DATA_WIDTH => 64,
            READ_MODE => "std",
            FIFO_READ_LATENCY => 1,
            FULL_RESET_VALUE => 1,
            USE_ADV_FEATURES => "0000",
            READ_DATA_WIDTH => 64,
            CDC_SYNC_STAGES => 2,
            WR_DATA_COUNT_WIDTH => 1,
            PROG_FULL_THRESH => 10,
            RD_DATA_COUNT_WIDTH => 1,
            PROG_EMPTY_THRESH => 10,
            DOUT_RESET_VALUE => "0",
            ECC_MODE => "no_ecc",
            WAKEUP_TIME => 0
        )
        port map (
            sleep => '0',
            rst => daq_fifo_flush,
            wr_clk => clk,
            wr_en => xpmfifo0_wr_en,
            din => xpmfifo0_din,
            full => xpmfifo0_full,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => open,
            almost_full => open,
            wr_ack => open,
            rd_clk => m_axis_aclk,
            rd_en => xpmfifo0_rd_en,
            dout => xpmfifo0_dout,
            empty => xpmfifo0_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => open,
            almost_empty => open,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );


    sync_reset : xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 2,
            INIT => 1,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0
        )
        port map (
            src_rst => daq_reset,
            dest_clk => m_axis_aclk,
            dest_rst => daq_reset_m_axis_aclk
        );

    sync_flush : xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 2,
            INIT => 0,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0
        )
        port map (
            src_rst => daq_fifo_flush,
            dest_clk => m_axis_aclk,
            dest_rst => daq_fifo_flush_m_axis_aclk
        );

    daq_fifo_flush_n_m_axis_aclk <= not daq_fifo_flush_m_axis_aclk;

    toaxis: process(m_axis_aclk, daq_reset_m_axis_aclk)
        variable PacketCnt : integer range 0 to 1;
        variable Trig: std_logic;
        variable trunc_v : std_logic;
        variable fifo_data_valid: std_logic;
    begin
        if daq_reset_m_axis_aclk = '1' then
            Trig := '0';
            s_axis.tdata <= (others => '0');
            s_axis.tuser <= (others => '0');
            s_axis.tkeep <= "0000";
            s_axis.tvalid <= '0';
            s_axis.tlast <= '0';
            xpmfifo0_rd_en <= '0';
            trunc_v := '0';
            PacketCnt := 0;
            fifo_data_valid := '0';
        elsif rising_edge(m_axis_aclk) then
            s_axis.tdata <= (others => '0');
            s_axis.tuser <= (others => '0');
            s_axis.tkeep <= "0000";
            s_axis.tvalid <= '0';
            s_axis.tlast <= '0';
            xpmfifo0_rd_en <= '0';

            if trunc_aclk = '1' then
                trunc_v := '1';
            end if;
            if Trig = '0' then
                if xpmfifo0_empty = '0' then
                    xpmfifo0_rd_en <= '1';
                    Trig := '1';
                end if;
                PacketCnt := 0;
            else

                if fifo_data_valid = '1' then
                    if s_axis_tready = '1' then
                        if PacketCnt /=1 then  --
                            s_axis.tdata <= xpmfifo0_dout(31 downto 0);
                            s_axis.tuser <= (others => '0');
                            s_axis.tkeep <= "1111";
                            s_axis.tvalid <= '1';
                            s_axis.tlast <= '0';
                            PacketCnt := 1;
                        else
                            Trig := '0';
                            s_axis.tdata <= xpmfifo0_dout(63 downto 32);
                            s_axis.tuser <= trunc_v & "000";
                            trunc_v := '0';
                            s_axis.tkeep <= "1111";
                            s_axis.tvalid <= '1';
                            s_axis.tlast <= '1';
                            PacketCnt := 0;
                        end if;
                    end if;
                end if;
                fifo_data_valid := Trig;
            end if;
        end if;
    end process;

    axis32fifo0: entity work.Axis32Fifo
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: VERSAL, BLOCKSIZE, ISPIXEL"
            DEPTH => BLOCKSIZE/2,
            --CLOCKING_MODE => "common_clock",
            --RELATED_CLOCKS => 0,
            --FIFO_MEMORY_TYPE => "auto",
            --PACKET_FIFO => "false",
            USE_BUILT_IN_FIFO => '0'
        )
        port map(
            -- axi stream slave
            s_axis_aresetn => daq_fifo_flush_n_m_axis_aclk,
            s_axis_aclk => m_axis_aclk,
            s_axis => s_axis,
            s_axis_tready => s_axis_tready,

            -- axi stream master
            m_axis_aclk => m_axis_aclk,
            m_axis => m_axis_s,
            m_axis_tready => m_axis_tready,

            --Indication that the FIFO contains a block of data (for MUX).
            m_axis_prog_empty => m_axis_prog_empty_s
        );

    sync_enable: xpm_cdc_single
        generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 1,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => clk,
            src_in => Enable,
            dest_clk => m_axis_aclk,
            dest_out => Enable_aclk
        );

    sync_trunc: xpm_cdc_single
        generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 1,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => clk,
            src_in => trunc,
            dest_clk => m_axis_aclk,
            dest_out => trunc_aclk
        );

    process(m_axis_s , m_axis_prog_empty_s, Enable_aclk)
    begin
        m_axis <= m_axis_s;
        m_axis_prog_empty <= m_axis_prog_empty_s;
        if Enable_aclk = '0' then
            m_axis.tvalid <= '0';
            m_axis_prog_empty <= '1';
        end if;
    end process;



end rtl;

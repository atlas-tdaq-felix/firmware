--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Soo Ryu
--!               Frans Schreuder
--!               Alexander Paramonov
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

-------------------------------------------------------------------------------
--  Argonne National Laboratory
-------------------------------------------------------------------------------
--  Filename: FELIX_busy_limiter.vhd
--  Title: Busy input filter for FELIX, adapted from Digital Gammasphere code.

--
-- This logic unit receives a Busy Request bit from the central router
-- and only allows it to pass to the BUSY OR if it has been continuously
-- asserted by the digitizer for a programmable delay time.
--
-- Once the limiter logic determines that the BUSY from a given source has persisted
-- long enough to merit assertion, an output signal is asserted for as long as BUSY
-- persists after the minimum time threshold.  It is assumed that the OR of some
-- number of copies of this logic will be done in a separate process, and the
-- output of that OR-ing process will have its own one-shot for a minimum assertion
-- time to the trigger system.
--
-------------------------------------------------------------------------------
-- Author:   John Anderson
-- revised by Alexander Paramonov alexander.paramonov@cern.ch
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  Library Declaration
-------------------------------------------------------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;      -- needed for logical operations
    use ieee.numeric_std_unsigned.all;
library UNISIM;
    use unisim.all;

-------------------------------------------------------------------------------
--  PORT DECLARATION
-------------------------------------------------------------------------------

entity busy_limiter is
    port (
        mclk  :  in std_logic;  --master clock (presumably 40-ish MHz in FELIX)
        mrst  :  in std_logic;  --global reset
        BUSY_REQUEST : in std_logic;  --active high busy request of unknown duration
        TEST_MODE : in std_logic;  --if set, can manually drive busy low or high for testing
        TEST_VALUE : in std_logic;  --the value BUSY should have in test mode
        BUSY_LIMIT_TIME : in std_logic_vector(7 downto 0);    --sets time that BUSY must continuously be asserted before accepted
        --this is a multiple of the period of PRESCALED_CLOCK_ENABLE.
        PRESCALED_CLOCK_ENABLE : in std_logic;  --clock enable from external process (so can be shared by multiple copies of this)
        --one tick of mclk wide at lesser rate, used to sample BUSY_REQ.
        ACCEPTED_BUSY_OUT : out std_logic  --BUSY, if it was asserted long enough to be accepted.
    );
end busy_limiter;

architecture rtl of busy_limiter is

    signal LIMIT_COUNT : std_logic_vector(7 downto 0);      -- counter for PRESCALED_CLOCK_ENABLEs

    --signal STRETCHED_LIMITED_BUSY_REQ : std_logic_vector(7 downto 0);  --ACCEPTED_BUSY_OUT, after passing through monostable to ensure minimum assertion time


    type LIMIT_MACH_STATES is (IDLE, ASSERT_BUSY); --REQUEST_RECEIVED,
    signal LIMIT_MACH_STATE : LIMIT_MACH_STATES;

    signal INTERNAL_BUSY_REQUEST : std_logic;  --manually override-able copy of BUSY_REQUEST.

begin


    INTERNAL_BUSY_REQUEST <= BUSY_REQUEST when (TEST_MODE = '0') else TEST_VALUE;

    -------------------------------------------------------------------------------
    -- PROCESS DECLARATION
    -------------------------------------------------------------------------------

    --
    --  Limit logic state machines.
    --
    LIMIT_MACH : process(mclk)
    begin

        if (mclk'event and (mclk = '1')) then
            if (mrst = '1') then -- synchronous reset
                LIMIT_MACH_STATE <= IDLE;
                ACCEPTED_BUSY_OUT <= '0';
                LIMIT_COUNT <= x"00";
            else
                case LIMIT_MACH_STATE is
                    --Sasha: The state machine should be able to respond to a 25ns-long busy from the central router
                    --                --sit in the idle state until a BUSY_REQUEST is received.
                    --                when IDLE =>
                    --                    ACCEPTED_BUSY_OUT <= '0';
                    --                    LIMIT_COUNT <= BUSY_LIMIT_TIME;
                    --                    if(INTERNAL_BUSY_REQUEST = '1') then
                    --                        LIMIT_MACH_STATE <= REQUEST_RECEIVED;
                    --                    else
                    --                        LIMIT_MACH_STATE <= IDLE;
                    --                    end if;
                    --                --  Once the request is received, sit in state REQUEST_RECEIVED until
                    --                --  one of two things happens:
                    --                --
                    --                --    a) BUSY_REQUEST goes away - go back to IDLE
                    --                --    b) LIMIT_COUNT hits zero - go to ASSERT_BUSY
                    when  IDLE => --REQUEST_RECEIVED =>
                        -- while in this state, count down the LIMIT_COUNT each time
                        -- PRESCALED_CLOCK_ENABLE occurs.  If the LIMIT_COUNT hits
                        -- zero, then assert the BUSY, but if on any clock prior to
                        -- LIMIT_COUNT = 0, reject the request and go idle.

                        -- State transition logic
                        if (INTERNAL_BUSY_REQUEST = '0') then    --go idle if request removed.
                            ACCEPTED_BUSY_OUT <= '0';
                            LIMIT_MACH_STATE <= IDLE;
                            LIMIT_COUNT <= BUSY_LIMIT_TIME;

                        else
                            if(LIMIT_COUNT = X"00") then    --assert if countdown satisfied.
                                ACCEPTED_BUSY_OUT <= '1';
                                LIMIT_MACH_STATE <= ASSERT_BUSY;
                            else --otherwise wait.
                                ACCEPTED_BUSY_OUT <= '0';
                                --each time the prescaled clock enable turns on, decrement the counter and flip
                                --the state monitor so we know what's happening in the long loop.
                                if (PRESCALED_CLOCK_ENABLE = '1') then
                                    LIMIT_COUNT <= LIMIT_COUNT - 1;
                                else
                                    LIMIT_COUNT <= LIMIT_COUNT;
                                end if;

                                LIMIT_MACH_STATE <= IDLE; --REQUEST_RECEIVED;
                            end if;
                        end if;
                    -- State ASSERT_BUSY simply turns on LIMITED_BUSY_REQUEST until
                    -- BUSY_REQUEST goes away.
                    when ASSERT_BUSY =>
                        if (INTERNAL_BUSY_REQUEST = '0') then
                            LIMIT_MACH_STATE <= IDLE;
                            ACCEPTED_BUSY_OUT <= '0';
                        else
                            LIMIT_MACH_STATE <= ASSERT_BUSY;
                            ACCEPTED_BUSY_OUT <= '1';
                        end if;
                end case;
            end if;
        end if;
    end process;

end rtl;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std_unsigned.all;
    use ieee.numeric_std.all;

library UNISIM;
    use UNISIM.VComponents.all;
library xpm;
    use xpm.vcomponents.all;

    use work.pcie_package.all;
    use work.FELIX_package.all;

entity LTI_CRC_check is
    port(
        clk240                  : in std_logic;
        DataIn                  : in std_logic_vector(31 downto 0);
        KIn                     : in std_logic_vector(3 downto 0);
        crc_valid_out           : out std_logic
    );
end LTI_CRC_check;

architecture rtl of LTI_CRC_check is
    signal frame_counter: integer range 0 to 5;
    signal crc_reset, crc_en: std_logic;
    signal crc_in: std_logic_vector(31 downto 0);
    signal crc_out : std_logic_vector (15 downto 0);
    signal DataIn_s: std_logic_vector(31 downto 0);
    signal KIn_s   : std_logic_vector(3 downto 0);
begin

    --Word# | 31 | 30 | 29 | 28 | 27 | 26 | 25 | 24 | 23 | 22 | 21 | 20 | 19 | 18 | 17 | 16 | 15 | 14 | 13 | 12 | 11 | 10 |  9 |  8 |  7 |  6 |  5 |  4 |  3 |  2 |  1 |  0 | fc@DataIn | fc@DataIn_s |
    --------|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|-----------|-------------|
    --    0 | MT1| PT |Partition| BCID[11..0]                                               | SyncUserData [15..0]                                                          | 5         | 0           |
    --    1 | SyncGlobalData [15..0]                                                        | TS | ErrorFlags[3..0]  |Xoff| Reserved                                        | 0         | 1           |
    --    2 | AsyncUserData[31..0]                                                                                                                                          | 1         | 2           |
    --    3 | AsyncUserData[63..32]                                                                                                                                         | 2         | 3           |
    --    4 | Reserved                                                                                                                                                      | 3         | 4           |
    --    5 | CRC [15..0]                                                                   | D16.2 (0x50)                          | K28.5 (0xBC)                          | 4         | 5           |


    pipe_proc: process(clk240)
    begin
        if rising_edge(clk240) then

            DataIn_s <= DataIn;

            KIn_s <= Kin;
        end if;
    end process;

    --Count from 0 to 5, reset when K28.5 is received in the last clock cycle
    framecnt_proc: process(clk240)
    begin
        if rising_edge(clk240) then
            if DataIn_s(15 downto 0) = x"50BC" and KIn_s = "0001" then
                frame_counter <= 0;
            else
                if frame_counter /= 5 then
                    frame_counter <= frame_counter + 1;
                else
                    frame_counter <= 0;
                end if;
            end if;
        end if;
    end process;




    crc_reset <= '1' when frame_counter = 5 else '0';
    crc_en <= not crc_reset;
    crc_in <= DataIn_s when frame_counter /= 5 else (others => '1');

    CRC16_0 : entity work.crc16_lti
        port map(
            data_in => crc_in,
            crc_en  => crc_en,
            rst     => crc_reset,
            clk     => clk240,
            crc_out => crc_out
        );

    compare_proc: process(clk240)
    begin
        if rising_edge(clk240) then
            if frame_counter = 5 then
                if crc_out = DataIn_s(31 downto 16) then
                    crc_valid_out <= '1';
                else
                    crc_valid_out <= '0';
                end if;
            end if;
        end if;
    end process;


end architecture rtl;

--moved all monitoring counters to this entity

library ieee;
    use ieee.std_logic_1164.all;
    use IEEE.NUMERIC_STD.ALL;
    use ieee.numeric_std_unsigned.all;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.FELIX_package.all;
    use work.pcie_package.all;
entity ltittc_mon_counters is
    Port (
        clk40                           : in std_logic;

        register_map_control            : in register_map_control_type;

        l0id                            : in std_logic_vector(37 downto 0);
        sl0id                           : in std_logic;
        l0a                             : in std_logic;
        typemsg                         : in std_logic;
        ttype                           : in std_logic_vector(15 downto 0);
        bcid                            : in std_logic_vector(11 downto 0);
        sorb                            : in std_logic;
        sync                            : in std_logic;
        grst                            : in std_logic;
        crc_valid                       : in std_logic;
        LTITTC_TTYPE_MONITOR_VALUE      : out std_logic_vector(31 downto 0);
        LTITTC_SL0ID_MONITOR_VALUE      : out std_logic_vector(31 downto 0);
        LTITTC_SORB_MONITOR_VALUE       : out std_logic_vector(31 downto 0);
        LTITTC_GRST_MONITOR_VALUE       : out std_logic_vector(31 downto 0);
        LTITTC_SYNC_MONITOR_VALUE       : out std_logic_vector(31 downto 0);
        LTITTC_BCR_ERR_MONITOR_VALUE    : out std_logic_vector(31 downto 0);
        LTITTC_L0ID_ERR_MONITOR_VALUE   : out std_logic_vector(31 downto 0);
        LTITTC_CRC_ERR_MONITOR_VALUE    : out std_logic_vector(31 downto 0)
    );
end ltittc_mon_counters;

architecture Behavioral of ltittc_mon_counters is

    signal rst_ttype_cntr           : std_logic := '0';
    signal rst_bcid_cntr            : std_logic := '0';
    signal rst_sl0id_cntr           : std_logic := '0';
    signal rst_sorb_cntr            : std_logic := '0';
    signal rst_sync_cntr            : std_logic := '0';
    signal rst_grst_cntr            : std_logic := '0';

    signal rst_l0id_err_cntr        : std_logic := '0';
    signal rst_bcr_err_cntr         : std_logic := '0';
    signal rst_crc_err_cntr         : std_logic := '0';

    signal l0id_cntr_reset          : std_logic_vector(1 downto 0) := "00";
    signal ttype_cntr_reset         : std_logic_vector(1 downto 0) := "00";
    signal bcid_cntr_reset          : std_logic_vector(1 downto 0) := "00";
    signal sl0id_cntr_reset         : std_logic_vector(1 downto 0) := "00";
    signal sorb_cntr_reset          : std_logic_vector(1 downto 0) := "00";
    signal sync_cntr_reset          : std_logic_vector(1 downto 0) := "00";
    signal grst_cntr_reset          : std_logic_vector(1 downto 0) := "00";

    signal bcr_err_cntr_reset       : std_logic_vector(1 downto 0);
    signal l0id_err_cntr_reset      : std_logic_vector(1 downto 0);
    signal crc_err_cntr_reset       : std_logic_vector(1 downto 0);

    signal ttype_reg                : std_logic_vector(15 downto 0) := (others => '0');

    signal l0id_counter             : std_logic_vector(37 downto 0)  := (others => '0');
    signal ttype_counter            : std_logic_vector(31 downto 0) := (others => '0');
    signal bcid_counter             : std_logic_vector(11 downto 0)  := (others => '0');
    signal sl0id_counter            : std_logic_vector(31 downto 0)  := (others => '0');
    signal sorb_counter             : std_logic_vector(31 downto 0)  := (others => '0');
    signal sync_counter             : std_logic_vector(31 downto 0)  := (others => '0');
    signal grst_counter             : std_logic_vector(31 downto 0)  := (others => '0');

    signal l0id_err_counter         : std_logic_vector(31 downto 0) := (others => '0');
    signal bcr_err_counter          : std_logic_vector(31 downto 0) := (others => '0');
    signal crc_err_counter          : std_logic_vector(31 downto 0) := (others => '0');

    signal brc_b                    : std_logic := '0';
    signal l0id_dly                 : std_logic_vector(37 downto 0);
    signal bcid_dly                 : std_logic_vector(11 downto 0);

begin

    --output
    LTITTC_TTYPE_MONITOR_VALUE      <= ttype_counter;
    LTITTC_SL0ID_MONITOR_VALUE      <= sl0id_counter;
    LTITTC_SORB_MONITOR_VALUE       <= sorb_counter;
    LTITTC_GRST_MONITOR_VALUE       <= grst_counter;
    LTITTC_SYNC_MONITOR_VALUE       <= sync_counter;
    LTITTC_BCR_ERR_MONITOR_VALUE    <= bcr_err_counter;
    LTITTC_L0ID_ERR_MONITOR_VALUE   <= l0id_err_counter;
    LTITTC_CRC_ERR_MONITOR_VALUE    <= crc_err_counter;
    --get clear from registers
    rst_ttype_cntr    <= to_sl(register_map_control.LTITTC_TTYPE_MONITOR.CLEAR);
    rst_bcid_cntr     <= to_sl(register_map_control.LTITTC_BCR_ERR_MONITOR.CLEAR);
    rst_sl0id_cntr    <= to_sl(register_map_control.LTITTC_SL0ID_MONITOR.CLEAR);
    rst_sorb_cntr     <= to_sl(register_map_control.LTITTC_SORB_MONITOR.CLEAR);
    rst_sync_cntr     <= to_sl(register_map_control.LTITTC_SYNC_MONITOR.CLEAR);
    rst_grst_cntr     <= to_sl(register_map_control.LTITTC_GRST_MONITOR.CLEAR);
    rst_l0id_err_cntr <= to_sl(register_map_control.LTITTC_L0ID_ERR_MONITOR.CLEAR);
    rst_bcr_err_cntr  <= to_sl(register_map_control.LTITTC_BCR_ERR_MONITOR.CLEAR);
    rst_crc_err_cntr  <= to_sl(register_map_control.LTITTC_CRC_ERR_MONITOR.CLEAR);

    ttype_reg         <= register_map_control.LTITTC_TTYPE_MONITOR.REFVALUE;

    --counters

    L0ID_gen: process (clk40)
    begin
        if rising_edge(clk40) then
            l0id_cntr_reset(0) <= rst_l0id_err_cntr;
            l0id_cntr_reset(1) <= l0id_cntr_reset(0);

            if (l0id_cntr_reset(1) = '0' and l0id_cntr_reset(0) = '1') then
                l0id_counter <= l0id;
            elsif (sl0id = '1') then
                l0id_counter <= l0id;
            elsif (l0a = '1' and l0id_counter /= x"3F_FFFF_FFFF") then
                l0id_counter  <= l0id_counter + 1;
            end if;
        end if;
    end process;

    TTYPE_CNT: process (clk40)
    begin
        if rising_edge(clk40) then
            ttype_cntr_reset(0) <= rst_ttype_cntr;
            ttype_cntr_reset(1) <= ttype_cntr_reset(0);

            if (ttype_cntr_reset(1) = '0' and ttype_cntr_reset(0) = '1') then
                ttype_counter <= (others => '0');
            elsif ( typemsg = '0' and ttype = ttype_reg) then
                ttype_counter <= ttype_counter + 1;
            end if;
        end if;
    end process;

    BCID_CNT: process (clk40)
    begin
        if rising_edge(clk40) then
            bcid_cntr_reset(0) <= rst_bcid_cntr;
            bcid_cntr_reset(1) <= bcid_cntr_reset(0);

            if (bcid_cntr_reset(1) = '0' and bcid_cntr_reset(0) = '1') then
                bcid_counter <= bcid;
            elsif (brc_b = '0') then
                bcid_counter <= bcid_counter + 1;
            else
                bcid_counter <= (others=>'0');
            end if;
            if (bcid_counter = x"DEC" - 2) then  --3564-2, RL: clock after will check counter vs id. clock after it resets the counter.
                brc_b <= '1';
            else
                brc_b <= '0';
            end if;
        end if;
    end process;

    SL0ID_CNT: process (clk40)
    begin
        if rising_edge(clk40) then
            sl0id_cntr_reset(0) <= rst_sl0id_cntr;
            sl0id_cntr_reset(1) <= sl0id_cntr_reset(0);

            if (sl0id_cntr_reset(1) = '0' and sl0id_cntr_reset(0) = '1') then
                sl0id_counter <= (others=>'0');
            elsif (sl0id = '1') then
                sl0id_counter <= sl0id_counter + 1;
            end if;
        end if;
    end process;

    SORB_CNT: process (clk40)
    begin
        if rising_edge(clk40) then
            sorb_cntr_reset(0) <= rst_sorb_cntr;
            sorb_cntr_reset(1) <= sorb_cntr_reset(0);

            if (sorb_cntr_reset(1) = '0' and sorb_cntr_reset(0) = '1') then
                sorb_counter <= (others=>'0');
            elsif (sorb = '1') then
                sorb_counter <= sorb_counter + 1;
            end if;
        end if;
    end process;

    SYNC_CNT: process (clk40)
    begin
        if rising_edge(clk40) then
            sync_cntr_reset(0) <= rst_sync_cntr;
            sync_cntr_reset(1) <= sync_cntr_reset(0);

            if (sync_cntr_reset(1) = '0' and sync_cntr_reset(0) = '1') then
                sync_counter <= (others=>'0');
            else
                if (sync = '1') then
                    sync_counter <= sync_counter + 1;
                end if;
            end if;
        end if;
    end process;

    GRST_CNT: process (clk40)
    begin
        if rising_edge(clk40) then
            grst_cntr_reset(0) <= rst_grst_cntr;
            grst_cntr_reset(1) <= grst_cntr_reset(0);

            if (grst_cntr_reset(1) = '0' and grst_cntr_reset(0) = '1') then
                grst_counter <= (others=>'0');
            elsif (grst = '1') then
                grst_counter <= grst_counter + 1;
            end if;
        end if;
    end process;

    --error counters. TO DO: crc

    L0ID_ERR_CNT: process (clk40)
    begin
        if rising_edge(clk40) then
            l0id_err_cntr_reset(0) <= rst_l0id_err_cntr;
            l0id_err_cntr_reset(1) <= l0id_err_cntr_reset(0);
            l0id_dly <= l0id; --has to be delayed by one clock: account for the clock l0id_counter takes to calculate

            if (l0id_err_cntr_reset(1) = '0' and l0id_err_cntr_reset(0) = '1') then
                l0id_err_counter  <= (others=>'0');
            elsif (l0id_dly /= l0id_counter) then
                if (l0id_err_counter /= x"FFFFFFFF") then
                    l0id_err_counter  <= l0id_err_counter + 1;
                end if;
            end if;
        end if;
    end process;


    BCR_ERR_CNT: process (clk40) --at the rollover
    begin
        if rising_edge(clk40) then
            bcr_err_cntr_reset(0) <= rst_bcr_err_cntr;
            bcr_err_cntr_reset(1) <= bcr_err_cntr_reset(0);
            bcid_dly <= bcid; --has to be delayed by one clock: account for the clock l0id_counter takes to calculate

            if (bcr_err_cntr_reset(1) = '0' and bcr_err_cntr_reset(0) = '1') then
                bcr_err_counter  <= (others=>'0');
            elsif (brc_b = '1' and bcid_dly /= bcid_counter) then --bcid_counter=x"DEB"
                if (bcr_err_counter /= x"FFFFFFFF") then
                    bcr_err_counter <= bcr_err_counter + 1;
                end if;
            end if;
        end if;
    end process;

    CRC_ERR_CNT: process (clk40)
    begin
        if rising_edge(clk40) then
            crc_err_cntr_reset(0) <= rst_crc_err_cntr;
            crc_err_cntr_reset(1) <= bcr_err_cntr_reset(0);

            if (crc_err_cntr_reset(1) = '0' and crc_err_cntr_reset(0) = '1') then
                crc_err_counter  <= (others=>'0');
            elsif (crc_valid = '0') then
                crc_err_counter <= crc_err_counter + 1;

            end if;
        end if;
    end process;

end Behavioral;

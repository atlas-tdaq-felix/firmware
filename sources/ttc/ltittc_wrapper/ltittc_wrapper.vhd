----------------------------------------------------------------------------------
--! Company:  Argonne National Laboratory
--! Engineer: Marco Trovato (mtrovato@anl.gov)
----------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use IEEE.NUMERIC_STD.ALL;
    use ieee.numeric_std_unsigned.all;

library UNISIM;
    use UNISIM.VComponents.all;
library XPM;
    use XPM.VComponents.all;

    use work.FELIX_package.all;
    use work.pcie_package.all;

--=================================================================================================--
--======================================= Module Body =============================================--
--=================================================================================================--
entity ltittc_wrapper is
    generic(
        CARD_TYPE                   : integer := 182;
        ITK_TRIGTAG_MODE            : integer := -1;    -- -1: not implemented, 0: RD53A, 1: ITkPixV1, 2: ABC*
        GBT_NUM                     : integer := 4;
        FIRMWARE_MODE               : integer;
        SIMULATION                  : boolean := false
    );
    port (
        reset_in                    : in std_logic;
        register_map_control            : in register_map_control_type; --synchronized to pcie clock
        register_map_ltittc_monitor : out  register_map_ltittc_monitor_type;
        TTC_out                     : out TTC_data_type;
        clk40_ttc_out               : out std_logic; --TTC 40 MHz clock from clk240_rx_ttc_in
        clk40_in                    : in std_logic;             --clock source to synchronize TTC_out to the rest of the logic
        cdrlocked_out               : out std_logic;
        TTC_ToHost_Data_out         : out TTC_data_type;
        BUSY_IN                     : in std_logic;
        GTREFCLK0_P_IN              : in std_logic;
        GTREFCLK0_N_IN              : in std_logic;
        GTREFCLK1_P_IN              : in std_logic_vector(NUM_LTITTC_GTREFCLK1(CARD_TYPE,'1')-1 downto 0);
        GTREFCLK1_N_IN              : in std_logic_vector(NUM_LTITTC_GTREFCLK1(CARD_TYPE,'1')-1 downto 0);
        FLX182_LTI_GTREFCLK1_in     : in std_logic; --for FLX182 the GTREFCLK is shared with quad 201/refclk1
        LMK_locked_in                   : in std_logic; --For FLX712, indication that rxrefclk is stable and reset can be initialized. Always 1 for other cards
        RX_P_LTITTC                 : in std_logic;
        RX_N_LTITTC                 : in std_logic;
        TX_P_LTITTC                 : out std_logic;
        TX_N_LTITTC                 : out std_logic;
        RXUSRCLK_LTI                    : out std_logic; --for monitoring on SMA connectors (felix_top)
        TXUSRCLK_LTI                    : out std_logic; --for monitoring on SMA connectors (felix_top)
        axi_clk_in : in std_logic;
        axi_miso_ttc_lti : out axi_miso_type;
        axi_mosi_ttc_lti : in axi_mosi_type;
        LTI2cips_gpio  : out STD_LOGIC_VECTOR ( 3 downto 0 );
        cips2LTI_gpio                   : in STD_LOGIC_VECTOR ( 3 downto 0 );
        LTI_TXUSRCLK_in                 : in  std_logic_vector(GBT_NUM - 1 downto 0);

        LTI_TX_Data_Transceiver_out     : out array_32b(0 to GBT_NUM - 1);
        LTI_TX_TX_CharIsK_out           : out array_4b(0 to GBT_NUM - 1);
        toHostXoff                      : in std_logic_vector(GBT_NUM-1 downto 0);
        sim_rxdata_in                   : in std_logic_vector(35 downto 0)
    );

end ltittc_wrapper;


architecture top of ltittc_wrapper is

    signal TTC_out_i                                    : TTC_data_type := TTC_zero;
    signal TTC_emu_out_i                                : TTC_data_type := TTC_zero;


    signal RXUSRCLK_TTC           : std_logic; --240 Mhz clock rcovered from the TTC signal
    signal RX_DATA_TTC            : std_logic_vector(31 downto 0); -- from MGT
    signal Rx_Disp_Err            : std_logic_vector(3 downto 0); -- @suppress "Signal Rx_Disp_Err is never read"
    signal Rx_Code_Err            : std_logic_vector(3 downto 0); -- @suppress "Signal Rx_Code_Err is never read"
    signal LinkAligned_TTC        : std_logic;

    signal TXUSRCLK_TTC           : std_logic; --TXUSRCLK from MGT TO emulator
    signal TX_DATA_TTC            : std_logic_vector(15 downto 0); --emulated data to MGT
    signal TX_ISK_TTC                : std_logic_vector(1 downto 0);
    signal RX_IsK_TTC             : std_logic_vector(3 downto 0);

    signal decoder_data                                 : std_logic_vector(191 downto 0);
    signal clk40_ttc_i                                  : std_logic;
    signal ready                                        : std_logic;
    signal decoder_aligned                              : std_logic;
    signal decoder_crc_valid                            : std_logic;
    signal TTC_ToHost_Data_out_i                        : TTC_data_type;
    signal TTC_ToHost_Data_emu_out_i                    : TTC_data_type;
    signal select_output                                : std_logic; --0 when LTI link is enabled (and LTI clock is used), when local clock is used, enable emulator.

    signal rxusrclk_div6 : STD_LOGIC;
    signal rxusrclk_div6_clr : STD_LOGIC;
    signal GTREFCLK1_P_IN_s : std_logic;
    signal GTREFCLK1_N_IN_s : std_logic;

    signal LTI_EMU_Data     : array_32b(0 to GBT_NUM - 1);
    signal LTI_EMU_CharIsK  : array_4b(0 to GBT_NUM - 1);
    signal LTITTC_RXRESET_DONE : std_logic_vector(0 downto 0);
    signal LTITTC_CPLL_FBCLK_LOST : std_logic_vector(0 downto 0);
    signal LTITTC_PLL_LOCK_CPLL_LOCK : std_logic_vector(0 downto 0);
    signal LTITTC_RXCDR_LOCK : std_logic_vector(0 downto 0);
    signal LTITTC_PLL_LOCK_QPLL_LOCK : std_logic_vector(0 downto 0);
    signal LTITTC_RX_BYTEISALIGNED : std_logic_vector(0 downto 0);
    signal LTITTC_RX_DISP_ERROR : std_logic_vector(3 downto 0);
    signal LTITTC_RX_NOTINTABLE : std_logic_vector(3 downto 0);
    signal LTITTC_TTYPE_MONITOR_VALUE : std_logic_vector(31 downto 0);
    signal LTITTC_SL0ID_MONITOR_VALUE : std_logic_vector(31 downto 0);
    signal LTITTC_SORB_MONITOR_VALUE : std_logic_vector(31 downto 0);
    signal LTITTC_GRST_MONITOR_VALUE : std_logic_vector(31 downto 0);
    signal LTITTC_SYNC_MONITOR_VALUE : std_logic_vector(31 downto 0);
    signal LTITTC_BCR_ERR_MONITOR_VALUE : std_logic_vector(31 downto 0);
    signal LTITTC_L0ID_ERR_MONITOR_VALUE : std_logic_vector(31 downto 0);
    signal LTITTC_CRC_ERR_MONITOR_VALUE : std_logic_vector(31 downto 0);
    signal lti_fe_crc_valid: std_logic_vector(23 downto 0);


begin
    RXUSRCLK_LTI <= RXUSRCLK_TTC; --for monitoring on SMA in felix_top
    TXUSRCLK_LTI <= TXUSRCLK_TTC; --for monitoring on SMA in felix_top

    clk40_ttc_out       <= clk40_ttc_i;
    cdrlocked_out       <= ready; --MT phase2 drive ready = 1 when ttc_clk_gated is present
    select_output <= register_map_control.TTC_EMU.SEL(1) or register_map_control.MMCM_MAIN.LCLK_SEL(3);
    TTC_ToHost_Data_out <= TTC_ToHost_Data_out_i when select_output = '0' else TTC_ToHost_Data_emu_out_i;
    TTC_out   <= TTC_out_i when select_output = '0' else TTC_emu_out_i;

    --LTI FE distribution happens only for FULL mode and Interlaken
    g_LTI_FE: if FIRMWARE_MODE = FIRMWARE_MODE_INTERLAKEN or
                  FIRMWARE_MODE = FIRMWARE_MODE_FULL generate
        signal daq_fifo_flush: std_logic;
    begin
        daq_fifo_flush <= register_map_control.TTC_EMU_RESET(64) or
                          register_map_control.CRFROMHOST_RESET(64);

        TTC_LTI_0: entity work.LTI_FE_Transmitter generic map(
                GBT_NUM => GBT_NUM
            )
            port map(
                clk40 => clk40_in,
                daq_fifo_flush => daq_fifo_flush,
                TTCin => TTC_emu_out_i,
                XoffIn => toHostXoff(GBT_NUM-1 downto 0),
                LTI_TX_Data_Transceiver => LTI_EMU_Data,
                LTI_TX_TX_CharIsK => LTI_EMU_CharIsK,
                LTI_TXUSRCLK_in => LTI_TXUSRCLK_in
            );


        g_channels : for i in 0 to GBT_NUM - 1 generate
            signal RX_DATA_TTC_retimed          : std_logic_vector(32 downto 0);
            signal lti_k_data_p1                : std_logic_vector(32 downto 0);
            signal select_output_retimed        : std_logic;
            signal toHostXoff_240: std_logic;
            signal RX_Data_TTC_xoff: std_logic_vector(31 downto 0);
            signal RX_IsK_TTC_xoff: std_logic_vector(3 downto 0);
            signal training: std_logic;
        begin

            cdc_training: xpm_cdc_single generic map(
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0
                )
                port map(
                    src_clk  => '0',
                    src_in   => register_map_control.LTI_SYNC_TRAINING.FE(i),
                    dest_clk => LTI_TXUSRCLK_in(i),
                    dest_out => training
                );

            cdc_xoff: xpm_cdc_single generic map(
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 1
                )
                port map(
                    src_clk => clk40_in,
                    src_in => toHostXoff(i),
                    dest_clk => RXUSRCLK_TTC,
                    dest_out => toHostXoff_240
                );

            insertXoff: entity work.LTI_Xoff_CRC port map(
                    clk240 => RXUSRCLK_TTC,
                    XoffIn => toHostXoff_240,
                    DataIn => RX_DATA_TTC,
                    KIn => RX_IsK_TTC,
                    DataOut => RX_DATA_TTC_xoff,
                    KOut => RX_IsK_TTC_xoff
                );

            proc_pipe_ltidata: process(RXUSRCLK_TTC)
            begin
                if rising_edge(RXUSRCLK_TTC) then
                    lti_k_data_p1 <= RX_IsK_TTC_xoff(0)&RX_DATA_TTC_xoff(31 downto 0);
                end if;
            end process;

            --This is a retimer that alternates every clock cycle between two different signals, before CDC, and remultiplexes after the CDC. The total sync takes 4 clock cycles
            lti_fe_sync0: entity work.LTI_FE_Sync generic map(
                    width => 33
                )
                port map(
                    clka => RXUSRCLK_TTC,
                    clkb => LTI_TXUSRCLK_in(i),
                    din => lti_k_data_p1,
                    dout => RX_DATA_TTC_retimed,
                    training => training
                );

            lti_fe_crc_check:  entity work.LTI_CRC_check
                port map(
                    clk240 =>LTI_TXUSRCLK_in(i),
                    DataIn => RX_DATA_TTC_retimed(31 downto 0),
                    KIn => "000"& RX_DATA_TTC_retimed(32),
                    crc_valid_out => lti_fe_crc_valid(i)
                );

            --TTC_EMU.SEL = '1' transmits TTC emulator over LTI_FE, otherwise retransmit retimed LTI data
            LTI_TX_Data_Transceiver_out(i) <= LTI_EMU_Data(i) when select_output_retimed = '1' else RX_DATA_TTC_retimed(31 downto 0);
            LTI_TX_TX_CharIsK_out(i)       <= LTI_EMU_CharIsK(i) when select_output_retimed = '1' else "000"& RX_DATA_TTC_retimed(32);

            xpm_emu_sel0: xpm_cdc_single generic map(
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 1,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0
                )
                port map(
                    src_clk => '0',
                    src_in => select_output,
                    dest_clk => LTI_TXUSRCLK_in(i),
                    dest_out => select_output_retimed
                );


        end generate g_channels;
    end generate g_LTI_FE;


    --Could be an empty range, resulting in no connection, or 1
    g_assignRefclk1: for i in 0 to NUM_LTITTC_GTREFCLK1(CARD_TYPE,'1')-1 generate
        GTREFCLK1_P_IN_s <= GTREFCLK1_P_IN(i);
        GTREFCLK1_N_IN_s <= GTREFCLK1_N_IN(i);
    end generate;
    --=====================--
    -- TTC Link Wrapper--
    --=====================--
    g_simulation: if SIMULATION generate

        BUFGCE_inst : BUFGCE_DIV
            generic map(
                BUFGCE_DIVIDE => 1,         -- 1-8
                -- Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
                IS_CE_INVERTED => '0',      -- Optional inversion for CE
                IS_CLR_INVERTED => '0',     -- Optional inversion for CLR
                IS_I_INVERTED => '0',       -- Optional inversion for I
                SIM_DEVICE => "ULTRASCALE"  -- ULTRASCALE
            )
            port map (
                O => RXUSRCLK_TTC,     -- 1-bit output: Buffer
                CE => '1',
                CLR => '0',
                I => GTREFCLK0_P_IN
            );

        TXUSRCLK_TTC <= RXUSRCLK_TTC;

        LinkAligned_TTC <= '1';

        rx_disp_err <= (others => '0');
        rx_code_err <= (others => '0');

        BUFGCE_DIV_inst : BUFGCE_DIV
            generic map(
                BUFGCE_DIVIDE => 6,         -- 1-8
                -- Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
                IS_CE_INVERTED => '0',      -- Optional inversion for CE
                IS_CLR_INVERTED => '0',     -- Optional inversion for CLR
                IS_I_INVERTED => '0',       -- Optional inversion for I
                SIM_DEVICE => "ULTRASCALE"  -- ULTRASCALE
            )
            port map (
                O => rxusrclk_div6,     -- 1-bit output: Buffer
                CE => '1',
                CLR => rxusrclk_div6_clr,
                I => GTREFCLK0_P_IN
            );
        RX_DATA_TTC <= sim_rxdata_in(31 downto 0);
        RX_IsK_TTC <=  sim_rxdata_in(35 downto 32);

    else generate
        u2: entity work.FLX_LTITTCLink_Wrapper
            generic map(
                CARD_TYPE => CARD_TYPE
            )
            Port map(
                rst_hw                  => reset_in,
                register_map_control    => register_map_control,
                LTITTC_RXRESET_DONE       => LTITTC_RXRESET_DONE,
                LTITTC_CPLL_FBCLK_LOST    => LTITTC_CPLL_FBCLK_LOST,
                LTITTC_PLL_LOCK_CPLL_LOCK => LTITTC_PLL_LOCK_CPLL_LOCK,
                LTITTC_RXCDR_LOCK         => LTITTC_RXCDR_LOCK,
                LTITTC_ALIGNMENT_DONE     => open,
                LTITTC_PLL_LOCK_QPLL_LOCK => LTITTC_PLL_LOCK_QPLL_LOCK,
                LTITTC_RX_BYTEISALIGNED   => LTITTC_RX_BYTEISALIGNED,
                LTITTC_RX_DISP_ERROR      => LTITTC_RX_DISP_ERROR,
                LTITTC_RX_NOTINTABLE      => LTITTC_RX_NOTINTABLE,
                RXP_IN(0)               => RX_P_LTITTC,  --RX(24) bank 232
                RXN_IN(0)               => RX_N_LTITTC,

                --clk40_in                => clk40_in,
                LMK_locked_in           => LMK_locked_in,

                DRP_CLK_IN              => axi_clk_in,
                GTREFCLK0_P_IN          => GTREFCLK0_P_IN,
                GTREFCLK0_N_IN          => GTREFCLK0_N_IN,
                GTREFCLK1_P_IN          => GTREFCLK1_P_IN_s,
                GTREFCLK1_N_IN          => GTREFCLK1_N_IN_s,

                RXUSERCLK_OUT           => RXUSRCLK_TTC,
                RX_DATA_32b_out         => RX_DATA_TTC, --1b(disperr)+32b @ 240 MHz out
                RX_DATA_32b_rdy_out     => LinkAligned_TTC,
                RX_IsK_OUT              => RX_IsK_TTC,

                --for testing only (either simu or loopback)
                TXUSERCLK_OUT           => TXUSRCLK_TTC ,
                TX_DATA_16b_in          => TX_DATA_TTC,
                TX_ISK_in               => TX_ISK_TTC,

                TXN_OUT(0)              => TX_N_LTITTC,
                TXP_OUT(0)              => TX_P_LTITTC,
                rxusrclk_div6           => rxusrclk_div6,
                FLX182_LTI_GTREFCLK1_in => FLX182_LTI_GTREFCLK1_in,
                rxusrclk_div6_clr       => rxusrclk_div6_clr,
                axi_miso_ttc_lti        => axi_miso_ttc_lti,
                axi_mosi_ttc_lti        => axi_mosi_ttc_lti,
                LTI2cips_gpio           => LTI2cips_gpio,
                cips2LTI_gpio           => cips2LTI_gpio,
                rx_disp_err_out         => rx_disp_err,
                rx_code_err_out         => rx_code_err
            );
    end generate g_simulation;

    --=====================--
    -- DECODER
    --=====================--

    ltittc_decoder: entity work.ltittc_decoder
        port map (
            clk240_in           => RXUSRCLK_TTC,
            rxusrclk_div6       => rxusrclk_div6,
            rxusrclk_div6_clr   => rxusrclk_div6_clr,
            LinkAligned_in      => LinkAligned_TTC,
            IsK_in              => RX_IsK_TTC,
            data_in             => RX_DATA_TTC,
            --disp_err_in         => rx_disp_err,
            --code_err_in         => rx_code_err,
            data_out            => decoder_data,
            clk40_out           => clk40_ttc_i,
            clk40_ready_out     => ready,  -- the ttc_clk_gated is present
            decoder_aligned_out => decoder_aligned,
            crc_valid_out       => decoder_crc_valid
        );

    --=====================--
    -- LTITTC DATA ROUTER
    --=====================--

    ltittc_routing: entity work.ltittc_routing
        generic map(
            ITK_TRIGTAG_MODE => ITK_TRIGTAG_MODE
        )
        port map
        (
            clk40_ttc                           => clk40_ttc_i,
            clk40                               => clk40_in,
            reset                               => reset_in,
            register_map_control                => register_map_control,
            decoder_data_in                     => decoder_data,
            decoder_aligned_in                  => decoder_aligned,
            decoder_crc_valid_in                => decoder_crc_valid,
            TTC_out                             => TTC_out_i,
            TTC_EMU_out                         => TTC_EMU_out_i,
            TTC_ToHost_Data_out                 => TTC_ToHost_Data_out_i,
            TTC_ToHost_Data_emu_out             => TTC_ToHost_Data_emu_out_i,
            BUSY_IN                             => BUSY_IN,
            LTITTC_TTYPE_MONITOR_VALUE          => LTITTC_TTYPE_MONITOR_VALUE,
            LTITTC_SL0ID_MONITOR_VALUE          => LTITTC_SL0ID_MONITOR_VALUE,
            LTITTC_SORB_MONITOR_VALUE           => LTITTC_SORB_MONITOR_VALUE,
            LTITTC_GRST_MONITOR_VALUE           => LTITTC_GRST_MONITOR_VALUE,
            LTITTC_SYNC_MONITOR_VALUE           => LTITTC_SYNC_MONITOR_VALUE,
            LTITTC_BCR_ERR_MONITOR_VALUE        => LTITTC_BCR_ERR_MONITOR_VALUE,
            LTITTC_L0ID_ERR_MONITOR_VALUE       => LTITTC_L0ID_ERR_MONITOR_VALUE,
            LTITTC_CRC_ERR_MONITOR_VALUE        => LTITTC_CRC_ERR_MONITOR_VALUE
        );

    ltittc_monitoring: entity work.ltittc_monitoring
        port map
        (
            LTITTC_RXRESET_DONE => LTITTC_RXRESET_DONE,
            LTITTC_CPLL_FBCLK_LOST => LTITTC_CPLL_FBCLK_LOST,
            LTITTC_PLL_LOCK_CPLL_LOCK => LTITTC_PLL_LOCK_CPLL_LOCK,
            LTITTC_RXCDR_LOCK => LTITTC_RXCDR_LOCK,
            LTITTC_ALIGNMENT_DONE => (others => decoder_aligned),
            LTITTC_PLL_LOCK_QPLL_LOCK => LTITTC_PLL_LOCK_QPLL_LOCK,
            LTITTC_RX_BYTEISALIGNED => LTITTC_RX_BYTEISALIGNED,
            LTITTC_RX_DISP_ERROR => LTITTC_RX_DISP_ERROR,
            LTITTC_RX_NOTINTABLE => LTITTC_RX_NOTINTABLE,
            LTITTC_TTYPE_MONITOR_VALUE => LTITTC_TTYPE_MONITOR_VALUE,
            LTITTC_SL0ID_MONITOR_VALUE => LTITTC_SL0ID_MONITOR_VALUE,
            LTITTC_SORB_MONITOR_VALUE => LTITTC_SORB_MONITOR_VALUE,
            LTITTC_GRST_MONITOR_VALUE => LTITTC_GRST_MONITOR_VALUE,
            LTITTC_SYNC_MONITOR_VALUE => LTITTC_SYNC_MONITOR_VALUE,
            LTITTC_BCR_ERR_MONITOR_VALUE => LTITTC_BCR_ERR_MONITOR_VALUE,
            LTITTC_L0ID_ERR_MONITOR_VALUE => LTITTC_L0ID_ERR_MONITOR_VALUE,
            LTITTC_CRC_ERR_MONITOR_VALUE => LTITTC_CRC_ERR_MONITOR_VALUE,
            LTI_FE_CRC_VALID => lti_fe_crc_valid,
            register_map_ltittc_monitor  => register_map_ltittc_monitor
        );

    --=====================--
    -- LTITTC 4.8 data generator
    --=====================--

    ltittc_transmitter: entity work.ltittc_transmitter
        port map
        (
            reset_in       => reset_in,
            clk40_in       => clk40_in, --_ttc_i,
            clk240_in      => TXUSRCLK_TTC,
            busy_in        => BUSY_IN,
            data_out       => TX_DATA_TTC,
            isK_out        => TX_ISK_TTC
        );

end top;


--merges all the different register_map_ltittc_monitor into one

library ieee;
    use ieee.std_logic_1164.all;
    use IEEE.NUMERIC_STD.ALL;
    use ieee.numeric_std_unsigned.all;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.FELIX_package.all;
    use work.pcie_package.all;

entity ltittc_monitoring is
    Port (
        --From Link Wrapper
        LTITTC_RXRESET_DONE             : in std_logic_vector(0 downto 0);
        LTITTC_CPLL_FBCLK_LOST          : in std_logic_vector(0 downto 0);
        LTITTC_PLL_LOCK_CPLL_LOCK       : in std_logic_vector(0 downto 0);
        LTITTC_RXCDR_LOCK               : in std_logic_vector(0 downto 0);
        LTITTC_ALIGNMENT_DONE           : in std_logic_vector(0 downto 0);
        LTITTC_PLL_LOCK_QPLL_LOCK       : in std_logic_vector(0 downto 0);
        LTITTC_RX_BYTEISALIGNED         : in std_logic_vector(0 downto 0);
        LTITTC_RX_DISP_ERROR            : in std_logic_vector(3 downto 0);
        LTITTC_RX_NOTINTABLE            : in std_logic_vector(3 downto 0);
        --From decoder / routing
        LTITTC_TTYPE_MONITOR_VALUE      : in std_logic_vector(31 downto 0);
        LTITTC_SL0ID_MONITOR_VALUE      : in std_logic_vector(31 downto 0);
        LTITTC_SORB_MONITOR_VALUE       : in std_logic_vector(31 downto 0);
        LTITTC_GRST_MONITOR_VALUE       : in std_logic_vector(31 downto 0);
        LTITTC_SYNC_MONITOR_VALUE       : in std_logic_vector(31 downto 0);
        LTITTC_BCR_ERR_MONITOR_VALUE    : in std_logic_vector(31 downto 0);
        LTITTC_L0ID_ERR_MONITOR_VALUE   : in std_logic_vector(31 downto 0);
        LTITTC_CRC_ERR_MONITOR_VALUE    : in std_logic_vector(31 downto 0);

        LTI_FE_CRC_VALID                : in std_logic_vector(23 downto 0);

        register_map_ltittc_monitor                 : out register_map_ltittc_monitor_type
    );
end ltittc_monitoring;

architecture Behavioral of ltittc_monitoring is

begin

    --type register_map_ltittc_monitor_type is record
    --    LTITTC_ALIGNMENT_DONE   : std_logic_vector(0 downto 0);
    --    LTITTC_CPLL_FBCLK_LOST  : std_logic_vector(0 downto 0);
    --    LTITTC_PLL_LOCK         : bitfield_ltittc_pll_lock_r_type;
    --    LTITTC_RXCDR_LOCK       : std_logic_vector(0 downto 0);
    --    LTITTC_RXRESET_DONE     : std_logic_vector(0 downto 0);
    --    LTITTC_RX_BYTEISALIGNED : std_logic_vector(0 downto 0);
    --    LTITTC_RX_DISP_ERROR    : std_logic_vector(3 downto 0);
    --    LTITTC_RX_NOTINTABLE    : std_logic_vector(3 downto 0);
    --    LTITTC_SL0ID_MONITOR    : bitfield_ltittc_sl0id_monitor_r_type;
    --    LTITTC_SORB_MONITOR     : bitfield_ltittc_sorb_monitor_r_type;
    --    LTITTC_GRST_MONITOR     : bitfield_ltittc_grst_monitor_r_type;
    --    LTITTC_SYNC_MONITOR     : bitfield_ltittc_sync_monitor_r_type;
    --    LTITTC_TTYPE_MONITOR    : bitfield_ltittc_ttype_monitor_r_type;
    --    LTITTC_L0ID_ERR_MONITOR : bitfield_ltittc_l0id_err_monitor_r_type;
    --    LTITTC_BCR_ERR_MONITOR  : bitfield_ltittc_bcr_err_monitor_r_type;
    --    LTITTC_CRC_ERR_MONITOR  : bitfield_ltittc_crc_err_monitor_r_type;
    --end record;

    --from LTI link wrapper
    register_map_ltittc_monitor.LTITTC_RXRESET_DONE        <= LTITTC_RXRESET_DONE;
    register_map_ltittc_monitor.LTITTC_CPLL_FBCLK_LOST     <= LTITTC_CPLL_FBCLK_LOST;
    register_map_ltittc_monitor.LTITTC_PLL_LOCK.CPLL_LOCK  <= LTITTC_PLL_LOCK_CPLL_LOCK;
    register_map_ltittc_monitor.LTITTC_RXCDR_LOCK          <= LTITTC_RXCDR_LOCK;
    register_map_ltittc_monitor.LTITTC_ALIGNMENT_DONE      <= LTITTC_ALIGNMENT_DONE ;
    register_map_ltittc_monitor.LTITTC_PLL_LOCK.QPLL_LOCK  <= LTITTC_PLL_LOCK_QPLL_LOCK;
    register_map_ltittc_monitor.LTITTC_RX_BYTEISALIGNED    <= LTITTC_RX_BYTEISALIGNED;
    register_map_ltittc_monitor.LTITTC_RX_DISP_ERROR       <= LTITTC_RX_DISP_ERROR;
    register_map_ltittc_monitor.LTITTC_RX_NOTINTABLE       <= LTITTC_RX_NOTINTABLE;

    --from LTI routing
    register_map_ltittc_monitor.LTITTC_TTYPE_MONITOR.VALUE      <= LTITTC_TTYPE_MONITOR_VALUE;
    register_map_ltittc_monitor.LTITTC_SL0ID_MONITOR.VALUE      <= LTITTC_SL0ID_MONITOR_VALUE;
    register_map_ltittc_monitor.LTITTC_SORB_MONITOR.VALUE       <= LTITTC_SORB_MONITOR_VALUE;
    register_map_ltittc_monitor.LTITTC_GRST_MONITOR.VALUE       <= LTITTC_GRST_MONITOR_VALUE;
    register_map_ltittc_monitor.LTITTC_SYNC_MONITOR.VALUE       <= LTITTC_SYNC_MONITOR_VALUE;
    register_map_ltittc_monitor.LTITTC_BCR_ERR_MONITOR.VALUE    <= LTITTC_BCR_ERR_MONITOR_VALUE;
    register_map_ltittc_monitor.LTITTC_L0ID_ERR_MONITOR.VALUE   <= LTITTC_L0ID_ERR_MONITOR_VALUE;
    register_map_ltittc_monitor.LTITTC_CRC_ERR_MONITOR.VALUE    <= LTITTC_CRC_ERR_MONITOR_VALUE;

    register_map_ltittc_monitor.LTI_FE_CRC_VALID                <= LTI_FE_CRC_VALID;

end Behavioral;

----------------------------------------------------------------------------------
--! Company:  Nikhef
--! Engineer: Melvin Leguijt
----------------------------------------------------------------------------------

--MGT with Aurora8b10b presets show no disperr when using TX_DATA_gt_32b_tmp= x"bc" & x"000000" as
--soon as alignment is achieved. When using wavegen data, some disperr right
--after alignment. After a bit no disperr anymore
--MGT from scratch disperr also with TX_DATA_gt_32b_tmp
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.FELIX_package.all;
library UNISIM;
    use UNISIM.VCOMPONENTS.ALL;
library XPM;
    use xpm.vcomponents.all;


--***************************** Entity Declaration ****************************
entity gty_ltittclink_wrapper_versal is
    port(
        --from LMK to RX REFCLK. Always clk40_xtal to ensure clock recovery which genetrates clk40_ttc
        GTREFCLK0_P_IN               : in  std_logic;
        GTREFCLK0_N_IN               : in  std_logic;

        --from SI5345 to TX REFCLK. Can be either clk40_xtal or clk40_ttc
        --Needs external IBUFDS_GTE, since it may be shared with a clock buffer from the link
        GT_REFCLK1_in                : in STD_LOGIC;
        --Reset signal for the clock buffer dividing RXUSRCLK by 6 to get a proper phase of clk40
        rxusrclk_div6_clr            : in STD_LOGIC;
        --RXUSRCLK divided by 6
        rxusrclk_div6                : out STD_LOGIC;
        --GTREFCLK1_P_IN               : in  std_logic;
        --GTREFCLK1_N_IN               : in  std_logic;
        APB3_CLK_IN                  : in  std_logic;

        --- RX clock, for each channel
        gt_rxusrclk_out              : out std_logic;
        --simu or loopback
        --gt_txusrclk_in             : in   std_logic;
        gt_txusrclk_out              : out std_logic;
        --
        -----------------------------------------
        ---- Control signals
        -----------------------------------------
        --        loopback_in                  : in std_logic_vector(2 downto 0);
        reset_all_in                 : in  std_logic;
        --        cpllreset_in                 : in std_logic;
        reset_tx_pll_and_datapath_in : in  std_logic;
        reset_tx_datapath_in         : in  std_logic;
        reset_rx_pll_and_datapath_in : in  std_logic;
        reset_rx_datapath_in         : in  std_logic;
        -----------------------------------------
        ---- STATUS signals
        -----------------------------------------
        gt_rxbyteisaligned_out       : out std_logic;

        gt_rxresetdone_out           : out std_logic;

        --gt_cpllfbclklost_out         : out std_logic;
        gt_cplllock_out              : out std_logic;
        gt_cpllrefclklost_out        : out std_logic;
        --gt_rxcdrlock_out             : out std_logic;

        --gt_qpll1refclklost_out       : out std_logic;
        gt_qpll1lock_out             : out std_logic;
        ----------------------------------------------------------------
        ----------RESET SIGNALs
        ----------------------------------------------------------------
        --    SOFT_RESET_IN               : in     std_logic;
        --    GTRX_RESET_IN               : in   std_logic;
        --    CPLL_RESET_IN               : in   std_logic;
        --    QPLL_RESET_IN               : in   std_logic;

        --    SOFT_RXRST_GT               : in   std_logic;

        --    SOFT_RXRST_ALL              : in   std_logic;

        -----------------------------------------------------------
        ----------- Data and RX Ports
        -----------------------------------------------------------
        RX_DATA_gt_32b               : out std_logic_vector(31 downto 0); --[31:0]=payload
        rx_isk_out                 : out std_logic_vector(3 downto 0);
        rx_disp_err_out            : out std_logic_vector(3 downto 0);
        rx_code_err_out            : out std_logic_vector(3 downto 0);
        TX_DATA_gt_16b               : in  std_logic_vector(15 downto 0);
        TX_isK_gt                    : in  std_logic_vector(1 downto 0);
        TXN_OUT                      : out std_logic_vector(0 downto 0);
        TXP_OUT                      : out std_logic_vector(0 downto 0);
        RXN_IN                       : in  std_logic_vector(0 downto 0);
        RXP_IN                       : in  std_logic_vector(0 downto 0);

        axi_miso_ttc_lti : out axi_miso_type;
        axi_mosi_ttc_lti : in axi_mosi_type;
        LTI2cips_gpio  : out STD_LOGIC_VECTOR ( 3 downto 0 );
        cips2LTI_gpio : in STD_LOGIC_VECTOR ( 3 downto 0 )
    );
end gty_ltittclink_wrapper_versal;

architecture RTL of gty_ltittclink_wrapper_versal is

    --    signal    GTH_RefClk0       : std_logic; --SI5345/TX
    --    signal    GTH_RefClk1             : std_logic; --LMK/RX
    --signal unused_AXI_LITE_araddr :std_logic_vector(1 downto 0);
    --signal unused_AXI_LITE_awaddr :std_logic_vector(1 downto 0);


    --signal rxcdrlock_out       : std_logic_vector(0 downto 0);

    signal cplllock_out    : std_logic_vector(0 downto 0);
    signal rxresetdone_out : std_logic_vector(0 downto 0);
    signal txresetdone_out : std_logic_vector(0 downto 0);

    signal qpll1lock_out : std_logic_vector(0 downto 0);

    signal reset_all : std_logic_vector(0 downto 0);

    signal reset_rx_pll_and_datapath : std_logic_vector(0 downto 0);
    signal reset_rx_datapath         : std_logic_vector(0 downto 0);

    signal reset_rx_datapath_casino : std_logic;

    signal gt_drp_clk_in : std_logic_vector(0 downto 0);

    signal rxusrclk_out : std_logic_vector(0 downto 0);

    component transceiver_versal_LTITTC_wrapper -- @suppress "Component declaration 'transceiver_versal_LTITTC_wrapper' has none or multiple matching entity declarations"
        port (
            AXI_LITE_0_araddr : in STD_LOGIC_VECTOR ( 17 downto 0 );
            AXI_LITE_0_arready : out STD_LOGIC;
            AXI_LITE_0_arvalid : in STD_LOGIC;
            AXI_LITE_0_awaddr : in STD_LOGIC_VECTOR ( 17 downto 0 );
            AXI_LITE_0_awready : out STD_LOGIC;
            AXI_LITE_0_awvalid : in STD_LOGIC;
            AXI_LITE_0_bready : in STD_LOGIC;
            AXI_LITE_0_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
            AXI_LITE_0_bvalid : out STD_LOGIC;
            AXI_LITE_0_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
            AXI_LITE_0_rready : in STD_LOGIC;
            AXI_LITE_0_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
            AXI_LITE_0_rvalid : out STD_LOGIC;
            AXI_LITE_0_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
            AXI_LITE_0_wready : out STD_LOGIC;
            AXI_LITE_0_wvalid : in STD_LOGIC;
            GT_REFCLK1_0 : in STD_LOGIC;
            GT_Serial_grx_n : in STD_LOGIC_VECTOR ( 3 downto 0 );
            GT_Serial_grx_p : in STD_LOGIC_VECTOR ( 3 downto 0 );
            GT_Serial_gtx_n : out STD_LOGIC_VECTOR ( 3 downto 0 );
            GT_Serial_gtx_p : out STD_LOGIC_VECTOR ( 3 downto 0 );
            apb3clk_gt_bridge_ip_0 : in STD_LOGIC;
            ch0_eyescanreset_ext_0 : in STD_LOGIC;
            ch0_rxbyteisaligned_ext_0 : out STD_LOGIC;
            ch0_rxbyterealign_ext_0 : out STD_LOGIC;
            ch0_rxcdrlock_ext_0 : out STD_LOGIC;
            ch0_rxcominitdet_0 : out STD_LOGIC;
            ch0_rxcommadet_0 : out STD_LOGIC;
            ch0_rxcommadet_ext_0 : out STD_LOGIC;
            ch0_rxcomsasdet_0 : out STD_LOGIC;
            ch0_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
            ch0_rxphdlyresetdone_ext_0 : out STD_LOGIC;
            ch0_rxresetdone_ext_0 : out STD_LOGIC;
            ch0_rxslide_ext_0 : in STD_LOGIC;
            ch0_rxsliderdy_ext_0 : out STD_LOGIC;
            ch0_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
            ch0_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
            ch0_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
            ch0_txheader_ext_0 : in STD_LOGIC_VECTOR ( 5 downto 0 );
            ch0_txpolarity_ext_0 : in STD_LOGIC;
            ch0_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
            gt_bridge_ip_0_diff_gt_ref_clock_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
            gt_bridge_ip_0_diff_gt_ref_clock_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
            gt_reset_gt_bridge_ip_0 : in STD_LOGIC;
            lcpll_lock_gt_bridge_ip_0 : out STD_LOGIC;
            link_status_gt_bridge_ip_0 : out STD_LOGIC;
            rate_sel_gt_bridge_ip_0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
            refclk_odiv2 : out STD_LOGIC;
            reset_rx_datapath_in_0 : in STD_LOGIC;
            reset_rx_pll_and_datapath_in_0 : in STD_LOGIC;
            reset_tx_datapath_in_0 : in STD_LOGIC;
            reset_tx_pll_and_datapath_in_0 : in STD_LOGIC;
            rpll_lock_gt_bridge_ip_0 : out STD_LOGIC;
            rx_resetdone_out_gt_bridge_ip_0 : out STD_LOGIC;
            rxusrclk_div6 : out STD_LOGIC_VECTOR ( 0 to 0 );
            rxusrclk_div6_clr : in STD_LOGIC;
            rxusrclk_gt_bridge_ip_0 : out STD_LOGIC;
            s_axi_lite_clk_0 : in STD_LOGIC;
            s_axi_lite_resetn_0 : in STD_LOGIC;
            tx_resetdone_out_gt_bridge_ip_0 : out STD_LOGIC;
            txusrclk_gt_bridge_ip_0 : out STD_LOGIC;
            usrclk_1 : out STD_LOGIC
        );
    end component transceiver_versal_LTITTC_wrapper;



    signal txusrclk_out : std_logic_vector(0 downto 0);

    signal reset_tx_pll_and_datapath : std_logic_vector(0 downto 0);
    signal reset_tx_datapath         : std_logic_vector(0 downto 0);

    --for simu only and loopback=0
    signal TXN_i               : std_logic_vector(0 downto 0) := "0";
    signal TXP_i               : std_logic_vector(0 downto 0) := "0";
    signal RXN_i               : std_logic_vector(0 downto 0) := "0";
    signal RXP_i               : std_logic_vector(0 downto 0) := "0";
    --
    --signal TX_DATA_gt_32b_tmp  : std_logic_vector(31 downto 0); --simu only or loopback -- @suppress "signal TX_DATA_gt_32b_tmp is never read"
    --signal TX_isK_gt_tmp       : std_logic_vector(3 downto 0); --simu only or loopback -- @suppress "signal TX_isK_gt_tmp is never read"
    signal ch0_rx_data_raw     : STD_LOGIC_VECTOR(127 downto 0);
    signal gtyrxn_in           : std_logic_vector(3 downto 0);
    signal gtyrxp_in           : std_logic_vector(3 downto 0);
    signal gtytxp_out          : std_logic_vector(3 downto 0);
    signal gtytxn_out          : std_logic_vector(3 downto 0);
    signal ch0_rxresetdone_out : std_logic_vector(0 downto 0);

    signal ch0_rx_data_raw_reversed     : STD_LOGIC_VECTOR(39 downto 0);


    signal decoder_aligned :std_logic;
    signal reset_dec_8b10b : std_logic;
    signal ch0_rxresetdone_rxoutclk : std_logic;
    attribute MARK_DEBUG : string;
    attribute MARK_DEBUG of ch0_rx_data_raw_reversed, RX_DATA_gt_32b, rx_isk_out, rx_disp_err_out, rx_code_err_out : signal is "TRUE";

--**************************** Main Body of Code *******************************

begin
    --------------------------------
    ---- Reset
    reset_all(0)                 <= reset_all_in;
    reset_tx_pll_and_datapath(0) <= reset_tx_pll_and_datapath_in;
    reset_tx_datapath(0)         <= reset_tx_datapath_in or cips2LTI_gpio(2);
    reset_rx_pll_and_datapath(0) <= reset_rx_pll_and_datapath_in or cips2LTI_gpio(0);
    reset_rx_datapath(0)         <= reset_rx_datapath_in or reset_rx_datapath_casino or cips2LTI_gpio(3);

    --------------------------------
    ---- Monitor
    LTI2cips_gpio(2) <= decoder_aligned;
    LTI2cips_gpio(1) <= rxresetdone_out(0);
    LTI2cips_gpio(0) <= txresetdone_out(0);

    --gt_rxcdrlock_out       <= rxcdrlock_out(0);
    gt_rxbyteisaligned_out <= decoder_aligned;


    gt_rxresetdone_out <= rxresetdone_out(0);

    gt_cplllock_out       <= cplllock_out(0);
    gt_cpllrefclklost_out <= not cplllock_out(0);

    gt_qpll1lock_out <= qpll1lock_out(0);

    gt_drp_clk_in(0) <= APB3_CLK_IN;

    gt_rxusrclk_out <= rxusrclk_out(0);
    gt_txusrclk_out <= txusrclk_out(0);

    gtyrxn_in <= "000" & RXN_i;         -- TODO:
    gtyrxp_in <= "000" & RXP_i;         -- TODO:
    TXN_i(0)  <= gtytxn_out(0);         -- TODO:
    TXP_i(0)  <= gtytxp_out(0);         -- TODO:


    transceiver_versal_LTITTC_i : transceiver_versal_LTITTC_wrapper
        port map(
            GT_Serial_grx_n(3 downto 0)               => gtyrxn_in,
            GT_Serial_grx_p(3 downto 0)               => gtyrxp_in,
            GT_Serial_gtx_n(3 downto 0)               => gtytxn_out, -- TODO:
            GT_Serial_gtx_p(3 downto 0)               => gtytxp_out, -- TODO:
            apb3clk_gt_bridge_ip_0                    => gt_drp_clk_in(0),
            gt_bridge_ip_0_diff_gt_ref_clock_clk_n(0) => GTREFCLK0_N_IN,
            gt_bridge_ip_0_diff_gt_ref_clock_clk_p(0) => GTREFCLK0_P_IN,
            gt_reset_gt_bridge_ip_0                   => reset_all(0),
            lcpll_lock_gt_bridge_ip_0                 => cplllock_out(0),
            link_status_gt_bridge_ip_0                => open,
            rate_sel_gt_bridge_ip_0(3 downto 0)       => "0000",
            rpll_lock_gt_bridge_ip_0                  => qpll1lock_out(0),
            rx_resetdone_out_gt_bridge_ip_0           => rxresetdone_out(0),
            rxusrclk_gt_bridge_ip_0                   => open,--rxoutclk_out(0),
            tx_resetdone_out_gt_bridge_ip_0           => txresetdone_out(0),
            txusrclk_gt_bridge_ip_0                   => txusrclk_out(0), -- TODO:
            reset_tx_pll_and_datapath_in_0            => reset_tx_pll_and_datapath(0),
            reset_tx_datapath_in_0                    => reset_tx_datapath(0),
            reset_rx_pll_and_datapath_in_0            => reset_rx_pll_and_datapath(0),
            reset_rx_datapath_in_0                    => reset_rx_datapath(0),
            ch0_txdata_ext_0                          => x"0000_0000_0000_0000_0000_0000_0000" & TX_DATA_gt_16b, -- TODO:
            ch0_rxdata_ext_0                          => ch0_rx_data_raw,
            ch0_rxcdrlock_ext_0                       => open,
            ch0_rxctrl3_ext_0                         => open,
            ch0_rxctrl1_ext_0                         => open,
            ch0_rxctrl0_ext_0                         => open,
            ch0_rxctrl2_ext_0                         => open,
            ch0_txctrl0_ext_0                         => (others => '0'),
            ch0_txctrl1_ext_0                         => (others => '0'),
            ch0_rxbyteisaligned_ext_0                 => open, --rxbyteisaligned_out(0),
            ch0_txctrl2_ext_0                         => "000000" & TX_isK_gt,
            ch0_rxresetdone_ext_0                     => ch0_rxresetdone_out(0),
            ch0_rxcommadet_ext_0                      => open,
            ch0_rxsliderdy_ext_0                      => open,
            ch0_rxcommadet_0                          => open,
            ch0_rxcominitdet_0                        => open,
            ch0_rxcomsasdet_0                         => open,
            ch0_rxphdlyresetdone_ext_0                => open,
            ch0_rxbyterealign_ext_0                   => open,
            ch0_rxslide_ext_0                         => '0',
            ch0_txheader_ext_0 => (others => '0'),
            ch0_txsequence_ext_0 => (others => '0'),
            ch0_txpolarity_ext_0 => '0',
            --gt_bridge_ip_1_diff_gt_ref_clock_clk_p(0) => GTREFCLK1_P_IN,
            --gt_bridge_ip_1_diff_gt_ref_clock_clk_n(0) => GTREFCLK1_N_IN,

            usrclk_1 => rxusrclk_out(0),

            AXI_LITE_0_arready => axi_miso_ttc_lti.AXI_LITE_0_arready,
            AXI_LITE_0_awready => axi_miso_ttc_lti.AXI_LITE_0_awready,
            AXI_LITE_0_bresp => axi_miso_ttc_lti.AXI_LITE_0_bresp,
            AXI_LITE_0_bvalid => axi_miso_ttc_lti.AXI_LITE_0_bvalid,
            AXI_LITE_0_rdata => axi_miso_ttc_lti.AXI_LITE_0_rdata,
            AXI_LITE_0_rresp => axi_miso_ttc_lti.AXI_LITE_0_rresp,
            AXI_LITE_0_rvalid => axi_miso_ttc_lti.AXI_LITE_0_rvalid,
            AXI_LITE_0_wready => axi_miso_ttc_lti.AXI_LITE_0_wready,

            AXI_LITE_0_araddr(17 downto 16) => "00",
            AXI_LITE_0_araddr(15 downto 0) => axi_mosi_ttc_lti.AXI_LITE_0_araddr,
            AXI_LITE_0_arvalid => axi_mosi_ttc_lti.AXI_LITE_0_arvalid,
            AXI_LITE_0_awaddr(17 downto 16) => "00",
            AXI_LITE_0_awaddr(15 downto 0) => axi_mosi_ttc_lti.AXI_LITE_0_awaddr,
            AXI_LITE_0_awvalid => axi_mosi_ttc_lti.AXI_LITE_0_awvalid,
            AXI_LITE_0_bready => axi_mosi_ttc_lti.AXI_LITE_0_bready,
            AXI_LITE_0_rready => axi_mosi_ttc_lti.AXI_LITE_0_rready,
            AXI_LITE_0_wdata => axi_mosi_ttc_lti.AXI_LITE_0_wdata,
            AXI_LITE_0_wvalid => axi_mosi_ttc_lti.AXI_LITE_0_wvalid,
            s_axi_lite_clk_0                => gt_drp_clk_in(0),
            s_axi_lite_resetn_0             => axi_mosi_ttc_lti.s_axi_lite_resetn_0,
            ch0_eyescanreset_ext_0 => cips2LTI_gpio(1),
            GT_REFCLK1_0 => GT_REFCLK1_in,
            rxusrclk_div6_clr => rxusrclk_div6_clr,
            rxusrclk_div6(0) => rxusrclk_div6,
            refclk_odiv2 => open --rxrefclk_odiv2
        );

    --TX_DATA_gt_32b_tmp <= x"bc" & x"000000";
    --TX_isK_gt_tmp      <= "1000";

    g_reverse : for i in 0 to 9 generate
        ch0_rx_data_raw_reversed(i) <= ch0_rx_data_raw(9 - i);
        ch0_rx_data_raw_reversed(i+10) <= ch0_rx_data_raw(19 - i);
        ch0_rx_data_raw_reversed(i+20) <= ch0_rx_data_raw(29 - i);
        ch0_rx_data_raw_reversed(i+30) <= ch0_rx_data_raw(39 - i);
    end generate;

    sync_rxresetdone: xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => '0',
            src_in => ch0_rxresetdone_out(0),
            dest_clk => rxusrclk_out(0),
            dest_out => ch0_rxresetdone_rxoutclk
        );

    reset_dec_8b10b <= not ch0_rxresetdone_rxoutclk;

    dec_8b10b : entity work.dec_8b10b_4x
        port map(
            reset_i           => reset_dec_8b10b, --! Active high reset
            clk_i             => rxusrclk_out(0), --! 240.474 MHz rxoutclk from transceiver
            data_i            => ch0_rx_data_raw_reversed(39 downto 0), --! raw 8b10b encoded data from transceiver
            data_o            => RX_DATA_gt_32b, --! decoded data output
            k_o               => rx_isk_out,   --! decoded CharIsK output
            code_err_o        => rx_code_err_out,  --! 8b10b coding error (1 per byte)
            disp_err_o        => rx_disp_err_out,  --! 8b10b disparity error (1 per byte)
            rxslide_o         => reset_rx_datapath_casino, --! bitslip output, connect to transceiver
            decoder_aligned_o => decoder_aligned   --! indication that K28.5 is found
        );



    TXN_OUT <= TXN_i;
    TXP_OUT <= TXP_i;
    RXN_i <= RXN_IN;
    RXP_i <= RXP_IN;



end RTL;

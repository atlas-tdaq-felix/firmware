----------------------------------------------------------------------------------
--! Company:  Argonne National Laboratory
--! Engineer: Marco Trovato (mtrovato@anl.gov)
--! Update 18-08-2023
--! Company:  NIKHEF
--! Engineer: Mesfin Gebyehu (mgebyehu@nikhef.nl)
----------------------------------------------------------------------------------
----------------------------------------------------------------------------------

--Assumption: data_in(7 downto 0) = K28.5


library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.FELIX_package.all;


--use work.all;
entity ltittc_decoder is
    port
    (
        --reset_in                  : in std_logic;
        clk240_in                 : in std_logic;
        rxusrclk_div6             : in std_logic;
        rxusrclk_div6_clr         : out std_logic;
        --@clk240_in
        LinkAligned_in            : in std_logic;
        IsK_in                    : in std_logic_vector(3 downto 0);
        data_in                   : in std_logic_vector(31 downto 0);
        --disp_err_in               : in std_logic_vector(3 downto 0);
        --code_err_in               : in std_logic_vector(3 downto 0);
        --@clk40_out
        data_out                  : out std_logic_vector(191 downto 0);
        clk40_out                 : out std_logic;
        clk40_ready_out           : out std_logic; --@clk240_in
        decoder_aligned_out       : out std_logic;
        crc_valid_out             : out std_logic
    );

end ltittc_decoder;

architecture top of ltittc_decoder is

    --    type   smtype  is (CLKSTART,ALIGN,COMMA_FOUND);
    --    constant   smtype : std_logic_vector(2 downto 0); -- is (CLKSTART,ALIGN,COMMA_FOUND);
    signal bufgdif6_clr_done: std_logic;
    type detector_state_type is (ALIGN, COMMA_FOUND);
    signal detector_state       : detector_state_type;
    signal data_in32       : std_logic_vector(31 downto 0);
    signal cnt                  : integer range 0 to 5 := 0;
    signal clk40_ready_i   : std_logic                      := '0';
    signal clk40_out_i     : std_logic := '0';
    signal data_out_i      : std_logic_vector(191 downto 0) := (others => '0');
    signal crc_reset, crc_en                                        : std_logic;
    signal crc_out                                                  : std_logic_vector(15 downto 0);
    signal crc_in: std_logic_vector(31 downto 0);

    attribute MARK_DEBUG: string;
    attribute MARK_DEBUG of LinkAligned_in, IsK_in, data_in, clk40_ready_out, decoder_aligned_out, crc_valid_out: signal is "TRUE";

begin

    data_in32  <= data_in(31 downto 0);


    decoder_aligned_out <= '1' when detector_state = COMMA_FOUND and LinkAligned_in = '1' else '0';

    crc_reset <= '1' when cnt = 5 else '0';
    crc_en <= not crc_reset;
    crc_in <= data_in32 when cnt /= 5 else (others => '1');


    CRC16_0 : entity work.crc16_lti
        port map(
            data_in => crc_in,
            crc_en  => crc_en,
            rst     => crc_reset,
            clk     => clk240_in,
            crc_out => crc_out
        );

    GEARBOX_CLKST_proc : process(clk240_in)
        variable data_i        : std_logic_vector(191 downto 0);
    begin
        if clk240_in'event and clk240_in='1' then
            case detector_state is
                when ALIGN =>
                    clk40_ready_i     <= '0';
                    bufgdif6_clr_done <= '0';
                    data_i            := (others=>'0');
                    cnt               <= 0;
                    crc_valid_out     <= '0';
                    if (data_in32(15 downto 0) = x"50" & Kchar_comma and IsK_in = "0001") then
                        detector_state                             <= COMMA_FOUND;
                    else
                        detector_state                             <= ALIGN;
                    end if;
                when COMMA_FOUND =>
                    if rxusrclk_div6_clr = '1' then
                        bufgdif6_clr_done <= '1';
                        clk40_ready_i <= '1';
                    end if;
                    if (cnt /= 5) then
                        detector_state                             <= COMMA_FOUND;
                        data_i(32*cnt+31 downto 32*cnt)   := data_in32; --writing word 0 to 4
                        cnt                               <= cnt+1;
                    else
                        cnt                           <= 0;
                        if (data_in32(15 downto 0) = x"50" & Kchar_comma and IsK_in = "0001") then         --double check that comma appear again
                            detector_state                           <= COMMA_FOUND;
                            data_i(32*cnt+31 downto 32*cnt) := data_in32(31 downto 8) & x"00"; --writing word 5, dropping comma of next bunch (since MGT is 4 byte aligned to comma it will have comma in the LSByte)
                        else --comma not found again in the same place.
                            data_i                          := (others=>'0');
                            detector_state                         <= ALIGN;
                        end if; --if (data_in32(7 downto 0) = Kchar_comma)
                        if(crc_out = data_in32(31 downto 16)) then
                            data_out_i <= data_i; --every 6 clocks
                            crc_valid_out <= '1';
                        else
                            data_out_i <= (others => '0'); --invalidate data in case of CRC error
                            crc_valid_out <= '0';
                        end if;

                    end if;

                when others => detector_state <= ALIGN; -- @suppress "Case statement contains all choices explicitly. You can safely remove the redundant 'others'"

            end case;
        --            end if;

        end if;
    end process;

    ---------------------------
    ---*********CLK*********
    ----------------------------
    --RL: cnt_fr was added to make sure there is clock when the decoder state machine is in the ALIGN state



    CLK4_align_proc: process(clk240_in)
    begin
        if rising_edge(clk240_in) then
            rxusrclk_div6_clr <= '0';
            if ((cnt = 2) and detector_state = COMMA_FOUND) then
                if  bufgdif6_clr_done = '0' then --Reset the BUFG_GT (div6) only once at the right phase
                    rxusrclk_div6_clr <= '1';
                end if;
            end if;
        end if;
    end process;

    clk40_out_i <= rxusrclk_div6;

    clk40_out       <= clk40_out_i;
    clk40_ready_out <= clk40_ready_i;

    RETIMING : process(clk40_out_i)
    begin
        if rising_edge(clk40_out_i) then
            --double register needed for simu
            data_out         <= data_out_i;
        end if;
    end process;



end top;



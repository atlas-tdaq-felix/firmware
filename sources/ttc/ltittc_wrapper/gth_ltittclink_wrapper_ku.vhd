----------------------------------------------------------------------------------
--! Company:  Argonne National Laboratory
--! Engineer: Marco Trovato (mtrovato@anl.gov)
----------------------------------------------------------------------------------

--MGT with Aurora8b10b presets show no disperr when using TX_DATA_gt_32b_tmp= x"bc" & x"000000" as
--soon as alignment is achieved. When using wavegen data, some disperr right
--after alignment. After a bit no disperr anymore
--MGT from scratch disperr also with TX_DATA_gt_32b_tmp
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
library UNISIM;
    use UNISIM.VCOMPONENTS.ALL;
library XPM;
    use XPM.VComponents.all;


--***************************** Entity Declaration ****************************
entity gth_ltittclink_wrapper_ku is
    port(
        --from SI5345 to TX REFCLK. Can be either clk40_xtal or clk40_ttc
        GTREFCLK0_P_IN                  : in std_logic;
        GTREFCLK0_N_IN                  : in std_logic;
        --from LMK to RX REFCLK. Always clk40_xtal to ensure clock recovery which genetrates clk40_ttc
        GTREFCLK1_P_IN                  : in std_logic;
        GTREFCLK1_N_IN                  : in std_logic;
        DRP_CLK_IN                      : in   std_logic;

        --- RX clock, for each channel
        rxusrclk_out             : out   std_logic;
        txusrclk_out             : out   std_logic;

        rxusrclk_div6            : out std_logic;
        rxusrclk_div6_clr        : in std_logic;

        --
        -----------------------------------------
        ---- Control signals
        -----------------------------------------
        reset_all_in                 : in std_logic;
        cpllreset_in                 : in std_logic;
        reset_tx_pll_and_datapath_in : in std_logic;
        reset_tx_datapath_in         : in std_logic;
        reset_rx_pll_and_datapath_in : in std_logic;
        reset_rx_datapath_in         : in std_logic;
        -----------------------------------------
        ---- STATUS signals
        -----------------------------------------
        gt_rxbyteisaligned_out      : out  std_logic;

        gt_rxresetdone_out          : out  std_logic;

        gt_cpllfbclklost_out        : out  std_logic;
        gt_cplllock_out             : out  std_logic;
        gt_cpllrefclklost_out       : out  std_logic;
        gt_rxcdrlock_out            : out  std_logic;

        gt_qpll1refclklost_out      : out std_logic;
        gt_qpll1lock_out            : out std_logic;
        -----------------------------------------------------------
        ----------- Data and RX Ports
        -----------------------------------------------------------
        RX_DATA_gt_32b             : out std_logic_vector(31 downto 0); --[31:0]=payload
        rx_isk_out                 : out std_logic_vector(3 downto 0);
        rx_disp_err_out            : out std_logic_vector(3 downto 0);
        rx_code_err_out            : out std_logic_vector(3 downto 0);

        TX_DATA_gt_16b             : in std_logic_vector(15 downto 0);
        TX_isK_gt                  : in std_logic_vector(1 downto 0);
        TXN_OUT                    : out   std_logic_vector(0 downto 0);
        TXP_OUT                    : out   std_logic_vector(0 downto 0);
        RXN_IN                     : in   std_logic_vector(0 downto 0);
        RXP_IN                     : in   std_logic_vector(0 downto 0)

    );
end gth_ltittclink_wrapper_ku;

architecture RTL of gth_ltittclink_wrapper_ku is

    component gtwizard_ttc_rxcpll -- @suppress "Component declaration 'gtwizard_ttc_rxcpll' has none or multiple matching entity declarations"
        port (
            gtwiz_userclk_tx_active_in : in std_logic_vector(0 downto 0);
            gtwiz_userclk_rx_active_in : in std_logic_vector(0 downto 0);
            gtwiz_buffbypass_tx_reset_in : in std_logic_vector(0 downto 0);
            gtwiz_buffbypass_tx_start_user_in : in std_logic_vector(0 downto 0);
            gtwiz_buffbypass_tx_done_out : out std_logic_vector(0 downto 0);
            gtwiz_buffbypass_tx_error_out : out std_logic_vector(0 downto 0);
            gtwiz_buffbypass_rx_reset_in : in std_logic_vector(0 downto 0);
            gtwiz_buffbypass_rx_start_user_in : in std_logic_vector(0 downto 0);
            gtwiz_buffbypass_rx_done_out : out std_logic_vector(0 downto 0);
            gtwiz_buffbypass_rx_error_out : out std_logic_vector(0 downto 0);
            gtwiz_reset_clk_freerun_in : in std_logic_vector(0 downto 0);
            gtwiz_reset_all_in : in std_logic_vector(0 downto 0);
            gtwiz_reset_tx_pll_and_datapath_in : in std_logic_vector(0 downto 0);
            gtwiz_reset_tx_datapath_in : in std_logic_vector(0 downto 0);
            gtwiz_reset_rx_pll_and_datapath_in : in std_logic_vector(0 downto 0);
            gtwiz_reset_rx_datapath_in : in std_logic_vector(0 downto 0);
            gtwiz_reset_rx_cdr_stable_out : out std_logic_vector(0 downto 0);
            gtwiz_reset_tx_done_out : out std_logic_vector(0 downto 0);
            gtwiz_reset_rx_done_out : out std_logic_vector(0 downto 0);
            gtwiz_userdata_tx_in : in std_logic_vector(15 downto 0);
            gtwiz_userdata_rx_out : out std_logic_vector(31 downto 0);
            gtrefclk01_in : in std_logic_vector(0 downto 0);
            qpll1fbclklost_out : out std_logic_vector(0 downto 0);
            qpll1lock_out : out std_logic_vector(0 downto 0);
            qpll1outclk_out : out std_logic_vector(0 downto 0);
            qpll1outrefclk_out : out std_logic_vector(0 downto 0);
            qpll1refclklost_out : out std_logic_vector(0 downto 0);
            cplllockdetclk_in : in std_logic_vector(0 downto 0);
            cpllreset_in : in std_logic_vector(0 downto 0);
            drpclk_in : in std_logic_vector(0 downto 0);
            gthrxn_in : in std_logic_vector(0 downto 0);
            gthrxp_in : in std_logic_vector(0 downto 0);
            gtrefclk0_in : in std_logic_vector(0 downto 0);
            loopback_in : in std_logic_vector(2 downto 0);
            rx8b10ben_in : in std_logic_vector(0 downto 0);
            rxcommadeten_in : in std_logic_vector(0 downto 0);
            rxmcommaalignen_in : in std_logic_vector(0 downto 0);
            rxpcommaalignen_in : in std_logic_vector(0 downto 0);
            rxusrclk_in : in std_logic_vector(0 downto 0);
            rxusrclk2_in : in std_logic_vector(0 downto 0);
            tx8b10ben_in : in std_logic_vector(0 downto 0);
            txctrl0_in : in std_logic_vector(15 downto 0);
            txctrl1_in : in std_logic_vector(15 downto 0);
            txctrl2_in : in std_logic_vector(7 downto 0);
            txusrclk_in : in std_logic_vector(0 downto 0);
            txusrclk2_in : in std_logic_vector(0 downto 0);
            cpllfbclklost_out : out std_logic_vector(0 downto 0);
            cplllock_out : out std_logic_vector(0 downto 0);
            gthtxn_out : out std_logic_vector(0 downto 0);
            gthtxp_out : out std_logic_vector(0 downto 0);
            gtpowergood_out : out std_logic_vector(0 downto 0);
            rxbyteisaligned_out : out std_logic_vector(0 downto 0);
            rxbyterealign_out : out std_logic_vector(0 downto 0);
            rxcdrlock_out : out std_logic_vector(0 downto 0);
            rxcommadet_out : out std_logic_vector(0 downto 0);
            rxctrl0_out : out std_logic_vector(15 downto 0);
            rxctrl1_out : out std_logic_vector(15 downto 0);
            rxctrl2_out : out std_logic_vector(7 downto 0);
            rxctrl3_out : out std_logic_vector(7 downto 0);
            rxoutclk_out : out std_logic_vector(0 downto 0);
            rxpmaresetdone_out : out std_logic_vector(0 downto 0);
            rxresetdone_out : out std_logic_vector(0 downto 0);
            txoutclk_out : out std_logic_vector(0 downto 0);
            txpmaresetdone_out : out std_logic_vector(0 downto 0);
            txprgdivresetdone_out : out std_logic_vector(0 downto 0);
            txresetdone_out : out std_logic_vector(0 downto 0)
        );
    end component;

    signal    GTH_RefClk0       : std_logic; --SI5345/TX
    signal    GTH_RefClk1             : std_logic; --LMK/RX

    signal rxcommadet_out              : std_logic_vector(0 downto 0); -- @suppress "Signal rxcommadet_out is never read"
    signal rxcdrlock_out               : std_logic_vector(0 downto 0);
    signal rxbyteisaligned_out         : std_logic_vector(0 downto 0); -- @suppress "Signal rxbyteisaligned_out is never read"

    signal userclk_rx_active_in       : std_logic_vector(0 downto 0);
    signal userclk_rx_active_in_p     : std_logic_vector(0 downto 0);

    signal rxpmaresetdone_out          : std_logic_vector(0 downto 0);
    signal cplllock_out                : std_logic_vector(0 downto 0);
    signal cpllfbclklost_out           : std_logic_vector(0 downto 0);
    signal rxresetdone_out             : std_logic_vector(0 downto 0);

    signal qpll1lock_out               : std_logic_vector(0 downto 0);
    signal qpll1refclklost_out               : std_logic_vector(0 downto 0);

    signal reset_all                   : std_logic_vector(0 downto 0);

    signal reset_rx_pll_and_datapath   : std_logic_vector(0 downto 0);
    signal reset_rx_datapath           : std_logic_vector(0 downto 0);

    signal gt_drp_clk_in               : std_logic_vector(0 downto 0);

    signal gtrefclk0_in                : std_logic_vector(0 downto 0);
    signal gtrefclk1_in                : std_logic_vector(0 downto 0);

    signal RXUSRCLK                    : std_logic_vector(0 downto 0);
    signal RXOUTCLK                    : std_logic_vector(0 downto 0);

    signal gt_drp_clk_vec              : std_logic_vector(0 downto 0);


    signal TXUSRCLK                    : std_logic_vector(0 downto 0);
    signal TXOUTCLK                    : std_logic_vector(0 downto 0);
    signal userclk_tx_active_in        : std_logic_vector(0 downto 0) := "0";
    signal userclk_tx_active_in_p      : std_logic_vector(0 downto 0) := "0";
    signal reset_tx_pll_and_datapath   : std_logic_vector(0 downto 0);
    signal reset_tx_datapath           : std_logic_vector(0 downto 0);
    signal txpmaresetdone_out          : std_logic_vector(0 downto 0);
    --signal txpd_i                      : std_logic_vector(1 downto 0) := "11";
    signal rxbyterealign_i             : std_logic_vector(0 downto 0) := "0"; -- @suppress "Signal rxbyterealign_i is never read"

    --signal commaalignen_i : std_logic_vector(0 downto 0) := "1";
    signal cpllreset_inbuf             : std_logic_vector(0 downto 0) := "0";
    signal buffbypass_tx_reset_in : std_logic_vector(0 downto 0);
    signal buffbypass_rx_reset_in : std_logic_vector(0 downto 0);
    signal buffbypass_rx_reset_in_rxusrclk : std_logic_vector(0 downto 0);
    signal buffbypass_tx_reset_in_txusrclk : std_logic_vector(0 downto 0);
    signal rxdata: std_logic_vector(31 downto 0);
    signal rxctrl0 : std_logic_vector(15 downto 0);
    signal rxctrl1 : std_logic_vector(15 downto 0);
    signal rxctrl3 : std_logic_vector(7 downto 0);
begin

    cpllreset_inbuf(0) <= cpllreset_in;

    ibufds_gtrefclk0_inst: IBUFDS_GTE3
        generic map (
            REFCLK_EN_TX_PATH => '0',
            REFCLK_HROW_CK_SEL => "00",
            REFCLK_ICNTL_RX => "00"
        )
        port map (
            O     => GTH_RefClk0,
            ODIV2 => open,
            CEB   => '0',
            I     => GTREFCLK0_P_IN,
            IB    => GTREFCLK0_N_IN
        );

    ibufds_gtrefclk1_inst: IBUFDS_GTE3
        generic map (
            REFCLK_EN_TX_PATH => '0',
            REFCLK_HROW_CK_SEL => "00",
            REFCLK_ICNTL_RX => "00"
        )
        port map (
            O     => GTH_RefClk1,
            ODIV2 => open,
            CEB   => '0',
            I     => GTREFCLK1_P_IN,
            IB    => GTREFCLK1_N_IN
        );



    bufg_gt_rx_inst: BUFG_GT
        generic map(
            SIM_DEVICE => "ULTRASCALE",
            STARTUP_SYNC => "FALSE"
        )
        port map(
            O => RXUSRCLK(0),
            CE => '1',
            CEMASK => '0',
            CLR => '0',
            CLRMASK => '0',
            DIV => "000",
            I => RXOUTCLK(0)
        );
    rxusrclk_out <= RXUSRCLK(0);


    bufg_gt_tx_inst: BUFG_GT
        generic map(
            SIM_DEVICE => "ULTRASCALE",
            STARTUP_SYNC => "FALSE"
        )
        port map(
            O => TXUSRCLK(0),
            CE => '1',
            CEMASK => '0',
            CLR => '0',
            CLRMASK => '0',
            DIV => "000",
            I => TXOUTCLK(0)
        );

    txusrclk_out <= TXUSRCLK(0);

    --Generate a 40 MHz clock out of the 240 MHz RXUSRCLK
    BUFGCE_DIV_inst : BUFGCE_DIV
        generic map (
            BUFGCE_DIVIDE => 6,         -- 1-8
            -- Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
            IS_CE_INVERTED => '0',      -- Optional inversion for CE
            IS_CLR_INVERTED => '0',     -- Optional inversion for CLR
            IS_I_INVERTED => '0',       -- Optional inversion for I
            SIM_DEVICE => "ULTRASCALE"  -- ULTRASCALE
        )
        port map (
            O => rxusrclk_div6,     -- 1-bit output: Buffer
            CE => '1',
            CLR => rxusrclk_div6_clr,
            I => RXUSRCLK(0)
        );

    --------------------------------
    ---- Reset
    reset_all(0)                    <= reset_all_in;
    reset_tx_pll_and_datapath(0)    <= reset_tx_pll_and_datapath_in;
    reset_tx_datapath(0)            <= reset_tx_datapath_in;
    reset_rx_pll_and_datapath(0)    <= reset_rx_pll_and_datapath_in;
    reset_rx_datapath(0)            <= reset_rx_datapath_in;

    --------------------------------
    ---- Monitor
    gt_rxcdrlock_out                <= rxcdrlock_out(0);
    gt_rxbyteisaligned_out          <= rxcdrlock_out(0) and rxresetdone_out(0); --rxbyteisaligned_out(0);
    RX_DATA_gt_32b                  <= rxdata(31 downto 0);
    rx_disp_err_out                 <= rxctrl1(3 downto 0); --[i] disp err found in byte i. Only [0] should be relevant because of the MGT configuration
    rx_isk_out                      <= rxctrl0(3 downto 0); --[i] k character found in byte i. Only [0] should be relevant because of the MGT configuration
    --gt_rxcommadet_out               <= rxctrl2(3 downto 0); --[i] comma character found in byte i. Only [0] should be relevant because of the MGT configuration
    rx_code_err_out                 <= rxctrl3(3 downto 0); --[i] not valid character found in byte i

    gt_rxresetdone_out              <= rxresetdone_out(0);

    gt_cpllfbclklost_out            <= cpllfbclklost_out(0);
    gt_cplllock_out                 <= cplllock_out(0);
    gt_cpllrefclklost_out           <= '1'; --cpllrefclklost_out(0);

    gt_rxcdrlock_out                <= rxcdrlock_out(0);

    gt_qpll1refclklost_out          <= qpll1refclklost_out(0);
    gt_qpll1lock_out                <= qpll1lock_out(0);

    bufbypass_reset_proc: process(DRP_CLK_IN)
        variable cplllock_p1, qpll1lock_p1: std_logic_vector(0 downto 0);
    begin
        if rising_edge(DRP_CLK_IN) then
            buffbypass_tx_reset_in <= "0";
            if cplllock_out(0) = '1' and cplllock_p1(0) = '0' then --right after cpll lock (for txusrclk)
                buffbypass_tx_reset_in <= "1";
            end if;
            cplllock_p1 := cplllock_out;
            buffbypass_rx_reset_in <= "0";
            if qpll1lock_out(0) = '1' and qpll1lock_p1(0) = '0' then --right after qpll1 lock (for rxusrclk)
                buffbypass_rx_reset_in <= "1";
            end if;
            qpll1lock_p1 := qpll1lock_out;
        end if;
    end process;


    gt_drp_clk_in(0)    <= DRP_CLK_IN;
    gtrefclk0_in(0)     <= GTH_RefClk0;
    gtrefclk1_in(0)     <= GTH_RefClk1;

    gt_drp_clk_vec(0)   <= DRP_CLK_IN;


    --MT imported from gth_fullmode_wrapper_ku.vhd
    rx_active_proc: process(rxpmaresetdone_out, RXUSRCLK)
    begin
        if rxpmaresetdone_out = "0" then
            userclk_rx_active_in <= "0";
            userclk_rx_active_in_p <= "0";
        elsif rising_edge(RXUSRCLK(0)) then
            userclk_rx_active_in_p <= "1";
            userclk_rx_active_in <= userclk_rx_active_in_p;
        end if;
    end process;

    tx_active_proc: process(txpmaresetdone_out, TXUSRCLK)
    begin
        if txpmaresetdone_out = "0" then
            userclk_tx_active_in <= "0";
            userclk_tx_active_in_p <= "0";
        elsif rising_edge(TXUSRCLK(0)) then
            userclk_tx_active_in_p <= "1";
            userclk_tx_active_in <= userclk_tx_active_in_p;
        end if;
    end process;

    cdc_bufbypass_tx_reset: xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 1
        )
        port map(
            src_clk => DRP_CLK_IN,
            src_in => buffbypass_tx_reset_in(0),
            dest_clk => TXUSRCLK(0),
            dest_out => buffbypass_tx_reset_in_txusrclk(0)
        );

    cdc_bufbypass_rx_reset: xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 1
        )
        port map(
            src_clk => DRP_CLK_IN,
            src_in => buffbypass_rx_reset_in(0),
            dest_clk => RXUSRCLK(0),
            dest_out => buffbypass_rx_reset_in_rxusrclk(0)
        );


    gtwizard_ttc_rxcpll_inst: gtwizard_ttc_rxcpll
        PORT MAP (
            gtwiz_userclk_tx_active_in          => userclk_tx_active_in,
            gtwiz_userclk_rx_active_in          => userclk_rx_active_in,
            gtwiz_buffbypass_tx_reset_in        => buffbypass_tx_reset_in_txusrclk,
            gtwiz_buffbypass_tx_start_user_in   => "0",
            gtwiz_buffbypass_tx_done_out        => open,
            gtwiz_buffbypass_tx_error_out       => open,
            gtwiz_buffbypass_rx_reset_in        => buffbypass_rx_reset_in_rxusrclk,
            gtwiz_buffbypass_rx_start_user_in   => "0",
            gtwiz_buffbypass_rx_done_out        => open,
            gtwiz_buffbypass_rx_error_out       => open,
            gtwiz_reset_clk_freerun_in          => gt_drp_clk_in,
            gtwiz_reset_all_in                  => reset_all,
            gtwiz_reset_tx_pll_and_datapath_in  => reset_tx_pll_and_datapath,
            gtwiz_reset_tx_datapath_in          => reset_tx_datapath,
            gtwiz_reset_rx_pll_and_datapath_in  => reset_rx_pll_and_datapath,
            gtwiz_reset_rx_datapath_in          => reset_rx_datapath,
            gtwiz_reset_rx_cdr_stable_out       => open,
            gtwiz_reset_tx_done_out             => open,
            gtwiz_reset_rx_done_out             => open,
            gtwiz_userdata_tx_in                => TX_DATA_gt_16b,
            gtwiz_userdata_rx_out               => rxdata,
            gtrefclk01_in                       => gtrefclk1_in,
            qpll1fbclklost_out                  => open,
            qpll1lock_out                       => qpll1lock_out,
            qpll1outclk_out                     => open,
            qpll1outrefclk_out                  => open,
            qpll1refclklost_out                 => qpll1refclklost_out,
            cplllockdetclk_in                   => gt_drp_clk_vec,
            cpllreset_in                        => cpllreset_inbuf,
            drpclk_in                           => gt_drp_clk_in,
            gthrxn_in                           => RXN_IN,
            gthrxp_in                           => RXP_IN,
            gtrefclk0_in                        => gtrefclk0_in,
            loopback_in                         => "000",
            rx8b10ben_in                        => "1",
            rxcommadeten_in                     => "1",
            rxmcommaalignen_in                  => "1",
            rxpcommaalignen_in                  => "1",
            rxusrclk_in                         => RXUSRCLK,
            rxusrclk2_in                        => RXUSRCLK,
            tx8b10ben_in                        => "1",
            txctrl0_in                          => (others => '0'),
            txctrl1_in                          => (others => '0'),
            txctrl2_in                          => "000000" & TX_isK_gt,
            txusrclk_in                         => TXUSRCLK,
            txusrclk2_in                        => TXUSRCLK,
            cpllfbclklost_out                   => cpllfbclklost_out,
            cplllock_out                        => cplllock_out,
            gthtxn_out                          => TXN_OUT,
            gthtxp_out                          => TXP_OUT,
            gtpowergood_out                     => open,
            rxbyteisaligned_out                 => rxbyteisaligned_out,
            rxbyterealign_out                   => rxbyterealign_i,
            rxcdrlock_out                       => rxcdrlock_out,
            rxcommadet_out                      => rxcommadet_out,
            rxctrl0_out                         => rxctrl0,
            rxctrl1_out                         => rxctrl1,
            rxctrl2_out                         => open,
            rxctrl3_out                         => rxctrl3,
            rxoutclk_out                        => RXOUTCLK,
            rxpmaresetdone_out                  => rxpmaresetdone_out,
            rxresetdone_out                     => rxresetdone_out,
            txoutclk_out                        => TXOUTCLK,
            txpmaresetdone_out                  => open,
            txprgdivresetdone_out               => txpmaresetdone_out,
            txresetdone_out                     => open
        );

end RTL;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
--use ieee.numeric_std_unsigned.all;
library xpm;
    use xpm.vcomponents.all;


entity LTI_FE_Sync is
    generic (
        width: integer := 33
    );
    port(
        clka: in std_logic;
        clkb: in std_logic;
        din: in std_logic_vector(width-1 downto 0);
        dout: out std_logic_vector(width-1 downto 0);
        training: in std_logic
    );
end entity LTI_FE_Sync;

architecture rtl of LTI_FE_Sync is

    signal din_3x, din_3x_async, dout_3x: std_logic_vector(3*width downto 0); --3x width +1 for the sync pulse (MSB).
    signal din_ptr, dout_ptr: std_logic_vector(1 downto 0) := "00";
    signal sync_pulse_p1 : std_logic;
    attribute DONT_TOUCH: string;
    attribute DONT_TOUCH of din_3x, din_3x_async : signal is "TRUE";
    attribute ASYNC_REG : string;
    attribute ASYNC_REG of dout_3x : signal is "TRUE";

begin

    din_ptr_proc: process(clka)
    begin
        if rising_edge(clka) then
            case din_ptr is
                when "00" =>
                    din_3x(width-1 downto 0) <= din;
                    din_3x(3*width) <= '1'; --synchronization pulse
                    din_ptr <= "01";
                when "01" =>
                    din_3x(2*width-1 downto width) <= din;
                    din_3x(3*width) <= '0';
                    din_ptr <= "11";
                when "11" =>
                    din_3x(3*width-1 downto 2*width) <= din;
                    din_3x(3*width) <= '0';
                    din_ptr <= "00";
                when others =>
                    din_ptr <= "00";
            end case;
        end if;
    end process;

    async_reg_proc: process(clka)
    begin
        if rising_edge(clka) then
            din_3x_async <= din_3x;
        end if;
    end process;


    sync_proc: process(clkb)
    begin
        if rising_edge(clkb) then
            dout_3x <= din_3x_async;
        end if;
    end process;


    --xpm_cdc_array_single_inst : xpm_cdc_array_single
    --    generic map (
    --        DEST_SYNC_FF => 2,
    --        INIT_SYNC_FF => 0,
    --        SIM_ASSERT_CHK => 0,
    --        SRC_INPUT_REG => 1,
    --        WIDTH => width*3+1
    --    )
    --    port map (
    --        dest_out => dout_3x,
    --        dest_clk => clkb,
    --        src_clk => clka,
    --        src_in => din_3x
    --    );

    dout_ptr_proc: process(clkb)
    begin
        if rising_edge(clkb) then
            case dout_ptr is
                when "00" =>
                    dout <= dout_3x(width-1 downto 0);
                    dout_ptr <= "01";
                when "01" =>
                    dout <= dout_3x(width*2-1 downto width);
                    dout_ptr <= "11";
                when "11" =>
                    dout <= dout_3x(width*3-1 downto width*2);
                    dout_ptr <= "00";
                when others =>
                    dout <= (others => '0');
                    dout_ptr <= "00";
            end case;
            if dout_3x(width*3) = '1' and sync_pulse_p1 = '0' and training = '1' then --Make sure we get only one clock cycle of sync pulse
                dout_ptr <= "00"; --One clock cycle later than the change
            end if;
            sync_pulse_p1 <= dout_3x(width*3);
        end if;
    end process;

end architecture rtl;

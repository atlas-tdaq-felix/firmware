--moved lti_wrapper.vhd register control and read here

library ieee;
    use ieee.std_logic_1164.all;
    use IEEE.NUMERIC_STD.ALL;
    use ieee.numeric_std_unsigned.all;

library UNISIM;
    use UNISIM.VComponents.all;
library xpm;
    use xpm.vcomponents.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;

entity ltittc_routing is
    generic(
        ITK_TRIGTAG_MODE            : integer := -1    -- -1: not implemented, 0: RD53A, 1: ITkPixV1, 2: ABC*
    );
    port
    (
        clk40_ttc                           : in std_logic;
        clk40                               : in std_logic;
        reset                               : in std_logic;

        register_map_control                : in register_map_control_type;     --synchronized to clk40

        decoder_data_in                     : in std_logic_vector(191 downto 0);
        decoder_aligned_in                  : in std_logic;
        decoder_crc_valid_in                : in std_logic;

        TTC_out                             : out TTC_data_type;
        TTC_EMU_out                         : out TTC_data_type;
        BUSY_IN                             : in std_logic;
        TTC_ToHost_Data_out                 : out TTC_data_type;
        TTC_ToHost_Data_emu_out             : out TTC_data_type;
        LTITTC_TTYPE_MONITOR_VALUE      : out std_logic_vector(31 downto 0);
        LTITTC_SL0ID_MONITOR_VALUE      : out std_logic_vector(31 downto 0);
        LTITTC_SORB_MONITOR_VALUE       : out std_logic_vector(31 downto 0);
        LTITTC_GRST_MONITOR_VALUE       : out std_logic_vector(31 downto 0);
        LTITTC_SYNC_MONITOR_VALUE       : out std_logic_vector(31 downto 0);
        LTITTC_BCR_ERR_MONITOR_VALUE    : out std_logic_vector(31 downto 0);
        LTITTC_L0ID_ERR_MONITOR_VALUE   : out std_logic_vector(31 downto 0);
        LTITTC_CRC_ERR_MONITOR_VALUE    : out std_logic_vector(31 downto 0)

    );

end ltittc_routing;

architecture behavioral of ltittc_routing is

    signal decoder_data             : std_logic_vector(191 downto 0);
    signal decoder_aligned          : std_logic;
    signal decoder_crc_valid        : std_logic;


    signal TTC_out_i                : TTC_data_type := TTC_zero;
    signal TTC_EMU_Out_i            : ttc_data_type;

    signal typemsg                  : std_logic := '0';
    signal partn                    : std_logic_vector( 2 downto 0) := (others=>'0');
    signal bcid                     : std_logic_vector(11 downto 0) := (others=>'0');
    signal syncusrdata              : std_logic_vector(15 downto 0) := (others => '0');
    signal l0a                      : std_logic := '0';
    signal sl0id                    : std_logic := '0';
    signal sorb                     : std_logic := '0';
    signal sync                     : std_logic := '0';
    signal grst                     : std_logic := '0';
    signal l0id                     : std_logic_vector(37 downto 0) := (others=>'0');
    signal orbitid                  : std_logic_vector(31 downto 0) := (others=>'0');
    signal ttype                    : std_logic_vector(15 downto 0) := (others => '0');
    signal lbid                     : std_logic_vector(15 downto 0) := (others => '0');
    signal asyncusrdata             : std_logic_vector(63 downto 0) := (others => '0');
    signal syncglobaldata           : std_logic_vector(15 downto 0) := (others => '0');
    signal turnsignal               : std_logic := '0';
    signal errorflags               : std_logic_vector( 3 downto 0) := (others => '0');
    signal crc                      : std_logic_vector(15 downto 0) := (others => '0');
    signal d16_2                    : std_logic_vector( 7 downto 0) := (others => '0');

    signal l0id_d                   : std_logic_vector(37 downto 0) := (others => '0');
    signal orbitid_d                : std_logic_vector(31 downto 0) := (others => '0');
    signal ttype_d                  : std_logic_vector(15 downto 0) := (others => '0');
    signal lbid_d                   : std_logic_vector(15 downto 0) := (others => '0');
    signal asyncusrdata_d           : std_logic_vector(63 downto 0) := (others => '0');

    signal ExtendedTestPulse        : std_logic := '0';
    signal itk_sync                 : std_logic;
    signal itk_trig                 : std_logic_vector(3 downto 0);
    signal itk_tag                  : std_logic_vector(6 downto 0);
    signal itk_ttc2h_tag            : std_logic_vector(7 downto 0);
    signal training : std_logic;
    signal emu_ttc2h_tag : std_logic_vector(7 downto 0);
    signal emu_ttc2h_tag_valid : std_logic;

begin
    cdc_training: xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk  => '0',
            src_in   => register_map_control.LTI_SYNC_TRAINING.DECODER(24),
            dest_clk => clk40,
            dest_out => training
        );

    cdc_decoder_data: entity work.LTI_FE_Sync generic map(
            width => 192
        )
        port map(
            clka => clk40_ttc,
            clkb => clk40,
            din => decoder_data_in,
            dout => decoder_data,
            training => training
        );

    cdc_aligned: xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => '0',
            src_in => decoder_aligned_in,
            dest_clk => clk40,
            dest_out => decoder_aligned
        );

    cdc_crc: xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => '0',
            src_in => decoder_crc_valid_in,
            dest_clk => clk40,
            dest_out => decoder_crc_valid
        );


    --As per Table 2.3 of https://edms.cern.ch/document/2379978/1 (v1.1)
    typemsg           <= decoder_data(31+0*32);
    partn             <= decoder_data(30+0*32 downto 28+0*32);
    bcid              <= decoder_data(27+0*32 downto 16+0*32);
    syncusrdata       <= decoder_data(15+0*32 downto  0+0*32);
    syncglobaldata    <= decoder_data(31+1*32 downto 16+1*32);
    turnsignal        <= decoder_data(15+1*32);
    errorflags        <= decoder_data(14+1*32 downto 11+1*32);
    sl0id             <= decoder_data(10+1*32)                when typemsg = '0' else '0';
    sorb              <= decoder_data( 9+1*32)                when typemsg = '0' else '0';
    sync              <= decoder_data( 8+1*32)                when typemsg = '0' else '0';
    grst              <= decoder_data( 7+1*32)                when typemsg = '0' else '0';
    l0a               <= decoder_data( 6+1*32)                when typemsg = '0' else '0';
    l0id              <= decoder_data(31+2*32 downto  0+2*32) &
                         decoder_data( 5+1*32 downto  0+1*32) when typemsg = '0' else l0id_d;
    orbitid           <= decoder_data(31+3*32 downto  0+3*32) when typemsg = '0' else orbitid_d;
    ttype             <= decoder_data(31+4*32 downto 16+4*32) when typemsg = '0' else ttype_d;
    lbid              <= decoder_data(15+4*32 downto  0+4*32) when typemsg = '0' else lbid_d;
    asyncusrdata      <= decoder_data(31+4*32 downto  0+3*32) when typemsg = '1' else asyncusrdata_d;
    crc               <= decoder_data(31+5*32 downto 16+5*32); --CRC is checked in ltittc_decoder
    d16_2             <= decoder_data(15+5*32 downto  8+5*32);

    delay_process: process(clk40)
    begin
        if rising_edge(clk40) then
            l0id_d          <= l0id;
            orbitid_d       <= orbitid;
            ttype_d         <= ttype;
            lbid_d          <= lbid;
            asyncusrdata_d  <= asyncusrdata;
        end if;
    end process;

    TTC_out                 <= TTC_out_i;
    --routing the whole TTC message minus the comma (192b-8b) rather than 136b mentioned in Table 2.2 of https://edms.cern.ch/ui/file/1563801/1/RequirementsPhaseII_v1.1.0.pdf
    --TTC_out_i.MT                    <= typemsg;
    TTC_out_i.PT                    <= partn(2);
    TTC_out_i.Partition             <= partn(1 downto 0);
    TTC_out_i.BCID                  <= bcid;
    TTC_out_i.SyncUserData          <= syncusrdata;
    TTC_out_i.SyncGlobalData        <= syncglobaldata;
    TTC_out_i.TS                    <= turnsignal;
    TTC_out_i.ErrorFlags            <= errorflags;
    TTC_out_i.SL0ID                 <= sl0id;
    TTC_out_i.SOrb                  <= sorb;
    TTC_out_i.Sync                  <= sync;
    TTC_out_i.GRst                  <= grst;
    TTC_out_i.L0A                   <= l0a;
    TTC_out_i.L0ID                  <= l0id;
    TTC_out_i.OrbitID               <= orbitid;
    TTC_out_i.TriggerType           <= ttype;
    TTC_out_i.LBID                  <= lbid;
    --! FS: No need to distribute LTI trailer info through the firmware
    TTC_out_i.CRC                   <= crc;
    TTC_out_i.D16_2                 <= d16_2;
    TTC_out_i.LTI_CRC_valid         <= decoder_crc_valid;
    TTC_out_i.LTI_decoder_aligned   <= decoder_aligned;
    TTC_out_i.AsyncUserData         <= asyncusrdata;

    --Phase 1 signals for backward compatibility:
    TTC_out_i.L1A                   <= l0a;
    TTC_out_i.Bchan                 <= '0';
    TTC_out_i.BCR                   <= turnsignal;
    TTC_out_i.Brcst                 <= syncusrdata(5 downto 0);
    TTC_out_i.Brcst_latched         <= syncusrdata(5 downto 0);
    TTC_out_i.L1Id                  <= l0id(23 downto 0);

    TTC_out_i.ExtendedTestPulse     <= ExtendedTestPulse;

    TTC_out_i.ITk_sync              <= itk_sync;
    TTC_out_i.ITk_tag               <= '0'&itk_tag;
    TTC_out_i.ITk_trig              <= itk_trig;

    --gen extended test pulse
    ExtendingTestPulse: entity work.ExtendedTestPulse (Behavioral)
        port map(
            clk40                => clk40,
            reset                => reset,
            TTCin_TestPulse      => syncusrdata(0),
            ExtendedTestPulse    => ExtendedTestPulse
        );

    --gen itk trigger_tags
    tag_gen_enable: if ITK_TRIGTAG_MODE >= 0 generate
        signal emu_l0a_occurred, l0a_occurred, ttc2h_tag_valid: std_logic;
        signal TTC_Out_i_stored, TTC_EMU_Out_i_stored: TTC_Data_type;
    begin
        tag_gen: entity work.itk_trigtag_generator
            generic map (
                MODE => ITK_TRIGTAG_MODE
            )
            port map (
                clk             => clk40,
                rst             => reset,
                enc_trig        => itk_trig,
                enc_sync        => itk_sync,
                enc_tag         => itk_tag,
                ttc2h_tag       => itk_ttc2h_tag, -- TODO: itk_ttc2h_tag,
                ttc2h_tag_valid => ttc2h_tag_valid,
                ttc_l0a         => l0a,--,
                ttc_bcr         => turnsignal
            --ttc_bcr         => turnsignal,
            --ttc_phase       => "00" -- TODO: connect to something meaningful in the regbank
            );
        --itk_ttc2h_tag <= (others => '0');
        itk_data_rdy: process(clk40)
        begin
            if rising_edge(clk40) then
                if TTC_Out_i.L0A = '1' then
                    l0a_occurred <= '1';
                    TTC_Out_i_stored <= TTC_Out_i;
                end if;
                if TTC_EMU_Out_i.L0A = '1' then
                    emu_l0a_occurred <= '1';
                    TTC_EMU_Out_i_stored <= TTC_EMU_Out_i;
                end if;
                if TTC_Out_i.ITk_Sync = '1' then
                    l0a_occurred <= '0';
                end if;
                if TTC_EMU_Out_i.ITk_Sync = '1' then
                    emu_l0a_occurred <= '0';
                end if;
            end if;
        end process;


        --First itk_sync after itk_sync validates ttc tohost data.
        sync_ttc_tohost_proc: process(l0a_occurred, TTC_Out_i_stored, ttc2h_tag_valid, TTC_out_i.ITk_trig, itk_ttc2h_tag)
        begin
            --default:
            TTC_ToHost_Data_out <= TTC_Out_i_stored;
            --TTC2HOST

            if l0a_occurred = '1' and ttc2h_tag_valid = '1' then
                TTC_ToHost_Data_out.itk_tag          <= itk_ttc2h_tag;
                TTC_ToHost_Data_out.itk_trig          <= TTC_Out_i.itk_trig;
                TTC_ToHost_Data_out.L0A     <= '1';
            else
                TTC_ToHost_Data_out.L0A     <= '0';
            end if;
        end process;

        sync_ttc_emu_tohost_proc: process(emu_l0a_occurred, TTC_emu_Out_i_stored, TTC_emu_Out_i, emu_ttc2h_tag, emu_ttc2h_tag_valid)
        begin
            --default:
            TTC_ToHost_Data_emu_out <= TTC_emu_Out_i_stored;
            --TTC2HOST

            if emu_l0a_occurred = '1' and emu_ttc2h_tag_valid = '1' then
                TTC_ToHost_Data_emu_out.itk_tag      <= emu_ttc2h_tag;
                TTC_ToHost_Data_emu_out.itk_trig      <= TTC_emu_Out_i.Itk_trig;
                TTC_ToHost_Data_emu_out.L0A     <= '1';
            else
                TTC_ToHost_Data_emu_out.L0A     <= '0';
            end if;
        end process;

    else generate
        itk_trig <= "0000";
        itk_sync <= '0';
        itk_tag <= (others => '0');
        --itk_ttc2h_tag <= (others => '0');

        --TTC2HOST
        TTC_ToHost_Data_out <= TTC_Out_i;
        TTC_ToHost_Data_emu_out <= TTC_EMU_Out_i;

    end generate;




    TTC_EMU_out <= TTC_EMU_Out_i;

    ltittc_mon_counters: entity work.ltittc_mon_counters
        port map(
            clk40                           => clk40,
            register_map_control            => register_map_control,
            l0id                            => l0id,
            sl0id                           => sl0id,
            l0a                             => l0a,
            typemsg                         => typemsg,
            ttype                           => ttype,
            bcid                            => bcid,
            sorb                            => sorb,
            sync                            => sync,
            grst                            => grst,
            crc_valid                       => decoder_crc_valid,
            LTITTC_TTYPE_MONITOR_VALUE => LTITTC_TTYPE_MONITOR_VALUE,
            LTITTC_SL0ID_MONITOR_VALUE => LTITTC_SL0ID_MONITOR_VALUE,
            LTITTC_SORB_MONITOR_VALUE => LTITTC_SORB_MONITOR_VALUE,
            LTITTC_GRST_MONITOR_VALUE => LTITTC_GRST_MONITOR_VALUE,
            LTITTC_SYNC_MONITOR_VALUE => LTITTC_SYNC_MONITOR_VALUE,
            LTITTC_BCR_ERR_MONITOR_VALUE => LTITTC_BCR_ERR_MONITOR_VALUE,
            LTITTC_L0ID_ERR_MONITOR_VALUE => LTITTC_L0ID_ERR_MONITOR_VALUE,
            LTITTC_CRC_ERR_MONITOR_VALUE => LTITTC_CRC_ERR_MONITOR_VALUE
        );


    ttcemu0: entity work.LTITTC_Emulator generic map(
            ITK_TRIGTAG_MODE => ITK_TRIGTAG_MODE
        )
        port map(
            Clock => clk40,
            Reset => reset,
            register_map_control => register_map_control,

            TTCout => TTC_EMU_Out_i,
            ttc2h_tag => emu_ttc2h_tag,
            ttc2h_tag_valid => emu_ttc2h_tag_valid,
            BUSYIn => BUSY_IN
        );


end behavioral;

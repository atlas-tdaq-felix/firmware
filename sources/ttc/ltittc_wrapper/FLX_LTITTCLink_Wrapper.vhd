----------------------------------------------------------------------------------
--! Company:  Argonne National Laboratory
--! Engineer: Marco Trovato (mtrovato@anl.gov)
----------------------------------------------------------------------------------
LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
library XPM;
    use xpm.vcomponents.all;

library UNISIM;
    use UNISIM.VComponents.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;


entity FLX_LTITTCLink_Wrapper is
    Generic (
        CARD_TYPE          : integer
    );
    Port (

        rst_hw                          : in std_logic;
        register_map_control            : in   register_map_control_type;
        --To register_map_ltittc_monitor
        LTITTC_RXRESET_DONE             : out std_logic_vector(0 downto 0);
        LTITTC_CPLL_FBCLK_LOST          : out std_logic_vector(0 downto 0);
        LTITTC_PLL_LOCK_CPLL_LOCK       : out std_logic_vector(0 downto 0);
        LTITTC_RXCDR_LOCK               : out std_logic_vector(0 downto 0);
        LTITTC_ALIGNMENT_DONE           : out std_logic_vector(0 downto 0);
        LTITTC_PLL_LOCK_QPLL_LOCK       : out std_logic_vector(0 downto 0);
        LTITTC_RX_BYTEISALIGNED         : out std_logic_vector(0 downto 0);
        LTITTC_RX_DISP_ERROR            : out std_logic_vector(3 downto 0);
        LTITTC_RX_NOTINTABLE            : out std_logic_vector(3 downto 0);

        TXN_OUT                         : out   std_logic_vector(0 downto 0);
        TXP_OUT                         : out   std_logic_vector(0 downto 0);
        RXN_IN                          : in   std_logic_vector(0 downto 0);
        RXP_IN                          : in   std_logic_vector(0 downto 0);
        --clk40_in                        : in std_logic;
        LMK_locked_in                   : in std_logic;
        DRP_CLK_IN                      : in std_logic;
        --from SI5345 to TX REFCLK. Can be either clk40_xtal or clk40_ttc
        GTREFCLK0_P_IN                  : in std_logic;
        GTREFCLK0_N_IN                  : in std_logic;
        --from LMK to RX REFCLK. Always clk40_xtal to ensure clock recovery which genetrates clk40_ttc
        GTREFCLK1_P_IN                  : in std_logic;
        GTREFCLK1_N_IN                  : in std_logic;

        --gtrefclk1 shared with Bank 201 of FLX182 (M15/M14)
        FLX182_LTI_GTREFCLK1_in         : in std_logic;

        RXUSERCLK_OUT                   : out std_logic;
        RX_DATA_32b_out                 : out std_logic_vector(31 downto 0);
        RX_DATA_32b_rdy_out             : out std_logic;
        RX_IsK_OUT                      : out std_logic_vector(3 downto 0);
        rx_disp_err_out                 : out std_logic_vector(3 downto 0);
        rx_code_err_out                 : out std_logic_vector(3 downto 0);

        TXUSERCLK_OUT                   : out std_logic;
        TX_DATA_16b_in                  : in  std_logic_vector(15 downto 0);
        TX_IsK_in                       : in  std_logic_vector( 1 downto 0);
        rxusrclk_div6_clr               : in STD_LOGIC;
        rxusrclk_div6                   : out STD_LOGIC;
        axi_miso_ttc_lti : out axi_miso_type;
        axi_mosi_ttc_lti : in axi_mosi_type;
        LTI2cips_gpio  : out STD_LOGIC_VECTOR ( 3 downto 0 );
        cips2LTI_gpio : in STD_LOGIC_VECTOR ( 3 downto 0 )
    );
end FLX_LTITTCLink_Wrapper;


architecture Behavioral of FLX_LTITTCLink_Wrapper is

    signal cpllfbclklost                : std_logic;
    signal cplllock                     : std_logic;
    signal qpll1lock                    : std_logic;
    signal rxresetdone                  : std_logic;
    signal RXUSRCLK                     : std_logic;
    signal TXUSRCLK                     : std_logic;
    signal RxCdrLock                    : std_logic;

    signal RX_DATA_32b_s                : std_logic_vector(31 downto 0);
    signal RXByteisAligned              : std_logic_vector(0 downto 0);

    signal soft_reset                   : std_logic;
    signal cpll_reset                   : std_logic;

    signal SOFT_RXRST_ALL               : std_logic;
    signal SOFT_TXRST_ALL               : std_logic;


    --@40 MHz
    signal RXByteisAligned_clk40        : std_logic_vector(0 downto 0);
    signal RxCdrLock_clk40              : std_logic;
    signal rxresetdone_clk40            : std_logic_vector(0 downto 0);
    signal cpllfbclklost_clk40          : std_logic_vector(0 downto 0);
    signal cplllock_clk40               : std_logic_vector(0 downto 0);
    signal qpll1lock_clk40              : std_logic_vector(0 downto 0);
    signal Rxdisperr_clk40              : std_logic_vector(3 downto 0);
    signal Rxnotintable_clk40           : std_logic_vector(3 downto 0);
    signal rx_disp_err : std_logic_vector(3 downto 0);
    signal rx_code_err : std_logic_vector(3 downto 0);
    signal lmk_reset_pulse : std_logic;

begin
    rx_disp_err_out <= rx_disp_err;
    rx_code_err_out <= rx_code_err;


    --GT_TX_WORD_CLK <= GT_TXUSRCLK;

    --clocks used in logic
    RXUSERCLK_OUT   <= RXUSRCLK;
    TXUSERCLK_OUT   <= TXUSRCLK;

    --LTI DATA OUT
    RX_DATA_32b_OUT     <= RX_DATA_32b_s;
    RX_DATA_32b_rdy_OUT <= RXByteisAligned(0);
    RX_IsK_OUT          <= rx_isk_out;

    xpm_cdc_rxbyteisaligned : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 4,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map (
            src_clk => RXUSRCLK,
            src_in => RXByteisAligned(0),
            dest_clk => DRP_CLK_IN,
            dest_out => RXByteisAligned_clk40(0)
        );

    xpm_cdc_rxcdrlock : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 4,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map (
            src_clk => RXUSRCLK,
            src_in => RxCdrLock,
            dest_clk => DRP_CLK_IN,
            dest_out => RxCdrLock_clk40
        );

    xpm_cdc_rxresetdone : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 4,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map (
            src_clk => RXUSRCLK,
            src_in => rxresetdone,
            dest_clk => DRP_CLK_IN,
            dest_out => rxresetdone_clk40(0)
        );

    xpm_cdc_cpllfbclklost : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 4,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map (
            src_clk => RXUSRCLK,
            src_in => cpllfbclklost,
            dest_clk => DRP_CLK_IN,
            dest_out => cpllfbclklost_clk40(0)
        );

    xpm_cdc_cplllock : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 4,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map (
            src_clk => RXUSRCLK,
            src_in => cplllock,
            dest_clk => DRP_CLK_IN,
            dest_out => cplllock_clk40(0)
        );

    xpm_cdc_qpll1lock : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 4,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map (
            src_clk => RXUSRCLK,
            src_in => qpll1lock,
            dest_clk => DRP_CLK_IN,
            dest_out => qpll1lock_clk40(0)
        );

    loop_rxdisperr: for i in 0 to 3 generate
        xpm_cdc_Rxdisperr : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 4,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => RXUSRCLK,
                src_in => rx_disp_err(i),
                dest_clk => DRP_CLK_IN,
                dest_out => Rxdisperr_clk40(i)
            );
    end generate;

    loop_rxnotintable: for i in 0 to 3 generate
        xpm_cdc_Rxnotintable : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 4,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => RXUSRCLK,
                src_in => rx_code_err(i),
                dest_clk => DRP_CLK_IN,
                dest_out => Rxnotintable_clk40(i)
            );
    end generate;

    lmk_reset_pulse_proc: process(DRP_CLK_IN)
        variable LMK_locked_in_p1: std_logic;
    begin
        if rising_edge(DRP_CLK_IN) then
            lmk_reset_pulse <= '0';
            if LMK_locked_in = '1' and LMK_locked_in_p1 = '0' then
                lmk_reset_pulse <= '1';

            end if;
            LMK_locked_in_p1 := LMK_locked_in;
        end if;
    end process;

    soft_reset                      <= to_sl(register_map_control.LTITTC_CTRL.LTITTC_SOFT_RESET) or rst_hw or lmk_reset_pulse;
    cpll_reset                      <= to_sl(register_map_control.LTITTC_CTRL.LTITTC_CPLL_RESET);
    SOFT_RXRST_ALL                  <= to_sl(register_map_control.LTITTC_CTRL.LTITTC_SOFT_RX_RESET) or rst_hw;
    SOFT_TXRST_ALL                  <= to_sl(register_map_control.LTITTC_CTRL.LTITTC_SOFT_TX_RESET) or rst_hw;

    --Connecting to register_map_ltittc_monitor in ltittc_wrapper
    LTITTC_RXRESET_DONE        <= rxresetdone_clk40;
    LTITTC_CPLL_FBCLK_LOST     <= cpllfbclklost_clk40;
    LTITTC_PLL_LOCK_CPLL_LOCK  <= cplllock_clk40;
    LTITTC_RXCDR_LOCK(0)       <= RxCdrLock_clk40;
    LTITTC_ALIGNMENT_DONE(0)   <= RXByteisAligned_clk40(0) and RxCdrLock_clk40;
    LTITTC_PLL_LOCK_QPLL_LOCK  <= qpll1lock_clk40;
    LTITTC_RX_BYTEISALIGNED    <= RXByteisAligned_clk40;
    LTITTC_RX_DISP_ERROR       <= Rxdisperr_clk40;
    LTITTC_RX_NOTINTABLE       <= Rxnotintable_clk40;

    --

    ---------------------------------
    --1channel only
    -- GTH TX using QPLL, GREFCLK0 from SI5345 fed by clk40_xtal or clkttc_40
    -- GTH RX using CPLL, GREFCLK1 from LMK fed by clk40_xtal
    ---------------------------------

    g_TTC_712: if CARD_TYPE = 711 or CARD_TYPE = 712 generate
        GTH_LTITTCLINK_TOP_INST: entity work.gth_ltittclink_wrapper_ku
            Port map  (
                ---------- Clocks
                --SI5345/TX
                GTREFCLK0_P_IN => GTREFCLK0_P_IN,
                GTREFCLK0_N_IN => GTREFCLK0_N_IN,
                --LMK/RX
                GTREFCLK1_P_IN => GTREFCLK1_P_IN,
                GTREFCLK1_N_IN => GTREFCLK1_N_IN,
                DRP_CLK_IN => DRP_CLK_IN,

                rxusrclk_out => RXUSRCLK,
                txusrclk_out => TXUSRCLK,
                rxusrclk_div6 => rxusrclk_div6,
                rxusrclk_div6_clr => rxusrclk_div6_clr,
                -----------------------------------------
                ---- Control signals
                -----------------------------------------
                reset_all_in                 => soft_reset,
                cpllreset_in                 => cpll_reset,
                reset_tx_pll_and_datapath_in => SOFT_TXRST_ALL,
                reset_tx_datapath_in         => SOFT_TXRST_ALL,
                reset_rx_pll_and_datapath_in => SOFT_RXRST_ALL,
                reset_rx_datapath_in         => SOFT_RXRST_ALL,

                -----------------------------------------
                ---- STATUS signals
                -----------------------------------------
                gt_rxbyteisaligned_out       => RXByteisAligned(0),

                gt_rxresetdone_out           => rxresetdone,

                gt_cpllfbclklost_out         => cpllfbclklost,
                gt_cplllock_out              => cplllock,
                gt_cpllrefclklost_out        => open,
                gt_rxcdrlock_out             => RxCdrLock,

                gt_qpll1refclklost_out       => open,
                gt_qpll1lock_out             => qpll1lock,

                ---------- DATA
                RX_DATA_gt_32b => RX_DATA_32b_s,
                rx_isk_out => rx_isk_out,
                rx_disp_err_out => rx_disp_err,
                rx_code_err_out => rx_code_err,
                TX_DATA_gt_16b => TX_DATA_16b_IN,
                TX_isK_gt      => TX_IsK_IN,

                TXN_OUT => TXN_OUT,
                TXP_OUT => TXP_OUT,

                --------- GTH Data pins
                RXN_IN => RXN_IN,
                RXP_IN => RXP_IN

            );
    end generate;

    g_TTC_182: if CARD_TYPE = 181 or CARD_TYPE = 182 or CARD_TYPE = 155 generate
        GTY_LTITTCLINK_TOP_INST: entity work.gty_ltittclink_wrapper_versal
            Port map  (
                ---------- Clocks
                --On board oscillator
                GTREFCLK0_P_IN => GTREFCLK0_P_IN,
                GTREFCLK0_N_IN => GTREFCLK0_N_IN,
                --Shared with GTY Bank 201, refclk1 (M15/M14)
                GT_REFCLK1_in => FLX182_LTI_GTREFCLK1_in,
                rxusrclk_div6_clr => rxusrclk_div6_clr,
                rxusrclk_div6 => rxusrclk_div6,
                --LMK/RX
                --GTREFCLK1_P_IN => GTREFCLK1_P_IN,
                --GTREFCLK1_N_IN => GTREFCLK1_N_IN,
                APB3_CLK_IN => DRP_CLK_IN,

                gt_rxusrclk_out => RXUSRCLK,
                --gt_txusrclk_in => GT_TX_WORD_CLK,
                gt_txusrclk_out => TXUSRCLK,

                -----------------------------------------
                ---- Control signals
                -----------------------------------------
                --            loopback_in                  => register_map_control.LTITTC_CTRL.LTITTC_GTH_LOOPBACK_CONTROL,
                reset_all_in                 => soft_reset,
                --            cpllreset_in                 => cpll_reset,
                reset_tx_pll_and_datapath_in => cpll_reset,
                reset_tx_datapath_in         => SOFT_TXRST_ALL,
                reset_rx_pll_and_datapath_in => cpll_reset,
                reset_rx_datapath_in         => SOFT_RXRST_ALL,

                -----------------------------------------
                ---- STATUS signals
                -----------------------------------------
                gt_rxbyteisaligned_out       => open,

                gt_rxresetdone_out           => rxresetdone,

                --gt_cpllfbclklost_out         => cpllfbclklost,
                gt_cplllock_out              => cplllock,
                gt_cpllrefclklost_out        => open,
                --gt_rxcdrlock_out             => RxCdrLock,

                --gt_qpll1refclklost_out       => qpll1refclklost,
                gt_qpll1lock_out             => qpll1lock,

                ---------- DATA
                RX_DATA_gt_32b => RX_DATA_32b_s,
                rx_isk_out => rx_isk_out,
                rx_disp_err_out => rx_disp_err,
                rx_code_err_out => rx_code_err,
                TX_DATA_gt_16b => TX_DATA_16b_in,
                TX_isK_gt      => TX_ISK_in,

                TXN_OUT => TXN_OUT,
                TXP_OUT => TXP_OUT,

                --------- GTH Data pins
                RXN_IN => RXN_IN,
                RXP_IN => RXP_IN,
                axi_miso_ttc_lti => axi_miso_ttc_lti,
                axi_mosi_ttc_lti => axi_mosi_ttc_lti,
                LTI2cips_gpio  => LTI2cips_gpio,
                cips2LTI_gpio => cips2LTI_gpio
            );
        RxCdrLock <= rxresetdone; --rxcdrlock from Versal GTY seems disfunctional, replacing with rxresetdone
        RXByteisAligned(0) <= rxresetdone;
    end generate;

end Behavioral;

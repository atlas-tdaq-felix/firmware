----------------------------------------------------------------------------------
--! Company:  Argonne National Laboratory
--! Engineer: Marco Trovato (mtrovato@anl.gov)
----------------------------------------------------------------------------------
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

library XPM;
    use xpm.vcomponents.all;

library UNISIM;
    use UNISIM.VComponents.all;
    use work.FELIX_package.all;


entity ltittc_wavegen is
    Generic (
        GENCOUNTER         : boolean := false
    );
    port (
        reset_in          : in std_logic;
        reset_cnt_in      : in std_logic;
        clk40_in          : in std_logic;
        clk240_in         : in std_logic;
        data_out          : out std_logic_vector(31 downto 0); --@clk240_in
        isK_out           : out std_logic_vector( 3 downto 0); --@clk240_in
        tx_data_40MHZ_out : out std_logic_vector(191 downto 0) --simu only
    );

end ltittc_wavegen;


architecture top of ltittc_wavegen is
    constant zero_24b             : std_logic_vector( 23 downto 0) := (others =>'0');
    constant zero_135b            : std_logic_vector(135 downto 0) := (others =>'0');
    signal counter_simu          : std_logic_vector(23 downto 0) := (others =>'0');
    --counter thresholds
    constant CNT1MHZ_TH            : std_logic_vector(5 downto 0)  := "100111"; --39
    constant ORBITID_TH            : std_logic_vector(11 downto 0) := "110111101011"; --3563
    --arbitrary
    constant CNTSL0ID_TH           : std_logic_vector(5 downto 0)  := "000111";
    constant CNTSORB_TH            : std_logic_vector(5 downto 0)  := "111100";
    constant CNTSYNC_TH            : std_logic_vector(5 downto 0)  := "011111";
    constant CNTGRST_TH            : std_logic_vector(5 downto 0)  := "111111";
    constant LBID_TH               : std_logic_vector(5 downto 0)  := "000011";
    --

    --@40 MHz
    signal tx_usrmsg_data_40MHZ  : std_logic_vector(191 downto 0) := (others =>'0');
    signal tx_ttcdata_40MHZ      : std_logic_vector(191 downto 0) := (others =>'0');
    signal tx_data_40MHZ         : std_logic_vector(191 downto 0) := (others =>'0'); --=ttc 1 clk40 out of 40, =usrmsg the rest
    signal tx_isk_40MHZ          : std_logic_vector( 23 downto 0) := (others =>'0');
    --@240 MHz
    signal tx_data_40MHZ_reg240  : std_logic_vector(191 downto 0) := (others =>'0');
    signal tx_isk_40MHZ_reg240   : std_logic_vector( 23 downto 0) := (others =>'0');
    signal tx_data_ttc           : std_logic_vector( 31 downto 0) := (others =>'0');
    signal tx_isk_ttc            : std_logic_vector(  3 downto 0) := (others =>'0');
    signal tx_data_240MHZ        : std_logic_vector(191 downto 0) := (others =>'0');
    signal tx_isk_240MHZ         : std_logic_vector( 23 downto 0) := (others =>'0');


    signal  TYPEMSG              : std_logic := '1';
    constant  PartN                : std_logic_vector(  3 downto 1)  := "101"; --arbitrary
    signal  BCID                 : std_logic_vector( 11 downto 0)  := (others => '0');
    constant  SYNCUSRDATA          : std_logic_vector( 15 downto 0)  := x"ABCD"; --arbitrary
    constant  ASYNCUSRDATA         : std_logic_vector( 119 downto 0) := x"123456789ABCDEF123456789ABCDEF"; --arbitrary                                                                    --
    constant  SYNCGLOBALDATA       : std_logic_vector( 15 downto 0) := (others => '0');
    constant  TURNSIGNAL           : std_logic := '0';
    constant  ERRORFLAGS           : std_logic_vector(3 downto 0) := (others => '0');
    signal  L0A                  : std_logic := '0';
    signal  SL0ID                : std_logic := '0';
    signal  SORB                 : std_logic := '0';
    signal  SYNC                 : std_logic := '0';
    signal  GRST                 : std_logic := '0';
    signal  L0ID                 : std_logic_vector(37 downto 0) := (others => '0');
    signal  ORBITID              : std_logic_vector(31 downto 0) := (others => '0');
    signal  TTYPE                : std_logic_vector(15 downto 0) := (others => '0');
    signal  LBID                 : std_logic_vector(15 downto 0) := (others => '0');
    constant  D16_2                : std_logic_vector( 7 downto 0) := (others => '0');
    constant  RSVD1                : std_logic_vector(31 downto 0) := (others => '0');
    constant  RSVD0                : std_logic_vector(10 downto 0) := (others => '0');
    constant  CRC                  : std_logic_vector(15 downto 0) := (others => '0'); --todo

    --Intermediate outputs of dsp counters
    signal BCID_extra, L0ID_extra, ORBITID_extra, LBID_extra: std_logic_vector(47 downto 0);

    signal counter               : integer range 0 to 4000 := 0;
    --signal cnt_tmp               : integer range 0 to 5;
    signal trigger1MHz           : std_logic := '0';

    signal orbitid_cnt_ce        : std_logic := '0'; --ce for orbitid
    signal lbid_cnt_ce           : std_logic := '0'; --ce for lbid -- @suppress "Signal lbid_cnt_ce is never read"



--COMPONENT wavegen_dsp_counter
--    PORT (
--        CLK : IN STD_LOGIC;
--        CE : IN STD_LOGIC;
--        SCLR : IN STD_LOGIC;
--        Q : OUT STD_LOGIC_VECTOR(47 DOWNTO 0)
--    );
--END COMPONENT;




begin
    tx_ttcdata_gencounter: if GENCOUNTER generate
        tx_ttcdata_40MHZ     <=  zero_24b & Kchar_comma & zero_135b & counter_simu;
        tx_usrmsg_data_40MHZ <=  tx_ttcdata_40MHZ;
    end generate;
    tx_ttcdata_notgencounter: if GENCOUNTER = false generate
        --keeping it registered because not to be biassed by routing delays of DSPs
        tx_usrmsg_data_40MHZ <= CRC & D16_2 & Kchar_comma                        &                     --word5(byte3,2,1)
                                RSVD1                                            &                     --word4
                                ASYNCUSRDATA(63 downto   0)                      &                     --word3,2
                                SYNCGLOBALDATA & TURNSIGNAL & ERRORFLAGS & RSVD0 &                     --word1
                                TYPEMSG & PartN & BCID & SYNCUSRDATA;                                  --word0

        --emulate TTC message : 192b @40 MHz
        tx_ttcdata_40MHZ     <= CRC & D16_2 & Kchar_comma &                                            --word5(byte3,2,1)
                                TTYPE & LBID &                                                         --word4b_8array_type
                                ORBITID &                                                              --word3
                                L0ID(37 downto 6) &                                                    --word2
                                SYNCGLOBALDATA & TURNSIGNAL & ERRORFLAGS & SL0ID &                     --word1
                                SORB & SYNC & GRST & L0A & L0ID(5 downto 0) &                          --word1
                                TYPEMSG & PartN & BCID & SYNCUSRDATA;                                  --word0
    end generate;

    --  tx_usrmsg_data_40MHZ <= Kchar_comma & zero_184b;

    --emulate TTC message : 192b @40 MHz
    --  tx_ttcdata_40MHZ     <=  Kchar_comma & zero_184b;



    tx_isk_40MHZ          <= "0" & "0" & "0" & "1" &                                                --word5(byte3,2,1)
                             "0" & "0" & "0" & "0" &                                                --word4
                             "0" & "0" & "0" & "0" &                                                --word3
                             "0" & "0" & "0" & "0" &                                                --word2
                             "0" & "0" & "0" & "0" &                                                --word1
                             "0" & "0" & "0" & "0" ;                                                --word0


    MAINCNTER: process (clk40_in)
    begin
        if (rising_edge(clk40_in)) then
            if (reset_cnt_in = '1') then
                counter <= 0;
            else
                counter <= counter + 1;
            end if;
        end if;
    end process;
    cnt_gencounter: if GENCOUNTER generate
        SIMUCNTER: process (clk40_in)
        begin
            if (rising_edge(clk40_in)) then
                if (reset_cnt_in = '1') then
                    counter_simu <= (others => '0');
                else
                    counter_simu <= counter_simu + x"1";
                end if;
            end if;
        end process;
    end generate;


    --comma needs to be always available otherwise ttc_clk40 disappears
    tx_data_40MHZ <= tx_ttcdata_40MHZ when TYPEMSG = '0' else tx_usrmsg_data_40MHZ;
    --
    CNTER_1MHZ: process (clk40_in)
    begin
        if (rising_edge(clk40_in)) then
            if (counter = CNT1MHZ_TH) then
                TYPEMSG <= '0';
                trigger1MHz <= '1';
                L0A     <= '1';
                TTYPE   <= TTYPE + 1;

            else
                TYPEMSG <= '1';
                trigger1MHz <= '0';
                L0A     <= '0';
                TTYPE   <= TTYPE;
            end if;

            if (counter = ORBITID_TH) then
                orbitid_cnt_ce     <= '1';
            else
                orbitid_cnt_ce     <= '0';
            end if;

            if (counter = LBID_TH) then
                lbid_cnt_ce     <= '1';
            else
                lbid_cnt_ce     <= '0';
            end if;


            if (counter = CNTSL0ID_TH) then
                SL0ID     <= '1';
            else
                SL0ID     <= '0';
            end if;

            if (counter = CNTSORB_TH) then
                SORB     <= '1';
            else
                SORB     <= '0';
            end if;

            if (counter = CNTSYNC_TH) then
                SYNC     <= '1';
            else
                SYNC     <= '0';
            end if;

            if (counter = CNTGRST_TH) then
                GRST     <= '1';
            else
                GRST     <= '0';
            end if;

        end if;
    end process;

    --    bcid_cnt : wavegen_dsp_counter
    --        PORT MAP (
    --            CLK  => clk40_in,
    --            CE   => '1',
    --            SCLR => reset_in,
    --            Q    => BCID_extra
    --        );

    bcid_cnt: process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            if reset_in = '1' then
                BCID_extra <= (others => '0');
            else
                BCID_extra <= BCID_extra + 1;
            end if;
        end if;
    end process;

    BCID <= BCID_extra(11 downto 0);

    --    l0id_cnt : wavegen_dsp_counter
    --        PORT MAP (
    --            CLK   => clk40_in,
    --            CE    => trigger1MHz,
    --            SCLR  => reset_in,
    --            Q     => L0ID_extra
    --        );


    l0id_cnt: process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            if reset_in = '1' then
                L0ID_extra <= (others => '0');
            else
                if trigger1MHz = '1' then
                    L0ID_extra <= L0ID_extra + 1;
                end if;
            end if;
        end if;
    end process;

    L0ID <= L0ID_extra(37 downto 0);

    --    orbitid_cnt : wavegen_dsp_counter
    --        PORT MAP (
    --            CLK   => clk40_in,
    --            CE    => orbitid_cnt_ce,
    --            SCLR  => reset_in,
    --            Q     => ORBITID_extra
    --        );

    orbitid_cnt: process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            if reset_in = '1' then
                ORBITID_extra <= (others => '0');
            else
                if orbitid_cnt_ce = '1' then
                    ORBITID_extra <= ORBITID_extra + 1;
                end if;
            end if;
        end if;
    end process;

    ORBITID <= ORBITID_extra(31 downto 0);

    --    lbid_cnt : wavegen_dsp_counter
    --        PORT MAP (
    --            CLK   => clk40_in,
    --            CE    => lbid_cnt_ce,
    --            SCLR  => reset_in,
    --            Q     => LBID_extra
    --        );

    lbid_cnt: process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            if reset_in = '1' then
                LBID_extra <= (others => '0');
            else
                if orbitid_cnt_ce = '1' then
                    LBID_extra <= LBID_extra + 1;
                end if;
            end if;
        end if;
    end process;

    LBID <= LBID_extra(15 downto 0);

    cdc_txdata_40to240: for i in 0 to 191 generate
        xpm_cdc_rxbyteisaligned : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 4,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 1
            )
            port map (
                src_clk => clk40_in,
                src_in => tx_data_40MHZ(i),
                dest_clk => clk240_in,
                dest_out => tx_data_40MHZ_reg240(i)
            );
    end generate;

    cdc_isk_40to240: for i in 0 to 23 generate
        xpm_cdc_rxbyteisaligned : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 4,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 1
            )
            port map (
                src_clk => clk40_in,
                src_in => tx_isk_40MHZ(i),
                dest_clk => clk240_in,
                dest_out => tx_isk_40MHZ_reg240(i)
            );
    end generate;

    ---192b @40 MHZ to 32b @240 MHZ gearbox
    process(clk240_in)
        variable cnt           : integer range 0 to 5;
    begin
        if rising_edge(clk240_in) then
            if (reset_in = '1') then
                tx_data_ttc      <= (others => '0');
                tx_isk_ttc       <= (others => '0');
                tx_data_240MHZ   <= (others => '0');
                tx_isk_240MHZ    <= (others => '0');
                cnt := 0;
            else
                tx_data_ttc            <= tx_data_240MHZ(31+32*cnt downto 32*cnt);
                tx_isk_ttc             <= tx_isk_240MHZ(3+4*cnt downto 4*cnt);

                if (cnt /= 5) then
                    cnt := cnt+1;
                    tx_data_240MHZ <= tx_data_240MHZ;
                    tx_isk_240MHZ  <= tx_isk_240MHZ;
                else
                    cnt := 0;
                    tx_data_240MHZ <= tx_data_40MHZ_reg240; --tx_data_40MHZ;
                    tx_isk_240MHZ  <= tx_isk_40MHZ_reg240;  -- tx_isk_40MHZ;
                end if;
            end if; --if(rst_hw='1')
        --cnt_tmp <= cnt;
        end if; --if rising_edge(clk40)
    end process;

    data_out <= tx_data_ttc;
    isK_out  <= tx_isk_ttc;

    tx_data_40MHZ_out <= tx_data_40MHZ;

end top;


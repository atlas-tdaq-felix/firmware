----------------------------------------------------------------------------------
-- Company:  Nikhef
-- Engineer: Mesfin Gebyehu (mgebyehu@nikhef.nl)
----------------------------------------------------------------------------------
LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
library XPM;
    use xpm.vcomponents.all;

entity ltittc_transmitter is
    port (
        reset_in       : in std_logic;
        clk40_in       : in std_logic;
        clk240_in      : in std_logic;
        busy_in        : in std_logic;
        data_out       : out std_logic_vector(15 downto 0);
        isK_out        : out std_logic_vector(1 downto 0)
    );
end ltittc_transmitter;


architecture Behavioral of ltittc_transmitter is

    --*****CRC interface signals
    signal crc_en     : std_logic := '0';
    signal crc_reset   : std_logic := '0';
    signal crc_in    : std_logic_vector(15 downto 0);
    signal crc_out    : std_logic_vector(15 downto 0);

    --*****Delay signals to insert CRC_OUT in a frame
    signal data_out_d0  : std_logic_vector(15 downto 0);
    signal Kchar_rx_d0  : std_logic_vector(1 downto 0);
    signal data_out_d1  : std_logic_vector(15 downto 0);
    signal Kchar_rx_d1  : std_logic_vector(1 downto 0);

    --***** frame counters
    signal cnt             : integer range 0 to 5;
    signal i_cnt             : integer range 0 to 5;

    --***********LTI_TTC Upstream data type
    signal data_out_t     : std_logic_vector(15 downto 0);
    signal Kchar_rx      : std_logic_vector(1 downto 0);
    signal busy_out      : std_logic := '0';
    constant CalReq       : std_logic_vector(2 downto 0) := (others => '1');
    constant Reserved     : std_logic_vector(59 downto 0) := (59 => '1', 49 => '1', 39 => '1', 29 => '1', 19 => '1', others => '0');
    type array_std_logic_vector16_type   is array (0 to 4)  of std_logic_vector(15 downto 0);
    signal ltittc_upstream : array_std_logic_vector16_type;

--  COMPONENT ila_ltittc_trans
--  PORT (
--    clk : IN STD_LOGIC;
--    probe0 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
--    probe1 : in STD_LOGIC_VECTOR(1 DOWNTO 0);
--    probe2 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
--    probe3 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
--    probe4 : in STD_LOGIC_VECTOR(0 DOWNTO 0);
--    probe5 : in STD_LOGIC_VECTOR(0 DOWNTO 0)
--      );
--  END COMPONENT;

begin

    --    --*** ILA to debug crc out value
    --      comp_ila_lti : ila_ltittc_trans
    --       port map (
    --         clk   => clk240_in,
    --         probe0 => data_out_d1,
    --         probe1 => Kchar_rx_d1,
    --         probe2 => crc_out,
    --         probe3 => crc_in,
    --         probe4(0) => crc_en,
    --         probe5(0) => crc_reset
    --        );

    xpm_busy_in_signal : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 4,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map (
            src_clk => clk40_in,
            src_in => busy_in,
            dest_clk => clk240_in,
            dest_out => busy_out
        );


    --********* crc signals. The crc output is over 4*16b input signals(payload)
    crc_en <= '1' when i_cnt < 4 else '0';
    crc_reset <= '1' when i_cnt = 5 else '0'; -- reset insterted at the k-char output position
    crc_in <= data_out_t when i_cnt < 4 else (others => '1');
    --**** To form the upstream frame

    --***delay to instert CRC output value
    data_out <= data_out_d1;
    isK_out <= Kchar_rx_d1;

    --*******upstream frame
    ltittc_upstream(0) <= busy_out & CalReq & Reserved(11 downto 0);
    ltittc_upstream(1) <= Reserved(27 downto 12);
    ltittc_upstream(2) <= Reserved(43 downto 28);
    ltittc_upstream(3) <= Reserved(59 downto 44);
    ltittc_upstream(4) <= (others => '0');


    CRC16_inst : entity work.crc16_ltittc
        port map
    (
            data_in => crc_in,
            crc_en  => crc_en,
            rst     => crc_reset,
            clk     => clk240_in,
            crc_out => crc_out
        );

    process(clk240_in)
    begin
        if rising_edge(clk240_in) then
            if (reset_in = '1') then
                cnt <= 0;
                i_cnt <= 0;
                data_out_t <= (others => '0');
                Kchar_rx <= "00";
                Kchar_rx_d0 <= "00";
                data_out_d0 <= (others => '0');
                Kchar_rx_d1 <= "00";
                data_out_d1 <= (others => '0');
            else

                i_cnt <= cnt; -- i_cnt is used to enable crc input
                if(cnt < 5) then cnt <= cnt + 1; else cnt <= 0; end if;

                Kchar_rx_d0 <= Kchar_rx;
                Kchar_rx_d1 <= Kchar_rx_d0;
                data_out_d0 <= data_out_t;
                if(cnt = 0) then data_out_d1 <= crc_out; else data_out_d1 <= data_out_d0;
                end if;

                if(cnt = 5) then data_out_t <= X"50BC"; Kchar_rx <= "01";  else
                    data_out_t <= ltittc_upstream(cnt); Kchar_rx <= "00";
                end if;

            end if;
        end if;
    end process;

end Behavioral;

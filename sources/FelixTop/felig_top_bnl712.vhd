--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Shelfali Saxena
--!               mtrovato
--!               Ricardo Luz
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.


--!------------------------------------------------------------------------------
--!
--!           NIKHEF - National Institute for Subatomic Physics
--!
--!                       Electronics Department
--!
--!-----------------------------------------------------------------------------
--! @class felix_top
--!
--!
--! @author      Andrea Borga    (andrea.borga@nikhef.nl)<br>
--!              Frans Schreuder (frans.schreuder@nikhef.nl)
--!              adapted by Marco Trovato (mtrovato@anl.gov)
--!
--!
--! @date        07/01/2015    created
--!
--! @version     1.0
--!
--! @brief
--! Top level for the FELIX project, containing GBT, CentralRouter and PCIe DMA core
--!
--!
--!
--! @detail
--!
--!-----------------------------------------------------------------------------
--! @TODO
--!
--!
--! ------------------------------------------------------------------------------
--
--! @brief ieee



library ieee, UNISIM;
    use ieee.numeric_std.all;
    use UNISIM.VCOMPONENTS.all;
    use ieee.std_logic_1164.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;
    use work.axi_stream_package.all;
    use work.type_lib.ALL;                  --MT added
    use work.interlaken_package.slv_67_array;
library xpm;
    use xpm.vcomponents.all;

entity felig_top_bnl712 is
    generic(
        NUMBER_OF_INTERRUPTS            : integer := 8;
        NUMBER_OF_DESCRIPTORS           : integer := 2;
        APP_CLK_FREQ                    : integer := 200;
        BUILD_DATETIME                  : std_logic_vector(39 downto 0) := x"0000FE71CE";
        GBT_NUM                         : integer := 8; -- number of GBT channels
        AUTOMATIC_CLOCK_SWITCH          : boolean := true;
        USE_BACKUP_CLK                  : boolean := true; -- true to use 100/200 mhz board crystal, false to use TTC clock
        CARD_TYPE                       : integer := 712;
        generateTTCemu                  : boolean := false;
        GIT_HASH                        : std_logic_vector(159 downto 0) := x"0000000000000000000000000000000000000000";
        GIT_TAG                         : std_logic_vector(127 downto 0) := x"00000000000000000000000000000000";
        GIT_COMMIT_NUMBER               : integer := 0;
        COMMIT_DATETIME                 : std_logic_vector(39 downto 0) := x"0000FE71CE";
        GTHREFCLK_SEL                   : std_logic := '0'; -- GREFCLK: '1', MGTREFCLK: '0'
        toHostTimeoutBitn               : integer := 16;
        FIRMWARE_MODE                   : integer := 0;
        PLL_SEL                         : std_logic := '1'; -- 0: CPLL, 1: QPLL
        USE_Si5324_RefCLK               : boolean := false;
        GENERATE_XOFF                   : boolean := true; -- FromHost Xoff transmission enabled on Full mode busy
        --Generics for semistatic GBT mode configurations. 1 bit per e-group to disable this type of epath at build time
        --For normal GBT mode, only the 5 LSBs are used.
        IncludeDecodingEpath2_HDLC      : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath2_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath4_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath8_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath16_8b10b    : std_logic_vector(6 downto 0) := "0000000"; --lpGBT only
        IncludeDecodingEpath32_8b10b    : std_logic_vector(6 downto 0) := "0000000"; --lpGBT only
        IncludeDirectDecoding           : std_logic_vector(6 downto 0) := "0000000";
        IncludeEncodingEpath2_HDLC      : std_logic_vector(4 downto 0) := "00000";
        IncludeEncodingEpath2_8b10b     : std_logic_vector(4 downto 0) := "00000";
        IncludeEncodingEpath4_8b10b     : std_logic_vector(4 downto 0) := "00000";
        IncludeEncodingEpath8_8b10b     : std_logic_vector(4 downto 0) := "00000";
        IncludeDirectEncoding           : std_logic_vector(4 downto 0) := "00000";
        INCLUDE_TTC                     : std_logic_vector(4 downto 0) := "00000";
        TTC_SYS_SEL                     : std_logic := '0'; -- 0: TTC, 1: LTITTC P2P
        INCLUDE_RD53                    : std_logic_vector(4 downto 0) := "00000";
        DATA_WIDTH                      : integer := 256;
        PCIE_LANES                      : integer := 8;
        BLOCKSIZE                       : integer := 1024;
        CHUNK_TRAILER_32B               : boolean := false;
        ENDPOINTS                       : integer := 2;
        GTREFCLKS                       : integer := 5;
        LMK_CLKS                        : integer := 8;
        SIMULATION                      : boolean := false;
        LOCK_PERIOD                     : integer := 20480;--Maximum chunk size of 2048 bytes on slowest E-link supported, used for locking 8b10b decoders
        FULL_HALFRATE                   : boolean := false;
        CREnableFromHost                : boolean := true;
        KCU_LOWER_LATENCY               : integer := 0;
        AddFULLMODEForDUNE              : boolean := false; --Add an additional FULL mode decoder without superchunk factor for DUNE
        SUPPORT_HDLC_DELAY              : boolean := false; -- support for inter-packet delays in HDLC encoders
        INCLUDE_XOFF                    : boolean := true;
        USE_VERSAL_CPM                  : boolean := false; --set to true for BNL182
        ENABLE_XVC                      : boolean := false;
        TOP_ILA                         : integer := 0
    );
    port (
        BUSY_OUT              : out    std_logic_vector(NUM_BUSY_OUTPUTS(CARD_TYPE)-1 downto 0);
        CLK40_FPGA2LMK_N      : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        CLK40_FPGA2LMK_P      : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        CLK_TTC_N             : in     std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);
        CLK_TTC_P             : in     std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);
        DATA_TTC_N            : in     std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);
        DATA_TTC_P            : in     std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);
        GTREFCLK_Si5324_N_IN  : in     std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0);
        GTREFCLK_Si5324_P_IN  : in     std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0);
        OPTO_LOS              : in     std_logic_vector(NUM_OPTO_LOS(CARD_TYPE)-1 downto 0);
        I2C_SMB               : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        I2C_SMBUS_CFG_nEN     : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        I2C_nRESET_PCIe       : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        LMK_CLK               : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_DATA              : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_GOE               : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_LD                : in     std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_LE                : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_SYNCn             : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LOL_ADN               : in     std_logic_vector(NUM_ADN(CARD_TYPE)-1 downto 0);
        LOS_ADN               : in     std_logic_vector(NUM_ADN(CARD_TYPE)-1 downto 0);
        MGMT_PORT_EN          : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        NT_PORTSEL            : out    std_logic_vector(NUM_NT_PORTSEL(CARD_TYPE)-1 downto 0);
        PCIE_PERSTn_out       : out    std_logic_vector(NUM_PEX(CARD_TYPE)*2-1 downto 0);
        PEX_PERSTn            : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        PEX_SCL               : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        PEX_SDA               : inout  std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        select_bifurcation    : in     std_logic_vector(NUM_BIFURCATION_SELECT(CARD_TYPE)-1 downto 0);
        PORT_GOOD             : in     std_logic_vector(NUM_PEX(CARD_TYPE)*8-1 downto 0);
        Perstn_open           : in     std_logic_vector(NUM_PEX(CARD_TYPE)*2-1 downto 0); -- @suppress "Unused port: Perstn_open is not used in work.felix_top(structure)"
        GTREFCLK_N_IN             : in     std_logic_vector(GTREFCLKS-1 downto 0);
        GTREFCLK_P_IN             : in     std_logic_vector(GTREFCLKS-1 downto 0);
        LMK_N                 : in     std_logic_vector(NUM_LMK(CARD_TYPE)*8-1 downto 0);
        LMK_P                 : in     std_logic_vector(NUM_LMK(CARD_TYPE)*8-1 downto 0);
        RX_N                      : in     std_logic_vector(GBT_NUM-1 downto 0);
        RX_P                      : in     std_logic_vector(GBT_NUM-1 downto 0);
        TX_N                      : out    std_logic_vector(GBT_NUM-1 downto 0);
        TX_P                      : out    std_logic_vector(GBT_NUM-1 downto 0);
        SCL                       : inout  std_logic;
        SDA                       : inout  std_logic;
        SHPC_INT              : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        SI5345_A              : out    std_logic_vector(NUM_SI5345(CARD_TYPE)*2-1 downto 0);
        SI5345_INSEL          : out    std_logic_vector(NUM_SI5345(CARD_TYPE)*2-1 downto 0);
        SI5345_OE             : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_RSTN           : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_SEL            : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_nLOL           : in     std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        STN0_PORTCFG          : out    std_logic_vector(NUM_STN0_PORTCFG(CARD_TYPE)-1 downto 0);
        STN1_PORTCFG          : out    std_logic_vector(NUM_STN1_PORTCFG(CARD_TYPE)-1 downto 0);
        SmaOut                : out    std_logic_vector(NUM_SMA(CARD_TYPE)-1 downto 0);
        TACH                  : in     std_logic_vector(NUM_TACH(CARD_TYPE)-1 downto 0);
        TESTMODE              : out    std_logic_vector(NUM_TESTMODE(CARD_TYPE)-1 downto 0);
        UPSTREAM_PORTSEL      : out    std_logic_vector(NUM_UPSTREAM_PORTSEL(CARD_TYPE)-1 downto 0);
        app_clk_in_n              : in     std_logic;
        app_clk_in_p              : in     std_logic;
        clk_adn_160_out_n     : out    std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0);
        clk_adn_160_out_p     : out    std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0);
        clk40_ttc_ref_out_n   : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0); -- Towards Si5345 CLKIN
        clk40_ttc_ref_out_p   : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0); -- Towards Si5345 CLKIN
        emcclk                : in     std_logic_vector(NUM_EMCCLK(CARD_TYPE)-1 downto 0);
        i2cmux_rst            : out    std_logic_vector(NUM_I2C_MUXES(CARD_TYPE)-1 downto 0);
        leds                  : out    std_logic_vector(NUM_LEDS(CARD_TYPE)-1 downto 0);
        flash_SEL             : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_a               : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)*25-1 downto 0);
        flash_a_msb           : inout  std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)*2-1 downto 0);
        flash_adv             : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_cclk            : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_ce              : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_d               : inout  std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)*16-1 downto 0);
        flash_re              : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_we              : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        opto_inhibit          : out    std_logic_vector(NUM_OPTO_LOS(CARD_TYPE)-1 downto 0);
        si5324_resetn         : out    std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0);
        pcie_rxn                  : in     std_logic_vector((ENDPOINTS*PCIE_LANES)-1 downto 0);
        pcie_rxp                  : in     std_logic_vector((ENDPOINTS*PCIE_LANES)-1 downto 0);
        pcie_txn                  : out    std_logic_vector((ENDPOINTS*PCIE_LANES)-1 downto 0);
        pcie_txp                  : out    std_logic_vector((ENDPOINTS*PCIE_LANES)-1 downto 0); --! PCIe link lanes
        sys_clk_n                 : in     std_logic_vector(ENDPOINTS-1 downto 0);
        sys_clk_p                 : in     std_logic_vector(ENDPOINTS-1 downto 0); --! 100MHz PCIe reference clock
        sys_reset_n               : in     std_logic; --! Active-low system reset from PCIe interface);
        uC_reset_N                : out    std_logic_vector(NUM_UC_RESET_N(CARD_TYPE)-1 downto 0)
    );
end entity felig_top_bnl712;


architecture structure of felig_top_bnl712 is
    constant NUMELINKmax       : integer := 112;
    constant NUMEGROUPmax      : integer := 7;
    signal rst_hw                              : std_logic;
    signal global_reset_soft_appreg_clk        : std_logic;
    signal global_rst_soft_40                  : std_logic;
    signal clk10_xtal                          : std_logic;
    signal clk40_xtal                          : std_logic;
    signal clk40                               : std_logic;
    signal clk240                              : std_logic;
    signal clk_adn_160                         : std_logic;
    signal global_appreg_clk                   : std_logic;
    signal global_register_map_control_appreg_clk : register_map_control_type;
    signal global_register_map_40_control      : register_map_control_type;
    signal register_map_gen_board_info         : register_map_gen_board_info_type;
    signal register_map_link_monitor           : register_map_link_monitor_type;
    signal register_map_ttc_monitor            : register_map_ttc_monitor_type;
    signal register_map_ltittc_monitor          : register_map_ltittc_monitor_type;
    signal register_map_hk_monitor             : register_map_hk_monitor_type;
    signal register_map_hk_monitor_wupper           : register_map_hk_monitor_type;
    signal register_map_generators             : register_map_generators_type;
    signal MMCM_Locked_out                     : std_logic;
    signal MMCM_OscSelect_out                  : std_logic;
    signal LinkAligned                              : std_logic_vector(GBT_NUM-1 downto 0);
    signal GBT_DOWNLINK_USER_DATA                   : array_120b(0 to (GBT_NUM-1));
    signal GBT_UPLINK_USER_DATA                     : array_120b(0 to (GBT_NUM-1));
    signal lpGBT_DOWNLINK_USER_DATA                 : array_224b(0 to GBT_NUM-1);
    signal lpGBT_DOWNLINK_IC_DATA                   : array_2b(0 to GBT_NUM-1);
    signal lpGBT_DOWNLINK_EC_DATA                   : array_2b(0 to GBT_NUM-1);
    signal lpGBT_UPLINK_USER_DATA                   : array_32b(0 to GBT_NUM-1);
    signal lpGBT_UPLINK_EC_DATA                     : array_2b(0 to GBT_NUM-1);
    signal lpGBT_UPLINK_IC_DATA                     : array_2b(0 to GBT_NUM-1);
    signal lpGBT_UPLINK_USER_DATA_FOSEL             : array_224b(0 to GBT_NUM/ENDPOINTS-1);
    signal lnk_up                              : std_logic_vector(1 downto 0);
    signal GTREFCLK_N_s : std_logic_vector(GTREFCLKS-1 downto 0);
    signal GTREFCLK_P_s : std_logic_vector(GTREFCLKS-1 downto 0);
    signal RXUSRCLK                            : std_logic_vector(GBT_NUM-1 downto 0);
    signal lane_control      : array_of_lane_control_type(GBT_NUM-1 downto 0);
    signal lane_monitor      : array_of_lane_monitor_type(GBT_NUM-1 downto 0);
    signal register_map_lane_remapper_output   : register_map_monitor_type;
    signal TXUSRCLK                            : std_logic_vector(GBT_NUM-1 downto 0);
    signal link_tx_data_228b_array_tmp              : array_228b(0 to GBT_NUM-1);
    signal link_rx_data_120b_array_tmp              : array_120b(0 to GBT_NUM-1);
    signal lpgbt_rx_data_120b_array_tmp             : array_120b(0 to GBT_NUM-1);
    signal link_tx_flag_i : std_logic_vector(GBT_NUM-1 downto 0);
    signal link_rx_flag_i : std_logic_vector(GBT_NUM-1 downto 0);
    signal CLK40_FPGA2LMK_N_link : std_logic;
    signal CLK40_FPGA2LMK_P_link : std_logic;
    signal leds7                                    : std_logic_vector(6 downto 0);
    signal LMK_LOCKED                               : std_logic;
    signal WupperToCPM                              : WupperToCPM_array_type(0 to 1);
    signal CPMToWupper                              : CPMToWupper_array_type(0 to 1);
    signal data_ready_tx_to_link_wrapper            : std_logic_vector(0 to GBT_NUM-1);
    signal DDR_inout                                : DDR_inout_array_type(0 to NUM_DDR(CARD_TYPE)-1);
    signal LPDDR_inout                              : LPDDR_inout_array_type(0 to NUM_LPDDR(CARD_TYPE)-1);
    signal axi_miso_ttc_lti                         : axi_miso_type;
begin

    NT_PORTSEL <= "111";
    TESTMODE <= "000";
    UPSTREAM_PORTSEL <= "000";
    STN0_PORTCFG <= "0Z";
    STN1_PORTCFG <= "01";

    -- assuming SmaOut x3,x4,x5,x6 corresponds to [0],[1],[2],[3]
    SmaOut(0)           <= TXUSRCLK(0);
    SmaOut(3 downto 1)  <= (others => '0');
    I2C_nRESET_PCIe(0)  <= '1';
    uC_reset_N(0)       <= '1';

    GTREFCLK_N_s <= GTREFCLK_N_IN;
    GTREFCLK_P_s <= GTREFCLK_P_IN;

    si5324_resetn(0)    <= not rst_hw;
    CLK40_FPGA2LMK_N(0) <= CLK40_FPGA2LMK_N_link;
    CLK40_FPGA2LMK_P(0) <= CLK40_FPGA2LMK_P_link;

    linkwrapper0: entity work.link_wrapper_FELIG
        generic map(
            GBT_NUM => GBT_NUM,
            CARD_TYPE => CARD_TYPE,
            GTHREFCLK_SEL => GTHREFCLK_SEL,
            FIRMWARE_MODE => FIRMWARE_MODE,
            PLL_SEL => '1', --CPLL not implemented
            GTREFCLKS => GTREFCLKS,
            OPTO_TRX => NUM_OPTO_LOS(CARD_TYPE))
        port map(
            register_map_control => global_register_map_40_control,
            register_map_link_monitor => register_map_link_monitor,
            clk40 => clk40,
            clk240 => clk240,
            clk40_xtal => clk40_xtal,
            GTREFCLK_N_in => GTREFCLK_N_s,
            GTREFCLK_P_in => GTREFCLK_P_s,
            rst_hw => rst_hw,
            OPTO_LOS => OPTO_LOS,
            RXUSRCLK_OUT => RXUSRCLK,
            GBT_DOWNLINK_USER_DATA => GBT_DOWNLINK_USER_DATA,
            GBT_UPLINK_USER_DATA => GBT_UPLINK_USER_DATA,
            lpGBT_DOWNLINK_USER_DATA => lpGBT_DOWNLINK_USER_DATA,
            lpGBT_DOWNLINK_IC_DATA => lpGBT_DOWNLINK_IC_DATA,
            lpGBT_DOWNLINK_EC_DATA => lpGBT_DOWNLINK_EC_DATA,
            lpGBT_UPLINK_USER_DATA => lpGBT_UPLINK_USER_DATA,
            lpGBT_UPLINK_EC_DATA => lpGBT_UPLINK_EC_DATA,
            lpGBT_UPLINK_IC_DATA => lpGBT_UPLINK_IC_DATA,
            data_ready_DOWNLINK => data_ready_tx_to_link_wrapper,
            LinkAligned => LinkAligned,
            TX_P => TX_P,
            TX_N => TX_N,
            RX_P => RX_P,
            RX_N => RX_N,
            LMK_P => LMK_P,
            LMK_N => LMK_N,
            link_rx_flag_i=>link_rx_flag_i,
            link_tx_flag_i=>link_tx_flag_i,
            TXUSRCLK_OUT => TXUSRCLK,
            CLK40_FPGA2LMK_N => CLK40_FPGA2LMK_N_link,
            CLK40_FPGA2LMK_P => CLK40_FPGA2LMK_P_link,
            clk320_in => '0', --simulation only
            clk10_xtal => clk10_xtal,
            LMK_DATA => LMK_DATA(0),
            LMK_CLK => LMK_CLK(0),
            LMK_LE => LMK_LE(0),
            LMK_GOE => LMK_GOE(0),
            LMK_LD => LMK_LD(0),
            LMK_SYNCn => LMK_SYNCn(0),
            LMK_LOCKED => LMK_LOCKED
        );

    clk0: entity work.clock_and_reset
        generic map(
            CARD_TYPE              => CARD_TYPE,
            APP_CLK_FREQ           => APP_CLK_FREQ,
            USE_BACKUP_CLK         => USE_BACKUP_CLK,
            AUTOMATIC_CLOCK_SWITCH => AUTOMATIC_CLOCK_SWITCH)
        port map(
            MMCM_Locked_out      => MMCM_Locked_out,
            MMCM_OscSelect_out   => MMCM_OscSelect_out,
            app_clk_in_n         => app_clk_in_n,
            app_clk_in_p         => app_clk_in_p,
            clk10_xtal           => clk10_xtal,
            clk160               => open,
            clk240               => clk240,
            clk250               => open,
            clk320               => open,
            clk40                => clk40,
            clk40_xtal           => clk40_xtal,
            clk80                => open,
            clk_adn_160          => clk_adn_160,
            clk_adn_160_out_n    => clk_adn_160_out_n,
            clk_adn_160_out_p    => clk_adn_160_out_p,
            clk_ttc_40           => '0',
            clk_ttcfx_ref_out_n  => clk40_ttc_ref_out_n,
            clk_ttcfx_ref_out_p  => clk40_ttc_ref_out_p,
            register_map_control => global_register_map_control_appreg_clk,
            reset_out            => rst_hw,
            sys_reset_n          => sys_reset_n);

    TXRXDATA_inst: for I in 0 to GBT_NUM - 1 generate
        link_rx_data_120b_array_tmp(I)  <= GBT_UPLINK_USER_DATA(I)         when FIRMWARE_MODE = FIRMWARE_MODE_FELIG_GBT else
                                           lpgbt_rx_data_120b_array_tmp(I) when FIRMWARE_MODE = FIRMWARE_MODE_FELIG_LPGBT;
        lpgbt_rx_data_120b_array_tmp(I)(119 downto 36) <= (others=>'0');
        lpgbt_rx_data_120b_array_tmp(I)( 35 downto  0) <= lpGBT_UPLINK_IC_DATA(I) & lpGBT_UPLINK_EC_DATA(I) & lpGBT_UPLINK_USER_DATA(I);

        GBT_DOWNLINK_USER_DATA(I)   <= link_tx_data_228b_array_tmp(I)(119 downto 0);
        lpGBT_DOWNLINK_USER_DATA(I) <= link_tx_data_228b_array_tmp(I)(223 downto 0);
        lpGBT_DOWNLINK_EC_DATA(I)   <= link_tx_data_228b_array_tmp(I)(225 downto 224);
        lpGBT_DOWNLINK_IC_DATA(I)   <= link_tx_data_228b_array_tmp(I)(227 downto 226);
    end generate TXRXDATA_inst;

    emulatorwrapper_i : entity work.EmulatorWrapper
        generic map(
            GBT_NUM                     => GBT_NUM,
            NUMELINKmax                 => NUMELINKmax,
            NUMEGROUPmax                    => NUMEGROUPmax,
            FIRMWARE_MODE                   => FIRMWARE_MODE)
        port map(
            clk40                           => clk40,
            gt_txusrclk_in                  => TXUSRCLK,
            gt_rxusrclk_in                  => RXUSRCLK,
            link_tx_data_228b_array_out       => link_tx_data_228b_array_tmp,
            data_ready_tx_out               => data_ready_tx_to_link_wrapper,
            link_rx_data_120b_array_in        => link_rx_data_120b_array_tmp,
            link_tx_flag_in                 => link_tx_flag_i,
            link_rx_flag_in                 => link_rx_flag_i,
            l1a_int_trigger_out             => open, --sim
            lane_control        => lane_control,
            lane_monitor                    => lane_monitor
        );

    comp_LaneRegisterRemapper : entity work.LaneRegisterRemapper
        generic map(
            GBT_NUM  => GBT_NUM,
            FIRMWARE_MODE => FIRMWARE_MODE)
        port map (
            register_map_monitor  => register_map_lane_remapper_output,
            register_map_control  => global_register_map_40_control,
            register_map_hk_monitor_in  => register_map_hk_monitor,
            register_map_hk_monitor_out => register_map_hk_monitor_wupper,
            LMK_LOCKED                  => LMK_LOCKED,
            LinkAligned                 => LinkAligned,
            lane_control      => lane_control,
            lane_monitor                => lane_monitor
        );

    appreg_sync: process(global_appreg_clk)
    begin
        if(rising_edge(global_appreg_clk)) then
            register_map_generators <= register_map_lane_remapper_output.register_map_generators;
        end if;
    end process;

    g_endpoints: for pcie_endpoint in 0 to ENDPOINTS-1 generate
        signal register_map_crtohost_monitor       : register_map_crtohost_monitor_type;--in master
        signal register_map_crfromhost_monitor     : register_map_crfromhost_monitor_type;--in master
        signal register_map_xoff_monitor           : register_map_xoff_monitor_type;
        signal register_map_gbtemu_monitor         : register_map_gbtemu_monitor_type;
        signal register_map_decoding_monitor       : register_map_decoding_monitor_type;
        signal register_map_encoding_monitor       : register_map_encoding_monitor_type;
        signal wr_data_count                       : slv12_array(0 to NUMBER_OF_DESCRIPTORS-2);
        signal rst_soft_40                         : std_logic;
        signal reset_soft_appreg_clk               : std_logic;
        signal appreg_clk: std_logic;
        signal register_map_control_appreg_clk       : register_map_control_type;
        signal register_map_40_control               : register_map_control_type;

    begin
        g_assign_endpoint0: if pcie_endpoint = 0 generate
            global_appreg_clk <= appreg_clk;
            global_register_map_control_appreg_clk <= register_map_control_appreg_clk;
            global_register_map_40_control <= register_map_40_control;
            global_reset_soft_appreg_clk <= reset_soft_appreg_clk;
            global_rst_soft_40 <= rst_soft_40;
        end generate;

        pcie0: entity work.wupper
            generic map(
                NUMBER_OF_INTERRUPTS => NUMBER_OF_INTERRUPTS,
                NUMBER_OF_DESCRIPTORS => NUMBER_OF_DESCRIPTORS,
                BUILD_DATETIME => BUILD_DATETIME,
                CARD_TYPE => CARD_TYPE,
                GIT_HASH => GIT_HASH,
                COMMIT_DATETIME => COMMIT_DATETIME,
                GIT_TAG => GIT_TAG,
                GIT_COMMIT_NUMBER => GIT_COMMIT_NUMBER,
                GBT_NUM => GBT_NUM,
                FIRMWARE_MODE => FIRMWARE_MODE,
                PCIE_ENDPOINT => pcie_endpoint,
                PCIE_LANES => PCIE_LANES,
                DATA_WIDTH => DATA_WIDTH,
                SIMULATION => SIMULATION,
                BLOCKSIZE => BLOCKSIZE,
                USE_ULTRARAM => false,
                ENABLE_XVC => ENABLE_XVC)
            port map(
                appreg_clk => appreg_clk,
                sync_clk => clk40,
                flush_fifo => open,
                interrupt_call => (others => '0'),
                lnk_up => lnk_up(pcie_endpoint),
                pcie_rxn => pcie_rxn(pcie_endpoint*8+(PCIE_LANES-1) downto pcie_endpoint*8),
                pcie_rxp => pcie_rxp(pcie_endpoint*8+(PCIE_LANES-1) downto pcie_endpoint*8),
                pcie_txn => pcie_txn(pcie_endpoint*8+(PCIE_LANES-1) downto pcie_endpoint*8),
                pcie_txp => pcie_txp(pcie_endpoint*8+(PCIE_LANES-1) downto pcie_endpoint*8),
                pll_locked => open,
                register_map_control_sync => register_map_40_control,
                register_map_control_appreg_clk => register_map_control_appreg_clk,
                register_map_gen_board_info => register_map_gen_board_info,
                register_map_crtohost_monitor => register_map_crtohost_monitor,
                register_map_crfromhost_monitor => register_map_crfromhost_monitor,
                register_map_decoding_monitor => register_map_decoding_monitor,
                register_map_encoding_monitor => register_map_encoding_monitor,
                register_map_gbtemu_monitor => register_map_gbtemu_monitor,
                register_map_link_monitor => register_map_link_monitor,
                register_map_ttc_monitor => register_map_ttc_monitor,
                register_map_ltittc_monitor => register_map_ltittc_monitor,
                register_map_xoff_monitor => register_map_xoff_monitor,
                register_map_hk_monitor => register_map_hk_monitor_wupper,
                register_map_generators => register_map_generators,
                wishbone_monitor => wishbone_monitor_c,
                ipbus_monitor => ipbus_monitor_c,
                regmap_mrod_monitor => regmap_mrod_monitor_c,
                reset_hard => open,
                reset_soft => open,
                reset_soft_appreg_clk => reset_soft_appreg_clk,
                sys_clk_n => sys_clk_n(pcie_endpoint),
                sys_clk_p => sys_clk_p(pcie_endpoint),
                sys_reset_n => sys_reset_n,
                tohost_busy_out => open,
                fromHostFifo_dout => open,
                fromHostFifo_empty => open,
                fromHostFifo_rd_clk => clk40,
                fromHostFifo_rd_en => '0',
                fromHostFifo_rst => rst_hw,
                toHostFifo_din => (others=> (others=> '0')),
                toHostFifo_prog_full => open,
                toHostFifo_rst => rst_hw,
                toHostFifo_wr_clk => clk40,
                toHostFifo_wr_en => (others=> '0'),
                clk250_out => open,
                master_busy_in => '0',
                Versal_network_device_fromHost_full_i => '0',
                Versal_network_device_fromHost_prog_full_i => '0',
                Versal_network_device_toHost_empty_i => '0',
                Versal_network_device_toHost_prog_empty_i => '0',
                Versal_network_device_tohost_dout_i => (others => '0'),
                Versal_network_device_fromHost_din_o => open,
                Versal_network_device_fromHost_wr_en_o => open,
                Versal_network_device_toHost_rd_en_o => open,
                Versal_network_device_fromHost_eop_o => open,
                Versal_network_device_toHost_eop_i => '0',
                Versal_network_device_fromHost_set_carrier_o => open,
                Versal_network_device_toHost_status_carrier_i => '0',
                CPMToWupper => CPMToWupper(pcie_endpoint),
                WupperToCPM => WupperToCPM(pcie_endpoint),
                dma_enable_out => open,
                toHostFifoBusy_out => open
            );

    end generate;

    hk0: entity work.housekeeping_module
        generic map(
            CARD_TYPE => CARD_TYPE,
            GBT_NUM => GBT_NUM/ENDPOINTS,
            ENDPOINTS => ENDPOINTS,
            generateTTCemu => generateTTCemu,
            AUTOMATIC_CLOCK_SWITCH => AUTOMATIC_CLOCK_SWITCH,
            FIRMWARE_MODE => FIRMWARE_MODE,
            USE_Si5324_RefCLK => USE_Si5324_RefCLK,
            GENERATE_XOFF => GENERATE_XOFF,
            IncludeDecodingEpath2_HDLC => IncludeDecodingEpath2_HDLC,
            IncludeDecodingEpath2_8b10b => IncludeDecodingEpath2_8b10b,
            IncludeDecodingEpath4_8b10b => IncludeDecodingEpath4_8b10b,
            IncludeDecodingEpath8_8b10b => IncludeDecodingEpath8_8b10b,
            IncludeDecodingEpath16_8b10b => IncludeDecodingEpath16_8b10b,
            IncludeDecodingEpath32_8b10b => IncludeDecodingEpath32_8b10b,
            IncludeDirectDecoding => IncludeDirectDecoding,
            IncludeEncodingEpath2_HDLC      => IncludeEncodingEpath2_HDLC,
            IncludeEncodingEpath2_8b10b     => IncludeEncodingEpath2_8b10b,
            IncludeEncodingEpath4_8b10b     => IncludeEncodingEpath4_8b10b,
            IncludeEncodingEpath8_8b10b     => IncludeEncodingEpath8_8b10b,
            IncludeDirectEncoding => IncludeDirectEncoding,
            BLOCKSIZE => BLOCKSIZE,
            DATA_WIDTH => DATA_WIDTH,
            FULL_HALFRATE => FULL_HALFRATE,
            SUPPORT_HDLC_DELAY => SUPPORT_HDLC_DELAY,
            TTC_SYS_SEL => TTC_SYS_SEL
        )
        port map(
            MMCM_Locked_in => MMCM_Locked_out,
            MMCM_OscSelect_in => MMCM_OscSelect_out,
            SCL => SCL,
            SDA => SDA,
            SI5345_A => SI5345_A,
            SI5345_INSEL => SI5345_INSEL,
            SI5345_OE => SI5345_OE,
            SI5345_RSTN => SI5345_RSTN,
            SI5345_SEL => SI5345_SEL,
            SI5345_nLOL => SI5345_nLOL,
            SI5345_FINC_B => open,
            SI5345_FDEC_B => open,
            SI5345_INTR_B => (others => '0'),
            appreg_clk => global_appreg_clk,
            emcclk => emcclk,
            flash_SEL => flash_SEL,
            flash_a => flash_a,
            flash_a_msb => flash_a_msb,
            flash_adv => flash_adv,
            flash_cclk => flash_cclk,
            flash_ce => flash_ce,
            flash_d => flash_d,
            flash_re => flash_re,
            flash_we => flash_we,
            i2cmux_rst => i2cmux_rst,
            TACH => TACH,
            FAN_FAIL_B => (others => '0'),
            FAN_FULLSP => (others => '0'),
            FAN_OT_B => (others => '0'),
            FAN_PWM => open,
            FF3_PRSNT_B => (others => '0'),
            IOEXPAN_INTR_B => (others => '0'),
            IOEXPAN_RST_B => open,
            clk10_xtal => clk10_xtal,
            clk40_xtal => clk40_xtal,
            leds => leds7,
            opto_inhibit => opto_inhibit,
            register_map_control => global_register_map_control_appreg_clk,
            register_map_gen_board_info => register_map_gen_board_info,
            register_map_hk_monitor => register_map_hk_monitor,
            rst_soft => global_reset_soft_appreg_clk,
            sys_reset_n => sys_reset_n,
            PCIE_PWRBRK => (others => '0'),
            PCIE_WAKE_B => (others => '0'),
            QSPI_RST_B => open,
            rst_hw => rst_hw,
            CLK40_FPGA2LMK_P => open,--CLK40_FPGA2LMK_P,
            CLK40_FPGA2LMK_N => open,--CLK40_FPGA2LMK_N,
            LMK_DATA => open,--LMK_DATA,
            LMK_CLK => open,--LMK_CLK,
            LMK_LE => open,--LMK_LE,
            LMK_GOE => open,--LMK_GOE,
            LMK_LD => (others => '0'),--LMK_LD,
            LMK_SYNCn => open,--LMK_SYNCn,
            LMK_locked_out => open,
            I2C_SMB => I2C_SMB,
            I2C_SMBUS_CFG_nEN => I2C_SMBUS_CFG_nEN,
            MGMT_PORT_EN => MGMT_PORT_EN,
            PCIE_PERSTn_out => PCIE_PERSTn_out,
            PEX_PERSTn => PEX_PERSTn,
            PEX_SCL => PEX_SCL,
            PEX_SDA => PEX_SDA,
            PORT_GOOD => PORT_GOOD,
            SHPC_INT => SHPC_INT,
            lnk_up => lnk_up,
            select_bifurcation => select_bifurcation,
            RXUSRCLK_IN => RXUSRCLK,
            Versal_network_device_fromHost_full_o => open,
            Versal_network_device_fromHost_prog_full_o => open,
            Versal_network_device_toHost_empty_o => open,
            Versal_network_device_toHost_prog_empty_o => open,
            Versal_network_device_tohost_dout_o => open,
            Versal_network_device_fromHost_din_i => (others => '0'),
            Versal_network_device_fromHost_wr_en_i => '0',
            Versal_network_device_toHost_rd_en_i => '0',
            Versal_network_device_toHost_eop_o => open,
            Versal_network_device_fromHost_eop_i => '0',
            Versal_network_device_fromHost_set_carrier_i => '0',
            Versal_network_device_toHost_status_carrier_o => open,
            versal_sys_reset_n_out => open,
            WupperToCPM => WupperToCPM,
            CPMToWupper => CPMToWupper,
            clk100_out => open,
            DDR_in => (others => (others => "0")),
            DDR_out => open,
            DDR_inout => DDR_inout,
            axi_miso_ttc_lti => axi_miso_ttc_lti,
            axi_mosi_ttc_lti => open,
            apb3_axi_clk => open,
            LTI2cips_gpio => (others => '0'),
            cips2LTI_gpio => open,

            LPDDR_in => (others => (others => "0")),
            LPDDR_out => open,
            LPDDR_inout => LPDDR_inout
        );

    leds <= leds7(NUM_LEDS(CARD_TYPE)-1 downto 0);
end architecture structure ; -- of felig_top_bnl712


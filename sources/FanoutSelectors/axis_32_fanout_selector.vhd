--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.





library ieee, UNISIM;
    use ieee.numeric_std.all;
    use UNISIM.VCOMPONENTS.all;
    use ieee.std_logic_1164.all;
    use work.pcie_package.all;
    use work.axi_stream_package.all;
    use work.FELIX_package.all;

library xpm;
    use xpm.vcomponents.all;

entity axis_32_fanout_selector is
    generic(
        GBT_NUM                 : integer := 4;
        STREAMS_TOHOST          : integer := 1);
    port (
        aclk                       : in std_logic;
        emu_axis                   : in axis_32_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1);
        emu_axis_tready            : out axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1);
        emu_axis_prog_empty        : in axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1);
        emu_FE_BUSY_in             : in  array_57b(0 to GBT_NUM-1);
        decoding_axis              : in axis_32_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1);
        decoding_axis_tready       : out axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1);
        decoding_axis_prog_empty   : in axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1);
        decoding_FE_BUSY_in        : in  array_57b(0 to GBT_NUM-1);
        fanout_sel_axis            : out axis_32_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1);
        fanout_sel_axis_tready     : in axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1);
        fanout_sel_axis_prog_empty : out axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1);
        fanout_sel_FE_BUSY_out     : out array_57b(0 to GBT_NUM-1);
        register_map_control       : in register_map_control_type);
end entity axis_32_fanout_selector;



architecture rtl of axis_32_fanout_selector is

    signal fosel_aclk: std_logic_vector(GBT_NUM-1 downto 0);
    signal fanout_sel_axis_s: axis_32_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1);
begin
    sync_fosel: xpm_cdc_array_single
        generic map (
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0,
            WIDTH => GBT_NUM
        )
        port map (
            src_clk => '0',
            src_in => register_map_control.GBT_TOHOST_FANOUT.SEL(GBT_NUM-1 downto 0),
            dest_clk => aclk,
            dest_out => fosel_aclk(GBT_NUM-1 downto 0)
        );

    decoding_axis_tready <= fanout_sel_axis_tready;
    emu_axis_tready <= fanout_sel_axis_tready;

    fanout: process(fosel_aclk, emu_axis, emu_axis_prog_empty, decoding_axis, decoding_axis_prog_empty, decoding_FE_BUSY_in, emu_FE_BUSY_in, register_map_control.GBT_TOHOST_FANOUT.SEL)
    begin
        for i in 0 to GBT_NUM-1 loop
            if register_map_control.GBT_TOHOST_FANOUT.SEL(i) = '1' then
                fanout_sel_FE_BUSY_out(i) <= emu_FE_BUSY_in(i);
            else
                fanout_sel_FE_BUSY_out(i) <= decoding_FE_BUSY_in(i);
            end if;
            for j in 0 to STREAMS_TOHOST-1 loop
                if fosel_aclk(i) = '1' then --use emulator data
                    fanout_sel_axis_s(i,j) <= emu_axis(i,j);
                    fanout_sel_axis_prog_empty(i,j) <= emu_axis_prog_empty(i,j);
                else          --use frontend link data
                    fanout_sel_axis_s(i,j) <= decoding_axis(i,j);
                    fanout_sel_axis_prog_empty(i,j) <= decoding_axis_prog_empty(i,j);
                end if;
            end loop;
        end loop;
    end process;

    tvalid_fanout: process(fanout_sel_axis_s)
    begin
        for i in 0 to GBT_NUM-1 loop
            for j in 0 to STREAMS_TOHOST-1 loop
                fanout_sel_axis(i,j) <= fanout_sel_axis_s(i,j);
            end loop;
        end loop;
    end process;

end architecture rtl ; -- of fromfrontend_fanout_selector_FM


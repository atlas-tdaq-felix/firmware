--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Kai Chen
--!               Andrea Borga
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen    (kchen@bnl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2017/05/19 04:43:14 PM
-- Design Name: FELIX BNL-711 FLASH IP CORE
-- Module Name: flash_ipcore_bnl - Behavioral
-- Project Name:
-- Target Devices: KCU
-- Tool Versions: Vivado
-- Description:
--              This module is to read/write/erase P30 FLASH
--              Fast programming mode is supported
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std_unsigned.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
    use UNISIM.VComponents.all;

entity flash_ipcore_bnl is
    generic (
        ADDR_WIDTH : integer := 27
    );
    Port (
        clk_i             : in std_logic;
        rst_i             : in std_logic;
        --init_i            : in std_logic;
        readstatus_i      : in std_logic;
        clearstatus_i     : in std_logic;
        eraseblock_i      : in std_logic;
        unlockblock_i     : in std_logic;
        write_word_i      : in std_logic;
        special_write_i   : in std_logic;
        read_i            : in std_logic;
        readdevid_i       : in std_logic;
        cfiquery_i        : in std_logic;
        special_rdstatus_i: in std_logic;

        flash_data_rd_o   : out std_logic_vector(15 downto 0);
        flash_data_wr_i   : in std_logic_vector(15 downto 0);
        flash_d_debug     : out std_logic_vector(15 downto 0);
        flash_addr_cmd_i  : in std_logic_vector(ADDR_WIDTH-1 downto 0);
        ipcore_op_done_o  : out std_logic;
        ipcore_busy_o     : out std_logic;

        flash_dq_io       : inout std_logic_vector(15 downto 0);
        flash_addr_o      : out std_logic_vector(ADDR_WIDTH-1 downto 0);
        flash_adv_n_o     : out std_logic;
        flash_ce_n_o      : out std_logic;
        flash_clk_o       : out std_logic;
        flash_re_n_o      : out std_logic;
        --flash_rst_n_o     : out std_logic;
        --flash_wait_i      : in std_logic;
        flash_we_n_o      : out std_logic;
        flash_wp_n_o      : out std_logic
    );
end flash_ipcore_bnl;

architecture Behavioral of flash_ipcore_bnl is


    type fsmtype            is (IDLE, WRITE1, WRITE2, READ1, OP_END, RD_STATUS_SPECIAL);
    signal bus_status       : fsmtype:=IDLE;


    signal dir              : std_logic:='0';
    signal ipcore_busy      : std_logic:='0';
    signal ipcore_op_done   : std_logic:='0';
    signal WRITE_A          : std_logic_vector(15 downto 0);
    signal WRITE_B          : std_logic_vector(15 downto 0);
    signal flash_data_out   : std_logic_vector(15 downto 0);
    --signal flash_data_rd_o_i : std_logic_vector(15 downto 0);
    signal cmd_type         : std_logic_vector(1 downto 0):="00";
    signal counter          : std_logic_vector(2 downto 0):="000";

begin

    flash_clk_o             <= '1';
    flash_wp_n_o            <= '1';
    flash_adv_n_o           <= '0';

    ipcore_busy_o           <= ipcore_busy;
    ipcore_op_done_o        <= ipcore_op_done;

    process(clk_i)
    begin
        if clk_i'event and clk_i='1' then
            if rst_i = '1' then
                ipcore_busy     <= '0';
                ipcore_op_done  <= '0';
                flash_ce_n_o    <= '1';
                flash_re_n_o    <= '1';
                flash_we_n_o    <= '1';
                dir             <= '0';
                bus_status      <= IDLE;
                WRITE_A         <= x"0000";
                WRITE_B         <= x"0000";
                cmd_type        <= "00";
                counter         <= "000";
                flash_data_out  <= x"0000";
                flash_data_rd_o <= x"0000";
            else
                case bus_status is
                    when IDLE =>
                        counter             <= "000";
                        flash_ce_n_o        <= '0';
                        if read_i = '1' or readstatus_i = '1' or readdevid_i = '1' or cfiquery_i = '1' then
                            bus_status        <= WRITE1;
                            cmd_type          <= "00";
                            ipcore_busy       <= '1';
                            dir <= '1';
                            if read_i = '1' then
                                WRITE_A         <= x"00FF";
                            elsif readstatus_i = '1' then
                                WRITE_A         <= x"0070";
                            elsif readdevid_i = '1' then
                                WRITE_A         <= x"0090";
                            else
                                WRITE_A         <= x"0098";       -- cfiquery
                            end if;
                        elsif unlockblock_i = '1' or write_word_i = '1' or eraseblock_i = '1' then
                            bus_status        <= WRITE1;
                            cmd_type          <= "01";
                            ipcore_busy       <= '1';
                            dir               <= '1';
                            if unlockblock_i = '1'  then
                                WRITE_A         <= x"0060";
                                WRITE_B         <= x"00D0";
                            elsif write_word_i = '1' then
                                WRITE_A         <= x"0040";
                                WRITE_B         <= flash_data_wr_i;
                            else
                                WRITE_A         <= x"0020";
                                WRITE_B         <= x"00D0";
                            end if;
                        elsif clearstatus_i = '1' or special_write_i='1' then
                            cmd_type          <= "10";
                            bus_status        <= WRITE1;
                            ipcore_busy       <= '1';
                            if clearstatus_i='1' then
                                WRITE_A         <= x"0050";
                            else
                                WRITE_A         <= flash_data_wr_i; --0xEA, RDSTATUS, then counter-1, then datas at
                            end if;
                            dir               <= '1';
                        elsif special_rdstatus_i = '1' then
                            cmd_type          <= "11";
                            ipcore_busy       <= '1';
                            dir               <= '0';
                            bus_status        <= RD_STATUS_SPECIAL;
                        else
                            bus_status        <= IDLE;
                            dir               <= '1';
                        end if;
                    when WRITE1 =>
                        counter             <= counter + '1';
                        dir                 <= '1';
                        case counter is
                            when "000" => flash_data_out      <= WRITE_A;
                            when "001" => flash_we_n_o        <= '0';
                            when "101" => flash_we_n_o        <= '1';
                            when "111" =>
                                case cmd_type is
                                    when "00" => bus_status       <= READ1;
                                    when "01" => bus_status       <= WRITE2;
                                        flash_data_out   <= WRITE_B;
                                    when "10" => bus_status       <= OP_END;
                                        ipcore_op_done   <= '1';
                                    when others => bus_status     <= OP_END;
                                        ipcore_op_done   <='1';
                                end case;
                            when others => null;
                        end case;
                    when READ1 =>
                        dir         <= '0';
                        counter     <= counter + '1';
                        case counter is
                            when "000" => flash_re_n_o        <= '0';
                            when "110" => flash_re_n_o        <= '1';
                            when "111" => ipcore_op_done      <= '1';
                                bus_status          <= OP_END;
                            when others => null;
                        end case;
                    when WRITE2 =>
                        dir         <= '1';
                        counter     <= counter + '1';
                        case counter is
                            when "000" => flash_we_n_o        <= '0';
                            when "100" => flash_we_n_o        <= '1';
                            when "101" => bus_status          <= OP_END;
                                ipcore_op_done      <= '1';
                            when others => null;
                        end case;
                    when RD_STATUS_SPECIAL =>
                        dir         <= '0';
                        counter     <= counter + '1';
                        case counter is
                            when "000" => flash_re_n_o        <= '0';
                            when "101" => flash_re_n_o        <= '1';
                            when "111" => bus_status          <= OP_END;
                                ipcore_op_done      <= '1';
                            when others => null;
                        end case;
                    when OP_END =>
                        dir         <= '0';
                        if cmd_type="00" or cmd_type="11" then
                            flash_data_rd_o           <= flash_dq_io;
                        end if;
                        counter             <= "000";
                        ipcore_op_done      <= '0';
                        ipcore_busy         <= '0';
                        bus_status          <= IDLE;
                    when others => -- @suppress "Case statement contains all choices explicitly. You can safely remove the redundant 'others'"
                        bus_status          <= IDLE;
                end case;
            end if;

        end if;
    end process;

    --flash_data_rd_o     <= flash_data_rd_o_i;
    flash_dq_io           <= flash_data_out when dir = '1' else
                             (others=>'Z');
    --flash_data_rd_o_i   <= flash_dq_io when dir='0' else flash_data_rd_o_i;
    flash_d_debug         <= flash_data_out;
    flash_addr_o          <= flash_addr_cmd_i;

end Behavioral;

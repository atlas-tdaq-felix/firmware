--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 2016/03/08 01:20:07 AM
-- Design Name:
-- Module Name:  - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.STD_LOGIC_ARITH.ALL;
    USE IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
    use UNISIM.VComponents.all;

library WORK;
    use work.pcie_package.all;


entity flash_wrapper is
    Port (
        --system reset
        rst : in std_logic;

        -- EMC clock
        clk : in std_logic;

        -- flash port
        flash_a_msb          : inout  std_logic_vector(1 downto 0);
        flash_a : out std_logic_vector(24 downto 0);
        flash_d : inout std_logic_vector(15 downto 0);
        flash_adv : out std_logic;
        flash_cclk : out std_logic;
        flash_ce : out std_logic;
        flash_we : out std_logic;
        flash_re : out std_logic;
        register_map_control: in register_map_control_type;
        config_flash_rd: out bitfield_config_flash_rd_r_type;

        flash_SEL : out std_logic
    );
end ;

architecture stub of flash_wrapper is



begin
    flash_a_msb <= (others => 'Z');
    flash_a <= (others => '0');
    flash_d <= (others => 'Z');
    flash_adv <= '0';
    flash_cclk <= '0';
    flash_ce <= '0';
    flash_we <= '0';
    flash_re <= '0';
    config_flash_rd <= (others => (others => '0'));
    flash_SEL <= '0';

end stub;

--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  Weizmann Institute of Science
--! Engineer: juna
--!
--! Create Date:    18/12/2014
--! Module Name:    pulse_fall_pw01
----------------------------------------------------------------------------------
--! Use standard library
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

--! generates a one-clk-pulse one clk after trigger's falling edge
entity pulse_fall_pw01 is
    Port (
        clk         : in   std_logic;
        trigger     : in   std_logic;
        pulseout    : out  std_logic
    );
end pulse_fall_pw01;

architecture behavioral of pulse_fall_pw01 is

    ------
    signal trigger_1clk_delayed, t0 : std_logic;
------

begin

    process (clk)
    begin
        if clk'event and clk = '1' then
            trigger_1clk_delayed <= trigger;
        end if;
    end process;
    --
    t0 <= (not trigger) and trigger_1clk_delayed; -- the first clk after trigger fall
    --
    pulseout <= t0;
--

end behavioral;


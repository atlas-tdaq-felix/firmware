
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Andrea Borga
#               Julia Narevicius
#               Frans Schreuder
#               RHabraken
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#
#	File import script for the FELIX hdl project
#  Modified for FMEmu_top
#

#Script Configuration
set proj_name fmemu_top
# Set the supportfiles directory path
set scriptdir [pwd]
set firmware_dir $scriptdir/../../
# Vivado project directory:
set project_dir $firmware_dir/Projects/$proj_name

#Close currently open project and create a new one. (OVERWRITES PROJECT!!)
close_project -quiet
set PART xc7vx690tffg1761-2
create_project -force -part xc7vx690tffg1761-2 $proj_name $firmware_dir/Projects/$proj_name

set_property target_language VHDL [current_project]
set_property default_lib work [current_project]

# ----------------------------------------------------------
# FELIX top module
# ----------------------------------------------------------
read_vhdl -library work $firmware_dir/sources/FullModeEmulator/FMEmu_top.vhd

# ----------------------------------------------------------
# packages
# ----------------------------------------------------------
read_vhdl -library work $firmware_dir/sources/templates/pcie_package.vhd
read_vhdl -library work $firmware_dir/sources/packages/FELIX_gbt_package.vhd
read_vhdl -library work $firmware_dir/sources/packages/centralRouter_package.vhd

# -- I2C master
read_vhdl -library work $firmware_dir/sources/i2c_master/i2c.vhd
read_vhdl -library work $firmware_dir/sources/housekeeping/si5324_init.vhd

#FM EMU files
read_vhdl -library work $firmware_dir/sources/felixUserSupport/gth_vc709/fullmodetransceiver_gth.vhd
read_vhdl -library work $firmware_dir/sources/felixUserSupport/gtx_vc707/fullmodetransceiver_gtx.vhd
read_vhdl -library work $firmware_dir/sources/FullModeEmulator/FIFO34to34b.vhd
read_vhdl -library work $firmware_dir/sources/FullModeEmulator/FIFOfromHost_256to32.vhd
read_vhdl -library work $firmware_dir/sources/FullModeEmulator/FIFOtoHost_32to256.vhd
read_vhdl -library work $firmware_dir/sources/FullModeEmulator/OUTPUTctrl.vhd
read_vhdl -library work $firmware_dir/sources/FullModeEmulator/FMemuRAM.vhd
read_vhdl -library work $firmware_dir/sources/CRC32/CRC32_v2.vhd

read_vhdl -library work $firmware_dir/sources/FullModeTransmitter/FMchannelTXctrl.vhd
read_vhdl -library work $firmware_dir/sources/centralRouter/pulse_pdxx_pwxx.vhd

#Housekeeping
read_vhdl -library work $firmware_dir/sources/felixUserSupport/FM_example_clocking.vhd
read_vhdl -library work $firmware_dir/sources/housekeeping/GenericConstantsToRegs.vhd

# ----------------------------------------------------------
# dma sources
# ----------------------------------------------------------
#read_vhdl -library work $firmware_dir/sources/pcie/dma_control.vhd
read_vhdl -library work $firmware_dir/sources/templates/dma_control.vhd

read_vhdl -library work $firmware_dir/sources/pcie/pcie_clocking.vhd
read_vhdl -library work $firmware_dir/sources/pcie/pcie_slow_clock.vhd
read_vhdl -library work $firmware_dir/sources/pcie/dma_read_write.vhd
read_vhdl -library work $firmware_dir/sources/pcie/intr_ctrl.vhd
read_vhdl -library work $firmware_dir/sources/pcie/wupper_core.vhd
read_vhdl -library work $firmware_dir/sources/pcie/pcie_ep_wrap.vhd
read_vhdl -library work $firmware_dir/sources/pcie/wupper.vhd
read_vhdl -library work $firmware_dir/sources/pcie/pcie_init.vhd
read_vhdl -library work $firmware_dir/sources/pcie/register_map_sync.vhd

#stub files for cores only used in UltraScale devices
read_vhdl -library work $firmware_dir/sources/ip_cores/virtex7/pcie3_ultrascale_7038_stub.vhdl
read_vhdl -library work $firmware_dir/sources/ip_cores/virtex7/pcie3_ultrascale_7039_stub.vhdl

import_ip $firmware_dir/sources/ip_cores/virtex7/pcie_x8_gen3_3_0.xci
import_ip $firmware_dir/sources/ip_cores/virtex7/clk_wiz_regmap.xci
import_ip $firmware_dir/sources/ip_cores/virtex7/clk_wiz_156_1.xci
import_ip $firmware_dir/sources/ip_cores/virtex7/fifo1KB_34bit.xci
import_ip $firmware_dir/sources/ip_cores/virtex7/fifo4KB_256bit_to_32bit.xci
import_ip $firmware_dir/sources/ip_cores/virtex7/fifo128KB_32bit_to_256bit.xci
import_ip $firmware_dir/sources/ip_cores/virtex7/DPram_32b.xci

# ----------------------------------------------------------
# XDC constraints files
# ----------------------------------------------------------
read_xdc -verbose $firmware_dir/constraints/felix_top_HTG710.xdc
read_xdc -verbose $firmware_dir/constraints/timing_constraints_fmemu_v7.xdc
read_xdc -verbose $firmware_dir/constraints/FMEmu_top_VC709.xdc

set_property is_enabled false [get_files  $firmware_dir/constraints/felix_top_HTG710.xdc]

close [ open $firmware_dir/constraints/felix_probes.xdc w ]
read_xdc -verbose $firmware_dir/constraints/felix_probes.xdc
set_property target_constrs_file $firmware_dir/constraints/felix_probes.xdc [current_fileset -constrset]

set_property top FMEmu_top [current_fileset]
#set_property STEPS.SYNTH_DESIGN.ARGS.FLATTEN_HIERARCHY none [get_runs synth_1]

puts "INFO: Done!"

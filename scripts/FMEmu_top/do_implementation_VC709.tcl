
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Andrea Borga
#               Soo Ryu
#               Frans Schreuder
#               RHabraken
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

source ../helper/do_implementation_pre.tcl
## General settings
# -- set to true in order to generate the GBT links
set NUMBER_OF_INTERRUPTS 8
set CARD_TYPE 709

## Constraints files
#set_property is_enabled false [get_files $HDLDIR/constraints/felix_gbt_cxp_HTG710.xdc]
#set_property is_enabled false [get_files $HDLDIR/constraints/felix_gbt_cxp_copper_HTG710.xdc]
#set_property is_enabled false [get_files $HDLDIR/constraints/felix_top_HTG710.xdc]
set_property is_enabled true [get_files  $HDLDIR/constraints/timing_constraints_fmemu_v7.xdc]
set_property is_enabled true [get_files  $HDLDIR/constraints/FMEmu_top_VC709.xdc]
#set_property is_enabled true [get_files  $HDLDIR/constraints/felix_top_VC709.xdc]


#source ../helper/do_implementation_fullmode_post.tcl
## Optimize place and route algorithm
set_property strategy Performance_ExplorePostRoutePhysOpt $IMPL_RUN
set_property STEPS.OPT_DESIGN.ARGS.DIRECTIVE ExploreWithRemap $IMPL_RUN
set_property STEPS.PLACE_DESIGN.ARGS.DIRECTIVE SpreadLogic_high $IMPL_RUN
set_property STEPS.PHYS_OPT_DESIGN.ARGS.DIRECTIVE AggressiveFanoutOpt $IMPL_RUN
set_property STEPS.ROUTE_DESIGN.ARGS.DIRECTIVE HigherDelayCost $IMPL_RUN
set_property STEPS.POST_ROUTE_PHYS_OPT_DESIGN.ARGS.DIRECTIVE AddRetime $IMPL_RUN

set_property generic "
BUILD_DATETIME=$build_date \
SVN_VERSION=$svn_version \
COMMIT_DATETIME=$COMMIT_DATETIME \           
GIT_HASH=$GIT_HASH \
REG_MAP_VERSION=$REG_MAP_VERSION \
NUMBER_OF_INTERRUPTS=$NUMBER_OF_INTERRUPTS \
NUMBER_OF_DESCRIPTORS=$NUMBER_OF_DESCRIPTORS \
CARD_TYPE=$CARD_TYPE \
" [current_fileset]

launch_runs $SYNTH_RUN  -jobs 8
#wait_on_run $SYNTH_RUN
#open_run $SYNTH_RUN
#return
launch_runs $IMPL_RUN -jobs 8
#launch_runs $IMPL_RUN  -to_step write_bitstream
#cd $HDLDIR/Synt/
wait_on_run $IMPL_RUN
set TIMESTAMP [clock format $systemTime -format {%y%m%d_%H_%M}]

open_run $IMPL_RUN
current_run $IMPL_RUN

# force Vivado to ignore usage of GTGREFCLK
set_property SEVERITY {Warning} [get_drc_checks REQP-44]
set_property SEVERITY {Warning} [get_drc_checks REQP-46]


set FileName FLX${CARD_TYPE}_FM_DATA_EMU_RM${REG_MAP_VERSION_STR}_SVN${svn_version}_${TIMESTAMP}

write_bitstream -force $HDLDIR/output/${FileName}.bit

cd $HDLDIR/output/

set pass [expr {[get_property SLACK [get_timing_paths -delay_type min_max]] >= 0}]
set report [report_timing -slack_lesser_than 0 -return_string -nworst 10]
set slack [get_property SLACK [get_timing_paths -delay_type min_max]]
set GenericFileData "
BUILD_DATETIME:               $build_date 
SVN_VERSION:                  $svn_version 
REG_MAP_VERSION:              $REG_MAP_VERSION 
NUMBER_OF_INTERRUPTS:         $NUMBER_OF_INTERRUPTS 
NUMBER_OF_DESCRIPTORS:        $NUMBER_OF_DESCRIPTORS
CARD_TYPE:                    $CARD_TYPE 
Timing met:                   $pass
Wost slack:                   $slack\n\n
Timing Report:\n
$report"


set GenericsFileName "${FileName}_generics_timing.txt"
set GenericsFileId [open $GenericsFileName "w"]
puts -nonewline $GenericsFileId $GenericFileData
close $GenericsFileId

set BitFile ${FileName}.bit
set IMPL_DIR [get_property DIRECTORY [current_run]]

write_cfgmem -force -format MCS -size 128 -interface BPIx16 -loadbit "up 0x00000000 ${FileName}.bit" ${FileName}.mcs
if {[file exists $IMPL_DIR/debug_nets.ltx] == 1} {
   file copy -force $IMPL_DIR/debug_nets.ltx ${FileName}_debug_nets.ltx
   
   if { [catch { exec tar -zcf ${HDLDIR}/output/${FileName}.tar.gz ${FileName}.bit ${FileName}.mcs ${FileName}_debug_nets.ltx ${GenericsFileName}} msg] } {
        puts "error creating archive ${FileName}.tar.gz"
   }
} else {
    if { [catch { exec tar -zcf ${HDLDIR}/output/${FileName}.tar.gz ${FileName}.bit ${FileName}.mcs ${GenericsFileName}} msg] } {
        puts "error creating archive ${FileName}.tar.gz"
    }
}
cd $scriptdir

source ../helper/do_implementation_pre.tcl

set GENERATE_FM_WRAP true
set GBT_NUM 24
set OPTO_TRX 4
set CARD_TYPE 181
set app_clk_freq 200
set ENDPOINTS 2
set NUM_LEDS 4
set DATA_WIDTH 512

set GTREFCLKS 6

# 0: gbt mode
set FIRMWARE_MODE $FIRMWARE_MODE_GBT

#The only function of the variable SemiStatic is to add an entry to the filename
set SemiStatic true
# Defining the Egroup's capabilities
# ToHost Egroups 6..0 (only 4..0 for GBT mode, 
# Epath16 and Epath32 are only for lpGBT (not GBT)
# Epath2 and Epath4 are only for GBT (not lpGBT)
set IncludeDecodingEpath2_HDLC   7'b0000011 
set IncludeDecodingEpath2_8b10b  7'b0000010
set IncludeDecodingEpath4_8b10b  7'b0001100
set IncludeDecodingEpath8_8b10b  7'b0011111
set IncludeDecodingEpath16_8b10b 7'b0000000
set IncludeDecodingEpath32_8b10b 7'b0000000

set IncludeEncodingEpath2_HDLC   5'b00111 
set IncludeEncodingEpath2_8b10b  5'b00010
set IncludeEncodingEpath4_8b10b  5'b00000
set IncludeEncodingEpath8_8b10b  5'b10000


source ../helper/do_implementation_post.tcl

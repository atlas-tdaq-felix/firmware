
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Andrea Borga
#               Kai Chen
#               Israel Grayzman
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

source ../helper/do_implementation_pre.tcl
#Uncomment in order to stop after synthesis, so ILA probes can be added.
#set STOP_TO_ADD_ILA 1
set GBT_NUM 48
set OPTO_TRX 4
set CARD_TYPE 712
set app_clk_freq 200
set ENDPOINTS 2
#1 descriptor ToHost, 1 FromHost.
set NUMBER_OF_DESCRIPTORS 2

set GTREFCLKS 5

# Defining the Egroup's capabilities
# ToHost Egroups 6..0 (only 4..0 for GBT mode, 
# Epath16 and Epath32 are only for lpGBT (not GBT)
# Epath2 and Epath4 are only for GBT (not lpGBT)
set IncludeDecodingEpath2_HDLC   7'b0000000 
set IncludeDecodingEpath2_8b10b  7'b0000000
set IncludeDecodingEpath4_8b10b  7'b0000000
set IncludeDecodingEpath8_8b10b  7'b0000000
set IncludeDecodingEpath16_8b10b 7'b0000000
set IncludeDecodingEpath32_8b10b 7'b0000000

set IncludeEncodingEpath2_HDLC   5'b00000 
set IncludeEncodingEpath2_8b10b  5'b00000
set IncludeEncodingEpath4_8b10b  5'b00000
set IncludeEncodingEpath8_8b10b  5'b00000

set PLL_SEL $QPLL

#determine the FIRMWARE_MODE register value
# 0: GBT mode                                       
# 1: FULL mode                                      
# 2: LTDB mode (GBT mode with only IC and TTC links)
# 3: FE-I4B mode
set FIRMWARE_MODE $FIRMWARE_MODE_LTDB

source ../helper/do_implementation_post.tcl


# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Andrea Borga
#               Soo Ryu
#               RHabraken
#               Israel Grayzman
#               Ohad Shaked
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

source ../helper/do_implementation_pre.tcl

#set STOP_TO_ADD_ILA to 1 in order to stop after synthesis phase and add an ILA
set STOP_TO_ADD_ILA 0

set GBT_NUM 4
set OPTO_TRX 4
set CARD_TYPE 709
set app_clk_freq 156
set ENDPOINTS 1
set GTREFCLKS 1

set NUMBER_OF_DESCRIPTORS 2

set PLL_SEL $QPLL

#set to false to use TTCfx3 (Si5345) refclk, true for onboard Si5324.
set USE_Si5324_RefCLK true

#Tell elinkconfig that only 4 and 8-bit decoding is allowed
set IncludeDecodingEpath2_HDLC   7'b0000000
set IncludeDecodingEpath2_8b10b  7'b0000000
set IncludeDecodingEpath4_8b10b  7'b0000000
set IncludeDecodingEpath8_8b10b  7'b1111111
set IncludeDecodingEpath16_8b10b 7'b1111111
set IncludeDecodingEpath32_8b10b 7'b0000000

#determine the FIRMWARE_MODE register value
# 0: GBT mode
# 1: FULL mode                                      
# 2: LTDB mode (GBT mode with only IC and TTC links)
# 3: FE-I4B mode
set FIRMWARE_MODE $FIRMWARE_MODE_STRIP

source ../helper/do_implementation_post.tcl

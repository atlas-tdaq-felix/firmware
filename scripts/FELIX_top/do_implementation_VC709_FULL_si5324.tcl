
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Andrea Borga
#               William Wulff
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

source ../helper/do_implementation_pre.tcl

#set STOP_TO_ADD_ILA to 1 in order to stop after synthesis phase and add an ILA
set STOP_TO_ADD_ILA 0

set GENERATE_FM_WRAP true
set GBT_NUM 4
set OPTO_TRX 4
set CARD_TYPE 709
set app_clk_freq 156

set NUMBER_OF_DESCRIPTORS 2

#set to false to use TTCfx3 (Si5345) refclk, true for onboard Si5324.
set USE_Si5324_RefCLK true

# 0: GBT mode
# 1: full mode
set FIRMWARE_MODE $FIRMWARE_MODE_FULL

source ../helper/do_implementation_post.tcl


# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Andrea Borga
#               Kai Chen
#               Weihao Wu
#               RHabraken
#               Frans Schreuder
#               Israel Grayzman
#               Shelfali Saxena
#               mtrovato
#               Ricardo Luz
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#to be adapted to FELIG
source ../helper/do_implementation_pre.tcl

#set STOP_TO_ADD_ILA to 1 in order to stop after synthesis phase and add an ILA
set STOP_TO_ADD_ILA 0

set GBT_NUM 24
set OPTO_TRX 4
set CARD_TYPE 712
set app_clk_freq 200
set ENDPOINTS 2
set GTREFCLKS 5

set PLL_SEL $QPLL 

#determine the FIRMWARE_MODE register value
# 6: FELIG GBT
# 11: FELIG lpGBT
set FIRMWARE_MODE $FIRMWARE_MODE_FELIG_LPGBT

# Defining the Egroup's capabilities
if {$FIRMWARE_MODE == $FIRMWARE_MODE_FELIG_GBT} {
    set IncludeDecodingEpath2_HDLC   7'b0011111 
    set IncludeDecodingEpath2_8b10b  7'b0011111
    set IncludeDecodingEpath4_8b10b  7'b0011111
    set IncludeDecodingEpath8_8b10b  7'b0011111
    set IncludeDecodingEpath16_8b10b 7'b0000000
    set IncludeDecodingEpath32_8b10b 7'b0000000
#
    set IncludeEncodingEpath2_HDLC   5'b00000 
    set IncludeEncodingEpath2_8b10b  5'b00000
    set IncludeEncodingEpath4_8b10b  5'b00000
    set IncludeEncodingEpath8_8b10b  5'b00000
}
#look into
if {$FIRMWARE_MODE == $FIRMWARE_MODE_FELIG_LPGBT} {
    set IncludeDecodingEpath2_HDLC   7'b1111111
    set IncludeDecodingEpath2_8b10b  7'b0000000
    set IncludeDecodingEpath4_8b10b  7'b0000000
    set IncludeDecodingEpath8_8b10b  7'b1111111
    set IncludeDecodingEpath16_8b10b 7'b1111111
    set IncludeDecodingEpath32_8b10b 7'b1111111
#
    set IncludeEncodingEpath2_HDLC   5'b00000
    set IncludeEncodingEpath2_8b10b  5'b00000
    set IncludeEncodingEpath4_8b10b  5'b00000
    set IncludeEncodingEpath8_8b10b  5'b00000
}

source ../helper/do_implementation_post.tcl

source ../helper/do_implementation_pre.tcl

set GENERATE_FM_WRAP true
set GBT_NUM 4
set GTREFCLKS 1
set OPTO_TRX 4
set CARD_TYPE 182
set app_clk_freq 200
set ENDPOINTS 2
set NUM_LEDS 4
set DATA_WIDTH 512

#set STOP_TO_ADD_ILA to 1 in order to stop after synthesis phase and add an ILA
set STOP_TO_ADD_ILA 0

# 1: full mode
set FIRMWARE_MODE $FIRMWARE_MODE_FULL

set ENABLE_RQS false

#Use LTI as clock input
set TTC_SYS_SEL                  $LTITTC


source ../helper/do_implementation_post.tcl


# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               mtrovato
#               Frans Schreuder
#               Ricardo Luz
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#
#  File import script for the FELIX hdl Vivado project
#  Board: BNL712
#

source ../helper/clear_filesets.tcl

set PROJECT_NAME FLX712_FELIG
set BOARD_TYPE 712
set TOPLEVEL felig_top_bnl712
#set TOPLEVEL_SIM felig_sim_top_bnl712
set VIVADO_ADD_SIM_FILES false

#Import blocks for different filesets
#source ../filesets/fullmode_toplevel_fileset.tcl
#source ../filesets/crToHost_fileset.tcl
#source ../filesets/crFromHost_fileset.tcl
source ../filesets/decoding_fileset.tcl
source ../filesets/encoding_fileset.tcl
#source ../filesets/gbt_toplevel_fileset.tcl
source ../filesets/wupper_fileset.tcl
source ../filesets/gbt_core_fileset.tcl
#source ../filesets/fullmode_gbt_core_fileset.tcl
source ../filesets/lpgbt_fe_core_fileset.tcl
#import lpgbt_core_fileset.tcl for simulation purposes
#source ../filesets/lpgbt_core_fileset.tcl
source ../filesets/housekeeping_fileset.tcl
#source ../filesets/housekeeping_felig_fileset.tcl
source ../filesets/gbt_fanout_fileset.tcl
#source ../filesets/gbt_emulator_fileset.tcl
#source ../filesets/fullmode_fanout_fileset.tcl
#source ../filesets/fullmode_emulator_fileset.tcl
#source ../filesets/ttc_decoder_fileset.tcl
#source ../filesets/ttc_emulator_fileset.tcl
source ../filesets/endeavour_fileset.tcl
source ../filesets/itk_strips_fileset.tcl
source ../filesets/encrd53_fileset.tcl

source ../filesets/felig_fileset.tcl
source ../filesets/64b66b_fileset.tcl
source ../filesets/itk_strips_fileset.tcl
source ../filesets/core1990_interlaken_fileset.tcl
source ../filesets/bcm_fileset.tcl

#Actually execute all the filesets
source ../helper/vivado_import_generic.tcl

puts "INFO: Done!"

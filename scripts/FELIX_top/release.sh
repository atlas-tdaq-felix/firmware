#!/bin/bash
#source /eda/fpga/xilinx/Vivado/2024.1/settings64.sh
#vivado -mode batch -nojournal -nolog -notrace -source FLX709_FELIX_import_vivado.tcl
#vivado -mode batch -nojournal -nolog -notrace -source FLX712_FELIX_import_vivado.tcl
#vivado -mode batch -nojournal -nolog -notrace -source FLX182_FELIX_import_vivado.tcl




while :
do
    #FLX712 builds
    if [ -f gbt712 ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX712_FELIX/FLX712_FELIX.xpr -source do_implementation_BNL712_GBT_24ch_semistatic.tcl
    else
        echo gbt712
    fi
    if [ -f ltdb ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX712_FELIX/FLX712_FELIX.xpr -source do_implementation_BNL712_ltdb_48ch.tcl

    else
        echo skipping ltdb
    fi
    if [ -f full712 ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX712_FELIX/FLX712_FELIX.xpr -source do_implementation_BNL712_FULL_24ch.tcl 
    else
        echo skipping full712
    fi
    if [ -f pixel712 ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX712_FELIX/FLX712_FELIX.xpr -source do_implementation_BNL712_pixel_24ch.tcl 
    else
        echo skipping pixel712
    fi
    
    if [ -f strip712 ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX712_FELIX/FLX712_FELIX.xpr -source do_implementation_BNL712_strips_24ch.tcl 
    else
        echo skipping strip712
    fi
    
    if [ -f lpgbt712 ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX712_FELIX/FLX712_FELIX.xpr -source do_implementation_BNL712_LPGBT_24ch.tcl 
    else
        echo skipping lpgbt712
    fi
    
    if [ -f bcm712 ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX712_FELIX/FLX712_FELIX.xpr -source do_implementation_BNL712_bcm_24ch.tcl 
    else
        echo skipping bcm712
    fi
    
    #FLX709 builds
    if [ -f gbt709 ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX709_FELIX/FLX709_FELIX.xpr -source do_implementation_VC709_GBT.tcl
    else
        echo skipping gbt709
    fi
    if [ -f gbt709s ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX709_FELIX/FLX709_FELIX.xpr -source do_implementation_VC709_GBT_si5324.tcl
    else
        echo skipping gbt709s
    fi
    
    if [ -f full709 ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX709_FELIX/FLX709_FELIX.xpr -source do_implementation_VC709_FULL.tcl 
    else
        echo skipping full709
    fi
    if [ -f full709s ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX709_FELIX/FLX709_FELIX.xpr -source do_implementation_VC709_FULL_si5324.tcl 
    else
        echo skipping full709s
    fi
    
    if [ -f lpgbt709 ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX709_FELIX/FLX709_FELIX.xpr -source do_implementation_VC709_LPGBT.tcl 
    else
        echo skipping lpgbt709
    fi
    if [ -f lpgbt709s ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX709_FELIX/FLX709_FELIX.xpr -source do_implementation_VC709_LPGBT_si5324.tcl 
    else
        echo skipping lpgbt709s
    fi
    
    if [ -f pixel709 ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX709_FELIX/FLX709_FELIX.xpr -source do_implementation_VC709_pixel.tcl 
    else
        echo skipping pixel709
    fi
    if [ -f pixel709s ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX709_FELIX/FLX709_FELIX.xpr -source do_implementation_VC709_pixel_si5324.tcl 
    else
        echo skipping pixel709s
    fi
    
    if [ -f strip709 ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX709_FELIX/FLX709_FELIX.xpr -source do_implementation_VC709_strips.tcl 
    else
        echo skipping FULL for VC709
    fi
    if [ -f strip709s ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX709_FELIX/FLX709_FELIX.xpr -source do_implementation_VC709_strips_si5324.tcl 
    else
        echo skipping FULL for VC709 Si5324
    fi
    
    
    #FLX182 builds
    if [ -f interlaken182 ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX182_FELIX/FLX182_FELIX.xpr -source do_implementation_BNL182_INTERLAKEN_24ch_raw.tcl 
    else
        echo skipping interlaken182
    fi

    if [ -f full182 ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX182_FELIX/FLX182_FELIX.xpr -source do_implementation_BNL182_FULL_24ch.tcl 
    else
        echo skipping full182
    fi

    if [ -f gbt182 ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX182_FELIX/FLX182_FELIX.xpr -source do_implementation_BNL182_GBT_24ch_semistatic.tcl 
    else
        echo skipping gbt182
    fi

    if [ -f lpgbt182 ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX182_FELIX/FLX182_FELIX.xpr -source do_implementation_BNL182_LPGBT_24ch.tcl 
    else
        echo skipping lpgbt182
    fi

    if [ -f pixel182 ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX182_FELIX/FLX182_FELIX.xpr -source do_implementation_BNL182_pixel_24ch.tcl 
    else
        echo skipping pixel182
    fi
    
    if [ -f strip182 ]; then
        git reset --hard
        vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX182_FELIX/FLX182_FELIX.xpr -source do_implementation_BNL182_STRIP_20ch.tcl 
    else
        echo skipping strip182
    fi
    
        
done



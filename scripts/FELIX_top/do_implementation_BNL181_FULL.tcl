source ../helper/do_implementation_pre.tcl

set GENERATE_FM_WRAP true
set GBT_NUM 24
set GTREFCLKS 6
set OPTO_TRX 4
set CARD_TYPE 181
set app_clk_freq 200
set ENDPOINTS 2
set NUM_LEDS 4
set DATA_WIDTH 512

# 1: full mode
set FIRMWARE_MODE $FIRMWARE_MODE_FULL

source ../helper/do_implementation_post.tcl

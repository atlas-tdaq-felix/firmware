source ../helper/do_implementation_pre.tcl

set GBT_NUM 20
set OPTO_TRX 4
set CARD_TYPE 182
set app_clk_freq 200
set ENDPOINTS 2
set NUM_LEDS 4
set DATA_WIDTH 512

set GTREFCLKS 5

set FIRMWARE_MODE $FIRMWARE_MODE_STRIP

#The only function of the variable SemiStatic is to add an entry to the filename
set SemiStatic false
# Defining the Egroup's capabilities
# ToHost Egroups 6..0 (only 4..0 for GBT mode, 
# Epath16 and Epath32 are only for lpGBT (not GBT)
# Epath2 and Epath4 are only for GBT (not lpGBT)

#Some random values chosen for semistatic lpGBT configuration, TBD.
set IncludeDecodingEpath2_HDLC   7'b0000000 
set IncludeDecodingEpath2_8b10b  7'b0000000
set IncludeDecodingEpath4_8b10b  7'b0000000
set IncludeDecodingEpath8_8b10b  7'b1111111
set IncludeDecodingEpath16_8b10b 7'b1111111
set IncludeDecodingEpath32_8b10b 7'b0000000
set IncludeDirectDecoding        7'b1111111

set IncludeEncodingEpath2_HDLC   5'b00000 
set IncludeEncodingEpath2_8b10b  5'b00000
set IncludeEncodingEpath4_8b10b  5'b01111
set IncludeEncodingEpath8_8b10b  5'b00000

set ENABLE_RQS false

#Use LTI as clock input
set TTC_SYS_SEL                  $LTITTC

#Set to false to set BUILD_TIME to the clock, true for GIT_COMMIT_TIME. With false, the build is reproducible.
set DETERMINISTIC_BUILD_TIME false

source ../helper/do_implementation_post.tcl

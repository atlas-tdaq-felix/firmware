
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Elena Zhivun
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# This script will generate firmware version compatible with ITk Strips

source ../helper/do_implementation_pre.tcl

set GBT_NUM 12
set OPTO_TRX 4
set CARD_TYPE 712
set app_clk_freq 200
set ENDPOINTS 2
set GTREFCLKS 5

set FIRMWARE_MODE $FIRMWARE_MODE_STRIP

#Tell elinkconfig that only 4 and 8-bit decoding is allowed
set IncludeDecodingEpath2_HDLC   7'b0000000
set IncludeDecodingEpath2_8b10b  7'b0000000
set IncludeDecodingEpath4_8b10b  7'b0000000
set IncludeDecodingEpath8_8b10b  7'b1111111
set IncludeDecodingEpath16_8b10b 7'b1111111
set IncludeDecodingEpath32_8b10b 7'b0000000


set_msg_config -id "Vivado 12-508" -limit 5000
set_msg_config -id "Vivado 12-180" -limit 5000

source ../helper/do_implementation_post.tcl

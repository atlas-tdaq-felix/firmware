source ../helper/do_implementation_pre.tcl

set GENERATE_FM_WRAP true
set GBT_NUM 24
set GTREFCLKS 6
set OPTO_TRX 4
set CARD_TYPE 182
set app_clk_freq 200
set ENDPOINTS 2
set NUM_LEDS 4
set DATA_WIDTH 512

set IncludeEncodingEpath4_8b10b  5'b00000
set IncludeEncodingEpath8_8b10b  5'b00000

# 1: full mode
set FIRMWARE_MODE $FIRMWARE_MODE_FULL

set IncludeEncodingEpath2_HDLC   5'b00000 
set IncludeEncodingEpath2_8b10b  5'b00111
set IncludeEncodingEpath4_8b10b  5'b00000
set IncludeEncodingEpath8_8b10b  5'b00000
set INCLUDE_TTC                  5'b11111

set ENABLE_RQS false

#Set to false to set BUILD_TIME to the clock, true for GIT_COMMIT_TIME. With false, the build is reproducible.
set DETERMINISTIC_BUILD_TIME false

#Use LTI as clock input
set TTC_SYS_SEL                  $LTITTC

#Set to false to set BUILD_TIME to the clock, true for GIT_COMMIT_TIME. With false, the build is reproducible.
set DETERMINISTIC_BUILD_TIME false

source ../helper/do_implementation_post.tcl

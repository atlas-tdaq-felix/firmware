source ../helper/do_implementation_pre.tcl

set STOP_TO_ADD_ILA 0

set GENERATE_FM_WRAP false
set GBT_NUM 8
set GTREFCLKS 2
set OPTO_TRX 4
set CARD_TYPE 182
set app_clk_freq 200
set ENDPOINTS 2
set NUM_LEDS 4
set DATA_WIDTH 512

set FIRMWARE_MODE $FIRMWARE_MODE_INTERLAKEN 

set ENABLE_RQS false

#Use LTI as clock input
set TTC_SYS_SEL                  $LTITTC


source ../helper/do_implementation_post.tcl

#!/bin/bash
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


source ./ci-common.sh

vivado -mode batch -nojournal -nolog -notrace -source FLX709_FELIX_import_vivado.tcl
vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX709_FELIX/FLX709_FELIX.xpr -source do_implementation_VC709_FULL_si5324.tcl 

cd ../../output
rm -f *.bit
rm -f *.mcs
rm -f *.txt
rm -f *.prm
if ls FLX709_FULL*.tar.gz 1> /dev/null 2>&1; then
    echo "Build successful"
else
    echo "FELIX archive does not exist"
    exit 2
fi

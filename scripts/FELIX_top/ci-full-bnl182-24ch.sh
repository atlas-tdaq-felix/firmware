#!/bin/bash
#We use Vivado 2024.1 for the BNL182
source ./ci-common-FLX182.sh
 
vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX182_FELIX/FLX182_FELIX.xpr -source do_implementation_BNL182_FULL_24ch.tcl

cd ../../output
rm -f *.bit
rm -f *.mcs
rm -f *.txt
rm -f *.prm

if ls FLX182_FULL*.tar.gz 1> /dev/null 2>&1; then
    echo "Build successful"
else
    echo "FELIX archive does not exist"
    exit 2 
fi


#!/bin/bash
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


if [ -f /eda/fpga/xilinx/Vivado/2024.1/settings64.sh ]; then
    #Vivado at Nikhef machines, is installed in /localstore
    export XILINXD_LICENSE_FILE="$LM_LICENSE_FILE"
    source /eda/fpga/xilinx/Vivado/2024.1/settings64.sh
elif [ -f /afs/cern.ch/work/f/fschreud/public/Xilinx/Vivado/2024.1/settings64.sh ]; then
    #Vivado at Cern machines is installed on /afs
    export XILINXD_LICENSE_FILE="2112@licenxilinx"
    source /afs/cern.ch/work/f/fschreud/public/Xilinx/Vivado/2024.1/settings64.sh
else
    #Otherwise set both Nikhef and Cern license servers and try to find in /opt 
    export XILINXD_LICENSE_FILE="$LM_LICENSE_FILE,2112@licenxilinx"
    source /opt/Xilinx/Vivado/2024.1/settings64.sh
fi

vivado -mode batch -nojournal -nolog -notrace -source FLX712_FELIG_import_vivado.tcl
vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX712_FELIG/FLX712_FELIG.xpr -source do_implementation_BNL712_FELIG_GBT_24ch.tcl 

cd ../../output
rm -f *.bit
rm -f *.mcs
rm -f *.txt
rm -f *.prm

if ls FLX712_FELIG*.tar.gz 1> /dev/null 2>&1; then
    echo "Build successful"
else
    echo "FELIX archive does not exist"
    exit 2 
fi


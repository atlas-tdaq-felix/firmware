source ../helper/do_implementation_pre.tcl

set GBT_NUM 16
set GTREFCLKS 4
set OPTO_TRX 4
set CARD_TYPE 181
set app_clk_freq 200
set ENDPOINTS 2
set NUM_LEDS 4
set DATA_WIDTH 512

set FIRMWARE_MODE $FIRMWARE_MODE_INTERLAKEN 

#set STOP_TO_ADD_ILA 1

source ../helper/do_implementation_post.tcl


# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Kai Chen
#               Weihao Wu
#               Andrea Borga
#               RHabraken
#               Mesfin Gebyehu
#               Enrico Gamberini
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

source ../helper/do_implementation_pre.tcl

set GBT_NUM 24
set OPTO_TRX 4
set CARD_TYPE 128
set app_clk_freq 100
set ENDPOINTS 2
set DATA_WIDTH 512

set GTREFCLKS 6

set PLL_SEL $QPLL

# 1: full mode
set FIRMWARE_MODE $FIRMWARE_MODE_GBT

source ../helper/do_implementation_post.tcl

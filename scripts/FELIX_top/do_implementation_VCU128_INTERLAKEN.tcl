source ../helper/do_implementation_pre.tcl

set GENERATE_FM_WRAP true
set GBT_NUM 8
set OPTO_TRX 4
set CARD_TYPE 128
set app_clk_freq 100
set ENDPOINTS 2
set DATA_WIDTH 512

set GTREFCLKS 2
set LMK_CLKS 0

set FIRMWARE_MODE $FIRMWARE_MODE_INTERLAKEN 

source ../helper/do_implementation_post.tcl

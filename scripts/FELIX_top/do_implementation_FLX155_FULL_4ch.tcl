
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Nayib Boukadida
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

source ../helper/do_implementation_pre.tcl

set GENERATE_FM_WRAP true
set GBT_NUM 4
set GTREFCLKS 1
#Uncomment in order to stop after synthesis, so ILA probes can be added.
#set STOP_TO_ADD_ILA 1
set CARD_TYPE 155
set app_clk_freq 200
set ENDPOINTS 2
set NUM_LEDS 4
set PCIE_LANES 8
set DATA_WIDTH 1024

set FIRMWARE_MODE $FIRMWARE_MODE_FULL

set ENABLE_RQS false

source ../helper/do_implementation_post.tcl

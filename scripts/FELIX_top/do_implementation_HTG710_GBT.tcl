
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Andrea Borga
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

source ../helper/do_implementation_pre.tcl
## General settings
# -- set to true in order to generate the GBT links
set GENERATE_GBT true
set GBT_NUM 4
set OPTO_TRX 2
# -- GBT mapping: 0 NORMAL CXP1 -> GBT1-12 | 1 ALTERNATE CXP1 -> GBT 1-4,9-12,17-20
set GBT_MAPPING 1 
set CARD_TYPE 710
# -- Include Data emulator for GBT
set useToHostGBTdataEmulator true
set useToFrontendGBTdataEmulator true

## Clock inputs settings
# -- use_backup_clk true means the 100MHz onboard clock is used to generate 40/80/160/240/320 MHz, false takes the clock from TTC
set use_backup_clk true
# -- set automatic_clock_switch to true in order to generate the automatic clock switch network to switch between 100MHz crystal and TTC clock
set automatic_clock_switch false
# -- App clk freq from the onboard oscillator
set app_clk_freq 100

## Debug mode
# -- true enables the debug_port_module and all signals connected to it
set debug_mode false

## centralRouter specific generics
# -- Eprocs 
set includeToHoEproc8s true
set includeToHoEproc16 false
set includeToHoEproc4s true
set includeToHoEproc2s true
set includeFrHoEproc8s true
set includeFrHoEproc4s true
set includeFrHoEproc2s true
# -- eproc 5 and 6 (aka wide mode)
set wideMode false
# -- disable direct mode
set includeDirectMode false
# -- don't change CR Enable FromHost, unless strictly necessary
set CREnableFromHost true
# -- centralRouter loopback
# true: enables a direct loopback inside the CR used to test the To-From Host paths  
set crInternalLoopbackMode false
# STATIC Central Router: Set to true to remove configurability of Central Router (reduce size) and only use default configuration
set STATIC_CENTRALROUTER false
# elink time out value expressed in n_bits at 40 MHz
set toHostTimeoutBitn 16
#Set to true to disable all central router channels except IC/EC/TTC
set generate_IC_EC_TTC_only false

## TTC settings
# -- generate TTC data emulator
# true: switches the source of the TTC data to be a 10-bit counter 
set generateTTCemu true
# -- TTC test mode
# true: initializes configuration registers to TTC loopback test
set TTC_test_mode false

## GBT Wrapper generics
# -- set the MGTREFCLOCK

set GTHREFCLK_SEL $GREFCLK
#set GTHREFCLK_SEL $MGTREFCLK
#set PLL_SEL $CPLL
set PLL_SEL $QPLL
#set to false to use TTCfx3 (Si5345) refclk, true for onboard Si5324.
set USE_Si5324_RefCLK false

## Constraints files
set_property is_enabled false [get_files $HDLDIR/constraints/felix_gbt_cxp_HTG710.xdc]
set_property is_enabled false [get_files $HDLDIR/constraints/felix_gbt_cxp_copper_HTG710.xdc]
set_property is_enabled false [get_files $HDLDIR/constraints/felix_gbt_sfp_VC709.xdc]
set_property is_enabled false [get_files $HDLDIR/constraints/felix_top_VC709.xdc]
set_property is_enabled true [get_files  $HDLDIR/constraints/felix_top_HTG710.xdc]

if {$GBT_MAPPING == 0} {set_property is_enabled true [get_files  $HDLDIR/constraints/felix_gbt_cxp_HTG710.xdc]} 
if {$GBT_MAPPING == 1} {set_property is_enabled true [get_files  $HDLDIR/constraints/felix_gbt_cxp_copper_HTG710.xdc]}

source ../helper/do_implementation_post.tcl

#!/bin/bash
echo 0 > _RETVAL_

function run_vsg () {
    local run=$1
    python3 scripts/vhdl-style-guide/bin/vsg -f sources/$run -c scripts/vsg-styles/felix.yaml
    if [[ $? != 0 ]]; then
        echo $run" failed, exiting with status 1"
        echo 1 > $SCRIPTDIR/_RETVAL_
    fi
}

VHDL_FILES=$(tclsh all_vhdl_files.tcl|sed 's/..\/simulation\/..\/sources\///g'|sort|uniq)

SCRIPTDIR=$(pwd)
cd ../../
CORES=32
i=0

for file in $VHDL_FILES
do
    #echo "Processing VHDL file: "$file
    ((i=i%CORES)); ((i++==0)) && wait
    #Skip checks for dma_control and pcie_package, as they take very long, especially in CI, also exclude submodules
    if [[ (! ${file} =~ "pcie_package") ]] &&\
        [[ (! ${file} =~ "dma_control") ]] &&\
        [[ (! ${file} =~ "simulation/UVVM") ]] &&\
        [[ (! ${file} =~ "LpGBT/LpGBT_CERN/BE/lpgbt-fpga/") ]] &&\
        [[ (! ${file} =~ "LpGBT/LpGBT_CERN/FE/lpgbt-emul/") ]] &&\
        [[ (! ${file} =~ "ip_cores/") ]] &&\
        [[ (! ${file} =~ "tclink-cern/") ]] &&\
        [[ (! ${file} =~ "tx_phase_aligner-cern/") ]] &&\
        [[ (! ${file} =~ "core1990_interlaken/") ]];then
        run_vsg "$file"&
    fi
done
wait
cd $SCRIPTDIR

RETVAL=$(cat _RETVAL_)
rm _RETVAL_
echo "Returning exit code" $RETVAL
exit $RETVAL

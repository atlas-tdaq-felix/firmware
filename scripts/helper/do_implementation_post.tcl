
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Andrea Borga
#               Enrico Gamberini
#               RHabraken
#               Israel Grayzman
#               Mesfin Gebyehu
#               Kai Chen
#               Shelfali Saxena
#               William Wulff
#               Thei Wijnen
#               Marius Wensing
#               Ricardo Luz
#               Alessandro Palombi
#               Filiberto Bonini
#               Elena Zhivun
#               Rene
#               Alessandro Thea
#               Ohad Shaked
#               mtrovato
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#file: do_implementation_post.tcl

set GIT_HASH [exec git rev-parse HEAD]
set GIT_HASH "160'h$GIT_HASH"
#set GIT_BRANCH [string map { / - } [exec git rev-parse --abbrev-ref HEAD]]
set COMMIT_DATETIME [exec git show -s --format=%cd --date=iso]
set YY [string range $COMMIT_DATETIME 2 3]
set MM [string range $COMMIT_DATETIME 5 6]
set DD [string range $COMMIT_DATETIME 8 9]
set hh [string range $COMMIT_DATETIME 11 12]
set mm [string range $COMMIT_DATETIME 14 15]
 

set COMMIT_DATETIME 40'h${YY}${MM}${DD}${hh}${mm}

if { $DETERMINISTIC_BUILD_TIME == true} {
    set TIMESTAMP ${YY}${MM}${DD}_${hh}_${mm}
set build_date $COMMIT_DATETIME
puts "BUILD_DATE = $build_date"
} else {
    set systemTime [clock seconds]
    set build_date "40'h[clock format $systemTime -format %y%m%d%H%M]"
    puts "BUILD_DATE = $build_date"
    set TIMESTAMP "[clock format $systemTime -format %y%m%d_%H%M]"
}
set git_tag_str [exec git describe --abbrev=0 --tags --match rm*]
set GIT_COMMIT_NUMBER [exec git rev-list ${git_tag_str}..HEAD]
set GIT_COMMIT_NUMBER [expr [regexp -all {[\n]+} $GIT_COMMIT_NUMBER ] +1]
binary scan [string reverse ${git_tag_str}] H* t
set GIT_TAG "128'h$t"
puts $GIT_TAG           
set svn_version "0"

set scriptdir [pwd]
set HDLDIR $scriptdir/../../


#Copy back all the IP cores from the project back into the repository
set IPS [get_ips -exclude_bd_ips]
set XCI_FILES ""
foreach IP $IPS {
    set XCI_FILES [concat $XCI_FILES " " [get_files $IP.xci]]
}
#set XCI_FILES [get_files *.xci]
set BD_FILES [get_files *.bd]
set PART [get_property part [current_project]]

if {$PART == "xcku115-flvf1924-2-e"} {
    set core_dir $HDLDIR/sources/ip_cores/kintexUltrascale/
} elseif {$PART == "xc7vx690tffg1761-2"} {
    set core_dir $HDLDIR/sources/ip_cores/virtex7/
} elseif {$PART == "xcvu37p-fsvh2892-2-e-es1"} {
    set core_dir $HDLDIR/sources/ip_cores/VU37P/
} elseif {$PART == "xcvu37p-fsvh2892-2-e"} {
    set core_dir $HDLDIR/sources/ip_cores/VU37P/
} elseif {$PART == "xcvu9p-flgb2104-2-e"} {
    set core_dir $HDLDIR/sources/ip_cores/VU9P/
} elseif {$PART == "xcvm1802-vsva2197-2MP-e-S-es1" || $PART == "xcvm1802-vsva2197-2MP-e-S"} {
    set core_dir $HDLDIR/sources/ip_cores/VMK180/
} elseif {$PART == "xcvm1802-vsva2197-1MP-e-S-es1"} {
   set core_dir $HDLDIR/sources/ip_cores/BNL181/
} elseif {$PART == "xcvm1802-vsva2197-1MP-e-S"} {
    set core_dir $HDLDIR/sources/ip_cores/BNL182/
} elseif {$PART == "xcvp1552-vsva3340-2MHP-e-S"} {
    set core_dir $HDLDIR/sources/ip_cores/FLX155/
} elseif {$PART == "xcvp1202-vsva2785-2MHP-e-S"} {
    set core_dir $HDLDIR/sources/ip_cores/VPK120/
} else {
    puts "Error: Unsupported part: $PART"
    return
}

if {$TTC_SYS_SEL == $LTITTC && $CARD_TYPE != 711 && $CARD_TYPE != 712 && $CARD_TYPE != 182} {
    puts "Error: LTITTC not implemented for CARD $CARD_TYPE"
    return
}
if {$TTC_SYS_SEL == $LTITTC && $GBT_NUM>24 && $CARD_TYPE == 712 } {
    puts "Error: LTITTC link and GBT=24 assigned to the same bank"
    return
 
}

foreach XCI_FILE $XCI_FILES {
    file copy -force $XCI_FILE $core_dir
}

foreach BD_FILE $BD_FILES {
    file copy -force $BD_FILE $core_dir
}


#Check if there are no uncommitted changes in the GIT repository, otherwise show the diff
#in generics_timing.txt, done in do_implementation_finish.tcl, here we raise a critical warning
# Use -G. to ignore git seeing 755 file access attributes on Windows filesystems on each file
set GitDiff [exec git diff -G.]
if {[string trim $GitDiff] != ""} {
    set MsgString "There were uncommitted changes in the GIT repository while starting this build:

$GitDiff

"
     
    send_msg_id {GitDiff-1} {CRITICAL WARNING} $MsgString
}

cd ../../
set GitSubmoduleStatus [exec git submodule status]
cd $scriptdir

#For 711 / 712 cards the core location in the PCIe endpoint must be selected correctly.
if {$CARD_TYPE == 711} {
    set loc_7039 [get_property CONFIG.pcie_blk_locn [get_ips pcie3_ultrascale_7039]]
    if { $loc_7039 != "X0Y1" } {
        set_property -dict [list CONFIG.pcie_blk_locn {X0Y0} CONFIG.gen_x0y1 {false} CONFIG.gen_x0y0 {true} CONFIG.PL_LINK_CAP_MAX_LINK_WIDTH {X8} CONFIG.mcap_enablement {None}] [get_ips pcie3_ultrascale_7038]
        set_property -dict [list CONFIG.pcie_blk_locn {X0Y1} CONFIG.gen_x0y1 {true} CONFIG.gen_x0y3 {false} CONFIG.PL_LINK_CAP_MAX_LINK_WIDTH {X8} CONFIG.mcap_enablement {None}] [get_ips pcie3_ultrascale_7039]
        set_property -dict [list CONFIG.dedicate_perst {false}] [get_ips pcie3_ultrascale_7038]
        generate_target all [get_files pcie3_ultrascale_7038.xci]
        export_ip_user_files -of_objects [get_files pcie3_ultrascale_7038.xci] -no_script -force -quiet
        reset_run pcie3_ultrascale_7038_synth_1
        launch_run -jobs 12 pcie3_ultrascale_7038_synth_1
        generate_target all [get_files pcie3_ultrascale_7039.xci]
        export_ip_user_files -of_objects [get_files pcie3_ultrascale_7039.xci] -no_script -force -quiet
        reset_run pcie3_ultrascale_7039_synth_1
        launch_run -jobs 12 pcie3_ultrascale_7039_synth_1
    }
} 
if {$CARD_TYPE == 712} {
    set loc_7039 [get_property CONFIG.pcie_blk_locn [get_ips pcie3_ultrascale_7039]]
    if { $loc_7039 != "X0Y3" } {
        set_property -dict [list CONFIG.pcie_blk_locn {X0Y1} CONFIG.gen_x0y0 {false} CONFIG.gen_x0y1 {true} CONFIG.PL_LINK_CAP_MAX_LINK_WIDTH {X8} CONFIG.mcap_enablement {None}] [get_ips pcie3_ultrascale_7038]
        set_property -dict [list CONFIG.pcie_blk_locn {X0Y3} CONFIG.gen_x0y1 {false} CONFIG.gen_x0y3 {true} CONFIG.PL_LINK_CAP_MAX_LINK_WIDTH {X8} CONFIG.mcap_enablement {None}] [get_ips pcie3_ultrascale_7039]
        set_property -dict [list CONFIG.dedicate_perst {false}] [get_ips pcie3_ultrascale_7038]
        generate_target all [get_files pcie3_ultrascale_7038.xci]
        export_ip_user_files -of_objects [get_files pcie3_ultrascale_7038.xci] -no_script -force -quiet
        reset_run pcie3_ultrascale_7038_synth_1
        launch_run -jobs 12 pcie3_ultrascale_7038_synth_1
        generate_target all [get_files pcie3_ultrascale_7039.xci]
        export_ip_user_files -of_objects [get_files pcie3_ultrascale_7039.xci] -no_script -force -quiet
        reset_run pcie3_ultrascale_7039_synth_1
        launch_run -jobs 12 pcie3_ultrascale_7039_synth_1
    }
}

#Disable the complete dma_control.vhd
if {($FIRMWARE_MODE >= 0) && ($FIRMWARE_MODE <= 15) } {
    set_property is_enabled false [get_files dma_control.vhd]
} else {
    set_property is_enabled true [get_files dma_control.vhd]
}

#Enable only the partially generated dma_control_N.vhd for the specific FIRMWARE_MODE
for {set mode 0} {$mode <= 15} {incr mode} {
    if {($FIRMWARE_MODE == $mode)} {
        set_property is_enabled true [get_files dma_control_${mode}.vhd]
    } else {
        set_property is_enabled false [get_files dma_control_${mode}.vhd]
    }
}

if {($FIRMWARE_MODE == $FIRMWARE_MODE_GBT|| \
     $FIRMWARE_MODE == $FIRMWARE_MODE_FULL|| \
     $FIRMWARE_MODE == $FIRMWARE_MODE_FMEMU|| \
     $FIRMWARE_MODE == $FIRMWARE_MODE_LTDB|| \
     $FIRMWARE_MODE == $FIRMWARE_MODE_FEI4|| \
     $FIRMWARE_MODE == $FIRMWARE_MODE_PIXEL|| \
     $FIRMWARE_MODE == $FIRMWARE_MODE_FELIG_GBT|| \
     $FIRMWARE_MODE == $FIRMWARE_MODE_FELIG_LPGBT|| \
     $FIRMWARE_MODE == $FIRMWARE_MODE_STRIP|| \
     $FIRMWARE_MODE == $FIRMWARE_MODE_LPGBT|| \
     $FIRMWARE_MODE == $FIRMWARE_MODE_HGTD_LUMI|| \
     $FIRMWARE_MODE == $FIRMWARE_MODE_BCM_PRIME|| \
     $FIRMWARE_MODE == $FIRMWARE_MODE_FELIG_PIXEL|| \
     $FIRMWARE_MODE == $FIRMWARE_MODE_FELIG_STRIP) \
     && ($CARD_TYPE == 712) } {
    if {$GBT_NUM <= 8 && $FE_EMU_EN == 0} {
        set_property is_enabled true [get_files  felix_gbt_minipod_BNL712_transceiver_8ch.xdc]
        set_property is_enabled false [get_files  felix_gbt_minipod_BNL712_transceiver_24ch.xdc]
    } else {
        set_property is_enabled true [get_files  felix_gbt_minipod_BNL712_transceiver_24ch.xdc]
        set_property is_enabled false [get_files  felix_gbt_minipod_BNL712_transceiver_8ch.xdc]
    }
    if {$GBT_NUM > 24  || ($FE_EMU_EN > 0 && $GBT_NUM > 12)} {
        set_property is_enabled true [get_files  felix_gbt_minipod_BNL712_transceiver_48ch.xdc]
        set_property is_enabled false [get_files  felix_gbt_minipod_BNL712_transceiver_8ch.xdc]
        set_property is_enabled false [get_files  felix_gbt_minipod_BNL712_transceiver_24ch.xdc]    
    } else {
        set_property is_enabled false [get_files  felix_gbt_minipod_BNL712_transceiver_48ch.xdc]
    }
}


if {($FIRMWARE_MODE == $FIRMWARE_MODE_GBT||\
     $FIRMWARE_MODE == $FIRMWARE_MODE_FULL|| \
     $FIRMWARE_MODE == $FIRMWARE_MODE_FMEMU| \
     $FIRMWARE_MODE == $FIRMWARE_MODE_LTDB||\
     $FIRMWARE_MODE == $FIRMWARE_MODE_FEI4|| \
     $FIRMWARE_MODE == $FIRMWARE_MODE_FELIG_GBT|| \
     $FIRMWARE_MODE == $FIRMWARE_MODE_FELIG_LPGBT|| \
     $FIRMWARE_MODE == $FIRMWARE_MODE_PIXEL|| \
     $FIRMWARE_MODE == $FIRMWARE_MODE_STRIP||\
     $FIRMWARE_MODE == $FIRMWARE_MODE_LPGBT|| \
     $FIRMWARE_MODE == $FIRMWARE_MODE_HGTD_LUMI) &&\
     ($CARD_TYPE == 711) } {
    if {$GBT_NUM <= 8} {
        set_property is_enabled true [get_files  felix_gbt_minipod_BNL712_transceiver_8ch.xdc]
        set_property is_enabled false [get_files  felix_gbt_minipod_BNL712_transceiver_24ch.xdc]
    } else {
        set_property is_enabled true [get_files  felix_gbt_minipod_BNL712_transceiver_24ch.xdc]
        set_property is_enabled false [get_files  felix_gbt_minipod_BNL712_transceiver_8ch.xdc]
    }
}

if { ($FIRMWARE_MODE == $FIRMWARE_MODE_PIXEL|| \
      $FIRMWARE_MODE == $FIRMWARE_MODE_STRIP||\
      $FIRMWARE_MODE == $FIRMWARE_MODE_LPGBT|| \
      $FIRMWARE_MODE == $FIRMWARE_MODE_HGTD_LUMI|| \
      $FIRMWARE_MODE == $FIRMWARE_MODE_BCM_PRIME) } {
    set TRANSCEIVER_TYPE 1
} elseif {$FIRMWARE_MODE == $FIRMWARE_MODE_INTERLAKEN} {
    set TRANSCEIVER_TYPE 2
} else {
    set TRANSCEIVER_TYPE 0
}

#Overwrite flavor-dependent ISLPGBT value into timing_constraints.xdc template
set constr_dir $HDLDIR/constraints/
set fd [open "$constr_dir/TRANSCEIVER_TYPE.tcl" w]
puts $fd "return $TRANSCEIVER_TYPE"
close $fd

set fd [open "$constr_dir/CARD_TYPE.tcl" w]
puts $fd "return $CARD_TYPE"
close $fd

set transvc_ip_list { KCU_NORXBUF_PCS_CPLL_1CH \
    KCU_RXBUF_PMA_CPLL_1CH \
    KCU_RXBUF_PMA_QPLL_4CH \
    KCU_RXBUF_PMA_QPLL_4CH \
    KCU_RXBUF_PMA_QPLL_FE4CH \
    KCU_RXBUF_PMA_QPLL_FE4CH_LPGBT \
    KCU_RXBUF_PMA_QPLL_4CH_LPGBT
}

foreach transvc_ip $transvc_ip_list {
    if { [file exists [get_files ${transvc_ip}.xdc] ] && [get_property is_enabled [get_files ${transvc_ip}.xdc]] } {
        puts "Launching run for $transvc_ip..."
        set_property is_enabled false [get_files ${transvc_ip}.xdc]
        reset_run -quiet ${transvc_ip}_synth_1
        launch_runs -quiet ${transvc_ip}_synth_1
    }
}


if {$FIRMWARE_MODE == $FIRMWARE_MODE_MROD} {
    if {$MROD_GENERATE_NOTRX == false} {
        if {$NUMCH <= 12} {
            if {$NUMCH > 4} {
                set_property is_enabled false [get_files  felix_minipod_BNL712_2x4ch_MROD.xdc]
                set_property is_enabled true [get_files  felix_minipod_BNL712_2x12ch_MROD.xdc]
                set_property is_enabled false [get_files  felix_minipod_BNL712_2x24ch_MROD.xdc]
                set_property is_enabled false [get_files  felix_minipod_BNL712_notrx_MROD.xdc]
            } else {
                set_property is_enabled true [get_files  felix_minipod_BNL712_2x4ch_MROD.xdc]
                set_property is_enabled false [get_files  felix_minipod_BNL712_2x12ch_MROD.xdc]
                set_property is_enabled false [get_files  felix_minipod_BNL712_2x24ch_MROD.xdc]
                set_property is_enabled false [get_files  felix_minipod_BNL712_notrx_MROD.xdc]
            }
        } else {
            set_property is_enabled false [get_files  felix_minipod_BNL712_2x4ch_MROD.xdc]
            set_property is_enabled false [get_files  felix_minipod_BNL712_2x12ch_MROD.xdc]
            set_property is_enabled true [get_files  felix_minipod_BNL712_2x24ch_MROD.xdc]
            set_property is_enabled false [get_files  felix_minipod_BNL712_notrx_MROD.xdc]
        }
    } else {
        set_property is_enabled false [get_files  felix_minipod_BNL712_2x4ch_MROD.xdc]
        set_property is_enabled false [get_files  felix_minipod_BNL712_2x12ch_MROD.xdc]
        set_property is_enabled false [get_files  felix_minipod_BNL712_2x24ch_MROD.xdc]
        set_property is_enabled true [get_files  felix_minipod_BNL712_notrx_MROD.xdc]
    }
    set_property USED_IN {implementation} [get_files pblocks_KCU_FM_v2_MROD.xdc]
    set_property USED_IN {implementation} [get_files pcie1_SRL1_BNL711_MROD.xdc]
    set_property USED_IN {implementation} [get_files timing_constraints_fullmode_ku_MROD.xdc]
    set_property USED_IN {implementation} [get_files timing_constraints_spec_MROD.xdc]
}

if {$FIRMWARE_MODE == $FIRMWARE_MODE_FELIG_GBT } {
    set_property is_enabled true [get_files timing_constraints_felig_gbt.xdc]
    set_property is_enabled false [get_files timing_constraints_felig_lpgbt.xdc]
}
if {$FIRMWARE_MODE == $FIRMWARE_MODE_FELIG_LPGBT } {
    set_property is_enabled false [get_files timing_constraints_felig_gbt.xdc]
    set_property is_enabled true [get_files timing_constraints_felig_lpgbt.xdc]
}


#Delete the RQS/ML run, start with the default impl_1 run instead.
if {[get_runs impl_2] != ""} {
    current_run [get_runs impl_1]
    delete_runs "impl_2"
}

set IMPL_RUN [get_runs impl_1]
set SYNTH_RUN [get_runs synth_1]

reset_run $SYNTH_RUN

foreach design [get_designs] {
    puts "Closing design: $design"
    current_design $design
    close_design
}



## PCIe EndPoint constraints
#For BNL711 v1p5 hardware
set SLR0 0
#For BNL711 v2p0 hardware
set SLR1 1

## BNL-711 PCIe location constraints
if {$CARD_TYPE == 712} {
    set PCIE_PLACEMENT $SLR1
} else {
    set PCIE_PLACEMENT $SLR0
}
#GBT mode seems to benefit more from AreaOptimized_medium, FULL mode is better with PerfOptimized_high
if {($FIRMWARE_MODE == $FIRMWARE_MODE_FULL||\
    $FIRMWARE_MODE == $FIRMWARE_MODE_INTERLAKEN)} {
    set_property strategy Flow_PerfOptimized_high $SYNTH_RUN
    
} else {
    set_property strategy Flow_AreaOptimized_high $SYNTH_RUN
}

#set_property STEPS.SYNTH_DESIGN.ARGS.RETIMING true [get_runs $SYNTH_RUN]
## Optimize place and route algorithm
set_property strategy Performance_ExplorePostRoutePhysOpt $IMPL_RUN
set_property STEPS.OPT_DESIGN.ARGS.DIRECTIVE ExploreWithRemap $IMPL_RUN
#set_property STEPS.PLACE_DESIGN.ARGS.DIRECTIVE SpreadLogic_high $IMPL_RUN
set_property STEPS.PHYS_OPT_DESIGN.ARGS.DIRECTIVE AggressiveFanoutOpt $IMPL_RUN
#set_property STEPS.ROUTE_DESIGN.ARGS.DIRECTIVE MoreGlobalIterations $IMPL_RUN
#set_property STEPS.ROUTE_DESIGN.ARGS.DIRECTIVE HigherDelayCost $IMPL_RUN
#set_property STEPS.ROUTE_DESIGN.ARGS.DIRECTIVE Explore $IMPL_RUN
set_property STEPS.ROUTE_DESIGN.ARGS.DIRECTIVE NoTimingRelaxation $IMPL_RUN
set_property STEPS.POST_ROUTE_PHYS_OPT_DESIGN.ARGS.DIRECTIVE AddRetime $IMPL_RUN

# Quick build to find time constrained paths fast E.Z.
# set_property strategy Performance_Explore $IMPL_RUN
# set_property STEPS.OPT_DESIGN.ARGS.DIRECTIVE Default $IMPL_RUN
# set_property STEPS.OPT_DESIGN.IS_ENABLED false $IMPL_RUN
# set_property STEPS.PHYS_OPT_DESIGN.IS_ENABLED false $IMPL_RUN
# set_property STEPS.PHYS_OPT_DESIGN.ARGS.DIRECTIVE Default $IMPL_RUN
# set_property STEPS.ROUTE_DESIGN.ARGS.DIRECTIVE Default $IMPL_RUN
# set_property STEPS.POST_ROUTE_PHYS_OPT_DESIGN.ARGS.DIRECTIVE Default $IMPL_RUN


set_property generic "
AUTOMATIC_CLOCK_SWITCH=$automatic_clock_switch \
USE_BACKUP_CLK=$use_backup_clk \
APP_CLK_FREQ=$app_clk_freq \
BUILD_DATETIME=$build_date \
SVN_VERSION=$svn_version \
COMMIT_DATETIME=$COMMIT_DATETIME \           
GIT_HASH=$GIT_HASH \
GIT_TAG=$GIT_TAG \
GIT_COMMIT_NUMBER=$GIT_COMMIT_NUMBER \
GENERATE_GBT=$GENERATE_GBT \
NUMBER_OF_INTERRUPTS=$NUMBER_OF_INTERRUPTS \
NUMBER_OF_DESCRIPTORS=$NUMBER_OF_DESCRIPTORS \
GBT_NUM=$GBT_NUM \
ENDPOINTS=$ENDPOINTS \
OPTO_TRX=$OPTO_TRX \
GBT_MAPPING=$GBT_MAPPING \
CARD_TYPE=$CARD_TYPE \
DEBUG_MODE=$debug_mode \
IncludeDecodingEpath2_HDLC=$IncludeDecodingEpath2_HDLC \
IncludeDecodingEpath2_8b10b=$IncludeDecodingEpath2_8b10b \
IncludeDecodingEpath4_8b10b=$IncludeDecodingEpath4_8b10b \
IncludeDecodingEpath8_8b10b=$IncludeDecodingEpath8_8b10b \
IncludeDecodingEpath16_8b10b=$IncludeDecodingEpath16_8b10b \ 
IncludeDecodingEpath32_8b10b=$IncludeDecodingEpath32_8b10b \
IncludeDirectDecoding=$IncludeDirectDecoding \
IncludeEncodingEpath2_HDLC=$IncludeEncodingEpath2_HDLC \
IncludeEncodingEpath2_8b10b=$IncludeEncodingEpath2_8b10b \
IncludeEncodingEpath4_8b10b=$IncludeEncodingEpath4_8b10b \
IncludeEncodingEpath8_8b10b=$IncludeEncodingEpath8_8b10b \
IncludeDirectEncoding=$IncludeDirectEncoding \
INCLUDE_TTC=$INCLUDE_TTC \
TTC_SYS_SEL=$TTC_SYS_SEL \
INCLUDE_RD53=$INCLUDE_RD53 \
DEBUGGING_RD53=$DEBUGGING_RD53 \
RD53Version=$RD53Version \
ISTESTBEAM=$ISTESTBEAM \
generateTTCemu=$generateTTCemu \
TTC_test_mode=$TTC_test_mode \
useToHostGBTdataEmulator=$useToHostGBTdataEmulator \
useToFrontendGBTdataEmulator=$useToFrontendGBTdataEmulator \
GTHREFCLK_SEL=$GTHREFCLK_SEL \
CREnableFromHost=$CREnableFromHost \
GENERATE_FM_WRAP=$GENERATE_FM_WRAP \
toHostTimeoutBitn=$toHostTimeoutBitn \
PLL_SEL=$PLL_SEL \
generate_IC_EC_TTC_only=$generate_IC_EC_TTC_only \
includeDirectMode=$includeDirectMode \
USE_Si5324_RefCLK=$USE_Si5324_RefCLK \
FIRMWARE_MODE=$FIRMWARE_MODE \
PCIE_LANES=$PCIE_LANES \
DATA_WIDTH=$DATA_WIDTH \
GENERATE_TRUNCATION_MECHANISM=$GENERATE_TRUNCATION_MECHANISM \
ENDPOINTS=$ENDPOINTS \
NUM_LEDS=$NUM_LEDS \
GTREFCLKS=$GTREFCLKS \
CHUNK_TRAILER_32B=$CHUNK_TRAILER_32B \
BLOCKSIZE=$BLOCKSIZE \
FULL_HALFRATE=$FULL_HALFRATE \
KCU_LOWER_LATENCY=$KCU_LOWER_LATENCY \
SUPPORT_HDLC_DELAY=$SUPPORT_HDLC_DELAY \
ENABLE_XVC=$ENABLE_XVC \
" [current_fileset]

set CORES 16
puts "INFO: $CORES cores are in used"

launch_runs $SYNTH_RUN  -jobs $CORES
wait_on_run $SYNTH_RUN
#open_run $SYNTH_RUN
#return

if {$STOP_TO_ADD_ILA == 1} {
open_run $SYNTH_RUN -name $SYNTH_RUN
# wait to: open_run $SYNTH_RUN, to finish
wait_on_run $SYNTH_RUN
set STOP_TO_ADD_ILA 0
puts ""
puts ""
puts "*** ============================================================ ***"
puts "*** The script stopped in order to provide the ability to add an ***"
puts "*** ila after you finish with defining the debug ports, run the  ***"
puts "*** tcl command: source ./../helper/do_implementation_finish.tcl ***"
puts "*** ============================================================ ***"
puts ""
puts ""
break
} else {
    source ../helper/do_implementation_finish.tcl
};

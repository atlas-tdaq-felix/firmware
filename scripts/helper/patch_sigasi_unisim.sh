#!/bin/bash
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

echo "2c2
< <com.sigasi.hdt.vhdl.scoping.librarymapping.model:LibraryMappings xmlns:com.sigasi.hdt.vhdl.scoping.librarymapping.model=\"com.sigasi.hdt.vhdl.scoping.librarymapping\" Version=\"2\">
---
> <com.sigasi.hdt.shared.librarymapping.model:LibraryMappings xmlns:com.sigasi.hdt.shared.librarymapping.model=\"com.sigasi.hdt.vhdl.scoping.librarymapping\" Version=\"2\">
13a14,19
>   <Mappings Location=\"Common Libraries/unisim/primitive\" Library=\"not mapped\"/>
>   <Mappings Location=\"Common Libraries/unisim/retarget\" Library=\"not mapped\"/>
>   <Mappings Location=\"Common Libraries/unisim/retarget_VCOMP.vhd\" Library=\"not mapped\"/>
>   <Mappings Location=\"Common Libraries/unisim/secureip\" Library=\"not mapped\"/>
>   <Mappings Location=\"Common Libraries/unisim/unisim_retarget_VCOMP.vhd\" Library=\"not mapped\"/>
>   <Mappings Location=\"Common Libraries/unisim\" Library="unisim"/>
186c192
< </com.sigasi.hdt.vhdl.scoping.librarymapping.model:LibraryMappings>
---
> </com.sigasi.hdt.shared.librarymapping.model:LibraryMappings>"|patch .library_mapping.xml
echo "39a40,44
> 			<name>Common Libraries/unisim</name>
> 			<type>2</type>
> 			<locationURI>SIGASI_TOOLCHAIN_XILINX_VIVADO/data/vhdl/src/unisims</locationURI>
> 		</link>
> 		<link>"|patch .project

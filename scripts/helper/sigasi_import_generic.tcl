
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Mesfin Gebyehu
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Set the supportfiles directory path
set scriptdir [pwd]
#Download SigasiProjectCreator from https://github.com/sigasi/SigasiProjectCreator 
#Extract it in the directory below (/opt by default).

set firmware_dir $scriptdir/../../
set SigasiProjectCreator ${scriptdir}/../helper/SigasiProjectCreator/
# Vivado project directory:
set project_dir $firmware_dir/Projects/$PROJECT_NAME

set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_KU]
set SIM_FILES [concat $SIM_FILES $SIM_FILES_KU]
set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_V7]
set SIM_FILES [concat $SIM_FILES $SIM_FILES_V7]
set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_VU9P]
set SIM_FILES [concat $SIM_FILES $SIM_FILES_VU9P]
set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_VU37P]
set SIM_FILES [concat $SIM_FILES $SIM_FILES_VU37P]
set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_VERSAL]
set SIM_FILES [concat $SIM_FILES $SIM_FILES_VERSAL]
set XCI_FILES [concat $XCI_FILES $XCI_FILES_KU]

file mkdir $project_dir

set CSV_FILE [open $project_dir/$PROJECT_NAME.csv w]


foreach VHDL_FILE $VHDL_FILES {
    if {[string first "data_width_package_512.vhd" $VHDL_FILE] == -1} {
        puts $CSV_FILE "work, ${firmware_dir}/sources/${VHDL_FILE}"
    }

}

foreach SIM_FILE $SIM_FILES {
	puts $CSV_FILE "work, ${firmware_dir}/simulation/${SIM_FILE}"
}
#puts $CSV_FILE "unisim, SIGASI_TOOLCHAIN_XILINX_VIVADO/data/vhdl/src/unisims"

foreach XCI_FILE $XCI_FILES {
	set XCI_FILE [string trimright $XCI_FILE .xci]
	puts $CSV_FILE "work, ${firmware_dir}/sources/ip_cores/stub/${XCI_FILE}_stub.vhdl"
}


foreach VERILOG_FILE $VERILOG_FILES {
	puts $CSV_FILE "work, ${firmware_dir}/sources/${VERILOG_FILE}"
}

close $CSV_FILE

cd $project_dir
exec python ${SigasiProjectCreator}/src/SigasiProjectCreator/convertCsvFileToLinks.py ${PROJECT_NAME} ${PROJECT_NAME}.csv
#exec ../../scripts/helper/patch_sigasi_unisim.sh

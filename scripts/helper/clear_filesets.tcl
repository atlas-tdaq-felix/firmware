
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Thei Wijnen
#               mtrovato
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
set SIM_ARCH "KU"

set XCI_FILES ""
set VHDL_FILES ""
set DISABLED_VHDL_FILES ""
set VERILOG_FILES ""
set SIM_FILES ""
set EXCLUDE_SIM_FILES ""
set WCFG_FILES ""
set BD_FILES ""

set XCI_FILES_V7 ""
set VHDL_FILES_V7 ""
set SIM_FILES_V7 ""
set BD_FILES_V7 ""

set XCI_FILES_KU ""
set VHDL_FILES_KU ""
set SIM_FILES_KU ""
set BD_FILES_KU ""

set XCI_FILES_VU9P ""
set VHDL_FILES_VU9P ""
set SIM_FILES_VU9P ""
set BD_FILES_VU9P ""

set XCI_FILES_VU37P ""
set VHDL_FILES_VU37P ""
set SIM_FILES_VU37P ""
set BD_FILES_VU37P ""

set XCI_FILES_VERSAL ""
set VHDL_FILES_VERSAL ""
set VHDL_FILES_BNL182 ""
set SIM_FILES_VERSAL ""
set BD_FILES_VMK180 ""
set BD_FILES_BNL181 ""
set BD_FILES_BNL182 ""

set XCI_FILES_VERSALPREMIUM ""
set VHDL_FILES_VERSALPREMIUM ""
set VHDL_FILES_VPK120 ""
set VHDL_FILES_FLX155 ""
set SIM_FILES_VERSALPREMIUM ""
set BD_FILES_VPK120 ""
set BD_FILES_FLX155 ""

set XDC_FILES_VC709 ""
set XDC_FILES_HTG710 ""
set XDC_FILES_BNL711 ""
set XDC_FILES_BNL712 ""
set XDC_FILES_VCU128 ""
set XDC_FILES_XUPP3R_VU9P ""
set XDC_FILES_BNL801 ""
set XDC_FILES_VMK180 ""
set XDC_FILES_BNL181 ""
set XDC_FILES_BNL182 ""
set XDC_FILES_VPK120 ""
set XDC_FILES_FLX155 ""
set RELATIVE_XDC_FILES ""
set XDCUNMANAGED_FILES ""
set MEM_FILES ""


#This property will be handled only in vivado_import_generic. Set to true to import also simulation only files. 
#Sim files that contain UVVM libraries are currently unsupported by Vivado and generate syntax errors. Use Modelsim/Questasim instead.
set VIVADO_ADD_SIM_FILES false
set ENGINEERING_SAMPLE false

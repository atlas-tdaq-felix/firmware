source ./clear_filesets.tcl

set filesets [glob -directory "../filesets" -- "*.tcl"]

foreach f $filesets {
    source "$f"
}

set VHDL_FILES [concat \
$VHDL_FILES \
$VHDL_FILES_V7 \
$VHDL_FILES_KU \
$VHDL_FILES_VU9P \
$VHDL_FILES_VU37P \
$VHDL_FILES_VERSAL \
]

set SIM_FILES [concat \
$SIM_FILES \
$SIM_FILES_V7 \
$SIM_FILES_KU \
$SIM_FILES_VU9P \
$SIM_FILES_VU37P \
$SIM_FILES_VERSAL \
]

#Make SIM_FILES relative to sources as well
foreach s $SIM_FILES {
    set VHDL_FILES [concat $VHDL_FILES "../simulation/$s"]
}

foreach v $VHDL_FILES {
    puts $v
#    puts "Processing $v with vhdl-style-guide"
#    exec ../scripts/vhdl-style-guide/bin/vsg -f $v -c ../scripts/vsg-styles/felix.yaml --fix
}

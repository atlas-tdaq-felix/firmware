
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Thei Wijnen
#               mtrovato
#               Frans Schreuder
#               Ohad Shaked
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Set the supportfiles directory path
set scriptdir [pwd]
set firmware_dir $scriptdir/../../
# Vivado project directory:
set project_dir $firmware_dir/Projects/$PROJECT_NAME

if {$BOARD_TYPE == 711 || $BOARD_TYPE == 712} {
	set PART xcku115-flvf1924-2-e
	set core_dir $firmware_dir/sources/ip_cores/kintexUltrascale/
	set XCI_FILES [concat $XCI_FILES $XCI_FILES_KU]
	set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_KU]
	set SIM_FILES [concat $SIM_FILES $SIM_FILES_KU]
    set BD_FILES [concat $BD_FILES $BD_FILES_KU]
} elseif {$BOARD_TYPE == 709 || $BOARD_TYPE == 710} {
	set PART xc7vx690tffg1761-2
	set core_dir $firmware_dir/sources/ip_cores/virtex7/
	set XCI_FILES [concat $XCI_FILES $XCI_FILES_V7]
	set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_V7]
	set SIM_FILES [concat $SIM_FILES $SIM_FILES_V7]
    set BD_FILES [concat $BD_FILES $BD_FILES_V7]
} elseif {$BOARD_TYPE == 128} {
	if {$ENGINEERING_SAMPLE == true} {
		set PART xcvu37p-fsvh2892-2-e-es1
	} else {
		set PART xcvu37p-fsvh2892-2-e
	}
	set core_dir $firmware_dir/sources/ip_cores/VU37P/
	set XCI_FILES [concat $XCI_FILES $XCI_FILES_VU37P]
	set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_VU37P]
	set SIM_FILES [concat $SIM_FILES $SIM_FILES_VU37P]
    set BD_FILES [concat $BD_FILES $BD_FILES_VU37P]
} elseif {$BOARD_TYPE == 800} {
	set PART xcvu9p-flgb2104-2-e
	set core_dir $firmware_dir/sources/ip_cores/VU9P/
	set XCI_FILES [concat $XCI_FILES $XCI_FILES_VU9P]
	set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_VU9P]
	set SIM_FILES [concat $SIM_FILES $SIM_FILES_VU9P]
    set BD_FILES [concat $BD_FILES $BD_FILES_VU9P]
} elseif {$BOARD_TYPE == 801} {
	set PART xcvu9p-flgc2104-2-e
	set core_dir $firmware_dir/sources/ip_cores/VU9P/
	set XCI_FILES [concat $XCI_FILES $XCI_FILES_VU9P]
	set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_VU9P]
	set SIM_FILES [concat $SIM_FILES $SIM_FILES_VU9P]
    set BD_FILES [concat $BD_FILES $BD_FILES_VU9P]
} elseif {$BOARD_TYPE == 180} {
	if {$ENGINEERING_SAMPLE == true} {
		set PART xcvm1802-vsva2197-2MP-e-S-es1
	} else {
		set PART xcvm1802-vsva2197-2MP-e-S
	}
	set core_dir $firmware_dir/sources/ip_cores/VMK180/
	set XCI_FILES [concat $XCI_FILES $XCI_FILES_VERSAL]
	set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_VERSAL]
	set SIM_FILES [concat $SIM_FILES $SIM_FILES_VERSAL]
    set BD_FILES [concat $BD_FILES $BD_FILES_VMK180]
} elseif {$BOARD_TYPE == 181} {
	set PART xcvm1802-vsva2197-1MP-e-S-es1
	set core_dir $firmware_dir/sources/ip_cores/BNL181/
	set XCI_FILES [concat $XCI_FILES $XCI_FILES_VERSAL]
	set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_VERSAL]
	set SIM_FILES [concat $SIM_FILES $SIM_FILES_VERSAL]
    set BD_FILES [concat $BD_FILES $BD_FILES_BNL181]
} elseif {$BOARD_TYPE == 182} {
	set PART xcvm1802-vsva2197-1MP-e-S
	set core_dir $firmware_dir/sources/ip_cores/BNL182/
	set XCI_FILES [concat $XCI_FILES $XCI_FILES_VERSAL]
	set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_VERSAL]
	set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_BNL182]
	set SIM_FILES [concat $SIM_FILES $SIM_FILES_VERSAL]
    set BD_FILES [concat $BD_FILES $BD_FILES_BNL182]
} elseif {$BOARD_TYPE == 120} {
	set PART xcvp1202-vsva2785-2MHP-e-S
	set core_dir $firmware_dir/sources/ip_cores/VPK120/
	set XCI_FILES [concat $XCI_FILES $XCI_FILES_VERSALPREMIUM]
	set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_VERSALPREMIUM]
	set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_VPK120]
	set SIM_FILES [concat $SIM_FILES $SIM_FILES_VERSALPREMIUM]
    set BD_FILES [concat $BD_FILES $BD_FILES_VPK120]
} elseif {$BOARD_TYPE == 155} {
    set PART xcvp1552-vsva3340-2MHP-e-S
    set core_dir $firmware_dir/sources/ip_cores/FLX155/
    set XCI_FILES [concat $XCI_FILES $XCI_FILES_VERSALPREMIUM]
	set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_VERSALPREMIUM]
	set VHDL_FILES [concat $VHDL_FILES $VHDL_FILES_FLX155]
	set SIM_FILES [concat $SIM_FILES $SIM_FILES_VERSALPREMIUM]
    set BD_FILES [concat $BD_FILES $BD_FILES_FLX155]
} else {
	puts "Error: BOARD_TYPE should be 128, 709, 710, 711, 712, 128, 800, 801, 180, 181, 182 or 120"
	return;
}

if {$BOARD_TYPE == 709} {
	set XDC_FILES $XDC_FILES_VC709
}
if {$BOARD_TYPE == 710} {
	set XDC_FILES $XDC_FILES_HTG710
}
if {$BOARD_TYPE == 711} {
	set XDC_FILES $XDC_FILES_BNL711
}
if {$BOARD_TYPE == 712} {
	set XDC_FILES $XDC_FILES_BNL712
}
if {$BOARD_TYPE == 128} {
	set XDC_FILES $XDC_FILES_VCU128
}
if {$BOARD_TYPE == 800} {
	set XDC_FILES $XDC_FILES_XUPP3R_VU9P
}
if {$BOARD_TYPE == 801} {
	set XDC_FILES $XDC_FILES_BNL801
}
if {$BOARD_TYPE == 180} {
	set XDC_FILES $XDC_FILES_VMK180
}
if {$BOARD_TYPE == 181} {
	set XDC_FILES $XDC_FILES_BNL181
}
if {$BOARD_TYPE == 182} {
	set XDC_FILES $XDC_FILES_BNL182
}
if {$BOARD_TYPE == 120} {
	set XDC_FILES $XDC_FILES_VPK120
}
if {$BOARD_TYPE == 155} {
	set XDC_FILES $XDC_FILES_FLX155
}



close_project -quiet
create_project -force -part $PART $PROJECT_NAME $firmware_dir/Projects/$PROJECT_NAME
if {$BOARD_TYPE == 120} {
    set_property board_part xilinx.com:vpk120:part0:1.2 [current_project]
}
#Contains an IP core for the block design of the FLX182, for the Versal virtual network device between PS and PCIe
if {$BOARD_TYPE == 182} {
    set_property ip_repo_paths $firmware_dir/sources/ip_cores/BNL182/ip_repo [current_project]
}
update_ip_catalog -rebuild -scan_changes
set_property target_language VHDL [current_project]
set_property default_lib work [current_project]
set_property XPM_LIBRARIES {XPM_CDC XPM_MEMORY XPM_FIFO} [current_project]



foreach VHDL_FILE $VHDL_FILES {
	set file_path [file normalize ${firmware_dir}/sources/${VHDL_FILE}]
	if {!($file_path in [get_files])} {
		read_vhdl -library work $file_path
		set_property FILE_TYPE {VHDL 2019} [get_files ${file_path}]
	}		
}

foreach VHDL_FILE $DISABLED_VHDL_FILES {
	set file_path [file normalize ${firmware_dir}/sources/${VHDL_FILE}]
	if {!($file_path in [get_files])} {
		read_vhdl -library work $file_path
		set_property FILE_TYPE {VHDL 2019} [get_files ${file_path}]
        set_property is_enabled false  [get_files ${file_path}]
	}		
}



foreach VERILOG_FILE $VERILOG_FILES {
	set file_path [file normalize ${firmware_dir}/sources/${VERILOG_FILE}]
	if {!($file_path in [get_files])} {
		read_verilog -library work $file_path
	}
}

foreach XCI_FILE $XCI_FILES {
	import_ip -quiet ${core_dir}/${XCI_FILE}
}


foreach BD_FILE $BD_FILES {
    import_files -norecurse ${core_dir}/${BD_FILE}
    set WRAPPER_FILE [make_wrapper -files [get_files $BD_FILE] -top]
    add_files -norecurse $WRAPPER_FILE
}

foreach XDC_FILE $XDC_FILES {
	read_xdc -verbose ${firmware_dir}/constraints/${XDC_FILE}
}

foreach XDCUNMANAGED_FILE $XDCUNMANAGED_FILES {
	read_xdc -verbose -unmanaged ${firmware_dir}/constraints/${XDCUNMANAGED_FILE}
}

#File name must be the same as the entity it is relative to. 
foreach RELATIVE_XDC_FILE $RELATIVE_XDC_FILES {
    read_xdc -ref ${RELATIVE_XDC_FILE} ../../constraints/${RELATIVE_XDC_FILE}.xdc  
}

foreach MEM_FILE $MEM_FILES {
    add_files -norecurse ${firmware_dir}/sources/${MEM_FILE}
}

set_property SOURCE_SET sources_1 [get_filesets sim_1]

#These files are for synthesis only, they must have a replacement for simulation purposes.
foreach EXCLUDE_SIM_FILE $EXCLUDE_SIM_FILES {
	set_property used_in_simulation false [get_files  ${firmware_dir}/sources/$EXCLUDE_SIM_FILE]
}

#by default we are not including simulation files since UVVM is unsupported by the Vivado simulator
if {$VIVADO_ADD_SIM_FILES == true} {
    foreach SIM_FILE $SIM_FILES {
        add_files -fileset sim_1 -force -norecurse ${firmware_dir}/simulation/$SIM_FILE
        set_property library work [get_files  ${firmware_dir}/simulation/$SIM_FILE]
        set_property file_type {VHDL 2019} [get_files  ${firmware_dir}/simulation/$SIM_FILE]
    }
    
    foreach WCFG_FILE $WCFG_FILES {
        add_files -fileset sim_1 -force -norecurse ${firmware_dir}/simulation/$WCFG_FILE
    }

    if {[info exists TOPLEVEL_SIM]} {
        set_property top $TOPLEVEL_SIM [get_filesets sim_1]
    }
    update_compile_order -fileset sim_1

    set_property -name {xsim.simulate.runtime} -value {5 us} -objects [current_fileset -simset]
}


close [ open $firmware_dir/constraints/felix_probes.xdc w ]
read_xdc -verbose $firmware_dir/constraints/felix_probes.xdc
set_property target_constrs_file $firmware_dir/constraints/felix_probes.xdc [current_fileset -constrset]




set_property top $TOPLEVEL [current_fileset]
upgrade_ip [get_ips -exclude_bd_ips]
generate_target all [get_ips -exclude_bd_ips]
export_ip_user_files -of_objects [get_ips -exclude_bd_ips] -no_script -force -quiet
set MAX_IP_RUNS 6
set IP_RUNS 0
foreach ip [get_ips -exclude_bd_ips] {
    set run [create_ip_run [get_ips $ip]]
    launch_run $run
    if { $IP_RUNS < $MAX_IP_RUNS } {
        set IP_RUNS [expr $IP_RUNS + 1]
    } else {
        #Wait on run, only if IP_RUNS == 6, run 6 ip cores parallel.
        wait_on_run $run
        set IP_RUNS 0
    }
}
#wait for the last run.
wait_on_run $run
export_simulation -of_objects [get_ips -exclude_bd_ips] -force -quiet

#MT disable KCU_RXBUF_PMA_QPLL_4CH_LPGBT.xdc ... ugly, very ugly. This procedure used as backup if DISABLE_LOC_XDC=1 doesn't work
#set_property IS_MANAGED false [get_files KCU_RXBUF_PMA_QPLL_4CH_LPGBT.xci]
#set_property is_enabled false [get_files ${project_dir}/${PROJECT_NAME}.srcs/sources_1/ip/KCU_RXBUF_PMA_QPLL_4CH_LPGBT/synth/KCU_RXBUF_PMA_QPLL_4CH_LPGBT.xdc]
#reset_runs KCU_RXBUF_PMA_QPLL_4CH_LPGBT_synth_1
#launch_runs  KCU_RXBUF_PMA_QPLL_4CH_LPGBT_synth_1
#

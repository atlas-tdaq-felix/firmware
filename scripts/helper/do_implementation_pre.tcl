
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               William Wulff
#               Elena Zhivun
#               Thei Wijnen
#               Frans Schreuder
#               Marius Wensing
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#do_implementation_pre.tcl
#some defaults:
#Install pre-commit hook to apply VSG on whitespace
set scriptdir [pwd]
if { [file exists ../../.git/hooks] == 1 } {
    cd ../../.git/hooks
    if { [file exists pre-commit] != 1} {               
        puts "Creating pre-commit hook to run VSG before any GIT commit"
        file link -symbolic pre-commit ../../.hooks/pre-commit
    } else {
        puts "pre-commit hook already exists."
    }
    cd $scriptdir
} else {
    puts "no git hooks directory found, omitting creation of symbolic link"
}

set FIRMWARE_MODE_GBT   0        
set FIRMWARE_MODE_FULL  1         
set FIRMWARE_MODE_LTDB  2
set FIRMWARE_MODE_FEI4  3         
set FIRMWARE_MODE_PIXEL 4        
set FIRMWARE_MODE_STRIP 5         
set FIRMWARE_MODE_FELIG_GBT   6             
set FIRMWARE_MODE_FMEMU 7
set FIRMWARE_MODE_MROD  8
set FIRMWARE_MODE_LPGBT 9
set FIRMWARE_MODE_INTERLAKEN  10
set FIRMWARE_MODE_FELIG_LPGBT 11
set FIRMWARE_MODE_HGTD_LUMI 12
set FIRMWARE_MODE_BCM_PRIME 13
set FIRMWARE_MODE_FELIG_PIXEL 14
set FIRMWARE_MODE_FELIG_STRIP 15
set FIRMWARE_MODE_WUPPER 16

set STOP_TO_ADD_ILA 0

set GENERATE_FM_WRAP false
set GENERATE_GBT true

#set 4.8 Gb/s
set FULL_HALFRATE false

# JBSC settings
set BLOCKSIZE 1024

# 32B trailer: Use 32b trailer format by default
set CHUNK_TRAILER_32B true

set generate_IC_EC_TTC_only false
set includeDirectMode false
set include_strips_ila false
set GENERATE_TRUNCATION_MECHANISM true
set useToHostGBTdataEmulator true
set useToFrontendGBTdataEmulator true

set NUMBER_OF_INTERRUPTS 8
#5 descriptors ToHost, 1 FromHost.
set NUMBER_OF_DESCRIPTORS 6
set GBT_MAPPING 1 
set debug_mode false
set CREnableFromHost true
set ENDPOINTS 2

set GBT_NUM 24
set OPTO_TRX 4
set FE_EMU_EN 0
set NUMCH 12
set KCU_LOWER_LATENCY 0

#New CR FromHost data format in which the length is defined in a 5 bit field, allowing odd number of bytes
set FROMHOST_LENGTH_IS_5BIT true

#Semistatic is determined by disabling certain e-groups using the 
#variables below, the variable SemiStatic will only be reflected
#in the filename of the output.
set SemiStatic false
# Defining the Egroup's capabilities
# ToHost - Egroup 0
set IncludeDecodingEpath2_HDLC   7'b1111111 
set IncludeDecodingEpath2_8b10b  7'b1111111
set IncludeDecodingEpath4_8b10b  7'b1111111
set IncludeDecodingEpath8_8b10b  7'b1111111
set IncludeDecodingEpath16_8b10b 7'b1111111
set IncludeDecodingEpath32_8b10b 7'b1111111
set IncludeDirectDecoding        7'b0000000

set IncludeEncodingEpath2_HDLC   5'b11111 
set IncludeEncodingEpath2_8b10b  5'b11111
set IncludeEncodingEpath4_8b10b  5'b11111
set IncludeEncodingEpath8_8b10b  5'b11111
set IncludeDirectEncoding        5'b00000
set INCLUDE_TTC                  5'b11111
set INCLUDE_RD53                 5'b00000

set DEBUGGING_RD53               false
set RD53Version                  "A"
set ISTESTBEAM                   false
# support HDLC inter-packet delay insertion
set SUPPORT_HDLC_DELAY           true

# Per channel timeout value expressed in n_bits at 40 MHz
set toHostTimeoutBitn 16

## TTC settings
# -- generate TTC data emulator
# true: switches the source of the TTC data to be a 10-bit counter 
set generateTTCemu true
# -- TTC test mode
# true: initializes configuration registers to TTC loopback test
set TTC_test_mode false

set TTC "1'h0"
set LTITTC "1'h1"
set TTC_SYS_SEL $TTC


#set to false to use TTCfx3 (Si5345) refclk, true for onboard Si5324.
set USE_Si5324_RefCLK false

set GREFCLK "1'h1"
set MGTREFCLK  "1'h0"

## GBT Wrapper generics
# -- set the MGTREFCLOCK
#set GTHREFCLK_SEL $GREFCLK
set GTHREFCLK_SEL $MGTREFCLK

set CPLL "1'h0"
set QPLL "1'h1"

set PLL_SEL $QPLL

set use_backup_clk true
set automatic_clock_switch true

set PCIE_LANES 8
set DATA_WIDTH 256

set NUM_LEDS 8
#Number of PCIe endpoints in the design (Set to 2 for BNL711 / BNL712)
set ENDPOINTS 1

#Number of GTREFCLK inputs
set GTREFCLKS 2


#Set to true to do a few more rounds of phys_opt_design and even route_design 
#if timing is not met (se do_implementation_finish.tcl)
set KEEP_TRYING_TO_MEET_TIMING false

#Set to true to add support for Xilinx Virtual Cable (XVC) Over PCI Express in Wupper
set ENABLE_XVC false

#Set to false to set BUILD_TIME to the clock, true for GIT_COMMIT_TIME. With false, the build is reproducible.
set DETERMINISTIC_BUILD_TIME false

#If the design doesn't meet timing, try another RQS implementation run to meet timing.
set ENABLE_RQS false

source ../helper/vivado_set_severity.tcl


#!/bin/bash

# get script directory
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# use virtual environment
VENV_DIR=${SCRIPT_DIR}/.venv

# check if virtual environment exists
if ! [ -d ${VENV_DIR} ]; then
	python -m venv ${VENV_DIR}
fi

# activate virtual environment
source ${VENV_DIR}/bin/activate

# install xlsxwriter
pip install xlsxwriter

# run the python script
python ${SCRIPT_DIR}/generate_util_xlsx.py $*

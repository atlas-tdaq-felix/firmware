# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2024 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Marius Wensing
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import sys
import xlsxwriter
import itertools
import os

def usage():
	print("usage:")
	print("    generate_util_xlsx.py <filename>")
	print()
	print("    <filename> is the path to the Vivado utilization report")
	print("               (generated with generate_utilization -hierarchical -hierarchical_depth 8 -file <filename>)")

def main(argv):
	# check if no argument has been supplied
	if len(argv) == 0:
		usage()
		return 1

	# check for --help/-h
	if any(arg in argv[0] for arg in ["-h", "-help"]):
		usage()
		return 0

	# get the filenames
	txtfilename = argv[0]
	xlsxfilename = os.path.splitext(txtfilename)[0] + ".xlsx"

	with open(txtfilename, "r") as f:
		workbook = xlsxwriter.Workbook(xlsxfilename)
		worksheet = workbook.add_worksheet("Utilization")
		worksheet.outline_settings(True, False, False, False)

		# read line by line
		skip = 2
		row = 0
		while True:
			line = f.readline()

			if not line:
				break

			# scan for beginning of table
			if line.startswith('+'):
				skip = skip - 1

			# skip header lines or lines without pipe (|)
			if (skip > 1) or not ('|' in line):
				continue

			fields = line.split('|')[1:-1]

			if skip == 1:
				bold = workbook.add_format({"bold": 1})

				# we have the header row here
				for (col, field) in enumerate(fields):
					worksheet.write(row, col, field.strip(), bold)
			else:
				# we have a regular row here
				# let's try to find out our hierarchy level
				hier_level = int((sum(1 for _ in itertools.takewhile(str.isspace, fields[0]))-1) / 2)
				hidden = hier_level > 1
				for (col, field) in enumerate(fields):
					indent = workbook.add_format({"indent": hier_level})
					if col == 0:
						worksheet.write(row, col, field.strip(), indent)
					else:
						worksheet.write(row, col, field.strip())

					#worksheet.set_row(row, None, None, {"level": hier_level, "collapsed": True, "hidden": hidden})
					worksheet.set_row(row, None, None, {"level": hier_level, "hidden": hidden})

			# next row
			row = row + 1

		worksheet.autofit()
		workbook.close()

		return 0

if __name__ == "__main__":
	sys.exit(main(sys.argv[1:]))

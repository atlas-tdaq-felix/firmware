
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Thei Wijnen
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#Issue's that are rebranded

#NULL port ignored
set_msg_config -id {Synth 8-506} -new_severity {INFO}
#Checking whether we already executed this script, we can detect that if we get a warning Common 17-1361 from the previous line.
#You have specified a new message control rule that is equivalent to an existing rule with attributes
if { [get_msg_config -id {Common 17-1361} -count] == 0 } {
    set_msg_config -id {Synth 8-3919} -new_severity {INFO}
    #Tying undriven pin to constant 0
    set_msg_config -id {Synth 8-3295} -new_severity {INFO}
    #Unsused sequential element was removed
    set_msg_config -id {Synth 8-6014} -new_severity {INFO}
    #Register is trimmed in number of bits
    set_msg_config -id {Synth 8-3936} -new_severity {INFO}
    #RAM from abstract data type for this pattern is not supported, flipflops will be used.
    set_msg_config -id {Synth 8-5858} -new_severity {INFO}
    set_msg_config -id {Synth 8-5856} -new_severity {INFO}
    #Unused toplevel generic
    set_msg_config -id {Synth 8-3301} -new_severity {INFO}
    set_msg_config -id {Synth 8-3819} -new_severity {INFO}
    #Output port is driven by constant
    set_msg_config -id {Synth 8-3917} -new_severity {INFO}
    #Signal does not have a driver. This happens a lot for unused registers in the registermap. Discutable whether this is valid for other projects.
    set_msg_config -id {Synth 8-3848} -new_severity {INFO}
    set_msg_config -id {Synth 8-3848} -limit 1000
    #design ... has unconnected port
    set_msg_config -id {Synth 8-3331} -new_severity {INFO}
    #root scope declaration is not allowed in verilog 95/2K mode
    set_msg_config -id {Synth 8-2644} -new_severity {INFO}
    #Nets in constraints were not found
    set_msg_config -id {Vivado 12-507} -new_severity {INFO}
    #Clocks in constraints were not found
    set_msg_config -id {Vivado 12-627} -new_severity {INFO}
    set_msg_config -id {Vivado 12-4739} -new_severity {INFO}
    #Ports in constraints were not found
    set_msg_config -id {Vivado 12-508} -new_severity {INFO}
    set_msg_config -id {Vivado 12-584} -new_severity {INFO}
    #Inferring latch
    set_msg_config -id {Synth 8-327} -new_severity {CRITICAL WARNING}
    #Multidriven net will fail later on Opt Design anyway, let's stop already during synthesis
    set_msg_config -id {Synth 8-3352} -new_severity {ERROR}
    #Object in constraints not found
    #set_msg_config -id {Common 17-55} -new_severity {WARNING}
    #Creating clock with multiple sources
    set_msg_config -id {Constraints 18-633} -new_severity {INFO}
    #Component port with null array found, Will be ignored
    set_msg_config -id {Synth 8-6778} -new_severity {INFO}
    #Vivado Synthesis ignores library specification for Verilog or SystemVerilog files.
    set_msg_config -id {filemgmt 56-99} -new_severity {INFO}
    #Use of 'set_multicycle_path' with '-hold' is not supported by synthesis. The constraint will not be passed to synthesis.
    set_msg_config -id {Designutils 20-1567} -new_severity {INFO}
    #[Synth 8-6060] Synth Warning: [XPM_MEMORY 30-25] MESSAGE_CONTROL (1) specifies simulation message reporting, but any potential collisions reported for this configuration should be further investigated in netlist timing simulations for improved accuracy.   ["/eda/fpga/xilinx/Vivado/2024.2/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv":573]
    set_msg_config -id {Synth 8-6060} -new_severity {INFO}
    #[Vivado 12-180] No cells matched '*'. ["/localstore/et/franss/felix/firmware/constraints/felix_top_BNL712.xdc":138]
    set_msg_config -id {Vivado 12-180} -new_severity {INFO}
    #[Constraints 18-929] The number of objects selected for the constraint defined at line 130 of constraint file '"/localstore/et/franss/felix/firmware/constraints/timing_constraints.xdc"' exceeds the default limit of 10000, this constraint will be excluded from synthesis.
    set_msg_config -id {Constraints 18-929} -new_severity {INFO}
    #[Synth 8-3332] Sequential element (gen_gtwizard_gthe3_top.KCU_NORXBUF_PCS_CPLL_1CH_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_tx_buffer_bypass_internal.gen_single_instance.gtwiz_buffbypass_tx_inst/FSM_onehot_gen_gtwiz_buffbypass_tx_main.gen_auto_mode.sm_buffbypass_tx_reg[0]) is unused and will be removed from module KCU_NORXBUF_PCS_CPLL_1CH_gtwizard_top.
    set_msg_config -id {Synth 8-3332} -new_severity {INFO}
    #[Synth 8-7080] Parallel synthesis criteria is not met
    set_msg_config -id {Synth 8-7080} -new_severity {INFO}
    #[Synth 8-11581] system task call 'printtimescale' not supported ["/localstore/et/franss/felix/firmware/Projects/FLX712_FELIX/FLX712_FELIX.gen/sources_1/ip/pcie3_ultrascale_7038/source/pcie3_ultrascale_7038_pcie3_uscale_core_top.v":1746]
    set_msg_config -id {Synth 8-11581} -new_severity {INFO}
    #[Designutils 20-1281] Could not find module 'LTI_FE_Sync'. The XDC file /localstore/et/franss/felix/firmware/constraints/LTI_FE_Sync.xdc will not be read for this module.
    set_msg_config -id {Designutils 20-1281} -new_severity {INFO}
    #[Designutils 20-1280] Could not find module 'gtwizard_fullmode_cpll_ku'. The XDC file /localstore/et/franss/felix/firmware/Projects/FLX712_FELIX/FLX712_FELIX.gen/sources_1/ip/gtwizard_fullmode_cpll_ku/synth/gtwizard_fullmode_cpll_ku.xdc will not be read for any cell of this module.
    set_msg_config -id {Designutils 20-1280} -new_severity {INFO}
    #[Vivado_Tcl 4-921] Waiver ID 'CDC-1' -to list should not be empty. ["/localstore/et/franss/felix/firmware/Projects/FLX712_FELIX/FLX712_FELIX.gen/sources_1/ip/pcie3_ultrascale_7038/source/pcie3_ultrascale_7038-PCIE_X0Y1.xdc":209]
    set_msg_config -id {Vivado_Tcl 4-921} -new_severity {INFO}
    set_msg_config -id {Vivado_Tcl 4-919} -new_severity {INFO}
    #[XPM_CDC_GRAY: TCL-1000] The source and destination clocks are the same. 
    set_msg_config -id {XPM_CDC_GRAY: TCL-1000} -new_severity {INFO}
    
    
}

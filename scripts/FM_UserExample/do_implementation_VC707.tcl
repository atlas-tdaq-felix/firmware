
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set IMPL_RUN [get_runs impl*]
set SYNTH_RUN [get_runs synth*]
set scriptdir [pwd]
set HDLDIR $scriptdir/../
set systemTime [clock seconds]
foreach design [get_designs] {
   puts "Closing design: $design"
   current_design $design
   close_design
}

set RECOVER_CLK_FROM_RX_GBT false

reset_run $SYNTH_RUN

set_property generic "
GTX_TRANSCEIVERS=true \
NUM_LINKS=1 \
RECOVER_CLK_FROM_RX_GBT=$RECOVER_CLK_FROM_RX_GBT \
" [current_fileset]

if { $RECOVER_CLK_FROM_RX_GBT==true } {
    set_property SEVERITY WARNING [get_drc_checks REQP-52] 
    set CLKSRC "GBT_RECOVERED_CLK"
} else {
    set CLKSRC "LOCAL_CLK"
}

launch_runs $SYNTH_RUN  -jobs 8
#wait_on_run $SYNTH_RUN
#open_run $SYNTH_RUN
#return
launch_runs $IMPL_RUN -jobs 8

wait_on_run $IMPL_RUN
set TIMESTAMP [clock format $systemTime -format {%y%m%d_%H_%M}]

open_run $IMPL_RUN
current_run $IMPL_RUN

set FileName FMUserExample_VC707_${CLKSRC}_${TIMESTAMP}

write_bitstream -force $HDLDIR/output/${FileName}.bit

cd $HDLDIR/output/


set pass [expr {[get_property SLACK [get_timing_paths -delay_type min_max]] >= 0}]
set report [report_timing -slack_lesser_than 0 -return_string -nworst 10]
set util [report_utilization -return_string]
set slack [get_property SLACK [get_timing_paths -delay_type min_max]]
set GenericFileData "
Timing met:                   $pass
Wost slack:                   $slack\n\n
Timing Report:\n
$report\n
Utilization Report:\n
$util\n"

set GenericsFileName "${FileName}_generics_timing.txt"
set GenericsFileId [open $GenericsFileName "w"]
puts -nonewline $GenericsFileId $GenericFileData
close $GenericsFileId

set BitFile ${FileName}.bit
set IMPL_DIR [get_property DIRECTORY [current_run]]

write_cfgmem -force -format MCS -size 128 -interface BPIx16 -loadbit "up 0x00000000 ${FileName}.bit" ${FileName}.mcs
if {[file exists $IMPL_DIR/debug_nets.ltx] == 1} {
   file copy -force $IMPL_DIR/debug_nets.ltx ${FileName}_debug_nets.ltx
   
   if { [catch { exec tar -zcf ${HDLDIR}/output/${FileName}.tar.gz ${FileName}.bit ${FileName}.mcs ${FileName}_debug_nets.ltx ${GenericsFileName}} msg] } {
        puts "error creating archive ${FileName}.tar.gz"
   }
} else {
    if { [catch { exec tar -zcf ${HDLDIR}/output/${FileName}.tar.gz ${FileName}.bit ${FileName}.mcs ${GenericsFileName}} msg] } {
        puts "error creating archive ${FileName}.tar.gz"
    }
}
cd $scriptdir



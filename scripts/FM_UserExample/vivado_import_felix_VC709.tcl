
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#
#	File import script for the FELIX hdl project
#
#

#Script Configuration

set proj_name FMUserExample_VC709
# Set the supportfiles directory path
set scriptdir [pwd]
set firmware_dir $scriptdir/../
# Vivado project directory:
set project_dir $firmware_dir/Projects/$proj_name
file mkdir $project_dir
file mkdir $scriptdir/../output

#Close currently open project and create a new one. (OVERWRITES PROJECT!!)
close_project -quiet
set PART xc7vx690tffg1761-2
create_project -force -part xc7vx690tffg1761-2 $proj_name $firmware_dir/Projects/$proj_name

set_property target_language VHDL [current_project]
set_property default_lib work [current_project]

# ----------------------------------------------------------
# FELIX top module
# ----------------------------------------------------------
read_vhdl -library work $firmware_dir/src/example_design_virtex7/FM_UserExample.vhd

# -- I2C master
read_vhdl -library work $firmware_dir/src/example_design_virtex7/si5324_init.vhd
read_vhdl -library work $firmware_dir/src/example_design_virtex7/i2c.vhd

read_vhdl -library work $firmware_dir/src/example_design_virtex7/FM_example_clocking.vhd
read_vhdl -library work $firmware_dir/src/example_design_virtex7/FM_example_emuram.vhd
read_vhdl -library work $firmware_dir/src/example_design_virtex7/FM_example_FIFOctrl.vhd
read_vhdl -library work $firmware_dir/src/example_design_virtex7/FIFO34to34b.vhd
read_vhdl -library work $firmware_dir/src/FMchannelTXctrl.vhd
read_vhdl -library work $firmware_dir/src/crc.vhd
read_vhdl -library work $firmware_dir/src/pulse_pdxx_pwxx.vhd
read_vhdl -library work $firmware_dir/src/example_design_virtex7/fullmodetransceiver.vhd
#read_vhdl -library work $firmware_dir/src/example_design_virtex7/fullmodetransceiver_reset_fsm.vhd
read_vhdl -library work $firmware_dir/src/example_design_virtex7/FMTransceiverPackage.vhd

read_vhdl -library work $firmware_dir/src/example_design_virtex7/FELIX_gbt_package.vhd
read_vhdl -library work $firmware_dir/src/example_design_virtex7/gbt_rx_descrambler_21bit.vhd            
read_vhdl -library work $firmware_dir/src/example_design_virtex7/gbt_rx_descrambler_16bit.vhd            
read_vhdl -library work $firmware_dir/src/example_design_virtex7/gbt_rx_decoder_gbtframe_rs2errcor.vhd   
read_vhdl -library work $firmware_dir/src/example_design_virtex7/gbt_rx_decoder_gbtframe_elpeval.vhd     
read_vhdl -library work $firmware_dir/src/example_design_virtex7/gbt_rx_decoder_gbtframe_lmbddet.vhd     
read_vhdl -library work $firmware_dir/src/example_design_virtex7/gbt_rx_decoder_gbtframe_syndrom.vhd     
read_vhdl -library work $firmware_dir/src/example_design_virtex7/gbt_rx_decoder_gbtframe_chnsrch.vhd     
read_vhdl -library work $firmware_dir/src/example_design_virtex7/gbt_rx_decoder_gbtframe_errlcpoly.vhd   
read_vhdl -library work $firmware_dir/src/example_design_virtex7/gbt_rx_decoder_gbtframe_rsdec.vhd       
read_vhdl -library work $firmware_dir/src/example_design_virtex7/gbt_rx_decoder_gbtframe_deintlver.vhd   
read_vhdl -library work $firmware_dir/src/example_design_virtex7/gbt_rx_decoder_gbtframe_rsdec_sync.vhd  
read_vhdl -library work $firmware_dir/src/example_design_virtex7/gbt_rx_descrambler_FELIX.vhd            
read_vhdl -library work $firmware_dir/src/example_design_virtex7/gbt_rx_gearbox_FELIX_wi_rxbuffer.vhd    
read_vhdl -library work $firmware_dir/src/example_design_virtex7/FELIX_GBT_RX_AUTO_RST.vhd               
read_vhdl -library work $firmware_dir/src/example_design_virtex7/FELIX_GBT_RXSLIDE_FSM.vhd               
read_vhdl -library work $firmware_dir/src/example_design_virtex7/gbt_rx_decoder_FELIX.vhd                
read_vhdl -library work $firmware_dir/src/example_design_virtex7/gbtRx_wrap_FELIX.vhd                    
read_vhdl -library work $firmware_dir/src/example_design_virtex7/gbtRx_FELIX.vhd                         
read_vhdl -library work $firmware_dir/src/example_design_virtex7/FELIX_gbt_wrapper_no_gth.vhd      

import_ip $firmware_dir/src/ip_cores/virtex7/fifo_GBT2CR.xci
import_ip $firmware_dir/src/ip_cores/virtex7/clk_wiz_156_1.xci
import_ip $firmware_dir/src/ip_cores/virtex7/DPram_32b.xci
import_ip $firmware_dir/src/ip_cores/virtex7/fifo1KB_34bit.xci
import_ip $firmware_dir/src/ip_cores/virtex7/ila_gbt_rx.xci
import_ip $firmware_dir/src/ip_cores/virtex7/ila_fullmode_rx_tx.xci
import_ip $firmware_dir/src/ip_cores/virtex7/vio_si5324_init.xci
import_ip $firmware_dir/src/ip_cores/virtex7/fullmodetransceiver_core.xci

read_xdc -verbose $firmware_dir/constraints/FMUserExample_VC709.xdc

close [ open $firmware_dir/constraints/felix_probes.xdc w ]
read_xdc -verbose $firmware_dir/constraints/felix_probes.xdc
set_property target_constrs_file $firmware_dir/constraints/felix_probes.xdc [current_fileset -constrset]

set_property top FM_UserExample [current_fileset]

generate_target all [get_files  *.xci]

puts "INFO: Done!"

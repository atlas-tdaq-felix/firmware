#!/bin/bash
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.




cd ../../
for i in $(find ./ -name '*.sh')  $(find ./ -name '*.tcl') $(find ./ -name '*.do'); do
  
  
  AUTHORS=$(git log --no-merges --pretty=format:"%an" --follow $i | sed 's/Frans Philip Schreuder/Frans Schreuder/g' | sed 's/root/Alexander Paramonov/g' | awk '!a[$0]++'|tac)
  
  #YEAR=$(git log  --date=format:%Y  --diff-filter=A --follow $i |head -3|tail -1)
  #YEAR=${YEAR: -4}

  if [ -z "$AUTHORS" ]
  then
    echo "Ignoring file $i"
  else
    #perl -MHardware::Vhdl::Tidy -e "Hardware::Vhdl::Tidy::parse_commandline" $i > $i.tmp && mv $i.tmp $i
    
    if head -19 "$i" | grep -q "Licensed under the Apache License" ; then
      echo "$i already contains a license"
    else
      if [[ $(cat $i |head -1) == \#\!* ]]; then 
        SHEBANG=$(cat $i |head -1)
      else 
        SHEBANG=
      fi
  LIC="# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:"
    MODE=$(stat --format '%a' $i)
  
    #echo $YEAR: $i
    OLDIFS="$IFS"
    IFS=$'\n'
    for AUTHOR in $AUTHORS
    do
    #    echo "--!         $AUTHOR"
        LIC=$LIC"
#               $AUTHOR"
    done
    IFS="$OLDIFS"
    
    LIC=$LIC"
# 
#   Licensed under the Apache License, Version 2.0 (the \"License\");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an \"AS IS\" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License."
      #echo $i
      #echo "$LIC"
      
      echo -e "$SHEBANG\n$LIC\n" > $i.tmp
      if [ -z $SHEBANG ]; then
        cat $i >> $i.tmp
      else
        tail -n +2 $i >> $i.tmp
      fi
      mv $i.tmp $i
      chmod $MODE $i
    fi
  fi
done

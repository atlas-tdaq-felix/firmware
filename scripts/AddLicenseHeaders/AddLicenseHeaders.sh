#!/bin/bash
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.




cd ../../
for i in $(find ./sources -name '*.vhd') $(find ./simulation -name '*.vhd'); do
  
  
  AUTHORS=$(git log --no-merges --pretty=format:"%an" --follow $i | sed 's/Frans Philip Schreuder/Frans Schreuder/g' | sed 's/root/Alexander Paramonov/g' | awk '!a[$0]++'|tac)
  
  #YEAR=$(git log  --date=format:%Y  --diff-filter=A --follow $i |head -3|tail -1)
  #YEAR=${YEAR: -4}

  if [ -z "$AUTHORS" ]
  then
    echo "Ignoring file $i"
  else
    #perl -MHardware::Vhdl::Tidy -e "Hardware::Vhdl::Tidy::parse_commandline" $i > $i.tmp && mv $i.tmp $i
    
    if head -20 "$i" | grep -q "Licensed under the Apache License" ; then
      echo "$i already contains a license"
    else
  LIC="--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:"

  
    #echo $YEAR: $i
    OLDIFS="$IFS"
    IFS=$'\n'
    for AUTHOR in $AUTHORS
    do
    #    echo "--!         $AUTHOR"
        LIC=$LIC"
--!               $AUTHOR"
    done
    IFS="$OLDIFS"
    
    LIC=$LIC"
--! 
--!   Licensed under the Apache License, Version 2.0 (the \"License\");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an \"AS IS\" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License."
      #echo $i
      #echo "$LIC"
      
      echo -e "$LIC\n" > $i.tmp
      cat $i >> $i.tmp
      mv $i.tmp $i
    fi
  fi
done

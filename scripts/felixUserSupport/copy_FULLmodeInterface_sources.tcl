
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Julia Narevicius
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#
#	project sources copy script for FullModeUserInterface
#   
#	

# Set the support files directory path
set scriptdir [pwd]
# firmware directory:
set firmware_dir $scriptdir/../../
set doc_dir $firmware_dir/../Documents/FullMode/Requirements/
set core_dir $firmware_dir/sources/ip_cores/virtex7/
# sources directories:
set cr_sources_dir $firmware_dir/sources/centralRouter/
set fmtx_sources_dir $firmware_dir/sources/FullModeTransmitter/
set proj_sources_dir $firmware_dir/sources/felixUserSupport/FullModeUserInterface/
# source share directory path
set share_dir $scriptdir/FULLmodeInterfaceSources/
file mkdir $share_dir
file mkdir $share_dir/doc
file mkdir $share_dir/src
file mkdir $share_dir/src/gth_kintexultrascale
file mkdir $share_dir/src/ip_cores
#

# --------------------------------------------------------------------------
#      local FullModeUserInterface sources
# --------------------------------------------------------------------------
file copy -force $proj_sources_dir/gth_qpll_wrapper_proca.vhd                                         $share_dir/src/gth_qpll_wrapper_proca.vhd
file copy -force $proj_sources_dir/gth_tx_fullmode_rx_gbtmode_qpll_ultrascale.v                       $share_dir/src/gth_kintexultrascale/gth_tx_fullmode_rx_gbtmode_qpll_ultrascale.v 
file copy -force $proj_sources_dir/gth_tx_fullmode_rx_gbtmode_qpll_ultrascale_gthe3_channel_wrapper.v $share_dir/src/gth_kintexultrascale/gth_tx_fullmode_rx_gbtmode_qpll_ultrascale_gthe3_channel_wrapper.v  
file copy -force $proj_sources_dir/gth_tx_fullmode_rx_gbtmode_qpll_ultrascale_gthe3_common_wrapper.v  $share_dir/src/gth_kintexultrascale/gth_tx_fullmode_rx_gbtmode_qpll_ultrascale_gthe3_common_wrapper.v
file copy -force $proj_sources_dir/gth_tx_fullmode_rx_gbtmode_qpll_ultrascale_gtwizard_gthe3.v        $share_dir/src/gth_kintexultrascale/gth_tx_fullmode_rx_gbtmode_qpll_ultrascale_gtwizard_gthe3.v
file copy -force $proj_sources_dir/gth_tx_fullmode_rx_gbtmode_qpll_ultrascale_gtwizard_top.v          $share_dir/src/gth_kintexultrascale/gth_tx_fullmode_rx_gbtmode_qpll_ultrascale_gtwizard_top.v
file copy -force $proj_sources_dir/gtwizard_ultrascale_v1_6_bit_synchronizer.v                        $share_dir/src/gth_kintexultrascale/gtwizard_ultrascale_v1_6_bit_synchronizer.v
file copy -force $proj_sources_dir/gtwizard_ultrascale_v1_6_gthe3_channel.v                           $share_dir/src/gth_kintexultrascale/gtwizard_ultrascale_v1_6_gthe3_channel.v
file copy -force $proj_sources_dir/gtwizard_ultrascale_v1_6_gthe3_common.v                            $share_dir/src/gth_kintexultrascale/gtwizard_ultrascale_v1_6_gthe3_common.v
file copy -force $proj_sources_dir/gtwizard_ultrascale_v1_6_gthe3_cpll_cal.v                          $share_dir/src/gth_kintexultrascale/gtwizard_ultrascale_v1_6_gthe3_cpll_cal.v
file copy -force $proj_sources_dir/gtwizard_ultrascale_v1_6_gthe3_cpll_cal_freq_counter.v             $share_dir/src/gth_kintexultrascale/gtwizard_ultrascale_v1_6_gthe3_cpll_cal_freq_counter.v
file copy -force $proj_sources_dir/gtwizard_ultrascale_v1_6_gtwiz_buffbypass_rx.v                     $share_dir/src/gth_kintexultrascale/gtwizard_ultrascale_v1_6_gtwiz_buffbypass_rx.v
file copy -force $proj_sources_dir/gtwizard_ultrascale_v1_6_gtwiz_buffbypass_tx.v                     $share_dir/src/gth_kintexultrascale/gtwizard_ultrascale_v1_6_gtwiz_buffbypass_tx.v
file copy -force $proj_sources_dir/gtwizard_ultrascale_v1_6_gtwiz_reset.v                             $share_dir/src/gth_kintexultrascale/gtwizard_ultrascale_v1_6_gtwiz_reset.v
file copy -force $proj_sources_dir/gtwizard_ultrascale_v1_6_gtwiz_userclk_rx.v                        $share_dir/src/gth_kintexultrascale/gtwizard_ultrascale_v1_6_gtwiz_userclk_rx.v
file copy -force $proj_sources_dir/gtwizard_ultrascale_v1_6_gtwiz_userclk_tx.v                        $share_dir/src/gth_kintexultrascale/gtwizard_ultrascale_v1_6_gtwiz_userclk_tx.v
file copy -force $proj_sources_dir/gtwizard_ultrascale_v1_6_gtwiz_userdata_rx.v                       $share_dir/src/gth_kintexultrascale/gtwizard_ultrascale_v1_6_gtwiz_userdata_rx.v
file copy -force $proj_sources_dir/gtwizard_ultrascale_v1_6_gtwiz_userdata_tx.v                       $share_dir/src/gth_kintexultrascale/gtwizard_ultrascale_v1_6_gtwiz_userdata_tx.v
file copy -force $proj_sources_dir/gtwizard_ultrascale_v1_6_reset_inv_synchronizer.v                  $share_dir/src/gth_kintexultrascale/gtwizard_ultrascale_v1_6_reset_inv_synchronizer.v
file copy -force $proj_sources_dir/gtwizard_ultrascale_v1_6_reset_synchronizer.v                      $share_dir/src/gth_kintexultrascale/gtwizard_ultrascale_v1_6_reset_synchronizer.v


# --------------------------------------------------------------------------
#      FULL mode TX controller/user example sources 
# --------------------------------------------------------------------------
file copy -force $fmtx_sources_dir/FMchannelTXctrl.vhd   $share_dir/src/FMchannelTXctrl.vhd
file copy -force $fmtx_sources_dir/FullModeUserLogic.vhd $share_dir/src/FullModeUserLogic.vhd


# --------------------------------------------------------------------------
#      aux - CR sources 
# --------------------------------------------------------------------------
file copy -force $cr_sources_dir/pulse_fall_pw01.vhd $share_dir/src/pulse_fall_pw01.vhd
file copy -force $cr_sources_dir/pulse_pdxx_pwxx.vhd $share_dir/src/pulse_pdxx_pwxx.vhd


# --------------------------------------------------------------------------
#          FULL mode interface cores
# --------------------------------------------------------------------------
file copy -force $core_dir/FMuserFIFO.xci    $share_dir/src/ip_cores/FMuserFIFO.xci
file copy -force $core_dir/FMuserDataRAM.xci $share_dir/src/ip_cores/FMuserDataRAM.xci
file copy -force $core_dir/fm_user_ram.coe   $share_dir/src/ip_cores/fm_user_ram.coe


# --------------------------------------------------------------------------
#      all packages
# --------------------------------------------------------------------------
file copy -force $firmware_dir/sources/packages/centralRouter_package.vhd $share_dir/src/centralRouter_package.vhd



# --------------------------------------------------------------------------
#      constraints
# --------------------------------------------------------------------------



# --------------------------------------------------------------------------
#      simulation sources
# --------------------------------------------------------------------------

# --------------------------------------------------------------------------
#      Documents
# --------------------------------------------------------------------------
file copy -force $doc_dir/FullMode.pdf $share_dir/doc/FullMode.pdf

puts "INFO: Done!"








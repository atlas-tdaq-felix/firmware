
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set VHDL_FILES [concat $VHDL_FILES \
  ttc/ttc_decoder/ttc_decoder_core.vhd \
  ttc/ttc_decoder/cdr2a_b_clk.vhd \
  ttc/ttc_decoder/ttc_fmc_wrapper_xilinx.vhd \
  ttc/ttc_decoder/TTC_hamming_decoder_alme.vhd \
  ttc/ttc_busy/ttc_busy_or.vhd \
  ttc/ttc_busy/ttc_busy_limiter.vhd \
  encoding/ExtendedTestPulse.vhd \
  ttc/itk_trigtag_generator.vhd \
  ttc/ttc_decoder/ttc10b_to_ttc_type.vhd]
 
set SIM_FILES [concat $SIM_FILES \
  UVVMtests/tb/ttc_emulator_tb.vhd]
  
#Converted to XPM FIFOs 
#set XCI_FILES [concat $XCI_FILES \
#  TTCtoHostData_fwft.xci \
#  TTCtoHostData_reclock.xci]

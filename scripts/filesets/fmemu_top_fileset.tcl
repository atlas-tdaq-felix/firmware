
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
#               Mesfin Gebyehu
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
  
set VHDL_FILES [concat $VHDL_FILES \
  FullModeEmulator/FMEmu_top.vhd]
 
set XDC_FILES_VC709 [concat $XDC_FILES_VC709 \
  felix_sfp_VC709.xdc \
  felix_top_VC709.xdc]

set XDC_FILES_BNL711 [concat $XDC_FILES_BNL711 \
  felix_top_BNL711.xdc \
  felix_gbt_minipod_BNL712_transceiver_8ch.xdc \
  felix_gbt_minipod_BNL712_transceiver_24ch.xdc \
  timing_constraints_fmemu_ku.xdc]
  
set XDC_FILES_BNL712 [concat $XDC_FILES_BNL712 \
  felix_top_BNL712.xdc \
  felix_gbt_minipod_BNL712_transceiver_8ch.xdc \
  felix_gbt_minipod_BNL712_transceiver_24ch.xdc \
  felix_gbt_minipod_BNL712_transceiver_48ch.xdc \
  timing_constraints_fmemu_ku.xdc]

#set XDCUNMANAGED_FILES [concat $XDCUNMANAGED_FILES \
#  timing_constraints.xdc]

set XDC_FILES_BNL181 [concat $XDC_FILES_BNL181 \
  felix_top_BNL181.xdc]
  
set XDC_FILES_BNL182 [concat $XDC_FILES_BNL182 \
  felix_top_FLX182.xdc]

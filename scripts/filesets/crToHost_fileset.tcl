
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set VHDL_FILES [concat $VHDL_FILES \
  packages/FELIX_package.vhd \
  packages/axi_stream_package.vhd \
  templates/generated/pcie_package.vhd \
  shared/pulse_pdxx_pwxx.vhd \
  CRToHost/CRresetManager.vhd \
  CRToHost/MUXn.vhd \
  CRToHost/ReMuxN.vhd \
  CRToHost/ToHostAxiStreamController.vhd \
  CRToHost/ToHostAxiStream64Controller.vhd \
  CRToHost/HIFIFO.vhd \
  CRToHost/CRToHost.vhd \
  CRToHost/CRToHostdm.vhd \
  CRToHost/CRToHostPCIeManager.vhd \
  shared/xpm_uram_fifo_async.vhd \
  CRToHost/XoffMonitoring.vhd]  
  
set SIM_FILES [concat $SIM_FILES \
  UVVMtests/tb/CRToHost_tb.vhd \
  UVVMtests/tb/FELIXDataSink.vhd \
  ../sources/AxisUtils/Axis64Fifo.vhd \
  ../sources/AxisUtils/Axis32Fifo.vhd]
  
#set VHDL_FILES_V7 [concat $VHDL_FILES_V7 \
#    pcie/data_width_package_256.vhd]
#
#set VHDL_FILES_KU [concat $VHDL_FILES_KU \
#    pcie/data_width_package_256.vhd]
#
#set VHDL_FILES_VU37P [concat $VHDL_FILES_VU37P \
#    pcie/data_width_package_512.vhd]
#
#set VHDL_FILES_VU9P [concat $VHDL_FILES_VU9P \
#    pcie/data_width_package_512.vhd]
#
#set VHDL_FILES_VERSAL [concat $VHDL_FILES_VERSAL \
#    pcie/data_width_package_512.vhd]
    


# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Mesfin Gebyehu
#               mtrovato
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set VHDL_FILES [concat $VHDL_FILES \
  packages/FELIX_gbt_package.vhd \
  GBT/gbt_code/FELIX_GBT_RXSLIDE_FSM.vhd \
  GBT/gbt_code/FELIX_GBT_RX_AUTO_RST.vhd \
  GBT/gbt_code/gbt_rx_decoder_FELIX.vhd \
  GBT/gbt_code/gbt_rx_decoder_gbtframe_chnsrch.vhd \
  GBT/gbt_code/gbt_rx_decoder_gbtframe_deintlver.vhd \
  GBT/gbt_code/gbt_rx_decoder_gbtframe_elpeval.vhd \
  GBT/gbt_code/gbt_rx_decoder_gbtframe_errlcpoly.vhd \
  GBT/gbt_code/gbt_rx_decoder_gbtframe_lmbddet.vhd \
  GBT/gbt_code/gbt_rx_decoder_gbtframe_rs2errcor.vhd \
  GBT/gbt_code/gbt_rx_decoder_gbtframe_rsdec.vhd \
  GBT/gbt_code/gbt_rx_decoder_gbtframe_rsdec_sync.vhd \
  GBT/gbt_code/gbt_rx_decoder_gbtframe_syndrom.vhd \
  GBT/gbt_code/gbt_rx_descrambler_16bit.vhd \
  GBT/gbt_code/gbt_rx_descrambler_21bit.vhd \
  GBT/gbt_code/gbt_rx_descrambler_FELIX.vhd \
  GBT/gbt_code/gbt_rx_gearbox_FELIX_wi_rxbuffer.vhd \
  GBT/gbt_code/gbt_tx_encoder_FELIX.vhd \
  GBT/gbt_code/gbt_tx_encoder_gbtframe_polydiv.vhd \
  GBT/gbt_code/gbt_tx_encoder_gbtframe_intlver.vhd \
  GBT/gbt_code/gbt_tx_encoder_gbtframe_rsencode.vhd \
  GBT/gbt_code/gbt_tx_gearbox_FELIX.vhd \
  GBT/gbt_code/gbt_tx_scrambler_16bit.vhd \
  GBT/gbt_code/gbt_tx_scrambler_21bit.vhd \
  GBT/gbt_code/gbt_tx_scrambler_FELIX.vhd \
  GBT/gbt_code/gbt_tx_timedomaincrossing_FELIX.vhd \
  GBT/gbt_code/gbtRx_FELIX.vhd \
  GBT/gbt_code/gbtTx_FELIX.vhd \
  GBT/gbt_code/gbtTxRx_FELIX.vhd \
  LinkWrapper/link_wrapper.vhd \
  GBT/gth_code/qpll4p8g4ch_V7/GTH_QPLL_Wrapper_V7.vhd \
  GBT/gth_code/cpll4p8g1ch_V7/GTH_CPLL_Wrapper_V7.vhd \
  GBT/gth_code/cpll4p8g1ch_KCU/GTH_CPLL_Wrapper.vhd \
  GBT/gth_code/qpll4p8g4ch_KCU/GTH_QPLL_Wrapper.vhd \
  GBT/gbt_code/FELIX_gbt_wrapper.vhd \
  GBT/gth_code/qpll4p8g4ch_VUP/GTH_QPLL_Wrapper_VUP.vhd \
  GBT/gbt_code/FELIX_gbt_wrapper_VUP.vhd \
  GBT/gbt_code/FELIX_gbt_wrapper_Versal.vhd]


set XCI_FILES_VU37P [concat $XCI_FILES_VU37P \
  VUP_RXBUF_PMA_QPLL_4CH.xci]
  #KCU_NORXBUF_PCS_CPLL_1CH.xci \
  #KCU_RXBUF_PMA_CPLL_1CH.xci \
  

set XCI_FILES_V7 [concat $XCI_FILES_V7 \
  gtwizard_QPLL_4p8g_V7.xci \
  gtwizard_CPLL_4p8g_V7.xci]

set XCI_FILES_KU [concat $XCI_FILES_KU \
  KCU_NORXBUF_PCS_CPLL_1CH.xci \
  KCU_RXBUF_PMA_CPLL_1CH.xci \
  KCU_RXBUF_PMA_QPLL_4CH.xci]
  
set BD_FILES_BNL181 [concat $BD_FILES_BNL181 \
  transceiver_versal_gbt.bd]
  
set BD_FILES_BNL182 [concat $BD_FILES_BNL182 \
  transceiver_versal_gbt.bd]

set BD_FILES_FLX155 [concat $BD_FILES_FLX155 \
  transceiver_versal_gbt.bd]

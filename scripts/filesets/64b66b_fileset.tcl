
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               mtrovato
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set VHDL_FILES [concat $VHDL_FILES \
  64b66b/package_64b66b.vhd \
  64b66b/laneInit_64b66b.vhd \
  64b66b/laneInit_sm_64b66b.vhd \
  64b66b/block_sync_sm_64b66b.vhd \
  64b66b/aggregator_64b66b_RD53A.vhd \
  64b66b/aggregator_64b66b_RD53B.vhd \
  64b66b/symbolDecoding_64b66b.vhd \
  64b66b/decoding_64b66b.vhd \
  64b66b/decoding_64b66b_RD53A.vhd\
  64b66b/descrambler_64b66b.vhd \
  64b66b/decodingGearbox_64b66b.vhd \
  64b66b/gearbox32to64_64b66b.vhd \
  64b66b/remapEpaths_64b66b.vhd \
  64b66b/split64bword_64b66b.vhd \
  64b66b/deskew_64b66b.vhd \
  64b66b/cntRcvdPckts_64b66b.vhd\
  64b66b/aggregator_wrapper.vhd\
  64b66b/deskew_wrapper.vhd\
  64b66b/mux1_64b66b.vhd\
  64b66b/mux2_64b66b.vhd\
]

#set XCI_FILES [concat $XCI_FILES \
#  ila_pixellinklpgbt_1.xci \
#  ila_pixellinklpgbt_2.xci \
#  ila_pixellinklpgbt_3.xci \
#]

set MEM_FILES [concat $MEM_FILES \
  GBTlinksDataEmulator/RD53B/lpGBT_RD53B_ToHostemuram_00.mem\
  GBTlinksDataEmulator/RD53B/lpGBT_RD53B_ToHostemuram_01.mem\
  GBTlinksDataEmulator/RD53B/lpGBT_RD53B_ToHostemuram_02.mem\
  GBTlinksDataEmulator/RD53B/lpGBT_RD53B_ToHostemuram_03.mem\
  GBTlinksDataEmulator/RD53B/lpGBT_RD53B_ToHostemuram_04.mem\
  GBTlinksDataEmulator/RD53B/lpGBT_RD53B_ToHostemuram_05.mem\
  GBTlinksDataEmulator/RD53B/lpGBT_RD53B_ToHostemuram_06.mem\
  GBTlinksDataEmulator/RD53B/lpGBT_RD53B_ToHostemuram_10.mem\
  GBTlinksDataEmulator/RD53B/lpGBT_RD53B_ToHostemuram_11.mem\
  GBTlinksDataEmulator/RD53B/lpGBT_RD53B_ToHostemuram_12.mem\
  GBTlinksDataEmulator/RD53B/lpGBT_RD53B_ToHostemuram_13.mem\
  GBTlinksDataEmulator/RD53B/lpGBT_RD53B_ToHostemuram_14.mem\
  GBTlinksDataEmulator/RD53B/lpGBT_RD53B_ToHostemuram_15.mem\
  GBTlinksDataEmulator/RD53B/lpGBT_RD53B_ToHostemuram_16.mem]

set SIM_FILES [concat $SIM_FILES \
  64b66b/Decoding_pixel_RD53B_tb_noUVVM_descoped.vhd\
  64b66b/Decoding_pixel_tb_noUVVM_descoped.vhd\
  64b66b/package_pixel.vhd]
  
set WCFG_FILES [concat $WCFG_FILES \
  64b66b/Decoding_pixel_RD53B_tb_behav.wcfg\
  64b66b/Decoding_pixel_tb_behav.wcfg]


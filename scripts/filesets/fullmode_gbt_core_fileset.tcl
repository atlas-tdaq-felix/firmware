
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set VHDL_FILES [concat $VHDL_FILES \
  packages/FELIX_gbt_package.vhd \
  GBT/gbt_code/gbt_tx_encoder_FELIX.vhd \
  GBT/gbt_code/gbt_tx_encoder_gbtframe_polydiv.vhd \
  GBT/gbt_code/gbt_tx_encoder_gbtframe_intlver.vhd \
  GBT/gbt_code/gbt_tx_encoder_gbtframe_rsencode.vhd \
  GBT/gbt_code/gbt_tx_gearbox_FELIX.vhd \
  GBT/gbt_code/gbt_tx_scrambler_16bit.vhd \
  GBT/gbt_code/gbt_tx_scrambler_21bit.vhd \
  GBT/gbt_code/gbt_tx_scrambler_FELIX.vhd \
  GBT/gbt_code/gbt_tx_timedomaincrossing_FELIX.vhd \
  GBT/gbt_code/gbtTx_FELIX.vhd \
  FullModeWrapper/FELIX_FM_gbt_wrapper.vhd \
  FullModeWrapper/fullmode_auto_rxreset.vhd \
  FullModeWrapper/gth_fullmode_wrapper_48g_vup.vhd \
  FullModeWrapper/gth_fullmode_wrapper_vup.vhd \
  FullModeWrapper/gth_fullmode_wrapper_48g_v7.vhd \
  FullModeWrapper/gth_fullmode_wrapper_v7.vhd \
  FullModeWrapper/gth_fullmode_wrapper_48g_ku.vhd \
  FullModeWrapper/gth_fullmode_wrapper_ku.vhd ]


set XCI_FILES_V7 [concat $XCI_FILES_V7 \
  gtwizard_fullmode_cpll.xci]
#  gtwizard_fullmode_cpll_48g_v7.xci \


set XCI_FILES_KU [concat $XCI_FILES_KU \
  gtwizard_fullmode_cpll_48g_ku.xci \
  gtwizard_fullmode_cpll_ku.xci]

set XCI_FILES_VU37P [concat $XCI_FILES_VU37P \
  gtwizard_fullmode_cpll_48g_vup.xci \
  gtwizard_fullmode_cpll_vup.xci]
  
set XCI_FILES_VU9P [concat $XCI_FILES_VU9P \
  gtwizard_fullmode_cpll_48g_vup.xci \
  gtwizard_fullmode_cpll_vup.xci]
  
set BD_FILES_BNL181 [concat $BD_FILES_BNL181 \
  transceiver_versal_full.bd]
  
set BD_FILES_BNL182 [concat $BD_FILES_BNL182 \
  transceiver_versal_full.bd]
  
set BD_FILES_FLX155 [concat $BD_FILES_FLX155 \
  transceiver_versal_full.bd]
  
set SIM_FILES [concat $SIM_FILES \
  UVVMtests/tb/FULL_FMEMU_Link_tb.vhd \
  ../sources/felixUserSupport/fullmodetransceiver_gth_gth/FM_transceiver_BNL711_GBTin_FMout.vhd \
  ../sources/FullModeEmulator/Frontend_TTC_LTI_Receiver.vhd \
  ../sources/felixUserSupport/packages/FMTransceiverPackage.vhd \
  ../sources/GBT/gbt_code/FELIX_gbt_wrapper_no_gth.vhd \
  ../sources/GBT/gbt_code/FELIX_GBT_RXSLIDE_FSM.vhd \
  ../sources/GBT/gbt_code/FELIX_GBT_RX_AUTO_RST.vhd \
  ../sources/GBT/gbt_code/gbt_rx_decoder_FELIX.vhd \
  ../sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_chnsrch.vhd \
  ../sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_deintlver.vhd \
  ../sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_elpeval.vhd \
  ../sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_errlcpoly.vhd \
  ../sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_lmbddet.vhd \
  ../sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_rs2errcor.vhd \
  ../sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_rsdec.vhd \
  ../sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_rsdec_sync.vhd \
  ../sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_syndrom.vhd \
  ../sources/GBT/gbt_code/gbt_rx_descrambler_16bit.vhd \
  ../sources/GBT/gbt_code/gbt_rx_descrambler_21bit.vhd \
  ../sources/GBT/gbt_code/gbt_rx_descrambler_FELIX.vhd \
  ../sources/GBT/gbt_code/gbt_rx_gearbox_FMEMU.vhd \
  ../sources/GBT/gbt_code/gbtRx_FMEMU.vhd \
  ../sources/ip_cores/sim/fullmodetransceiver_core_sim_netlist.vhdl \
  ]

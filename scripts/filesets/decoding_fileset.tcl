
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Marco
#               mtrovato
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set VHDL_FILES [concat $VHDL_FILES \
  packages/axi_stream_package.vhd \
  packages/code6b8b_package.vhd \
  packages/FELIX_package.vhd \
  templates/generated/pcie_package.vhd \
  LpGBT/LpGBT_CERN/BE/lpgbt-fpga/lpgbtfpga_package.vhd \
  packages/FELIX_gbt_package.vhd \
  decoding/decoding.vhd \
  decoding/FullToAxis.vhd \
  decoding/dec_8b10b.vhd \
  decoding/ByteToAxiStream.vhd \
  decoding/ByteToAxiStream32b.vhd \
  decoding/DecoderHDLC.vhd \
  decoding/DecodingPixelLinkLPGBT.vhd \
  decoding/DecodingBCMLinkLPGBT.vhd \
  decoding/DecodingEpathGBT.vhd \
  decoding/AlignmentPulseGen.vhd \
  decoding/Decoder8b10b.vhd \
  decoding/DecodingEgroupGBT.vhd \
  decoding/DecEgroup_8b10b.vhd \
  decoding/DecodingGearBox.vhd \
  shared/pulse_fall_pw01.vhd \
  shared/pulse_pdxx_pwxx.vhd \
  CRC20/crc.vhd \
  decoding/DecodingGearBox.vhd \
  AxisUtils/Axis32Fifo.vhd \
  ttc/ttc_busy/BusyVirtualElink.vhd \
  ttc/ttc_busy/TTCToHostVirtualElink.vhd \
  HGTD/encoder_6b8b.vhd \
  HGTD/decoder_6b8b.vhd \
  HGTD/align_6b8b.vhd \
  HGTD/HGTDLumiDebugDataSource.vhd \
  HGTD/HGTDLumiAggregatorChannel.vhd \
  HGTD/HGTDLumiAggregator.vhd \
  HGTD/HGTDLumiEvent.vhd \
  HGTD/HGTDLumiQuadDecoder.vhd]

  
set SIM_FILES [concat $SIM_FILES \
  UVVMtests/tb/crc20_datagen.vhd \
  UVVMtests/tb/crc20_tb.vhd \
  UVVMtests/tb/decodingpixel_tb.vhd \
  UVVMtests/tb/decodingpixelRD53B_tb.vhd \
  UVVMtests/tb/DecodingGearBox_tb.vhd \
  UVVMtests/tb/GBTLinkToHost_tb.vhd \
  UVVMtests/tb/GBTCrCoding_tb.vhd \
  UVVMtests/tb/FULLModeToHost_tb.vhd \
  UVVMtests/tb/FELIXDataSink.vhd \
  UVVMtests/tb/FELIXDataSource.vhd \
  UVVMtests/tb/BusyVirtualElink_tb.vhd \
  UVVMtests/tb/ByteToAxiStream_tb.vhd \
  UVVMtests/tb/DecEgroup_8b10b_tb.vhd \
  UVVMtests/tb/DecEgroup_8b10b_Strips_tb.vhd \
  UVVMtests/tb/DecEgroup_8b10b_framegen.vhd \
  UVVMtests/tb/lpGBTLinkToHost_tb.vhd \
  UVVMtests/tb/TTCToHostVirtualElink_tb.vhd \
  UVVMtests/tb/HGTDLumiDataSource.vhd \
  UVVMtests/tb/HGTDLumiAggregator_tb.vhd \
  UVVMtests/tb/HDLCDataPath_tb.vhd]
  
set XCI_FILES_KU [concat $XCI_FILES_KU \
  axis32_fifo_bif.xci]
  #ila_decegrp8b10b.xci]

set XCI_FILES_V7 [concat $XCI_FILES_V7 \
  axis32_fifo_bif.xci]

set XCI_FILES_VU37P [concat $XCI_FILES_VU37P \
  axis32_fifo_bif.xci]

set XCI_FILES_VU9P [concat $XCI_FILES_VU9P \
  axis32_fifo_bif.xci]


#set VHDL_FILES_V7 [concat $VHDL_FILES_V7 \
#  pcie/data_width_package_256.vhd]
#  
#set VHDL_FILES_KU [concat $VHDL_FILES_KU \
#  pcie/data_width_package_256.vhd]
#  
#set VHDL_FILES_VU9P [concat $VHDL_FILES_VU9P \
#  pcie/data_width_package_512.vhd]
#  
#set VHDL_FILES_VU37P [concat $VHDL_FILES_VU37P \
#  pcie/data_width_package_512.vhd]
#  
#set VHDL_FILES_VERSAL [concat $VHDL_FILES_VERSAL \
#  pcie/data_width_package_512.vhd]

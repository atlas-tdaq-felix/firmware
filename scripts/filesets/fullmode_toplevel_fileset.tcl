
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
#               Rene
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set VHDL_FILES [concat $VHDL_FILES \
  FelixTop/felix_top.vhd \
  packages/axi_stream_package.vhd]
  
set XDC_FILES_VC709 [concat $XDC_FILES_VC709 \
  felix_sfp_VC709.xdc \
  felix_top_VC709.xdc]

set XDC_FILES_HTG710 [concat $XDC_FILES_HTG710 \
  felix_gbt_cxp_HTG710.xdc \
  felix_gbt_cxp_copper_HTG710.xdc \
  felix_top_HTG710.xdc]

set XDC_FILES_BNL711 [concat $XDC_FILES_BNL711 \
  felix_top_BNL711.xdc]
  
set XDC_FILES_BNL712 [concat $XDC_FILES_BNL712 \
  felix_top_BNL712.xdc]

set XDC_FILES_VCU128 [concat $XDC_FILES_VCU128 \
  felix_top_VCU128.xdc]
  
set XDC_FILES_BNL801 [concat $XDC_FILES_BNL801 \
   BNL_ph2_v1_VU9P.xdc]

set XDCUNMANAGED_FILES [concat $XDCUNMANAGED_FILES \
  timing_constraints.xdc]


# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Kai Chen
#               Frans Schreuder
#               Elena Zhivun
#               mtrovato
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set VHDL_FILES [concat $VHDL_FILES \
  LpGBT/LpGBT_FELIX/FELIX_LpGBT_Wrapper.vhd \
  LpGBT/LpGBT_FELIX/FLX_LpGBT_BE.vhd \
  LpGBT/LpGBT_FELIX/FLX_LpGBT_BE_Wrapper.vhd \
  LpGBT/LpGBT_FELIX/RefClk_Gen.vhd \
  LpGBT/LpGBT_FELIX/Regs_RW.vhd \
  LpGBT/LpGBT_FELIX/lpgbtfpga_uplink.vhd \
  LpGBT/LpGBT_FELIX/lpgbtfpga_framealigner.vhd \
  LpGBT/LpGBT_FELIX/lpgbtfpga_rxgearbox.vhd \
  LpGBT/LpGBT_FELIX/lpgbtemul_top.vhd \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/mgt_frameAligner_pv.vhd \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/txgearbox.vhd]

set XCI_FILES_V7 [concat $XCI_FILES_V7 \
  fifo_generator_felix.xci \
  VC7_PMA_QPLL_4CH_LPGBT.xci]

set XCI_FILES_KU [concat $XCI_FILES_KU \
  fifo_generator_felix.xci \
  KCU_RXBUF_PMA_QPLL_FE4CH_LPGBT.xci \
  KCU_RXBUF_PMA_QPLL_4CH_LPGBT.xci \
  KCU_PMA_QPLL_4CH_LPGBT.xci]

set XCI_FILESVU37P [concat $XCI_FILES_VU37P \
  rxclkgen.xci \
  fifo_generator_felix.xci \
  KCU_RXBUF_PMA_QPLL_FE4CH_LPGBT.xci \
  KCU_RXBUF_PMA_QPLL_4CH_LPGBT.xci]

set XCI_FILESVU9P [concat $XCI_FILES_VU9P \
  rxclkgen.xci \
  fifo_generator_felix.xci \
  KCU_RXBUF_PMA_QPLL_FE4CH_LPGBT.xci \
  KCU_RXBUF_PMA_QPLL_4CH_LPGBT.xci]


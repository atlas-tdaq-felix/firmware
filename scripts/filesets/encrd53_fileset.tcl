
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               mtrovato
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set VHDL_FILES [concat $VHDL_FILES \
  RD53/rd53_package.vhd \
  RD53/ENCRD53.vhd \
  RD53/newtriggerunit.vhd \
  RD53/sync_timer.vhd \
  RD53/cmd_top_v1.vhd \
  RD53/cmd_top_v2.vhd \
  RD53/az_controller.vhd \
  RD53/RD53Interface.vhd \
  RD53/RD53B_CommandErrors.vhd \
  RD53/RD53A/RD53A_DebuggingModule.vhd \
  RD53/RD53A/RD53A_CntIssuedTrigger.vhd \
  RD53/RD53A/RD53A_CntIssuedCommand.vhd \
  RD53/RD53A/RD53A_CntIssuedGivenCommand.vhd \
  RD53/RD53A/RD53A_CntIssuedCommand8b.vhd \
  RD53/RD53A/RD53A_MsrGenCalToTrigLatency.vhd \
  RD53/RD53A/RD53A_MsrTrigFreq.vhd \
  RD53/RD53A/RD53A_CommandErrors.vhd \
  RD53/RD53B_TriggerLoop.vhd]
  

set XCI_FILES [concat $XCI_FILES \
  dsp_counter.xci \
]
#  fifo_generator_0_ENCRRD53A.xci]
#  ila_encoder_firmtriggers.xci]

#simulation/FELIX_Top/Encoding_pixel_tb_behav.wcfg
#simulation/FELIX_Top/Encoding_pixel_tb_noUVVM.vhd


# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Kazuki Todome
#               Nico Giangiacomi
#               Marius Wensing
#               mtrovato
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set VHDL_FILES [concat $VHDL_FILES \
  packages/axi_stream_package.vhd \
  packages/FELIX_package.vhd \
  ItkStrip/strips_package.vhd \
  templates/generated/pcie_package.vhd \
  LpGBT/LpGBT_CERN/BE/lpgbt-fpga/lpgbtfpga_package.vhd \
  packages/FELIX_gbt_package.vhd \
  encoding/encoding.vhd \
  encoding/enc_8b10b.vhd \
  encoding/HGTD_Altiroc_fastcmd.vhd \
  encoding/AxiStreamToByte.vhd \
  encoding/EncoderHDLC.vhd \
  encoding/EncodingEpathGBT.vhd \
  encoding/EncodingEpathLPGBT.vhd \
  encoding/Encoder8b10b.vhd \
  encoding/EncoderFEI4.vhd \
  encoding/EncodingEgroupGBT.vhd \
  encoding/EncodingEgroupLPGBT.vhd \
  encoding/EncodingGearBox.vhd \
  encoding/EncoderTTC.vhd \
  encoding/ExtendedTestPulse.vhd \
  encoding/XoffMapping.vhd \
  shared/pulse_fall_pw01.vhd \
  shared/pulse_pdxx_pwxx.vhd \
  encoding/MUX8_Nbit.vhd \
  CRC20/crc.vhd \
  AxisUtils/Axis8Fifo.vhd \
  AxisUtils/SRL16_FIFO.vhd \
  encoding/LTI_FE_Transmitter.vhd \
  encoding/CRC16_LTI.vhd]
  
set SIM_FILES [concat $SIM_FILES \
  ../sources/decoding/dec_8b10b.vhd \
  ../sources/decoding/DecoderHDLC.vhd \
  UVVMtests/tb/HGTD_fastcmd_tb.vhd \
  UVVMtests/tb/EncodingEpath_tb.vhd \
  UVVMtests/tb/XcodingEpath_tb.vhd \
  UVVMtests/tb/validate_8b10b_tb.vhd \
  UVVMtests/tb/lookup_8b10b.vhd \
  ../sources/FullModeEmulator/Frontend_TTC_LTI_Receiver.vhd \
  UVVMtests/tb/LTI_FE_Transmitter_tb.vhd \
  UVVMtests/tb/HDLCDataPath_tb.vhd]

  
set XCI_FILES [concat $XCI_FILES \
  axi8_fifo_bif.xci \
  axi8_fifo_bram.xci]

#set VHDL_FILES_V7 [concat $VHDL_FILES_V7 \
#  pcie/data_width_package_256.vhd]
#  
#set VHDL_FILES_KU [concat $VHDL_FILES_KU \
#  pcie/data_width_package_256.vhd]
#  
#set VHDL_FILES_VU9P [concat $VHDL_FILES_VU9P \
#  pcie/data_width_package_512.vhd]
#  
#set VHDL_FILES_VU37P [concat $VHDL_FILES_VU37P \
#  pcie/data_width_package_512.vhd]

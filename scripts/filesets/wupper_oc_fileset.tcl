
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Nayib Boukadida
#               Alessandro Thea
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#FPGA series specific files 
  
set VHDL_FILES [concat $VHDL_FILES \
  opencores/wupper_oc_top.vhd]
 
set XDC_FILES_VCU128 [concat $XDC_FILES_VCU128 \
  wupper_oc_top_vcu128.xdc]
  
set XDC_FILES_XUPP3R_VU9P [concat $XDC_FILES_XUPP3R_VU9P \
  xupp3r_vu9p.xdc]

set XDC_FILES_VMK180 [concat $XDC_FILES_VMK180 \
  felix_top_VMK180.xdc]

set XDC_FILES_BNL181 [concat $XDC_FILES_BNL181 \
  felix_top_BNL181.xdc]

set XDC_FILES_BNL182 [concat $XDC_FILES_BNL182 \
  felix_top_FLX182.xdc]
  
set XCI_FILES [concat $XCI_FILES \
  fh_dout_ila.xci]

set XDC_FILES_VPK120 [concat $XDC_FILES_VPK120 \
  felix_top_vpk120.xdc]
  
set XDC_FILES_FLX155 [concat $XDC_FILES_FLX155 \
  felix_top_FLX155.xdc]
  
set XDC_FILES_VC709 [concat $XDC_FILES_VC709 \
  pcie_dma_top_VC709.xdc]
  
set XDCUNMANAGED_FILES [concat $XDCUNMANAGED_FILES \
  timing_constraints.xdc]


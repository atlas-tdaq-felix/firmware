
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               mtrovato
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set VHDL_FILES [concat $VHDL_FILES \
  packages/FELIX_package.vhd \
  ItkStrip/strips_package.vhd \
  templates/generated/pcie_package.vhd \
  shared/xadc_drp.vhd \
  shared/dna.vhd \
  housekeeping/housekeeping_module.vhd \
  housekeeping/i2c_interface.vhd \
  housekeeping/clock_and_reset.vhd \
  housekeeping/GenericConstantsToRegs.vhd \
  i2c_master/i2c.vhd \
  housekeeping/gc_multichannel_frequency_meter.vhd \
  i2c_master/I2C_Master_PEX.vhd \
  spi/LMK03200_spi.vhd \
  spi/LMK03200_wrapper.vhd \
  shared/pex_init.vhd \
  flash/flash_wrapper.vhd \
  flash/flash_ipcore_bnl.vhd]
  
#set VHDL_FILES_KU [concat $VHDL_FILES_KU \
#  pcie/data_width_package_256.vhd]
#  
#set VHDL_FILES_V7 [concat $VHDL_FILES_V7 \
#  pcie/data_width_package_256.vhd]
#  
#set VHDL_FILES_VU9P [concat $VHDL_FILES_VU9P \
#  pcie/data_width_package_512.vhd]
#  
#set VHDL_FILES_VU37P [concat $VHDL_FILES_VU37P \
#  pcie/data_width_package_512.vhd]
#  
#set VHDL_FILES_VERSAL [concat $VHDL_FILES_VERSAL \
#  pcie/data_width_package_512.vhd]
  
set SIM_FILES [concat $SIM_FILES \
  UVVMtests/tb/i2c_tb.vhd \
  ../sources/ip_cores/BNL182/ip_repo/versal_network_device_1.0/hdl/versal_network_device_tb.vhd \
  ../sources/ip_cores/BNL182/ip_repo/versal_network_device_1.0/hdl/versal_network_device_v1_0_S00_AXI.vhd \
  ../sources/ip_cores/BNL182/ip_repo/versal_network_device_1.0/hdl/versal_network_device_v1_0.vhd]

set XCI_FILES [concat $XCI_FILES \
  clk_wiz_40_0.xci \
  clk_wiz_200_0.xci \
  clk_wiz_156_0.xci \
  clk_wiz_100_0.xci \
  clk_wiz_250.xci]

#Kintex ultrascale only
set XCI_FILES_KU [concat $XCI_FILES_KU \
  system_management_wiz_0.xci]

set XCI_FILES_VU37P [concat $XCI_FILES_VU37P \
  system_management_wiz_0.xci]

set XCI_FILES_VU9P [concat $XCI_FILES_VU9P \
  system_management_wiz_0.xci]

  
set VHDL_FILES_KU [concat $VHDL_FILES_KU \
  ip_cores/kintexUltrascale/xadc_wiz_0_stub.vhdl]

set VHDL_FILES_VU37P [concat $VHDL_FILES_VU37P \
  ip_cores/kintexUltrascale/xadc_wiz_0_stub.vhdl]

set VHDL_FILES_VU9P [concat $VHDL_FILES_VU9P \
  ip_cores/kintexUltrascale/xadc_wiz_0_stub.vhdl]

#Virtex 7 only
set XCI_FILES_V7 [concat $XCI_FILES_V7 \
  xadc_wiz_0.xci]
  
set VHDL_FILES_V7 [concat $VHDL_FILES_V7 \
  ip_cores/virtex7/system_management_wiz_0_stub.vhdl]
  
#Versal only, we need the CIPS block somewhere. For now it only provides the PCIe reset.
set BD_FILES_VMK180 [concat $BD_FILES_VMK180 \
  cips_bd.bd]
  
#Versal only, we need the CIPS block somewhere. For now it only provides the PCIe reset.
set BD_FILES_BNL181 [concat $BD_FILES_BNL181 \
  cips_bd_BNL181.bd]
  
#Versal only, we need the CIPS block somewhere. For now it only provides the PCIe reset.
set BD_FILES_BNL182 [concat $BD_FILES_BNL182 \
  cips_bd_BNL182.bd]

set XCI_FILES_VERSAL [concat $XCI_FILES_VERSAL \
  clk_wiz_200_se.xci]

set XCI_FILES_VERSALPREMIUM [concat $XCI_FILES_VERSALPREMIUM \
  clk_wiz_200_se.xci]


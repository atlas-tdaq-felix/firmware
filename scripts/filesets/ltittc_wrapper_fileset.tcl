set VHDL_FILES [concat $VHDL_FILES \
  ttc/ltittc_wrapper/FLX_LTITTCLink_Wrapper.vhd \
  ttc/ltittc_wrapper/gth_ltittclink_wrapper_ku.vhd \
  ttc/ltittc_wrapper/gty_ltittclink_wrapper_versal.vhd \
  ttc/ltittc_wrapper/ltittc_wrapper.vhd \
  ttc/ltittc_wrapper/ltittc_decoder.vhd \
  ttc/ltittc_wrapper/ltittc_wavegen.vhd \
  ttc/ltittc_wrapper/ltittc_transmitter.vhd \
  ttc/ltittc_wrapper/ltittc_routing.vhd \
  ttc/ltittc_wrapper/ltittc_mon_counters.vhd \
  ttc/ltittc_wrapper/ltittc_monitoring.vhd \
  ttc/ltittc_wrapper/crc16_ltittc.vhd \
  ttc/ltittc_wrapper/LTI_FE_Sync.vhd \
  ttc/ltittc_wrapper/LTI_Xoff_CRC.vhd \
  ttc/ltittc_wrapper/LTI_CRC_check.vhd \
  TTCdataEmulator/LTITTC_Emulator.vhd \
  encoding/ExtendedTestPulse.vhd \
  encoding/CRC16_LTI.vhd \
  ttc/itk_trigtag_generator.vhd \
  decoding/dec_8b10b_4x.vhd ]
 
set XCI_FILES_KU [concat $XCI_FILES_KU \
  gtwizard_ttc_rxcpll.xci \
  ]

#set XCI_FILES [concat $XCI_FILES \
#  gtwizard_ttc_rxcpll.xci \
#  wavegen_dsp_counter.xci # \
#  apb3_vio.xci ]

set BD_FILES_BNL182 [concat $BD_FILES_BNL182 \
  transceiver_versal_LTITTC.bd]
  
set SIM_FILES [concat $SIM_FILES \
  UVVMtests/tb/ltittc_tb.vhd \
  LTITTC/PCK_CRC16_D16.vhd \
  LTITTC/PCK_CRC16_D32.vhd]

#read_xdc -ref LTI_FE_Sync ../../constraints/LTI_FE_Sync.xdc  
#read_xdc -ref LTI_40_Sync ../../constraints/LTI_40_Sync.xdc  
set RELATIVE_XDC_FILES [concat $RELATIVE_XDC_FILES \
  LTI_FE_Sync]

#  LTITTC/LTITTC_sim_data_generation.vhd \
#  LTITTC/LTITTC_sim_genRX.vhd \
#  LTITTC/LTITTC_sim_genRX_wrapper.vhd \
#  LTITTC/LTITTC_wrapper_sim.vhd \
#  LTITTC/edge_det.vhd \
#  LTITTC/ltittc_sim.vhd ]

#set WCFG_FILES [concat $WCFG_FILES \
#  LTITTC/LTITTC_wrapper_sim_behav.wcfg \
#  LTITTC/ltittc.wcfg]


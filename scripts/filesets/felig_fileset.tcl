# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Shelfali Saxena
#               mtrovato
#               Frans Schreuder
#               Ricardo Luz
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set VHDL_FILES [concat $VHDL_FILES \
  FELIG/data_generator/elink_data_emulator.vhd \
  FELIG/data_generator/elink_packet_generator.vhd \
  FELIG/data_generator/nsw_packet_generator.vhd \
  FELIG/data_generator/elink_printer.vhd \
  FELIG/data_generator/elink_printer_bit_feeder.vhd \
  FELIG/data_generator/elink_printer_printhead.vhd \
  FELIG/emulator/Emulator.vhd \
  FELIG/emulator/freq_counter.vhd \
  FELIG/emulator/mux_128_sync.vhd \
  FELIG/emulator/mux_16.vhd \
  FELIG/emulator/mux_8.vhd \
  FELIG/emulator/EmulatorWrapper.vhd \
  FELIG/packages/ip_lib.vhd \
  FELIG/packages/sim_lib.vhd \
  FELIG/packages/type_lib.vhd \
  FELIG/templates/LaneRegisterRemapper.vhd \
  FELIG/checkers/gbtword_checker.vhd \
  FELIG/felix_modified/ttc/ttc_decoder/ttc_decoder_core_felig.vhd \
  FELIG/felix_modified/ttc/ttc_decoder/ttc_decode_wrapper_felig.vhd \
  FELIG/PRandomDGen/randomd_gen.vhd \
  FELIG/felix_modified/centralRouter/upstreamEpathFifoWrap_felig.vhd \
  ttc/ttc_decoder/TTC_hamming_decoder_alme.vhd \
  FELIG/data_generator/elink_printer_v2.vhd \
  FELIG/data_generator/elink_printer_bit_feeder_v2.vhd \
  FELIG/aurora/64b66b_encoding.vhd \
  FELIG/aurora/64b66b_encoding_placeholder.vhd \
  FELIG/aurora/64b66b_wrapper_FELIG.vhd \
  CRToHost/ToHostAxiStreamController.vhd \
  CRToHost/CRresetManager.vhd \
  FELIG/data_generator/enc8b10_wrap.vhd \
  encoding/enc_8b10b.vhd \
  packages/axi_stream_package.vhd \
  FelixTop/felig_top_bnl712.vhd \
]

set VERILOG_FILES [concat $VERILOG_FILES \
  FELIG/aurora_will/aurora_64b66b_1TXch_64b66b_scrambler.v \
  FELIG/aurora_will/aurora_64b66b_1TXch_cdc_sync.v \
  FELIG/aurora_will/aurora_64b66b_1TXch_sym_gen.v \
  FELIG/aurora_will/aurora_64b66b_1TXch_tx_aurora_lane_simplex.v \
  FELIG/aurora_will/aurora_64b66b_1TXch_tx_err_detect_simplex.v \
  FELIG/aurora_will/aurora_64b66b_1TXch_tx_lane_init_sm_simplex.v \
  FELIG/aurora_will/aurora_64b66b_1TXch_wrapper.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/descrambler36bitOrder36.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/downLinkDeinterleaver.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/downLinkFECDecoder.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/downLinkRxDataPath.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/gf_add_3.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/gf_add_4.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/gf_add_5.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/gf_inv_3.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/gf_log_3.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/gf_mult_3.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/gf_multBy2_3.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/gf_multBy2_4.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/gf_multBy2_5.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/gf_multBy3_4.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/gf_multBy3_5.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/rs_decoder_N7K5.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/rs_encoder_N15K13.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/rs_encoder_N31K29.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/scrambler51bitOrder49.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/scrambler53bitOrder49.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/scrambler58bitOrder58.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/scrambler60bitOrder58.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/upLinkDataSelect.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/upLinkFECEncoder.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/upLinkInterleaver.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/upLinkScrambler.v \
  LpGBT/LpGBT_CERN/FE/lpgbt-emul/upLinkTxDataPath.v\
  ]


set VHDL_FILES_KU [concat $VHDL_FILES_KU \
  FELIG/LinkWrapper/link_wrapper_FELIG.vhd\
  FELIG/LinkWrapper/FELIX_gbt_wrapper_FELIGKCU.vhd\
  GBT/gth_code/qpll4p8g4ch_KCU/GTH_QPLL_Wrapper_FELIG.vhd \
  FELIG/LinkWrapper/FELIG_LpGBT_Wrapper.vhd\
  FELIG/LinkWrapper/FLX_LpGBT_FE_Wrapper_FELIG.vhd\
  FELIG/LinkWrapper/FLX_LpGBT_GTH_FE_FELIG.vhd\
  LpGBT/LpGBT_FELIX/FLX_LpGBT_FE.vhd\
  FELIG/LinkWrapper/LMK_FELIG_wrapper.vhd\
  FelixTop/felig_top_bnl712.vhd]

set XCI_FILES_KU [concat $XCI_FILES_KU \
  KCU_RXBUF_PMA_QPLLTXGTREF1_CPLLRXGTREF0.xci \
  KCU_TXQPLLREFCLK10g24_RXCPLLREFCLK05g12_FELIG4CH.xci \
  dsp_counter.xci \
  L1A_Fifo.xci \
  TTCtoHostData.xci \
  epath_fifo.xci \
  Distr_LUT_felig.xci \
  fifo_GBT2CR.xci \
  fifo_generator_fe.xci \
  rxclkgen.xci]
  #ila_link_frame.xci \
  #ila_downlink.xci \
  #ila_header.xci \
  #ila_lmk_signals.xci \
  #ila_signals_to_LMK.xci \
  #ila_tx128.xci \
  #ila_SM_LMK.xci \
  #]

set XDC_FILES_BNL712 [concat $XDC_FILES_BNL712 \
  felix_top_BNL712.xdc \
  timing_constraints_felig_gbt.xdc \
  timing_constraints_felig_lpgbt.xdc \
  felix_gbt_minipod_BNL712_transceiver_8ch.xdc \
  felix_gbt_minipod_BNL712_transceiver_24ch.xdc \
  felix_gbt_minipod_BNL712_transceiver_48ch.xdc]

#  felig_top_BNL712_v2.0.xdc \

set SIM_FILES [concat $SIM_FILES \
  FELIG/felig_sim_top_bnl712.vhd\
  FELIG/felig_sim_64b66b_decoding.vhd\
  FELIG/felig_lpgbt_sim.vhd\
  UVVMtests/tb/FELIGemulator_tb.vhd\
  UVVMtests/tb/FELIGlinkwrapperLPGBT_tb.vhd\
  UVVMtests/tb/FELIGexternaltrigger_tb.vhd\
]

set WCFG_FILES [concat $WCFG_FILES \
  FELIG/waveforms/FELIGemulator_tb_behav.wcfg\
  FELIG/waveforms/FELIGexternaltrigger_tb_behav.wcfg\
  FELIG/waveforms/FELIGlinkwrapperLPGBT_tb_behav.wcfg\
  FELIG/waveforms/felig_lpgbt_sim.wcfg\
  FELIG/waveforms/FELIG_phase2_behav.wcfg\
  ]
  

  

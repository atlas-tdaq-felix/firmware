set VHDL_FILES [concat $VHDL_FILES \
    packages/axi_stream_package.vhd \
    AxisUtils/Axis64Fifo.vhd \
    core1990_interlaken/sources/interlaken/transceiver/word_boundary_sync.vhd \
    core1990_interlaken/sources/interlaken/receiver/decoder.vhd \
    core1990_interlaken/sources/interlaken/receiver/deframing_burst.vhd \
    core1990_interlaken/sources/interlaken/receiver/sync67to67fifo.vhd \
    core1990_interlaken/sources/interlaken/receiver/lane_to_chan.vhd \
    core1990_interlaken/sources/interlaken/receiver/descrambler.vhd \
    core1990_interlaken/sources/interlaken/receiver/deframing_meta.vhd \
    core1990_interlaken/sources/interlaken/receiver/interlaken_receiver_channel.vhd \
    core1990_interlaken/sources/interlaken/receiver/interlaken_receiver.vhd \
    core1990_interlaken/sources/interlaken/packages/interlaken_package.vhd \
    core1990_interlaken/sources/interlaken/transmitter/framing_meta.vhd \
    core1990_interlaken/sources/interlaken/transmitter/interlaken_transmitter_channel.vhd \
    core1990_interlaken/sources/interlaken/transmitter/async66to264Fifo.vhd \
    core1990_interlaken/sources/interlaken/transmitter/interlaken_transmitter.vhd \
    core1990_interlaken/sources/interlaken/transmitter/encoder.vhd \
    core1990_interlaken/sources/interlaken/transmitter/scrambler.vhd \
    core1990_interlaken/sources/interlaken/transmitter/framing_burst.vhd \
    core1990_interlaken/sources/interlaken/crc/crc-24.vhd \
    core1990_interlaken/sources/interlaken/crc/crc-32.vhd \
    core1990_interlaken/sources/interlaken/transceiver/rxgearbox_64b67b.vhd \
    core1990_felix/interlaken_gty.vhd] 
    #core1990_interlaken/sources/interlaken/transceiver/transceiver_10g_64b67b_block_sync_sm.vhd \
    #core1990_interlaken/sources/interlaken/interface/interlaken_interface.vhd \
    #core1990_interlaken/sources/interlaken/transceiver/interlaken_gty.vhd

set XCI_FILES_VU37P [concat $XCI_FILES_VU37P \
    gty_ultrascale_ilkn_ttc0.xci]
    #../../core1990_interlaken/sources/ip_cores/VU37P/gtwizard_ultrascale_0.xci \
    clk_wiz_365.xci]

#set XCI_FILES_VERSAL [concat $XCI_FILES_VERSAL \
#    clk_wiz_365.xci]    

set XCI_FILES_VERSAL [concat $XCI_FILES_VERSAL \
  transceiver_versal_interlaken.xci]
    
set XCI_FILES_VERSALPREMIUM [concat $XCI_FILES_VERSALPREMIUM \
  transceiver_versal_interlaken.xci]

set SIM_FILES [concat $SIM_FILES \
    UVVMtests/tb/Loopback25G_tb.vhd]
#    ../sources/core1990_interlaken/sources/interlaken/test/Core1990_Test.vhd \
#    ../sources/core1990_interlaken/sources/interlaken/test/interlaken150G_wrapper.vhd \
#    ../sources/core1990_interlaken/sources/interlaken/test/interlaken_top_tb.vhd \
#    ../sources/core1990_interlaken/sources/interlaken/test/axis_data_generator.vhd \
#    ../sources/core1990_interlaken/sources/interlaken/test/pipeline.vhd \
#    ../sources/core1990_interlaken/sources/interlaken/test/data_generator.vhd]
    
#set VHDL_FILES_V7 [concat $VHDL_FILES_V7 
#set VHDL_FILES_KU [concat $VHDL_FILES_KU 
#set VHDL_FILES_VU37P [concat $VHDL_FILES_VU37P 
#set VHDL_FILES_VU9P [concat $VHDL_FILES_VU9P 
#set VHDL_FILES_VERSAL [concat $VHDL_FILES_VERSAL 




# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Jacopo Pinzino
#               jacopo pinzino
#               Frans Schreuder
#               mtrovato
#               Elena Zhivun
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set VHDL_FILES [concat $VHDL_FILES \
  Endeavour/EndeavourPackage.vhd \
  Endeavour/EndeavourDecoder.vhd \
  Endeavour/EndeavourEncoder.vhd \
  Endeavour/EncoderBlk.vhd \
  Endeavour/FrameMsg.vhd \
  Endeavour/FrameParser.vhd \
  Endeavour/EndeavourDeglitcher.vhd]


#set XCI_FILES [concat $XCI_FILES \
#  ila_endeavour_encoder.xci \
#  ila_endeavour_decoder.xci]


set SIM_FILES [concat $SIM_FILES \
  Endeavour/amac_demo_tb.vhd \
  Endeavour/tb_amac_deglitcher.vhd \
  Endeavour/tb_amac_decoder.vhd \
  Endeavour/tb_amac_encoder.vhd \
  Endeavour/amac_chip.vhd]

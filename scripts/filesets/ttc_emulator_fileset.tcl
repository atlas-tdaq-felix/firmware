
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Israel Grayzman
#               Frans Schreuder
#               Thei Wijnen
#               Ali Skaf
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


set VHDL_FILES [concat $VHDL_FILES \
  TTCdataEmulator/TTC_Emulator.vhd \
  TTCdataEmulator/delay_chain.vhd \
  TTCdataEmulator/signal_delay.vhd \
  TTCdataEmulator/hilo_detect.vhd \
  TTCdataEmulator/pulse_extender.vhd \
  packages/FELIX_package.vhd]



# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Mesfin Gebyehu
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# ----------------------------------------------------------
#FM EMU .vhd & .xci files
# ----------------------------------------------------------
set VHDL_FILES [concat $VHDL_FILES \
    felixUserSupport/fullmodetransceiver_gth_gth/FM_transceiver_BNL711_GBTin_FMout.vhd \
    packages/FELIX_package.vhd \
    packages/FELIX_gbt_package.vhd \
    FullModeEmulator/FMemuRAM.vhd \
    FullModeEmulator/FMEmu_FSM_mealy.vhd \
    FullModeEmulator/Frontend_TTC_LTI_Receiver.vhd \
    encoding/CRC16_LTI.vhd \
    CRC32/CRC32_v2.vhd \
    CRC20/crc.vhd \
    FullModeTransmitter/FMchannelTXctrl_emu.vhd \
    PRandomDGen/randomd_gen_fmemu.vhd \
    Xoff_decoder/8b10_dec_fmemu.vhd \
    Xoff_decoder/8b10_dec_wrap_fmemu.vhd \
    Xoff_decoder/InputShifterNb.vhd \
    GBT/gbt_code/FELIX_gbt_wrapper_no_gth.vhd \
    felixUserSupport/packages/FMTransceiverPackage.vhd \
    Xoff_decoder/Xoff_decoder_top.vhd \
    GBT/gbt_code/FELIX_GBT_RXSLIDE_FSM.vhd \
    GBT/gbt_code/FELIX_GBT_RX_AUTO_RST.vhd \
    GBT/gbt_code/gbt_rx_decoder_FELIX.vhd \
    GBT/gbt_code/gbt_rx_decoder_gbtframe_chnsrch.vhd \
    GBT/gbt_code/gbt_rx_decoder_gbtframe_deintlver.vhd \
    GBT/gbt_code/gbt_rx_decoder_gbtframe_elpeval.vhd \
    GBT/gbt_code/gbt_rx_decoder_gbtframe_errlcpoly.vhd \
    GBT/gbt_code/gbt_rx_decoder_gbtframe_lmbddet.vhd \
    GBT/gbt_code/gbt_rx_decoder_gbtframe_rs2errcor.vhd \
    GBT/gbt_code/gbt_rx_decoder_gbtframe_rsdec.vhd \
    GBT/gbt_code/gbt_rx_decoder_gbtframe_rsdec_sync.vhd \
    GBT/gbt_code/gbt_rx_decoder_gbtframe_syndrom.vhd \
    GBT/gbt_code/gbt_rx_descrambler_16bit.vhd \
    GBT/gbt_code/gbt_rx_descrambler_21bit.vhd \
    GBT/gbt_code/gbt_rx_descrambler_FELIX.vhd \
    GBT/gbt_code/gbt_rx_gearbox_FMEMU.vhd \
    GBT/gbt_code/gbtRx_FMEMU.vhd]
  
set XCI_FILES [concat $XCI_FILES \
    fullmodetransceiver_core.xci \
    DPram_32b.xci \
    Distr_LUT.xci \
    ila_gbt_rx_frame.xci \
    ila_lti_decoder.xci \
    ila_lti_rx_data.xci \
    vio_tclink_clk40_reset.xci \
    ]
